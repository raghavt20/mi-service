class com.android.server.MiuiBatteryServiceImpl$BatteryHandler extends android.os.Handler {
	 /* .source "MiuiBatteryServiceImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/MiuiBatteryServiceImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "BatteryHandler" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver; */
/* } */
} // .end annotation
/* # static fields */
private static final java.lang.String ACTION_HANDLE_BATTERY_STATE_CHANGED;
private static final java.lang.String ACTION_HANDLE_STATE_CHANGED;
private static final java.lang.String ACTION_MIUI_PC_BATTERY_CHANGED;
private static final java.lang.String ACTION_MOISTURE_DET;
private static final java.lang.String ACTION_POGO_CONNECTED_STATE;
private static final java.lang.String ACTION_REVERSE_PEN_CHARGE_STATE;
private static final java.lang.String ACTION_RX_OFFSET;
private static final java.lang.String ACTION_TYPE_C_HIGH_TEMP;
private static final java.lang.String ACTION_WIRELESS_CHARGING;
private static final java.lang.String ACTION_WIRELESS_CHG_WARNING_ACTIVITY;
private static final java.lang.String ACTION_WIRELESS_FW_UPDATE;
private static final java.lang.String CAR_APP_STATE_EVENT;
private static final java.lang.String CONNECTOR_TEMP_EVENT;
private static final java.lang.String EXTRA_CAR_CHG;
private static final java.lang.String EXTRA_HANDLE_CONNECT_STATE;
private static final java.lang.String EXTRA_HANDLE_VERSION;
private static final java.lang.String EXTRA_LOW_TX_OFFESET_STATE;
private static final java.lang.String EXTRA_MOISTURE_DET;
private static final java.lang.String EXTRA_NTC_ALARM;
private static final java.lang.String EXTRA_POGO_CONNECTED_STATE;
private static final java.lang.String EXTRA_POWER_MAX;
private static final java.lang.String EXTRA_REVERSE_PEN_CHARGE_STATE;
private static final java.lang.String EXTRA_REVERSE_PEN_SOC;
private static final java.lang.String EXTRA_RX_OFFSET;
private static final java.lang.String EXTRA_TYPE_C_HIGH_TEMP;
private static final java.lang.String EXTRA_WIRELESS_CHARGING;
private static final java.lang.String EXTRA_WIRELESS_FW_UPDATE;
private static final java.lang.String HANDLE_BATTERY_CHANGED;
private static final Integer HANDLE_STATE_CONNECTED;
private static final Integer HANDLE_STATE_DISCONNECTED;
private static final java.lang.String HAPTIC_STATE;
private static final java.lang.String HVDCP3_TYPE_EVENT;
private static final java.lang.String LOW_INDUCTANCE_OFFSET_EVENT;
private static final java.lang.String MOISTURE_DET;
static final Integer MSG_ADJUST_VOLTAGE;
static final Integer MSG_BATTERY_CHANGED;
static final Integer MSG_BLUETOOTH_CHANGED;
static final Integer MSG_CAR_APP_STATE;
static final Integer MSG_CHARGE_LIMIT;
static final Integer MSG_CHECK_TIME_REGION;
static final Integer MSG_CONNECTOR_TEMP;
static final Integer MSG_HANDLE_ATTACHED;
static final Integer MSG_HANDLE_DETACHED;
static final Integer MSG_HANDLE_LOW_TX_OFFSET;
static final Integer MSG_HANDLE_REBOOT;
static final Integer MSG_HANDLE_TEMP_STATE;
static final Integer MSG_HVDCP3_DETECT;
static final Integer MSG_MOISTURE_DET;
static final Integer MSG_NFC_DISABLED;
static final Integer MSG_NFC_ENABLED;
static final Integer MSG_NTC_ALARM;
static final Integer MSG_POWER_OFF;
static final Integer MSG_QUICKCHARGE_DETECT;
static final Integer MSG_REVERSE_PEN_CHG_STATE;
static final Integer MSG_RX_OFFSET;
static final Integer MSG_SCREEN_OFF;
static final Integer MSG_SCREEN_UNLOCK;
static final Integer MSG_SHUTDOWN_DELAY;
static final Integer MSG_SHUTDOWN_DELAY_WARNING;
static final Integer MSG_SOC_DECIMAL;
static final Integer MSG_WIRELESS_CHARGE_CLOSE;
static final Integer MSG_WIRELESS_CHARGE_OPEN;
static final Integer MSG_WIRELESS_FW_STATE;
static final Integer MSG_WIRELESS_TX;
private static final java.lang.String NFC_CLOED;
private static final java.lang.String NTC_ALARM_EVENT;
private static final java.lang.String POWER_COMMON_RECEIVER_PERMISSION;
private static final java.lang.String POWER_SUPPLY_CAPACITY;
private static final java.lang.String POWER_SUPPLY_STATUS;
private static final java.lang.String QUICK_CHARGE_TYPE_EVENT;
private static final Integer REDUCE_FULL_CHARGE_VBATT_10;
private static final Integer REDUCE_FULL_CHARGE_VBATT_15;
private static final Integer REDUCE_FULL_CHARGE_VBATT_20;
private static final Integer RESET_FULL_CHARGE_VBATT;
private static final Integer RETRY_UPDATE_DELAY;
private static final java.lang.String REVERSE_CHG_MODE_EVENT;
private static final java.lang.String REVERSE_CHG_STATE_EVENT;
private static final java.lang.String REVERSE_PEN_CHG_STATE_EVENT;
private static final java.lang.String REVERSE_PEN_MAC_EVENT;
private static final java.lang.String REVERSE_PEN_PLACE_ERR_EVENT;
private static final java.lang.String REVERSE_PEN_SOC_EVENT;
private static final java.lang.String RX_OFFSET_EVENT;
private static final java.lang.String SHUTDOWN_DELAY_EVENT;
private static final java.lang.String SOC_DECIMAL_EVENT;
private static final java.lang.String SOC_DECIMAL_RATE_EVENT;
private static final Integer UPDATE_DELAY;
private static final Integer WIRELESS_AUTO_CLOSED_STATE;
private static final Integer WIRELESS_CHG_ERROR_STATE;
private static final Integer WIRELESS_LOW_BATTERY_LEVEL_STATE;
private static final Integer WIRELESS_NO_ERROR_STATE;
private static final Integer WIRELESS_OTHER_WIRELESS_CHG_STATE;
private static final java.lang.String WIRELESS_REVERSE_CHARGING;
private static final java.lang.String WIRELESS_TX_TYPE_EVENT;
private static final java.lang.String WLS_FW_STATE_EVENT;
/* # instance fields */
private Integer mChargingNotificationId;
private Boolean mClosedNfcFromCharging;
private final android.content.ContentResolver mContentResolver;
private Integer mCount;
private java.util.Date mEndHighTempDate;
private com.android.server.MiuiBatteryServiceImpl$HandleInfo mHandleInfo;
private Boolean mHapticState;
private Boolean mIsReverseWirelessCharge;
private Integer mLastCloseReason;
private Integer mLastConnectorTemp;
private Integer mLastHvdcpType;
private Integer mLastMoistureDet;
private Integer mLastNtcAlarm;
private Integer mLastOffsetState;
private Integer mLastOpenStatus;
private Integer mLastPenReverseChargeState;
private java.lang.String mLastPenReverseMac;
private java.lang.String mLastPenReversePLaceErr;
private Integer mLastPenReverseSoc;
private Integer mLastPogoConnectedState;
private Integer mLastQuickChargeType;
private Integer mLastRxOffset;
private Integer mLastShutdownDelay;
private Integer mLastWirelessFwState;
private Integer mLastWirelessTxType;
private android.nfc.NfcAdapter mNfcAdapter;
private Boolean mRetryAfterOneMin;
private Boolean mShowDisableNfc;
private Boolean mShowEnableNfc;
private java.util.Date mStartHithTempDate;
private final android.os.UEventObserver mUEventObserver;
private Boolean mUpdateSocDecimal;
final com.android.server.MiuiBatteryServiceImpl this$0; //synthetic
/* # direct methods */
static com.android.server.MiuiBatteryServiceImpl$HandleInfo -$$Nest$fgetmHandleInfo ( com.android.server.MiuiBatteryServiceImpl$BatteryHandler p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHandleInfo;
} // .end method
static Integer -$$Nest$fgetmLastCloseReason ( com.android.server.MiuiBatteryServiceImpl$BatteryHandler p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastCloseReason:I */
} // .end method
static Integer -$$Nest$fgetmLastConnectorTemp ( com.android.server.MiuiBatteryServiceImpl$BatteryHandler p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastConnectorTemp:I */
} // .end method
static Integer -$$Nest$fgetmLastHvdcpType ( com.android.server.MiuiBatteryServiceImpl$BatteryHandler p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastHvdcpType:I */
} // .end method
static Integer -$$Nest$fgetmLastMoistureDet ( com.android.server.MiuiBatteryServiceImpl$BatteryHandler p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastMoistureDet:I */
} // .end method
static Integer -$$Nest$fgetmLastNtcAlarm ( com.android.server.MiuiBatteryServiceImpl$BatteryHandler p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastNtcAlarm:I */
} // .end method
static Integer -$$Nest$fgetmLastOffsetState ( com.android.server.MiuiBatteryServiceImpl$BatteryHandler p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastOffsetState:I */
} // .end method
static Integer -$$Nest$fgetmLastOpenStatus ( com.android.server.MiuiBatteryServiceImpl$BatteryHandler p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastOpenStatus:I */
} // .end method
static Integer -$$Nest$fgetmLastPenReverseChargeState ( com.android.server.MiuiBatteryServiceImpl$BatteryHandler p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastPenReverseChargeState:I */
} // .end method
static java.lang.String -$$Nest$fgetmLastPenReverseMac ( com.android.server.MiuiBatteryServiceImpl$BatteryHandler p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mLastPenReverseMac;
} // .end method
static java.lang.String -$$Nest$fgetmLastPenReversePLaceErr ( com.android.server.MiuiBatteryServiceImpl$BatteryHandler p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mLastPenReversePLaceErr;
} // .end method
static Integer -$$Nest$fgetmLastPenReverseSoc ( com.android.server.MiuiBatteryServiceImpl$BatteryHandler p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastPenReverseSoc:I */
} // .end method
static Integer -$$Nest$fgetmLastPogoConnectedState ( com.android.server.MiuiBatteryServiceImpl$BatteryHandler p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastPogoConnectedState:I */
} // .end method
static Integer -$$Nest$fgetmLastQuickChargeType ( com.android.server.MiuiBatteryServiceImpl$BatteryHandler p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastQuickChargeType:I */
} // .end method
static Integer -$$Nest$fgetmLastRxOffset ( com.android.server.MiuiBatteryServiceImpl$BatteryHandler p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastRxOffset:I */
} // .end method
static Integer -$$Nest$fgetmLastShutdownDelay ( com.android.server.MiuiBatteryServiceImpl$BatteryHandler p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastShutdownDelay:I */
} // .end method
static Integer -$$Nest$fgetmLastWirelessFwState ( com.android.server.MiuiBatteryServiceImpl$BatteryHandler p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastWirelessFwState:I */
} // .end method
static Integer -$$Nest$fgetmLastWirelessTxType ( com.android.server.MiuiBatteryServiceImpl$BatteryHandler p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastWirelessTxType:I */
} // .end method
static Boolean -$$Nest$fgetmUpdateSocDecimal ( com.android.server.MiuiBatteryServiceImpl$BatteryHandler p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mUpdateSocDecimal:Z */
} // .end method
static void -$$Nest$fputmLastCloseReason ( com.android.server.MiuiBatteryServiceImpl$BatteryHandler p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastCloseReason:I */
return;
} // .end method
static void -$$Nest$fputmLastConnectorTemp ( com.android.server.MiuiBatteryServiceImpl$BatteryHandler p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastConnectorTemp:I */
return;
} // .end method
static void -$$Nest$fputmLastHvdcpType ( com.android.server.MiuiBatteryServiceImpl$BatteryHandler p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastHvdcpType:I */
return;
} // .end method
static void -$$Nest$fputmLastMoistureDet ( com.android.server.MiuiBatteryServiceImpl$BatteryHandler p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastMoistureDet:I */
return;
} // .end method
static void -$$Nest$fputmLastNtcAlarm ( com.android.server.MiuiBatteryServiceImpl$BatteryHandler p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastNtcAlarm:I */
return;
} // .end method
static void -$$Nest$fputmLastOffsetState ( com.android.server.MiuiBatteryServiceImpl$BatteryHandler p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastOffsetState:I */
return;
} // .end method
static void -$$Nest$fputmLastOpenStatus ( com.android.server.MiuiBatteryServiceImpl$BatteryHandler p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastOpenStatus:I */
return;
} // .end method
static void -$$Nest$fputmLastPenReverseChargeState ( com.android.server.MiuiBatteryServiceImpl$BatteryHandler p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastPenReverseChargeState:I */
return;
} // .end method
static void -$$Nest$fputmLastPenReverseMac ( com.android.server.MiuiBatteryServiceImpl$BatteryHandler p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.mLastPenReverseMac = p1;
return;
} // .end method
static void -$$Nest$fputmLastPenReversePLaceErr ( com.android.server.MiuiBatteryServiceImpl$BatteryHandler p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.mLastPenReversePLaceErr = p1;
return;
} // .end method
static void -$$Nest$fputmLastPenReverseSoc ( com.android.server.MiuiBatteryServiceImpl$BatteryHandler p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastPenReverseSoc:I */
return;
} // .end method
static void -$$Nest$fputmLastPogoConnectedState ( com.android.server.MiuiBatteryServiceImpl$BatteryHandler p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastPogoConnectedState:I */
return;
} // .end method
static void -$$Nest$fputmLastQuickChargeType ( com.android.server.MiuiBatteryServiceImpl$BatteryHandler p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastQuickChargeType:I */
return;
} // .end method
static void -$$Nest$fputmLastRxOffset ( com.android.server.MiuiBatteryServiceImpl$BatteryHandler p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastRxOffset:I */
return;
} // .end method
static void -$$Nest$fputmLastShutdownDelay ( com.android.server.MiuiBatteryServiceImpl$BatteryHandler p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastShutdownDelay:I */
return;
} // .end method
static void -$$Nest$fputmLastWirelessFwState ( com.android.server.MiuiBatteryServiceImpl$BatteryHandler p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastWirelessFwState:I */
return;
} // .end method
static void -$$Nest$fputmLastWirelessTxType ( com.android.server.MiuiBatteryServiceImpl$BatteryHandler p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastWirelessTxType:I */
return;
} // .end method
static void -$$Nest$fputmUpdateSocDecimal ( com.android.server.MiuiBatteryServiceImpl$BatteryHandler p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mUpdateSocDecimal:Z */
return;
} // .end method
static void -$$Nest$msendHandleBatteryStatsChangeBroadcast ( com.android.server.MiuiBatteryServiceImpl$BatteryHandler p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendHandleBatteryStatsChangeBroadcast()V */
return;
} // .end method
public com.android.server.MiuiBatteryServiceImpl$BatteryHandler ( ) {
/* .locals 2 */
/* .param p1, "this$0" # Lcom/android/server/MiuiBatteryServiceImpl; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 667 */
this.this$0 = p1;
/* .line 668 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 649 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastOffsetState:I */
/* .line 650 */
/* iput v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastNtcAlarm:I */
/* .line 653 */
/* iput-boolean v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mShowEnableNfc:Z */
/* .line 654 */
/* iput-boolean v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mShowDisableNfc:Z */
/* .line 656 */
/* iput-boolean v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mIsReverseWirelessCharge:Z */
/* .line 657 */
/* iput-boolean v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mRetryAfterOneMin:Z */
/* .line 659 */
int v1 = 0; // const/4 v1, 0x0
this.mHandleInfo = v1;
/* .line 664 */
/* iput v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mCount:I */
/* .line 669 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->initChargeStatus()V */
/* .line 670 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->initBatteryAuthentic()V */
/* .line 671 */
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmContext ( p1 );
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
this.mContentResolver = v0;
/* .line 672 */
/* new-instance v0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver; */
/* invoke-direct {v0, p0, v1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;-><init>(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver-IA;)V */
this.mUEventObserver = v0;
/* .line 673 */
final String v1 = "POWER_SUPPLY_TX_ADAPTER"; // const-string v1, "POWER_SUPPLY_TX_ADAPTER"
(( android.os.UEventObserver ) v0 ).startObserving ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V
/* .line 674 */
final String v1 = "POWER_SUPPLY_HVDCP3_TYPE"; // const-string v1, "POWER_SUPPLY_HVDCP3_TYPE"
(( android.os.UEventObserver ) v0 ).startObserving ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V
/* .line 675 */
final String v1 = "POWER_SUPPLY_QUICK_CHARGE_TYPE"; // const-string v1, "POWER_SUPPLY_QUICK_CHARGE_TYPE"
(( android.os.UEventObserver ) v0 ).startObserving ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V
/* .line 676 */
final String v1 = "POWER_SUPPLY_SOC_DECIMAL"; // const-string v1, "POWER_SUPPLY_SOC_DECIMAL"
(( android.os.UEventObserver ) v0 ).startObserving ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V
/* .line 677 */
final String v1 = "POWER_SUPPLY_REVERSE_CHG_STATE"; // const-string v1, "POWER_SUPPLY_REVERSE_CHG_STATE"
(( android.os.UEventObserver ) v0 ).startObserving ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V
/* .line 678 */
final String v1 = "POWER_SUPPLY_REVERSE_CHG_MODE"; // const-string v1, "POWER_SUPPLY_REVERSE_CHG_MODE"
(( android.os.UEventObserver ) v0 ).startObserving ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V
/* .line 679 */
final String v1 = "POWER_SUPPLY_SHUTDOWN_DELAY"; // const-string v1, "POWER_SUPPLY_SHUTDOWN_DELAY"
(( android.os.UEventObserver ) v0 ).startObserving ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V
/* .line 680 */
final String v1 = "POWER_SUPPLY_WLS_FW_STATE"; // const-string v1, "POWER_SUPPLY_WLS_FW_STATE"
(( android.os.UEventObserver ) v0 ).startObserving ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V
/* .line 681 */
final String v1 = "POWER_SUPPLY_REVERSE_PEN_CHG_STATE"; // const-string v1, "POWER_SUPPLY_REVERSE_PEN_CHG_STATE"
(( android.os.UEventObserver ) v0 ).startObserving ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V
/* .line 683 */
final String v1 = "POWER_SUPPLY_REVERSE_PEN_SOC"; // const-string v1, "POWER_SUPPLY_REVERSE_PEN_SOC"
(( android.os.UEventObserver ) v0 ).startObserving ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V
/* .line 684 */
final String v1 = "POWER_SUPPLY_PEN_MAC"; // const-string v1, "POWER_SUPPLY_PEN_MAC"
(( android.os.UEventObserver ) v0 ).startObserving ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V
/* .line 685 */
final String v1 = "POWER_SUPPLY_PEN_PLACE_ERR"; // const-string v1, "POWER_SUPPLY_PEN_PLACE_ERR"
(( android.os.UEventObserver ) v0 ).startObserving ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V
/* .line 687 */
final String v1 = "POWER_SUPPLY_CONNECTOR_TEMP"; // const-string v1, "POWER_SUPPLY_CONNECTOR_TEMP"
(( android.os.UEventObserver ) v0 ).startObserving ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V
/* .line 688 */
final String v1 = "POWER_SUPPLY_RX_OFFSET"; // const-string v1, "POWER_SUPPLY_RX_OFFSET"
(( android.os.UEventObserver ) v0 ).startObserving ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V
/* .line 689 */
final String v1 = "POWER_SUPPLY_MOISTURE_DET_STS"; // const-string v1, "POWER_SUPPLY_MOISTURE_DET_STS"
(( android.os.UEventObserver ) v0 ).startObserving ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V
/* .line 690 */
final String v1 = "POWER_SUPPLY_CAR_APP_STATE"; // const-string v1, "POWER_SUPPLY_CAR_APP_STATE"
(( android.os.UEventObserver ) v0 ).startObserving ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V
/* .line 691 */
final String v1 = "POWER_SUPPLY_NAME"; // const-string v1, "POWER_SUPPLY_NAME"
(( android.os.UEventObserver ) v0 ).startObserving ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V
/* .line 692 */
final String v1 = "POWER_SUPPLY_LOW_INDUCTANCE_OFFSET"; // const-string v1, "POWER_SUPPLY_LOW_INDUCTANCE_OFFSET"
(( android.os.UEventObserver ) v0 ).startObserving ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V
/* .line 693 */
final String v1 = "POWER_SUPPLY_NTC_ALARM"; // const-string v1, "POWER_SUPPLY_NTC_ALARM"
(( android.os.UEventObserver ) v0 ).startObserving ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V
/* .line 694 */
return;
} // .end method
private void adjustVoltageFromStatsBroadcast ( android.content.Intent p0 ) {
/* .locals 4 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .line 1433 */
v0 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmHandler ( v0 );
v0 = /* invoke-direct {v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->getSBState()I */
/* .line 1434 */
/* .local v0, "smartBatt":I */
v1 = this.this$0;
final String v2 = "miui.intent.extra.ADJUST_VOLTAGE_TS"; // const-string v2, "miui.intent.extra.ADJUST_VOLTAGE_TS"
int v3 = 0; // const/4 v3, 0x0
v2 = (( android.content.Intent ) p1 ).getBooleanExtra ( v2, v3 ); // invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fputmIsSatisfyTempSocCondition ( v1,v2 );
/* .line 1435 */
v1 = this.this$0;
final String v2 = "miui.intent.extra.ADJUST_VOLTAGE_TL"; // const-string v2, "miui.intent.extra.ADJUST_VOLTAGE_TL"
v2 = (( android.content.Intent ) p1 ).getBooleanExtra ( v2, v3 ); // invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fputmIsSatisfyTempLevelCondition ( v1,v2 );
/* .line 1436 */
v1 = this.this$0;
v1 = com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmIsSatisfyTempLevelCondition ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1437 */
/* const/16 v1, 0x14 */
/* if-eq v0, v1, :cond_3 */
/* .line 1438 */
v2 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmMiCharge ( v2 );
(( miui.util.IMiCharge ) v2 ).setSBState ( v1 ); // invoke-virtual {v2, v1}, Lmiui/util/IMiCharge;->setSBState(I)Z
/* .line 1439 */
} // :cond_0
v1 = this.this$0;
v1 = com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmIsSatisfyTempSocCondition ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1440 */
/* const/16 v1, 0xf */
/* if-eq v0, v1, :cond_3 */
/* .line 1441 */
v2 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmMiCharge ( v2 );
(( miui.util.IMiCharge ) v2 ).setSBState ( v1 ); // invoke-virtual {v2, v1}, Lmiui/util/IMiCharge;->setSBState(I)Z
/* .line 1442 */
} // :cond_1
v1 = this.this$0;
v1 = com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmIsSatisfyTimeRegionCondition ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 1443 */
/* const/16 v1, 0xa */
/* if-eq v0, v1, :cond_3 */
/* .line 1444 */
v2 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmMiCharge ( v2 );
(( miui.util.IMiCharge ) v2 ).setSBState ( v1 ); // invoke-virtual {v2, v1}, Lmiui/util/IMiCharge;->setSBState(I)Z
/* .line 1446 */
} // :cond_2
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 1447 */
v1 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmMiCharge ( v1 );
(( miui.util.IMiCharge ) v1 ).setSBState ( v3 ); // invoke-virtual {v1, v3}, Lmiui/util/IMiCharge;->setSBState(I)Z
/* .line 1449 */
} // :cond_3
} // :goto_0
return;
} // .end method
private void adjustVoltageFromTimeRegion ( ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/text/ParseException; */
/* } */
} // .end annotation
/* .line 1421 */
v0 = /* invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->getSBState()I */
/* .line 1422 */
/* .local v0, "smartBatt":I */
v1 = this.this$0;
v1 = com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetDEBUG ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "smartBatt = " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiBatteryServiceImpl"; // const-string v2, "MiuiBatteryServiceImpl"
android.util.Slog .d ( v2,v1 );
/* .line 1423 */
} // :cond_0
v1 = /* invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->isDateOfHighTemp()Z */
/* const/16 v2, 0xa */
if ( v1 != null) { // if-eqz v1, :cond_1
v1 = /* invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->isInTragetCountry()Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* if-nez v0, :cond_1 */
/* .line 1424 */
v1 = this.this$0;
int v3 = 1; // const/4 v3, 0x1
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fputmIsSatisfyTimeRegionCondition ( v1,v3 );
/* .line 1425 */
v1 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmMiCharge ( v1 );
(( miui.util.IMiCharge ) v1 ).setSBState ( v2 ); // invoke-virtual {v1, v2}, Lmiui/util/IMiCharge;->setSBState(I)Z
/* .line 1426 */
} // :cond_1
v1 = /* invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->isDateOfHighTemp()Z */
if ( v1 != null) { // if-eqz v1, :cond_2
v1 = /* invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->isInTragetCountry()Z */
/* if-nez v1, :cond_3 */
} // :cond_2
/* if-ne v0, v2, :cond_3 */
/* .line 1427 */
v1 = this.this$0;
int v2 = 0; // const/4 v2, 0x0
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fputmIsSatisfyTimeRegionCondition ( v1,v2 );
/* .line 1428 */
v1 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmMiCharge ( v1 );
(( miui.util.IMiCharge ) v1 ).setSBState ( v2 ); // invoke-virtual {v1, v2}, Lmiui/util/IMiCharge;->setSBState(I)Z
/* .line 1430 */
} // :cond_3
} // :goto_0
return;
} // .end method
private void checkSpecialHandle ( android.hardware.usb.UsbDevice p0 ) {
/* .locals 12 */
/* .param p1, "usbDevice" # Landroid/hardware/usb/UsbDevice; */
/* .line 1486 */
/* if-nez p1, :cond_0 */
/* .line 1487 */
return;
/* .line 1489 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
(( android.hardware.usb.UsbDevice ) p1 ).getConfiguration ( v0 ); // invoke-virtual {p1, v0}, Landroid/hardware/usb/UsbDevice;->getConfiguration(I)Landroid/hardware/usb/UsbConfiguration;
(( android.hardware.usb.UsbConfiguration ) v1 ).getInterface ( v0 ); // invoke-virtual {v1, v0}, Landroid/hardware/usb/UsbConfiguration;->getInterface(I)Landroid/hardware/usb/UsbInterface;
v0 = (( android.hardware.usb.UsbInterface ) v0 ).getInterfaceProtocol ( ); // invoke-virtual {v0}, Landroid/hardware/usb/UsbInterface;->getInterfaceProtocol()I
/* .line 1490 */
/* .local v0, "num":I */
/* invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->getDecrpytedAddress(I)Ljava/lang/String; */
/* .line 1492 */
/* .local v1, "address":Ljava/lang/String; */
(( android.hardware.usb.UsbDevice ) p1 ).getSerialNumber ( ); // invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getSerialNumber()Ljava/lang/String;
/* .line 1493 */
/* .local v2, "encryptedText":Ljava/lang/String; */
v3 = android.text.TextUtils .isEmpty ( v2 );
final String v4 = "MiuiBatteryServiceImpl"; // const-string v4, "MiuiBatteryServiceImpl"
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 1494 */
final String v3 = "getSerialNumber Failed"; // const-string v3, "getSerialNumber Failed"
android.util.Slog .d ( v4,v3 );
/* .line 1495 */
return;
/* .line 1499 */
} // :cond_1
try { // :try_start_0
final String v3 = "123456789abcdefa"; // const-string v3, "123456789abcdefa"
final String v5 = "US-ASCII"; // const-string v5, "US-ASCII"
(( java.lang.String ) v3 ).getBytes ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
/* .line 1501 */
/* .local v3, "key":[B */
final String v5 = "AES/ECB/NoPadding"; // const-string v5, "AES/ECB/NoPadding"
javax.crypto.Cipher .getInstance ( v5 );
/* .line 1502 */
/* .local v5, "ciper":Ljavax/crypto/Cipher; */
/* new-instance v6, Ljavax/crypto/spec/SecretKeySpec; */
final String v7 = "AES"; // const-string v7, "AES"
/* invoke-direct {v6, v3, v7}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V */
/* .line 1503 */
/* .local v6, "keySpec":Ljavax/crypto/spec/SecretKeySpec; */
int v7 = 2; // const/4 v7, 0x2
(( javax.crypto.Cipher ) v5 ).init ( v7, v6 ); // invoke-virtual {v5, v7, v6}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V
/* .line 1506 */
(( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) p0 ).hexStringToByteArray ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->hexStringToByteArray(Ljava/lang/String;)[B
/* .line 1507 */
/* .local v7, "encrypted":[B */
(( javax.crypto.Cipher ) v5 ).doFinal ( v7 ); // invoke-virtual {v5, v7}, Ljavax/crypto/Cipher;->doFinal([B)[B
/* .line 1509 */
/* .local v8, "decrypted":[B */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "0123456789abcd"; // const-string v10, "0123456789abcd"
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v1 ); // invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* new-instance v10, Ljava/lang/String; */
v11 = java.nio.charset.StandardCharsets.UTF_8;
/* invoke-direct {v10, v8, v11}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V */
v9 = (( java.lang.String ) v9 ).equals ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v9 != null) { // if-eqz v9, :cond_2
/* .line 1510 */
/* new-instance v9, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo; */
v10 = this.this$0;
/* invoke-direct {v9, v10, p1}, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;-><init>(Lcom/android/server/MiuiBatteryServiceImpl;Landroid/hardware/usb/UsbDevice;)V */
this.mHandleInfo = v9;
/* .line 1511 */
final String v9 = "Decryption successful"; // const-string v9, "Decryption successful"
android.util.Slog .d ( v4,v9 );
/* .line 1512 */
v9 = this.mHandleInfo;
v10 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmMiCharge ( v10 );
final String v11 = "getHandleColor"; // const-string v11, "getHandleColor"
(( miui.util.IMiCharge ) v10 ).getTypeCCommonInfo ( v11 ); // invoke-virtual {v10, v11}, Lmiui/util/IMiCharge;->getTypeCCommonInfo(Ljava/lang/String;)Ljava/lang/String;
(( com.android.server.MiuiBatteryServiceImpl$HandleInfo ) v9 ).setColorNumber ( v10 ); // invoke-virtual {v9, v10}, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->setColorNumber(Ljava/lang/String;)V
/* .line 1513 */
v9 = this.mHandleInfo;
int v10 = 1; // const/4 v10, 0x1
(( com.android.server.MiuiBatteryServiceImpl$HandleInfo ) v9 ).setConnectState ( v10 ); // invoke-virtual {v9, v10}, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->setConnectState(I)V
/* .line 1514 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendHandleStateChangeBroadcast()V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1518 */
} // .end local v3 # "key":[B
} // .end local v5 # "ciper":Ljavax/crypto/Cipher;
} // .end local v6 # "keySpec":Ljavax/crypto/spec/SecretKeySpec;
} // .end local v7 # "encrypted":[B
} // .end local v8 # "decrypted":[B
} // :cond_2
/* .line 1516 */
/* :catch_0 */
/* move-exception v3 */
/* .line 1517 */
/* .local v3, "e":Ljava/lang/Exception; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "decrypted exception = "; // const-string v6, "decrypted exception = "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v4,v5 );
/* .line 1519 */
} // .end local v3 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private Integer getCarChargingType ( ) {
/* .locals 2 */
/* .line 1267 */
v0 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmMiCharge ( v0 );
(( miui.util.IMiCharge ) v0 ).getCarChargingType ( ); // invoke-virtual {v0}, Lmiui/util/IMiCharge;->getCarChargingType()Ljava/lang/String;
/* .line 1268 */
/* .local v0, "carCharging":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = (( java.lang.String ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/String;->length()I
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1269 */
v1 = (( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) p0 ).parseInt ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->parseInt(Ljava/lang/String;)I
/* .line 1271 */
} // :cond_0
int v1 = -1; // const/4 v1, -0x1
} // .end method
private Integer getChargingPowerMax ( ) {
/* .locals 2 */
/* .line 1259 */
v0 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmMiCharge ( v0 );
(( miui.util.IMiCharge ) v0 ).getChargingPowerMax ( ); // invoke-virtual {v0}, Lmiui/util/IMiCharge;->getChargingPowerMax()Ljava/lang/String;
/* .line 1260 */
/* .local v0, "powerMax":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = (( java.lang.String ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/String;->length()I
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1261 */
v1 = (( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) p0 ).parseInt ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->parseInt(Ljava/lang/String;)I
/* .line 1263 */
} // :cond_0
int v1 = -1; // const/4 v1, -0x1
} // .end method
private java.lang.String getCurrentDate ( ) {
/* .locals 4 */
/* .line 806 */
/* new-instance v0, Ljava/text/SimpleDateFormat; */
final String v1 = "MM-dd"; // const-string v1, "MM-dd"
/* invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V */
/* .line 807 */
/* .local v0, "simpleDateFormat":Ljava/text/SimpleDateFormat; */
/* new-instance v1, Ljava/util/Date; */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v2 */
/* invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V */
/* .line 808 */
/* .local v1, "date":Ljava/util/Date; */
(( java.text.SimpleDateFormat ) v0 ).format ( v1 ); // invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
/* .line 809 */
/* .local v2, "today":Ljava/lang/String; */
} // .end method
private java.lang.String getDecrpytedAddress ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "num" # I */
/* .line 1583 */
/* const/16 v0, 0xa */
/* if-ge p1, v0, :cond_0 */
/* .line 1584 */
/* mul-int/2addr p1, v0 */
/* .line 1585 */
java.lang.Integer .toString ( p1 );
/* .line 1587 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
java.lang.Integer .toString ( p1 );
/* invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V */
(( java.lang.StringBuilder ) v0 ).reverse ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->reverse()Ljava/lang/StringBuilder;
int v1 = 0; // const/4 v1, 0x0
int v2 = 2; // const/4 v2, 0x2
(( java.lang.StringBuilder ) v0 ).substring ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;
(( java.lang.String ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;
/* .line 1588 */
/* .local v0, "address":Ljava/lang/String; */
} // .end method
private Integer getOneHundredThousandDigits ( ) {
/* .locals 4 */
/* .line 1597 */
v0 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v1 = 0; // const/4 v1, 0x0
int v2 = -2; // const/4 v2, -0x2
/* const-string/jumbo v3, "thermal_temp_state_value" */
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v3,v1,v2 );
/* .line 1599 */
/* .local v0, "tempState":I */
/* const v1, 0x186a0 */
/* div-int v1, v0, v1 */
/* rem-int/lit8 v1, v1, 0xa */
} // .end method
private Integer getPSValue ( ) {
/* .locals 2 */
/* .line 1275 */
v0 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmMiCharge ( v0 );
(( miui.util.IMiCharge ) v0 ).getPSValue ( ); // invoke-virtual {v0}, Lmiui/util/IMiCharge;->getPSValue()Ljava/lang/String;
/* .line 1276 */
/* .local v0, "penSoc":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = (( java.lang.String ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/String;->length()I
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1277 */
v1 = (( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) p0 ).parseInt ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->parseInt(Ljava/lang/String;)I
/* .line 1279 */
} // :cond_0
int v1 = -1; // const/4 v1, -0x1
} // .end method
private Integer getSBState ( ) {
/* .locals 2 */
/* .line 1283 */
v0 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmMiCharge ( v0 );
(( miui.util.IMiCharge ) v0 ).getSBState ( ); // invoke-virtual {v0}, Lmiui/util/IMiCharge;->getSBState()Ljava/lang/String;
/* .line 1284 */
/* .local v0, "smartBatt":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = (( java.lang.String ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/String;->length()I
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1285 */
v1 = (( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) p0 ).parseInt ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->parseInt(Ljava/lang/String;)I
/* .line 1287 */
} // :cond_0
int v1 = -1; // const/4 v1, -0x1
} // .end method
private void handleConnectReboot ( ) {
/* .locals 5 */
/* .line 1522 */
v0 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmContext ( v0 );
/* const-string/jumbo v1, "usb" */
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/hardware/usb/UsbManager; */
/* .line 1523 */
/* .local v0, "usbManager":Landroid/hardware/usb/UsbManager; */
(( android.hardware.usb.UsbManager ) v0 ).getDeviceList ( ); // invoke-virtual {v0}, Landroid/hardware/usb/UsbManager;->getDeviceList()Ljava/util/HashMap;
/* if-nez v1, :cond_0 */
/* .line 1524 */
return;
/* .line 1526 */
} // :cond_0
(( android.hardware.usb.UsbManager ) v0 ).getDeviceList ( ); // invoke-virtual {v0}, Landroid/hardware/usb/UsbManager;->getDeviceList()Ljava/util/HashMap;
(( java.util.HashMap ) v1 ).values ( ); // invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Landroid/hardware/usb/UsbDevice; */
/* .line 1527 */
/* .local v2, "device":Landroid/hardware/usb/UsbDevice; */
v3 = (( android.hardware.usb.UsbDevice ) v2 ).getProductId ( ); // invoke-virtual {v2}, Landroid/hardware/usb/UsbDevice;->getProductId()I
/* const/16 v4, 0x5083 */
/* if-ne v3, v4, :cond_1 */
v3 = (( android.hardware.usb.UsbDevice ) v2 ).getVendorId ( ); // invoke-virtual {v2}, Landroid/hardware/usb/UsbDevice;->getVendorId()I
/* const/16 v4, 0x2717 */
/* if-ne v3, v4, :cond_1 */
/* .line 1528 */
final String v1 = "MiuiBatteryServiceImpl"; // const-string v1, "MiuiBatteryServiceImpl"
final String v3 = "miHandle Attached when reboot"; // const-string v3, "miHandle Attached when reboot"
android.util.Slog .d ( v1,v3 );
/* .line 1529 */
/* invoke-direct {p0, v2}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->checkSpecialHandle(Landroid/hardware/usb/UsbDevice;)V */
/* .line 1530 */
return;
/* .line 1532 */
} // .end local v2 # "device":Landroid/hardware/usb/UsbDevice;
} // :cond_1
/* .line 1533 */
} // :cond_2
return;
} // .end method
private void handleTempState ( ) {
/* .locals 5 */
/* .line 1608 */
v0 = /* invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->getOneHundredThousandDigits()I */
/* .line 1609 */
/* .local v0, "tempStateValue":I */
v1 = /* invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->isBatteryHighTemp()Z */
/* .line 1610 */
/* .local v1, "isBatteryHighTemp":Z */
v2 = /* invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->isBatteryHighTemp()Z */
/* const-string/jumbo v3, "set_rx_sleep" */
int v4 = 5; // const/4 v4, 0x5
if ( v2 != null) { // if-eqz v2, :cond_1
/* if-ne v0, v4, :cond_1 */
/* .line 1611 */
v2 = this.this$0;
v2 = com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmIsStopCharge ( v2 );
/* if-nez v2, :cond_0 */
/* .line 1613 */
v2 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmMiCharge ( v2 );
final String v4 = "1"; // const-string v4, "1"
(( miui.util.IMiCharge ) v2 ).setMiChargePath ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Lmiui/util/IMiCharge;->setMiChargePath(Ljava/lang/String;Ljava/lang/String;)Z
/* .line 1614 */
} // :cond_0
v2 = this.this$0;
int v3 = 1; // const/4 v3, 0x1
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fputmIsStopCharge ( v2,v3 );
/* .line 1615 */
} // :cond_1
/* if-eq v0, v4, :cond_3 */
/* .line 1616 */
v2 = this.this$0;
v2 = com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmIsStopCharge ( v2 );
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 1618 */
v2 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmMiCharge ( v2 );
final String v4 = "0"; // const-string v4, "0"
(( miui.util.IMiCharge ) v2 ).setMiChargePath ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Lmiui/util/IMiCharge;->setMiChargePath(Ljava/lang/String;Ljava/lang/String;)Z
/* .line 1619 */
} // :cond_2
v2 = this.this$0;
int v3 = 0; // const/4 v3, 0x0
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fputmIsStopCharge ( v2,v3 );
/* .line 1621 */
} // :cond_3
} // :goto_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "tempStateValue = " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ", isBatteryHighTemp = "; // const-string v3, ", isBatteryHighTemp = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MiuiBatteryServiceImpl"; // const-string v3, "MiuiBatteryServiceImpl"
android.util.Slog .d ( v3,v2 );
/* .line 1622 */
return;
} // .end method
private void initBatteryAuthentic ( ) {
/* .locals 5 */
/* .line 734 */
final String v0 = "ro.product.name"; // const-string v0, "ro.product.name"
final String v1 = ""; // const-string v1, ""
android.os.SystemProperties .get ( v0,v1 );
final String v3 = "nabu"; // const-string v3, "nabu"
v2 = (( java.lang.String ) v2 ).startsWith ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
/* if-nez v2, :cond_0 */
/* .line 735 */
android.os.SystemProperties .get ( v0,v1 );
final String v1 = "pipa"; // const-string v1, "pipa"
v0 = (( java.lang.String ) v0 ).startsWith ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 736 */
} // :cond_0
v0 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmMiCharge ( v0 );
(( miui.util.IMiCharge ) v0 ).getBatteryAuthentic ( ); // invoke-virtual {v0}, Lmiui/util/IMiCharge;->getBatteryAuthentic()Ljava/lang/String;
/* .line 737 */
/* .local v0, "batteryAuthentic":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_1
v1 = (( java.lang.String ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/String;->length()I
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 738 */
v1 = (( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) p0 ).parseInt ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->parseInt(Ljava/lang/String;)I
/* .line 739 */
/* .local v1, "batteryAuthenticValue":I */
/* if-nez v1, :cond_1 */
/* .line 740 */
/* const/16 v2, 0xd */
/* const-wide/16 v3, 0x7530 */
(( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) p0 ).sendMessageDelayed ( v2, v3, v4 ); // invoke-virtual {p0, v2, v3, v4}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessageDelayed(IJ)V
/* .line 744 */
} // .end local v0 # "batteryAuthentic":Ljava/lang/String;
} // .end local v1 # "batteryAuthenticValue":I
} // :cond_1
return;
} // .end method
private void initChargeStatus ( ) {
/* .locals 4 */
/* .line 708 */
v0 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmMiCharge ( v0 );
(( miui.util.IMiCharge ) v0 ).getQuickChargeType ( ); // invoke-virtual {v0}, Lmiui/util/IMiCharge;->getQuickChargeType()Ljava/lang/String;
/* .line 709 */
/* .local v0, "quickChargeType":Ljava/lang/String; */
v1 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmMiCharge ( v1 );
(( miui.util.IMiCharge ) v1 ).getTxAdapt ( ); // invoke-virtual {v1}, Lmiui/util/IMiCharge;->getTxAdapt()Ljava/lang/String;
/* .line 711 */
/* .local v1, "txAdapter":Ljava/lang/String; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "quickChargeType = "; // const-string v3, "quickChargeType = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " txAdapter = "; // const-string v3, " txAdapter = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MiuiBatteryServiceImpl"; // const-string v3, "MiuiBatteryServiceImpl"
android.util.Slog .d ( v3,v2 );
/* .line 713 */
if ( v0 != null) { // if-eqz v0, :cond_0
v2 = (( java.lang.String ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/String;->length()I
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 714 */
v2 = (( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) p0 ).parseInt ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->parseInt(Ljava/lang/String;)I
/* .line 715 */
/* .local v2, "quickChargeValue":I */
/* if-lez v2, :cond_0 */
/* .line 716 */
/* iput v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastQuickChargeType:I */
/* .line 717 */
int v3 = 3; // const/4 v3, 0x3
(( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) p0 ).sendMessage ( v3, v2 ); // invoke-virtual {p0, v3, v2}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessage(II)V
/* .line 718 */
/* if-lt v2, v3, :cond_0 */
/* .line 719 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendSocDecimaBroadcast()V */
/* .line 724 */
} // .end local v2 # "quickChargeValue":I
} // :cond_0
if ( v1 != null) { // if-eqz v1, :cond_1
v2 = (( java.lang.String ) v1 ).length ( ); // invoke-virtual {v1}, Ljava/lang/String;->length()I
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 725 */
v2 = (( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) p0 ).parseInt ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->parseInt(Ljava/lang/String;)I
/* .line 726 */
/* .local v2, "txAdapterValue":I */
/* if-lez v2, :cond_1 */
/* .line 727 */
/* iput v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastWirelessTxType:I */
/* .line 728 */
int v3 = 1; // const/4 v3, 0x1
(( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) p0 ).sendMessage ( v3, v2 ); // invoke-virtual {p0, v3, v2}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessage(II)V
/* .line 731 */
} // .end local v2 # "txAdapterValue":I
} // :cond_1
return;
} // .end method
private Boolean isBatteryHighTemp ( ) {
/* .locals 3 */
/* .line 1604 */
v0 = this.mContentResolver;
final String v1 = "allowed_kill_battery_temp_threshhold"; // const-string v1, "allowed_kill_battery_temp_threshhold"
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v1,v2,v2 );
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* move v2, v1 */
} // :cond_0
} // .end method
private Boolean isDateOfHighTemp ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/text/ParseException; */
/* } */
} // .end annotation
/* .line 1452 */
final String v0 = "06-15"; // const-string v0, "06-15"
/* invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->parseDate(Ljava/lang/String;)Ljava/util/Date; */
this.mStartHithTempDate = v0;
/* .line 1453 */
final String v0 = "09-15"; // const-string v0, "09-15"
/* invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->parseDate(Ljava/lang/String;)Ljava/util/Date; */
this.mEndHighTempDate = v0;
/* .line 1454 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->getCurrentDate()Ljava/lang/String; */
/* invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->parseDate(Ljava/lang/String;)Ljava/util/Date; */
/* .line 1455 */
/* .local v0, "currentDate":Ljava/util/Date; */
v1 = this.this$0;
v1 = com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetDEBUG ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "currentDate = "; // const-string v2, "currentDate = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiBatteryServiceImpl"; // const-string v2, "MiuiBatteryServiceImpl"
android.util.Slog .d ( v2,v1 );
/* .line 1456 */
} // :cond_0
(( java.util.Date ) v0 ).getTime ( ); // invoke-virtual {v0}, Ljava/util/Date;->getTime()J
/* move-result-wide v1 */
v3 = this.mStartHithTempDate;
(( java.util.Date ) v3 ).getTime ( ); // invoke-virtual {v3}, Ljava/util/Date;->getTime()J
/* move-result-wide v3 */
/* cmp-long v1, v1, v3 */
/* if-ltz v1, :cond_1 */
(( java.util.Date ) v0 ).getTime ( ); // invoke-virtual {v0}, Ljava/util/Date;->getTime()J
/* move-result-wide v1 */
v3 = this.mEndHighTempDate;
(( java.util.Date ) v3 ).getTime ( ); // invoke-virtual {v3}, Ljava/util/Date;->getTime()J
/* move-result-wide v3 */
/* cmp-long v1, v1, v3 */
/* if-gtz v1, :cond_1 */
/* .line 1457 */
int v1 = 1; // const/4 v1, 0x1
/* .line 1459 */
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
} // .end method
private Boolean isInChina ( ) {
/* .locals 4 */
/* .line 1464 */
v0 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmContext ( v0 );
final String v1 = "phone"; // const-string v1, "phone"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/telephony/TelephonyManager; */
/* .line 1465 */
/* .local v0, "tel":Landroid/telephony/TelephonyManager; */
(( android.telephony.TelephonyManager ) v0 ).getNetworkOperator ( ); // invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;
/* .line 1466 */
/* .local v1, "networkOperator":Ljava/lang/String; */
v2 = android.text.TextUtils .isEmpty ( v1 );
/* if-nez v2, :cond_1 */
/* .line 1467 */
v2 = this.this$0;
v2 = com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetDEBUG ( v2 );
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1468 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "networkOperator = "; // const-string v3, "networkOperator = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MiuiBatteryServiceImpl"; // const-string v3, "MiuiBatteryServiceImpl"
android.util.Slog .d ( v3,v2 );
/* .line 1469 */
} // :cond_0
final String v2 = "460"; // const-string v2, "460"
v2 = (( java.lang.String ) v1 ).startsWith ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
/* .line 1471 */
} // :cond_1
int v2 = 0; // const/4 v2, 0x0
} // .end method
private Boolean isInTragetCountry ( ) {
/* .locals 3 */
/* .line 1475 */
v0 = /* invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->isInChina()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
/* .line 1477 */
} // :cond_0
final String v0 = "ro.product.mod_device"; // const-string v0, "ro.product.mod_device"
final String v1 = ""; // const-string v1, ""
android.os.SystemProperties .get ( v0,v1 );
/* const-string/jumbo v2, "taoyao" */
v0 = (( java.lang.String ) v0 ).startsWith ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
/* if-nez v0, :cond_2 */
/* .line 1478 */
final String v0 = "persist.vendor.domain.charge"; // const-string v0, "persist.vendor.domain.charge"
int v2 = 0; // const/4 v2, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v2 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1482 */
} // :cond_1
/* .line 1479 */
} // :cond_2
} // :goto_0
final String v0 = "ro.miui.region"; // const-string v0, "ro.miui.region"
android.os.SystemProperties .get ( v0,v1 );
/* .line 1480 */
/* .local v0, "value":Ljava/lang/String; */
v1 = this.this$0;
v1 = this.SUPPORT_COUNTRY;
v1 = java.util.Arrays .asList ( v1 );
} // .end method
private Boolean isSupportControlHaptic ( ) {
/* .locals 2 */
/* .line 747 */
final String v0 = "ro.product.device"; // const-string v0, "ro.product.device"
final String v1 = ""; // const-string v1, ""
android.os.SystemProperties .get ( v0,v1 );
final String v1 = "mayfly"; // const-string v1, "mayfly"
v0 = (( java.lang.String ) v0 ).startsWith ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
/* if-nez v0, :cond_1 */
/* .line 748 */
final String v0 = "persist.vendor.revchg.shutmotor"; // const-string v0, "persist.vendor.revchg.shutmotor"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 751 */
} // :cond_0
/* .line 749 */
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
private java.util.Date parseDate ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "date" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/text/ParseException; */
/* } */
} // .end annotation
/* .line 812 */
final String v0 = "MM-dd"; // const-string v0, "MM-dd"
/* .line 813 */
/* .local v0, "dateFormat":Ljava/lang/String; */
/* new-instance v1, Ljava/text/SimpleDateFormat; */
/* invoke-direct {v1, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V */
/* .line 814 */
/* .local v1, "simpleDateFormat":Ljava/text/SimpleDateFormat; */
(( java.text.SimpleDateFormat ) v1 ).parse ( p1 ); // invoke-virtual {v1, p1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
} // .end method
private void sendBroadcast ( android.content.Intent p0 ) {
/* .locals 2 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .line 1298 */
/* const/high16 v0, 0x31000000 */
(( android.content.Intent ) p1 ).addFlags ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 1301 */
v0 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmContext ( v0 );
v1 = android.os.UserHandle.ALL;
(( android.content.Context ) v0 ).sendBroadcastAsUser ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
/* .line 1302 */
return;
} // .end method
private void sendHandleBatteryStatsChangeBroadcast ( ) {
/* .locals 3 */
/* .line 1553 */
v0 = this.mHandleInfo;
final String v1 = "MiuiBatteryServiceImpl"; // const-string v1, "MiuiBatteryServiceImpl"
/* if-nez v0, :cond_0 */
/* .line 1554 */
final String v0 = "faild to get handleInfo"; // const-string v0, "faild to get handleInfo"
android.util.Slog .d ( v1,v0 );
/* .line 1555 */
return;
/* .line 1557 */
} // :cond_0
v0 = this.this$0;
v0 = com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetDEBUG ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1558 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "batteryInfo changed info = "; // const-string v2, "batteryInfo changed info = "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mHandleInfo;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 1560 */
} // :cond_1
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "miui.intent.action.ACTION_HANDLE_BATTERY_STATE_CHANGED"; // const-string v1, "miui.intent.action.ACTION_HANDLE_BATTERY_STATE_CHANGED"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 1561 */
/* .local v0, "handleBatteryChangeBroadcast":Landroid/content/Intent; */
v1 = this.mHandleInfo;
/* iget v1, v1, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->mConnectState:I */
final String v2 = "miui.intent.extra.EXTRA_HANDLE_CONNECT_STATE"; // const-string v2, "miui.intent.extra.EXTRA_HANDLE_CONNECT_STATE"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 1562 */
v1 = this.mHandleInfo;
/* iget v1, v1, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->mBatteryLevel:I */
final String v2 = "batteryLevel"; // const-string v2, "batteryLevel"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 1563 */
v1 = this.mHandleInfo;
v1 = this.mBatteryStats;
final String v2 = "batteryStats"; // const-string v2, "batteryStats"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 1564 */
/* invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendStickyBroadcast(Landroid/content/Intent;)V */
/* .line 1565 */
return;
} // .end method
private void sendHandleStateChangeBroadcast ( ) {
/* .locals 3 */
/* .line 1536 */
v0 = this.mHandleInfo;
final String v1 = "MiuiBatteryServiceImpl"; // const-string v1, "MiuiBatteryServiceImpl"
/* if-nez v0, :cond_0 */
/* .line 1537 */
final String v0 = "faild to get handleInfo"; // const-string v0, "faild to get handleInfo"
android.util.Slog .d ( v1,v0 );
/* .line 1538 */
return;
/* .line 1540 */
} // :cond_0
v0 = this.this$0;
v0 = com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetDEBUG ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1541 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "connect changed info = "; // const-string v2, "connect changed info = "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mHandleInfo;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 1543 */
} // :cond_1
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "miui.intent.action.ACTION_HANDLE_STATE_CHANGED"; // const-string v1, "miui.intent.action.ACTION_HANDLE_STATE_CHANGED"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 1544 */
/* .local v0, "handleBroadcast":Landroid/content/Intent; */
v1 = this.mHandleInfo;
/* iget v1, v1, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->mConnectState:I */
final String v2 = "miui.intent.extra.EXTRA_HANDLE_CONNECT_STATE"; // const-string v2, "miui.intent.extra.EXTRA_HANDLE_CONNECT_STATE"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 1545 */
v1 = this.mHandleInfo;
v1 = this.mFwVersion;
final String v2 = "miui.intent.extra.EXTRA_HANDLE_VERSION"; // const-string v2, "miui.intent.extra.EXTRA_HANDLE_VERSION"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 1546 */
v1 = this.mHandleInfo;
/* iget v1, v1, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->mVid:I */
/* const-string/jumbo v2, "vid" */
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 1547 */
v1 = this.mHandleInfo;
/* iget v1, v1, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->mPid:I */
final String v2 = "pid"; // const-string v2, "pid"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 1548 */
v1 = this.mHandleInfo;
v1 = this.mColorNumber;
final String v2 = "ColorNumber"; // const-string v2, "ColorNumber"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 1549 */
/* invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendBroadcast(Landroid/content/Intent;)V */
/* .line 1550 */
return;
} // .end method
private void sendSocDecimaBroadcast ( ) {
/* .locals 5 */
/* .line 697 */
v0 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmMiCharge ( v0 );
(( miui.util.IMiCharge ) v0 ).getSocDecimal ( ); // invoke-virtual {v0}, Lmiui/util/IMiCharge;->getSocDecimal()Ljava/lang/String;
/* .line 698 */
/* .local v0, "socDecimal":Ljava/lang/String; */
v1 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmMiCharge ( v1 );
(( miui.util.IMiCharge ) v1 ).getSocDecimalRate ( ); // invoke-virtual {v1}, Lmiui/util/IMiCharge;->getSocDecimalRate()Ljava/lang/String;
/* .line 699 */
/* .local v1, "socDecimalRate":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_0
v2 = (( java.lang.String ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/String;->length()I
if ( v2 != null) { // if-eqz v2, :cond_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 700 */
v2 = (( java.lang.String ) v1 ).length ( ); // invoke-virtual {v1}, Ljava/lang/String;->length()I
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 701 */
v2 = (( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) p0 ).parseInt ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->parseInt(Ljava/lang/String;)I
/* .line 702 */
/* .local v2, "socDecimalValue":I */
v3 = (( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) p0 ).parseInt ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->parseInt(Ljava/lang/String;)I
/* .line 703 */
/* .local v3, "socDecimalRateVaule":I */
int v4 = 4; // const/4 v4, 0x4
(( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) p0 ).sendMessage ( v4, v2, v3 ); // invoke-virtual {p0, v4, v2, v3}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessage(III)V
/* .line 705 */
} // .end local v2 # "socDecimalValue":I
} // .end local v3 # "socDecimalRateVaule":I
} // :cond_0
return;
} // .end method
private void sendStickyBroadcast ( android.content.Intent p0 ) {
/* .locals 2 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .line 1291 */
/* const/high16 v0, 0x31000000 */
(( android.content.Intent ) p1 ).addFlags ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 1294 */
v0 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmContext ( v0 );
v1 = android.os.UserHandle.ALL;
(( android.content.Context ) v0 ).sendStickyBroadcastAsUser ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
/* .line 1295 */
return;
} // .end method
private void sendUpdateStatusBroadCast ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "status" # I */
/* .line 1327 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "miui.intent.action.ACTION_WIRELESS_CHARGING"; // const-string v1, "miui.intent.action.ACTION_WIRELESS_CHARGING"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 1328 */
/* .local v0, "intent":Landroid/content/Intent; */
/* const/high16 v1, 0x31000000 */
(( android.content.Intent ) v0 ).addFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 1331 */
final String v1 = "miui.intent.extra.WIRELESS_CHARGING"; // const-string v1, "miui.intent.extra.WIRELESS_CHARGING"
(( android.content.Intent ) v0 ).putExtra ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 1332 */
v1 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmContext ( v1 );
v2 = android.os.UserHandle.ALL;
(( android.content.Context ) v1 ).sendStickyBroadcastAsUser ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
/* .line 1333 */
return;
} // .end method
private void shouldCloseWirelessReverseCharging ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "batteryLevel" # I */
/* .line 1398 */
v0 = this.mContentResolver;
/* const-string/jumbo v1, "wireless_reverse_charging" */
/* const/16 v2, 0x1e */
v0 = android.provider.Settings$Global .getInt ( v0,v1,v2 );
/* if-ge p1, v0, :cond_0 */
/* .line 1399 */
int v0 = 4; // const/4 v0, 0x4
/* invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->updateWirelessReverseChargingNotification(I)V */
/* .line 1401 */
} // :cond_0
return;
} // .end method
private void showPowerOffWarningDialog ( ) {
/* .locals 6 */
/* .line 1404 */
android.app.ActivityThread .currentActivityThread ( );
(( android.app.ActivityThread ) v0 ).getSystemUiContext ( ); // invoke-virtual {v0}, Landroid/app/ActivityThread;->getSystemUiContext()Landroid/app/ContextImpl;
/* .line 1405 */
/* .local v0, "systemuiContext":Landroid/content/Context; */
/* new-instance v1, Lmiuix/appcompat/app/AlertDialog$Builder; */
/* const v2, 0x66110006 */
/* invoke-direct {v1, v0, v2}, Lmiuix/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V */
/* .line 1406 */
int v2 = 0; // const/4 v2, 0x0
(( miuix.appcompat.app.AlertDialog$Builder ) v1 ).setCancelable ( v2 ); // invoke-virtual {v1, v2}, Lmiuix/appcompat/app/AlertDialog$Builder;->setCancelable(Z)Lmiuix/appcompat/app/AlertDialog$Builder;
/* .line 1407 */
/* const v2, 0x110f0070 */
(( miuix.appcompat.app.AlertDialog$Builder ) v1 ).setTitle ( v2 ); // invoke-virtual {v1, v2}, Lmiuix/appcompat/app/AlertDialog$Builder;->setTitle(I)Lmiuix/appcompat/app/AlertDialog$Builder;
v2 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmContext ( v2 );
/* .line 1408 */
(( android.content.Context ) v2 ).getResources ( ); // invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 1409 */
/* const/16 v3, 0x1e */
java.lang.Integer .valueOf ( v3 );
/* filled-new-array {v4}, [Ljava/lang/Object; */
/* const/high16 v5, 0x110d0000 */
(( android.content.res.Resources ) v2 ).getQuantityString ( v5, v3, v4 ); // invoke-virtual {v2, v5, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;
/* .line 1408 */
(( miuix.appcompat.app.AlertDialog$Builder ) v1 ).setMessage ( v2 ); // invoke-virtual {v1, v2}, Lmiuix/appcompat/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/AlertDialog$Builder;
/* new-instance v2, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$1; */
/* invoke-direct {v2, p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$1;-><init>(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)V */
/* .line 1410 */
/* const v3, 0x110f006f */
(( miuix.appcompat.app.AlertDialog$Builder ) v1 ).setPositiveButton ( v3, v2 ); // invoke-virtual {v1, v3, v2}, Lmiuix/appcompat/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;
/* .line 1415 */
(( miuix.appcompat.app.AlertDialog$Builder ) v1 ).create ( ); // invoke-virtual {v1}, Lmiuix/appcompat/app/AlertDialog$Builder;->create()Lmiuix/appcompat/app/AlertDialog;
/* .line 1416 */
/* .local v1, "powerOffDialog":Lmiuix/appcompat/app/AlertDialog; */
(( miuix.appcompat.app.AlertDialog ) v1 ).getWindow ( ); // invoke-virtual {v1}, Lmiuix/appcompat/app/AlertDialog;->getWindow()Landroid/view/Window;
/* const/16 v3, 0x7da */
(( android.view.Window ) v2 ).setType ( v3 ); // invoke-virtual {v2, v3}, Landroid/view/Window;->setType(I)V
/* .line 1417 */
(( miuix.appcompat.app.AlertDialog ) v1 ).show ( ); // invoke-virtual {v1}, Lmiuix/appcompat/app/AlertDialog;->show()V
/* .line 1418 */
return;
} // .end method
private void showWirelessCharingWarningDialog ( ) {
/* .locals 3 */
/* .line 1316 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "miui.intent.action.ACTIVITY_WIRELESS_CHG_WARNING"; // const-string v1, "miui.intent.action.ACTIVITY_WIRELESS_CHG_WARNING"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 1317 */
/* .local v0, "dialogIntent":Landroid/content/Intent; */
/* const/high16 v1, 0x10000000 */
(( android.content.Intent ) v0 ).addFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 1318 */
final String v1 = "plugstatus"; // const-string v1, "plugstatus"
int v2 = 4; // const/4 v2, 0x4
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 1319 */
v1 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmContext ( v1 );
(( android.content.Context ) v1 ).startActivity ( v0 ); // invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
/* .line 1320 */
int v1 = 1; // const/4 v1, 0x1
/* invoke-direct {p0, v1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendUpdateStatusBroadCast(I)V */
/* .line 1321 */
return;
} // .end method
private void updateMiuiPCBatteryChanged ( ) {
/* .locals 4 */
/* .line 1305 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "miui.intent.action.MIUI_PC_BATTERY_CHANGED"; // const-string v1, "miui.intent.action.MIUI_PC_BATTERY_CHANGED"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 1306 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "miui.intent.extra.EXTRA_LOW_TX_OFFSET_STATE"; // const-string v1, "miui.intent.extra.EXTRA_LOW_TX_OFFSET_STATE"
/* iget v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastOffsetState:I */
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 1307 */
final String v1 = "miui.intent.extra.EXTRA_NTC_ALARM"; // const-string v1, "miui.intent.extra.EXTRA_NTC_ALARM"
/* iget v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastNtcAlarm:I */
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 1309 */
/* const/high16 v1, 0x31000000 */
(( android.content.Intent ) v0 ).addFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 1312 */
v1 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmContext ( v1 );
v2 = android.os.UserHandle.ALL;
final String v3 = "com.miui.securitycenter.POWER_CENTER_COMMON_PERMISSION"; // const-string v3, "com.miui.securitycenter.POWER_CENTER_COMMON_PERMISSION"
(( android.content.Context ) v1 ).sendBroadcastAsUser ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;)V
/* .line 1313 */
return;
} // .end method
private void updateWirelessReverseChargingNotification ( Integer p0 ) {
/* .locals 14 */
/* .param p1, "closedReason" # I */
/* .line 1336 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1337 */
/* .local v0, "messageRes":I */
v1 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmContext ( v1 );
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 1338 */
/* .local v1, "r":Landroid/content/res/Resources; */
/* const v2, 0x110f0418 */
(( android.content.res.Resources ) v1 ).getText ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;
/* .line 1340 */
/* .local v2, "title":Ljava/lang/CharSequence; */
v3 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmContext ( v3 );
/* .line 1341 */
final String v4 = "notification"; // const-string v4, "notification"
(( android.content.Context ) v3 ).getSystemService ( v4 ); // invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v3, Landroid/app/NotificationManager; */
/* .line 1342 */
/* .local v3, "notificationManager":Landroid/app/NotificationManager; */
final String v4 = "MiuiBatteryServiceImpl"; // const-string v4, "MiuiBatteryServiceImpl"
/* if-nez v3, :cond_0 */
/* .line 1343 */
final String v5 = "get notification service failed"; // const-string v5, "get notification service failed"
android.util.Slog .d ( v4,v5 );
/* .line 1344 */
return;
/* .line 1347 */
} // :cond_0
int v5 = 4; // const/4 v5, 0x4
int v6 = 1; // const/4 v6, 0x1
/* if-ne p1, v6, :cond_1 */
/* .line 1348 */
/* const v0, 0x110f0415 */
/* .line 1349 */
} // :cond_1
/* if-ne p1, v5, :cond_2 */
/* .line 1350 */
/* const v0, 0x110f0416 */
/* .line 1351 */
} // :cond_2
int v7 = 2; // const/4 v7, 0x2
/* if-ne p1, v7, :cond_3 */
/* .line 1352 */
/* const v0, 0x110f0417 */
/* .line 1356 */
} // :cond_3
} // :goto_0
/* iget v7, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mChargingNotificationId:I */
int v8 = 0; // const/4 v8, 0x0
int v9 = 0; // const/4 v9, 0x0
if ( v7 != null) { // if-eqz v7, :cond_4
/* .line 1357 */
v10 = android.os.UserHandle.ALL;
(( android.app.NotificationManager ) v3 ).cancelAsUser ( v8, v7, v10 ); // invoke-virtual {v3, v8, v7, v10}, Landroid/app/NotificationManager;->cancelAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)V
/* .line 1358 */
final String v7 = "Clear notification"; // const-string v7, "Clear notification"
android.util.Slog .d ( v4,v7 );
/* .line 1359 */
/* iput v9, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mChargingNotificationId:I */
/* .line 1362 */
} // :cond_4
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 1363 */
/* new-instance v7, Landroid/app/Notification$Builder; */
v10 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmContext ( v10 );
v11 = com.android.internal.notification.SystemNotificationChannels.USB;
/* invoke-direct {v7, v10, v11}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;)V */
/* .line 1364 */
/* const v10, 0x108089a */
(( android.app.Notification$Builder ) v7 ).setSmallIcon ( v10 ); // invoke-virtual {v7, v10}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;
/* .line 1365 */
/* const-wide/16 v10, 0x0 */
(( android.app.Notification$Builder ) v7 ).setWhen ( v10, v11 ); // invoke-virtual {v7, v10, v11}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;
/* .line 1366 */
(( android.app.Notification$Builder ) v7 ).setOngoing ( v9 ); // invoke-virtual {v7, v9}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;
/* .line 1367 */
(( android.app.Notification$Builder ) v7 ).setTicker ( v2 ); // invoke-virtual {v7, v2}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;
/* .line 1368 */
(( android.app.Notification$Builder ) v7 ).setDefaults ( v9 ); // invoke-virtual {v7, v9}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;
v10 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmContext ( v10 );
/* .line 1369 */
/* const v11, 0x106001c */
v10 = (( android.content.Context ) v10 ).getColor ( v11 ); // invoke-virtual {v10, v11}, Landroid/content/Context;->getColor(I)I
(( android.app.Notification$Builder ) v7 ).setColor ( v10 ); // invoke-virtual {v7, v10}, Landroid/app/Notification$Builder;->setColor(I)Landroid/app/Notification$Builder;
/* .line 1372 */
(( android.app.Notification$Builder ) v7 ).setContentTitle ( v2 ); // invoke-virtual {v7, v2}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;
/* .line 1373 */
(( android.app.Notification$Builder ) v7 ).setVisibility ( v6 ); // invoke-virtual {v7, v6}, Landroid/app/Notification$Builder;->setVisibility(I)Landroid/app/Notification$Builder;
/* .line 1375 */
/* .local v7, "builder":Landroid/app/Notification$Builder; */
/* if-ne p1, v5, :cond_5 */
/* .line 1376 */
java.text.NumberFormat .getPercentInstance ( );
v11 = this.mContentResolver;
/* .line 1377 */
/* const-string/jumbo v12, "wireless_reverse_charging" */
/* const/16 v13, 0x1e */
v11 = android.provider.Settings$Global .getInt ( v11,v12,v13 );
/* int-to-float v11, v11 */
/* const/high16 v12, 0x42c80000 # 100.0f */
/* div-float/2addr v11, v12 */
/* float-to-double v11, v11 */
(( java.text.NumberFormat ) v10 ).format ( v11, v12 ); // invoke-virtual {v10, v11, v12}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;
/* filled-new-array {v10}, [Ljava/lang/Object; */
/* .line 1376 */
(( android.content.res.Resources ) v1 ).getString ( v0, v10 ); // invoke-virtual {v1, v0, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;
/* .line 1378 */
/* .local v10, "messageString":Ljava/lang/String; */
(( android.app.Notification$Builder ) v7 ).setContentText ( v10 ); // invoke-virtual {v7, v10}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;
/* .line 1379 */
} // .end local v10 # "messageString":Ljava/lang/String;
/* .line 1380 */
} // :cond_5
(( android.content.res.Resources ) v1 ).getText ( v0 ); // invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;
/* .line 1381 */
/* .local v10, "message":Ljava/lang/CharSequence; */
(( android.app.Notification$Builder ) v7 ).setContentText ( v10 ); // invoke-virtual {v7, v10}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;
/* .line 1383 */
} // .end local v10 # "message":Ljava/lang/CharSequence;
} // :goto_1
(( android.app.Notification$Builder ) v7 ).build ( ); // invoke-virtual {v7}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;
/* .line 1384 */
/* .local v10, "notification":Landroid/app/Notification; */
v11 = android.os.UserHandle.ALL;
(( android.app.NotificationManager ) v3 ).notifyAsUser ( v8, v0, v10, v11 ); // invoke-virtual {v3, v8, v0, v10, v11}, Landroid/app/NotificationManager;->notifyAsUser(Ljava/lang/String;ILandroid/app/Notification;Landroid/os/UserHandle;)V
/* .line 1385 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v11 = "push notification:"; // const-string v11, "push notification:"
(( java.lang.StringBuilder ) v8 ).append ( v11 ); // invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v2 ); // invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v8 );
/* .line 1386 */
/* iput v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mChargingNotificationId:I */
/* .line 1389 */
} // .end local v7 # "builder":Landroid/app/Notification$Builder;
} // .end local v10 # "notification":Landroid/app/Notification;
} // :cond_6
/* if-ne p1, v5, :cond_7 */
/* .line 1390 */
v4 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmMiCharge ( v4 );
(( miui.util.IMiCharge ) v4 ).setWirelessChargingEnabled ( v9 ); // invoke-virtual {v4, v9}, Lmiui/util/IMiCharge;->setWirelessChargingEnabled(Z)I
/* .line 1391 */
} // :cond_7
/* if-nez p1, :cond_8 */
/* .line 1392 */
return;
/* .line 1394 */
} // :cond_8
} // :goto_2
/* invoke-direct {p0, v6}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendUpdateStatusBroadCast(I)V */
/* .line 1395 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 11 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 1018 */
/* iget v0, p1, Landroid/os/Message;->what:I */
final String v1 = "Get NFC failed"; // const-string v1, "Get NFC failed"
final String v2 = "0"; // const-string v2, "0"
final String v3 = "1"; // const-string v3, "1"
/* const-string/jumbo v4, "screen_unlock" */
/* const/16 v5, 0x8 */
final String v6 = "nfc_closd_from_wirelss"; // const-string v6, "nfc_closd_from_wirelss"
int v7 = 3; // const/4 v7, 0x3
final String v8 = "MiuiBatteryServiceImpl"; // const-string v8, "MiuiBatteryServiceImpl"
int v9 = 1; // const/4 v9, 0x1
int v10 = 0; // const/4 v10, 0x0
/* packed-switch v0, :pswitch_data_0 */
/* .line 1254 */
final String v0 = "NO Message"; // const-string v0, "NO Message"
android.util.Slog .d ( v8,v0 );
/* goto/16 :goto_6 */
/* .line 1251 */
/* :pswitch_0 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->handleTempState()V */
/* .line 1252 */
/* goto/16 :goto_6 */
/* .line 1248 */
/* :pswitch_1 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->updateMiuiPCBatteryChanged()V */
/* .line 1249 */
/* goto/16 :goto_6 */
/* .line 1239 */
/* :pswitch_2 */
/* iget v0, p1, Landroid/os/Message;->arg1:I */
/* .line 1240 */
/* .local v0, "batteryLevel":I */
/* const/16 v1, 0x4b */
/* if-lt v0, v1, :cond_0 */
/* .line 1241 */
v1 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmMiCharge ( v1 );
(( miui.util.IMiCharge ) v1 ).setInputSuspendState ( v3 ); // invoke-virtual {v1, v3}, Lmiui/util/IMiCharge;->setInputSuspendState(Ljava/lang/String;)Z
/* goto/16 :goto_6 */
/* .line 1242 */
} // :cond_0
/* const/16 v1, 0x28 */
/* if-gt v0, v1, :cond_12 */
/* .line 1243 */
v1 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmMiCharge ( v1 );
(( miui.util.IMiCharge ) v1 ).setInputSuspendState ( v2 ); // invoke-virtual {v1, v2}, Lmiui/util/IMiCharge;->setInputSuspendState(Ljava/lang/String;)Z
/* goto/16 :goto_6 */
/* .line 1236 */
} // .end local v0 # "batteryLevel":I
/* :pswitch_3 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->handleConnectReboot()V */
/* .line 1237 */
/* goto/16 :goto_6 */
/* .line 1222 */
/* :pswitch_4 */
v0 = this.mHandleInfo;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1223 */
(( com.android.server.MiuiBatteryServiceImpl$HandleInfo ) v0 ).setConnectState ( v10 ); // invoke-virtual {v0, v10}, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->setConnectState(I)V
/* .line 1224 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendHandleStateChangeBroadcast()V */
/* .line 1225 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendHandleBatteryStatsChangeBroadcast()V */
/* .line 1227 */
} // :cond_1
v0 = this.this$0;
int v1 = -1; // const/4 v1, -0x1
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fputmLastPhoneBatteryLevel ( v0,v1 );
/* .line 1228 */
int v0 = 0; // const/4 v0, 0x0
this.mHandleInfo = v0;
/* .line 1229 */
/* goto/16 :goto_6 */
/* .line 1218 */
/* :pswitch_5 */
v0 = this.obj;
/* check-cast v0, Landroid/hardware/usb/UsbDevice; */
/* .line 1219 */
/* .local v0, "au":Landroid/hardware/usb/UsbDevice; */
/* invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->checkSpecialHandle(Landroid/hardware/usb/UsbDevice;)V */
/* .line 1220 */
/* goto/16 :goto_6 */
/* .line 1231 */
} // .end local v0 # "au":Landroid/hardware/usb/UsbDevice;
/* :pswitch_6 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "miui.intent.action.ACTION_POGO_CONNECTED_STATE"; // const-string v1, "miui.intent.action.ACTION_POGO_CONNECTED_STATE"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 1232 */
/* .local v0, "pogoConnectedIntent":Landroid/content/Intent; */
final String v1 = "miui.intent.extra.EXTRA_POGO_CONNECTED_STATE"; // const-string v1, "miui.intent.extra.EXTRA_POGO_CONNECTED_STATE"
/* iget v2, p1, Landroid/os/Message;->arg1:I */
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 1233 */
/* invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendStickyBroadcast(Landroid/content/Intent;)V */
/* .line 1234 */
/* goto/16 :goto_6 */
/* .line 1212 */
} // .end local v0 # "pogoConnectedIntent":Landroid/content/Intent;
/* :pswitch_7 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "miui.intent.action.ACTION_MOISTURE_DET"; // const-string v1, "miui.intent.action.ACTION_MOISTURE_DET"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 1213 */
/* .local v0, "moistureDetIntent":Landroid/content/Intent; */
final String v1 = "miui.intent.extra.EXTRA_MOISTURE_DET"; // const-string v1, "miui.intent.extra.EXTRA_MOISTURE_DET"
/* iget v2, p1, Landroid/os/Message;->arg1:I */
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 1214 */
/* invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendStickyBroadcast(Landroid/content/Intent;)V */
/* .line 1215 */
/* goto/16 :goto_6 */
/* .line 1208 */
} // .end local v0 # "moistureDetIntent":Landroid/content/Intent;
/* :pswitch_8 */
v0 = this.obj;
/* check-cast v0, Landroid/content/Intent; */
/* .line 1209 */
/* .local v0, "j":Landroid/content/Intent; */
/* invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->adjustVoltageFromStatsBroadcast(Landroid/content/Intent;)V */
/* .line 1210 */
/* goto/16 :goto_6 */
/* .line 1026 */
} // .end local v0 # "j":Landroid/content/Intent;
/* :pswitch_9 */
v0 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmMiCharge ( v0 );
(( miui.util.IMiCharge ) v0 ).setMiChargePath ( v4, v2 ); // invoke-virtual {v0, v4, v2}, Lmiui/util/IMiCharge;->setMiChargePath(Ljava/lang/String;Ljava/lang/String;)Z
/* .line 1027 */
/* goto/16 :goto_6 */
/* .line 1023 */
/* :pswitch_a */
v0 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmMiCharge ( v0 );
(( miui.util.IMiCharge ) v0 ).setMiChargePath ( v4, v3 ); // invoke-virtual {v0, v4, v3}, Lmiui/util/IMiCharge;->setMiChargePath(Ljava/lang/String;Ljava/lang/String;)Z
/* .line 1024 */
/* goto/16 :goto_6 */
/* .line 1203 */
/* :pswitch_b */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "miui.intent.action.ACTION_RX_OFFSET"; // const-string v1, "miui.intent.action.ACTION_RX_OFFSET"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 1204 */
/* .local v0, "rxOffsetIntent":Landroid/content/Intent; */
final String v1 = "miui.intent.extra.EXTRA_RX_OFFSET"; // const-string v1, "miui.intent.extra.EXTRA_RX_OFFSET"
/* iget v2, p1, Landroid/os/Message;->arg1:I */
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 1205 */
/* invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendStickyBroadcast(Landroid/content/Intent;)V */
/* .line 1206 */
/* goto/16 :goto_6 */
/* .line 1198 */
} // .end local v0 # "rxOffsetIntent":Landroid/content/Intent;
/* :pswitch_c */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "miui.intent.action.ACTION_TYPE_C_HIGH_TEMP"; // const-string v1, "miui.intent.action.ACTION_TYPE_C_HIGH_TEMP"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 1199 */
/* .local v0, "connectorTempIntent":Landroid/content/Intent; */
final String v1 = "miui.intent.extra.EXTRA_TYPE_C_HIGH_TEMP"; // const-string v1, "miui.intent.extra.EXTRA_TYPE_C_HIGH_TEMP"
/* iget v2, p1, Landroid/os/Message;->arg1:I */
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 1200 */
/* invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendStickyBroadcast(Landroid/content/Intent;)V */
/* .line 1201 */
/* goto/16 :goto_6 */
/* .line 1192 */
} // .end local v0 # "connectorTempIntent":Landroid/content/Intent;
/* :pswitch_d */
try { // :try_start_0
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->adjustVoltageFromTimeRegion()V */
/* :try_end_0 */
/* .catch Ljava/text/ParseException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1195 */
/* goto/16 :goto_6 */
/* .line 1193 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1194 */
/* .local v0, "e":Ljava/text/ParseException; */
(( java.text.ParseException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/text/ParseException;->printStackTrace()V
/* .line 1196 */
} // .end local v0 # "e":Ljava/text/ParseException;
/* goto/16 :goto_6 */
/* .line 1185 */
/* :pswitch_e */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "com.android.internal.intent.action.REQUEST_SHUTDOWN"; // const-string v1, "com.android.internal.intent.action.REQUEST_SHUTDOWN"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 1186 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "android.intent.extra.KEY_CONFIRM"; // const-string v1, "android.intent.extra.KEY_CONFIRM"
(( android.content.Intent ) v0 ).putExtra ( v1, v10 ); // invoke-virtual {v0, v1, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 1187 */
/* const/high16 v1, 0x10000000 */
(( android.content.Intent ) v0 ).setFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 1188 */
v1 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmContext ( v1 );
v2 = android.os.UserHandle.CURRENT;
(( android.content.Context ) v1 ).startActivityAsUser ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
/* .line 1189 */
/* goto/16 :goto_6 */
/* .line 1181 */
} // .end local v0 # "intent":Landroid/content/Intent;
/* :pswitch_f */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->showPowerOffWarningDialog()V */
/* .line 1182 */
/* const/16 v0, 0xe */
/* const-wide/16 v1, 0x7530 */
(( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) p0 ).sendMessageDelayed ( v0, v1, v2 ); // invoke-virtual {p0, v0, v1, v2}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessageDelayed(IJ)V
/* .line 1183 */
/* goto/16 :goto_6 */
/* .line 1175 */
/* :pswitch_10 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "miui.intent.action.ACTION_PEN_REVERSE_CHARGE_STATE"; // const-string v1, "miui.intent.action.ACTION_PEN_REVERSE_CHARGE_STATE"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 1176 */
/* .local v0, "penReverseChgStateIntent":Landroid/content/Intent; */
final String v1 = "miui.intent.extra.ACTION_PEN_REVERSE_CHARGE_STATE"; // const-string v1, "miui.intent.extra.ACTION_PEN_REVERSE_CHARGE_STATE"
/* iget v2, p1, Landroid/os/Message;->arg1:I */
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 1177 */
final String v1 = "miui.intent.extra.REVERSE_PEN_SOC"; // const-string v1, "miui.intent.extra.REVERSE_PEN_SOC"
v2 = /* invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->getPSValue()I */
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 1178 */
/* invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendStickyBroadcast(Landroid/content/Intent;)V */
/* .line 1179 */
/* goto/16 :goto_6 */
/* .line 1170 */
} // .end local v0 # "penReverseChgStateIntent":Landroid/content/Intent;
/* :pswitch_11 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "miui.intent.action.ACTION_WIRELESS_FW_UPDATE"; // const-string v1, "miui.intent.action.ACTION_WIRELESS_FW_UPDATE"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 1171 */
/* .local v0, "wirelessFwIntent":Landroid/content/Intent; */
final String v1 = "miui.intent.extra.EXTRA_WIRELESS_FW_UPDATE"; // const-string v1, "miui.intent.extra.EXTRA_WIRELESS_FW_UPDATE"
/* iget v2, p1, Landroid/os/Message;->arg1:I */
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 1172 */
/* invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendStickyBroadcast(Landroid/content/Intent;)V */
/* .line 1173 */
/* goto/16 :goto_6 */
/* .line 1166 */
} // .end local v0 # "wirelessFwIntent":Landroid/content/Intent;
/* :pswitch_12 */
/* iget v0, p1, Landroid/os/Message;->arg1:I */
/* .line 1167 */
/* .local v0, "bluetoothState":I */
v1 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmMiCharge ( v1 );
(( miui.util.IMiCharge ) v1 ).setBtTransferStartState ( v0 ); // invoke-virtual {v1, v0}, Lmiui/util/IMiCharge;->setBtTransferStartState(I)Z
/* .line 1168 */
/* goto/16 :goto_6 */
/* .line 1136 */
} // .end local v0 # "bluetoothState":I
/* :pswitch_13 */
try { // :try_start_1
v0 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmContext ( v0 );
android.nfc.NfcAdapter .getNfcAdapter ( v0 );
this.mNfcAdapter = v0;
/* :try_end_1 */
/* .catch Ljava/lang/UnsupportedOperationException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 1139 */
/* .line 1137 */
/* :catch_1 */
/* move-exception v0 */
/* .line 1138 */
/* .local v0, "e":Ljava/lang/UnsupportedOperationException; */
android.util.Slog .e ( v8,v1 );
/* .line 1140 */
} // .end local v0 # "e":Ljava/lang/UnsupportedOperationException;
} // :goto_0
v0 = this.mNfcAdapter;
if ( v0 != null) { // if-eqz v0, :cond_3
v0 = (( android.nfc.NfcAdapter ) v0 ).isEnabled ( ); // invoke-virtual {v0}, Landroid/nfc/NfcAdapter;->isEnabled()Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 1141 */
v0 = this.mNfcAdapter;
v0 = (( android.nfc.NfcAdapter ) v0 ).disable ( ); // invoke-virtual {v0}, Landroid/nfc/NfcAdapter;->disable()Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1142 */
v0 = this.mContentResolver;
android.provider.Settings$Global .putInt ( v0,v6,v9 );
/* .line 1143 */
/* iput-boolean v9, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mClosedNfcFromCharging:Z */
/* .line 1144 */
/* iput-boolean v9, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mShowDisableNfc:Z */
/* .line 1146 */
} // :cond_2
final String v0 = "close NFC failed"; // const-string v0, "close NFC failed"
android.util.Slog .e ( v8,v0 );
/* .line 1149 */
} // :cond_3
} // :goto_1
/* iget-boolean v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mIsReverseWirelessCharge:Z */
/* if-nez v0, :cond_4 */
/* .line 1150 */
/* iget-boolean v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mShowDisableNfc:Z */
if ( v0 != null) { // if-eqz v0, :cond_12
/* .line 1151 */
v0 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmContext ( v0 );
/* const v1, 0x110f01cf */
android.widget.Toast .makeText ( v0,v1,v10 );
(( android.widget.Toast ) v0 ).show ( ); // invoke-virtual {v0}, Landroid/widget/Toast;->show()V
/* .line 1152 */
/* iput-boolean v10, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mShowDisableNfc:Z */
/* goto/16 :goto_6 */
/* .line 1155 */
} // :cond_4
v0 = /* invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->isSupportControlHaptic()Z */
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 1156 */
v0 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmContext ( v0 );
/* const v1, 0x110f01d0 */
android.widget.Toast .makeText ( v0,v1,v10 );
(( android.widget.Toast ) v0 ).show ( ); // invoke-virtual {v0}, Landroid/widget/Toast;->show()V
/* goto/16 :goto_6 */
/* .line 1158 */
} // :cond_5
/* iget-boolean v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mShowDisableNfc:Z */
if ( v0 != null) { // if-eqz v0, :cond_12
/* .line 1159 */
v0 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmContext ( v0 );
/* const v1, 0x110f01d1 */
android.widget.Toast .makeText ( v0,v1,v10 );
(( android.widget.Toast ) v0 ).show ( ); // invoke-virtual {v0}, Landroid/widget/Toast;->show()V
/* .line 1160 */
/* iput-boolean v10, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mShowDisableNfc:Z */
/* goto/16 :goto_6 */
/* .line 1084 */
/* :pswitch_14 */
/* iget v0, p1, Landroid/os/Message;->arg1:I */
/* .line 1085 */
/* .local v0, "txType":I */
v2 = this.mContentResolver;
v2 = android.provider.Settings$Global .getInt ( v2,v6,v10 );
/* if-lez v2, :cond_6 */
/* move v2, v9 */
} // :cond_6
/* move v2, v10 */
} // :goto_2
/* iput-boolean v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mClosedNfcFromCharging:Z */
/* .line 1087 */
try { // :try_start_2
v2 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmContext ( v2 );
android.nfc.NfcAdapter .getNfcAdapter ( v2 );
this.mNfcAdapter = v2;
/* :try_end_2 */
/* .catch Ljava/lang/UnsupportedOperationException; {:try_start_2 ..:try_end_2} :catch_2 */
/* .line 1090 */
/* .line 1088 */
/* :catch_2 */
/* move-exception v2 */
/* .line 1089 */
/* .local v2, "e":Ljava/lang/UnsupportedOperationException; */
android.util.Slog .e ( v8,v1 );
/* .line 1091 */
} // .end local v2 # "e":Ljava/lang/UnsupportedOperationException;
} // :goto_3
/* if-nez v0, :cond_9 */
/* iget-boolean v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mClosedNfcFromCharging:Z */
if ( v1 != null) { // if-eqz v1, :cond_9
v1 = this.mNfcAdapter;
if ( v1 != null) { // if-eqz v1, :cond_9
v1 = (( android.nfc.NfcAdapter ) v1 ).isEnabled ( ); // invoke-virtual {v1}, Landroid/nfc/NfcAdapter;->isEnabled()Z
/* if-nez v1, :cond_9 */
/* .line 1092 */
v1 = this.mNfcAdapter;
v1 = (( android.nfc.NfcAdapter ) v1 ).enable ( ); // invoke-virtual {v1}, Landroid/nfc/NfcAdapter;->enable()Z
if ( v1 != null) { // if-eqz v1, :cond_7
/* .line 1093 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "try to open NFC " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mCount:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " times success"; // const-string v2, " times success"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v8,v1 );
/* .line 1094 */
/* iput-boolean v10, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mClosedNfcFromCharging:Z */
/* .line 1095 */
/* iput-boolean v9, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mShowEnableNfc:Z */
/* .line 1096 */
/* iput v10, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mCount:I */
/* .line 1097 */
v1 = this.mContentResolver;
android.provider.Settings$Global .putInt ( v1,v6,v10 );
/* .line 1099 */
} // :cond_7
/* iget v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mCount:I */
/* if-ge v1, v7, :cond_8 */
/* .line 1100 */
/* add-int/2addr v1, v9 */
/* iput v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mCount:I */
/* .line 1101 */
/* const-wide/16 v1, 0x3e8 */
(( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) p0 ).sendMessageDelayed ( v5, v1, v2 ); // invoke-virtual {p0, v5, v1, v2}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessageDelayed(IJ)V
/* .line 1103 */
} // :cond_8
final String v1 = "open NFC failed"; // const-string v1, "open NFC failed"
android.util.Slog .d ( v8,v1 );
/* .line 1104 */
/* iput v10, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mCount:I */
/* .line 1105 */
/* iget-boolean v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mRetryAfterOneMin:Z */
/* if-nez v1, :cond_9 */
/* .line 1106 */
/* const-wide/32 v1, 0xea60 */
(( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) p0 ).sendMessageDelayed ( v5, v1, v2 ); // invoke-virtual {p0, v5, v1, v2}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessageDelayed(IJ)V
/* .line 1107 */
/* iput-boolean v9, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mRetryAfterOneMin:Z */
/* .line 1112 */
} // :cond_9
} // :goto_4
/* iget-boolean v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mIsReverseWirelessCharge:Z */
/* if-nez v1, :cond_a */
/* .line 1113 */
/* iget-boolean v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mShowEnableNfc:Z */
if ( v1 != null) { // if-eqz v1, :cond_12
/* .line 1114 */
v1 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmContext ( v1 );
/* const v2, 0x110f01e1 */
android.widget.Toast .makeText ( v1,v2,v10 );
(( android.widget.Toast ) v1 ).show ( ); // invoke-virtual {v1}, Landroid/widget/Toast;->show()V
/* .line 1115 */
/* iput-boolean v10, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mShowEnableNfc:Z */
/* goto/16 :goto_6 */
/* .line 1118 */
} // :cond_a
v1 = /* invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->isSupportControlHaptic()Z */
if ( v1 != null) { // if-eqz v1, :cond_d
/* .line 1119 */
v1 = this.mContentResolver;
final String v2 = "haptic_feedback_disable"; // const-string v2, "haptic_feedback_disable"
v1 = android.provider.Settings$System .getIntForUser ( v1,v2,v10,v10 );
/* if-lez v1, :cond_b */
} // :cond_b
/* move v9, v10 */
} // :goto_5
/* iput-boolean v9, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mHapticState:Z */
/* .line 1120 */
if ( v9 != null) { // if-eqz v9, :cond_c
/* .line 1122 */
final String v1 = "open haptic when wireless reverse charge"; // const-string v1, "open haptic when wireless reverse charge"
android.util.Slog .d ( v8,v1 );
/* .line 1123 */
v1 = this.mContentResolver;
android.provider.Settings$System .putIntForUser ( v1,v2,v10,v10 );
/* .line 1125 */
} // :cond_c
v1 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmContext ( v1 );
/* const v2, 0x110f01e2 */
android.widget.Toast .makeText ( v1,v2,v10 );
(( android.widget.Toast ) v1 ).show ( ); // invoke-virtual {v1}, Landroid/widget/Toast;->show()V
/* goto/16 :goto_6 */
/* .line 1127 */
} // :cond_d
/* iget-boolean v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mShowEnableNfc:Z */
if ( v1 != null) { // if-eqz v1, :cond_12
/* .line 1128 */
v1 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmContext ( v1 );
/* const v2, 0x110f01e3 */
android.widget.Toast .makeText ( v1,v2,v10 );
(( android.widget.Toast ) v1 ).show ( ); // invoke-virtual {v1}, Landroid/widget/Toast;->show()V
/* .line 1129 */
/* iput-boolean v10, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mShowEnableNfc:Z */
/* goto/16 :goto_6 */
/* .line 1079 */
} // .end local v0 # "txType":I
/* :pswitch_15 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "miui.intent.action.ACTION_SHUTDOWN_DELAY"; // const-string v1, "miui.intent.action.ACTION_SHUTDOWN_DELAY"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 1080 */
/* .local v0, "shutdownDelayIntent":Landroid/content/Intent; */
final String v1 = "miui.intent.extra.shutdown_delay"; // const-string v1, "miui.intent.extra.shutdown_delay"
/* iget v2, p1, Landroid/os/Message;->arg1:I */
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 1081 */
/* invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendStickyBroadcast(Landroid/content/Intent;)V */
/* .line 1082 */
/* goto/16 :goto_6 */
/* .line 1070 */
} // .end local v0 # "shutdownDelayIntent":Landroid/content/Intent;
/* :pswitch_16 */
/* iget v0, p1, Landroid/os/Message;->arg1:I */
/* .line 1071 */
/* .local v0, "openStatus":I */
/* if-lez v0, :cond_e */
/* .line 1073 */
/* invoke-direct {p0, v10}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->updateWirelessReverseChargingNotification(I)V */
/* .line 1075 */
} // :cond_e
/* iput-boolean v9, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mIsReverseWirelessCharge:Z */
/* .line 1076 */
/* if-lez v0, :cond_f */
/* const/16 v5, 0x9 */
} // :cond_f
(( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) p0 ).sendMessage ( v5, v10 ); // invoke-virtual {p0, v5, v10}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessage(II)V
/* .line 1077 */
/* goto/16 :goto_6 */
/* .line 1060 */
} // .end local v0 # "openStatus":I
/* :pswitch_17 */
/* iget v0, p1, Landroid/os/Message;->arg1:I */
/* .line 1061 */
/* .local v0, "closeReason":I */
/* if-ne v0, v9, :cond_10 */
/* .line 1062 */
/* invoke-direct {p0, v9}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->updateWirelessReverseChargingNotification(I)V */
/* goto/16 :goto_6 */
/* .line 1063 */
} // :cond_10
int v1 = 2; // const/4 v1, 0x2
/* if-ne v0, v1, :cond_11 */
/* .line 1064 */
/* invoke-direct {p0, v1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->updateWirelessReverseChargingNotification(I)V */
/* goto/16 :goto_6 */
/* .line 1065 */
} // :cond_11
/* if-ne v0, v7, :cond_12 */
/* .line 1066 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->showWirelessCharingWarningDialog()V */
/* .line 1054 */
} // .end local v0 # "closeReason":I
/* :pswitch_18 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "miui.intent.action.ACTION_SOC_DECIMAL"; // const-string v1, "miui.intent.action.ACTION_SOC_DECIMAL"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 1055 */
/* .local v0, "socDecimalIntent":Landroid/content/Intent; */
final String v1 = "miui.intent.extra.soc_decimal"; // const-string v1, "miui.intent.extra.soc_decimal"
/* iget v2, p1, Landroid/os/Message;->arg1:I */
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 1056 */
final String v1 = "miui.intent.extra.soc_decimal_rate"; // const-string v1, "miui.intent.extra.soc_decimal_rate"
/* iget v2, p1, Landroid/os/Message;->arg2:I */
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 1057 */
/* invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendStickyBroadcast(Landroid/content/Intent;)V */
/* .line 1058 */
/* .line 1044 */
} // .end local v0 # "socDecimalIntent":Landroid/content/Intent;
/* :pswitch_19 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "miui.intent.action.ACTION_QUICK_CHARGE_TYPE"; // const-string v1, "miui.intent.action.ACTION_QUICK_CHARGE_TYPE"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 1045 */
/* .local v0, "quickChargeTypeIntent":Landroid/content/Intent; */
final String v1 = "miui.intent.extra.quick_charge_type"; // const-string v1, "miui.intent.extra.quick_charge_type"
/* iget v2, p1, Landroid/os/Message;->arg1:I */
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 1046 */
final String v1 = "miui.intent.extra.POWER_MAX"; // const-string v1, "miui.intent.extra.POWER_MAX"
v2 = /* invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->getChargingPowerMax()I */
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 1047 */
final String v1 = "miui.intent.extra.CAR_CHARGE"; // const-string v1, "miui.intent.extra.CAR_CHARGE"
v2 = /* invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->getCarChargingType()I */
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 1048 */
/* invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendStickyBroadcast(Landroid/content/Intent;)V */
/* .line 1049 */
/* iget v1, p1, Landroid/os/Message;->arg1:I */
/* if-lt v1, v7, :cond_12 */
/* .line 1050 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendSocDecimaBroadcast()V */
/* .line 1039 */
} // .end local v0 # "quickChargeTypeIntent":Landroid/content/Intent;
/* :pswitch_1a */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "miui.intent.action.ACTION_HVDCP_TYPE"; // const-string v1, "miui.intent.action.ACTION_HVDCP_TYPE"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 1040 */
/* .local v0, "hvdcpTypeIntent":Landroid/content/Intent; */
final String v1 = "miui.intent.extra.hvdcp_type"; // const-string v1, "miui.intent.extra.hvdcp_type"
/* iget v2, p1, Landroid/os/Message;->arg1:I */
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 1041 */
/* invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendStickyBroadcast(Landroid/content/Intent;)V */
/* .line 1042 */
/* .line 1029 */
} // .end local v0 # "hvdcpTypeIntent":Landroid/content/Intent;
/* :pswitch_1b */
/* iget v0, p1, Landroid/os/Message;->arg1:I */
/* .line 1030 */
/* .local v0, "wirelessTxType":I */
/* new-instance v1, Landroid/content/Intent; */
final String v2 = "miui.intent.action.ACTION_WIRELESS_TX_TYPE"; // const-string v2, "miui.intent.action.ACTION_WIRELESS_TX_TYPE"
/* invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 1031 */
/* .local v1, "wirelessTxTypeIntent":Landroid/content/Intent; */
final String v2 = "miui.intent.extra.wireless_tx_type"; // const-string v2, "miui.intent.extra.wireless_tx_type"
(( android.content.Intent ) v1 ).putExtra ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 1032 */
/* invoke-direct {p0, v1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendStickyBroadcast(Landroid/content/Intent;)V */
/* .line 1033 */
/* iput-boolean v10, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mIsReverseWirelessCharge:Z */
/* .line 1036 */
/* iput-boolean v10, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mRetryAfterOneMin:Z */
/* .line 1037 */
/* .line 1020 */
} // .end local v0 # "wirelessTxType":I
} // .end local v1 # "wirelessTxTypeIntent":Landroid/content/Intent;
/* :pswitch_1c */
/* iget v0, p1, Landroid/os/Message;->arg1:I */
/* invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->shouldCloseWirelessReverseCharging(I)V */
/* .line 1021 */
/* nop */
/* .line 1256 */
} // :cond_12
} // :goto_6
return;
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_1c */
/* :pswitch_1b */
/* :pswitch_1a */
/* :pswitch_19 */
/* :pswitch_18 */
/* :pswitch_17 */
/* :pswitch_16 */
/* :pswitch_15 */
/* :pswitch_14 */
/* :pswitch_13 */
/* :pswitch_12 */
/* :pswitch_11 */
/* :pswitch_10 */
/* :pswitch_f */
/* :pswitch_e */
/* :pswitch_d */
/* :pswitch_c */
/* :pswitch_b */
/* :pswitch_a */
/* :pswitch_9 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public hexStringToByteArray ( java.lang.String p0 ) {
/* .locals 7 */
/* .param p1, "s" # Ljava/lang/String; */
/* .line 1568 */
v0 = (( java.lang.String ) p1 ).length ( ); // invoke-virtual {p1}, Ljava/lang/String;->length()I
/* .line 1569 */
/* .local v0, "len":I */
/* div-int/lit8 v1, v0, 0x2 */
/* new-array v1, v1, [B */
/* .line 1570 */
/* .local v1, "data":[B */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
/* if-ge v2, v0, :cond_0 */
/* .line 1572 */
try { // :try_start_0
/* div-int/lit8 v3, v2, 0x2 */
v4 = (( java.lang.String ) p1 ).charAt ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C
/* const/16 v5, 0x10 */
v4 = java.lang.Character .digit ( v4,v5 );
/* shl-int/lit8 v4, v4, 0x4 */
/* add-int/lit8 v6, v2, 0x1 */
/* .line 1573 */
v6 = (( java.lang.String ) p1 ).charAt ( v6 ); // invoke-virtual {p1, v6}, Ljava/lang/String;->charAt(I)C
v5 = java.lang.Character .digit ( v6,v5 );
/* add-int/2addr v4, v5 */
/* int-to-byte v4, v4 */
/* aput-byte v4, v1, v3 */
/* :try_end_0 */
/* .catch Ljava/lang/NumberFormatException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1577 */
/* nop */
/* .line 1570 */
/* add-int/lit8 v2, v2, 0x2 */
/* .line 1574 */
/* :catch_0 */
/* move-exception v3 */
/* .line 1575 */
/* .local v3, "e":Ljava/lang/NumberFormatException; */
v4 = java.lang.System.out;
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Invalid hex string: "; // const-string v6, "Invalid hex string: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( p1 ); // invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintStream ) v4 ).println ( v5 ); // invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
/* .line 1576 */
int v4 = 0; // const/4 v4, 0x0
/* .line 1579 */
} // .end local v2 # "i":I
} // .end local v3 # "e":Ljava/lang/NumberFormatException;
} // :cond_0
} // .end method
Boolean isHandleConnect ( ) {
/* .locals 2 */
/* .line 1592 */
v0 = this.mHandleInfo;
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget v0, v0, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->mConnectState:I */
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
} // .end method
public Integer parseInt ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "argument" # Ljava/lang/String; */
/* .line 798 */
try { // :try_start_0
v0 = java.lang.Integer .parseInt ( p1 );
/* :try_end_0 */
/* .catch Ljava/lang/NumberFormatException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 799 */
/* :catch_0 */
/* move-exception v0 */
/* .line 800 */
/* .local v0, "e":Ljava/lang/NumberFormatException; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Invalid integer argument "; // const-string v2, "Invalid integer argument "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiBatteryServiceImpl"; // const-string v2, "MiuiBatteryServiceImpl"
android.util.Slog .e ( v2,v1 );
/* .line 801 */
int v1 = -1; // const/4 v1, -0x1
} // .end method
public void sendMessage ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "what" # I */
/* .param p2, "arg1" # I */
/* .line 762 */
(( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) p0 ).removeMessages ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->removeMessages(I)V
/* .line 763 */
android.os.Message .obtain ( p0,p1 );
/* .line 764 */
/* .local v0, "m":Landroid/os/Message; */
/* iput p2, v0, Landroid/os/Message;->arg1:I */
/* .line 765 */
(( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) p0 ).sendMessage ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 766 */
return;
} // .end method
public void sendMessage ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 1 */
/* .param p1, "what" # I */
/* .param p2, "arg1" # I */
/* .param p3, "arg2" # I */
/* .line 769 */
(( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) p0 ).removeMessages ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->removeMessages(I)V
/* .line 770 */
android.os.Message .obtain ( p0,p1 );
/* .line 771 */
/* .local v0, "m":Landroid/os/Message; */
/* iput p2, v0, Landroid/os/Message;->arg1:I */
/* .line 772 */
/* iput p3, v0, Landroid/os/Message;->arg2:I */
/* .line 773 */
(( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) p0 ).sendMessage ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 774 */
return;
} // .end method
public void sendMessage ( Integer p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "what" # I */
/* .param p2, "arg" # Z */
/* .line 755 */
(( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) p0 ).removeMessages ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->removeMessages(I)V
/* .line 756 */
android.os.Message .obtain ( p0,p1 );
/* .line 757 */
/* .local v0, "m":Landroid/os/Message; */
/* iput p2, v0, Landroid/os/Message;->arg1:I */
/* .line 758 */
(( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) p0 ).sendMessage ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 759 */
return;
} // .end method
public void sendMessageDelayed ( Integer p0, Long p1 ) {
/* .locals 1 */
/* .param p1, "what" # I */
/* .param p2, "delayMillis" # J */
/* .line 777 */
(( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) p0 ).removeMessages ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->removeMessages(I)V
/* .line 778 */
android.os.Message .obtain ( p0,p1 );
/* .line 779 */
/* .local v0, "m":Landroid/os/Message; */
(( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) p0 ).sendMessageDelayed ( v0, p2, p3 ); // invoke-virtual {p0, v0, p2, p3}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 780 */
return;
} // .end method
public void sendMessageDelayed ( Integer p0, java.lang.Object p1, Long p2 ) {
/* .locals 1 */
/* .param p1, "what" # I */
/* .param p2, "arg" # Ljava/lang/Object; */
/* .param p3, "delayMillis" # J */
/* .line 790 */
(( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) p0 ).removeMessages ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->removeMessages(I)V
/* .line 791 */
android.os.Message .obtain ( p0,p1 );
/* .line 792 */
/* .local v0, "m":Landroid/os/Message; */
this.obj = p2;
/* .line 793 */
(( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) p0 ).sendMessageDelayed ( v0, p3, p4 ); // invoke-virtual {p0, v0, p3, p4}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 794 */
return;
} // .end method
public void sendMessageDelayed ( Integer p0, Boolean p1, Long p2 ) {
/* .locals 1 */
/* .param p1, "what" # I */
/* .param p2, "arg" # Z */
/* .param p3, "delayMillis" # J */
/* .line 783 */
(( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) p0 ).removeMessages ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->removeMessages(I)V
/* .line 784 */
android.os.Message .obtain ( p0,p1 );
/* .line 785 */
/* .local v0, "m":Landroid/os/Message; */
/* iput p2, v0, Landroid/os/Message;->arg1:I */
/* .line 786 */
(( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) p0 ).sendMessageDelayed ( v0, p3, p4 ); // invoke-virtual {p0, v0, p3, p4}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 787 */
return;
} // .end method
