.class public Lcom/android/server/appwidget/MiuiAppWidgetServiceImpl;
.super Lcom/android/server/appwidget/MiuiAppWidgetServiceStub;
.source "MiuiAppWidgetServiceImpl.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.appwidget.MiuiAppWidgetServiceStub$$"
.end annotation


# static fields
.field public static final ENABLED_WIDGETS:Ljava/lang/String; = "enabled_widgets"

.field private static final PKG_PA:Ljava/lang/String; = "com.miui.personalassistant"

.field private static TAG:Ljava/lang/String;


# instance fields
.field private mDebuggable:Z

.field private mSecurityManagerInternal:Lmiui/security/SecurityManagerInternal;

.field private mWindowManagerInternal:Lcom/android/server/wm/WindowManagerInternal;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 34
    const-string v0, "MiuiAppWidgetServiceImpl"

    sput-object v0, Lcom/android/server/appwidget/MiuiAppWidgetServiceImpl;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 33
    invoke-direct {p0}, Lcom/android/server/appwidget/MiuiAppWidgetServiceStub;-><init>()V

    return-void
.end method

.method private static convertDrawableToBitmap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;
    .locals 5
    .param p0, "drawable"    # Landroid/graphics/drawable/Drawable;

    .line 132
    if-nez p0, :cond_0

    .line 133
    const/4 v0, 0x0

    return-object v0

    .line 135
    :cond_0
    instance-of v0, p0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_1

    .line 136
    move-object v0, p0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 137
    .local v0, "bitmapDrawable":Landroid/graphics/drawable/BitmapDrawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 138
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    return-object v1

    .line 142
    .end local v0    # "bitmapDrawable":Landroid/graphics/drawable/BitmapDrawable;
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    if-lez v0, :cond_3

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    if-gtz v0, :cond_2

    goto :goto_0

    .line 146
    :cond_2
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 147
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 146
    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .local v0, "bitmap":Landroid/graphics/Bitmap;
    goto :goto_1

    .line 143
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_3
    :goto_0
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v1, 0x1

    invoke-static {v1, v1, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 149
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    :goto_1
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 150
    .local v1, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {p0, v4, v4, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 151
    invoke-virtual {p0, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 152
    return-object v0
.end method


# virtual methods
.method public checkDebugParams([Ljava/lang/String;)Z
    .locals 4
    .param p1, "args"    # [Ljava/lang/String;

    .line 157
    array-length v0, p1

    const/4 v1, 0x0

    if-lez v0, :cond_5

    const-string v0, "logging"

    aget-object v2, p1, v1

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 158
    const/4 v0, 0x1

    .line 159
    .local v0, "opti":I
    const/4 v1, 0x0

    .line 160
    .local v1, "debug":Z
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_4

    .line 161
    aget-object v2, p1, v0

    .line 162
    .local v2, "opt":Ljava/lang/String;
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-gtz v3, :cond_0

    .line 163
    goto :goto_2

    .line 165
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 167
    const-string v3, "enable-text"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 168
    const/4 v1, 0x1

    goto :goto_1

    .line 169
    :cond_1
    const-string v3, "disable-text"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 170
    const/4 v1, 0x0

    goto :goto_1

    .line 171
    :cond_2
    const-string v3, "DEBUG"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 172
    iput-boolean v1, p0, Lcom/android/server/appwidget/MiuiAppWidgetServiceImpl;->mDebuggable:Z

    .line 173
    goto :goto_2

    .line 175
    .end local v2    # "opt":Ljava/lang/String;
    :cond_3
    :goto_1
    goto :goto_0

    .line 176
    :cond_4
    :goto_2
    const/4 v2, 0x1

    return v2

    .line 178
    .end local v0    # "opti":I
    .end local v1    # "debug":Z
    :cond_5
    return v1
.end method

.method public isDebuggable()Z
    .locals 1

    .line 183
    iget-boolean v0, p0, Lcom/android/server/appwidget/MiuiAppWidgetServiceImpl;->mDebuggable:Z

    return v0
.end method

.method public isForMiui(Landroid/content/pm/IPackageManager;Landroid/appwidget/AppWidgetProviderInfo;I)Z
    .locals 4
    .param p1, "pm"    # Landroid/content/pm/IPackageManager;
    .param p2, "info"    # Landroid/appwidget/AppWidgetProviderInfo;
    .param p3, "userId"    # I

    .line 45
    const/4 v0, 0x0

    if-eqz p2, :cond_2

    iget-object v1, p2, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    if-nez v1, :cond_0

    goto :goto_1

    .line 49
    :cond_0
    :try_start_0
    iget-object v1, p2, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    const-wide/16 v2, 0x80

    invoke-interface {p1, v1, v2, v3, p3}, Landroid/content/pm/IPackageManager;->getReceiverInfo(Landroid/content/ComponentName;JI)Landroid/content/pm/ActivityInfo;

    move-result-object v1

    .line 50
    .local v1, "activityInfo":Landroid/content/pm/ActivityInfo;
    if-eqz v1, :cond_1

    iget-object v2, v1, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    if-eqz v2, :cond_1

    .line 52
    iget-object v2, v1, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    const-string v3, "miuiWidget"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 56
    .end local v1    # "activityInfo":Landroid/content/pm/ActivityInfo;
    :cond_1
    goto :goto_0

    .line 54
    :catch_0
    move-exception v1

    .line 55
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/android/server/appwidget/MiuiAppWidgetServiceImpl;->TAG:Ljava/lang/String;

    const-string v3, "isForMiui"

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 57
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return v0

    .line 46
    :cond_2
    :goto_1
    return v0
.end method

.method public isPACallingAndFocused(ILjava/lang/String;)Z
    .locals 5
    .param p1, "callingUid"    # I
    .param p2, "callingPackage"    # Ljava/lang/String;

    .line 91
    iget-object v0, p0, Lcom/android/server/appwidget/MiuiAppWidgetServiceImpl;->mWindowManagerInternal:Lcom/android/server/wm/WindowManagerInternal;

    if-nez v0, :cond_0

    .line 92
    const-class v0, Lcom/android/server/wm/WindowManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/WindowManagerInternal;

    iput-object v0, p0, Lcom/android/server/appwidget/MiuiAppWidgetServiceImpl;->mWindowManagerInternal:Lcom/android/server/wm/WindowManagerInternal;

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/android/server/appwidget/MiuiAppWidgetServiceImpl;->mWindowManagerInternal:Lcom/android/server/wm/WindowManagerInternal;

    invoke-virtual {v0, p1}, Lcom/android/server/wm/WindowManagerInternal;->isUidFocused(I)Z

    move-result v0

    .line 95
    .local v0, "isUidFocused":Z
    const-string v1, "com.miui.personalassistant"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 96
    .local v1, "isPA":Z
    sget-object v2, Lcom/android/server/appwidget/MiuiAppWidgetServiceImpl;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MIUILOG-PersonalAssistant isPACallingAndFocused: isUidFocused="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", isPA="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    return v2
.end method

.method public loadMaskWidgetView(Landroid/content/pm/ApplicationInfo;Landroid/widget/RemoteViews;Landroid/content/Context;)Z
    .locals 3
    .param p1, "appInfo"    # Landroid/content/pm/ApplicationInfo;
    .param p2, "views"    # Landroid/widget/RemoteViews;
    .param p3, "context"    # Landroid/content/Context;

    .line 120
    invoke-virtual {p3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 121
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_0

    .line 122
    invoke-static {v0}, Lcom/android/server/appwidget/MiuiAppWidgetServiceImpl;->convertDrawableToBitmap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 123
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    const v2, 0x10205be

    invoke-virtual {p2, v2, v1}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 124
    const/4 v2, 0x1

    return v2

    .line 126
    .end local v1    # "bitmap":Landroid/graphics/Bitmap;
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method public onWidgetProviderAddedOrChanged(Landroid/content/pm/IPackageManager;Landroid/appwidget/AppWidgetProviderInfo;I)V
    .locals 7
    .param p1, "pm"    # Landroid/content/pm/IPackageManager;
    .param p2, "info"    # Landroid/appwidget/AppWidgetProviderInfo;
    .param p3, "userId"    # I

    .line 103
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/server/appwidget/MiuiAppWidgetServiceImpl;->isForMiui(Landroid/content/pm/IPackageManager;Landroid/appwidget/AppWidgetProviderInfo;I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 104
    if-eqz p2, :cond_2

    iget-object v0, p2, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    if-nez v0, :cond_0

    goto :goto_0

    .line 107
    :cond_0
    iget-object v0, p0, Lcom/android/server/appwidget/MiuiAppWidgetServiceImpl;->mSecurityManagerInternal:Lmiui/security/SecurityManagerInternal;

    if-nez v0, :cond_1

    .line 108
    const-class v0, Lmiui/security/SecurityManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/security/SecurityManagerInternal;

    iput-object v0, p0, Lcom/android/server/appwidget/MiuiAppWidgetServiceImpl;->mSecurityManagerInternal:Lmiui/security/SecurityManagerInternal;

    .line 110
    :cond_1
    iget-object v1, p0, Lcom/android/server/appwidget/MiuiAppWidgetServiceImpl;->mSecurityManagerInternal:Lmiui/security/SecurityManagerInternal;

    if-eqz v1, :cond_3

    .line 111
    const/16 v2, 0x1f

    iget-object v0, p2, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    .line 112
    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-wide/16 v4, 0x1

    iget-object v0, p2, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    .line 113
    invoke-virtual {v0}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v6

    .line 111
    invoke-virtual/range {v1 .. v6}, Lmiui/security/SecurityManagerInternal;->recordAppBehaviorAsync(ILjava/lang/String;JLjava/lang/String;)V

    goto :goto_1

    .line 105
    :cond_2
    :goto_0
    return-void

    .line 116
    :cond_3
    :goto_1
    return-void
.end method

.method public updateWidgetPackagesLocked(Landroid/content/Context;Ljava/util/List;I)V
    .locals 8
    .param p1, "ctx"    # Landroid/content/Context;
    .param p3, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Lcom/android/server/appwidget/AppWidgetServiceImpl$Provider;",
            ">;I)V"
        }
    .end annotation

    .line 61
    .local p2, "mProviders":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/appwidget/AppWidgetServiceImpl$Provider;>;"
    invoke-static {}, Lcom/google/android/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    .line 62
    .local v0, "pkgSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    .line 63
    .local v1, "N":I
    const/4 v2, 0x0

    .local v2, "index":I
    :goto_0
    if-ge v2, v1, :cond_2

    .line 64
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/appwidget/AppWidgetServiceImpl$Provider;

    .line 66
    .local v3, "provider":Lcom/android/server/appwidget/AppWidgetServiceImpl$Provider;
    invoke-virtual {v3}, Lcom/android/server/appwidget/AppWidgetServiceImpl$Provider;->getUserId()I

    move-result v4

    if-eq v4, p3, :cond_0

    .line 67
    goto :goto_1

    .line 69
    :cond_0
    iget-object v4, v3, Lcom/android/server/appwidget/AppWidgetServiceImpl$Provider;->widgets:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_1

    .line 70
    iget-object v4, v3, Lcom/android/server/appwidget/AppWidgetServiceImpl$Provider;->info:Landroid/appwidget/AppWidgetProviderInfo;

    iget-object v4, v4, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 63
    .end local v3    # "provider":Lcom/android/server/appwidget/AppWidgetServiceImpl$Provider;
    :cond_1
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 73
    .end local v2    # "index":I
    :cond_2
    const/4 v2, 0x0

    .line 74
    .local v2, "sb":Ljava/lang/StringBuilder;
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 75
    .local v4, "pkg":Ljava/lang/String;
    if-nez v2, :cond_3

    .line 76
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object v2, v5

    goto :goto_3

    .line 78
    :cond_3
    const/16 v5, 0x3a

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 80
    :goto_3
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    .end local v4    # "pkg":Ljava/lang/String;
    goto :goto_2

    .line 82
    :cond_4
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v3

    .line 83
    .local v3, "identity":J
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    .line 85
    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_4

    :cond_5
    const-string v6, ""

    .line 83
    :goto_4
    const-string v7, "enabled_widgets"

    invoke-static {v5, v7, v6, p3}, Landroid/provider/Settings$Secure;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    .line 87
    invoke-static {v3, v4}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 88
    return-void
.end method
