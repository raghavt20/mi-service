public class com.android.server.appwidget.MiuiAppWidgetServiceImpl extends com.android.server.appwidget.MiuiAppWidgetServiceStub {
	 /* .source "MiuiAppWidgetServiceImpl.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.appwidget.MiuiAppWidgetServiceStub$$" */
} // .end annotation
/* # static fields */
public static final java.lang.String ENABLED_WIDGETS;
private static final java.lang.String PKG_PA;
private static java.lang.String TAG;
/* # instance fields */
private Boolean mDebuggable;
private miui.security.SecurityManagerInternal mSecurityManagerInternal;
private com.android.server.wm.WindowManagerInternal mWindowManagerInternal;
/* # direct methods */
static com.android.server.appwidget.MiuiAppWidgetServiceImpl ( ) {
	 /* .locals 1 */
	 /* .line 34 */
	 final String v0 = "MiuiAppWidgetServiceImpl"; // const-string v0, "MiuiAppWidgetServiceImpl"
	 return;
} // .end method
public com.android.server.appwidget.MiuiAppWidgetServiceImpl ( ) {
	 /* .locals 0 */
	 /* .line 33 */
	 /* invoke-direct {p0}, Lcom/android/server/appwidget/MiuiAppWidgetServiceStub;-><init>()V */
	 return;
} // .end method
private static android.graphics.Bitmap convertDrawableToBitmap ( android.graphics.drawable.Drawable p0 ) {
	 /* .locals 5 */
	 /* .param p0, "drawable" # Landroid/graphics/drawable/Drawable; */
	 /* .line 132 */
	 /* if-nez p0, :cond_0 */
	 /* .line 133 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* .line 135 */
} // :cond_0
/* instance-of v0, p0, Landroid/graphics/drawable/BitmapDrawable; */
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 136 */
	 /* move-object v0, p0 */
	 /* check-cast v0, Landroid/graphics/drawable/BitmapDrawable; */
	 /* .line 137 */
	 /* .local v0, "bitmapDrawable":Landroid/graphics/drawable/BitmapDrawable; */
	 (( android.graphics.drawable.BitmapDrawable ) v0 ).getBitmap ( ); // invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;
	 if ( v1 != null) { // if-eqz v1, :cond_1
		 /* .line 138 */
		 (( android.graphics.drawable.BitmapDrawable ) v0 ).getBitmap ( ); // invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;
		 /* .line 142 */
	 } // .end local v0 # "bitmapDrawable":Landroid/graphics/drawable/BitmapDrawable;
} // :cond_1
v0 = (( android.graphics.drawable.Drawable ) p0 ).getIntrinsicWidth ( ); // invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I
/* if-lez v0, :cond_3 */
v0 = (( android.graphics.drawable.Drawable ) p0 ).getIntrinsicHeight ( ); // invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I
/* if-gtz v0, :cond_2 */
/* .line 146 */
} // :cond_2
v0 = (( android.graphics.drawable.Drawable ) p0 ).getIntrinsicWidth ( ); // invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I
/* .line 147 */
v1 = (( android.graphics.drawable.Drawable ) p0 ).getIntrinsicHeight ( ); // invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I
v2 = android.graphics.Bitmap$Config.ARGB_8888;
/* .line 146 */
android.graphics.Bitmap .createBitmap ( v0,v1,v2 );
/* .local v0, "bitmap":Landroid/graphics/Bitmap; */
/* .line 143 */
} // .end local v0 # "bitmap":Landroid/graphics/Bitmap;
} // :cond_3
} // :goto_0
v0 = android.graphics.Bitmap$Config.ARGB_8888;
int v1 = 1; // const/4 v1, 0x1
android.graphics.Bitmap .createBitmap ( v1,v1,v0 );
/* .line 149 */
/* .restart local v0 # "bitmap":Landroid/graphics/Bitmap; */
} // :goto_1
/* new-instance v1, Landroid/graphics/Canvas; */
/* invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V */
/* .line 150 */
/* .local v1, "canvas":Landroid/graphics/Canvas; */
v2 = (( android.graphics.Canvas ) v1 ).getWidth ( ); // invoke-virtual {v1}, Landroid/graphics/Canvas;->getWidth()I
v3 = (( android.graphics.Canvas ) v1 ).getHeight ( ); // invoke-virtual {v1}, Landroid/graphics/Canvas;->getHeight()I
int v4 = 0; // const/4 v4, 0x0
(( android.graphics.drawable.Drawable ) p0 ).setBounds ( v4, v4, v2, v3 ); // invoke-virtual {p0, v4, v4, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V
/* .line 151 */
(( android.graphics.drawable.Drawable ) p0 ).draw ( v1 ); // invoke-virtual {p0, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V
/* .line 152 */
} // .end method
/* # virtual methods */
public Boolean checkDebugParams ( java.lang.String[] p0 ) {
/* .locals 4 */
/* .param p1, "args" # [Ljava/lang/String; */
/* .line 157 */
/* array-length v0, p1 */
int v1 = 0; // const/4 v1, 0x0
/* if-lez v0, :cond_5 */
final String v0 = "logging"; // const-string v0, "logging"
/* aget-object v2, p1, v1 */
v0 = (( java.lang.String ) v0 ).equals ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 158 */
int v0 = 1; // const/4 v0, 0x1
/* .line 159 */
/* .local v0, "opti":I */
int v1 = 0; // const/4 v1, 0x0
/* .line 160 */
/* .local v1, "debug":Z */
} // :goto_0
/* array-length v2, p1 */
/* if-ge v0, v2, :cond_4 */
/* .line 161 */
/* aget-object v2, p1, v0 */
/* .line 162 */
/* .local v2, "opt":Ljava/lang/String; */
if ( v2 != null) { // if-eqz v2, :cond_4
v3 = (( java.lang.String ) v2 ).length ( ); // invoke-virtual {v2}, Ljava/lang/String;->length()I
/* if-gtz v3, :cond_0 */
/* .line 163 */
/* .line 165 */
} // :cond_0
/* add-int/lit8 v0, v0, 0x1 */
/* .line 167 */
final String v3 = "enable-text"; // const-string v3, "enable-text"
v3 = (( java.lang.String ) v3 ).equals ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 168 */
int v1 = 1; // const/4 v1, 0x1
/* .line 169 */
} // :cond_1
final String v3 = "disable-text"; // const-string v3, "disable-text"
v3 = (( java.lang.String ) v3 ).equals ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 170 */
int v1 = 0; // const/4 v1, 0x0
/* .line 171 */
} // :cond_2
final String v3 = "DEBUG"; // const-string v3, "DEBUG"
v3 = (( java.lang.String ) v3 ).equals ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 172 */
/* iput-boolean v1, p0, Lcom/android/server/appwidget/MiuiAppWidgetServiceImpl;->mDebuggable:Z */
/* .line 173 */
/* .line 175 */
} // .end local v2 # "opt":Ljava/lang/String;
} // :cond_3
} // :goto_1
/* .line 176 */
} // :cond_4
} // :goto_2
int v2 = 1; // const/4 v2, 0x1
/* .line 178 */
} // .end local v0 # "opti":I
} // .end local v1 # "debug":Z
} // :cond_5
} // .end method
public Boolean isDebuggable ( ) {
/* .locals 1 */
/* .line 183 */
/* iget-boolean v0, p0, Lcom/android/server/appwidget/MiuiAppWidgetServiceImpl;->mDebuggable:Z */
} // .end method
public Boolean isForMiui ( android.content.pm.IPackageManager p0, android.appwidget.AppWidgetProviderInfo p1, Integer p2 ) {
/* .locals 4 */
/* .param p1, "pm" # Landroid/content/pm/IPackageManager; */
/* .param p2, "info" # Landroid/appwidget/AppWidgetProviderInfo; */
/* .param p3, "userId" # I */
/* .line 45 */
int v0 = 0; // const/4 v0, 0x0
if ( p2 != null) { // if-eqz p2, :cond_2
v1 = this.provider;
/* if-nez v1, :cond_0 */
/* .line 49 */
} // :cond_0
try { // :try_start_0
v1 = this.provider;
/* const-wide/16 v2, 0x80 */
/* .line 50 */
/* .local v1, "activityInfo":Landroid/content/pm/ActivityInfo; */
if ( v1 != null) { // if-eqz v1, :cond_1
v2 = this.metaData;
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 52 */
v2 = this.metaData;
final String v3 = "miuiWidget"; // const-string v3, "miuiWidget"
v0 = (( android.os.Bundle ) v2 ).getBoolean ( v3, v0 ); // invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 56 */
} // .end local v1 # "activityInfo":Landroid/content/pm/ActivityInfo;
} // :cond_1
/* .line 54 */
/* :catch_0 */
/* move-exception v1 */
/* .line 55 */
/* .local v1, "e":Ljava/lang/Exception; */
v2 = com.android.server.appwidget.MiuiAppWidgetServiceImpl.TAG;
final String v3 = "isForMiui"; // const-string v3, "isForMiui"
android.util.Slog .e ( v2,v3,v1 );
/* .line 57 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
/* .line 46 */
} // :cond_2
} // :goto_1
} // .end method
public Boolean isPACallingAndFocused ( Integer p0, java.lang.String p1 ) {
/* .locals 5 */
/* .param p1, "callingUid" # I */
/* .param p2, "callingPackage" # Ljava/lang/String; */
/* .line 91 */
v0 = this.mWindowManagerInternal;
/* if-nez v0, :cond_0 */
/* .line 92 */
/* const-class v0, Lcom/android/server/wm/WindowManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/wm/WindowManagerInternal; */
this.mWindowManagerInternal = v0;
/* .line 94 */
} // :cond_0
v0 = this.mWindowManagerInternal;
v0 = (( com.android.server.wm.WindowManagerInternal ) v0 ).isUidFocused ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/WindowManagerInternal;->isUidFocused(I)Z
/* .line 95 */
/* .local v0, "isUidFocused":Z */
final String v1 = "com.miui.personalassistant"; // const-string v1, "com.miui.personalassistant"
v1 = (( java.lang.String ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* .line 96 */
/* .local v1, "isPA":Z */
v2 = com.android.server.appwidget.MiuiAppWidgetServiceImpl.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "MIUILOG-PersonalAssistant isPACallingAndFocused: isUidFocused="; // const-string v4, "MIUILOG-PersonalAssistant isPACallingAndFocused: isUidFocused="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v4 = ", isPA="; // const-string v4, ", isPA="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v3 );
/* .line 98 */
if ( v1 != null) { // if-eqz v1, :cond_1
if ( v0 != null) { // if-eqz v0, :cond_1
int v2 = 1; // const/4 v2, 0x1
} // :cond_1
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
} // .end method
public Boolean loadMaskWidgetView ( android.content.pm.ApplicationInfo p0, android.widget.RemoteViews p1, android.content.Context p2 ) {
/* .locals 3 */
/* .param p1, "appInfo" # Landroid/content/pm/ApplicationInfo; */
/* .param p2, "views" # Landroid/widget/RemoteViews; */
/* .param p3, "context" # Landroid/content/Context; */
/* .line 120 */
(( android.content.Context ) p3 ).getPackageManager ( ); // invoke-virtual {p3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
(( android.content.pm.ApplicationInfo ) p1 ).loadIcon ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;
/* .line 121 */
/* .local v0, "drawable":Landroid/graphics/drawable/Drawable; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 122 */
com.android.server.appwidget.MiuiAppWidgetServiceImpl .convertDrawableToBitmap ( v0 );
/* .line 123 */
/* .local v1, "bitmap":Landroid/graphics/Bitmap; */
/* const v2, 0x10205be */
(( android.widget.RemoteViews ) p2 ).setImageViewBitmap ( v2, v1 ); // invoke-virtual {p2, v2, v1}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V
/* .line 124 */
int v2 = 1; // const/4 v2, 0x1
/* .line 126 */
} // .end local v1 # "bitmap":Landroid/graphics/Bitmap;
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public void onWidgetProviderAddedOrChanged ( android.content.pm.IPackageManager p0, android.appwidget.AppWidgetProviderInfo p1, Integer p2 ) {
/* .locals 7 */
/* .param p1, "pm" # Landroid/content/pm/IPackageManager; */
/* .param p2, "info" # Landroid/appwidget/AppWidgetProviderInfo; */
/* .param p3, "userId" # I */
/* .line 103 */
v0 = (( com.android.server.appwidget.MiuiAppWidgetServiceImpl ) p0 ).isForMiui ( p1, p2, p3 ); // invoke-virtual {p0, p1, p2, p3}, Lcom/android/server/appwidget/MiuiAppWidgetServiceImpl;->isForMiui(Landroid/content/pm/IPackageManager;Landroid/appwidget/AppWidgetProviderInfo;I)Z
/* if-nez v0, :cond_3 */
/* .line 104 */
if ( p2 != null) { // if-eqz p2, :cond_2
v0 = this.provider;
/* if-nez v0, :cond_0 */
/* .line 107 */
} // :cond_0
v0 = this.mSecurityManagerInternal;
/* if-nez v0, :cond_1 */
/* .line 108 */
/* const-class v0, Lmiui/security/SecurityManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lmiui/security/SecurityManagerInternal; */
this.mSecurityManagerInternal = v0;
/* .line 110 */
} // :cond_1
v1 = this.mSecurityManagerInternal;
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 111 */
/* const/16 v2, 0x1f */
v0 = this.provider;
/* .line 112 */
(( android.content.ComponentName ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* const-wide/16 v4, 0x1 */
v0 = this.provider;
/* .line 113 */
(( android.content.ComponentName ) v0 ).flattenToShortString ( ); // invoke-virtual {v0}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
/* .line 111 */
/* invoke-virtual/range {v1 ..v6}, Lmiui/security/SecurityManagerInternal;->recordAppBehaviorAsync(ILjava/lang/String;JLjava/lang/String;)V */
/* .line 105 */
} // :cond_2
} // :goto_0
return;
/* .line 116 */
} // :cond_3
} // :goto_1
return;
} // .end method
public void updateWidgetPackagesLocked ( android.content.Context p0, java.util.List p1, Integer p2 ) {
/* .locals 8 */
/* .param p1, "ctx" # Landroid/content/Context; */
/* .param p3, "userId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/content/Context;", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/appwidget/AppWidgetServiceImpl$Provider;", */
/* ">;I)V" */
/* } */
} // .end annotation
/* .line 61 */
/* .local p2, "mProviders":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/appwidget/AppWidgetServiceImpl$Provider;>;" */
com.google.android.collect.Sets .newHashSet ( );
/* .line 62 */
v1 = /* .local v0, "pkgSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
/* .line 63 */
/* .local v1, "N":I */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "index":I */
} // :goto_0
/* if-ge v2, v1, :cond_2 */
/* .line 64 */
/* check-cast v3, Lcom/android/server/appwidget/AppWidgetServiceImpl$Provider; */
/* .line 66 */
/* .local v3, "provider":Lcom/android/server/appwidget/AppWidgetServiceImpl$Provider; */
v4 = (( com.android.server.appwidget.AppWidgetServiceImpl$Provider ) v3 ).getUserId ( ); // invoke-virtual {v3}, Lcom/android/server/appwidget/AppWidgetServiceImpl$Provider;->getUserId()I
/* if-eq v4, p3, :cond_0 */
/* .line 67 */
/* .line 69 */
} // :cond_0
v4 = this.widgets;
v4 = (( java.util.ArrayList ) v4 ).size ( ); // invoke-virtual {v4}, Ljava/util/ArrayList;->size()I
/* if-lez v4, :cond_1 */
/* .line 70 */
v4 = this.info;
v4 = this.provider;
(( android.content.ComponentName ) v4 ).getPackageName ( ); // invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 63 */
} // .end local v3 # "provider":Lcom/android/server/appwidget/AppWidgetServiceImpl$Provider;
} // :cond_1
} // :goto_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 73 */
} // .end local v2 # "index":I
} // :cond_2
int v2 = 0; // const/4 v2, 0x0
/* .line 74 */
/* .local v2, "sb":Ljava/lang/StringBuilder; */
v4 = } // :goto_2
if ( v4 != null) { // if-eqz v4, :cond_4
/* check-cast v4, Ljava/lang/String; */
/* .line 75 */
/* .local v4, "pkg":Ljava/lang/String; */
/* if-nez v2, :cond_3 */
/* .line 76 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* move-object v2, v5 */
/* .line 78 */
} // :cond_3
/* const/16 v5, 0x3a */
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* .line 80 */
} // :goto_3
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 81 */
} // .end local v4 # "pkg":Ljava/lang/String;
/* .line 82 */
} // :cond_4
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v3 */
/* .line 83 */
/* .local v3, "identity":J */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 85 */
if ( v2 != null) { // if-eqz v2, :cond_5
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // :cond_5
final String v6 = ""; // const-string v6, ""
/* .line 83 */
} // :goto_4
final String v7 = "enabled_widgets"; // const-string v7, "enabled_widgets"
android.provider.Settings$Secure .putStringForUser ( v5,v7,v6,p3 );
/* .line 87 */
android.os.Binder .restoreCallingIdentity ( v3,v4 );
/* .line 88 */
return;
} // .end method
