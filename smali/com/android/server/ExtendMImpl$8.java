class com.android.server.ExtendMImpl$8 extends android.os.IVoldTaskListener$Stub {
	 /* .source "ExtendMImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/ExtendMImpl;->stopFlush()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.ExtendMImpl this$0; //synthetic
/* # direct methods */
 com.android.server.ExtendMImpl$8 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/ExtendMImpl; */
/* .line 1272 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/os/IVoldTaskListener$Stub;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onFinished ( Integer p0, android.os.PersistableBundle p1 ) {
/* .locals 2 */
/* .param p1, "status" # I */
/* .param p2, "extras" # Landroid/os/PersistableBundle; */
/* .line 1280 */
final String v0 = "ExtM"; // const-string v0, "ExtM"
/* if-nez p1, :cond_0 */
/* .line 1281 */
/* const-string/jumbo v1, "send SIGUSR1 to flush thread success" */
android.util.Slog .w ( v0,v1 );
/* .line 1282 */
v0 = this.this$0;
int v1 = 3; // const/4 v1, 0x3
com.android.server.ExtendMImpl .-$$Nest$mupdateState ( v0,v1 );
/* .line 1283 */
} // :cond_0
int v1 = -1; // const/4 v1, -0x1
/* if-ne p1, v1, :cond_1 */
/* .line 1284 */
final String v1 = "The flush thread is not running"; // const-string v1, "The flush thread is not running"
android.util.Slog .w ( v0,v1 );
/* .line 1286 */
} // :cond_1
} // :goto_0
v0 = this.this$0;
int v1 = 1; // const/4 v1, 0x1
com.android.server.ExtendMImpl .-$$Nest$fputisFlushFinished ( v0,v1 );
/* .line 1287 */
return;
} // .end method
public void onStatus ( Integer p0, android.os.PersistableBundle p1 ) {
/* .locals 0 */
/* .param p1, "status" # I */
/* .param p2, "extras" # Landroid/os/PersistableBundle; */
/* .line 1276 */
return;
} // .end method
