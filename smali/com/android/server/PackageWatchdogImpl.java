public class com.android.server.PackageWatchdogImpl extends com.android.server.PackageWatchdogStub {
	 /* .source "PackageWatchdogImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/PackageWatchdogImpl$ResultJudgeHandler; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.util.Set SYSTEMUI_DEPEND_APPS;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private final Long DELAY_TIME;
private android.os.HandlerThread ResultJudgeThread;
private com.android.server.PackageWatchdogImpl$ResultJudgeHandler mResultJudgeHandler;
java.util.HashMap rescuemap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/Integer;", */
/* "Lmiui/mqsas/sdk/event/GeneralExceptionEvent;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static com.android.server.PackageWatchdogImpl ( ) {
/* .locals 1 */
/* .line 45 */
/* new-instance v0, Lcom/android/server/PackageWatchdogImpl$1; */
/* invoke-direct {v0}, Lcom/android/server/PackageWatchdogImpl$1;-><init>()V */
return;
} // .end method
public com.android.server.PackageWatchdogImpl ( ) {
/* .locals 2 */
/* .line 89 */
/* invoke-direct {p0}, Lcom/android/server/PackageWatchdogStub;-><init>()V */
/* .line 41 */
/* const-wide/32 v0, 0x1b7740 */
/* iput-wide v0, p0, Lcom/android/server/PackageWatchdogImpl;->DELAY_TIME:J */
/* .line 53 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.rescuemap = v0;
/* .line 90 */
/* new-instance v0, Landroid/os/HandlerThread; */
final String v1 = "resultJudgeWork"; // const-string v1, "resultJudgeWork"
/* invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
this.ResultJudgeThread = v0;
/* .line 91 */
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 92 */
/* new-instance v0, Lcom/android/server/PackageWatchdogImpl$ResultJudgeHandler; */
v1 = this.ResultJudgeThread;
(( android.os.HandlerThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/PackageWatchdogImpl$ResultJudgeHandler;-><init>(Lcom/android/server/PackageWatchdogImpl;Landroid/os/Looper;)V */
this.mResultJudgeHandler = v0;
/* .line 93 */
return;
} // .end method
private void clearAppCacheAndData ( android.content.pm.PackageManager p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "pm" # Landroid/content/pm/PackageManager; */
/* .param p2, "currentCrashAppName" # Ljava/lang/String; */
/* .line 96 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Clear app cache and data: "; // const-string v1, "Clear app cache and data: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "RescuePartyPlus"; // const-string v2, "RescuePartyPlus"
android.util.Slog .w ( v2,v0 );
/* .line 97 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v1 = 3; // const/4 v1, 0x3
com.android.server.pm.PackageManagerServiceUtils .logCriticalInfo ( v1,v0 );
/* .line 98 */
int v0 = 0; // const/4 v0, 0x0
(( android.content.pm.PackageManager ) p1 ).deleteApplicationCacheFiles ( p2, v0 ); // invoke-virtual {p1, p2, v0}, Landroid/content/pm/PackageManager;->deleteApplicationCacheFiles(Ljava/lang/String;Landroid/content/pm/IPackageDataObserver;)V
/* .line 99 */
(( android.content.pm.PackageManager ) p1 ).clearApplicationUserData ( p2, v0 ); // invoke-virtual {p1, p2, v0}, Landroid/content/pm/PackageManager;->clearApplicationUserData(Ljava/lang/String;Landroid/content/pm/IPackageDataObserver;)V
/* .line 100 */
return;
} // .end method
static void lambda$maybeShowRecoveryTip$0 ( android.content.Context p0 ) { //synthethic
/* .locals 8 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 272 */
final String v0 = "ShowTipsUI Start"; // const-string v0, "ShowTipsUI Start"
final String v1 = "RescuePartyPlus"; // const-string v1, "RescuePartyPlus"
android.util.Slog .w ( v1,v0 );
/* .line 275 */
} // :goto_0
v0 = com.android.server.RescuePartyPlusHelper .checkBootCompletedStatus ( );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 276 */
v0 = com.android.server.RescuePartyPlusHelper .getShowResetConfigUIStatus ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* goto/16 :goto_1 */
/* .line 281 */
} // :cond_0
final String v0 = "Show RescueParty Plus Tips UI Ready!!!"; // const-string v0, "Show RescueParty Plus Tips UI Ready!!!"
android.util.Slog .w ( v1,v0 );
/* .line 284 */
/* const v0, 0x110f0318 */
(( android.content.Context ) p0 ).getString ( v0 ); // invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;
android.net.Uri .parse ( v0 );
/* .line 285 */
/* .local v0, "uri":Landroid/net/Uri; */
/* new-instance v1, Landroid/content/Intent; */
final String v2 = "android.intent.action.VIEW"; // const-string v2, "android.intent.action.VIEW"
/* invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V */
/* .line 286 */
/* .local v1, "intent":Landroid/content/Intent; */
final String v2 = "notification"; // const-string v2, "notification"
(( android.content.Context ) p0 ).getSystemService ( v2 ); // invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v2, Landroid/app/NotificationManager; */
/* .line 287 */
/* .local v2, "notificationManager":Landroid/app/NotificationManager; */
int v3 = 0; // const/4 v3, 0x0
/* const/high16 v4, 0x4000000 */
android.app.PendingIntent .getActivity ( p0,v3,v1,v4 );
/* .line 288 */
/* .local v3, "pendingIntent":Landroid/app/PendingIntent; */
/* new-instance v4, Landroid/app/Notification$Builder; */
v5 = com.android.internal.notification.SystemNotificationChannels.DEVELOPER_IMPORTANT;
/* invoke-direct {v4, p0, v5}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;)V */
/* .line 289 */
/* const v5, 0x1080078 */
(( android.app.Notification$Builder ) v4 ).setSmallIcon ( v5 ); // invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;
/* .line 290 */
/* const-string/jumbo v5, "sys" */
(( android.app.Notification$Builder ) v4 ).setCategory ( v5 ); // invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setCategory(Ljava/lang/String;)Landroid/app/Notification$Builder;
/* .line 291 */
int v5 = 1; // const/4 v5, 0x1
(( android.app.Notification$Builder ) v4 ).setShowWhen ( v5 ); // invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setShowWhen(Z)Landroid/app/Notification$Builder;
/* .line 292 */
/* const v6, 0x110f0317 */
(( android.content.Context ) p0 ).getString ( v6 ); // invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;
(( android.app.Notification$Builder ) v4 ).setContentTitle ( v6 ); // invoke-virtual {v4, v6}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;
/* .line 293 */
/* const v6, 0x110f0316 */
(( android.content.Context ) p0 ).getString ( v6 ); // invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;
(( android.app.Notification$Builder ) v4 ).setContentText ( v6 ); // invoke-virtual {v4, v6}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;
/* .line 294 */
(( android.app.Notification$Builder ) v4 ).setContentIntent ( v3 ); // invoke-virtual {v4, v3}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;
/* .line 295 */
(( android.app.Notification$Builder ) v4 ).setOngoing ( v5 ); // invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;
/* .line 296 */
int v6 = 5; // const/4 v6, 0x5
(( android.app.Notification$Builder ) v4 ).setPriority ( v6 ); // invoke-virtual {v4, v6}, Landroid/app/Notification$Builder;->setPriority(I)Landroid/app/Notification$Builder;
/* .line 297 */
int v6 = -1; // const/4 v6, -0x1
(( android.app.Notification$Builder ) v4 ).setDefaults ( v6 ); // invoke-virtual {v4, v6}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;
/* .line 298 */
(( android.app.Notification$Builder ) v4 ).setVisibility ( v5 ); // invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setVisibility(I)Landroid/app/Notification$Builder;
/* .line 299 */
(( android.app.Notification$Builder ) v4 ).setAutoCancel ( v5 ); // invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;
/* .line 300 */
(( android.app.Notification$Builder ) v4 ).build ( ); // invoke-virtual {v4}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;
/* .line 301 */
/* .local v4, "notification":Landroid/app/Notification; */
final String v6 = "RescueParty"; // const-string v6, "RescueParty"
v7 = (( java.lang.String ) v6 ).hashCode ( ); // invoke-virtual {v6}, Ljava/lang/String;->hashCode()I
(( android.app.NotificationManager ) v2 ).notify ( v6, v7, v4 ); // invoke-virtual {v2, v6, v7, v4}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V
/* .line 302 */
com.android.server.RescuePartyPlusHelper .setShowResetConfigUIStatus ( v5 );
/* .line 303 */
return;
/* .line 278 */
} // .end local v0 # "uri":Landroid/net/Uri;
} // .end local v1 # "intent":Landroid/content/Intent;
} // .end local v2 # "notificationManager":Landroid/app/NotificationManager;
} // .end local v3 # "pendingIntent":Landroid/app/PendingIntent;
} // .end local v4 # "notification":Landroid/app/Notification;
} // :cond_1
} // :goto_1
/* const-wide/16 v2, 0x2710 */
try { // :try_start_0
java.lang.Thread .sleep ( v2,v3 );
/* :try_end_0 */
/* .catch Ljava/lang/InterruptedException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 279 */
} // :goto_2
/* goto/16 :goto_0 */
/* :catch_0 */
/* move-exception v0 */
} // .end method
/* # virtual methods */
public Boolean doRescuePartyPlusStep ( Integer p0, android.content.pm.VersionedPackage p1, android.content.Context p2 ) {
/* .locals 16 */
/* .param p1, "mitigationCount" # I */
/* .param p2, "versionedPackage" # Landroid/content/pm/VersionedPackage; */
/* .param p3, "context" # Landroid/content/Context; */
/* .line 105 */
/* move-object/from16 v1, p0 */
/* move/from16 v2, p1 */
/* move-object/from16 v3, p2 */
v0 = com.android.server.RescuePartyPlusHelper .checkDisableRescuePartyPlus ( );
int v4 = 0; // const/4 v4, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 106 */
} // :cond_0
/* invoke-static/range {p1 ..p1}, Lcom/android/server/RescuePartyPlusHelper;->setMitigationTempCount(I)V */
/* .line 107 */
final String v5 = "RescuePartyPlus"; // const-string v5, "RescuePartyPlus"
if ( v3 != null) { // if-eqz v3, :cond_f
/* invoke-virtual/range {p2 ..p2}, Landroid/content/pm/VersionedPackage;->getPackageName()Ljava/lang/String; */
/* if-nez v0, :cond_1 */
/* goto/16 :goto_7 */
/* .line 111 */
} // :cond_1
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "doRescuePartyPlusStep "; // const-string v6, "doRescuePartyPlusStep "
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* invoke-virtual/range {p2 ..p2}, Landroid/content/pm/VersionedPackage;->getPackageName()Ljava/lang/String; */
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = ": ["; // const-string v6, ": ["
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = "]"; // const-string v6, "]"
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v5,v0 );
/* .line 112 */
/* invoke-virtual/range {p2 ..p2}, Landroid/content/pm/VersionedPackage;->getPackageName()Ljava/lang/String; */
/* .line 115 */
/* .local v6, "currentCrashAppName":Ljava/lang/String; */
v0 = com.android.server.RescuePartyPlusHelper .checkPackageIsCore ( v6 );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 116 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Skip because system crash: "; // const-string v7, "Skip because system crash: "
(( java.lang.StringBuilder ) v0 ).append ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v5,v0 );
/* .line 117 */
/* .line 119 */
} // :cond_2
/* new-instance v0, Lmiui/mqsas/sdk/event/GeneralExceptionEvent; */
/* invoke-direct {v0}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;-><init>()V */
/* move-object v7, v0 */
/* .line 120 */
/* .local v7, "event":Lmiui/mqsas/sdk/event/GeneralExceptionEvent; */
/* const/16 v0, 0x1ae */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v7 ).setType ( v0 ); // invoke-virtual {v7, v0}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setType(I)V
/* .line 121 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v7 ).setPackageName ( v6 ); // invoke-virtual {v7, v6}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setPackageName(Ljava/lang/String;)V
/* .line 122 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "rescueparty level: "; // const-string v8, "rescueparty level: "
(( java.lang.StringBuilder ) v0 ).append ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v7 ).setSummary ( v0 ); // invoke-virtual {v7, v0}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setSummary(Ljava/lang/String;)V
/* .line 123 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v8 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v7 ).setTimeStamp ( v8, v9 ); // invoke-virtual {v7, v8, v9}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setTimeStamp(J)V
/* .line 125 */
/* invoke-virtual/range {p3 ..p3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager; */
/* .line 126 */
/* .local v8, "pm":Landroid/content/pm/PackageManager; */
final String v0 = "Delete Top UI App cache and data: "; // const-string v0, "Delete Top UI App cache and data: "
final String v9 = "Clear app cache: "; // const-string v9, "Clear app cache: "
int v11 = 4; // const/4 v11, 0x4
final String v12 = " LEVEL_RESET_SETTINGS_UNTRUSTED_CHANGES;"; // const-string v12, " LEVEL_RESET_SETTINGS_UNTRUSTED_CHANGES;"
int v13 = 0; // const/4 v13, 0x0
final String v14 = " CLEAR_APP_CACHE_AND_DATA;"; // const-string v14, " CLEAR_APP_CACHE_AND_DATA;"
int v10 = 2; // const/4 v10, 0x2
int v15 = 1; // const/4 v15, 0x1
/* packed-switch v2, :pswitch_data_0 */
/* .line 229 */
int v9 = 5; // const/4 v9, 0x5
(( com.android.server.PackageWatchdogImpl ) v1 ).removeMessage ( v9, v6 ); // invoke-virtual {v1, v9, v6}, Lcom/android/server/PackageWatchdogImpl;->removeMessage(ILjava/lang/String;)V
/* .line 230 */
/* invoke-direct {v1, v8, v6}, Lcom/android/server/PackageWatchdogImpl;->clearAppCacheAndData(Landroid/content/pm/PackageManager;Ljava/lang/String;)V */
/* .line 231 */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v9 ).append ( v6 ); // invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v14 ); // invoke-virtual {v9, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 233 */
/* .local v9, "details":Ljava/lang/String; */
/* invoke-static/range {p3 ..p3}, Lcom/android/server/RescuePartyPlusHelper;->getLauncherPackageName(Landroid/content/Context;)Ljava/lang/String; */
v10 = (( java.lang.String ) v6 ).equals ( v10 ); // invoke-virtual {v6, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v10 != null) { // if-eqz v10, :cond_d
/* goto/16 :goto_6 */
/* .line 181 */
} // .end local v9 # "details":Ljava/lang/String;
/* :pswitch_0 */
(( com.android.server.PackageWatchdogImpl ) v1 ).removeMessage ( v11, v6 ); // invoke-virtual {v1, v11, v6}, Lcom/android/server/PackageWatchdogImpl;->removeMessage(ILjava/lang/String;)V
/* .line 182 */
/* new-instance v11, Ljava/lang/StringBuilder; */
/* invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v11 ).append ( v6 ); // invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).toString ( ); // invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 183 */
/* .local v11, "details":Ljava/lang/String; */
v12 = com.android.server.RescuePartyPlusHelper .checkPackageIsTOPUI ( v6 );
if ( v12 != null) { // if-eqz v12, :cond_4
/* .line 184 */
/* invoke-direct {v1, v8, v6}, Lcom/android/server/PackageWatchdogImpl;->clearAppCacheAndData(Landroid/content/pm/PackageManager;Ljava/lang/String;)V */
/* .line 185 */
v9 = com.android.server.PackageWatchdogImpl.SYSTEMUI_DEPEND_APPS;
v12 = } // :goto_0
if ( v12 != null) { // if-eqz v12, :cond_3
/* check-cast v12, Ljava/lang/String; */
/* .line 186 */
/* .local v12, "systemuiPlugin":Ljava/lang/String; */
(( android.content.pm.PackageManager ) v8 ).deletePackage ( v12, v13, v10 ); // invoke-virtual {v8, v12, v13, v10}, Landroid/content/pm/PackageManager;->deletePackage(Ljava/lang/String;Landroid/content/pm/IPackageDeleteObserver;I)V
/* .line 187 */
/* new-instance v14, Ljava/lang/StringBuilder; */
/* invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V */
final String v15 = "Try to rollback Top UI App plugin: "; // const-string v15, "Try to rollback Top UI App plugin: "
(( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v14 ).append ( v12 ); // invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v15 = " ("; // const-string v15, " ("
(( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v14 ).append ( v6 ); // invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v15 = ")"; // const-string v15, ")"
(( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v14 ).toString ( ); // invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v5,v14 );
/* .line 189 */
} // .end local v12 # "systemuiPlugin":Ljava/lang/String;
/* .line 190 */
} // :cond_3
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v9 ).append ( v0 ); // invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v5,v0 );
/* .line 191 */
/* .line 193 */
} // :cond_4
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v12 = "Try to rollback app and clear cache : "; // const-string v12, "Try to rollback app and clear cache : "
(( java.lang.StringBuilder ) v0 ).append ( v12 ); // invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v5,v0 );
/* .line 195 */
/* invoke-static/range {p3 ..p3}, Lcom/android/server/RescuePartyPlusHelper;->getLauncherPackageName(Landroid/content/Context;)Ljava/lang/String; */
v0 = (( java.lang.String ) v6 ).equals ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 196 */
/* invoke-direct {v1, v8, v6}, Lcom/android/server/PackageWatchdogImpl;->clearAppCacheAndData(Landroid/content/pm/PackageManager;Ljava/lang/String;)V */
/* .line 197 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v11 ); // invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v9 = "CLEAR_APP_CACHE_AND_DATA;"; // const-string v9, "CLEAR_APP_CACHE_AND_DATA;"
(( java.lang.StringBuilder ) v0 ).append ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* move-object v9, v0 */
} // .end local v11 # "details":Ljava/lang/String;
/* .local v0, "details":Ljava/lang/String; */
/* .line 199 */
} // .end local v0 # "details":Ljava/lang/String;
/* .restart local v11 # "details":Ljava/lang/String; */
} // :cond_5
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v5,v0 );
/* .line 200 */
(( android.content.pm.PackageManager ) v8 ).deleteApplicationCacheFiles ( v6, v13 ); // invoke-virtual {v8, v6, v13}, Landroid/content/pm/PackageManager;->deleteApplicationCacheFiles(Ljava/lang/String;Landroid/content/pm/IPackageDataObserver;)V
/* .line 201 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v11 ); // invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v9 = "DELETE_APPLICATION_CACHE_FILES;"; // const-string v9, "DELETE_APPLICATION_CACHE_FILES;"
(( java.lang.StringBuilder ) v0 ).append ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* move-object v9, v0 */
/* .line 203 */
} // .end local v11 # "details":Ljava/lang/String;
/* .restart local v9 # "details":Ljava/lang/String; */
} // :goto_1
int v11 = 0; // const/4 v11, 0x0
/* .line 205 */
/* .local v11, "applicationInfo":Landroid/content/pm/ApplicationInfo; */
try { // :try_start_0
(( android.content.pm.PackageManager ) v8 ).getApplicationInfo ( v6, v4 ); // invoke-virtual {v8, v6, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
/* move-object v11, v0 */
/* .line 206 */
if ( v11 != null) { // if-eqz v11, :cond_8
/* .line 207 */
v0 = (( android.content.pm.ApplicationInfo ) v11 ).isSystemApp ( ); // invoke-virtual {v11}, Landroid/content/pm/ApplicationInfo;->isSystemApp()Z
if ( v0 != null) { // if-eqz v0, :cond_6
v0 = (( android.content.pm.ApplicationInfo ) v11 ).isUpdatedSystemApp ( ); // invoke-virtual {v11}, Landroid/content/pm/ApplicationInfo;->isUpdatedSystemApp()Z
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 208 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "App install path: "; // const-string v4, "App install path: "
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.sourceDir;
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v5,v0 );
/* .line 209 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Finished rescue level ROLLBACK_APP for package "; // const-string v4, "Finished rescue level ROLLBACK_APP for package "
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v4 = 3; // const/4 v4, 0x3
com.android.server.pm.PackageManagerServiceUtils .logCriticalInfo ( v4,v0 );
/* .line 210 */
(( android.content.pm.PackageManager ) v8 ).deletePackage ( v6, v13, v10 ); // invoke-virtual {v8, v6, v13, v10}, Landroid/content/pm/PackageManager;->deletePackage(Ljava/lang/String;Landroid/content/pm/IPackageDeleteObserver;I)V
/* .line 211 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Uninstall app: "; // const-string v4, "Uninstall app: "
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v5,v0 );
/* .line 212 */
} // :cond_6
v0 = (( android.content.pm.ApplicationInfo ) v11 ).isSystemApp ( ); // invoke-virtual {v11}, Landroid/content/pm/ApplicationInfo;->isSystemApp()Z
/* if-nez v0, :cond_7 */
/* .line 213 */
/* invoke-static/range {p3 ..p3}, Lcom/android/server/RescuePartyPlusHelper;->getLauncherPackageName(Landroid/content/Context;)Ljava/lang/String; */
v0 = (( java.lang.String ) v6 ).equals ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_7
/* .line 214 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Third party Launcher, no action for app: "; // const-string v4, "Third party Launcher, no action for app: "
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v5,v0 );
/* .line 216 */
} // :cond_7
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "no action for app: "; // const-string v4, "no action for app: "
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v5,v0 );
/* :try_end_0 */
/* .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 221 */
} // :cond_8
} // :goto_2
/* .line 219 */
/* :catch_0 */
/* move-exception v0 */
/* .line 220 */
/* .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException; */
final String v4 = "get application info failed!"; // const-string v4, "get application info failed!"
android.util.Slog .e ( v5,v4,v0 );
/* .line 222 */
} // .end local v0 # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
} // :goto_3
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = "ROLLBACK_APP;"; // const-string v4, "ROLLBACK_APP;"
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 223 */
} // .end local v9 # "details":Ljava/lang/String;
/* .local v0, "details":Ljava/lang/String; */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v7 ).setDetails ( v0 ); // invoke-virtual {v7, v0}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setDetails(Ljava/lang/String;)V
/* .line 224 */
int v4 = 5; // const/4 v4, 0x5
(( com.android.server.PackageWatchdogImpl ) v1 ).sendMessage ( v6, v4, v7 ); // invoke-virtual {v1, v6, v4, v7}, Lcom/android/server/PackageWatchdogImpl;->sendMessage(Ljava/lang/String;ILmiui/mqsas/sdk/event/GeneralExceptionEvent;)V
/* .line 225 */
/* .line 155 */
} // .end local v0 # "details":Ljava/lang/String;
} // .end local v11 # "applicationInfo":Landroid/content/pm/ApplicationInfo;
/* :pswitch_1 */
int v0 = 3; // const/4 v0, 0x3
(( com.android.server.PackageWatchdogImpl ) v1 ).removeMessage ( v0, v6 ); // invoke-virtual {v1, v0, v6}, Lcom/android/server/PackageWatchdogImpl;->removeMessage(ILjava/lang/String;)V
/* .line 157 */
/* invoke-static/range {p3 ..p3}, Lcom/android/server/RescuePartyPlusHelper;->getLauncherPackageName(Landroid/content/Context;)Ljava/lang/String; */
v0 = (( java.lang.String ) v6 ).equals ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_9
/* .line 158 */
/* invoke-direct {v1, v8, v6}, Lcom/android/server/PackageWatchdogImpl;->clearAppCacheAndData(Landroid/content/pm/PackageManager;Ljava/lang/String;)V */
/* .line 159 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v14 ); // invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v7 ).setDetails ( v0 ); // invoke-virtual {v7, v0}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setDetails(Ljava/lang/String;)V
/* move-object/from16 v12, p3 */
/* goto/16 :goto_5 */
/* .line 160 */
} // :cond_9
v0 = com.android.server.RescuePartyPlusHelper .checkPackageIsTOPUI ( v6 );
if ( v0 != null) { // if-eqz v0, :cond_b
/* .line 162 */
/* invoke-direct {v1, v8, v6}, Lcom/android/server/PackageWatchdogImpl;->clearAppCacheAndData(Landroid/content/pm/PackageManager;Ljava/lang/String;)V */
/* .line 163 */
v0 = com.android.server.RescuePartyPlusHelper .resetTheme ( v6 );
/* if-nez v0, :cond_a */
/* .line 164 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "Reset theme failed: "; // const-string v9, "Reset theme failed: "
(( java.lang.StringBuilder ) v0 ).append ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v5,v0 );
/* .line 165 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v14 ); // invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v7 ).setDetails ( v0 ); // invoke-virtual {v7, v0}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setDetails(Ljava/lang/String;)V
/* .line 167 */
} // :cond_a
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " CLEAR_APP_CACHE_AND_DATA;RESET_THEME;"; // const-string v5, " CLEAR_APP_CACHE_AND_DATA;RESET_THEME;"
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v7 ).setDetails ( v0 ); // invoke-virtual {v7, v0}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setDetails(Ljava/lang/String;)V
/* .line 169 */
} // :goto_4
com.android.server.RescuePartyPlusHelper .setLastResetConfigStatus ( v15 );
/* .line 170 */
com.android.server.RescuePartyPlusHelper .setShowResetConfigUIStatus ( v4 );
/* .line 171 */
/* move-object/from16 v12, p3 */
(( com.android.server.PackageWatchdogImpl ) v1 ).maybeShowRecoveryTip ( v12 ); // invoke-virtual {v1, v12}, Lcom/android/server/PackageWatchdogImpl;->maybeShowRecoveryTip(Landroid/content/Context;)V
/* .line 173 */
} // :cond_b
/* move-object/from16 v12, p3 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v5,v0 );
/* .line 174 */
(( android.content.pm.PackageManager ) v8 ).deleteApplicationCacheFiles ( v6, v13 ); // invoke-virtual {v8, v6, v13}, Landroid/content/pm/PackageManager;->deleteApplicationCacheFiles(Ljava/lang/String;Landroid/content/pm/IPackageDataObserver;)V
/* .line 175 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " DELETE_APPLICATION_CACHE_FILES;"; // const-string v4, " DELETE_APPLICATION_CACHE_FILES;"
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v7 ).setDetails ( v0 ); // invoke-virtual {v7, v0}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setDetails(Ljava/lang/String;)V
/* .line 177 */
} // :goto_5
(( com.android.server.PackageWatchdogImpl ) v1 ).sendMessage ( v6, v11, v7 ); // invoke-virtual {v1, v6, v11, v7}, Lcom/android/server/PackageWatchdogImpl;->sendMessage(Ljava/lang/String;ILmiui/mqsas/sdk/event/GeneralExceptionEvent;)V
/* .line 178 */
/* .line 140 */
/* :pswitch_2 */
/* move-object/from16 v12, p3 */
(( com.android.server.PackageWatchdogImpl ) v1 ).removeMessage ( v10, v6 ); // invoke-virtual {v1, v10, v6}, Lcom/android/server/PackageWatchdogImpl;->removeMessage(ILjava/lang/String;)V
/* .line 141 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " LEVEL_RESET_SETTINGS_TRUSTED_DEFAULTS;"; // const-string v5, " LEVEL_RESET_SETTINGS_TRUSTED_DEFAULTS;"
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 143 */
/* .restart local v0 # "details":Ljava/lang/String; */
v5 = com.android.server.RescuePartyPlusHelper .checkPackageIsTOPUI ( v6 );
if ( v5 != null) { // if-eqz v5, :cond_c
/* .line 144 */
/* invoke-direct {v1, v8, v6}, Lcom/android/server/PackageWatchdogImpl;->clearAppCacheAndData(Landroid/content/pm/PackageManager;Ljava/lang/String;)V */
/* .line 145 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v14 ); // invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 146 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v7 ).setDetails ( v0 ); // invoke-virtual {v7, v0}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setDetails(Ljava/lang/String;)V
/* .line 147 */
int v5 = 3; // const/4 v5, 0x3
(( com.android.server.PackageWatchdogImpl ) v1 ).sendMessage ( v6, v5, v7 ); // invoke-virtual {v1, v6, v5, v7}, Lcom/android/server/PackageWatchdogImpl;->sendMessage(Ljava/lang/String;ILmiui/mqsas/sdk/event/GeneralExceptionEvent;)V
/* .line 148 */
/* .line 150 */
} // :cond_c
int v5 = 3; // const/4 v5, 0x3
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v7 ).setDetails ( v0 ); // invoke-virtual {v7, v0}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setDetails(Ljava/lang/String;)V
/* .line 151 */
(( com.android.server.PackageWatchdogImpl ) v1 ).sendMessage ( v6, v5, v7 ); // invoke-virtual {v1, v6, v5, v7}, Lcom/android/server/PackageWatchdogImpl;->sendMessage(Ljava/lang/String;ILmiui/mqsas/sdk/event/GeneralExceptionEvent;)V
/* .line 152 */
/* .line 134 */
} // .end local v0 # "details":Ljava/lang/String;
/* :pswitch_3 */
(( com.android.server.PackageWatchdogImpl ) v1 ).removeMessage ( v15, v6 ); // invoke-virtual {v1, v15, v6}, Lcom/android/server/PackageWatchdogImpl;->removeMessage(ILjava/lang/String;)V
/* .line 135 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v12 ); // invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v7 ).setDetails ( v0 ); // invoke-virtual {v7, v0}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setDetails(Ljava/lang/String;)V
/* .line 136 */
(( com.android.server.PackageWatchdogImpl ) v1 ).sendMessage ( v6, v10, v7 ); // invoke-virtual {v1, v6, v10, v7}, Lcom/android/server/PackageWatchdogImpl;->sendMessage(Ljava/lang/String;ILmiui/mqsas/sdk/event/GeneralExceptionEvent;)V
/* .line 137 */
/* .line 129 */
/* :pswitch_4 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " LEVEL_RESET_SETTINGS_UNTRUSTED_DEFAULTS;"; // const-string v5, " LEVEL_RESET_SETTINGS_UNTRUSTED_DEFAULTS;"
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v7 ).setDetails ( v0 ); // invoke-virtual {v7, v0}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setDetails(Ljava/lang/String;)V
/* .line 130 */
(( com.android.server.PackageWatchdogImpl ) v1 ).sendMessage ( v6, v15, v7 ); // invoke-virtual {v1, v6, v15, v7}, Lcom/android/server/PackageWatchdogImpl;->sendMessage(Ljava/lang/String;ILmiui/mqsas/sdk/event/GeneralExceptionEvent;)V
/* .line 131 */
/* .line 127 */
/* :pswitch_5 */
/* .line 235 */
/* .restart local v9 # "details":Ljava/lang/String; */
} // :cond_d
v10 = com.android.server.RescuePartyPlusHelper .checkPackageIsTOPUI ( v6 );
if ( v10 != null) { // if-eqz v10, :cond_e
/* .line 236 */
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v10 ).append ( v0 ); // invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v5,v0 );
/* .line 237 */
/* .line 239 */
} // :cond_e
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "Disable App restart, than clear app cache and data: "; // const-string v10, "Disable App restart, than clear app cache and data: "
(( java.lang.StringBuilder ) v0 ).append ( v10 ); // invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v5,v0 );
/* .line 240 */
com.android.server.RescuePartyPlusHelper .disableAppRestart ( v6 );
/* .line 241 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "DISABLE_APP_RESTART;"; // const-string v5, "DISABLE_APP_RESTART;"
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 257 */
} // :goto_6
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v7 ).setDetails ( v9 ); // invoke-virtual {v7, v9}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setDetails(Ljava/lang/String;)V
/* .line 258 */
(( com.android.server.PackageWatchdogImpl ) v1 ).sendMessage ( v6, v4, v7 ); // invoke-virtual {v1, v6, v4, v7}, Lcom/android/server/PackageWatchdogImpl;->sendMessage(Ljava/lang/String;ILmiui/mqsas/sdk/event/GeneralExceptionEvent;)V
/* .line 259 */
/* .line 108 */
} // .end local v6 # "currentCrashAppName":Ljava/lang/String;
} // .end local v7 # "event":Lmiui/mqsas/sdk/event/GeneralExceptionEvent;
} // .end local v8 # "pm":Landroid/content/pm/PackageManager;
} // .end local v9 # "details":Ljava/lang/String;
} // :cond_f
} // :goto_7
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Package Watchdog check versioned package failed: "; // const-string v6, "Package Watchdog check versioned package failed: "
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v5,v0 );
/* .line 109 */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public void maybeShowRecoveryTip ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 266 */
v0 = com.android.server.RescuePartyPlusHelper .checkDisableRescuePartyPlus ( );
/* if-nez v0, :cond_2 */
/* .line 267 */
v0 = com.android.server.RescuePartyPlusHelper .getConfigResetProcessStatus ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 268 */
} // :cond_0
v0 = com.android.server.RescuePartyPlusHelper .getLastResetConfigStatus ( );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 269 */
v0 = com.android.server.RescuePartyPlusHelper .getShowResetConfigUIStatus ( );
/* if-nez v0, :cond_1 */
/* .line 270 */
int v0 = 0; // const/4 v0, 0x0
com.android.server.RescuePartyPlusHelper .setLastResetConfigStatus ( v0 );
/* .line 271 */
/* new-instance v0, Ljava/lang/Thread; */
/* new-instance v1, Lcom/android/server/PackageWatchdogImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p1}, Lcom/android/server/PackageWatchdogImpl$$ExternalSyntheticLambda0;-><init>(Landroid/content/Context;)V */
/* invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V */
/* .line 303 */
(( java.lang.Thread ) v0 ).start ( ); // invoke-virtual {v0}, Ljava/lang/Thread;->start()V
/* .line 305 */
} // :cond_1
return;
/* .line 267 */
} // :cond_2
} // :goto_0
return;
} // .end method
public void recordMitigationCount ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "mitigationCount" # I */
/* .line 309 */
v0 = com.android.server.RescuePartyPlusHelper .checkDisableRescuePartyPlus ( );
if ( v0 != null) { // if-eqz v0, :cond_0
return;
/* .line 310 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "note SystemServer crash: "; // const-string v1, "note SystemServer crash: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "RescuePartyPlus"; // const-string v1, "RescuePartyPlus"
android.util.Slog .w ( v1,v0 );
/* .line 311 */
com.android.server.RescuePartyPlusHelper .setMitigationTempCount ( p1 );
/* .line 312 */
return;
} // .end method
public void removeMessage ( Integer p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "rescueLevel" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 81 */
v0 = this.rescuemap;
/* new-instance v1, Ljava/util/HashMap; */
/* invoke-direct {v1}, Ljava/util/HashMap;-><init>()V */
(( java.util.HashMap ) v0 ).getOrDefault ( p2, v1 ); // invoke-virtual {v0, p2, v1}, Ljava/util/HashMap;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/util/HashMap; */
/* .line 82 */
java.lang.Integer .valueOf ( p1 );
(( java.util.HashMap ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 83 */
v0 = this.rescuemap;
/* new-instance v1, Ljava/util/HashMap; */
/* invoke-direct {v1}, Ljava/util/HashMap;-><init>()V */
(( java.util.HashMap ) v0 ).getOrDefault ( p2, v1 ); // invoke-virtual {v0, p2, v1}, Ljava/util/HashMap;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/util/HashMap; */
/* .line 84 */
java.lang.Integer .valueOf ( p1 );
(( java.util.HashMap ) v0 ).remove ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lmiui/mqsas/sdk/event/GeneralExceptionEvent; */
/* .line 85 */
/* .local v0, "removeEvent":Lmiui/mqsas/sdk/event/GeneralExceptionEvent; */
v1 = this.mResultJudgeHandler;
(( com.android.server.PackageWatchdogImpl$ResultJudgeHandler ) v1 ).removeMessages ( p1, v0 ); // invoke-virtual {v1, p1, v0}, Lcom/android/server/PackageWatchdogImpl$ResultJudgeHandler;->removeMessages(ILjava/lang/Object;)V
/* .line 87 */
} // .end local v0 # "removeEvent":Lmiui/mqsas/sdk/event/GeneralExceptionEvent;
} // :cond_0
return;
} // .end method
public void sendMessage ( java.lang.String p0, Integer p1, miui.mqsas.sdk.event.GeneralExceptionEvent p2 ) {
/* .locals 5 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "rescueLevel" # I */
/* .param p3, "event" # Lmiui/mqsas/sdk/event/GeneralExceptionEvent; */
/* .line 72 */
v0 = this.rescuemap;
/* new-instance v1, Ljava/util/HashMap; */
/* invoke-direct {v1}, Ljava/util/HashMap;-><init>()V */
(( java.util.HashMap ) v0 ).getOrDefault ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/util/HashMap; */
/* .line 74 */
/* .local v0, "status":Ljava/util/HashMap; */
java.lang.Integer .valueOf ( p2 );
(( java.util.HashMap ) v0 ).put ( v1, p3 ); // invoke-virtual {v0, v1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 75 */
v1 = this.rescuemap;
(( java.util.HashMap ) v1 ).put ( p1, v0 ); // invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 76 */
v1 = this.mResultJudgeHandler;
(( com.android.server.PackageWatchdogImpl$ResultJudgeHandler ) v1 ).obtainMessage ( p2, p3 ); // invoke-virtual {v1, p2, p3}, Lcom/android/server/PackageWatchdogImpl$ResultJudgeHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 77 */
/* .local v1, "msg":Landroid/os/Message; */
v2 = this.mResultJudgeHandler;
/* const-wide/32 v3, 0x1b7740 */
(( com.android.server.PackageWatchdogImpl$ResultJudgeHandler ) v2 ).sendMessageDelayed ( v1, v3, v4 ); // invoke-virtual {v2, v1, v3, v4}, Lcom/android/server/PackageWatchdogImpl$ResultJudgeHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 78 */
return;
} // .end method
