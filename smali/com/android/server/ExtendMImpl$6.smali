.class Lcom/android/server/ExtendMImpl$6;
.super Landroid/os/IVoldTaskListener$Stub;
.source "ExtendMImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/ExtendMImpl;->runFlush(I)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/ExtendMImpl;


# direct methods
.method constructor <init>(Lcom/android/server/ExtendMImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/ExtendMImpl;

    .line 1202
    iput-object p1, p0, Lcom/android/server/ExtendMImpl$6;->this$0:Lcom/android/server/ExtendMImpl;

    invoke-direct {p0}, Landroid/os/IVoldTaskListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onFinished(ILandroid/os/PersistableBundle;)V
    .locals 8
    .param p1, "status"    # I
    .param p2, "extras"    # Landroid/os/PersistableBundle;

    .line 1210
    const-string v0, ""

    const-string v1, "MFZ"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Flush finished with status: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ExtM"

    invoke-static {v3, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1212
    const/16 v2, 0x80

    :try_start_0
    new-instance v4, Ljava/io/File;

    const-string v5, "/sys/block/zram0/idle_stat"

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1213
    invoke-static {v4, v2, v0}, Landroid/os/FileUtils;->readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1214
    .local v4, "idle_stats":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Flush finished! After Flush idle_stats: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1217
    nop

    .end local v4    # "idle_stats":Ljava/lang/String;
    goto :goto_0

    .line 1215
    :catch_0
    move-exception v4

    .line 1216
    .local v4, "e":Ljava/io/IOException;
    const-string v5, "After Flush idle_stats: error"

    invoke-static {v1, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1219
    .end local v4    # "e":Ljava/io/IOException;
    :goto_0
    :try_start_1
    new-instance v4, Ljava/io/File;

    const-string v5, "/sys/block/zram0/bd_stat"

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1220
    invoke-static {v4, v2, v0}, Landroid/os/FileUtils;->readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1221
    .local v0, "bd_stats":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Flush finished! After Flush bd_stats: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1224
    nop

    .end local v0    # "bd_stats":Ljava/lang/String;
    goto :goto_1

    .line 1222
    :catch_1
    move-exception v0

    .line 1223
    .local v0, "e":Ljava/io/IOException;
    const-string v2, "After Flush bd_stats: error"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1225
    .end local v0    # "e":Ljava/io/IOException;
    :goto_1
    const-wide/16 v4, 0x0

    .line 1226
    .local v4, "zramFree":J
    iget-object v0, p0, Lcom/android/server/ExtendMImpl$6;->this$0:Lcom/android/server/ExtendMImpl;

    invoke-static {v0}, Lcom/android/server/ExtendMImpl;->-$$Nest$mreadMemInfo(Lcom/android/server/ExtendMImpl;)V

    .line 1227
    iget-object v0, p0, Lcom/android/server/ExtendMImpl$6;->this$0:Lcom/android/server/ExtendMImpl;

    invoke-static {v0}, Lcom/android/server/ExtendMImpl;->-$$Nest$fgetmMemInfo(Lcom/android/server/ExtendMImpl;)Ljava/util/Map;

    move-result-object v0

    const-string v2, "SwapFree"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 1228
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Flush finished! After Flush zramFreeKb: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "KB"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1229
    iget-object v0, p0, Lcom/android/server/ExtendMImpl$6;->this$0:Lcom/android/server/ExtendMImpl;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/server/ExtendMImpl;->-$$Nest$fputisFlushFinished(Lcom/android/server/ExtendMImpl;Z)V

    .line 1230
    if-nez p1, :cond_1

    .line 1231
    iget-object v0, p0, Lcom/android/server/ExtendMImpl$6;->this$0:Lcom/android/server/ExtendMImpl;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/android/server/ExtendMImpl;->-$$Nest$fputlastFlushTime(Lcom/android/server/ExtendMImpl;J)V

    .line 1232
    iget-object v0, p0, Lcom/android/server/ExtendMImpl$6;->this$0:Lcom/android/server/ExtendMImpl;

    invoke-static {v0}, Lcom/android/server/ExtendMImpl;->-$$Nest$fgetprev_bd_write(Lcom/android/server/ExtendMImpl;)I

    move-result v1

    iget-object v2, p0, Lcom/android/server/ExtendMImpl$6;->this$0:Lcom/android/server/ExtendMImpl;

    invoke-static {v2}, Lcom/android/server/ExtendMImpl;->-$$Nest$fgetlastFlushTime(Lcom/android/server/ExtendMImpl;)J

    move-result-wide v6

    invoke-static {v0, v1, v6, v7}, Lcom/android/server/ExtendMImpl;->-$$Nest$mshowAndSaveFlushedStat(Lcom/android/server/ExtendMImpl;IJ)V

    .line 1233
    invoke-static {}, Lcom/android/server/ExtendMImpl;->-$$Nest$sfgetLOG_VERBOSE()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Exit flush"

    invoke-static {v3, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1234
    :cond_0
    iget-object v0, p0, Lcom/android/server/ExtendMImpl$6;->this$0:Lcom/android/server/ExtendMImpl;

    invoke-static {v0}, Lcom/android/server/ExtendMImpl;->-$$Nest$mshowTotalStat(Lcom/android/server/ExtendMImpl;)V

    goto :goto_2

    .line 1235
    :cond_1
    const/4 v0, -0x1

    if-ne p1, v0, :cond_2

    .line 1236
    const-string v0, "Flush process fork error "

    invoke-static {v3, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1238
    :cond_2
    :goto_2
    return-void
.end method

.method public onStatus(ILandroid/os/PersistableBundle;)V
    .locals 0
    .param p1, "status"    # I
    .param p2, "extras"    # Landroid/os/PersistableBundle;

    .line 1206
    return-void
.end method
