public class com.android.server.AppOpsServiceStubImpl extends com.android.server.appop.AppOpsServiceStub implements com.miui.app.AppOpsServiceInternal {
	 /* .source "AppOpsServiceStubImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.appop.AppOpsServiceStub$$" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lcom/android/server/AppOpsServiceStubImpl$UserState;, */
/* Lcom/android/server/AppOpsServiceStubImpl$Callback; */
/* } */
} // .end annotation
/* # static fields */
public static final Boolean DEBUG;
private static final java.lang.String KEY_APP_BEHAVIOR_RECORD_ENABLE;
private static final java.util.Set MESSAGE_NOT_RECORD;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.lang.String MESSAGE_NO_ASK;
private static final java.lang.String MESSAGE_NO_ASK_NO_RECORD;
private static final android.util.ArrayMap PLATFORM_PERMISSIONS;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArrayMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.lang.String TAG;
private static final java.lang.String WIFI_MESSAGE_STARTWITH;
private static final java.util.Set sCtsIgnore;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.util.Map supportVirtualGrant;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private android.app.ActivityManagerInternal mActivityManagerInternal;
private com.android.server.appop.AppOpsService mAppOpsService;
private android.content.Context mContext;
private android.content.pm.PackageManagerInternal mPackageManagerInternal;
private Boolean mRecordEnable;
private miui.security.SecurityManagerInternal mSecurityManagerInternal;
final android.util.SparseArray mUidStates;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Lcom/android/server/AppOpsServiceStubImpl$UserState;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
public static void $r8$lambda$GbgRas45nSDKpjmp1ouQqx7_qXg ( com.android.server.AppOpsServiceStubImpl p0, Integer p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/AppOpsServiceStubImpl;->lambda$startService$0(I)V */
return;
} // .end method
static void -$$Nest$mstartService ( com.android.server.AppOpsServiceStubImpl p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/AppOpsServiceStubImpl;->startService(I)V */
return;
} // .end method
static void -$$Nest$mupdateRecordState ( com.android.server.AppOpsServiceStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/AppOpsServiceStubImpl;->updateRecordState()V */
return;
} // .end method
static com.android.server.AppOpsServiceStubImpl ( ) {
/* .locals 5 */
/* .line 78 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 81 */
/* new-instance v1, Ljava/util/HashSet; */
/* invoke-direct {v1}, Ljava/util/HashSet;-><init>()V */
/* .line 107 */
/* new-instance v2, Landroid/util/ArrayMap; */
/* invoke-direct {v2}, Landroid/util/ArrayMap;-><init>()V */
/* .line 108 */
final String v3 = "android.permission.SEND_SMS"; // const-string v3, "android.permission.SEND_SMS"
final String v4 = "android.permission-group.SMS"; // const-string v4, "android.permission-group.SMS"
(( android.util.ArrayMap ) v2 ).put ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 109 */
final String v3 = "android.permission.RECEIVE_SMS"; // const-string v3, "android.permission.RECEIVE_SMS"
(( android.util.ArrayMap ) v2 ).put ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 110 */
final String v3 = "android.permission.READ_SMS"; // const-string v3, "android.permission.READ_SMS"
(( android.util.ArrayMap ) v2 ).put ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 111 */
final String v3 = "android.permission.RECEIVE_MMS"; // const-string v3, "android.permission.RECEIVE_MMS"
(( android.util.ArrayMap ) v2 ).put ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 112 */
final String v3 = "android.permission.RECEIVE_WAP_PUSH"; // const-string v3, "android.permission.RECEIVE_WAP_PUSH"
(( android.util.ArrayMap ) v2 ).put ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 113 */
final String v3 = "android.permission.READ_CELL_BROADCASTS"; // const-string v3, "android.permission.READ_CELL_BROADCASTS"
(( android.util.ArrayMap ) v2 ).put ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 115 */
final String v2 = "android.app.usage.cts"; // const-string v2, "android.app.usage.cts"
/* .line 116 */
final String v2 = "com.android.cts.usepermission"; // const-string v2, "com.android.cts.usepermission"
/* .line 117 */
final String v2 = "com.android.cts.permission"; // const-string v2, "com.android.cts.permission"
/* .line 118 */
final String v2 = "com.android.cts.netlegacy22.permission"; // const-string v2, "com.android.cts.netlegacy22.permission"
/* .line 119 */
final String v2 = "android.netlegacy22.permission.cts"; // const-string v2, "android.netlegacy22.permission.cts"
/* .line 120 */
final String v2 = "android.provider.cts"; // const-string v2, "android.provider.cts"
/* .line 121 */
final String v2 = "android.telephony2.cts"; // const-string v2, "android.telephony2.cts"
/* .line 122 */
final String v2 = "android.permission.cts"; // const-string v2, "android.permission.cts"
/* .line 123 */
final String v2 = "com.android.cts.writeexternalstorageapp"; // const-string v2, "com.android.cts.writeexternalstorageapp"
/* .line 124 */
final String v2 = "com.android.cts.readexternalstorageapp"; // const-string v2, "com.android.cts.readexternalstorageapp"
/* .line 125 */
final String v2 = "com.android.cts.externalstorageapp"; // const-string v2, "com.android.cts.externalstorageapp"
/* .line 126 */
final String v2 = "android.server.alertwindowapp"; // const-string v2, "android.server.alertwindowapp"
/* .line 127 */
final String v2 = "android.server.alertwindowappsdk25"; // const-string v2, "android.server.alertwindowappsdk25"
/* .line 128 */
final String v2 = "com.android.app2"; // const-string v2, "com.android.app2"
/* .line 129 */
final String v2 = "com.android.cts.appbinding.app"; // const-string v2, "com.android.cts.appbinding.app"
/* .line 130 */
final String v2 = "com.android.cts.launcherapps.simplepremapp"; // const-string v2, "com.android.cts.launcherapps.simplepremapp"
/* .line 131 */
final String v0 = "AutoStartManagerService#signalStopProcessesLocked"; // const-string v0, "AutoStartManagerService#signalStopProcessesLocked"
/* .line 132 */
final String v0 = "ContentProvider#enforceReadPermission"; // const-string v0, "ContentProvider#enforceReadPermission"
/* .line 133 */
final String v0 = "ContentProvider#enforceWritePermission"; // const-string v0, "ContentProvider#enforceWritePermission"
/* .line 134 */
final String v0 = "AppOpsHelper#checkLocationAccess"; // const-string v0, "AppOpsHelper#checkLocationAccess"
/* .line 135 */
final String v0 = "BroadcastQueueImpl#specifyBroadcast"; // const-string v0, "BroadcastQueueImpl#specifyBroadcast"
/* .line 136 */
final String v0 = "WifiPermissionsUtil#noteAppOpAllowed"; // const-string v0, "WifiPermissionsUtil#noteAppOpAllowed"
/* .line 442 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* .line 445 */
/* const/16 v1, 0xe */
java.lang.Integer .valueOf ( v1 );
/* const/16 v2, 0x272c */
java.lang.Integer .valueOf ( v2 );
/* .line 446 */
int v1 = 4; // const/4 v1, 0x4
java.lang.Integer .valueOf ( v1 );
/* const/16 v2, 0x272d */
java.lang.Integer .valueOf ( v2 );
/* .line 447 */
/* const/16 v1, 0x8 */
java.lang.Integer .valueOf ( v1 );
/* const/16 v2, 0x272e */
java.lang.Integer .valueOf ( v2 );
/* .line 448 */
int v1 = 6; // const/4 v1, 0x6
java.lang.Integer .valueOf ( v1 );
/* const/16 v2, 0x272f */
java.lang.Integer .valueOf ( v2 );
/* .line 449 */
return;
} // .end method
public com.android.server.AppOpsServiceStubImpl ( ) {
/* .locals 1 */
/* .line 69 */
/* invoke-direct {p0}, Lcom/android/server/appop/AppOpsServiceStub;-><init>()V */
/* .line 95 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
this.mUidStates = v0;
/* .line 97 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/AppOpsServiceStubImpl;->mRecordEnable:Z */
return;
} // .end method
private android.app.ActivityManagerInternal getActivityManagerInternal ( ) {
/* .locals 1 */
/* .line 205 */
v0 = this.mActivityManagerInternal;
/* if-nez v0, :cond_0 */
/* .line 206 */
/* const-class v0, Landroid/app/ActivityManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Landroid/app/ActivityManagerInternal; */
this.mActivityManagerInternal = v0;
/* .line 208 */
} // :cond_0
v0 = this.mActivityManagerInternal;
} // .end method
private com.android.server.appop.AppOpsService getAppOpsService ( ) {
/* .locals 2 */
/* .line 574 */
v0 = this.mAppOpsService;
/* if-nez v0, :cond_0 */
/* .line 575 */
final String v0 = "appops"; // const-string v0, "appops"
android.os.ServiceManager .getService ( v0 );
/* .line 576 */
/* .local v0, "b":Landroid/os/IBinder; */
com.android.internal.app.IAppOpsService$Stub .asInterface ( v0 );
/* check-cast v1, Lcom/android/server/appop/AppOpsService; */
this.mAppOpsService = v1;
/* .line 578 */
} // .end local v0 # "b":Landroid/os/IBinder;
} // :cond_0
v0 = this.mAppOpsService;
} // .end method
private android.content.pm.PackageManagerInternal getPackageManagerInternal ( ) {
/* .locals 1 */
/* .line 212 */
v0 = this.mPackageManagerInternal;
/* if-nez v0, :cond_0 */
/* .line 213 */
/* const-class v0, Landroid/content/pm/PackageManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Landroid/content/pm/PackageManagerInternal; */
this.mPackageManagerInternal = v0;
/* .line 215 */
} // :cond_0
v0 = this.mPackageManagerInternal;
} // .end method
private miui.security.SecurityManagerInternal getSecurityManager ( ) {
/* .locals 1 */
/* .line 555 */
v0 = this.mSecurityManagerInternal;
/* if-nez v0, :cond_0 */
/* .line 556 */
/* const-class v0, Lmiui/security/SecurityManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lmiui/security/SecurityManagerInternal; */
this.mSecurityManagerInternal = v0;
/* .line 558 */
} // :cond_0
v0 = this.mSecurityManagerInternal;
} // .end method
private synchronized com.android.server.AppOpsServiceStubImpl$UserState getUidState ( Integer p0, Boolean p1 ) {
/* .locals 3 */
/* .param p1, "userHandle" # I */
/* .param p2, "edit" # Z */
/* monitor-enter p0 */
/* .line 153 */
try { // :try_start_0
v0 = this.mUidStates;
(( android.util.SparseArray ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/AppOpsServiceStubImpl$UserState; */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 154 */
/* .local v0, "userState":Lcom/android/server/AppOpsServiceStubImpl$UserState; */
/* if-nez v0, :cond_1 */
/* .line 155 */
int v1 = 0; // const/4 v1, 0x0
/* if-nez p2, :cond_0 */
/* .line 156 */
/* monitor-exit p0 */
/* .line 158 */
} // :cond_0
try { // :try_start_1
/* new-instance v2, Lcom/android/server/AppOpsServiceStubImpl$UserState; */
/* invoke-direct {v2, v1}, Lcom/android/server/AppOpsServiceStubImpl$UserState;-><init>(Lcom/android/server/AppOpsServiceStubImpl$UserState-IA;)V */
/* move-object v0, v2 */
/* .line 159 */
v1 = this.mUidStates;
(( android.util.SparseArray ) v1 ).put ( p1, v0 ); // invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 161 */
} // .end local p0 # "this":Lcom/android/server/AppOpsServiceStubImpl;
} // :cond_1
/* monitor-exit p0 */
/* .line 152 */
} // .end local v0 # "userState":Lcom/android/server/AppOpsServiceStubImpl$UserState;
} // .end local p1 # "userHandle":I
} // .end local p2 # "edit":Z
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
public static Boolean isCtsIgnore ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p0, "packageName" # Ljava/lang/String; */
/* .line 145 */
v0 = v0 = com.android.server.AppOpsServiceStubImpl.sCtsIgnore;
} // .end method
private void lambda$startService$0 ( Integer p0 ) { //synthethic
/* .locals 3 */
/* .param p1, "userId" # I */
/* .line 375 */
try { // :try_start_0
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "com.miui.permission.Action.SecurityService"; // const-string v1, "com.miui.permission.Action.SecurityService"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 376 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "com.lbe.security.miui"; // const-string v1, "com.lbe.security.miui"
(( android.content.Intent ) v0 ).setPackage ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 377 */
v1 = this.mContext;
/* new-instance v2, Landroid/os/UserHandle; */
/* invoke-direct {v2, p1}, Landroid/os/UserHandle;-><init>(I)V */
(( android.content.Context ) v1 ).startServiceAsUser ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 381 */
/* nop */
} // .end local v0 # "intent":Landroid/content/Intent;
/* .line 378 */
/* :catch_0 */
/* move-exception v0 */
/* .line 380 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "AppOpsServiceStubImpl"; // const-string v1, "AppOpsServiceStubImpl"
final String v2 = "Start Error"; // const-string v2, "Start Error"
android.util.Slog .e ( v1,v2,v0 );
/* .line 382 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void startService ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "userId" # I */
/* .line 373 */
/* new-instance v0, Landroid/os/Handler; */
android.os.Looper .getMainLooper ( );
/* invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* new-instance v1, Lcom/android/server/AppOpsServiceStubImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/AppOpsServiceStubImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/AppOpsServiceStubImpl;I)V */
/* const-wide/16 v2, 0x514 */
(( android.os.Handler ) v0 ).postDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 383 */
return;
} // .end method
private void updateRecordState ( ) {
/* .locals 3 */
/* .line 181 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "key_app_behavior_record_enable"; // const-string v1, "key_app_behavior_record_enable"
int v2 = 1; // const/4 v2, 0x1
v0 = android.provider.Settings$Global .getInt ( v0,v1,v2 );
/* if-ne v0, v2, :cond_0 */
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
/* iput-boolean v2, p0, Lcom/android/server/AppOpsServiceStubImpl;->mRecordEnable:Z */
/* .line 182 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "updateRecordState: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/AppOpsServiceStubImpl;->mRecordEnable:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "AppOpsServiceStubImpl"; // const-string v1, "AppOpsServiceStubImpl"
android.util.Slog .d ( v1,v0 );
/* .line 183 */
return;
} // .end method
/* # virtual methods */
public Integer askOperationLocked ( Integer p0, Integer p1, java.lang.String p2, java.lang.String p3 ) {
/* .locals 7 */
/* .param p1, "code" # I */
/* .param p2, "uid" # I */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .param p4, "message" # Ljava/lang/String; */
/* .line 187 */
final String v0 = "noteNoAsk"; // const-string v0, "noteNoAsk"
v0 = (( java.lang.String ) v0 ).equals ( p4 ); // invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v1 = 1; // const/4 v1, 0x1
/* if-nez v0, :cond_3 */
final String v0 = "NoAskNoRecord"; // const-string v0, "NoAskNoRecord"
v0 = (( java.lang.String ) v0 ).equals ( p4 ); // invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 190 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* .line 191 */
/* .local v0, "result":I */
v2 = android.os.UserHandle .getUserId ( p2 );
/* .line 192 */
/* .local v2, "userId":I */
/* const/16 v3, 0x3e7 */
/* if-ne v2, v3, :cond_1 */
/* .line 193 */
int v2 = 0; // const/4 v2, 0x0
/* .line 194 */
v3 = android.os.UserHandle .getAppId ( p2 );
p2 = android.os.UserHandle .getUid ( v2,v3 );
/* .line 196 */
} // :cond_1
v3 = android.os.UserHandle .getUserId ( p2 );
int v4 = 0; // const/4 v4, 0x0
/* invoke-direct {p0, v3, v4}, Lcom/android/server/AppOpsServiceStubImpl;->getUidState(IZ)Lcom/android/server/AppOpsServiceStubImpl$UserState; */
/* .line 197 */
/* .local v3, "uidState":Lcom/android/server/AppOpsServiceStubImpl$UserState; */
if ( v3 != null) { // if-eqz v3, :cond_2
v4 = this.mCallbackBinder;
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 198 */
v4 = this.mCallbackBinder;
/* .line 199 */
java.lang.Integer .valueOf ( p2 );
java.lang.Integer .valueOf ( p1 );
/* filled-new-array {v5, p3, v6}, [Ljava/lang/Object; */
/* .line 198 */
v0 = (( android.os.MiuiBinderProxy ) v4 ).callNonOneWayTransact ( v1, v5 ); // invoke-virtual {v4, v1, v5}, Landroid/os/MiuiBinderProxy;->callNonOneWayTransact(I[Ljava/lang/Object;)I
/* .line 201 */
} // :cond_2
/* .line 188 */
} // .end local v0 # "result":I
} // .end local v2 # "userId":I
} // .end local v3 # "uidState":Lcom/android/server/AppOpsServiceStubImpl$UserState;
} // :cond_3
} // :goto_0
} // .end method
public Boolean assessVirtualEnable ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 8 */
/* .param p1, "code" # I */
/* .param p2, "uid" # I */
/* .param p3, "pid" # I */
/* .line 429 */
int v0 = 1; // const/4 v0, 0x1
/* const/16 v1, 0x2710 */
/* if-gt p1, v1, :cond_3 */
/* const/16 v2, 0x1d */
/* if-eq p1, v2, :cond_3 */
/* const/16 v2, 0x1e */
/* if-ne p1, v2, :cond_0 */
/* .line 434 */
} // :cond_0
v2 = android.os.UserHandle .getAppId ( p2 );
/* if-ge v2, v1, :cond_1 */
/* .line 435 */
/* .line 437 */
} // :cond_1
/* invoke-direct {p0}, Lcom/android/server/AppOpsServiceStubImpl;->getPackageManagerInternal()Landroid/content/pm/PackageManagerInternal; */
(( android.content.pm.PackageManagerInternal ) v1 ).getPackage ( p2 ); // invoke-virtual {v1, p2}, Landroid/content/pm/PackageManagerInternal;->getPackage(I)Lcom/android/server/pm/pkg/AndroidPackage;
/* .line 438 */
/* .local v1, "pkg":Lcom/android/server/pm/pkg/AndroidPackage; */
if ( v1 != null) { // if-eqz v1, :cond_2
/* invoke-direct {p0}, Lcom/android/server/AppOpsServiceStubImpl;->getPackageManagerInternal()Landroid/content/pm/PackageManagerInternal; */
/* .line 439 */
/* const-wide/16 v4, 0x0 */
/* const/16 v6, 0x3e8 */
v7 = android.os.UserHandle .getUserId ( p2 );
/* .line 438 */
/* invoke-virtual/range {v2 ..v7}, Landroid/content/pm/PackageManagerInternal;->getApplicationInfo(Ljava/lang/String;JII)Landroid/content/pm/ApplicationInfo; */
v2 = com.android.server.am.ProcessUtils .isSystem ( v2 );
if ( v2 != null) { // if-eqz v2, :cond_2
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
/* .line 432 */
} // .end local v1 # "pkg":Lcom/android/server/pm/pkg/AndroidPackage;
} // :cond_3
} // :goto_1
} // .end method
public Integer checkPermission ( android.content.pm.PackageManager p0, java.lang.String p1, java.lang.String p2, Integer p3 ) {
/* .locals 1 */
/* .param p1, "packageManager" # Landroid/content/pm/PackageManager; */
/* .param p2, "permission" # Ljava/lang/String; */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .param p4, "uid" # I */
/* .line 564 */
/* nop */
/* .line 565 */
v0 = android.os.UserHandle .getUserId ( p4 );
/* .line 564 */
v0 = android.permission.PermissionManager .checkPackageNamePermission ( p2,p3,v0 );
} // .end method
public Integer convertVirtualOp ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "code" # I */
/* .line 464 */
v0 = com.android.server.AppOpsServiceStubImpl.supportVirtualGrant;
java.lang.Integer .valueOf ( p1 );
java.lang.Integer .valueOf ( p1 );
/* check-cast v0, Ljava/lang/Integer; */
v0 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
} // .end method
public Integer getNotificationRequestDelay ( android.content.pm.ActivityInfo p0 ) {
/* .locals 3 */
/* .param p1, "activityInfo" # Landroid/content/pm/ActivityInfo; */
/* .line 519 */
int v0 = 0; // const/4 v0, 0x0
/* .line 520 */
/* .local v0, "delay":I */
/* if-nez p1, :cond_0 */
/* .line 521 */
/* .line 523 */
} // :cond_0
v1 = this.mContext;
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
(( android.content.res.Resources ) v1 ).getConfiguration ( ); // invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;
/* iget v1, v1, Landroid/content/res/Configuration;->orientation:I */
int v2 = 1; // const/4 v2, 0x1
/* if-ne v1, v2, :cond_1 */
/* .line 524 */
/* iget v1, p1, Landroid/content/pm/ActivityInfo;->screenOrientation:I */
/* sparse-switch v1, :sswitch_data_0 */
/* .line 529 */
/* :sswitch_0 */
/* const/16 v0, 0x3e8 */
/* .line 530 */
} // :goto_0
/* .line 533 */
} // :cond_1
/* iget v1, p1, Landroid/content/pm/ActivityInfo;->screenOrientation:I */
/* sparse-switch v1, :sswitch_data_1 */
/* .line 538 */
/* :sswitch_1 */
/* const/16 v0, 0x3e8 */
/* .line 542 */
} // :goto_1
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x0 -> :sswitch_0 */
/* 0x6 -> :sswitch_0 */
/* 0x8 -> :sswitch_0 */
/* 0xb -> :sswitch_0 */
} // .end sparse-switch
/* :sswitch_data_1 */
/* .sparse-switch */
/* 0x1 -> :sswitch_1 */
/* 0x7 -> :sswitch_1 */
/* 0x9 -> :sswitch_1 */
/* 0xc -> :sswitch_1 */
} // .end sparse-switch
} // .end method
public void init ( android.content.Context p0 ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 140 */
this.mContext = p1;
/* .line 141 */
/* const-class v0, Lcom/miui/app/AppOpsServiceInternal; */
com.android.server.LocalServices .addService ( v0,p0 );
/* .line 142 */
return;
} // .end method
public void initSwitchedOp ( android.util.SparseArray p0 ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/util/SparseArray<", */
/* "[I>;)V" */
/* } */
} // .end annotation
/* .line 547 */
/* .local p1, "switchedOps":Landroid/util/SparseArray;, "Landroid/util/SparseArray<[I>;" */
/* const/16 v0, 0x2711 */
/* .local v0, "miuiCode":I */
} // :goto_0
/* const/16 v1, 0x2743 */
/* if-ge v0, v1, :cond_0 */
/* .line 548 */
v1 = android.app.AppOpsManager .opToSwitch ( v0 );
/* .line 549 */
/* .local v1, "switchCode":I */
/* nop */
/* .line 550 */
(( android.util.SparseArray ) p1 ).get ( v1 ); // invoke-virtual {p1, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v2, [I */
com.android.internal.util.ArrayUtils .appendInt ( v2,v0 );
/* .line 549 */
(( android.util.SparseArray ) p1 ).put ( v1, v2 ); // invoke-virtual {p1, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 547 */
} // .end local v1 # "switchCode":I
/* add-int/lit8 v0, v0, 0x1 */
/* .line 552 */
} // .end local v0 # "miuiCode":I
} // :cond_0
return;
} // .end method
public Boolean isMiuiOp ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "op" # I */
/* .line 570 */
/* const/16 v0, 0x2710 */
/* if-le p1, v0, :cond_0 */
/* const/16 v0, 0x2743 */
/* if-ge p1, v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean isRevokedCompatByMiui ( java.lang.String p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "permission" # Ljava/lang/String; */
/* .param p2, "mode" # I */
/* .line 486 */
v0 = (( com.android.server.AppOpsServiceStubImpl ) p0 ).supportForegroundByMiui ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/AppOpsServiceStubImpl;->supportForegroundByMiui(Ljava/lang/String;)Z
int v1 = 1; // const/4 v1, 0x1
int v2 = 0; // const/4 v2, 0x0
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 487 */
if ( p2 != null) { // if-eqz p2, :cond_0
int v0 = 4; // const/4 v0, 0x4
/* if-eq p2, v0, :cond_0 */
} // :cond_0
/* move v1, v2 */
} // :goto_0
/* .line 490 */
} // :cond_1
if ( p2 != null) { // if-eqz p2, :cond_2
} // :cond_2
/* move v1, v2 */
} // :goto_1
} // .end method
public Boolean isSupportVirtualGrant ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "code" # I */
/* .line 456 */
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
/* if-nez v0, :cond_0 */
v0 = com.android.server.AppOpsServiceStubImpl.supportVirtualGrant;
v0 = java.lang.Integer .valueOf ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean isTestSuitSpecialIgnore ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 149 */
v0 = v0 = com.android.server.AppOpsServiceStubImpl.sCtsIgnore;
} // .end method
public void onAppApplyOperation ( Integer p0, java.lang.String p1, Integer p2, Integer p3, Integer p4, Integer p5, Integer p6, Boolean p7 ) {
/* .locals 15 */
/* .param p1, "uid" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "op" # I */
/* .param p4, "mode" # I */
/* .param p5, "operationType" # I */
/* .param p6, "processState" # I */
/* .param p7, "capability" # I */
/* .param p8, "appWidgetVisible" # Z */
/* .line 232 */
/* move-object v0, p0 */
/* move/from16 v1, p1 */
/* move-object/from16 v8, p2 */
/* move/from16 v9, p3 */
/* move/from16 v2, p4 */
/* move/from16 v3, p6 */
v4 = /* invoke-static/range {p1 ..p1}, Landroid/os/UserHandle;->getAppId(I)I */
/* const/16 v5, 0x7d0 */
/* if-le v4, v5, :cond_c */
/* iget-boolean v4, v0, Lcom/android/server/AppOpsServiceStubImpl;->mRecordEnable:Z */
/* if-nez v4, :cond_0 */
/* goto/16 :goto_3 */
/* .line 236 */
} // :cond_0
/* const/16 v4, 0x2730 */
/* if-ne v9, v4, :cond_1 */
/* if-nez v2, :cond_1 */
/* .line 237 */
return;
/* .line 239 */
} // :cond_1
/* const/16 v5, 0x33 */
/* if-eq v9, v5, :cond_2 */
/* const/16 v5, 0x41 */
/* if-ne v9, v5, :cond_3 */
} // :cond_2
/* if-nez v2, :cond_3 */
/* .line 241 */
/* invoke-direct {p0}, Lcom/android/server/AppOpsServiceStubImpl;->getAppOpsService()Lcom/android/server/appop/AppOpsService; */
v4 = (( com.android.server.appop.AppOpsService ) v5 ).checkOperation ( v4, v1, v8 ); // invoke-virtual {v5, v4, v1, v8}, Lcom/android/server/appop/AppOpsService;->checkOperation(IILjava/lang/String;)I
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 243 */
return;
/* .line 246 */
} // :cond_3
android.app.PrivacyTestModeStub .get ( );
v5 = this.mContext;
(( android.app.PrivacyTestModeStub ) v4 ).collectPrivacyTestModeInfo ( v9, v1, v5, v8 ); // invoke-virtual {v4, v9, v1, v5, v8}, Landroid/app/PrivacyTestModeStub;->collectPrivacyTestModeInfo(IILandroid/content/Context;Ljava/lang/String;)V
/* .line 247 */
int v10 = 4; // const/4 v10, 0x4
/* if-eq v2, v10, :cond_4 */
/* if-nez v2, :cond_9 */
} // :cond_4
/* const/16 v4, 0xc8 */
/* if-le v3, v4, :cond_9 */
/* .line 248 */
/* if-nez p8, :cond_8 */
/* .line 249 */
/* invoke-direct {p0}, Lcom/android/server/AppOpsServiceStubImpl;->getActivityManagerInternal()Landroid/app/ActivityManagerInternal; */
if ( v4 != null) { // if-eqz v4, :cond_5
/* .line 250 */
/* invoke-direct {p0}, Lcom/android/server/AppOpsServiceStubImpl;->getActivityManagerInternal()Landroid/app/ActivityManagerInternal; */
v4 = (( android.app.ActivityManagerInternal ) v4 ).isPendingTopUid ( v1 ); // invoke-virtual {v4, v1}, Landroid/app/ActivityManagerInternal;->isPendingTopUid(I)Z
if ( v4 != null) { // if-eqz v4, :cond_5
/* .line 253 */
} // :cond_5
v4 = /* invoke-static/range {p3 ..p3}, Landroid/app/AppOpsManager;->resolveFirstUnrestrictedUidState(I)I */
/* if-gt v3, v4, :cond_9 */
/* .line 254 */
/* sparse-switch v9, :sswitch_data_0 */
/* .line 277 */
/* const/16 v3, 0xc8 */
/* .line 278 */
} // .end local p6 # "processState":I
/* .local v3, "processState":I */
int v2 = 0; // const/4 v2, 0x0
} // .end local p4 # "mode":I
/* .local v2, "mode":I */
/* .line 271 */
} // .end local v2 # "mode":I
} // .end local v3 # "processState":I
/* .restart local p4 # "mode":I */
/* .restart local p6 # "processState":I */
/* :sswitch_0 */
/* and-int/lit8 v4, p7, 0x4 */
if ( v4 != null) { // if-eqz v4, :cond_6
/* .line 272 */
/* const/16 v3, 0xc8 */
/* .line 273 */
} // .end local p6 # "processState":I
/* .restart local v3 # "processState":I */
int v2 = 0; // const/4 v2, 0x0
} // .end local p4 # "mode":I
/* .restart local v2 # "mode":I */
/* .line 265 */
} // .end local v2 # "mode":I
} // .end local v3 # "processState":I
/* .restart local p4 # "mode":I */
/* .restart local p6 # "processState":I */
/* :sswitch_1 */
/* and-int/lit8 v4, p7, 0x2 */
if ( v4 != null) { // if-eqz v4, :cond_6
/* .line 266 */
/* const/16 v3, 0xc8 */
/* .line 267 */
} // .end local p6 # "processState":I
/* .restart local v3 # "processState":I */
int v2 = 0; // const/4 v2, 0x0
} // .end local p4 # "mode":I
/* .restart local v2 # "mode":I */
/* .line 259 */
} // .end local v2 # "mode":I
} // .end local v3 # "processState":I
/* .restart local p4 # "mode":I */
/* .restart local p6 # "processState":I */
/* :sswitch_2 */
/* and-int/lit8 v4, p7, 0x1 */
if ( v4 != null) { // if-eqz v4, :cond_6
/* .line 260 */
/* const/16 v3, 0xc8 */
/* .line 261 */
} // .end local p6 # "processState":I
/* .restart local v3 # "processState":I */
int v2 = 0; // const/4 v2, 0x0
/* .line 281 */
} // .end local p4 # "mode":I
/* .restart local v2 # "mode":I */
} // :cond_6
} // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_7
/* .line 282 */
int v2 = 1; // const/4 v2, 0x1
/* move v11, v2 */
/* move v12, v3 */
/* .line 281 */
} // :cond_7
/* move v11, v2 */
/* move v12, v3 */
/* .line 251 */
} // .end local v2 # "mode":I
} // .end local v3 # "processState":I
/* .restart local p4 # "mode":I */
/* .restart local p6 # "processState":I */
} // :cond_8
} // :goto_1
/* const/16 v3, 0xc8 */
/* .line 252 */
} // .end local p6 # "processState":I
/* .restart local v3 # "processState":I */
int v2 = 0; // const/4 v2, 0x0
/* move v11, v2 */
/* move v12, v3 */
} // .end local p4 # "mode":I
/* .restart local v2 # "mode":I */
/* .line 286 */
} // .end local v2 # "mode":I
} // .end local v3 # "processState":I
/* .restart local p4 # "mode":I */
/* .restart local p6 # "processState":I */
} // :cond_9
/* move v11, v2 */
/* move v12, v3 */
} // .end local p4 # "mode":I
} // .end local p6 # "processState":I
/* .local v11, "mode":I */
/* .local v12, "processState":I */
} // :goto_2
v2 = /* invoke-static/range {p1 ..p1}, Landroid/os/UserHandle;->getUserId(I)I */
int v3 = 0; // const/4 v3, 0x0
/* invoke-direct {p0, v2, v3}, Lcom/android/server/AppOpsServiceStubImpl;->getUidState(IZ)Lcom/android/server/AppOpsServiceStubImpl$UserState; */
/* .line 287 */
/* .local v13, "uidState":Lcom/android/server/AppOpsServiceStubImpl$UserState; */
if ( v13 != null) { // if-eqz v13, :cond_a
v2 = this.mCallbackBinder;
if ( v2 != null) { // if-eqz v2, :cond_a
/* .line 288 */
v14 = this.mCallbackBinder;
/* .line 290 */
/* invoke-static/range {p1 ..p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer; */
/* invoke-static/range {p3 ..p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer; */
java.lang.Integer .valueOf ( v11 );
/* invoke-static/range {p5 ..p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer; */
java.lang.Integer .valueOf ( v12 );
/* move-object/from16 v3, p2 */
/* filled-new-array/range {v2 ..v7}, [Ljava/lang/Object; */
/* .line 288 */
(( android.os.MiuiBinderProxy ) v14 ).callOneWayTransact ( v10, v2 ); // invoke-virtual {v14, v10, v2}, Landroid/os/MiuiBinderProxy;->callOneWayTransact(I[Ljava/lang/Object;)I
/* .line 292 */
} // :cond_a
v2 = /* invoke-static/range {p3 ..p3}, Lmiui/security/AppBehavior;->switchOpToBehavior(I)I */
if ( v2 != null) { // if-eqz v2, :cond_b
/* .line 293 */
/* invoke-direct {p0}, Lcom/android/server/AppOpsServiceStubImpl;->getSecurityManager()Lmiui/security/SecurityManagerInternal; */
if ( v2 != null) { // if-eqz v2, :cond_b
/* .line 294 */
/* invoke-direct {p0}, Lcom/android/server/AppOpsServiceStubImpl;->getSecurityManager()Lmiui/security/SecurityManagerInternal; */
v3 = /* invoke-static/range {p3 ..p3}, Lmiui/security/AppBehavior;->switchOpToBehavior(I)I */
/* const-wide/16 v5, 0x1 */
int v7 = 0; // const/4 v7, 0x0
/* move-object/from16 v4, p2 */
/* invoke-virtual/range {v2 ..v7}, Lmiui/security/SecurityManagerInternal;->recordAppBehaviorAsync(ILjava/lang/String;JLjava/lang/String;)V */
/* .line 297 */
} // :cond_b
return;
/* .line 233 */
} // .end local v11 # "mode":I
} // .end local v12 # "processState":I
} // .end local v13 # "uidState":Lcom/android/server/AppOpsServiceStubImpl$UserState;
/* .restart local p4 # "mode":I */
/* .restart local p6 # "processState":I */
} // :cond_c
} // :goto_3
return;
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x0 -> :sswitch_2 */
/* 0x1 -> :sswitch_2 */
/* 0x1a -> :sswitch_1 */
/* 0x1b -> :sswitch_0 */
/* 0x29 -> :sswitch_2 */
/* 0x2a -> :sswitch_2 */
} // .end sparse-switch
} // .end method
public void onAppApplyOperation ( Integer p0, java.lang.String p1, Integer p2, Integer p3, Integer p4, Integer p5, Integer p6, Boolean p7, Integer p8, java.lang.String p9 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "op" # I */
/* .param p4, "mode" # I */
/* .param p5, "operationType" # I */
/* .param p6, "processState" # I */
/* .param p7, "capability" # I */
/* .param p8, "appWidgetVisible" # Z */
/* .param p9, "flags" # I */
/* .param p10, "message" # Ljava/lang/String; */
/* .line 220 */
/* and-int/lit8 v0, p9, 0x20 */
/* if-nez v0, :cond_2 */
v0 = com.android.server.AppOpsServiceStubImpl.MESSAGE_NOT_RECORD;
v0 = /* .line 221 */
/* if-nez v0, :cond_2 */
/* .line 222 */
v0 = android.text.TextUtils .isEmpty ( p10 );
/* if-nez v0, :cond_0 */
final String v0 = "WifiPermissionsUtil#noteAppOpAllowed"; // const-string v0, "WifiPermissionsUtil#noteAppOpAllowed"
v0 = (( java.lang.String ) p10 ).startsWith ( v0 ); // invoke-virtual {p10, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
/* if-nez v0, :cond_2 */
/* .line 223 */
} // :cond_0
final String v0 = "NoAskNoRecord"; // const-string v0, "NoAskNoRecord"
v0 = (( java.lang.String ) v0 ).equals ( p10 ); // invoke-virtual {v0, p10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 226 */
} // :cond_1
/* invoke-virtual/range {p0 ..p8}, Lcom/android/server/AppOpsServiceStubImpl;->onAppApplyOperation(ILjava/lang/String;IIIIIZ)V */
/* .line 227 */
return;
/* .line 224 */
} // :cond_2
} // :goto_0
return;
} // .end method
public Integer registerCallback ( android.os.IBinder p0 ) {
/* .locals 5 */
/* .param p1, "callback" # Landroid/os/IBinder; */
/* .line 337 */
v0 = this.mContext;
/* .line 338 */
v1 = android.os.Binder .getCallingPid ( );
v2 = android.os.Binder .getCallingUid ( );
/* .line 337 */
final String v3 = "android.permission.UPDATE_APP_OPS_STATS"; // const-string v3, "android.permission.UPDATE_APP_OPS_STATS"
int v4 = 0; // const/4 v4, 0x0
(( android.content.Context ) v0 ).enforcePermission ( v3, v1, v2, v4 ); // invoke-virtual {v0, v3, v1, v2, v4}, Landroid/content/Context;->enforcePermission(Ljava/lang/String;IILjava/lang/String;)V
/* .line 340 */
/* if-nez p1, :cond_0 */
/* .line 341 */
int v0 = -1; // const/4 v0, -0x1
/* .line 343 */
} // :cond_0
v0 = android.os.UserHandle .getCallingUserId ( );
/* .line 344 */
/* .local v0, "callingUserId":I */
/* if-nez v0, :cond_1 */
/* .line 345 */
/* const/16 v1, 0x3e7 */
(( com.android.server.AppOpsServiceStubImpl ) p0 ).registerCallback ( p1, v1 ); // invoke-virtual {p0, p1, v1}, Lcom/android/server/AppOpsServiceStubImpl;->registerCallback(Landroid/os/IBinder;I)I
/* .line 347 */
} // :cond_1
v1 = (( com.android.server.AppOpsServiceStubImpl ) p0 ).registerCallback ( p1, v0 ); // invoke-virtual {p0, p1, v0}, Lcom/android/server/AppOpsServiceStubImpl;->registerCallback(Landroid/os/IBinder;I)I
} // .end method
public Integer registerCallback ( android.os.IBinder p0, Integer p1 ) {
/* .locals 5 */
/* .param p1, "callback" # Landroid/os/IBinder; */
/* .param p2, "userId" # I */
/* .line 352 */
v0 = this.mContext;
/* .line 353 */
v1 = android.os.Binder .getCallingPid ( );
v2 = android.os.Binder .getCallingUid ( );
/* .line 352 */
final String v3 = "android.permission.UPDATE_APP_OPS_STATS"; // const-string v3, "android.permission.UPDATE_APP_OPS_STATS"
int v4 = 0; // const/4 v4, 0x0
(( android.content.Context ) v0 ).enforcePermission ( v3, v1, v2, v4 ); // invoke-virtual {v0, v3, v1, v2, v4}, Landroid/content/Context;->enforcePermission(Ljava/lang/String;IILjava/lang/String;)V
/* .line 355 */
int v0 = -1; // const/4 v0, -0x1
/* if-nez p1, :cond_0 */
/* .line 356 */
/* .line 358 */
} // :cond_0
int v1 = 1; // const/4 v1, 0x1
/* invoke-direct {p0, p2, v1}, Lcom/android/server/AppOpsServiceStubImpl;->getUidState(IZ)Lcom/android/server/AppOpsServiceStubImpl$UserState; */
/* .line 360 */
/* .local v1, "uidState":Lcom/android/server/AppOpsServiceStubImpl$UserState; */
/* if-nez v1, :cond_1 */
/* .line 361 */
/* .line 363 */
} // :cond_1
/* new-instance v0, Landroid/os/MiuiBinderProxy; */
final String v2 = "com.android.internal.app.IOpsCallback"; // const-string v2, "com.android.internal.app.IOpsCallback"
/* invoke-direct {v0, p1, v2}, Landroid/os/MiuiBinderProxy;-><init>(Landroid/os/IBinder;Ljava/lang/String;)V */
this.mCallbackBinder = v0;
/* .line 365 */
v0 = this.mCallback;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 366 */
v0 = this.mCallback;
(( com.android.server.AppOpsServiceStubImpl$Callback ) v0 ).unlinkToDeath ( ); // invoke-virtual {v0}, Lcom/android/server/AppOpsServiceStubImpl$Callback;->unlinkToDeath()V
/* .line 368 */
} // :cond_2
/* new-instance v0, Lcom/android/server/AppOpsServiceStubImpl$Callback; */
/* invoke-direct {v0, p0, p1, p2}, Lcom/android/server/AppOpsServiceStubImpl$Callback;-><init>(Lcom/android/server/AppOpsServiceStubImpl;Landroid/os/IBinder;I)V */
this.mCallback = v0;
/* .line 369 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public synchronized void removeUser ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "userHandle" # I */
/* monitor-enter p0 */
/* .line 165 */
try { // :try_start_0
v0 = this.mUidStates;
(( android.util.SparseArray ) v0 ).remove ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 166 */
/* monitor-exit p0 */
return;
/* .line 164 */
} // .end local p0 # "this":Lcom/android/server/AppOpsServiceStubImpl;
} // .end local p1 # "userHandle":I
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
public Boolean skipNotificationRequest ( android.content.pm.ActivityInfo p0 ) {
/* .locals 2 */
/* .param p1, "activityInfo" # Landroid/content/pm/ActivityInfo; */
/* .line 512 */
if ( p1 != null) { // if-eqz p1, :cond_0
v0 = this.name;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.name;
/* .line 514 */
final String v1 = "SplashActivity"; // const-string v1, "SplashActivity"
v0 = (( java.lang.String ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 512 */
} // :goto_0
} // .end method
public Boolean skipSyncAppOpsWithRuntime ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 3 */
/* .param p1, "code" # I */
/* .param p2, "mode" # I */
/* .param p3, "oldMode" # I */
/* .line 496 */
android.content.pm.PackageManagerStub .get ( );
v0 = (( android.content.pm.PackageManagerStub ) v0 ).isOptimizationMode ( ); // invoke-virtual {v0}, Landroid/content/pm/PackageManagerStub;->isOptimizationMode()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 497 */
android.app.AppOpsManager .opToPermission ( p1 );
/* .line 498 */
/* .local v0, "permission":Ljava/lang/String; */
int v1 = 4; // const/4 v1, 0x4
/* if-ne p3, v1, :cond_0 */
/* .line 499 */
v1 = (( com.android.server.AppOpsServiceStubImpl ) p0 ).supportForegroundByMiui ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/AppOpsServiceStubImpl;->supportForegroundByMiui(Ljava/lang/String;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 501 */
int v1 = 1; // const/4 v1, 0x1
/* .line 505 */
} // :cond_0
v1 = com.android.server.AppOpsServiceStubImpl.PLATFORM_PERMISSIONS;
(( android.util.ArrayMap ) v1 ).get ( v0 ); // invoke-virtual {v1, v0}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/CharSequence; */
final String v2 = "android.permission-group.SMS"; // const-string v2, "android.permission-group.SMS"
v1 = android.text.TextUtils .equals ( v1,v2 );
/* .line 507 */
} // .end local v0 # "permission":Ljava/lang/String;
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean supportForegroundByMiui ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "permission" # Ljava/lang/String; */
/* .line 473 */
final String v0 = "android.permission.READ_EXTERNAL_STORAGE"; // const-string v0, "android.permission.READ_EXTERNAL_STORAGE"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
/* .line 474 */
final String v0 = "android.permission.WRITE_EXTERNAL_STORAGE"; // const-string v0, "android.permission.WRITE_EXTERNAL_STORAGE"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
/* .line 475 */
final String v0 = "android.permission.READ_CALL_LOG"; // const-string v0, "android.permission.READ_CALL_LOG"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
/* .line 476 */
final String v0 = "android.permission.WRITE_CALL_LOG"; // const-string v0, "android.permission.WRITE_CALL_LOG"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
/* .line 477 */
final String v0 = "android.permission.READ_CALENDAR"; // const-string v0, "android.permission.READ_CALENDAR"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
/* .line 478 */
final String v0 = "android.permission.WRITE_CALENDAR"; // const-string v0, "android.permission.WRITE_CALENDAR"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
/* .line 479 */
final String v0 = "android.permission.READ_CONTACTS"; // const-string v0, "android.permission.READ_CONTACTS"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
/* .line 480 */
final String v0 = "android.permission.WRITE_CONTACTS"; // const-string v0, "android.permission.WRITE_CONTACTS"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
/* .line 481 */
final String v0 = "android.permission.GET_ACCOUNTS"; // const-string v0, "android.permission.GET_ACCOUNTS"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
/* .line 473 */
} // :goto_1
} // .end method
public void systemReady ( ) {
/* .locals 4 */
/* .line 169 */
final String v0 = "key_app_behavior_record_enable"; // const-string v0, "key_app_behavior_record_enable"
android.provider.Settings$Global .getUriFor ( v0 );
/* .line 170 */
/* .local v0, "enableUri":Landroid/net/Uri; */
/* new-instance v1, Lcom/android/server/AppOpsServiceStubImpl$1; */
com.android.internal.os.BackgroundThread .getHandler ( );
/* invoke-direct {v1, p0, v2}, Lcom/android/server/AppOpsServiceStubImpl$1;-><init>(Lcom/android/server/AppOpsServiceStubImpl;Landroid/os/Handler;)V */
/* .line 176 */
/* .local v1, "observer":Landroid/database/ContentObserver; */
v2 = this.mContext;
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v3 = 0; // const/4 v3, 0x0
(( android.content.ContentResolver ) v2 ).registerContentObserver ( v0, v3, v1 ); // invoke-virtual {v2, v0, v3, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 177 */
/* invoke-direct {p0}, Lcom/android/server/AppOpsServiceStubImpl;->updateRecordState()V */
/* .line 178 */
return;
} // .end method
public void updateProcessState ( Integer p0, Integer p1 ) {
/* .locals 17 */
/* .param p1, "uid" # I */
/* .param p2, "procState" # I */
/* .line 301 */
/* move/from16 v0, p2 */
v1 = /* invoke-static/range {p1 ..p1}, Landroid/os/UserHandle;->getAppId(I)I */
/* const/16 v2, 0x2710 */
/* if-ge v1, v2, :cond_0 */
/* .line 302 */
return;
/* .line 304 */
} // :cond_0
v1 = /* invoke-static/range {p1 ..p1}, Landroid/os/UserHandle;->getUserId(I)I */
/* .line 305 */
/* .local v1, "userId":I */
int v2 = 0; // const/4 v2, 0x0
/* move-object/from16 v9, p0 */
/* invoke-direct {v9, v1, v2}, Lcom/android/server/AppOpsServiceStubImpl;->getUidState(IZ)Lcom/android/server/AppOpsServiceStubImpl$UserState; */
/* .line 306 */
/* .local v2, "uidState":Lcom/android/server/AppOpsServiceStubImpl$UserState; */
if ( v2 != null) { // if-eqz v2, :cond_1
v3 = this.mCallbackBinder;
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 307 */
v3 = this.mCallbackBinder;
/* .line 308 */
/* invoke-static/range {p1 ..p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer; */
/* invoke-static/range {p2 ..p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer; */
/* filled-new-array {v4, v5}, [Ljava/lang/Object; */
/* .line 307 */
int v5 = 5; // const/4 v5, 0x5
(( android.os.MiuiBinderProxy ) v3 ).callOneWayTransact ( v5, v4 ); // invoke-virtual {v3, v5, v4}, Landroid/os/MiuiBinderProxy;->callOneWayTransact(I[Ljava/lang/Object;)I
/* .line 310 */
} // :cond_1
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/AppOpsServiceStubImpl;->getSecurityManager()Lmiui/security/SecurityManagerInternal; */
/* if-nez v3, :cond_2 */
/* .line 311 */
return;
/* .line 313 */
} // :cond_2
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/AppOpsServiceStubImpl;->getPackageManagerInternal()Landroid/content/pm/PackageManagerInternal; */
/* move/from16 v15, p1 */
(( android.content.pm.PackageManagerInternal ) v3 ).getPackage ( v15 ); // invoke-virtual {v3, v15}, Landroid/content/pm/PackageManagerInternal;->getPackage(I)Lcom/android/server/pm/pkg/AndroidPackage;
/* .line 314 */
/* .local v16, "pkg":Lcom/android/server/pm/pkg/AndroidPackage; */
if ( v16 != null) { // if-eqz v16, :cond_5
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/AppOpsServiceStubImpl;->getPackageManagerInternal()Landroid/content/pm/PackageManagerInternal; */
/* .line 315 */
/* invoke-interface/range {v16 ..v16}, Lcom/android/server/pm/pkg/AndroidPackage;->getPackageName()Ljava/lang/String; */
/* const-wide/16 v5, 0x0 */
/* const/16 v7, 0x3e8 */
/* .line 314 */
/* move v8, v1 */
/* invoke-virtual/range {v3 ..v8}, Landroid/content/pm/PackageManagerInternal;->getApplicationInfo(Ljava/lang/String;JII)Landroid/content/pm/ApplicationInfo; */
v3 = com.android.server.am.ProcessUtils .isSystem ( v3 );
/* if-nez v3, :cond_5 */
/* .line 316 */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "fgState":Z */
int v4 = 0; // const/4 v4, 0x0
/* .line 317 */
/* .local v4, "bgState":Z */
/* const/16 v5, 0x14 */
/* if-ge v0, v5, :cond_4 */
/* .line 318 */
int v5 = 7; // const/4 v5, 0x7
/* if-ge v0, v5, :cond_3 */
/* .line 319 */
int v3 = 1; // const/4 v3, 0x1
/* .line 321 */
} // :cond_3
int v4 = 1; // const/4 v4, 0x1
/* .line 324 */
} // :cond_4
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/AppOpsServiceStubImpl;->getSecurityManager()Lmiui/security/SecurityManagerInternal; */
/* invoke-interface/range {v16 ..v16}, Lcom/android/server/pm/pkg/AndroidPackage;->getPackageName()Ljava/lang/String; */
(( miui.security.SecurityManagerInternal ) v5 ).removeCalleeRestrictChain ( v6 ); // invoke-virtual {v5, v6}, Lmiui/security/SecurityManagerInternal;->removeCalleeRestrictChain(Ljava/lang/String;)V
/* .line 326 */
} // :goto_0
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/AppOpsServiceStubImpl;->getSecurityManager()Lmiui/security/SecurityManagerInternal; */
/* const/16 v11, 0x16 */
int v12 = 0; // const/4 v12, 0x0
/* .line 327 */
/* invoke-interface/range {v16 ..v16}, Lcom/android/server/pm/pkg/AndroidPackage;->getPackageName()Ljava/lang/String; */
/* .line 326 */
/* move/from16 v13, p1 */
/* move v15, v3 */
/* invoke-virtual/range {v10 ..v15}, Lmiui/security/SecurityManagerInternal;->recordDurationInfo(ILandroid/os/IBinder;ILjava/lang/String;Z)V */
/* .line 329 */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/AppOpsServiceStubImpl;->getSecurityManager()Lmiui/security/SecurityManagerInternal; */
/* const/16 v11, 0x17 */
/* .line 330 */
/* invoke-interface/range {v16 ..v16}, Lcom/android/server/pm/pkg/AndroidPackage;->getPackageName()Ljava/lang/String; */
/* .line 329 */
/* move v15, v4 */
/* invoke-virtual/range {v10 ..v15}, Lmiui/security/SecurityManagerInternal;->recordDurationInfo(ILandroid/os/IBinder;ILjava/lang/String;Z)V */
/* .line 333 */
} // .end local v3 # "fgState":Z
} // .end local v4 # "bgState":Z
} // :cond_5
return;
} // .end method
