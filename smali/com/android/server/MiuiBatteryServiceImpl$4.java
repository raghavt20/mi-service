class com.android.server.MiuiBatteryServiceImpl$4 extends android.content.BroadcastReceiver {
	 /* .source "MiuiBatteryServiceImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/MiuiBatteryServiceImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.MiuiBatteryServiceImpl this$0; //synthetic
/* # direct methods */
 com.android.server.MiuiBatteryServiceImpl$4 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/MiuiBatteryServiceImpl; */
/* .line 203 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 8 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 206 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 207 */
/* .local v0, "action":Ljava/lang/String; */
v1 = (( java.lang.String ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/String;->hashCode()I
int v2 = -1; // const/4 v2, -0x1
/* sparse-switch v1, :sswitch_data_0 */
} // :cond_0
/* :sswitch_0 */
final String v1 = "android.intent.action.BOOT_COMPLETED"; // const-string v1, "android.intent.action.BOOT_COMPLETED"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
int v1 = 2; // const/4 v1, 0x2
/* :sswitch_1 */
final String v1 = "android.intent.action.BATTERY_CHANGED"; // const-string v1, "android.intent.action.BATTERY_CHANGED"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
	 int v1 = 3; // const/4 v1, 0x3
	 /* :sswitch_2 */
	 final String v1 = "android.hardware.usb.action.USB_DEVICE_DETACHED"; // const-string v1, "android.hardware.usb.action.USB_DEVICE_DETACHED"
	 v1 = 	 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 int v1 = 1; // const/4 v1, 0x1
		 /* :sswitch_3 */
		 final String v1 = "android.hardware.usb.action.USB_DEVICE_ATTACHED"; // const-string v1, "android.hardware.usb.action.USB_DEVICE_ATTACHED"
		 v1 = 		 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v1 != null) { // if-eqz v1, :cond_0
			 int v1 = 0; // const/4 v1, 0x0
		 } // :goto_0
		 /* move v1, v2 */
	 } // :goto_1
	 /* const/16 v3, 0x5083 */
	 /* const/16 v4, 0x2717 */
	 final String v5 = "device"; // const-string v5, "device"
	 /* const-wide/16 v6, 0x0 */
	 /* packed-switch v1, :pswitch_data_0 */
	 /* goto/16 :goto_2 */
	 /* .line 228 */
	 /* :pswitch_0 */
	 final String v1 = "level"; // const-string v1, "level"
	 v1 = 	 (( android.content.Intent ) p2 ).getIntExtra ( v1, v2 ); // invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
	 /* .line 229 */
	 /* .local v1, "phoneBatteryLevel":I */
	 v2 = this.this$0;
	 com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmHandler ( v2 );
	 v2 = 	 (( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) v2 ).isHandleConnect ( ); // invoke-virtual {v2}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->isHandleConnect()Z
	 if ( v2 != null) { // if-eqz v2, :cond_1
		 v2 = this.this$0;
		 v2 = 		 com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmLastPhoneBatteryLevel ( v2 );
		 /* if-eq v2, v1, :cond_1 */
		 /* .line 230 */
		 v2 = this.this$0;
		 com.android.server.MiuiBatteryServiceImpl .-$$Nest$fputmLastPhoneBatteryLevel ( v2,v1 );
		 /* .line 231 */
		 v2 = this.this$0;
		 com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmMiCharge ( v2 );
		 v3 = this.this$0;
		 v3 = 		 com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmLastPhoneBatteryLevel ( v3 );
		 java.lang.Integer .toString ( v3 );
		 final String v4 = "PhoneBatteryChanged"; // const-string v4, "PhoneBatteryChanged"
		 (( miui.util.IMiCharge ) v2 ).setTypeCCommonInfo ( v4, v3 ); // invoke-virtual {v2, v4, v3}, Lmiui/util/IMiCharge;->setTypeCCommonInfo(Ljava/lang/String;Ljava/lang/String;)Z
		 /* .line 225 */
	 } // .end local v1 # "phoneBatteryLevel":I
	 /* :pswitch_1 */
	 v1 = this.this$0;
	 com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmHandler ( v1 );
	 /* const/16 v2, 0x19 */
	 (( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) v1 ).sendMessageDelayed ( v2, v6, v7 ); // invoke-virtual {v1, v2, v6, v7}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessageDelayed(IJ)V
	 /* .line 226 */
	 /* .line 217 */
	 /* :pswitch_2 */
	 (( android.content.Intent ) p2 ).getParcelableExtra ( v5 ); // invoke-virtual {p2, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;
	 /* check-cast v1, Landroid/hardware/usb/UsbDevice; */
	 /* .line 218 */
	 /* .local v1, "detachedDevice":Landroid/hardware/usb/UsbDevice; */
	 if ( v1 != null) { // if-eqz v1, :cond_1
		 /* .line 219 */
		 v2 = 		 (( android.hardware.usb.UsbDevice ) v1 ).getVendorId ( ); // invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getVendorId()I
		 /* if-ne v4, v2, :cond_1 */
		 /* .line 220 */
		 v2 = 		 (( android.hardware.usb.UsbDevice ) v1 ).getProductId ( ); // invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getProductId()I
		 /* if-ne v3, v2, :cond_1 */
		 /* .line 221 */
		 v2 = this.this$0;
		 com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmHandler ( v2 );
		 /* const/16 v3, 0x18 */
		 (( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) v2 ).sendMessageDelayed ( v3, v1, v6, v7 ); // invoke-virtual {v2, v3, v1, v6, v7}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessageDelayed(ILjava/lang/Object;J)V
		 /* .line 209 */
	 } // .end local v1 # "detachedDevice":Landroid/hardware/usb/UsbDevice;
	 /* :pswitch_3 */
	 (( android.content.Intent ) p2 ).getParcelableExtra ( v5 ); // invoke-virtual {p2, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;
	 /* check-cast v1, Landroid/hardware/usb/UsbDevice; */
	 /* .line 210 */
	 /* .local v1, "attachedDevice":Landroid/hardware/usb/UsbDevice; */
	 if ( v1 != null) { // if-eqz v1, :cond_1
		 /* .line 211 */
		 v2 = 		 (( android.hardware.usb.UsbDevice ) v1 ).getVendorId ( ); // invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getVendorId()I
		 /* if-ne v4, v2, :cond_1 */
		 /* .line 212 */
		 v2 = 		 (( android.hardware.usb.UsbDevice ) v1 ).getProductId ( ); // invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getProductId()I
		 /* if-ne v3, v2, :cond_1 */
		 /* .line 213 */
		 v2 = this.this$0;
		 com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmHandler ( v2 );
		 /* const/16 v3, 0x17 */
		 (( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) v2 ).sendMessageDelayed ( v3, v1, v6, v7 ); // invoke-virtual {v2, v3, v1, v6, v7}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessageDelayed(ILjava/lang/Object;J)V
		 /* .line 235 */
	 } // .end local v1 # "attachedDevice":Landroid/hardware/usb/UsbDevice;
} // :cond_1
} // :goto_2
return;
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x7e02a835 -> :sswitch_3 */
/* -0x5fdc9a67 -> :sswitch_2 */
/* -0x5bb23923 -> :sswitch_1 */
/* 0x2f94f923 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
