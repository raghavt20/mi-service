public class com.android.server.MiuiBatteryStatsService {
	 /* .source "MiuiBatteryStatsService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;, */
	 /* Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;, */
	 /* Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;, */
	 /* Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;, */
	 /* Lcom/android/server/MiuiBatteryStatsService$TrackBatteryUsbInfo;, */
	 /* Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo; */
	 /* } */
} // .end annotation
/* # static fields */
public static final java.lang.String ADJUST_VOLTAGE;
public static final java.lang.String ADJUST_VOLTAGE_TL_EXTRA;
public static final java.lang.String ADJUST_VOLTAGE_TS_EXTRA;
public static final java.lang.String CHECK_SOC;
public static final java.lang.String CYCLE_CHECK;
private static final Long DAY;
private static final Long FIVEMIN;
private static final Long HALFMIN;
private static volatile com.android.server.MiuiBatteryStatsService INSTANCE;
public static final java.lang.String LIMIT_TIME;
private static final Long ONEHOUR;
private static final Long TENMIN;
private static final Long TWOHOUR;
public static final java.lang.String UPDATE_BATTERY_DATA;
private static Boolean mIsSatisfyTempLevelCondition;
private static Boolean mIsSatisfyTempSocCondition;
/* # instance fields */
private final Boolean DEBUG;
private final java.lang.String TAG;
private android.app.AlarmManager mAlarmManager;
private com.android.server.MiuiAppUsageStats mAppUsageStats;
private com.android.server.MiuiBatteryStatsService$BatteryTempVoltageTimeInfo mBatteryInfoFull;
private com.android.server.MiuiBatteryStatsService$BatteryTempVoltageTimeInfo mBatteryInfoNormal;
private com.android.server.MiuiBatteryStatsService$BatteryTempLevelInfo mBatteryTempLevel;
private com.android.server.MiuiBatteryStatsService$BatteryTempSocTimeInfo mBatteryTempSocTime;
private Boolean mBootCompleted;
private Integer mChargeEndCapacity;
private Long mChargeEndTime;
private Integer mChargeMaxTemp;
private Integer mChargeMinTemp;
private Integer mChargeStartCapacity;
private Long mChargeStartTime;
private final android.content.Context mContext;
private Integer mDischargingCount;
private Long mFullChargeEndTime;
private Long mFullChargeStartTime;
private final com.android.server.MiuiBatteryStatsService$BatteryStatsHandler mHandler;
private Boolean mIsHandleIntermittentCharge;
private Boolean mIsOrderedCheckSocTimer;
private Boolean mIsScreenOn;
private Boolean mIsTablet;
private Integer mLastBatteryStatus;
private Integer mLastBatteryTemp;
private Integer mLastBatteryVoltage;
private Integer mLastLpdState;
private Boolean mLastPlugged;
private Integer mLastSoc;
private Integer mLpdCount;
private miui.util.IMiCharge mMiCharge;
private Boolean mOtgConnected;
private android.app.PendingIntent mPendingIntent;
private android.app.PendingIntent mPendingIntentCheckSoc;
private android.app.PendingIntent mPendingIntentCycleCheck;
private android.app.PendingIntent mPendingIntentLimitTime;
private Integer mPlugType;
private Long mScreenOnChargingStart;
private Long mScreenOnTime;
private Boolean mStartRecordDischarging;
private Boolean mSupportedCellVolt;
private Boolean mSupportedSB;
/* # direct methods */
static Boolean -$$Nest$fgetDEBUG ( com.android.server.MiuiBatteryStatsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/android/server/MiuiBatteryStatsService;->DEBUG:Z */
} // .end method
static android.app.AlarmManager -$$Nest$fgetmAlarmManager ( com.android.server.MiuiBatteryStatsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mAlarmManager;
} // .end method
static com.android.server.MiuiAppUsageStats -$$Nest$fgetmAppUsageStats ( com.android.server.MiuiBatteryStatsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mAppUsageStats;
} // .end method
static com.android.server.MiuiBatteryStatsService$BatteryTempVoltageTimeInfo -$$Nest$fgetmBatteryInfoFull ( com.android.server.MiuiBatteryStatsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mBatteryInfoFull;
} // .end method
static com.android.server.MiuiBatteryStatsService$BatteryTempVoltageTimeInfo -$$Nest$fgetmBatteryInfoNormal ( com.android.server.MiuiBatteryStatsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mBatteryInfoNormal;
} // .end method
static com.android.server.MiuiBatteryStatsService$BatteryTempLevelInfo -$$Nest$fgetmBatteryTempLevel ( com.android.server.MiuiBatteryStatsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mBatteryTempLevel;
} // .end method
static com.android.server.MiuiBatteryStatsService$BatteryTempSocTimeInfo -$$Nest$fgetmBatteryTempSocTime ( com.android.server.MiuiBatteryStatsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mBatteryTempSocTime;
} // .end method
static Boolean -$$Nest$fgetmBootCompleted ( com.android.server.MiuiBatteryStatsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mBootCompleted:Z */
} // .end method
static Integer -$$Nest$fgetmChargeEndCapacity ( com.android.server.MiuiBatteryStatsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mChargeEndCapacity:I */
} // .end method
static Long -$$Nest$fgetmChargeEndTime ( com.android.server.MiuiBatteryStatsService p0 ) { //bridge//synthethic
	 /* .locals 2 */
	 /* iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService;->mChargeEndTime:J */
	 /* return-wide v0 */
} // .end method
static Integer -$$Nest$fgetmChargeMaxTemp ( com.android.server.MiuiBatteryStatsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mChargeMaxTemp:I */
} // .end method
static Integer -$$Nest$fgetmChargeMinTemp ( com.android.server.MiuiBatteryStatsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mChargeMinTemp:I */
} // .end method
static Integer -$$Nest$fgetmChargeStartCapacity ( com.android.server.MiuiBatteryStatsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mChargeStartCapacity:I */
} // .end method
static Long -$$Nest$fgetmChargeStartTime ( com.android.server.MiuiBatteryStatsService p0 ) { //bridge//synthethic
	 /* .locals 2 */
	 /* iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService;->mChargeStartTime:J */
	 /* return-wide v0 */
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.android.server.MiuiBatteryStatsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mContext;
} // .end method
static Integer -$$Nest$fgetmDischargingCount ( com.android.server.MiuiBatteryStatsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mDischargingCount:I */
} // .end method
static Long -$$Nest$fgetmFullChargeEndTime ( com.android.server.MiuiBatteryStatsService p0 ) { //bridge//synthethic
	 /* .locals 2 */
	 /* iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService;->mFullChargeEndTime:J */
	 /* return-wide v0 */
} // .end method
static Long -$$Nest$fgetmFullChargeStartTime ( com.android.server.MiuiBatteryStatsService p0 ) { //bridge//synthethic
	 /* .locals 2 */
	 /* iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService;->mFullChargeStartTime:J */
	 /* return-wide v0 */
} // .end method
static com.android.server.MiuiBatteryStatsService$BatteryStatsHandler -$$Nest$fgetmHandler ( com.android.server.MiuiBatteryStatsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mHandler;
} // .end method
static Boolean -$$Nest$fgetmIsHandleIntermittentCharge ( com.android.server.MiuiBatteryStatsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mIsHandleIntermittentCharge:Z */
} // .end method
static Boolean -$$Nest$fgetmIsOrderedCheckSocTimer ( com.android.server.MiuiBatteryStatsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mIsOrderedCheckSocTimer:Z */
} // .end method
static Boolean -$$Nest$fgetmIsScreenOn ( com.android.server.MiuiBatteryStatsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mIsScreenOn:Z */
} // .end method
static Boolean -$$Nest$fgetmIsTablet ( com.android.server.MiuiBatteryStatsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mIsTablet:Z */
} // .end method
static Integer -$$Nest$fgetmLastBatteryStatus ( com.android.server.MiuiBatteryStatsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mLastBatteryStatus:I */
} // .end method
static Integer -$$Nest$fgetmLastBatteryTemp ( com.android.server.MiuiBatteryStatsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mLastBatteryTemp:I */
} // .end method
static Integer -$$Nest$fgetmLastBatteryVoltage ( com.android.server.MiuiBatteryStatsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mLastBatteryVoltage:I */
} // .end method
static Integer -$$Nest$fgetmLastLpdState ( com.android.server.MiuiBatteryStatsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mLastLpdState:I */
} // .end method
static Boolean -$$Nest$fgetmLastPlugged ( com.android.server.MiuiBatteryStatsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mLastPlugged:Z */
} // .end method
static Integer -$$Nest$fgetmLastSoc ( com.android.server.MiuiBatteryStatsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mLastSoc:I */
} // .end method
static Integer -$$Nest$fgetmLpdCount ( com.android.server.MiuiBatteryStatsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mLpdCount:I */
} // .end method
static miui.util.IMiCharge -$$Nest$fgetmMiCharge ( com.android.server.MiuiBatteryStatsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mMiCharge;
} // .end method
static Boolean -$$Nest$fgetmOtgConnected ( com.android.server.MiuiBatteryStatsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mOtgConnected:Z */
} // .end method
static android.app.PendingIntent -$$Nest$fgetmPendingIntent ( com.android.server.MiuiBatteryStatsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mPendingIntent;
} // .end method
static android.app.PendingIntent -$$Nest$fgetmPendingIntentCheckSoc ( com.android.server.MiuiBatteryStatsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mPendingIntentCheckSoc;
} // .end method
static android.app.PendingIntent -$$Nest$fgetmPendingIntentCycleCheck ( com.android.server.MiuiBatteryStatsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mPendingIntentCycleCheck;
} // .end method
static android.app.PendingIntent -$$Nest$fgetmPendingIntentLimitTime ( com.android.server.MiuiBatteryStatsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mPendingIntentLimitTime;
} // .end method
static Integer -$$Nest$fgetmPlugType ( com.android.server.MiuiBatteryStatsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mPlugType:I */
} // .end method
static Long -$$Nest$fgetmScreenOnChargingStart ( com.android.server.MiuiBatteryStatsService p0 ) { //bridge//synthethic
	 /* .locals 2 */
	 /* iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService;->mScreenOnChargingStart:J */
	 /* return-wide v0 */
} // .end method
static Long -$$Nest$fgetmScreenOnTime ( com.android.server.MiuiBatteryStatsService p0 ) { //bridge//synthethic
	 /* .locals 2 */
	 /* iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService;->mScreenOnTime:J */
	 /* return-wide v0 */
} // .end method
static Boolean -$$Nest$fgetmStartRecordDischarging ( com.android.server.MiuiBatteryStatsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mStartRecordDischarging:Z */
} // .end method
static Boolean -$$Nest$fgetmSupportedCellVolt ( com.android.server.MiuiBatteryStatsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mSupportedCellVolt:Z */
} // .end method
static Boolean -$$Nest$fgetmSupportedSB ( com.android.server.MiuiBatteryStatsService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/android/server/MiuiBatteryStatsService;->mSupportedSB:Z */
} // .end method
static void -$$Nest$fputmBootCompleted ( com.android.server.MiuiBatteryStatsService p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mBootCompleted:Z */
	 return;
} // .end method
static void -$$Nest$fputmChargeEndCapacity ( com.android.server.MiuiBatteryStatsService p0, Integer p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mChargeEndCapacity:I */
	 return;
} // .end method
static void -$$Nest$fputmChargeEndTime ( com.android.server.MiuiBatteryStatsService p0, Long p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-wide p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mChargeEndTime:J */
	 return;
} // .end method
static void -$$Nest$fputmChargeMaxTemp ( com.android.server.MiuiBatteryStatsService p0, Integer p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mChargeMaxTemp:I */
	 return;
} // .end method
static void -$$Nest$fputmChargeMinTemp ( com.android.server.MiuiBatteryStatsService p0, Integer p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mChargeMinTemp:I */
	 return;
} // .end method
static void -$$Nest$fputmChargeStartCapacity ( com.android.server.MiuiBatteryStatsService p0, Integer p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mChargeStartCapacity:I */
	 return;
} // .end method
static void -$$Nest$fputmChargeStartTime ( com.android.server.MiuiBatteryStatsService p0, Long p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-wide p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mChargeStartTime:J */
	 return;
} // .end method
static void -$$Nest$fputmDischargingCount ( com.android.server.MiuiBatteryStatsService p0, Integer p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mDischargingCount:I */
	 return;
} // .end method
static void -$$Nest$fputmFullChargeEndTime ( com.android.server.MiuiBatteryStatsService p0, Long p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-wide p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mFullChargeEndTime:J */
	 return;
} // .end method
static void -$$Nest$fputmFullChargeStartTime ( com.android.server.MiuiBatteryStatsService p0, Long p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-wide p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mFullChargeStartTime:J */
	 return;
} // .end method
static void -$$Nest$fputmIsHandleIntermittentCharge ( com.android.server.MiuiBatteryStatsService p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mIsHandleIntermittentCharge:Z */
	 return;
} // .end method
static void -$$Nest$fputmIsOrderedCheckSocTimer ( com.android.server.MiuiBatteryStatsService p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mIsOrderedCheckSocTimer:Z */
	 return;
} // .end method
static void -$$Nest$fputmIsScreenOn ( com.android.server.MiuiBatteryStatsService p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mIsScreenOn:Z */
	 return;
} // .end method
static void -$$Nest$fputmLastBatteryStatus ( com.android.server.MiuiBatteryStatsService p0, Integer p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mLastBatteryStatus:I */
	 return;
} // .end method
static void -$$Nest$fputmLastBatteryTemp ( com.android.server.MiuiBatteryStatsService p0, Integer p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mLastBatteryTemp:I */
	 return;
} // .end method
static void -$$Nest$fputmLastBatteryVoltage ( com.android.server.MiuiBatteryStatsService p0, Integer p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mLastBatteryVoltage:I */
	 return;
} // .end method
static void -$$Nest$fputmLastLpdState ( com.android.server.MiuiBatteryStatsService p0, Integer p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mLastLpdState:I */
	 return;
} // .end method
static void -$$Nest$fputmLastPlugged ( com.android.server.MiuiBatteryStatsService p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mLastPlugged:Z */
	 return;
} // .end method
static void -$$Nest$fputmLastSoc ( com.android.server.MiuiBatteryStatsService p0, Integer p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mLastSoc:I */
	 return;
} // .end method
static void -$$Nest$fputmLpdCount ( com.android.server.MiuiBatteryStatsService p0, Integer p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mLpdCount:I */
	 return;
} // .end method
static void -$$Nest$fputmOtgConnected ( com.android.server.MiuiBatteryStatsService p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mOtgConnected:Z */
	 return;
} // .end method
static void -$$Nest$fputmPlugType ( com.android.server.MiuiBatteryStatsService p0, Integer p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mPlugType:I */
	 return;
} // .end method
static void -$$Nest$fputmScreenOnChargingStart ( com.android.server.MiuiBatteryStatsService p0, Long p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-wide p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mScreenOnChargingStart:J */
	 return;
} // .end method
static void -$$Nest$fputmScreenOnTime ( com.android.server.MiuiBatteryStatsService p0, Long p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-wide p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mScreenOnTime:J */
	 return;
} // .end method
static void -$$Nest$fputmStartRecordDischarging ( com.android.server.MiuiBatteryStatsService p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mStartRecordDischarging:Z */
	 return;
} // .end method
static void -$$Nest$fputmSupportedCellVolt ( com.android.server.MiuiBatteryStatsService p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mSupportedCellVolt:Z */
	 return;
} // .end method
static void -$$Nest$fputmSupportedSB ( com.android.server.MiuiBatteryStatsService p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/MiuiBatteryStatsService;->mSupportedSB:Z */
	 return;
} // .end method
static Long -$$Nest$sfgetDAY ( ) { //bridge//synthethic
	 /* .locals 2 */
	 /* sget-wide v0, Lcom/android/server/MiuiBatteryStatsService;->DAY:J */
	 /* return-wide v0 */
} // .end method
static Boolean -$$Nest$sfgetmIsSatisfyTempLevelCondition ( ) { //bridge//synthethic
	 /* .locals 1 */
	 /* sget-boolean v0, Lcom/android/server/MiuiBatteryStatsService;->mIsSatisfyTempLevelCondition:Z */
} // .end method
static Boolean -$$Nest$sfgetmIsSatisfyTempSocCondition ( ) { //bridge//synthethic
	 /* .locals 1 */
	 /* sget-boolean v0, Lcom/android/server/MiuiBatteryStatsService;->mIsSatisfyTempSocCondition:Z */
} // .end method
static void -$$Nest$sfputmIsSatisfyTempLevelCondition ( Boolean p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 com.android.server.MiuiBatteryStatsService.mIsSatisfyTempLevelCondition = (p0!= 0);
	 return;
} // .end method
static void -$$Nest$sfputmIsSatisfyTempSocCondition ( Boolean p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 com.android.server.MiuiBatteryStatsService.mIsSatisfyTempSocCondition = (p0!= 0);
	 return;
} // .end method
static com.android.server.MiuiBatteryStatsService ( ) {
	 /* .locals 4 */
	 /* .line 63 */
	 final String v0 = "persist.sys.report_time"; // const-string v0, "persist.sys.report_time"
	 /* const v1, 0x15180 */
	 v0 = 	 android.os.SystemProperties .getInt ( v0,v1 );
	 /* int-to-long v0, v0 */
	 /* const-wide/16 v2, 0x3e8 */
	 /* mul-long/2addr v0, v2 */
	 /* sput-wide v0, Lcom/android/server/MiuiBatteryStatsService;->DAY:J */
	 /* .line 70 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* .line 71 */
	 int v0 = 0; // const/4 v0, 0x0
	 com.android.server.MiuiBatteryStatsService.mIsSatisfyTempLevelCondition = (v0!= 0);
	 /* .line 72 */
	 com.android.server.MiuiBatteryStatsService.mIsSatisfyTempSocCondition = (v0!= 0);
	 return;
} // .end method
public com.android.server.MiuiBatteryStatsService ( ) {
	 /* .locals 13 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .line 127 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 50 */
	 final String v0 = "MiuiBatteryStatsService"; // const-string v0, "MiuiBatteryStatsService"
	 this.TAG = v0;
	 /* .line 51 */
	 final String v1 = "persist.sys.debug_stats"; // const-string v1, "persist.sys.debug_stats"
	 int v2 = 0; // const/4 v2, 0x0
	 v1 = 	 android.os.SystemProperties .getBoolean ( v1,v2 );
	 /* iput-boolean v1, p0, Lcom/android/server/MiuiBatteryStatsService;->DEBUG:Z */
	 /* .line 84 */
	 /* const-wide/16 v3, 0x0 */
	 /* iput-wide v3, p0, Lcom/android/server/MiuiBatteryStatsService;->mChargeStartTime:J */
	 /* .line 85 */
	 /* iput-wide v3, p0, Lcom/android/server/MiuiBatteryStatsService;->mChargeEndTime:J */
	 /* .line 86 */
	 /* iput-wide v3, p0, Lcom/android/server/MiuiBatteryStatsService;->mFullChargeStartTime:J */
	 /* .line 87 */
	 /* iput-wide v3, p0, Lcom/android/server/MiuiBatteryStatsService;->mFullChargeEndTime:J */
	 /* .line 88 */
	 /* iput-wide v3, p0, Lcom/android/server/MiuiBatteryStatsService;->mScreenOnChargingStart:J */
	 /* .line 89 */
	 /* iput-wide v3, p0, Lcom/android/server/MiuiBatteryStatsService;->mScreenOnTime:J */
	 /* .line 90 */
	 /* iput v2, p0, Lcom/android/server/MiuiBatteryStatsService;->mChargeStartCapacity:I */
	 /* .line 91 */
	 /* iput v2, p0, Lcom/android/server/MiuiBatteryStatsService;->mChargeEndCapacity:I */
	 /* .line 92 */
	 int v3 = -1; // const/4 v3, -0x1
	 /* iput v3, p0, Lcom/android/server/MiuiBatteryStatsService;->mLastBatteryStatus:I */
	 /* .line 93 */
	 /* iput v3, p0, Lcom/android/server/MiuiBatteryStatsService;->mLastSoc:I */
	 /* .line 94 */
	 /* iput v2, p0, Lcom/android/server/MiuiBatteryStatsService;->mChargeMaxTemp:I */
	 /* .line 95 */
	 /* iput v2, p0, Lcom/android/server/MiuiBatteryStatsService;->mChargeMinTemp:I */
	 /* .line 96 */
	 /* iput v2, p0, Lcom/android/server/MiuiBatteryStatsService;->mDischargingCount:I */
	 /* .line 97 */
	 /* iput-boolean v2, p0, Lcom/android/server/MiuiBatteryStatsService;->mIsScreenOn:Z */
	 /* .line 98 */
	 /* iput-boolean v2, p0, Lcom/android/server/MiuiBatteryStatsService;->mStartRecordDischarging:Z */
	 /* .line 99 */
	 /* iput-boolean v2, p0, Lcom/android/server/MiuiBatteryStatsService;->mIsHandleIntermittentCharge:Z */
	 /* .line 100 */
	 /* iput v3, p0, Lcom/android/server/MiuiBatteryStatsService;->mPlugType:I */
	 /* .line 101 */
	 /* iput-boolean v2, p0, Lcom/android/server/MiuiBatteryStatsService;->mOtgConnected:Z */
	 /* .line 102 */
	 /* iput-boolean v2, p0, Lcom/android/server/MiuiBatteryStatsService;->mIsOrderedCheckSocTimer:Z */
	 /* .line 104 */
	 miui.util.IMiCharge .getInstance ( );
	 this.mMiCharge = v3;
	 /* .line 105 */
	 /* new-instance v3, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo; */
	 final String v4 = "Full"; // const-string v4, "Full"
	 /* invoke-direct {v3, p0, v4}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;-><init>(Lcom/android/server/MiuiBatteryStatsService;Ljava/lang/String;)V */
	 this.mBatteryInfoFull = v3;
	 /* .line 106 */
	 /* new-instance v3, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo; */
	 final String v4 = "Normal"; // const-string v4, "Normal"
	 /* invoke-direct {v3, p0, v4}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;-><init>(Lcom/android/server/MiuiBatteryStatsService;Ljava/lang/String;)V */
	 this.mBatteryInfoNormal = v3;
	 /* .line 107 */
	 /* new-instance v3, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo; */
	 /* invoke-direct {v3, p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;-><init>(Lcom/android/server/MiuiBatteryStatsService;)V */
	 this.mBatteryTempSocTime = v3;
	 /* .line 108 */
	 /* new-instance v3, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo; */
	 /* invoke-direct {v3, p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;-><init>(Lcom/android/server/MiuiBatteryStatsService;)V */
	 this.mBatteryTempLevel = v3;
	 /* .line 128 */
	 this.mContext = p1;
	 /* .line 129 */
	 /* new-instance v3, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler; */
	 com.android.server.MiuiBgThread .get ( );
	 (( com.android.server.MiuiBgThread ) v4 ).getLooper ( ); // invoke-virtual {v4}, Lcom/android/server/MiuiBgThread;->getLooper()Landroid/os/Looper;
	 /* invoke-direct {v3, p0, v4}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;-><init>(Lcom/android/server/MiuiBatteryStatsService;Landroid/os/Looper;)V */
	 this.mHandler = v3;
	 /* .line 130 */
	 /* new-instance v3, Lcom/android/server/MiuiAppUsageStats; */
	 /* invoke-direct {v3}, Lcom/android/server/MiuiAppUsageStats;-><init>()V */
	 this.mAppUsageStats = v3;
	 /* .line 131 */
	 final String v3 = "ro.build.characteristics"; // const-string v3, "ro.build.characteristics"
	 final String v4 = ""; // const-string v4, ""
	 android.os.SystemProperties .get ( v3,v4 );
	 /* filled-new-array {v3}, [Ljava/lang/String; */
	 java.util.Arrays .asList ( v3 );
	 v3 = 	 /* const-string/jumbo v4, "tablet" */
	 /* iput-boolean v3, p0, Lcom/android/server/MiuiBatteryStatsService;->mIsTablet:Z */
	 /* .line 133 */
	 final String v3 = "alarm"; // const-string v3, "alarm"
	 (( android.content.Context ) p1 ).getSystemService ( v3 ); // invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
	 /* check-cast v3, Landroid/app/AlarmManager; */
	 this.mAlarmManager = v3;
	 /* .line 135 */
	 /* new-instance v3, Lcom/android/server/MiuiBatteryStatsService$1; */
	 /* invoke-direct {v3, p0}, Lcom/android/server/MiuiBatteryStatsService$1;-><init>(Lcom/android/server/MiuiBatteryStatsService;)V */
	 /* .line 317 */
	 /* .local v3, "stateChangedReceiver":Landroid/content/BroadcastReceiver; */
	 /* new-instance v4, Landroid/content/IntentFilter; */
	 final String v5 = "android.hardware.usb.action.USB_STATE"; // const-string v5, "android.hardware.usb.action.USB_STATE"
	 /* invoke-direct {v4, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V */
	 /* .line 318 */
	 /* .local v4, "filter":Landroid/content/IntentFilter; */
	 final String v5 = "android.intent.action.BATTERY_CHANGED"; // const-string v5, "android.intent.action.BATTERY_CHANGED"
	 (( android.content.IntentFilter ) v4 ).addAction ( v5 ); // invoke-virtual {v4, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
	 /* .line 319 */
	 final String v5 = "miui.intent.action.UPDATE_BATTERY_DATA"; // const-string v5, "miui.intent.action.UPDATE_BATTERY_DATA"
	 (( android.content.IntentFilter ) v4 ).addAction ( v5 ); // invoke-virtual {v4, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
	 /* .line 320 */
	 final String v6 = "miui.intent.action.ACTION_SHUTDOWN_DELAY"; // const-string v6, "miui.intent.action.ACTION_SHUTDOWN_DELAY"
	 (( android.content.IntentFilter ) v4 ).addAction ( v6 ); // invoke-virtual {v4, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
	 /* .line 321 */
	 final String v6 = "android.intent.action.ACTION_SHUTDOWN"; // const-string v6, "android.intent.action.ACTION_SHUTDOWN"
	 (( android.content.IntentFilter ) v4 ).addAction ( v6 ); // invoke-virtual {v4, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
	 /* .line 322 */
	 final String v6 = "android.intent.action.BOOT_COMPLETED"; // const-string v6, "android.intent.action.BOOT_COMPLETED"
	 (( android.content.IntentFilter ) v4 ).addAction ( v6 ); // invoke-virtual {v4, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
	 /* .line 323 */
	 final String v6 = "android.intent.action.SCREEN_OFF"; // const-string v6, "android.intent.action.SCREEN_OFF"
	 (( android.content.IntentFilter ) v4 ).addAction ( v6 ); // invoke-virtual {v4, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
	 /* .line 324 */
	 final String v6 = "android.intent.action.SCREEN_ON"; // const-string v6, "android.intent.action.SCREEN_ON"
	 (( android.content.IntentFilter ) v4 ).addAction ( v6 ); // invoke-virtual {v4, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
	 /* .line 325 */
	 final String v6 = "miui.intent.action.LIMIT_TIME"; // const-string v6, "miui.intent.action.LIMIT_TIME"
	 (( android.content.IntentFilter ) v4 ).addAction ( v6 ); // invoke-virtual {v4, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
	 /* .line 326 */
	 final String v7 = "miui.intent.action.CYCLE_CHECK"; // const-string v7, "miui.intent.action.CYCLE_CHECK"
	 (( android.content.IntentFilter ) v4 ).addAction ( v7 ); // invoke-virtual {v4, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
	 /* .line 327 */
	 final String v8 = "android.hardware.usb.action.USB_PORT_CHANGED"; // const-string v8, "android.hardware.usb.action.USB_PORT_CHANGED"
	 (( android.content.IntentFilter ) v4 ).addAction ( v8 ); // invoke-virtual {v4, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
	 /* .line 328 */
	 final String v8 = "miui.intent.action.CHECK_SOC"; // const-string v8, "miui.intent.action.CHECK_SOC"
	 (( android.content.IntentFilter ) v4 ).addAction ( v8 ); // invoke-virtual {v4, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
	 /* .line 329 */
	 int v9 = 2; // const/4 v9, 0x2
	 (( android.content.Context ) p1 ).registerReceiver ( v3, v4, v9 ); // invoke-virtual {p1, v3, v4, v9}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;
	 /* .line 331 */
	 /* new-instance v10, Landroid/content/Intent; */
	 /* invoke-direct {v10, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
	 /* move-object v5, v10 */
	 /* .line 332 */
	 /* .local v5, "intent":Landroid/content/Intent; */
	 /* const/high16 v10, 0x40000000 # 2.0f */
	 (( android.content.Intent ) v5 ).setFlags ( v10 ); // invoke-virtual {v5, v10}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
	 /* .line 333 */
	 /* const/high16 v11, 0x4000000 */
	 android.app.PendingIntent .getBroadcast ( p1,v2,v5,v11 );
	 this.mPendingIntent = v12;
	 /* .line 335 */
	 /* new-instance v12, Landroid/content/Intent; */
	 /* invoke-direct {v12, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
	 /* move-object v6, v12 */
	 /* .line 336 */
	 /* .local v6, "limitTime":Landroid/content/Intent; */
	 (( android.content.Intent ) v6 ).addFlags ( v10 ); // invoke-virtual {v6, v10}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
	 /* .line 337 */
	 android.app.PendingIntent .getBroadcast ( p1,v2,v6,v11 );
	 this.mPendingIntentLimitTime = v12;
	 /* .line 339 */
	 /* new-instance v12, Landroid/content/Intent; */
	 /* invoke-direct {v12, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
	 /* move-object v7, v12 */
	 /* .line 340 */
	 /* .local v7, "cycleCheck":Landroid/content/Intent; */
	 (( android.content.Intent ) v7 ).addFlags ( v10 ); // invoke-virtual {v7, v10}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
	 /* .line 341 */
	 android.app.PendingIntent .getBroadcast ( p1,v2,v7,v11 );
	 this.mPendingIntentCycleCheck = v12;
	 /* .line 343 */
	 /* new-instance v12, Landroid/content/Intent; */
	 /* invoke-direct {v12, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
	 /* move-object v8, v12 */
	 /* .line 344 */
	 /* .local v8, "checkSoc":Landroid/content/Intent; */
	 (( android.content.Intent ) v8 ).addFlags ( v10 ); // invoke-virtual {v8, v10}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
	 /* .line 345 */
	 android.app.PendingIntent .getBroadcast ( p1,v2,v8,v11 );
	 this.mPendingIntentCheckSoc = v2;
	 /* .line 348 */
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 /* new-instance v1, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v2 = "DAY = "; // const-string v2, "DAY = "
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* sget-wide v10, Lcom/android/server/MiuiBatteryStatsService;->DAY:J */
		 (( java.lang.StringBuilder ) v1 ).append ( v10, v11 ); // invoke-virtual {v1, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Slog .d ( v0,v1 );
		 /* .line 349 */
	 } // :cond_0
	 v0 = this.mAlarmManager;
	 /* .line 350 */
	 android.os.SystemClock .elapsedRealtime ( );
	 /* move-result-wide v1 */
	 v10 = this.mPendingIntent;
	 /* .line 349 */
	 (( android.app.AlarmManager ) v0 ).setExactAndAllowWhileIdle ( v9, v1, v2, v10 ); // invoke-virtual {v0, v9, v1, v2, v10}, Landroid/app/AlarmManager;->setExactAndAllowWhileIdle(IJLandroid/app/PendingIntent;)V
	 /* .line 351 */
	 return;
} // .end method
public static com.android.server.MiuiBatteryStatsService getInstance ( android.content.Context p0 ) {
	 /* .locals 2 */
	 /* .param p0, "context" # Landroid/content/Context; */
	 /* .line 117 */
	 v0 = com.android.server.MiuiBatteryStatsService.INSTANCE;
	 /* if-nez v0, :cond_1 */
	 /* .line 118 */
	 /* const-class v0, Lcom/android/server/MiuiBatteryStatsService; */
	 /* monitor-enter v0 */
	 /* .line 119 */
	 try { // :try_start_0
		 v1 = com.android.server.MiuiBatteryStatsService.INSTANCE;
		 /* if-nez v1, :cond_0 */
		 /* .line 120 */
		 /* new-instance v1, Lcom/android/server/MiuiBatteryStatsService; */
		 /* invoke-direct {v1, p0}, Lcom/android/server/MiuiBatteryStatsService;-><init>(Landroid/content/Context;)V */
		 /* .line 122 */
	 } // :cond_0
	 /* monitor-exit v0 */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 /* monitor-exit v0 */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* throw v1 */
	 /* .line 124 */
} // :cond_1
} // :goto_0
v0 = com.android.server.MiuiBatteryStatsService.INSTANCE;
} // .end method
