.class Lcom/android/server/MiuiBatteryAuthentic$IMTService;
.super Ljava/lang/Object;
.source "MiuiBatteryAuthentic.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/MiuiBatteryAuthentic;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "IMTService"
.end annotation


# instance fields
.field private final DEFAULT:Ljava/lang/String;

.field private final GET_ECC_SIGN:I

.field private final GET_FID:I

.field private final INTERFACE_DESCRIPTOR:Ljava/lang/String;

.field private final SERVICE_NAME:Ljava/lang/String;

.field final synthetic this$0:Lcom/android/server/MiuiBatteryAuthentic;


# direct methods
.method constructor <init>(Lcom/android/server/MiuiBatteryAuthentic;)V
    .locals 1
    .param p1, "this$0"    # Lcom/android/server/MiuiBatteryAuthentic;

    .line 654
    iput-object p1, p0, Lcom/android/server/MiuiBatteryAuthentic$IMTService;->this$0:Lcom/android/server/MiuiBatteryAuthentic;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 655
    const-string/jumbo v0, "vendor.xiaomi.hardware.mtdservice@1.0::IMTService"

    iput-object v0, p0, Lcom/android/server/MiuiBatteryAuthentic$IMTService;->SERVICE_NAME:Ljava/lang/String;

    .line 656
    iput-object v0, p0, Lcom/android/server/MiuiBatteryAuthentic$IMTService;->INTERFACE_DESCRIPTOR:Ljava/lang/String;

    .line 657
    const-string v0, "default"

    iput-object v0, p0, Lcom/android/server/MiuiBatteryAuthentic$IMTService;->DEFAULT:Ljava/lang/String;

    .line 658
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/server/MiuiBatteryAuthentic$IMTService;->GET_FID:I

    .line 659
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/server/MiuiBatteryAuthentic$IMTService;->GET_ECC_SIGN:I

    return-void
.end method


# virtual methods
.method public eccSign(ILjava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "keyType"    # I
    .param p2, "text"    # Ljava/lang/String;

    .line 683
    const-string/jumbo v0, "vendor.xiaomi.hardware.mtdservice@1.0::IMTService"

    new-instance v1, Landroid/os/HwParcel;

    invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V

    .line 684
    .local v1, "hidl_reply":Landroid/os/HwParcel;
    const/4 v2, 0x0

    .line 686
    .local v2, "val":Ljava/lang/String;
    :try_start_0
    const-string v3, "default"

    invoke-static {v0, v3}, Landroid/os/HwBinder;->getService(Ljava/lang/String;Ljava/lang/String;)Landroid/os/IHwBinder;

    move-result-object v3

    .line 687
    .local v3, "hwService":Landroid/os/IHwBinder;
    if-eqz v3, :cond_0

    .line 688
    new-instance v4, Landroid/os/HwParcel;

    invoke-direct {v4}, Landroid/os/HwParcel;-><init>()V

    .line 689
    .local v4, "hidl_request":Landroid/os/HwParcel;
    invoke-virtual {v4, v0}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 690
    invoke-virtual {v4, p1}, Landroid/os/HwParcel;->writeInt32(I)V

    .line 691
    invoke-virtual {v4, p2}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V

    .line 692
    const/4 v0, 0x2

    const/4 v5, 0x0

    invoke-interface {v3, v0, v4, v1, v5}, Landroid/os/IHwBinder;->transact(ILandroid/os/HwParcel;Landroid/os/HwParcel;I)V

    .line 693
    invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V

    .line 694
    invoke-virtual {v4}, Landroid/os/HwParcel;->releaseTemporaryStorage()V

    .line 695
    invoke-virtual {v1}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v2, v0

    .line 700
    .end local v3    # "hwService":Landroid/os/IHwBinder;
    .end local v4    # "hidl_request":Landroid/os/HwParcel;
    :cond_0
    nop

    :goto_0
    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    .line 701
    goto :goto_1

    .line 700
    :catchall_0
    move-exception v0

    goto :goto_2

    .line 697
    :catch_0
    move-exception v0

    .line 698
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v3, "MiuiBatteryAuthentic"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "IMTService eccSign transact failed. "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 700
    nop

    .end local v0    # "e":Ljava/lang/Exception;
    goto :goto_0

    .line 702
    :goto_1
    return-object v2

    .line 700
    :goto_2
    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    .line 701
    throw v0
.end method

.method public getFid()Ljava/lang/String;
    .locals 6

    .line 662
    const-string/jumbo v0, "vendor.xiaomi.hardware.mtdservice@1.0::IMTService"

    new-instance v1, Landroid/os/HwParcel;

    invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V

    .line 663
    .local v1, "hidl_reply":Landroid/os/HwParcel;
    const/4 v2, 0x0

    .line 665
    .local v2, "val":Ljava/lang/String;
    :try_start_0
    const-string v3, "default"

    invoke-static {v0, v3}, Landroid/os/HwBinder;->getService(Ljava/lang/String;Ljava/lang/String;)Landroid/os/IHwBinder;

    move-result-object v3

    .line 666
    .local v3, "hwService":Landroid/os/IHwBinder;
    if-eqz v3, :cond_0

    .line 667
    new-instance v4, Landroid/os/HwParcel;

    invoke-direct {v4}, Landroid/os/HwParcel;-><init>()V

    .line 668
    .local v4, "hidl_request":Landroid/os/HwParcel;
    invoke-virtual {v4, v0}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 669
    const/4 v0, 0x1

    const/4 v5, 0x0

    invoke-interface {v3, v0, v4, v1, v5}, Landroid/os/IHwBinder;->transact(ILandroid/os/HwParcel;Landroid/os/HwParcel;I)V

    .line 670
    invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V

    .line 671
    invoke-virtual {v4}, Landroid/os/HwParcel;->releaseTemporaryStorage()V

    .line 672
    invoke-virtual {v1}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v2, v0

    .line 677
    .end local v3    # "hwService":Landroid/os/IHwBinder;
    .end local v4    # "hidl_request":Landroid/os/HwParcel;
    :cond_0
    nop

    :goto_0
    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    .line 678
    goto :goto_1

    .line 677
    :catchall_0
    move-exception v0

    goto :goto_2

    .line 674
    :catch_0
    move-exception v0

    .line 675
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v3, "MiuiBatteryAuthentic"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "IMTService getFid transact failed. "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 677
    nop

    .end local v0    # "e":Ljava/lang/Exception;
    goto :goto_0

    .line 679
    :goto_1
    return-object v2

    .line 677
    :goto_2
    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    .line 678
    throw v0
.end method
