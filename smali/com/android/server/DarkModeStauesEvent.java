public class com.android.server.DarkModeStauesEvent implements com.android.server.DarkModeEvent {
	 /* .source "DarkModeStauesEvent.java" */
	 /* # interfaces */
	 /* # instance fields */
	 private java.lang.String mAppEnable;
	 private java.util.List mAppList;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/List<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private java.lang.String mAppName;
private java.lang.String mAppPkg;
private java.lang.String mAutoSwitch;
private java.lang.String mBeginTime;
private java.lang.String mContrastStatus;
private java.lang.String mDarkModeStatus;
private java.lang.String mEndTime;
private java.lang.String mEventName;
private java.lang.String mSettingChannel;
private java.lang.String mStatusAfterClick;
private Integer mSuggest;
private Integer mSuggestClick;
private Integer mSuggestEnable;
private Integer mSuggestOpenInSetting;
private java.lang.String mTimeModePattern;
private java.lang.String mTimeModeStatus;
private java.lang.String mTip;
private java.lang.String mWallPaperStatus;
/* # direct methods */
public com.android.server.DarkModeStauesEvent ( ) {
/* .locals 1 */
/* .line 52 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 30 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mAppList = v0;
/* .line 53 */
return;
} // .end method
/* # virtual methods */
public com.android.server.DarkModeEvent clone ( ) {
/* .locals 3 */
/* .line 236 */
/* new-instance v0, Lcom/android/server/DarkModeStauesEvent; */
/* invoke-direct {v0}, Lcom/android/server/DarkModeStauesEvent;-><init>()V */
/* .line 237 */
(( com.android.server.DarkModeStauesEvent ) p0 ).getEventName ( ); // invoke-virtual {p0}, Lcom/android/server/DarkModeStauesEvent;->getEventName()Ljava/lang/String;
(( com.android.server.DarkModeStauesEvent ) v0 ).setEventName ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setEventName(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
/* .line 238 */
(( com.android.server.DarkModeStauesEvent ) p0 ).getTip ( ); // invoke-virtual {p0}, Lcom/android/server/DarkModeStauesEvent;->getTip()Ljava/lang/String;
(( com.android.server.DarkModeStauesEvent ) v0 ).setTip ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setTip(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
/* .line 239 */
(( com.android.server.DarkModeStauesEvent ) p0 ).getAppEnable ( ); // invoke-virtual {p0}, Lcom/android/server/DarkModeStauesEvent;->getAppEnable()Ljava/lang/String;
(( com.android.server.DarkModeStauesEvent ) v0 ).setAppEnable ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setAppEnable(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
/* new-instance v1, Ljava/util/ArrayList; */
/* .line 240 */
(( com.android.server.DarkModeStauesEvent ) p0 ).getAppList ( ); // invoke-virtual {p0}, Lcom/android/server/DarkModeStauesEvent;->getAppList()Ljava/util/List;
/* invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
(( com.android.server.DarkModeStauesEvent ) v0 ).setAppList ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setAppList(Ljava/util/List;)Lcom/android/server/DarkModeStauesEvent;
/* .line 241 */
(( com.android.server.DarkModeStauesEvent ) p0 ).getAppName ( ); // invoke-virtual {p0}, Lcom/android/server/DarkModeStauesEvent;->getAppName()Ljava/lang/String;
(( com.android.server.DarkModeStauesEvent ) v0 ).setAppName ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setAppName(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
/* .line 242 */
(( com.android.server.DarkModeStauesEvent ) p0 ).getAppPkg ( ); // invoke-virtual {p0}, Lcom/android/server/DarkModeStauesEvent;->getAppPkg()Ljava/lang/String;
(( com.android.server.DarkModeStauesEvent ) v0 ).setAppPkg ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setAppPkg(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
/* .line 243 */
(( com.android.server.DarkModeStauesEvent ) p0 ).getAutoSwitch ( ); // invoke-virtual {p0}, Lcom/android/server/DarkModeStauesEvent;->getAutoSwitch()Ljava/lang/String;
(( com.android.server.DarkModeStauesEvent ) v0 ).setAutoSwitch ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setAutoSwitch(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
/* .line 244 */
(( com.android.server.DarkModeStauesEvent ) p0 ).getBeginTime ( ); // invoke-virtual {p0}, Lcom/android/server/DarkModeStauesEvent;->getBeginTime()Ljava/lang/String;
(( com.android.server.DarkModeStauesEvent ) v0 ).setBeginTime ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setBeginTime(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
/* .line 245 */
(( com.android.server.DarkModeStauesEvent ) p0 ).getContrastStatus ( ); // invoke-virtual {p0}, Lcom/android/server/DarkModeStauesEvent;->getContrastStatus()Ljava/lang/String;
(( com.android.server.DarkModeStauesEvent ) v0 ).setContrastStatus ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setContrastStatus(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
/* .line 246 */
(( com.android.server.DarkModeStauesEvent ) p0 ).getDarkModeStatus ( ); // invoke-virtual {p0}, Lcom/android/server/DarkModeStauesEvent;->getDarkModeStatus()Ljava/lang/String;
(( com.android.server.DarkModeStauesEvent ) v0 ).setDarkModeStatus ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setDarkModeStatus(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
/* .line 247 */
(( com.android.server.DarkModeStauesEvent ) p0 ).getEndTime ( ); // invoke-virtual {p0}, Lcom/android/server/DarkModeStauesEvent;->getEndTime()Ljava/lang/String;
(( com.android.server.DarkModeStauesEvent ) v0 ).setEndTime ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setEndTime(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
/* .line 248 */
v1 = (( com.android.server.DarkModeStauesEvent ) p0 ).getSuggest ( ); // invoke-virtual {p0}, Lcom/android/server/DarkModeStauesEvent;->getSuggest()I
(( com.android.server.DarkModeStauesEvent ) v0 ).setSuggest ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setSuggest(I)Lcom/android/server/DarkModeStauesEvent;
/* .line 249 */
v1 = (( com.android.server.DarkModeStauesEvent ) p0 ).getSuggestClick ( ); // invoke-virtual {p0}, Lcom/android/server/DarkModeStauesEvent;->getSuggestClick()I
(( com.android.server.DarkModeStauesEvent ) v0 ).setSuggestClick ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setSuggestClick(I)Lcom/android/server/DarkModeStauesEvent;
/* .line 250 */
(( com.android.server.DarkModeStauesEvent ) p0 ).getSettingChannel ( ); // invoke-virtual {p0}, Lcom/android/server/DarkModeStauesEvent;->getSettingChannel()Ljava/lang/String;
(( com.android.server.DarkModeStauesEvent ) v0 ).setSettingChannel ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setSettingChannel(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
/* .line 251 */
(( com.android.server.DarkModeStauesEvent ) p0 ).getStatusAfterClick ( ); // invoke-virtual {p0}, Lcom/android/server/DarkModeStauesEvent;->getStatusAfterClick()Ljava/lang/String;
(( com.android.server.DarkModeStauesEvent ) v0 ).setStatusAfterClick ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setStatusAfterClick(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
/* .line 252 */
v1 = (( com.android.server.DarkModeStauesEvent ) p0 ).getSuggestEnable ( ); // invoke-virtual {p0}, Lcom/android/server/DarkModeStauesEvent;->getSuggestEnable()I
(( com.android.server.DarkModeStauesEvent ) v0 ).setSuggestEnable ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setSuggestEnable(I)Lcom/android/server/DarkModeStauesEvent;
/* .line 253 */
v1 = (( com.android.server.DarkModeStauesEvent ) p0 ).getSuggestOpenInSetting ( ); // invoke-virtual {p0}, Lcom/android/server/DarkModeStauesEvent;->getSuggestOpenInSetting()I
(( com.android.server.DarkModeStauesEvent ) v0 ).setSuggestOpenInSetting ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setSuggestOpenInSetting(I)Lcom/android/server/DarkModeStauesEvent;
/* .line 254 */
(( com.android.server.DarkModeStauesEvent ) p0 ).getTimeModePattern ( ); // invoke-virtual {p0}, Lcom/android/server/DarkModeStauesEvent;->getTimeModePattern()Ljava/lang/String;
(( com.android.server.DarkModeStauesEvent ) v0 ).setTimeModePattern ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setTimeModePattern(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
/* .line 255 */
(( com.android.server.DarkModeStauesEvent ) p0 ).getTimeModeStatus ( ); // invoke-virtual {p0}, Lcom/android/server/DarkModeStauesEvent;->getTimeModeStatus()Ljava/lang/String;
(( com.android.server.DarkModeStauesEvent ) v0 ).setTimeModeStatus ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setTimeModeStatus(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
/* .line 256 */
(( com.android.server.DarkModeStauesEvent ) p0 ).getWallPaperStatus ( ); // invoke-virtual {p0}, Lcom/android/server/DarkModeStauesEvent;->getWallPaperStatus()Ljava/lang/String;
(( com.android.server.DarkModeStauesEvent ) v0 ).setWallPaperStatus ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setWallPaperStatus(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
/* .line 257 */
/* .local v0, "event":Lcom/android/server/DarkModeEvent; */
} // .end method
public java.lang.Object clone ( ) { //bridge//synthethic
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/CloneNotSupportedException; */
/* } */
} // .end annotation
/* .line 10 */
(( com.android.server.DarkModeStauesEvent ) p0 ).clone ( ); // invoke-virtual {p0}, Lcom/android/server/DarkModeStauesEvent;->clone()Lcom/android/server/DarkModeEvent;
} // .end method
public java.lang.String getAppEnable ( ) {
/* .locals 1 */
/* .line 155 */
v0 = this.mAppEnable;
} // .end method
public java.util.List getAppList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 128 */
v0 = this.mAppList;
} // .end method
public java.lang.String getAppName ( ) {
/* .locals 1 */
/* .line 146 */
v0 = this.mAppName;
} // .end method
public java.lang.String getAppPkg ( ) {
/* .locals 1 */
/* .line 137 */
v0 = this.mAppPkg;
} // .end method
public java.lang.String getAutoSwitch ( ) {
/* .locals 1 */
/* .line 164 */
v0 = this.mAutoSwitch;
} // .end method
public java.lang.String getBeginTime ( ) {
/* .locals 1 */
/* .line 92 */
v0 = this.mBeginTime;
} // .end method
public java.lang.String getContrastStatus ( ) {
/* .locals 1 */
/* .line 119 */
v0 = this.mContrastStatus;
} // .end method
public java.lang.String getDarkModeStatus ( ) {
/* .locals 1 */
/* .line 65 */
v0 = this.mDarkModeStatus;
} // .end method
public java.lang.String getEndTime ( ) {
/* .locals 1 */
/* .line 101 */
v0 = this.mEndTime;
} // .end method
public java.lang.String getEventName ( ) {
/* .locals 1 */
/* .line 56 */
v0 = this.mEventName;
} // .end method
public java.lang.String getSettingChannel ( ) {
/* .locals 1 */
/* .line 182 */
v0 = this.mSettingChannel;
} // .end method
public java.lang.String getStatusAfterClick ( ) {
/* .locals 1 */
/* .line 173 */
v0 = this.mStatusAfterClick;
} // .end method
public Integer getSuggest ( ) {
/* .locals 1 */
/* .line 200 */
/* iget v0, p0, Lcom/android/server/DarkModeStauesEvent;->mSuggest:I */
} // .end method
public Integer getSuggestClick ( ) {
/* .locals 1 */
/* .line 209 */
/* iget v0, p0, Lcom/android/server/DarkModeStauesEvent;->mSuggestClick:I */
} // .end method
public Integer getSuggestEnable ( ) {
/* .locals 1 */
/* .line 218 */
/* iget v0, p0, Lcom/android/server/DarkModeStauesEvent;->mSuggestEnable:I */
} // .end method
public Integer getSuggestOpenInSetting ( ) {
/* .locals 1 */
/* .line 227 */
/* iget v0, p0, Lcom/android/server/DarkModeStauesEvent;->mSuggestOpenInSetting:I */
} // .end method
public java.lang.String getTimeModePattern ( ) {
/* .locals 1 */
/* .line 83 */
v0 = this.mTimeModePattern;
} // .end method
public java.lang.String getTimeModeStatus ( ) {
/* .locals 1 */
/* .line 74 */
v0 = this.mTimeModeStatus;
} // .end method
public java.lang.String getTip ( ) {
/* .locals 1 */
/* .line 191 */
v0 = this.mTip;
} // .end method
public java.lang.String getWallPaperStatus ( ) {
/* .locals 1 */
/* .line 110 */
v0 = this.mWallPaperStatus;
} // .end method
public com.android.server.DarkModeStauesEvent setAppEnable ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "enable" # Ljava/lang/String; */
/* .line 159 */
this.mAppEnable = p1;
/* .line 160 */
} // .end method
public com.android.server.DarkModeStauesEvent setAppList ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)", */
/* "Lcom/android/server/DarkModeStauesEvent;" */
/* } */
} // .end annotation
/* .line 132 */
/* .local p1, "appList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
this.mAppList = p1;
/* .line 133 */
} // .end method
public com.android.server.DarkModeStauesEvent setAppName ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "appName" # Ljava/lang/String; */
/* .line 150 */
this.mAppName = p1;
/* .line 151 */
} // .end method
public com.android.server.DarkModeStauesEvent setAppPkg ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "appPkg" # Ljava/lang/String; */
/* .line 141 */
this.mAppPkg = p1;
/* .line 142 */
} // .end method
public com.android.server.DarkModeStauesEvent setAutoSwitch ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "autoSwitch" # Ljava/lang/String; */
/* .line 168 */
this.mAutoSwitch = p1;
/* .line 169 */
} // .end method
public com.android.server.DarkModeStauesEvent setBeginTime ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "beginTime" # Ljava/lang/String; */
/* .line 96 */
this.mBeginTime = p1;
/* .line 97 */
} // .end method
public com.android.server.DarkModeStauesEvent setContrastStatus ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "montrastStatus" # Ljava/lang/String; */
/* .line 123 */
this.mContrastStatus = p1;
/* .line 124 */
} // .end method
public com.android.server.DarkModeStauesEvent setDarkModeStatus ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "darkModeStatus" # Ljava/lang/String; */
/* .line 69 */
this.mDarkModeStatus = p1;
/* .line 70 */
} // .end method
public com.android.server.DarkModeStauesEvent setEndTime ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "endTime" # Ljava/lang/String; */
/* .line 105 */
this.mEndTime = p1;
/* .line 106 */
} // .end method
public com.android.server.DarkModeStauesEvent setEventName ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "eventName" # Ljava/lang/String; */
/* .line 60 */
this.mEventName = p1;
/* .line 61 */
} // .end method
public com.android.server.DarkModeStauesEvent setSettingChannel ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "settingChannel" # Ljava/lang/String; */
/* .line 186 */
this.mSettingChannel = p1;
/* .line 187 */
} // .end method
public com.android.server.DarkModeStauesEvent setStatusAfterClick ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "statusAfterClick" # Ljava/lang/String; */
/* .line 177 */
this.mStatusAfterClick = p1;
/* .line 178 */
} // .end method
public com.android.server.DarkModeStauesEvent setSuggest ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "suggest" # I */
/* .line 204 */
/* iput p1, p0, Lcom/android/server/DarkModeStauesEvent;->mSuggest:I */
/* .line 205 */
} // .end method
public com.android.server.DarkModeStauesEvent setSuggestClick ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "suggestClick" # I */
/* .line 213 */
/* iput p1, p0, Lcom/android/server/DarkModeStauesEvent;->mSuggestClick:I */
/* .line 214 */
} // .end method
public com.android.server.DarkModeStauesEvent setSuggestEnable ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "suggestEnable" # I */
/* .line 222 */
/* iput p1, p0, Lcom/android/server/DarkModeStauesEvent;->mSuggestEnable:I */
/* .line 223 */
} // .end method
public com.android.server.DarkModeStauesEvent setSuggestOpenInSetting ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "suggestOpenInSetting" # I */
/* .line 231 */
/* iput p1, p0, Lcom/android/server/DarkModeStauesEvent;->mSuggestOpenInSetting:I */
/* .line 232 */
} // .end method
public com.android.server.DarkModeStauesEvent setTimeModePattern ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "timeModePattern" # Ljava/lang/String; */
/* .line 87 */
this.mTimeModePattern = p1;
/* .line 88 */
} // .end method
public com.android.server.DarkModeStauesEvent setTimeModeStatus ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "timeModeStatus" # Ljava/lang/String; */
/* .line 78 */
this.mTimeModeStatus = p1;
/* .line 79 */
} // .end method
public com.android.server.DarkModeStauesEvent setTip ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "tip" # Ljava/lang/String; */
/* .line 195 */
this.mTip = p1;
/* .line 196 */
} // .end method
public com.android.server.DarkModeStauesEvent setWallPaperStatus ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "wallPaperStatus" # Ljava/lang/String; */
/* .line 114 */
this.mWallPaperStatus = p1;
/* .line 115 */
} // .end method
public java.lang.String toString ( ) {
/* .locals 3 */
/* .line 262 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "DarkModeStauesEvent{mEventName=\'"; // const-string v1, "DarkModeStauesEvent{mEventName=\'"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mEventName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v1, 0x27 */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
final String v2 = ", mTip=\'"; // const-string v2, ", mTip=\'"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mTip;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
final String v2 = ", mDarkModeStatus=\'"; // const-string v2, ", mDarkModeStatus=\'"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mDarkModeStatus;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
final String v2 = ", mTimeModeStatus=\'"; // const-string v2, ", mTimeModeStatus=\'"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mTimeModeStatus;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
final String v2 = ", mTimeModePattern=\'"; // const-string v2, ", mTimeModePattern=\'"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mTimeModePattern;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
final String v2 = ", mBeginTime=\'"; // const-string v2, ", mBeginTime=\'"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mBeginTime;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
final String v2 = ", mEndTime=\'"; // const-string v2, ", mEndTime=\'"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mEndTime;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
final String v2 = ", mWallPaperStatus=\'"; // const-string v2, ", mWallPaperStatus=\'"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mWallPaperStatus;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
final String v2 = ", mContrastStatus=\'"; // const-string v2, ", mContrastStatus=\'"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mContrastStatus;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
final String v2 = ", mAppList="; // const-string v2, ", mAppList="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mAppList;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v2 = ", mAppPkg=\'"; // const-string v2, ", mAppPkg=\'"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mAppPkg;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
final String v2 = ", mAppName=\'"; // const-string v2, ", mAppName=\'"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mAppName;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
final String v2 = ", mAppEnable=\'"; // const-string v2, ", mAppEnable=\'"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mAppEnable;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
final String v2 = ", mAutoSwitch=\'"; // const-string v2, ", mAutoSwitch=\'"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mAutoSwitch;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
final String v2 = ", mStatusAfterClick=\'"; // const-string v2, ", mStatusAfterClick=\'"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mStatusAfterClick;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
final String v2 = ", mSettingChannel=\'"; // const-string v2, ", mSettingChannel=\'"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mSettingChannel;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
final String v1 = ", mSuggest="; // const-string v1, ", mSuggest="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/DarkModeStauesEvent;->mSuggest:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", mSuggestClick="; // const-string v1, ", mSuggestClick="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/DarkModeStauesEvent;->mSuggestClick:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", mSuggestEnable="; // const-string v1, ", mSuggestEnable="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/DarkModeStauesEvent;->mSuggestEnable:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", mSuggestOpenInSetting="; // const-string v1, ", mSuggestOpenInSetting="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/DarkModeStauesEvent;->mSuggestOpenInSetting:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const/16 v1, 0x7d */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
