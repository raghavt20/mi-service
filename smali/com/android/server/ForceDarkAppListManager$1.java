class com.android.server.ForceDarkAppListManager$1 implements com.android.server.ForceDarkAppListProvider$ForceDarkAppListChangeListener {
	 /* .source "ForceDarkAppListManager.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/ForceDarkAppListManager;-><init>(Landroid/content/Context;Lcom/android/server/ForceDarkUiModeModeManager;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.ForceDarkAppListManager this$0; //synthetic
/* # direct methods */
 com.android.server.ForceDarkAppListManager$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/ForceDarkAppListManager; */
/* .line 57 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onChange ( ) {
/* .locals 2 */
/* .line 60 */
v0 = this.this$0;
com.android.server.ForceDarkAppListManager .-$$Nest$fgetmForceDarkAppListProvider ( v0 );
(( com.android.server.ForceDarkAppListProvider ) v1 ).getForceDarkAppSettings ( ); // invoke-virtual {v1}, Lcom/android/server/ForceDarkAppListProvider;->getForceDarkAppSettings()Ljava/util/HashMap;
com.android.server.ForceDarkAppListManager .-$$Nest$fputmForceDarkAppSettings ( v0,v1 );
/* .line 61 */
v0 = this.this$0;
com.android.server.ForceDarkAppListManager .-$$Nest$fgetmForceDarkUiModeModeManager ( v0 );
(( com.android.server.ForceDarkUiModeModeManager ) v0 ).notifyAppListChanged ( ); // invoke-virtual {v0}, Lcom/android/server/ForceDarkUiModeModeManager;->notifyAppListChanged()V
/* .line 62 */
v0 = this.this$0;
com.android.server.ForceDarkAppListManager .-$$Nest$mclearCache ( v0 );
/* .line 63 */
return;
} // .end method
