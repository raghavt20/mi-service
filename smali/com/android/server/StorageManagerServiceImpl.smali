.class public Lcom/android/server/StorageManagerServiceImpl;
.super Lcom/android/server/StorageManagerServiceStub;
.source "StorageManagerServiceImpl.java"


# static fields
.field public static final EUA_SUPPORTEDSIZE:I

.field private static final HAL_DEFAULT:Ljava/lang/String; = "default"

.field private static final HAL_INTERFACE_DESCRIPTOR:Ljava/lang/String; = "vendor.xiaomi.hardware.fbo@1.0::IFbo"

.field private static final HAL_SERVICE_NAME:Ljava/lang/String; = "vendor.xiaomi.hardware.fbo@1.0::IFbo"

.field private static final PROP_ABREUSE_SIZE:Ljava/lang/String; = "sys.abreuse.size"

.field private static final TAG:Ljava/lang/String; = "StorageManagerService"

.field private static final THRESHOLD_1:J

.field private static final THRESHOLD_2:J

.field private static final THRESHOLD_3:J

.field private static final TRANSACTION_EUASUPPORTEDSIZE:I = 0x9

.field public static sLowStorage:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 24
    sget-object v0, Landroid/util/DataUnit;->GIBIBYTES:Landroid/util/DataUnit;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/util/DataUnit;->toBytes(J)J

    move-result-wide v0

    sput-wide v0, Lcom/android/server/StorageManagerServiceImpl;->THRESHOLD_1:J

    .line 25
    sget-object v0, Landroid/util/DataUnit;->GIBIBYTES:Landroid/util/DataUnit;

    const-wide/16 v1, 0x5

    invoke-virtual {v0, v1, v2}, Landroid/util/DataUnit;->toBytes(J)J

    move-result-wide v0

    sput-wide v0, Lcom/android/server/StorageManagerServiceImpl;->THRESHOLD_2:J

    .line 26
    sget-object v0, Landroid/util/DataUnit;->GIBIBYTES:Landroid/util/DataUnit;

    const-wide/16 v1, 0xa

    invoke-virtual {v0, v1, v2}, Landroid/util/DataUnit;->toBytes(J)J

    move-result-wide v0

    sput-wide v0, Lcom/android/server/StorageManagerServiceImpl;->THRESHOLD_3:J

    .line 28
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/server/StorageManagerServiceImpl;->sLowStorage:Z

    .line 35
    invoke-static {}, Lcom/android/server/StorageManagerServiceImpl;->getEUASupportSizeInternal()I

    move-result v0

    sput v0, Lcom/android/server/StorageManagerServiceImpl;->EUA_SUPPORTEDSIZE:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/android/server/StorageManagerServiceStub;-><init>()V

    return-void
.end method

.method private static getEUASupportSizeInternal()I
    .locals 8

    .line 93
    const-string/jumbo v0, "vendor.xiaomi.hardware.fbo@1.0::IFbo"

    const-string v1, "StorageManagerService"

    new-instance v2, Landroid/os/HwParcel;

    invoke-direct {v2}, Landroid/os/HwParcel;-><init>()V

    .line 95
    .local v2, "hidlReply":Landroid/os/HwParcel;
    const/4 v3, 0x0

    :try_start_0
    const-string v4, "default"

    invoke-static {v0, v4}, Landroid/os/HwBinder;->getService(Ljava/lang/String;Ljava/lang/String;)Landroid/os/IHwBinder;

    move-result-object v4

    .line 96
    .local v4, "hwService":Landroid/os/IHwBinder;
    if-eqz v4, :cond_0

    .line 97
    new-instance v5, Landroid/os/HwParcel;

    invoke-direct {v5}, Landroid/os/HwParcel;-><init>()V

    .line 98
    .local v5, "hidlRequest":Landroid/os/HwParcel;
    invoke-virtual {v5, v0}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 99
    const/16 v0, 0x9

    invoke-interface {v4, v0, v5, v2, v3}, Landroid/os/IHwBinder;->transact(ILandroid/os/HwParcel;Landroid/os/HwParcel;I)V

    .line 100
    invoke-virtual {v2}, Landroid/os/HwParcel;->verifySuccess()V

    .line 101
    invoke-virtual {v5}, Landroid/os/HwParcel;->releaseTemporaryStorage()V

    .line 102
    invoke-virtual {v2}, Landroid/os/HwParcel;->readInt32()I

    move-result v0

    .line 103
    .local v0, "size":I
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "return hidlReply.readInt();"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 104
    nop

    .line 111
    invoke-virtual {v2}, Landroid/os/HwParcel;->release()V

    .line 104
    return v0

    .line 106
    .end local v0    # "size":I
    .end local v5    # "hidlRequest":Landroid/os/HwParcel;
    :cond_0
    :try_start_1
    const-string v0, "Failed to get MiuiFboService for EUA support size"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 111
    nop

    .end local v4    # "hwService":Landroid/os/IHwBinder;
    :goto_0
    invoke-virtual {v2}, Landroid/os/HwParcel;->release()V

    .line 112
    goto :goto_1

    .line 111
    :catchall_0
    move-exception v0

    goto :goto_2

    .line 108
    :catch_0
    move-exception v0

    .line 109
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "fail to user MiuiFboService EUA_SupportSize:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 111
    nop

    .end local v0    # "e":Ljava/lang/Exception;
    goto :goto_0

    .line 113
    :goto_1
    return v3

    .line 111
    :goto_2
    invoke-virtual {v2}, Landroid/os/HwParcel;->release()V

    .line 112
    throw v0
.end method

.method public static getSizeInternal(JJJ)J
    .locals 3
    .param p0, "abreuseSize"    # J
    .param p2, "availableUserdataSize"    # J
    .param p4, "primaryStorageSize"    # J

    .line 56
    sget-boolean v0, Lcom/android/server/StorageManagerServiceImpl;->sLowStorage:Z

    invoke-static {p4, p5}, Lcom/android/server/StorageManagerServiceImpl;->getThreshold(J)J

    move-result-wide v1

    cmp-long v1, p2, v1

    if-gtz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    or-int/2addr v0, v1

    sput-boolean v0, Lcom/android/server/StorageManagerServiceImpl;->sLowStorage:Z

    .line 57
    if-eqz v0, :cond_1

    const-wide/16 v0, 0x0

    goto :goto_1

    :cond_1
    move-wide v0, p0

    :goto_1
    add-long/2addr v0, p2

    return-wide v0
.end method

.method private static getThreshold(J)J
    .locals 4
    .param p0, "totalStorageSize"    # J

    .line 42
    sget-object v0, Landroid/util/DataUnit;->GIGABYTES:Landroid/util/DataUnit;

    sget v1, Lcom/android/server/StorageManagerServiceImpl;->EUA_SUPPORTEDSIZE:I

    add-int/lit8 v2, v1, 0x40

    int-to-long v2, v2

    invoke-virtual {v0, v2, v3}, Landroid/util/DataUnit;->toBytes(J)J

    move-result-wide v2

    cmp-long v0, p0, v2

    if-gez v0, :cond_0

    .line 43
    sget-wide v0, Lcom/android/server/StorageManagerServiceImpl;->THRESHOLD_1:J

    return-wide v0

    .line 44
    :cond_0
    sget-object v0, Landroid/util/DataUnit;->GIGABYTES:Landroid/util/DataUnit;

    add-int/lit16 v1, v1, 0x200

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/util/DataUnit;->toBytes(J)J

    move-result-wide v0

    cmp-long v0, p0, v0

    if-gez v0, :cond_1

    .line 45
    sget-wide v0, Lcom/android/server/StorageManagerServiceImpl;->THRESHOLD_2:J

    return-wide v0

    .line 47
    :cond_1
    sget-wide v0, Lcom/android/server/StorageManagerServiceImpl;->THRESHOLD_3:J

    return-wide v0
.end method


# virtual methods
.method public getAvailableStorageSize(Landroid/content/Context;J)J
    .locals 12
    .param p1, "mContext"    # Landroid/content/Context;
    .param p2, "availableBytes"    # J

    .line 73
    const-class v0, Landroid/os/storage/StorageManager;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/storage/StorageManager;

    .line 74
    .local v0, "storage":Landroid/os/storage/StorageManager;
    invoke-virtual {v0}, Landroid/os/storage/StorageManager;->getPrimaryStorageSize()J

    move-result-wide v7

    .line 76
    .local v7, "primaryStorageSize":J
    const-string/jumbo v1, "sys.abreuse.size"

    const-string v2, "0"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 77
    .local v9, "abreuseSizeStr":Ljava/lang/String;
    const-wide/16 v1, 0x0

    .line 79
    .local v1, "abreuseSize":J
    :try_start_0
    invoke-static {v9}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-wide v1, v3

    .line 82
    move-wide v10, v1

    goto :goto_0

    .line 80
    :catch_0
    move-exception v3

    .line 81
    .local v3, "e":Ljava/lang/NumberFormatException;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "abnormal reuse size "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "StorageManagerService"

    invoke-static {v5, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-wide v10, v1

    .line 84
    .end local v1    # "abreuseSize":J
    .end local v3    # "e":Ljava/lang/NumberFormatException;
    .local v10, "abreuseSize":J
    :goto_0
    move-wide v1, v10

    move-wide v3, p2

    move-wide v5, v7

    invoke-static/range {v1 .. v6}, Lcom/android/server/StorageManagerServiceImpl;->getSizeInternal(JJJ)J

    move-result-wide v1

    return-wide v1
.end method

.method public getEUASupportSize()I
    .locals 1

    .line 132
    sget v0, Lcom/android/server/StorageManagerServiceImpl;->EUA_SUPPORTEDSIZE:I

    return v0
.end method

.method public getPrimaryStorageSizeInternal(J)J
    .locals 6
    .param p1, "totalSize"    # J

    .line 124
    sget-object v0, Landroid/util/DataUnit;->GIBIBYTES:Landroid/util/DataUnit;

    sget v1, Lcom/android/server/StorageManagerServiceImpl;->EUA_SUPPORTEDSIZE:I

    int-to-long v2, v1

    .line 125
    invoke-virtual {v0, v2, v3}, Landroid/util/DataUnit;->toBytes(J)J

    move-result-wide v2

    sub-long v2, p1, v2

    .line 124
    invoke-static {v2, v3}, Landroid/os/FileUtils;->squareStorageSize(J)J

    move-result-wide v2

    .line 126
    .local v2, "ufsIntrinsic":J
    sget-object v0, Landroid/util/DataUnit;->GIGABYTES:Landroid/util/DataUnit;

    int-to-long v4, v1

    invoke-virtual {v0, v4, v5}, Landroid/util/DataUnit;->toBytes(J)J

    move-result-wide v0

    add-long/2addr v0, v2

    return-wide v0
.end method
