public class com.android.server.ScoutHelper$Action {
	 /* .source "ScoutHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/ScoutHelper; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "Action" */
} // .end annotation
/* # instance fields */
private java.util.List actions;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List includeFiles;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List params;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static java.util.List -$$Nest$fgetactions ( com.android.server.ScoutHelper$Action p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.actions;
} // .end method
static java.util.List -$$Nest$fgetincludeFiles ( com.android.server.ScoutHelper$Action p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.includeFiles;
} // .end method
static java.util.List -$$Nest$fgetparams ( com.android.server.ScoutHelper$Action p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.params;
} // .end method
public com.android.server.ScoutHelper$Action ( ) {
/* .locals 1 */
/* .line 191 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 192 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.actions = v0;
/* .line 193 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.params = v0;
/* .line 194 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.includeFiles = v0;
/* .line 195 */
return;
} // .end method
/* # virtual methods */
public void addActionAndParam ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "action" # Ljava/lang/String; */
/* .param p2, "param" # Ljava/lang/String; */
/* .line 203 */
v0 = this.actions;
/* .line 204 */
/* if-nez p2, :cond_0 */
/* .line 205 */
final String p2 = ""; // const-string p2, ""
/* .line 207 */
} // :cond_0
v0 = this.params;
/* .line 208 */
return;
} // .end method
public void addIncludeFile ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "path" # Ljava/lang/String; */
/* .line 215 */
v0 = android.text.TextUtils .isEmpty ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 216 */
return;
/* .line 218 */
} // :cond_0
v0 = this.includeFiles;
/* .line 219 */
return;
} // .end method
public void addIncludeFiles ( java.util.List p0 ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 223 */
/* .local p1, "includeFiles":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = if ( p1 != null) { // if-eqz p1, :cond_0
/* if-lez v0, :cond_0 */
/* .line 224 */
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* check-cast v1, Ljava/lang/String; */
/* .line 225 */
/* .local v1, "path":Ljava/lang/String; */
(( com.android.server.ScoutHelper$Action ) p0 ).addIncludeFile ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/ScoutHelper$Action;->addIncludeFile(Ljava/lang/String;)V
/* .line 226 */
} // .end local v1 # "path":Ljava/lang/String;
/* .line 228 */
} // :cond_0
return;
} // .end method
public void clearIncludeFiles ( ) {
/* .locals 1 */
/* .line 234 */
v0 = this.includeFiles;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 235 */
/* .line 237 */
} // :cond_0
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 184 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Action{actions="; // const-string v1, "Action{actions="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.actions;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = ", params="; // const-string v1, ", params="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.params;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = ", includeFiles="; // const-string v1, ", includeFiles="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.includeFiles;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
/* const/16 v1, 0x7d */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
