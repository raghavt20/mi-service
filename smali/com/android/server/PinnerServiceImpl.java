public class com.android.server.PinnerServiceImpl implements com.android.server.PinnerServiceStub {
	 /* .source "PinnerServiceImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final Boolean DEBUG;
	 private static final Integer MAX_NORMAL_APP_PIN_SIZE;
	 private static final java.lang.String TAG;
	 private static final java.util.ArrayList sDefaultDynamicPinEnableFileList;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/ArrayList<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private static final java.util.ArrayList sDefaultDynamicPinEnablePKGList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private java.util.List mAllInfoList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Landroid/content/pm/ApplicationInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.content.Context mContext;
private java.lang.String mLastPkgName;
private com.android.server.PinnerService mPinnerService;
/* # direct methods */
static com.android.server.PinnerServiceImpl ( ) {
/* .locals 2 */
/* .line 28 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 30 */
final String v1 = "com.tencent.mm"; // const-string v1, "com.tencent.mm"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 31 */
final String v1 = "com.ss.android.ugc.aweme"; // const-string v1, "com.ss.android.ugc.aweme"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 32 */
final String v1 = "com.smile.gifmaker"; // const-string v1, "com.smile.gifmaker"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 33 */
final String v1 = "com.kuaishou.nebula"; // const-string v1, "com.kuaishou.nebula"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 34 */
final String v1 = "com.tencent.mobileqq"; // const-string v1, "com.tencent.mobileqq"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 35 */
/* const-string/jumbo v1, "tv.danmaku.bili" */
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 36 */
final String v1 = "com.tencent.qqlive"; // const-string v1, "com.tencent.qqlive"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 37 */
final String v1 = "com.ss.android.article.lite"; // const-string v1, "com.ss.android.article.lite"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 38 */
final String v1 = "com.baidu.searchbox"; // const-string v1, "com.baidu.searchbox"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 39 */
final String v1 = "com.xunmeng.pinduoduo"; // const-string v1, "com.xunmeng.pinduoduo"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 40 */
final String v1 = "com.UCMobile"; // const-string v1, "com.UCMobile"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 41 */
final String v1 = "com.dragon.read"; // const-string v1, "com.dragon.read"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 42 */
final String v1 = "com.qiyi.video"; // const-string v1, "com.qiyi.video"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 43 */
final String v1 = "com.ss.android.article.video"; // const-string v1, "com.ss.android.article.video"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 44 */
final String v1 = "com.taobao.taobao"; // const-string v1, "com.taobao.taobao"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 45 */
final String v1 = "com.eg.android.AlipayGphone"; // const-string v1, "com.eg.android.AlipayGphone"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 46 */
final String v1 = "com.tencent.mtt"; // const-string v1, "com.tencent.mtt"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 47 */
final String v1 = "com.kmxs.reader"; // const-string v1, "com.kmxs.reader"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 48 */
final String v1 = "com.youku.phone"; // const-string v1, "com.youku.phone"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 49 */
final String v1 = "com.sina.weibo"; // const-string v1, "com.sina.weibo"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 50 */
final String v1 = "com.ss.android.ugc.live"; // const-string v1, "com.ss.android.ugc.live"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 51 */
final String v1 = "com.autonavi.minimap"; // const-string v1, "com.autonavi.minimap"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 52 */
final String v1 = "com.duowan.kiwi"; // const-string v1, "com.duowan.kiwi"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 53 */
final String v1 = "com.baidu.haokan"; // const-string v1, "com.baidu.haokan"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 54 */
final String v1 = "com.tencent.news"; // const-string v1, "com.tencent.news"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 55 */
final String v1 = "com.xingin.xhs"; // const-string v1, "com.xingin.xhs"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 56 */
final String v1 = "air.tv.douyu.android"; // const-string v1, "air.tv.douyu.android"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 57 */
final String v1 = "com.alibaba.android.rimet"; // const-string v1, "com.alibaba.android.rimet"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 58 */
final String v1 = "com.tencent.qqmusic"; // const-string v1, "com.tencent.qqmusic"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 59 */
final String v1 = "com.android.browser"; // const-string v1, "com.android.browser"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 60 */
final String v1 = "com.ss.android.ugc.aweme.lite"; // const-string v1, "com.ss.android.ugc.aweme.lite"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 63 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 65 */
final String v1 = "/system/lib64/libhwui.so"; // const-string v1, "/system/lib64/libhwui.so"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 66 */
final String v1 = "/vendor/lib64/hw/vulkan.adreno.so"; // const-string v1, "/vendor/lib64/hw/vulkan.adreno.so"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 67 */
final String v1 = "/vendor/lib64/libllvm-glnext.so"; // const-string v1, "/vendor/lib64/libllvm-glnext.so"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 68 */
final String v1 = "/vendor/lib64/libgsl.so"; // const-string v1, "/vendor/lib64/libgsl.so"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 69 */
final String v1 = "/system/lib64/libEGL.so"; // const-string v1, "/system/lib64/libEGL.so"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 70 */
final String v1 = "/vendor/lib64/egl/libGLESv2_adreno.so"; // const-string v1, "/vendor/lib64/egl/libGLESv2_adreno.so"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 71 */
return;
} // .end method
public com.android.server.PinnerServiceImpl ( ) {
/* .locals 1 */
/* .line 17 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 25 */
final String v0 = "com.miui.home"; // const-string v0, "com.miui.home"
this.mLastPkgName = v0;
return;
} // .end method
private java.util.List getAllInfoList ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Landroid/content/pm/ApplicationInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 126 */
miui.util.TypefaceUtils .getContext ( );
/* .line 127 */
/* .local v0, "context":Landroid/content/Context; */
(( android.content.Context ) v0 ).getPackageManager ( ); // invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 128 */
/* .local v1, "pm":Landroid/content/pm/PackageManager; */
/* const/16 v2, 0x2000 */
(( android.content.pm.PackageManager ) v1 ).getInstalledApplications ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;
} // .end method
/* # virtual methods */
public java.util.ArrayList getFilesFromKey ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "key" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I)", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 142 */
v0 = com.android.server.PinnerServiceImpl.sDefaultDynamicPinEnableFileList;
} // .end method
public android.content.pm.ApplicationInfo getInfoFromKey ( Integer p0 ) {
/* .locals 6 */
/* .param p1, "key" # I */
/* .line 105 */
v0 = this.mAllInfoList;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Landroid/content/pm/ApplicationInfo; */
/* .line 106 */
/* .local v1, "info":Landroid/content/pm/ApplicationInfo; */
v2 = this.packageName;
(( com.android.server.PinnerServiceImpl ) p0 ).getPkgNameFromKey ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/PinnerServiceImpl;->getPkgNameFromKey(I)Ljava/lang/String;
v2 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 107 */
/* .line 109 */
} // .end local v1 # "info":Landroid/content/pm/ApplicationInfo;
} // :cond_0
/* .line 110 */
} // :cond_1
/* invoke-direct {p0}, Lcom/android/server/PinnerServiceImpl;->getAllInfoList()Ljava/util/List; */
this.mAllInfoList = v0;
/* .line 111 */
v1 = } // :goto_1
final String v2 = "!"; // const-string v2, "!"
final String v3 = "PinnerServiceImpl"; // const-string v3, "PinnerServiceImpl"
if ( v1 != null) { // if-eqz v1, :cond_3
/* check-cast v1, Landroid/content/pm/ApplicationInfo; */
/* .line 112 */
/* .restart local v1 # "info":Landroid/content/pm/ApplicationInfo; */
v4 = this.packageName;
(( com.android.server.PinnerServiceImpl ) p0 ).getPkgNameFromKey ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/PinnerServiceImpl;->getPkgNameFromKey(I)Ljava/lang/String;
v4 = (( java.lang.String ) v4 ).equals ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 113 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "new installed app at key "; // const-string v4, "new installed app at key "
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v0 );
/* .line 114 */
/* .line 116 */
} // .end local v1 # "info":Landroid/content/pm/ApplicationInfo;
} // :cond_2
/* .line 117 */
} // :cond_3
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "error happens at key "; // const-string v1, "error happens at key "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v0 );
/* .line 118 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Integer getKeyFromPkgName ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "name" # Ljava/lang/String; */
/* .line 164 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v1 = com.android.server.PinnerServiceImpl.sDefaultDynamicPinEnablePKGList;
v2 = (( java.util.ArrayList ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->size()I
/* if-ge v0, v2, :cond_1 */
/* .line 165 */
(( java.util.ArrayList ) v1 ).get ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/String; */
v1 = (( java.lang.String ) v1 ).equals ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 169 */
/* add-int/lit8 v1, v0, 0x3 */
/* .line 164 */
} // :cond_0
/* add-int/lit8 v0, v0, 0x1 */
/* .line 172 */
} // .end local v0 # "i":I
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
public java.lang.String getNameForKey ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "key" # I */
/* .line 133 */
int v0 = 2; // const/4 v0, 0x2
/* if-gt p1, v0, :cond_0 */
/* .line 134 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "error key for key "; // const-string v1, "error key for key "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = "!"; // const-string v1, "!"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "PinnerServiceImpl"; // const-string v1, "PinnerServiceImpl"
android.util.Slog .e ( v1,v0 );
/* .line 135 */
int v0 = 0; // const/4 v0, 0x0
/* .line 137 */
} // :cond_0
v0 = com.android.server.PinnerServiceImpl.sDefaultDynamicPinEnablePKGList;
/* add-int/lit8 v1, p1, -0x3 */
(( java.util.ArrayList ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/String; */
} // .end method
public java.lang.String getPkgNameFromKey ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "key" # I */
/* .line 147 */
int v0 = 2; // const/4 v0, 0x2
/* if-gt p1, v0, :cond_0 */
/* .line 148 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "error key for key "; // const-string v1, "error key for key "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = "!"; // const-string v1, "!"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "PinnerServiceImpl"; // const-string v1, "PinnerServiceImpl"
android.util.Slog .e ( v1,v0 );
/* .line 149 */
int v0 = 0; // const/4 v0, 0x0
/* .line 151 */
} // :cond_0
v0 = com.android.server.PinnerServiceImpl.sDefaultDynamicPinEnablePKGList;
/* add-int/lit8 v1, p1, -0x3 */
(( java.util.ArrayList ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/String; */
} // .end method
public Integer getSizeLimitForKey ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "key" # I */
/* .line 156 */
int v0 = 2; // const/4 v0, 0x2
/* if-le p1, v0, :cond_1 */
v0 = com.android.server.PinnerServiceImpl.sDefaultDynamicPinEnablePKGList;
v0 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* add-int/lit8 v0, v0, 0x3 */
/* if-lt p1, v0, :cond_0 */
/* .line 160 */
} // :cond_0
/* const/high16 v0, 0x3200000 */
/* .line 157 */
} // :cond_1
} // :goto_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "error key for key "; // const-string v1, "error key for key "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = "!"; // const-string v1, "!"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "PinnerServiceImpl"; // const-string v1, "PinnerServiceImpl"
android.util.Slog .e ( v1,v0 );
/* .line 158 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void init ( com.android.server.PinnerService p0, android.content.Context p1 ) {
/* .locals 1 */
/* .param p1, "pinnerService" # Lcom/android/server/PinnerService; */
/* .param p2, "context" # Landroid/content/Context; */
/* .line 98 */
this.mPinnerService = p1;
/* .line 99 */
this.mContext = p2;
/* .line 100 */
/* invoke-direct {p0}, Lcom/android/server/PinnerServiceImpl;->getAllInfoList()Ljava/util/List; */
this.mAllInfoList = v0;
/* .line 101 */
return;
} // .end method
public void onActivityChanged ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .line 75 */
v0 = this.mLastPkgName;
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 77 */
return;
/* .line 79 */
} // :cond_0
v0 = this.mLastPkgName;
v0 = (( com.android.server.PinnerServiceImpl ) p0 ).getKeyFromPkgName ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/PinnerServiceImpl;->getKeyFromPkgName(Ljava/lang/String;)I
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 83 */
v0 = this.mPinnerService;
v1 = this.mLastPkgName;
v1 = (( com.android.server.PinnerServiceImpl ) p0 ).getKeyFromPkgName ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/PinnerServiceImpl;->getKeyFromPkgName(Ljava/lang/String;)I
(( com.android.server.PinnerService ) v0 ).sendUnPinAppMessageForStub ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/PinnerService;->sendUnPinAppMessageForStub(I)V
/* .line 85 */
} // :cond_1
v0 = (( com.android.server.PinnerServiceImpl ) p0 ).getKeyFromPkgName ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/PinnerServiceImpl;->getKeyFromPkgName(Ljava/lang/String;)I
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 89 */
v0 = this.mPinnerService;
v1 = (( com.android.server.PinnerServiceImpl ) p0 ).getKeyFromPkgName ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/PinnerServiceImpl;->getKeyFromPkgName(Ljava/lang/String;)I
v2 = android.app.ActivityManager .getCurrentUser ( );
int v3 = 0; // const/4 v3, 0x0
(( com.android.server.PinnerService ) v0 ).sendPinAppMessageForStub ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/PinnerService;->sendPinAppMessageForStub(IIZ)V
/* .line 91 */
} // :cond_2
this.mLastPkgName = p1;
/* .line 94 */
return;
} // .end method
