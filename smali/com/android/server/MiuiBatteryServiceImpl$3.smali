.class Lcom/android/server/MiuiBatteryServiceImpl$3;
.super Ljava/lang/Object;
.source "MiuiBatteryServiceImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/MiuiBatteryServiceImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/MiuiBatteryServiceImpl;


# direct methods
.method constructor <init>(Lcom/android/server/MiuiBatteryServiceImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/MiuiBatteryServiceImpl;

    .line 183
    iput-object p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$3;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 187
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$3;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetisDisCharging(Lcom/android/server/MiuiBatteryServiceImpl;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$3;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmBtConnectState(Lcom/android/server/MiuiBatteryServiceImpl;)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$3;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmBtConnectState(Lcom/android/server/MiuiBatteryServiceImpl;)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 190
    :cond_0
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$3;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetpm(Lcom/android/server/MiuiBatteryServiceImpl;)Landroid/os/PowerManager;

    move-result-object v0

    const-string v1, "bt_disconnect_5min"

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1, v2}, Landroid/os/PowerManager;->shutdown(ZLjava/lang/String;Z)V

    .line 192
    :cond_1
    return-void
.end method
