.class Lcom/android/server/MiuiBatteryServiceImpl$2;
.super Landroid/content/BroadcastReceiver;
.source "MiuiBatteryServiceImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/MiuiBatteryServiceImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/MiuiBatteryServiceImpl;


# direct methods
.method constructor <init>(Lcom/android/server/MiuiBatteryServiceImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/MiuiBatteryServiceImpl;

    .line 164
    iput-object p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$2;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 167
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 168
    .local v0, "action":Ljava/lang/String;
    const-string v1, "android.bluetooth.device.action.ACL_CONNECTED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 169
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$2;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    const/4 v2, 0x2

    invoke-static {v1, v2}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fputmBtConnectState(Lcom/android/server/MiuiBatteryServiceImpl;I)V

    .line 170
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$2;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryServiceImpl;)Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$2;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetshutDownRnuable(Lcom/android/server/MiuiBatteryServiceImpl;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 171
    :cond_0
    const-string v1, "android.bluetooth.device.action.ACL_DISCONNECTED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 172
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$2;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fputmBtConnectState(Lcom/android/server/MiuiBatteryServiceImpl;I)V

    .line 173
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$2;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetisDisCharging(Lcom/android/server/MiuiBatteryServiceImpl;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 174
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$2;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetwakeLock(Lcom/android/server/MiuiBatteryServiceImpl;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    const-wide/32 v2, 0x57e40

    invoke-virtual {v1, v2, v3}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    .line 175
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$2;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryServiceImpl;)Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$2;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetshutDownRnuable(Lcom/android/server/MiuiBatteryServiceImpl;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 176
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$2;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryServiceImpl;)Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$2;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetshutDownRnuable(Lcom/android/server/MiuiBatteryServiceImpl;)Ljava/lang/Runnable;

    move-result-object v2

    const-wide/32 v3, 0x493e0

    invoke-virtual {v1, v2, v3, v4}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 180
    :cond_1
    :goto_0
    return-void
.end method
