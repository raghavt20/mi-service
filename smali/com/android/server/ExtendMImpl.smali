.class public Lcom/android/server/ExtendMImpl;
.super Landroid/app/job/JobService;
.source "ExtendMImpl.java"

# interfaces
.implements Lcom/android/server/ExtendMStub;


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.ExtendMStub$$"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/ExtendMImpl$MyHandler;,
        Lcom/android/server/ExtendMImpl$ExtendMRecord;
    }
.end annotation


# static fields
.field private static final BDEV_SYS:Ljava/lang/String; = "/sys/block/zram0/backing_dev"

.field private static final BROADCASTS_ACTION_MARK:Ljava/lang/String; = "miui.extm.action.mark"

.field private static final CLOUD_DM_OPT_PROP:Ljava/lang/String; = "cloud_dm_opt_enable"

.field private static final CLOUD_EXT_MEM_PROP:Ljava/lang/String; = "cloud_extm_percent"

.field private static final CLOUD_MFZ_PROP:Ljava/lang/String; = "cloud_memFreeze_control"

.field private static final DELAY_DURATION:J

.field private static final DELAY_TIME:J = 0x3e8L

.field private static final EXTM_SETTINGS_PROP:Ljava/lang/String;

.field private static final EXTM_UUID:Ljava/lang/String; = "extm_uuid"

.field private static final FLUSH_DURATION:J

.field private static final FLUSH_HIGH_LEVEL:I

.field private static final FLUSH_INTERVAL_LIMIT:J

.field private static final FLUSH_LEVEL:I

.field private static final FLUSH_LOW_LEVEL:I

.field private static final FLUSH_MEDIUM_LEVEL:I

.field private static final FLUSH_RWTHRESHOLD_PART:I

.field private static final FLUSH_ZRAM_USEDRATE:I

.field private static final LOG_VERBOSE:Z

.field private static final MARK_DURATION:J

.field private static final MEMINFO_MEM_TOTAL:Ljava/lang/String; = "MemTotal"

.field private static final MEMINFO_SWAP_FREE:Ljava/lang/String; = "SwapFree"

.field private static final MEMINFO_SWAP_TOTAL:Ljava/lang/String; = "SwapTotal"

.field private static final MEMORY_FREEZE_ENABLE:Z

.field private static final MEM_CONF_FILE_PATH:Ljava/lang/String;

.field private static final MFZ_ENABLE_SYS:Ljava/lang/String; = "/sys/block/zram0/mfz_enable"

.field private static final MIUI_DM_OPT_ENABLE:Z

.field private static final MM_STATS_MAX_FILE_SIZE:I = 0x80

.field private static final MM_STATS_SYS:Ljava/lang/String; = "/sys/block/zram0/mm_stat"

.field private static final MSG_DO_FLUSH_HIGH:I = 0x3

.field private static final MSG_DO_FLUSH_LOW:I = 0x1

.field private static final MSG_DO_FLUSH_MEDIUM:I = 0x2

.field private static final MSG_DO_MARK:I = 0x6

.field private static final MSG_START_EXTM:I = 0x5

.field private static final MSG_STOP_FLUSH:I = 0x4

.field private static final OLD_COUNT_SYS:Ljava/lang/String; = "/sys/block/zram0/idle_stat"

.field private static final PER_FLUSH_QUOTA_HIGH:I

.field private static final PER_FLUSH_QUOTA_LOW:I

.field private static final PER_FLUSH_QUOTA_MEDIUM:I

.field private static final REPORTMEMP_EXTM_LIMIT:I = 0x3

.field private static final RESET_WB_LIMIT_JOB_ID:I = 0x82f4

.field private static final STATE_NOT_RUNNING:I = 0x0

.field private static final STATE_WAIT_FOR_MARK:I = 0x1

.field private static final STATE_WAIT_FOR_RESUME_FLUSH:I = 0x3

.field private static final TAG:Ljava/lang/String; = "ExtM"

.field private static final TAG_AM:Ljava/lang/String; = "MFZ"

.field private static final THIRTY_SECONDS:J = 0x7530L

.field private static final THREE_MINUTES:J = 0x2bf20L

.field private static final VERSION:Ljava/lang/String; = "3.0"

.field private static final WB_LIMIT_ENABLE_SYS:Ljava/lang/String; = "/sys/block/zram0/writeback_limit_enable"

.field private static final WB_LIMIT_SYS:Ljava/lang/String; = "/sys/block/zram0/writeback_limit"

.field private static final WB_STATS_MAX_FILE_SIZE:I = 0x80

.field private static final WB_STATS_SYS:Ljava/lang/String; = "/sys/block/zram0/bd_stat"

.field private static final WB_SYS_LIMIT_PAGES:Ljava/lang/String; = "persist.miui.extm.daily_flush_count"

.field private static extmGear:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private static final sExtendM:Landroid/content/ComponentName;

.field private static sExtendMRecord:Lcom/android/server/ExtendMImpl$ExtendMRecord;

.field private static sMemTags:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static sSelf:Lcom/android/server/ExtendMImpl;


# instance fields
.field private binder:Landroid/os/IBinder;

.field death:Landroid/os/IBinder$DeathRecipient;

.field private dmoptLocalCloudEnable:Z

.field private flush_level_adjust:I

.field private flush_number_adjust:I

.field private flush_per_quota:I

.field private hasParseMemConfSuccessful:Z

.field private isFlushFinished:Z

.field private isInitSuccess:Z

.field private kernelSupportCheckCnt:I

.field private lastFlushTime:J

.field private lastReportMPtime:J

.field private mContext:Landroid/content/Context;

.field private mCurState:I

.field private mFlushDiffTime:J

.field private mHandler:Lcom/android/server/ExtendMImpl$MyHandler;

.field private mLastOldPages:I

.field private mLocalCloudEnable:Z

.field private mMarkDiffTime:J

.field private mMarkStartTime:J

.field private mMemInfo:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mScreenOnTime:J

.field private mThread:Landroid/os/HandlerThread;

.field private mTotalFlushed:J

.field private mTotalMarked:J

.field private volatile mVold:Landroid/os/IVold;

.field private mfzLocalCloudEnable:Z

.field private prev_bd_read:I

.field private prev_bd_write:I

.field private reportMemPressureCnt:I


# direct methods
.method static bridge synthetic -$$Nest$fgetisFlushFinished(Lcom/android/server/ExtendMImpl;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/ExtendMImpl;->isFlushFinished:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetlastFlushTime(Lcom/android/server/ExtendMImpl;)J
    .locals 2

    iget-wide v0, p0, Lcom/android/server/ExtendMImpl;->lastFlushTime:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetmCurState(Lcom/android/server/ExtendMImpl;)I
    .locals 0

    iget p0, p0, Lcom/android/server/ExtendMImpl;->mCurState:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmFlushDiffTime(Lcom/android/server/ExtendMImpl;)J
    .locals 2

    iget-wide v0, p0, Lcom/android/server/ExtendMImpl;->mFlushDiffTime:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/server/ExtendMImpl;)Lcom/android/server/ExtendMImpl$MyHandler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/ExtendMImpl;->mHandler:Lcom/android/server/ExtendMImpl$MyHandler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmMarkDiffTime(Lcom/android/server/ExtendMImpl;)J
    .locals 2

    iget-wide v0, p0, Lcom/android/server/ExtendMImpl;->mMarkDiffTime:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetmMarkStartTime(Lcom/android/server/ExtendMImpl;)J
    .locals 2

    iget-wide v0, p0, Lcom/android/server/ExtendMImpl;->mMarkStartTime:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetmMemInfo(Lcom/android/server/ExtendMImpl;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lcom/android/server/ExtendMImpl;->mMemInfo:Ljava/util/Map;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmScreenOnTime(Lcom/android/server/ExtendMImpl;)J
    .locals 2

    iget-wide v0, p0, Lcom/android/server/ExtendMImpl;->mScreenOnTime:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetprev_bd_write(Lcom/android/server/ExtendMImpl;)I
    .locals 0

    iget p0, p0, Lcom/android/server/ExtendMImpl;->prev_bd_write:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fputisFlushFinished(Lcom/android/server/ExtendMImpl;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/ExtendMImpl;->isFlushFinished:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputlastFlushTime(Lcom/android/server/ExtendMImpl;J)V
    .locals 0

    iput-wide p1, p0, Lcom/android/server/ExtendMImpl;->lastFlushTime:J

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmFlushDiffTime(Lcom/android/server/ExtendMImpl;J)V
    .locals 0

    iput-wide p1, p0, Lcom/android/server/ExtendMImpl;->mFlushDiffTime:J

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmMarkDiffTime(Lcom/android/server/ExtendMImpl;J)V
    .locals 0

    iput-wide p1, p0, Lcom/android/server/ExtendMImpl;->mMarkDiffTime:J

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmMarkStartTime(Lcom/android/server/ExtendMImpl;J)V
    .locals 0

    iput-wide p1, p0, Lcom/android/server/ExtendMImpl;->mMarkStartTime:J

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmScreenOnTime(Lcom/android/server/ExtendMImpl;J)V
    .locals 0

    iput-wide p1, p0, Lcom/android/server/ExtendMImpl;->mScreenOnTime:J

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmVold(Lcom/android/server/ExtendMImpl;Landroid/os/IVold;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/ExtendMImpl;->mVold:Landroid/os/IVold;

    return-void
.end method

.method static bridge synthetic -$$Nest$mParseMfzSettings(Lcom/android/server/ExtendMImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->ParseMfzSettings()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mParsedmoptSettings(Lcom/android/server/ExtendMImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->ParsedmoptSettings()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mSetMfzEnable(Lcom/android/server/ExtendMImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->SetMfzEnable()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mSetdmoptEnable(Lcom/android/server/ExtendMImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->SetdmoptEnable()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mconnect(Lcom/android/server/ExtendMImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->connect()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetQuotaLeft(Lcom/android/server/ExtendMImpl;)I
    .locals 0

    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->getQuotaLeft()I

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mreadMemInfo(Lcom/android/server/ExtendMImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->readMemInfo()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mrefreshExtmSettings(Lcom/android/server/ExtendMImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->refreshExtmSettings()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mregisterAlarmClock(Lcom/android/server/ExtendMImpl;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/ExtendMImpl;->registerAlarmClock(J)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mrunFlush(Lcom/android/server/ExtendMImpl;I)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/ExtendMImpl;->runFlush(I)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mshowAndSaveFlushedStat(Lcom/android/server/ExtendMImpl;IJ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/ExtendMImpl;->showAndSaveFlushedStat(IJ)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mshowTotalStat(Lcom/android/server/ExtendMImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->showTotalStat()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mstartExtM(Lcom/android/server/ExtendMImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->startExtM()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mstopFlush(Lcom/android/server/ExtendMImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->stopFlush()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mtryToFlushPages(Lcom/android/server/ExtendMImpl;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->tryToFlushPages()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mtryToMarkPages(Lcom/android/server/ExtendMImpl;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->tryToMarkPages()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mtryToStopFlush(Lcom/android/server/ExtendMImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->tryToStopFlush()V

    return-void
.end method

.method static bridge synthetic -$$Nest$munregisterAlarmClock(Lcom/android/server/ExtendMImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->unregisterAlarmClock()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateState(Lcom/android/server/ExtendMImpl;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/ExtendMImpl;->updateState(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetFLUSH_DURATION()J
    .locals 2

    sget-wide v0, Lcom/android/server/ExtendMImpl;->FLUSH_DURATION:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$sfgetFLUSH_HIGH_LEVEL()I
    .locals 1

    sget v0, Lcom/android/server/ExtendMImpl;->FLUSH_HIGH_LEVEL:I

    return v0
.end method

.method static bridge synthetic -$$Nest$sfgetFLUSH_LOW_LEVEL()I
    .locals 1

    sget v0, Lcom/android/server/ExtendMImpl;->FLUSH_LOW_LEVEL:I

    return v0
.end method

.method static bridge synthetic -$$Nest$sfgetFLUSH_MEDIUM_LEVEL()I
    .locals 1

    sget v0, Lcom/android/server/ExtendMImpl;->FLUSH_MEDIUM_LEVEL:I

    return v0
.end method

.method static bridge synthetic -$$Nest$sfgetLOG_VERBOSE()Z
    .locals 1

    sget-boolean v0, Lcom/android/server/ExtendMImpl;->LOG_VERBOSE:Z

    return v0
.end method

.method static bridge synthetic -$$Nest$sfgetMARK_DURATION()J
    .locals 2

    sget-wide v0, Lcom/android/server/ExtendMImpl;->MARK_DURATION:J

    return-wide v0
.end method

.method static constructor <clinit>()V
    .locals 6

    .line 77
    const-string v0, "persist.miui.extm.debug_enable"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/ExtendMImpl;->LOG_VERBOSE:Z

    .line 80
    const-string v0, "persist.miui.extm.enable"

    const-string v2, "0"

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/ExtendMImpl;->EXTM_SETTINGS_PROP:Ljava/lang/String;

    .line 81
    const-string v0, "persist.sys.mfz.enable"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/ExtendMImpl;->MEMORY_FREEZE_ENABLE:Z

    .line 82
    const-string v0, "persist.miui.extm.dm_opt.enable"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/ExtendMImpl;->MIUI_DM_OPT_ENABLE:Z

    .line 87
    new-instance v0, Landroid/content/ComponentName;

    .line 88
    const-class v1, Lcom/android/server/ExtendMImpl;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android"

    invoke-direct {v0, v2, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/android/server/ExtendMImpl;->sExtendM:Landroid/content/ComponentName;

    .line 124
    const-string v0, "persist.lm.em.flush_level"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/server/ExtendMImpl;->FLUSH_LEVEL:I

    .line 126
    const-string v0, "persist.lm.em.mark_duration"

    const-wide/32 v2, 0x1d4c0

    invoke-static {v0, v2, v3}, Landroid/os/SystemProperties;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    sput-wide v2, Lcom/android/server/ExtendMImpl;->MARK_DURATION:J

    .line 128
    const-string v0, "persist.lm.em.flush_duration"

    const-wide/32 v2, 0x927c0

    invoke-static {v0, v2, v3}, Landroid/os/SystemProperties;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    sput-wide v2, Lcom/android/server/ExtendMImpl;->FLUSH_DURATION:J

    .line 130
    const-string v0, "persist.lm.em.delay_duration"

    const-wide/16 v2, 0x7530

    invoke-static {v0, v2, v3}, Landroid/os/SystemProperties;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    sput-wide v4, Lcom/android/server/ExtendMImpl;->DELAY_DURATION:J

    .line 132
    const-string v0, "persist.lm.em.flush_interval_limit"

    invoke-static {v0, v2, v3}, Landroid/os/SystemProperties;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    sput-wide v2, Lcom/android/server/ExtendMImpl;->FLUSH_INTERVAL_LIMIT:J

    .line 134
    const-string v0, "persist.lm.em.per_flush_quota_low"

    const/16 v2, 0x1388

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/server/ExtendMImpl;->PER_FLUSH_QUOTA_LOW:I

    .line 136
    const-string v0, "persist.lm.em.per_flush_quota_medium"

    const/16 v2, 0xbb8

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/server/ExtendMImpl;->PER_FLUSH_QUOTA_MEDIUM:I

    .line 138
    const-string v0, "persist.lm.em.per_flush_quota_high"

    const/16 v2, 0x7d0

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/server/ExtendMImpl;->PER_FLUSH_QUOTA_HIGH:I

    .line 140
    const-string v0, "persist.lm.em.flush_zram_usedrate"

    const/16 v2, 0x32

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/server/ExtendMImpl;->FLUSH_ZRAM_USEDRATE:I

    .line 142
    const-string v0, "persist.lm.em.flush_rwthreshold_part"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/server/ExtendMImpl;->FLUSH_RWTHRESHOLD_PART:I

    .line 162
    const-string v0, "persist.em.low_flush_level"

    const/16 v2, 0xa

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/server/ExtendMImpl;->FLUSH_LOW_LEVEL:I

    .line 164
    const-string v0, "persist.em.medium_flush_level"

    const/16 v2, 0x8

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/server/ExtendMImpl;->FLUSH_MEDIUM_LEVEL:I

    .line 166
    const-string v0, "persist.em.high_flush_level"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/server/ExtendMImpl;->FLUSH_HIGH_LEVEL:I

    .line 177
    const-string v0, "persist.miui.extm.memory_conf_file"

    const-string v1, "/system_ext/etc/perfinit_bdsize_zram.conf"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/ExtendMImpl;->MEM_CONF_FILE_PATH:Ljava/lang/String;

    .line 179
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/ExtendMImpl;->extmGear:Ljava/util/ArrayList;

    .line 182
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/ExtendMImpl;->sMemTags:Ljava/util/ArrayList;

    .line 184
    const-string v1, "SwapTotal"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 185
    sget-object v0, Lcom/android/server/ExtendMImpl;->sMemTags:Ljava/util/ArrayList;

    const-string v1, "SwapFree"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 186
    sget-object v0, Lcom/android/server/ExtendMImpl;->sMemTags:Ljava/util/ArrayList;

    const-string v1, "MemTotal"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 187
    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .line 189
    invoke-direct {p0}, Landroid/app/job/JobService;-><init>()V

    .line 108
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/ExtendMImpl;->mCurState:I

    .line 109
    iput-boolean v0, p0, Lcom/android/server/ExtendMImpl;->mLocalCloudEnable:Z

    .line 110
    iput-boolean v0, p0, Lcom/android/server/ExtendMImpl;->mfzLocalCloudEnable:Z

    .line 111
    iput-boolean v0, p0, Lcom/android/server/ExtendMImpl;->dmoptLocalCloudEnable:Z

    .line 112
    iput v0, p0, Lcom/android/server/ExtendMImpl;->mLastOldPages:I

    .line 113
    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/android/server/ExtendMImpl;->mTotalMarked:J

    .line 114
    iput-wide v1, p0, Lcom/android/server/ExtendMImpl;->mTotalFlushed:J

    .line 115
    iput-wide v1, p0, Lcom/android/server/ExtendMImpl;->lastFlushTime:J

    .line 116
    const/16 v3, 0xa

    iput v3, p0, Lcom/android/server/ExtendMImpl;->flush_level_adjust:I

    .line 117
    iput v0, p0, Lcom/android/server/ExtendMImpl;->flush_number_adjust:I

    .line 118
    iput v0, p0, Lcom/android/server/ExtendMImpl;->flush_per_quota:I

    .line 119
    iput v0, p0, Lcom/android/server/ExtendMImpl;->prev_bd_write:I

    .line 120
    iput v0, p0, Lcom/android/server/ExtendMImpl;->prev_bd_read:I

    .line 123
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, p0, Lcom/android/server/ExtendMImpl;->mMemInfo:Ljava/util/Map;

    .line 149
    iput-wide v1, p0, Lcom/android/server/ExtendMImpl;->lastReportMPtime:J

    .line 150
    iput v0, p0, Lcom/android/server/ExtendMImpl;->reportMemPressureCnt:I

    .line 151
    iput v0, p0, Lcom/android/server/ExtendMImpl;->kernelSupportCheckCnt:I

    .line 169
    new-instance v3, Landroid/os/HandlerThread;

    const-string v4, "ExtM"

    invoke-direct {v3, v4}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v3, p0, Lcom/android/server/ExtendMImpl;->mThread:Landroid/os/HandlerThread;

    .line 170
    iput-boolean v0, p0, Lcom/android/server/ExtendMImpl;->isInitSuccess:Z

    .line 171
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/android/server/ExtendMImpl;->mHandler:Lcom/android/server/ExtendMImpl$MyHandler;

    .line 180
    iput-boolean v0, p0, Lcom/android/server/ExtendMImpl;->hasParseMemConfSuccessful:Z

    .line 941
    iput-wide v1, p0, Lcom/android/server/ExtendMImpl;->mScreenOnTime:J

    .line 942
    iput-wide v1, p0, Lcom/android/server/ExtendMImpl;->mMarkStartTime:J

    .line 943
    iput-wide v1, p0, Lcom/android/server/ExtendMImpl;->mMarkDiffTime:J

    .line 944
    iput-wide v1, p0, Lcom/android/server/ExtendMImpl;->mFlushDiffTime:J

    .line 946
    new-instance v0, Lcom/android/server/ExtendMImpl$1;

    invoke-direct {v0, p0}, Lcom/android/server/ExtendMImpl$1;-><init>(Lcom/android/server/ExtendMImpl;)V

    iput-object v0, p0, Lcom/android/server/ExtendMImpl;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 1084
    new-instance v0, Lcom/android/server/ExtendMImpl$2;

    invoke-direct {v0, p0}, Lcom/android/server/ExtendMImpl$2;-><init>(Lcom/android/server/ExtendMImpl;)V

    iput-object v0, p0, Lcom/android/server/ExtendMImpl;->death:Landroid/os/IBinder$DeathRecipient;

    .line 1173
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/ExtendMImpl;->isFlushFinished:Z

    .line 190
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 192
    invoke-direct {p0}, Landroid/app/job/JobService;-><init>()V

    .line 108
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/ExtendMImpl;->mCurState:I

    .line 109
    iput-boolean v0, p0, Lcom/android/server/ExtendMImpl;->mLocalCloudEnable:Z

    .line 110
    iput-boolean v0, p0, Lcom/android/server/ExtendMImpl;->mfzLocalCloudEnable:Z

    .line 111
    iput-boolean v0, p0, Lcom/android/server/ExtendMImpl;->dmoptLocalCloudEnable:Z

    .line 112
    iput v0, p0, Lcom/android/server/ExtendMImpl;->mLastOldPages:I

    .line 113
    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/android/server/ExtendMImpl;->mTotalMarked:J

    .line 114
    iput-wide v1, p0, Lcom/android/server/ExtendMImpl;->mTotalFlushed:J

    .line 115
    iput-wide v1, p0, Lcom/android/server/ExtendMImpl;->lastFlushTime:J

    .line 116
    const/16 v3, 0xa

    iput v3, p0, Lcom/android/server/ExtendMImpl;->flush_level_adjust:I

    .line 117
    iput v0, p0, Lcom/android/server/ExtendMImpl;->flush_number_adjust:I

    .line 118
    iput v0, p0, Lcom/android/server/ExtendMImpl;->flush_per_quota:I

    .line 119
    iput v0, p0, Lcom/android/server/ExtendMImpl;->prev_bd_write:I

    .line 120
    iput v0, p0, Lcom/android/server/ExtendMImpl;->prev_bd_read:I

    .line 123
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, p0, Lcom/android/server/ExtendMImpl;->mMemInfo:Ljava/util/Map;

    .line 149
    iput-wide v1, p0, Lcom/android/server/ExtendMImpl;->lastReportMPtime:J

    .line 150
    iput v0, p0, Lcom/android/server/ExtendMImpl;->reportMemPressureCnt:I

    .line 151
    iput v0, p0, Lcom/android/server/ExtendMImpl;->kernelSupportCheckCnt:I

    .line 169
    new-instance v3, Landroid/os/HandlerThread;

    const-string v4, "ExtM"

    invoke-direct {v3, v4}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v3, p0, Lcom/android/server/ExtendMImpl;->mThread:Landroid/os/HandlerThread;

    .line 170
    iput-boolean v0, p0, Lcom/android/server/ExtendMImpl;->isInitSuccess:Z

    .line 171
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/android/server/ExtendMImpl;->mHandler:Lcom/android/server/ExtendMImpl$MyHandler;

    .line 180
    iput-boolean v0, p0, Lcom/android/server/ExtendMImpl;->hasParseMemConfSuccessful:Z

    .line 941
    iput-wide v1, p0, Lcom/android/server/ExtendMImpl;->mScreenOnTime:J

    .line 942
    iput-wide v1, p0, Lcom/android/server/ExtendMImpl;->mMarkStartTime:J

    .line 943
    iput-wide v1, p0, Lcom/android/server/ExtendMImpl;->mMarkDiffTime:J

    .line 944
    iput-wide v1, p0, Lcom/android/server/ExtendMImpl;->mFlushDiffTime:J

    .line 946
    new-instance v0, Lcom/android/server/ExtendMImpl$1;

    invoke-direct {v0, p0}, Lcom/android/server/ExtendMImpl$1;-><init>(Lcom/android/server/ExtendMImpl;)V

    iput-object v0, p0, Lcom/android/server/ExtendMImpl;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 1084
    new-instance v0, Lcom/android/server/ExtendMImpl$2;

    invoke-direct {v0, p0}, Lcom/android/server/ExtendMImpl$2;-><init>(Lcom/android/server/ExtendMImpl;)V

    iput-object v0, p0, Lcom/android/server/ExtendMImpl;->death:Landroid/os/IBinder$DeathRecipient;

    .line 1173
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/ExtendMImpl;->isFlushFinished:Z

    .line 193
    iput-object p1, p0, Lcom/android/server/ExtendMImpl;->mContext:Landroid/content/Context;

    .line 194
    return-void
.end method

.method private ParseMfzSettings()V
    .locals 4

    .line 346
    const-string v0, "Enter mfz cloud control"

    const-string v1, "MFZ"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 348
    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->getMfzCloud()I

    move-result v0

    .line 349
    .local v0, "mfzCloudEnable":I
    if-ltz v0, :cond_2

    const/4 v2, 0x1

    if-le v0, v2, :cond_0

    goto :goto_1

    .line 353
    :cond_0
    if-nez v0, :cond_1

    .line 354
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/server/ExtendMImpl;->mfzLocalCloudEnable:Z

    goto :goto_0

    .line 356
    :cond_1
    iput-boolean v2, p0, Lcom/android/server/ExtendMImpl;->mfzLocalCloudEnable:Z

    .line 358
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CloudEnable: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/server/ExtendMImpl;->mfzLocalCloudEnable:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", Exit mfz cloud control"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 359
    return-void

    .line 350
    :cond_2
    :goto_1
    const-string v2, "Invalid cloud control enable"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 351
    return-void
.end method

.method private ParsedmoptSettings()V
    .locals 4

    .line 363
    const-string v0, "Enter dmopt cloud control"

    const-string v1, "ExtM"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->getdmoptCloud()I

    move-result v0

    .line 366
    .local v0, "dmoptCloudEnable":I
    if-ltz v0, :cond_2

    const/4 v2, 0x1

    if-le v0, v2, :cond_0

    goto :goto_1

    .line 370
    :cond_0
    if-nez v0, :cond_1

    .line 371
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/server/ExtendMImpl;->dmoptLocalCloudEnable:Z

    goto :goto_0

    .line 373
    :cond_1
    iput-boolean v2, p0, Lcom/android/server/ExtendMImpl;->dmoptLocalCloudEnable:Z

    .line 375
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CloudEnable:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/server/ExtendMImpl;->dmoptLocalCloudEnable:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",Exit dmopt cloud control"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 376
    return-void

    .line 367
    :cond_2
    :goto_1
    const-string v2, "Invalid cloud control enable"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    return-void
.end method

.method private SetMfzEnable()V
    .locals 3

    .line 762
    :try_start_0
    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->getMfzCloud()I

    move-result v0

    .line 763
    .local v0, "MFZ_SYS_Control":I
    const-string v1, "persist.sys.mfz.enable"

    invoke-virtual {p0}, Lcom/android/server/ExtendMImpl;->isMfzCloud()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 764
    new-instance v1, Ljava/io/File;

    const-string v2, "/sys/block/zram0/mfz_enable"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/os/FileUtils;->stringToFile(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 767
    .end local v0    # "MFZ_SYS_Control":I
    goto :goto_0

    .line 765
    :catch_0
    move-exception v0

    .line 766
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "MFZ"

    const-string v2, "Failed to set mfz sys"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 768
    .end local v0    # "e":Ljava/io/IOException;
    :goto_0
    return-void
.end method

.method private SetdmoptEnable()V
    .locals 3

    .line 772
    :try_start_0
    sget-boolean v0, Lcom/android/server/ExtendMImpl;->MIUI_DM_OPT_ENABLE:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 773
    const-string v0, "persist.miui.extm.dm_opt.enable"

    invoke-virtual {p0}, Lcom/android/server/ExtendMImpl;->isdmoptCloud()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 777
    :cond_0
    goto :goto_0

    .line 775
    :catch_0
    move-exception v0

    .line 776
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "MFZ"

    const-string v2, "Failed to set dmopt sys"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 778
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private changeArrayListToFloatArray(Ljava/util/ArrayList;)[F
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Float;",
            ">;)[F"
        }
    .end annotation

    .line 657
    .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Float;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [F

    .line 658
    .local v0, "ret":[F
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 659
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    aput v2, v0, v1

    .line 658
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 661
    .end local v1    # "i":I
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "arrayList value is: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/util/Arrays;->toString([F)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ExtM"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 662
    return-object v0
.end method

.method private checkTime(JLjava/lang/String;J)V
    .locals 5
    .param p1, "startTime"    # J
    .param p3, "where"    # Ljava/lang/String;
    .param p4, "threshold"    # J

    .line 912
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 913
    .local v0, "now":J
    sub-long v2, v0, p1

    cmp-long v2, v2, p4

    if-lez v2, :cond_0

    .line 914
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Slow operation: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sub-long v3, v0, p1

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ms so far, now at "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ExtM"

    invoke-static {v3, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 916
    :cond_0
    return-void
.end method

.method private connect()V
    .locals 3

    .line 1094
    const-string/jumbo v0, "vold"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/ExtendMImpl;->binder:Landroid/os/IBinder;

    .line 1095
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/ExtendMImpl;->mVold:Landroid/os/IVold;

    if-nez v0, :cond_0

    .line 1097
    :try_start_0
    iget-object v0, p0, Lcom/android/server/ExtendMImpl;->binder:Landroid/os/IBinder;

    iget-object v1, p0, Lcom/android/server/ExtendMImpl;->death:Landroid/os/IBinder$DeathRecipient;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1100
    goto :goto_0

    .line 1098
    :catch_0
    move-exception v0

    .line 1099
    .local v0, "e":Landroid/os/RemoteException;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/server/ExtendMImpl;->binder:Landroid/os/IBinder;

    .line 1102
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/server/ExtendMImpl;->binder:Landroid/os/IBinder;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/ExtendMImpl;->mVold:Landroid/os/IVold;

    if-nez v0, :cond_1

    .line 1103
    const-string v0, "ExtM"

    const-string v1, "init vold"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1104
    iget-object v0, p0, Lcom/android/server/ExtendMImpl;->binder:Landroid/os/IBinder;

    invoke-static {v0}, Landroid/os/IVold$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/IVold;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/ExtendMImpl;->mVold:Landroid/os/IVold;

    .line 1106
    :cond_1
    return-void
.end method

.method private disconnect()V
    .locals 3

    .line 1109
    iget-object v0, p0, Lcom/android/server/ExtendMImpl;->binder:Landroid/os/IBinder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/ExtendMImpl;->mVold:Landroid/os/IVold;

    if-eqz v0, :cond_0

    .line 1110
    iget-object v0, p0, Lcom/android/server/ExtendMImpl;->binder:Landroid/os/IBinder;

    iget-object v1, p0, Lcom/android/server/ExtendMImpl;->death:Landroid/os/IBinder$DeathRecipient;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 1112
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/ExtendMImpl;->mVold:Landroid/os/IVold;

    .line 1113
    iput-object v0, p0, Lcom/android/server/ExtendMImpl;->binder:Landroid/os/IBinder;

    .line 1114
    return-void
.end method

.method private enableFlushQuota()V
    .locals 3

    .line 745
    :try_start_0
    new-instance v0, Ljava/io/File;

    const-string v1, "/sys/block/zram0/writeback_limit_enable"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v1, "1"

    invoke-static {v0, v1}, Landroid/os/FileUtils;->stringToFile(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 748
    goto :goto_0

    .line 746
    :catch_0
    move-exception v0

    .line 747
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "ExtM"

    const-string v2, "Failed to enable flush quota "

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 749
    .end local v0    # "e":Ljava/io/IOException;
    :goto_0
    return-void
.end method

.method private exit()V
    .locals 2

    .line 317
    iget-object v0, p0, Lcom/android/server/ExtendMImpl;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/server/ExtendMImpl;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 318
    const-string v0, "exit: unregister screen on/off monitor"

    const-string v1, "ExtM"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->disconnect()V

    .line 320
    const-string v0, "exit: disconnect vold"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 321
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/ExtendMImpl;->mCurState:I

    .line 322
    iput-boolean v0, p0, Lcom/android/server/ExtendMImpl;->isInitSuccess:Z

    .line 323
    return-void
.end method

.method private flushIsAllowed()Z
    .locals 7

    .line 881
    iget-wide v0, p0, Lcom/android/server/ExtendMImpl;->lastFlushTime:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    const/4 v1, 0x0

    const-string v2, "ExtM"

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    iget-wide v5, p0, Lcom/android/server/ExtendMImpl;->lastFlushTime:J

    sub-long/2addr v3, v5

    sget-wide v5, Lcom/android/server/ExtendMImpl;->FLUSH_INTERVAL_LIMIT:J

    cmp-long v0, v3, v5

    if-gez v0, :cond_0

    .line 882
    const-string v0, "runFlush  interval less than FLUSH_INTERVAL_LIMIT, skip flush"

    invoke-static {v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 883
    return v1

    .line 887
    :cond_0
    sget v0, Lcom/android/server/ExtendMImpl;->FLUSH_ZRAM_USEDRATE:I

    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->getZramUseRate()I

    move-result v3

    if-le v0, v3, :cond_1

    .line 888
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "zram userate is low, no need to flush: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->getZramUseRate()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 889
    return v1

    .line 891
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method private getCloudPercent()I
    .locals 4

    .line 207
    const/16 v0, 0x9

    .line 209
    .local v0, "percent":I
    :try_start_0
    iget-object v1, p0, Lcom/android/server/ExtendMImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "cloud_extm_percent"

    const/4 v3, -0x2

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 214
    goto :goto_0

    .line 211
    :catch_0
    move-exception v1

    .line 212
    .local v1, "e":Landroid/provider/Settings$SettingNotFoundException;
    const-string v2, "ExtM"

    const-string v3, "Do not get local cloud percent, set as 9"

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    const/16 v0, 0x9

    .line 215
    .end local v1    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :goto_0
    return v0
.end method

.method private getDataSizeGB()I
    .locals 9

    .line 615
    new-instance v0, Landroid/os/StatFs;

    const-string v1, "/data"

    invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 616
    .local v0, "statFs":Landroid/os/StatFs;
    invoke-virtual {v0}, Landroid/os/StatFs;->getTotalBytes()J

    move-result-wide v1

    const-wide/32 v3, 0x40000000

    div-long/2addr v1, v3

    .line 617
    .local v1, "flashTotal":J
    const-wide/16 v3, 0x10

    .line 618
    .local v3, "gap":J
    const-wide/16 v5, 0x20

    cmp-long v5, v1, v5

    const-wide/16 v6, 0x40

    if-lez v5, :cond_0

    cmp-long v5, v1, v6

    if-gtz v5, :cond_0

    .line 619
    const-wide/16 v3, 0x20

    goto :goto_0

    .line 620
    :cond_0
    cmp-long v5, v1, v6

    const-wide/16 v6, 0x100

    if-lez v5, :cond_1

    cmp-long v5, v1, v6

    if-gtz v5, :cond_1

    .line 621
    const-wide/16 v3, 0x40

    goto :goto_0

    .line 622
    :cond_1
    cmp-long v5, v1, v6

    const-wide/16 v6, 0x400

    if-lez v5, :cond_2

    cmp-long v5, v1, v6

    if-gtz v5, :cond_2

    .line 623
    const-wide/16 v3, 0x80

    goto :goto_0

    .line 624
    :cond_2
    cmp-long v5, v1, v6

    if-lez v5, :cond_3

    .line 625
    const-wide/16 v3, 0x100

    .line 627
    :cond_3
    :goto_0
    div-long v5, v1, v3

    const-wide/16 v7, 0x1

    add-long/2addr v5, v7

    mul-long/2addr v5, v3

    long-to-int v5, v5

    return v5
.end method

.method private getDefaultExtMGear(II)[F
    .locals 5
    .param p1, "memSize"    # I
    .param p2, "dataSize"    # I

    .line 643
    const/4 v0, 0x3

    new-array v0, v0, [F

    .line 644
    .local v0, "ret":[F
    const/16 v1, 0x8

    const/4 v2, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-lt p1, v1, :cond_0

    const/16 v1, 0x80

    if-lt p2, v1, :cond_0

    .line 645
    const/high16 v1, 0x40800000    # 4.0f

    aput v1, v0, v4

    .line 646
    const/high16 v1, 0x40c00000    # 6.0f

    aput v1, v0, v3

    .line 647
    const/high16 v1, 0x41000000    # 8.0f

    aput v1, v0, v2

    goto :goto_0

    .line 649
    :cond_0
    const/high16 v1, 0x3f800000    # 1.0f

    aput v1, v0, v4

    .line 650
    const/high16 v1, 0x40000000    # 2.0f

    aput v1, v0, v3

    .line 651
    const/high16 v1, 0x40400000    # 3.0f

    aput v1, v0, v2

    .line 653
    :goto_0
    return-object v0
.end method

.method private static getExtendMRecordInstance()Lcom/android/server/ExtendMImpl$ExtendMRecord;
    .locals 1

    .line 300
    sget-object v0, Lcom/android/server/ExtendMImpl;->sExtendMRecord:Lcom/android/server/ExtendMImpl$ExtendMRecord;

    if-nez v0, :cond_0

    .line 301
    new-instance v0, Lcom/android/server/ExtendMImpl$ExtendMRecord;

    invoke-direct {v0}, Lcom/android/server/ExtendMImpl$ExtendMRecord;-><init>()V

    sput-object v0, Lcom/android/server/ExtendMImpl;->sExtendMRecord:Lcom/android/server/ExtendMImpl$ExtendMRecord;

    .line 303
    :cond_0
    sget-object v0, Lcom/android/server/ExtendMImpl;->sExtendMRecord:Lcom/android/server/ExtendMImpl$ExtendMRecord;

    return-object v0
.end method

.method private getFlushedPages()I
    .locals 3

    .line 469
    :try_start_0
    new-instance v0, Ljava/io/File;

    const-string v1, "/sys/block/zram0/bd_stat"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v1, ""

    .line 470
    const/16 v2, 0x80

    invoke-static {v0, v2, v1}, Landroid/os/FileUtils;->readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 471
    .local v0, "wbStats":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const-string v2, "\\s+"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    aget-object v1, v1, v2

    const/16 v2, 0xa

    invoke-static {v1, v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return v1

    .line 472
    .end local v0    # "wbStats":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 473
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "ExtM"

    const-string v2, "Failed to read flushed page count"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 475
    .end local v0    # "e":Ljava/io/IOException;
    const/4 v0, -0x1

    return v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/android/server/ExtendMImpl;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .line 307
    if-nez p0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 308
    :cond_0
    const-class v0, Lcom/android/server/ExtendMImpl;

    monitor-enter v0

    .line 309
    :try_start_0
    sget-object v1, Lcom/android/server/ExtendMImpl;->sSelf:Lcom/android/server/ExtendMImpl;

    if-nez v1, :cond_1

    .line 310
    new-instance v1, Lcom/android/server/ExtendMImpl;

    invoke-direct {v1, p0}, Lcom/android/server/ExtendMImpl;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/android/server/ExtendMImpl;->sSelf:Lcom/android/server/ExtendMImpl;

    .line 312
    :cond_1
    sget-object v1, Lcom/android/server/ExtendMImpl;->sSelf:Lcom/android/server/ExtendMImpl;

    monitor-exit v0

    return-object v1

    .line 313
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private getMemSizeGB()I
    .locals 8

    .line 631
    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->readMemInfo()V

    .line 632
    iget-object v0, p0, Lcom/android/server/ExtendMImpl;->mMemInfo:Ljava/util/Map;

    const-string v1, "MemTotal"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/32 v2, 0x100000

    div-long/2addr v0, v2

    .line 633
    .local v0, "memTotal":J
    const-wide/16 v2, 0x1

    .line 634
    .local v2, "gap":J
    const-wide/16 v4, 0x4

    cmp-long v4, v0, v4

    const-wide/16 v5, 0x8

    if-lez v4, :cond_0

    cmp-long v4, v0, v5

    if-gtz v4, :cond_0

    .line 635
    const-wide/16 v2, 0x2

    goto :goto_0

    .line 636
    :cond_0
    cmp-long v4, v0, v5

    if-lez v4, :cond_1

    .line 637
    const-wide/16 v2, 0x4

    .line 639
    :cond_1
    :goto_0
    div-long v4, v0, v2

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    mul-long/2addr v4, v2

    long-to-int v4, v4

    return v4
.end method

.method private getMfzCloud()I
    .locals 4

    .line 219
    const/4 v0, 0x1

    .line 221
    .local v0, "mfzcloud":I
    :try_start_0
    iget-object v1, p0, Lcom/android/server/ExtendMImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "cloud_memFreeze_control"

    const/4 v3, -0x2

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 226
    goto :goto_0

    .line 223
    :catch_0
    move-exception v1

    .line 224
    .local v1, "e":Landroid/provider/Settings$SettingNotFoundException;
    const-string v2, "MFZ"

    const-string v3, "continue mfz cloud control"

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    const/4 v0, 0x1

    .line 227
    .end local v1    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :goto_0
    return v0
.end method

.method private getPagesNeedFlush(I)V
    .locals 9
    .param p1, "flushLevel"    # I

    .line 424
    const-string v0, "ExtM"

    const/4 v1, 0x0

    iput v1, p0, Lcom/android/server/ExtendMImpl;->flush_number_adjust:I

    .line 425
    const/16 v2, 0xa

    iput v2, p0, Lcom/android/server/ExtendMImpl;->flush_level_adjust:I

    .line 427
    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->isFlushthrashing()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 428
    sget p1, Lcom/android/server/ExtendMImpl;->FLUSH_LOW_LEVEL:I

    .line 429
    sget v2, Lcom/android/server/ExtendMImpl;->PER_FLUSH_QUOTA_LOW:I

    iput v2, p0, Lcom/android/server/ExtendMImpl;->flush_per_quota:I

    .line 433
    :cond_0
    :try_start_0
    new-instance v2, Ljava/io/File;

    const-string v3, "/sys/block/zram0/idle_stat"

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v3, ""

    invoke-static {v2, v1, v3}, Landroid/os/FileUtils;->readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 434
    .local v1, "idlePages":Ljava/lang/String;
    if-nez v1, :cond_1

    return-void

    .line 435
    :cond_1
    const-string v2, "\\s+"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 436
    .local v2, "pages":[Ljava/lang/String;
    const/4 v3, 0x0

    .line 437
    .local v3, "pagesCount":I
    const/4 v4, 0x0

    .local v4, "i":I
    const/4 v5, 0x0

    .line 438
    .local v5, "tmpNum":I
    const/4 v6, 0x0

    .line 440
    .local v6, "current_idle_pages":I
    array-length v7, v2

    add-int/lit8 v7, v7, -0x1

    .end local v4    # "i":I
    .local v7, "i":I
    :goto_0
    add-int/lit8 v4, p1, -0x1

    if-lt v7, v4, :cond_2

    if-ltz v7, :cond_2

    .line 441
    aget-object v4, v2, v7

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v6, v4

    .line 440
    add-int/lit8 v7, v7, -0x1

    goto :goto_0

    .line 444
    :cond_2
    array-length v4, v2

    add-int/lit8 v4, v4, -0x1

    .end local v7    # "i":I
    .restart local v4    # "i":I
    :goto_1
    add-int/lit8 v7, p1, -0x1

    if-lt v4, v7, :cond_4

    if-ltz v4, :cond_4

    .line 445
    aget-object v7, v2, v4

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    move v5, v7

    .line 446
    if-lez v5, :cond_3

    .line 447
    add-int/2addr v3, v5

    .line 448
    iget v7, p0, Lcom/android/server/ExtendMImpl;->flush_per_quota:I

    if-le v3, v7, :cond_3

    .line 449
    goto :goto_2

    .line 444
    :cond_3
    add-int/lit8 v4, v4, -0x1

    goto :goto_1

    .line 453
    :cond_4
    :goto_2
    add-int/lit8 v7, p1, -0x1

    if-le v4, v7, :cond_5

    .line 454
    add-int/lit8 v7, v4, 0x1

    iput v7, p0, Lcom/android/server/ExtendMImpl;->flush_level_adjust:I

    goto :goto_3

    .line 456
    :cond_5
    iput p1, p0, Lcom/android/server/ExtendMImpl;->flush_level_adjust:I

    .line 458
    :goto_3
    iget v7, p0, Lcom/android/server/ExtendMImpl;->flush_per_quota:I

    if-le v3, v7, :cond_6

    goto :goto_4

    :cond_6
    move v7, v3

    :goto_4
    iput v7, p0, Lcom/android/server/ExtendMImpl;->flush_number_adjust:I

    .line 460
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "can_flush_idle_pages:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " flush_number_adjust: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/android/server/ExtendMImpl;->flush_number_adjust:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 463
    nop

    .end local v1    # "idlePages":Ljava/lang/String;
    .end local v2    # "pages":[Ljava/lang/String;
    .end local v3    # "pagesCount":I
    .end local v4    # "i":I
    .end local v5    # "tmpNum":I
    .end local v6    # "current_idle_pages":I
    goto :goto_5

    .line 461
    :catch_0
    move-exception v1

    .line 462
    .local v1, "e":Ljava/io/IOException;
    const-string v2, "Failed to get pages need to flush"

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 464
    .end local v1    # "e":Ljava/io/IOException;
    :goto_5
    return-void
.end method

.method private getQuotaLeft()I
    .locals 3

    .line 730
    :try_start_0
    new-instance v0, Ljava/io/File;

    const-string v1, "/sys/block/zram0/writeback_limit"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v1, ""

    .line 731
    const/4 v2, 0x0

    invoke-static {v0, v2, v1}, Landroid/os/FileUtils;->readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 732
    .local v0, "remainingPages":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return v1

    .line 733
    .end local v0    # "remainingPages":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 734
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "ExtM"

    const-string v2, "Failed to get remaining pages"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 736
    .end local v0    # "e":Ljava/io/IOException;
    const/4 v0, -0x1

    return v0
.end method

.method private getReadBackRate()Ljava/lang/String;
    .locals 11

    .line 480
    :try_start_0
    new-instance v0, Ljava/io/File;

    const-string v1, "/sys/block/zram0/bd_stat"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v1, ""

    .line 481
    const/16 v2, 0x80

    invoke-static {v0, v2, v1}, Landroid/os/FileUtils;->readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 482
    .local v0, "wbStats":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const-string v2, "\\s+"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 485
    .local v1, "wbStat":[Ljava/lang/String;
    new-instance v2, Ljava/math/BigDecimal;

    const/4 v3, 0x1

    aget-object v4, v1, v3

    invoke-direct {v2, v4}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 486
    .local v2, "b1":Ljava/math/BigDecimal;
    new-instance v4, Ljava/math/BigDecimal;

    const/4 v5, 0x2

    aget-object v6, v1, v5

    invoke-direct {v4, v6}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 487
    .local v4, "b2":Ljava/math/BigDecimal;
    invoke-virtual {v4}, Ljava/math/BigDecimal;->intValue()I

    move-result v6

    if-nez v6, :cond_0

    const-string v3, "Failed to get read back rate"

    return-object v3

    .line 488
    :cond_0
    const/4 v6, 0x4

    invoke-virtual {v2, v4, v5, v6}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;II)Ljava/math/BigDecimal;

    move-result-object v5

    invoke-virtual {v5}, Ljava/math/BigDecimal;->doubleValue()D

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    .line 489
    .local v5, "result":Ljava/lang/Double;
    const-string v6, "%.2f%%"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v7

    const-wide/high16 v9, 0x4059000000000000L    # 100.0

    mul-double/2addr v7, v9

    invoke-static {v7, v8}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    const/4 v8, 0x0

    aput-object v7, v3, v8

    invoke-static {v6, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v3

    .line 490
    .end local v0    # "wbStats":Ljava/lang/String;
    .end local v1    # "wbStat":[Ljava/lang/String;
    .end local v2    # "b1":Ljava/math/BigDecimal;
    .end local v4    # "b2":Ljava/math/BigDecimal;
    .end local v5    # "result":Ljava/lang/Double;
    :catch_0
    move-exception v0

    .line 491
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "ExtM"

    const-string v2, "Failed to read flushed page count"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 493
    .end local v0    # "e":Ljava/io/IOException;
    const-string v0, " calculate read back rate error"

    return-object v0
.end method

.method private getRefinedUUID()I
    .locals 5

    .line 382
    iget-object v0, p0, Lcom/android/server/ExtendMImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 383
    .local v0, "cr":Landroid/content/ContentResolver;
    const-string v1, "extm_uuid"

    invoke-static {v0, v1}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 384
    .local v2, "uuid":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 385
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    .line 386
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 389
    :cond_0
    const/4 v1, 0x0

    .line 390
    .local v1, "sum":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v3, v4, :cond_1

    .line 391
    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    add-int/lit8 v4, v4, -0x30

    add-int/2addr v1, v4

    .line 390
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 393
    .end local v3    # "i":I
    :cond_1
    rem-int/lit8 v3, v1, 0xa

    add-int/lit8 v3, v3, 0xa

    rem-int/lit8 v3, v3, 0xa

    return v3
.end method

.method private declared-synchronized getZramUseRate()I
    .locals 9

    monitor-enter p0

    .line 569
    const/4 v0, 0x0

    .line 570
    .local v0, "zramUsedRate":I
    const-wide/16 v1, 0x0

    .line 571
    .local v1, "swapTotal":J
    const-wide/16 v3, 0x0

    .line 573
    .local v3, "swapFree":J
    :try_start_0
    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->readMemInfo()V

    .line 575
    iget-object v5, p0, Lcom/android/server/ExtendMImpl;->mMemInfo:Ljava/util/Map;

    const/4 v6, 0x0

    if-eqz v5, :cond_3

    const-string v7, "SwapTotal"

    invoke-interface {v5, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/android/server/ExtendMImpl;->mMemInfo:Ljava/util/Map;

    const-string v7, "SwapFree"

    invoke-interface {v5, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-nez v5, :cond_0

    goto :goto_0

    .line 578
    :cond_0
    iget-object v5, p0, Lcom/android/server/ExtendMImpl;->mMemInfo:Ljava/util/Map;

    const-string v7, "SwapTotal"

    invoke-interface {v5, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    move-wide v1, v7

    .line 579
    iget-object v5, p0, Lcom/android/server/ExtendMImpl;->mMemInfo:Ljava/util/Map;

    const-string v7, "SwapFree"

    invoke-interface {v5, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-wide v3, v7

    .line 582
    const-wide/16 v7, 0x0

    cmp-long v5, v1, v7

    if-nez v5, :cond_1

    .line 583
    monitor-exit p0

    return v6

    .line 585
    :cond_1
    sub-long v5, v1, v3

    const-wide/16 v7, 0x64

    mul-long/2addr v5, v7

    :try_start_1
    div-long/2addr v5, v1

    long-to-int v0, v5

    .line 586
    sget-boolean v5, Lcom/android/server/ExtendMImpl;->LOG_VERBOSE:Z

    if-eqz v5, :cond_2

    .line 587
    const-string v5, "ExtM"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getZramUseRate zramUsedRate:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 588
    .end local p0    # "this":Lcom/android/server/ExtendMImpl;
    :cond_2
    monitor-exit p0

    return v0

    .line 576
    :cond_3
    :goto_0
    monitor-exit p0

    return v6

    .line 568
    .end local v0    # "zramUsedRate":I
    .end local v1    # "swapTotal":J
    .end local v3    # "swapFree":J
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private getdmoptCloud()I
    .locals 4

    .line 231
    const/4 v0, 0x1

    .line 233
    .local v0, "dmoptcloud":I
    :try_start_0
    iget-object v1, p0, Lcom/android/server/ExtendMImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "cloud_dm_opt_enable"

    const/4 v3, -0x2

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 238
    goto :goto_0

    .line 235
    :catch_0
    move-exception v1

    .line 236
    .local v1, "e":Landroid/provider/Settings$SettingNotFoundException;
    const-string v2, "ExtM"

    const-string v3, "continue dmopt cloud control"

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    const/4 v0, 0x1

    .line 239
    .end local v1    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :goto_0
    return v0
.end method

.method private init()V
    .locals 5

    .line 280
    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->connect()V

    .line 281
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/ExtendMImpl;->mMarkStartTime:J

    .line 282
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/ExtendMImpl;->mScreenOnTime:J

    .line 283
    invoke-static {}, Lcom/android/server/ExtendMImpl;->getExtendMRecordInstance()Lcom/android/server/ExtendMImpl$ExtendMRecord;

    .line 285
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 286
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 287
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 288
    const-string v1, "miui.extm.action.mark"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 289
    iget-object v1, p0, Lcom/android/server/ExtendMImpl;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/ExtendMImpl;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 290
    const-string v1, "ExtM"

    const-string v2, "init: register mark monitor"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->enableFlushQuota()V

    .line 293
    iget-object v1, p0, Lcom/android/server/ExtendMImpl;->mContext:Landroid/content/Context;

    const-string v2, "jobscheduler"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/job/JobScheduler;

    .line 294
    .local v1, "js":Landroid/app/job/JobScheduler;
    new-instance v2, Landroid/app/job/JobInfo$Builder;

    const v3, 0x82f4

    sget-object v4, Lcom/android/server/ExtendMImpl;->sExtendM:Landroid/content/ComponentName;

    invoke-direct {v2, v3, v4}, Landroid/app/job/JobInfo$Builder;-><init>(ILandroid/content/ComponentName;)V

    invoke-virtual {v2}, Landroid/app/job/JobInfo$Builder;->build()Landroid/app/job/JobInfo;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/job/JobScheduler;->schedule(Landroid/app/job/JobInfo;)I

    .line 295
    const/4 v2, 0x1

    iput v2, p0, Lcom/android/server/ExtendMImpl;->mCurState:I

    .line 296
    iput-boolean v2, p0, Lcom/android/server/ExtendMImpl;->isInitSuccess:Z

    .line 297
    return-void
.end method

.method private isFlushablePagesExist(II)Z
    .locals 7
    .param p1, "flush_level"    # I
    .param p2, "flush_count_least"    # I

    .line 857
    const-string v0, "ExtM"

    sget v1, Lcom/android/server/ExtendMImpl;->FLUSH_HIGH_LEVEL:I

    const/4 v2, 0x0

    if-lt p1, v1, :cond_4

    sget v1, Lcom/android/server/ExtendMImpl;->FLUSH_LOW_LEVEL:I

    if-le p1, v1, :cond_0

    goto :goto_2

    .line 860
    :cond_0
    :try_start_0
    new-instance v1, Ljava/io/File;

    const-string v3, "/sys/block/zram0/idle_stat"

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v3, ""

    invoke-static {v1, v2, v3}, Landroid/os/FileUtils;->readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 861
    .local v1, "idlePages":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 862
    return v2

    .line 864
    :cond_1
    const-string v3, "\\s+"

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 865
    .local v3, "pages":[Ljava/lang/String;
    const/4 v4, 0x0

    .line 866
    .local v4, "pagesCount":I
    add-int/lit8 v5, p1, -0x1

    .local v5, "i":I
    :goto_0
    array-length v6, v3

    if-ge v5, v6, :cond_2

    .line 867
    aget-object v6, v3, v5

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v4, v6

    .line 866
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 869
    .end local v5    # "i":I
    :cond_2
    if-ge v4, p2, :cond_3

    .line 870
    const-string/jumbo v5, "the number of flushable pages is relatively few"

    invoke-static {v0, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 871
    return v2

    .line 875
    .end local v1    # "idlePages":Ljava/lang/String;
    .end local v3    # "pages":[Ljava/lang/String;
    .end local v4    # "pagesCount":I
    :cond_3
    goto :goto_1

    .line 873
    :catch_0
    move-exception v1

    .line 874
    .local v1, "e":Ljava/io/IOException;
    const-string v2, "Failed to get pages need to flush"

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 876
    .end local v1    # "e":Ljava/io/IOException;
    :goto_1
    const/4 v0, 0x1

    return v0

    .line 858
    :cond_4
    :goto_2
    return v2
.end method

.method private isFlushthrashing()Z
    .locals 9

    .line 531
    const-string v0, "ExtM"

    const/4 v1, 0x0

    .line 532
    .local v1, "cur_bd_write":I
    const/4 v2, 0x0

    .line 533
    .local v2, "cur_bd_read":I
    const/4 v3, 0x0

    .line 534
    .local v3, "bdRWRate":I
    const/4 v4, 0x0

    .line 536
    .local v4, "bThrashing":Z
    :try_start_0
    new-instance v5, Ljava/io/File;

    const-string v6, "/sys/block/zram0/bd_stat"

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v6, ""

    .line 537
    const/16 v7, 0x80

    invoke-static {v5, v7, v6}, Landroid/os/FileUtils;->readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 538
    .local v5, "wbStats":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    const-string v7, "\\s+"

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 540
    .local v6, "wbStat":[Ljava/lang/String;
    const/4 v7, 0x1

    aget-object v7, v6, v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    move v2, v7

    .line 541
    const/4 v7, 0x2

    aget-object v7, v6, v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    move v1, v7

    .line 542
    if-eqz v1, :cond_2

    if-nez v2, :cond_0

    goto :goto_0

    .line 546
    :cond_0
    iget v7, p0, Lcom/android/server/ExtendMImpl;->prev_bd_write:I

    sub-int v8, v1, v7

    if-lez v8, :cond_1

    .line 547
    iget v8, p0, Lcom/android/server/ExtendMImpl;->prev_bd_read:I

    sub-int v8, v2, v8

    mul-int/lit8 v8, v8, 0x64

    sub-int v7, v1, v7

    div-int/2addr v8, v7

    move v3, v8

    .line 548
    sget v7, Lcom/android/server/ExtendMImpl;->FLUSH_RWTHRESHOLD_PART:I

    if-le v3, v7, :cond_1

    .line 549
    const-string v7, "ExtM flush exist thrashing"

    invoke-static {v0, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 550
    const/4 v0, 0x1

    move v4, v0

    .line 561
    .end local v5    # "wbStats":Ljava/lang/String;
    .end local v6    # "wbStat":[Ljava/lang/String;
    :cond_1
    goto :goto_1

    .line 543
    .restart local v5    # "wbStats":Ljava/lang/String;
    .restart local v6    # "wbStat":[Ljava/lang/String;
    :cond_2
    :goto_0
    return v4

    .line 559
    .end local v5    # "wbStats":Ljava/lang/String;
    .end local v6    # "wbStat":[Ljava/lang/String;
    :catch_0
    move-exception v5

    .line 560
    .local v5, "e":Ljava/io/IOException;
    const-string v6, "Failed to read bd_stat"

    invoke-static {v0, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 562
    .end local v5    # "e":Ljava/io/IOException;
    :goto_1
    iput v2, p0, Lcom/android/server/ExtendMImpl;->prev_bd_read:I

    .line 563
    iput v1, p0, Lcom/android/server/ExtendMImpl;->prev_bd_write:I

    .line 565
    return v4
.end method

.method private isKernelSupported()Z
    .locals 3

    .line 256
    :try_start_0
    new-instance v0, Ljava/io/File;

    const-string v1, "/sys/block/zram0/backing_dev"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v1, ""

    const/16 v2, 0x80

    invoke-static {v0, v2, v1}, Landroid/os/FileUtils;->readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 257
    .local v0, "backingDev":Ljava/lang/String;
    const-string v1, "none"

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    .line 258
    const/4 v1, 0x1

    return v1

    .line 262
    .end local v0    # "backingDev":Ljava/lang/String;
    :cond_0
    goto :goto_0

    .line 260
    :catch_0
    move-exception v0

    .line 261
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "ExtM"

    const-string v2, "Failed to read kernel info"

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    .end local v0    # "e":Ljava/io/IOException;
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method private isMfzSupported()Z
    .locals 4

    .line 267
    new-instance v0, Ljava/io/File;

    const-string v1, "/sys/block/zram0/mfz_enable"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 268
    .local v0, "MFZ_sys":Ljava/io/File;
    const/4 v1, 0x1

    .line 269
    .local v1, "MfzSup":Z
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    const-string v3, "MFZ"

    if-nez v2, :cond_0

    .line 270
    const/4 v1, 0x0

    .line 271
    const-string v2, "Do not supported MFZ"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 273
    :cond_0
    const-string v2, "Supported MFZ"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    :goto_0
    return v1
.end method

.method private markPagesAsNew()V
    .locals 0

    .line 741
    return-void
.end method

.method private pagesToMb(J)J
    .locals 2
    .param p1, "pageCnt"    # J

    .line 420
    const-wide/16 v0, 0x100

    div-long v0, p1, v0

    return-wide v0
.end method

.method private parseExtMemCloudControl()V
    .locals 6

    .line 397
    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->getCloudPercent()I

    move-result v0

    .line 399
    .local v0, "cloudPercent":I
    const/4 v1, 0x0

    const-string v2, "ExtM"

    if-ltz v0, :cond_3

    const/16 v3, 0xa

    if-lt v0, v3, :cond_0

    goto :goto_2

    .line 404
    :cond_0
    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->getRefinedUUID()I

    move-result v3

    .line 405
    .local v3, "thres":I
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ExtMemCloudControl value is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", thres is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 411
    const/16 v2, 0x9

    if-eq v0, v2, :cond_2

    if-le v0, v3, :cond_1

    goto :goto_0

    .line 414
    :cond_1
    iput-boolean v1, p0, Lcom/android/server/ExtendMImpl;->mLocalCloudEnable:Z

    goto :goto_1

    .line 412
    :cond_2
    :goto_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/server/ExtendMImpl;->mLocalCloudEnable:Z

    .line 416
    :goto_1
    return-void

    .line 400
    .end local v3    # "thres":I
    :cond_3
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid clound control value: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", must be in the range [0,9]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 401
    iput-boolean v1, p0, Lcom/android/server/ExtendMImpl;->mLocalCloudEnable:Z

    .line 402
    return-void
.end method

.method private declared-synchronized readMemInfo()V
    .locals 11

    monitor-enter p0

    .line 497
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/android/server/ExtendMImpl;->mMemInfo:Ljava/util/Map;

    .line 498
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 499
    .local v0, "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskReads()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_6

    .line 501
    .local v1, "oldPolicy":Landroid/os/StrictMode$ThreadPolicy;
    :try_start_1
    new-instance v2, Ljava/io/FileReader;

    const-string v3, "/proc/meminfo"

    invoke-direct {v2, v3}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_4

    .line 503
    .local v2, "fileReader":Ljava/io/FileReader;
    :try_start_2
    new-instance v3, Ljava/io/BufferedReader;

    invoke-direct {v3, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 505
    .local v3, "reader":Ljava/io/BufferedReader;
    const/4 v4, 0x0

    .line 506
    .local v4, "line":Ljava/lang/String;
    :goto_0
    :try_start_3
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-object v4, v5

    if-eqz v5, :cond_1

    .line 508
    :try_start_4
    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 509
    .local v5, "items":[Ljava/lang/String;
    sget-object v6, Lcom/android/server/ExtendMImpl;->sMemTags:Ljava/util/ArrayList;

    const/4 v7, 0x0

    aget-object v8, v5, v7

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 510
    aget-object v6, v5, v7

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    const/4 v8, 0x1

    aget-object v9, v5, v8

    aget-object v8, v5, v8

    const-string v10, "k"

    invoke-virtual {v8, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v9, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v0, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 514
    .end local v5    # "items":[Ljava/lang/String;
    :cond_0
    goto :goto_0

    .line 518
    .end local v4    # "line":Ljava/lang/String;
    :catchall_0
    move-exception v4

    goto :goto_1

    .line 512
    .restart local v4    # "line":Ljava/lang/String;
    :catch_0
    move-exception v5

    .line 518
    .local v5, "e":Ljava/lang/Exception;
    :try_start_5
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 521
    :try_start_6
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_5

    .line 526
    :try_start_7
    invoke-static {v1}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_6

    .line 513
    monitor-exit p0

    return-void

    .line 516
    .end local v5    # "e":Ljava/lang/Exception;
    .end local p0    # "this":Lcom/android/server/ExtendMImpl;
    :cond_1
    :try_start_8
    iput-object v0, p0, Lcom/android/server/ExtendMImpl;->mMemInfo:Ljava/util/Map;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 518
    .end local v4    # "line":Ljava/lang/String;
    :try_start_9
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 519
    nop

    .line 521
    .end local v3    # "reader":Ljava/io/BufferedReader;
    :try_start_a
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_5

    .line 522
    nop

    .line 526
    .end local v2    # "fileReader":Ljava/io/FileReader;
    :try_start_b
    invoke-static {v1}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_6

    .line 527
    goto :goto_4

    .line 523
    :catch_1
    move-exception v2

    goto :goto_3

    .line 521
    .restart local v2    # "fileReader":Ljava/io/FileReader;
    :catchall_1
    move-exception v3

    goto :goto_2

    .line 518
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catchall_2
    move-exception v4

    :goto_1
    :try_start_c
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V

    .line 519
    nop

    .end local v0    # "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
    .end local v1    # "oldPolicy":Landroid/os/StrictMode$ThreadPolicy;
    .end local v2    # "fileReader":Ljava/io/FileReader;
    throw v4
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    .line 521
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v0    # "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
    .restart local v1    # "oldPolicy":Landroid/os/StrictMode$ThreadPolicy;
    .restart local v2    # "fileReader":Ljava/io/FileReader;
    .restart local p0    # "this":Lcom/android/server/ExtendMImpl;
    :catchall_3
    move-exception v3

    :goto_2
    :try_start_d
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V

    .line 522
    nop

    .end local v0    # "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
    .end local v1    # "oldPolicy":Landroid/os/StrictMode$ThreadPolicy;
    .end local p0    # "this":Lcom/android/server/ExtendMImpl;
    throw v3
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_2
    .catchall {:try_start_d .. :try_end_d} :catchall_4

    .line 526
    .end local v2    # "fileReader":Ljava/io/FileReader;
    .restart local v0    # "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
    .restart local v1    # "oldPolicy":Landroid/os/StrictMode$ThreadPolicy;
    .restart local p0    # "this":Lcom/android/server/ExtendMImpl;
    :catchall_4
    move-exception v2

    goto :goto_5

    .line 523
    :catch_2
    move-exception v2

    .line 524
    .local v2, "e":Ljava/lang/Exception;
    :goto_3
    :try_start_e
    const-string v3, "ExtM"

    const-string v4, "Cannot readMemInfo from /proc/meminfo"

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_5

    .line 526
    .end local v2    # "e":Ljava/lang/Exception;
    :try_start_f
    invoke-static {v1}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_6

    .line 527
    nop

    .line 528
    :goto_4
    monitor-exit p0

    return-void

    .line 526
    .end local p0    # "this":Lcom/android/server/ExtendMImpl;
    :catchall_5
    move-exception v2

    :goto_5
    :try_start_10
    invoke-static {v1}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 527
    throw v2
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_6

    .line 496
    .end local v0    # "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
    .end local v1    # "oldPolicy":Landroid/os/StrictMode$ThreadPolicy;
    :catchall_6
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private readerJsonFromFile(Ljava/io/File;)Ljava/lang/String;
    .locals 8
    .param p1, "file"    # Ljava/io/File;

    .line 592
    const/4 v0, 0x0

    .line 593
    .local v0, "jsonStr":Ljava/lang/String;
    const/4 v1, 0x0

    if-nez p1, :cond_0

    .line 594
    return-object v1

    .line 597
    :cond_0
    :try_start_0
    new-instance v2, Ljava/io/FileReader;

    invoke-direct {v2, p1}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    .line 598
    .local v2, "fileReader":Ljava/io/FileReader;
    new-instance v3, Ljava/io/InputStreamReader;

    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const-string v5, "Utf-8"

    invoke-direct {v3, v4, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 599
    .local v3, "reader":Ljava/io/Reader;
    const/4 v4, 0x0

    .line 600
    .local v4, "ch":I
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    .line 601
    .local v5, "sb":Ljava/lang/StringBuffer;
    :goto_0
    invoke-virtual {v3}, Ljava/io/Reader;->read()I

    move-result v6

    move v4, v6

    const/4 v7, -0x1

    if-eq v6, v7, :cond_1

    .line 602
    int-to-char v6, v4

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 604
    :cond_1
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V

    .line 605
    invoke-virtual {v3}, Ljava/io/Reader;->close()V

    .line 606
    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 610
    .end local v2    # "fileReader":Ljava/io/FileReader;
    .end local v3    # "reader":Ljava/io/Reader;
    .end local v4    # "ch":I
    .end local v5    # "sb":Ljava/lang/StringBuffer;
    nop

    .line 611
    return-object v0

    .line 607
    :catch_0
    move-exception v2

    .line 608
    .local v2, "e":Ljava/io/IOException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "file read error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "ExtM"

    invoke-static {v4, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 609
    return-object v1
.end method

.method private refreshExtmSettings()V
    .locals 2

    .line 326
    const-string v0, "Enter cloud control"

    const-string v1, "ExtM"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 328
    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->parseExtMemCloudControl()V

    .line 329
    invoke-virtual {p0}, Lcom/android/server/ExtendMImpl;->isCloudAllowed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 330
    iget v0, p0, Lcom/android/server/ExtendMImpl;->mCurState:I

    if-nez v0, :cond_0

    .line 331
    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->init()V

    goto :goto_0

    .line 333
    :cond_0
    const-string v0, "Already running"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 336
    :cond_1
    iget v0, p0, Lcom/android/server/ExtendMImpl;->mCurState:I

    if-eqz v0, :cond_2

    .line 337
    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->exit()V

    goto :goto_0

    .line 339
    :cond_2
    const-string v0, "Already stopped"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    :goto_0
    const-string v0, "Exit cloud control"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    return-void
.end method

.method private registerAlarmClock(J)V
    .locals 5
    .param p1, "starttime"    # J

    .line 919
    iget-object v0, p0, Lcom/android/server/ExtendMImpl;->mContext:Landroid/content/Context;

    const-string v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 920
    .local v0, "alarmManager":Landroid/app/AlarmManager;
    if-eqz v0, :cond_0

    .line 921
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 922
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "miui.extm.action.mark"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 923
    iget-object v3, p0, Lcom/android/server/ExtendMImpl;->mContext:Landroid/content/Context;

    .line 924
    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    .line 923
    const/high16 v4, 0x4000000

    invoke-static {v3, v2, v1, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 925
    .local v2, "pendingIntent":Landroid/app/PendingIntent;
    invoke-virtual {v0, v2}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 926
    const/4 v3, 0x0

    invoke-virtual {v0, v3, p1, p2, v2}, Landroid/app/AlarmManager;->setExactAndAllowWhileIdle(IJLandroid/app/PendingIntent;)V

    .line 928
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "pendingIntent":Landroid/app/PendingIntent;
    :cond_0
    return-void
.end method

.method private reportLowMemPressureLimit(I)Z
    .locals 5
    .param p1, "pressureState"    # I

    .line 895
    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    .line 896
    return v0

    .line 898
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/android/server/ExtendMImpl;->lastReportMPtime:J

    sub-long/2addr v1, v3

    const-wide/16 v3, 0x7530

    cmp-long v1, v1, v3

    const/4 v2, 0x0

    if-ltz v1, :cond_1

    .line 899
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/android/server/ExtendMImpl;->lastReportMPtime:J

    .line 900
    iput v2, p0, Lcom/android/server/ExtendMImpl;->reportMemPressureCnt:I

    .line 902
    :cond_1
    iget v1, p0, Lcom/android/server/ExtendMImpl;->reportMemPressureCnt:I

    const/4 v3, 0x3

    if-lt v1, v3, :cond_2

    .line 903
    return v2

    .line 906
    :cond_2
    add-int/2addr v1, v0

    iput v1, p0, Lcom/android/server/ExtendMImpl;->reportMemPressureCnt:I

    .line 907
    return v0
.end method

.method private static resetNextFlushQuota(Landroid/content/Context;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .line 781
    invoke-static {}, Lcom/android/server/ExtendMImpl;->tomorrowMidnight()Ljava/util/Calendar;

    move-result-object v0

    .line 782
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    sub-long/2addr v1, v3

    .line 783
    .local v1, "timeToMidnight":J
    const-string v3, "jobscheduler"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/job/JobScheduler;

    .line 784
    .local v3, "js":Landroid/app/job/JobScheduler;
    new-instance v4, Landroid/app/job/JobInfo$Builder;

    const v5, 0x82f4

    sget-object v6, Lcom/android/server/ExtendMImpl;->sExtendM:Landroid/content/ComponentName;

    invoke-direct {v4, v5, v6}, Landroid/app/job/JobInfo$Builder;-><init>(ILandroid/content/ComponentName;)V

    .line 785
    invoke-virtual {v4, v1, v2}, Landroid/app/job/JobInfo$Builder;->setMinimumLatency(J)Landroid/app/job/JobInfo$Builder;

    move-result-object v4

    .line 786
    invoke-virtual {v4}, Landroid/app/job/JobInfo$Builder;->build()Landroid/app/job/JobInfo;

    move-result-object v4

    .line 784
    invoke-virtual {v3, v4}, Landroid/app/job/JobScheduler;->schedule(Landroid/app/job/JobInfo;)I

    .line 787
    return-void
.end method

.method private runFlush(I)Z
    .locals 9
    .param p1, "flushLevel"    # I

    .line 1177
    invoke-direct {p0, p1}, Lcom/android/server/ExtendMImpl;->getPagesNeedFlush(I)V

    .line 1178
    iget v0, p0, Lcom/android/server/ExtendMImpl;->flush_number_adjust:I

    const/4 v1, 0x0

    const-string v2, "ExtM"

    if-gtz v0, :cond_0

    .line 1179
    const-string v0, "no pages need to flush"

    invoke-static {v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1180
    return v1

    .line 1183
    :cond_0
    sget v0, Lcom/android/server/ExtendMImpl;->FLUSH_LOW_LEVEL:I

    const-string v3, " pages "

    const-string v4, " Need Flush: "

    const-string v5, "MFZ"

    if-ne p1, v0, :cond_1

    .line 1184
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Report MEM_PRESSURE_LOW!  Flush Level is: "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v4, p0, Lcom/android/server/ExtendMImpl;->flush_number_adjust:I

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1185
    :cond_1
    sget v0, Lcom/android/server/ExtendMImpl;->FLUSH_MEDIUM_LEVEL:I

    if-ne p1, v0, :cond_2

    .line 1186
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Report MEM_PRESSURE_MEDIUM!  Flush Level is: "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v4, p0, Lcom/android/server/ExtendMImpl;->flush_number_adjust:I

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1188
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Report MEM_PRESSURE_HIGH!  Flush Level is: "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v4, p0, Lcom/android/server/ExtendMImpl;->flush_number_adjust:I

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1191
    :goto_0
    :try_start_0
    new-instance v0, Ljava/io/File;

    const-string v3, "/sys/block/zram0/idle_stat"

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v3, ""

    .line 1192
    const/16 v4, 0x80

    invoke-static {v0, v4, v3}, Landroid/os/FileUtils;->readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1193
    .local v0, "before_idle_stats":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "before Flush idle_stats: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1196
    nop

    .end local v0    # "before_idle_stats":Ljava/lang/String;
    goto :goto_1

    .line 1194
    :catch_0
    move-exception v0

    .line 1195
    .local v0, "e":Ljava/io/IOException;
    const-string v3, "before Flush idle_stats: error"

    invoke-static {v5, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1198
    .end local v0    # "e":Ljava/io/IOException;
    :goto_1
    iput-boolean v1, p0, Lcom/android/server/ExtendMImpl;->isFlushFinished:Z

    .line 1199
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Flush thread start , "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, Lcom/android/server/ExtendMImpl;->flush_number_adjust:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " pages need to flush"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1201
    const/4 v0, 0x1

    :try_start_1
    const-class v3, Landroid/os/IVold;

    const-string v4, "runExtMFlush"

    const/4 v5, 0x3

    new-array v6, v5, [Ljava/lang/Class;

    sget-object v7, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v7, v6, v1

    sget-object v7, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v7, v6, v0

    const-class v7, Landroid/os/IVoldTaskListener;

    const/4 v8, 0x2

    aput-object v7, v6, v8

    invoke-virtual {v3, v4, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 1202
    .local v3, "method":Ljava/lang/reflect/Method;
    iget-object v4, p0, Lcom/android/server/ExtendMImpl;->mVold:Landroid/os/IVold;

    new-array v5, v5, [Ljava/lang/Object;

    iget v6, p0, Lcom/android/server/ExtendMImpl;->flush_number_adjust:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    iget v1, p0, Lcom/android/server/ExtendMImpl;->flush_level_adjust:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v0

    new-instance v1, Lcom/android/server/ExtendMImpl$6;

    invoke-direct {v1, p0}, Lcom/android/server/ExtendMImpl$6;-><init>(Lcom/android/server/ExtendMImpl;)V

    aput-object v1, v5, v8

    invoke-virtual {v3, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1243
    nop

    .end local v3    # "method":Ljava/lang/reflect/Method;
    goto :goto_2

    .line 1240
    :catch_1
    move-exception v1

    .line 1241
    .local v1, "e":Ljava/lang/Exception;
    invoke-static {v2, v1}, Landroid/util/Slog;->wtf(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1242
    const-string v3, "Failed to runExtmFlushPages"

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1244
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_2
    return v0
.end method

.method private runMark()Z
    .locals 7

    .line 1248
    const-string v0, "Mark thread start "

    const-string v1, "ExtM"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1250
    const/4 v0, 0x1

    :try_start_0
    const-class v2, Landroid/os/IVold;

    const-string v3, "runExtMMark"

    new-array v4, v0, [Ljava/lang/Class;

    const-class v5, Landroid/os/IVoldTaskListener;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 1251
    .local v2, "method":Ljava/lang/reflect/Method;
    iget-object v3, p0, Lcom/android/server/ExtendMImpl;->mVold:Landroid/os/IVold;

    new-array v4, v0, [Ljava/lang/Object;

    new-instance v5, Lcom/android/server/ExtendMImpl$7;

    invoke-direct {v5, p0}, Lcom/android/server/ExtendMImpl$7;-><init>(Lcom/android/server/ExtendMImpl;)V

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1264
    nop

    .end local v2    # "method":Ljava/lang/reflect/Method;
    goto :goto_0

    .line 1261
    :catch_0
    move-exception v2

    .line 1262
    .local v2, "e":Ljava/lang/Exception;
    invoke-static {v1, v2}, Landroid/util/Slog;->wtf(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1263
    const-string v3, "Failed to runExtmMarkPages"

    invoke-static {v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1265
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_0
    return v0
.end method

.method private saveFlushData(ILjava/lang/String;J)V
    .locals 10
    .param p1, "writeCount"    # I
    .param p2, "mReadBackRate"    # Ljava/lang/String;
    .param p3, "startTime"    # J

    .line 1307
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 1308
    .local v0, "now":J
    sub-long v8, v0, p3

    .line 1309
    .local v8, "costTime":J
    sget-object v2, Lcom/android/server/ExtendMImpl;->sExtendMRecord:Lcom/android/server/ExtendMImpl$ExtendMRecord;

    int-to-long v3, p1

    invoke-direct {p0, v3, v4}, Lcom/android/server/ExtendMImpl;->pagesToMb(J)J

    move-result-wide v3

    long-to-int v4, v3

    move v3, p1

    move-object v5, p2

    move-wide v6, v8

    invoke-virtual/range {v2 .. v7}, Lcom/android/server/ExtendMImpl$ExtendMRecord;->setFlushRecord(IILjava/lang/String;J)V

    .line 1310
    return-void
.end method

.method private setFlushQuota()V
    .locals 3

    .line 753
    :try_start_0
    const-string v0, "persist.miui.extm.daily_flush_count"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 754
    .local v0, "WriteBackLimit":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    const-string v2, "/sys/block/zram0/writeback_limit"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v0}, Landroid/os/FileUtils;->stringToFile(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 757
    .end local v0    # "WriteBackLimit":Ljava/lang/String;
    goto :goto_0

    .line 755
    :catch_0
    move-exception v0

    .line 756
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "ExtM"

    const-string v2, "Failed to set flush quota"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 758
    .end local v0    # "e":Ljava/io/IOException;
    :goto_0
    return-void
.end method

.method private showAndSaveFlushedStat(IJ)V
    .locals 8
    .param p1, "prevCount"    # I
    .param p2, "startTime"    # J

    .line 790
    const/4 v0, -0x1

    const-string v1, "ExtM"

    if-eq p1, v0, :cond_0

    .line 791
    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->getFlushedPages()I

    move-result v0

    sub-int/2addr v0, p1

    .line 792
    .local v0, "writeCount":I
    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->getReadBackRate()Ljava/lang/String;

    move-result-object v2

    .line 793
    .local v2, "mReadBackRate":Ljava/lang/String;
    invoke-direct {p0, v0, v2, p2, p3}, Lcom/android/server/ExtendMImpl;->saveFlushData(ILjava/lang/String;J)V

    .line 794
    iget-wide v3, p0, Lcom/android/server/ExtendMImpl;->mTotalFlushed:J

    int-to-long v5, v0

    add-long/2addr v3, v5

    iput-wide v3, p0, Lcom/android/server/ExtendMImpl;->mTotalFlushed:J

    .line 795
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    int-to-long v4, v0

    invoke-direct {p0, v4, v5}, Lcom/android/server/ExtendMImpl;->pagesToMb(J)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " m) pages flushed"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 797
    .end local v0    # "writeCount":I
    .end local v2    # "mReadBackRate":Ljava/lang/String;
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/ExtendMImpl;->mLastOldPages:I

    .line 798
    const-string v5, "flush pages"

    const-wide/16 v6, 0x3e8

    move-object v2, p0

    move-wide v3, p2

    invoke-direct/range {v2 .. v7}, Lcom/android/server/ExtendMImpl;->checkTime(JLjava/lang/String;J)V

    .line 799
    const-string v0, "After flushing"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 800
    sget-boolean v0, Lcom/android/server/ExtendMImpl;->LOG_VERBOSE:Z

    if-eqz v0, :cond_1

    .line 801
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Quota left "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->getQuotaLeft()I

    move-result v2

    int-to-long v2, v2

    invoke-direct {p0, v2, v3}, Lcom/android/server/ExtendMImpl;->pagesToMb(J)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "m"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 803
    :cond_1
    return-void
.end method

.method private showTotalStat()V
    .locals 6

    .line 806
    const-string v0, ""

    const-string v1, "Total stat:"

    const-string v2, "ExtM"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 807
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "  total marked "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v3, p0, Lcom/android/server/ExtendMImpl;->mTotalMarked:J

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v4, p0, Lcom/android/server/ExtendMImpl;->mTotalMarked:J

    invoke-direct {p0, v4, v5}, Lcom/android/server/ExtendMImpl;->pagesToMb(J)J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " m), total flushed "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v4, p0, Lcom/android/server/ExtendMImpl;->mTotalFlushed:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v3, p0, Lcom/android/server/ExtendMImpl;->mTotalFlushed:J

    .line 808
    invoke-direct {p0, v3, v4}, Lcom/android/server/ExtendMImpl;->pagesToMb(J)J

    move-result-wide v3

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " m)"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 807
    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 810
    const/16 v1, 0x80

    :try_start_0
    new-instance v3, Ljava/io/File;

    const-string v4, "/sys/block/zram0/bd_stat"

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 811
    invoke-static {v3, v1, v0}, Landroid/os/FileUtils;->readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 812
    .local v3, "wbStats":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "  wb stat: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 815
    nop

    .end local v3    # "wbStats":Ljava/lang/String;
    goto :goto_0

    .line 813
    :catch_0
    move-exception v3

    .line 814
    .local v3, "e":Ljava/io/IOException;
    const-string v4, "  wb stat: error"

    invoke-static {v2, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 817
    .end local v3    # "e":Ljava/io/IOException;
    :goto_0
    :try_start_1
    new-instance v3, Ljava/io/File;

    const-string v4, "/sys/block/zram0/mm_stat"

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 818
    invoke-static {v3, v1, v0}, Landroid/os/FileUtils;->readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 819
    .local v0, "mm_stats":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "  mm stat: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 822
    nop

    .end local v0    # "mm_stats":Ljava/lang/String;
    goto :goto_1

    .line 820
    :catch_1
    move-exception v0

    .line 821
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "  mm  stat: error"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 823
    .end local v0    # "e":Ljava/io/IOException;
    :goto_1
    return-void
.end method

.method private startExtM()V
    .locals 7

    .line 1117
    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->isKernelSupported()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1118
    sget-object v0, Lcom/android/server/ExtendMImpl;->EXTM_SETTINGS_PROP:Ljava/lang/String;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1120
    new-instance v0, Lcom/android/server/ExtendMImpl$3;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/server/ExtendMImpl$3;-><init>(Lcom/android/server/ExtendMImpl;Landroid/os/Handler;)V

    .line 1127
    .local v0, "observer":Landroid/database/ContentObserver;
    iget-object v2, p0, Lcom/android/server/ExtendMImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 1128
    const-string v3, "cloud_extm_percent"

    invoke-static {v3}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 1127
    const/4 v4, 0x0

    const/4 v5, -0x2

    invoke-virtual {v2, v3, v4, v0, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 1130
    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->parseExtMemCloudControl()V

    .line 1131
    invoke-virtual {p0}, Lcom/android/server/ExtendMImpl;->isCloudAllowed()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1132
    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->init()V

    .line 1134
    :cond_0
    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->isMfzSupported()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1136
    new-instance v2, Lcom/android/server/ExtendMImpl$4;

    invoke-direct {v2, p0, v1}, Lcom/android/server/ExtendMImpl$4;-><init>(Lcom/android/server/ExtendMImpl;Landroid/os/Handler;)V

    .line 1144
    .local v2, "mfzobsever":Landroid/database/ContentObserver;
    iget-object v3, p0, Lcom/android/server/ExtendMImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 1145
    const-string v6, "cloud_memFreeze_control"

    invoke-static {v6}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 1144
    invoke-virtual {v3, v6, v4, v2, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 1147
    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->ParseMfzSettings()V

    .line 1148
    invoke-virtual {p0}, Lcom/android/server/ExtendMImpl;->isMfzCloud()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1149
    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->SetMfzEnable()V

    .line 1152
    .end local v2    # "mfzobsever":Landroid/database/ContentObserver;
    :cond_1
    new-instance v2, Lcom/android/server/ExtendMImpl$5;

    invoke-direct {v2, p0, v1}, Lcom/android/server/ExtendMImpl$5;-><init>(Lcom/android/server/ExtendMImpl;Landroid/os/Handler;)V

    move-object v1, v2

    .line 1159
    .local v1, "dmoptobsever":Landroid/database/ContentObserver;
    iget-object v2, p0, Lcom/android/server/ExtendMImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 1160
    const-string v3, "cloud_dm_opt_enable"

    invoke-static {v3}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 1159
    invoke-virtual {v2, v3, v4, v1, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 1162
    .end local v0    # "observer":Landroid/database/ContentObserver;
    .end local v1    # "dmoptobsever":Landroid/database/ContentObserver;
    goto :goto_0

    .line 1164
    :cond_2
    iget v0, p0, Lcom/android/server/ExtendMImpl;->kernelSupportCheckCnt:I

    const/16 v1, 0x1e

    if-ge v0, v1, :cond_3

    .line 1165
    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/ExtendMImpl;->kernelSupportCheckCnt:I

    .line 1166
    iget-object v0, p0, Lcom/android/server/ExtendMImpl;->mHandler:Lcom/android/server/ExtendMImpl$MyHandler;

    const/4 v1, 0x5

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/ExtendMImpl$MyHandler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 1168
    :cond_3
    const-string v0, "ExtM"

    const-string/jumbo v1, "the number of times has reached the upper limit, skip"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1171
    :cond_4
    :goto_0
    return-void
.end method

.method private stopFlush()V
    .locals 7

    .line 1269
    const-string v0, "-----------------------"

    const-string v1, "ExtM"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1271
    :try_start_0
    const-class v0, Landroid/os/IVold;

    const-string/jumbo v2, "stopExtMFlush"

    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/Class;

    const-class v5, Landroid/os/IVoldTaskListener;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    invoke-virtual {v0, v2, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 1272
    .local v0, "method":Ljava/lang/reflect/Method;
    iget-object v2, p0, Lcom/android/server/ExtendMImpl;->mVold:Landroid/os/IVold;

    new-array v3, v3, [Ljava/lang/Object;

    new-instance v4, Lcom/android/server/ExtendMImpl$8;

    invoke-direct {v4, p0}, Lcom/android/server/ExtendMImpl$8;-><init>(Lcom/android/server/ExtendMImpl;)V

    aput-object v4, v3, v6

    invoke-virtual {v0, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1292
    nop

    .end local v0    # "method":Ljava/lang/reflect/Method;
    goto :goto_0

    .line 1289
    :catch_0
    move-exception v0

    .line 1290
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v1, v0}, Landroid/util/Slog;->wtf(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1291
    const-string v2, "Failed to stopExtmFlushPages"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1293
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private static tomorrowMidnight()Ljava/util/Calendar;
    .locals 3

    .line 1296
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 1297
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1298
    const/16 v1, 0xb

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 1299
    const/16 v1, 0xc

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 1300
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 1301
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 1302
    const/4 v1, 0x5

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 1303
    return-object v0
.end method

.method private tryToFlushPages()Z
    .locals 2

    .line 839
    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->getQuotaLeft()I

    move-result v0

    if-gtz v0, :cond_1

    .line 840
    sget-boolean v0, Lcom/android/server/ExtendMImpl;->LOG_VERBOSE:Z

    if-eqz v0, :cond_0

    const-string v0, "ExtM"

    const-string v1, "Total flush pages over limit, exit"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 841
    :cond_0
    const/4 v0, 0x0

    return v0

    .line 843
    :cond_1
    sget v0, Lcom/android/server/ExtendMImpl;->FLUSH_LEVEL:I

    invoke-direct {p0, v0}, Lcom/android/server/ExtendMImpl;->runFlush(I)Z

    .line 844
    const/4 v0, 0x1

    return v0
.end method

.method private tryToMarkPages()Z
    .locals 9

    .line 826
    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->getQuotaLeft()I

    move-result v0

    const-string v1, "ExtM"

    if-gtz v0, :cond_1

    .line 827
    sget-boolean v0, Lcom/android/server/ExtendMImpl;->LOG_VERBOSE:Z

    if-eqz v0, :cond_0

    const-string v0, "Total flush pages over limit, exit"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 828
    :cond_0
    const/4 v0, 0x0

    return v0

    .line 830
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v7

    .line 832
    .local v7, "startTime":J
    const-string v0, "Start marking ..."

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 833
    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->runMark()Z

    .line 834
    const-string v4, "mark pages"

    const-wide/16 v5, 0x64

    move-object v1, p0

    move-wide v2, v7

    invoke-direct/range {v1 .. v6}, Lcom/android/server/ExtendMImpl;->checkTime(JLjava/lang/String;J)V

    .line 835
    const/4 v0, 0x1

    return v0
.end method

.method private tryToStopFlush()V
    .locals 0

    .line 848
    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->stopFlush()V

    .line 849
    return-void
.end method

.method private unregisterAlarmClock()V
    .locals 5

    .line 931
    iget-object v0, p0, Lcom/android/server/ExtendMImpl;->mContext:Landroid/content/Context;

    const-string v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 932
    .local v0, "alarmManager":Landroid/app/AlarmManager;
    if-eqz v0, :cond_0

    .line 933
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 934
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "miui.extm.action.mark"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 935
    iget-object v3, p0, Lcom/android/server/ExtendMImpl;->mContext:Landroid/content/Context;

    .line 936
    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    .line 935
    const/high16 v4, 0x4000000

    invoke-static {v3, v2, v1, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 937
    .local v2, "pendingIntent":Landroid/app/PendingIntent;
    invoke-virtual {v0, v2}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 939
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "pendingIntent":Landroid/app/PendingIntent;
    :cond_0
    return-void
.end method

.method private updateState(I)V
    .locals 2
    .param p1, "newState"    # I

    .line 852
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "State update : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/ExtendMImpl;->mCurState:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " -> "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ExtM"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 853
    iput p1, p0, Lcom/android/server/ExtendMImpl;->mCurState:I

    .line 854
    return-void
.end method


# virtual methods
.method public dump(Lcom/android/internal/util/IndentingPrintWriter;)V
    .locals 4
    .param p1, "pw"    # Lcom/android/internal/util/IndentingPrintWriter;

    .line 1315
    :try_start_0
    sget-object v0, Lcom/android/server/ExtendMImpl;->sExtendMRecord:Lcom/android/server/ExtendMImpl$ExtendMRecord;

    invoke-virtual {v0}, Lcom/android/server/ExtendMImpl$ExtendMRecord;->getSingleRecord()Ljava/util/LinkedList;

    move-result-object v0

    .line 1316
    .local v0, "records":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/String;>;"
    sget-object v1, Lcom/android/server/ExtendMImpl;->sExtendMRecord:Lcom/android/server/ExtendMImpl$ExtendMRecord;

    invoke-virtual {v1}, Lcom/android/server/ExtendMImpl$ExtendMRecord;->getDailyRecords()Ljava/util/LinkedList;

    move-result-object v1

    .line 1317
    .local v1, "dailyRecords":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/String;>;"
    const-string v2, "ExtM details:"

    invoke-virtual {p1, v2}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    .line 1318
    invoke-virtual {p1}, Lcom/android/internal/util/IndentingPrintWriter;->increaseIndent()Lcom/android/internal/util/IndentingPrintWriter;

    .line 1319
    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1320
    .local v3, "record":Ljava/lang/String;
    invoke-virtual {p1, v3}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    .line 1321
    .end local v3    # "record":Ljava/lang/String;
    goto :goto_0

    .line 1322
    :cond_0
    invoke-virtual {p1}, Lcom/android/internal/util/IndentingPrintWriter;->decreaseIndent()Lcom/android/internal/util/IndentingPrintWriter;

    .line 1323
    invoke-virtual {p1}, Lcom/android/internal/util/IndentingPrintWriter;->println()V

    .line 1324
    const-string v2, "ExtM daily details:"

    invoke-virtual {p1, v2}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    .line 1325
    invoke-virtual {p1}, Lcom/android/internal/util/IndentingPrintWriter;->increaseIndent()Lcom/android/internal/util/IndentingPrintWriter;

    .line 1326
    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1327
    .local v3, "dailyRecord":Ljava/lang/String;
    invoke-virtual {p1, v3}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    .line 1328
    .end local v3    # "dailyRecord":Ljava/lang/String;
    goto :goto_1

    .line 1329
    :cond_1
    invoke-virtual {p1}, Lcom/android/internal/util/IndentingPrintWriter;->decreaseIndent()Lcom/android/internal/util/IndentingPrintWriter;

    .line 1330
    invoke-virtual {p1}, Lcom/android/internal/util/IndentingPrintWriter;->println()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1333
    .end local v0    # "records":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/String;>;"
    .end local v1    # "dailyRecords":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/String;>;"
    goto :goto_2

    .line 1331
    :catch_0
    move-exception v0

    .line 1332
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "ExtM not init, nothing to dump"

    invoke-virtual {p1, v1}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    .line 1334
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_2
    return-void
.end method

.method public getExtMGear()[F
    .locals 20

    .line 666
    move-object/from16 v1, p0

    sget-object v0, Lcom/android/server/ExtendMImpl;->extmGear:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const-string v2, "ExtM"

    if-lez v0, :cond_0

    iget-boolean v0, v1, Lcom/android/server/ExtendMImpl;->hasParseMemConfSuccessful:Z

    if-eqz v0, :cond_0

    .line 667
    const-string v0, "extmGear exist"

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 668
    sget-object v0, Lcom/android/server/ExtendMImpl;->extmGear:Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Lcom/android/server/ExtendMImpl;->changeArrayListToFloatArray(Ljava/util/ArrayList;)[F

    move-result-object v0

    return-object v0

    .line 671
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/android/server/ExtendMImpl;->getDataSizeGB()I

    move-result v3

    .line 672
    .local v3, "flashSizeGB":I
    invoke-direct/range {p0 .. p0}, Lcom/android/server/ExtendMImpl;->getMemSizeGB()I

    move-result v4

    .line 673
    .local v4, "memSizeGB":I
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "flashSize(GB): "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ", memSize(GB):"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 675
    new-instance v0, Ljava/io/File;

    sget-object v5, Lcom/android/server/ExtendMImpl;->MEM_CONF_FILE_PATH:Ljava/lang/String;

    invoke-direct {v0, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object v5, v0

    .line 676
    .local v5, "jsonFile":Ljava/io/File;
    invoke-direct {v1, v5}, Lcom/android/server/ExtendMImpl;->readerJsonFromFile(Ljava/io/File;)Ljava/lang/String;

    move-result-object v6

    .line 677
    .local v6, "jsonData":Ljava/lang/String;
    if-nez v6, :cond_1

    .line 678
    const-string v0, "No json data."

    invoke-static {v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 679
    invoke-direct {v1, v4, v3}, Lcom/android/server/ExtendMImpl;->getDefaultExtMGear(II)[F

    move-result-object v0

    return-object v0

    .line 682
    :cond_1
    :try_start_0
    sget-object v0, Lcom/android/server/ExtendMImpl;->extmGear:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 683
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, v6}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    move-object v7, v0

    .line 685
    .local v7, "parse":Lorg/json/JSONObject;
    const-string v0, "auto_zram"

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    move-object v8, v0

    .line 686
    .local v8, "autoZramItems":Lorg/json/JSONArray;
    const/4 v0, 0x0

    move v9, v0

    .local v9, "i":I
    :goto_0
    invoke-virtual {v8}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-ge v9, v0, :cond_6

    .line 687
    invoke-virtual {v8, v9}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    move-object v10, v0

    .line 688
    .local v10, "autoZramItem":Lorg/json/JSONObject;
    const-string v0, "auto_zram_flash"

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    move-object v11, v0

    .line 689
    .local v11, "autoZramFlashItems":Lorg/json/JSONArray;
    const/4 v0, 0x0

    .line 691
    .local v0, "getFlashSize":Z
    const/4 v12, 0x0

    .local v12, "count":I
    :goto_1
    invoke-virtual {v11}, Lorg/json/JSONArray;->length()I

    move-result v13
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_4

    if-ge v12, v13, :cond_3

    .line 692
    :try_start_1
    invoke-virtual {v11, v12}, Lorg/json/JSONArray;->getInt(I)I

    move-result v13
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    if-ne v13, v3, :cond_2

    .line 693
    const/4 v0, 0x1

    .line 694
    move v12, v0

    goto :goto_2

    .line 691
    :cond_2
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    .line 713
    .end local v0    # "getFlashSize":Z
    .end local v7    # "parse":Lorg/json/JSONObject;
    .end local v8    # "autoZramItems":Lorg/json/JSONArray;
    .end local v9    # "i":I
    .end local v10    # "autoZramItem":Lorg/json/JSONObject;
    .end local v11    # "autoZramFlashItems":Lorg/json/JSONArray;
    .end local v12    # "count":I
    :catch_0
    move-exception v0

    move-object/from16 v18, v5

    move-object/from16 v19, v6

    goto/16 :goto_5

    .line 691
    .restart local v0    # "getFlashSize":Z
    .restart local v7    # "parse":Lorg/json/JSONObject;
    .restart local v8    # "autoZramItems":Lorg/json/JSONArray;
    .restart local v9    # "i":I
    .restart local v10    # "autoZramItem":Lorg/json/JSONObject;
    .restart local v11    # "autoZramFlashItems":Lorg/json/JSONArray;
    .restart local v12    # "count":I
    :cond_3
    move v12, v0

    .line 698
    .end local v0    # "getFlashSize":Z
    .local v12, "getFlashSize":Z
    :goto_2
    if-eqz v12, :cond_5

    .line 699
    :try_start_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "auto_zram_ram_"

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v13, "G"

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v13, v0

    .line 700
    .local v13, "autoZramRamTag":Ljava/lang/String;
    invoke-virtual {v10, v13}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    move-object v14, v0

    .line 701
    .local v14, "autoZramRamTagItems":Lorg/json/JSONObject;
    invoke-virtual {v14}, Lorg/json/JSONObject;->keySet()Ljava/util/Set;

    move-result-object v0

    move-object v15, v0

    .line 702
    .local v15, "keys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v15}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :goto_3
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_4

    move-object/from16 v17, v0

    .line 705
    .local v17, "key":Ljava/lang/String;
    :try_start_3
    invoke-static/range {v17 .. v17}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_4

    .line 708
    .local v0, "value":F
    nop

    .line 709
    move-object/from16 v18, v5

    .end local v5    # "jsonFile":Ljava/io/File;
    .local v18, "jsonFile":Ljava/io/File;
    :try_start_4
    sget-object v5, Lcom/android/server/ExtendMImpl;->extmGear:Ljava/util/ArrayList;
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_2

    move-object/from16 v19, v6

    .end local v6    # "jsonData":Ljava/lang/String;
    .local v19, "jsonData":Ljava/lang/String;
    :try_start_5
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_1

    .line 710
    move-object/from16 v5, v18

    move-object/from16 v6, v19

    .end local v0    # "value":F
    .end local v17    # "key":Ljava/lang/String;
    goto :goto_3

    .line 713
    .end local v7    # "parse":Lorg/json/JSONObject;
    .end local v8    # "autoZramItems":Lorg/json/JSONArray;
    .end local v9    # "i":I
    .end local v10    # "autoZramItem":Lorg/json/JSONObject;
    .end local v11    # "autoZramFlashItems":Lorg/json/JSONArray;
    .end local v12    # "getFlashSize":Z
    .end local v13    # "autoZramRamTag":Ljava/lang/String;
    .end local v14    # "autoZramRamTagItems":Lorg/json/JSONObject;
    .end local v15    # "keys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :catch_1
    move-exception v0

    goto :goto_5

    .end local v19    # "jsonData":Ljava/lang/String;
    .restart local v6    # "jsonData":Ljava/lang/String;
    :catch_2
    move-exception v0

    move-object/from16 v19, v6

    .end local v6    # "jsonData":Ljava/lang/String;
    .restart local v19    # "jsonData":Ljava/lang/String;
    goto :goto_5

    .line 706
    .end local v18    # "jsonFile":Ljava/io/File;
    .end local v19    # "jsonData":Ljava/lang/String;
    .restart local v5    # "jsonFile":Ljava/io/File;
    .restart local v6    # "jsonData":Ljava/lang/String;
    .restart local v7    # "parse":Lorg/json/JSONObject;
    .restart local v8    # "autoZramItems":Lorg/json/JSONArray;
    .restart local v9    # "i":I
    .restart local v10    # "autoZramItem":Lorg/json/JSONObject;
    .restart local v11    # "autoZramFlashItems":Lorg/json/JSONArray;
    .restart local v12    # "getFlashSize":Z
    .restart local v13    # "autoZramRamTag":Ljava/lang/String;
    .restart local v14    # "autoZramRamTagItems":Lorg/json/JSONObject;
    .restart local v15    # "keys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v17    # "key":Ljava/lang/String;
    :catch_3
    move-exception v0

    move-object/from16 v18, v5

    move-object/from16 v19, v6

    move-object v5, v0

    .end local v5    # "jsonFile":Ljava/io/File;
    .end local v6    # "jsonData":Ljava/lang/String;
    .restart local v18    # "jsonFile":Ljava/io/File;
    .restart local v19    # "jsonData":Ljava/lang/String;
    move-object v0, v5

    .line 707
    .local v0, "expected":Ljava/lang/NumberFormatException;
    move-object/from16 v5, v18

    move-object/from16 v6, v19

    goto :goto_3

    .line 702
    .end local v0    # "expected":Ljava/lang/NumberFormatException;
    .end local v17    # "key":Ljava/lang/String;
    .end local v18    # "jsonFile":Ljava/io/File;
    .end local v19    # "jsonData":Ljava/lang/String;
    .restart local v5    # "jsonFile":Ljava/io/File;
    .restart local v6    # "jsonData":Ljava/lang/String;
    :cond_4
    move-object/from16 v18, v5

    move-object/from16 v19, v6

    .end local v5    # "jsonFile":Ljava/io/File;
    .end local v6    # "jsonData":Ljava/lang/String;
    .restart local v18    # "jsonFile":Ljava/io/File;
    .restart local v19    # "jsonData":Ljava/lang/String;
    goto :goto_4

    .line 698
    .end local v13    # "autoZramRamTag":Ljava/lang/String;
    .end local v14    # "autoZramRamTagItems":Lorg/json/JSONObject;
    .end local v15    # "keys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v18    # "jsonFile":Ljava/io/File;
    .end local v19    # "jsonData":Ljava/lang/String;
    .restart local v5    # "jsonFile":Ljava/io/File;
    .restart local v6    # "jsonData":Ljava/lang/String;
    :cond_5
    move-object/from16 v18, v5

    move-object/from16 v19, v6

    .line 686
    .end local v5    # "jsonFile":Ljava/io/File;
    .end local v6    # "jsonData":Ljava/lang/String;
    .end local v10    # "autoZramItem":Lorg/json/JSONObject;
    .end local v11    # "autoZramFlashItems":Lorg/json/JSONArray;
    .end local v12    # "getFlashSize":Z
    .restart local v18    # "jsonFile":Ljava/io/File;
    .restart local v19    # "jsonData":Ljava/lang/String;
    :goto_4
    add-int/lit8 v9, v9, 0x1

    move-object/from16 v5, v18

    move-object/from16 v6, v19

    goto/16 :goto_0

    .end local v18    # "jsonFile":Ljava/io/File;
    .end local v19    # "jsonData":Ljava/lang/String;
    .restart local v5    # "jsonFile":Ljava/io/File;
    .restart local v6    # "jsonData":Ljava/lang/String;
    :cond_6
    move-object/from16 v18, v5

    move-object/from16 v19, v6

    .line 717
    .end local v5    # "jsonFile":Ljava/io/File;
    .end local v6    # "jsonData":Ljava/lang/String;
    .end local v7    # "parse":Lorg/json/JSONObject;
    .end local v8    # "autoZramItems":Lorg/json/JSONArray;
    .end local v9    # "i":I
    .restart local v18    # "jsonFile":Ljava/io/File;
    .restart local v19    # "jsonData":Ljava/lang/String;
    nop

    .line 718
    sget-object v0, Lcom/android/server/ExtendMImpl;->extmGear:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_7

    .line 719
    invoke-direct {v1, v4, v3}, Lcom/android/server/ExtendMImpl;->getDefaultExtMGear(II)[F

    move-result-object v0

    return-object v0

    .line 722
    :cond_7
    sget-object v0, Lcom/android/server/ExtendMImpl;->extmGear:Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Lcom/android/server/ExtendMImpl;->changeArrayListToFloatArray(Ljava/util/ArrayList;)[F

    move-result-object v0

    .line 723
    .local v0, "ret":[F
    const/4 v5, 0x1

    iput-boolean v5, v1, Lcom/android/server/ExtendMImpl;->hasParseMemConfSuccessful:Z

    .line 724
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "read extm gear success and gear is: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0}, Ljava/util/Arrays;->toString([F)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 725
    return-object v0

    .line 713
    .end local v0    # "ret":[F
    .end local v18    # "jsonFile":Ljava/io/File;
    .end local v19    # "jsonData":Ljava/lang/String;
    .restart local v5    # "jsonFile":Ljava/io/File;
    .restart local v6    # "jsonData":Ljava/lang/String;
    :catch_4
    move-exception v0

    move-object/from16 v18, v5

    move-object/from16 v19, v6

    .line 714
    .end local v5    # "jsonFile":Ljava/io/File;
    .end local v6    # "jsonData":Ljava/lang/String;
    .local v0, "e":Lorg/json/JSONException;
    .restart local v18    # "jsonFile":Ljava/io/File;
    .restart local v19    # "jsonData":Ljava/lang/String;
    :goto_5
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Json parse error: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 716
    invoke-direct {v1, v4, v3}, Lcom/android/server/ExtendMImpl;->getDefaultExtMGear(II)[F

    move-result-object v2

    return-object v2
.end method

.method public isCloudAllowed()Z
    .locals 1

    .line 243
    iget-boolean v0, p0, Lcom/android/server/ExtendMImpl;->mLocalCloudEnable:Z

    return v0
.end method

.method public isMfzCloud()Z
    .locals 1

    .line 247
    iget-boolean v0, p0, Lcom/android/server/ExtendMImpl;->mfzLocalCloudEnable:Z

    return v0
.end method

.method public isdmoptCloud()Z
    .locals 1

    .line 251
    iget-boolean v0, p0, Lcom/android/server/ExtendMImpl;->dmoptLocalCloudEnable:Z

    return v0
.end method

.method public onStartJob(Landroid/app/job/JobParameters;)Z
    .locals 7
    .param p1, "params"    # Landroid/app/job/JobParameters;

    .line 1066
    invoke-virtual {p1}, Landroid/app/job/JobParameters;->getJobId()I

    move-result v0

    const v1, 0x82f4

    const/4 v2, 0x0

    if-ne v0, v1, :cond_0

    .line 1068
    sget-object v0, Lcom/android/server/ExtendMImpl;->sExtendMRecord:Lcom/android/server/ExtendMImpl$ExtendMRecord;

    iget-wide v3, p0, Lcom/android/server/ExtendMImpl;->mTotalMarked:J

    iget-wide v5, p0, Lcom/android/server/ExtendMImpl;->mTotalFlushed:J

    invoke-virtual {v0, v3, v4, v5, v6}, Lcom/android/server/ExtendMImpl$ExtendMRecord;->setDailyRecord(JJ)V

    .line 1069
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/ExtendMImpl;->mTotalMarked:J

    .line 1070
    iput-wide v0, p0, Lcom/android/server/ExtendMImpl;->mTotalFlushed:J

    .line 1071
    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->setFlushQuota()V

    .line 1072
    invoke-static {p0}, Lcom/android/server/ExtendMImpl;->resetNextFlushQuota(Landroid/content/Context;)V

    .line 1073
    invoke-virtual {p0, p1, v2}, Lcom/android/server/ExtendMImpl;->jobFinished(Landroid/app/job/JobParameters;Z)V

    .line 1074
    const/4 v0, 0x1

    return v0

    .line 1076
    :cond_0
    return v2
.end method

.method public onStopJob(Landroid/app/job/JobParameters;)Z
    .locals 1
    .param p1, "params"    # Landroid/app/job/JobParameters;

    .line 1081
    const/4 v0, 0x0

    return v0
.end method

.method public reportMemPressure(I)V
    .locals 8
    .param p1, "pressureState"    # I

    .line 1013
    const-string v0, "ExtM"

    const/4 v1, 0x0

    .line 1014
    .local v1, "flush_level":I
    const/4 v2, 0x0

    .line 1015
    .local v2, "flush_count_least":I
    const/4 v3, 0x0

    .line 1018
    .local v3, "msg_do_flush_level":I
    iget-boolean v4, p0, Lcom/android/server/ExtendMImpl;->isInitSuccess:Z

    if-eqz v4, :cond_5

    const/4 v4, 0x1

    if-ge p1, v4, :cond_0

    goto/16 :goto_3

    .line 1023
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/server/ExtendMImpl;->reportLowMemPressureLimit(I)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->flushIsAllowed()Z

    move-result v4

    if-nez v4, :cond_1

    goto :goto_2

    .line 1027
    :cond_1
    packed-switch p1, :pswitch_data_0

    .line 1044
    return-void

    .line 1039
    :pswitch_0
    sget v1, Lcom/android/server/ExtendMImpl;->FLUSH_HIGH_LEVEL:I

    .line 1040
    sget v4, Lcom/android/server/ExtendMImpl;->PER_FLUSH_QUOTA_HIGH:I

    iput v4, p0, Lcom/android/server/ExtendMImpl;->flush_per_quota:I

    .line 1041
    const/4 v3, 0x3

    .line 1042
    goto :goto_0

    .line 1034
    :pswitch_1
    sget v1, Lcom/android/server/ExtendMImpl;->FLUSH_MEDIUM_LEVEL:I

    .line 1035
    sget v4, Lcom/android/server/ExtendMImpl;->PER_FLUSH_QUOTA_MEDIUM:I

    iput v4, p0, Lcom/android/server/ExtendMImpl;->flush_per_quota:I

    .line 1036
    const/4 v3, 0x2

    .line 1037
    goto :goto_0

    .line 1029
    :pswitch_2
    sget v1, Lcom/android/server/ExtendMImpl;->FLUSH_LOW_LEVEL:I

    .line 1030
    sget v4, Lcom/android/server/ExtendMImpl;->PER_FLUSH_QUOTA_LOW:I

    iput v4, p0, Lcom/android/server/ExtendMImpl;->flush_per_quota:I

    .line 1031
    const/4 v3, 0x1

    .line 1032
    nop

    .line 1047
    :goto_0
    iget v4, p0, Lcom/android/server/ExtendMImpl;->flush_per_quota:I

    div-int/lit8 v4, v4, 0x2

    .line 1048
    .end local v2    # "flush_count_least":I
    .local v4, "flush_count_least":I
    invoke-direct {p0, v1, v4}, Lcom/android/server/ExtendMImpl;->isFlushablePagesExist(II)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1049
    return-void

    .line 1052
    :cond_2
    iget-boolean v2, p0, Lcom/android/server/ExtendMImpl;->isFlushFinished:Z

    if-eqz v2, :cond_3

    invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->getQuotaLeft()I

    move-result v2

    if-lez v2, :cond_3

    .line 1054
    :try_start_0
    iget-object v2, p0, Lcom/android/server/ExtendMImpl;->mHandler:Lcom/android/server/ExtendMImpl$MyHandler;

    const/4 v5, 0x4

    invoke-virtual {v2, v5}, Lcom/android/server/ExtendMImpl$MyHandler;->removeMessages(I)V

    .line 1055
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "reportMemPressure: "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, ", start flush"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1056
    iget-object v2, p0, Lcom/android/server/ExtendMImpl;->mHandler:Lcom/android/server/ExtendMImpl$MyHandler;

    invoke-virtual {v2, v3}, Lcom/android/server/ExtendMImpl$MyHandler;->sendEmptyMessage(I)Z

    .line 1057
    iget-object v2, p0, Lcom/android/server/ExtendMImpl;->mHandler:Lcom/android/server/ExtendMImpl$MyHandler;

    sget-wide v6, Lcom/android/server/ExtendMImpl;->DELAY_DURATION:J

    invoke-virtual {v2, v5, v6, v7}, Lcom/android/server/ExtendMImpl$MyHandler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1060
    goto :goto_1

    .line 1058
    :catch_0
    move-exception v2

    .line 1059
    .local v2, "e":Ljava/lang/Exception;
    const-string v5, "reportMemPressure error "

    invoke-static {v0, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1062
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_3
    :goto_1
    return-void

    .line 1024
    .end local v4    # "flush_count_least":I
    .local v2, "flush_count_least":I
    :cond_4
    :goto_2
    return-void

    .line 1019
    :cond_5
    :goto_3
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public start(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 198
    const-string v0, "ExtM"

    const-string v1, "Extm Version 3.0"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    invoke-static {p1}, Lcom/android/server/ExtendMImpl;->getInstance(Landroid/content/Context;)Lcom/android/server/ExtendMImpl;

    .line 200
    iget-object v0, p0, Lcom/android/server/ExtendMImpl;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    iput-object p1, p0, Lcom/android/server/ExtendMImpl;->mContext:Landroid/content/Context;

    .line 201
    :cond_0
    iget-object v0, p0, Lcom/android/server/ExtendMImpl;->mThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 202
    new-instance v0, Lcom/android/server/ExtendMImpl$MyHandler;

    iget-object v1, p0, Lcom/android/server/ExtendMImpl;->mThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/server/ExtendMImpl$MyHandler;-><init>(Lcom/android/server/ExtendMImpl;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/ExtendMImpl;->mHandler:Lcom/android/server/ExtendMImpl$MyHandler;

    .line 203
    const/4 v1, 0x5

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/ExtendMImpl$MyHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 204
    return-void
.end method
