class com.android.server.MiuiBatteryIntelligence$BatteryInelligenceHandler extends android.os.Handler {
	 /* .source "MiuiBatteryIntelligence.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/MiuiBatteryIntelligence; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "BatteryInelligenceHandler" */
} // .end annotation
/* # static fields */
public static final java.lang.String CLOSE_LOW_BATTERY_FAST_CHARGE_FUNCTION;
public static final java.lang.String KEY_LOW_BATTERY_FAST_CHARGE;
static final Integer MSG_BOOT_COMPLETED;
static final Integer MSG_DELAY_JUDGMEMNT;
static final Integer MSG_NEED_SCAN_WIFI;
static final Integer MSG_PLUGGED_CHANGED;
static final Integer MSG_WIFI_STATE_CHANGE;
public static final java.lang.String SMART_CHARGER_NODE;
public static final java.lang.String START_LOW_BATTERY_FAST_CHARGE_FUNCTION;
public static final java.lang.String START_OUT_DOOR_CHARGE_FUNCTION;
public static final java.lang.String STOP_OUT_DOOR_CHARGE_FUNCTION;
/* # instance fields */
private android.app.AlarmManager mAlarmManager;
private com.android.server.MiuiBatteryIntelligence$BatteryNotificationListernerService mBatteryNotificationListerner;
private final android.content.ContentResolver mContentResolver;
private Boolean mIsOutDoor;
private android.app.PendingIntent mNeedScanWifi;
private java.util.List mSavedApInfo;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Landroid/net/wifi/WifiConfiguration;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.database.ContentObserver mSettingsObserver;
private android.net.wifi.WifiManager mWifiManager;
final com.android.server.MiuiBatteryIntelligence this$0; //synthetic
/* # direct methods */
static android.content.ContentResolver -$$Nest$fgetmContentResolver ( com.android.server.MiuiBatteryIntelligence$BatteryInelligenceHandler p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContentResolver;
} // .end method
static void -$$Nest$msetSmartChargeFunction ( com.android.server.MiuiBatteryIntelligence$BatteryInelligenceHandler p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->setSmartChargeFunction(Ljava/lang/String;)V */
return;
} // .end method
public com.android.server.MiuiBatteryIntelligence$BatteryInelligenceHandler ( ) {
/* .locals 2 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 249 */
this.this$0 = p1;
/* .line 250 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 220 */
/* new-instance v0, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, p1, v1}, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;-><init>(Lcom/android/server/MiuiBatteryIntelligence;Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService-IA;)V */
this.mBatteryNotificationListerner = v0;
/* .line 231 */
/* new-instance v0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler$1; */
com.android.server.MiuiBatteryIntelligence .-$$Nest$fgetmHandler ( p1 );
/* invoke-direct {v0, p0, v1}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler$1;-><init>(Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;Landroid/os/Handler;)V */
this.mSettingsObserver = v0;
/* .line 251 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
this.mContentResolver = v0;
/* .line 252 */
v0 = this.mContext;
/* const-string/jumbo v1, "wifi" */
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/net/wifi/WifiManager; */
this.mWifiManager = v0;
/* .line 253 */
v0 = this.mContext;
final String v1 = "alarm"; // const-string v1, "alarm"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/AlarmManager; */
this.mAlarmManager = v0;
/* .line 254 */
p1 = (( com.android.server.MiuiBatteryIntelligence ) p1 ).isSupportLowBatteryFastCharge ( ); // invoke-virtual {p1}, Lcom/android/server/MiuiBatteryIntelligence;->isSupportLowBatteryFastCharge()Z
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 255 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->registerSettingsObserver()V */
/* .line 257 */
} // :cond_0
return;
} // .end method
private void cancelScanAlarm ( ) {
/* .locals 2 */
/* .line 433 */
v0 = this.mNeedScanWifi;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 434 */
v1 = this.mAlarmManager;
(( android.app.AlarmManager ) v1 ).cancel ( v0 ); // invoke-virtual {v1, v0}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V
/* .line 435 */
int v0 = 0; // const/4 v0, 0x0
this.mNeedScanWifi = v0;
/* .line 436 */
final String v0 = "MiuiBatteryIntelligence"; // const-string v0, "MiuiBatteryIntelligence"
final String v1 = "cancel alarm"; // const-string v1, "cancel alarm"
android.util.Slog .d ( v0,v1 );
/* .line 438 */
} // :cond_0
return;
} // .end method
private void changeListenerStatus ( ) {
/* .locals 6 */
/* .line 307 */
v0 = this.this$0;
v0 = com.android.server.MiuiBatteryIntelligence .-$$Nest$fgetmPlugged ( v0 );
final String v1 = "MiuiBatteryIntelligence"; // const-string v1, "MiuiBatteryIntelligence"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 309 */
try { // :try_start_0
v0 = this.mBatteryNotificationListerner;
v2 = this.this$0;
v2 = this.mContext;
/* new-instance v3, Landroid/content/ComponentName; */
v4 = this.this$0;
v4 = this.mContext;
/* .line 310 */
(( android.content.Context ) v4 ).getPackageName ( ); // invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;
(( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
(( java.lang.Class ) v5 ).getCanonicalName ( ); // invoke-virtual {v5}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;
/* invoke-direct {v3, v4, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* .line 309 */
int v4 = -1; // const/4 v4, -0x1
(( com.android.server.MiuiBatteryIntelligence$BatteryNotificationListernerService ) v0 ).registerAsSystemService ( v2, v3, v4 ); // invoke-virtual {v0, v2, v3, v4}, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->registerAsSystemService(Landroid/content/Context;Landroid/content/ComponentName;I)V
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 311 */
/* :catch_0 */
/* move-exception v0 */
/* .line 312 */
/* .local v0, "e":Landroid/os/RemoteException; */
final String v2 = "Cannot register listener"; // const-string v2, "Cannot register listener"
android.util.Slog .e ( v1,v2 );
/* .line 313 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :goto_0
/* .line 316 */
} // :cond_0
try { // :try_start_1
v0 = this.mBatteryNotificationListerner;
(( com.android.server.MiuiBatteryIntelligence$BatteryNotificationListernerService ) v0 ).clearStartAppList ( ); // invoke-virtual {v0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->clearStartAppList()V
/* .line 317 */
v0 = this.mBatteryNotificationListerner;
(( com.android.server.MiuiBatteryIntelligence$BatteryNotificationListernerService ) v0 ).unregisterAsSystemService ( ); // invoke-virtual {v0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->unregisterAsSystemService()V
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 320 */
/* .line 318 */
/* :catch_1 */
/* move-exception v0 */
/* .line 319 */
/* .restart local v0 # "e":Landroid/os/RemoteException; */
final String v2 = "Cannot unregister listener"; // const-string v2, "Cannot unregister listener"
android.util.Slog .e ( v1,v2 );
/* .line 322 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :goto_1
return;
} // .end method
private void changeOutDoorState ( ) {
/* .locals 3 */
/* .line 325 */
v0 = this.this$0;
v0 = com.android.server.MiuiBatteryIntelligence .-$$Nest$fgetmPlugged ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 326 */
int v0 = 5; // const/4 v0, 0x5
/* const-wide/16 v1, 0x3e8 */
(( com.android.server.MiuiBatteryIntelligence$BatteryInelligenceHandler ) p0 ).sendMessageDelayed ( v0, v1, v2 ); // invoke-virtual {p0, v0, v1, v2}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->sendMessageDelayed(IJ)V
/* .line 328 */
} // :cond_0
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->turnOffMonitorIfNeeded()V */
/* .line 330 */
} // :goto_0
return;
} // .end method
private Boolean compareSavedList ( java.util.List p0 ) {
/* .locals 8 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Landroid/net/wifi/ScanResult;", */
/* ">;)Z" */
/* } */
} // .end annotation
/* .line 407 */
/* .local p1, "scanResults":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/ScanResult;>;" */
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_3
/* check-cast v1, Landroid/net/wifi/ScanResult; */
/* .line 408 */
/* .local v1, "scanResult":Landroid/net/wifi/ScanResult; */
v2 = this.SSID;
/* .line 409 */
/* .local v2, "ssid":Ljava/lang/String; */
v3 = com.android.server.MiuiBatteryIntelligence .-$$Nest$sfgetDEBUG ( );
final String v4 = "MiuiBatteryIntelligence"; // const-string v4, "MiuiBatteryIntelligence"
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 410 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "scanResult ssid = "; // const-string v5, "scanResult ssid = "
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v3 );
/* .line 413 */
} // :cond_0
v3 = this.mSavedApInfo;
v5 = } // :goto_1
if ( v5 != null) { // if-eqz v5, :cond_2
/* check-cast v5, Landroid/net/wifi/WifiConfiguration; */
/* .line 414 */
/* .local v5, "wifiConfiguration":Landroid/net/wifi/WifiConfiguration; */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "\""; // const-string v7, "\""
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
v7 = this.SSID;
v6 = (( java.lang.String ) v6 ).equals ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 415 */
final String v0 = "already has saved"; // const-string v0, "already has saved"
android.util.Slog .d ( v4,v0 );
/* .line 416 */
int v0 = 1; // const/4 v0, 0x1
/* .line 418 */
} // .end local v5 # "wifiConfiguration":Landroid/net/wifi/WifiConfiguration;
} // :cond_1
/* .line 419 */
} // .end local v1 # "scanResult":Landroid/net/wifi/ScanResult;
} // .end local v2 # "ssid":Ljava/lang/String;
} // :cond_2
/* .line 420 */
} // :cond_3
int v0 = 0; // const/4 v0, 0x0
} // .end method
private void confrimOutDoorScene ( ) {
/* .locals 2 */
/* .line 424 */
/* iget-boolean v0, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->mIsOutDoor:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 425 */
return;
/* .line 427 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->mIsOutDoor:Z */
/* .line 428 */
final String v0 = "MiuiBatteryIntelligence"; // const-string v0, "MiuiBatteryIntelligence"
final String v1 = "Now we get outDoorScene"; // const-string v1, "Now we get outDoorScene"
android.util.Slog .d ( v0,v1 );
/* .line 429 */
final String v0 = "5"; // const-string v0, "5"
/* invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->setSmartChargeFunction(Ljava/lang/String;)V */
/* .line 430 */
return;
} // .end method
private void getSavedApInfo ( ) {
/* .locals 1 */
/* .line 445 */
v0 = this.mWifiManager;
(( android.net.wifi.WifiManager ) v0 ).getConfiguredNetworks ( ); // invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConfiguredNetworks()Ljava/util/List;
this.mSavedApInfo = v0;
/* .line 446 */
return;
} // .end method
private void handlerWifiChangedDisable ( ) {
/* .locals 0 */
/* .line 333 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->cancelScanAlarm()V */
/* .line 334 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->confrimOutDoorScene()V */
/* .line 335 */
return;
} // .end method
private void outDoorJudgment ( ) {
/* .locals 4 */
/* .line 361 */
v0 = this.this$0;
com.android.server.MiuiBatteryIntelligence .-$$Nest$fgetmMiCharge ( v0 );
(( miui.util.IMiCharge ) v0 ).getBatteryChargeType ( ); // invoke-virtual {v0}, Lmiui/util/IMiCharge;->getBatteryChargeType()Ljava/lang/String;
/* .line 362 */
/* .local v0, "chargeType":Ljava/lang/String; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "TYPE = "; // const-string v2, "TYPE = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiBatteryIntelligence"; // const-string v2, "MiuiBatteryIntelligence"
android.util.Slog .d ( v2,v1 );
/* .line 363 */
final String v1 = "DCP"; // const-string v1, "DCP"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_0 */
/* .line 364 */
return;
/* .line 366 */
} // :cond_0
v1 = this.this$0;
v1 = com.android.server.MiuiBatteryIntelligence .-$$Nest$fgetmWifiState ( v1 );
int v3 = 1; // const/4 v3, 0x1
/* if-ne v1, v3, :cond_1 */
/* .line 367 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->confrimOutDoorScene()V */
/* .line 368 */
} // :cond_1
v1 = this.this$0;
v1 = com.android.server.MiuiBatteryIntelligence .-$$Nest$fgetmWifiState ( v1 );
int v3 = 3; // const/4 v3, 0x3
/* if-ne v1, v3, :cond_3 */
/* .line 369 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->getSavedApInfo()V */
/* .line 370 */
v1 = this.mSavedApInfo;
/* if-nez v1, :cond_2 */
/* .line 371 */
/* const-string/jumbo v1, "there is no saved AP info" */
android.util.Slog .d ( v2,v1 );
/* .line 372 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->confrimOutDoorScene()V */
/* .line 373 */
return;
/* .line 375 */
} // :cond_2
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->scanConfirmSavaStatus()V */
/* .line 378 */
} // :cond_3
} // :goto_0
return;
} // .end method
private void registerSettingsObserver ( ) {
/* .locals 4 */
/* .line 260 */
final String v0 = "pc_low_battery_fast_charge"; // const-string v0, "pc_low_battery_fast_charge"
android.provider.Settings$Secure .getUriFor ( v0 );
/* .line 261 */
/* .local v0, "u":Landroid/net/Uri; */
v1 = this.mContentResolver;
int v2 = 1; // const/4 v2, 0x1
v3 = this.mSettingsObserver;
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 262 */
return;
} // .end method
private void resetLowBatteryFastChargeState ( ) {
/* .locals 4 */
/* .line 338 */
v0 = this.mContentResolver;
final String v1 = "pc_low_battery_fast_charge"; // const-string v1, "pc_low_battery_fast_charge"
int v2 = -1; // const/4 v2, -0x1
v0 = android.provider.Settings$Secure .getInt ( v0,v1,v2 );
/* .line 339 */
/* .local v0, "state":I */
final String v1 = "MiuiBatteryIntelligence"; // const-string v1, "MiuiBatteryIntelligence"
/* if-ne v0, v2, :cond_0 */
/* .line 340 */
final String v2 = "Failed to get settings"; // const-string v2, "Failed to get settings"
android.util.Slog .e ( v1,v2 );
/* .line 341 */
return;
/* .line 343 */
} // :cond_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Boot Completed reset low-Battery-Charge State = "; // const-string v3, "Boot Completed reset low-Battery-Charge State = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 344 */
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_1 */
/* .line 345 */
final String v1 = "9"; // const-string v1, "9"
/* invoke-direct {p0, v1}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->setSmartChargeFunction(Ljava/lang/String;)V */
/* .line 346 */
} // :cond_1
/* if-nez v0, :cond_2 */
/* .line 347 */
final String v1 = "8"; // const-string v1, "8"
/* invoke-direct {p0, v1}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->setSmartChargeFunction(Ljava/lang/String;)V */
/* .line 349 */
} // :cond_2
} // :goto_0
return;
} // .end method
private java.util.List sacnApList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Landroid/net/wifi/ScanResult;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 441 */
v0 = this.mWifiManager;
(( android.net.wifi.WifiManager ) v0 ).getScanResults ( ); // invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;
} // .end method
private void scanConfirmSavaStatus ( ) {
/* .locals 8 */
/* .line 390 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->sacnApList()Ljava/util/List; */
/* .line 391 */
/* .local v0, "scanResults":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/ScanResult;>;" */
final String v1 = "MiuiBatteryIntelligence"; // const-string v1, "MiuiBatteryIntelligence"
if ( v0 != null) { // if-eqz v0, :cond_2
v2 = this.mSavedApInfo;
/* if-nez v2, :cond_0 */
/* .line 395 */
} // :cond_0
v2 = /* invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->compareSavedList(Ljava/util/List;)Z */
/* if-nez v2, :cond_1 */
/* .line 396 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->confrimOutDoorScene()V */
/* .line 398 */
} // :cond_1
/* new-instance v2, Landroid/content/Intent; */
final String v3 = "miui.intent.action.NEED_SCAN_WIFI"; // const-string v3, "miui.intent.action.NEED_SCAN_WIFI"
/* invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 399 */
/* .local v2, "needScan":Landroid/content/Intent; */
v3 = this.this$0;
v3 = this.mContext;
int v4 = 0; // const/4 v4, 0x0
/* const/high16 v5, 0x4000000 */
android.app.PendingIntent .getBroadcast ( v3,v4,v2,v5 );
this.mNeedScanWifi = v3;
/* .line 400 */
v3 = this.mAlarmManager;
/* .line 401 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v4 */
/* const-wide/32 v6, 0x493e0 */
/* add-long/2addr v4, v6 */
v6 = this.mNeedScanWifi;
/* .line 400 */
int v7 = 2; // const/4 v7, 0x2
(( android.app.AlarmManager ) v3 ).setExactAndAllowWhileIdle ( v7, v4, v5, v6 ); // invoke-virtual {v3, v7, v4, v5, v6}, Landroid/app/AlarmManager;->setExactAndAllowWhileIdle(IJLandroid/app/PendingIntent;)V
/* .line 402 */
final String v3 = "has saved AP,wait 5 mins"; // const-string v3, "has saved AP,wait 5 mins"
android.util.Slog .d ( v1,v3 );
/* .line 404 */
} // .end local v2 # "needScan":Landroid/content/Intent;
} // :goto_0
return;
/* .line 392 */
} // :cond_2
} // :goto_1
final String v2 = "Faild to get current AP list"; // const-string v2, "Faild to get current AP list"
android.util.Slog .e ( v1,v2 );
/* .line 393 */
return;
} // .end method
private void setSmartChargeFunction ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "function" # Ljava/lang/String; */
/* .line 353 */
final String v0 = "MiuiBatteryIntelligence"; // const-string v0, "MiuiBatteryIntelligence"
try { // :try_start_0
v1 = this.this$0;
com.android.server.MiuiBatteryIntelligence .-$$Nest$fgetmMiCharge ( v1 );
/* const-string/jumbo v2, "smart_chg" */
(( miui.util.IMiCharge ) v1 ).setMiChargePath ( v2, p1 ); // invoke-virtual {v1, v2, p1}, Lmiui/util/IMiCharge;->setMiChargePath(Ljava/lang/String;Ljava/lang/String;)Z
/* .line 354 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "set smart charge = " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 357 */
/* .line 355 */
/* :catch_0 */
/* move-exception v1 */
/* .line 356 */
/* .local v1, "ee":Ljava/lang/Exception; */
final String v2 = "failed to set function"; // const-string v2, "failed to set function"
android.util.Slog .e ( v0,v2 );
/* .line 358 */
} // .end local v1 # "ee":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void turnOffMonitorIfNeeded ( ) {
/* .locals 1 */
/* .line 381 */
/* iget-boolean v0, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->mIsOutDoor:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 382 */
final String v0 = "4"; // const-string v0, "4"
/* invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->setSmartChargeFunction(Ljava/lang/String;)V */
/* .line 384 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->mIsOutDoor:Z */
/* .line 385 */
int v0 = 5; // const/4 v0, 0x5
(( com.android.server.MiuiBatteryIntelligence$BatteryInelligenceHandler ) p0 ).removeMessages ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->removeMessages(I)V
/* .line 386 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->cancelScanAlarm()V */
/* .line 387 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 3 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 272 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* packed-switch v0, :pswitch_data_0 */
/* .line 302 */
final String v0 = "MiuiBatteryIntelligence"; // const-string v0, "MiuiBatteryIntelligence"
final String v1 = "no msg need to handle"; // const-string v1, "no msg need to handle"
android.util.Slog .d ( v0,v1 );
/* .line 299 */
/* :pswitch_0 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->outDoorJudgment()V */
/* .line 300 */
/* .line 293 */
/* :pswitch_1 */
v0 = this.this$0;
com.android.server.MiuiBatteryIntelligence .-$$Nest$fgetmMiCharge ( v0 );
(( miui.util.IMiCharge ) v0 ).getBatteryChargeType ( ); // invoke-virtual {v0}, Lmiui/util/IMiCharge;->getBatteryChargeType()Ljava/lang/String;
/* .line 294 */
/* .local v0, "type":Ljava/lang/String; */
final String v1 = "DCP"; // const-string v1, "DCP"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 295 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->handlerWifiChangedDisable()V */
/* .line 290 */
} // .end local v0 # "type":Ljava/lang/String;
/* :pswitch_2 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->scanConfirmSavaStatus()V */
/* .line 291 */
/* .line 282 */
/* :pswitch_3 */
v0 = this.this$0;
v0 = (( com.android.server.MiuiBatteryIntelligence ) v0 ).isSupportNavigationCharge ( ); // invoke-virtual {v0}, Lcom/android/server/MiuiBatteryIntelligence;->isSupportNavigationCharge()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 283 */
v0 = this.this$0;
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v2 = "NavigationWhiteList"; // const-string v2, "NavigationWhiteList"
com.android.server.MiuiBatteryIntelligence .-$$Nest$mreadLocalCloudControlData ( v0,v1,v2 );
/* .line 285 */
} // :cond_0
v0 = this.this$0;
v0 = (( com.android.server.MiuiBatteryIntelligence ) v0 ).isSupportLowBatteryFastCharge ( ); // invoke-virtual {v0}, Lcom/android/server/MiuiBatteryIntelligence;->isSupportLowBatteryFastCharge()Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 286 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->resetLowBatteryFastChargeState()V */
/* .line 274 */
/* :pswitch_4 */
v0 = this.this$0;
v0 = (( com.android.server.MiuiBatteryIntelligence ) v0 ).isSupportNavigationCharge ( ); // invoke-virtual {v0}, Lcom/android/server/MiuiBatteryIntelligence;->isSupportNavigationCharge()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 275 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->changeListenerStatus()V */
/* .line 277 */
} // :cond_1
v0 = this.this$0;
v0 = (( com.android.server.MiuiBatteryIntelligence ) v0 ).isSupportOutDoorCharge ( ); // invoke-virtual {v0}, Lcom/android/server/MiuiBatteryIntelligence;->isSupportOutDoorCharge()Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 278 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->changeOutDoorState()V */
/* .line 304 */
} // :cond_2
} // :goto_0
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public void sendMessageDelayed ( Integer p0, Long p1 ) {
/* .locals 1 */
/* .param p1, "what" # I */
/* .param p2, "delayMillis" # J */
/* .line 265 */
(( com.android.server.MiuiBatteryIntelligence$BatteryInelligenceHandler ) p0 ).removeMessages ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->removeMessages(I)V
/* .line 266 */
android.os.Message .obtain ( p0,p1 );
/* .line 267 */
/* .local v0, "m":Landroid/os/Message; */
(( com.android.server.MiuiBatteryIntelligence$BatteryInelligenceHandler ) p0 ).sendMessageDelayed ( v0, p2, p3 ); // invoke-virtual {p0, v0, p2, p3}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 268 */
return;
} // .end method
