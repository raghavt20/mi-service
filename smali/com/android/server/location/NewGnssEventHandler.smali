.class public Lcom/android/server/location/NewGnssEventHandler;
.super Ljava/lang/Object;
.source "NewGnssEventHandler.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "GnssNps"

.field private static mInstance:Lcom/android/server/location/NewGnssEventHandler;


# instance fields
.field locationManager:Landroid/location/LocationManager;

.field private mAppContext:Landroid/content/Context;

.field private mNotifyManager:Lcom/android/server/location/NewNotifyManager;

.field private packageNameListNavNow:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/location/NewGnssEventHandler;->packageNameListNavNow:Ljava/util/List;

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/location/NewGnssEventHandler;->locationManager:Landroid/location/LocationManager;

    .line 30
    iput-object p1, p0, Lcom/android/server/location/NewGnssEventHandler;->mAppContext:Landroid/content/Context;

    .line 31
    new-instance v0, Lcom/android/server/location/NewNotifyManager;

    invoke-direct {v0, p1}, Lcom/android/server/location/NewNotifyManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/location/NewGnssEventHandler;->mNotifyManager:Lcom/android/server/location/NewNotifyManager;

    .line 32
    return-void
.end method

.method private getGnssEnableStatus()Z
    .locals 2

    .line 117
    iget-object v0, p0, Lcom/android/server/location/NewGnssEventHandler;->mAppContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 118
    const/4 v0, 0x0

    return v0

    .line 120
    :cond_0
    iget-object v1, p0, Lcom/android/server/location/NewGnssEventHandler;->locationManager:Landroid/location/LocationManager;

    if-nez v1, :cond_1

    .line 121
    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lcom/android/server/location/NewGnssEventHandler;->locationManager:Landroid/location/LocationManager;

    .line 123
    :cond_1
    iget-object v0, p0, Lcom/android/server/location/NewGnssEventHandler;->locationManager:Landroid/location/LocationManager;

    const-string v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/android/server/location/NewGnssEventHandler;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    const-class v0, Lcom/android/server/location/NewGnssEventHandler;

    monitor-enter v0

    .line 24
    :try_start_0
    sget-object v1, Lcom/android/server/location/NewGnssEventHandler;->mInstance:Lcom/android/server/location/NewGnssEventHandler;

    if-nez v1, :cond_0

    .line 25
    new-instance v1, Lcom/android/server/location/NewGnssEventHandler;

    invoke-direct {v1, p0}, Lcom/android/server/location/NewGnssEventHandler;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/android/server/location/NewGnssEventHandler;->mInstance:Lcom/android/server/location/NewGnssEventHandler;

    .line 26
    :cond_0
    sget-object v1, Lcom/android/server/location/NewGnssEventHandler;->mInstance:Lcom/android/server/location/NewGnssEventHandler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    .line 23
    .end local p0    # "context":Landroid/content/Context;
    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method


# virtual methods
.method public getLastForegroundNavApp()Ljava/lang/String;
    .locals 1

    .line 113
    const/4 v0, 0x0

    return-object v0
.end method

.method public declared-synchronized handleStart(Ljava/lang/String;)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    monitor-enter p0

    .line 53
    :try_start_0
    iget-object v0, p0, Lcom/android/server/location/NewGnssEventHandler;->packageNameListNavNow:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54
    invoke-direct {p0}, Lcom/android/server/location/NewGnssEventHandler;->getGnssEnableStatus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/android/server/location/NewGnssEventHandler;->mNotifyManager:Lcom/android/server/location/NewNotifyManager;

    invoke-virtual {v0, p1}, Lcom/android/server/location/NewNotifyManager;->showNavigationNotification(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 57
    .end local p0    # "this":Lcom/android/server/location/NewGnssEventHandler;
    :cond_0
    monitor-exit p0

    return-void

    .line 52
    .end local p1    # "packageName":Ljava/lang/String;
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized handleStop(Ljava/lang/String;)V
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;

    monitor-enter p0

    .line 60
    :try_start_0
    iget-object v0, p0, Lcom/android/server/location/NewGnssEventHandler;->mNotifyManager:Lcom/android/server/location/NewNotifyManager;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/server/location/NewGnssEventHandler;->packageNameListNavNow:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_3

    .line 63
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/NewGnssEventHandler;->packageNameListNavNow:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 64
    iget-object v0, p0, Lcom/android/server/location/NewGnssEventHandler;->packageNameListNavNow:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lcom/android/server/location/NewGnssEventHandler;->getGnssEnableStatus()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 65
    iget-object v0, p0, Lcom/android/server/location/NewGnssEventHandler;->packageNameListNavNow:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    invoke-virtual {p0}, Lcom/android/server/location/NewGnssEventHandler;->moreThanOneNavApp()Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_1

    .line 70
    :cond_1
    invoke-virtual {p0}, Lcom/android/server/location/NewGnssEventHandler;->getLastForegroundNavApp()Ljava/lang/String;

    move-result-object v0

    .line 71
    .local v0, "lastForegroundNavApp":Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 72
    iget-object v1, p0, Lcom/android/server/location/NewGnssEventHandler;->mNotifyManager:Lcom/android/server/location/NewNotifyManager;

    invoke-virtual {v1, v0}, Lcom/android/server/location/NewNotifyManager;->showNavigationNotification(Ljava/lang/String;)V

    goto :goto_0

    .line 74
    .end local p0    # "this":Lcom/android/server/location/NewGnssEventHandler;
    :cond_2
    iget-object v2, p0, Lcom/android/server/location/NewGnssEventHandler;->mNotifyManager:Lcom/android/server/location/NewNotifyManager;

    iget-object v3, p0, Lcom/android/server/location/NewGnssEventHandler;->packageNameListNavNow:Ljava/util/List;

    .line 75
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    sub-int/2addr v4, v1

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 74
    invoke-virtual {v2, v1}, Lcom/android/server/location/NewNotifyManager;->showNavigationNotification(Ljava/lang/String;)V

    .line 77
    .end local v0    # "lastForegroundNavApp":Ljava/lang/String;
    :goto_0
    goto :goto_2

    .line 66
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/android/server/location/NewGnssEventHandler;->mNotifyManager:Lcom/android/server/location/NewNotifyManager;

    iget-object v1, p0, Lcom/android/server/location/NewGnssEventHandler;->packageNameListNavNow:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/server/location/NewNotifyManager;->showNavigationNotification(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67
    monitor-exit p0

    return-void

    .line 78
    :cond_4
    :try_start_1
    iget-object v0, p0, Lcom/android/server/location/NewGnssEventHandler;->mNotifyManager:Lcom/android/server/location/NewNotifyManager;

    invoke-virtual {v0}, Lcom/android/server/location/NewNotifyManager;->removeNotification()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 80
    :goto_2
    monitor-exit p0

    return-void

    .line 61
    :cond_5
    :goto_3
    monitor-exit p0

    return-void

    .line 59
    .end local p1    # "packageName":Ljava/lang/String;
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized handleUpdateGnssStatus()V
    .locals 5

    monitor-enter p0

    .line 35
    :try_start_0
    invoke-direct {p0}, Lcom/android/server/location/NewGnssEventHandler;->getGnssEnableStatus()Z

    move-result v0

    .line 36
    .local v0, "gnssEnable":Z
    const-string v1, "GnssNps"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "gnss status update: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    if-eqz v0, :cond_1

    .line 38
    iget-object v1, p0, Lcom/android/server/location/NewGnssEventHandler;->packageNameListNavNow:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 39
    invoke-virtual {p0}, Lcom/android/server/location/NewGnssEventHandler;->getLastForegroundNavApp()Ljava/lang/String;

    move-result-object v1

    .line 40
    .local v1, "lastForegroundNavApp":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 41
    iget-object v2, p0, Lcom/android/server/location/NewGnssEventHandler;->mNotifyManager:Lcom/android/server/location/NewNotifyManager;

    invoke-virtual {v2, v1}, Lcom/android/server/location/NewNotifyManager;->showNavigationNotification(Ljava/lang/String;)V

    goto :goto_0

    .line 43
    .end local p0    # "this":Lcom/android/server/location/NewGnssEventHandler;
    :cond_0
    iget-object v2, p0, Lcom/android/server/location/NewGnssEventHandler;->mNotifyManager:Lcom/android/server/location/NewNotifyManager;

    iget-object v3, p0, Lcom/android/server/location/NewGnssEventHandler;->packageNameListNavNow:Ljava/util/List;

    .line 44
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 43
    invoke-virtual {v2, v3}, Lcom/android/server/location/NewNotifyManager;->showNavigationNotification(Ljava/lang/String;)V

    .line 46
    .end local v1    # "lastForegroundNavApp":Ljava/lang/String;
    :goto_0
    goto :goto_1

    .line 48
    :cond_1
    iget-object v1, p0, Lcom/android/server/location/NewGnssEventHandler;->mNotifyManager:Lcom/android/server/location/NewNotifyManager;

    invoke-virtual {v1}, Lcom/android/server/location/NewNotifyManager;->removeNotification()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 50
    :cond_2
    :goto_1
    monitor-exit p0

    return-void

    .line 34
    .end local v0    # "gnssEnable":Z
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized handlerUpdateFixStatus(Z)V
    .locals 4
    .param p1, "fixing"    # Z

    monitor-enter p0

    .line 83
    if-eqz p1, :cond_0

    .line 84
    :try_start_0
    iget-object v0, p0, Lcom/android/server/location/NewGnssEventHandler;->mNotifyManager:Lcom/android/server/location/NewNotifyManager;

    invoke-virtual {v0}, Lcom/android/server/location/NewNotifyManager;->removeNotification()V

    goto :goto_0

    .line 86
    .end local p0    # "this":Lcom/android/server/location/NewGnssEventHandler;
    :cond_0
    invoke-direct {p0}, Lcom/android/server/location/NewGnssEventHandler;->getGnssEnableStatus()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/server/location/NewGnssEventHandler;->packageNameListNavNow:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 87
    invoke-virtual {p0}, Lcom/android/server/location/NewGnssEventHandler;->getLastForegroundNavApp()Ljava/lang/String;

    move-result-object v0

    .line 88
    .local v0, "lastForegroundNavApp":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 89
    iget-object v1, p0, Lcom/android/server/location/NewGnssEventHandler;->mNotifyManager:Lcom/android/server/location/NewNotifyManager;

    invoke-virtual {v1, v0}, Lcom/android/server/location/NewNotifyManager;->showNavigationNotification(Ljava/lang/String;)V

    goto :goto_0

    .line 91
    :cond_1
    iget-object v1, p0, Lcom/android/server/location/NewGnssEventHandler;->mNotifyManager:Lcom/android/server/location/NewNotifyManager;

    iget-object v2, p0, Lcom/android/server/location/NewGnssEventHandler;->packageNameListNavNow:Ljava/util/List;

    .line 92
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 91
    invoke-virtual {v1, v2}, Lcom/android/server/location/NewNotifyManager;->showNavigationNotification(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 96
    .end local v0    # "lastForegroundNavApp":Ljava/lang/String;
    :cond_2
    :goto_0
    monitor-exit p0

    return-void

    .line 82
    .end local p1    # "fixing":Z
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public moreThanOneNavApp()Z
    .locals 6

    .line 100
    iget-object v0, p0, Lcom/android/server/location/NewGnssEventHandler;->packageNameListNavNow:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-gt v0, v2, :cond_0

    .line 101
    return v1

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/NewGnssEventHandler;->packageNameListNavNow:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 104
    .local v0, "appNameFirst":Ljava/lang/String;
    const/4 v3, 0x1

    .local v3, "i":I
    :goto_0
    iget-object v4, p0, Lcom/android/server/location/NewGnssEventHandler;->packageNameListNavNow:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_2

    .line 105
    iget-object v4, p0, Lcom/android/server/location/NewGnssEventHandler;->packageNameListNavNow:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v4

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v5

    if-eq v4, v5, :cond_1

    .line 106
    return v2

    .line 104
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 109
    .end local v3    # "i":I
    :cond_2
    return v1
.end method
