public class com.android.server.location.NewNotifyManager {
	 /* .source "NewNotifyManager.java" */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private final java.lang.String CHANNEL_ID;
	 private final Integer NOTIFY_ID;
	 private android.app.PendingIntent intentToSettings;
	 private android.content.Context mAppContext;
	 private android.app.Notification$Builder mBuilder;
	 private android.app.NotificationManager mNotificationManager;
	 /* # direct methods */
	 public com.android.server.location.NewNotifyManager ( ) {
		 /* .locals 4 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .line 36 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 27 */
		 final String v0 = "GPS_STATUS_MONITOR_ID"; // const-string v0, "GPS_STATUS_MONITOR_ID"
		 this.CHANNEL_ID = v0;
		 /* .line 28 */
		 int v1 = 1; // const/4 v1, 0x1
		 /* iput v1, p0, Lcom/android/server/location/NewNotifyManager;->NOTIFY_ID:I */
		 /* .line 37 */
		 this.mAppContext = p1;
		 /* .line 38 */
		 final String v1 = "notification"; // const-string v1, "notification"
		 (( android.content.Context ) p1 ).getSystemService ( v1 ); // invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
		 /* check-cast v1, Landroid/app/NotificationManager; */
		 this.mNotificationManager = v1;
		 /* .line 39 */
		 /* new-instance v1, Landroid/app/Notification$Builder; */
		 v2 = this.mAppContext;
		 /* invoke-direct {v1, v2, v0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;)V */
		 this.mBuilder = v1;
		 /* .line 41 */
		 (( com.android.server.location.NewNotifyManager ) p0 ).initNotifyChannel ( ); // invoke-virtual {p0}, Lcom/android/server/location/NewNotifyManager;->initNotifyChannel()V
		 /* .line 43 */
		 /* new-instance v0, Landroid/content/Intent; */
		 /* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
		 /* .line 44 */
		 /* .local v0, "intent":Landroid/content/Intent; */
		 final String v1 = "com.android.settings"; // const-string v1, "com.android.settings"
		 final String v2 = "com.android.settings.Settings$LocationSettingsActivity"; // const-string v2, "com.android.settings.Settings$LocationSettingsActivity"
		 (( android.content.Intent ) v0 ).setClassName ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 45 */
		 /* const/high16 v1, 0x10000000 */
		 (( android.content.Intent ) v0 ).addFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
		 /* .line 46 */
		 v1 = this.mAppContext;
		 int v2 = 0; // const/4 v2, 0x0
		 /* const/high16 v3, 0xc000000 */
		 android.app.PendingIntent .getActivity ( v1,v2,v0,v3 );
		 this.intentToSettings = v1;
		 /* .line 47 */
		 return;
	 } // .end method
	 private android.graphics.Bitmap getAppIcon ( android.graphics.drawable.Drawable p0 ) {
		 /* .locals 11 */
		 /* .param p1, "draw" # Landroid/graphics/drawable/Drawable; */
		 /* .line 96 */
		 /* instance-of v0, p1, Landroid/graphics/drawable/BitmapDrawable; */
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 97 */
			 /* move-object v0, p1 */
			 /* check-cast v0, Landroid/graphics/drawable/BitmapDrawable; */
			 (( android.graphics.drawable.BitmapDrawable ) v0 ).getBitmap ( ); // invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;
			 /* .line 98 */
		 } // :cond_0
		 /* instance-of v0, p1, Landroid/graphics/drawable/AdaptiveIconDrawable; */
		 if ( v0 != null) { // if-eqz v0, :cond_1
			 /* .line 99 */
			 /* move-object v0, p1 */
			 /* check-cast v0, Landroid/graphics/drawable/AdaptiveIconDrawable; */
			 (( android.graphics.drawable.AdaptiveIconDrawable ) v0 ).getBackground ( ); // invoke-virtual {v0}, Landroid/graphics/drawable/AdaptiveIconDrawable;->getBackground()Landroid/graphics/drawable/Drawable;
			 /* .line 100 */
			 /* .local v0, "backgroundDr":Landroid/graphics/drawable/Drawable; */
			 /* move-object v1, p1 */
			 /* check-cast v1, Landroid/graphics/drawable/AdaptiveIconDrawable; */
			 (( android.graphics.drawable.AdaptiveIconDrawable ) v1 ).getForeground ( ); // invoke-virtual {v1}, Landroid/graphics/drawable/AdaptiveIconDrawable;->getForeground()Landroid/graphics/drawable/Drawable;
			 /* .line 101 */
			 /* .local v1, "foregroundDr":Landroid/graphics/drawable/Drawable; */
			 int v2 = 2; // const/4 v2, 0x2
			 /* new-array v2, v2, [Landroid/graphics/drawable/Drawable; */
			 /* .line 102 */
			 /* .local v2, "drr":[Landroid/graphics/drawable/Drawable; */
			 int v3 = 0; // const/4 v3, 0x0
			 /* aput-object v0, v2, v3 */
			 /* .line 103 */
			 int v4 = 1; // const/4 v4, 0x1
			 /* aput-object v1, v2, v4 */
			 /* .line 104 */
			 /* new-instance v4, Landroid/graphics/drawable/LayerDrawable; */
			 /* invoke-direct {v4, v2}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V */
			 /* .line 105 */
			 /* .local v4, "layerDrawable":Landroid/graphics/drawable/LayerDrawable; */
			 v5 = 			 (( android.graphics.drawable.LayerDrawable ) v4 ).getIntrinsicWidth ( ); // invoke-virtual {v4}, Landroid/graphics/drawable/LayerDrawable;->getIntrinsicWidth()I
			 /* .line 106 */
			 /* .local v5, "width":I */
			 v6 = 			 (( android.graphics.drawable.LayerDrawable ) v4 ).getIntrinsicHeight ( ); // invoke-virtual {v4}, Landroid/graphics/drawable/LayerDrawable;->getIntrinsicHeight()I
			 /* .line 107 */
			 /* .local v6, "height":I */
			 v7 = android.graphics.Bitmap$Config.ARGB_8888;
			 android.graphics.Bitmap .createBitmap ( v5,v6,v7 );
			 /* .line 108 */
			 /* .local v7, "bitmap":Landroid/graphics/Bitmap; */
			 /* new-instance v8, Landroid/graphics/Canvas; */
			 /* invoke-direct {v8, v7}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V */
			 /* .line 109 */
			 /* .local v8, "canvas":Landroid/graphics/Canvas; */
			 v9 = 			 (( android.graphics.Canvas ) v8 ).getWidth ( ); // invoke-virtual {v8}, Landroid/graphics/Canvas;->getWidth()I
			 v10 = 			 (( android.graphics.Canvas ) v8 ).getHeight ( ); // invoke-virtual {v8}, Landroid/graphics/Canvas;->getHeight()I
			 (( android.graphics.drawable.LayerDrawable ) v4 ).setBounds ( v3, v3, v9, v10 ); // invoke-virtual {v4, v3, v3, v9, v10}, Landroid/graphics/drawable/LayerDrawable;->setBounds(IIII)V
			 /* .line 110 */
			 (( android.graphics.drawable.LayerDrawable ) v4 ).draw ( v8 ); // invoke-virtual {v4, v8}, Landroid/graphics/drawable/LayerDrawable;->draw(Landroid/graphics/Canvas;)V
			 /* .line 111 */
			 /* .line 113 */
		 } // .end local v0 # "backgroundDr":Landroid/graphics/drawable/Drawable;
	 } // .end local v1 # "foregroundDr":Landroid/graphics/drawable/Drawable;
} // .end local v2 # "drr":[Landroid/graphics/drawable/Drawable;
} // .end local v4 # "layerDrawable":Landroid/graphics/drawable/LayerDrawable;
} // .end local v5 # "width":I
} // .end local v6 # "height":I
} // .end local v7 # "bitmap":Landroid/graphics/Bitmap;
} // .end local v8 # "canvas":Landroid/graphics/Canvas;
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean isChineseLanguage ( ) {
/* .locals 2 */
/* .line 117 */
java.util.Locale .getDefault ( );
(( java.util.Locale ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;
/* .line 118 */
/* .local v0, "language":Ljava/lang/String; */
/* const-string/jumbo v1, "zh_CN" */
v1 = (( java.lang.String ) v0 ).endsWith ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z
} // .end method
/* # virtual methods */
public void initNotifyChannel ( ) {
/* .locals 6 */
/* .line 122 */
v0 = this.mNotificationManager;
/* if-nez v0, :cond_0 */
/* .line 123 */
return;
/* .line 125 */
} // :cond_0
v0 = this.mAppContext;
/* const v1, 0x110f02bd */
(( android.content.Context ) v0 ).getString ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;
/* .line 126 */
/* .local v0, "description":Ljava/lang/String; */
/* new-instance v1, Landroid/app/NotificationChannel; */
final String v2 = "GPS_STATUS_MONITOR_ID"; // const-string v2, "GPS_STATUS_MONITOR_ID"
int v3 = 2; // const/4 v3, 0x2
/* invoke-direct {v1, v2, v0, v3}, Landroid/app/NotificationChannel;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V */
/* .line 127 */
/* .local v1, "channel":Landroid/app/NotificationChannel; */
int v2 = 1; // const/4 v2, 0x1
(( android.app.NotificationChannel ) v1 ).setLockscreenVisibility ( v2 ); // invoke-virtual {v1, v2}, Landroid/app/NotificationChannel;->setLockscreenVisibility(I)V
/* .line 128 */
int v3 = 0; // const/4 v3, 0x0
(( android.app.NotificationChannel ) v1 ).setSound ( v3, v3 ); // invoke-virtual {v1, v3, v3}, Landroid/app/NotificationChannel;->setSound(Landroid/net/Uri;Landroid/media/AudioAttributes;)V
/* .line 129 */
int v3 = 0; // const/4 v3, 0x0
(( android.app.NotificationChannel ) v1 ).enableVibration ( v3 ); // invoke-virtual {v1, v3}, Landroid/app/NotificationChannel;->enableVibration(Z)V
/* .line 130 */
/* new-array v2, v2, [J */
/* const-wide/16 v4, 0x0 */
/* aput-wide v4, v2, v3 */
(( android.app.NotificationChannel ) v1 ).setVibrationPattern ( v2 ); // invoke-virtual {v1, v2}, Landroid/app/NotificationChannel;->setVibrationPattern([J)V
/* .line 131 */
v2 = this.mNotificationManager;
(( android.app.NotificationManager ) v2 ).createNotificationChannel ( v1 ); // invoke-virtual {v2, v1}, Landroid/app/NotificationManager;->createNotificationChannel(Landroid/app/NotificationChannel;)V
/* .line 132 */
return;
} // .end method
public void removeNotification ( ) {
/* .locals 2 */
/* .line 50 */
final String v0 = "GnssNps-NotifyManager"; // const-string v0, "GnssNps-NotifyManager"
final String v1 = "removeNotification"; // const-string v1, "removeNotification"
android.util.Log .d ( v0,v1 );
/* .line 52 */
try { // :try_start_0
v0 = this.mNotificationManager;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 53 */
(( android.app.NotificationManager ) v0 ).cancelAll ( ); // invoke-virtual {v0}, Landroid/app/NotificationManager;->cancelAll()V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 57 */
} // :cond_0
/* .line 55 */
/* :catch_0 */
/* move-exception v0 */
/* .line 56 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 58 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public void showNavigationNotification ( java.lang.String p0 ) {
/* .locals 8 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 61 */
v0 = /* invoke-direct {p0}, Lcom/android/server/location/NewNotifyManager;->isChineseLanguage()Z */
final String v1 = "GnssNps-NotifyManager"; // const-string v1, "GnssNps-NotifyManager"
/* if-nez v0, :cond_0 */
/* .line 62 */
/* const-string/jumbo v0, "show notification only in CHINESE language" */
android.util.Log .d ( v1,v0 );
/* .line 63 */
return;
/* .line 65 */
} // :cond_0
v0 = this.mAppContext;
(( android.content.Context ) v0 ).getPackageManager ( ); // invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 69 */
/* .local v0, "pm":Landroid/content/pm/PackageManager; */
int v2 = 0; // const/4 v2, 0x0
try { // :try_start_0
(( android.content.pm.PackageManager ) v0 ).getApplicationInfo ( p1, v2 ); // invoke-virtual {v0, p1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
/* .line 70 */
/* .local v2, "ai":Landroid/content/pm/ApplicationInfo; */
(( android.content.pm.ApplicationInfo ) v2 ).loadLabel ( v0 ); // invoke-virtual {v2, v0}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
/* .line 71 */
/* .local v3, "appName":Ljava/lang/String; */
(( android.content.pm.PackageManager ) v0 ).getApplicationIcon ( p1 ); // invoke-virtual {v0, p1}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
/* .line 72 */
/* .local v4, "draw":Landroid/graphics/drawable/Drawable; */
/* invoke-direct {p0, v4}, Lcom/android/server/location/NewNotifyManager;->getAppIcon(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap; */
/* .line 73 */
/* .local v5, "iconBmp":Landroid/graphics/Bitmap; */
/* if-nez v5, :cond_1 */
/* .line 74 */
v6 = this.mAppContext;
(( android.content.Context ) v6 ).getResources ( ); // invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v7, 0x110801b8 */
android.graphics.BitmapFactory .decodeResource ( v6,v7 );
/* :try_end_0 */
/* .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 ..:try_end_0} :catch_1 */
/* move-object v5, v1 */
/* .line 80 */
} // .end local v2 # "ai":Landroid/content/pm/ApplicationInfo;
} // .end local v4 # "draw":Landroid/graphics/drawable/Drawable;
} // :cond_1
/* nop */
/* .line 81 */
v1 = this.mBuilder;
(( android.app.Notification$Builder ) v1 ).setLargeIcon ( v5 ); // invoke-virtual {v1, v5}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;
/* .line 82 */
v1 = this.mBuilder;
/* const v2, 0x110801b9 */
(( android.app.Notification$Builder ) v1 ).setSmallIcon ( v2 ); // invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;
/* .line 84 */
try { // :try_start_1
v1 = this.mBuilder;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.mAppContext;
/* const v6, 0x110f02b8 */
(( android.content.Context ) v4 ).getString ( v6 ); // invoke-virtual {v4, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( android.app.Notification$Builder ) v1 ).setContentTitle ( v2 ); // invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;
v2 = this.mAppContext;
/* .line 85 */
/* const v4, 0x110f02b7 */
(( android.content.Context ) v2 ).getString ( v4 ); // invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;
(( android.app.Notification$Builder ) v1 ).setContentText ( v2 ); // invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;
v2 = this.intentToSettings;
/* .line 86 */
(( android.app.Notification$Builder ) v1 ).setContentIntent ( v2 ); // invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;
/* .line 87 */
int v2 = 1; // const/4 v2, 0x1
(( android.app.Notification$Builder ) v1 ).setOngoing ( v2 ); // invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;
/* .line 88 */
(( android.app.Notification$Builder ) v1 ).setAutoCancel ( v2 ); // invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;
/* .line 89 */
v1 = this.mNotificationManager;
v4 = this.mBuilder;
(( android.app.Notification$Builder ) v4 ).build ( ); // invoke-virtual {v4}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;
(( android.app.NotificationManager ) v1 ).notify ( v2, v4 ); // invoke-virtual {v1, v2, v4}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 92 */
/* .line 90 */
/* :catch_0 */
/* move-exception v1 */
/* .line 91 */
/* .local v1, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 93 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
return;
/* .line 77 */
} // .end local v3 # "appName":Ljava/lang/String;
} // .end local v5 # "iconBmp":Landroid/graphics/Bitmap;
/* :catch_1 */
/* move-exception v2 */
/* .line 78 */
/* .local v2, "nameNotFoundException":Landroid/content/pm/PackageManager$NameNotFoundException; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "No such application for this name:"; // const-string v4, "No such application for this name:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .w ( v1,v3 );
/* .line 79 */
return;
} // .end method
