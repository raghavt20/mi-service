.class Lcom/android/server/location/LocationExtCooperateImpl$1;
.super Landroid/content/BroadcastReceiver;
.source "LocationExtCooperateImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/location/LocationExtCooperateImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/location/LocationExtCooperateImpl;


# direct methods
.method constructor <init>(Lcom/android/server/location/LocationExtCooperateImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/location/LocationExtCooperateImpl;

    .line 72
    iput-object p1, p0, Lcom/android/server/location/LocationExtCooperateImpl$1;->this$0:Lcom/android/server/location/LocationExtCooperateImpl;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 75
    const-string v0, "com.android.app.action.SATELLITE_STATE_CHANGE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 77
    const-string v0, "is_enable"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 79
    .local v0, "satelliteCallSwitcher":Z
    const-string v1, "phone_id"

    const/4 v2, -0x1

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 80
    .local v1, "phoneId":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Satellite call is enable:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", phoneId:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "LocationExtCooperate"

    invoke-static {v3, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    if-eqz v0, :cond_0

    .line 84
    invoke-static {}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub;->getInstance()Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/location/LocationExtCooperateImpl$1;->this$0:Lcom/android/server/location/LocationExtCooperateImpl;

    invoke-static {v3}, Lcom/android/server/location/LocationExtCooperateImpl;->-$$Nest$fgetsPkg(Lcom/android/server/location/LocationExtCooperateImpl;)Ljava/util/HashSet;

    move-result-object v3

    const-string v4, "MI_GNSS"

    invoke-virtual {v2, v3, v4}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub;->registerSatelliteCallMode(Ljava/util/HashSet;Ljava/lang/String;)V

    goto :goto_0

    .line 87
    :cond_0
    invoke-static {}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub;->getInstance()Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub;->unRegisterSatelliteCallMode()V

    .line 90
    .end local v0    # "satelliteCallSwitcher":Z
    .end local v1    # "phoneId":I
    :cond_1
    :goto_0
    return-void
.end method
