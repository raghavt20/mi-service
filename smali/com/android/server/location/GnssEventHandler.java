public class com.android.server.location.GnssEventHandler {
	 /* .source "GnssEventHandler.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/location/GnssEventHandler$NotifyManager; */
	 /* } */
} // .end annotation
/* # static fields */
private static java.lang.String CLOSE_BLUR_LOCATION;
private static final java.lang.String TAG;
private static com.android.server.location.GnssEventHandler mInstance;
/* # instance fields */
private android.app.Notification$Action mAction;
private android.content.Context mAppContext;
private Boolean mBlur;
private com.android.server.location.GnssEventHandler$NotifyManager mNotifyManager;
private java.lang.String pkgName;
/* # direct methods */
static android.app.Notification$Action -$$Nest$fgetmAction ( com.android.server.location.GnssEventHandler p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mAction;
} // .end method
static android.content.Context -$$Nest$fgetmAppContext ( com.android.server.location.GnssEventHandler p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mAppContext;
} // .end method
static Boolean -$$Nest$fgetmBlur ( com.android.server.location.GnssEventHandler p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/android/server/location/GnssEventHandler;->mBlur:Z */
} // .end method
static com.android.server.location.GnssEventHandler$NotifyManager -$$Nest$fgetmNotifyManager ( com.android.server.location.GnssEventHandler p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mNotifyManager;
} // .end method
static java.lang.String -$$Nest$fgetpkgName ( com.android.server.location.GnssEventHandler p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.pkgName;
} // .end method
static void -$$Nest$fputmAction ( com.android.server.location.GnssEventHandler p0, android.app.Notification$Action p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 this.mAction = p1;
	 return;
} // .end method
static void -$$Nest$fputmBlur ( com.android.server.location.GnssEventHandler p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/location/GnssEventHandler;->mBlur:Z */
	 return;
} // .end method
static Boolean -$$Nest$misChineseLanguage ( com.android.server.location.GnssEventHandler p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = 	 /* invoke-direct {p0}, Lcom/android/server/location/GnssEventHandler;->isChineseLanguage()Z */
} // .end method
static java.lang.String -$$Nest$sfgetCLOSE_BLUR_LOCATION ( ) { //bridge//synthethic
	 /* .locals 1 */
	 v0 = com.android.server.location.GnssEventHandler.CLOSE_BLUR_LOCATION;
} // .end method
static com.android.server.location.GnssEventHandler ( ) {
	 /* .locals 1 */
	 /* .line 39 */
	 final String v0 = "com.miui.gnss.CLOSE_BLUR_LOCATION"; // const-string v0, "com.miui.gnss.CLOSE_BLUR_LOCATION"
	 return;
} // .end method
private com.android.server.location.GnssEventHandler ( ) {
	 /* .locals 1 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .line 50 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 37 */
	 /* new-instance v0, Lcom/android/server/location/GnssEventHandler$NotifyManager; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/location/GnssEventHandler$NotifyManager;-><init>(Lcom/android/server/location/GnssEventHandler;)V */
	 this.mNotifyManager = v0;
	 /* .line 41 */
	 final String v0 = ""; // const-string v0, ""
	 this.pkgName = v0;
	 /* .line 42 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* iput-boolean v0, p0, Lcom/android/server/location/GnssEventHandler;->mBlur:Z */
	 /* .line 51 */
	 this.mAppContext = p1;
	 /* .line 52 */
	 return;
} // .end method
public static synchronized com.android.server.location.GnssEventHandler getInstance ( android.content.Context p0 ) {
	 /* .locals 2 */
	 /* .param p0, "context" # Landroid/content/Context; */
	 /* const-class v0, Lcom/android/server/location/GnssEventHandler; */
	 /* monitor-enter v0 */
	 /* .line 45 */
	 try { // :try_start_0
		 v1 = com.android.server.location.GnssEventHandler.mInstance;
		 /* if-nez v1, :cond_0 */
		 /* .line 46 */
		 /* new-instance v1, Lcom/android/server/location/GnssEventHandler; */
		 /* invoke-direct {v1, p0}, Lcom/android/server/location/GnssEventHandler;-><init>(Landroid/content/Context;)V */
		 /* .line 47 */
	 } // :cond_0
	 v1 = com.android.server.location.GnssEventHandler.mInstance;
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* monitor-exit v0 */
	 /* .line 44 */
} // .end local p0 # "context":Landroid/content/Context;
/* :catchall_0 */
/* move-exception p0 */
/* monitor-exit v0 */
/* throw p0 */
} // .end method
private Boolean isChineseLanguage ( ) {
/* .locals 2 */
/* .line 94 */
java.util.Locale .getDefault ( );
(( java.util.Locale ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;
/* .line 95 */
/* .local v0, "language":Ljava/lang/String; */
/* const-string/jumbo v1, "zh_CN" */
v1 = (( java.lang.String ) v0 ).endsWith ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z
} // .end method
/* # virtual methods */
public void handleCallerName ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "name" # Ljava/lang/String; */
/* .line 82 */
final String v0 = "GnssNps"; // const-string v0, "GnssNps"
final String v1 = "APP caller name"; // const-string v1, "APP caller name"
android.util.Log .d ( v0,v1 );
/* .line 83 */
v0 = this.mNotifyManager;
(( com.android.server.location.GnssEventHandler$NotifyManager ) v0 ).updateCallerName ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/location/GnssEventHandler$NotifyManager;->updateCallerName(Ljava/lang/String;)V
/* .line 84 */
return;
} // .end method
public void handleCallerName ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "name" # Ljava/lang/String; */
/* .param p2, "event" # Ljava/lang/String; */
/* .line 88 */
final String v0 = "GnssNps"; // const-string v0, "GnssNps"
final String v1 = "APP caller name"; // const-string v1, "APP caller name"
android.util.Log .d ( v0,v1 );
/* .line 89 */
this.pkgName = p1;
/* .line 90 */
v0 = this.mNotifyManager;
(( com.android.server.location.GnssEventHandler$NotifyManager ) v0 ).updateCallerName ( p2 ); // invoke-virtual {v0, p2}, Lcom/android/server/location/GnssEventHandler$NotifyManager;->updateCallerName(Ljava/lang/String;)V
/* .line 91 */
return;
} // .end method
public void handleFix ( ) {
/* .locals 2 */
/* .line 67 */
final String v0 = "GnssNps"; // const-string v0, "GnssNps"
final String v1 = "fixed"; // const-string v1, "fixed"
android.util.Log .d ( v0,v1 );
/* .line 68 */
v0 = this.mNotifyManager;
(( com.android.server.location.GnssEventHandler$NotifyManager ) v0 ).removeNotification ( ); // invoke-virtual {v0}, Lcom/android/server/location/GnssEventHandler$NotifyManager;->removeNotification()V
/* .line 69 */
return;
} // .end method
public void handleLose ( ) {
/* .locals 2 */
/* .line 72 */
final String v0 = "GnssNps"; // const-string v0, "GnssNps"
final String v1 = "lose location"; // const-string v1, "lose location"
android.util.Log .d ( v0,v1 );
/* .line 73 */
v0 = this.mNotifyManager;
(( com.android.server.location.GnssEventHandler$NotifyManager ) v0 ).showNotification ( ); // invoke-virtual {v0}, Lcom/android/server/location/GnssEventHandler$NotifyManager;->showNotification()V
/* .line 74 */
return;
} // .end method
public void handleRecover ( ) {
/* .locals 2 */
/* .line 77 */
final String v0 = "GnssNps"; // const-string v0, "GnssNps"
final String v1 = "fix again"; // const-string v1, "fix again"
android.util.Log .d ( v0,v1 );
/* .line 78 */
v0 = this.mNotifyManager;
(( com.android.server.location.GnssEventHandler$NotifyManager ) v0 ).removeNotification ( ); // invoke-virtual {v0}, Lcom/android/server/location/GnssEventHandler$NotifyManager;->removeNotification()V
/* .line 79 */
return;
} // .end method
public void handleStart ( ) {
/* .locals 1 */
/* .line 55 */
v0 = this.mNotifyManager;
(( com.android.server.location.GnssEventHandler$NotifyManager ) v0 ).initNotification ( ); // invoke-virtual {v0}, Lcom/android/server/location/GnssEventHandler$NotifyManager;->initNotification()V
/* .line 56 */
v0 = this.mNotifyManager;
(( com.android.server.location.GnssEventHandler$NotifyManager ) v0 ).showNotification ( ); // invoke-virtual {v0}, Lcom/android/server/location/GnssEventHandler$NotifyManager;->showNotification()V
/* .line 57 */
return;
} // .end method
public void handleStop ( ) {
/* .locals 2 */
/* .line 60 */
/* iget-boolean v0, p0, Lcom/android/server/location/GnssEventHandler;->mBlur:Z */
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* .line 61 */
return;
/* .line 63 */
} // :cond_0
v0 = this.mNotifyManager;
(( com.android.server.location.GnssEventHandler$NotifyManager ) v0 ).removeNotification ( ); // invoke-virtual {v0}, Lcom/android/server/location/GnssEventHandler$NotifyManager;->removeNotification()V
/* .line 64 */
return;
} // .end method
