.class Lcom/android/server/location/MiuiBlurLocationManagerImpl$1;
.super Landroid/os/Handler;
.source "MiuiBlurLocationManagerImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/location/MiuiBlurLocationManagerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/location/MiuiBlurLocationManagerImpl;


# direct methods
.method constructor <init>(Lcom/android/server/location/MiuiBlurLocationManagerImpl;Landroid/os/Looper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/location/MiuiBlurLocationManagerImpl;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 86
    iput-object p1, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl$1;->this$0:Lcom/android/server/location/MiuiBlurLocationManagerImpl;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 14
    .param p1, "msg"    # Landroid/os/Message;

    .line 89
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 90
    .local v0, "bundle":Landroid/os/Bundle;
    if-nez v0, :cond_0

    .line 91
    const-string v1, "MiuiBlurLocationManager"

    const-string v2, "msg from client is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    return-void

    .line 94
    :cond_0
    iget v1, p1, Landroid/os/Message;->what:I

    const/4 v2, 0x0

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 104
    :pswitch_0
    iget-object v1, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl$1;->this$0:Lcom/android/server/location/MiuiBlurLocationManagerImpl;

    new-instance v11, Lmiui/security/SvStatusData;

    .line 105
    const-string v3, "key_svcount"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 106
    const-string v2, "key_svidWithFlags"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v5

    .line 107
    const-string v2, "key_cn0s"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getFloatArray(Ljava/lang/String;)[F

    move-result-object v6

    .line 108
    const-string v2, "key_svElevations"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getFloatArray(Ljava/lang/String;)[F

    move-result-object v7

    .line 109
    const-string v2, "key_svAzimuths"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getFloatArray(Ljava/lang/String;)[F

    move-result-object v8

    .line 110
    const-string v2, "key_svCarrierFreqs"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getFloatArray(Ljava/lang/String;)[F

    move-result-object v9

    .line 111
    const-string v2, "key_basebandCn0s"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getFloatArray(Ljava/lang/String;)[F

    move-result-object v10

    move-object v3, v11

    invoke-direct/range {v3 .. v10}, Lmiui/security/SvStatusData;-><init>(I[I[F[F[F[F[F)V

    invoke-static {v1, v11}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->-$$Nest$fputmSvStatusData(Lcom/android/server/location/MiuiBlurLocationManagerImpl;Lmiui/security/SvStatusData;)V

    .line 113
    goto :goto_0

    .line 96
    :pswitch_1
    const-string v1, "key_lac"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 97
    .local v1, "lac":I
    const-string v3, "key_cid"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 98
    .local v2, "cid":I
    const-string v3, "key_longitude"

    const-wide/16 v4, 0x0

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;D)D

    move-result-wide v10

    .line 99
    .local v10, "longitude":D
    const-string v3, "key_latitude"

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;D)D

    move-result-wide v12

    .line 100
    .local v12, "latitude":D
    iget-object v3, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl$1;->this$0:Lcom/android/server/location/MiuiBlurLocationManagerImpl;

    invoke-static {v3}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->-$$Nest$fgetmStashBlurLocationInfo(Lcom/android/server/location/MiuiBlurLocationManagerImpl;)Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;

    move-result-object v3

    move v4, v1

    move v5, v2

    move-wide v6, v10

    move-wide v8, v12

    invoke-virtual/range {v3 .. v9}, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->set(IIDD)V

    .line 102
    nop

    .line 117
    .end local v1    # "lac":I
    .end local v2    # "cid":I
    .end local v10    # "longitude":D
    .end local v12    # "latitude":D
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
