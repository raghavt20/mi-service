class com.android.server.location.GnssSmartSatelliteSwitchImpl$3 extends java.util.TimerTask {
	 /* .source "GnssSmartSatelliteSwitchImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->initOnce()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.location.GnssSmartSatelliteSwitchImpl this$0; //synthetic
/* # direct methods */
 com.android.server.location.GnssSmartSatelliteSwitchImpl$3 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/location/GnssSmartSatelliteSwitchImpl; */
/* .line 236 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 4 */
/* .line 239 */
v0 = this.this$0;
com.android.server.location.GnssSmartSatelliteSwitchImpl .-$$Nest$fgetmHandler ( v0 );
(( android.os.Handler ) v0 ).obtainMessage ( ); // invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;
/* .line 240 */
/* .local v0, "message":Landroid/os/Message; */
int v1 = 0; // const/4 v1, 0x0
/* iput v1, v0, Landroid/os/Message;->what:I */
/* .line 241 */
v2 = this.this$0;
com.android.server.location.GnssSmartSatelliteSwitchImpl .-$$Nest$fgetGPS_COLLECTOR ( v2 );
v2 = com.android.server.location.GnssSmartSatelliteSwitchImpl .-$$Nest$mgetAvgOfTopFourCN0 ( v2,v3 );
if ( v2 != null) { // if-eqz v2, :cond_0
	 v2 = this.this$0;
	 com.android.server.location.GnssSmartSatelliteSwitchImpl .-$$Nest$fgetBDS_COLLECTOR ( v2 );
	 v2 = 	 com.android.server.location.GnssSmartSatelliteSwitchImpl .-$$Nest$mgetAvgOfTopFourCN0 ( v2,v3 );
	 if ( v2 != null) { // if-eqz v2, :cond_0
		 /* .line 242 */
		 int v1 = 1; // const/4 v1, 0x1
		 /* iput v1, v0, Landroid/os/Message;->what:I */
		 /* .line 243 */
		 v1 = this.this$0;
		 com.android.server.location.GnssSmartSatelliteSwitchImpl .-$$Nest$fgetmHandler ( v1 );
		 (( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
		 /* .line 244 */
		 return;
		 /* .line 246 */
	 } // :cond_0
	 /* iput v1, v0, Landroid/os/Message;->what:I */
	 /* .line 247 */
	 v1 = this.this$0;
	 com.android.server.location.GnssSmartSatelliteSwitchImpl .-$$Nest$fgetmHandler ( v1 );
	 (( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
	 /* .line 249 */
	 return;
} // .end method
