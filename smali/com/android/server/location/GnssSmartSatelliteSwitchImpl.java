public class com.android.server.location.GnssSmartSatelliteSwitchImpl implements com.android.server.location.GnssSmartSatelliteSwitchStub {
	 /* .source "GnssSmartSatelliteSwitchImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/location/GnssSmartSatelliteSwitchImpl$BlockListHandler; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String BLOCK_GLO_QZSS_NAV_GAL;
private static final java.lang.String BLOCK_NONE;
private static final java.lang.String CLOUD_KEY_SWITCH_Enable;
private static final java.lang.String CLOUD_MODULE_GNSS_SMART_SATELLITE_SWITCH_CONFIG;
private static final Long EFFETCTIVE_NAVI_INTERVAL;
private static final java.lang.String GNSS_CONFIG_SUPPORT_BLOCK_LIST_PROP;
private static final Integer NAVI_STATUS_BLOCK_SATELLITE;
private static final Integer NAVI_STATUS_NORMAL;
private static final Integer NAVI_STATUS_START;
private static final Integer NAVI_STATUS_STOP;
private static final Integer SATTELLITE_POWERSAVE_LEVEL_0;
private static final Integer SATTELLITE_POWERSAVE_LEVEL_1;
private static final java.lang.String TAG;
private static final Long UPDATE_INTERVAL;
private static java.lang.String controlledAppList;
private static final Boolean isEnabled;
/* # instance fields */
private volatile java.util.concurrent.ConcurrentHashMap BDS_COLLECTOR;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/concurrent/ConcurrentHashMap<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/lang/Float;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private volatile java.util.concurrent.ConcurrentHashMap GPS_COLLECTOR;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/concurrent/ConcurrentHashMap<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/lang/Float;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Long effectiveTime;
private Boolean isInControlledList;
private java.util.concurrent.atomic.AtomicBoolean isInit;
private android.content.Context mContext;
private android.os.Handler mHandler;
private android.os.HandlerThread mHandlerThread;
private java.util.Timer mTimer;
private java.util.TimerTask mTimerTask;
private android.util.SparseArray naviTime;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Ljava/lang/Long;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Boolean startControllerListener;
private Long totalNaviTime;
/* # direct methods */
static java.util.concurrent.ConcurrentHashMap -$$Nest$fgetBDS_COLLECTOR ( com.android.server.location.GnssSmartSatelliteSwitchImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.BDS_COLLECTOR;
} // .end method
static java.util.concurrent.ConcurrentHashMap -$$Nest$fgetGPS_COLLECTOR ( com.android.server.location.GnssSmartSatelliteSwitchImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.GPS_COLLECTOR;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.android.server.location.GnssSmartSatelliteSwitchImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static android.os.Handler -$$Nest$fgetmHandler ( com.android.server.location.GnssSmartSatelliteSwitchImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHandler;
} // .end method
static Boolean -$$Nest$mgetAvgOfTopFourCN0 ( com.android.server.location.GnssSmartSatelliteSwitchImpl p0, java.util.Map p1 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1}, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->getAvgOfTopFourCN0(Ljava/util/Map;)Z */
} // .end method
static void -$$Nest$mupdateBlockList ( com.android.server.location.GnssSmartSatelliteSwitchImpl p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->updateBlockList(Ljava/lang/String;)V */
return;
} // .end method
static com.android.server.location.GnssSmartSatelliteSwitchImpl ( ) {
/* .locals 3 */
/* .line 35 */
final String v0 = "com.autonavi.minimap"; // const-string v0, "com.autonavi.minimap"
final String v1 = "com.tencent.map"; // const-string v1, "com.tencent.map"
final String v2 = "com.baidu.BaiduMap"; // const-string v2, "com.baidu.BaiduMap"
/* filled-new-array {v2, v0, v1}, [Ljava/lang/String; */
/* .line 49 */
v0 = com.android.server.location.GnssSmartSatelliteSwitchImpl .isCnVersion ( );
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 50 */
final String v0 = "persist.sys.gps.support_block_list_prop"; // const-string v0, "persist.sys.gps.support_block_list_prop"
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
/* nop */
} // :goto_0
com.android.server.location.GnssSmartSatelliteSwitchImpl.isEnabled = (v1!= 0);
/* .line 49 */
return;
} // .end method
public com.android.server.location.GnssSmartSatelliteSwitchImpl ( ) {
/* .locals 2 */
/* .line 33 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 46 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.GPS_COLLECTOR = v0;
/* .line 47 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.BDS_COLLECTOR = v0;
/* .line 51 */
/* new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V */
this.isInit = v0;
/* .line 52 */
/* iput-boolean v1, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->startControllerListener:Z */
/* .line 65 */
/* iput-boolean v1, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->isInControlledList:Z */
/* .line 67 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
this.naviTime = v0;
/* .line 73 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->effectiveTime:J */
/* .line 74 */
/* iput-wide v0, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->totalNaviTime:J */
return;
} // .end method
private Boolean getAvgOfTopFourCN0 ( java.util.Map p0 ) {
/* .locals 8 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/lang/Float;", */
/* ">;)Z" */
/* } */
} // .end annotation
/* .line 155 */
v0 = /* .local p1, "collector":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/Float;>;" */
/* .line 156 */
/* .local v0, "size":I */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_4
int v2 = 4; // const/4 v2, 0x4
/* if-ge v0, v2, :cond_0 */
/* .line 158 */
} // :cond_0
int v3 = 0; // const/4 v3, 0x0
/* .line 159 */
/* .local v3, "sum":F */
/* new-instance v4, Ljava/util/ArrayList; */
/* invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V */
/* .line 160 */
/* .local v4, "vals":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;" */
v6 = } // :goto_0
if ( v6 != null) { // if-eqz v6, :cond_1
/* check-cast v6, Ljava/lang/Integer; */
/* .line 161 */
/* .local v6, "key":Ljava/lang/Integer; */
/* check-cast v7, Ljava/lang/Float; */
/* .line 162 */
} // .end local v6 # "key":Ljava/lang/Integer;
/* .line 163 */
} // :cond_1
/* new-instance v5, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl$2; */
/* invoke-direct {v5, p0}, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl$2;-><init>(Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;)V */
java.util.Collections .sort ( v4,v5 );
/* .line 169 */
int v5 = 0; // const/4 v5, 0x0
/* .local v5, "i":I */
} // :goto_1
/* if-ge v5, v2, :cond_2 */
/* .line 170 */
/* check-cast v6, Ljava/lang/Float; */
v6 = (( java.lang.Float ) v6 ).floatValue ( ); // invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F
/* add-float/2addr v3, v6 */
/* .line 169 */
/* add-int/lit8 v5, v5, 0x1 */
/* .line 172 */
} // .end local v5 # "i":I
} // :cond_2
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "getAvgOfTopFourCN0: "; // const-string v5, "getAvgOfTopFourCN0: "
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/high16 v5, 0x40800000 # 4.0f */
/* div-float v6, v3, v5 */
(( java.lang.StringBuilder ) v2 ).append ( v6 ); // invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v6 = "GnssSmartSatelliteSwitchImpl"; // const-string v6, "GnssSmartSatelliteSwitchImpl"
android.util.Log .d ( v6,v2 );
/* .line 173 */
/* div-float v2, v3, v5 */
/* const/high16 v5, 0x42200000 # 40.0f */
/* cmpl-float v2, v2, v5 */
/* if-lez v2, :cond_3 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_3
/* .line 157 */
} // .end local v3 # "sum":F
} // .end local v4 # "vals":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
} // :cond_4
} // :goto_2
} // .end method
private Long getEffectiveTime ( ) {
/* .locals 2 */
/* .line 322 */
/* iget-wide v0, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->effectiveTime:J */
/* return-wide v0 */
} // .end method
private void handleGnssStatus ( android.location.GnssStatus p0 ) {
/* .locals 5 */
/* .param p1, "status" # Landroid/location/GnssStatus; */
/* .line 207 */
v0 = this.isInit;
v0 = (( java.util.concurrent.atomic.AtomicBoolean ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
/* if-nez v0, :cond_0 */
/* .line 208 */
return;
/* .line 210 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 211 */
/* .local v0, "constellationType":I */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
v2 = (( android.location.GnssStatus ) p1 ).getSatelliteCount ( ); // invoke-virtual {p1}, Landroid/location/GnssStatus;->getSatelliteCount()I
/* if-ge v1, v2, :cond_1 */
/* .line 212 */
v0 = (( android.location.GnssStatus ) p1 ).getConstellationType ( v1 ); // invoke-virtual {p1, v1}, Landroid/location/GnssStatus;->getConstellationType(I)I
/* .line 213 */
/* sparse-switch v0, :sswitch_data_0 */
/* .line 215 */
/* :sswitch_0 */
v2 = this.BDS_COLLECTOR;
v3 = (( android.location.GnssStatus ) p1 ).getSvid ( v1 ); // invoke-virtual {p1, v1}, Landroid/location/GnssStatus;->getSvid(I)I
java.lang.Integer .valueOf ( v3 );
v4 = (( android.location.GnssStatus ) p1 ).getCn0DbHz ( v1 ); // invoke-virtual {p1, v1}, Landroid/location/GnssStatus;->getCn0DbHz(I)F
java.lang.Float .valueOf ( v4 );
(( java.util.concurrent.ConcurrentHashMap ) v2 ).put ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 216 */
/* .line 218 */
/* :sswitch_1 */
v2 = this.GPS_COLLECTOR;
v3 = (( android.location.GnssStatus ) p1 ).getSvid ( v1 ); // invoke-virtual {p1, v1}, Landroid/location/GnssStatus;->getSvid(I)I
java.lang.Integer .valueOf ( v3 );
v4 = (( android.location.GnssStatus ) p1 ).getCn0DbHz ( v1 ); // invoke-virtual {p1, v1}, Landroid/location/GnssStatus;->getCn0DbHz(I)F
java.lang.Float .valueOf ( v4 );
(( java.util.concurrent.ConcurrentHashMap ) v2 ).put ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 219 */
/* nop */
/* .line 211 */
} // :goto_1
/* add-int/lit8 v1, v1, 0x1 */
/* .line 224 */
} // .end local v1 # "i":I
} // :cond_1
return;
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x1 -> :sswitch_1 */
/* 0x5 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
private void initCollector ( ) {
/* .locals 5 */
/* .line 257 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v0 */
/* .line 258 */
/* .local v0, "currentTimemills":J */
v2 = this.naviTime;
/* if-nez v2, :cond_0 */
/* .line 259 */
/* new-instance v2, Landroid/util/SparseArray; */
/* invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V */
this.naviTime = v2;
/* .line 261 */
} // :cond_0
v2 = this.naviTime;
int v3 = 0; // const/4 v3, 0x0
java.lang.Long .valueOf ( v0,v1 );
(( android.util.SparseArray ) v2 ).put ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 262 */
v2 = this.naviTime;
int v3 = 1; // const/4 v3, 0x1
java.lang.Long .valueOf ( v0,v1 );
(( android.util.SparseArray ) v2 ).put ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 263 */
v2 = this.naviTime;
int v3 = 2; // const/4 v3, 0x2
java.lang.Long .valueOf ( v0,v1 );
(( android.util.SparseArray ) v2 ).put ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 264 */
v2 = this.naviTime;
int v3 = 3; // const/4 v3, 0x3
java.lang.Long .valueOf ( v0,v1 );
(( android.util.SparseArray ) v2 ).put ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 265 */
/* const-wide/16 v2, 0x0 */
/* iput-wide v2, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->effectiveTime:J */
/* .line 266 */
return;
} // .end method
private void initOnce ( ) {
/* .locals 9 */
/* .line 228 */
v0 = this.isInit;
v0 = (( java.util.concurrent.atomic.AtomicBoolean ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 229 */
return;
/* .line 230 */
} // :cond_0
/* invoke-direct {p0}, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->initCollector()V */
/* .line 231 */
v0 = this.naviTime;
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v1 */
java.lang.Long .valueOf ( v1,v2 );
int v2 = 1; // const/4 v2, 0x1
(( android.util.SparseArray ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 232 */
/* new-instance v0, Landroid/os/HandlerThread; */
final String v1 = "mGnssSmartSatelliteHandler"; // const-string v1, "mGnssSmartSatelliteHandler"
/* invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
this.mHandlerThread = v0;
/* .line 233 */
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 234 */
/* new-instance v0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl$BlockListHandler; */
v1 = this.mHandlerThread;
(( android.os.HandlerThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl$BlockListHandler;-><init>(Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 235 */
/* new-instance v0, Ljava/util/Timer; */
/* invoke-direct {v0}, Ljava/util/Timer;-><init>()V */
this.mTimer = v0;
/* .line 236 */
/* new-instance v4, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl$3; */
/* invoke-direct {v4, p0}, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl$3;-><init>(Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;)V */
this.mTimerTask = v4;
/* .line 251 */
v3 = this.mTimer;
/* const-wide/16 v5, 0x4e20 */
/* const-wide/32 v7, 0x493e0 */
/* invoke-virtual/range {v3 ..v8}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V */
/* .line 252 */
final String v0 = "GnssSmartSatelliteSwitchImpl"; // const-string v0, "GnssSmartSatelliteSwitchImpl"
final String v1 = "init"; // const-string v1, "init"
android.util.Log .d ( v0,v1 );
/* .line 253 */
v0 = this.isInit;
(( java.util.concurrent.atomic.AtomicBoolean ) v0 ).set ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
/* .line 254 */
return;
} // .end method
private static Boolean isCnVersion ( ) {
/* .locals 2 */
/* .line 119 */
final String v0 = "ro.miui.build.region"; // const-string v0, "ro.miui.build.region"
android.os.SystemProperties .get ( v0 );
final String v1 = "CN"; // const-string v1, "CN"
v0 = (( java.lang.String ) v1 ).equalsIgnoreCase ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
} // .end method
private Boolean isEffective ( Long p0, Long p1 ) {
/* .locals 4 */
/* .param p1, "var1" # J */
/* .param p3, "var2" # J */
/* .line 299 */
/* sub-long v0, p1, p3 */
/* const-wide/32 v2, 0x493e0 */
/* cmp-long v0, v0, v2 */
/* if-lez v0, :cond_0 */
/* .line 300 */
int v0 = 1; // const/4 v0, 0x1
/* .line 302 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private void sendToGnssEnent ( ) {
/* .locals 10 */
/* .line 328 */
try { // :try_start_0
v0 = this.naviTime;
/* invoke-direct {p0, v0}, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->updateEffectiveTime(Landroid/util/SparseArray;)V */
/* .line 329 */
/* iget-wide v0, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->totalNaviTime:J */
/* const-wide/16 v2, 0x0 */
/* cmp-long v0, v0, v2 */
/* if-lez v0, :cond_0 */
/* .line 330 */
com.android.server.location.gnss.GnssEventTrackingStub .getInstance ( );
/* iget-wide v5, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->totalNaviTime:J */
/* .line 331 */
/* invoke-direct {p0}, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->getEffectiveTime()J */
/* move-result-wide v7 */
int v9 = 0; // const/4 v9, 0x0
/* invoke-interface/range {v4 ..v9}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->recordSatelliteBlockListChanged(JJLjava/lang/String;)V */
/* .line 333 */
} // :cond_0
v0 = this.naviTime;
(( android.util.SparseArray ) v0 ).clear ( ); // invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V
/* .line 334 */
/* iput-wide v2, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->effectiveTime:J */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 337 */
/* .line 335 */
/* :catch_0 */
/* move-exception v0 */
/* .line 336 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "GnssSmartSatelliteSwitchImpl"; // const-string v1, "GnssSmartSatelliteSwitchImpl"
(( java.lang.Exception ) v0 ).getMessage ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
android.util.Log .d ( v1,v2 );
/* .line 338 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private synchronized void updateBlockList ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p1, "mBlockList" # Ljava/lang/String; */
/* monitor-enter p0 */
/* .line 178 */
try { // :try_start_0
final String v0 = ","; // const-string v0, ","
(( java.lang.String ) p1 ).split ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 179 */
/* .local v0, "blockLists":[Ljava/lang/String; */
/* array-length v1, v0 */
int v2 = 2; // const/4 v2, 0x2
/* rem-int/2addr v1, v2 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 180 */
final String v1 = ""; // const-string v1, ""
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move-object p1, v1 */
/* .line 183 */
} // .end local p0 # "this":Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;
} // :cond_0
try { // :try_start_1
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v3 = "gnss_satellite_blocklist"; // const-string v3, "gnss_satellite_blocklist"
android.provider.Settings$Global .getString ( v1,v3 );
/* .line 185 */
/* .local v1, "blockListNow":Ljava/lang/String; */
/* if-nez v1, :cond_1 */
/* .line 186 */
final String v3 = ""; // const-string v3, ""
/* move-object v1, v3 */
/* .line 188 */
} // :cond_1
v3 = (( java.lang.String ) v1 ).equals ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 189 */
/* monitor-exit p0 */
return;
/* .line 191 */
/* .restart local p0 # "this":Lcom/android/server/location/GnssSmartSatelliteSwitchImpl; */
} // :cond_2
try { // :try_start_2
v3 = this.mContext;
/* .line 192 */
(( android.content.Context ) v3 ).getContentResolver ( ); // invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v4 = "gnss_satellite_blocklist"; // const-string v4, "gnss_satellite_blocklist"
/* .line 191 */
android.provider.Settings$Global .putString ( v3,v4,p1 );
/* .line 194 */
final String v3 = "3,0,4,0,6,0,7,0"; // const-string v3, "3,0,4,0,6,0,7,0"
v3 = (( java.lang.String ) v3 ).equals ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 195 */
v2 = this.naviTime;
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v3 */
java.lang.Long .valueOf ( v3,v4 );
int v4 = 3; // const/4 v4, 0x3
(( android.util.SparseArray ) v2 ).put ( v4, v3 ); // invoke-virtual {v2, v4, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 197 */
} // .end local p0 # "this":Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;
} // :cond_3
v3 = this.naviTime;
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v4 */
java.lang.Long .valueOf ( v4,v5 );
(( android.util.SparseArray ) v3 ).put ( v2, v4 ); // invoke-virtual {v3, v2, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 199 */
} // :goto_0
final String v2 = "GnssSmartSatelliteSwitchImpl"; // const-string v2, "GnssSmartSatelliteSwitchImpl"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "update blockList:" */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v3 );
/* .line 200 */
v2 = this.naviTime;
/* invoke-direct {p0, v2}, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->updateEffectiveTime(Landroid/util/SparseArray;)V */
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 203 */
} // .end local v1 # "blockListNow":Ljava/lang/String;
/* .line 201 */
/* :catch_0 */
/* move-exception v1 */
/* .line 202 */
/* .local v1, "e":Ljava/lang/Exception; */
try { // :try_start_3
final String v2 = "GnssSmartSatelliteSwitchImpl"; // const-string v2, "GnssSmartSatelliteSwitchImpl"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "update blocklist fail, cause :" */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v1 ).getCause ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v3 );
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 204 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_1
/* monitor-exit p0 */
return;
/* .line 177 */
} // .end local v0 # "blockLists":[Ljava/lang/String;
} // .end local p1 # "mBlockList":Ljava/lang/String;
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
private synchronized void updateEffectiveTime ( android.util.SparseArray p0 ) {
/* .locals 12 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/util/SparseArray<", */
/* "Ljava/lang/Long;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .local p1, "naviTimeCollector":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/Long;>;" */
/* monitor-enter p0 */
/* .line 307 */
int v0 = 1; // const/4 v0, 0x1
try { // :try_start_0
(( android.util.SparseArray ) p1 ).valueAt ( v0 ); // invoke-virtual {p1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/Long; */
(( java.lang.Long ) v0 ).longValue ( ); // invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
/* move-result-wide v0 */
/* .line 308 */
/* .local v0, "beginTime":J */
int v2 = 0; // const/4 v2, 0x0
(( android.util.SparseArray ) p1 ).valueAt ( v2 ); // invoke-virtual {p1, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v2, Ljava/lang/Long; */
(( java.lang.Long ) v2 ).longValue ( ); // invoke-virtual {v2}, Ljava/lang/Long;->longValue()J
/* move-result-wide v2 */
/* .line 309 */
/* .local v2, "stopTime":J */
int v4 = 3; // const/4 v4, 0x3
(( android.util.SparseArray ) p1 ).valueAt ( v4 ); // invoke-virtual {p1, v4}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v4, Ljava/lang/Long; */
(( java.lang.Long ) v4 ).longValue ( ); // invoke-virtual {v4}, Ljava/lang/Long;->longValue()J
/* move-result-wide v4 */
/* .line 310 */
/* .local v4, "blockTime":J */
int v6 = 2; // const/4 v6, 0x2
(( android.util.SparseArray ) p1 ).valueAt ( v6 ); // invoke-virtual {p1, v6}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v6, Ljava/lang/Long; */
(( java.lang.Long ) v6 ).longValue ( ); // invoke-virtual {v6}, Ljava/lang/Long;->longValue()J
/* move-result-wide v6 */
/* .line 311 */
/* .local v6, "normalTime":J */
v8 = /* invoke-direct {p0, v2, v3, v0, v1}, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->isEffective(JJ)Z */
if ( v8 != null) { // if-eqz v8, :cond_0
/* sub-long v8, v2, v0 */
} // :cond_0
/* const-wide/16 v8, 0x0 */
} // :goto_0
/* iput-wide v8, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->totalNaviTime:J */
/* .line 312 */
/* cmp-long v8, v6, v4 */
/* if-ltz v8, :cond_1 */
/* .line 313 */
/* iget-wide v8, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->effectiveTime:J */
/* sub-long v10, v6, v4 */
/* add-long/2addr v8, v10 */
/* iput-wide v8, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->effectiveTime:J */
/* .line 314 */
} // .end local p0 # "this":Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;
} // :cond_1
/* sub-long v8, v4, v6 */
/* const-wide/16 v10, 0x4e20 */
/* cmp-long v8, v8, v10 */
/* if-ltz v8, :cond_2 */
/* .line 315 */
/* iget-wide v8, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->effectiveTime:J */
/* sub-long v10, v2, v4 */
/* add-long/2addr v8, v10 */
/* iput-wide v8, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->effectiveTime:J */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 319 */
} // :goto_1
/* monitor-exit p0 */
return;
/* .line 317 */
} // :cond_2
/* monitor-exit p0 */
return;
/* .line 306 */
} // .end local v0 # "beginTime":J
} // .end local v2 # "stopTime":J
} // .end local v4 # "blockTime":J
} // .end local v6 # "normalTime":J
} // .end local p1 # "naviTimeCollector":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/Long;>;"
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
/* # virtual methods */
public void addCloudControllListener ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 78 */
/* iget-boolean v0, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->startControllerListener:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 79 */
return;
/* .line 81 */
} // :cond_0
/* if-nez p1, :cond_1 */
/* .line 82 */
return;
/* .line 84 */
} // :cond_1
this.mContext = p1;
/* .line 85 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 86 */
android.provider.MiuiSettings$SettingsCloudData .getCloudDataNotifyUri ( );
/* new-instance v2, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl$1; */
int v3 = 0; // const/4 v3, 0x0
/* invoke-direct {v2, p0, v3}, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl$1;-><init>(Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;Landroid/os/Handler;)V */
/* .line 85 */
int v3 = 1; // const/4 v3, 0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2 ); // invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 108 */
final String v0 = "GnssSmartSatelliteSwitchImpl"; // const-string v0, "GnssSmartSatelliteSwitchImpl"
final String v1 = "register cloud controller listener"; // const-string v1, "register cloud controller listener"
android.util.Log .i ( v0,v1 );
/* .line 109 */
/* iput-boolean v3, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->startControllerListener:Z */
/* .line 110 */
return;
} // .end method
public void isControlled ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "workSource" # Ljava/lang/String; */
/* .line 139 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v1 = com.android.server.location.GnssSmartSatelliteSwitchImpl.controlledAppList;
/* array-length v2, v1 */
/* if-ge v0, v2, :cond_1 */
/* .line 140 */
/* aget-object v1, v1, v0 */
v1 = (( java.lang.String ) p1 ).contains ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 141 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->isInControlledList:Z */
/* .line 139 */
} // :cond_0
/* add-int/lit8 v0, v0, 0x1 */
/* .line 144 */
} // .end local v0 # "i":I
} // :cond_1
/* iget-boolean v0, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->isInControlledList:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 145 */
/* invoke-direct {p0}, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->initOnce()V */
/* .line 147 */
} // :cond_2
return;
} // .end method
public synchronized void resetBlockList ( ) {
/* .locals 4 */
/* monitor-enter p0 */
/* .line 270 */
try { // :try_start_0
v0 = this.isInit;
v0 = (( java.util.concurrent.atomic.AtomicBoolean ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 272 */
try { // :try_start_1
v0 = this.naviTime;
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v1 */
java.lang.Long .valueOf ( v1,v2 );
int v2 = 0; // const/4 v2, 0x0
(( android.util.SparseArray ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 273 */
/* invoke-direct {p0}, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->sendToGnssEnent()V */
/* .line 274 */
v0 = this.mTimerTask;
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 275 */
(( java.util.TimerTask ) v0 ).cancel ( ); // invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z
/* .line 276 */
this.mTimerTask = v1;
/* .line 278 */
} // .end local p0 # "this":Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;
} // :cond_0
v0 = this.mTimer;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 279 */
(( java.util.Timer ) v0 ).cancel ( ); // invoke-virtual {v0}, Ljava/util/Timer;->cancel()V
/* .line 280 */
v0 = this.mTimer;
(( java.util.Timer ) v0 ).purge ( ); // invoke-virtual {v0}, Ljava/util/Timer;->purge()I
/* .line 281 */
this.mTimer = v1;
/* .line 283 */
} // :cond_1
v0 = this.mHandlerThread;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 284 */
(( android.os.HandlerThread ) v0 ).getLooper ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
(( android.os.Looper ) v0 ).quitSafely ( ); // invoke-virtual {v0}, Landroid/os/Looper;->quitSafely()V
/* .line 286 */
} // :cond_2
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "gnss_satellite_blocklist"; // const-string v1, "gnss_satellite_blocklist"
final String v3 = ""; // const-string v3, ""
android.provider.Settings$Global .putString ( v0,v1,v3 );
/* .line 288 */
final String v0 = "GnssSmartSatelliteSwitchImpl"; // const-string v0, "GnssSmartSatelliteSwitchImpl"
final String v1 = "reset Blocklist"; // const-string v1, "reset Blocklist"
android.util.Log .d ( v0,v1 );
/* .line 289 */
/* iput-boolean v2, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->isInControlledList:Z */
/* .line 290 */
v0 = this.isInit;
(( java.util.concurrent.atomic.AtomicBoolean ) v0 ).set ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 293 */
/* .line 291 */
/* :catch_0 */
/* move-exception v0 */
/* .line 292 */
/* .local v0, "e":Ljava/lang/Exception; */
try { // :try_start_2
final String v1 = "GnssSmartSatelliteSwitchImpl"; // const-string v1, "GnssSmartSatelliteSwitchImpl"
android.util.Log .getStackTraceString ( v0 );
android.util.Log .d ( v1,v2 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 295 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_3
} // :goto_0
/* monitor-exit p0 */
return;
/* .line 269 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* throw v0 */
} // .end method
public void smartSatelliteSwitchMonitor ( android.location.GnssStatus p0 ) {
/* .locals 1 */
/* .param p1, "gnssStatus" # Landroid/location/GnssStatus; */
/* .line 124 */
v0 = this.mContext;
/* if-nez v0, :cond_0 */
/* .line 125 */
return;
/* .line 127 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->isInControlledList:Z */
/* if-nez v0, :cond_1 */
/* .line 128 */
return;
/* .line 130 */
} // :cond_1
/* invoke-direct {p0, p1}, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->handleGnssStatus(Landroid/location/GnssStatus;)V */
/* .line 131 */
return;
} // .end method
public Boolean supportConstellationBlockListFeature ( ) {
/* .locals 1 */
/* .line 114 */
/* sget-boolean v0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->isEnabled:Z */
} // .end method
