.class public Lcom/android/server/location/GnssCollectData;
.super Ljava/lang/Object;
.source "GnssCollectData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/location/GnssCollectData$CollectDbEntry;
    }
.end annotation


# instance fields
.field private B1Top4MeanCn0:D

.field private L1Top4MeanCn0:D

.field private L5Top4MeanCn0:D

.field private PDRNumber:I

.field private SAPNumber:I

.field private TTFF:J

.field private id:I

.field private loseTimes:I

.field private runTime:J

.field private startTime:J

.field private totalNumber:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/location/GnssCollectData;->startTime:J

    .line 23
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/android/server/location/GnssCollectData;->TTFF:J

    .line 24
    iput-wide v0, p0, Lcom/android/server/location/GnssCollectData;->runTime:J

    .line 25
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/location/GnssCollectData;->loseTimes:I

    .line 26
    iput v0, p0, Lcom/android/server/location/GnssCollectData;->SAPNumber:I

    .line 27
    iput v0, p0, Lcom/android/server/location/GnssCollectData;->PDRNumber:I

    .line 28
    iput v0, p0, Lcom/android/server/location/GnssCollectData;->totalNumber:I

    .line 29
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/location/GnssCollectData;->L1Top4MeanCn0:D

    .line 30
    iput-wide v0, p0, Lcom/android/server/location/GnssCollectData;->L5Top4MeanCn0:D

    .line 31
    iput-wide v0, p0, Lcom/android/server/location/GnssCollectData;->B1Top4MeanCn0:D

    .line 32
    return-void
.end method

.method public constructor <init>(JJJIIIIDDD)V
    .locals 15
    .param p1, "startTime"    # J
    .param p3, "TTFF"    # J
    .param p5, "runTime"    # J
    .param p7, "loseTimes"    # I
    .param p8, "SAPNumber"    # I
    .param p9, "PDRNumber"    # I
    .param p10, "totalNumber"    # I
    .param p11, "L1Top4MeanCn0"    # D
    .param p13, "L5Top4MeanCn0"    # D
    .param p15, "B1Top4MeanCn0"    # D

    .line 36
    move-object v0, p0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    move-wide/from16 v1, p1

    iput-wide v1, v0, Lcom/android/server/location/GnssCollectData;->startTime:J

    .line 38
    move-wide/from16 v3, p3

    iput-wide v3, v0, Lcom/android/server/location/GnssCollectData;->TTFF:J

    .line 39
    move-wide/from16 v5, p5

    iput-wide v5, v0, Lcom/android/server/location/GnssCollectData;->runTime:J

    .line 40
    move/from16 v7, p7

    iput v7, v0, Lcom/android/server/location/GnssCollectData;->loseTimes:I

    .line 41
    move/from16 v8, p8

    iput v8, v0, Lcom/android/server/location/GnssCollectData;->SAPNumber:I

    .line 42
    move/from16 v9, p9

    iput v9, v0, Lcom/android/server/location/GnssCollectData;->PDRNumber:I

    .line 43
    move/from16 v10, p10

    iput v10, v0, Lcom/android/server/location/GnssCollectData;->totalNumber:I

    .line 44
    move-wide/from16 v11, p11

    iput-wide v11, v0, Lcom/android/server/location/GnssCollectData;->L1Top4MeanCn0:D

    .line 45
    move-wide/from16 v13, p13

    iput-wide v13, v0, Lcom/android/server/location/GnssCollectData;->L5Top4MeanCn0:D

    .line 46
    move-wide/from16 v1, p15

    iput-wide v1, v0, Lcom/android/server/location/GnssCollectData;->B1Top4MeanCn0:D

    .line 47
    return-void
.end method


# virtual methods
.method public getB1Top4MeanCn0()D
    .locals 2

    .line 90
    iget-wide v0, p0, Lcom/android/server/location/GnssCollectData;->B1Top4MeanCn0:D

    return-wide v0
.end method

.method public getId()I
    .locals 1

    .line 50
    iget v0, p0, Lcom/android/server/location/GnssCollectData;->id:I

    return v0
.end method

.method public getL1Top4MeanCn0()D
    .locals 2

    .line 82
    iget-wide v0, p0, Lcom/android/server/location/GnssCollectData;->L1Top4MeanCn0:D

    return-wide v0
.end method

.method public getL5Top4MeanCn0()D
    .locals 2

    .line 86
    iget-wide v0, p0, Lcom/android/server/location/GnssCollectData;->L5Top4MeanCn0:D

    return-wide v0
.end method

.method public getLoseTimes()I
    .locals 1

    .line 66
    iget v0, p0, Lcom/android/server/location/GnssCollectData;->loseTimes:I

    return v0
.end method

.method public getPdrNumber()I
    .locals 1

    .line 74
    iget v0, p0, Lcom/android/server/location/GnssCollectData;->PDRNumber:I

    return v0
.end method

.method public getRunTime()J
    .locals 2

    .line 62
    iget-wide v0, p0, Lcom/android/server/location/GnssCollectData;->runTime:J

    return-wide v0
.end method

.method public getSapNumber()I
    .locals 1

    .line 70
    iget v0, p0, Lcom/android/server/location/GnssCollectData;->SAPNumber:I

    return v0
.end method

.method public getStartTime()J
    .locals 2

    .line 54
    iget-wide v0, p0, Lcom/android/server/location/GnssCollectData;->startTime:J

    return-wide v0
.end method

.method public getTotalNumber()I
    .locals 1

    .line 78
    iget v0, p0, Lcom/android/server/location/GnssCollectData;->totalNumber:I

    return v0
.end method

.method public getTtff()J
    .locals 2

    .line 58
    iget-wide v0, p0, Lcom/android/server/location/GnssCollectData;->TTFF:J

    return-wide v0
.end method

.method public setB1Top4MeanCn0(D)V
    .locals 0
    .param p1, "B1Top4MeanCn0"    # D

    .line 134
    iput-wide p1, p0, Lcom/android/server/location/GnssCollectData;->B1Top4MeanCn0:D

    .line 135
    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .line 94
    iput p1, p0, Lcom/android/server/location/GnssCollectData;->id:I

    .line 95
    return-void
.end method

.method public setL1Top4MeanCn0(D)V
    .locals 0
    .param p1, "L1Top4MeanCn0"    # D

    .line 126
    iput-wide p1, p0, Lcom/android/server/location/GnssCollectData;->L1Top4MeanCn0:D

    .line 127
    return-void
.end method

.method public setL5Top4MeanCn0(D)V
    .locals 0
    .param p1, "L5Top4MeanCn0"    # D

    .line 130
    iput-wide p1, p0, Lcom/android/server/location/GnssCollectData;->L5Top4MeanCn0:D

    .line 131
    return-void
.end method

.method public setLoseTimes(I)V
    .locals 0
    .param p1, "loseTimes"    # I

    .line 110
    iput p1, p0, Lcom/android/server/location/GnssCollectData;->loseTimes:I

    .line 111
    return-void
.end method

.method public setPdrNumber(I)V
    .locals 0
    .param p1, "PDRNumber"    # I

    .line 118
    iput p1, p0, Lcom/android/server/location/GnssCollectData;->PDRNumber:I

    .line 119
    return-void
.end method

.method public setRunTime(J)V
    .locals 2
    .param p1, "runTime"    # J

    .line 106
    invoke-virtual {p0}, Lcom/android/server/location/GnssCollectData;->getRunTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/location/GnssCollectData;->runTime:J

    .line 107
    return-void
.end method

.method public setSapNumber(I)V
    .locals 0
    .param p1, "SAPNumber"    # I

    .line 114
    iput p1, p0, Lcom/android/server/location/GnssCollectData;->SAPNumber:I

    .line 115
    return-void
.end method

.method public setStartTime(J)V
    .locals 0
    .param p1, "startTime"    # J

    .line 98
    iput-wide p1, p0, Lcom/android/server/location/GnssCollectData;->startTime:J

    .line 99
    return-void
.end method

.method public setTotalNumber(I)V
    .locals 0
    .param p1, "totalNumber"    # I

    .line 122
    iput p1, p0, Lcom/android/server/location/GnssCollectData;->totalNumber:I

    .line 123
    return-void
.end method

.method public setTtff(J)V
    .locals 0
    .param p1, "TTFF"    # J

    .line 102
    iput-wide p1, p0, Lcom/android/server/location/GnssCollectData;->TTFF:J

    .line 103
    return-void
.end method
