.class Lcom/android/server/location/GnssMockLocationOptImpl$1;
.super Ljava/lang/Object;
.source "GnssMockLocationOptImpl.java"

# interfaces
.implements Lcom/android/server/location/Order$OnChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/location/GnssMockLocationOptImpl;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/location/GnssMockLocationOptImpl;


# direct methods
.method constructor <init>(Lcom/android/server/location/GnssMockLocationOptImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/location/GnssMockLocationOptImpl;

    .line 138
    iput-object p1, p0, Lcom/android/server/location/GnssMockLocationOptImpl$1;->this$0:Lcom/android/server/location/GnssMockLocationOptImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChange()V
    .locals 3

    .line 141
    iget-object v0, p0, Lcom/android/server/location/GnssMockLocationOptImpl$1;->this$0:Lcom/android/server/location/GnssMockLocationOptImpl;

    invoke-static {v0}, Lcom/android/server/location/GnssMockLocationOptImpl;->-$$Nest$fgetmRecordMockSchedule(Lcom/android/server/location/GnssMockLocationOptImpl;)Ljava/util/Map;

    move-result-object v0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {}, Lcom/android/server/location/Order;->getFlag()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    iget-object v0, p0, Lcom/android/server/location/GnssMockLocationOptImpl$1;->this$0:Lcom/android/server/location/GnssMockLocationOptImpl;

    invoke-static {v0}, Lcom/android/server/location/GnssMockLocationOptImpl;->-$$Nest$fgetD(Lcom/android/server/location/GnssMockLocationOptImpl;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GnssMockLocationOpt"

    const-string v1, "callback set function"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    :cond_0
    return-void
.end method
