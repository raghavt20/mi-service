.class Lcom/android/server/location/MtkGnssPowerSaveImpl$PowerSaveReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MtkGnssPowerSaveImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/location/MtkGnssPowerSaveImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PowerSaveReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/location/MtkGnssPowerSaveImpl;


# direct methods
.method private constructor <init>(Lcom/android/server/location/MtkGnssPowerSaveImpl;)V
    .locals 0

    .line 518
    iput-object p1, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl$PowerSaveReceiver;->this$0:Lcom/android/server/location/MtkGnssPowerSaveImpl;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/location/MtkGnssPowerSaveImpl;Lcom/android/server/location/MtkGnssPowerSaveImpl$PowerSaveReceiver-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/location/MtkGnssPowerSaveImpl$PowerSaveReceiver;-><init>(Lcom/android/server/location/MtkGnssPowerSaveImpl;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 521
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 522
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_4

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    .line 525
    :cond_0
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 526
    .local v1, "msg":Landroid/os/Message;
    const/4 v2, 0x0

    iput v2, v1, Landroid/os/Message;->what:I

    .line 527
    const-string v3, "POWER_SAVE_MODE_OPEN"

    invoke-virtual {p2, v3, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 528
    .local v3, "powerSaveStatus":Z
    iget-object v4, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl$PowerSaveReceiver;->this$0:Lcom/android/server/location/MtkGnssPowerSaveImpl;

    invoke-static {v4}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->-$$Nest$ml5Device(Lcom/android/server/location/MtkGnssPowerSaveImpl;)Z

    move-result v4

    .line 529
    .local v4, "isL5Device":Z
    const-string v5, "miui.intent.action.POWER_SAVE_MODE_CHANGED"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 530
    if-eqz v4, :cond_1

    .line 531
    const/4 v2, 0x2

    iput v2, v1, Landroid/os/Message;->what:I

    goto :goto_0

    .line 532
    :cond_1
    if-eqz v3, :cond_2

    .line 533
    const/4 v2, 0x1

    iput v2, v1, Landroid/os/Message;->what:I

    goto :goto_0

    .line 535
    :cond_2
    iput v2, v1, Landroid/os/Message;->what:I

    .line 537
    :goto_0
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 541
    iget-object v2, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl$PowerSaveReceiver;->this$0:Lcom/android/server/location/MtkGnssPowerSaveImpl;

    invoke-static {v2}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->-$$Nest$fgetmHandler(Lcom/android/server/location/MtkGnssPowerSaveImpl;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 542
    return-void

    .line 539
    :cond_3
    return-void

    .line 523
    .end local v1    # "msg":Landroid/os/Message;
    .end local v3    # "powerSaveStatus":Z
    .end local v4    # "isL5Device":Z
    :cond_4
    :goto_1
    return-void
.end method
