.class public Lcom/android/server/location/LocationExtCooperateImpl;
.super Ljava/lang/Object;
.source "LocationExtCooperateImpl.java"

# interfaces
.implements Lcom/android/server/location/LocationExtCooperateStub;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/location/LocationExtCooperateImpl$SettingsObserver;
    }
.end annotation


# static fields
.field private static final ACTION_SATELLITE_STATE_CHANGE:Ljava/lang/String; = "com.android.app.action.SATELLITE_STATE_CHANGE"

.field private static final DEF_GSCO_PKG:Ljava/lang/String; = "gscoPkg invalid"

.field private static final FLAG_GSCO_APPEND_PKG:Ljava/lang/String; = "1"

.field private static final FLAG_GSCO_COVER_PKG:Ljava/lang/String; = "0"

.field private static final KEY_API_USE:Ljava/lang/String; = "MI_GNSS"

.field private static final KEY_GSCO_PKG:Ljava/lang/String; = "gscoPkg"

.field private static final KEY_GSCO_STATUS:Ljava/lang/String; = "gscoStatus"

.field private static final KEY_SATELLITE_STATE_CHANGE_IS_ENABLE:Ljava/lang/String; = "is_enable"

.field private static final KEY_SATELLITE_STATE_CHANGE_PHONE_ID:Ljava/lang/String; = "phone_id"

.field private static final SATELLITE_STATE:Ljava/lang/String; = "satellite_state"

.field private static final TAG:Ljava/lang/String; = "LocationExtCooperate"

.field private static final sDevices:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mCloudSpFile:Ljava/io/File;

.field private mContext:Landroid/content/Context;

.field private final mDefaultFeatureStatus:Z

.field private mIsSpecifiedDevice:Z

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mSettingsObserver:Lcom/android/server/location/LocationExtCooperateImpl$SettingsObserver;

.field private final sModule:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private sPkg:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/location/LocationExtCooperateImpl;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/location/LocationExtCooperateImpl;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsSpecifiedDevice(Lcom/android/server/location/LocationExtCooperateImpl;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/location/LocationExtCooperateImpl;->mIsSpecifiedDevice:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetsPkg(Lcom/android/server/location/LocationExtCooperateImpl;)Ljava/util/HashSet;
    .locals 0

    iget-object p0, p0, Lcom/android/server/location/LocationExtCooperateImpl;->sPkg:Ljava/util/HashSet;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmIsSpecifiedDevice(Lcom/android/server/location/LocationExtCooperateImpl;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/location/LocationExtCooperateImpl;->mIsSpecifiedDevice:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mregisterReceiver(Lcom/android/server/location/LocationExtCooperateImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/location/LocationExtCooperateImpl;->registerReceiver()V

    return-void
.end method

.method static bridge synthetic -$$Nest$msaveCloudDataToSP(Lcom/android/server/location/LocationExtCooperateImpl;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/location/LocationExtCooperateImpl;->saveCloudDataToSP(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$munRegisterReceiver(Lcom/android/server/location/LocationExtCooperateImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/location/LocationExtCooperateImpl;->unRegisterReceiver()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 49
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/android/server/location/LocationExtCooperateImpl;->sDevices:Ljava/util/HashSet;

    .line 56
    const-string v1, "aurora"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 57
    return-void
.end method

.method constructor <init>()V
    .locals 4

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v2

    const-string/jumbo v3, "system"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const-string v2, "satelliteCallOpt.xml"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/location/LocationExtCooperateImpl;->mCloudSpFile:Ljava/io/File;

    .line 45
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/server/location/LocationExtCooperateImpl;->sModule:Ljava/util/HashSet;

    .line 46
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/android/server/location/LocationExtCooperateImpl;->sPkg:Ljava/util/HashSet;

    .line 50
    const-string v1, "persist.sys.gnss_gsco"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/server/location/LocationExtCooperateImpl;->mDefaultFeatureStatus:Z

    .line 72
    new-instance v1, Lcom/android/server/location/LocationExtCooperateImpl$1;

    invoke-direct {v1, p0}, Lcom/android/server/location/LocationExtCooperateImpl$1;-><init>(Lcom/android/server/location/LocationExtCooperateImpl;)V

    iput-object v1, p0, Lcom/android/server/location/LocationExtCooperateImpl;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 65
    const-string v1, "power"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 66
    iget-object v0, p0, Lcom/android/server/location/LocationExtCooperateImpl;->sPkg:Ljava/util/HashSet;

    const-string v1, "com.android.mms"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 67
    iget-object v0, p0, Lcom/android/server/location/LocationExtCooperateImpl;->sPkg:Ljava/util/HashSet;

    const-string v1, "com.android.incallui"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 68
    invoke-direct {p0}, Lcom/android/server/location/LocationExtCooperateImpl;->loadCloudDataGscoPkgFromSP()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/location/LocationExtCooperateImpl;->setPkgFromCloud(Ljava/lang/String;)V

    .line 69
    new-instance v0, Lcom/android/server/location/LocationExtCooperateImpl$SettingsObserver;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/android/server/location/LocationExtCooperateImpl$SettingsObserver;-><init>(Lcom/android/server/location/LocationExtCooperateImpl;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/server/location/LocationExtCooperateImpl;->mSettingsObserver:Lcom/android/server/location/LocationExtCooperateImpl$SettingsObserver;

    .line 70
    return-void
.end method

.method private loadCloudDataGscoPkgFromSP()Ljava/lang/String;
    .locals 5

    .line 205
    const-string v0, "gscoPkg invalid"

    const-string v1, "load Cloud Data Gsco Status From SP running..."

    const-string v2, "LocationExtCooperate"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    :try_start_0
    iget-object v1, p0, Lcom/android/server/location/LocationExtCooperateImpl;->mContext:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 208
    return-object v0

    .line 210
    :cond_0
    invoke-virtual {v1}, Landroid/content/Context;->createDeviceProtectedStorageContext()Landroid/content/Context;

    move-result-object v1

    .line 211
    .local v1, "directBootContext":Landroid/content/Context;
    iget-object v3, p0, Lcom/android/server/location/LocationExtCooperateImpl;->mCloudSpFile:Ljava/io/File;

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/io/File;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 212
    .local v3, "editor":Landroid/content/SharedPreferences;
    const-string v4, "gscoPkg"

    invoke-interface {v3, v4, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 216
    .end local v1    # "directBootContext":Landroid/content/Context;
    .end local v3    # "editor":Landroid/content/SharedPreferences;
    .local v0, "pkg":Ljava/lang/String;
    nop

    .line 217
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Success to load GscoPkg:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    return-object v0

    .line 213
    .end local v0    # "pkg":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 214
    .local v1, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to load GscoPkg..., "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    return-object v0
.end method

.method private loadCloudDataGscoStatusFromSP()Z
    .locals 5

    .line 223
    iget-boolean v0, p0, Lcom/android/server/location/LocationExtCooperateImpl;->mDefaultFeatureStatus:Z

    const/4 v1, 0x0

    if-nez v0, :cond_1

    sget-object v0, Lcom/android/server/location/LocationExtCooperateImpl;->sDevices:Ljava/util/HashSet;

    sget-object v2, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    move v0, v1

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 224
    .local v0, "defVal":Z
    :goto_1
    const-string v2, "load Cloud Data Gsco Status From SP running..."

    const-string v3, "LocationExtCooperate"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    :try_start_0
    iget-object v2, p0, Lcom/android/server/location/LocationExtCooperateImpl;->mContext:Landroid/content/Context;

    if-nez v2, :cond_2

    .line 227
    return v0

    .line 229
    :cond_2
    invoke-virtual {v2}, Landroid/content/Context;->createDeviceProtectedStorageContext()Landroid/content/Context;

    move-result-object v2

    .line 230
    .local v2, "directBootContext":Landroid/content/Context;
    iget-object v4, p0, Lcom/android/server/location/LocationExtCooperateImpl;->mCloudSpFile:Ljava/io/File;

    invoke-virtual {v2, v4, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/io/File;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 231
    .local v1, "editor":Landroid/content/SharedPreferences;
    const-string v4, "gscoStatus"

    invoke-interface {v1, v4, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v1, v4

    .line 235
    .end local v2    # "directBootContext":Landroid/content/Context;
    .local v1, "status":Z
    nop

    .line 236
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Success to load GscoStatus:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    return v1

    .line 232
    .end local v1    # "status":Z
    :catch_0
    move-exception v1

    .line 233
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to load GscoStatus..., "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    return v0
.end method

.method private registerReceiver()V
    .locals 7

    .line 182
    const-class v0, Lcom/android/server/location/LocationExtCooperateImpl;

    monitor-enter v0

    .line 183
    :try_start_0
    iget-object v1, p0, Lcom/android/server/location/LocationExtCooperateImpl;->mContext:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 184
    const-string v1, "LocationExtCooperate"

    const-string v2, "registerReceiver fail due to context == null."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    monitor-exit v0

    return-void

    .line 188
    :cond_0
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 189
    .local v1, "filter":Landroid/content/IntentFilter;
    const-string v2, "com.android.app.action.SATELLITE_STATE_CHANGE"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 190
    iget-object v2, p0, Lcom/android/server/location/LocationExtCooperateImpl;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/server/location/LocationExtCooperateImpl;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 192
    iget-object v2, p0, Lcom/android/server/location/LocationExtCooperateImpl;->mSettingsObserver:Lcom/android/server/location/LocationExtCooperateImpl$SettingsObserver;

    const/4 v3, -0x2

    const/4 v4, 0x0

    if-eqz v2, :cond_1

    .line 193
    iget-object v2, p0, Lcom/android/server/location/LocationExtCooperateImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v5, "satellite_state"

    invoke-static {v5}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    iget-object v6, p0, Lcom/android/server/location/LocationExtCooperateImpl;->mSettingsObserver:Lcom/android/server/location/LocationExtCooperateImpl$SettingsObserver;

    invoke-virtual {v2, v5, v4, v6, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    goto :goto_0

    .line 196
    :cond_1
    new-instance v2, Lcom/android/server/location/LocationExtCooperateImpl$SettingsObserver;

    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    invoke-direct {v2, p0, v5}, Lcom/android/server/location/LocationExtCooperateImpl$SettingsObserver;-><init>(Lcom/android/server/location/LocationExtCooperateImpl;Landroid/os/Handler;)V

    iput-object v2, p0, Lcom/android/server/location/LocationExtCooperateImpl;->mSettingsObserver:Lcom/android/server/location/LocationExtCooperateImpl$SettingsObserver;

    .line 197
    iget-object v2, p0, Lcom/android/server/location/LocationExtCooperateImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v5, "satellite_state"

    invoke-static {v5}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    iget-object v6, p0, Lcom/android/server/location/LocationExtCooperateImpl;->mSettingsObserver:Lcom/android/server/location/LocationExtCooperateImpl$SettingsObserver;

    invoke-virtual {v2, v5, v4, v6, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 200
    .end local v1    # "filter":Landroid/content/IntentFilter;
    :goto_0
    monitor-exit v0

    .line 201
    return-void

    .line 200
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private saveCloudDataToSP(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 4
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Object;

    .line 242
    const-string v0, "LocationExtCooperate"

    :try_start_0
    iget-object v1, p0, Lcom/android/server/location/LocationExtCooperateImpl;->mContext:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 243
    return-void

    .line 245
    :cond_0
    iget-object v2, p0, Lcom/android/server/location/LocationExtCooperateImpl;->mCloudSpFile:Ljava/io/File;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/io/File;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 246
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    instance-of v2, p2, Ljava/lang/Boolean;

    if-eqz v2, :cond_1

    .line 247
    move-object v2, p2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-interface {v1, p1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 248
    :cond_1
    instance-of v2, p2, Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 249
    move-object v2, p2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v1, p1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 254
    :goto_0
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 258
    .end local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    nop

    .line 259
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Success to save data to sp, data:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    return-void

    .line 251
    .restart local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_2
    :try_start_1
    const-string v2, "save cloud data to sp value is not valid..."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 252
    return-void

    .line 255
    .end local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    :catch_0
    move-exception v1

    .line 256
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Save data to sp Exception:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    return-void
.end method

.method private setPkgFromCloud(Ljava/lang/String;)V
    .locals 6
    .param p1, "pkgs"    # Ljava/lang/String;

    .line 141
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "set Pkg From Cloud:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "LocationExtCooperate"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    if-eqz p1, :cond_4

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_2

    .line 144
    :cond_0
    const-string v0, ","

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 145
    .local v0, "parts":[Ljava/lang/String;
    const/4 v2, 0x0

    aget-object v2, v0, v2

    .line 146
    .local v2, "flag":Ljava/lang/String;
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 147
    .local v3, "pkgSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v4, 0x1

    .local v4, "i":I
    :goto_0
    array-length v5, v0

    if-ge v4, v5, :cond_1

    .line 148
    aget-object v5, v0, v4

    invoke-interface {v3, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 147
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 151
    .end local v4    # "i":I
    :cond_1
    const-string v4, "0"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 153
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4, v3}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v4, p0, Lcom/android/server/location/LocationExtCooperateImpl;->sPkg:Ljava/util/HashSet;

    goto :goto_1

    .line 154
    :cond_2
    const-string v4, "1"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 156
    iget-object v4, p0, Lcom/android/server/location/LocationExtCooperateImpl;->sPkg:Ljava/util/HashSet;

    invoke-virtual {v4, v3}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 159
    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "receive cloud pkg data not valid:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    :goto_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "set pkg success:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/server/location/LocationExtCooperateImpl;->sPkg:Ljava/util/HashSet;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    return-void

    .line 142
    .end local v0    # "parts":[Ljava/lang/String;
    .end local v2    # "flag":Ljava/lang/String;
    .end local v3    # "pkgSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_4
    :goto_2
    return-void
.end method

.method private unRegisterReceiver()V
    .locals 3

    .line 165
    const-class v0, Lcom/android/server/location/LocationExtCooperateImpl;

    monitor-enter v0

    .line 166
    :try_start_0
    iget-object v1, p0, Lcom/android/server/location/LocationExtCooperateImpl;->mContext:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 167
    const-string v1, "LocationExtCooperate"

    const-string/jumbo v2, "unRegisterReceiver fail due to context == null."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    monitor-exit v0

    return-void

    .line 170
    :cond_0
    iget-object v2, p0, Lcom/android/server/location/LocationExtCooperateImpl;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 173
    invoke-static {}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub;->getInstance()Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub;->unRegisterSatelliteCallMode()V

    .line 174
    iget-object v1, p0, Lcom/android/server/location/LocationExtCooperateImpl;->mSettingsObserver:Lcom/android/server/location/LocationExtCooperateImpl$SettingsObserver;

    if-eqz v1, :cond_1

    .line 175
    iget-object v1, p0, Lcom/android/server/location/LocationExtCooperateImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/location/LocationExtCooperateImpl;->mSettingsObserver:Lcom/android/server/location/LocationExtCooperateImpl$SettingsObserver;

    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 176
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/server/location/LocationExtCooperateImpl;->mSettingsObserver:Lcom/android/server/location/LocationExtCooperateImpl$SettingsObserver;

    .line 178
    :cond_1
    monitor-exit v0

    .line 179
    return-void

    .line 178
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method


# virtual methods
.method public init(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 100
    iput-object p1, p0, Lcom/android/server/location/LocationExtCooperateImpl;->mContext:Landroid/content/Context;

    .line 102
    invoke-direct {p0}, Lcom/android/server/location/LocationExtCooperateImpl;->loadCloudDataGscoStatusFromSP()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/location/LocationExtCooperateImpl;->mIsSpecifiedDevice:Z

    .line 103
    if-eqz v0, :cond_0

    .line 105
    invoke-direct {p0}, Lcom/android/server/location/LocationExtCooperateImpl;->registerReceiver()V

    .line 108
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/LocationExtCooperateImpl;->mSettingsObserver:Lcom/android/server/location/LocationExtCooperateImpl$SettingsObserver;

    if-eqz v0, :cond_1

    .line 109
    iget-object v0, p0, Lcom/android/server/location/LocationExtCooperateImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "gscoStatus"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/location/LocationExtCooperateImpl;->mSettingsObserver:Lcom/android/server/location/LocationExtCooperateImpl$SettingsObserver;

    const/4 v3, -0x2

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 112
    :cond_1
    return-void
.end method

.method public registerSatelliteCallMode(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "module"    # Ljava/lang/String;
    .param p2, "pkg"    # Ljava/lang/String;

    .line 265
    new-instance v0, Ljava/util/HashSet;

    const-string v1, ","

    invoke-virtual {p2, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 266
    .local v0, "pkgSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const-string v3, "LocationExtCooperate"

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 267
    .local v2, "p":Ljava/lang/String;
    iget-object v4, p0, Lcom/android/server/location/LocationExtCooperateImpl;->sPkg:Ljava/util/HashSet;

    invoke-virtual {v4, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 269
    invoke-virtual {v0, v2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 270
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "i"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    .end local v2    # "p":Ljava/lang/String;
    :cond_0
    goto :goto_0

    .line 273
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setSingleAppPosMode module:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",pkg:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", uid:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    iget-object v1, p0, Lcom/android/server/location/LocationExtCooperateImpl;->sModule:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    goto :goto_1

    .line 280
    :cond_2
    invoke-static {}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub;->getInstance()Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub;

    move-result-object v1

    const-string v2, "MI_GNSS"

    invoke-virtual {v1, v0, v2}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub;->registerSatelliteCallMode(Ljava/util/HashSet;Ljava/lang/String;)V

    .line 281
    return-void

    .line 275
    :cond_3
    :goto_1
    const-string/jumbo v1, "setSingleAppPosMode not specify module or pkg..."

    invoke-static {v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    return-void
.end method

.method public setSatelliteCallOptPkg(Ljava/lang/String;)V
    .locals 1
    .param p1, "pkgs"    # Ljava/lang/String;

    .line 135
    const-string v0, "gscoPkg"

    invoke-direct {p0, v0, p1}, Lcom/android/server/location/LocationExtCooperateImpl;->saveCloudDataToSP(Ljava/lang/String;Ljava/lang/Object;)V

    .line 137
    invoke-direct {p0, p1}, Lcom/android/server/location/LocationExtCooperateImpl;->setPkgFromCloud(Ljava/lang/String;)V

    .line 138
    return-void
.end method

.method public setSatelliteCallOptStatus(Z)V
    .locals 3
    .param p1, "status"    # Z

    .line 117
    iget-boolean v0, p0, Lcom/android/server/location/LocationExtCooperateImpl;->mIsSpecifiedDevice:Z

    const-string v1, "gscoStatus"

    const-string v2, "LocationExtCooperate"

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 118
    const-string v0, "Has Register Process Observer by cloud..."

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/location/LocationExtCooperateImpl;->mIsSpecifiedDevice:Z

    .line 120
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/android/server/location/LocationExtCooperateImpl;->saveCloudDataToSP(Ljava/lang/String;Ljava/lang/Object;)V

    .line 122
    invoke-direct {p0}, Lcom/android/server/location/LocationExtCooperateImpl;->registerReceiver()V

    goto :goto_0

    .line 123
    :cond_0
    if-eqz v0, :cond_1

    if-nez p1, :cond_1

    .line 124
    const-string v0, "Has unRegister Process Observer by cloud..."

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/location/LocationExtCooperateImpl;->mIsSpecifiedDevice:Z

    .line 126
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/android/server/location/LocationExtCooperateImpl;->saveCloudDataToSP(Ljava/lang/String;Ljava/lang/Object;)V

    .line 128
    invoke-direct {p0}, Lcom/android/server/location/LocationExtCooperateImpl;->unRegisterReceiver()V

    .line 130
    :cond_1
    :goto_0
    return-void
.end method

.method public unRegisterSatelliteCallMode(Ljava/lang/String;)V
    .locals 2
    .param p1, "module"    # Ljava/lang/String;

    .line 285
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "removeSingleAppPosMode module:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", uid:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "LocationExtCooperate"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    iget-object v0, p0, Lcom/android/server/location/LocationExtCooperateImpl;->sModule:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 287
    const-string v0, "removeSingleAppPosMode not specify module..."

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    return-void

    .line 290
    :cond_0
    invoke-static {}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub;->getInstance()Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub;->unRegisterSatelliteCallMode()V

    .line 291
    return-void
.end method
