.class Lcom/android/server/location/MtkGnssPowerSaveImpl$BootCompletedReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MtkGnssPowerSaveImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/location/MtkGnssPowerSaveImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BootCompletedReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/location/MtkGnssPowerSaveImpl;


# direct methods
.method private constructor <init>(Lcom/android/server/location/MtkGnssPowerSaveImpl;)V
    .locals 0

    .line 466
    iput-object p1, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl$BootCompletedReceiver;->this$0:Lcom/android/server/location/MtkGnssPowerSaveImpl;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/location/MtkGnssPowerSaveImpl;Lcom/android/server/location/MtkGnssPowerSaveImpl$BootCompletedReceiver-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/location/MtkGnssPowerSaveImpl$BootCompletedReceiver;-><init>(Lcom/android/server/location/MtkGnssPowerSaveImpl;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 469
    const-string v0, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 470
    return-void

    .line 472
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl$BootCompletedReceiver;->this$0:Lcom/android/server/location/MtkGnssPowerSaveImpl;

    invoke-static {v0}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->-$$Nest$fgetTAG(Lcom/android/server/location/MtkGnssPowerSaveImpl;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "receiver boot completed, start init listener"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 473
    iget-object v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl$BootCompletedReceiver;->this$0:Lcom/android/server/location/MtkGnssPowerSaveImpl;

    invoke-static {v0}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->-$$Nest$minitListenerAndRegisterIEmd(Lcom/android/server/location/MtkGnssPowerSaveImpl;)V

    .line 476
    iget-object v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl$BootCompletedReceiver;->this$0:Lcom/android/server/location/MtkGnssPowerSaveImpl;

    invoke-static {v0}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->-$$Nest$fgetmContext(Lcom/android/server/location/MtkGnssPowerSaveImpl;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl$BootCompletedReceiver;->this$0:Lcom/android/server/location/MtkGnssPowerSaveImpl;

    invoke-static {v1}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->-$$Nest$fgetbootCompletedReceiver(Lcom/android/server/location/MtkGnssPowerSaveImpl;)Landroid/content/BroadcastReceiver;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 477
    return-void
.end method
