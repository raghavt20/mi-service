class com.android.server.location.LocationExtCooperateImpl$1 extends android.content.BroadcastReceiver {
	 /* .source "LocationExtCooperateImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/location/LocationExtCooperateImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.location.LocationExtCooperateImpl this$0; //synthetic
/* # direct methods */
 com.android.server.location.LocationExtCooperateImpl$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/location/LocationExtCooperateImpl; */
/* .line 72 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 75 */
final String v0 = "com.android.app.action.SATELLITE_STATE_CHANGE"; // const-string v0, "com.android.app.action.SATELLITE_STATE_CHANGE"
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 77 */
	 final String v0 = "is_enable"; // const-string v0, "is_enable"
	 int v1 = 0; // const/4 v1, 0x0
	 v0 = 	 (( android.content.Intent ) p2 ).getBooleanExtra ( v0, v1 ); // invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z
	 /* .line 79 */
	 /* .local v0, "satelliteCallSwitcher":Z */
	 final String v1 = "phone_id"; // const-string v1, "phone_id"
	 int v2 = -1; // const/4 v2, -0x1
	 v1 = 	 (( android.content.Intent ) p2 ).getIntExtra ( v1, v2 ); // invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
	 /* .line 80 */
	 /* .local v1, "phoneId":I */
	 /* new-instance v2, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v3 = "Satellite call is enable:"; // const-string v3, "Satellite call is enable:"
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
	 final String v3 = ", phoneId:"; // const-string v3, ", phoneId:"
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v3 = "LocationExtCooperate"; // const-string v3, "LocationExtCooperate"
	 android.util.Log .i ( v3,v2 );
	 /* .line 81 */
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 84 */
		 com.android.server.location.gnss.exp.GnssBackgroundUsageOptStub .getInstance ( );
		 v3 = this.this$0;
		 com.android.server.location.LocationExtCooperateImpl .-$$Nest$fgetsPkg ( v3 );
		 final String v4 = "MI_GNSS"; // const-string v4, "MI_GNSS"
		 (( com.android.server.location.gnss.exp.GnssBackgroundUsageOptStub ) v2 ).registerSatelliteCallMode ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub;->registerSatelliteCallMode(Ljava/util/HashSet;Ljava/lang/String;)V
		 /* .line 87 */
	 } // :cond_0
	 com.android.server.location.gnss.exp.GnssBackgroundUsageOptStub .getInstance ( );
	 (( com.android.server.location.gnss.exp.GnssBackgroundUsageOptStub ) v2 ).unRegisterSatelliteCallMode ( ); // invoke-virtual {v2}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub;->unRegisterSatelliteCallMode()V
	 /* .line 90 */
} // .end local v0 # "satelliteCallSwitcher":Z
} // .end local v1 # "phoneId":I
} // :cond_1
} // :goto_0
return;
} // .end method
