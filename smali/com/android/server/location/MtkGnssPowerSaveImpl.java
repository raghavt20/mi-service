public class com.android.server.location.MtkGnssPowerSaveImpl implements com.android.server.location.MtkGnssPowerSaveStub {
	 /* .source "MtkGnssPowerSaveImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/location/MtkGnssPowerSaveImpl$SmartSatelliteHandler;, */
	 /* Lcom/android/server/location/MtkGnssPowerSaveImpl$BootCompletedReceiver;, */
	 /* Lcom/android/server/location/MtkGnssPowerSaveImpl$GnssPowerSaveObs;, */
	 /* Lcom/android/server/location/MtkGnssPowerSaveImpl$GnssSmartSwitchObs;, */
	 /* Lcom/android/server/location/MtkGnssPowerSaveImpl$PowerSaveReceiver; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String ACTION_BOOT_COMPLETED;
private static final java.lang.String ACTION_POWER_SAVE_MODE_CHANGED;
private static final java.lang.String CLOUD_KEY_MTK_DISABLE_L5;
private static final java.lang.String CLOUD_KEY_MTK_DISABLE_SATELLITE;
private static final java.lang.String CLOUD_MODULE_MTK_GNSS_CONFIG;
private static final java.lang.String DISABLE_L5_MODE_CHANGED_ACTION;
private static final Integer GNSS_BD_GPS_QZSS;
private static final Integer GNSS_MODE_BD_GPS_GA_GL_QZSS;
private static final java.lang.String KEY_POWER_MODE_OPEN;
private static final java.lang.String MI_MNL_CONFIG_KEY_L1_ONLY_ENABLE;
private static final java.lang.String MI_MNL_CONFIG_KEY_MNL_VERSION;
private static final java.lang.String MI_MNL_CONFIG_KEY_SMART_SWITCH_ENABLE;
private static final Integer MSG_DISABLE_L5_AND_SATELLITE;
private static final Integer MSG_RECOVERY_SATELLITE;
private static final Integer MSG_SWITCH_SATELLITE_FOR_L1_ONLY_DEVICE;
private static final java.lang.String MTK_GNSS_CONFIG_SUPPORT_DISABLE_L5_PROP;
private static final java.lang.String MTK_GNSS_CONFIG_SUPPORT_DISABLE_SATELLITE_PROP;
private static final java.lang.String MTK_GNSS_CONFIG_SUPPORT_L5_PROP;
private static final java.lang.String MTK_GNSS_CONFIG_SUPPORT_PROP;
private static final java.lang.String XM_MTK_GNSS_CONF_DISABLE_L5_CONF;
private static final Integer XM_MTK_GNSS_CONF_DISABLE_L5_OFF;
private static final Integer XM_MTK_GNSS_CONF_DISABLE_L5_ON;
private static final java.lang.String XM_MTK_GNSS_CONF_SMART_SWITCH_CONF;
private static final Integer XM_MTK_GNSS_CONF_SMART_SWITCH_OFF;
private static final Integer XM_MTK_GNSS_CONF_SMART_SWITCH_ON;
/* # instance fields */
private final java.lang.String MNL_DGPSMode_FEATURE_NAME;
private final java.lang.String MNL_GLP_FEATURE_NAME;
private final java.lang.String MNL_GNSS_MODE_FEATURE_NAME;
private final java.lang.String MNL_L1_ONLY_FEATURE_NAME;
private final java.lang.String MNL_L1_ONLY_FORMAT;
private java.lang.String TAG;
private android.content.BroadcastReceiver bootCompletedReceiver;
private Boolean isSmartSwitchEnableFlag;
private android.location.LocationManager locationManager;
private android.content.Context mContext;
private android.os.Handler mHandler;
private com.android.server.location.mnlutils.MnlConfigUtils mnlConfigUtils;
private android.database.ContentObserver mtkGnssDisableL5Obs;
private android.database.ContentObserver mtkGnssSmartSwitchObs;
private android.content.BroadcastReceiver powerSaveReceiver;
private Boolean startControllerListener;
/* # direct methods */
static java.lang.String -$$Nest$fgetTAG ( com.android.server.location.MtkGnssPowerSaveImpl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.TAG;
} // .end method
static android.content.BroadcastReceiver -$$Nest$fgetbootCompletedReceiver ( com.android.server.location.MtkGnssPowerSaveImpl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.bootCompletedReceiver;
} // .end method
static Boolean -$$Nest$fgetisSmartSwitchEnableFlag ( com.android.server.location.MtkGnssPowerSaveImpl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->isSmartSwitchEnableFlag:Z */
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.android.server.location.MtkGnssPowerSaveImpl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mContext;
} // .end method
static android.os.Handler -$$Nest$fgetmHandler ( com.android.server.location.MtkGnssPowerSaveImpl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mHandler;
} // .end method
static void -$$Nest$fputisSmartSwitchEnableFlag ( com.android.server.location.MtkGnssPowerSaveImpl p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->isSmartSwitchEnableFlag:Z */
	 return;
} // .end method
static void -$$Nest$minitListenerAndRegisterIEmd ( com.android.server.location.MtkGnssPowerSaveImpl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->initListenerAndRegisterIEmd()V */
	 return;
} // .end method
static Boolean -$$Nest$ml5Device ( com.android.server.location.MtkGnssPowerSaveImpl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = 	 /* invoke-direct {p0}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->l5Device()Z */
} // .end method
static void -$$Nest$mupdateDisableL5CloudConfig ( com.android.server.location.MtkGnssPowerSaveImpl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->updateDisableL5CloudConfig()V */
	 return;
} // .end method
static void -$$Nest$mupdateDisableSatelliteCloudConfig ( com.android.server.location.MtkGnssPowerSaveImpl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->updateDisableSatelliteCloudConfig()V */
	 return;
} // .end method
public com.android.server.location.MtkGnssPowerSaveImpl ( ) {
	 /* .locals 1 */
	 /* .line 30 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 31 */
	 final String v0 = "Glp-MtkGnssPowerSaveImpl"; // const-string v0, "Glp-MtkGnssPowerSaveImpl"
	 this.TAG = v0;
	 /* .line 35 */
	 final String v0 = "L1Only"; // const-string v0, "L1Only"
	 this.MNL_L1_ONLY_FEATURE_NAME = v0;
	 /* .line 36 */
	 final String v0 = "L1 only"; // const-string v0, "L1 only"
	 this.MNL_L1_ONLY_FORMAT = v0;
	 /* .line 37 */
	 final String v0 = "GLP"; // const-string v0, "GLP"
	 this.MNL_GLP_FEATURE_NAME = v0;
	 /* .line 38 */
	 final String v0 = "GnssMode"; // const-string v0, "GnssMode"
	 this.MNL_GNSS_MODE_FEATURE_NAME = v0;
	 /* .line 39 */
	 final String v0 = "DGPSMode"; // const-string v0, "DGPSMode"
	 this.MNL_DGPSMode_FEATURE_NAME = v0;
	 /* .line 76 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* iput-boolean v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->startControllerListener:Z */
	 /* .line 78 */
	 /* iput-boolean v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->isSmartSwitchEnableFlag:Z */
	 return;
} // .end method
private void initListenerAndRegisterIEmd ( ) {
	 /* .locals 5 */
	 /* .line 481 */
	 v0 = 	 (( com.android.server.location.MtkGnssPowerSaveImpl ) p0 ).supportMtkGnssConfig ( ); // invoke-virtual {p0}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->supportMtkGnssConfig()Z
	 /* if-nez v0, :cond_0 */
	 /* .line 482 */
	 v0 = this.TAG;
	 final String v1 = "not support mtk gnss config feature"; // const-string v1, "not support mtk gnss config feature"
	 android.util.Log .d ( v0,v1 );
	 /* .line 483 */
	 return;
	 /* .line 485 */
} // :cond_0
v0 = this.mtkGnssDisableL5Obs;
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_1 */
/* .line 486 */
/* new-instance v0, Lcom/android/server/location/MtkGnssPowerSaveImpl$GnssPowerSaveObs; */
v2 = this.mContext;
/* invoke-direct {v0, p0, v2, v1}, Lcom/android/server/location/MtkGnssPowerSaveImpl$GnssPowerSaveObs;-><init>(Lcom/android/server/location/MtkGnssPowerSaveImpl;Landroid/content/Context;Landroid/os/Handler;)V */
this.mtkGnssDisableL5Obs = v0;
/* .line 488 */
} // :cond_1
v0 = this.mtkGnssSmartSwitchObs;
/* if-nez v0, :cond_2 */
/* .line 489 */
/* new-instance v0, Lcom/android/server/location/MtkGnssPowerSaveImpl$GnssSmartSwitchObs; */
v2 = this.mContext;
/* invoke-direct {v0, p0, v2, v1}, Lcom/android/server/location/MtkGnssPowerSaveImpl$GnssSmartSwitchObs;-><init>(Lcom/android/server/location/MtkGnssPowerSaveImpl;Landroid/content/Context;Landroid/os/Handler;)V */
this.mtkGnssSmartSwitchObs = v0;
/* .line 493 */
} // :cond_2
try { // :try_start_0
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v2, "xiaomi_gnss_config_disable_l5" */
/* .line 494 */
android.provider.Settings$Secure .getUriFor ( v2 );
v3 = this.mtkGnssDisableL5Obs;
/* .line 493 */
int v4 = 0; // const/4 v4, 0x0
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v2, v4, v3 ); // invoke-virtual {v0, v2, v4, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 497 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v2, "xiaomi_gnss_smart_switch" */
/* .line 498 */
android.provider.Settings$Secure .getUriFor ( v2 );
v3 = this.mtkGnssSmartSwitchObs;
/* .line 497 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v2, v4, v3 ); // invoke-virtual {v0, v2, v4, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 503 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 504 */
/* .local v0, "intentFilter":Landroid/content/IntentFilter; */
final String v2 = "miui.intent.action.POWER_SAVE_MODE_CHANGED"; // const-string v2, "miui.intent.action.POWER_SAVE_MODE_CHANGED"
(( android.content.IntentFilter ) v0 ).addAction ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 505 */
v2 = this.powerSaveReceiver;
/* if-nez v2, :cond_3 */
/* .line 506 */
/* new-instance v2, Lcom/android/server/location/MtkGnssPowerSaveImpl$PowerSaveReceiver; */
/* invoke-direct {v2, p0, v1}, Lcom/android/server/location/MtkGnssPowerSaveImpl$PowerSaveReceiver;-><init>(Lcom/android/server/location/MtkGnssPowerSaveImpl;Lcom/android/server/location/MtkGnssPowerSaveImpl$PowerSaveReceiver-IA;)V */
this.powerSaveReceiver = v2;
/* .line 508 */
} // :cond_3
v1 = this.mContext;
v2 = this.powerSaveReceiver;
(( android.content.Context ) v1 ).registerReceiver ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 511 */
/* nop */
} // .end local v0 # "intentFilter":Landroid/content/IntentFilter;
/* .line 509 */
/* :catch_0 */
/* move-exception v0 */
/* .line 510 */
/* .local v0, "e":Ljava/lang/Exception; */
v1 = this.TAG;
final String v2 = "init exception:"; // const-string v2, "init exception:"
android.util.Log .e ( v1,v2,v0 );
/* .line 512 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private Boolean isCnVersion ( ) {
/* .locals 2 */
/* .line 88 */
final String v0 = "ro.miui.region"; // const-string v0, "ro.miui.region"
android.os.SystemProperties .get ( v0 );
final String v1 = "CN"; // const-string v1, "CN"
v0 = (( java.lang.String ) v1 ).equalsIgnoreCase ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
} // .end method
private Boolean isL1OnlyEnable ( com.android.server.location.mnlutils.bean.MnlConfig p0 ) {
/* .locals 8 */
/* .param p1, "mnlConfig" # Lcom/android/server/location/mnlutils/bean/MnlConfig; */
/* .line 359 */
final String v0 = "L1Only"; // const-string v0, "L1Only"
(( com.android.server.location.mnlutils.bean.MnlConfig ) p1 ).getFeature ( v0 ); // invoke-virtual {p1, v0}, Lcom/android/server/location/mnlutils/bean/MnlConfig;->getFeature(Ljava/lang/String;)Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
/* .line 360 */
/* .local v0, "l1OnlyFeature":Lcom/android/server/location/mnlutils/bean/MnlConfigFeature; */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 361 */
/* .line 364 */
} // :cond_0
(( com.android.server.location.mnlutils.bean.MnlConfigFeature ) v0 ).getConfig ( ); // invoke-virtual {v0}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->getConfig()Ljava/lang/String;
/* .line 365 */
/* .local v2, "l1OnlyConfig":Ljava/lang/String; */
final String v3 = ""; // const-string v3, ""
/* .line 367 */
/* .local v3, "l1OnlyFormatSetting":Ljava/lang/String; */
(( com.android.server.location.mnlutils.bean.MnlConfigFeature ) v0 ).getFormatSettings ( ); // invoke-virtual {v0}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->getFormatSettings()Ljava/util/LinkedHashMap;
(( java.util.LinkedHashMap ) v4 ).entrySet ( ); // invoke-virtual {v4}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;
v5 = } // :goto_0
if ( v5 != null) { // if-eqz v5, :cond_2
/* check-cast v5, Ljava/util/Map$Entry; */
/* .line 368 */
/* .local v5, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;" */
/* check-cast v6, Ljava/lang/String; */
(( java.lang.String ) v6 ).toLowerCase ( ); // invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;
final String v7 = "L1 only"; // const-string v7, "L1 only"
(( java.lang.String ) v7 ).toLowerCase ( ); // invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;
v6 = (( java.lang.String ) v6 ).contains ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 369 */
/* move-object v3, v4 */
/* check-cast v3, Ljava/lang/String; */
/* .line 370 */
/* .line 372 */
} // .end local v5 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
} // :cond_1
/* .line 374 */
} // :cond_2
} // :goto_1
final String v4 = "1"; // const-string v4, "1"
v5 = (( java.lang.String ) v4 ).equals ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_3
v4 = (( java.lang.String ) v4 ).equals ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_3
int v1 = 1; // const/4 v1, 0x1
} // :cond_3
} // .end method
private Boolean isSmartSwitchEnable ( com.android.server.location.mnlutils.bean.MnlConfig p0 ) {
/* .locals 8 */
/* .param p1, "mnlConfig" # Lcom/android/server/location/mnlutils/bean/MnlConfig; */
/* .line 378 */
final String v0 = "GnssMode"; // const-string v0, "GnssMode"
(( com.android.server.location.mnlutils.bean.MnlConfig ) p1 ).getFeature ( v0 ); // invoke-virtual {p1, v0}, Lcom/android/server/location/mnlutils/bean/MnlConfig;->getFeature(Ljava/lang/String;)Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
/* .line 379 */
/* .local v0, "smartSwitchFeature":Lcom/android/server/location/mnlutils/bean/MnlConfigFeature; */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 380 */
/* .line 383 */
} // :cond_0
(( com.android.server.location.mnlutils.bean.MnlConfigFeature ) v0 ).getConfig ( ); // invoke-virtual {v0}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->getConfig()Ljava/lang/String;
/* .line 384 */
/* .local v2, "smartSwitchConfig":Ljava/lang/String; */
final String v3 = ""; // const-string v3, ""
/* .line 385 */
/* .local v3, "smartFormatSetting":Ljava/lang/String; */
(( com.android.server.location.mnlutils.bean.MnlConfigFeature ) v0 ).getFormatSettings ( ); // invoke-virtual {v0}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->getFormatSettings()Ljava/util/LinkedHashMap;
(( java.util.LinkedHashMap ) v4 ).entrySet ( ); // invoke-virtual {v4}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;
v5 = } // :goto_0
if ( v5 != null) { // if-eqz v5, :cond_2
/* check-cast v5, Ljava/util/Map$Entry; */
/* .line 386 */
/* .local v5, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;" */
/* check-cast v6, Ljava/lang/String; */
(( java.lang.String ) v6 ).toLowerCase ( ); // invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;
final String v7 = "GP+"; // const-string v7, "GP+"
(( java.lang.String ) v7 ).toLowerCase ( ); // invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;
v6 = (( java.lang.String ) v6 ).contains ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 387 */
/* move-object v3, v4 */
/* check-cast v3, Ljava/lang/String; */
/* .line 388 */
/* .line 390 */
} // .end local v5 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
} // :cond_1
/* .line 391 */
} // :cond_2
} // :goto_1
final String v4 = "1"; // const-string v4, "1"
v4 = (( java.lang.String ) v4 ).equals ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 392 */
int v4 = 1; // const/4 v4, 0x1
java.lang.String .valueOf ( v4 );
v5 = (( java.lang.String ) v5 ).equals ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_3
/* move v1, v4 */
} // :cond_3
/* nop */
} // :goto_2
/* iput-boolean v1, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->isSmartSwitchEnableFlag:Z */
/* .line 393 */
} // .end method
private Boolean l5Device ( ) {
/* .locals 3 */
/* .line 515 */
/* const-string/jumbo v0, "vendor.debug.gps.support.l5" */
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getInt ( v0,v1 );
int v2 = 1; // const/4 v2, 0x1
/* if-ne v2, v0, :cond_0 */
/* move v1, v2 */
} // :cond_0
} // .end method
private synchronized void registerControlListener ( ) {
/* .locals 4 */
/* monitor-enter p0 */
/* .line 92 */
try { // :try_start_0
/* iget-boolean v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->startControllerListener:Z */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 93 */
/* monitor-exit p0 */
return;
/* .line 95 */
} // :cond_0
try { // :try_start_1
v0 = this.mContext;
/* if-nez v0, :cond_1 */
/* .line 96 */
v0 = this.TAG;
final String v1 = "no context"; // const-string v1, "no context"
android.util.Log .e ( v0,v1 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 97 */
/* monitor-exit p0 */
return;
/* .line 99 */
} // .end local p0 # "this":Lcom/android/server/location/MtkGnssPowerSaveImpl;
} // :cond_1
try { // :try_start_2
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 100 */
android.provider.MiuiSettings$SettingsCloudData .getCloudDataNotifyUri ( );
/* new-instance v2, Lcom/android/server/location/MtkGnssPowerSaveImpl$1; */
int v3 = 0; // const/4 v3, 0x0
/* invoke-direct {v2, p0, v3}, Lcom/android/server/location/MtkGnssPowerSaveImpl$1;-><init>(Lcom/android/server/location/MtkGnssPowerSaveImpl;Landroid/os/Handler;)V */
/* .line 99 */
int v3 = 1; // const/4 v3, 0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2 ); // invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 108 */
v0 = this.TAG;
final String v1 = "register cloud controller listener"; // const-string v1, "register cloud controller listener"
android.util.Log .i ( v0,v1 );
/* .line 109 */
/* iput-boolean v3, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->startControllerListener:Z */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 110 */
/* monitor-exit p0 */
return;
/* .line 91 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* throw v0 */
} // .end method
private void resetWithBackupConfig ( com.android.server.location.mnlutils.bean.MnlConfig p0, com.android.server.location.mnlutils.bean.MnlConfigFeature p1 ) {
/* .locals 2 */
/* .param p1, "backupMnlConfig" # Lcom/android/server/location/mnlutils/bean/MnlConfig; */
/* .param p2, "smartSwitchFeature" # Lcom/android/server/location/mnlutils/bean/MnlConfigFeature; */
/* .line 351 */
final String v0 = "GnssMode"; // const-string v0, "GnssMode"
(( com.android.server.location.mnlutils.bean.MnlConfig ) p1 ).getFeature ( v0 ); // invoke-virtual {p1, v0}, Lcom/android/server/location/mnlutils/bean/MnlConfig;->getFeature(Ljava/lang/String;)Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
/* .line 352 */
/* .local v0, "backupSmartSwitchFeature":Lcom/android/server/location/mnlutils/bean/MnlConfigFeature; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 353 */
(( com.android.server.location.mnlutils.bean.MnlConfigFeature ) v0 ).getFormatSettings ( ); // invoke-virtual {v0}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->getFormatSettings()Ljava/util/LinkedHashMap;
(( com.android.server.location.mnlutils.bean.MnlConfigFeature ) p2 ).setFormatSettings ( v1 ); // invoke-virtual {p2, v1}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->setFormatSettings(Ljava/util/LinkedHashMap;)V
/* .line 354 */
(( com.android.server.location.mnlutils.bean.MnlConfigFeature ) v0 ).getConfig ( ); // invoke-virtual {v0}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->getConfig()Ljava/lang/String;
(( com.android.server.location.mnlutils.bean.MnlConfigFeature ) p2 ).setConfig ( v1 ); // invoke-virtual {p2, v1}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->setConfig(Ljava/lang/String;)V
/* .line 356 */
} // :cond_0
return;
} // .end method
private void resetWithBackupConfig ( com.android.server.location.mnlutils.bean.MnlConfig p0, com.android.server.location.mnlutils.bean.MnlConfigFeature p1, com.android.server.location.mnlutils.bean.MnlConfigFeature p2 ) {
/* .locals 1 */
/* .param p1, "backupMnlConfig" # Lcom/android/server/location/mnlutils/bean/MnlConfig; */
/* .param p2, "l1OnlyFeature" # Lcom/android/server/location/mnlutils/bean/MnlConfigFeature; */
/* .param p3, "glpFeature" # Lcom/android/server/location/mnlutils/bean/MnlConfigFeature; */
/* .line 295 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->resetWithBackupConfig(Lcom/android/server/location/mnlutils/bean/MnlConfig;Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;)V */
/* .line 296 */
return;
} // .end method
private void resetWithBackupConfig ( com.android.server.location.mnlutils.bean.MnlConfig p0, com.android.server.location.mnlutils.bean.MnlConfigFeature p1, com.android.server.location.mnlutils.bean.MnlConfigFeature p2, com.android.server.location.mnlutils.bean.MnlConfigFeature p3 ) {
/* .locals 4 */
/* .param p1, "backupMnlConfig" # Lcom/android/server/location/mnlutils/bean/MnlConfig; */
/* .param p2, "l1OnlyFeature" # Lcom/android/server/location/mnlutils/bean/MnlConfigFeature; */
/* .param p3, "glpFeature" # Lcom/android/server/location/mnlutils/bean/MnlConfigFeature; */
/* .param p4, "smartSwitchFeature" # Lcom/android/server/location/mnlutils/bean/MnlConfigFeature; */
/* .line 301 */
final String v0 = "L1Only"; // const-string v0, "L1Only"
(( com.android.server.location.mnlutils.bean.MnlConfig ) p1 ).getFeature ( v0 ); // invoke-virtual {p1, v0}, Lcom/android/server/location/mnlutils/bean/MnlConfig;->getFeature(Ljava/lang/String;)Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
/* .line 302 */
/* .local v0, "backupL1OnlyFeature":Lcom/android/server/location/mnlutils/bean/MnlConfigFeature; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 303 */
(( com.android.server.location.mnlutils.bean.MnlConfigFeature ) v0 ).getFormatSettings ( ); // invoke-virtual {v0}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->getFormatSettings()Ljava/util/LinkedHashMap;
(( com.android.server.location.mnlutils.bean.MnlConfigFeature ) p2 ).setFormatSettings ( v1 ); // invoke-virtual {p2, v1}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->setFormatSettings(Ljava/util/LinkedHashMap;)V
/* .line 304 */
(( com.android.server.location.mnlutils.bean.MnlConfigFeature ) v0 ).getConfig ( ); // invoke-virtual {v0}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->getConfig()Ljava/lang/String;
(( com.android.server.location.mnlutils.bean.MnlConfigFeature ) p2 ).setConfig ( v1 ); // invoke-virtual {p2, v1}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->setConfig(Ljava/lang/String;)V
/* .line 306 */
} // :cond_0
final String v1 = "GLP"; // const-string v1, "GLP"
(( com.android.server.location.mnlutils.bean.MnlConfig ) p1 ).getFeature ( v1 ); // invoke-virtual {p1, v1}, Lcom/android/server/location/mnlutils/bean/MnlConfig;->getFeature(Ljava/lang/String;)Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
/* .line 307 */
/* .local v1, "backupGlpFeature":Lcom/android/server/location/mnlutils/bean/MnlConfigFeature; */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 308 */
(( com.android.server.location.mnlutils.bean.MnlConfigFeature ) v1 ).getFormatSettings ( ); // invoke-virtual {v1}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->getFormatSettings()Ljava/util/LinkedHashMap;
(( com.android.server.location.mnlutils.bean.MnlConfigFeature ) p3 ).setFormatSettings ( v2 ); // invoke-virtual {p3, v2}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->setFormatSettings(Ljava/util/LinkedHashMap;)V
/* .line 309 */
(( com.android.server.location.mnlutils.bean.MnlConfigFeature ) v1 ).getConfig ( ); // invoke-virtual {v1}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->getConfig()Ljava/lang/String;
(( com.android.server.location.mnlutils.bean.MnlConfigFeature ) p3 ).setConfig ( v2 ); // invoke-virtual {p3, v2}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->setConfig(Ljava/lang/String;)V
/* .line 311 */
} // :cond_1
if ( p4 != null) { // if-eqz p4, :cond_2
/* .line 312 */
final String v2 = "GnssMode"; // const-string v2, "GnssMode"
(( com.android.server.location.mnlutils.bean.MnlConfig ) p1 ).getFeature ( v2 ); // invoke-virtual {p1, v2}, Lcom/android/server/location/mnlutils/bean/MnlConfig;->getFeature(Ljava/lang/String;)Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
/* .line 313 */
/* .local v2, "backupSmartSwitch":Lcom/android/server/location/mnlutils/bean/MnlConfigFeature; */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 314 */
(( com.android.server.location.mnlutils.bean.MnlConfigFeature ) v2 ).getFormatSettings ( ); // invoke-virtual {v2}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->getFormatSettings()Ljava/util/LinkedHashMap;
(( com.android.server.location.mnlutils.bean.MnlConfigFeature ) p4 ).setFormatSettings ( v3 ); // invoke-virtual {p4, v3}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->setFormatSettings(Ljava/util/LinkedHashMap;)V
/* .line 315 */
(( com.android.server.location.mnlutils.bean.MnlConfigFeature ) v2 ).getConfig ( ); // invoke-virtual {v2}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->getConfig()Ljava/lang/String;
(( com.android.server.location.mnlutils.bean.MnlConfigFeature ) p4 ).setConfig ( v3 ); // invoke-virtual {p4, v3}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->setConfig(Ljava/lang/String;)V
/* .line 318 */
} // .end local v2 # "backupSmartSwitch":Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
} // :cond_2
return;
} // .end method
private void restartGps ( ) {
/* .locals 4 */
/* .line 444 */
v0 = this.mContext;
/* if-nez v0, :cond_0 */
/* .line 445 */
return;
/* .line 448 */
} // :cond_0
try { // :try_start_0
v1 = this.locationManager;
/* if-nez v1, :cond_1 */
/* .line 449 */
final String v1 = "location"; // const-string v1, "location"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/location/LocationManager; */
this.locationManager = v0;
/* .line 451 */
} // :cond_1
v0 = this.locationManager;
final String v1 = "gps"; // const-string v1, "gps"
v0 = (( android.location.LocationManager ) v0 ).isProviderEnabled ( v1 ); // invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z
/* .line 452 */
/* .local v0, "gpsEnable":Z */
/* if-nez v0, :cond_2 */
/* .line 453 */
v1 = this.TAG;
final String v2 = "gnss is close, don\'t need restart"; // const-string v2, "gnss is close, don\'t need restart"
android.util.Log .d ( v1,v2 );
/* .line 454 */
return;
/* .line 456 */
} // :cond_2
v1 = this.TAG;
final String v2 = "restart gnss"; // const-string v2, "restart gnss"
android.util.Log .d ( v1,v2 );
/* .line 457 */
v1 = this.locationManager;
/* .line 458 */
v2 = android.os.UserHandle .myUserId ( );
android.os.UserHandle .of ( v2 );
/* .line 457 */
int v3 = 0; // const/4 v3, 0x0
(( android.location.LocationManager ) v1 ).setLocationEnabledForUser ( v3, v2 ); // invoke-virtual {v1, v3, v2}, Landroid/location/LocationManager;->setLocationEnabledForUser(ZLandroid/os/UserHandle;)V
/* .line 459 */
v1 = this.locationManager;
/* .line 460 */
v2 = android.os.UserHandle .myUserId ( );
android.os.UserHandle .of ( v2 );
/* .line 459 */
int v3 = 1; // const/4 v3, 0x1
(( android.location.LocationManager ) v1 ).setLocationEnabledForUser ( v3, v2 ); // invoke-virtual {v1, v3, v2}, Landroid/location/LocationManager;->setLocationEnabledForUser(ZLandroid/os/UserHandle;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 463 */
} // .end local v0 # "gpsEnable":Z
/* .line 461 */
/* :catch_0 */
/* move-exception v0 */
/* .line 462 */
/* .local v0, "e":Ljava/lang/Exception; */
v1 = this.TAG;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "exception in restart gps : "; // const-string v3, "exception in restart gps : "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
android.util.Log .getStackTraceString ( v0 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v2 );
/* .line 464 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void sendModeChangedBroadcast ( ) {
/* .locals 3 */
/* .line 546 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "android.location.MODE_CHANGED"; // const-string v1, "android.location.MODE_CHANGED"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 547 */
/* .local v0, "intent":Landroid/content/Intent; */
/* const/high16 v1, 0x40000000 # 2.0f */
(( android.content.Intent ) v0 ).addFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 548 */
v1 = this.mContext;
v2 = android.os.UserHandle.ALL;
(( android.content.Context ) v1 ).sendBroadcastAsUser ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
/* .line 549 */
return;
} // .end method
private void setGlpFeature ( com.android.server.location.mnlutils.bean.MnlConfigFeature p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "glpFeature" # Lcom/android/server/location/mnlutils/bean/MnlConfigFeature; */
/* .param p2, "enable" # Z */
/* .line 440 */
if ( p2 != null) { // if-eqz p2, :cond_0
final String v0 = "1"; // const-string v0, "1"
} // :cond_0
final String v0 = "0"; // const-string v0, "0"
} // :goto_0
(( com.android.server.location.mnlutils.bean.MnlConfigFeature ) p1 ).setConfig ( v0 ); // invoke-virtual {p1, v0}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->setConfig(Ljava/lang/String;)V
/* .line 441 */
return;
} // .end method
private void setL1OnlyFeature ( com.android.server.location.mnlutils.bean.MnlConfigFeature p0, Boolean p1 ) {
/* .locals 5 */
/* .param p1, "l1OnlyFeature" # Lcom/android/server/location/mnlutils/bean/MnlConfigFeature; */
/* .param p2, "enable" # Z */
/* .line 397 */
/* if-nez p1, :cond_0 */
/* .line 398 */
v0 = this.TAG;
final String v1 = "l1OnlyFeature or glpFeature is null"; // const-string v1, "l1OnlyFeature or glpFeature is null"
android.util.Log .d ( v0,v1 );
/* .line 399 */
return;
/* .line 402 */
} // :cond_0
final String v0 = ""; // const-string v0, ""
/* .line 404 */
/* .local v0, "l1OnlySettingKey":Ljava/lang/String; */
(( com.android.server.location.mnlutils.bean.MnlConfigFeature ) p1 ).getFormatSettings ( ); // invoke-virtual {p1}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->getFormatSettings()Ljava/util/LinkedHashMap;
(( java.util.LinkedHashMap ) v1 ).keySet ( ); // invoke-virtual {v1}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Ljava/lang/String; */
/* .line 405 */
/* .local v2, "key":Ljava/lang/String; */
(( java.lang.String ) v2 ).toLowerCase ( ); // invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;
final String v4 = "L1 only"; // const-string v4, "L1 only"
(( java.lang.String ) v4 ).toLowerCase ( ); // invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;
v3 = (( java.lang.String ) v3 ).contains ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 406 */
/* move-object v0, v2 */
/* .line 408 */
} // .end local v2 # "key":Ljava/lang/String;
} // :cond_1
/* .line 410 */
} // :cond_2
final String v1 = ""; // const-string v1, ""
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 411 */
return;
/* .line 413 */
} // :cond_3
final String v1 = "1"; // const-string v1, "1"
final String v2 = "0"; // const-string v2, "0"
if ( p2 != null) { // if-eqz p2, :cond_4
/* move-object v3, v1 */
} // :cond_4
/* move-object v3, v2 */
} // :goto_1
(( com.android.server.location.mnlutils.bean.MnlConfigFeature ) p1 ).setConfig ( v3 ); // invoke-virtual {p1, v3}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->setConfig(Ljava/lang/String;)V
/* .line 414 */
(( com.android.server.location.mnlutils.bean.MnlConfigFeature ) p1 ).getFormatSettings ( ); // invoke-virtual {p1}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->getFormatSettings()Ljava/util/LinkedHashMap;
if ( p2 != null) { // if-eqz p2, :cond_5
} // :cond_5
/* move-object v1, v2 */
} // :goto_2
(( java.util.LinkedHashMap ) v3 ).put ( v0, v1 ); // invoke-virtual {v3, v0, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 415 */
v1 = this.TAG;
/* const-string/jumbo v2, "setL1OnlyFeature done" */
android.util.Log .d ( v1,v2 );
/* .line 416 */
return;
} // .end method
private void setSmartSwitchFeature ( com.android.server.location.mnlutils.bean.MnlConfigFeature p0, Boolean p1, Integer p2 ) {
/* .locals 5 */
/* .param p1, "smartSwitchFeature" # Lcom/android/server/location/mnlutils/bean/MnlConfigFeature; */
/* .param p2, "enable" # Z */
/* .param p3, "gnssModeType" # I */
/* .line 419 */
/* if-nez p1, :cond_0 */
/* .line 420 */
v0 = this.TAG;
/* const-string/jumbo v1, "smartSwitchFeature is null" */
android.util.Log .d ( v0,v1 );
/* .line 421 */
return;
/* .line 424 */
} // :cond_0
final String v0 = ""; // const-string v0, ""
/* .line 426 */
/* .local v0, "gnssModeSettingKey":Ljava/lang/String; */
(( com.android.server.location.mnlutils.bean.MnlConfigFeature ) p1 ).getFormatSettings ( ); // invoke-virtual {p1}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->getFormatSettings()Ljava/util/LinkedHashMap;
(( java.util.LinkedHashMap ) v1 ).keySet ( ); // invoke-virtual {v1}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Ljava/lang/String; */
/* .line 427 */
/* .local v2, "key":Ljava/lang/String; */
(( java.lang.String ) v2 ).toLowerCase ( ); // invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;
final String v4 = "GP+"; // const-string v4, "GP+"
(( java.lang.String ) v4 ).toLowerCase ( ); // invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;
v3 = (( java.lang.String ) v3 ).contains ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 428 */
/* move-object v0, v2 */
/* .line 430 */
} // .end local v2 # "key":Ljava/lang/String;
} // :cond_1
/* .line 432 */
} // :cond_2
final String v1 = ""; // const-string v1, ""
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 433 */
return;
/* .line 435 */
} // :cond_3
if ( p2 != null) { // if-eqz p2, :cond_4
final String v1 = "1"; // const-string v1, "1"
} // :cond_4
final String v1 = "0"; // const-string v1, "0"
} // :goto_1
(( com.android.server.location.mnlutils.bean.MnlConfigFeature ) p1 ).setConfig ( v1 ); // invoke-virtual {p1, v1}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->setConfig(Ljava/lang/String;)V
/* .line 436 */
(( com.android.server.location.mnlutils.bean.MnlConfigFeature ) p1 ).getFormatSettings ( ); // invoke-virtual {p1}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->getFormatSettings()Ljava/util/LinkedHashMap;
java.lang.String .valueOf ( p3 );
(( java.util.LinkedHashMap ) v1 ).put ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 437 */
return;
} // .end method
private void updateDisableL5CloudConfig ( ) {
/* .locals 6 */
/* .line 113 */
int v0 = 0; // const/4 v0, 0x0
final String v1 = "persist.sys.gps.support_disable_l5_config"; // const-string v1, "persist.sys.gps.support_disable_l5_config"
v0 = android.os.SystemProperties .getBoolean ( v1,v0 );
/* .line 114 */
/* .local v0, "settingsNow":Z */
v2 = this.mContext;
/* .line 115 */
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 114 */
final String v3 = "mtkGnssConfig"; // const-string v3, "mtkGnssConfig"
final String v4 = "disableL5"; // const-string v4, "disableL5"
int v5 = 0; // const/4 v5, 0x0
android.provider.MiuiSettings$SettingsCloudData .getCloudDataString ( v2,v3,v4,v5 );
/* .line 119 */
/* .local v2, "newSettings":Ljava/lang/String; */
/* if-nez v2, :cond_0 */
/* .line 120 */
return;
/* .line 122 */
} // :cond_0
v3 = this.TAG;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "receive l5 config, new value: "; // const-string v5, "receive l5 config, new value: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = ", settingsNow: "; // const-string v5, ", settingsNow: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v3,v4 );
/* .line 124 */
v3 = java.lang.Boolean .parseBoolean ( v2 );
/* .line 125 */
/* .local v3, "newSettingsBool":Z */
/* if-ne v3, v0, :cond_1 */
/* .line 126 */
return;
/* .line 128 */
} // :cond_1
java.lang.String .valueOf ( v3 );
android.os.SystemProperties .set ( v1,v4 );
/* .line 129 */
return;
} // .end method
private void updateDisableSatelliteCloudConfig ( ) {
/* .locals 6 */
/* .line 132 */
int v0 = 0; // const/4 v0, 0x0
final String v1 = "persist.sys.gps.support_disable_satellite_config"; // const-string v1, "persist.sys.gps.support_disable_satellite_config"
v0 = android.os.SystemProperties .getBoolean ( v1,v0 );
/* .line 133 */
/* .local v0, "settingsNow":Z */
v2 = this.mContext;
/* .line 134 */
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 133 */
final String v3 = "mtkGnssConfig"; // const-string v3, "mtkGnssConfig"
final String v4 = "disableSatellite"; // const-string v4, "disableSatellite"
int v5 = 0; // const/4 v5, 0x0
android.provider.MiuiSettings$SettingsCloudData .getCloudDataString ( v2,v3,v4,v5 );
/* .line 138 */
/* .local v2, "newSettings":Ljava/lang/String; */
/* if-nez v2, :cond_0 */
/* .line 139 */
return;
/* .line 141 */
} // :cond_0
v3 = this.TAG;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "receive constellation config, new value: "; // const-string v5, "receive constellation config, new value: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = ", settingsNow: "; // const-string v5, ", settingsNow: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v3,v4 );
/* .line 143 */
v3 = java.lang.Boolean .parseBoolean ( v2 );
/* .line 144 */
/* .local v3, "newSettingsBool":Z */
/* if-ne v3, v0, :cond_1 */
/* .line 145 */
return;
/* .line 147 */
} // :cond_1
java.lang.String .valueOf ( v3 );
android.os.SystemProperties .set ( v1,v4 );
/* .line 148 */
return;
} // .end method
/* # virtual methods */
public synchronized void activeSmartSwitch ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "gnssMode" # I */
/* monitor-enter p0 */
/* .line 203 */
try { // :try_start_0
v0 = this.mnlConfigUtils;
/* if-nez v0, :cond_0 */
/* .line 204 */
/* new-instance v0, Lcom/android/server/location/mnlutils/MnlConfigUtils; */
/* invoke-direct {v0}, Lcom/android/server/location/mnlutils/MnlConfigUtils;-><init>()V */
this.mnlConfigUtils = v0;
/* .line 206 */
} // .end local p0 # "this":Lcom/android/server/location/MtkGnssPowerSaveImpl;
} // :cond_0
v0 = this.mnlConfigUtils;
(( com.android.server.location.mnlutils.MnlConfigUtils ) v0 ).getMnlConfig ( ); // invoke-virtual {v0}, Lcom/android/server/location/mnlutils/MnlConfigUtils;->getMnlConfig()Lcom/android/server/location/mnlutils/bean/MnlConfig;
/* .line 207 */
/* .local v0, "mnlConfig":Lcom/android/server/location/mnlutils/bean/MnlConfig; */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 208 */
final String v1 = "GnssMode"; // const-string v1, "GnssMode"
(( com.android.server.location.mnlutils.bean.MnlConfig ) v0 ).getFeature ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/location/mnlutils/bean/MnlConfig;->getFeature(Ljava/lang/String;)Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
/* .line 209 */
/* .local v1, "SmartSwitchFeature":Lcom/android/server/location/mnlutils/bean/MnlConfigFeature; */
int v2 = 1; // const/4 v2, 0x1
/* invoke-direct {p0, v1, v2, p1}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->setSmartSwitchFeature(Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;ZI)V */
/* .line 210 */
v2 = this.mnlConfigUtils;
v2 = (( com.android.server.location.mnlutils.MnlConfigUtils ) v2 ).saveMnlConfig ( v0 ); // invoke-virtual {v2, v0}, Lcom/android/server/location/mnlutils/MnlConfigUtils;->saveMnlConfig(Lcom/android/server/location/mnlutils/bean/MnlConfig;)Z
/* if-nez v2, :cond_1 */
/* .line 211 */
v2 = this.TAG;
final String v3 = "save mnl config fail"; // const-string v3, "save mnl config fail"
android.util.Log .e ( v2,v3 );
/* .line 213 */
} // :cond_1
/* invoke-direct {p0}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->restartGps()V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 217 */
} // .end local v0 # "mnlConfig":Lcom/android/server/location/mnlutils/bean/MnlConfig;
} // .end local v1 # "SmartSwitchFeature":Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
} // :cond_2
/* .line 202 */
} // .end local p1 # "gnssMode":I
/* :catchall_0 */
/* move-exception p1 */
/* .line 215 */
/* .restart local p1 # "gnssMode":I */
/* :catch_0 */
/* move-exception v0 */
/* .line 216 */
/* .local v0, "e":Ljava/lang/Exception; */
try { // :try_start_1
v1 = this.TAG;
android.util.Log .getStackTraceString ( v0 );
android.util.Log .e ( v1,v2 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 218 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
/* monitor-exit p0 */
return;
/* .line 202 */
} // .end local p1 # "gnssMode":I
} // :goto_1
/* monitor-exit p0 */
/* throw p1 */
} // .end method
public synchronized void disableL5AndDisableGlp ( ) {
/* .locals 5 */
/* monitor-enter p0 */
/* .line 234 */
try { // :try_start_0
v0 = this.mnlConfigUtils;
/* if-nez v0, :cond_0 */
/* .line 235 */
/* new-instance v0, Lcom/android/server/location/mnlutils/MnlConfigUtils; */
/* invoke-direct {v0}, Lcom/android/server/location/mnlutils/MnlConfigUtils;-><init>()V */
this.mnlConfigUtils = v0;
/* .line 238 */
} // .end local p0 # "this":Lcom/android/server/location/MtkGnssPowerSaveImpl;
} // :cond_0
v0 = this.mnlConfigUtils;
(( com.android.server.location.mnlutils.MnlConfigUtils ) v0 ).getMnlConfig ( ); // invoke-virtual {v0}, Lcom/android/server/location/mnlutils/MnlConfigUtils;->getMnlConfig()Lcom/android/server/location/mnlutils/bean/MnlConfig;
/* .line 240 */
/* .local v0, "mnlConfig":Lcom/android/server/location/mnlutils/bean/MnlConfig; */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 241 */
final String v1 = "L1Only"; // const-string v1, "L1Only"
(( com.android.server.location.mnlutils.bean.MnlConfig ) v0 ).getFeature ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/location/mnlutils/bean/MnlConfig;->getFeature(Ljava/lang/String;)Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
/* .line 242 */
/* .local v1, "l1OnlyFeature":Lcom/android/server/location/mnlutils/bean/MnlConfigFeature; */
final String v2 = "GLP"; // const-string v2, "GLP"
(( com.android.server.location.mnlutils.bean.MnlConfig ) v0 ).getFeature ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/location/mnlutils/bean/MnlConfig;->getFeature(Ljava/lang/String;)Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
/* .line 243 */
/* .local v2, "glpFeature":Lcom/android/server/location/mnlutils/bean/MnlConfigFeature; */
if ( v1 != null) { // if-eqz v1, :cond_2
/* if-nez v2, :cond_1 */
/* .line 247 */
} // :cond_1
int v3 = 1; // const/4 v3, 0x1
/* invoke-direct {p0, v1, v3}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->setL1OnlyFeature(Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;Z)V */
/* .line 248 */
int v3 = 0; // const/4 v3, 0x0
/* invoke-direct {p0, v2, v3}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->setGlpFeature(Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;Z)V */
/* .line 249 */
v3 = this.mnlConfigUtils;
v3 = (( com.android.server.location.mnlutils.MnlConfigUtils ) v3 ).saveMnlConfig ( v0 ); // invoke-virtual {v3, v0}, Lcom/android/server/location/mnlutils/MnlConfigUtils;->saveMnlConfig(Lcom/android/server/location/mnlutils/bean/MnlConfig;)Z
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 250 */
v3 = this.TAG;
final String v4 = "save mnl config success"; // const-string v4, "save mnl config success"
android.util.Log .d ( v3,v4 );
/* .line 251 */
/* invoke-direct {p0}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->restartGps()V */
/* .line 244 */
} // :cond_2
} // :goto_0
v3 = this.TAG;
final String v4 = "l1Onlyfeature or glpFeature is null"; // const-string v4, "l1Onlyfeature or glpFeature is null"
android.util.Log .d ( v3,v4 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 245 */
/* monitor-exit p0 */
return;
/* .line 256 */
} // .end local v0 # "mnlConfig":Lcom/android/server/location/mnlutils/bean/MnlConfig;
} // .end local v1 # "l1OnlyFeature":Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
} // .end local v2 # "glpFeature":Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
} // :cond_3
} // :goto_1
/* .line 233 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 254 */
/* :catch_0 */
/* move-exception v0 */
/* .line 255 */
/* .local v0, "e":Ljava/lang/Exception; */
try { // :try_start_1
v1 = this.TAG;
android.util.Log .getStackTraceString ( v0 );
android.util.Log .e ( v1,v2 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 257 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_2
/* monitor-exit p0 */
return;
/* .line 233 */
} // :goto_3
/* monitor-exit p0 */
/* throw v0 */
} // .end method
public synchronized void enableL5AndEnableGlp ( ) {
/* .locals 6 */
/* monitor-enter p0 */
/* .line 261 */
try { // :try_start_0
v0 = this.mnlConfigUtils;
/* if-nez v0, :cond_0 */
/* .line 262 */
/* new-instance v0, Lcom/android/server/location/mnlutils/MnlConfigUtils; */
/* invoke-direct {v0}, Lcom/android/server/location/mnlutils/MnlConfigUtils;-><init>()V */
this.mnlConfigUtils = v0;
/* .line 264 */
} // .end local p0 # "this":Lcom/android/server/location/MtkGnssPowerSaveImpl;
} // :cond_0
v0 = this.mnlConfigUtils;
(( com.android.server.location.mnlutils.MnlConfigUtils ) v0 ).getMnlConfig ( ); // invoke-virtual {v0}, Lcom/android/server/location/mnlutils/MnlConfigUtils;->getMnlConfig()Lcom/android/server/location/mnlutils/bean/MnlConfig;
/* .line 265 */
/* .local v0, "mnlConfig":Lcom/android/server/location/mnlutils/bean/MnlConfig; */
v1 = this.mnlConfigUtils;
(( com.android.server.location.mnlutils.MnlConfigUtils ) v1 ).getBackUpMnlConfig ( ); // invoke-virtual {v1}, Lcom/android/server/location/mnlutils/MnlConfigUtils;->getBackUpMnlConfig()Lcom/android/server/location/mnlutils/bean/MnlConfig;
/* .line 267 */
/* .local v1, "backupMnlConfig":Lcom/android/server/location/mnlutils/bean/MnlConfig; */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 268 */
final String v2 = "L1Only"; // const-string v2, "L1Only"
(( com.android.server.location.mnlutils.bean.MnlConfig ) v0 ).getFeature ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/location/mnlutils/bean/MnlConfig;->getFeature(Ljava/lang/String;)Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
/* .line 269 */
/* .local v2, "l1OnlyFeature":Lcom/android/server/location/mnlutils/bean/MnlConfigFeature; */
final String v3 = "GLP"; // const-string v3, "GLP"
(( com.android.server.location.mnlutils.bean.MnlConfig ) v0 ).getFeature ( v3 ); // invoke-virtual {v0, v3}, Lcom/android/server/location/mnlutils/bean/MnlConfig;->getFeature(Ljava/lang/String;)Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
/* .line 271 */
/* .local v3, "glpFeature":Lcom/android/server/location/mnlutils/bean/MnlConfigFeature; */
if ( v2 != null) { // if-eqz v2, :cond_3
/* if-nez v3, :cond_1 */
/* .line 276 */
} // :cond_1
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 277 */
/* invoke-direct {p0, v1, v2, v3}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->resetWithBackupConfig(Lcom/android/server/location/mnlutils/bean/MnlConfig;Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;)V */
/* .line 279 */
} // :cond_2
int v4 = 0; // const/4 v4, 0x0
/* invoke-direct {p0, v2, v4}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->setL1OnlyFeature(Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;Z)V */
/* .line 280 */
int v4 = 1; // const/4 v4, 0x1
/* invoke-direct {p0, v3, v4}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->setGlpFeature(Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;Z)V */
/* .line 282 */
} // :goto_0
v4 = this.mnlConfigUtils;
v4 = (( com.android.server.location.mnlutils.MnlConfigUtils ) v4 ).saveMnlConfig ( v0 ); // invoke-virtual {v4, v0}, Lcom/android/server/location/mnlutils/MnlConfigUtils;->saveMnlConfig(Lcom/android/server/location/mnlutils/bean/MnlConfig;)Z
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 283 */
v4 = this.TAG;
final String v5 = "save mnl config success"; // const-string v5, "save mnl config success"
android.util.Log .d ( v4,v5 );
/* .line 284 */
/* invoke-direct {p0}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->restartGps()V */
/* .line 272 */
} // :cond_3
} // :goto_1
v4 = this.TAG;
final String v5 = "l1OnlyFeature, smartSwitchFeature or glpFeature is null"; // const-string v5, "l1OnlyFeature, smartSwitchFeature or glpFeature is null"
android.util.Log .d ( v4,v5 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 273 */
/* monitor-exit p0 */
return;
/* .line 289 */
} // .end local v0 # "mnlConfig":Lcom/android/server/location/mnlutils/bean/MnlConfig;
} // .end local v1 # "backupMnlConfig":Lcom/android/server/location/mnlutils/bean/MnlConfig;
} // .end local v2 # "l1OnlyFeature":Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
} // .end local v3 # "glpFeature":Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
} // :cond_4
} // :goto_2
/* .line 260 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 287 */
/* :catch_0 */
/* move-exception v0 */
/* .line 288 */
/* .local v0, "e":Ljava/lang/Exception; */
try { // :try_start_1
v1 = this.TAG;
android.util.Log .getStackTraceString ( v0 );
android.util.Log .e ( v1,v2 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 290 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_3
/* monitor-exit p0 */
return;
/* .line 260 */
} // :goto_4
/* monitor-exit p0 */
/* throw v0 */
} // .end method
public synchronized java.lang.String getMtkGnssConfig ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "name" # Ljava/lang/String; */
/* monitor-enter p0 */
/* .line 165 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
v1 = this.mnlConfigUtils;
/* if-nez v1, :cond_0 */
/* .line 166 */
/* new-instance v1, Lcom/android/server/location/mnlutils/MnlConfigUtils; */
/* invoke-direct {v1}, Lcom/android/server/location/mnlutils/MnlConfigUtils;-><init>()V */
this.mnlConfigUtils = v1;
/* .line 168 */
} // .end local p0 # "this":Lcom/android/server/location/MtkGnssPowerSaveImpl;
} // :cond_0
v1 = this.mnlConfigUtils;
(( com.android.server.location.mnlutils.MnlConfigUtils ) v1 ).getMnlConfig ( ); // invoke-virtual {v1}, Lcom/android/server/location/mnlutils/MnlConfigUtils;->getMnlConfig()Lcom/android/server/location/mnlutils/bean/MnlConfig;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 169 */
/* .local v1, "mnlConfig":Lcom/android/server/location/mnlutils/bean/MnlConfig; */
/* if-nez v1, :cond_1 */
/* .line 170 */
/* monitor-exit p0 */
/* .line 173 */
/* .restart local p0 # "this":Lcom/android/server/location/MtkGnssPowerSaveImpl; */
} // :cond_1
try { // :try_start_1
v2 = (( java.lang.String ) p1 ).hashCode ( ); // invoke-virtual {p1}, Ljava/lang/String;->hashCode()I
/* sparse-switch v2, :sswitch_data_0 */
} // .end local p0 # "this":Lcom/android/server/location/MtkGnssPowerSaveImpl;
} // :cond_2
/* .restart local p0 # "this":Lcom/android/server/location/MtkGnssPowerSaveImpl; */
/* :sswitch_0 */
/* const-string/jumbo v2, "smartSwitchEnable" */
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_3
int v2 = 2; // const/4 v2, 0x2
} // .end local p0 # "this":Lcom/android/server/location/MtkGnssPowerSaveImpl;
/* :sswitch_1 */
final String v2 = "l1OnlyEnable"; // const-string v2, "l1OnlyEnable"
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
int v2 = 1; // const/4 v2, 0x1
/* :sswitch_2 */
final String v2 = "mnlVersion"; // const-string v2, "mnlVersion"
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
int v2 = 0; // const/4 v2, 0x0
} // :cond_3
} // :goto_0
int v2 = -1; // const/4 v2, -0x1
} // :goto_1
/* packed-switch v2, :pswitch_data_0 */
/* .line 181 */
/* .line 179 */
/* :pswitch_0 */
v2 = /* invoke-direct {p0, v1}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->isSmartSwitchEnable(Lcom/android/server/location/mnlutils/bean/MnlConfig;)Z */
java.lang.String .valueOf ( v2 );
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* monitor-exit p0 */
/* .line 177 */
/* :pswitch_1 */
try { // :try_start_2
v2 = /* invoke-direct {p0, v1}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->isL1OnlyEnable(Lcom/android/server/location/mnlutils/bean/MnlConfig;)Z */
java.lang.String .valueOf ( v2 );
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* monitor-exit p0 */
/* .line 175 */
/* :pswitch_2 */
try { // :try_start_3
(( com.android.server.location.mnlutils.bean.MnlConfig ) v1 ).getVersion ( ); // invoke-virtual {v1}, Lcom/android/server/location/mnlutils/bean/MnlConfig;->getVersion()Ljava/lang/String;
/* :try_end_3 */
/* .catch Ljava/lang/Exception; {:try_start_3 ..:try_end_3} :catch_0 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* monitor-exit p0 */
/* .line 181 */
} // :goto_2
/* monitor-exit p0 */
/* .line 164 */
} // .end local v1 # "mnlConfig":Lcom/android/server/location/mnlutils/bean/MnlConfig;
} // .end local p1 # "name":Ljava/lang/String;
/* :catchall_0 */
/* move-exception p1 */
/* .line 183 */
/* .restart local p1 # "name":Ljava/lang/String; */
/* :catch_0 */
/* move-exception v1 */
/* .line 184 */
/* .local v1, "e":Ljava/lang/Exception; */
try { // :try_start_4
v2 = this.TAG;
(( java.lang.Exception ) v1 ).getMessage ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
android.util.Log .d ( v2,v3 );
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* .line 185 */
/* monitor-exit p0 */
/* .line 164 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // .end local p1 # "name":Ljava/lang/String;
} // :goto_3
/* monitor-exit p0 */
/* throw p1 */
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x3d97f6d -> :sswitch_2 */
/* 0x26949774 -> :sswitch_1 */
/* 0x37501640 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public synchronized void recoveryAllSatellite ( ) {
/* .locals 5 */
/* monitor-enter p0 */
/* .line 322 */
try { // :try_start_0
v0 = this.mnlConfigUtils;
/* if-nez v0, :cond_0 */
/* .line 323 */
/* new-instance v0, Lcom/android/server/location/mnlutils/MnlConfigUtils; */
/* invoke-direct {v0}, Lcom/android/server/location/mnlutils/MnlConfigUtils;-><init>()V */
this.mnlConfigUtils = v0;
/* .line 325 */
} // .end local p0 # "this":Lcom/android/server/location/MtkGnssPowerSaveImpl;
} // :cond_0
v0 = this.mnlConfigUtils;
(( com.android.server.location.mnlutils.MnlConfigUtils ) v0 ).getMnlConfig ( ); // invoke-virtual {v0}, Lcom/android/server/location/mnlutils/MnlConfigUtils;->getMnlConfig()Lcom/android/server/location/mnlutils/bean/MnlConfig;
/* .line 326 */
/* .local v0, "mnlConfig":Lcom/android/server/location/mnlutils/bean/MnlConfig; */
v1 = this.mnlConfigUtils;
(( com.android.server.location.mnlutils.MnlConfigUtils ) v1 ).getBackUpMnlConfig ( ); // invoke-virtual {v1}, Lcom/android/server/location/mnlutils/MnlConfigUtils;->getBackUpMnlConfig()Lcom/android/server/location/mnlutils/bean/MnlConfig;
/* .line 328 */
/* .local v1, "backupMnlConfig":Lcom/android/server/location/mnlutils/bean/MnlConfig; */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 329 */
final String v2 = "GnssMode"; // const-string v2, "GnssMode"
(( com.android.server.location.mnlutils.bean.MnlConfig ) v0 ).getFeature ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/location/mnlutils/bean/MnlConfig;->getFeature(Ljava/lang/String;)Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
/* .line 330 */
/* .local v2, "smartSwitchFeature":Lcom/android/server/location/mnlutils/bean/MnlConfigFeature; */
/* if-nez v2, :cond_1 */
/* .line 331 */
v3 = this.TAG;
/* const-string/jumbo v4, "smartSwitchFeature or glpFeature is null" */
android.util.Log .d ( v3,v4 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 332 */
/* monitor-exit p0 */
return;
/* .line 334 */
} // :cond_1
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 335 */
try { // :try_start_1
/* invoke-direct {p0, v1, v2}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->resetWithBackupConfig(Lcom/android/server/location/mnlutils/bean/MnlConfig;Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;)V */
/* .line 337 */
} // :cond_2
int v3 = 0; // const/4 v3, 0x0
int v4 = 6; // const/4 v4, 0x6
/* invoke-direct {p0, v2, v3, v4}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->setSmartSwitchFeature(Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;ZI)V */
/* .line 339 */
} // :goto_0
v3 = this.mnlConfigUtils;
v3 = (( com.android.server.location.mnlutils.MnlConfigUtils ) v3 ).saveMnlConfig ( v0 ); // invoke-virtual {v3, v0}, Lcom/android/server/location/mnlutils/MnlConfigUtils;->saveMnlConfig(Lcom/android/server/location/mnlutils/bean/MnlConfig;)Z
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 340 */
v3 = this.TAG;
final String v4 = "recovery mnl config success"; // const-string v4, "recovery mnl config success"
android.util.Log .d ( v3,v4 );
/* .line 341 */
/* invoke-direct {p0}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->restartGps()V */
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 346 */
} // .end local v0 # "mnlConfig":Lcom/android/server/location/mnlutils/bean/MnlConfig;
} // .end local v1 # "backupMnlConfig":Lcom/android/server/location/mnlutils/bean/MnlConfig;
} // .end local v2 # "smartSwitchFeature":Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
} // :cond_3
/* .line 321 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 344 */
/* :catch_0 */
/* move-exception v0 */
/* .line 345 */
/* .local v0, "e":Ljava/lang/Exception; */
try { // :try_start_2
v1 = this.TAG;
android.util.Log .getStackTraceString ( v0 );
android.util.Log .e ( v1,v2 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 347 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
/* monitor-exit p0 */
return;
/* .line 321 */
} // :goto_2
/* monitor-exit p0 */
/* throw v0 */
} // .end method
public void startPowerSaveListener ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 152 */
this.mContext = p1;
/* .line 153 */
/* new-instance v0, Lcom/android/server/location/MtkGnssPowerSaveImpl$SmartSatelliteHandler; */
android.os.Looper .myLooper ( );
/* invoke-direct {v0, p0, v1}, Lcom/android/server/location/MtkGnssPowerSaveImpl$SmartSatelliteHandler;-><init>(Lcom/android/server/location/MtkGnssPowerSaveImpl;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 155 */
v0 = this.bootCompletedReceiver;
/* if-nez v0, :cond_0 */
/* .line 156 */
/* new-instance v0, Lcom/android/server/location/MtkGnssPowerSaveImpl$BootCompletedReceiver; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, p0, v1}, Lcom/android/server/location/MtkGnssPowerSaveImpl$BootCompletedReceiver;-><init>(Lcom/android/server/location/MtkGnssPowerSaveImpl;Lcom/android/server/location/MtkGnssPowerSaveImpl$BootCompletedReceiver-IA;)V */
this.bootCompletedReceiver = v0;
/* .line 157 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 158 */
/* .local v0, "intentFilter":Landroid/content/IntentFilter; */
final String v1 = "android.intent.action.BOOT_COMPLETED"; // const-string v1, "android.intent.action.BOOT_COMPLETED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 159 */
v1 = this.bootCompletedReceiver;
(( android.content.Context ) p1 ).registerReceiver ( v1, v0 ); // invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 161 */
} // .end local v0 # "intentFilter":Landroid/content/IntentFilter;
} // :cond_0
return;
} // .end method
public Boolean supportMtkGnssConfig ( ) {
/* .locals 2 */
/* .line 83 */
/* invoke-direct {p0}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->registerControlListener()V */
/* .line 84 */
v0 = /* invoke-direct {p0}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->isCnVersion()Z */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
final String v0 = "persist.sys.gps.support_gnss_config"; // const-string v0, "persist.sys.gps.support_gnss_config"
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
} // .end method
