.class public Lcom/android/server/location/MtkGnssPowerSaveImpl;
.super Ljava/lang/Object;
.source "MtkGnssPowerSaveImpl.java"

# interfaces
.implements Lcom/android/server/location/MtkGnssPowerSaveStub;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/location/MtkGnssPowerSaveImpl$SmartSatelliteHandler;,
        Lcom/android/server/location/MtkGnssPowerSaveImpl$BootCompletedReceiver;,
        Lcom/android/server/location/MtkGnssPowerSaveImpl$GnssPowerSaveObs;,
        Lcom/android/server/location/MtkGnssPowerSaveImpl$GnssSmartSwitchObs;,
        Lcom/android/server/location/MtkGnssPowerSaveImpl$PowerSaveReceiver;
    }
.end annotation


# static fields
.field private static final ACTION_BOOT_COMPLETED:Ljava/lang/String; = "android.intent.action.BOOT_COMPLETED"

.field private static final ACTION_POWER_SAVE_MODE_CHANGED:Ljava/lang/String; = "miui.intent.action.POWER_SAVE_MODE_CHANGED"

.field private static final CLOUD_KEY_MTK_DISABLE_L5:Ljava/lang/String; = "disableL5"

.field private static final CLOUD_KEY_MTK_DISABLE_SATELLITE:Ljava/lang/String; = "disableSatellite"

.field private static final CLOUD_MODULE_MTK_GNSS_CONFIG:Ljava/lang/String; = "mtkGnssConfig"

.field private static final DISABLE_L5_MODE_CHANGED_ACTION:Ljava/lang/String; = "android.location.MODE_CHANGED"

.field private static final GNSS_BD_GPS_QZSS:I = 0x1

.field private static final GNSS_MODE_BD_GPS_GA_GL_QZSS:I = 0x6

.field private static final KEY_POWER_MODE_OPEN:Ljava/lang/String; = "POWER_SAVE_MODE_OPEN"

.field private static final MI_MNL_CONFIG_KEY_L1_ONLY_ENABLE:Ljava/lang/String; = "l1OnlyEnable"

.field private static final MI_MNL_CONFIG_KEY_MNL_VERSION:Ljava/lang/String; = "mnlVersion"

.field private static final MI_MNL_CONFIG_KEY_SMART_SWITCH_ENABLE:Ljava/lang/String; = "smartSwitchEnable"

.field private static final MSG_DISABLE_L5_AND_SATELLITE:I = 0x2

.field private static final MSG_RECOVERY_SATELLITE:I = 0x0

.field private static final MSG_SWITCH_SATELLITE_FOR_L1_ONLY_DEVICE:I = 0x1

.field private static final MTK_GNSS_CONFIG_SUPPORT_DISABLE_L5_PROP:Ljava/lang/String; = "persist.sys.gps.support_disable_l5_config"

.field private static final MTK_GNSS_CONFIG_SUPPORT_DISABLE_SATELLITE_PROP:Ljava/lang/String; = "persist.sys.gps.support_disable_satellite_config"

.field private static final MTK_GNSS_CONFIG_SUPPORT_L5_PROP:Ljava/lang/String; = "vendor.debug.gps.support.l5"

.field private static final MTK_GNSS_CONFIG_SUPPORT_PROP:Ljava/lang/String; = "persist.sys.gps.support_gnss_config"

.field private static final XM_MTK_GNSS_CONF_DISABLE_L5_CONF:Ljava/lang/String; = "xiaomi_gnss_config_disable_l5"

.field private static final XM_MTK_GNSS_CONF_DISABLE_L5_OFF:I = 0x2

.field private static final XM_MTK_GNSS_CONF_DISABLE_L5_ON:I = 0x1

.field private static final XM_MTK_GNSS_CONF_SMART_SWITCH_CONF:Ljava/lang/String; = "xiaomi_gnss_smart_switch"

.field private static final XM_MTK_GNSS_CONF_SMART_SWITCH_OFF:I = 0x2

.field private static final XM_MTK_GNSS_CONF_SMART_SWITCH_ON:I = 0x1


# instance fields
.field private final MNL_DGPSMode_FEATURE_NAME:Ljava/lang/String;

.field private final MNL_GLP_FEATURE_NAME:Ljava/lang/String;

.field private final MNL_GNSS_MODE_FEATURE_NAME:Ljava/lang/String;

.field private final MNL_L1_ONLY_FEATURE_NAME:Ljava/lang/String;

.field private final MNL_L1_ONLY_FORMAT:Ljava/lang/String;

.field private TAG:Ljava/lang/String;

.field private bootCompletedReceiver:Landroid/content/BroadcastReceiver;

.field private isSmartSwitchEnableFlag:Z

.field private locationManager:Landroid/location/LocationManager;

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mnlConfigUtils:Lcom/android/server/location/mnlutils/MnlConfigUtils;

.field private mtkGnssDisableL5Obs:Landroid/database/ContentObserver;

.field private mtkGnssSmartSwitchObs:Landroid/database/ContentObserver;

.field private powerSaveReceiver:Landroid/content/BroadcastReceiver;

.field private startControllerListener:Z


# direct methods
.method static bridge synthetic -$$Nest$fgetTAG(Lcom/android/server/location/MtkGnssPowerSaveImpl;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->TAG:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetbootCompletedReceiver(Lcom/android/server/location/MtkGnssPowerSaveImpl;)Landroid/content/BroadcastReceiver;
    .locals 0

    iget-object p0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->bootCompletedReceiver:Landroid/content/BroadcastReceiver;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetisSmartSwitchEnableFlag(Lcom/android/server/location/MtkGnssPowerSaveImpl;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->isSmartSwitchEnableFlag:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/location/MtkGnssPowerSaveImpl;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/server/location/MtkGnssPowerSaveImpl;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputisSmartSwitchEnableFlag(Lcom/android/server/location/MtkGnssPowerSaveImpl;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->isSmartSwitchEnableFlag:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$minitListenerAndRegisterIEmd(Lcom/android/server/location/MtkGnssPowerSaveImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->initListenerAndRegisterIEmd()V

    return-void
.end method

.method static bridge synthetic -$$Nest$ml5Device(Lcom/android/server/location/MtkGnssPowerSaveImpl;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->l5Device()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mupdateDisableL5CloudConfig(Lcom/android/server/location/MtkGnssPowerSaveImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->updateDisableL5CloudConfig()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateDisableSatelliteCloudConfig(Lcom/android/server/location/MtkGnssPowerSaveImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->updateDisableSatelliteCloudConfig()V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const-string v0, "Glp-MtkGnssPowerSaveImpl"

    iput-object v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->TAG:Ljava/lang/String;

    .line 35
    const-string v0, "L1Only"

    iput-object v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->MNL_L1_ONLY_FEATURE_NAME:Ljava/lang/String;

    .line 36
    const-string v0, "L1 only"

    iput-object v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->MNL_L1_ONLY_FORMAT:Ljava/lang/String;

    .line 37
    const-string v0, "GLP"

    iput-object v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->MNL_GLP_FEATURE_NAME:Ljava/lang/String;

    .line 38
    const-string v0, "GnssMode"

    iput-object v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->MNL_GNSS_MODE_FEATURE_NAME:Ljava/lang/String;

    .line 39
    const-string v0, "DGPSMode"

    iput-object v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->MNL_DGPSMode_FEATURE_NAME:Ljava/lang/String;

    .line 76
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->startControllerListener:Z

    .line 78
    iput-boolean v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->isSmartSwitchEnableFlag:Z

    return-void
.end method

.method private initListenerAndRegisterIEmd()V
    .locals 5

    .line 481
    invoke-virtual {p0}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->supportMtkGnssConfig()Z

    move-result v0

    if-nez v0, :cond_0

    .line 482
    iget-object v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->TAG:Ljava/lang/String;

    const-string v1, "not support mtk gnss config feature"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 483
    return-void

    .line 485
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->mtkGnssDisableL5Obs:Landroid/database/ContentObserver;

    const/4 v1, 0x0

    if-nez v0, :cond_1

    .line 486
    new-instance v0, Lcom/android/server/location/MtkGnssPowerSaveImpl$GnssPowerSaveObs;

    iget-object v2, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v2, v1}, Lcom/android/server/location/MtkGnssPowerSaveImpl$GnssPowerSaveObs;-><init>(Lcom/android/server/location/MtkGnssPowerSaveImpl;Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->mtkGnssDisableL5Obs:Landroid/database/ContentObserver;

    .line 488
    :cond_1
    iget-object v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->mtkGnssSmartSwitchObs:Landroid/database/ContentObserver;

    if-nez v0, :cond_2

    .line 489
    new-instance v0, Lcom/android/server/location/MtkGnssPowerSaveImpl$GnssSmartSwitchObs;

    iget-object v2, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v2, v1}, Lcom/android/server/location/MtkGnssPowerSaveImpl$GnssSmartSwitchObs;-><init>(Lcom/android/server/location/MtkGnssPowerSaveImpl;Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->mtkGnssSmartSwitchObs:Landroid/database/ContentObserver;

    .line 493
    :cond_2
    :try_start_0
    iget-object v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v2, "xiaomi_gnss_config_disable_l5"

    .line 494
    invoke-static {v2}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->mtkGnssDisableL5Obs:Landroid/database/ContentObserver;

    .line 493
    const/4 v4, 0x0

    invoke-virtual {v0, v2, v4, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 497
    iget-object v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v2, "xiaomi_gnss_smart_switch"

    .line 498
    invoke-static {v2}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->mtkGnssSmartSwitchObs:Landroid/database/ContentObserver;

    .line 497
    invoke-virtual {v0, v2, v4, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 503
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 504
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v2, "miui.intent.action.POWER_SAVE_MODE_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 505
    iget-object v2, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->powerSaveReceiver:Landroid/content/BroadcastReceiver;

    if-nez v2, :cond_3

    .line 506
    new-instance v2, Lcom/android/server/location/MtkGnssPowerSaveImpl$PowerSaveReceiver;

    invoke-direct {v2, p0, v1}, Lcom/android/server/location/MtkGnssPowerSaveImpl$PowerSaveReceiver;-><init>(Lcom/android/server/location/MtkGnssPowerSaveImpl;Lcom/android/server/location/MtkGnssPowerSaveImpl$PowerSaveReceiver-IA;)V

    iput-object v2, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->powerSaveReceiver:Landroid/content/BroadcastReceiver;

    .line 508
    :cond_3
    iget-object v1, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->powerSaveReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 511
    nop

    .end local v0    # "intentFilter":Landroid/content/IntentFilter;
    goto :goto_0

    .line 509
    :catch_0
    move-exception v0

    .line 510
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->TAG:Ljava/lang/String;

    const-string v2, "init exception:"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 512
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private isCnVersion()Z
    .locals 2

    .line 88
    const-string v0, "ro.miui.region"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "CN"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private isL1OnlyEnable(Lcom/android/server/location/mnlutils/bean/MnlConfig;)Z
    .locals 8
    .param p1, "mnlConfig"    # Lcom/android/server/location/mnlutils/bean/MnlConfig;

    .line 359
    const-string v0, "L1Only"

    invoke-virtual {p1, v0}, Lcom/android/server/location/mnlutils/bean/MnlConfig;->getFeature(Ljava/lang/String;)Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;

    move-result-object v0

    .line 360
    .local v0, "l1OnlyFeature":Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 361
    return v1

    .line 364
    :cond_0
    invoke-virtual {v0}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->getConfig()Ljava/lang/String;

    move-result-object v2

    .line 365
    .local v2, "l1OnlyConfig":Ljava/lang/String;
    const-string v3, ""

    .line 367
    .local v3, "l1OnlyFormatSetting":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->getFormatSettings()Ljava/util/LinkedHashMap;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 368
    .local v5, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    const-string v7, "L1 only"

    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 369
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    move-object v3, v4

    check-cast v3, Ljava/lang/String;

    .line 370
    goto :goto_1

    .line 372
    .end local v5    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_1
    goto :goto_0

    .line 374
    :cond_2
    :goto_1
    const-string v4, "1"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v1, 0x1

    :cond_3
    return v1
.end method

.method private isSmartSwitchEnable(Lcom/android/server/location/mnlutils/bean/MnlConfig;)Z
    .locals 8
    .param p1, "mnlConfig"    # Lcom/android/server/location/mnlutils/bean/MnlConfig;

    .line 378
    const-string v0, "GnssMode"

    invoke-virtual {p1, v0}, Lcom/android/server/location/mnlutils/bean/MnlConfig;->getFeature(Ljava/lang/String;)Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;

    move-result-object v0

    .line 379
    .local v0, "smartSwitchFeature":Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 380
    return v1

    .line 383
    :cond_0
    invoke-virtual {v0}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->getConfig()Ljava/lang/String;

    move-result-object v2

    .line 384
    .local v2, "smartSwitchConfig":Ljava/lang/String;
    const-string v3, ""

    .line 385
    .local v3, "smartFormatSetting":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->getFormatSettings()Ljava/util/LinkedHashMap;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 386
    .local v5, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    const-string v7, "GP+"

    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 387
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    move-object v3, v4

    check-cast v3, Ljava/lang/String;

    .line 388
    goto :goto_1

    .line 390
    .end local v5    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_1
    goto :goto_0

    .line 391
    :cond_2
    :goto_1
    const-string v4, "1"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 392
    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    move v1, v4

    goto :goto_2

    :cond_3
    nop

    :goto_2
    iput-boolean v1, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->isSmartSwitchEnableFlag:Z

    .line 393
    return v1
.end method

.method private l5Device()Z
    .locals 3

    .line 515
    const-string/jumbo v0, "vendor.debug.gps.support.l5"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    const/4 v2, 0x1

    if-ne v2, v0, :cond_0

    move v1, v2

    :cond_0
    return v1
.end method

.method private declared-synchronized registerControlListener()V
    .locals 4

    monitor-enter p0

    .line 92
    :try_start_0
    iget-boolean v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->startControllerListener:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 93
    monitor-exit p0

    return-void

    .line 95
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->mContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 96
    iget-object v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->TAG:Ljava/lang/String;

    const-string v1, "no context"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 97
    monitor-exit p0

    return-void

    .line 99
    .end local p0    # "this":Lcom/android/server/location/MtkGnssPowerSaveImpl;
    :cond_1
    :try_start_2
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 100
    invoke-static {}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataNotifyUri()Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lcom/android/server/location/MtkGnssPowerSaveImpl$1;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/android/server/location/MtkGnssPowerSaveImpl$1;-><init>(Lcom/android/server/location/MtkGnssPowerSaveImpl;Landroid/os/Handler;)V

    .line 99
    const/4 v3, 0x1

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 108
    iget-object v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->TAG:Ljava/lang/String;

    const-string v1, "register cloud controller listener"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    iput-boolean v3, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->startControllerListener:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 110
    monitor-exit p0

    return-void

    .line 91
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private resetWithBackupConfig(Lcom/android/server/location/mnlutils/bean/MnlConfig;Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;)V
    .locals 2
    .param p1, "backupMnlConfig"    # Lcom/android/server/location/mnlutils/bean/MnlConfig;
    .param p2, "smartSwitchFeature"    # Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;

    .line 351
    const-string v0, "GnssMode"

    invoke-virtual {p1, v0}, Lcom/android/server/location/mnlutils/bean/MnlConfig;->getFeature(Ljava/lang/String;)Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;

    move-result-object v0

    .line 352
    .local v0, "backupSmartSwitchFeature":Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
    if-eqz v0, :cond_0

    .line 353
    invoke-virtual {v0}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->getFormatSettings()Ljava/util/LinkedHashMap;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->setFormatSettings(Ljava/util/LinkedHashMap;)V

    .line 354
    invoke-virtual {v0}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->getConfig()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->setConfig(Ljava/lang/String;)V

    .line 356
    :cond_0
    return-void
.end method

.method private resetWithBackupConfig(Lcom/android/server/location/mnlutils/bean/MnlConfig;Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;)V
    .locals 1
    .param p1, "backupMnlConfig"    # Lcom/android/server/location/mnlutils/bean/MnlConfig;
    .param p2, "l1OnlyFeature"    # Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
    .param p3, "glpFeature"    # Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;

    .line 295
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->resetWithBackupConfig(Lcom/android/server/location/mnlutils/bean/MnlConfig;Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;)V

    .line 296
    return-void
.end method

.method private resetWithBackupConfig(Lcom/android/server/location/mnlutils/bean/MnlConfig;Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;)V
    .locals 4
    .param p1, "backupMnlConfig"    # Lcom/android/server/location/mnlutils/bean/MnlConfig;
    .param p2, "l1OnlyFeature"    # Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
    .param p3, "glpFeature"    # Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
    .param p4, "smartSwitchFeature"    # Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;

    .line 301
    const-string v0, "L1Only"

    invoke-virtual {p1, v0}, Lcom/android/server/location/mnlutils/bean/MnlConfig;->getFeature(Ljava/lang/String;)Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;

    move-result-object v0

    .line 302
    .local v0, "backupL1OnlyFeature":Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
    if-eqz v0, :cond_0

    .line 303
    invoke-virtual {v0}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->getFormatSettings()Ljava/util/LinkedHashMap;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->setFormatSettings(Ljava/util/LinkedHashMap;)V

    .line 304
    invoke-virtual {v0}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->getConfig()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->setConfig(Ljava/lang/String;)V

    .line 306
    :cond_0
    const-string v1, "GLP"

    invoke-virtual {p1, v1}, Lcom/android/server/location/mnlutils/bean/MnlConfig;->getFeature(Ljava/lang/String;)Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;

    move-result-object v1

    .line 307
    .local v1, "backupGlpFeature":Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
    if-eqz v1, :cond_1

    .line 308
    invoke-virtual {v1}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->getFormatSettings()Ljava/util/LinkedHashMap;

    move-result-object v2

    invoke-virtual {p3, v2}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->setFormatSettings(Ljava/util/LinkedHashMap;)V

    .line 309
    invoke-virtual {v1}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->getConfig()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->setConfig(Ljava/lang/String;)V

    .line 311
    :cond_1
    if-eqz p4, :cond_2

    .line 312
    const-string v2, "GnssMode"

    invoke-virtual {p1, v2}, Lcom/android/server/location/mnlutils/bean/MnlConfig;->getFeature(Ljava/lang/String;)Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;

    move-result-object v2

    .line 313
    .local v2, "backupSmartSwitch":Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
    if-eqz v2, :cond_2

    .line 314
    invoke-virtual {v2}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->getFormatSettings()Ljava/util/LinkedHashMap;

    move-result-object v3

    invoke-virtual {p4, v3}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->setFormatSettings(Ljava/util/LinkedHashMap;)V

    .line 315
    invoke-virtual {v2}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->getConfig()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p4, v3}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->setConfig(Ljava/lang/String;)V

    .line 318
    .end local v2    # "backupSmartSwitch":Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
    :cond_2
    return-void
.end method

.method private restartGps()V
    .locals 4

    .line 444
    iget-object v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 445
    return-void

    .line 448
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->locationManager:Landroid/location/LocationManager;

    if-nez v1, :cond_1

    .line 449
    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->locationManager:Landroid/location/LocationManager;

    .line 451
    :cond_1
    iget-object v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->locationManager:Landroid/location/LocationManager;

    const-string v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    .line 452
    .local v0, "gpsEnable":Z
    if-nez v0, :cond_2

    .line 453
    iget-object v1, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->TAG:Ljava/lang/String;

    const-string v2, "gnss is close, don\'t need restart"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 454
    return-void

    .line 456
    :cond_2
    iget-object v1, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->TAG:Ljava/lang/String;

    const-string v2, "restart gnss"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    iget-object v1, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->locationManager:Landroid/location/LocationManager;

    .line 458
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    invoke-static {v2}, Landroid/os/UserHandle;->of(I)Landroid/os/UserHandle;

    move-result-object v2

    .line 457
    const/4 v3, 0x0

    invoke-virtual {v1, v3, v2}, Landroid/location/LocationManager;->setLocationEnabledForUser(ZLandroid/os/UserHandle;)V

    .line 459
    iget-object v1, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->locationManager:Landroid/location/LocationManager;

    .line 460
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    invoke-static {v2}, Landroid/os/UserHandle;->of(I)Landroid/os/UserHandle;

    move-result-object v2

    .line 459
    const/4 v3, 0x1

    invoke-virtual {v1, v3, v2}, Landroid/location/LocationManager;->setLocationEnabledForUser(ZLandroid/os/UserHandle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 463
    .end local v0    # "gpsEnable":Z
    goto :goto_0

    .line 461
    :catch_0
    move-exception v0

    .line 462
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "exception in restart gps : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 464
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private sendModeChangedBroadcast()V
    .locals 3

    .line 546
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.location.MODE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 547
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 548
    iget-object v1, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 549
    return-void
.end method

.method private setGlpFeature(Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;Z)V
    .locals 1
    .param p1, "glpFeature"    # Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
    .param p2, "enable"    # Z

    .line 440
    if-eqz p2, :cond_0

    const-string v0, "1"

    goto :goto_0

    :cond_0
    const-string v0, "0"

    :goto_0
    invoke-virtual {p1, v0}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->setConfig(Ljava/lang/String;)V

    .line 441
    return-void
.end method

.method private setL1OnlyFeature(Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;Z)V
    .locals 5
    .param p1, "l1OnlyFeature"    # Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
    .param p2, "enable"    # Z

    .line 397
    if-nez p1, :cond_0

    .line 398
    iget-object v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->TAG:Ljava/lang/String;

    const-string v1, "l1OnlyFeature or glpFeature is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 399
    return-void

    .line 402
    :cond_0
    const-string v0, ""

    .line 404
    .local v0, "l1OnlySettingKey":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->getFormatSettings()Ljava/util/LinkedHashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 405
    .local v2, "key":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    const-string v4, "L1 only"

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 406
    move-object v0, v2

    .line 408
    .end local v2    # "key":Ljava/lang/String;
    :cond_1
    goto :goto_0

    .line 410
    :cond_2
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 411
    return-void

    .line 413
    :cond_3
    const-string v1, "1"

    const-string v2, "0"

    if-eqz p2, :cond_4

    move-object v3, v1

    goto :goto_1

    :cond_4
    move-object v3, v2

    :goto_1
    invoke-virtual {p1, v3}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->setConfig(Ljava/lang/String;)V

    .line 414
    invoke-virtual {p1}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->getFormatSettings()Ljava/util/LinkedHashMap;

    move-result-object v3

    if-eqz p2, :cond_5

    goto :goto_2

    :cond_5
    move-object v1, v2

    :goto_2
    invoke-virtual {v3, v0, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 415
    iget-object v1, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "setL1OnlyFeature done"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 416
    return-void
.end method

.method private setSmartSwitchFeature(Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;ZI)V
    .locals 5
    .param p1, "smartSwitchFeature"    # Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
    .param p2, "enable"    # Z
    .param p3, "gnssModeType"    # I

    .line 419
    if-nez p1, :cond_0

    .line 420
    iget-object v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "smartSwitchFeature is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 421
    return-void

    .line 424
    :cond_0
    const-string v0, ""

    .line 426
    .local v0, "gnssModeSettingKey":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->getFormatSettings()Ljava/util/LinkedHashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 427
    .local v2, "key":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    const-string v4, "GP+"

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 428
    move-object v0, v2

    .line 430
    .end local v2    # "key":Ljava/lang/String;
    :cond_1
    goto :goto_0

    .line 432
    :cond_2
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 433
    return-void

    .line 435
    :cond_3
    if-eqz p2, :cond_4

    const-string v1, "1"

    goto :goto_1

    :cond_4
    const-string v1, "0"

    :goto_1
    invoke-virtual {p1, v1}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->setConfig(Ljava/lang/String;)V

    .line 436
    invoke-virtual {p1}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->getFormatSettings()Ljava/util/LinkedHashMap;

    move-result-object v1

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 437
    return-void
.end method

.method private updateDisableL5CloudConfig()V
    .locals 6

    .line 113
    const/4 v0, 0x0

    const-string v1, "persist.sys.gps.support_disable_l5_config"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 114
    .local v0, "settingsNow":Z
    iget-object v2, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->mContext:Landroid/content/Context;

    .line 115
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 114
    const-string v3, "mtkGnssConfig"

    const-string v4, "disableL5"

    const/4 v5, 0x0

    invoke-static {v2, v3, v4, v5}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 119
    .local v2, "newSettings":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 120
    return-void

    .line 122
    :cond_0
    iget-object v3, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "receive l5 config, new value: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", settingsNow: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    invoke-static {v2}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v3

    .line 125
    .local v3, "newSettingsBool":Z
    if-ne v3, v0, :cond_1

    .line 126
    return-void

    .line 128
    :cond_1
    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    return-void
.end method

.method private updateDisableSatelliteCloudConfig()V
    .locals 6

    .line 132
    const/4 v0, 0x0

    const-string v1, "persist.sys.gps.support_disable_satellite_config"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 133
    .local v0, "settingsNow":Z
    iget-object v2, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->mContext:Landroid/content/Context;

    .line 134
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 133
    const-string v3, "mtkGnssConfig"

    const-string v4, "disableSatellite"

    const/4 v5, 0x0

    invoke-static {v2, v3, v4, v5}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 138
    .local v2, "newSettings":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 139
    return-void

    .line 141
    :cond_0
    iget-object v3, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "receive constellation config, new value: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", settingsNow: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    invoke-static {v2}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v3

    .line 144
    .local v3, "newSettingsBool":Z
    if-ne v3, v0, :cond_1

    .line 145
    return-void

    .line 147
    :cond_1
    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    return-void
.end method


# virtual methods
.method public declared-synchronized activeSmartSwitch(I)V
    .locals 4
    .param p1, "gnssMode"    # I

    monitor-enter p0

    .line 203
    :try_start_0
    iget-object v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->mnlConfigUtils:Lcom/android/server/location/mnlutils/MnlConfigUtils;

    if-nez v0, :cond_0

    .line 204
    new-instance v0, Lcom/android/server/location/mnlutils/MnlConfigUtils;

    invoke-direct {v0}, Lcom/android/server/location/mnlutils/MnlConfigUtils;-><init>()V

    iput-object v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->mnlConfigUtils:Lcom/android/server/location/mnlutils/MnlConfigUtils;

    .line 206
    .end local p0    # "this":Lcom/android/server/location/MtkGnssPowerSaveImpl;
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->mnlConfigUtils:Lcom/android/server/location/mnlutils/MnlConfigUtils;

    invoke-virtual {v0}, Lcom/android/server/location/mnlutils/MnlConfigUtils;->getMnlConfig()Lcom/android/server/location/mnlutils/bean/MnlConfig;

    move-result-object v0

    .line 207
    .local v0, "mnlConfig":Lcom/android/server/location/mnlutils/bean/MnlConfig;
    if-eqz v0, :cond_2

    .line 208
    const-string v1, "GnssMode"

    invoke-virtual {v0, v1}, Lcom/android/server/location/mnlutils/bean/MnlConfig;->getFeature(Ljava/lang/String;)Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;

    move-result-object v1

    .line 209
    .local v1, "SmartSwitchFeature":Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
    const/4 v2, 0x1

    invoke-direct {p0, v1, v2, p1}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->setSmartSwitchFeature(Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;ZI)V

    .line 210
    iget-object v2, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->mnlConfigUtils:Lcom/android/server/location/mnlutils/MnlConfigUtils;

    invoke-virtual {v2, v0}, Lcom/android/server/location/mnlutils/MnlConfigUtils;->saveMnlConfig(Lcom/android/server/location/mnlutils/bean/MnlConfig;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 211
    iget-object v2, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->TAG:Ljava/lang/String;

    const-string v3, "save mnl config fail"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    :cond_1
    invoke-direct {p0}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->restartGps()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 217
    .end local v0    # "mnlConfig":Lcom/android/server/location/mnlutils/bean/MnlConfig;
    .end local v1    # "SmartSwitchFeature":Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
    :cond_2
    goto :goto_0

    .line 202
    .end local p1    # "gnssMode":I
    :catchall_0
    move-exception p1

    goto :goto_1

    .line 215
    .restart local p1    # "gnssMode":I
    :catch_0
    move-exception v0

    .line 216
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    iget-object v1, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->TAG:Ljava/lang/String;

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 218
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    monitor-exit p0

    return-void

    .line 202
    .end local p1    # "gnssMode":I
    :goto_1
    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized disableL5AndDisableGlp()V
    .locals 5

    monitor-enter p0

    .line 234
    :try_start_0
    iget-object v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->mnlConfigUtils:Lcom/android/server/location/mnlutils/MnlConfigUtils;

    if-nez v0, :cond_0

    .line 235
    new-instance v0, Lcom/android/server/location/mnlutils/MnlConfigUtils;

    invoke-direct {v0}, Lcom/android/server/location/mnlutils/MnlConfigUtils;-><init>()V

    iput-object v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->mnlConfigUtils:Lcom/android/server/location/mnlutils/MnlConfigUtils;

    .line 238
    .end local p0    # "this":Lcom/android/server/location/MtkGnssPowerSaveImpl;
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->mnlConfigUtils:Lcom/android/server/location/mnlutils/MnlConfigUtils;

    invoke-virtual {v0}, Lcom/android/server/location/mnlutils/MnlConfigUtils;->getMnlConfig()Lcom/android/server/location/mnlutils/bean/MnlConfig;

    move-result-object v0

    .line 240
    .local v0, "mnlConfig":Lcom/android/server/location/mnlutils/bean/MnlConfig;
    if-eqz v0, :cond_3

    .line 241
    const-string v1, "L1Only"

    invoke-virtual {v0, v1}, Lcom/android/server/location/mnlutils/bean/MnlConfig;->getFeature(Ljava/lang/String;)Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;

    move-result-object v1

    .line 242
    .local v1, "l1OnlyFeature":Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
    const-string v2, "GLP"

    invoke-virtual {v0, v2}, Lcom/android/server/location/mnlutils/bean/MnlConfig;->getFeature(Ljava/lang/String;)Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;

    move-result-object v2

    .line 243
    .local v2, "glpFeature":Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
    if-eqz v1, :cond_2

    if-nez v2, :cond_1

    goto :goto_0

    .line 247
    :cond_1
    const/4 v3, 0x1

    invoke-direct {p0, v1, v3}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->setL1OnlyFeature(Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;Z)V

    .line 248
    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->setGlpFeature(Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;Z)V

    .line 249
    iget-object v3, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->mnlConfigUtils:Lcom/android/server/location/mnlutils/MnlConfigUtils;

    invoke-virtual {v3, v0}, Lcom/android/server/location/mnlutils/MnlConfigUtils;->saveMnlConfig(Lcom/android/server/location/mnlutils/bean/MnlConfig;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 250
    iget-object v3, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->TAG:Ljava/lang/String;

    const-string v4, "save mnl config success"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    invoke-direct {p0}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->restartGps()V

    goto :goto_1

    .line 244
    :cond_2
    :goto_0
    iget-object v3, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->TAG:Ljava/lang/String;

    const-string v4, "l1Onlyfeature or glpFeature is null"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 245
    monitor-exit p0

    return-void

    .line 256
    .end local v0    # "mnlConfig":Lcom/android/server/location/mnlutils/bean/MnlConfig;
    .end local v1    # "l1OnlyFeature":Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
    .end local v2    # "glpFeature":Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
    :cond_3
    :goto_1
    goto :goto_2

    .line 233
    :catchall_0
    move-exception v0

    goto :goto_3

    .line 254
    :catch_0
    move-exception v0

    .line 255
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    iget-object v1, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->TAG:Ljava/lang/String;

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 257
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_2
    monitor-exit p0

    return-void

    .line 233
    :goto_3
    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized enableL5AndEnableGlp()V
    .locals 6

    monitor-enter p0

    .line 261
    :try_start_0
    iget-object v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->mnlConfigUtils:Lcom/android/server/location/mnlutils/MnlConfigUtils;

    if-nez v0, :cond_0

    .line 262
    new-instance v0, Lcom/android/server/location/mnlutils/MnlConfigUtils;

    invoke-direct {v0}, Lcom/android/server/location/mnlutils/MnlConfigUtils;-><init>()V

    iput-object v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->mnlConfigUtils:Lcom/android/server/location/mnlutils/MnlConfigUtils;

    .line 264
    .end local p0    # "this":Lcom/android/server/location/MtkGnssPowerSaveImpl;
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->mnlConfigUtils:Lcom/android/server/location/mnlutils/MnlConfigUtils;

    invoke-virtual {v0}, Lcom/android/server/location/mnlutils/MnlConfigUtils;->getMnlConfig()Lcom/android/server/location/mnlutils/bean/MnlConfig;

    move-result-object v0

    .line 265
    .local v0, "mnlConfig":Lcom/android/server/location/mnlutils/bean/MnlConfig;
    iget-object v1, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->mnlConfigUtils:Lcom/android/server/location/mnlutils/MnlConfigUtils;

    invoke-virtual {v1}, Lcom/android/server/location/mnlutils/MnlConfigUtils;->getBackUpMnlConfig()Lcom/android/server/location/mnlutils/bean/MnlConfig;

    move-result-object v1

    .line 267
    .local v1, "backupMnlConfig":Lcom/android/server/location/mnlutils/bean/MnlConfig;
    if-eqz v0, :cond_4

    .line 268
    const-string v2, "L1Only"

    invoke-virtual {v0, v2}, Lcom/android/server/location/mnlutils/bean/MnlConfig;->getFeature(Ljava/lang/String;)Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;

    move-result-object v2

    .line 269
    .local v2, "l1OnlyFeature":Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
    const-string v3, "GLP"

    invoke-virtual {v0, v3}, Lcom/android/server/location/mnlutils/bean/MnlConfig;->getFeature(Ljava/lang/String;)Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;

    move-result-object v3

    .line 271
    .local v3, "glpFeature":Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
    if-eqz v2, :cond_3

    if-nez v3, :cond_1

    goto :goto_1

    .line 276
    :cond_1
    if-eqz v1, :cond_2

    .line 277
    invoke-direct {p0, v1, v2, v3}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->resetWithBackupConfig(Lcom/android/server/location/mnlutils/bean/MnlConfig;Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;)V

    goto :goto_0

    .line 279
    :cond_2
    const/4 v4, 0x0

    invoke-direct {p0, v2, v4}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->setL1OnlyFeature(Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;Z)V

    .line 280
    const/4 v4, 0x1

    invoke-direct {p0, v3, v4}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->setGlpFeature(Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;Z)V

    .line 282
    :goto_0
    iget-object v4, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->mnlConfigUtils:Lcom/android/server/location/mnlutils/MnlConfigUtils;

    invoke-virtual {v4, v0}, Lcom/android/server/location/mnlutils/MnlConfigUtils;->saveMnlConfig(Lcom/android/server/location/mnlutils/bean/MnlConfig;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 283
    iget-object v4, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->TAG:Ljava/lang/String;

    const-string v5, "save mnl config success"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 284
    invoke-direct {p0}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->restartGps()V

    goto :goto_2

    .line 272
    :cond_3
    :goto_1
    iget-object v4, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->TAG:Ljava/lang/String;

    const-string v5, "l1OnlyFeature, smartSwitchFeature or glpFeature is null"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 273
    monitor-exit p0

    return-void

    .line 289
    .end local v0    # "mnlConfig":Lcom/android/server/location/mnlutils/bean/MnlConfig;
    .end local v1    # "backupMnlConfig":Lcom/android/server/location/mnlutils/bean/MnlConfig;
    .end local v2    # "l1OnlyFeature":Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
    .end local v3    # "glpFeature":Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
    :cond_4
    :goto_2
    goto :goto_3

    .line 260
    :catchall_0
    move-exception v0

    goto :goto_4

    .line 287
    :catch_0
    move-exception v0

    .line 288
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    iget-object v1, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->TAG:Ljava/lang/String;

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 290
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_3
    monitor-exit p0

    return-void

    .line 260
    :goto_4
    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getMtkGnssConfig(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    monitor-enter p0

    .line 165
    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->mnlConfigUtils:Lcom/android/server/location/mnlutils/MnlConfigUtils;

    if-nez v1, :cond_0

    .line 166
    new-instance v1, Lcom/android/server/location/mnlutils/MnlConfigUtils;

    invoke-direct {v1}, Lcom/android/server/location/mnlutils/MnlConfigUtils;-><init>()V

    iput-object v1, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->mnlConfigUtils:Lcom/android/server/location/mnlutils/MnlConfigUtils;

    .line 168
    .end local p0    # "this":Lcom/android/server/location/MtkGnssPowerSaveImpl;
    :cond_0
    iget-object v1, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->mnlConfigUtils:Lcom/android/server/location/mnlutils/MnlConfigUtils;

    invoke-virtual {v1}, Lcom/android/server/location/mnlutils/MnlConfigUtils;->getMnlConfig()Lcom/android/server/location/mnlutils/bean/MnlConfig;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 169
    .local v1, "mnlConfig":Lcom/android/server/location/mnlutils/bean/MnlConfig;
    if-nez v1, :cond_1

    .line 170
    monitor-exit p0

    return-object v0

    .line 173
    .restart local p0    # "this":Lcom/android/server/location/MtkGnssPowerSaveImpl;
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .end local p0    # "this":Lcom/android/server/location/MtkGnssPowerSaveImpl;
    :cond_2
    goto :goto_0

    .restart local p0    # "this":Lcom/android/server/location/MtkGnssPowerSaveImpl;
    :sswitch_0
    const-string/jumbo v2, "smartSwitchEnable"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x2

    goto :goto_1

    .end local p0    # "this":Lcom/android/server/location/MtkGnssPowerSaveImpl;
    :sswitch_1
    const-string v2, "l1OnlyEnable"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    goto :goto_1

    :sswitch_2
    const-string v2, "mnlVersion"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x0

    goto :goto_1

    :cond_3
    :goto_0
    const/4 v2, -0x1

    :goto_1
    packed-switch v2, :pswitch_data_0

    .line 181
    goto :goto_2

    .line 179
    :pswitch_0
    invoke-direct {p0, v1}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->isSmartSwitchEnable(Lcom/android/server/location/mnlutils/bean/MnlConfig;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0

    .line 177
    :pswitch_1
    :try_start_2
    invoke-direct {p0, v1}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->isL1OnlyEnable(Lcom/android/server/location/mnlutils/bean/MnlConfig;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v0

    .line 175
    :pswitch_2
    :try_start_3
    invoke-virtual {v1}, Lcom/android/server/location/mnlutils/bean/MnlConfig;->getVersion()Ljava/lang/String;

    move-result-object v0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    return-object v0

    .line 181
    :goto_2
    monitor-exit p0

    return-object v0

    .line 164
    .end local v1    # "mnlConfig":Lcom/android/server/location/mnlutils/bean/MnlConfig;
    .end local p1    # "name":Ljava/lang/String;
    :catchall_0
    move-exception p1

    goto :goto_3

    .line 183
    .restart local p1    # "name":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 184
    .local v1, "e":Ljava/lang/Exception;
    :try_start_4
    iget-object v2, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->TAG:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 185
    monitor-exit p0

    return-object v0

    .line 164
    .end local v1    # "e":Ljava/lang/Exception;
    .end local p1    # "name":Ljava/lang/String;
    :goto_3
    monitor-exit p0

    throw p1

    nop

    :sswitch_data_0
    .sparse-switch
        0x3d97f6d -> :sswitch_2
        0x26949774 -> :sswitch_1
        0x37501640 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public declared-synchronized recoveryAllSatellite()V
    .locals 5

    monitor-enter p0

    .line 322
    :try_start_0
    iget-object v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->mnlConfigUtils:Lcom/android/server/location/mnlutils/MnlConfigUtils;

    if-nez v0, :cond_0

    .line 323
    new-instance v0, Lcom/android/server/location/mnlutils/MnlConfigUtils;

    invoke-direct {v0}, Lcom/android/server/location/mnlutils/MnlConfigUtils;-><init>()V

    iput-object v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->mnlConfigUtils:Lcom/android/server/location/mnlutils/MnlConfigUtils;

    .line 325
    .end local p0    # "this":Lcom/android/server/location/MtkGnssPowerSaveImpl;
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->mnlConfigUtils:Lcom/android/server/location/mnlutils/MnlConfigUtils;

    invoke-virtual {v0}, Lcom/android/server/location/mnlutils/MnlConfigUtils;->getMnlConfig()Lcom/android/server/location/mnlutils/bean/MnlConfig;

    move-result-object v0

    .line 326
    .local v0, "mnlConfig":Lcom/android/server/location/mnlutils/bean/MnlConfig;
    iget-object v1, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->mnlConfigUtils:Lcom/android/server/location/mnlutils/MnlConfigUtils;

    invoke-virtual {v1}, Lcom/android/server/location/mnlutils/MnlConfigUtils;->getBackUpMnlConfig()Lcom/android/server/location/mnlutils/bean/MnlConfig;

    move-result-object v1

    .line 328
    .local v1, "backupMnlConfig":Lcom/android/server/location/mnlutils/bean/MnlConfig;
    if-eqz v0, :cond_3

    .line 329
    const-string v2, "GnssMode"

    invoke-virtual {v0, v2}, Lcom/android/server/location/mnlutils/bean/MnlConfig;->getFeature(Ljava/lang/String;)Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;

    move-result-object v2

    .line 330
    .local v2, "smartSwitchFeature":Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
    if-nez v2, :cond_1

    .line 331
    iget-object v3, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "smartSwitchFeature or glpFeature is null"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 332
    monitor-exit p0

    return-void

    .line 334
    :cond_1
    if-eqz v1, :cond_2

    .line 335
    :try_start_1
    invoke-direct {p0, v1, v2}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->resetWithBackupConfig(Lcom/android/server/location/mnlutils/bean/MnlConfig;Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;)V

    goto :goto_0

    .line 337
    :cond_2
    const/4 v3, 0x0

    const/4 v4, 0x6

    invoke-direct {p0, v2, v3, v4}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->setSmartSwitchFeature(Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;ZI)V

    .line 339
    :goto_0
    iget-object v3, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->mnlConfigUtils:Lcom/android/server/location/mnlutils/MnlConfigUtils;

    invoke-virtual {v3, v0}, Lcom/android/server/location/mnlutils/MnlConfigUtils;->saveMnlConfig(Lcom/android/server/location/mnlutils/bean/MnlConfig;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 340
    iget-object v3, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->TAG:Ljava/lang/String;

    const-string v4, "recovery mnl config success"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 341
    invoke-direct {p0}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->restartGps()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 346
    .end local v0    # "mnlConfig":Lcom/android/server/location/mnlutils/bean/MnlConfig;
    .end local v1    # "backupMnlConfig":Lcom/android/server/location/mnlutils/bean/MnlConfig;
    .end local v2    # "smartSwitchFeature":Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
    :cond_3
    goto :goto_1

    .line 321
    :catchall_0
    move-exception v0

    goto :goto_2

    .line 344
    :catch_0
    move-exception v0

    .line 345
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    iget-object v1, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->TAG:Ljava/lang/String;

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 347
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    monitor-exit p0

    return-void

    .line 321
    :goto_2
    monitor-exit p0

    throw v0
.end method

.method public startPowerSaveListener(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 152
    iput-object p1, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->mContext:Landroid/content/Context;

    .line 153
    new-instance v0, Lcom/android/server/location/MtkGnssPowerSaveImpl$SmartSatelliteHandler;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/server/location/MtkGnssPowerSaveImpl$SmartSatelliteHandler;-><init>(Lcom/android/server/location/MtkGnssPowerSaveImpl;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->mHandler:Landroid/os/Handler;

    .line 155
    iget-object v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->bootCompletedReceiver:Landroid/content/BroadcastReceiver;

    if-nez v0, :cond_0

    .line 156
    new-instance v0, Lcom/android/server/location/MtkGnssPowerSaveImpl$BootCompletedReceiver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/server/location/MtkGnssPowerSaveImpl$BootCompletedReceiver;-><init>(Lcom/android/server/location/MtkGnssPowerSaveImpl;Lcom/android/server/location/MtkGnssPowerSaveImpl$BootCompletedReceiver-IA;)V

    iput-object v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->bootCompletedReceiver:Landroid/content/BroadcastReceiver;

    .line 157
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 158
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 159
    iget-object v1, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl;->bootCompletedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 161
    .end local v0    # "intentFilter":Landroid/content/IntentFilter;
    :cond_0
    return-void
.end method

.method public supportMtkGnssConfig()Z
    .locals 2

    .line 83
    invoke-direct {p0}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->registerControlListener()V

    .line 84
    invoke-direct {p0}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->isCnVersion()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const-string v0, "persist.sys.gps.support_gnss_config"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method
