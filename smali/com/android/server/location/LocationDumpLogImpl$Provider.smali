.class public final Lcom/android/server/location/LocationDumpLogImpl$Provider;
.super Ljava/lang/Object;
.source "LocationDumpLogImpl$Provider.java"

# interfaces
.implements Lcom/miui/base/MiuiStubRegistry$ImplProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/location/LocationDumpLogImpl$Provider$SINGLETON;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/miui/base/MiuiStubRegistry$ImplProvider<",
        "Lcom/android/server/location/LocationDumpLogImpl;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public provideNewInstance()Lcom/android/server/location/LocationDumpLogImpl;
    .locals 1

    .line 17
    new-instance v0, Lcom/android/server/location/LocationDumpLogImpl;

    invoke-direct {v0}, Lcom/android/server/location/LocationDumpLogImpl;-><init>()V

    return-object v0
.end method

.method public bridge synthetic provideNewInstance()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/android/server/location/LocationDumpLogImpl$Provider;->provideNewInstance()Lcom/android/server/location/LocationDumpLogImpl;

    move-result-object v0

    return-object v0
.end method

.method public provideSingleton()Lcom/android/server/location/LocationDumpLogImpl;
    .locals 1

    .line 13
    sget-object v0, Lcom/android/server/location/LocationDumpLogImpl$Provider$SINGLETON;->INSTANCE:Lcom/android/server/location/LocationDumpLogImpl;

    return-object v0
.end method

.method public bridge synthetic provideSingleton()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/android/server/location/LocationDumpLogImpl$Provider;->provideSingleton()Lcom/android/server/location/LocationDumpLogImpl;

    move-result-object v0

    return-object v0
.end method
