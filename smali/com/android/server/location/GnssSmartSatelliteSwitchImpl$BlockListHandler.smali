.class Lcom/android/server/location/GnssSmartSatelliteSwitchImpl$BlockListHandler;
.super Landroid/os/Handler;
.source "GnssSmartSatelliteSwitchImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BlockListHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;


# direct methods
.method public constructor <init>(Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 341
    iput-object p1, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl$BlockListHandler;->this$0:Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;

    .line 342
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 343
    return-void
.end method

.method public constructor <init>(Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;Landroid/os/Looper;Landroid/os/Handler$Callback;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;
    .param p3, "callback"    # Landroid/os/Handler$Callback;

    .line 345
    iput-object p1, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl$BlockListHandler;->this$0:Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;

    .line 346
    invoke-direct {p0, p2, p3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    .line 347
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .line 351
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 352
    if-nez p1, :cond_0

    .line 353
    return-void

    .line 354
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 359
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl$BlockListHandler;->this$0:Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;

    const-string v1, "3,0,4,0,6,0,7,0"

    invoke-static {v0, v1}, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->-$$Nest$mupdateBlockList(Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;Ljava/lang/String;)V

    .line 360
    goto :goto_0

    .line 356
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl$BlockListHandler;->this$0:Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->-$$Nest$mupdateBlockList(Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;Ljava/lang/String;)V

    .line 357
    nop

    .line 364
    :goto_0
    iget-object v0, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl$BlockListHandler;->this$0:Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;

    invoke-static {v0}, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->-$$Nest$fgetBDS_COLLECTOR(Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 365
    iget-object v0, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl$BlockListHandler;->this$0:Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;

    invoke-static {v0}, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->-$$Nest$fgetGPS_COLLECTOR(Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 366
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
