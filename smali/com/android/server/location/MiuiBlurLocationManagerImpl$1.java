class com.android.server.location.MiuiBlurLocationManagerImpl$1 extends android.os.Handler {
	 /* .source "MiuiBlurLocationManagerImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/location/MiuiBlurLocationManagerImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.location.MiuiBlurLocationManagerImpl this$0; //synthetic
/* # direct methods */
 com.android.server.location.MiuiBlurLocationManagerImpl$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/location/MiuiBlurLocationManagerImpl; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 86 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 14 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 89 */
(( android.os.Message ) p1 ).getData ( ); // invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;
/* .line 90 */
/* .local v0, "bundle":Landroid/os/Bundle; */
/* if-nez v0, :cond_0 */
/* .line 91 */
final String v1 = "MiuiBlurLocationManager"; // const-string v1, "MiuiBlurLocationManager"
final String v2 = "msg from client is null"; // const-string v2, "msg from client is null"
android.util.Log .e ( v1,v2 );
/* .line 92 */
return;
/* .line 94 */
} // :cond_0
/* iget v1, p1, Landroid/os/Message;->what:I */
int v2 = 0; // const/4 v2, 0x0
/* packed-switch v1, :pswitch_data_0 */
/* .line 104 */
/* :pswitch_0 */
v1 = this.this$0;
/* new-instance v11, Lmiui/security/SvStatusData; */
/* .line 105 */
final String v3 = "key_svcount"; // const-string v3, "key_svcount"
v4 = (( android.os.Bundle ) v0 ).getInt ( v3, v2 ); // invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I
/* .line 106 */
final String v2 = "key_svidWithFlags"; // const-string v2, "key_svidWithFlags"
(( android.os.Bundle ) v0 ).getIntArray ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I
/* .line 107 */
final String v2 = "key_cn0s"; // const-string v2, "key_cn0s"
(( android.os.Bundle ) v0 ).getFloatArray ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Bundle;->getFloatArray(Ljava/lang/String;)[F
/* .line 108 */
final String v2 = "key_svElevations"; // const-string v2, "key_svElevations"
(( android.os.Bundle ) v0 ).getFloatArray ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Bundle;->getFloatArray(Ljava/lang/String;)[F
/* .line 109 */
final String v2 = "key_svAzimuths"; // const-string v2, "key_svAzimuths"
(( android.os.Bundle ) v0 ).getFloatArray ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Bundle;->getFloatArray(Ljava/lang/String;)[F
/* .line 110 */
final String v2 = "key_svCarrierFreqs"; // const-string v2, "key_svCarrierFreqs"
(( android.os.Bundle ) v0 ).getFloatArray ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Bundle;->getFloatArray(Ljava/lang/String;)[F
/* .line 111 */
final String v2 = "key_basebandCn0s"; // const-string v2, "key_basebandCn0s"
(( android.os.Bundle ) v0 ).getFloatArray ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Bundle;->getFloatArray(Ljava/lang/String;)[F
/* move-object v3, v11 */
/* invoke-direct/range {v3 ..v10}, Lmiui/security/SvStatusData;-><init>(I[I[F[F[F[F[F)V */
com.android.server.location.MiuiBlurLocationManagerImpl .-$$Nest$fputmSvStatusData ( v1,v11 );
/* .line 113 */
/* .line 96 */
/* :pswitch_1 */
final String v1 = "key_lac"; // const-string v1, "key_lac"
v1 = (( android.os.Bundle ) v0 ).getInt ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I
/* .line 97 */
/* .local v1, "lac":I */
final String v3 = "key_cid"; // const-string v3, "key_cid"
v2 = (( android.os.Bundle ) v0 ).getInt ( v3, v2 ); // invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I
/* .line 98 */
/* .local v2, "cid":I */
final String v3 = "key_longitude"; // const-string v3, "key_longitude"
/* const-wide/16 v4, 0x0 */
(( android.os.Bundle ) v0 ).getDouble ( v3, v4, v5 ); // invoke-virtual {v0, v3, v4, v5}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;D)D
/* move-result-wide v10 */
/* .line 99 */
/* .local v10, "longitude":D */
final String v3 = "key_latitude"; // const-string v3, "key_latitude"
(( android.os.Bundle ) v0 ).getDouble ( v3, v4, v5 ); // invoke-virtual {v0, v3, v4, v5}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;D)D
/* move-result-wide v12 */
/* .line 100 */
/* .local v12, "latitude":D */
v3 = this.this$0;
com.android.server.location.MiuiBlurLocationManagerImpl .-$$Nest$fgetmStashBlurLocationInfo ( v3 );
/* move v4, v1 */
/* move v5, v2 */
/* move-wide v6, v10 */
/* move-wide v8, v12 */
/* invoke-virtual/range {v3 ..v9}, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->set(IIDD)V */
/* .line 102 */
/* nop */
/* .line 117 */
} // .end local v1 # "lac":I
} // .end local v2 # "cid":I
} // .end local v10 # "longitude":D
} // .end local v12 # "latitude":D
} // :goto_0
return;
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
