class com.android.server.location.MtkGnssPowerSaveImpl$GnssSmartSwitchObs extends android.database.ContentObserver {
	 /* .source "MtkGnssPowerSaveImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/location/MtkGnssPowerSaveImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "GnssSmartSwitchObs" */
} // .end annotation
/* # instance fields */
private android.content.Context mContext;
final com.android.server.location.MtkGnssPowerSaveImpl this$0; //synthetic
/* # direct methods */
public com.android.server.location.MtkGnssPowerSaveImpl$GnssSmartSwitchObs ( ) {
/* .locals 0 */
/* .param p2, "mContext" # Landroid/content/Context; */
/* .param p3, "handler" # Landroid/os/Handler; */
/* .line 581 */
this.this$0 = p1;
/* .line 582 */
/* invoke-direct {p0, p3}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
/* .line 583 */
this.mContext = p2;
/* .line 584 */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0 ) {
/* .locals 5 */
/* .param p1, "selfChange" # Z */
/* .line 588 */
/* invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V */
/* .line 589 */
final String v0 = "persist.sys.gps.support_disable_satellite_config"; // const-string v0, "persist.sys.gps.support_disable_satellite_config"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
/* if-nez v0, :cond_0 */
/* .line 590 */
return;
/* .line 592 */
} // :cond_0
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v2, "xiaomi_gnss_smart_switch" */
int v3 = 2; // const/4 v3, 0x2
v0 = android.provider.Settings$Secure .getInt ( v0,v2,v3 );
/* .line 595 */
/* .local v0, "state":I */
v2 = this.this$0;
com.android.server.location.MtkGnssPowerSaveImpl .-$$Nest$fgetTAG ( v2 );
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "xiaomi_mtk_gnss_smart_switch_feature had set to " */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v3 );
/* .line 596 */
int v2 = 1; // const/4 v2, 0x1
/* if-ne v0, v2, :cond_1 */
/* .line 598 */
v1 = this.this$0;
(( com.android.server.location.MtkGnssPowerSaveImpl ) v1 ).activeSmartSwitch ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->activeSmartSwitch(I)V
/* .line 599 */
v1 = this.this$0;
com.android.server.location.MtkGnssPowerSaveImpl .-$$Nest$fgetTAG ( v1 );
/* const-string/jumbo v3, "start smart switch" */
android.util.Log .d ( v1,v3 );
/* .line 600 */
v1 = this.this$0;
com.android.server.location.MtkGnssPowerSaveImpl .-$$Nest$fputisSmartSwitchEnableFlag ( v1,v2 );
/* .line 602 */
} // :cond_1
v2 = this.this$0;
v2 = com.android.server.location.MtkGnssPowerSaveImpl .-$$Nest$fgetisSmartSwitchEnableFlag ( v2 );
/* if-nez v2, :cond_2 */
/* .line 603 */
v1 = this.this$0;
com.android.server.location.MtkGnssPowerSaveImpl .-$$Nest$fgetTAG ( v1 );
/* const-string/jumbo v2, "smartSatellite is not actived,no need to modify" */
android.util.Log .d ( v1,v2 );
/* .line 604 */
return;
/* .line 606 */
} // :cond_2
v2 = this.this$0;
(( com.android.server.location.MtkGnssPowerSaveImpl ) v2 ).recoveryAllSatellite ( ); // invoke-virtual {v2}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->recoveryAllSatellite()V
/* .line 607 */
v2 = this.this$0;
com.android.server.location.MtkGnssPowerSaveImpl .-$$Nest$fputisSmartSwitchEnableFlag ( v2,v1 );
/* .line 608 */
v1 = this.this$0;
com.android.server.location.MtkGnssPowerSaveImpl .-$$Nest$fgetTAG ( v1 );
/* const-string/jumbo v2, "start recovery all satellite system" */
android.util.Log .d ( v1,v2 );
/* .line 610 */
} // :goto_0
return;
} // .end method
