.class Lcom/android/server/location/MiuiBlurLocationManagerImpl$2;
.super Landroid/os/Handler;
.source "MiuiBlurLocationManagerImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/location/MiuiBlurLocationManagerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/location/MiuiBlurLocationManagerImpl;


# direct methods
.method constructor <init>(Lcom/android/server/location/MiuiBlurLocationManagerImpl;Landroid/os/Looper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/location/MiuiBlurLocationManagerImpl;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 471
    iput-object p1, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl$2;->this$0:Lcom/android/server/location/MiuiBlurLocationManagerImpl;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .line 474
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v0, v0, Landroid/location/util/identity/CallerIdentity;

    if-eqz v0, :cond_0

    .line 476
    iget-object v0, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl$2;->this$0:Lcom/android/server/location/MiuiBlurLocationManagerImpl;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/location/util/identity/CallerIdentity;

    const-string v2, "gps"

    invoke-virtual {v0, v2, v1}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->handleGpsLocationChangedLocked(Ljava/lang/String;Landroid/location/util/identity/CallerIdentity;)V

    .line 478
    :cond_0
    return-void
.end method
