public class com.android.server.location.EnCode {
	 /* .source "EnCode.java" */
	 /* # static fields */
	 private static mkeyRandom;
	 private static java.lang.String moutPutString;
	 /* # direct methods */
	 static com.android.server.location.EnCode ( ) {
		 /* .locals 1 */
		 /* .line 20 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* .line 21 */
		 return;
	 } // .end method
	 private com.android.server.location.EnCode ( ) {
		 /* .locals 0 */
		 /* .line 19 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static final encBytes ( Object[] p0, Object[] p1, Object[] p2 ) {
		 /* .locals 4 */
		 /* .param p0, "srcBytes" # [B */
		 /* .param p1, "key" # [B */
		 /* .param p2, "newIv" # [B */
		 /* .annotation system Ldalvik/annotation/Throws; */
		 /* value = { */
		 /* Ljava/lang/Exception; */
		 /* } */
	 } // .end annotation
	 /* .line 52 */
	 final String v0 = "AES/CBC/PKCS5Padding"; // const-string v0, "AES/CBC/PKCS5Padding"
	 javax.crypto.Cipher .getInstance ( v0 );
	 /* .line 53 */
	 /* .local v0, "cipher":Ljavax/crypto/Cipher; */
	 /* new-instance v1, Ljavax/crypto/spec/SecretKeySpec; */
	 final String v2 = "AES"; // const-string v2, "AES"
	 /* invoke-direct {v1, p1, v2}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V */
	 /* .line 54 */
	 /* .local v1, "skeySpec":Ljavax/crypto/spec/SecretKeySpec; */
	 /* new-instance v2, Ljavax/crypto/spec/IvParameterSpec; */
	 /* invoke-direct {v2, p2}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V */
	 /* .line 55 */
	 /* .local v2, "iv":Ljavax/crypto/spec/IvParameterSpec; */
	 int v3 = 1; // const/4 v3, 0x1
	 (( javax.crypto.Cipher ) v0 ).init ( v3, v1, v2 ); // invoke-virtual {v0, v3, v1, v2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V
	 /* .line 56 */
	 (( javax.crypto.Cipher ) v0 ).doFinal ( p0 ); // invoke-virtual {v0, p0}, Ljavax/crypto/Cipher;->doFinal([B)[B
	 /* .line 57 */
	 /* .local v3, "encrypted":[B */
} // .end method
public static final java.lang.String encText ( java.lang.String p0 ) {
	 /* .locals 8 */
	 /* .param p0, "sSrc" # Ljava/lang/String; */
	 /* .annotation system Ldalvik/annotation/Throws; */
	 /* value = { */
	 /* Ljava/lang/Exception; */
	 /* } */
} // .end annotation
/* .line 62 */
final String v0 = ""; // const-string v0, ""
int v1 = 0; // const/4 v1, 0x0
/* .line 63 */
/* .local v1, "outPut":Ljava/lang/String; */
int v2 = 0; // const/4 v2, 0x0
/* .line 64 */
/* .local v2, "outPutTxt":Ljava/lang/String; */
/* const/16 v3, 0x10 */
/* new-array v3, v3, [B */
/* fill-array-data v3, :array_0 */
/* .line 66 */
/* .local v3, "ivk":[B */
try { // :try_start_0
	 final String v4 = "UTF-8"; // const-string v4, "UTF-8"
	 (( java.lang.String ) p0 ).getBytes ( v4 ); // invoke-virtual {p0, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
	 /* .line 67 */
	 /* .local v4, "srcBytes":[B */
	 v5 = com.android.server.location.EnCode.mkeyRandom;
	 /* if-nez v5, :cond_0 */
	 /* .line 68 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* .line 70 */
} // :cond_0
com.android.server.location.EnCode .encBytes ( v4,v5,v3 );
/* .line 71 */
/* .local v5, "encryptedTxt":[B */
int v6 = 0; // const/4 v6, 0x0
android.util.Base64 .encodeToString ( v5,v6 );
/* move-object v2, v6 */
/* .line 72 */
final String v6 = "\n"; // const-string v6, "\n"
(( java.lang.String ) v2 ).replace ( v6, v0 ); // invoke-virtual {v2, v6, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
final String v7 = "\r"; // const-string v7, "\r"
(( java.lang.String ) v6 ).replace ( v7, v0 ); // invoke-virtual {v6, v7, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
/* :try_end_0 */
/* .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 77 */
} // .end local v2 # "outPutTxt":Ljava/lang/String;
} // .end local v4 # "srcBytes":[B
} // .end local v5 # "encryptedTxt":[B
/* .local v0, "outPutTxt":Ljava/lang/String; */
/* .line 75 */
} // .end local v0 # "outPutTxt":Ljava/lang/String;
/* .restart local v2 # "outPutTxt":Ljava/lang/String; */
/* :catch_0 */
/* move-exception v0 */
/* .line 76 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* move-object v0, v2 */
/* .line 73 */
} // .end local v0 # "e":Ljava/lang/Exception;
/* :catch_1 */
/* move-exception v0 */
/* .line 74 */
/* .local v0, "e":Ljava/io/UnsupportedEncodingException; */
(( java.io.UnsupportedEncodingException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V
/* .line 77 */
} // .end local v0 # "e":Ljava/io/UnsupportedEncodingException;
/* move-object v0, v2 */
/* .line 79 */
} // .end local v2 # "outPutTxt":Ljava/lang/String;
/* .local v0, "outPutTxt":Ljava/lang/String; */
} // :goto_0
/* :array_0 */
/* .array-data 1 */
/* 0x30t */
/* 0x30t */
/* 0x30t */
/* 0x30t */
/* 0x30t */
/* 0x30t */
/* 0x30t */
/* 0x30t */
/* 0x30t */
/* 0x30t */
/* 0x30t */
/* 0x30t */
/* 0x30t */
/* 0x30t */
/* 0x30t */
/* 0x30t */
} // .end array-data
} // .end method
public static final java.lang.String initRandom ( ) {
/* .locals 8 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 24 */
final String v0 = ""; // const-string v0, ""
v1 = com.android.server.location.EnCode.mkeyRandom;
/* if-nez v1, :cond_1 */
/* .line 25 */
int v1 = 0; // const/4 v1, 0x0
/* .line 26 */
/* .local v1, "outPutRandom":Ljava/lang/String; */
/* new-instance v2, Ljava/util/Random; */
/* invoke-direct {v2}, Ljava/util/Random;-><init>()V */
/* .line 27 */
/* .local v2, "ranDom":Ljava/util/Random; */
/* const/16 v3, 0x10 */
/* new-array v4, v3, [B */
/* .line 29 */
/* .local v4, "keyRandom":[B */
int v5 = 0; // const/4 v5, 0x0
/* .local v5, "i":I */
} // :goto_0
/* if-ge v5, v3, :cond_0 */
/* .line 30 */
/* const/16 v6, 0x14 */
v6 = (( java.util.Random ) v2 ).nextInt ( v6 ); // invoke-virtual {v2, v6}, Ljava/util/Random;->nextInt(I)I
/* .line 31 */
/* .local v6, "number":I */
/* add-int/lit8 v7, v6, 0x61 */
/* int-to-byte v7, v7 */
/* aput-byte v7, v4, v5 */
/* .line 29 */
} // .end local v6 # "number":I
/* add-int/lit8 v5, v5, 0x1 */
/* .line 34 */
} // .end local v5 # "i":I
} // :cond_0
/* .line 37 */
try { // :try_start_0
com.android.server.location.RsaUtil .encryptByPublicKey ( v4 );
/* .line 38 */
/* .local v3, "encryptedRandom":[B */
int v5 = 0; // const/4 v5, 0x0
android.util.Base64 .encodeToString ( v3,v5 );
/* move-object v1, v5 */
/* .line 39 */
final String v5 = "\n"; // const-string v5, "\n"
(( java.lang.String ) v1 ).replace ( v5, v0 ); // invoke-virtual {v1, v5, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
final String v6 = "\r"; // const-string v6, "\r"
(( java.lang.String ) v5 ).replace ( v6, v0 ); // invoke-virtual {v5, v6, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
/* move-object v1, v0 */
/* .line 40 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 43 */
} // .end local v3 # "encryptedRandom":[B
/* .line 41 */
/* :catch_0 */
/* move-exception v0 */
/* .line 42 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 44 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // .end local v1 # "outPutRandom":Ljava/lang/String;
} // .end local v2 # "ranDom":Ljava/util/Random;
} // .end local v4 # "keyRandom":[B
} // :goto_1
/* .line 45 */
} // :cond_1
/* const-string/jumbo v0, "test" */
final String v1 = "mkeyRandom init done here\n"; // const-string v1, "mkeyRandom init done here\n"
android.util.Log .e ( v0,v1 );
/* .line 47 */
} // :goto_2
v0 = com.android.server.location.EnCode.moutPutString;
} // .end method
