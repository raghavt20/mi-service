public class com.android.server.location.LocationOpHandler extends android.os.Handler {
	 /* .source "LocationOpHandler.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/location/LocationOpHandler$LocationOpRecord; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer MSG_DELAYED_LOCATION_OP;
private static final java.lang.String TAG;
/* # instance fields */
private final android.content.Context mContext;
private android.util.SparseArray mLastLocationOps;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Lcom/android/server/location/LocationOpHandler$LocationOpRecord;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.lang.Object mLock;
private final android.net.wifi.WifiManager mWifiManager;
/* # direct methods */
public com.android.server.location.LocationOpHandler ( ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 28 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 24 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
this.mLock = v0;
/* .line 25 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
this.mLastLocationOps = v0;
/* .line 29 */
this.mContext = p1;
/* .line 30 */
/* const-string/jumbo v0, "wifi" */
(( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/net/wifi/WifiManager; */
this.mWifiManager = v0;
/* .line 31 */
return;
} // .end method
private void postWifiScanRequest ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .line 45 */
v0 = this.mWifiManager;
/* new-instance v1, Landroid/os/WorkSource; */
/* invoke-direct {v1, p1}, Landroid/os/WorkSource;-><init>(I)V */
(( android.net.wifi.WifiManager ) v0 ).startScan ( v1 ); // invoke-virtual {v0, v1}, Landroid/net/wifi/WifiManager;->startScan(Landroid/os/WorkSource;)Z
/* .line 46 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 2 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 35 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* packed-switch v0, :pswitch_data_0 */
/* .line 37 */
/* :pswitch_0 */
/* iget v0, p1, Landroid/os/Message;->arg1:I */
int v1 = 2; // const/4 v1, 0x2
/* if-ne v0, v1, :cond_0 */
/* .line 38 */
/* iget v0, p1, Landroid/os/Message;->arg2:I */
/* invoke-direct {p0, v0}, Lcom/android/server/location/LocationOpHandler;->postWifiScanRequest(I)V */
/* .line 42 */
} // :cond_0
} // :goto_0
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public Boolean isFrequenctlyOp ( Integer p0, Integer p1, Long p2, Integer p3 ) {
/* .locals 8 */
/* .param p1, "uid" # I */
/* .param p2, "op" # I */
/* .param p3, "optime" # J */
/* .param p5, "minInterval" # I */
/* .line 50 */
int v0 = 2; // const/4 v0, 0x2
/* if-eq p2, v0, :cond_0 */
int v1 = 3; // const/4 v1, 0x3
/* if-eq p2, v1, :cond_0 */
/* .line 51 */
int v0 = 0; // const/4 v0, 0x0
/* .line 54 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 55 */
/* .local v1, "isFrequenctlyOp":Z */
v2 = this.mLock;
/* monitor-enter v2 */
/* .line 56 */
try { // :try_start_0
v3 = this.mLastLocationOps;
int v4 = 0; // const/4 v4, 0x0
(( android.util.SparseArray ) v3 ).get ( p2, v4 ); // invoke-virtual {v3, p2, v4}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;
/* check-cast v3, Lcom/android/server/location/LocationOpHandler$LocationOpRecord; */
/* .line 58 */
/* .local v3, "lastOp":Lcom/android/server/location/LocationOpHandler$LocationOpRecord; */
if ( v3 != null) { // if-eqz v3, :cond_1
/* if-ne p2, v0, :cond_1 */
/* .line 59 */
/* iget-wide v4, v3, Lcom/android/server/location/LocationOpHandler$LocationOpRecord;->timestamp:J */
/* cmp-long v0, p3, v4 */
/* if-lez v0, :cond_1 */
/* iget-wide v4, v3, Lcom/android/server/location/LocationOpHandler$LocationOpRecord;->timestamp:J */
/* int-to-long v6, p5 */
/* add-long/2addr v4, v6 */
/* cmp-long v0, p3, v4 */
/* if-gez v0, :cond_1 */
/* .line 60 */
int v0 = 1; // const/4 v0, 0x1
/* move v1, v0 */
/* .line 63 */
} // .end local v3 # "lastOp":Lcom/android/server/location/LocationOpHandler$LocationOpRecord;
} // :cond_1
/* monitor-exit v2 */
/* .line 64 */
/* .line 63 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v0 */
} // .end method
public void setFollowupAction ( Integer p0, Integer p1, Long p2, Integer p3 ) {
/* .locals 6 */
/* .param p1, "uid" # I */
/* .param p2, "op" # I */
/* .param p3, "optime" # J */
/* .param p5, "minInterval" # I */
/* .line 68 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 69 */
int v1 = 2; // const/4 v1, 0x2
/* if-ne p2, v1, :cond_1 */
/* .line 70 */
/* int-to-long v1, p5 */
/* .line 71 */
/* .local v1, "delay":J */
try { // :try_start_0
v3 = this.mLastLocationOps;
int v4 = 0; // const/4 v4, 0x0
(( android.util.SparseArray ) v3 ).get ( p2, v4 ); // invoke-virtual {v3, p2, v4}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;
/* check-cast v3, Lcom/android/server/location/LocationOpHandler$LocationOpRecord; */
/* .line 72 */
/* .local v3, "lastOp":Lcom/android/server/location/LocationOpHandler$LocationOpRecord; */
if ( v3 != null) { // if-eqz v3, :cond_0
/* iget-wide v4, v3, Lcom/android/server/location/LocationOpHandler$LocationOpRecord;->timestamp:J */
/* cmp-long v4, p3, v4 */
/* if-lez v4, :cond_0 */
/* .line 73 */
/* iget-wide v4, v3, Lcom/android/server/location/LocationOpHandler$LocationOpRecord;->timestamp:J */
/* sub-long v4, p3, v4 */
/* sub-long/2addr v1, v4 */
/* .line 75 */
} // :cond_0
int v4 = 1; // const/4 v4, 0x1
android.os.Message .obtain ( p0,v4,p2,p1 );
/* .line 76 */
/* .local v4, "msg":Landroid/os/Message; */
(( com.android.server.location.LocationOpHandler ) p0 ).sendMessageDelayed ( v4, v1, v2 ); // invoke-virtual {p0, v4, v1, v2}, Lcom/android/server/location/LocationOpHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 78 */
} // .end local v1 # "delay":J
} // .end local v3 # "lastOp":Lcom/android/server/location/LocationOpHandler$LocationOpRecord;
} // .end local v4 # "msg":Landroid/os/Message;
} // :cond_1
/* monitor-exit v0 */
/* .line 79 */
return;
/* .line 78 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void updateLastLocationOp ( Integer p0, Integer p1, Long p2 ) {
/* .locals 9 */
/* .param p1, "uid" # I */
/* .param p2, "op" # I */
/* .param p3, "optime" # J */
/* .line 83 */
int v0 = 2; // const/4 v0, 0x2
/* if-eq p2, v0, :cond_0 */
int v1 = 3; // const/4 v1, 0x3
/* if-eq p2, v1, :cond_0 */
/* .line 84 */
return;
/* .line 86 */
} // :cond_0
v1 = this.mLock;
/* monitor-enter v1 */
/* .line 87 */
try { // :try_start_0
/* new-instance v8, Lcom/android/server/location/LocationOpHandler$LocationOpRecord; */
/* move-object v2, v8 */
/* move-object v3, p0 */
/* move v4, p1 */
/* move v5, p2 */
/* move-wide v6, p3 */
/* invoke-direct/range {v2 ..v7}, Lcom/android/server/location/LocationOpHandler$LocationOpRecord;-><init>(Lcom/android/server/location/LocationOpHandler;IIJ)V */
/* move-object v2, v8 */
/* .line 88 */
/* .local v2, "opRecord":Lcom/android/server/location/LocationOpHandler$LocationOpRecord; */
v3 = this.mLastLocationOps;
(( android.util.SparseArray ) v3 ).put ( p2, v2 ); // invoke-virtual {v3, p2, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 89 */
/* if-ne p2, v0, :cond_1 */
/* .line 90 */
int v0 = 1; // const/4 v0, 0x1
(( com.android.server.location.LocationOpHandler ) p0 ).removeMessages ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/LocationOpHandler;->removeMessages(I)V
/* .line 92 */
} // .end local v2 # "opRecord":Lcom/android/server/location/LocationOpHandler$LocationOpRecord;
} // :cond_1
/* monitor-exit v1 */
/* .line 93 */
return;
/* .line 92 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v0 */
} // .end method
