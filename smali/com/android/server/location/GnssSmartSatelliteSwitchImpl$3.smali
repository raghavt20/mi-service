.class Lcom/android/server/location/GnssSmartSatelliteSwitchImpl$3;
.super Ljava/util/TimerTask;
.source "GnssSmartSatelliteSwitchImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->initOnce()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;


# direct methods
.method constructor <init>(Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;

    .line 236
    iput-object p1, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl$3;->this$0:Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 239
    iget-object v0, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl$3;->this$0:Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;

    invoke-static {v0}, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->-$$Nest$fgetmHandler(Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 240
    .local v0, "message":Landroid/os/Message;
    const/4 v1, 0x0

    iput v1, v0, Landroid/os/Message;->what:I

    .line 241
    iget-object v2, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl$3;->this$0:Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;

    invoke-static {v2}, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->-$$Nest$fgetGPS_COLLECTOR(Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->-$$Nest$mgetAvgOfTopFourCN0(Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;Ljava/util/Map;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl$3;->this$0:Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;

    invoke-static {v2}, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->-$$Nest$fgetBDS_COLLECTOR(Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->-$$Nest$mgetAvgOfTopFourCN0(Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;Ljava/util/Map;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 242
    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->what:I

    .line 243
    iget-object v1, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl$3;->this$0:Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;

    invoke-static {v1}, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->-$$Nest$fgetmHandler(Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 244
    return-void

    .line 246
    :cond_0
    iput v1, v0, Landroid/os/Message;->what:I

    .line 247
    iget-object v1, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl$3;->this$0:Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;

    invoke-static {v1}, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->-$$Nest$fgetmHandler(Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 249
    return-void
.end method
