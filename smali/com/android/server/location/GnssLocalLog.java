public class com.android.server.location.GnssLocalLog {
	 /* .source "GnssLocalLog.java" */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private java.lang.String mGlpEn;
	 private java.util.LinkedList mLog;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/LinkedList<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private Integer mMaxLines;
private java.lang.String mNmea;
private Long mNow;
private java.lang.String mRandomInit;
private java.lang.String mRandomString;
/* # direct methods */
public com.android.server.location.GnssLocalLog ( ) {
/* .locals 1 */
/* .param p1, "maxLines" # I */
/* .line 23 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 14 */
final String v0 = "=MI GLP EN="; // const-string v0, "=MI GLP EN="
this.mGlpEn = v0;
/* .line 15 */
final String v0 = "=MI NMEA="; // const-string v0, "=MI NMEA="
this.mNmea = v0;
/* .line 16 */
final String v0 = "=MI Random="; // const-string v0, "=MI Random="
this.mRandomInit = v0;
/* .line 17 */
int v0 = 0; // const/4 v0, 0x0
this.mRandomString = v0;
/* .line 24 */
/* new-instance v0, Ljava/util/LinkedList; */
/* invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V */
this.mLog = v0;
/* .line 25 */
/* iput p1, p0, Lcom/android/server/location/GnssLocalLog;->mMaxLines:I */
/* .line 27 */
try { // :try_start_0
	 com.android.server.location.EnCode .initRandom ( );
	 this.mRandomString = v0;
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 31 */
	 /* .line 29 */
	 /* :catch_0 */
	 /* move-exception v0 */
	 /* .line 30 */
	 /* .local v0, "e":Ljava/lang/Exception; */
	 (( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
	 /* .line 32 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private java.lang.String getRawString ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "rawString" # Ljava/lang/String; */
/* .param p2, "keyWord" # Ljava/lang/String; */
/* .line 81 */
v0 = (( java.lang.String ) p1 ).indexOf ( p2 ); // invoke-virtual {p1, p2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
v1 = (( java.lang.String ) p2 ).length ( ); // invoke-virtual {p2}, Ljava/lang/String;->length()I
/* add-int/2addr v0, v1 */
(( java.lang.String ) p1 ).substring ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;
} // .end method
private java.lang.String getTime ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "raw" # Ljava/lang/String; */
/* .param p2, "keyWord" # Ljava/lang/String; */
/* .line 85 */
v0 = (( java.lang.String ) p1 ).indexOf ( p2 ); // invoke-virtual {p1, p2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
/* .line 86 */
/* .local v0, "index":I */
int v1 = -1; // const/4 v1, -0x1
/* if-ne v0, v1, :cond_0 */
/* .line 87 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "no keyWord "; // const-string v2, "no keyWord "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " here\n"; // const-string v2, " here\n"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "GnssLocalLog"; // const-string v2, "GnssLocalLog"
android.util.Log .e ( v2,v1 );
/* .line 88 */
final String v1 = "get time error "; // const-string v1, "get time error "
/* .line 90 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
(( java.lang.String ) p1 ).substring ( v1, v0 ); // invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;
} // .end method
private Boolean isPresence ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "max" # Ljava/lang/String; */
/* .param p2, "min" # Ljava/lang/String; */
/* .line 77 */
v0 = (( java.lang.String ) p1 ).contains ( p2 ); // invoke-virtual {p1, p2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
} // .end method
/* # virtual methods */
public synchronized void clearData ( ) {
/* .locals 1 */
/* monitor-enter p0 */
/* .line 41 */
try { // :try_start_0
v0 = this.mLog;
(( java.util.LinkedList ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 42 */
/* monitor-exit p0 */
return;
/* .line 40 */
} // .end local p0 # "this":Lcom/android/server/location/GnssLocalLog;
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* throw v0 */
} // .end method
public synchronized void dump ( java.io.PrintWriter p0 ) {
/* .locals 5 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* monitor-enter p0 */
/* .line 57 */
try { // :try_start_0
v0 = this.mLog;
int v1 = 0; // const/4 v1, 0x0
(( java.util.LinkedList ) v0 ).listIterator ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/LinkedList;->listIterator(I)Ljava/util/ListIterator;
/* .line 58 */
/* .local v0, "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;" */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
v2 = this.mRandomInit;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mRandomString;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 60 */
} // :goto_0
v1 = try { // :try_start_1
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 61 */
/* check-cast v1, Ljava/lang/String; */
/* .line 62 */
/* .local v1, "log":Ljava/lang/String; */
v2 = this.mGlpEn;
v2 = /* invoke-direct {p0, v1, v2}, Lcom/android/server/location/GnssLocalLog;->isPresence(Ljava/lang/String;Ljava/lang/String;)Z */
if ( v2 != null) { // if-eqz v2, :cond_0
	 /* .line 64 */
	 /* new-instance v2, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
	 v3 = this.mGlpEn;
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* new-instance v3, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
	 v4 = this.mGlpEn;
	 /* invoke-direct {p0, v1, v4}, Lcom/android/server/location/GnssLocalLog;->getTime(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; */
	 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v4 = this.mGlpEn;
	 /* invoke-direct {p0, v1, v4}, Lcom/android/server/location/GnssLocalLog;->getRawString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; */
	 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 com.android.server.location.EnCode .encText ( v3 );
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 (( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
	 /* .line 65 */
} // .end local p0 # "this":Lcom/android/server/location/GnssLocalLog;
} // :cond_0
v2 = this.mNmea;
v2 = /* invoke-direct {p0, v1, v2}, Lcom/android/server/location/GnssLocalLog;->isPresence(Ljava/lang/String;Ljava/lang/String;)Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 67 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
v3 = this.mNmea;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
v4 = this.mNmea;
/* invoke-direct {p0, v1, v4}, Lcom/android/server/location/GnssLocalLog;->getTime(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.mNmea;
/* invoke-direct {p0, v1, v4}, Lcom/android/server/location/GnssLocalLog;->getRawString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.location.EnCode .encText ( v3 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 69 */
} // :cond_1
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 70 */
} // .end local v1 # "log":Ljava/lang/String;
} // :goto_1
/* goto/16 :goto_0 */
/* .line 73 */
} // :cond_2
/* .line 71 */
/* :catch_0 */
/* move-exception v1 */
/* .line 72 */
/* .local v1, "e":Ljava/lang/Exception; */
try { // :try_start_2
final String v2 = "GnssLocalLog"; // const-string v2, "GnssLocalLog"
final String v3 = "Error encountered on encrypt the log."; // const-string v3, "Error encountered on encrypt the log."
android.util.Log .e ( v2,v3,v1 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 74 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_2
/* monitor-exit p0 */
return;
/* .line 56 */
} // .end local v0 # "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
} // .end local p1 # "pw":Ljava/io/PrintWriter;
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
public synchronized void log ( java.lang.String p0 ) {
/* .locals 9 */
/* .param p1, "msg" # Ljava/lang/String; */
/* monitor-enter p0 */
/* .line 45 */
try { // :try_start_0
/* iget v0, p0, Lcom/android/server/location/GnssLocalLog;->mMaxLines:I */
/* if-lez v0, :cond_0 */
/* .line 46 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/location/GnssLocalLog;->mNow:J */
/* .line 47 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 48 */
/* .local v0, "sb":Ljava/lang/StringBuilder; */
java.util.Calendar .getInstance ( );
/* .line 49 */
/* .local v1, "c":Ljava/util/Calendar; */
/* iget-wide v2, p0, Lcom/android/server/location/GnssLocalLog;->mNow:J */
(( java.util.Calendar ) v1 ).setTimeInMillis ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V
/* .line 50 */
final String v8 = "%tm-%td %tH:%tM:%tS.%tL"; // const-string v8, "%tm-%td %tH:%tM:%tS.%tL"
/* move-object v2, v1 */
/* move-object v3, v1 */
/* move-object v4, v1 */
/* move-object v5, v1 */
/* move-object v6, v1 */
/* move-object v7, v1 */
/* filled-new-array/range {v2 ..v7}, [Ljava/lang/Object; */
java.lang.String .format ( v8,v2 );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 51 */
v2 = this.mLog;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " - "; // const-string v4, " - "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.util.LinkedList ) v2 ).add ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
/* .line 52 */
} // :goto_0
v2 = this.mLog;
v2 = (( java.util.LinkedList ) v2 ).size ( ); // invoke-virtual {v2}, Ljava/util/LinkedList;->size()I
/* iget v3, p0, Lcom/android/server/location/GnssLocalLog;->mMaxLines:I */
/* if-le v2, v3, :cond_0 */
v2 = this.mLog;
(( java.util.LinkedList ) v2 ).remove ( ); // invoke-virtual {v2}, Ljava/util/LinkedList;->remove()Ljava/lang/Object;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 54 */
} // .end local v0 # "sb":Ljava/lang/StringBuilder;
} // .end local v1 # "c":Ljava/util/Calendar;
} // .end local p0 # "this":Lcom/android/server/location/GnssLocalLog;
} // :cond_0
/* monitor-exit p0 */
return;
/* .line 44 */
} // .end local p1 # "msg":Ljava/lang/String;
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
public synchronized Integer setLength ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "length" # I */
/* monitor-enter p0 */
/* .line 35 */
/* if-lez p1, :cond_0 */
/* .line 36 */
try { // :try_start_0
/* iput p1, p0, Lcom/android/server/location/GnssLocalLog;->mMaxLines:I */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 34 */
} // .end local p0 # "this":Lcom/android/server/location/GnssLocalLog;
} // .end local p1 # "length":I
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
/* .line 37 */
/* .restart local p0 # "this":Lcom/android/server/location/GnssLocalLog; */
/* .restart local p1 # "length":I */
} // :cond_0
} // :goto_0
/* monitor-exit p0 */
} // .end method
