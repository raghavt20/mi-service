class com.android.server.location.mnlutils.MnlConfigParseUtils {
	 /* .source "MnlConfigParseUtils.java" */
	 /* # static fields */
	 private static final java.lang.String TAG_CONFIG;
	 private static final java.lang.String TAG_FEATURE;
	 private static final java.lang.String TAG_FORMAT;
	 private static final java.lang.String TAG_MNL_CONFIG;
	 private static final java.lang.String TAG_SETTING;
	 private static final java.lang.String TAG_VERSION;
	 private static com.android.server.location.mnlutils.bean.MnlConfigFeature featureTemp;
	 private static java.lang.String nowFeatureAtt;
	 private static java.lang.String nowFormat;
	 /* # direct methods */
	 private com.android.server.location.mnlutils.MnlConfigParseUtils ( ) {
		 /* .locals 0 */
		 /* .line 30 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 31 */
		 return;
	 } // .end method
	 private static void handleEndTag ( org.xmlpull.v1.XmlPullParser p0, com.android.server.location.mnlutils.bean.MnlConfig p1 ) {
		 /* .locals 2 */
		 /* .param p0, "parser" # Lorg/xmlpull/v1/XmlPullParser; */
		 /* .param p1, "mnlConfig" # Lcom/android/server/location/mnlutils/bean/MnlConfig; */
		 /* .line 130 */
		 final String v0 = "feature"; // const-string v0, "feature"
		 v0 = 		 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 132 */
			 (( com.android.server.location.mnlutils.bean.MnlConfig ) p1 ).getFeatureList ( ); // invoke-virtual {p1}, Lcom/android/server/location/mnlutils/bean/MnlConfig;->getFeatureList()Ljava/util/ArrayList;
			 v1 = com.android.server.location.mnlutils.MnlConfigParseUtils.featureTemp;
			 (( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
			 /* .line 134 */
			 int v0 = 0; // const/4 v0, 0x0
			 /* .line 136 */
		 } // :cond_0
		 return;
	 } // .end method
	 private static void handleStartTag ( org.xmlpull.v1.XmlPullParser p0, com.android.server.location.mnlutils.bean.MnlConfig p1 ) {
		 /* .locals 3 */
		 /* .param p0, "parser" # Lorg/xmlpull/v1/XmlPullParser; */
		 /* .param p1, "mnlConfig" # Lcom/android/server/location/mnlutils/bean/MnlConfig; */
		 /* .line 73 */
		 /* if-nez p1, :cond_0 */
		 /* .line 74 */
		 /* new-instance v0, Lcom/android/server/location/mnlutils/bean/MnlConfig; */
		 /* invoke-direct {v0}, Lcom/android/server/location/mnlutils/bean/MnlConfig;-><init>()V */
		 /* move-object p1, v0 */
		 /* .line 77 */
	 } // :cond_0
	 final String v0 = "mnl_config"; // const-string v0, "mnl_config"
	 v0 = 	 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 /* const-string/jumbo v1, "version" */
	 if ( v0 != null) { // if-eqz v0, :cond_3
		 /* .line 79 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* .local v0, "i":I */
	 v2 = 	 } // :goto_0
	 /* if-ge v0, v2, :cond_2 */
	 /* .line 80 */
	 v2 = 	 (( java.lang.String ) v2 ).equals ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v2 != null) { // if-eqz v2, :cond_1
		 /* .line 81 */
		 (( com.android.server.location.mnlutils.bean.MnlConfig ) p1 ).setVersion ( v2 ); // invoke-virtual {p1, v2}, Lcom/android/server/location/mnlutils/bean/MnlConfig;->setVersion(Ljava/lang/String;)V
		 /* .line 79 */
	 } // :cond_1
	 /* add-int/lit8 v0, v0, 0x1 */
} // .end local v0 # "i":I
} // :cond_2
/* .line 84 */
} // :cond_3
final String v2 = "feature"; // const-string v2, "feature"
v0 = (( java.lang.String ) v2 ).equals ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 85 */
/* new-instance v0, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature; */
/* invoke-direct {v0}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;-><init>()V */
/* .line 86 */
/* .line 87 */
} // :cond_4
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 89 */
/* .line 90 */
} // :cond_5
final String v1 = "config"; // const-string v1, "config"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 91 */
/* .line 92 */
} // :cond_6
final String v1 = "format"; // const-string v1, "format"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_7
/* .line 93 */
/* .line 94 */
} // :cond_7
/* const-string/jumbo v1, "setting" */
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_8
/* .line 95 */
/* .line 97 */
} // :cond_8
} // :goto_1
return;
} // .end method
private static void handleText ( org.xmlpull.v1.XmlPullParser p0 ) {
/* .locals 4 */
/* .param p0, "parser" # Lorg/xmlpull/v1/XmlPullParser; */
/* .line 100 */
v0 = com.android.server.location.mnlutils.MnlConfigParseUtils.nowFeatureAtt;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 101 */
v1 = (( java.lang.String ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/String;->hashCode()I
/* sparse-switch v1, :sswitch_data_0 */
} // :cond_0
/* :sswitch_0 */
/* const-string/jumbo v1, "setting" */
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 4; // const/4 v0, 0x4
/* :sswitch_1 */
/* const-string/jumbo v1, "version" */
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
/* :sswitch_2 */
final String v1 = "feature"; // const-string v1, "feature"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 0; // const/4 v0, 0x0
/* :sswitch_3 */
final String v1 = "format"; // const-string v1, "format"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
	 int v0 = 3; // const/4 v0, 0x3
	 /* :sswitch_4 */
	 final String v1 = "config"; // const-string v1, "config"
	 v0 = 	 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 int v0 = 2; // const/4 v0, 0x2
	 } // :goto_0
	 int v0 = -1; // const/4 v0, -0x1
} // :goto_1
int v1 = 0; // const/4 v1, 0x0
/* packed-switch v0, :pswitch_data_0 */
/* .line 120 */
/* :pswitch_0 */
v0 = com.android.server.location.mnlutils.MnlConfigParseUtils.featureTemp;
(( com.android.server.location.mnlutils.bean.MnlConfigFeature ) v0 ).getFormatSettings ( ); // invoke-virtual {v0}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->getFormatSettings()Ljava/util/LinkedHashMap;
v2 = com.android.server.location.mnlutils.MnlConfigParseUtils.nowFormat;
com.android.server.location.mnlutils.MnlConfigParseUtils .trimString ( v3 );
(( java.util.LinkedHashMap ) v0 ).put ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 121 */
/* .line 122 */
/* .line 116 */
/* :pswitch_1 */
com.android.server.location.mnlutils.MnlConfigParseUtils .trimString ( v0 );
/* .line 117 */
/* .line 118 */
/* .line 112 */
/* :pswitch_2 */
v0 = com.android.server.location.mnlutils.MnlConfigParseUtils.featureTemp;
com.android.server.location.mnlutils.MnlConfigParseUtils .trimString ( v2 );
(( com.android.server.location.mnlutils.bean.MnlConfigFeature ) v0 ).setConfig ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->setConfig(Ljava/lang/String;)V
/* .line 113 */
/* .line 114 */
/* .line 108 */
/* :pswitch_3 */
v0 = com.android.server.location.mnlutils.MnlConfigParseUtils.featureTemp;
com.android.server.location.mnlutils.MnlConfigParseUtils .trimString ( v2 );
(( com.android.server.location.mnlutils.bean.MnlConfigFeature ) v0 ).setVersion ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->setVersion(Ljava/lang/String;)V
/* .line 109 */
/* .line 110 */
/* .line 103 */
/* :pswitch_4 */
v0 = com.android.server.location.mnlutils.MnlConfigParseUtils.featureTemp;
com.android.server.location.mnlutils.MnlConfigParseUtils .trimString ( v2 );
(( com.android.server.location.mnlutils.bean.MnlConfigFeature ) v0 ).setName ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->setName(Ljava/lang/String;)V
/* .line 104 */
/* .line 105 */
/* nop */
/* .line 127 */
} // :cond_1
} // :goto_2
return;
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x50c07cbe -> :sswitch_4 */
/* -0x4ba00809 -> :sswitch_3 */
/* -0x3a5d850a -> :sswitch_2 */
/* 0x14f51cd8 -> :sswitch_1 */
/* 0x765f0e50 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public static synchronized com.android.server.location.mnlutils.bean.MnlConfig parseXml ( java.lang.String p0 ) {
/* .locals 8 */
/* .param p0, "xml" # Ljava/lang/String; */
/* const-class v0, Lcom/android/server/location/mnlutils/MnlConfigParseUtils; */
/* monitor-enter v0 */
/* .line 35 */
try { // :try_start_0
/* new-instance v1, Lcom/android/server/location/mnlutils/bean/MnlConfig; */
/* invoke-direct {v1}, Lcom/android/server/location/mnlutils/bean/MnlConfig;-><init>()V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 37 */
/* .local v1, "mnlConfigNow":Lcom/android/server/location/mnlutils/bean/MnlConfig; */
int v2 = 0; // const/4 v2, 0x0
/* if-nez p0, :cond_0 */
/* .line 38 */
/* monitor-exit v0 */
/* .line 41 */
} // :cond_0
try { // :try_start_1
/* new-instance v3, Ljava/io/ByteArrayInputStream; */
(( java.lang.String ) p0 ).getBytes ( ); // invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B
/* invoke-direct {v3, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 43 */
/* .local v3, "inputStream":Ljava/io/InputStream; */
try { // :try_start_2
org.xmlpull.v1.XmlPullParserFactory .newInstance ( );
/* .line 44 */
/* .local v4, "factory":Lorg/xmlpull/v1/XmlPullParserFactory; */
(( org.xmlpull.v1.XmlPullParserFactory ) v4 ).newPullParser ( ); // invoke-virtual {v4}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;
/* .line 45 */
/* .local v5, "parser":Lorg/xmlpull/v1/XmlPullParser; */
final String v6 = "UTF-8"; // const-string v6, "UTF-8"
v6 = /* .line 46 */
/* .line 47 */
/* .local v6, "eventType":I */
} // :goto_0
int v7 = 1; // const/4 v7, 0x1
/* if-eq v6, v7, :cond_1 */
/* .line 48 */
/* packed-switch v6, :pswitch_data_0 */
/* .line 53 */
/* :pswitch_0 */
com.android.server.location.mnlutils.MnlConfigParseUtils .handleText ( v5 );
/* .line 54 */
/* .line 56 */
/* :pswitch_1 */
com.android.server.location.mnlutils.MnlConfigParseUtils .handleEndTag ( v5,v1 );
/* .line 57 */
/* .line 50 */
/* :pswitch_2 */
com.android.server.location.mnlutils.MnlConfigParseUtils .handleStartTag ( v5,v1 );
/* .line 51 */
/* nop */
/* .line 61 */
v7 = } // :goto_1
/* move v6, v7 */
/* .line 63 */
} // :cond_1
(( java.io.InputStream ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/InputStream;->close()V
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 64 */
/* monitor-exit v0 */
/* .line 65 */
} // .end local v4 # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
} // .end local v5 # "parser":Lorg/xmlpull/v1/XmlPullParser;
} // .end local v6 # "eventType":I
/* :catch_0 */
/* move-exception v4 */
/* .line 66 */
/* .local v4, "e":Ljava/lang/Exception; */
try { // :try_start_3
(( java.lang.Exception ) v4 ).printStackTrace ( ); // invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 68 */
} // .end local v4 # "e":Ljava/lang/Exception;
/* monitor-exit v0 */
/* .line 34 */
} // .end local v1 # "mnlConfigNow":Lcom/android/server/location/mnlutils/bean/MnlConfig;
} // .end local v3 # "inputStream":Ljava/io/InputStream;
} // .end local p0 # "xml":Ljava/lang/String;
/* :catchall_0 */
/* move-exception p0 */
/* monitor-exit v0 */
/* throw p0 */
/* :pswitch_data_0 */
/* .packed-switch 0x2 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private static java.lang.String trimString ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p0, "text" # Ljava/lang/String; */
/* .line 139 */
final String v0 = "\n"; // const-string v0, "\n"
final String v1 = ""; // const-string v1, ""
(( java.lang.String ) p0 ).replaceAll ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
(( java.lang.String ) v0 ).trim ( ); // invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;
} // .end method
