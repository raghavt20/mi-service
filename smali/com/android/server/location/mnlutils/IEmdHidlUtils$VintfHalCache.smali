.class Lcom/android/server/location/mnlutils/IEmdHidlUtils$VintfHalCache;
.super Ljava/lang/Object;
.source "IEmdHidlUtils.java"

# interfaces
.implements Ljava/util/function/Supplier;
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/location/mnlutils/IEmdHidlUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "VintfHalCache"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/function/Supplier<",
        "Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds;",
        ">;",
        "Landroid/os/IBinder$DeathRecipient;"
    }
.end annotation


# instance fields
.field private mInstance:Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds;


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/location/mnlutils/IEmdHidlUtils$VintfHalCache;->mInstance:Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds;

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/location/mnlutils/IEmdHidlUtils$VintfHalCache-IA;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/location/mnlutils/IEmdHidlUtils$VintfHalCache;-><init>()V

    return-void
.end method


# virtual methods
.method public declared-synchronized binderDied()V
    .locals 1

    monitor-enter p0

    .line 68
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/android/server/location/mnlutils/IEmdHidlUtils$VintfHalCache;->mInstance:Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 69
    monitor-exit p0

    return-void

    .line 67
    .end local p0    # "this":Lcom/android/server/location/mnlutils/IEmdHidlUtils$VintfHalCache;
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized get()Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds;
    .locals 4

    monitor-enter p0

    .line 50
    :try_start_0
    iget-object v0, p0, Lcom/android/server/location/mnlutils/IEmdHidlUtils$VintfHalCache;->mInstance:Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 52
    :try_start_1
    const-string/jumbo v0, "vendor.mediatek.hardware.engineermode.IEmds/default"

    .line 53
    invoke-static {v0}, Landroid/os/ServiceManager;->waitForDeclaredService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 52
    invoke-static {v0}, Landroid/os/Binder;->allowBlocking(Landroid/os/IBinder;)Landroid/os/IBinder;

    move-result-object v0

    .line 55
    .local v0, "binder":Landroid/os/IBinder;
    if-eqz v0, :cond_0

    .line 56
    invoke-static {v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/location/mnlutils/IEmdHidlUtils$VintfHalCache;->mInstance:Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds;

    .line 57
    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 62
    .end local v0    # "binder":Landroid/os/IBinder;
    .end local p0    # "this":Lcom/android/server/location/mnlutils/IEmdHidlUtils$VintfHalCache;
    :cond_0
    goto :goto_0

    .line 59
    :catch_0
    move-exception v0

    .line 60
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    :try_start_2
    iput-object v1, p0, Lcom/android/server/location/mnlutils/IEmdHidlUtils$VintfHalCache;->mInstance:Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds;

    .line 61
    const-string v1, "Glp-IEmdHidlUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to get EM AIDL Service: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/android/server/location/mnlutils/IEmdHidlUtils$VintfHalCache;->mInstance:Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v0

    .line 49
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 46
    invoke-virtual {p0}, Lcom/android/server/location/mnlutils/IEmdHidlUtils$VintfHalCache;->get()Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds;

    move-result-object v0

    return-object v0
.end method
