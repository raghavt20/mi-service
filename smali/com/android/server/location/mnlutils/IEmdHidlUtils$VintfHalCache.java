class com.android.server.location.mnlutils.IEmdHidlUtils$VintfHalCache implements java.util.function.Supplier implements android.os.IBinder$DeathRecipient {
	 /* .source "IEmdHidlUtils.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/location/mnlutils/IEmdHidlUtils; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "VintfHalCache" */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/Object;", */
/* "Ljava/util/function/Supplier<", */
/* "Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds;", */
/* ">;", */
/* "Landroid/os/IBinder$DeathRecipient;" */
/* } */
} // .end annotation
/* # instance fields */
private com.android.server.location.hardware.mtk.engineermode.aidl.IEmds mInstance;
/* # direct methods */
private com.android.server.location.mnlutils.IEmdHidlUtils$VintfHalCache ( ) {
/* .locals 1 */
/* .line 46 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 47 */
int v0 = 0; // const/4 v0, 0x0
this.mInstance = v0;
return;
} // .end method
 com.android.server.location.mnlutils.IEmdHidlUtils$VintfHalCache ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/location/mnlutils/IEmdHidlUtils$VintfHalCache;-><init>()V */
return;
} // .end method
/* # virtual methods */
public synchronized void binderDied ( ) {
/* .locals 1 */
/* monitor-enter p0 */
/* .line 68 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
this.mInstance = v0;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 69 */
/* monitor-exit p0 */
return;
/* .line 67 */
} // .end local p0 # "this":Lcom/android/server/location/mnlutils/IEmdHidlUtils$VintfHalCache;
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* throw v0 */
} // .end method
public synchronized com.android.server.location.hardware.mtk.engineermode.aidl.IEmds get ( ) {
/* .locals 4 */
/* monitor-enter p0 */
/* .line 50 */
try { // :try_start_0
v0 = this.mInstance;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* if-nez v0, :cond_1 */
/* .line 52 */
try { // :try_start_1
	 /* const-string/jumbo v0, "vendor.mediatek.hardware.engineermode.IEmds/default" */
	 /* .line 53 */
	 android.os.ServiceManager .waitForDeclaredService ( v0 );
	 /* .line 52 */
	 android.os.Binder .allowBlocking ( v0 );
	 /* .line 55 */
	 /* .local v0, "binder":Landroid/os/IBinder; */
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 56 */
		 com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub .asInterface ( v0 );
		 this.mInstance = v1;
		 /* .line 57 */
		 int v1 = 0; // const/4 v1, 0x0
		 /* :try_end_1 */
		 /* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_0 */
		 /* .catch Ljava/lang/RuntimeException; {:try_start_1 ..:try_end_1} :catch_0 */
		 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
		 /* .line 62 */
	 } // .end local v0 # "binder":Landroid/os/IBinder;
} // .end local p0 # "this":Lcom/android/server/location/mnlutils/IEmdHidlUtils$VintfHalCache;
} // :cond_0
/* .line 59 */
/* :catch_0 */
/* move-exception v0 */
/* .line 60 */
/* .local v0, "e":Ljava/lang/Exception; */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_2
this.mInstance = v1;
/* .line 61 */
final String v1 = "Glp-IEmdHidlUtils"; // const-string v1, "Glp-IEmdHidlUtils"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Unable to get EM AIDL Service: "; // const-string v3, "Unable to get EM AIDL Service: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v2 );
/* .line 64 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_1
} // :goto_0
v0 = this.mInstance;
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* monitor-exit p0 */
/* .line 49 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* throw v0 */
} // .end method
public java.lang.Object get ( ) { //bridge//synthethic
/* .locals 1 */
/* .line 46 */
(( com.android.server.location.mnlutils.IEmdHidlUtils$VintfHalCache ) p0 ).get ( ); // invoke-virtual {p0}, Lcom/android/server/location/mnlutils/IEmdHidlUtils$VintfHalCache;->get()Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds;
} // .end method
