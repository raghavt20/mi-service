public class com.android.server.location.mnlutils.bean.MnlConfigFeature {
	 /* .source "MnlConfigFeature.java" */
	 /* # instance fields */
	 private java.lang.String config;
	 private java.util.LinkedHashMap formatSettings;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/LinkedHashMap<", */
	 /* "Ljava/lang/String;", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private java.lang.String name;
private java.lang.String version;
/* # direct methods */
public com.android.server.location.mnlutils.bean.MnlConfigFeature ( ) {
/* .locals 0 */
/* .line 25 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public java.lang.String getConfig ( ) {
/* .locals 1 */
/* .line 52 */
v0 = this.config;
} // .end method
public java.util.LinkedHashMap getFormatSettings ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/LinkedHashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 60 */
v0 = this.formatSettings;
/* if-nez v0, :cond_0 */
/* .line 61 */
/* new-instance v0, Ljava/util/LinkedHashMap; */
/* invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V */
this.formatSettings = v0;
/* .line 63 */
} // :cond_0
v0 = this.formatSettings;
} // .end method
public java.lang.String getName ( ) {
/* .locals 1 */
/* .line 36 */
v0 = this.name;
} // .end method
public java.lang.String getVersion ( ) {
/* .locals 1 */
/* .line 44 */
v0 = this.version;
} // .end method
public void setConfig ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "config" # Ljava/lang/String; */
/* .line 56 */
this.config = p1;
/* .line 57 */
return;
} // .end method
public void setFormatSettings ( java.util.LinkedHashMap p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/LinkedHashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 67 */
/* .local p1, "formatSettings":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Ljava/lang/String;Ljava/lang/String;>;" */
this.formatSettings = p1;
/* .line 68 */
return;
} // .end method
public void setName ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "name" # Ljava/lang/String; */
/* .line 40 */
this.name = p1;
/* .line 41 */
return;
} // .end method
public void setVersion ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "version" # Ljava/lang/String; */
/* .line 48 */
this.version = p1;
/* .line 49 */
return;
} // .end method
public java.lang.String toXmlString ( ) {
/* .locals 5 */
/* .line 71 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "\n <feature>"; // const-string v2, "\n <feature>"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.name;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "\n"; // const-string v2, "\n"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V */
/* .line 72 */
/* .local v0, "buffer":Ljava/lang/StringBuilder; */
final String v1 = " <version>"; // const-string v1, " <version>"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.version;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "</version>\n"; // const-string v2, "</version>\n"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 73 */
final String v1 = " <config>"; // const-string v1, " <config>"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.config;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "</config>\n"; // const-string v2, "</config>\n"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 74 */
v1 = this.formatSettings;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 75 */
(( java.util.LinkedHashMap ) v1 ).entrySet ( ); // invoke-virtual {v1}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Ljava/util/Map$Entry; */
/* .line 76 */
/* .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;" */
final String v3 = " <format>"; // const-string v3, " <format>"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* check-cast v4, Ljava/lang/String; */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = "</format>\n"; // const-string v4, "</format>\n"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 77 */
final String v3 = " <setting>"; // const-string v3, " <setting>"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* check-cast v4, Ljava/lang/String; */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = "</setting>\n"; // const-string v4, "</setting>\n"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 78 */
} // .end local v2 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
/* .line 80 */
} // :cond_0
final String v1 = " </feature>\n"; // const-string v1, " </feature>\n"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 81 */
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
