.class public Lcom/android/server/location/mnlutils/bean/MnlConfig;
.super Ljava/lang/Object;
.source "MnlConfig.java"


# instance fields
.field private featureList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;",
            ">;"
        }
    .end annotation
.end field

.field private type:Ljava/lang/String;

.field private version:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    const-string v0, "gps"

    iput-object v0, p0, Lcom/android/server/location/mnlutils/bean/MnlConfig;->type:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getFeature(Ljava/lang/String;)Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .line 53
    iget-object v0, p0, Lcom/android/server/location/mnlutils/bean/MnlConfig;->featureList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;

    .line 54
    .local v1, "feature":Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
    invoke-virtual {v1}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 55
    return-object v1

    .line 57
    .end local v1    # "feature":Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
    :cond_0
    goto :goto_0

    .line 58
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public getFeatureList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;",
            ">;"
        }
    .end annotation

    .line 36
    iget-object v0, p0, Lcom/android/server/location/mnlutils/bean/MnlConfig;->featureList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/location/mnlutils/bean/MnlConfig;->featureList:Ljava/util/ArrayList;

    .line 39
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/mnlutils/bean/MnlConfig;->featureList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/android/server/location/mnlutils/bean/MnlConfig;->version:Ljava/lang/String;

    return-object v0
.end method

.method public setFeatureList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;",
            ">;)V"
        }
    .end annotation

    .line 43
    .local p1, "featureList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;>;"
    iput-object p1, p0, Lcom/android/server/location/mnlutils/bean/MnlConfig;->featureList:Ljava/util/ArrayList;

    .line 44
    return-void
.end method

.method public setVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "version"    # Ljava/lang/String;

    .line 32
    iput-object p1, p0, Lcom/android/server/location/mnlutils/bean/MnlConfig;->version:Ljava/lang/String;

    .line 33
    return-void
.end method

.method public toXmlString()Ljava/lang/String;
    .locals 4

    .line 16
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 17
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v1, "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<mnl_config version=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/location/mnlutils/bean/MnlConfig;->version:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" type=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/location/mnlutils/bean/MnlConfig;->type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\">\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    iget-object v1, p0, Lcom/android/server/location/mnlutils/bean/MnlConfig;->featureList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;

    .line 21
    .local v2, "feature":Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
    invoke-virtual {v2}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->toXmlString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22
    .end local v2    # "feature":Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
    goto :goto_0

    .line 23
    :cond_0
    const-string v1, "</mnl_config>\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
