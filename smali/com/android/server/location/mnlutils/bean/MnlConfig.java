public class com.android.server.location.mnlutils.bean.MnlConfig {
	 /* .source "MnlConfig.java" */
	 /* # instance fields */
	 private java.util.ArrayList featureList;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/ArrayList<", */
	 /* "Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private java.lang.String type;
private java.lang.String version;
/* # direct methods */
public com.android.server.location.mnlutils.bean.MnlConfig ( ) {
/* .locals 1 */
/* .line 5 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 7 */
final String v0 = "gps"; // const-string v0, "gps"
this.type = v0;
return;
} // .end method
/* # virtual methods */
public com.android.server.location.mnlutils.bean.MnlConfigFeature getFeature ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "name" # Ljava/lang/String; */
/* .line 53 */
v0 = this.featureList;
(( java.util.ArrayList ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature; */
/* .line 54 */
/* .local v1, "feature":Lcom/android/server/location/mnlutils/bean/MnlConfigFeature; */
(( com.android.server.location.mnlutils.bean.MnlConfigFeature ) v1 ).getName ( ); // invoke-virtual {v1}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->getName()Ljava/lang/String;
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
	 /* .line 55 */
	 /* .line 57 */
} // .end local v1 # "feature":Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
} // :cond_0
/* .line 58 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
public java.util.ArrayList getFeatureList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 36 */
v0 = this.featureList;
/* if-nez v0, :cond_0 */
/* .line 37 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.featureList = v0;
/* .line 39 */
} // :cond_0
v0 = this.featureList;
} // .end method
public java.lang.String getVersion ( ) {
/* .locals 1 */
/* .line 28 */
v0 = this.version;
} // .end method
public void setFeatureList ( java.util.ArrayList p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/ArrayList<", */
/* "Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 43 */
/* .local p1, "featureList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;>;" */
this.featureList = p1;
/* .line 44 */
return;
} // .end method
public void setVersion ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "version" # Ljava/lang/String; */
/* .line 32 */
this.version = p1;
/* .line 33 */
return;
} // .end method
public java.lang.String toXmlString ( ) {
/* .locals 4 */
/* .line 16 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 17 */
/* .local v0, "builder":Ljava/lang/StringBuilder; */
final String v1 = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"; // const-string v1, "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 18 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "<mnl_config version=\""; // const-string v2, "<mnl_config version=\""
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.version;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "\" type=\""; // const-string v2, "\" type=\""
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.type;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "\">\n"; // const-string v2, "\">\n"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 20 */
v1 = this.featureList;
(( java.util.ArrayList ) v1 ).iterator ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature; */
/* .line 21 */
/* .local v2, "feature":Lcom/android/server/location/mnlutils/bean/MnlConfigFeature; */
(( com.android.server.location.mnlutils.bean.MnlConfigFeature ) v2 ).toXmlString ( ); // invoke-virtual {v2}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->toXmlString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 22 */
} // .end local v2 # "feature":Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;
/* .line 23 */
} // :cond_0
final String v1 = "</mnl_config>\n"; // const-string v1, "</mnl_config>\n"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 24 */
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
