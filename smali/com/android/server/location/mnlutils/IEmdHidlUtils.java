class com.android.server.location.mnlutils.IEmdHidlUtils {
	 /* .source "IEmdHidlUtils.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/location/mnlutils/IEmdHidlUtils$VintfHalCache; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String TAG;
public static com.android.server.location.hardware.mtk.engineermode.V1_3.IEmd mEmHIDLService;
private static java.util.function.Supplier mVintfEmService;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/function/Supplier<", */
/* "Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static com.android.server.location.mnlutils.IEmdHidlUtils ( ) {
/* .locals 1 */
/* .line 18 */
int v0 = 0; // const/4 v0, 0x0
return;
} // .end method
private com.android.server.location.mnlutils.IEmdHidlUtils ( ) {
/* .locals 0 */
/* .line 21 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 22 */
return;
} // .end method
public static com.android.server.location.hardware.mtk.engineermode.aidl.IEmds getEmAidlService ( ) {
/* .locals 2 */
/* .line 40 */
final String v0 = "Glp-IEmdHidlUtils"; // const-string v0, "Glp-IEmdHidlUtils"
final String v1 = "getEmAidlService ..."; // const-string v1, "getEmAidlService ..."
android.util.Log .d ( v0,v1 );
/* .line 41 */
v0 = com.android.server.location.mnlutils.IEmdHidlUtils.mVintfEmService;
/* if-nez v0, :cond_0 */
/* .line 42 */
/* new-instance v0, Lcom/android/server/location/mnlutils/IEmdHidlUtils$VintfHalCache; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, v1}, Lcom/android/server/location/mnlutils/IEmdHidlUtils$VintfHalCache;-><init>(Lcom/android/server/location/mnlutils/IEmdHidlUtils$VintfHalCache-IA;)V */
/* .line 44 */
} // :cond_0
v0 = com.android.server.location.mnlutils.IEmdHidlUtils.mVintfEmService;
/* check-cast v0, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds; */
} // .end method
public static synchronized com.android.server.location.hardware.mtk.engineermode.V1_3.IEmd getEmHidlService ( ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* const-class v0, Lcom/android/server/location/mnlutils/IEmdHidlUtils; */
/* monitor-enter v0 */
/* .line 25 */
try { // :try_start_0
final String v1 = "Glp-IEmdHidlUtils"; // const-string v1, "Glp-IEmdHidlUtils"
final String v2 = "getEmHidlService"; // const-string v2, "getEmHidlService"
android.util.Log .d ( v1,v2 );
/* .line 26 */
v1 = com.android.server.location.mnlutils.IEmdHidlUtils.mEmHIDLService;
/* if-nez v1, :cond_0 */
/* .line 27 */
final String v1 = "Glp-IEmdHidlUtils"; // const-string v1, "Glp-IEmdHidlUtils"
final String v2 = "getEmHidlService init..."; // const-string v2, "getEmHidlService init..."
android.util.Log .v ( v1,v2 );
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 29 */
try { // :try_start_1
final String v1 = "EmHidlServer"; // const-string v1, "EmHidlServer"
int v2 = 1; // const/4 v2, 0x1
com.android.server.location.hardware.mtk.engineermode.V1_3.IEmd .getService ( v1,v2 );
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 34 */
/* .line 30 */
/* :catch_0 */
/* move-exception v1 */
/* .line 31 */
/* .local v1, "e":Landroid/os/RemoteException; */
try { // :try_start_2
final String v2 = "Glp-IEmdHidlUtils"; // const-string v2, "Glp-IEmdHidlUtils"
final String v3 = "EmHIDLConnection Exception"; // const-string v3, "EmHIDLConnection Exception"
android.util.Log .e ( v2,v3 );
/* .line 32 */
final String v2 = "Glp-IEmdHidlUtils"; // const-string v2, "Glp-IEmdHidlUtils"
android.util.Log .getStackTraceString ( v1 );
android.util.Log .e ( v2,v3 );
/* .line 33 */
/* throw v1 */
/* .line 36 */
} // .end local v1 # "e":Landroid/os/RemoteException;
} // :cond_0
} // :goto_0
v1 = com.android.server.location.mnlutils.IEmdHidlUtils.mEmHIDLService;
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* monitor-exit v0 */
/* .line 24 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* throw v1 */
} // .end method
