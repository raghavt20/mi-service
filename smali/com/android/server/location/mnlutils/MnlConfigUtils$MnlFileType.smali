.class final enum Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;
.super Ljava/lang/Enum;
.source "MnlConfigUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/location/mnlutils/MnlConfigUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "MnlFileType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;

.field public static final enum MNL_FILE_BACKUP:Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;

.field public static final enum MNL_FILE_CURRENT:Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;


# direct methods
.method private static synthetic $values()[Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;
    .locals 2

    .line 23
    sget-object v0, Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;->MNL_FILE_CURRENT:Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;

    sget-object v1, Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;->MNL_FILE_BACKUP:Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;

    filled-new-array {v0, v1}, [Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;

    move-result-object v0

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 24
    new-instance v0, Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;

    const-string v1, "MNL_FILE_CURRENT"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;->MNL_FILE_CURRENT:Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;

    .line 25
    new-instance v0, Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;

    const-string v1, "MNL_FILE_BACKUP"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;->MNL_FILE_BACKUP:Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;

    .line 23
    invoke-static {}, Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;->$values()[Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;

    move-result-object v0

    sput-object v0, Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;->$VALUES:[Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .line 23
    const-class v0, Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;

    return-object v0
.end method

.method public static values()[Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;
    .locals 1

    .line 23
    sget-object v0, Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;->$VALUES:[Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;

    invoke-virtual {v0}, [Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;

    return-object v0
.end method
