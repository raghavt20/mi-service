.class Lcom/android/server/location/mnlutils/IEmdHidlUtils;
.super Ljava/lang/Object;
.source "IEmdHidlUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/location/mnlutils/IEmdHidlUtils$VintfHalCache;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Glp-IEmdHidlUtils"

.field public static mEmHIDLService:Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd;

.field private static mVintfEmService:Ljava/util/function/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/function/Supplier<",
            "Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 18
    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/location/mnlutils/IEmdHidlUtils;->mEmHIDLService:Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    return-void
.end method

.method public static getEmAidlService()Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds;
    .locals 2

    .line 40
    const-string v0, "Glp-IEmdHidlUtils"

    const-string v1, "getEmAidlService ..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    sget-object v0, Lcom/android/server/location/mnlutils/IEmdHidlUtils;->mVintfEmService:Ljava/util/function/Supplier;

    if-nez v0, :cond_0

    .line 42
    new-instance v0, Lcom/android/server/location/mnlutils/IEmdHidlUtils$VintfHalCache;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/android/server/location/mnlutils/IEmdHidlUtils$VintfHalCache;-><init>(Lcom/android/server/location/mnlutils/IEmdHidlUtils$VintfHalCache-IA;)V

    sput-object v0, Lcom/android/server/location/mnlutils/IEmdHidlUtils;->mVintfEmService:Ljava/util/function/Supplier;

    .line 44
    :cond_0
    sget-object v0, Lcom/android/server/location/mnlutils/IEmdHidlUtils;->mVintfEmService:Ljava/util/function/Supplier;

    invoke-interface {v0}, Ljava/util/function/Supplier;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds;

    return-object v0
.end method

.method public static declared-synchronized getEmHidlService()Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-class v0, Lcom/android/server/location/mnlutils/IEmdHidlUtils;

    monitor-enter v0

    .line 25
    :try_start_0
    const-string v1, "Glp-IEmdHidlUtils"

    const-string v2, "getEmHidlService"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 26
    sget-object v1, Lcom/android/server/location/mnlutils/IEmdHidlUtils;->mEmHIDLService:Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd;

    if-nez v1, :cond_0

    .line 27
    const-string v1, "Glp-IEmdHidlUtils"

    const-string v2, "getEmHidlService init..."

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 29
    :try_start_1
    const-string v1, "EmHidlServer"

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd;->getService(Ljava/lang/String;Z)Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd;

    move-result-object v1

    sput-object v1, Lcom/android/server/location/mnlutils/IEmdHidlUtils;->mEmHIDLService:Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 34
    goto :goto_0

    .line 30
    :catch_0
    move-exception v1

    .line 31
    .local v1, "e":Landroid/os/RemoteException;
    :try_start_2
    const-string v2, "Glp-IEmdHidlUtils"

    const-string v3, "EmHIDLConnection Exception"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 32
    const-string v2, "Glp-IEmdHidlUtils"

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 33
    throw v1

    .line 36
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_0
    :goto_0
    sget-object v1, Lcom/android/server/location/mnlutils/IEmdHidlUtils;->mEmHIDLService:Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit v0

    return-object v1

    .line 24
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method
