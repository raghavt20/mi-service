.class public Lcom/android/server/location/mnlutils/MnlConfigUtils;
.super Ljava/lang/Object;
.source "MnlConfigUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Glp-MnlConfigUtils"


# instance fields
.field aidlService:Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds;

.field hidlService:Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/location/mnlutils/MnlConfigUtils;->hidlService:Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd;

    .line 18
    iput-object v0, p0, Lcom/android/server/location/mnlutils/MnlConfigUtils;->aidlService:Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds;

    return-void
.end method

.method private arrayToByteList([B)Ljava/util/ArrayList;
    .locals 7
    .param p1, "bytesPrim"    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation

    .line 70
    if-nez p1, :cond_0

    .line 71
    const/4 v0, 0x0

    return-object v0

    .line 72
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 73
    .local v0, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
    array-length v1, p1

    new-array v1, v1, [Ljava/lang/Byte;

    .line 74
    .local v1, "bytes":[Ljava/lang/Byte;
    const/4 v2, 0x0

    .line 75
    .local v2, "i":I
    array-length v3, p1

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_1

    aget-byte v5, p1, v4

    .local v5, "b":B
    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .end local v5    # "b":B
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 76
    :cond_1
    return-object v0
.end method

.method private listToByteArray(Ljava/util/ArrayList;)[B
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Byte;",
            ">;)[B"
        }
    .end annotation

    .line 79
    .local p1, "bytesPrim":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
    if-nez p1, :cond_0

    .line 80
    const/4 v0, 0x0

    return-object v0

    .line 81
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/Byte;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Byte;

    .line 82
    .local v0, "bytes":[Ljava/lang/Byte;
    array-length v1, v0

    new-array v1, v1, [B

    .line 83
    .local v1, "result":[B
    const/4 v2, 0x0

    .line 84
    .local v2, "i":I
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Byte;

    .local v4, "b":Ljava/lang/Byte;
    add-int/lit8 v5, v2, 0x1

    .end local v2    # "i":I
    .local v5, "i":I
    invoke-virtual {v4}, Ljava/lang/Byte;->byteValue()B

    move-result v6

    aput-byte v6, v1, v2

    move v2, v5

    .end local v4    # "b":Ljava/lang/Byte;
    goto :goto_0

    .line 85
    .end local v5    # "i":I
    .restart local v2    # "i":I
    :cond_1
    return-object v1
.end method

.method private loadFile(Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;)Ljava/util/ArrayList;
    .locals 3
    .param p1, "fileType"    # Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;",
            ")",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation

    .line 30
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 31
    .local v0, "input":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {p1}, Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 32
    invoke-static {}, Lcom/android/server/location/mnlutils/IEmdHidlUtils;->getEmAidlService()Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 33
    invoke-static {}, Lcom/android/server/location/mnlutils/IEmdHidlUtils;->getEmAidlService()Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/location/mnlutils/MnlConfigUtils;->aidlService:Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds;

    .line 34
    invoke-virtual {v0}, Ljava/util/ArrayList;->stream()Ljava/util/stream/Stream;

    move-result-object v1

    new-instance v2, Lcom/android/server/location/mnlutils/MnlConfigUtils$$ExternalSyntheticLambda0;

    invoke-direct {v2}, Lcom/android/server/location/mnlutils/MnlConfigUtils$$ExternalSyntheticLambda0;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/stream/Stream;->mapToInt(Ljava/util/function/ToIntFunction;)Ljava/util/stream/IntStream;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/stream/IntStream;->toArray()[I

    move-result-object v1

    .line 35
    .local v1, "array":[I
    iget-object v2, p0, Lcom/android/server/location/mnlutils/MnlConfigUtils;->aidlService:Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds;

    invoke-interface {v2, v1}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds;->readMnlConfigFile([I)[B

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/server/location/mnlutils/MnlConfigUtils;->arrayToByteList([B)Ljava/util/ArrayList;

    move-result-object v2

    return-object v2

    .line 37
    .end local v1    # "array":[I
    :cond_0
    iget-object v1, p0, Lcom/android/server/location/mnlutils/MnlConfigUtils;->hidlService:Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd;

    if-nez v1, :cond_1

    .line 38
    invoke-static {}, Lcom/android/server/location/mnlutils/IEmdHidlUtils;->getEmHidlService()Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/location/mnlutils/MnlConfigUtils;->hidlService:Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd;

    .line 40
    :cond_1
    iget-object v1, p0, Lcom/android/server/location/mnlutils/MnlConfigUtils;->hidlService:Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd;

    invoke-interface {v1, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd;->readMnlConfigFile(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v1
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 42
    .end local v0    # "input":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :catch_0
    move-exception v0

    .line 43
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "Glp-MnlConfigUtils"

    const-string v2, "load mnl config fail"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 45
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    return-object v1
.end method

.method private saveFile(Ljava/util/ArrayList;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Byte;",
            ">;)Z"
        }
    .end annotation

    .line 50
    .local p1, "dataToWrite":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 52
    .local v0, "reserve":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :try_start_0
    invoke-static {}, Lcom/android/server/location/mnlutils/IEmdHidlUtils;->getEmAidlService()Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 53
    invoke-static {}, Lcom/android/server/location/mnlutils/IEmdHidlUtils;->getEmAidlService()Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/location/mnlutils/MnlConfigUtils;->aidlService:Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds;

    .line 54
    invoke-virtual {v0}, Ljava/util/ArrayList;->stream()Ljava/util/stream/Stream;

    move-result-object v1

    new-instance v2, Lcom/android/server/location/mnlutils/MnlConfigUtils$$ExternalSyntheticLambda0;

    invoke-direct {v2}, Lcom/android/server/location/mnlutils/MnlConfigUtils$$ExternalSyntheticLambda0;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/stream/Stream;->mapToInt(Ljava/util/function/ToIntFunction;)Ljava/util/stream/IntStream;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/stream/IntStream;->toArray()[I

    move-result-object v1

    .line 55
    .local v1, "array":[I
    iget-object v2, p0, Lcom/android/server/location/mnlutils/MnlConfigUtils;->aidlService:Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds;

    invoke-direct {p0, p1}, Lcom/android/server/location/mnlutils/MnlConfigUtils;->listToByteArray(Ljava/util/ArrayList;)[B

    move-result-object v3

    invoke-interface {v2, v3, v1}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds;->writeMnlConfigFile([B[I)Z

    move-result v2

    return v2

    .line 57
    .end local v1    # "array":[I
    :cond_0
    iget-object v1, p0, Lcom/android/server/location/mnlutils/MnlConfigUtils;->hidlService:Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd;

    if-nez v1, :cond_1

    .line 58
    invoke-static {}, Lcom/android/server/location/mnlutils/IEmdHidlUtils;->getEmHidlService()Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/location/mnlutils/MnlConfigUtils;->hidlService:Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd;

    .line 60
    :cond_1
    iget-object v1, p0, Lcom/android/server/location/mnlutils/MnlConfigUtils;->hidlService:Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd;

    invoke-interface {v1, p1, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd;->writeMnlConfigFile(Ljava/util/ArrayList;Ljava/util/ArrayList;)Z

    move-result v1
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return v1

    .line 62
    :catch_0
    move-exception v1

    .line 63
    .local v1, "e":Landroid/os/RemoteException;
    const-string v2, "Glp-MnlConfigUtils"

    const-string v3, "save mnl config fail"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 65
    const/4 v2, 0x0

    return v2
.end method


# virtual methods
.method public getBackUpMnlConfig()Lcom/android/server/location/mnlutils/bean/MnlConfig;
    .locals 7

    .line 107
    sget-object v0, Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;->MNL_FILE_BACKUP:Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;

    invoke-direct {p0, v0}, Lcom/android/server/location/mnlutils/MnlConfigUtils;->loadFile(Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;)Ljava/util/ArrayList;

    move-result-object v0

    .line 108
    .local v0, "byteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 109
    const/4 v1, 0x0

    return-object v1

    .line 111
    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [B

    .line 112
    .local v1, "bytes":[B
    const/4 v2, 0x0

    .line 113
    .local v2, "index":I
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Byte;

    .line 114
    .local v4, "bt":Ljava/lang/Byte;
    add-int/lit8 v5, v2, 0x1

    .end local v2    # "index":I
    .local v5, "index":I
    invoke-virtual {v4}, Ljava/lang/Byte;->byteValue()B

    move-result v6

    aput-byte v6, v1, v2

    .line 115
    .end local v4    # "bt":Ljava/lang/Byte;
    move v2, v5

    goto :goto_0

    .line 116
    .end local v5    # "index":I
    .restart local v2    # "index":I
    :cond_1
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v1}, Ljava/lang/String;-><init>([B)V

    invoke-static {v3}, Lcom/android/server/location/mnlutils/MnlConfigParseUtils;->parseXml(Ljava/lang/String;)Lcom/android/server/location/mnlutils/bean/MnlConfig;

    move-result-object v3

    return-object v3
.end method

.method public getMnlConfig()Lcom/android/server/location/mnlutils/bean/MnlConfig;
    .locals 7

    .line 94
    sget-object v0, Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;->MNL_FILE_CURRENT:Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;

    invoke-direct {p0, v0}, Lcom/android/server/location/mnlutils/MnlConfigUtils;->loadFile(Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;)Ljava/util/ArrayList;

    move-result-object v0

    .line 95
    .local v0, "byteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 96
    const/4 v1, 0x0

    return-object v1

    .line 98
    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [B

    .line 99
    .local v1, "bytes":[B
    const/4 v2, 0x0

    .line 100
    .local v2, "index":I
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Byte;

    .line 101
    .local v4, "bt":Ljava/lang/Byte;
    add-int/lit8 v5, v2, 0x1

    .end local v2    # "index":I
    .local v5, "index":I
    invoke-virtual {v4}, Ljava/lang/Byte;->byteValue()B

    move-result v6

    aput-byte v6, v1, v2

    .line 102
    .end local v4    # "bt":Ljava/lang/Byte;
    move v2, v5

    goto :goto_0

    .line 103
    .end local v5    # "index":I
    .restart local v2    # "index":I
    :cond_1
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v1}, Ljava/lang/String;-><init>([B)V

    invoke-static {v3}, Lcom/android/server/location/mnlutils/MnlConfigParseUtils;->parseXml(Ljava/lang/String;)Lcom/android/server/location/mnlutils/bean/MnlConfig;

    move-result-object v3

    return-object v3
.end method

.method public resetMnlConfig()Z
    .locals 2

    .line 141
    sget-object v0, Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;->MNL_FILE_BACKUP:Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;

    invoke-direct {p0, v0}, Lcom/android/server/location/mnlutils/MnlConfigUtils;->loadFile(Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;)Ljava/util/ArrayList;

    move-result-object v0

    .line 142
    .local v0, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eqz v1, :cond_0

    .line 143
    invoke-direct {p0, v0}, Lcom/android/server/location/mnlutils/MnlConfigUtils;->saveFile(Ljava/util/ArrayList;)Z

    move-result v1

    return v1

    .line 145
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method public saveMnlConfig(Lcom/android/server/location/mnlutils/bean/MnlConfig;)Z
    .locals 7
    .param p1, "mnlConfig"    # Lcom/android/server/location/mnlutils/bean/MnlConfig;

    .line 126
    invoke-virtual {p1}, Lcom/android/server/location/mnlutils/bean/MnlConfig;->toXmlString()Ljava/lang/String;

    move-result-object v0

    .line 127
    .local v0, "s":Ljava/lang/String;
    sget-object v1, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    .line 128
    .local v1, "bytes":[B
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 129
    .local v2, "byteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
    array-length v3, v1

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_0

    aget-byte v5, v1, v4

    .line 130
    .local v5, "b":B
    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 129
    .end local v5    # "b":B
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 132
    :cond_0
    invoke-direct {p0, v2}, Lcom/android/server/location/mnlutils/MnlConfigUtils;->saveFile(Ljava/util/ArrayList;)Z

    move-result v3

    return v3
.end method
