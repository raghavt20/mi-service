.class Lcom/android/server/location/mnlutils/MnlConfigParseUtils;
.super Ljava/lang/Object;
.source "MnlConfigParseUtils.java"


# static fields
.field private static final TAG_CONFIG:Ljava/lang/String; = "config"

.field private static final TAG_FEATURE:Ljava/lang/String; = "feature"

.field private static final TAG_FORMAT:Ljava/lang/String; = "format"

.field private static final TAG_MNL_CONFIG:Ljava/lang/String; = "mnl_config"

.field private static final TAG_SETTING:Ljava/lang/String; = "setting"

.field private static final TAG_VERSION:Ljava/lang/String; = "version"

.field private static featureTemp:Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;

.field private static nowFeatureAtt:Ljava/lang/String;

.field private static nowFormat:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    return-void
.end method

.method private static handleEndTag(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/server/location/mnlutils/bean/MnlConfig;)V
    .locals 2
    .param p0, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .param p1, "mnlConfig"    # Lcom/android/server/location/mnlutils/bean/MnlConfig;

    .line 130
    const-string v0, "feature"

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132
    invoke-virtual {p1}, Lcom/android/server/location/mnlutils/bean/MnlConfig;->getFeatureList()Ljava/util/ArrayList;

    move-result-object v0

    sget-object v1, Lcom/android/server/location/mnlutils/MnlConfigParseUtils;->featureTemp:Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 134
    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/location/mnlutils/MnlConfigParseUtils;->featureTemp:Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;

    .line 136
    :cond_0
    return-void
.end method

.method private static handleStartTag(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/server/location/mnlutils/bean/MnlConfig;)V
    .locals 3
    .param p0, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .param p1, "mnlConfig"    # Lcom/android/server/location/mnlutils/bean/MnlConfig;

    .line 73
    if-nez p1, :cond_0

    .line 74
    new-instance v0, Lcom/android/server/location/mnlutils/bean/MnlConfig;

    invoke-direct {v0}, Lcom/android/server/location/mnlutils/bean/MnlConfig;-><init>()V

    move-object p1, v0

    .line 77
    :cond_0
    const-string v0, "mnl_config"

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string/jumbo v1, "version"

    if-eqz v0, :cond_3

    .line 79
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeCount()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 80
    invoke-interface {p0, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 81
    invoke-interface {p0, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/android/server/location/mnlutils/bean/MnlConfig;->setVersion(Ljava/lang/String;)V

    .line 79
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .end local v0    # "i":I
    :cond_2
    goto :goto_1

    .line 84
    :cond_3
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v2, "feature"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 85
    new-instance v0, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;

    invoke-direct {v0}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;-><init>()V

    sput-object v0, Lcom/android/server/location/mnlutils/MnlConfigParseUtils;->featureTemp:Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;

    .line 86
    sput-object v2, Lcom/android/server/location/mnlutils/MnlConfigParseUtils;->nowFeatureAtt:Ljava/lang/String;

    goto :goto_1

    .line 87
    :cond_4
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 89
    sput-object v1, Lcom/android/server/location/mnlutils/MnlConfigParseUtils;->nowFeatureAtt:Ljava/lang/String;

    goto :goto_1

    .line 90
    :cond_5
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "config"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 91
    sput-object v1, Lcom/android/server/location/mnlutils/MnlConfigParseUtils;->nowFeatureAtt:Ljava/lang/String;

    goto :goto_1

    .line 92
    :cond_6
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "format"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 93
    sput-object v1, Lcom/android/server/location/mnlutils/MnlConfigParseUtils;->nowFeatureAtt:Ljava/lang/String;

    goto :goto_1

    .line 94
    :cond_7
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "setting"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 95
    sput-object v1, Lcom/android/server/location/mnlutils/MnlConfigParseUtils;->nowFeatureAtt:Ljava/lang/String;

    .line 97
    :cond_8
    :goto_1
    return-void
.end method

.method private static handleText(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 4
    .param p0, "parser"    # Lorg/xmlpull/v1/XmlPullParser;

    .line 100
    sget-object v0, Lcom/android/server/location/mnlutils/MnlConfigParseUtils;->nowFeatureAtt:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 101
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    goto :goto_0

    :sswitch_0
    const-string/jumbo v1, "setting"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    goto :goto_1

    :sswitch_1
    const-string/jumbo v1, "version"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_2
    const-string v1, "feature"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_3
    const-string v1, "format"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    goto :goto_1

    :sswitch_4
    const-string v1, "config"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    goto :goto_1

    :goto_0
    const/4 v0, -0x1

    :goto_1
    const/4 v1, 0x0

    packed-switch v0, :pswitch_data_0

    goto :goto_2

    .line 120
    :pswitch_0
    sget-object v0, Lcom/android/server/location/mnlutils/MnlConfigParseUtils;->featureTemp:Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;

    invoke-virtual {v0}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->getFormatSettings()Ljava/util/LinkedHashMap;

    move-result-object v0

    sget-object v2, Lcom/android/server/location/mnlutils/MnlConfigParseUtils;->nowFormat:Ljava/lang/String;

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/server/location/mnlutils/MnlConfigParseUtils;->trimString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    sput-object v1, Lcom/android/server/location/mnlutils/MnlConfigParseUtils;->nowFeatureAtt:Ljava/lang/String;

    .line 122
    goto :goto_2

    .line 116
    :pswitch_1
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/location/mnlutils/MnlConfigParseUtils;->trimString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/location/mnlutils/MnlConfigParseUtils;->nowFormat:Ljava/lang/String;

    .line 117
    sput-object v1, Lcom/android/server/location/mnlutils/MnlConfigParseUtils;->nowFeatureAtt:Ljava/lang/String;

    .line 118
    goto :goto_2

    .line 112
    :pswitch_2
    sget-object v0, Lcom/android/server/location/mnlutils/MnlConfigParseUtils;->featureTemp:Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/server/location/mnlutils/MnlConfigParseUtils;->trimString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->setConfig(Ljava/lang/String;)V

    .line 113
    sput-object v1, Lcom/android/server/location/mnlutils/MnlConfigParseUtils;->nowFeatureAtt:Ljava/lang/String;

    .line 114
    goto :goto_2

    .line 108
    :pswitch_3
    sget-object v0, Lcom/android/server/location/mnlutils/MnlConfigParseUtils;->featureTemp:Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/server/location/mnlutils/MnlConfigParseUtils;->trimString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->setVersion(Ljava/lang/String;)V

    .line 109
    sput-object v1, Lcom/android/server/location/mnlutils/MnlConfigParseUtils;->nowFeatureAtt:Ljava/lang/String;

    .line 110
    goto :goto_2

    .line 103
    :pswitch_4
    sget-object v0, Lcom/android/server/location/mnlutils/MnlConfigParseUtils;->featureTemp:Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/server/location/mnlutils/MnlConfigParseUtils;->trimString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/android/server/location/mnlutils/bean/MnlConfigFeature;->setName(Ljava/lang/String;)V

    .line 104
    sput-object v1, Lcom/android/server/location/mnlutils/MnlConfigParseUtils;->nowFeatureAtt:Ljava/lang/String;

    .line 105
    nop

    .line 127
    :cond_1
    :goto_2
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x50c07cbe -> :sswitch_4
        -0x4ba00809 -> :sswitch_3
        -0x3a5d850a -> :sswitch_2
        0x14f51cd8 -> :sswitch_1
        0x765f0e50 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static declared-synchronized parseXml(Ljava/lang/String;)Lcom/android/server/location/mnlutils/bean/MnlConfig;
    .locals 8
    .param p0, "xml"    # Ljava/lang/String;

    const-class v0, Lcom/android/server/location/mnlutils/MnlConfigParseUtils;

    monitor-enter v0

    .line 35
    :try_start_0
    new-instance v1, Lcom/android/server/location/mnlutils/bean/MnlConfig;

    invoke-direct {v1}, Lcom/android/server/location/mnlutils/bean/MnlConfig;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 37
    .local v1, "mnlConfigNow":Lcom/android/server/location/mnlutils/bean/MnlConfig;
    const/4 v2, 0x0

    if-nez p0, :cond_0

    .line 38
    monitor-exit v0

    return-object v2

    .line 41
    :cond_0
    :try_start_1
    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 43
    .local v3, "inputStream":Ljava/io/InputStream;
    :try_start_2
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v4

    .line 44
    .local v4, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    invoke-virtual {v4}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v5

    .line 45
    .local v5, "parser":Lorg/xmlpull/v1/XmlPullParser;
    const-string v6, "UTF-8"

    invoke-interface {v5, v3, v6}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 46
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v6

    .line 47
    .local v6, "eventType":I
    :goto_0
    const/4 v7, 0x1

    if-eq v6, v7, :cond_1

    .line 48
    packed-switch v6, :pswitch_data_0

    goto :goto_1

    .line 53
    :pswitch_0
    invoke-static {v5}, Lcom/android/server/location/mnlutils/MnlConfigParseUtils;->handleText(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 54
    goto :goto_1

    .line 56
    :pswitch_1
    invoke-static {v5, v1}, Lcom/android/server/location/mnlutils/MnlConfigParseUtils;->handleEndTag(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/server/location/mnlutils/bean/MnlConfig;)V

    .line 57
    goto :goto_1

    .line 50
    :pswitch_2
    invoke-static {v5, v1}, Lcom/android/server/location/mnlutils/MnlConfigParseUtils;->handleStartTag(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/server/location/mnlutils/bean/MnlConfig;)V

    .line 51
    nop

    .line 61
    :goto_1
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v7

    move v6, v7

    goto :goto_0

    .line 63
    :cond_1
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 64
    monitor-exit v0

    return-object v1

    .line 65
    .end local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v5    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v6    # "eventType":I
    :catch_0
    move-exception v4

    .line 66
    .local v4, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 68
    .end local v4    # "e":Ljava/lang/Exception;
    monitor-exit v0

    return-object v2

    .line 34
    .end local v1    # "mnlConfigNow":Lcom/android/server/location/mnlutils/bean/MnlConfig;
    .end local v3    # "inputStream":Ljava/io/InputStream;
    .end local p0    # "xml":Ljava/lang/String;
    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static trimString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "text"    # Ljava/lang/String;

    .line 139
    const-string v0, "\n"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
