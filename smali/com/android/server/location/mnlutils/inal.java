class inal extends java.lang.Enum {
	 /* .source "MnlConfigUtils.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/location/mnlutils/MnlConfigUtils; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x401a */
/* name = "MnlFileType" */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/Enum<", */
/* "Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;", */
/* ">;" */
/* } */
} // .end annotation
/* # static fields */
private static final com.android.server.location.mnlutils.MnlConfigUtils$MnlFileType $VALUES; //synthetic
public static final com.android.server.location.mnlutils.MnlConfigUtils$MnlFileType MNL_FILE_BACKUP;
public static final com.android.server.location.mnlutils.MnlConfigUtils$MnlFileType MNL_FILE_CURRENT;
/* # direct methods */
private static com.android.server.location.mnlutils.MnlConfigUtils$MnlFileType $values ( ) { //synthethic
/* .locals 2 */
/* .line 23 */
v0 = com.android.server.location.mnlutils.MnlConfigUtils$MnlFileType.MNL_FILE_CURRENT;
v1 = com.android.server.location.mnlutils.MnlConfigUtils$MnlFileType.MNL_FILE_BACKUP;
/* filled-new-array {v0, v1}, [Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType; */
} // .end method
static inal ( ) {
/* .locals 3 */
/* .line 24 */
/* new-instance v0, Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType; */
final String v1 = "MNL_FILE_CURRENT"; // const-string v1, "MNL_FILE_CURRENT"
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {v0, v1, v2}, Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;-><init>(Ljava/lang/String;I)V */
/* .line 25 */
/* new-instance v0, Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType; */
final String v1 = "MNL_FILE_BACKUP"; // const-string v1, "MNL_FILE_BACKUP"
int v2 = 1; // const/4 v2, 0x1
/* invoke-direct {v0, v1, v2}, Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;-><init>(Ljava/lang/String;I)V */
/* .line 23 */
com.android.server.location.mnlutils.MnlConfigUtils$MnlFileType .$values ( );
return;
} // .end method
private inal ( ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 23 */
/* invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V */
return;
} // .end method
public static com.android.server.location.mnlutils.MnlConfigUtils$MnlFileType valueOf ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p0, "name" # Ljava/lang/String; */
/* .line 23 */
/* const-class v0, Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType; */
java.lang.Enum .valueOf ( v0,p0 );
/* check-cast v0, Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType; */
} // .end method
public static com.android.server.location.mnlutils.MnlConfigUtils$MnlFileType values ( ) {
/* .locals 1 */
/* .line 23 */
v0 = com.android.server.location.mnlutils.MnlConfigUtils$MnlFileType.$VALUES;
(( com.android.server.location.mnlutils.MnlConfigUtils$MnlFileType ) v0 ).clone ( ); // invoke-virtual {v0}, [Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;->clone()Ljava/lang/Object;
/* check-cast v0, [Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType; */
} // .end method
