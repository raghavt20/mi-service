public class com.android.server.location.mnlutils.MnlConfigUtils {
	 /* .source "MnlConfigUtils.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String TAG;
/* # instance fields */
com.android.server.location.hardware.mtk.engineermode.aidl.IEmds aidlService;
com.android.server.location.hardware.mtk.engineermode.V1_3.IEmd hidlService;
/* # direct methods */
public com.android.server.location.mnlutils.MnlConfigUtils ( ) {
	 /* .locals 1 */
	 /* .line 13 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 17 */
	 int v0 = 0; // const/4 v0, 0x0
	 this.hidlService = v0;
	 /* .line 18 */
	 this.aidlService = v0;
	 return;
} // .end method
private java.util.ArrayList arrayToByteList ( Object[] p0 ) {
	 /* .locals 7 */
	 /* .param p1, "bytesPrim" # [B */
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "([B)", */
	 /* "Ljava/util/ArrayList<", */
	 /* "Ljava/lang/Byte;", */
	 /* ">;" */
	 /* } */
} // .end annotation
/* .line 70 */
/* if-nez p1, :cond_0 */
/* .line 71 */
int v0 = 0; // const/4 v0, 0x0
/* .line 72 */
} // :cond_0
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 73 */
/* .local v0, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;" */
/* array-length v1, p1 */
/* new-array v1, v1, [Ljava/lang/Byte; */
/* .line 74 */
/* .local v1, "bytes":[Ljava/lang/Byte; */
int v2 = 0; // const/4 v2, 0x0
/* .line 75 */
/* .local v2, "i":I */
/* array-length v3, p1 */
int v4 = 0; // const/4 v4, 0x0
} // :goto_0
/* if-ge v4, v3, :cond_1 */
/* aget-byte v5, p1, v4 */
/* .local v5, "b":B */
java.lang.Byte .valueOf ( v5 );
(( java.util.ArrayList ) v0 ).add ( v6 ); // invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
} // .end local v5 # "b":B
/* add-int/lit8 v4, v4, 0x1 */
/* .line 76 */
} // :cond_1
} // .end method
private listToByteArray ( java.util.ArrayList p0 ) {
/* .locals 7 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Byte;", */
/* ">;)[B" */
/* } */
} // .end annotation
/* .line 79 */
/* .local p1, "bytesPrim":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;" */
/* if-nez p1, :cond_0 */
/* .line 80 */
int v0 = 0; // const/4 v0, 0x0
/* .line 81 */
} // :cond_0
v0 = (( java.util.ArrayList ) p1 ).size ( ); // invoke-virtual {p1}, Ljava/util/ArrayList;->size()I
/* new-array v0, v0, [Ljava/lang/Byte; */
(( java.util.ArrayList ) p1 ).toArray ( v0 ); // invoke-virtual {p1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;
/* check-cast v0, [Ljava/lang/Byte; */
/* .line 82 */
/* .local v0, "bytes":[Ljava/lang/Byte; */
/* array-length v1, v0 */
/* new-array v1, v1, [B */
/* .line 83 */
/* .local v1, "result":[B */
int v2 = 0; // const/4 v2, 0x0
/* .line 84 */
/* .local v2, "i":I */
(( java.util.ArrayList ) p1 ).iterator ( ); // invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_1
/* check-cast v4, Ljava/lang/Byte; */
/* .local v4, "b":Ljava/lang/Byte; */
/* add-int/lit8 v5, v2, 0x1 */
} // .end local v2 # "i":I
/* .local v5, "i":I */
v6 = (( java.lang.Byte ) v4 ).byteValue ( ); // invoke-virtual {v4}, Ljava/lang/Byte;->byteValue()B
/* aput-byte v6, v1, v2 */
/* move v2, v5 */
} // .end local v4 # "b":Ljava/lang/Byte;
/* .line 85 */
} // .end local v5 # "i":I
/* .restart local v2 # "i":I */
} // :cond_1
} // .end method
private java.util.ArrayList loadFile ( com.android.server.location.mnlutils.MnlConfigUtils$MnlFileType p0 ) {
/* .locals 3 */
/* .param p1, "fileType" # Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;", */
/* ")", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Byte;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 30 */
try { // :try_start_0
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 31 */
/* .local v0, "input":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
v1 = (( com.android.server.location.mnlutils.MnlConfigUtils$MnlFileType ) p1 ).ordinal ( ); // invoke-virtual {p1}, Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;->ordinal()I
java.lang.Integer .valueOf ( v1 );
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 32 */
com.android.server.location.mnlutils.IEmdHidlUtils .getEmAidlService ( );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 33 */
com.android.server.location.mnlutils.IEmdHidlUtils .getEmAidlService ( );
this.aidlService = v1;
/* .line 34 */
(( java.util.ArrayList ) v0 ).stream ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->stream()Ljava/util/stream/Stream;
/* new-instance v2, Lcom/android/server/location/mnlutils/MnlConfigUtils$$ExternalSyntheticLambda0; */
/* invoke-direct {v2}, Lcom/android/server/location/mnlutils/MnlConfigUtils$$ExternalSyntheticLambda0;-><init>()V */
/* .line 35 */
/* .local v1, "array":[I */
v2 = this.aidlService;
/* invoke-direct {p0, v2}, Lcom/android/server/location/mnlutils/MnlConfigUtils;->arrayToByteList([B)Ljava/util/ArrayList; */
/* .line 37 */
} // .end local v1 # "array":[I
} // :cond_0
v1 = this.hidlService;
/* if-nez v1, :cond_1 */
/* .line 38 */
com.android.server.location.mnlutils.IEmdHidlUtils .getEmHidlService ( );
this.hidlService = v1;
/* .line 40 */
} // :cond_1
v1 = this.hidlService;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 42 */
} // .end local v0 # "input":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
/* :catch_0 */
/* move-exception v0 */
/* .line 43 */
/* .local v0, "e":Landroid/os/RemoteException; */
final String v1 = "Glp-MnlConfigUtils"; // const-string v1, "Glp-MnlConfigUtils"
final String v2 = "load mnl config fail"; // const-string v2, "load mnl config fail"
android.util.Log .d ( v1,v2 );
/* .line 44 */
(( android.os.RemoteException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V
/* .line 45 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
} // .end method
private Boolean saveFile ( java.util.ArrayList p0 ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Byte;", */
/* ">;)Z" */
/* } */
} // .end annotation
/* .line 50 */
/* .local p1, "dataToWrite":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;" */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 52 */
/* .local v0, "reserve":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
try { // :try_start_0
com.android.server.location.mnlutils.IEmdHidlUtils .getEmAidlService ( );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 53 */
com.android.server.location.mnlutils.IEmdHidlUtils .getEmAidlService ( );
this.aidlService = v1;
/* .line 54 */
(( java.util.ArrayList ) v0 ).stream ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->stream()Ljava/util/stream/Stream;
/* new-instance v2, Lcom/android/server/location/mnlutils/MnlConfigUtils$$ExternalSyntheticLambda0; */
/* invoke-direct {v2}, Lcom/android/server/location/mnlutils/MnlConfigUtils$$ExternalSyntheticLambda0;-><init>()V */
/* .line 55 */
/* .local v1, "array":[I */
v2 = this.aidlService;
v2 = /* invoke-direct {p0, p1}, Lcom/android/server/location/mnlutils/MnlConfigUtils;->listToByteArray(Ljava/util/ArrayList;)[B */
/* .line 57 */
} // .end local v1 # "array":[I
} // :cond_0
v1 = this.hidlService;
/* if-nez v1, :cond_1 */
/* .line 58 */
com.android.server.location.mnlutils.IEmdHidlUtils .getEmHidlService ( );
this.hidlService = v1;
/* .line 60 */
} // :cond_1
v1 = v1 = this.hidlService;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 62 */
/* :catch_0 */
/* move-exception v1 */
/* .line 63 */
/* .local v1, "e":Landroid/os/RemoteException; */
final String v2 = "Glp-MnlConfigUtils"; // const-string v2, "Glp-MnlConfigUtils"
final String v3 = "save mnl config fail"; // const-string v3, "save mnl config fail"
android.util.Log .d ( v2,v3 );
/* .line 64 */
(( android.os.RemoteException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V
/* .line 65 */
int v2 = 0; // const/4 v2, 0x0
} // .end method
/* # virtual methods */
public com.android.server.location.mnlutils.bean.MnlConfig getBackUpMnlConfig ( ) {
/* .locals 7 */
/* .line 107 */
v0 = com.android.server.location.mnlutils.MnlConfigUtils$MnlFileType.MNL_FILE_BACKUP;
/* invoke-direct {p0, v0}, Lcom/android/server/location/mnlutils/MnlConfigUtils;->loadFile(Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;)Ljava/util/ArrayList; */
/* .line 108 */
/* .local v0, "byteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;" */
v1 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* if-nez v1, :cond_0 */
/* .line 109 */
int v1 = 0; // const/4 v1, 0x0
/* .line 111 */
} // :cond_0
v1 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* new-array v1, v1, [B */
/* .line 112 */
/* .local v1, "bytes":[B */
int v2 = 0; // const/4 v2, 0x0
/* .line 113 */
/* .local v2, "index":I */
(( java.util.ArrayList ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_1
/* check-cast v4, Ljava/lang/Byte; */
/* .line 114 */
/* .local v4, "bt":Ljava/lang/Byte; */
/* add-int/lit8 v5, v2, 0x1 */
} // .end local v2 # "index":I
/* .local v5, "index":I */
v6 = (( java.lang.Byte ) v4 ).byteValue ( ); // invoke-virtual {v4}, Ljava/lang/Byte;->byteValue()B
/* aput-byte v6, v1, v2 */
/* .line 115 */
} // .end local v4 # "bt":Ljava/lang/Byte;
/* move v2, v5 */
/* .line 116 */
} // .end local v5 # "index":I
/* .restart local v2 # "index":I */
} // :cond_1
/* new-instance v3, Ljava/lang/String; */
/* invoke-direct {v3, v1}, Ljava/lang/String;-><init>([B)V */
com.android.server.location.mnlutils.MnlConfigParseUtils .parseXml ( v3 );
} // .end method
public com.android.server.location.mnlutils.bean.MnlConfig getMnlConfig ( ) {
/* .locals 7 */
/* .line 94 */
v0 = com.android.server.location.mnlutils.MnlConfigUtils$MnlFileType.MNL_FILE_CURRENT;
/* invoke-direct {p0, v0}, Lcom/android/server/location/mnlutils/MnlConfigUtils;->loadFile(Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;)Ljava/util/ArrayList; */
/* .line 95 */
/* .local v0, "byteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;" */
v1 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* if-nez v1, :cond_0 */
/* .line 96 */
int v1 = 0; // const/4 v1, 0x0
/* .line 98 */
} // :cond_0
v1 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* new-array v1, v1, [B */
/* .line 99 */
/* .local v1, "bytes":[B */
int v2 = 0; // const/4 v2, 0x0
/* .line 100 */
/* .local v2, "index":I */
(( java.util.ArrayList ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_1
/* check-cast v4, Ljava/lang/Byte; */
/* .line 101 */
/* .local v4, "bt":Ljava/lang/Byte; */
/* add-int/lit8 v5, v2, 0x1 */
} // .end local v2 # "index":I
/* .local v5, "index":I */
v6 = (( java.lang.Byte ) v4 ).byteValue ( ); // invoke-virtual {v4}, Ljava/lang/Byte;->byteValue()B
/* aput-byte v6, v1, v2 */
/* .line 102 */
} // .end local v4 # "bt":Ljava/lang/Byte;
/* move v2, v5 */
/* .line 103 */
} // .end local v5 # "index":I
/* .restart local v2 # "index":I */
} // :cond_1
/* new-instance v3, Ljava/lang/String; */
/* invoke-direct {v3, v1}, Ljava/lang/String;-><init>([B)V */
com.android.server.location.mnlutils.MnlConfigParseUtils .parseXml ( v3 );
} // .end method
public Boolean resetMnlConfig ( ) {
/* .locals 2 */
/* .line 141 */
v0 = com.android.server.location.mnlutils.MnlConfigUtils$MnlFileType.MNL_FILE_BACKUP;
/* invoke-direct {p0, v0}, Lcom/android/server/location/mnlutils/MnlConfigUtils;->loadFile(Lcom/android/server/location/mnlutils/MnlConfigUtils$MnlFileType;)Ljava/util/ArrayList; */
/* .line 142 */
/* .local v0, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;" */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 143 */
v1 = /* invoke-direct {p0, v0}, Lcom/android/server/location/mnlutils/MnlConfigUtils;->saveFile(Ljava/util/ArrayList;)Z */
/* .line 145 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Boolean saveMnlConfig ( com.android.server.location.mnlutils.bean.MnlConfig p0 ) {
/* .locals 7 */
/* .param p1, "mnlConfig" # Lcom/android/server/location/mnlutils/bean/MnlConfig; */
/* .line 126 */
(( com.android.server.location.mnlutils.bean.MnlConfig ) p1 ).toXmlString ( ); // invoke-virtual {p1}, Lcom/android/server/location/mnlutils/bean/MnlConfig;->toXmlString()Ljava/lang/String;
/* .line 127 */
/* .local v0, "s":Ljava/lang/String; */
v1 = java.nio.charset.StandardCharsets.UTF_8;
(( java.lang.String ) v0 ).getBytes ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B
/* .line 128 */
/* .local v1, "bytes":[B */
/* new-instance v2, Ljava/util/ArrayList; */
/* invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V */
/* .line 129 */
/* .local v2, "byteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;" */
/* array-length v3, v1 */
int v4 = 0; // const/4 v4, 0x0
} // :goto_0
/* if-ge v4, v3, :cond_0 */
/* aget-byte v5, v1, v4 */
/* .line 130 */
/* .local v5, "b":B */
java.lang.Byte .valueOf ( v5 );
(( java.util.ArrayList ) v2 ).add ( v6 ); // invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 129 */
} // .end local v5 # "b":B
/* add-int/lit8 v4, v4, 0x1 */
/* .line 132 */
} // :cond_0
v3 = /* invoke-direct {p0, v2}, Lcom/android/server/location/mnlutils/MnlConfigUtils;->saveFile(Ljava/util/ArrayList;)Z */
} // .end method
