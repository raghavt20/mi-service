public class com.android.server.location.GnssEventHandler$NotifyManager {
	 /* .source "GnssEventHandler.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/location/GnssEventHandler; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x1 */
/* name = "NotifyManager" */
} // .end annotation
/* # instance fields */
private final java.lang.String CHANNEL_ID;
private java.lang.CharSequence appName;
private android.app.Notification$Builder mBuilder;
private final android.content.BroadcastReceiver mCloseBlurLocationListener;
private android.app.NotificationManager mNotificationManager;
private Boolean mnoise;
private java.lang.String packageName;
final com.android.server.location.GnssEventHandler this$0; //synthetic
/* # direct methods */
static android.content.BroadcastReceiver -$$Nest$fgetmCloseBlurLocationListener ( com.android.server.location.GnssEventHandler$NotifyManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mCloseBlurLocationListener;
} // .end method
public com.android.server.location.GnssEventHandler$NotifyManager ( ) {
/* .locals 1 */
/* .param p1, "this$0" # Lcom/android/server/location/GnssEventHandler; */
/* .line 98 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 99 */
final String v0 = "GPS_STATUS_MONITOR_ID"; // const-string v0, "GPS_STATUS_MONITOR_ID"
this.CHANNEL_ID = v0;
/* .line 102 */
int v0 = 0; // const/4 v0, 0x0
this.packageName = v0;
/* .line 103 */
this.appName = v0;
/* .line 104 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->mnoise:Z */
/* .line 186 */
/* new-instance v0, Lcom/android/server/location/GnssEventHandler$NotifyManager$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/location/GnssEventHandler$NotifyManager$1;-><init>(Lcom/android/server/location/GnssEventHandler$NotifyManager;)V */
this.mCloseBlurLocationListener = v0;
return;
} // .end method
private void constructNotification ( ) {
/* .locals 14 */
/* .line 113 */
v0 = this.this$0;
com.android.server.location.GnssEventHandler .-$$Nest$fgetmAppContext ( v0 );
/* const v1, 0x110f02bd */
(( android.content.Context ) v0 ).getString ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;
/* .line 114 */
/* .local v0, "description":Ljava/lang/String; */
/* new-instance v1, Landroid/content/Intent; */
/* invoke-direct {v1}, Landroid/content/Intent;-><init>()V */
/* .line 115 */
/* .local v1, "intent":Landroid/content/Intent; */
final String v2 = "com.android.settings"; // const-string v2, "com.android.settings"
final String v3 = "com.android.settings.Settings$LocationSettingsActivity"; // const-string v3, "com.android.settings.Settings$LocationSettingsActivity"
(( android.content.Intent ) v1 ).setClassName ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 116 */
/* const/high16 v2, 0x10000000 */
(( android.content.Intent ) v1 ).addFlags ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 117 */
v2 = this.this$0;
com.android.server.location.GnssEventHandler .-$$Nest$fgetmAppContext ( v2 );
/* const/high16 v3, 0xc000000 */
int v4 = 0; // const/4 v4, 0x0
android.app.PendingIntent .getActivity ( v2,v4,v1,v3 );
/* .line 119 */
/* .local v2, "pendingIntent":Landroid/app/PendingIntent; */
/* new-instance v3, Landroid/app/NotificationChannel; */
final String v5 = "GPS_STATUS_MONITOR_ID"; // const-string v5, "GPS_STATUS_MONITOR_ID"
int v6 = 2; // const/4 v6, 0x2
/* invoke-direct {v3, v5, v0, v6}, Landroid/app/NotificationChannel;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V */
/* .line 121 */
/* .local v3, "channel":Landroid/app/NotificationChannel; */
int v5 = 1; // const/4 v5, 0x1
(( android.app.NotificationChannel ) v3 ).setLockscreenVisibility ( v5 ); // invoke-virtual {v3, v5}, Landroid/app/NotificationChannel;->setLockscreenVisibility(I)V
/* .line 122 */
int v7 = 0; // const/4 v7, 0x0
(( android.app.NotificationChannel ) v3 ).setSound ( v7, v7 ); // invoke-virtual {v3, v7, v7}, Landroid/app/NotificationChannel;->setSound(Landroid/net/Uri;Landroid/media/AudioAttributes;)V
/* .line 123 */
(( android.app.NotificationChannel ) v3 ).enableVibration ( v4 ); // invoke-virtual {v3, v4}, Landroid/app/NotificationChannel;->enableVibration(Z)V
/* .line 124 */
/* new-array v7, v5, [J */
/* const-wide/16 v8, 0x0 */
/* aput-wide v8, v7, v4 */
(( android.app.NotificationChannel ) v3 ).setVibrationPattern ( v7 ); // invoke-virtual {v3, v7}, Landroid/app/NotificationChannel;->setVibrationPattern([J)V
/* .line 125 */
v7 = this.mBuilder;
/* const v8, 0x110801b9 */
(( android.app.Notification$Builder ) v7 ).setSmallIcon ( v8 ); // invoke-virtual {v7, v8}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;
/* .line 127 */
try { // :try_start_0
	 v7 = this.mNotificationManager;
	 (( android.app.NotificationManager ) v7 ).createNotificationChannel ( v3 ); // invoke-virtual {v7, v3}, Landroid/app/NotificationManager;->createNotificationChannel(Landroid/app/NotificationChannel;)V
	 /* .line 128 */
	 /* iget-boolean v7, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->mnoise:Z */
	 /* if-ne v7, v5, :cond_0 */
	 /* .line 129 */
	 v4 = this.mBuilder;
	 v6 = this.this$0;
	 com.android.server.location.GnssEventHandler .-$$Nest$fgetmAppContext ( v6 );
	 /* const v7, 0x110f02c1 */
	 (( android.content.Context ) v6 ).getString ( v7 ); // invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;
	 (( android.app.Notification$Builder ) v4 ).setContentTitle ( v6 ); // invoke-virtual {v4, v6}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;
	 v6 = this.this$0;
	 com.android.server.location.GnssEventHandler .-$$Nest$fgetmAppContext ( v6 );
	 /* .line 131 */
	 /* const v7, 0x110f02b7 */
	 (( android.content.Context ) v6 ).getString ( v7 ); // invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;
	 (( android.app.Notification$Builder ) v4 ).setContentText ( v6 ); // invoke-virtual {v4, v6}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;
	 /* .line 132 */
	 (( android.app.Notification$Builder ) v4 ).setContentIntent ( v2 ); // invoke-virtual {v4, v2}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;
	 /* .line 133 */
	 (( android.app.Notification$Builder ) v4 ).setOngoing ( v5 ); // invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;
	 /* .line 134 */
	 (( android.app.Notification$Builder ) v4 ).setAutoCancel ( v5 ); // invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;
	 /* goto/16 :goto_1 */
	 /* .line 135 */
} // :cond_0
v7 = this.this$0;
v7 = com.android.server.location.GnssEventHandler .-$$Nest$fgetmBlur ( v7 );
/* if-ne v7, v5, :cond_2 */
/* .line 136 */
v7 = this.this$0;
com.android.server.location.GnssEventHandler .-$$Nest$fgetmAppContext ( v7 );
(( android.content.Context ) v7 ).getPackageManager ( ); // invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_1 */
/* .line 138 */
/* .local v7, "pm":Landroid/content/pm/PackageManager; */
try { // :try_start_1
	 v8 = this.packageName;
	 (( android.content.pm.PackageManager ) v7 ).getApplicationInfo ( v8, v4 ); // invoke-virtual {v7, v8, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
	 /* .line 139 */
	 /* .local v8, "ai":Landroid/content/pm/ApplicationInfo; */
	 (( android.content.pm.ApplicationInfo ) v8 ).loadLabel ( v7 ); // invoke-virtual {v8, v7}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
	 this.appName = v9;
	 /* :try_end_1 */
	 /* .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 ..:try_end_1} :catch_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_1 */
	 /* .line 142 */
} // .end local v8 # "ai":Landroid/content/pm/ApplicationInfo;
/* .line 140 */
/* :catch_0 */
/* move-exception v8 */
/* .line 141 */
/* .local v8, "e":Landroid/content/pm/PackageManager$NameNotFoundException; */
try { // :try_start_2
	 final String v9 = "GnssNps"; // const-string v9, "GnssNps"
	 final String v10 = "No such package for this name!"; // const-string v10, "No such package for this name!"
	 android.util.Log .w ( v9,v10 );
	 /* .line 143 */
} // .end local v8 # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
} // :goto_0
v8 = this.appName;
/* if-nez v8, :cond_1 */
/* .line 144 */
final String v8 = "gps server"; // const-string v8, "gps server"
this.appName = v8;
/* .line 146 */
} // :cond_1
v8 = this.this$0;
com.android.server.location.GnssEventHandler .-$$Nest$fgetmAppContext ( v8 );
/* new-instance v9, Landroid/content/Intent; */
com.android.server.location.GnssEventHandler .-$$Nest$sfgetCLOSE_BLUR_LOCATION ( );
/* invoke-direct {v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* const/high16 v10, 0x4000000 */
android.app.PendingIntent .getBroadcast ( v8,v4,v9,v10 );
/* .line 148 */
/* .local v8, "blurLocationPendingIntent":Landroid/app/PendingIntent; */
/* new-instance v9, Landroid/os/Bundle; */
/* invoke-direct {v9}, Landroid/os/Bundle;-><init>()V */
/* .line 149 */
/* .local v9, "bundle":Landroid/os/Bundle; */
final String v10 = "miui.showAction"; // const-string v10, "miui.showAction"
(( android.os.Bundle ) v9 ).putBoolean ( v10, v5 ); // invoke-virtual {v9, v10, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
/* .line 150 */
v10 = this.this$0;
/* new-instance v11, Landroid/app/Notification$Action; */
v12 = this.this$0;
com.android.server.location.GnssEventHandler .-$$Nest$fgetmAppContext ( v12 );
/* .line 151 */
/* const v13, 0x110f02bb */
(( android.content.Context ) v12 ).getString ( v13 ); // invoke-virtual {v12, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;
/* const v13, 0x11080198 # 1.0729E-28f */
/* invoke-direct {v11, v13, v12, v8}, Landroid/app/Notification$Action;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V */
com.android.server.location.GnssEventHandler .-$$Nest$fputmAction ( v10,v11 );
/* .line 153 */
v10 = this.mBuilder;
v11 = this.appName;
(( android.app.Notification$Builder ) v10 ).setContentTitle ( v11 ); // invoke-virtual {v10, v11}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;
/* .line 155 */
(( android.app.Notification$Builder ) v10 ).setPriority ( v6 ); // invoke-virtual {v10, v6}, Landroid/app/Notification$Builder;->setPriority(I)Landroid/app/Notification$Builder;
v10 = this.this$0;
com.android.server.location.GnssEventHandler .-$$Nest$fgetmAppContext ( v10 );
/* .line 156 */
/* const v11, 0x110f02ba */
(( android.content.Context ) v10 ).getString ( v11 ); // invoke-virtual {v10, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;
(( android.app.Notification$Builder ) v6 ).setContentText ( v10 ); // invoke-virtual {v6, v10}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;
/* new-array v10, v5, [Landroid/app/Notification$Action; */
v11 = this.this$0;
com.android.server.location.GnssEventHandler .-$$Nest$fgetmAction ( v11 );
/* aput-object v11, v10, v4 */
/* .line 157 */
(( android.app.Notification$Builder ) v6 ).setActions ( v10 ); // invoke-virtual {v6, v10}, Landroid/app/Notification$Builder;->setActions([Landroid/app/Notification$Action;)Landroid/app/Notification$Builder;
/* .line 158 */
(( android.app.Notification$Builder ) v4 ).setExtras ( v9 ); // invoke-virtual {v4, v9}, Landroid/app/Notification$Builder;->setExtras(Landroid/os/Bundle;)Landroid/app/Notification$Builder;
/* .line 159 */
(( android.app.Notification$Builder ) v4 ).setAutoCancel ( v5 ); // invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;
/* .line 160 */
v4 = this.this$0;
com.android.server.location.GnssEventHandler .-$$Nest$fgetmAppContext ( v4 );
v5 = this.mCloseBlurLocationListener;
/* new-instance v6, Landroid/content/IntentFilter; */
com.android.server.location.GnssEventHandler .-$$Nest$sfgetCLOSE_BLUR_LOCATION ( );
/* invoke-direct {v6, v10}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V */
(( android.content.Context ) v4 ).registerReceiver ( v5, v6 ); // invoke-virtual {v4, v5, v6}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_1 */
/* .line 165 */
} // .end local v7 # "pm":Landroid/content/pm/PackageManager;
} // .end local v8 # "blurLocationPendingIntent":Landroid/app/PendingIntent;
} // .end local v9 # "bundle":Landroid/os/Bundle;
} // :cond_2
} // :goto_1
/* .line 163 */
/* :catch_1 */
/* move-exception v4 */
/* .line 164 */
/* .local v4, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v4 ).printStackTrace ( ); // invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V
/* .line 166 */
} // .end local v4 # "e":Ljava/lang/Exception;
} // :goto_2
return;
} // .end method
private void unregisterBlurLocationListener ( ) {
/* .locals 5 */
/* .line 229 */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 230 */
/* .local v0, "queryIntent":Landroid/content/Intent; */
com.android.server.location.GnssEventHandler .-$$Nest$sfgetCLOSE_BLUR_LOCATION ( );
(( android.content.Intent ) v0 ).setAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
/* .line 231 */
v1 = this.this$0;
com.android.server.location.GnssEventHandler .-$$Nest$fgetmAppContext ( v1 );
(( android.content.Context ) v1 ).getPackageManager ( ); // invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 232 */
/* .local v1, "pm":Landroid/content/pm/PackageManager; */
int v2 = 0; // const/4 v2, 0x0
(( android.content.pm.PackageManager ) v1 ).queryBroadcastReceivers ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->queryBroadcastReceivers(Landroid/content/Intent;I)Ljava/util/List;
/* .line 233 */
/* .local v2, "resolveInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;" */
v3 = if ( v2 != null) { // if-eqz v2, :cond_0
/* if-nez v3, :cond_0 */
/* .line 234 */
v3 = this.this$0;
com.android.server.location.GnssEventHandler .-$$Nest$fgetmAppContext ( v3 );
v4 = this.mCloseBlurLocationListener;
(( android.content.Context ) v3 ).unregisterReceiver ( v4 ); // invoke-virtual {v3, v4}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
/* .line 236 */
} // :cond_0
return;
} // .end method
/* # virtual methods */
public void initNotification ( ) {
/* .locals 3 */
/* .line 107 */
v0 = this.this$0;
com.android.server.location.GnssEventHandler .-$$Nest$fgetmAppContext ( v0 );
final String v1 = "notification"; // const-string v1, "notification"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/NotificationManager; */
this.mNotificationManager = v0;
/* .line 108 */
/* new-instance v0, Landroid/app/Notification$Builder; */
v1 = this.this$0;
com.android.server.location.GnssEventHandler .-$$Nest$fgetmAppContext ( v1 );
final String v2 = "GPS_STATUS_MONITOR_ID"; // const-string v2, "GPS_STATUS_MONITOR_ID"
/* invoke-direct {v0, v1, v2}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;)V */
this.mBuilder = v0;
/* .line 109 */
/* invoke-direct {p0}, Lcom/android/server/location/GnssEventHandler$NotifyManager;->constructNotification()V */
/* .line 110 */
return;
} // .end method
public void removeNotification ( ) {
/* .locals 5 */
/* .line 239 */
final String v0 = "removeNotification"; // const-string v0, "removeNotification"
final String v1 = "GnssNps"; // const-string v1, "GnssNps"
android.util.Log .d ( v1,v0 );
/* .line 240 */
v0 = this.mNotificationManager;
/* if-nez v0, :cond_0 */
/* .line 241 */
final String v0 = "mNotificationManager is null"; // const-string v0, "mNotificationManager is null"
android.util.Log .d ( v1,v0 );
/* .line 242 */
(( com.android.server.location.GnssEventHandler$NotifyManager ) p0 ).initNotification ( ); // invoke-virtual {p0}, Lcom/android/server/location/GnssEventHandler$NotifyManager;->initNotification()V
/* .line 245 */
} // :cond_0
try { // :try_start_0
v0 = this.mNotificationManager;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 247 */
/* const v2, 0x110801b9 */
/* .line 248 */
/* .local v2, "id":I */
v3 = android.os.UserHandle.ALL;
int v4 = 0; // const/4 v4, 0x0
(( android.app.NotificationManager ) v0 ).cancelAsUser ( v4, v2, v3 ); // invoke-virtual {v0, v4, v2, v3}, Landroid/app/NotificationManager;->cancelAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)V
/* .line 249 */
/* invoke-direct {p0}, Lcom/android/server/location/GnssEventHandler$NotifyManager;->unregisterBlurLocationListener()V */
/* .line 250 */
this.mBuilder = v4;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 254 */
} // .end local v2 # "id":I
} // :cond_1
/* .line 252 */
/* :catch_0 */
/* move-exception v0 */
/* .line 253 */
/* .local v0, "e":Ljava/lang/Exception; */
android.util.Log .getStackTraceString ( v0 );
android.util.Log .e ( v1,v2 );
/* .line 255 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public void showNotification ( ) {
/* .locals 5 */
/* .line 169 */
v0 = this.this$0;
v0 = com.android.server.location.GnssEventHandler .-$$Nest$misChineseLanguage ( v0 );
final String v1 = "GnssNps"; // const-string v1, "GnssNps"
/* if-nez v0, :cond_0 */
/* .line 170 */
/* const-string/jumbo v0, "show notification only in CHINESE language" */
android.util.Log .d ( v1,v0 );
/* .line 171 */
return;
/* .line 173 */
} // :cond_0
v0 = this.packageName;
/* if-nez v0, :cond_1 */
/* .line 174 */
final String v0 = "no package name presence"; // const-string v0, "no package name presence"
android.util.Log .d ( v1,v0 );
/* .line 175 */
return;
/* .line 179 */
} // :cond_1
/* const v0, 0x110801b9 */
/* .line 180 */
/* .local v0, "id":I */
try { // :try_start_0
v1 = this.mNotificationManager;
v2 = this.mBuilder;
(( android.app.Notification$Builder ) v2 ).build ( ); // invoke-virtual {v2}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;
v3 = android.os.UserHandle.ALL;
int v4 = 0; // const/4 v4, 0x0
(( android.app.NotificationManager ) v1 ).notifyAsUser ( v4, v0, v2, v3 ); // invoke-virtual {v1, v4, v0, v2, v3}, Landroid/app/NotificationManager;->notifyAsUser(Ljava/lang/String;ILandroid/app/Notification;Landroid/os/UserHandle;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 183 */
} // .end local v0 # "id":I
/* .line 181 */
/* :catch_0 */
/* move-exception v0 */
/* .line 182 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 184 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public void updateCallerName ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "name" # Ljava/lang/String; */
/* .line 208 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "updateCallerName=" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "GnssNps"; // const-string v1, "GnssNps"
android.util.Log .d ( v1,v0 );
/* .line 209 */
this.packageName = p1;
/* .line 210 */
final String v0 = "noise_environment"; // const-string v0, "noise_environment"
v0 = (( java.lang.String ) p1 ).contains ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->mnoise:Z */
/* if-nez v0, :cond_0 */
/* .line 211 */
/* iput-boolean v1, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->mnoise:Z */
/* .line 212 */
v0 = this.this$0;
com.android.server.location.GnssEventHandler .-$$Nest$fgetmNotifyManager ( v0 );
(( com.android.server.location.GnssEventHandler$NotifyManager ) v0 ).initNotification ( ); // invoke-virtual {v0}, Lcom/android/server/location/GnssEventHandler$NotifyManager;->initNotification()V
/* .line 213 */
v0 = this.this$0;
com.android.server.location.GnssEventHandler .-$$Nest$fgetmNotifyManager ( v0 );
(( com.android.server.location.GnssEventHandler$NotifyManager ) v0 ).showNotification ( ); // invoke-virtual {v0}, Lcom/android/server/location/GnssEventHandler$NotifyManager;->showNotification()V
/* .line 214 */
} // :cond_0
final String v0 = "normal_environment"; // const-string v0, "normal_environment"
v0 = (( java.lang.String ) p1 ).contains ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
int v2 = 0; // const/4 v2, 0x0
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget-boolean v0, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->mnoise:Z */
/* if-ne v0, v1, :cond_1 */
/* .line 215 */
/* iput-boolean v2, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->mnoise:Z */
/* .line 216 */
v0 = this.this$0;
com.android.server.location.GnssEventHandler .-$$Nest$fgetmNotifyManager ( v0 );
(( com.android.server.location.GnssEventHandler$NotifyManager ) v0 ).removeNotification ( ); // invoke-virtual {v0}, Lcom/android/server/location/GnssEventHandler$NotifyManager;->removeNotification()V
/* .line 217 */
} // :cond_1
final String v0 = "blurLocation_notify is on"; // const-string v0, "blurLocation_notify is on"
v0 = (( java.lang.String ) p1 ).contains ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 218 */
v0 = this.this$0;
com.android.server.location.GnssEventHandler .-$$Nest$fputmBlur ( v0,v1 );
/* .line 219 */
v0 = this.this$0;
com.android.server.location.GnssEventHandler .-$$Nest$fgetpkgName ( v0 );
this.packageName = v0;
/* .line 220 */
v0 = this.this$0;
com.android.server.location.GnssEventHandler .-$$Nest$fgetmNotifyManager ( v0 );
(( com.android.server.location.GnssEventHandler$NotifyManager ) v0 ).initNotification ( ); // invoke-virtual {v0}, Lcom/android/server/location/GnssEventHandler$NotifyManager;->initNotification()V
/* .line 221 */
v0 = this.this$0;
com.android.server.location.GnssEventHandler .-$$Nest$fgetmNotifyManager ( v0 );
(( com.android.server.location.GnssEventHandler$NotifyManager ) v0 ).showNotification ( ); // invoke-virtual {v0}, Lcom/android/server/location/GnssEventHandler$NotifyManager;->showNotification()V
/* .line 222 */
} // :cond_2
final String v0 = "blurLocation_notify is off"; // const-string v0, "blurLocation_notify is off"
v0 = (( java.lang.String ) p1 ).contains ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 223 */
v0 = this.this$0;
com.android.server.location.GnssEventHandler .-$$Nest$fputmBlur ( v0,v2 );
/* .line 224 */
v0 = this.this$0;
com.android.server.location.GnssEventHandler .-$$Nest$fgetmNotifyManager ( v0 );
(( com.android.server.location.GnssEventHandler$NotifyManager ) v0 ).removeNotification ( ); // invoke-virtual {v0}, Lcom/android/server/location/GnssEventHandler$NotifyManager;->removeNotification()V
/* .line 226 */
} // :cond_3
} // :goto_0
return;
} // .end method
