.class public Lcom/android/server/location/GnssCollectDataImpl;
.super Ljava/lang/Object;
.source "GnssCollectDataImpl.java"

# interfaces
.implements Lcom/android/server/location/gnss/GnssCollectDataStub;


# static fields
.field private static final ACTION_COLLECT_DATA:Ljava/lang/String; = "action collect data"

.field private static final BO_DEF_VAL:Ljava/lang/String; = "-1"

.field private static final CLOUDGPOKEY:Ljava/lang/String; = "GpoVersion"

.field private static final CLOUDGSRKEY:Ljava/lang/String; = "enableGSR"

.field private static final CLOUDKEY:Ljava/lang/String; = "enabled"

.field private static final CLOUDKEYSUPL:Ljava/lang/String; = "enableCaict"

.field private static final CLOUDMODULE:Ljava/lang/String; = "bigdata"

.field private static final CLOUDSDKKEY:Ljava/lang/String; = "SDK_source"

.field private static final CLOUD_MODULE_NLP:Ljava/lang/String; = "nlp"

.field private static final CLOUD_MODULE_RTK:Ljava/lang/String; = "gnssRtk"

.field private static final CN_MCC:Ljava/lang/String; = "460"

.field private static final COLLECT_DATA_PATH:Ljava/lang/String; = "/data/mqsas/gps/gps-strength"

.field private static final DEBUG:Z = false

.field private static final DEF_VAL_GSCO:Ljava/lang/String; = "-1"

.field private static final GMO_DEF_VAL:Ljava/lang/String; = "-1"

.field private static final GMO_DISABLE:Ljava/lang/String; = "0"

.field private static final GMO_ENABLE:Ljava/lang/String; = "1"

.field private static final GNSS_GSR_SWITCH:Ljava/lang/String; = "persist.sys.gps.selfRecovery"

.field private static final GNSS_MQS_SWITCH:Ljava/lang/String; = "persist.sys.mqs.gps"

.field public static final INFO_B1CN0_TOP4:I = 0x9

.field public static final INFO_E1CN0_TOP4:I = 0xb

.field public static final INFO_G1CN0_TOP4:I = 0xa

.field public static final INFO_L1CN0_TOP4:I = 0x7

.field public static final INFO_L5CN0_TOP4:I = 0x8

.field public static final INFO_NMEA_PQWP6:I = 0x6

.field private static final IS_COLLECT:Ljava/lang/String; = "1"

.field private static final IS_STABLE_VERSION:Z

.field private static final KEY_CLOUD_BO:Ljava/lang/String; = "bo_status"

.field private static final KEY_CLOUD_GMO:Ljava/lang/String; = "gmo_status"

.field private static final KEY_CLOUD_GSCO_PKG:Ljava/lang/String; = "gsco_status_pkg"

.field private static final KEY_CLOUD_GSCO_STATUS:Ljava/lang/String; = "gsco_status"

.field private static final KEY_DISABLE:Ljava/lang/String; = "0"

.field private static final KEY_ENABLE:Ljava/lang/String; = "1"

.field private static final RTK_SWITCH:Ljava/lang/String; = "persist.sys.mqs.gps.rtk"

.field public static final SIGNAL_ERROR_CODE:I = 0x36c725c9

.field public static final STATE_FIX:I = 0x2

.field public static final STATE_INIT:I = 0x0

.field public static final STATE_LOSE:I = 0x4

.field public static final STATE_SAVE:I = 0x5

.field public static final STATE_START:I = 0x1

.field public static final STATE_STOP:I = 0x3

.field public static final STATE_UNKNOWN:I = 0x64

.field private static final SUPL_SWITCH:Ljava/lang/String; = "persist.sys.mqs.gps.supl"

.field public static final SV_ERROR_CODE:I = 0x36c70305

.field private static final TAG:Ljava/lang/String; = "GnssCD"

.field public static mCurrentState:I

.field private static mMqsGpsModuleId:Ljava/lang/String;


# instance fields
.field private AmapBackCount:I

.field private BaiduBackCount:I

.field private SV_ERROR_SEND_INTERVAL:I

.field private TencentBackCount:I

.field private UPLOAD_REPEAT_TIME:I

.field private hasStartUploadData:Z

.field private mContext:Landroid/content/Context;

.field private mGnssCollectDataDbDao:Lcom/android/server/location/GnssCollectDataDbDao;

.field private mHandler:Landroid/os/Handler;

.field private mHandlerThread:Landroid/os/HandlerThread;

.field private mIsCnSim:Z

.field private mJsonArray:Lorg/json/JSONArray;

.field private mLastSvTime:J

.field private mMqsEventManagerDelegate:Lmiui/mqsas/sdk/MQSEventManagerDelegate;

.field private mNumBEIDOUUsedInFix:I

.field private mNumGALILEOUsedInFix:I

.field private mNumGLONASSUsedInFix:I

.field private mNumGPSUsedInFix:I

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mSessionInfo:Lcom/android/server/location/GnssSessionInfo;

.field private mSvUsedInFix:I


# direct methods
.method static bridge synthetic -$$Nest$mparseNmea(Lcom/android/server/location/GnssCollectDataImpl;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/location/GnssCollectDataImpl;->parseNmea(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msaveFixStatus(Lcom/android/server/location/GnssCollectDataImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/location/GnssCollectDataImpl;->saveFixStatus()V

    return-void
.end method

.method static bridge synthetic -$$Nest$msaveLog(Lcom/android/server/location/GnssCollectDataImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/location/GnssCollectDataImpl;->saveLog()V

    return-void
.end method

.method static bridge synthetic -$$Nest$msaveLoseStatus(Lcom/android/server/location/GnssCollectDataImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/location/GnssCollectDataImpl;->saveLoseStatus()V

    return-void
.end method

.method static bridge synthetic -$$Nest$msaveStartStatus(Lcom/android/server/location/GnssCollectDataImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/location/GnssCollectDataImpl;->saveStartStatus()V

    return-void
.end method

.method static bridge synthetic -$$Nest$msaveState(Lcom/android/server/location/GnssCollectDataImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/location/GnssCollectDataImpl;->saveState()V

    return-void
.end method

.method static bridge synthetic -$$Nest$msaveStopStatus(Lcom/android/server/location/GnssCollectDataImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/location/GnssCollectDataImpl;->saveStopStatus()V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetB1Cn0(Lcom/android/server/location/GnssCollectDataImpl;D)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/location/GnssCollectDataImpl;->setB1Cn0(D)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetCurrentState(Lcom/android/server/location/GnssCollectDataImpl;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/location/GnssCollectDataImpl;->setCurrentState(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetL1Cn0(Lcom/android/server/location/GnssCollectDataImpl;D)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/location/GnssCollectDataImpl;->setL1Cn0(D)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetL5Cn0(Lcom/android/server/location/GnssCollectDataImpl;D)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/location/GnssCollectDataImpl;->setL5Cn0(D)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mstartUploadFixData(Lcom/android/server/location/GnssCollectDataImpl;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/location/GnssCollectDataImpl;->startUploadFixData(Landroid/content/Context;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateControlState(Lcom/android/server/location/GnssCollectDataImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/location/GnssCollectDataImpl;->updateControlState()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 103
    const/16 v0, 0x64

    sput v0, Lcom/android/server/location/GnssCollectDataImpl;->mCurrentState:I

    .line 105
    const-string v0, "mqs_gps_data_63921000"

    sput-object v0, Lcom/android/server/location/GnssCollectDataImpl;->mMqsGpsModuleId:Ljava/lang/String;

    .line 106
    sget-boolean v0, Lmiui/os/Build;->IS_STABLE_VERSION:Z

    sput-boolean v0, Lcom/android/server/location/GnssCollectDataImpl;->IS_STABLE_VERSION:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    invoke-static {}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->getInstance()Lmiui/mqsas/sdk/MQSEventManagerDelegate;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mMqsEventManagerDelegate:Lmiui/mqsas/sdk/MQSEventManagerDelegate;

    .line 112
    new-instance v0, Lcom/android/server/location/GnssSessionInfo;

    invoke-direct {v0}, Lcom/android/server/location/GnssSessionInfo;-><init>()V

    iput-object v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mSessionInfo:Lcom/android/server/location/GnssSessionInfo;

    .line 113
    const v0, 0x927c0

    iput v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->SV_ERROR_SEND_INTERVAL:I

    .line 114
    const v0, 0x5265c00

    iput v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->UPLOAD_REPEAT_TIME:I

    .line 115
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    iput-object v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mJsonArray:Lorg/json/JSONArray;

    .line 116
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mIsCnSim:Z

    .line 118
    const-wide/32 v1, -0x927c0

    iput-wide v1, p0, Lcom/android/server/location/GnssCollectDataImpl;->mLastSvTime:J

    .line 119
    iput v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mSvUsedInFix:I

    .line 120
    iput v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mNumGPSUsedInFix:I

    .line 121
    iput v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mNumBEIDOUUsedInFix:I

    .line 122
    iput v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mNumGLONASSUsedInFix:I

    .line 123
    iput v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mNumGALILEOUsedInFix:I

    .line 124
    iput v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->AmapBackCount:I

    .line 125
    iput v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->BaiduBackCount:I

    .line 126
    iput v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->TencentBackCount:I

    .line 895
    new-instance v0, Lcom/android/server/location/GnssCollectDataImpl$3;

    invoke-direct {v0, p0}, Lcom/android/server/location/GnssCollectDataImpl$3;-><init>(Lcom/android/server/location/GnssCollectDataImpl;)V

    iput-object v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private allowCollect()Z
    .locals 2

    .line 135
    const-string v0, "persist.sys.mqs.gps"

    const-string v1, "1"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private isL1Sv(F)Z
    .locals 4
    .param p1, "carrierFreq"    # F

    .line 422
    float-to-double v0, p1

    const-wide v2, 0x41d73b1cf0000000L    # 1.559E9

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_0

    float-to-double v0, p1

    const-wide v2, 0x41d7fda9a0000000L    # 1.61E9

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isL5Sv(F)Z
    .locals 4
    .param p1, "carrierFreq"    # F

    .line 427
    float-to-double v0, p1

    const-wide v2, 0x41d1584ec0000000L    # 1.164E9

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_0

    float-to-double v0, p1

    const-wide v2, 0x41d1b7acd0000000L    # 1.189E9

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private packToJsonArray()Ljava/lang/String;
    .locals 7

    .line 163
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 165
    .local v0, "jsonObj":Lorg/json/JSONObject;
    iget-object v1, p0, Lcom/android/server/location/GnssCollectDataImpl;->mSessionInfo:Lcom/android/server/location/GnssSessionInfo;

    invoke-virtual {v1}, Lcom/android/server/location/GnssSessionInfo;->getTtff()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    const-string v2, "GnssCD"

    if-gez v1, :cond_0

    .line 166
    const-string v1, "abnormal data"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    const/4 v1, 0x0

    return-object v1

    .line 169
    :cond_0
    iget-object v1, p0, Lcom/android/server/location/GnssCollectDataImpl;->mSessionInfo:Lcom/android/server/location/GnssSessionInfo;

    invoke-virtual {v1}, Lcom/android/server/location/GnssSessionInfo;->getL1Top4Cn0Mean()D

    move-result-wide v3

    const-wide/high16 v5, 0x403c000000000000L    # 28.0

    cmpg-double v1, v3, v5

    if-gez v1, :cond_1

    .line 170
    const-string v1, "SignalWeak"

    const-string v3, "Signal weak"

    filled-new-array {v1, v3}, [Ljava/lang/Object;

    move-result-object v1

    const v3, 0x36c725c9

    invoke-static {v3, v1}, Lcom/miui/misight/MiSight;->constructEvent(I[Ljava/lang/Object;)Lcom/miui/misight/MiEvent;

    move-result-object v1

    .line 172
    .local v1, "event":Lcom/miui/misight/MiEvent;
    invoke-static {v1}, Lcom/miui/misight/MiSight;->sendEvent(Lcom/miui/misight/MiEvent;)V

    .line 175
    .end local v1    # "event":Lcom/miui/misight/MiEvent;
    :cond_1
    :try_start_0
    const-string/jumbo v1, "startTime"

    iget-object v3, p0, Lcom/android/server/location/GnssCollectDataImpl;->mSessionInfo:Lcom/android/server/location/GnssSessionInfo;

    invoke-virtual {v3}, Lcom/android/server/location/GnssSessionInfo;->getStartTimeInHour()I

    move-result v3

    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 176
    const-string v1, "TTFF"

    iget-object v3, p0, Lcom/android/server/location/GnssCollectDataImpl;->mSessionInfo:Lcom/android/server/location/GnssSessionInfo;

    invoke-virtual {v3}, Lcom/android/server/location/GnssSessionInfo;->getTtff()J

    move-result-wide v3

    invoke-virtual {v0, v1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 177
    const-string v1, "runTime"

    iget-object v3, p0, Lcom/android/server/location/GnssCollectDataImpl;->mSessionInfo:Lcom/android/server/location/GnssSessionInfo;

    invoke-virtual {v3}, Lcom/android/server/location/GnssSessionInfo;->getRunTime()J

    move-result-wide v3

    invoke-virtual {v0, v1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 178
    const-string v1, "loseTimes"

    iget-object v3, p0, Lcom/android/server/location/GnssCollectDataImpl;->mSessionInfo:Lcom/android/server/location/GnssSessionInfo;

    invoke-virtual {v3}, Lcom/android/server/location/GnssSessionInfo;->getLoseTimes()I

    move-result v3

    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 179
    const-string v1, "L1Top4MeanCn0"

    iget-object v3, p0, Lcom/android/server/location/GnssCollectDataImpl;->mSessionInfo:Lcom/android/server/location/GnssSessionInfo;

    invoke-virtual {v3}, Lcom/android/server/location/GnssSessionInfo;->getL1Top4Cn0Mean()D

    move-result-wide v3

    invoke-virtual {v0, v1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 180
    const-string v1, "L5Top4MeanCn0"

    iget-object v3, p0, Lcom/android/server/location/GnssCollectDataImpl;->mSessionInfo:Lcom/android/server/location/GnssSessionInfo;

    invoke-virtual {v3}, Lcom/android/server/location/GnssSessionInfo;->getL5Top4Cn0Mean()D

    move-result-wide v3

    invoke-virtual {v0, v1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 181
    const-string v1, "B1Top4MeanCn0"

    iget-object v3, p0, Lcom/android/server/location/GnssCollectDataImpl;->mSessionInfo:Lcom/android/server/location/GnssSessionInfo;

    invoke-virtual {v3}, Lcom/android/server/location/GnssSessionInfo;->getB1Top4Cn0Mean()D

    move-result-wide v3

    invoke-virtual {v0, v1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 182
    const-string v1, "G1Top4MeanCn0"

    iget-object v3, p0, Lcom/android/server/location/GnssCollectDataImpl;->mSessionInfo:Lcom/android/server/location/GnssSessionInfo;

    invoke-virtual {v3}, Lcom/android/server/location/GnssSessionInfo;->getG1Top4Cn0Mean()D

    move-result-wide v3

    invoke-virtual {v0, v1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 183
    const-string v1, "E1Top4MeanCn0"

    iget-object v3, p0, Lcom/android/server/location/GnssCollectDataImpl;->mSessionInfo:Lcom/android/server/location/GnssSessionInfo;

    invoke-virtual {v3}, Lcom/android/server/location/GnssSessionInfo;->getE1Top4Cn0Mean()D

    move-result-wide v3

    invoke-virtual {v0, v1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 184
    iget-object v1, p0, Lcom/android/server/location/GnssCollectDataImpl;->mJsonArray:Lorg/json/JSONArray;

    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 185
    iget-object v1, p0, Lcom/android/server/location/GnssCollectDataImpl;->mJsonArray:Lorg/json/JSONArray;

    invoke-virtual {v1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 193
    .local v1, "jsonString":Ljava/lang/String;
    goto :goto_0

    .line 190
    .end local v1    # "jsonString":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 191
    .local v1, "e":Lorg/json/JSONException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "JSON exception "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    const/4 v2, 0x0

    move-object v1, v2

    .line 195
    .local v1, "jsonString":Ljava/lang/String;
    :goto_0
    return-object v1
.end method

.method private parseNmea(Ljava/lang/String;)V
    .locals 1
    .param p1, "nmea"    # Ljava/lang/String;

    .line 860
    iget-object v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mSessionInfo:Lcom/android/server/location/GnssSessionInfo;

    invoke-virtual {v0, p1}, Lcom/android/server/location/GnssSessionInfo;->parseNmea(Ljava/lang/String;)V

    .line 861
    return-void
.end method

.method private registerControlListener()V
    .locals 4

    .line 674
    iget-object v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 675
    const-string v0, "GnssCD"

    const-string v1, "no context"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 676
    return-void

    .line 678
    :cond_0
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 679
    invoke-static {}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataNotifyUri()Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lcom/android/server/location/GnssCollectDataImpl$1;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/android/server/location/GnssCollectDataImpl$1;-><init>(Lcom/android/server/location/GnssCollectDataImpl;Landroid/os/Handler;)V

    .line 678
    const/4 v3, 0x1

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 686
    return-void
.end method

.method private saveB1Cn0(I[F[F[F)V
    .locals 7
    .param p1, "svCount"    # I
    .param p2, "cn0s"    # [F
    .param p3, "svCarrierFreqs"    # [F
    .param p4, "svConstellation"    # [F

    .line 533
    if-eqz p1, :cond_7

    if-eqz p2, :cond_7

    array-length v0, p2

    if-eqz v0, :cond_7

    array-length v0, p2

    if-lt v0, p1, :cond_7

    if-eqz p3, :cond_7

    array-length v0, p3

    if-eqz v0, :cond_7

    array-length v0, p3

    if-ge v0, p1, :cond_0

    goto :goto_3

    .line 539
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 540
    .local v0, "CnoB1Array":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Float;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, p1, :cond_2

    .line 541
    aget v2, p3, v1

    invoke-direct {p0, v2}, Lcom/android/server/location/GnssCollectDataImpl;->isL1Sv(F)Z

    move-result v2

    if-eqz v2, :cond_1

    aget v2, p4, v1

    const/high16 v3, 0x40a00000    # 5.0f

    cmpl-float v2, v2, v3

    if-nez v2, :cond_1

    .line 542
    aget v2, p2, v1

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 540
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 545
    .end local v1    # "i":I
    :cond_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x4

    if-ge v1, v2, :cond_3

    goto :goto_2

    .line 548
    :cond_3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 549
    .local v1, "numSvB1":I
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 550
    add-int/lit8 v2, v1, -0x4

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    float-to-double v2, v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-lez v2, :cond_5

    .line 551
    const-wide/16 v2, 0x0

    .line 552
    .local v2, "top4AvgCn0":D
    add-int/lit8 v4, v1, -0x4

    .local v4, "i":I
    :goto_1
    if-ge v4, v1, :cond_4

    .line 553
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    float-to-double v5, v5

    add-double/2addr v2, v5

    .line 552
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 555
    .end local v4    # "i":I
    :cond_4
    const-wide/high16 v4, 0x4010000000000000L    # 4.0

    div-double/2addr v2, v4

    .line 556
    const/16 v4, 0x9

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lcom/android/server/location/GnssCollectDataImpl;->sendMessage(ILjava/lang/Object;)V

    .line 558
    .end local v2    # "top4AvgCn0":D
    :cond_5
    return-void

    .line 546
    .end local v1    # "numSvB1":I
    :cond_6
    :goto_2
    return-void

    .line 536
    .end local v0    # "CnoB1Array":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Float;>;"
    :cond_7
    :goto_3
    return-void
.end method

.method private saveE1Cn0(I[F[F[F)V
    .locals 7
    .param p1, "svCount"    # I
    .param p2, "cn0s"    # [F
    .param p3, "svCarrierFreqs"    # [F
    .param p4, "svConstellation"    # [F

    .line 591
    if-eqz p1, :cond_7

    if-eqz p2, :cond_7

    array-length v0, p2

    if-eqz v0, :cond_7

    array-length v0, p2

    if-lt v0, p1, :cond_7

    if-eqz p3, :cond_7

    array-length v0, p3

    if-eqz v0, :cond_7

    array-length v0, p3

    if-ge v0, p1, :cond_0

    goto :goto_3

    .line 597
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 598
    .local v0, "CnoE1Array":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Float;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, p1, :cond_2

    .line 599
    aget v2, p3, v1

    invoke-direct {p0, v2}, Lcom/android/server/location/GnssCollectDataImpl;->isL1Sv(F)Z

    move-result v2

    if-eqz v2, :cond_1

    aget v2, p4, v1

    const/high16 v3, 0x40c00000    # 6.0f

    cmpl-float v2, v2, v3

    if-nez v2, :cond_1

    .line 600
    aget v2, p2, v1

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 598
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 603
    .end local v1    # "i":I
    :cond_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x4

    if-ge v1, v2, :cond_3

    goto :goto_2

    .line 606
    :cond_3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 607
    .local v1, "numSvE1":I
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 608
    add-int/lit8 v2, v1, -0x4

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    float-to-double v2, v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-lez v2, :cond_5

    .line 609
    const-wide/16 v2, 0x0

    .line 610
    .local v2, "top4AvgCn0":D
    add-int/lit8 v4, v1, -0x4

    .local v4, "i":I
    :goto_1
    if-ge v4, v1, :cond_4

    .line 611
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    float-to-double v5, v5

    add-double/2addr v2, v5

    .line 610
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 613
    .end local v4    # "i":I
    :cond_4
    const-wide/high16 v4, 0x4010000000000000L    # 4.0

    div-double/2addr v2, v4

    .line 614
    const/16 v4, 0xb

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lcom/android/server/location/GnssCollectDataImpl;->sendMessage(ILjava/lang/Object;)V

    .line 616
    .end local v2    # "top4AvgCn0":D
    :cond_5
    return-void

    .line 604
    .end local v1    # "numSvE1":I
    :cond_6
    :goto_2
    return-void

    .line 594
    .end local v0    # "CnoE1Array":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Float;>;"
    :cond_7
    :goto_3
    return-void
.end method

.method private saveFixStatus()V
    .locals 1

    .line 844
    iget-object v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mSessionInfo:Lcom/android/server/location/GnssSessionInfo;

    invoke-virtual {v0}, Lcom/android/server/location/GnssSessionInfo;->setTtffAuto()V

    .line 845
    return-void
.end method

.method private saveG1Cn0(I[F[F[F)V
    .locals 7
    .param p1, "svCount"    # I
    .param p2, "cn0s"    # [F
    .param p3, "svCarrierFreqs"    # [F
    .param p4, "svConstellation"    # [F

    .line 562
    if-eqz p1, :cond_7

    if-eqz p2, :cond_7

    array-length v0, p2

    if-eqz v0, :cond_7

    array-length v0, p2

    if-lt v0, p1, :cond_7

    if-eqz p3, :cond_7

    array-length v0, p3

    if-eqz v0, :cond_7

    array-length v0, p3

    if-ge v0, p1, :cond_0

    goto :goto_3

    .line 568
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 569
    .local v0, "CnoG1Array":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Float;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, p1, :cond_2

    .line 570
    aget v2, p3, v1

    invoke-direct {p0, v2}, Lcom/android/server/location/GnssCollectDataImpl;->isL1Sv(F)Z

    move-result v2

    if-eqz v2, :cond_1

    aget v2, p4, v1

    const/high16 v3, 0x40400000    # 3.0f

    cmpl-float v2, v2, v3

    if-nez v2, :cond_1

    .line 571
    aget v2, p2, v1

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 569
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 574
    .end local v1    # "i":I
    :cond_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x4

    if-ge v1, v2, :cond_3

    goto :goto_2

    .line 577
    :cond_3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 578
    .local v1, "numSvG1":I
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 579
    add-int/lit8 v2, v1, -0x4

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    float-to-double v2, v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-lez v2, :cond_5

    .line 580
    const-wide/16 v2, 0x0

    .line 581
    .local v2, "top4AvgCn0":D
    add-int/lit8 v4, v1, -0x4

    .local v4, "i":I
    :goto_1
    if-ge v4, v1, :cond_4

    .line 582
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    float-to-double v5, v5

    add-double/2addr v2, v5

    .line 581
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 584
    .end local v4    # "i":I
    :cond_4
    const-wide/high16 v4, 0x4010000000000000L    # 4.0

    div-double/2addr v2, v4

    .line 585
    const/16 v4, 0xa

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lcom/android/server/location/GnssCollectDataImpl;->sendMessage(ILjava/lang/Object;)V

    .line 587
    .end local v2    # "top4AvgCn0":D
    :cond_5
    return-void

    .line 575
    .end local v1    # "numSvG1":I
    :cond_6
    :goto_2
    return-void

    .line 565
    .end local v0    # "CnoG1Array":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Float;>;"
    :cond_7
    :goto_3
    return-void
.end method

.method private saveL1Cn0(I[F[F[F)V
    .locals 7
    .param p1, "svCount"    # I
    .param p2, "cn0s"    # [F
    .param p3, "svCarrierFreqs"    # [F
    .param p4, "svConstellation"    # [F

    .line 475
    if-eqz p1, :cond_7

    if-eqz p2, :cond_7

    array-length v0, p2

    if-eqz v0, :cond_7

    array-length v0, p2

    if-lt v0, p1, :cond_7

    if-eqz p3, :cond_7

    array-length v0, p3

    if-eqz v0, :cond_7

    array-length v0, p3

    if-ge v0, p1, :cond_0

    goto :goto_3

    .line 481
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 482
    .local v0, "CnoL1Array":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Float;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, p1, :cond_2

    .line 483
    aget v2, p3, v1

    invoke-direct {p0, v2}, Lcom/android/server/location/GnssCollectDataImpl;->isL1Sv(F)Z

    move-result v2

    if-eqz v2, :cond_1

    aget v2, p4, v1

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v3

    if-nez v2, :cond_1

    .line 484
    aget v2, p2, v1

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 482
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 487
    .end local v1    # "i":I
    :cond_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x4

    if-ge v1, v2, :cond_3

    goto :goto_2

    .line 490
    :cond_3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 491
    .local v1, "numSvL1":I
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 492
    add-int/lit8 v2, v1, -0x4

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    float-to-double v2, v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-lez v2, :cond_5

    .line 493
    const-wide/16 v2, 0x0

    .line 494
    .local v2, "top4AvgCn0":D
    add-int/lit8 v4, v1, -0x4

    .local v4, "i":I
    :goto_1
    if-ge v4, v1, :cond_4

    .line 495
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    float-to-double v5, v5

    add-double/2addr v2, v5

    .line 494
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 497
    .end local v4    # "i":I
    :cond_4
    const-wide/high16 v4, 0x4010000000000000L    # 4.0

    div-double/2addr v2, v4

    .line 498
    const/4 v4, 0x7

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lcom/android/server/location/GnssCollectDataImpl;->sendMessage(ILjava/lang/Object;)V

    .line 500
    .end local v2    # "top4AvgCn0":D
    :cond_5
    return-void

    .line 488
    .end local v1    # "numSvL1":I
    :cond_6
    :goto_2
    return-void

    .line 478
    .end local v0    # "CnoL1Array":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Float;>;"
    :cond_7
    :goto_3
    return-void
.end method

.method private saveL5Cn0(I[F[F[F)V
    .locals 7
    .param p1, "svCount"    # I
    .param p2, "cn0s"    # [F
    .param p3, "svCarrierFreqs"    # [F
    .param p4, "svConstellation"    # [F

    .line 504
    if-eqz p1, :cond_7

    if-eqz p2, :cond_7

    array-length v0, p2

    if-eqz v0, :cond_7

    array-length v0, p2

    if-lt v0, p1, :cond_7

    if-eqz p3, :cond_7

    array-length v0, p3

    if-eqz v0, :cond_7

    array-length v0, p3

    if-ge v0, p1, :cond_0

    goto :goto_3

    .line 510
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 511
    .local v0, "CnoL5Array":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Float;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, p1, :cond_2

    .line 512
    aget v2, p3, v1

    invoke-direct {p0, v2}, Lcom/android/server/location/GnssCollectDataImpl;->isL5Sv(F)Z

    move-result v2

    if-eqz v2, :cond_1

    aget v2, p4, v1

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v3

    if-nez v2, :cond_1

    .line 513
    aget v2, p2, v1

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 511
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 516
    .end local v1    # "i":I
    :cond_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x4

    if-ge v1, v2, :cond_3

    goto :goto_2

    .line 519
    :cond_3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 520
    .local v1, "numSvL5":I
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 521
    add-int/lit8 v2, v1, -0x4

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    float-to-double v2, v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-lez v2, :cond_5

    .line 522
    const-wide/16 v2, 0x0

    .line 523
    .local v2, "top4AvgCn0":D
    add-int/lit8 v4, v1, -0x4

    .local v4, "i":I
    :goto_1
    if-ge v4, v1, :cond_4

    .line 524
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    float-to-double v5, v5

    add-double/2addr v2, v5

    .line 523
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 526
    .end local v4    # "i":I
    :cond_4
    const-wide/high16 v4, 0x4010000000000000L    # 4.0

    div-double/2addr v2, v4

    .line 527
    const/16 v4, 0x8

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lcom/android/server/location/GnssCollectDataImpl;->sendMessage(ILjava/lang/Object;)V

    .line 529
    .end local v2    # "top4AvgCn0":D
    :cond_5
    return-void

    .line 517
    .end local v1    # "numSvL5":I
    :cond_6
    :goto_2
    return-void

    .line 507
    .end local v0    # "CnoL5Array":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Float;>;"
    :cond_7
    :goto_3
    return-void
.end method

.method private saveLog()V
    .locals 13

    .line 241
    const-string v0, "loseTimes"

    const-string v1, "runTime"

    const-string v2, "TTFF"

    const-string/jumbo v3, "startTime"

    const-string v4, "com.miui.analytics"

    invoke-direct {p0}, Lcom/android/server/location/GnssCollectDataImpl;->packToJsonArray()Ljava/lang/String;

    move-result-object v5

    .line 242
    .local v5, "output":Ljava/lang/String;
    iget-object v6, p0, Lcom/android/server/location/GnssCollectDataImpl;->mSessionInfo:Lcom/android/server/location/GnssSessionInfo;

    invoke-virtual {v6}, Lcom/android/server/location/GnssSessionInfo;->checkValidity()Z

    move-result v6

    if-nez v6, :cond_0

    .line 244
    return-void

    .line 246
    :cond_0
    iget-object v6, p0, Lcom/android/server/location/GnssCollectDataImpl;->mContext:Landroid/content/Context;

    const-string v7, "GnssCD"

    if-eqz v6, :cond_6

    if-nez v5, :cond_1

    goto/16 :goto_3

    .line 250
    :cond_1
    iget-object v6, p0, Lcom/android/server/location/GnssCollectDataImpl;->mJsonArray:Lorg/json/JSONArray;

    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v6

    const/16 v8, 0x14

    const/4 v9, 0x1

    if-le v6, v8, :cond_4

    .line 251
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    iget-object v8, p0, Lcom/android/server/location/GnssCollectDataImpl;->mJsonArray:Lorg/json/JSONArray;

    invoke-virtual {v8}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v6, v8, :cond_3

    .line 253
    :try_start_0
    iget-object v8, p0, Lcom/android/server/location/GnssCollectDataImpl;->mJsonArray:Lorg/json/JSONArray;

    invoke-virtual {v8, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    .line 254
    .local v8, "jsons":Lorg/json/JSONObject;
    new-instance v10, Landroid/content/Intent;

    const-string v11, "onetrack.action.TRACK_EVENT"

    invoke-direct {v10, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 255
    .local v10, "intent":Landroid/content/Intent;
    invoke-virtual {v10, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 256
    sget-boolean v11, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v11, :cond_2

    .line 257
    invoke-virtual {v10, v9}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto :goto_1

    .line 259
    :cond_2
    const/4 v11, 0x3

    invoke-virtual {v10, v11}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 261
    :goto_1
    const-string v11, "APP_ID"

    const-string v12, "2882303761518758754"

    invoke-virtual {v10, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 262
    const-string v11, "EVENT_NAME"

    const-string v12, "GNSS"

    invoke-virtual {v10, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 263
    const-string v11, "PACKAGE"

    invoke-virtual {v10, v11, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 264
    new-instance v11, Landroid/os/Bundle;

    invoke-direct {v11}, Landroid/os/Bundle;-><init>()V

    .line 265
    .local v11, "params":Landroid/os/Bundle;
    invoke-virtual {v8, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v3, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    invoke-virtual {v8, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v2, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    invoke-virtual {v8, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v1, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v0, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    invoke-virtual {v10, v11}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 270
    iget-object v12, p0, Lcom/android/server/location/GnssCollectDataImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v12, v10}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 271
    const-string v12, "GNSS data uploaded"

    invoke-static {v7, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    invoke-direct {p0, v8}, Lcom/android/server/location/GnssCollectDataImpl;->uploadCn0(Lorg/json/JSONObject;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 276
    .end local v8    # "jsons":Lorg/json/JSONObject;
    .end local v10    # "intent":Landroid/content/Intent;
    .end local v11    # "params":Landroid/os/Bundle;
    goto :goto_2

    .line 273
    :catch_0
    move-exception v8

    .line 274
    .local v8, "e":Ljava/lang/Exception;
    const-string/jumbo v10, "unexpected error when send GNSS event to onetrack"

    invoke-static {v7, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    .line 251
    .end local v8    # "e":Ljava/lang/Exception;
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 278
    .end local v6    # "i":I
    :cond_3
    invoke-direct {p0, v5}, Lcom/android/server/location/GnssCollectDataImpl;->saveToFile(Ljava/lang/String;)V

    .line 279
    const-string/jumbo v0, "send to file"

    invoke-static {v7, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    iput-object v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mJsonArray:Lorg/json/JSONArray;

    .line 282
    :cond_4
    iget-boolean v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->hasStartUploadData:Z

    if-nez v0, :cond_5

    .line 283
    iput-boolean v9, p0, Lcom/android/server/location/GnssCollectDataImpl;->hasStartUploadData:Z

    .line 284
    iget-object v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mContext:Landroid/content/Context;

    const-string v1, "action collect data"

    invoke-direct {p0, v0, v1}, Lcom/android/server/location/GnssCollectDataImpl;->setAlarm(Landroid/content/Context;Ljava/lang/String;)V

    .line 286
    :cond_5
    return-void

    .line 247
    :cond_6
    :goto_3
    const-string v0, "mContext == null || output == null"

    invoke-static {v7, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    return-void
.end method

.method private saveLoseStatus()V
    .locals 1

    .line 848
    iget-object v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mSessionInfo:Lcom/android/server/location/GnssSessionInfo;

    invoke-virtual {v0}, Lcom/android/server/location/GnssSessionInfo;->setLostTimes()V

    .line 849
    return-void
.end method

.method private saveStartStatus()V
    .locals 1

    .line 840
    iget-object v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mSessionInfo:Lcom/android/server/location/GnssSessionInfo;

    invoke-virtual {v0}, Lcom/android/server/location/GnssSessionInfo;->setStart()V

    .line 841
    return-void
.end method

.method private saveState()V
    .locals 2

    .line 856
    const/4 v0, 0x5

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/android/server/location/GnssCollectDataImpl;->sendMessage(ILjava/lang/Object;)V

    .line 857
    return-void
.end method

.method private saveStopStatus()V
    .locals 1

    .line 852
    iget-object v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mSessionInfo:Lcom/android/server/location/GnssSessionInfo;

    invoke-virtual {v0}, Lcom/android/server/location/GnssSessionInfo;->setEnd()V

    .line 853
    return-void
.end method

.method private saveToFile(Ljava/lang/String;)V
    .locals 8
    .param p1, "messageToFile"    # Ljava/lang/String;

    .line 204
    const-wide/16 v0, 0x0

    .line 205
    .local v0, "fileSize":J
    const/4 v2, 0x0

    .line 207
    .local v2, "out":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v3, Ljava/io/File;

    const-string v4, "/data/mqsas/gps/gps-strength"

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 208
    .local v3, "bigdataFile":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 209
    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v4

    const-wide/16 v6, 0x400

    div-long/2addr v4, v6

    move-wide v0, v4

    .line 210
    const-wide/16 v4, 0x5

    cmp-long v4, v0, v4

    if-lez v4, :cond_1

    .line 212
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 213
    invoke-virtual {v3}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z

    .line 214
    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z

    goto :goto_0

    .line 217
    :cond_0
    invoke-virtual {v3}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z

    .line 218
    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z

    .line 221
    :cond_1
    :goto_0
    new-instance v4, Ljava/io/FileOutputStream;

    const/4 v5, 0x1

    invoke-direct {v4, v3, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    move-object v2, v4

    .line 222
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 226
    .end local v3    # "bigdataFile":Ljava/io/File;
    nop

    .line 228
    :try_start_1
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 231
    :goto_1
    goto :goto_2

    .line 229
    :catch_0
    move-exception v3

    .line 230
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    .end local v3    # "e":Ljava/io/IOException;
    goto :goto_1

    .line 226
    :catchall_0
    move-exception v3

    goto :goto_3

    .line 223
    :catch_1
    move-exception v3

    .line 224
    .local v3, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 226
    .end local v3    # "e":Ljava/lang/Exception;
    if-eqz v2, :cond_2

    .line 228
    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_1

    .line 234
    :cond_2
    :goto_2
    return-void

    .line 226
    :goto_3
    if-eqz v2, :cond_3

    .line 228
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 231
    goto :goto_4

    .line 229
    :catch_2
    move-exception v4

    .line 230
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    .line 233
    .end local v4    # "e":Ljava/io/IOException;
    :cond_3
    :goto_4
    throw v3
.end method

.method private sendMessage(ILjava/lang/Object;)V
    .locals 2
    .param p1, "message"    # I
    .param p2, "obj"    # Ljava/lang/Object;

    .line 413
    iget-object v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 414
    const-string v0, "GnssCD"

    const-string v1, "mhandler is null  "

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 415
    return-void

    .line 417
    :cond_0
    invoke-static {v0, p1, p2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 418
    .local v0, "lMessage":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/location/GnssCollectDataImpl;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 419
    return-void
.end method

.method private setAlarm(Landroid/content/Context;Ljava/lang/String;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "action"    # Ljava/lang/String;

    .line 876
    if-eqz p1, :cond_1

    if-nez p2, :cond_0

    goto :goto_1

    .line 881
    :cond_0
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 882
    .local v0, "filter":Landroid/content/IntentFilter;
    invoke-virtual {v0, p2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 883
    iget-object v1, p0, Lcom/android/server/location/GnssCollectDataImpl;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 885
    :try_start_0
    const-string v1, "alarm"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Landroid/app/AlarmManager;

    .line 886
    .local v2, "alarmManager":Landroid/app/AlarmManager;
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, p2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 887
    .local v1, "i":Landroid/content/Intent;
    const/4 v3, 0x0

    const/high16 v4, 0x4000000

    invoke-static {p1, v3, v1, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v8

    .line 888
    .local v8, "p":Landroid/app/PendingIntent;
    const/4 v3, 0x2

    .line 889
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget v6, p0, Lcom/android/server/location/GnssCollectDataImpl;->UPLOAD_REPEAT_TIME:I

    int-to-long v9, v6

    add-long/2addr v4, v9

    int-to-long v6, v6

    .line 888
    invoke-virtual/range {v2 .. v8}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 892
    .end local v1    # "i":Landroid/content/Intent;
    .end local v2    # "alarmManager":Landroid/app/AlarmManager;
    .end local v8    # "p":Landroid/app/PendingIntent;
    goto :goto_0

    .line 890
    :catch_0
    move-exception v1

    .line 891
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 893
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 877
    .end local v0    # "filter":Landroid/content/IntentFilter;
    :cond_1
    :goto_1
    const-string v0, "GnssCD"

    const-string v1, "context || action == null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 878
    return-void
.end method

.method private setB1Cn0(D)V
    .locals 1
    .param p1, "cn0"    # D

    .line 872
    iget-object v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mSessionInfo:Lcom/android/server/location/GnssSessionInfo;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/location/GnssSessionInfo;->setB1Cn0(D)V

    .line 873
    return-void
.end method

.method private setCurrentState(I)V
    .locals 0
    .param p1, "s"    # I

    .line 836
    sput p1, Lcom/android/server/location/GnssCollectDataImpl;->mCurrentState:I

    .line 837
    return-void
.end method

.method private setL1Cn0(D)V
    .locals 1
    .param p1, "cn0"    # D

    .line 864
    iget-object v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mSessionInfo:Lcom/android/server/location/GnssSessionInfo;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/location/GnssSessionInfo;->setL1Cn0(D)V

    .line 865
    return-void
.end method

.method private setL5Cn0(D)V
    .locals 1
    .param p1, "cn0"    # D

    .line 868
    iget-object v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mSessionInfo:Lcom/android/server/location/GnssSessionInfo;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/location/GnssSessionInfo;->setL5Cn0(D)V

    .line 869
    return-void
.end method

.method private startHandlerThread()V
    .locals 2

    .line 788
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "GnssCD thread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mHandlerThread:Landroid/os/HandlerThread;

    .line 789
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 790
    new-instance v0, Lcom/android/server/location/GnssCollectDataImpl$2;

    iget-object v1, p0, Lcom/android/server/location/GnssCollectDataImpl;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/server/location/GnssCollectDataImpl$2;-><init>(Lcom/android/server/location/GnssCollectDataImpl;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mHandler:Landroid/os/Handler;

    .line 833
    return-void
.end method

.method private startUploadFixData(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 316
    const-string v0, "com.miui.analytics"

    const-string v1, "GnssCD"

    iget v2, p0, Lcom/android/server/location/GnssCollectDataImpl;->mSvUsedInFix:I

    if-nez v2, :cond_0

    .line 317
    return-void

    .line 320
    :cond_0
    :try_start_0
    new-instance v2, Landroid/content/Intent;

    const-string v3, "onetrack.action.TRACK_EVENT"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 321
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {v2, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 322
    sget-boolean v3, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v3, :cond_1

    .line 323
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto :goto_0

    .line 325
    :cond_1
    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 327
    :goto_0
    const-string v3, "APP_ID"

    const-string v4, "2882303761518758754"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 328
    const-string v3, "EVENT_NAME"

    const-string v4, "GNSS_CONSTELLATION_FIX"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 329
    const-string v3, "PACKAGE"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 330
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 331
    .local v0, "params":Landroid/os/Bundle;
    const-string v3, "SV_FIX"

    iget v4, p0, Lcom/android/server/location/GnssCollectDataImpl;->mSvUsedInFix:I

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 332
    const-string v3, "GPS_FIX"

    iget v4, p0, Lcom/android/server/location/GnssCollectDataImpl;->mNumGPSUsedInFix:I

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 333
    const-string v3, "BEIDOU_FIX"

    iget v4, p0, Lcom/android/server/location/GnssCollectDataImpl;->mNumBEIDOUUsedInFix:I

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 334
    const-string v3, "GLONASS_FIX"

    iget v4, p0, Lcom/android/server/location/GnssCollectDataImpl;->mNumGLONASSUsedInFix:I

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 335
    const-string v3, "GALILEO_FIX"

    iget v4, p0, Lcom/android/server/location/GnssCollectDataImpl;->mNumGALILEOUsedInFix:I

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 336
    invoke-virtual {v2, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 337
    iget-object v3, p0, Lcom/android/server/location/GnssCollectDataImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 338
    const-string v3, "GNSS_CONSTELLATION_FIX uploaded"

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 342
    nop

    .end local v0    # "params":Landroid/os/Bundle;
    .end local v2    # "intent":Landroid/content/Intent;
    goto :goto_1

    .line 339
    :catch_0
    move-exception v0

    .line 340
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v2, "unexpected error when send GNSS event to onetrack"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 341
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 343
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mSvUsedInFix:I

    .line 344
    iput v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mNumGPSUsedInFix:I

    .line 345
    iput v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mNumBEIDOUUsedInFix:I

    .line 346
    iput v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mNumGLONASSUsedInFix:I

    .line 347
    iput v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mNumGALILEOUsedInFix:I

    .line 348
    return-void
.end method

.method private updateBackgroundOptCloudConfig()V
    .locals 4

    .line 752
    iget-object v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mContext:Landroid/content/Context;

    .line 753
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 752
    const-string v1, "bigdata"

    const-string v2, "bo_status"

    const-string v3, "-1"

    invoke-static {v0, v1, v2, v3}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 754
    .local v0, "newBoStatus":Ljava/lang/String;
    const-string v1, "0"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const-string v2, "GnssCD"

    if-eqz v1, :cond_0

    .line 755
    invoke-static {}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub;->getInstance()Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub;->setBackgroundOptStatus(Z)V

    .line 756
    const-string v1, "background Opt disable by cloud..."

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 757
    :cond_0
    const-string v1, "1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 758
    invoke-static {}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub;->getInstance()Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub;->setBackgroundOptStatus(Z)V

    .line 759
    const-string v1, "background Opt enable by cloud..."

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 761
    :cond_1
    :goto_0
    return-void
.end method

.method private updateControlState()V
    .locals 9

    .line 689
    const-string v0, "got rule changed"

    const-string v1, "GnssCD"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 690
    iget-object v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mContext:Landroid/content/Context;

    .line 691
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 690
    const-string v2, "bigdata"

    const-string v3, "enabled"

    const-string v4, "1"

    invoke-static {v0, v2, v3, v4}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 693
    .local v0, "newState":Ljava/lang/String;
    iget-object v4, p0, Lcom/android/server/location/GnssCollectDataImpl;->mContext:Landroid/content/Context;

    .line 694
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 693
    const-string v5, "enableCaict"

    const-string v6, "0"

    invoke-static {v4, v2, v5, v6}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 695
    .local v2, "newsuplState":Ljava/lang/String;
    const-string v4, "persist.sys.mqs.gps"

    invoke-static {v4, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 696
    const-string v4, "persist.sys.mqs.gps.supl"

    invoke-static {v4, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 697
    iget-object v4, p0, Lcom/android/server/location/GnssCollectDataImpl;->mContext:Landroid/content/Context;

    .line 698
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 697
    const-string v5, "gnssRtk"

    const-string v6, "ON"

    invoke-static {v4, v5, v3, v6}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 699
    .local v3, "rtkNewState":Ljava/lang/String;
    const-string v4, "persist.sys.mqs.gps.rtk"

    invoke-static {v4, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 700
    invoke-direct {p0}, Lcom/android/server/location/GnssCollectDataImpl;->updateGsrCloudConfig()V

    .line 701
    invoke-direct {p0}, Lcom/android/server/location/GnssCollectDataImpl;->updateGpoCloudConfig()V

    .line 702
    invoke-direct {p0}, Lcom/android/server/location/GnssCollectDataImpl;->updateBackgroundOptCloudConfig()V

    .line 703
    invoke-direct {p0}, Lcom/android/server/location/GnssCollectDataImpl;->updateSatelliteCallOptCloudConfig()V

    .line 704
    invoke-direct {p0}, Lcom/android/server/location/GnssCollectDataImpl;->updateMockLocationOptCloudConfig()V

    .line 706
    iget-object v4, p0, Lcom/android/server/location/GnssCollectDataImpl;->mContext:Landroid/content/Context;

    .line 707
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 706
    const-string v5, "nlp"

    const-string v6, "SDK_source"

    const/4 v7, 0x5

    invoke-static {v4, v5, v6, v7}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataInt(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)I

    move-result v4

    .line 709
    .local v4, "sdk_source":I
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    .line 710
    .local v5, "intent":Landroid/content/Intent;
    const-string/jumbo v7, "send CLOUD_UPDATE Broadcast"

    invoke-static {v1, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 711
    const-string v1, "com.xiaomi.location.metoknlp.CLOUD_UPDATE"

    invoke-virtual {v5, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 712
    new-instance v1, Landroid/content/ComponentName;

    const-string v7, "com.xiaomi.metoknlp"

    const-string v8, "com.xiaomi.location.metoknlp.CloudReceiver"

    invoke-direct {v1, v7, v8}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 713
    invoke-virtual {v5, v6, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 714
    iget-object v1, p0, Lcom/android/server/location/GnssCollectDataImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 715
    return-void
.end method

.method private updateGpoCloudConfig()V
    .locals 4

    .line 779
    iget-object v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mContext:Landroid/content/Context;

    .line 780
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 779
    const-string v1, "bigdata"

    const-string v2, "GpoVersion"

    const/4 v3, -0x1

    invoke-static {v0, v1, v2, v3}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataInt(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    .line 781
    .local v0, "gpoNewVersion":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Got GpoVersion Cloud Config ? "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-ltz v0, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GnssCD"

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 782
    if-ltz v0, :cond_1

    .line 783
    invoke-static {}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeStub;->getInstance()Lcom/android/server/location/gnss/hal/GnssPowerOptimizeStub;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeStub;->setGpoVersionValue(I)V

    .line 785
    :cond_1
    return-void
.end method

.method private updateGsrCloudConfig()V
    .locals 4

    .line 743
    iget-object v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mContext:Landroid/content/Context;

    .line 744
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 743
    const-string v1, "bigdata"

    const-string v2, "enableGSR"

    const/4 v3, -0x1

    invoke-static {v0, v1, v2, v3}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataInt(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    .line 745
    .local v0, "gnssSelfRecovery":I
    if-ltz v0, :cond_0

    .line 746
    const-string v1, "persist.sys.gps.selfRecovery"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 747
    invoke-static {}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecoveryStub;->getInstance()Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecoveryStub;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecoveryStub;->setGsrConfig()V

    .line 749
    :cond_0
    return-void
.end method

.method private updateMockLocationOptCloudConfig()V
    .locals 4

    .line 767
    iget-object v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mContext:Landroid/content/Context;

    .line 768
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 767
    const-string v1, "bigdata"

    const-string v2, "gmo_status"

    const-string v3, "-1"

    invoke-static {v0, v1, v2, v3}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 769
    .local v0, "newGmoStatus":Ljava/lang/String;
    const-string v1, "0"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const-string v2, "GnssCD"

    if-eqz v1, :cond_0

    .line 770
    invoke-static {}, Lcom/android/server/location/GnssMockLocationOptStub;->getInstance()Lcom/android/server/location/GnssMockLocationOptStub;

    move-result-object v1

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Lcom/android/server/location/GnssMockLocationOptStub;->setMockLocationOptStatus(Z)V

    .line 771
    const-string v1, "mock location Opt disable by cloud"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 772
    :cond_0
    const-string v1, "1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 773
    invoke-static {}, Lcom/android/server/location/GnssMockLocationOptStub;->getInstance()Lcom/android/server/location/GnssMockLocationOptStub;

    move-result-object v1

    const/4 v3, 0x1

    invoke-interface {v1, v3}, Lcom/android/server/location/GnssMockLocationOptStub;->setMockLocationOptStatus(Z)V

    .line 774
    const-string v1, "mock location Opt enable by cloud"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 776
    :cond_1
    :goto_0
    return-void
.end method

.method private updateSatelliteCallOptCloudConfig()V
    .locals 6

    .line 722
    iget-object v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mContext:Landroid/content/Context;

    .line 723
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 722
    const-string v1, "bigdata"

    const-string v2, "gsco_status"

    const-string v3, "-1"

    invoke-static {v0, v1, v2, v3}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 724
    .local v0, "newScoStatus":Ljava/lang/String;
    const-string v2, "0"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const-string v4, "GnssCD"

    if-eqz v2, :cond_0

    .line 725
    invoke-static {}, Lcom/android/server/location/LocationExtCooperateStub;->getInstance()Lcom/android/server/location/LocationExtCooperateStub;

    move-result-object v2

    const/4 v5, 0x0

    invoke-interface {v2, v5}, Lcom/android/server/location/LocationExtCooperateStub;->setSatelliteCallOptStatus(Z)V

    .line 726
    const-string v2, "Satellite Call Opt disable by cloud..."

    invoke-static {v4, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 727
    :cond_0
    const-string v2, "1"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 728
    invoke-static {}, Lcom/android/server/location/LocationExtCooperateStub;->getInstance()Lcom/android/server/location/LocationExtCooperateStub;

    move-result-object v2

    const/4 v5, 0x1

    invoke-interface {v2, v5}, Lcom/android/server/location/LocationExtCooperateStub;->setSatelliteCallOptStatus(Z)V

    .line 729
    const-string v2, "Satellite Call Opt enable by cloud..."

    invoke-static {v4, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 732
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/android/server/location/GnssCollectDataImpl;->mContext:Landroid/content/Context;

    .line 733
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 732
    const-string v4, "gsco_status_pkg"

    invoke-static {v2, v1, v4, v3}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 734
    .local v1, "newScoPkg":Ljava/lang/String;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 735
    invoke-static {}, Lcom/android/server/location/LocationExtCooperateStub;->getInstance()Lcom/android/server/location/LocationExtCooperateStub;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/android/server/location/LocationExtCooperateStub;->setSatelliteCallOptPkg(Ljava/lang/String;)V

    .line 737
    :cond_2
    return-void
.end method

.method private uploadCn0(Lorg/json/JSONObject;)V
    .locals 10
    .param p1, "jsons"    # Lorg/json/JSONObject;

    .line 290
    const-string v0, "E1Top4MeanCn0"

    const-string v1, "G1Top4MeanCn0"

    const-string v2, "B1Top4MeanCn0"

    const-string v3, "L5Top4MeanCn0"

    const-string v4, "L1Top4MeanCn0"

    const-string v5, "com.miui.analytics"

    const-string v6, "GnssCD"

    :try_start_0
    new-instance v7, Landroid/content/Intent;

    const-string v8, "onetrack.action.TRACK_EVENT"

    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 291
    .local v7, "intent":Landroid/content/Intent;
    invoke-virtual {v7, v5}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 292
    sget-boolean v8, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v8, :cond_0

    .line 293
    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto :goto_0

    .line 295
    :cond_0
    const/4 v8, 0x3

    invoke-virtual {v7, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 297
    :goto_0
    const-string v8, "APP_ID"

    const-string v9, "2882303761518758754"

    invoke-virtual {v7, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 298
    const-string v8, "EVENT_NAME"

    const-string v9, "GNSS_CN0"

    invoke-virtual {v7, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 299
    const-string v8, "PACKAGE"

    invoke-virtual {v7, v8, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 300
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 301
    .local v5, "params":Landroid/os/Bundle;
    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v4, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    invoke-virtual {v7, v5}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 307
    iget-object v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v7}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 308
    const-string v0, "GNSS_CN0 uploaded"

    invoke-static {v6, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 312
    nop

    .end local v5    # "params":Landroid/os/Bundle;
    .end local v7    # "intent":Landroid/content/Intent;
    goto :goto_1

    .line 309
    :catch_0
    move-exception v0

    .line 310
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v1, "unexpected error when send GNSS event to onetrack"

    invoke-static {v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 313
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    return-void
.end method


# virtual methods
.method public getCurrentTime()Ljava/lang/String;
    .locals 11

    .line 404
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 405
    .local v0, "mNow":J
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 406
    .local v2, "sb":Ljava/lang/StringBuilder;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v9

    .line 407
    .local v9, "c":Ljava/util/Calendar;
    invoke-virtual {v9, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 408
    const-string v10, "%tm-%td %tH:%tM:%tS.%tL"

    move-object v3, v9

    move-object v4, v9

    move-object v5, v9

    move-object v6, v9

    move-object v7, v9

    move-object v8, v9

    filled-new-array/range {v3 .. v8}, [Ljava/lang/Object;

    move-result-object v3

    invoke-static {v10, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 409
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public getSuplState()Z
    .locals 2

    .line 141
    const-string v0, "persist.sys.mqs.gps.supl"

    const-string v1, "0"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 143
    .local v0, "suplstate":Ljava/lang/String;
    const-string v1, "1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method public isCnSimInserted()Z
    .locals 1

    .line 155
    iget-boolean v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mIsCnSim:Z

    return v0
.end method

.method public savePoint(ILjava/lang/String;)V
    .locals 4
    .param p1, "type"    # I
    .param p2, "extraInfo"    # Ljava/lang/String;

    .line 635
    invoke-direct {p0}, Lcom/android/server/location/GnssCollectDataImpl;->allowCollect()Z

    move-result v0

    if-nez v0, :cond_0

    .line 636
    const-string v0, "GnssCD"

    const-string v1, "no GnssCD enabled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 637
    return-void

    .line 639
    :cond_0
    if-nez p1, :cond_1

    .line 640
    invoke-direct {p0}, Lcom/android/server/location/GnssCollectDataImpl;->startHandlerThread()V

    .line 641
    invoke-direct {p0, p1}, Lcom/android/server/location/GnssCollectDataImpl;->setCurrentState(I)V

    goto :goto_0

    .line 642
    :cond_1
    const/4 v0, 0x3

    const/4 v1, 0x1

    if-ne v1, p1, :cond_3

    .line 643
    sget v1, Lcom/android/server/location/GnssCollectDataImpl;->mCurrentState:I

    if-eqz v1, :cond_2

    if-eq v1, v0, :cond_2

    const/4 v0, 0x5

    if-ne v1, v0, :cond_9

    .line 644
    :cond_2
    iget-object v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mSessionInfo:Lcom/android/server/location/GnssSessionInfo;

    invoke-virtual {v0}, Lcom/android/server/location/GnssSessionInfo;->newSessionReset()V

    .line 645
    invoke-direct {p0, p1, p2}, Lcom/android/server/location/GnssCollectDataImpl;->sendMessage(ILjava/lang/Object;)V

    goto :goto_0

    .line 647
    :cond_3
    const/4 v2, 0x2

    if-ne v2, p1, :cond_4

    .line 648
    sget v0, Lcom/android/server/location/GnssCollectDataImpl;->mCurrentState:I

    if-ne v0, v1, :cond_9

    .line 649
    invoke-direct {p0, p1, p2}, Lcom/android/server/location/GnssCollectDataImpl;->sendMessage(ILjava/lang/Object;)V

    goto :goto_0

    .line 651
    :cond_4
    const/4 v3, 0x4

    if-ne v0, p1, :cond_6

    .line 652
    sget v0, Lcom/android/server/location/GnssCollectDataImpl;->mCurrentState:I

    if-eq v0, v1, :cond_5

    if-eq v0, v2, :cond_5

    if-ne v0, v3, :cond_9

    .line 653
    :cond_5
    invoke-direct {p0, p1, p2}, Lcom/android/server/location/GnssCollectDataImpl;->sendMessage(ILjava/lang/Object;)V

    goto :goto_0

    .line 655
    :cond_6
    if-ne v3, p1, :cond_8

    .line 659
    sget v0, Lcom/android/server/location/GnssCollectDataImpl;->mCurrentState:I

    if-eq v0, v2, :cond_7

    if-ne v0, v3, :cond_9

    .line 660
    :cond_7
    invoke-direct {p0, p1, p2}, Lcom/android/server/location/GnssCollectDataImpl;->sendMessage(ILjava/lang/Object;)V

    goto :goto_0

    .line 662
    :cond_8
    const/4 v0, 0x6

    if-ne v0, p1, :cond_a

    .line 664
    if-eqz p2, :cond_9

    const-string v0, "$PQWP6"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 665
    invoke-direct {p0, p1, p2}, Lcom/android/server/location/GnssCollectDataImpl;->sendMessage(ILjava/lang/Object;)V

    .line 671
    :cond_9
    :goto_0
    return-void

    .line 669
    :cond_a
    return-void
.end method

.method public savePoint(ILjava/lang/String;Landroid/content/Context;)V
    .locals 2
    .param p1, "type"    # I
    .param p2, "extraInfo"    # Ljava/lang/String;
    .param p3, "context"    # Landroid/content/Context;

    .line 621
    invoke-direct {p0}, Lcom/android/server/location/GnssCollectDataImpl;->allowCollect()Z

    move-result v0

    const-string v1, "GnssCD"

    if-nez v0, :cond_0

    .line 622
    const-string v0, "no GnssCD enabled"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 623
    return-void

    .line 625
    :cond_0
    if-nez p1, :cond_1

    .line 626
    iput-object p3, p0, Lcom/android/server/location/GnssCollectDataImpl;->mContext:Landroid/content/Context;

    .line 627
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p2}, Lcom/android/server/location/GnssCollectDataImpl;->savePoint(ILjava/lang/String;)V

    .line 628
    const-string v0, "register listener"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 629
    invoke-direct {p0}, Lcom/android/server/location/GnssCollectDataImpl;->registerControlListener()V

    .line 631
    :cond_1
    return-void
.end method

.method public savePoint(I[FI[F[F)V
    .locals 4
    .param p1, "type"    # I
    .param p2, "cn0s"    # [F
    .param p3, "numSv"    # I
    .param p4, "svCarrierFreqs"    # [F
    .param p5, "svConstellation"    # [F

    .line 433
    invoke-direct {p0}, Lcom/android/server/location/GnssCollectDataImpl;->allowCollect()Z

    move-result v0

    if-nez v0, :cond_0

    .line 434
    const-string v0, "GnssCD"

    const-string v1, "no GnssCD enabled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 435
    return-void

    .line 438
    :cond_0
    const/16 v0, 0xa

    if-eq v0, p1, :cond_1

    .line 439
    return-void

    .line 441
    :cond_1
    if-eqz p3, :cond_b

    if-eqz p2, :cond_b

    array-length v0, p2

    if-eqz v0, :cond_b

    array-length v0, p2

    if-ge v0, p3, :cond_2

    goto/16 :goto_3

    .line 444
    :cond_2
    const/4 v0, 0x4

    if-ge p3, v0, :cond_4

    .line 445
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/android/server/location/GnssCollectDataImpl;->mLastSvTime:J

    sub-long/2addr v0, v2

    iget v2, p0, Lcom/android/server/location/GnssCollectDataImpl;->SV_ERROR_SEND_INTERVAL:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_3

    .line 446
    const-string v0, "SvCount"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    filled-new-array {v0, v1}, [Ljava/lang/Object;

    move-result-object v0

    const v1, 0x36c70305

    invoke-static {v1, v0}, Lcom/miui/misight/MiSight;->constructEvent(I[Ljava/lang/Object;)Lcom/miui/misight/MiEvent;

    move-result-object v0

    .line 447
    .local v0, "event":Lcom/miui/misight/MiEvent;
    invoke-static {v0}, Lcom/miui/misight/MiSight;->sendEvent(Lcom/miui/misight/MiEvent;)V

    .line 448
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/server/location/GnssCollectDataImpl;->mLastSvTime:J

    .line 450
    .end local v0    # "event":Lcom/miui/misight/MiEvent;
    :cond_3
    return-void

    .line 452
    :cond_4
    invoke-direct {p0, p3, p2, p4, p5}, Lcom/android/server/location/GnssCollectDataImpl;->saveL1Cn0(I[F[F[F)V

    .line 453
    invoke-direct {p0, p3, p2, p4, p5}, Lcom/android/server/location/GnssCollectDataImpl;->saveL5Cn0(I[F[F[F)V

    .line 454
    invoke-direct {p0, p3, p2, p4, p5}, Lcom/android/server/location/GnssCollectDataImpl;->saveB1Cn0(I[F[F[F)V

    .line 455
    invoke-direct {p0, p3, p2, p4, p5}, Lcom/android/server/location/GnssCollectDataImpl;->saveG1Cn0(I[F[F[F)V

    .line 456
    invoke-direct {p0, p3, p2, p4, p5}, Lcom/android/server/location/GnssCollectDataImpl;->saveE1Cn0(I[F[F[F)V

    .line 457
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p5

    if-ge v0, v1, :cond_a

    .line 458
    aget v1, p5, v0

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-nez v1, :cond_5

    .line 459
    goto :goto_2

    .line 460
    :cond_5
    aget v1, p5, v0

    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v1, v1, v2

    if-nez v1, :cond_6

    .line 461
    iget v1, p0, Lcom/android/server/location/GnssCollectDataImpl;->mNumGPSUsedInFix:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/server/location/GnssCollectDataImpl;->mNumGPSUsedInFix:I

    goto :goto_1

    .line 462
    :cond_6
    aget v1, p5, v0

    const/high16 v2, 0x40a00000    # 5.0f

    cmpl-float v1, v1, v2

    if-nez v1, :cond_7

    .line 463
    iget v1, p0, Lcom/android/server/location/GnssCollectDataImpl;->mNumBEIDOUUsedInFix:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/server/location/GnssCollectDataImpl;->mNumBEIDOUUsedInFix:I

    goto :goto_1

    .line 464
    :cond_7
    aget v1, p5, v0

    const/high16 v2, 0x40400000    # 3.0f

    cmpl-float v1, v1, v2

    if-nez v1, :cond_8

    .line 465
    iget v1, p0, Lcom/android/server/location/GnssCollectDataImpl;->mNumGLONASSUsedInFix:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/server/location/GnssCollectDataImpl;->mNumGLONASSUsedInFix:I

    goto :goto_1

    .line 466
    :cond_8
    aget v1, p5, v0

    const/high16 v2, 0x40c00000    # 6.0f

    cmpl-float v1, v1, v2

    if-nez v1, :cond_9

    .line 467
    iget v1, p0, Lcom/android/server/location/GnssCollectDataImpl;->mNumGALILEOUsedInFix:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/server/location/GnssCollectDataImpl;->mNumGALILEOUsedInFix:I

    .line 469
    :cond_9
    :goto_1
    iget v1, p0, Lcom/android/server/location/GnssCollectDataImpl;->mSvUsedInFix:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/server/location/GnssCollectDataImpl;->mSvUsedInFix:I

    .line 457
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 471
    .end local v0    # "i":I
    :cond_a
    return-void

    .line 442
    :cond_b
    :goto_3
    return-void
.end method

.method public saveUngrantedBackPermission(IILjava/lang/String;)V
    .locals 2
    .param p1, "mUid"    # I
    .param p2, "mPid"    # I
    .param p3, "packageName"    # Ljava/lang/String;

    .line 382
    iget-object v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 383
    return-void

    .line 385
    :cond_0
    const-string v1, "android.permission.ACCESS_BACKGROUND_LOCATION"

    invoke-virtual {v0, v1, p2, p1}, Landroid/content/Context;->checkPermission(Ljava/lang/String;II)I

    move-result v0

    if-nez v0, :cond_1

    .line 387
    return-void

    .line 389
    :cond_1
    const-string v0, "com.autonavi.minimap"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 390
    iget v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->AmapBackCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->AmapBackCount:I

    goto :goto_0

    .line 391
    :cond_2
    const-string v0, "com.baidu.BaiduMap"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 392
    iget v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->BaiduBackCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->BaiduBackCount:I

    goto :goto_0

    .line 393
    :cond_3
    const-string v0, "com.tencent.map"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 394
    iget v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->TencentBackCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->TencentBackCount:I

    .line 396
    :cond_4
    :goto_0
    return-void
.end method

.method public setCnSimInserted(Ljava/lang/String;)V
    .locals 1
    .param p1, "mccMnc"    # Ljava/lang/String;

    .line 148
    const-string v0, "460"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mIsCnSim:Z

    .line 150
    const-string v0, "persist.sys.mcc.mnc"

    invoke-static {v0, p1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    return-void
.end method

.method public startUploadBackData(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .line 351
    const-string v0, "com.miui.analytics"

    const-string v1, "GnssCD"

    iget v2, p0, Lcom/android/server/location/GnssCollectDataImpl;->AmapBackCount:I

    if-nez v2, :cond_0

    iget v2, p0, Lcom/android/server/location/GnssCollectDataImpl;->BaiduBackCount:I

    if-nez v2, :cond_0

    iget v2, p0, Lcom/android/server/location/GnssCollectDataImpl;->TencentBackCount:I

    if-nez v2, :cond_0

    .line 352
    return-void

    .line 355
    :cond_0
    :try_start_0
    new-instance v2, Landroid/content/Intent;

    const-string v3, "onetrack.action.TRACK_EVENT"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 356
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {v2, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 357
    sget-boolean v3, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    const/4 v4, 0x3

    if-eqz v3, :cond_1

    .line 358
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto :goto_0

    .line 360
    :cond_1
    invoke-virtual {v2, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 362
    :goto_0
    const-string v3, "APP_ID"

    const-string v5, "2882303761518758754"

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 363
    const-string v3, "EVENT_NAME"

    const-string v5, "GNSS_BACKGROUND_PERMISSION"

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 364
    const-string v3, "PACKAGE"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 365
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 366
    .local v0, "params":Landroid/os/Bundle;
    const-string v3, "AmapBackCount"

    iget v5, p0, Lcom/android/server/location/GnssCollectDataImpl;->AmapBackCount:I

    div-int/2addr v5, v4

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 367
    const-string v3, "BaiduBackCount"

    iget v5, p0, Lcom/android/server/location/GnssCollectDataImpl;->BaiduBackCount:I

    div-int/2addr v5, v4

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 368
    const-string v3, "TencentBackCount"

    iget v5, p0, Lcom/android/server/location/GnssCollectDataImpl;->TencentBackCount:I

    div-int/2addr v5, v4

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 369
    invoke-virtual {v2, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 370
    iget-object v3, p0, Lcom/android/server/location/GnssCollectDataImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 371
    const-string v3, "GNSS_BACKGROUND_PERMISSION uploaded"

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 375
    nop

    .end local v0    # "params":Landroid/os/Bundle;
    .end local v2    # "intent":Landroid/content/Intent;
    goto :goto_1

    .line 372
    :catch_0
    move-exception v0

    .line 373
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v2, "unexpected error when send GNSS event to onetrack"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 374
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 376
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->AmapBackCount:I

    .line 377
    iput v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->BaiduBackCount:I

    .line 378
    iput v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->TencentBackCount:I

    .line 379
    return-void
.end method
