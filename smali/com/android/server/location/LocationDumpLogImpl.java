public class com.android.server.location.LocationDumpLogImpl implements com.android.server.location.LocationDumpLogStub {
	 /* .source "LocationDumpLogImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final java.lang.String DUMP_TAG_GLP;
	 private static final java.lang.String DUMP_TAG_GLP_EN;
	 private static final java.lang.String DUMP_TAG_LMS;
	 private static final java.lang.String DUMP_TAG_NMEA;
	 private static final com.android.server.location.GnssLocalLog mdumpGlp;
	 private static final com.android.server.location.GnssLocalLog mdumpLms;
	 private static final com.android.server.location.GnssLocalLog mdumpNmea;
	 /* # instance fields */
	 private Integer defaultFusedProviderName;
	 private Integer defaultGeocoderProviderName;
	 private Integer defaultGeofenceProviderName;
	 private Integer defaultNetworkProviderName;
	 private Boolean mRecordLoseCount;
	 /* # direct methods */
	 static com.android.server.location.LocationDumpLogImpl ( ) {
		 /* .locals 2 */
		 /* .line 14 */
		 /* new-instance v0, Lcom/android/server/location/GnssLocalLog; */
		 /* const/16 v1, 0x3e8 */
		 /* invoke-direct {v0, v1}, Lcom/android/server/location/GnssLocalLog;-><init>(I)V */
		 /* .line 15 */
		 /* new-instance v0, Lcom/android/server/location/GnssLocalLog; */
		 /* invoke-direct {v0, v1}, Lcom/android/server/location/GnssLocalLog;-><init>(I)V */
		 /* .line 16 */
		 /* new-instance v0, Lcom/android/server/location/GnssLocalLog; */
		 /* const/16 v1, 0x4e20 */
		 /* invoke-direct {v0, v1}, Lcom/android/server/location/GnssLocalLog;-><init>(I)V */
		 return;
	 } // .end method
	 public com.android.server.location.LocationDumpLogImpl ( ) {
		 /* .locals 1 */
		 /* .line 12 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 23 */
		 /* const v0, 0x10402b7 */
		 /* iput v0, p0, Lcom/android/server/location/LocationDumpLogImpl;->defaultNetworkProviderName:I */
		 /* .line 24 */
		 /* const v0, 0x104029b */
		 /* iput v0, p0, Lcom/android/server/location/LocationDumpLogImpl;->defaultFusedProviderName:I */
		 /* .line 25 */
		 /* const v0, 0x104029c */
		 /* iput v0, p0, Lcom/android/server/location/LocationDumpLogImpl;->defaultGeocoderProviderName:I */
		 /* .line 26 */
		 /* const v0, 0x104029d */
		 /* iput v0, p0, Lcom/android/server/location/LocationDumpLogImpl;->defaultGeofenceProviderName:I */
		 /* .line 28 */
		 int v0 = 1; // const/4 v0, 0x1
		 /* iput-boolean v0, p0, Lcom/android/server/location/LocationDumpLogImpl;->mRecordLoseCount:Z */
		 return;
	 } // .end method
	 private Boolean isCnVersion ( ) {
		 /* .locals 2 */
		 /* .line 84 */
		 final String v0 = "ro.miui.build.region"; // const-string v0, "ro.miui.build.region"
		 android.os.SystemProperties .get ( v0 );
		 final String v1 = "CN"; // const-string v1, "CN"
		 v0 = 		 (( java.lang.String ) v1 ).equalsIgnoreCase ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
	 } // .end method
	 private Boolean isXOptMode ( ) {
		 /* .locals 2 */
		 /* .line 80 */
		 final String v0 = "persist.sys.miui_optimization"; // const-string v0, "persist.sys.miui_optimization"
		 int v1 = 1; // const/4 v1, 0x1
		 v0 = 		 android.os.SystemProperties .getBoolean ( v0,v1 );
		 /* xor-int/2addr v0, v1 */
	 } // .end method
	 private com.android.server.location.GnssLocalLog switchTypeToDump ( Integer p0 ) {
		 /* .locals 1 */
		 /* .param p1, "type" # I */
		 /* .line 88 */
		 /* packed-switch p1, :pswitch_data_0 */
		 /* .line 95 */
		 v0 = com.android.server.location.LocationDumpLogImpl.mdumpNmea;
		 /* .line 93 */
		 /* :pswitch_0 */
		 v0 = com.android.server.location.LocationDumpLogImpl.mdumpGlp;
		 /* .line 90 */
		 /* :pswitch_1 */
		 v0 = com.android.server.location.LocationDumpLogImpl.mdumpLms;
		 /* :pswitch_data_0 */
		 /* .packed-switch 0x1 */
		 /* :pswitch_1 */
		 /* :pswitch_0 */
		 /* :pswitch_0 */
	 } // .end packed-switch
} // .end method
private java.lang.String switchTypeToLogTag ( Integer p0 ) {
	 /* .locals 1 */
	 /* .param p1, "type" # I */
	 /* .line 100 */
	 /* packed-switch p1, :pswitch_data_0 */
	 /* .line 108 */
	 final String v0 = "=MI NMEA="; // const-string v0, "=MI NMEA="
	 /* .line 106 */
	 /* :pswitch_0 */
	 final String v0 = "=MI GLP EN="; // const-string v0, "=MI GLP EN="
	 /* .line 104 */
	 /* :pswitch_1 */
	 final String v0 = "=MI GLP= "; // const-string v0, "=MI GLP= "
	 /* .line 102 */
	 /* :pswitch_2 */
	 final String v0 = "=MI LMS= "; // const-string v0, "=MI LMS= "
	 /* nop */
	 /* :pswitch_data_0 */
	 /* .packed-switch 0x1 */
	 /* :pswitch_2 */
	 /* :pswitch_1 */
	 /* :pswitch_0 */
} // .end packed-switch
} // .end method
/* # virtual methods */
public void addToBugreport ( Integer p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "type" # I */
/* .param p2, "log" # Ljava/lang/String; */
/* .line 32 */
/* invoke-direct {p0, p1}, Lcom/android/server/location/LocationDumpLogImpl;->switchTypeToDump(I)Lcom/android/server/location/GnssLocalLog; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* invoke-direct {p0, p1}, Lcom/android/server/location/LocationDumpLogImpl;->switchTypeToLogTag(I)Ljava/lang/String; */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.android.server.location.GnssLocalLog ) v0 ).log ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/location/GnssLocalLog;->log(Ljava/lang/String;)V
/* .line 33 */
return;
} // .end method
public void clearData ( ) {
/* .locals 1 */
/* .line 114 */
v0 = com.android.server.location.LocationDumpLogImpl.mdumpGlp;
(( com.android.server.location.GnssLocalLog ) v0 ).clearData ( ); // invoke-virtual {v0}, Lcom/android/server/location/GnssLocalLog;->clearData()V
/* .line 115 */
v0 = com.android.server.location.LocationDumpLogImpl.mdumpNmea;
(( com.android.server.location.GnssLocalLog ) v0 ).clearData ( ); // invoke-virtual {v0}, Lcom/android/server/location/GnssLocalLog;->clearData()V
/* .line 116 */
return;
} // .end method
public void dump ( Integer p0, java.io.PrintWriter p1 ) {
/* .locals 1 */
/* .param p1, "type" # I */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .line 76 */
/* invoke-direct {p0, p1}, Lcom/android/server/location/LocationDumpLogImpl;->switchTypeToDump(I)Lcom/android/server/location/GnssLocalLog; */
(( com.android.server.location.GnssLocalLog ) v0 ).dump ( p2 ); // invoke-virtual {v0, p2}, Lcom/android/server/location/GnssLocalLog;->dump(Ljava/io/PrintWriter;)V
/* .line 77 */
return;
} // .end method
public java.lang.String getConfig ( java.util.Properties p0, java.lang.String p1, java.lang.String p2 ) {
/* .locals 1 */
/* .param p1, "properties" # Ljava/util/Properties; */
/* .param p2, "config" # Ljava/lang/String; */
/* .param p3, "defaultConfig" # Ljava/lang/String; */
/* .line 42 */
(( java.util.Properties ) p1 ).getProperty ( p2, p3 ); // invoke-virtual {p1, p2, p3}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
} // .end method
public Integer getDefaultProviderName ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "type" # Ljava/lang/String; */
/* .line 57 */
v0 = /* invoke-direct {p0}, Lcom/android/server/location/LocationDumpLogImpl;->isXOptMode()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
	 v0 = 	 /* invoke-direct {p0}, Lcom/android/server/location/LocationDumpLogImpl;->isCnVersion()Z */
	 /* if-nez v0, :cond_0 */
	 /* .line 58 */
	 /* const v0, 0x10402b8 */
	 /* iput v0, p0, Lcom/android/server/location/LocationDumpLogImpl;->defaultNetworkProviderName:I */
	 /* .line 59 */
	 /* iput v0, p0, Lcom/android/server/location/LocationDumpLogImpl;->defaultFusedProviderName:I */
	 /* .line 61 */
} // :cond_0
v0 = (( java.lang.String ) p1 ).hashCode ( ); // invoke-virtual {p1}, Ljava/lang/String;->hashCode()I
/* sparse-switch v0, :sswitch_data_0 */
} // :cond_1
/* :sswitch_0 */
final String v0 = "network"; // const-string v0, "network"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
int v0 = 0; // const/4 v0, 0x0
/* :sswitch_1 */
final String v0 = "geofence"; // const-string v0, "geofence"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
	 int v0 = 3; // const/4 v0, 0x3
	 /* :sswitch_2 */
	 final String v0 = "geocoder"; // const-string v0, "geocoder"
	 v0 = 	 (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 int v0 = 2; // const/4 v0, 0x2
		 /* :sswitch_3 */
		 final String v0 = "fused"; // const-string v0, "fused"
		 v0 = 		 (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v0 != null) { // if-eqz v0, :cond_1
			 int v0 = 1; // const/4 v0, 0x1
		 } // :goto_0
		 int v0 = -1; // const/4 v0, -0x1
	 } // :goto_1
	 /* packed-switch v0, :pswitch_data_0 */
	 /* .line 70 */
	 /* iget v0, p0, Lcom/android/server/location/LocationDumpLogImpl;->defaultGeofenceProviderName:I */
	 /* .line 67 */
	 /* :pswitch_0 */
	 /* iget v0, p0, Lcom/android/server/location/LocationDumpLogImpl;->defaultGeocoderProviderName:I */
	 /* .line 65 */
	 /* :pswitch_1 */
	 /* iget v0, p0, Lcom/android/server/location/LocationDumpLogImpl;->defaultFusedProviderName:I */
	 /* .line 63 */
	 /* :pswitch_2 */
	 /* iget v0, p0, Lcom/android/server/location/LocationDumpLogImpl;->defaultNetworkProviderName:I */
	 /* nop */
	 /* :sswitch_data_0 */
	 /* .sparse-switch */
	 /* 0x5d44923 -> :sswitch_3 */
	 /* 0x6d7f6b74 -> :sswitch_2 */
	 /* 0x6da54b80 -> :sswitch_1 */
	 /* 0x6de15a2e -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public Boolean getRecordLoseLocation ( ) {
/* .locals 1 */
/* .line 47 */
/* iget-boolean v0, p0, Lcom/android/server/location/LocationDumpLogImpl;->mRecordLoseCount:Z */
} // .end method
public void setLength ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "type" # I */
/* .param p2, "length" # I */
/* .line 37 */
/* invoke-direct {p0, p1}, Lcom/android/server/location/LocationDumpLogImpl;->switchTypeToDump(I)Lcom/android/server/location/GnssLocalLog; */
(( com.android.server.location.GnssLocalLog ) v0 ).setLength ( p2 ); // invoke-virtual {v0, p2}, Lcom/android/server/location/GnssLocalLog;->setLength(I)I
/* .line 38 */
return;
} // .end method
public void setRecordLoseLocation ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "newValue" # Z */
/* .line 52 */
/* iput-boolean p1, p0, Lcom/android/server/location/LocationDumpLogImpl;->mRecordLoseCount:Z */
/* .line 53 */
return;
} // .end method
