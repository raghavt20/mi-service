.class Lcom/android/server/location/GnssSmartSatelliteSwitchImpl$1;
.super Landroid/database/ContentObserver;
.source "GnssSmartSatelliteSwitchImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->addCloudControllListener(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;


# direct methods
.method constructor <init>(Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 87
    iput-object p1, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl$1;->this$0:Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 6
    .param p1, "selfChange"    # Z

    .line 90
    const/4 v0, 0x0

    const-string v1, "persist.sys.gps.support_block_list_prop"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 91
    .local v0, "settingsNow":Z
    iget-object v2, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl$1;->this$0:Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;

    invoke-static {v2}, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->-$$Nest$fgetmContext(Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;)Landroid/content/Context;

    move-result-object v2

    .line 92
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 91
    const-string v3, "mtkGnssConfig"

    const-string/jumbo v4, "smartSatelliteSwitch"

    const/4 v5, 0x0

    invoke-static {v2, v3, v4, v5}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 96
    .local v2, "newSettings":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 97
    return-void

    .line 99
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "receiver new config, new value: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", settingsNow: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "GnssSmartSatelliteSwitchImpl"

    invoke-static {v4, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    invoke-static {v2}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v3

    .line 102
    .local v3, "newSettingsBool":Z
    if-ne v3, v0, :cond_1

    .line 103
    return-void

    .line 105
    :cond_1
    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    return-void
.end method
