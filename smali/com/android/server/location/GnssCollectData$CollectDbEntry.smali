.class public Lcom/android/server/location/GnssCollectData$CollectDbEntry;
.super Ljava/lang/Object;
.source "GnssCollectData.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/location/GnssCollectData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CollectDbEntry"
.end annotation


# static fields
.field public static final COLUMN_NAME_B1TOP4MEANCN0:Ljava/lang/String; = "B1Top4MeanCn0"

.field public static final COLUMN_NAME_L1TOP4MEANCN0:Ljava/lang/String; = "L1Top4MeanCn0"

.field public static final COLUMN_NAME_L5TOP4MEANCN0:Ljava/lang/String; = "L5Top4MeanCn0"

.field public static final COLUMN_NAME_LOSETIMES:Ljava/lang/String; = "loseTimes"

.field public static final COLUMN_NAME_PDRNUMBER:Ljava/lang/String; = "PDRNumber"

.field public static final COLUMN_NAME_RUNTIME:Ljava/lang/String; = "runTime"

.field public static final COLUMN_NAME_SAPNUMBER:Ljava/lang/String; = "SAPNumber"

.field public static final COLUMN_NAME_STARTTIME:Ljava/lang/String; = "startTime"

.field public static final COLUMN_NAME_TOTALNUMBER:Ljava/lang/String; = "totalNumber"

.field public static final COLUMN_NAME_TTFF:Ljava/lang/String; = "TTFF"

.field public static final TABLE_NAME:Ljava/lang/String; = "GnssCollectData"


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
