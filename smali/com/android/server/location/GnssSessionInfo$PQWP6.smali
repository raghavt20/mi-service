.class Lcom/android/server/location/GnssSessionInfo$PQWP6;
.super Ljava/lang/Object;
.source "GnssSessionInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/location/GnssSessionInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PQWP6"
.end annotation


# instance fields
.field private mNmea:Ljava/lang/String;

.field private pdrEnaged:Z

.field private sapEnaged:Z

.field private sub1:[Ljava/lang/String;

.field private sub2:[Ljava/lang/String;

.field final synthetic this$0:Lcom/android/server/location/GnssSessionInfo;


# direct methods
.method public constructor <init>(Lcom/android/server/location/GnssSessionInfo;Ljava/lang/String;)V
    .locals 0
    .param p2, "nmea"    # Ljava/lang/String;

    .line 244
    iput-object p1, p0, Lcom/android/server/location/GnssSessionInfo$PQWP6;->this$0:Lcom/android/server/location/GnssSessionInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 241
    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/android/server/location/GnssSessionInfo$PQWP6;->sapEnaged:Z

    .line 242
    iput-boolean p1, p0, Lcom/android/server/location/GnssSessionInfo$PQWP6;->pdrEnaged:Z

    .line 245
    iput-object p2, p0, Lcom/android/server/location/GnssSessionInfo$PQWP6;->mNmea:Ljava/lang/String;

    .line 246
    return-void
.end method


# virtual methods
.method public isPdrEngaged()Z
    .locals 1

    .line 264
    iget-boolean v0, p0, Lcom/android/server/location/GnssSessionInfo$PQWP6;->pdrEnaged:Z

    return v0
.end method

.method public isSapEnaged()Z
    .locals 1

    .line 260
    iget-boolean v0, p0, Lcom/android/server/location/GnssSessionInfo$PQWP6;->sapEnaged:Z

    return v0
.end method

.method public parse()V
    .locals 5

    .line 249
    iget-object v0, p0, Lcom/android/server/location/GnssSessionInfo$PQWP6;->mNmea:Ljava/lang/String;

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/location/GnssSessionInfo$PQWP6;->sub1:[Ljava/lang/String;

    .line 250
    array-length v1, v0

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    .line 251
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Error nmea is: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/location/GnssSessionInfo$PQWP6;->mNmea:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GnssSessionInfo"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    return-void

    .line 254
    :cond_0
    const/4 v1, 0x2

    aget-object v0, v0, v1

    const-string v2, "\\*"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/location/GnssSessionInfo$PQWP6;->sub2:[Ljava/lang/String;

    .line 255
    const/4 v2, 0x0

    aget-object v0, v0, v2

    const/16 v3, 0x10

    invoke-static {v0, v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    const/4 v4, 0x1

    shr-int/2addr v0, v4

    and-int/2addr v0, v4

    if-ne v0, v4, :cond_1

    move v0, v4

    goto :goto_0

    :cond_1
    move v0, v2

    :goto_0
    iput-boolean v0, p0, Lcom/android/server/location/GnssSessionInfo$PQWP6;->sapEnaged:Z

    .line 256
    iget-object v0, p0, Lcom/android/server/location/GnssSessionInfo$PQWP6;->sub2:[Ljava/lang/String;

    aget-object v0, v0, v2

    invoke-static {v0, v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    shr-int/2addr v0, v1

    and-int/2addr v0, v4

    if-ne v0, v4, :cond_2

    move v2, v4

    :cond_2
    iput-boolean v2, p0, Lcom/android/server/location/GnssSessionInfo$PQWP6;->pdrEnaged:Z

    .line 257
    return-void
.end method
