public class com.android.server.location.RsaUtil {
	 /* .source "RsaUtil.java" */
	 /* # static fields */
	 public static final java.lang.String KEY_ALGORITHM;
	 private static final Integer MAX_ENCRYPT_BLOCK;
	 public static final java.lang.String publicKeyStr;
	 /* # direct methods */
	 public com.android.server.location.RsaUtil ( ) {
		 /* .locals 0 */
		 /* .line 23 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static encryptByPublicKey ( Object[] p0 ) {
		 /* .locals 10 */
		 /* .param p0, "data" # [B */
		 /* .line 40 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* new-array v1, v0, [B */
		 /* .line 42 */
		 /* .local v1, "encryptedData":[B */
		 try { // :try_start_0
			 com.android.server.location.RsaUtil .getPublicKey ( );
			 /* .line 43 */
			 /* .local v2, "publicKey":Ljava/security/PublicKey; */
			 int v3 = 0; // const/4 v3, 0x0
			 /* .line 44 */
			 /* .local v3, "cipher":Ljavax/crypto/Cipher; */
			 final String v4 = "RSA/ECB/PKCS1Padding"; // const-string v4, "RSA/ECB/PKCS1Padding"
			 javax.crypto.Cipher .getInstance ( v4 );
			 /* move-object v3, v4 */
			 /* .line 45 */
			 int v4 = 1; // const/4 v4, 0x1
			 (( javax.crypto.Cipher ) v3 ).init ( v4, v2 ); // invoke-virtual {v3, v4, v2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V
			 /* .line 46 */
			 /* array-length v4, p0 */
			 /* .line 47 */
			 /* .local v4, "inputLen":I */
			 /* new-instance v5, Ljava/io/ByteArrayOutputStream; */
			 /* invoke-direct {v5}, Ljava/io/ByteArrayOutputStream;-><init>()V */
			 /* .line 48 */
			 /* .local v5, "out":Ljava/io/ByteArrayOutputStream; */
			 int v6 = 0; // const/4 v6, 0x0
			 /* .line 50 */
			 /* .local v6, "offSet":I */
			 int v7 = 0; // const/4 v7, 0x0
			 /* .line 51 */
			 /* .local v7, "i":I */
		 } // :goto_0
		 /* sub-int v8, v4, v6 */
		 /* if-lez v8, :cond_1 */
		 /* .line 52 */
		 /* sub-int v8, v4, v6 */
		 /* const/16 v9, 0x75 */
		 /* if-le v8, v9, :cond_0 */
		 /* .line 53 */
		 (( javax.crypto.Cipher ) v3 ).doFinal ( p0, v6, v9 ); // invoke-virtual {v3, p0, v6, v9}, Ljavax/crypto/Cipher;->doFinal([BII)[B
		 /* .local v8, "cache":[B */
		 /* .line 55 */
	 } // .end local v8 # "cache":[B
} // :cond_0
/* sub-int v8, v4, v6 */
(( javax.crypto.Cipher ) v3 ).doFinal ( p0, v6, v8 ); // invoke-virtual {v3, p0, v6, v8}, Ljavax/crypto/Cipher;->doFinal([BII)[B
/* .line 57 */
/* .restart local v8 # "cache":[B */
} // :goto_1
/* array-length v9, v8 */
(( java.io.ByteArrayOutputStream ) v5 ).write ( v8, v0, v9 ); // invoke-virtual {v5, v8, v0, v9}, Ljava/io/ByteArrayOutputStream;->write([BII)V
/* .line 58 */
/* add-int/lit8 v7, v7, 0x1 */
/* .line 59 */
/* mul-int/lit8 v6, v7, 0x75 */
/* .line 61 */
} // .end local v8 # "cache":[B
} // :cond_1
(( java.io.ByteArrayOutputStream ) v5 ).toByteArray ( ); // invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
/* move-object v1, v0 */
/* .line 62 */
(( java.io.ByteArrayOutputStream ) v5 ).close ( ); // invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->close()V
/* :try_end_0 */
/* .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 ..:try_end_0} :catch_2 */
/* .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
} // .end local v2 # "publicKey":Ljava/security/PublicKey;
} // .end local v3 # "cipher":Ljavax/crypto/Cipher;
} // .end local v4 # "inputLen":I
} // .end local v5 # "out":Ljava/io/ByteArrayOutputStream;
} // .end local v6 # "offSet":I
} // .end local v7 # "i":I
/* .line 67 */
/* :catch_0 */
/* move-exception v0 */
/* .line 68 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 65 */
} // .end local v0 # "e":Ljava/lang/Exception;
/* :catch_1 */
/* move-exception v0 */
/* .line 66 */
/* .local v0, "e":Ljavax/crypto/NoSuchPaddingException; */
(( javax.crypto.NoSuchPaddingException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljavax/crypto/NoSuchPaddingException;->printStackTrace()V
} // .end local v0 # "e":Ljavax/crypto/NoSuchPaddingException;
/* .line 63 */
/* :catch_2 */
/* move-exception v0 */
/* .line 64 */
/* .local v0, "e":Ljava/security/NoSuchAlgorithmException; */
(( java.security.NoSuchAlgorithmException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V
/* .line 69 */
} // .end local v0 # "e":Ljava/security/NoSuchAlgorithmException;
} // :goto_2
/* nop */
/* .line 70 */
} // :goto_3
} // .end method
public static java.security.PublicKey getPublicKey ( ) {
/* .locals 4 */
/* .line 74 */
int v0 = 0; // const/4 v0, 0x0
/* .line 76 */
/* .local v0, "publicKey":Ljava/security/PublicKey; */
try { // :try_start_0
final String v1 = "RSA"; // const-string v1, "RSA"
java.security.KeyFactory .getInstance ( v1 );
/* .line 77 */
/* .local v1, "keyFactory":Ljava/security/KeyFactory; */
final String v2 = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCsThShFHYm1FvbAD2x37QhCW5u\nwm75AZwxNqlZJRL+dc67b7U6y2aBpt5TACYs7eKvslYO7WNVAc8smgt3NL7GyBeR\n1cBaowUTcXkOQYzahqhd3Y0qbz6bvGeakzSeCYXQh4kknkdt64K/EI4QvyKTKmdz\nCVOG8VFnc7fuH+uWSQIDAQAB\n"; // const-string v2, "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCsThShFHYm1FvbAD2x37QhCW5u\nwm75AZwxNqlZJRL+dc67b7U6y2aBpt5TACYs7eKvslYO7WNVAc8smgt3NL7GyBeR\n1cBaowUTcXkOQYzahqhd3Y0qbz6bvGeakzSeCYXQh4kknkdt64K/EI4QvyKTKmdz\nCVOG8VFnc7fuH+uWSQIDAQAB\n"
(( java.lang.String ) v2 ).getBytes ( ); // invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B
int v3 = 0; // const/4 v3, 0x0
android.util.Base64 .decode ( v2,v3 );
/* .line 78 */
/* .local v2, "base64Bytes":[B */
/* new-instance v3, Ljava/security/spec/X509EncodedKeySpec; */
/* invoke-direct {v3, v2}, Ljava/security/spec/X509EncodedKeySpec;-><init>([B)V */
(( java.security.KeyFactory ) v1 ).generatePublic ( v3 ); // invoke-virtual {v1, v3}, Ljava/security/KeyFactory;->generatePublic(Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;
/* :try_end_0 */
/* .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/security/spec/InvalidKeySpecException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object v0, v3 */
/* .line 83 */
} // .end local v1 # "keyFactory":Ljava/security/KeyFactory;
} // .end local v2 # "base64Bytes":[B
} // :goto_0
/* .line 81 */
/* :catch_0 */
/* move-exception v1 */
/* .line 82 */
/* .local v1, "e":Ljava/security/spec/InvalidKeySpecException; */
(( java.security.spec.InvalidKeySpecException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/security/spec/InvalidKeySpecException;->printStackTrace()V
/* .line 79 */
} // .end local v1 # "e":Ljava/security/spec/InvalidKeySpecException;
/* :catch_1 */
/* move-exception v1 */
/* .line 80 */
/* .local v1, "e":Ljava/security/NoSuchAlgorithmException; */
(( java.security.NoSuchAlgorithmException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V
} // .end local v1 # "e":Ljava/security/NoSuchAlgorithmException;
/* .line 84 */
} // :goto_1
} // .end method
