public class com.android.server.location.GnssCollectDataImpl implements com.android.server.location.gnss.GnssCollectDataStub {
	 /* .source "GnssCollectDataImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final java.lang.String ACTION_COLLECT_DATA;
	 private static final java.lang.String BO_DEF_VAL;
	 private static final java.lang.String CLOUDGPOKEY;
	 private static final java.lang.String CLOUDGSRKEY;
	 private static final java.lang.String CLOUDKEY;
	 private static final java.lang.String CLOUDKEYSUPL;
	 private static final java.lang.String CLOUDMODULE;
	 private static final java.lang.String CLOUDSDKKEY;
	 private static final java.lang.String CLOUD_MODULE_NLP;
	 private static final java.lang.String CLOUD_MODULE_RTK;
	 private static final java.lang.String CN_MCC;
	 private static final java.lang.String COLLECT_DATA_PATH;
	 private static final Boolean DEBUG;
	 private static final java.lang.String DEF_VAL_GSCO;
	 private static final java.lang.String GMO_DEF_VAL;
	 private static final java.lang.String GMO_DISABLE;
	 private static final java.lang.String GMO_ENABLE;
	 private static final java.lang.String GNSS_GSR_SWITCH;
	 private static final java.lang.String GNSS_MQS_SWITCH;
	 public static final Integer INFO_B1CN0_TOP4;
	 public static final Integer INFO_E1CN0_TOP4;
	 public static final Integer INFO_G1CN0_TOP4;
	 public static final Integer INFO_L1CN0_TOP4;
	 public static final Integer INFO_L5CN0_TOP4;
	 public static final Integer INFO_NMEA_PQWP6;
	 private static final java.lang.String IS_COLLECT;
	 private static final Boolean IS_STABLE_VERSION;
	 private static final java.lang.String KEY_CLOUD_BO;
	 private static final java.lang.String KEY_CLOUD_GMO;
	 private static final java.lang.String KEY_CLOUD_GSCO_PKG;
	 private static final java.lang.String KEY_CLOUD_GSCO_STATUS;
	 private static final java.lang.String KEY_DISABLE;
	 private static final java.lang.String KEY_ENABLE;
	 private static final java.lang.String RTK_SWITCH;
	 public static final Integer SIGNAL_ERROR_CODE;
	 public static final Integer STATE_FIX;
	 public static final Integer STATE_INIT;
	 public static final Integer STATE_LOSE;
	 public static final Integer STATE_SAVE;
	 public static final Integer STATE_START;
	 public static final Integer STATE_STOP;
	 public static final Integer STATE_UNKNOWN;
	 private static final java.lang.String SUPL_SWITCH;
	 public static final Integer SV_ERROR_CODE;
	 private static final java.lang.String TAG;
	 public static Integer mCurrentState;
	 private static java.lang.String mMqsGpsModuleId;
	 /* # instance fields */
	 private Integer AmapBackCount;
	 private Integer BaiduBackCount;
	 private Integer SV_ERROR_SEND_INTERVAL;
	 private Integer TencentBackCount;
	 private Integer UPLOAD_REPEAT_TIME;
	 private Boolean hasStartUploadData;
	 private android.content.Context mContext;
	 private com.android.server.location.GnssCollectDataDbDao mGnssCollectDataDbDao;
	 private android.os.Handler mHandler;
	 private android.os.HandlerThread mHandlerThread;
	 private Boolean mIsCnSim;
	 private org.json.JSONArray mJsonArray;
	 private Long mLastSvTime;
	 private miui.mqsas.sdk.MQSEventManagerDelegate mMqsEventManagerDelegate;
	 private Integer mNumBEIDOUUsedInFix;
	 private Integer mNumGALILEOUsedInFix;
	 private Integer mNumGLONASSUsedInFix;
	 private Integer mNumGPSUsedInFix;
	 private final android.content.BroadcastReceiver mReceiver;
	 private com.android.server.location.GnssSessionInfo mSessionInfo;
	 private Integer mSvUsedInFix;
	 /* # direct methods */
	 static void -$$Nest$mparseNmea ( com.android.server.location.GnssCollectDataImpl p0, java.lang.String p1 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0, p1}, Lcom/android/server/location/GnssCollectDataImpl;->parseNmea(Ljava/lang/String;)V */
		 return;
	 } // .end method
	 static void -$$Nest$msaveFixStatus ( com.android.server.location.GnssCollectDataImpl p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0}, Lcom/android/server/location/GnssCollectDataImpl;->saveFixStatus()V */
		 return;
	 } // .end method
	 static void -$$Nest$msaveLog ( com.android.server.location.GnssCollectDataImpl p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0}, Lcom/android/server/location/GnssCollectDataImpl;->saveLog()V */
		 return;
	 } // .end method
	 static void -$$Nest$msaveLoseStatus ( com.android.server.location.GnssCollectDataImpl p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0}, Lcom/android/server/location/GnssCollectDataImpl;->saveLoseStatus()V */
		 return;
	 } // .end method
	 static void -$$Nest$msaveStartStatus ( com.android.server.location.GnssCollectDataImpl p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0}, Lcom/android/server/location/GnssCollectDataImpl;->saveStartStatus()V */
		 return;
	 } // .end method
	 static void -$$Nest$msaveState ( com.android.server.location.GnssCollectDataImpl p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0}, Lcom/android/server/location/GnssCollectDataImpl;->saveState()V */
		 return;
	 } // .end method
	 static void -$$Nest$msaveStopStatus ( com.android.server.location.GnssCollectDataImpl p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0}, Lcom/android/server/location/GnssCollectDataImpl;->saveStopStatus()V */
		 return;
	 } // .end method
	 static void -$$Nest$msetB1Cn0 ( com.android.server.location.GnssCollectDataImpl p0, Double p1 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0, p1, p2}, Lcom/android/server/location/GnssCollectDataImpl;->setB1Cn0(D)V */
		 return;
	 } // .end method
	 static void -$$Nest$msetCurrentState ( com.android.server.location.GnssCollectDataImpl p0, Integer p1 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0, p1}, Lcom/android/server/location/GnssCollectDataImpl;->setCurrentState(I)V */
		 return;
	 } // .end method
	 static void -$$Nest$msetL1Cn0 ( com.android.server.location.GnssCollectDataImpl p0, Double p1 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0, p1, p2}, Lcom/android/server/location/GnssCollectDataImpl;->setL1Cn0(D)V */
		 return;
	 } // .end method
	 static void -$$Nest$msetL5Cn0 ( com.android.server.location.GnssCollectDataImpl p0, Double p1 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0, p1, p2}, Lcom/android/server/location/GnssCollectDataImpl;->setL5Cn0(D)V */
		 return;
	 } // .end method
	 static void -$$Nest$mstartUploadFixData ( com.android.server.location.GnssCollectDataImpl p0, android.content.Context p1 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0, p1}, Lcom/android/server/location/GnssCollectDataImpl;->startUploadFixData(Landroid/content/Context;)V */
		 return;
	 } // .end method
	 static void -$$Nest$mupdateControlState ( com.android.server.location.GnssCollectDataImpl p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0}, Lcom/android/server/location/GnssCollectDataImpl;->updateControlState()V */
		 return;
	 } // .end method
	 static com.android.server.location.GnssCollectDataImpl ( ) {
		 /* .locals 1 */
		 /* .line 103 */
		 /* const/16 v0, 0x64 */
		 /* .line 105 */
		 final String v0 = "mqs_gps_data_63921000"; // const-string v0, "mqs_gps_data_63921000"
		 /* .line 106 */
		 /* sget-boolean v0, Lmiui/os/Build;->IS_STABLE_VERSION:Z */
		 com.android.server.location.GnssCollectDataImpl.IS_STABLE_VERSION = (v0!= 0);
		 return;
	 } // .end method
	 public com.android.server.location.GnssCollectDataImpl ( ) {
		 /* .locals 3 */
		 /* .line 46 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 111 */
		 miui.mqsas.sdk.MQSEventManagerDelegate .getInstance ( );
		 this.mMqsEventManagerDelegate = v0;
		 /* .line 112 */
		 /* new-instance v0, Lcom/android/server/location/GnssSessionInfo; */
		 /* invoke-direct {v0}, Lcom/android/server/location/GnssSessionInfo;-><init>()V */
		 this.mSessionInfo = v0;
		 /* .line 113 */
		 /* const v0, 0x927c0 */
		 /* iput v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->SV_ERROR_SEND_INTERVAL:I */
		 /* .line 114 */
		 /* const v0, 0x5265c00 */
		 /* iput v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->UPLOAD_REPEAT_TIME:I */
		 /* .line 115 */
		 /* new-instance v0, Lorg/json/JSONArray; */
		 /* invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V */
		 this.mJsonArray = v0;
		 /* .line 116 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* iput-boolean v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mIsCnSim:Z */
		 /* .line 118 */
		 /* const-wide/32 v1, -0x927c0 */
		 /* iput-wide v1, p0, Lcom/android/server/location/GnssCollectDataImpl;->mLastSvTime:J */
		 /* .line 119 */
		 /* iput v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mSvUsedInFix:I */
		 /* .line 120 */
		 /* iput v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mNumGPSUsedInFix:I */
		 /* .line 121 */
		 /* iput v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mNumBEIDOUUsedInFix:I */
		 /* .line 122 */
		 /* iput v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mNumGLONASSUsedInFix:I */
		 /* .line 123 */
		 /* iput v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mNumGALILEOUsedInFix:I */
		 /* .line 124 */
		 /* iput v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->AmapBackCount:I */
		 /* .line 125 */
		 /* iput v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->BaiduBackCount:I */
		 /* .line 126 */
		 /* iput v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->TencentBackCount:I */
		 /* .line 895 */
		 /* new-instance v0, Lcom/android/server/location/GnssCollectDataImpl$3; */
		 /* invoke-direct {v0, p0}, Lcom/android/server/location/GnssCollectDataImpl$3;-><init>(Lcom/android/server/location/GnssCollectDataImpl;)V */
		 this.mReceiver = v0;
		 return;
	 } // .end method
	 private Boolean allowCollect ( ) {
		 /* .locals 2 */
		 /* .line 135 */
		 final String v0 = "persist.sys.mqs.gps"; // const-string v0, "persist.sys.mqs.gps"
		 final String v1 = "1"; // const-string v1, "1"
		 android.os.SystemProperties .get ( v0,v1 );
		 v0 = 		 (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 } // .end method
	 private Boolean isL1Sv ( Float p0 ) {
		 /* .locals 4 */
		 /* .param p1, "carrierFreq" # F */
		 /* .line 422 */
		 /* float-to-double v0, p1 */
		 /* const-wide v2, 0x41d73b1cf0000000L # 1.559E9 */
		 /* cmpl-double v0, v0, v2 */
		 /* if-ltz v0, :cond_0 */
		 /* float-to-double v0, p1 */
		 /* const-wide v2, 0x41d7fda9a0000000L # 1.61E9 */
		 /* cmpg-double v0, v0, v2 */
		 /* if-gtz v0, :cond_0 */
		 int v0 = 1; // const/4 v0, 0x1
	 } // :cond_0
	 int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private Boolean isL5Sv ( Float p0 ) {
/* .locals 4 */
/* .param p1, "carrierFreq" # F */
/* .line 427 */
/* float-to-double v0, p1 */
/* const-wide v2, 0x41d1584ec0000000L # 1.164E9 */
/* cmpl-double v0, v0, v2 */
/* if-ltz v0, :cond_0 */
/* float-to-double v0, p1 */
/* const-wide v2, 0x41d1b7acd0000000L # 1.189E9 */
/* cmpg-double v0, v0, v2 */
/* if-gtz v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private java.lang.String packToJsonArray ( ) {
/* .locals 7 */
/* .line 163 */
/* new-instance v0, Lorg/json/JSONObject; */
/* invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V */
/* .line 165 */
/* .local v0, "jsonObj":Lorg/json/JSONObject; */
v1 = this.mSessionInfo;
(( com.android.server.location.GnssSessionInfo ) v1 ).getTtff ( ); // invoke-virtual {v1}, Lcom/android/server/location/GnssSessionInfo;->getTtff()J
/* move-result-wide v1 */
/* const-wide/16 v3, 0x0 */
/* cmp-long v1, v1, v3 */
final String v2 = "GnssCD"; // const-string v2, "GnssCD"
/* if-gez v1, :cond_0 */
/* .line 166 */
final String v1 = "abnormal data"; // const-string v1, "abnormal data"
android.util.Log .d ( v2,v1 );
/* .line 167 */
int v1 = 0; // const/4 v1, 0x0
/* .line 169 */
} // :cond_0
v1 = this.mSessionInfo;
(( com.android.server.location.GnssSessionInfo ) v1 ).getL1Top4Cn0Mean ( ); // invoke-virtual {v1}, Lcom/android/server/location/GnssSessionInfo;->getL1Top4Cn0Mean()D
/* move-result-wide v3 */
/* const-wide/high16 v5, 0x403c000000000000L # 28.0 */
/* cmpg-double v1, v3, v5 */
/* if-gez v1, :cond_1 */
/* .line 170 */
final String v1 = "SignalWeak"; // const-string v1, "SignalWeak"
final String v3 = "Signal weak"; // const-string v3, "Signal weak"
/* filled-new-array {v1, v3}, [Ljava/lang/Object; */
/* const v3, 0x36c725c9 */
com.miui.misight.MiSight .constructEvent ( v3,v1 );
/* .line 172 */
/* .local v1, "event":Lcom/miui/misight/MiEvent; */
com.miui.misight.MiSight .sendEvent ( v1 );
/* .line 175 */
} // .end local v1 # "event":Lcom/miui/misight/MiEvent;
} // :cond_1
try { // :try_start_0
/* const-string/jumbo v1, "startTime" */
v3 = this.mSessionInfo;
v3 = (( com.android.server.location.GnssSessionInfo ) v3 ).getStartTimeInHour ( ); // invoke-virtual {v3}, Lcom/android/server/location/GnssSessionInfo;->getStartTimeInHour()I
(( org.json.JSONObject ) v0 ).put ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 176 */
final String v1 = "TTFF"; // const-string v1, "TTFF"
v3 = this.mSessionInfo;
(( com.android.server.location.GnssSessionInfo ) v3 ).getTtff ( ); // invoke-virtual {v3}, Lcom/android/server/location/GnssSessionInfo;->getTtff()J
/* move-result-wide v3 */
(( org.json.JSONObject ) v0 ).put ( v1, v3, v4 ); // invoke-virtual {v0, v1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
/* .line 177 */
final String v1 = "runTime"; // const-string v1, "runTime"
v3 = this.mSessionInfo;
(( com.android.server.location.GnssSessionInfo ) v3 ).getRunTime ( ); // invoke-virtual {v3}, Lcom/android/server/location/GnssSessionInfo;->getRunTime()J
/* move-result-wide v3 */
(( org.json.JSONObject ) v0 ).put ( v1, v3, v4 ); // invoke-virtual {v0, v1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
/* .line 178 */
final String v1 = "loseTimes"; // const-string v1, "loseTimes"
v3 = this.mSessionInfo;
v3 = (( com.android.server.location.GnssSessionInfo ) v3 ).getLoseTimes ( ); // invoke-virtual {v3}, Lcom/android/server/location/GnssSessionInfo;->getLoseTimes()I
(( org.json.JSONObject ) v0 ).put ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 179 */
final String v1 = "L1Top4MeanCn0"; // const-string v1, "L1Top4MeanCn0"
v3 = this.mSessionInfo;
(( com.android.server.location.GnssSessionInfo ) v3 ).getL1Top4Cn0Mean ( ); // invoke-virtual {v3}, Lcom/android/server/location/GnssSessionInfo;->getL1Top4Cn0Mean()D
/* move-result-wide v3 */
(( org.json.JSONObject ) v0 ).put ( v1, v3, v4 ); // invoke-virtual {v0, v1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;
/* .line 180 */
final String v1 = "L5Top4MeanCn0"; // const-string v1, "L5Top4MeanCn0"
v3 = this.mSessionInfo;
(( com.android.server.location.GnssSessionInfo ) v3 ).getL5Top4Cn0Mean ( ); // invoke-virtual {v3}, Lcom/android/server/location/GnssSessionInfo;->getL5Top4Cn0Mean()D
/* move-result-wide v3 */
(( org.json.JSONObject ) v0 ).put ( v1, v3, v4 ); // invoke-virtual {v0, v1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;
/* .line 181 */
final String v1 = "B1Top4MeanCn0"; // const-string v1, "B1Top4MeanCn0"
v3 = this.mSessionInfo;
(( com.android.server.location.GnssSessionInfo ) v3 ).getB1Top4Cn0Mean ( ); // invoke-virtual {v3}, Lcom/android/server/location/GnssSessionInfo;->getB1Top4Cn0Mean()D
/* move-result-wide v3 */
(( org.json.JSONObject ) v0 ).put ( v1, v3, v4 ); // invoke-virtual {v0, v1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;
/* .line 182 */
final String v1 = "G1Top4MeanCn0"; // const-string v1, "G1Top4MeanCn0"
v3 = this.mSessionInfo;
(( com.android.server.location.GnssSessionInfo ) v3 ).getG1Top4Cn0Mean ( ); // invoke-virtual {v3}, Lcom/android/server/location/GnssSessionInfo;->getG1Top4Cn0Mean()D
/* move-result-wide v3 */
(( org.json.JSONObject ) v0 ).put ( v1, v3, v4 ); // invoke-virtual {v0, v1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;
/* .line 183 */
final String v1 = "E1Top4MeanCn0"; // const-string v1, "E1Top4MeanCn0"
v3 = this.mSessionInfo;
(( com.android.server.location.GnssSessionInfo ) v3 ).getE1Top4Cn0Mean ( ); // invoke-virtual {v3}, Lcom/android/server/location/GnssSessionInfo;->getE1Top4Cn0Mean()D
/* move-result-wide v3 */
(( org.json.JSONObject ) v0 ).put ( v1, v3, v4 ); // invoke-virtual {v0, v1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;
/* .line 184 */
v1 = this.mJsonArray;
(( org.json.JSONArray ) v1 ).put ( v0 ); // invoke-virtual {v1, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
/* .line 185 */
v1 = this.mJsonArray;
(( org.json.JSONArray ) v1 ).toString ( ); // invoke-virtual {v1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 193 */
/* .local v1, "jsonString":Ljava/lang/String; */
/* .line 190 */
} // .end local v1 # "jsonString":Ljava/lang/String;
/* :catch_0 */
/* move-exception v1 */
/* .line 191 */
/* .local v1, "e":Lorg/json/JSONException; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "JSON exception "; // const-string v4, "JSON exception "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v2,v3 );
/* .line 192 */
int v2 = 0; // const/4 v2, 0x0
/* move-object v1, v2 */
/* .line 195 */
/* .local v1, "jsonString":Ljava/lang/String; */
} // :goto_0
} // .end method
private void parseNmea ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "nmea" # Ljava/lang/String; */
/* .line 860 */
v0 = this.mSessionInfo;
(( com.android.server.location.GnssSessionInfo ) v0 ).parseNmea ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/location/GnssSessionInfo;->parseNmea(Ljava/lang/String;)V
/* .line 861 */
return;
} // .end method
private void registerControlListener ( ) {
/* .locals 4 */
/* .line 674 */
v0 = this.mContext;
/* if-nez v0, :cond_0 */
/* .line 675 */
final String v0 = "GnssCD"; // const-string v0, "GnssCD"
final String v1 = "no context"; // const-string v1, "no context"
android.util.Log .e ( v0,v1 );
/* .line 676 */
return;
/* .line 678 */
} // :cond_0
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 679 */
android.provider.MiuiSettings$SettingsCloudData .getCloudDataNotifyUri ( );
/* new-instance v2, Lcom/android/server/location/GnssCollectDataImpl$1; */
int v3 = 0; // const/4 v3, 0x0
/* invoke-direct {v2, p0, v3}, Lcom/android/server/location/GnssCollectDataImpl$1;-><init>(Lcom/android/server/location/GnssCollectDataImpl;Landroid/os/Handler;)V */
/* .line 678 */
int v3 = 1; // const/4 v3, 0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2 ); // invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 686 */
return;
} // .end method
private void saveB1Cn0 ( Integer p0, Float[] p1, Float[] p2, Float[] p3 ) {
/* .locals 7 */
/* .param p1, "svCount" # I */
/* .param p2, "cn0s" # [F */
/* .param p3, "svCarrierFreqs" # [F */
/* .param p4, "svConstellation" # [F */
/* .line 533 */
if ( p1 != null) { // if-eqz p1, :cond_7
if ( p2 != null) { // if-eqz p2, :cond_7
/* array-length v0, p2 */
if ( v0 != null) { // if-eqz v0, :cond_7
/* array-length v0, p2 */
/* if-lt v0, p1, :cond_7 */
if ( p3 != null) { // if-eqz p3, :cond_7
/* array-length v0, p3 */
if ( v0 != null) { // if-eqz v0, :cond_7
/* array-length v0, p3 */
/* if-ge v0, p1, :cond_0 */
/* .line 539 */
} // :cond_0
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 540 */
/* .local v0, "CnoB1Array":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Float;>;" */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* if-ge v1, p1, :cond_2 */
/* .line 541 */
/* aget v2, p3, v1 */
v2 = /* invoke-direct {p0, v2}, Lcom/android/server/location/GnssCollectDataImpl;->isL1Sv(F)Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* aget v2, p4, v1 */
/* const/high16 v3, 0x40a00000 # 5.0f */
/* cmpl-float v2, v2, v3 */
/* if-nez v2, :cond_1 */
/* .line 542 */
/* aget v2, p2, v1 */
java.lang.Float .valueOf ( v2 );
(( java.util.ArrayList ) v0 ).add ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 540 */
} // :cond_1
/* add-int/lit8 v1, v1, 0x1 */
/* .line 545 */
} // .end local v1 # "i":I
} // :cond_2
v1 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
if ( v1 != null) { // if-eqz v1, :cond_6
v1 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
int v2 = 4; // const/4 v2, 0x4
/* if-ge v1, v2, :cond_3 */
/* .line 548 */
} // :cond_3
v1 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* .line 549 */
/* .local v1, "numSvB1":I */
java.util.Collections .sort ( v0 );
/* .line 550 */
/* add-int/lit8 v2, v1, -0x4 */
(( java.util.ArrayList ) v0 ).get ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v2, Ljava/lang/Float; */
v2 = (( java.lang.Float ) v2 ).floatValue ( ); // invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F
/* float-to-double v2, v2 */
/* const-wide/16 v4, 0x0 */
/* cmpl-double v2, v2, v4 */
/* if-lez v2, :cond_5 */
/* .line 551 */
/* const-wide/16 v2, 0x0 */
/* .line 552 */
/* .local v2, "top4AvgCn0":D */
/* add-int/lit8 v4, v1, -0x4 */
/* .local v4, "i":I */
} // :goto_1
/* if-ge v4, v1, :cond_4 */
/* .line 553 */
(( java.util.ArrayList ) v0 ).get ( v4 ); // invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v5, Ljava/lang/Float; */
v5 = (( java.lang.Float ) v5 ).floatValue ( ); // invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F
/* float-to-double v5, v5 */
/* add-double/2addr v2, v5 */
/* .line 552 */
/* add-int/lit8 v4, v4, 0x1 */
/* .line 555 */
} // .end local v4 # "i":I
} // :cond_4
/* const-wide/high16 v4, 0x4010000000000000L # 4.0 */
/* div-double/2addr v2, v4 */
/* .line 556 */
/* const/16 v4, 0x9 */
java.lang.Double .valueOf ( v2,v3 );
/* invoke-direct {p0, v4, v5}, Lcom/android/server/location/GnssCollectDataImpl;->sendMessage(ILjava/lang/Object;)V */
/* .line 558 */
} // .end local v2 # "top4AvgCn0":D
} // :cond_5
return;
/* .line 546 */
} // .end local v1 # "numSvB1":I
} // :cond_6
} // :goto_2
return;
/* .line 536 */
} // .end local v0 # "CnoB1Array":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Float;>;"
} // :cond_7
} // :goto_3
return;
} // .end method
private void saveE1Cn0 ( Integer p0, Float[] p1, Float[] p2, Float[] p3 ) {
/* .locals 7 */
/* .param p1, "svCount" # I */
/* .param p2, "cn0s" # [F */
/* .param p3, "svCarrierFreqs" # [F */
/* .param p4, "svConstellation" # [F */
/* .line 591 */
if ( p1 != null) { // if-eqz p1, :cond_7
if ( p2 != null) { // if-eqz p2, :cond_7
/* array-length v0, p2 */
if ( v0 != null) { // if-eqz v0, :cond_7
/* array-length v0, p2 */
/* if-lt v0, p1, :cond_7 */
if ( p3 != null) { // if-eqz p3, :cond_7
/* array-length v0, p3 */
if ( v0 != null) { // if-eqz v0, :cond_7
/* array-length v0, p3 */
/* if-ge v0, p1, :cond_0 */
/* .line 597 */
} // :cond_0
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 598 */
/* .local v0, "CnoE1Array":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Float;>;" */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* if-ge v1, p1, :cond_2 */
/* .line 599 */
/* aget v2, p3, v1 */
v2 = /* invoke-direct {p0, v2}, Lcom/android/server/location/GnssCollectDataImpl;->isL1Sv(F)Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* aget v2, p4, v1 */
/* const/high16 v3, 0x40c00000 # 6.0f */
/* cmpl-float v2, v2, v3 */
/* if-nez v2, :cond_1 */
/* .line 600 */
/* aget v2, p2, v1 */
java.lang.Float .valueOf ( v2 );
(( java.util.ArrayList ) v0 ).add ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 598 */
} // :cond_1
/* add-int/lit8 v1, v1, 0x1 */
/* .line 603 */
} // .end local v1 # "i":I
} // :cond_2
v1 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
if ( v1 != null) { // if-eqz v1, :cond_6
v1 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
int v2 = 4; // const/4 v2, 0x4
/* if-ge v1, v2, :cond_3 */
/* .line 606 */
} // :cond_3
v1 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* .line 607 */
/* .local v1, "numSvE1":I */
java.util.Collections .sort ( v0 );
/* .line 608 */
/* add-int/lit8 v2, v1, -0x4 */
(( java.util.ArrayList ) v0 ).get ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v2, Ljava/lang/Float; */
v2 = (( java.lang.Float ) v2 ).floatValue ( ); // invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F
/* float-to-double v2, v2 */
/* const-wide/16 v4, 0x0 */
/* cmpl-double v2, v2, v4 */
/* if-lez v2, :cond_5 */
/* .line 609 */
/* const-wide/16 v2, 0x0 */
/* .line 610 */
/* .local v2, "top4AvgCn0":D */
/* add-int/lit8 v4, v1, -0x4 */
/* .local v4, "i":I */
} // :goto_1
/* if-ge v4, v1, :cond_4 */
/* .line 611 */
(( java.util.ArrayList ) v0 ).get ( v4 ); // invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v5, Ljava/lang/Float; */
v5 = (( java.lang.Float ) v5 ).floatValue ( ); // invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F
/* float-to-double v5, v5 */
/* add-double/2addr v2, v5 */
/* .line 610 */
/* add-int/lit8 v4, v4, 0x1 */
/* .line 613 */
} // .end local v4 # "i":I
} // :cond_4
/* const-wide/high16 v4, 0x4010000000000000L # 4.0 */
/* div-double/2addr v2, v4 */
/* .line 614 */
/* const/16 v4, 0xb */
java.lang.Double .valueOf ( v2,v3 );
/* invoke-direct {p0, v4, v5}, Lcom/android/server/location/GnssCollectDataImpl;->sendMessage(ILjava/lang/Object;)V */
/* .line 616 */
} // .end local v2 # "top4AvgCn0":D
} // :cond_5
return;
/* .line 604 */
} // .end local v1 # "numSvE1":I
} // :cond_6
} // :goto_2
return;
/* .line 594 */
} // .end local v0 # "CnoE1Array":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Float;>;"
} // :cond_7
} // :goto_3
return;
} // .end method
private void saveFixStatus ( ) {
/* .locals 1 */
/* .line 844 */
v0 = this.mSessionInfo;
(( com.android.server.location.GnssSessionInfo ) v0 ).setTtffAuto ( ); // invoke-virtual {v0}, Lcom/android/server/location/GnssSessionInfo;->setTtffAuto()V
/* .line 845 */
return;
} // .end method
private void saveG1Cn0 ( Integer p0, Float[] p1, Float[] p2, Float[] p3 ) {
/* .locals 7 */
/* .param p1, "svCount" # I */
/* .param p2, "cn0s" # [F */
/* .param p3, "svCarrierFreqs" # [F */
/* .param p4, "svConstellation" # [F */
/* .line 562 */
if ( p1 != null) { // if-eqz p1, :cond_7
if ( p2 != null) { // if-eqz p2, :cond_7
/* array-length v0, p2 */
if ( v0 != null) { // if-eqz v0, :cond_7
/* array-length v0, p2 */
/* if-lt v0, p1, :cond_7 */
if ( p3 != null) { // if-eqz p3, :cond_7
/* array-length v0, p3 */
if ( v0 != null) { // if-eqz v0, :cond_7
/* array-length v0, p3 */
/* if-ge v0, p1, :cond_0 */
/* .line 568 */
} // :cond_0
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 569 */
/* .local v0, "CnoG1Array":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Float;>;" */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* if-ge v1, p1, :cond_2 */
/* .line 570 */
/* aget v2, p3, v1 */
v2 = /* invoke-direct {p0, v2}, Lcom/android/server/location/GnssCollectDataImpl;->isL1Sv(F)Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* aget v2, p4, v1 */
/* const/high16 v3, 0x40400000 # 3.0f */
/* cmpl-float v2, v2, v3 */
/* if-nez v2, :cond_1 */
/* .line 571 */
/* aget v2, p2, v1 */
java.lang.Float .valueOf ( v2 );
(( java.util.ArrayList ) v0 ).add ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 569 */
} // :cond_1
/* add-int/lit8 v1, v1, 0x1 */
/* .line 574 */
} // .end local v1 # "i":I
} // :cond_2
v1 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
if ( v1 != null) { // if-eqz v1, :cond_6
v1 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
int v2 = 4; // const/4 v2, 0x4
/* if-ge v1, v2, :cond_3 */
/* .line 577 */
} // :cond_3
v1 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* .line 578 */
/* .local v1, "numSvG1":I */
java.util.Collections .sort ( v0 );
/* .line 579 */
/* add-int/lit8 v2, v1, -0x4 */
(( java.util.ArrayList ) v0 ).get ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v2, Ljava/lang/Float; */
v2 = (( java.lang.Float ) v2 ).floatValue ( ); // invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F
/* float-to-double v2, v2 */
/* const-wide/16 v4, 0x0 */
/* cmpl-double v2, v2, v4 */
/* if-lez v2, :cond_5 */
/* .line 580 */
/* const-wide/16 v2, 0x0 */
/* .line 581 */
/* .local v2, "top4AvgCn0":D */
/* add-int/lit8 v4, v1, -0x4 */
/* .local v4, "i":I */
} // :goto_1
/* if-ge v4, v1, :cond_4 */
/* .line 582 */
(( java.util.ArrayList ) v0 ).get ( v4 ); // invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v5, Ljava/lang/Float; */
v5 = (( java.lang.Float ) v5 ).floatValue ( ); // invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F
/* float-to-double v5, v5 */
/* add-double/2addr v2, v5 */
/* .line 581 */
/* add-int/lit8 v4, v4, 0x1 */
/* .line 584 */
} // .end local v4 # "i":I
} // :cond_4
/* const-wide/high16 v4, 0x4010000000000000L # 4.0 */
/* div-double/2addr v2, v4 */
/* .line 585 */
/* const/16 v4, 0xa */
java.lang.Double .valueOf ( v2,v3 );
/* invoke-direct {p0, v4, v5}, Lcom/android/server/location/GnssCollectDataImpl;->sendMessage(ILjava/lang/Object;)V */
/* .line 587 */
} // .end local v2 # "top4AvgCn0":D
} // :cond_5
return;
/* .line 575 */
} // .end local v1 # "numSvG1":I
} // :cond_6
} // :goto_2
return;
/* .line 565 */
} // .end local v0 # "CnoG1Array":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Float;>;"
} // :cond_7
} // :goto_3
return;
} // .end method
private void saveL1Cn0 ( Integer p0, Float[] p1, Float[] p2, Float[] p3 ) {
/* .locals 7 */
/* .param p1, "svCount" # I */
/* .param p2, "cn0s" # [F */
/* .param p3, "svCarrierFreqs" # [F */
/* .param p4, "svConstellation" # [F */
/* .line 475 */
if ( p1 != null) { // if-eqz p1, :cond_7
if ( p2 != null) { // if-eqz p2, :cond_7
/* array-length v0, p2 */
if ( v0 != null) { // if-eqz v0, :cond_7
/* array-length v0, p2 */
/* if-lt v0, p1, :cond_7 */
if ( p3 != null) { // if-eqz p3, :cond_7
/* array-length v0, p3 */
if ( v0 != null) { // if-eqz v0, :cond_7
/* array-length v0, p3 */
/* if-ge v0, p1, :cond_0 */
/* .line 481 */
} // :cond_0
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 482 */
/* .local v0, "CnoL1Array":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Float;>;" */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* if-ge v1, p1, :cond_2 */
/* .line 483 */
/* aget v2, p3, v1 */
v2 = /* invoke-direct {p0, v2}, Lcom/android/server/location/GnssCollectDataImpl;->isL1Sv(F)Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* aget v2, p4, v1 */
/* const/high16 v3, 0x3f800000 # 1.0f */
/* cmpl-float v2, v2, v3 */
/* if-nez v2, :cond_1 */
/* .line 484 */
/* aget v2, p2, v1 */
java.lang.Float .valueOf ( v2 );
(( java.util.ArrayList ) v0 ).add ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 482 */
} // :cond_1
/* add-int/lit8 v1, v1, 0x1 */
/* .line 487 */
} // .end local v1 # "i":I
} // :cond_2
v1 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
if ( v1 != null) { // if-eqz v1, :cond_6
v1 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
int v2 = 4; // const/4 v2, 0x4
/* if-ge v1, v2, :cond_3 */
/* .line 490 */
} // :cond_3
v1 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* .line 491 */
/* .local v1, "numSvL1":I */
java.util.Collections .sort ( v0 );
/* .line 492 */
/* add-int/lit8 v2, v1, -0x4 */
(( java.util.ArrayList ) v0 ).get ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v2, Ljava/lang/Float; */
v2 = (( java.lang.Float ) v2 ).floatValue ( ); // invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F
/* float-to-double v2, v2 */
/* const-wide/16 v4, 0x0 */
/* cmpl-double v2, v2, v4 */
/* if-lez v2, :cond_5 */
/* .line 493 */
/* const-wide/16 v2, 0x0 */
/* .line 494 */
/* .local v2, "top4AvgCn0":D */
/* add-int/lit8 v4, v1, -0x4 */
/* .local v4, "i":I */
} // :goto_1
/* if-ge v4, v1, :cond_4 */
/* .line 495 */
(( java.util.ArrayList ) v0 ).get ( v4 ); // invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v5, Ljava/lang/Float; */
v5 = (( java.lang.Float ) v5 ).floatValue ( ); // invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F
/* float-to-double v5, v5 */
/* add-double/2addr v2, v5 */
/* .line 494 */
/* add-int/lit8 v4, v4, 0x1 */
/* .line 497 */
} // .end local v4 # "i":I
} // :cond_4
/* const-wide/high16 v4, 0x4010000000000000L # 4.0 */
/* div-double/2addr v2, v4 */
/* .line 498 */
int v4 = 7; // const/4 v4, 0x7
java.lang.Double .valueOf ( v2,v3 );
/* invoke-direct {p0, v4, v5}, Lcom/android/server/location/GnssCollectDataImpl;->sendMessage(ILjava/lang/Object;)V */
/* .line 500 */
} // .end local v2 # "top4AvgCn0":D
} // :cond_5
return;
/* .line 488 */
} // .end local v1 # "numSvL1":I
} // :cond_6
} // :goto_2
return;
/* .line 478 */
} // .end local v0 # "CnoL1Array":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Float;>;"
} // :cond_7
} // :goto_3
return;
} // .end method
private void saveL5Cn0 ( Integer p0, Float[] p1, Float[] p2, Float[] p3 ) {
/* .locals 7 */
/* .param p1, "svCount" # I */
/* .param p2, "cn0s" # [F */
/* .param p3, "svCarrierFreqs" # [F */
/* .param p4, "svConstellation" # [F */
/* .line 504 */
if ( p1 != null) { // if-eqz p1, :cond_7
if ( p2 != null) { // if-eqz p2, :cond_7
/* array-length v0, p2 */
if ( v0 != null) { // if-eqz v0, :cond_7
/* array-length v0, p2 */
/* if-lt v0, p1, :cond_7 */
if ( p3 != null) { // if-eqz p3, :cond_7
/* array-length v0, p3 */
if ( v0 != null) { // if-eqz v0, :cond_7
/* array-length v0, p3 */
/* if-ge v0, p1, :cond_0 */
/* .line 510 */
} // :cond_0
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 511 */
/* .local v0, "CnoL5Array":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Float;>;" */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* if-ge v1, p1, :cond_2 */
/* .line 512 */
/* aget v2, p3, v1 */
v2 = /* invoke-direct {p0, v2}, Lcom/android/server/location/GnssCollectDataImpl;->isL5Sv(F)Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* aget v2, p4, v1 */
/* const/high16 v3, 0x3f800000 # 1.0f */
/* cmpl-float v2, v2, v3 */
/* if-nez v2, :cond_1 */
/* .line 513 */
/* aget v2, p2, v1 */
java.lang.Float .valueOf ( v2 );
(( java.util.ArrayList ) v0 ).add ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 511 */
} // :cond_1
/* add-int/lit8 v1, v1, 0x1 */
/* .line 516 */
} // .end local v1 # "i":I
} // :cond_2
v1 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
if ( v1 != null) { // if-eqz v1, :cond_6
v1 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
int v2 = 4; // const/4 v2, 0x4
/* if-ge v1, v2, :cond_3 */
/* .line 519 */
} // :cond_3
v1 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* .line 520 */
/* .local v1, "numSvL5":I */
java.util.Collections .sort ( v0 );
/* .line 521 */
/* add-int/lit8 v2, v1, -0x4 */
(( java.util.ArrayList ) v0 ).get ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v2, Ljava/lang/Float; */
v2 = (( java.lang.Float ) v2 ).floatValue ( ); // invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F
/* float-to-double v2, v2 */
/* const-wide/16 v4, 0x0 */
/* cmpl-double v2, v2, v4 */
/* if-lez v2, :cond_5 */
/* .line 522 */
/* const-wide/16 v2, 0x0 */
/* .line 523 */
/* .local v2, "top4AvgCn0":D */
/* add-int/lit8 v4, v1, -0x4 */
/* .local v4, "i":I */
} // :goto_1
/* if-ge v4, v1, :cond_4 */
/* .line 524 */
(( java.util.ArrayList ) v0 ).get ( v4 ); // invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v5, Ljava/lang/Float; */
v5 = (( java.lang.Float ) v5 ).floatValue ( ); // invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F
/* float-to-double v5, v5 */
/* add-double/2addr v2, v5 */
/* .line 523 */
/* add-int/lit8 v4, v4, 0x1 */
/* .line 526 */
} // .end local v4 # "i":I
} // :cond_4
/* const-wide/high16 v4, 0x4010000000000000L # 4.0 */
/* div-double/2addr v2, v4 */
/* .line 527 */
/* const/16 v4, 0x8 */
java.lang.Double .valueOf ( v2,v3 );
/* invoke-direct {p0, v4, v5}, Lcom/android/server/location/GnssCollectDataImpl;->sendMessage(ILjava/lang/Object;)V */
/* .line 529 */
} // .end local v2 # "top4AvgCn0":D
} // :cond_5
return;
/* .line 517 */
} // .end local v1 # "numSvL5":I
} // :cond_6
} // :goto_2
return;
/* .line 507 */
} // .end local v0 # "CnoL5Array":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Float;>;"
} // :cond_7
} // :goto_3
return;
} // .end method
private void saveLog ( ) {
/* .locals 13 */
/* .line 241 */
final String v0 = "loseTimes"; // const-string v0, "loseTimes"
final String v1 = "runTime"; // const-string v1, "runTime"
final String v2 = "TTFF"; // const-string v2, "TTFF"
/* const-string/jumbo v3, "startTime" */
final String v4 = "com.miui.analytics"; // const-string v4, "com.miui.analytics"
/* invoke-direct {p0}, Lcom/android/server/location/GnssCollectDataImpl;->packToJsonArray()Ljava/lang/String; */
/* .line 242 */
/* .local v5, "output":Ljava/lang/String; */
v6 = this.mSessionInfo;
v6 = (( com.android.server.location.GnssSessionInfo ) v6 ).checkValidity ( ); // invoke-virtual {v6}, Lcom/android/server/location/GnssSessionInfo;->checkValidity()Z
/* if-nez v6, :cond_0 */
/* .line 244 */
return;
/* .line 246 */
} // :cond_0
v6 = this.mContext;
final String v7 = "GnssCD"; // const-string v7, "GnssCD"
if ( v6 != null) { // if-eqz v6, :cond_6
/* if-nez v5, :cond_1 */
/* goto/16 :goto_3 */
/* .line 250 */
} // :cond_1
v6 = this.mJsonArray;
v6 = (( org.json.JSONArray ) v6 ).length ( ); // invoke-virtual {v6}, Lorg/json/JSONArray;->length()I
/* const/16 v8, 0x14 */
int v9 = 1; // const/4 v9, 0x1
/* if-le v6, v8, :cond_4 */
/* .line 251 */
int v6 = 0; // const/4 v6, 0x0
/* .local v6, "i":I */
} // :goto_0
v8 = this.mJsonArray;
v8 = (( org.json.JSONArray ) v8 ).length ( ); // invoke-virtual {v8}, Lorg/json/JSONArray;->length()I
/* if-ge v6, v8, :cond_3 */
/* .line 253 */
try { // :try_start_0
v8 = this.mJsonArray;
(( org.json.JSONArray ) v8 ).getJSONObject ( v6 ); // invoke-virtual {v8, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
/* .line 254 */
/* .local v8, "jsons":Lorg/json/JSONObject; */
/* new-instance v10, Landroid/content/Intent; */
final String v11 = "onetrack.action.TRACK_EVENT"; // const-string v11, "onetrack.action.TRACK_EVENT"
/* invoke-direct {v10, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 255 */
/* .local v10, "intent":Landroid/content/Intent; */
(( android.content.Intent ) v10 ).setPackage ( v4 ); // invoke-virtual {v10, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 256 */
/* sget-boolean v11, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
if ( v11 != null) { // if-eqz v11, :cond_2
/* .line 257 */
(( android.content.Intent ) v10 ).setFlags ( v9 ); // invoke-virtual {v10, v9}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 259 */
} // :cond_2
int v11 = 3; // const/4 v11, 0x3
(( android.content.Intent ) v10 ).setFlags ( v11 ); // invoke-virtual {v10, v11}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 261 */
} // :goto_1
final String v11 = "APP_ID"; // const-string v11, "APP_ID"
final String v12 = "2882303761518758754"; // const-string v12, "2882303761518758754"
(( android.content.Intent ) v10 ).putExtra ( v11, v12 ); // invoke-virtual {v10, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 262 */
final String v11 = "EVENT_NAME"; // const-string v11, "EVENT_NAME"
final String v12 = "GNSS"; // const-string v12, "GNSS"
(( android.content.Intent ) v10 ).putExtra ( v11, v12 ); // invoke-virtual {v10, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 263 */
final String v11 = "PACKAGE"; // const-string v11, "PACKAGE"
(( android.content.Intent ) v10 ).putExtra ( v11, v4 ); // invoke-virtual {v10, v11, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 264 */
/* new-instance v11, Landroid/os/Bundle; */
/* invoke-direct {v11}, Landroid/os/Bundle;-><init>()V */
/* .line 265 */
/* .local v11, "params":Landroid/os/Bundle; */
(( org.json.JSONObject ) v8 ).getString ( v3 ); // invoke-virtual {v8, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
(( android.os.Bundle ) v11 ).putString ( v3, v12 ); // invoke-virtual {v11, v3, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 266 */
(( org.json.JSONObject ) v8 ).getString ( v2 ); // invoke-virtual {v8, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
(( android.os.Bundle ) v11 ).putString ( v2, v12 ); // invoke-virtual {v11, v2, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 267 */
(( org.json.JSONObject ) v8 ).getString ( v1 ); // invoke-virtual {v8, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
(( android.os.Bundle ) v11 ).putString ( v1, v12 ); // invoke-virtual {v11, v1, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 268 */
(( org.json.JSONObject ) v8 ).getString ( v0 ); // invoke-virtual {v8, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
(( android.os.Bundle ) v11 ).putString ( v0, v12 ); // invoke-virtual {v11, v0, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 269 */
(( android.content.Intent ) v10 ).putExtras ( v11 ); // invoke-virtual {v10, v11}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;
/* .line 270 */
v12 = this.mContext;
(( android.content.Context ) v12 ).startService ( v10 ); // invoke-virtual {v12, v10}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
/* .line 271 */
final String v12 = "GNSS data uploaded"; // const-string v12, "GNSS data uploaded"
android.util.Log .d ( v7,v12 );
/* .line 272 */
/* invoke-direct {p0, v8}, Lcom/android/server/location/GnssCollectDataImpl;->uploadCn0(Lorg/json/JSONObject;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 276 */
} // .end local v8 # "jsons":Lorg/json/JSONObject;
} // .end local v10 # "intent":Landroid/content/Intent;
} // .end local v11 # "params":Landroid/os/Bundle;
/* .line 273 */
/* :catch_0 */
/* move-exception v8 */
/* .line 274 */
/* .local v8, "e":Ljava/lang/Exception; */
/* const-string/jumbo v10, "unexpected error when send GNSS event to onetrack" */
android.util.Log .e ( v7,v10 );
/* .line 275 */
(( java.lang.Exception ) v8 ).printStackTrace ( ); // invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
/* .line 251 */
} // .end local v8 # "e":Ljava/lang/Exception;
} // :goto_2
/* add-int/lit8 v6, v6, 0x1 */
/* .line 278 */
} // .end local v6 # "i":I
} // :cond_3
/* invoke-direct {p0, v5}, Lcom/android/server/location/GnssCollectDataImpl;->saveToFile(Ljava/lang/String;)V */
/* .line 279 */
/* const-string/jumbo v0, "send to file" */
android.util.Log .d ( v7,v0 );
/* .line 280 */
/* new-instance v0, Lorg/json/JSONArray; */
/* invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V */
this.mJsonArray = v0;
/* .line 282 */
} // :cond_4
/* iget-boolean v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->hasStartUploadData:Z */
/* if-nez v0, :cond_5 */
/* .line 283 */
/* iput-boolean v9, p0, Lcom/android/server/location/GnssCollectDataImpl;->hasStartUploadData:Z */
/* .line 284 */
v0 = this.mContext;
final String v1 = "action collect data"; // const-string v1, "action collect data"
/* invoke-direct {p0, v0, v1}, Lcom/android/server/location/GnssCollectDataImpl;->setAlarm(Landroid/content/Context;Ljava/lang/String;)V */
/* .line 286 */
} // :cond_5
return;
/* .line 247 */
} // :cond_6
} // :goto_3
final String v0 = "mContext == null || output == null"; // const-string v0, "mContext == null || output == null"
android.util.Log .d ( v7,v0 );
/* .line 248 */
return;
} // .end method
private void saveLoseStatus ( ) {
/* .locals 1 */
/* .line 848 */
v0 = this.mSessionInfo;
(( com.android.server.location.GnssSessionInfo ) v0 ).setLostTimes ( ); // invoke-virtual {v0}, Lcom/android/server/location/GnssSessionInfo;->setLostTimes()V
/* .line 849 */
return;
} // .end method
private void saveStartStatus ( ) {
/* .locals 1 */
/* .line 840 */
v0 = this.mSessionInfo;
(( com.android.server.location.GnssSessionInfo ) v0 ).setStart ( ); // invoke-virtual {v0}, Lcom/android/server/location/GnssSessionInfo;->setStart()V
/* .line 841 */
return;
} // .end method
private void saveState ( ) {
/* .locals 2 */
/* .line 856 */
int v0 = 5; // const/4 v0, 0x5
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {p0, v0, v1}, Lcom/android/server/location/GnssCollectDataImpl;->sendMessage(ILjava/lang/Object;)V */
/* .line 857 */
return;
} // .end method
private void saveStopStatus ( ) {
/* .locals 1 */
/* .line 852 */
v0 = this.mSessionInfo;
(( com.android.server.location.GnssSessionInfo ) v0 ).setEnd ( ); // invoke-virtual {v0}, Lcom/android/server/location/GnssSessionInfo;->setEnd()V
/* .line 853 */
return;
} // .end method
private void saveToFile ( java.lang.String p0 ) {
/* .locals 8 */
/* .param p1, "messageToFile" # Ljava/lang/String; */
/* .line 204 */
/* const-wide/16 v0, 0x0 */
/* .line 205 */
/* .local v0, "fileSize":J */
int v2 = 0; // const/4 v2, 0x0
/* .line 207 */
/* .local v2, "out":Ljava/io/FileOutputStream; */
try { // :try_start_0
/* new-instance v3, Ljava/io/File; */
final String v4 = "/data/mqsas/gps/gps-strength"; // const-string v4, "/data/mqsas/gps/gps-strength"
/* invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 208 */
/* .local v3, "bigdataFile":Ljava/io/File; */
v4 = (( java.io.File ) v3 ).exists ( ); // invoke-virtual {v3}, Ljava/io/File;->exists()Z
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 209 */
(( java.io.File ) v3 ).length ( ); // invoke-virtual {v3}, Ljava/io/File;->length()J
/* move-result-wide v4 */
/* const-wide/16 v6, 0x400 */
/* div-long/2addr v4, v6 */
/* move-wide v0, v4 */
/* .line 210 */
/* const-wide/16 v4, 0x5 */
/* cmp-long v4, v0, v4 */
/* if-lez v4, :cond_1 */
/* .line 212 */
(( java.io.File ) v3 ).delete ( ); // invoke-virtual {v3}, Ljava/io/File;->delete()Z
/* .line 213 */
(( java.io.File ) v3 ).getParentFile ( ); // invoke-virtual {v3}, Ljava/io/File;->getParentFile()Ljava/io/File;
(( java.io.File ) v4 ).mkdirs ( ); // invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z
/* .line 214 */
(( java.io.File ) v3 ).createNewFile ( ); // invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z
/* .line 217 */
} // :cond_0
(( java.io.File ) v3 ).getParentFile ( ); // invoke-virtual {v3}, Ljava/io/File;->getParentFile()Ljava/io/File;
(( java.io.File ) v4 ).mkdirs ( ); // invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z
/* .line 218 */
(( java.io.File ) v3 ).createNewFile ( ); // invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z
/* .line 221 */
} // :cond_1
} // :goto_0
/* new-instance v4, Ljava/io/FileOutputStream; */
int v5 = 1; // const/4 v5, 0x1
/* invoke-direct {v4, v3, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V */
/* move-object v2, v4 */
/* .line 222 */
(( java.lang.String ) p1 ).getBytes ( ); // invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B
(( java.io.FileOutputStream ) v2 ).write ( v4 ); // invoke-virtual {v2, v4}, Ljava/io/FileOutputStream;->write([B)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 226 */
} // .end local v3 # "bigdataFile":Ljava/io/File;
/* nop */
/* .line 228 */
try { // :try_start_1
(( java.io.FileOutputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 231 */
} // :goto_1
/* .line 229 */
/* :catch_0 */
/* move-exception v3 */
/* .line 230 */
/* .local v3, "e":Ljava/io/IOException; */
(( java.io.IOException ) v3 ).printStackTrace ( ); // invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
} // .end local v3 # "e":Ljava/io/IOException;
/* .line 226 */
/* :catchall_0 */
/* move-exception v3 */
/* .line 223 */
/* :catch_1 */
/* move-exception v3 */
/* .line 224 */
/* .local v3, "e":Ljava/lang/Exception; */
try { // :try_start_2
(( java.lang.Exception ) v3 ).printStackTrace ( ); // invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 226 */
} // .end local v3 # "e":Ljava/lang/Exception;
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 228 */
try { // :try_start_3
(( java.io.FileOutputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_0 */
/* .line 234 */
} // :cond_2
} // :goto_2
return;
/* .line 226 */
} // :goto_3
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 228 */
try { // :try_start_4
(( java.io.FileOutputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_2 */
/* .line 231 */
/* .line 229 */
/* :catch_2 */
/* move-exception v4 */
/* .line 230 */
/* .local v4, "e":Ljava/io/IOException; */
(( java.io.IOException ) v4 ).printStackTrace ( ); // invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V
/* .line 233 */
} // .end local v4 # "e":Ljava/io/IOException;
} // :cond_3
} // :goto_4
/* throw v3 */
} // .end method
private void sendMessage ( Integer p0, java.lang.Object p1 ) {
/* .locals 2 */
/* .param p1, "message" # I */
/* .param p2, "obj" # Ljava/lang/Object; */
/* .line 413 */
v0 = this.mHandler;
/* if-nez v0, :cond_0 */
/* .line 414 */
final String v0 = "GnssCD"; // const-string v0, "GnssCD"
final String v1 = "mhandler is null "; // const-string v1, "mhandler is null "
android.util.Log .e ( v0,v1 );
/* .line 415 */
return;
/* .line 417 */
} // :cond_0
android.os.Message .obtain ( v0,p1,p2 );
/* .line 418 */
/* .local v0, "lMessage":Landroid/os/Message; */
v1 = this.mHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 419 */
return;
} // .end method
private void setAlarm ( android.content.Context p0, java.lang.String p1 ) {
/* .locals 11 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "action" # Ljava/lang/String; */
/* .line 876 */
if ( p1 != null) { // if-eqz p1, :cond_1
/* if-nez p2, :cond_0 */
/* .line 881 */
} // :cond_0
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 882 */
/* .local v0, "filter":Landroid/content/IntentFilter; */
(( android.content.IntentFilter ) v0 ).addAction ( p2 ); // invoke-virtual {v0, p2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 883 */
v1 = this.mReceiver;
(( android.content.Context ) p1 ).registerReceiver ( v1, v0 ); // invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 885 */
try { // :try_start_0
final String v1 = "alarm"; // const-string v1, "alarm"
(( android.content.Context ) p1 ).getSystemService ( v1 ); // invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* move-object v2, v1 */
/* check-cast v2, Landroid/app/AlarmManager; */
/* .line 886 */
/* .local v2, "alarmManager":Landroid/app/AlarmManager; */
/* new-instance v1, Landroid/content/Intent; */
/* invoke-direct {v1, p2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 887 */
/* .local v1, "i":Landroid/content/Intent; */
int v3 = 0; // const/4 v3, 0x0
/* const/high16 v4, 0x4000000 */
android.app.PendingIntent .getBroadcast ( p1,v3,v1,v4 );
/* .line 888 */
/* .local v8, "p":Landroid/app/PendingIntent; */
int v3 = 2; // const/4 v3, 0x2
/* .line 889 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v4 */
/* iget v6, p0, Lcom/android/server/location/GnssCollectDataImpl;->UPLOAD_REPEAT_TIME:I */
/* int-to-long v9, v6 */
/* add-long/2addr v4, v9 */
/* int-to-long v6, v6 */
/* .line 888 */
/* invoke-virtual/range {v2 ..v8}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 892 */
} // .end local v1 # "i":Landroid/content/Intent;
} // .end local v2 # "alarmManager":Landroid/app/AlarmManager;
} // .end local v8 # "p":Landroid/app/PendingIntent;
/* .line 890 */
/* :catch_0 */
/* move-exception v1 */
/* .line 891 */
/* .local v1, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 893 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
return;
/* .line 877 */
} // .end local v0 # "filter":Landroid/content/IntentFilter;
} // :cond_1
} // :goto_1
final String v0 = "GnssCD"; // const-string v0, "GnssCD"
final String v1 = "context || action == null"; // const-string v1, "context || action == null"
android.util.Log .d ( v0,v1 );
/* .line 878 */
return;
} // .end method
private void setB1Cn0 ( Double p0 ) {
/* .locals 1 */
/* .param p1, "cn0" # D */
/* .line 872 */
v0 = this.mSessionInfo;
(( com.android.server.location.GnssSessionInfo ) v0 ).setB1Cn0 ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/location/GnssSessionInfo;->setB1Cn0(D)V
/* .line 873 */
return;
} // .end method
private void setCurrentState ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "s" # I */
/* .line 836 */
/* .line 837 */
return;
} // .end method
private void setL1Cn0 ( Double p0 ) {
/* .locals 1 */
/* .param p1, "cn0" # D */
/* .line 864 */
v0 = this.mSessionInfo;
(( com.android.server.location.GnssSessionInfo ) v0 ).setL1Cn0 ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/location/GnssSessionInfo;->setL1Cn0(D)V
/* .line 865 */
return;
} // .end method
private void setL5Cn0 ( Double p0 ) {
/* .locals 1 */
/* .param p1, "cn0" # D */
/* .line 868 */
v0 = this.mSessionInfo;
(( com.android.server.location.GnssSessionInfo ) v0 ).setL5Cn0 ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/location/GnssSessionInfo;->setL5Cn0(D)V
/* .line 869 */
return;
} // .end method
private void startHandlerThread ( ) {
/* .locals 2 */
/* .line 788 */
/* new-instance v0, Landroid/os/HandlerThread; */
final String v1 = "GnssCD thread"; // const-string v1, "GnssCD thread"
/* invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
this.mHandlerThread = v0;
/* .line 789 */
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 790 */
/* new-instance v0, Lcom/android/server/location/GnssCollectDataImpl$2; */
v1 = this.mHandlerThread;
(( android.os.HandlerThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/location/GnssCollectDataImpl$2;-><init>(Lcom/android/server/location/GnssCollectDataImpl;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 833 */
return;
} // .end method
private void startUploadFixData ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 316 */
final String v0 = "com.miui.analytics"; // const-string v0, "com.miui.analytics"
final String v1 = "GnssCD"; // const-string v1, "GnssCD"
/* iget v2, p0, Lcom/android/server/location/GnssCollectDataImpl;->mSvUsedInFix:I */
/* if-nez v2, :cond_0 */
/* .line 317 */
return;
/* .line 320 */
} // :cond_0
try { // :try_start_0
/* new-instance v2, Landroid/content/Intent; */
final String v3 = "onetrack.action.TRACK_EVENT"; // const-string v3, "onetrack.action.TRACK_EVENT"
/* invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 321 */
/* .local v2, "intent":Landroid/content/Intent; */
(( android.content.Intent ) v2 ).setPackage ( v0 ); // invoke-virtual {v2, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 322 */
/* sget-boolean v3, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 323 */
int v3 = 1; // const/4 v3, 0x1
(( android.content.Intent ) v2 ).setFlags ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 325 */
} // :cond_1
int v3 = 3; // const/4 v3, 0x3
(( android.content.Intent ) v2 ).setFlags ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 327 */
} // :goto_0
final String v3 = "APP_ID"; // const-string v3, "APP_ID"
final String v4 = "2882303761518758754"; // const-string v4, "2882303761518758754"
(( android.content.Intent ) v2 ).putExtra ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 328 */
final String v3 = "EVENT_NAME"; // const-string v3, "EVENT_NAME"
final String v4 = "GNSS_CONSTELLATION_FIX"; // const-string v4, "GNSS_CONSTELLATION_FIX"
(( android.content.Intent ) v2 ).putExtra ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 329 */
final String v3 = "PACKAGE"; // const-string v3, "PACKAGE"
(( android.content.Intent ) v2 ).putExtra ( v3, v0 ); // invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 330 */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 331 */
/* .local v0, "params":Landroid/os/Bundle; */
final String v3 = "SV_FIX"; // const-string v3, "SV_FIX"
/* iget v4, p0, Lcom/android/server/location/GnssCollectDataImpl;->mSvUsedInFix:I */
(( android.os.Bundle ) v0 ).putInt ( v3, v4 ); // invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 332 */
final String v3 = "GPS_FIX"; // const-string v3, "GPS_FIX"
/* iget v4, p0, Lcom/android/server/location/GnssCollectDataImpl;->mNumGPSUsedInFix:I */
(( android.os.Bundle ) v0 ).putInt ( v3, v4 ); // invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 333 */
final String v3 = "BEIDOU_FIX"; // const-string v3, "BEIDOU_FIX"
/* iget v4, p0, Lcom/android/server/location/GnssCollectDataImpl;->mNumBEIDOUUsedInFix:I */
(( android.os.Bundle ) v0 ).putInt ( v3, v4 ); // invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 334 */
final String v3 = "GLONASS_FIX"; // const-string v3, "GLONASS_FIX"
/* iget v4, p0, Lcom/android/server/location/GnssCollectDataImpl;->mNumGLONASSUsedInFix:I */
(( android.os.Bundle ) v0 ).putInt ( v3, v4 ); // invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 335 */
final String v3 = "GALILEO_FIX"; // const-string v3, "GALILEO_FIX"
/* iget v4, p0, Lcom/android/server/location/GnssCollectDataImpl;->mNumGALILEOUsedInFix:I */
(( android.os.Bundle ) v0 ).putInt ( v3, v4 ); // invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 336 */
(( android.content.Intent ) v2 ).putExtras ( v0 ); // invoke-virtual {v2, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;
/* .line 337 */
v3 = this.mContext;
(( android.content.Context ) v3 ).startService ( v2 ); // invoke-virtual {v3, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
/* .line 338 */
final String v3 = "GNSS_CONSTELLATION_FIX uploaded"; // const-string v3, "GNSS_CONSTELLATION_FIX uploaded"
android.util.Log .d ( v1,v3 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 342 */
/* nop */
} // .end local v0 # "params":Landroid/os/Bundle;
} // .end local v2 # "intent":Landroid/content/Intent;
/* .line 339 */
/* :catch_0 */
/* move-exception v0 */
/* .line 340 */
/* .local v0, "e":Ljava/lang/Exception; */
/* const-string/jumbo v2, "unexpected error when send GNSS event to onetrack" */
android.util.Log .e ( v1,v2 );
/* .line 341 */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 343 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mSvUsedInFix:I */
/* .line 344 */
/* iput v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mNumGPSUsedInFix:I */
/* .line 345 */
/* iput v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mNumBEIDOUUsedInFix:I */
/* .line 346 */
/* iput v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mNumGLONASSUsedInFix:I */
/* .line 347 */
/* iput v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mNumGALILEOUsedInFix:I */
/* .line 348 */
return;
} // .end method
private void updateBackgroundOptCloudConfig ( ) {
/* .locals 4 */
/* .line 752 */
v0 = this.mContext;
/* .line 753 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 752 */
final String v1 = "bigdata"; // const-string v1, "bigdata"
final String v2 = "bo_status"; // const-string v2, "bo_status"
final String v3 = "-1"; // const-string v3, "-1"
android.provider.MiuiSettings$SettingsCloudData .getCloudDataString ( v0,v1,v2,v3 );
/* .line 754 */
/* .local v0, "newBoStatus":Ljava/lang/String; */
final String v1 = "0"; // const-string v1, "0"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
final String v2 = "GnssCD"; // const-string v2, "GnssCD"
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 755 */
com.android.server.location.gnss.exp.GnssBackgroundUsageOptStub .getInstance ( );
int v3 = 0; // const/4 v3, 0x0
(( com.android.server.location.gnss.exp.GnssBackgroundUsageOptStub ) v1 ).setBackgroundOptStatus ( v3 ); // invoke-virtual {v1, v3}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub;->setBackgroundOptStatus(Z)V
/* .line 756 */
final String v1 = "background Opt disable by cloud..."; // const-string v1, "background Opt disable by cloud..."
android.util.Log .d ( v2,v1 );
/* .line 757 */
} // :cond_0
final String v1 = "1"; // const-string v1, "1"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 758 */
com.android.server.location.gnss.exp.GnssBackgroundUsageOptStub .getInstance ( );
int v3 = 1; // const/4 v3, 0x1
(( com.android.server.location.gnss.exp.GnssBackgroundUsageOptStub ) v1 ).setBackgroundOptStatus ( v3 ); // invoke-virtual {v1, v3}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub;->setBackgroundOptStatus(Z)V
/* .line 759 */
final String v1 = "background Opt enable by cloud..."; // const-string v1, "background Opt enable by cloud..."
android.util.Log .d ( v2,v1 );
/* .line 761 */
} // :cond_1
} // :goto_0
return;
} // .end method
private void updateControlState ( ) {
/* .locals 9 */
/* .line 689 */
final String v0 = "got rule changed"; // const-string v0, "got rule changed"
final String v1 = "GnssCD"; // const-string v1, "GnssCD"
android.util.Log .d ( v1,v0 );
/* .line 690 */
v0 = this.mContext;
/* .line 691 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 690 */
final String v2 = "bigdata"; // const-string v2, "bigdata"
final String v3 = "enabled"; // const-string v3, "enabled"
final String v4 = "1"; // const-string v4, "1"
android.provider.MiuiSettings$SettingsCloudData .getCloudDataString ( v0,v2,v3,v4 );
/* .line 693 */
/* .local v0, "newState":Ljava/lang/String; */
v4 = this.mContext;
/* .line 694 */
(( android.content.Context ) v4 ).getContentResolver ( ); // invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 693 */
final String v5 = "enableCaict"; // const-string v5, "enableCaict"
final String v6 = "0"; // const-string v6, "0"
android.provider.MiuiSettings$SettingsCloudData .getCloudDataString ( v4,v2,v5,v6 );
/* .line 695 */
/* .local v2, "newsuplState":Ljava/lang/String; */
final String v4 = "persist.sys.mqs.gps"; // const-string v4, "persist.sys.mqs.gps"
android.os.SystemProperties .set ( v4,v0 );
/* .line 696 */
final String v4 = "persist.sys.mqs.gps.supl"; // const-string v4, "persist.sys.mqs.gps.supl"
android.os.SystemProperties .set ( v4,v2 );
/* .line 697 */
v4 = this.mContext;
/* .line 698 */
(( android.content.Context ) v4 ).getContentResolver ( ); // invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 697 */
final String v5 = "gnssRtk"; // const-string v5, "gnssRtk"
final String v6 = "ON"; // const-string v6, "ON"
android.provider.MiuiSettings$SettingsCloudData .getCloudDataString ( v4,v5,v3,v6 );
/* .line 699 */
/* .local v3, "rtkNewState":Ljava/lang/String; */
final String v4 = "persist.sys.mqs.gps.rtk"; // const-string v4, "persist.sys.mqs.gps.rtk"
android.os.SystemProperties .set ( v4,v3 );
/* .line 700 */
/* invoke-direct {p0}, Lcom/android/server/location/GnssCollectDataImpl;->updateGsrCloudConfig()V */
/* .line 701 */
/* invoke-direct {p0}, Lcom/android/server/location/GnssCollectDataImpl;->updateGpoCloudConfig()V */
/* .line 702 */
/* invoke-direct {p0}, Lcom/android/server/location/GnssCollectDataImpl;->updateBackgroundOptCloudConfig()V */
/* .line 703 */
/* invoke-direct {p0}, Lcom/android/server/location/GnssCollectDataImpl;->updateSatelliteCallOptCloudConfig()V */
/* .line 704 */
/* invoke-direct {p0}, Lcom/android/server/location/GnssCollectDataImpl;->updateMockLocationOptCloudConfig()V */
/* .line 706 */
v4 = this.mContext;
/* .line 707 */
(( android.content.Context ) v4 ).getContentResolver ( ); // invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 706 */
final String v5 = "nlp"; // const-string v5, "nlp"
final String v6 = "SDK_source"; // const-string v6, "SDK_source"
int v7 = 5; // const/4 v7, 0x5
v4 = android.provider.MiuiSettings$SettingsCloudData .getCloudDataInt ( v4,v5,v6,v7 );
/* .line 709 */
/* .local v4, "sdk_source":I */
/* new-instance v5, Landroid/content/Intent; */
/* invoke-direct {v5}, Landroid/content/Intent;-><init>()V */
/* .line 710 */
/* .local v5, "intent":Landroid/content/Intent; */
/* const-string/jumbo v7, "send CLOUD_UPDATE Broadcast" */
android.util.Log .i ( v1,v7 );
/* .line 711 */
final String v1 = "com.xiaomi.location.metoknlp.CLOUD_UPDATE"; // const-string v1, "com.xiaomi.location.metoknlp.CLOUD_UPDATE"
(( android.content.Intent ) v5 ).setAction ( v1 ); // invoke-virtual {v5, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
/* .line 712 */
/* new-instance v1, Landroid/content/ComponentName; */
final String v7 = "com.xiaomi.metoknlp"; // const-string v7, "com.xiaomi.metoknlp"
final String v8 = "com.xiaomi.location.metoknlp.CloudReceiver"; // const-string v8, "com.xiaomi.location.metoknlp.CloudReceiver"
/* invoke-direct {v1, v7, v8}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( android.content.Intent ) v5 ).setComponent ( v1 ); // invoke-virtual {v5, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 713 */
(( android.content.Intent ) v5 ).putExtra ( v6, v4 ); // invoke-virtual {v5, v6, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 714 */
v1 = this.mContext;
(( android.content.Context ) v1 ).sendBroadcast ( v5 ); // invoke-virtual {v1, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
/* .line 715 */
return;
} // .end method
private void updateGpoCloudConfig ( ) {
/* .locals 4 */
/* .line 779 */
v0 = this.mContext;
/* .line 780 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 779 */
final String v1 = "bigdata"; // const-string v1, "bigdata"
final String v2 = "GpoVersion"; // const-string v2, "GpoVersion"
int v3 = -1; // const/4 v3, -0x1
v0 = android.provider.MiuiSettings$SettingsCloudData .getCloudDataInt ( v0,v1,v2,v3 );
/* .line 781 */
/* .local v0, "gpoNewVersion":I */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Got GpoVersion Cloud Config ? "; // const-string v2, "Got GpoVersion Cloud Config ? "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* if-ltz v0, :cond_0 */
int v2 = 1; // const/4 v2, 0x1
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "GnssCD"; // const-string v2, "GnssCD"
android.util.Log .i ( v2,v1 );
/* .line 782 */
/* if-ltz v0, :cond_1 */
/* .line 783 */
com.android.server.location.gnss.hal.GnssPowerOptimizeStub .getInstance ( );
/* .line 785 */
} // :cond_1
return;
} // .end method
private void updateGsrCloudConfig ( ) {
/* .locals 4 */
/* .line 743 */
v0 = this.mContext;
/* .line 744 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 743 */
final String v1 = "bigdata"; // const-string v1, "bigdata"
final String v2 = "enableGSR"; // const-string v2, "enableGSR"
int v3 = -1; // const/4 v3, -0x1
v0 = android.provider.MiuiSettings$SettingsCloudData .getCloudDataInt ( v0,v1,v2,v3 );
/* .line 745 */
/* .local v0, "gnssSelfRecovery":I */
/* if-ltz v0, :cond_0 */
/* .line 746 */
final String v1 = "persist.sys.gps.selfRecovery"; // const-string v1, "persist.sys.gps.selfRecovery"
java.lang.String .valueOf ( v0 );
android.os.SystemProperties .set ( v1,v2 );
/* .line 747 */
com.android.server.location.gnss.gnssSelfRecovery.GnssSelfRecoveryStub .getInstance ( );
/* .line 749 */
} // :cond_0
return;
} // .end method
private void updateMockLocationOptCloudConfig ( ) {
/* .locals 4 */
/* .line 767 */
v0 = this.mContext;
/* .line 768 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 767 */
final String v1 = "bigdata"; // const-string v1, "bigdata"
final String v2 = "gmo_status"; // const-string v2, "gmo_status"
final String v3 = "-1"; // const-string v3, "-1"
android.provider.MiuiSettings$SettingsCloudData .getCloudDataString ( v0,v1,v2,v3 );
/* .line 769 */
/* .local v0, "newGmoStatus":Ljava/lang/String; */
final String v1 = "0"; // const-string v1, "0"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
final String v2 = "GnssCD"; // const-string v2, "GnssCD"
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 770 */
com.android.server.location.GnssMockLocationOptStub .getInstance ( );
int v3 = 0; // const/4 v3, 0x0
/* .line 771 */
final String v1 = "mock location Opt disable by cloud"; // const-string v1, "mock location Opt disable by cloud"
android.util.Log .d ( v2,v1 );
/* .line 772 */
} // :cond_0
final String v1 = "1"; // const-string v1, "1"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 773 */
com.android.server.location.GnssMockLocationOptStub .getInstance ( );
int v3 = 1; // const/4 v3, 0x1
/* .line 774 */
final String v1 = "mock location Opt enable by cloud"; // const-string v1, "mock location Opt enable by cloud"
android.util.Log .d ( v2,v1 );
/* .line 776 */
} // :cond_1
} // :goto_0
return;
} // .end method
private void updateSatelliteCallOptCloudConfig ( ) {
/* .locals 6 */
/* .line 722 */
v0 = this.mContext;
/* .line 723 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 722 */
final String v1 = "bigdata"; // const-string v1, "bigdata"
final String v2 = "gsco_status"; // const-string v2, "gsco_status"
final String v3 = "-1"; // const-string v3, "-1"
android.provider.MiuiSettings$SettingsCloudData .getCloudDataString ( v0,v1,v2,v3 );
/* .line 724 */
/* .local v0, "newScoStatus":Ljava/lang/String; */
final String v2 = "0"; // const-string v2, "0"
v2 = (( java.lang.String ) v2 ).equals ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
final String v4 = "GnssCD"; // const-string v4, "GnssCD"
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 725 */
com.android.server.location.LocationExtCooperateStub .getInstance ( );
int v5 = 0; // const/4 v5, 0x0
/* .line 726 */
final String v2 = "Satellite Call Opt disable by cloud..."; // const-string v2, "Satellite Call Opt disable by cloud..."
android.util.Log .d ( v4,v2 );
/* .line 727 */
} // :cond_0
final String v2 = "1"; // const-string v2, "1"
v2 = (( java.lang.String ) v2 ).equals ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 728 */
com.android.server.location.LocationExtCooperateStub .getInstance ( );
int v5 = 1; // const/4 v5, 0x1
/* .line 729 */
final String v2 = "Satellite Call Opt enable by cloud..."; // const-string v2, "Satellite Call Opt enable by cloud..."
android.util.Log .d ( v4,v2 );
/* .line 732 */
} // :cond_1
} // :goto_0
v2 = this.mContext;
/* .line 733 */
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 732 */
final String v4 = "gsco_status_pkg"; // const-string v4, "gsco_status_pkg"
android.provider.MiuiSettings$SettingsCloudData .getCloudDataString ( v2,v1,v4,v3 );
/* .line 734 */
/* .local v1, "newScoPkg":Ljava/lang/String; */
if ( v1 != null) { // if-eqz v1, :cond_2
v2 = (( java.lang.String ) v1 ).isEmpty ( ); // invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z
/* if-nez v2, :cond_2 */
/* .line 735 */
com.android.server.location.LocationExtCooperateStub .getInstance ( );
/* .line 737 */
} // :cond_2
return;
} // .end method
private void uploadCn0 ( org.json.JSONObject p0 ) {
/* .locals 10 */
/* .param p1, "jsons" # Lorg/json/JSONObject; */
/* .line 290 */
final String v0 = "E1Top4MeanCn0"; // const-string v0, "E1Top4MeanCn0"
final String v1 = "G1Top4MeanCn0"; // const-string v1, "G1Top4MeanCn0"
final String v2 = "B1Top4MeanCn0"; // const-string v2, "B1Top4MeanCn0"
final String v3 = "L5Top4MeanCn0"; // const-string v3, "L5Top4MeanCn0"
final String v4 = "L1Top4MeanCn0"; // const-string v4, "L1Top4MeanCn0"
final String v5 = "com.miui.analytics"; // const-string v5, "com.miui.analytics"
final String v6 = "GnssCD"; // const-string v6, "GnssCD"
try { // :try_start_0
/* new-instance v7, Landroid/content/Intent; */
final String v8 = "onetrack.action.TRACK_EVENT"; // const-string v8, "onetrack.action.TRACK_EVENT"
/* invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 291 */
/* .local v7, "intent":Landroid/content/Intent; */
(( android.content.Intent ) v7 ).setPackage ( v5 ); // invoke-virtual {v7, v5}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 292 */
/* sget-boolean v8, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
if ( v8 != null) { // if-eqz v8, :cond_0
/* .line 293 */
int v8 = 1; // const/4 v8, 0x1
(( android.content.Intent ) v7 ).setFlags ( v8 ); // invoke-virtual {v7, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 295 */
} // :cond_0
int v8 = 3; // const/4 v8, 0x3
(( android.content.Intent ) v7 ).setFlags ( v8 ); // invoke-virtual {v7, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 297 */
} // :goto_0
final String v8 = "APP_ID"; // const-string v8, "APP_ID"
final String v9 = "2882303761518758754"; // const-string v9, "2882303761518758754"
(( android.content.Intent ) v7 ).putExtra ( v8, v9 ); // invoke-virtual {v7, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 298 */
final String v8 = "EVENT_NAME"; // const-string v8, "EVENT_NAME"
final String v9 = "GNSS_CN0"; // const-string v9, "GNSS_CN0"
(( android.content.Intent ) v7 ).putExtra ( v8, v9 ); // invoke-virtual {v7, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 299 */
final String v8 = "PACKAGE"; // const-string v8, "PACKAGE"
(( android.content.Intent ) v7 ).putExtra ( v8, v5 ); // invoke-virtual {v7, v8, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 300 */
/* new-instance v5, Landroid/os/Bundle; */
/* invoke-direct {v5}, Landroid/os/Bundle;-><init>()V */
/* .line 301 */
/* .local v5, "params":Landroid/os/Bundle; */
(( org.json.JSONObject ) p1 ).getString ( v4 ); // invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
(( android.os.Bundle ) v5 ).putString ( v4, v8 ); // invoke-virtual {v5, v4, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 302 */
(( org.json.JSONObject ) p1 ).getString ( v3 ); // invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
(( android.os.Bundle ) v5 ).putString ( v3, v4 ); // invoke-virtual {v5, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 303 */
(( org.json.JSONObject ) p1 ).getString ( v2 ); // invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
(( android.os.Bundle ) v5 ).putString ( v2, v3 ); // invoke-virtual {v5, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 304 */
(( org.json.JSONObject ) p1 ).getString ( v1 ); // invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
(( android.os.Bundle ) v5 ).putString ( v1, v2 ); // invoke-virtual {v5, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 305 */
(( org.json.JSONObject ) p1 ).getString ( v0 ); // invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
(( android.os.Bundle ) v5 ).putString ( v0, v1 ); // invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 306 */
(( android.content.Intent ) v7 ).putExtras ( v5 ); // invoke-virtual {v7, v5}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;
/* .line 307 */
v0 = this.mContext;
(( android.content.Context ) v0 ).startService ( v7 ); // invoke-virtual {v0, v7}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
/* .line 308 */
final String v0 = "GNSS_CN0 uploaded"; // const-string v0, "GNSS_CN0 uploaded"
android.util.Log .d ( v6,v0 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 312 */
/* nop */
} // .end local v5 # "params":Landroid/os/Bundle;
} // .end local v7 # "intent":Landroid/content/Intent;
/* .line 309 */
/* :catch_0 */
/* move-exception v0 */
/* .line 310 */
/* .local v0, "e":Ljava/lang/Exception; */
/* const-string/jumbo v1, "unexpected error when send GNSS event to onetrack" */
android.util.Log .e ( v6,v1 );
/* .line 311 */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 313 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
return;
} // .end method
/* # virtual methods */
public java.lang.String getCurrentTime ( ) {
/* .locals 11 */
/* .line 404 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* .line 405 */
/* .local v0, "mNow":J */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 406 */
/* .local v2, "sb":Ljava/lang/StringBuilder; */
java.util.Calendar .getInstance ( );
/* .line 407 */
/* .local v9, "c":Ljava/util/Calendar; */
(( java.util.Calendar ) v9 ).setTimeInMillis ( v0, v1 ); // invoke-virtual {v9, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V
/* .line 408 */
final String v10 = "%tm-%td %tH:%tM:%tS.%tL"; // const-string v10, "%tm-%td %tH:%tM:%tS.%tL"
/* move-object v3, v9 */
/* move-object v4, v9 */
/* move-object v5, v9 */
/* move-object v6, v9 */
/* move-object v7, v9 */
/* move-object v8, v9 */
/* filled-new-array/range {v3 ..v8}, [Ljava/lang/Object; */
java.lang.String .format ( v10,v3 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 409 */
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
public Boolean getSuplState ( ) {
/* .locals 2 */
/* .line 141 */
final String v0 = "persist.sys.mqs.gps.supl"; // const-string v0, "persist.sys.mqs.gps.supl"
final String v1 = "0"; // const-string v1, "0"
android.os.SystemProperties .get ( v0,v1 );
/* .line 143 */
/* .local v0, "suplstate":Ljava/lang/String; */
final String v1 = "1"; // const-string v1, "1"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
} // .end method
public Boolean isCnSimInserted ( ) {
/* .locals 1 */
/* .line 155 */
/* iget-boolean v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mIsCnSim:Z */
} // .end method
public void savePoint ( Integer p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "type" # I */
/* .param p2, "extraInfo" # Ljava/lang/String; */
/* .line 635 */
v0 = /* invoke-direct {p0}, Lcom/android/server/location/GnssCollectDataImpl;->allowCollect()Z */
/* if-nez v0, :cond_0 */
/* .line 636 */
final String v0 = "GnssCD"; // const-string v0, "GnssCD"
final String v1 = "no GnssCD enabled"; // const-string v1, "no GnssCD enabled"
android.util.Log .d ( v0,v1 );
/* .line 637 */
return;
/* .line 639 */
} // :cond_0
/* if-nez p1, :cond_1 */
/* .line 640 */
/* invoke-direct {p0}, Lcom/android/server/location/GnssCollectDataImpl;->startHandlerThread()V */
/* .line 641 */
/* invoke-direct {p0, p1}, Lcom/android/server/location/GnssCollectDataImpl;->setCurrentState(I)V */
/* .line 642 */
} // :cond_1
int v0 = 3; // const/4 v0, 0x3
int v1 = 1; // const/4 v1, 0x1
/* if-ne v1, p1, :cond_3 */
/* .line 643 */
if ( v1 != null) { // if-eqz v1, :cond_2
/* if-eq v1, v0, :cond_2 */
int v0 = 5; // const/4 v0, 0x5
/* if-ne v1, v0, :cond_9 */
/* .line 644 */
} // :cond_2
v0 = this.mSessionInfo;
(( com.android.server.location.GnssSessionInfo ) v0 ).newSessionReset ( ); // invoke-virtual {v0}, Lcom/android/server/location/GnssSessionInfo;->newSessionReset()V
/* .line 645 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/location/GnssCollectDataImpl;->sendMessage(ILjava/lang/Object;)V */
/* .line 647 */
} // :cond_3
int v2 = 2; // const/4 v2, 0x2
/* if-ne v2, p1, :cond_4 */
/* .line 648 */
/* if-ne v0, v1, :cond_9 */
/* .line 649 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/location/GnssCollectDataImpl;->sendMessage(ILjava/lang/Object;)V */
/* .line 651 */
} // :cond_4
int v3 = 4; // const/4 v3, 0x4
/* if-ne v0, p1, :cond_6 */
/* .line 652 */
/* if-eq v0, v1, :cond_5 */
/* if-eq v0, v2, :cond_5 */
/* if-ne v0, v3, :cond_9 */
/* .line 653 */
} // :cond_5
/* invoke-direct {p0, p1, p2}, Lcom/android/server/location/GnssCollectDataImpl;->sendMessage(ILjava/lang/Object;)V */
/* .line 655 */
} // :cond_6
/* if-ne v3, p1, :cond_8 */
/* .line 659 */
/* if-eq v0, v2, :cond_7 */
/* if-ne v0, v3, :cond_9 */
/* .line 660 */
} // :cond_7
/* invoke-direct {p0, p1, p2}, Lcom/android/server/location/GnssCollectDataImpl;->sendMessage(ILjava/lang/Object;)V */
/* .line 662 */
} // :cond_8
int v0 = 6; // const/4 v0, 0x6
/* if-ne v0, p1, :cond_a */
/* .line 664 */
if ( p2 != null) { // if-eqz p2, :cond_9
final String v0 = "$PQWP6"; // const-string v0, "$PQWP6"
v0 = (( java.lang.String ) p2 ).startsWith ( v0 ); // invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_9
/* .line 665 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/location/GnssCollectDataImpl;->sendMessage(ILjava/lang/Object;)V */
/* .line 671 */
} // :cond_9
} // :goto_0
return;
/* .line 669 */
} // :cond_a
return;
} // .end method
public void savePoint ( Integer p0, java.lang.String p1, android.content.Context p2 ) {
/* .locals 2 */
/* .param p1, "type" # I */
/* .param p2, "extraInfo" # Ljava/lang/String; */
/* .param p3, "context" # Landroid/content/Context; */
/* .line 621 */
v0 = /* invoke-direct {p0}, Lcom/android/server/location/GnssCollectDataImpl;->allowCollect()Z */
final String v1 = "GnssCD"; // const-string v1, "GnssCD"
/* if-nez v0, :cond_0 */
/* .line 622 */
final String v0 = "no GnssCD enabled"; // const-string v0, "no GnssCD enabled"
android.util.Log .d ( v1,v0 );
/* .line 623 */
return;
/* .line 625 */
} // :cond_0
/* if-nez p1, :cond_1 */
/* .line 626 */
this.mContext = p3;
/* .line 627 */
int v0 = 0; // const/4 v0, 0x0
(( com.android.server.location.GnssCollectDataImpl ) p0 ).savePoint ( v0, p2 ); // invoke-virtual {p0, v0, p2}, Lcom/android/server/location/GnssCollectDataImpl;->savePoint(ILjava/lang/String;)V
/* .line 628 */
final String v0 = "register listener"; // const-string v0, "register listener"
android.util.Log .d ( v1,v0 );
/* .line 629 */
/* invoke-direct {p0}, Lcom/android/server/location/GnssCollectDataImpl;->registerControlListener()V */
/* .line 631 */
} // :cond_1
return;
} // .end method
public void savePoint ( Integer p0, Float[] p1, Integer p2, Float[] p3, Float[] p4 ) {
/* .locals 4 */
/* .param p1, "type" # I */
/* .param p2, "cn0s" # [F */
/* .param p3, "numSv" # I */
/* .param p4, "svCarrierFreqs" # [F */
/* .param p5, "svConstellation" # [F */
/* .line 433 */
v0 = /* invoke-direct {p0}, Lcom/android/server/location/GnssCollectDataImpl;->allowCollect()Z */
/* if-nez v0, :cond_0 */
/* .line 434 */
final String v0 = "GnssCD"; // const-string v0, "GnssCD"
final String v1 = "no GnssCD enabled"; // const-string v1, "no GnssCD enabled"
android.util.Log .d ( v0,v1 );
/* .line 435 */
return;
/* .line 438 */
} // :cond_0
/* const/16 v0, 0xa */
/* if-eq v0, p1, :cond_1 */
/* .line 439 */
return;
/* .line 441 */
} // :cond_1
if ( p3 != null) { // if-eqz p3, :cond_b
if ( p2 != null) { // if-eqz p2, :cond_b
/* array-length v0, p2 */
if ( v0 != null) { // if-eqz v0, :cond_b
/* array-length v0, p2 */
/* if-ge v0, p3, :cond_2 */
/* goto/16 :goto_3 */
/* .line 444 */
} // :cond_2
int v0 = 4; // const/4 v0, 0x4
/* if-ge p3, v0, :cond_4 */
/* .line 445 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v0 */
/* iget-wide v2, p0, Lcom/android/server/location/GnssCollectDataImpl;->mLastSvTime:J */
/* sub-long/2addr v0, v2 */
/* iget v2, p0, Lcom/android/server/location/GnssCollectDataImpl;->SV_ERROR_SEND_INTERVAL:I */
/* int-to-long v2, v2 */
/* cmp-long v0, v0, v2 */
/* if-lez v0, :cond_3 */
/* .line 446 */
final String v0 = "SvCount"; // const-string v0, "SvCount"
java.lang.Integer .valueOf ( p3 );
/* filled-new-array {v0, v1}, [Ljava/lang/Object; */
/* const v1, 0x36c70305 */
com.miui.misight.MiSight .constructEvent ( v1,v0 );
/* .line 447 */
/* .local v0, "event":Lcom/miui/misight/MiEvent; */
com.miui.misight.MiSight .sendEvent ( v0 );
/* .line 448 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v1 */
/* iput-wide v1, p0, Lcom/android/server/location/GnssCollectDataImpl;->mLastSvTime:J */
/* .line 450 */
} // .end local v0 # "event":Lcom/miui/misight/MiEvent;
} // :cond_3
return;
/* .line 452 */
} // :cond_4
/* invoke-direct {p0, p3, p2, p4, p5}, Lcom/android/server/location/GnssCollectDataImpl;->saveL1Cn0(I[F[F[F)V */
/* .line 453 */
/* invoke-direct {p0, p3, p2, p4, p5}, Lcom/android/server/location/GnssCollectDataImpl;->saveL5Cn0(I[F[F[F)V */
/* .line 454 */
/* invoke-direct {p0, p3, p2, p4, p5}, Lcom/android/server/location/GnssCollectDataImpl;->saveB1Cn0(I[F[F[F)V */
/* .line 455 */
/* invoke-direct {p0, p3, p2, p4, p5}, Lcom/android/server/location/GnssCollectDataImpl;->saveG1Cn0(I[F[F[F)V */
/* .line 456 */
/* invoke-direct {p0, p3, p2, p4, p5}, Lcom/android/server/location/GnssCollectDataImpl;->saveE1Cn0(I[F[F[F)V */
/* .line 457 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
/* array-length v1, p5 */
/* if-ge v0, v1, :cond_a */
/* .line 458 */
/* aget v1, p5, v0 */
int v2 = 0; // const/4 v2, 0x0
/* cmpl-float v1, v1, v2 */
/* if-nez v1, :cond_5 */
/* .line 459 */
/* .line 460 */
} // :cond_5
/* aget v1, p5, v0 */
/* const/high16 v2, 0x3f800000 # 1.0f */
/* cmpl-float v1, v1, v2 */
/* if-nez v1, :cond_6 */
/* .line 461 */
/* iget v1, p0, Lcom/android/server/location/GnssCollectDataImpl;->mNumGPSUsedInFix:I */
/* add-int/lit8 v1, v1, 0x1 */
/* iput v1, p0, Lcom/android/server/location/GnssCollectDataImpl;->mNumGPSUsedInFix:I */
/* .line 462 */
} // :cond_6
/* aget v1, p5, v0 */
/* const/high16 v2, 0x40a00000 # 5.0f */
/* cmpl-float v1, v1, v2 */
/* if-nez v1, :cond_7 */
/* .line 463 */
/* iget v1, p0, Lcom/android/server/location/GnssCollectDataImpl;->mNumBEIDOUUsedInFix:I */
/* add-int/lit8 v1, v1, 0x1 */
/* iput v1, p0, Lcom/android/server/location/GnssCollectDataImpl;->mNumBEIDOUUsedInFix:I */
/* .line 464 */
} // :cond_7
/* aget v1, p5, v0 */
/* const/high16 v2, 0x40400000 # 3.0f */
/* cmpl-float v1, v1, v2 */
/* if-nez v1, :cond_8 */
/* .line 465 */
/* iget v1, p0, Lcom/android/server/location/GnssCollectDataImpl;->mNumGLONASSUsedInFix:I */
/* add-int/lit8 v1, v1, 0x1 */
/* iput v1, p0, Lcom/android/server/location/GnssCollectDataImpl;->mNumGLONASSUsedInFix:I */
/* .line 466 */
} // :cond_8
/* aget v1, p5, v0 */
/* const/high16 v2, 0x40c00000 # 6.0f */
/* cmpl-float v1, v1, v2 */
/* if-nez v1, :cond_9 */
/* .line 467 */
/* iget v1, p0, Lcom/android/server/location/GnssCollectDataImpl;->mNumGALILEOUsedInFix:I */
/* add-int/lit8 v1, v1, 0x1 */
/* iput v1, p0, Lcom/android/server/location/GnssCollectDataImpl;->mNumGALILEOUsedInFix:I */
/* .line 469 */
} // :cond_9
} // :goto_1
/* iget v1, p0, Lcom/android/server/location/GnssCollectDataImpl;->mSvUsedInFix:I */
/* add-int/lit8 v1, v1, 0x1 */
/* iput v1, p0, Lcom/android/server/location/GnssCollectDataImpl;->mSvUsedInFix:I */
/* .line 457 */
} // :goto_2
/* add-int/lit8 v0, v0, 0x1 */
/* .line 471 */
} // .end local v0 # "i":I
} // :cond_a
return;
/* .line 442 */
} // :cond_b
} // :goto_3
return;
} // .end method
public void saveUngrantedBackPermission ( Integer p0, Integer p1, java.lang.String p2 ) {
/* .locals 2 */
/* .param p1, "mUid" # I */
/* .param p2, "mPid" # I */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .line 382 */
v0 = this.mContext;
/* if-nez v0, :cond_0 */
/* .line 383 */
return;
/* .line 385 */
} // :cond_0
final String v1 = "android.permission.ACCESS_BACKGROUND_LOCATION"; // const-string v1, "android.permission.ACCESS_BACKGROUND_LOCATION"
v0 = (( android.content.Context ) v0 ).checkPermission ( v1, p2, p1 ); // invoke-virtual {v0, v1, p2, p1}, Landroid/content/Context;->checkPermission(Ljava/lang/String;II)I
/* if-nez v0, :cond_1 */
/* .line 387 */
return;
/* .line 389 */
} // :cond_1
final String v0 = "com.autonavi.minimap"; // const-string v0, "com.autonavi.minimap"
v0 = (( java.lang.String ) p3 ).equals ( v0 ); // invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 390 */
/* iget v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->AmapBackCount:I */
/* add-int/lit8 v0, v0, 0x1 */
/* iput v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->AmapBackCount:I */
/* .line 391 */
} // :cond_2
final String v0 = "com.baidu.BaiduMap"; // const-string v0, "com.baidu.BaiduMap"
v0 = (( java.lang.String ) p3 ).equals ( v0 ); // invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 392 */
/* iget v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->BaiduBackCount:I */
/* add-int/lit8 v0, v0, 0x1 */
/* iput v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->BaiduBackCount:I */
/* .line 393 */
} // :cond_3
final String v0 = "com.tencent.map"; // const-string v0, "com.tencent.map"
v0 = (( java.lang.String ) p3 ).equals ( v0 ); // invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 394 */
/* iget v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->TencentBackCount:I */
/* add-int/lit8 v0, v0, 0x1 */
/* iput v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->TencentBackCount:I */
/* .line 396 */
} // :cond_4
} // :goto_0
return;
} // .end method
public void setCnSimInserted ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "mccMnc" # Ljava/lang/String; */
/* .line 148 */
final String v0 = "460"; // const-string v0, "460"
v0 = (( java.lang.String ) p1 ).startsWith ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
/* iput-boolean v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->mIsCnSim:Z */
/* .line 150 */
final String v0 = "persist.sys.mcc.mnc"; // const-string v0, "persist.sys.mcc.mnc"
android.os.SystemProperties .set ( v0,p1 );
/* .line 151 */
return;
} // .end method
public void startUploadBackData ( android.content.Context p0 ) {
/* .locals 6 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 351 */
final String v0 = "com.miui.analytics"; // const-string v0, "com.miui.analytics"
final String v1 = "GnssCD"; // const-string v1, "GnssCD"
/* iget v2, p0, Lcom/android/server/location/GnssCollectDataImpl;->AmapBackCount:I */
/* if-nez v2, :cond_0 */
/* iget v2, p0, Lcom/android/server/location/GnssCollectDataImpl;->BaiduBackCount:I */
/* if-nez v2, :cond_0 */
/* iget v2, p0, Lcom/android/server/location/GnssCollectDataImpl;->TencentBackCount:I */
/* if-nez v2, :cond_0 */
/* .line 352 */
return;
/* .line 355 */
} // :cond_0
try { // :try_start_0
/* new-instance v2, Landroid/content/Intent; */
final String v3 = "onetrack.action.TRACK_EVENT"; // const-string v3, "onetrack.action.TRACK_EVENT"
/* invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 356 */
/* .local v2, "intent":Landroid/content/Intent; */
(( android.content.Intent ) v2 ).setPackage ( v0 ); // invoke-virtual {v2, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 357 */
/* sget-boolean v3, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
int v4 = 3; // const/4 v4, 0x3
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 358 */
int v3 = 1; // const/4 v3, 0x1
(( android.content.Intent ) v2 ).setFlags ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 360 */
} // :cond_1
(( android.content.Intent ) v2 ).setFlags ( v4 ); // invoke-virtual {v2, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 362 */
} // :goto_0
final String v3 = "APP_ID"; // const-string v3, "APP_ID"
final String v5 = "2882303761518758754"; // const-string v5, "2882303761518758754"
(( android.content.Intent ) v2 ).putExtra ( v3, v5 ); // invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 363 */
final String v3 = "EVENT_NAME"; // const-string v3, "EVENT_NAME"
final String v5 = "GNSS_BACKGROUND_PERMISSION"; // const-string v5, "GNSS_BACKGROUND_PERMISSION"
(( android.content.Intent ) v2 ).putExtra ( v3, v5 ); // invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 364 */
final String v3 = "PACKAGE"; // const-string v3, "PACKAGE"
(( android.content.Intent ) v2 ).putExtra ( v3, v0 ); // invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 365 */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 366 */
/* .local v0, "params":Landroid/os/Bundle; */
final String v3 = "AmapBackCount"; // const-string v3, "AmapBackCount"
/* iget v5, p0, Lcom/android/server/location/GnssCollectDataImpl;->AmapBackCount:I */
/* div-int/2addr v5, v4 */
(( android.os.Bundle ) v0 ).putInt ( v3, v5 ); // invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 367 */
final String v3 = "BaiduBackCount"; // const-string v3, "BaiduBackCount"
/* iget v5, p0, Lcom/android/server/location/GnssCollectDataImpl;->BaiduBackCount:I */
/* div-int/2addr v5, v4 */
(( android.os.Bundle ) v0 ).putInt ( v3, v5 ); // invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 368 */
final String v3 = "TencentBackCount"; // const-string v3, "TencentBackCount"
/* iget v5, p0, Lcom/android/server/location/GnssCollectDataImpl;->TencentBackCount:I */
/* div-int/2addr v5, v4 */
(( android.os.Bundle ) v0 ).putInt ( v3, v5 ); // invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 369 */
(( android.content.Intent ) v2 ).putExtras ( v0 ); // invoke-virtual {v2, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;
/* .line 370 */
v3 = this.mContext;
(( android.content.Context ) v3 ).startService ( v2 ); // invoke-virtual {v3, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
/* .line 371 */
final String v3 = "GNSS_BACKGROUND_PERMISSION uploaded"; // const-string v3, "GNSS_BACKGROUND_PERMISSION uploaded"
android.util.Log .d ( v1,v3 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 375 */
/* nop */
} // .end local v0 # "params":Landroid/os/Bundle;
} // .end local v2 # "intent":Landroid/content/Intent;
/* .line 372 */
/* :catch_0 */
/* move-exception v0 */
/* .line 373 */
/* .local v0, "e":Ljava/lang/Exception; */
/* const-string/jumbo v2, "unexpected error when send GNSS event to onetrack" */
android.util.Log .e ( v1,v2 );
/* .line 374 */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 376 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->AmapBackCount:I */
/* .line 377 */
/* iput v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->BaiduBackCount:I */
/* .line 378 */
/* iput v0, p0, Lcom/android/server/location/GnssCollectDataImpl;->TencentBackCount:I */
/* .line 379 */
return;
} // .end method
