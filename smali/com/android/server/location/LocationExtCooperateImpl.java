public class com.android.server.location.LocationExtCooperateImpl implements com.android.server.location.LocationExtCooperateStub {
	 /* .source "LocationExtCooperateImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/location/LocationExtCooperateImpl$SettingsObserver; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String ACTION_SATELLITE_STATE_CHANGE;
private static final java.lang.String DEF_GSCO_PKG;
private static final java.lang.String FLAG_GSCO_APPEND_PKG;
private static final java.lang.String FLAG_GSCO_COVER_PKG;
private static final java.lang.String KEY_API_USE;
private static final java.lang.String KEY_GSCO_PKG;
private static final java.lang.String KEY_GSCO_STATUS;
private static final java.lang.String KEY_SATELLITE_STATE_CHANGE_IS_ENABLE;
private static final java.lang.String KEY_SATELLITE_STATE_CHANGE_PHONE_ID;
private static final java.lang.String SATELLITE_STATE;
private static final java.lang.String TAG;
private static final java.util.HashSet sDevices;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashSet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private final java.io.File mCloudSpFile;
private android.content.Context mContext;
private final Boolean mDefaultFeatureStatus;
private Boolean mIsSpecifiedDevice;
private final android.content.BroadcastReceiver mReceiver;
private com.android.server.location.LocationExtCooperateImpl$SettingsObserver mSettingsObserver;
private final java.util.HashSet sModule;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashSet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.HashSet sPkg;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashSet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static android.content.Context -$$Nest$fgetmContext ( com.android.server.location.LocationExtCooperateImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static Boolean -$$Nest$fgetmIsSpecifiedDevice ( com.android.server.location.LocationExtCooperateImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/location/LocationExtCooperateImpl;->mIsSpecifiedDevice:Z */
} // .end method
static java.util.HashSet -$$Nest$fgetsPkg ( com.android.server.location.LocationExtCooperateImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.sPkg;
} // .end method
static void -$$Nest$fputmIsSpecifiedDevice ( com.android.server.location.LocationExtCooperateImpl p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/location/LocationExtCooperateImpl;->mIsSpecifiedDevice:Z */
return;
} // .end method
static void -$$Nest$mregisterReceiver ( com.android.server.location.LocationExtCooperateImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/location/LocationExtCooperateImpl;->registerReceiver()V */
return;
} // .end method
static void -$$Nest$msaveCloudDataToSP ( com.android.server.location.LocationExtCooperateImpl p0, java.lang.String p1, java.lang.Object p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/location/LocationExtCooperateImpl;->saveCloudDataToSP(Ljava/lang/String;Ljava/lang/Object;)V */
return;
} // .end method
static void -$$Nest$munRegisterReceiver ( com.android.server.location.LocationExtCooperateImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/location/LocationExtCooperateImpl;->unRegisterReceiver()V */
return;
} // .end method
static com.android.server.location.LocationExtCooperateImpl ( ) {
/* .locals 2 */
/* .line 49 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 56 */
final String v1 = "aurora"; // const-string v1, "aurora"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 57 */
return;
} // .end method
 com.android.server.location.LocationExtCooperateImpl ( ) {
/* .locals 4 */
/* .line 59 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 43 */
/* new-instance v0, Ljava/io/File; */
/* new-instance v1, Ljava/io/File; */
android.os.Environment .getDataDirectory ( );
/* const-string/jumbo v3, "system" */
/* invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
final String v2 = "satelliteCallOpt.xml"; // const-string v2, "satelliteCallOpt.xml"
/* invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
this.mCloudSpFile = v0;
/* .line 45 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.sModule = v0;
/* .line 46 */
/* new-instance v1, Ljava/util/HashSet; */
/* invoke-direct {v1}, Ljava/util/HashSet;-><init>()V */
this.sPkg = v1;
/* .line 50 */
final String v1 = "persist.sys.gnss_gsco"; // const-string v1, "persist.sys.gnss_gsco"
int v2 = 0; // const/4 v2, 0x0
v1 = android.os.SystemProperties .getBoolean ( v1,v2 );
/* iput-boolean v1, p0, Lcom/android/server/location/LocationExtCooperateImpl;->mDefaultFeatureStatus:Z */
/* .line 72 */
/* new-instance v1, Lcom/android/server/location/LocationExtCooperateImpl$1; */
/* invoke-direct {v1, p0}, Lcom/android/server/location/LocationExtCooperateImpl$1;-><init>(Lcom/android/server/location/LocationExtCooperateImpl;)V */
this.mReceiver = v1;
/* .line 65 */
final String v1 = "power"; // const-string v1, "power"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 66 */
v0 = this.sPkg;
final String v1 = "com.android.mms"; // const-string v1, "com.android.mms"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 67 */
v0 = this.sPkg;
final String v1 = "com.android.incallui"; // const-string v1, "com.android.incallui"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 68 */
/* invoke-direct {p0}, Lcom/android/server/location/LocationExtCooperateImpl;->loadCloudDataGscoPkgFromSP()Ljava/lang/String; */
/* invoke-direct {p0, v0}, Lcom/android/server/location/LocationExtCooperateImpl;->setPkgFromCloud(Ljava/lang/String;)V */
/* .line 69 */
/* new-instance v0, Lcom/android/server/location/LocationExtCooperateImpl$SettingsObserver; */
/* new-instance v1, Landroid/os/Handler; */
/* invoke-direct {v1}, Landroid/os/Handler;-><init>()V */
/* invoke-direct {v0, p0, v1}, Lcom/android/server/location/LocationExtCooperateImpl$SettingsObserver;-><init>(Lcom/android/server/location/LocationExtCooperateImpl;Landroid/os/Handler;)V */
this.mSettingsObserver = v0;
/* .line 70 */
return;
} // .end method
private java.lang.String loadCloudDataGscoPkgFromSP ( ) {
/* .locals 5 */
/* .line 205 */
final String v0 = "gscoPkg invalid"; // const-string v0, "gscoPkg invalid"
final String v1 = "load Cloud Data Gsco Status From SP running..."; // const-string v1, "load Cloud Data Gsco Status From SP running..."
final String v2 = "LocationExtCooperate"; // const-string v2, "LocationExtCooperate"
android.util.Log .d ( v2,v1 );
/* .line 207 */
try { // :try_start_0
v1 = this.mContext;
/* if-nez v1, :cond_0 */
/* .line 208 */
/* .line 210 */
} // :cond_0
(( android.content.Context ) v1 ).createDeviceProtectedStorageContext ( ); // invoke-virtual {v1}, Landroid/content/Context;->createDeviceProtectedStorageContext()Landroid/content/Context;
/* .line 211 */
/* .local v1, "directBootContext":Landroid/content/Context; */
v3 = this.mCloudSpFile;
int v4 = 0; // const/4 v4, 0x0
(( android.content.Context ) v1 ).getSharedPreferences ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/io/File;I)Landroid/content/SharedPreferences;
/* .line 212 */
/* .local v3, "editor":Landroid/content/SharedPreferences; */
final String v4 = "gscoPkg"; // const-string v4, "gscoPkg"
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 216 */
} // .end local v1 # "directBootContext":Landroid/content/Context;
} // .end local v3 # "editor":Landroid/content/SharedPreferences;
/* .local v0, "pkg":Ljava/lang/String; */
/* nop */
/* .line 217 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Success to load GscoPkg:"; // const-string v3, "Success to load GscoPkg:"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v1 );
/* .line 218 */
/* .line 213 */
} // .end local v0 # "pkg":Ljava/lang/String;
/* :catch_0 */
/* move-exception v1 */
/* .line 214 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Failed to load GscoPkg..., "; // const-string v4, "Failed to load GscoPkg..., "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v2,v3 );
/* .line 215 */
} // .end method
private Boolean loadCloudDataGscoStatusFromSP ( ) {
/* .locals 5 */
/* .line 223 */
/* iget-boolean v0, p0, Lcom/android/server/location/LocationExtCooperateImpl;->mDefaultFeatureStatus:Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_1 */
v0 = com.android.server.location.LocationExtCooperateImpl.sDevices;
v2 = android.os.Build.DEVICE;
(( java.lang.String ) v2 ).toLowerCase ( ); // invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;
v0 = (( java.util.HashSet ) v0 ).contains ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
/* move v0, v1 */
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
/* .line 224 */
/* .local v0, "defVal":Z */
} // :goto_1
final String v2 = "load Cloud Data Gsco Status From SP running..."; // const-string v2, "load Cloud Data Gsco Status From SP running..."
final String v3 = "LocationExtCooperate"; // const-string v3, "LocationExtCooperate"
android.util.Log .d ( v3,v2 );
/* .line 226 */
try { // :try_start_0
v2 = this.mContext;
/* if-nez v2, :cond_2 */
/* .line 227 */
/* .line 229 */
} // :cond_2
(( android.content.Context ) v2 ).createDeviceProtectedStorageContext ( ); // invoke-virtual {v2}, Landroid/content/Context;->createDeviceProtectedStorageContext()Landroid/content/Context;
/* .line 230 */
/* .local v2, "directBootContext":Landroid/content/Context; */
v4 = this.mCloudSpFile;
(( android.content.Context ) v2 ).getSharedPreferences ( v4, v1 ); // invoke-virtual {v2, v4, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/io/File;I)Landroid/content/SharedPreferences;
/* .line 231 */
/* .local v1, "editor":Landroid/content/SharedPreferences; */
v4 = final String v4 = "gscoStatus"; // const-string v4, "gscoStatus"
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* move v1, v4 */
/* .line 235 */
} // .end local v2 # "directBootContext":Landroid/content/Context;
/* .local v1, "status":Z */
/* nop */
/* .line 236 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Success to load GscoStatus:"; // const-string v4, "Success to load GscoStatus:"
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v3,v2 );
/* .line 237 */
/* .line 232 */
} // .end local v1 # "status":Z
/* :catch_0 */
/* move-exception v1 */
/* .line 233 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Failed to load GscoStatus..., "; // const-string v4, "Failed to load GscoStatus..., "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v3,v2 );
/* .line 234 */
} // .end method
private void registerReceiver ( ) {
/* .locals 7 */
/* .line 182 */
/* const-class v0, Lcom/android/server/location/LocationExtCooperateImpl; */
/* monitor-enter v0 */
/* .line 183 */
try { // :try_start_0
v1 = this.mContext;
/* if-nez v1, :cond_0 */
/* .line 184 */
final String v1 = "LocationExtCooperate"; // const-string v1, "LocationExtCooperate"
final String v2 = "registerReceiver fail due to context == null."; // const-string v2, "registerReceiver fail due to context == null."
android.util.Log .e ( v1,v2 );
/* .line 185 */
/* monitor-exit v0 */
return;
/* .line 188 */
} // :cond_0
/* new-instance v1, Landroid/content/IntentFilter; */
/* invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V */
/* .line 189 */
/* .local v1, "filter":Landroid/content/IntentFilter; */
final String v2 = "com.android.app.action.SATELLITE_STATE_CHANGE"; // const-string v2, "com.android.app.action.SATELLITE_STATE_CHANGE"
(( android.content.IntentFilter ) v1 ).addAction ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 190 */
v2 = this.mContext;
v3 = this.mReceiver;
(( android.content.Context ) v2 ).registerReceiver ( v3, v1 ); // invoke-virtual {v2, v3, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 192 */
v2 = this.mSettingsObserver;
int v3 = -2; // const/4 v3, -0x2
int v4 = 0; // const/4 v4, 0x0
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 193 */
v2 = this.mContext;
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v5 = "satellite_state"; // const-string v5, "satellite_state"
android.provider.Settings$System .getUriFor ( v5 );
v6 = this.mSettingsObserver;
(( android.content.ContentResolver ) v2 ).registerContentObserver ( v5, v4, v6, v3 ); // invoke-virtual {v2, v5, v4, v6, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 196 */
} // :cond_1
/* new-instance v2, Lcom/android/server/location/LocationExtCooperateImpl$SettingsObserver; */
/* new-instance v5, Landroid/os/Handler; */
/* invoke-direct {v5}, Landroid/os/Handler;-><init>()V */
/* invoke-direct {v2, p0, v5}, Lcom/android/server/location/LocationExtCooperateImpl$SettingsObserver;-><init>(Lcom/android/server/location/LocationExtCooperateImpl;Landroid/os/Handler;)V */
this.mSettingsObserver = v2;
/* .line 197 */
v2 = this.mContext;
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v5 = "satellite_state"; // const-string v5, "satellite_state"
android.provider.Settings$System .getUriFor ( v5 );
v6 = this.mSettingsObserver;
(( android.content.ContentResolver ) v2 ).registerContentObserver ( v5, v4, v6, v3 ); // invoke-virtual {v2, v5, v4, v6, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 200 */
} // .end local v1 # "filter":Landroid/content/IntentFilter;
} // :goto_0
/* monitor-exit v0 */
/* .line 201 */
return;
/* .line 200 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void saveCloudDataToSP ( java.lang.String p0, java.lang.Object p1 ) {
/* .locals 4 */
/* .param p1, "key" # Ljava/lang/String; */
/* .param p2, "value" # Ljava/lang/Object; */
/* .line 242 */
final String v0 = "LocationExtCooperate"; // const-string v0, "LocationExtCooperate"
try { // :try_start_0
v1 = this.mContext;
/* if-nez v1, :cond_0 */
/* .line 243 */
return;
/* .line 245 */
} // :cond_0
v2 = this.mCloudSpFile;
int v3 = 0; // const/4 v3, 0x0
(( android.content.Context ) v1 ).getSharedPreferences ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/io/File;I)Landroid/content/SharedPreferences;
/* .line 246 */
/* .local v1, "editor":Landroid/content/SharedPreferences$Editor; */
/* instance-of v2, p2, Ljava/lang/Boolean; */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 247 */
/* move-object v2, p2 */
/* check-cast v2, Ljava/lang/Boolean; */
v2 = (( java.lang.Boolean ) v2 ).booleanValue ( ); // invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z
/* .line 248 */
} // :cond_1
/* instance-of v2, p2, Ljava/lang/String; */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 249 */
/* move-object v2, p2 */
/* check-cast v2, Ljava/lang/String; */
/* .line 254 */
} // :goto_0
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 258 */
} // .end local v1 # "editor":Landroid/content/SharedPreferences$Editor;
/* nop */
/* .line 259 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Success to save data to sp, data:"; // const-string v2, "Success to save data to sp, data:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v1 );
/* .line 260 */
return;
/* .line 251 */
/* .restart local v1 # "editor":Landroid/content/SharedPreferences$Editor; */
} // :cond_2
try { // :try_start_1
final String v2 = "save cloud data to sp value is not valid..."; // const-string v2, "save cloud data to sp value is not valid..."
android.util.Log .e ( v0,v2 );
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 252 */
return;
/* .line 255 */
} // .end local v1 # "editor":Landroid/content/SharedPreferences$Editor;
/* :catch_0 */
/* move-exception v1 */
/* .line 256 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Save data to sp Exception:"; // const-string v3, "Save data to sp Exception:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v0,v2 );
/* .line 257 */
return;
} // .end method
private void setPkgFromCloud ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p1, "pkgs" # Ljava/lang/String; */
/* .line 141 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "set Pkg From Cloud:" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "LocationExtCooperate"; // const-string v1, "LocationExtCooperate"
android.util.Log .i ( v1,v0 );
/* .line 142 */
if ( p1 != null) { // if-eqz p1, :cond_4
v0 = (( java.lang.String ) p1 ).isEmpty ( ); // invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 144 */
} // :cond_0
final String v0 = ","; // const-string v0, ","
(( java.lang.String ) p1 ).split ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 145 */
/* .local v0, "parts":[Ljava/lang/String; */
int v2 = 0; // const/4 v2, 0x0
/* aget-object v2, v0, v2 */
/* .line 146 */
/* .local v2, "flag":Ljava/lang/String; */
/* new-instance v3, Ljava/util/HashSet; */
/* invoke-direct {v3}, Ljava/util/HashSet;-><init>()V */
/* .line 147 */
/* .local v3, "pkgSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
int v4 = 1; // const/4 v4, 0x1
/* .local v4, "i":I */
} // :goto_0
/* array-length v5, v0 */
/* if-ge v4, v5, :cond_1 */
/* .line 148 */
/* aget-object v5, v0, v4 */
/* .line 147 */
/* add-int/lit8 v4, v4, 0x1 */
/* .line 151 */
} // .end local v4 # "i":I
} // :cond_1
final String v4 = "0"; // const-string v4, "0"
v4 = (( java.lang.String ) v4 ).equals ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 153 */
/* new-instance v4, Ljava/util/HashSet; */
/* invoke-direct {v4, v3}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V */
this.sPkg = v4;
/* .line 154 */
} // :cond_2
final String v4 = "1"; // const-string v4, "1"
v4 = (( java.lang.String ) v4 ).equals ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 156 */
v4 = this.sPkg;
(( java.util.HashSet ) v4 ).addAll ( v3 ); // invoke-virtual {v4, v3}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z
/* .line 159 */
} // :cond_3
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "receive cloud pkg data not valid:"; // const-string v5, "receive cloud pkg data not valid:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v4 );
/* .line 161 */
} // :goto_1
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "set pkg success:" */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.sPkg;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v1,v4 );
/* .line 162 */
return;
/* .line 142 */
} // .end local v0 # "parts":[Ljava/lang/String;
} // .end local v2 # "flag":Ljava/lang/String;
} // .end local v3 # "pkgSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
} // :cond_4
} // :goto_2
return;
} // .end method
private void unRegisterReceiver ( ) {
/* .locals 3 */
/* .line 165 */
/* const-class v0, Lcom/android/server/location/LocationExtCooperateImpl; */
/* monitor-enter v0 */
/* .line 166 */
try { // :try_start_0
v1 = this.mContext;
/* if-nez v1, :cond_0 */
/* .line 167 */
final String v1 = "LocationExtCooperate"; // const-string v1, "LocationExtCooperate"
/* const-string/jumbo v2, "unRegisterReceiver fail due to context == null." */
android.util.Log .e ( v1,v2 );
/* .line 168 */
/* monitor-exit v0 */
return;
/* .line 170 */
} // :cond_0
v2 = this.mReceiver;
(( android.content.Context ) v1 ).unregisterReceiver ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
/* .line 173 */
com.android.server.location.gnss.exp.GnssBackgroundUsageOptStub .getInstance ( );
(( com.android.server.location.gnss.exp.GnssBackgroundUsageOptStub ) v1 ).unRegisterSatelliteCallMode ( ); // invoke-virtual {v1}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub;->unRegisterSatelliteCallMode()V
/* .line 174 */
v1 = this.mSettingsObserver;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 175 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v2 = this.mSettingsObserver;
(( android.content.ContentResolver ) v1 ).unregisterContentObserver ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V
/* .line 176 */
int v1 = 0; // const/4 v1, 0x0
this.mSettingsObserver = v1;
/* .line 178 */
} // :cond_1
/* monitor-exit v0 */
/* .line 179 */
return;
/* .line 178 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
/* # virtual methods */
public void init ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 100 */
this.mContext = p1;
/* .line 102 */
v0 = /* invoke-direct {p0}, Lcom/android/server/location/LocationExtCooperateImpl;->loadCloudDataGscoStatusFromSP()Z */
/* iput-boolean v0, p0, Lcom/android/server/location/LocationExtCooperateImpl;->mIsSpecifiedDevice:Z */
/* .line 103 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 105 */
/* invoke-direct {p0}, Lcom/android/server/location/LocationExtCooperateImpl;->registerReceiver()V */
/* .line 108 */
} // :cond_0
v0 = this.mSettingsObserver;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 109 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "gscoStatus"; // const-string v1, "gscoStatus"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mSettingsObserver;
int v3 = -2; // const/4 v3, -0x2
int v4 = 0; // const/4 v4, 0x0
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v4, v2, v3 ); // invoke-virtual {v0, v1, v4, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 112 */
} // :cond_1
return;
} // .end method
public void registerSatelliteCallMode ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 6 */
/* .param p1, "module" # Ljava/lang/String; */
/* .param p2, "pkg" # Ljava/lang/String; */
/* .line 265 */
/* new-instance v0, Ljava/util/HashSet; */
final String v1 = ","; // const-string v1, ","
(( java.lang.String ) p2 ).split ( v1 ); // invoke-virtual {p2, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V */
/* .line 266 */
/* .local v0, "pkgSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;" */
(( java.util.HashSet ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;
v2 = } // :goto_0
final String v3 = "LocationExtCooperate"; // const-string v3, "LocationExtCooperate"
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Ljava/lang/String; */
/* .line 267 */
/* .local v2, "p":Ljava/lang/String; */
v4 = this.sPkg;
v4 = (( java.util.HashSet ) v4 ).contains ( v2 ); // invoke-virtual {v4, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
/* if-nez v4, :cond_0 */
/* .line 269 */
(( java.util.HashSet ) v0 ).remove ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z
/* .line 270 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "i"; // const-string v5, "i"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v3,v4 );
/* .line 272 */
} // .end local v2 # "p":Ljava/lang/String;
} // :cond_0
/* .line 273 */
} // :cond_1
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "setSingleAppPosMode module:" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ",pkg:"; // const-string v2, ",pkg:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ", uid:"; // const-string v2, ", uid:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = android.os.Binder .getCallingUid ( );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v3,v1 );
/* .line 274 */
v1 = this.sModule;
v1 = (( java.util.HashSet ) v1 ).contains ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_3
v1 = (( java.util.HashSet ) v0 ).isEmpty ( ); // invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 280 */
} // :cond_2
com.android.server.location.gnss.exp.GnssBackgroundUsageOptStub .getInstance ( );
final String v2 = "MI_GNSS"; // const-string v2, "MI_GNSS"
(( com.android.server.location.gnss.exp.GnssBackgroundUsageOptStub ) v1 ).registerSatelliteCallMode ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub;->registerSatelliteCallMode(Ljava/util/HashSet;Ljava/lang/String;)V
/* .line 281 */
return;
/* .line 275 */
} // :cond_3
} // :goto_1
/* const-string/jumbo v1, "setSingleAppPosMode not specify module or pkg..." */
android.util.Log .e ( v3,v1 );
/* .line 276 */
return;
} // .end method
public void setSatelliteCallOptPkg ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "pkgs" # Ljava/lang/String; */
/* .line 135 */
final String v0 = "gscoPkg"; // const-string v0, "gscoPkg"
/* invoke-direct {p0, v0, p1}, Lcom/android/server/location/LocationExtCooperateImpl;->saveCloudDataToSP(Ljava/lang/String;Ljava/lang/Object;)V */
/* .line 137 */
/* invoke-direct {p0, p1}, Lcom/android/server/location/LocationExtCooperateImpl;->setPkgFromCloud(Ljava/lang/String;)V */
/* .line 138 */
return;
} // .end method
public void setSatelliteCallOptStatus ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "status" # Z */
/* .line 117 */
/* iget-boolean v0, p0, Lcom/android/server/location/LocationExtCooperateImpl;->mIsSpecifiedDevice:Z */
final String v1 = "gscoStatus"; // const-string v1, "gscoStatus"
final String v2 = "LocationExtCooperate"; // const-string v2, "LocationExtCooperate"
/* if-nez v0, :cond_0 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 118 */
final String v0 = "Has Register Process Observer by cloud..."; // const-string v0, "Has Register Process Observer by cloud..."
android.util.Log .d ( v2,v0 );
/* .line 119 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/location/LocationExtCooperateImpl;->mIsSpecifiedDevice:Z */
/* .line 120 */
java.lang.Boolean .valueOf ( v0 );
/* invoke-direct {p0, v1, v0}, Lcom/android/server/location/LocationExtCooperateImpl;->saveCloudDataToSP(Ljava/lang/String;Ljava/lang/Object;)V */
/* .line 122 */
/* invoke-direct {p0}, Lcom/android/server/location/LocationExtCooperateImpl;->registerReceiver()V */
/* .line 123 */
} // :cond_0
if ( v0 != null) { // if-eqz v0, :cond_1
/* if-nez p1, :cond_1 */
/* .line 124 */
final String v0 = "Has unRegister Process Observer by cloud..."; // const-string v0, "Has unRegister Process Observer by cloud..."
android.util.Log .d ( v2,v0 );
/* .line 125 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/location/LocationExtCooperateImpl;->mIsSpecifiedDevice:Z */
/* .line 126 */
java.lang.Boolean .valueOf ( v0 );
/* invoke-direct {p0, v1, v0}, Lcom/android/server/location/LocationExtCooperateImpl;->saveCloudDataToSP(Ljava/lang/String;Ljava/lang/Object;)V */
/* .line 128 */
/* invoke-direct {p0}, Lcom/android/server/location/LocationExtCooperateImpl;->unRegisterReceiver()V */
/* .line 130 */
} // :cond_1
} // :goto_0
return;
} // .end method
public void unRegisterSatelliteCallMode ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "module" # Ljava/lang/String; */
/* .line 285 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "removeSingleAppPosMode module:"; // const-string v1, "removeSingleAppPosMode module:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", uid:"; // const-string v1, ", uid:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = android.os.Binder .getCallingUid ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "LocationExtCooperate"; // const-string v1, "LocationExtCooperate"
android.util.Log .i ( v1,v0 );
/* .line 286 */
v0 = this.sModule;
v0 = (( java.util.HashSet ) v0 ).contains ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
/* if-nez v0, :cond_0 */
/* .line 287 */
final String v0 = "removeSingleAppPosMode not specify module..."; // const-string v0, "removeSingleAppPosMode not specify module..."
android.util.Log .e ( v1,v0 );
/* .line 288 */
return;
/* .line 290 */
} // :cond_0
com.android.server.location.gnss.exp.GnssBackgroundUsageOptStub .getInstance ( );
(( com.android.server.location.gnss.exp.GnssBackgroundUsageOptStub ) v0 ).unRegisterSatelliteCallMode ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub;->unRegisterSatelliteCallMode()V
/* .line 291 */
return;
} // .end method
