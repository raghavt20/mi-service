.class Lcom/android/server/location/GnssCollectDataImpl$2;
.super Landroid/os/Handler;
.source "GnssCollectDataImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/location/GnssCollectDataImpl;->startHandlerThread()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/location/GnssCollectDataImpl;


# direct methods
.method constructor <init>(Lcom/android/server/location/GnssCollectDataImpl;Landroid/os/Looper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/location/GnssCollectDataImpl;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 790
    iput-object p1, p0, Lcom/android/server/location/GnssCollectDataImpl$2;->this$0:Lcom/android/server/location/GnssCollectDataImpl;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .line 792
    iget v0, p1, Landroid/os/Message;->what:I

    .line 793
    .local v0, "message":I
    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    .line 825
    :pswitch_0
    iget-object v1, p0, Lcom/android/server/location/GnssCollectDataImpl$2;->this$0:Lcom/android/server/location/GnssCollectDataImpl;

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/android/server/location/GnssCollectDataImpl;->-$$Nest$msetB1Cn0(Lcom/android/server/location/GnssCollectDataImpl;D)V

    .line 826
    goto :goto_0

    .line 822
    :pswitch_1
    iget-object v1, p0, Lcom/android/server/location/GnssCollectDataImpl$2;->this$0:Lcom/android/server/location/GnssCollectDataImpl;

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/android/server/location/GnssCollectDataImpl;->-$$Nest$msetL5Cn0(Lcom/android/server/location/GnssCollectDataImpl;D)V

    .line 823
    goto :goto_0

    .line 819
    :pswitch_2
    iget-object v1, p0, Lcom/android/server/location/GnssCollectDataImpl$2;->this$0:Lcom/android/server/location/GnssCollectDataImpl;

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/android/server/location/GnssCollectDataImpl;->-$$Nest$msetL1Cn0(Lcom/android/server/location/GnssCollectDataImpl;D)V

    .line 820
    goto :goto_0

    .line 816
    :pswitch_3
    iget-object v1, p0, Lcom/android/server/location/GnssCollectDataImpl$2;->this$0:Lcom/android/server/location/GnssCollectDataImpl;

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/android/server/location/GnssCollectDataImpl;->-$$Nest$mparseNmea(Lcom/android/server/location/GnssCollectDataImpl;Ljava/lang/String;)V

    .line 817
    goto :goto_0

    .line 812
    :pswitch_4
    iget-object v1, p0, Lcom/android/server/location/GnssCollectDataImpl$2;->this$0:Lcom/android/server/location/GnssCollectDataImpl;

    invoke-static {v1}, Lcom/android/server/location/GnssCollectDataImpl;->-$$Nest$msaveLog(Lcom/android/server/location/GnssCollectDataImpl;)V

    .line 813
    iget-object v1, p0, Lcom/android/server/location/GnssCollectDataImpl$2;->this$0:Lcom/android/server/location/GnssCollectDataImpl;

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lcom/android/server/location/GnssCollectDataImpl;->-$$Nest$msetCurrentState(Lcom/android/server/location/GnssCollectDataImpl;I)V

    .line 814
    goto :goto_0

    .line 808
    :pswitch_5
    iget-object v1, p0, Lcom/android/server/location/GnssCollectDataImpl$2;->this$0:Lcom/android/server/location/GnssCollectDataImpl;

    invoke-static {v1}, Lcom/android/server/location/GnssCollectDataImpl;->-$$Nest$msaveLoseStatus(Lcom/android/server/location/GnssCollectDataImpl;)V

    .line 809
    iget-object v1, p0, Lcom/android/server/location/GnssCollectDataImpl$2;->this$0:Lcom/android/server/location/GnssCollectDataImpl;

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lcom/android/server/location/GnssCollectDataImpl;->-$$Nest$msetCurrentState(Lcom/android/server/location/GnssCollectDataImpl;I)V

    .line 810
    goto :goto_0

    .line 803
    :pswitch_6
    iget-object v1, p0, Lcom/android/server/location/GnssCollectDataImpl$2;->this$0:Lcom/android/server/location/GnssCollectDataImpl;

    invoke-static {v1}, Lcom/android/server/location/GnssCollectDataImpl;->-$$Nest$msaveStopStatus(Lcom/android/server/location/GnssCollectDataImpl;)V

    .line 804
    iget-object v1, p0, Lcom/android/server/location/GnssCollectDataImpl$2;->this$0:Lcom/android/server/location/GnssCollectDataImpl;

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/android/server/location/GnssCollectDataImpl;->-$$Nest$msetCurrentState(Lcom/android/server/location/GnssCollectDataImpl;I)V

    .line 805
    iget-object v1, p0, Lcom/android/server/location/GnssCollectDataImpl$2;->this$0:Lcom/android/server/location/GnssCollectDataImpl;

    invoke-static {v1}, Lcom/android/server/location/GnssCollectDataImpl;->-$$Nest$msaveState(Lcom/android/server/location/GnssCollectDataImpl;)V

    .line 806
    goto :goto_0

    .line 799
    :pswitch_7
    iget-object v1, p0, Lcom/android/server/location/GnssCollectDataImpl$2;->this$0:Lcom/android/server/location/GnssCollectDataImpl;

    invoke-static {v1}, Lcom/android/server/location/GnssCollectDataImpl;->-$$Nest$msaveFixStatus(Lcom/android/server/location/GnssCollectDataImpl;)V

    .line 800
    iget-object v1, p0, Lcom/android/server/location/GnssCollectDataImpl$2;->this$0:Lcom/android/server/location/GnssCollectDataImpl;

    const/4 v2, 0x2

    invoke-static {v1, v2}, Lcom/android/server/location/GnssCollectDataImpl;->-$$Nest$msetCurrentState(Lcom/android/server/location/GnssCollectDataImpl;I)V

    .line 801
    goto :goto_0

    .line 795
    :pswitch_8
    iget-object v1, p0, Lcom/android/server/location/GnssCollectDataImpl$2;->this$0:Lcom/android/server/location/GnssCollectDataImpl;

    invoke-static {v1}, Lcom/android/server/location/GnssCollectDataImpl;->-$$Nest$msaveStartStatus(Lcom/android/server/location/GnssCollectDataImpl;)V

    .line 796
    iget-object v1, p0, Lcom/android/server/location/GnssCollectDataImpl$2;->this$0:Lcom/android/server/location/GnssCollectDataImpl;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/android/server/location/GnssCollectDataImpl;->-$$Nest$msetCurrentState(Lcom/android/server/location/GnssCollectDataImpl;I)V

    .line 797
    nop

    .line 831
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
