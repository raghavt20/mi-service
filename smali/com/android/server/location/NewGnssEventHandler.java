public class com.android.server.location.NewGnssEventHandler {
	 /* .source "NewGnssEventHandler.java" */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 private static com.android.server.location.NewGnssEventHandler mInstance;
	 /* # instance fields */
	 android.location.LocationManager locationManager;
	 private android.content.Context mAppContext;
	 private com.android.server.location.NewNotifyManager mNotifyManager;
	 private java.util.List packageNameListNavNow;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/List<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
/* # direct methods */
private com.android.server.location.NewGnssEventHandler ( ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 29 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 19 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.packageNameListNavNow = v0;
/* .line 21 */
int v0 = 0; // const/4 v0, 0x0
this.locationManager = v0;
/* .line 30 */
this.mAppContext = p1;
/* .line 31 */
/* new-instance v0, Lcom/android/server/location/NewNotifyManager; */
/* invoke-direct {v0, p1}, Lcom/android/server/location/NewNotifyManager;-><init>(Landroid/content/Context;)V */
this.mNotifyManager = v0;
/* .line 32 */
return;
} // .end method
private Boolean getGnssEnableStatus ( ) {
/* .locals 2 */
/* .line 117 */
v0 = this.mAppContext;
/* if-nez v0, :cond_0 */
/* .line 118 */
int v0 = 0; // const/4 v0, 0x0
/* .line 120 */
} // :cond_0
v1 = this.locationManager;
/* if-nez v1, :cond_1 */
/* .line 121 */
final String v1 = "location"; // const-string v1, "location"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/location/LocationManager; */
this.locationManager = v0;
/* .line 123 */
} // :cond_1
v0 = this.locationManager;
final String v1 = "gps"; // const-string v1, "gps"
v0 = (( android.location.LocationManager ) v0 ).isProviderEnabled ( v1 ); // invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z
} // .end method
public static synchronized com.android.server.location.NewGnssEventHandler getInstance ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p0, "context" # Landroid/content/Context; */
/* const-class v0, Lcom/android/server/location/NewGnssEventHandler; */
/* monitor-enter v0 */
/* .line 24 */
try { // :try_start_0
v1 = com.android.server.location.NewGnssEventHandler.mInstance;
/* if-nez v1, :cond_0 */
/* .line 25 */
/* new-instance v1, Lcom/android/server/location/NewGnssEventHandler; */
/* invoke-direct {v1, p0}, Lcom/android/server/location/NewGnssEventHandler;-><init>(Landroid/content/Context;)V */
/* .line 26 */
} // :cond_0
v1 = com.android.server.location.NewGnssEventHandler.mInstance;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* monitor-exit v0 */
/* .line 23 */
} // .end local p0 # "context":Landroid/content/Context;
/* :catchall_0 */
/* move-exception p0 */
/* monitor-exit v0 */
/* throw p0 */
} // .end method
/* # virtual methods */
public java.lang.String getLastForegroundNavApp ( ) {
/* .locals 1 */
/* .line 113 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public synchronized void handleStart ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* monitor-enter p0 */
/* .line 53 */
try { // :try_start_0
v0 = this.packageNameListNavNow;
/* .line 54 */
v0 = /* invoke-direct {p0}, Lcom/android/server/location/NewGnssEventHandler;->getGnssEnableStatus()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 55 */
v0 = this.mNotifyManager;
(( com.android.server.location.NewNotifyManager ) v0 ).showNavigationNotification ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/location/NewNotifyManager;->showNavigationNotification(Ljava/lang/String;)V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 57 */
} // .end local p0 # "this":Lcom/android/server/location/NewGnssEventHandler;
} // :cond_0
/* monitor-exit p0 */
return;
/* .line 52 */
} // .end local p1 # "packageName":Ljava/lang/String;
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
public synchronized void handleStop ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* monitor-enter p0 */
/* .line 60 */
try { // :try_start_0
v0 = this.mNotifyManager;
if ( v0 != null) { // if-eqz v0, :cond_5
v0 = v0 = this.packageNameListNavNow;
/* if-nez v0, :cond_0 */
/* .line 63 */
} // :cond_0
v0 = this.packageNameListNavNow;
/* .line 64 */
v0 = v0 = this.packageNameListNavNow;
if ( v0 != null) { // if-eqz v0, :cond_4
v0 = /* invoke-direct {p0}, Lcom/android/server/location/NewGnssEventHandler;->getGnssEnableStatus()Z */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 65 */
v0 = v0 = this.packageNameListNavNow;
int v1 = 1; // const/4 v1, 0x1
/* if-eq v0, v1, :cond_3 */
v0 = (( com.android.server.location.NewGnssEventHandler ) p0 ).moreThanOneNavApp ( ); // invoke-virtual {p0}, Lcom/android/server/location/NewGnssEventHandler;->moreThanOneNavApp()Z
/* if-nez v0, :cond_1 */
/* .line 70 */
} // :cond_1
(( com.android.server.location.NewGnssEventHandler ) p0 ).getLastForegroundNavApp ( ); // invoke-virtual {p0}, Lcom/android/server/location/NewGnssEventHandler;->getLastForegroundNavApp()Ljava/lang/String;
/* .line 71 */
/* .local v0, "lastForegroundNavApp":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 72 */
v1 = this.mNotifyManager;
(( com.android.server.location.NewNotifyManager ) v1 ).showNavigationNotification ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/location/NewNotifyManager;->showNavigationNotification(Ljava/lang/String;)V
/* .line 74 */
} // .end local p0 # "this":Lcom/android/server/location/NewGnssEventHandler;
} // :cond_2
v2 = this.mNotifyManager;
v3 = this.packageNameListNavNow;
v4 = /* .line 75 */
/* sub-int/2addr v4, v1 */
/* check-cast v1, Ljava/lang/String; */
/* .line 74 */
(( com.android.server.location.NewNotifyManager ) v2 ).showNavigationNotification ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/location/NewNotifyManager;->showNavigationNotification(Ljava/lang/String;)V
/* .line 77 */
} // .end local v0 # "lastForegroundNavApp":Ljava/lang/String;
} // :goto_0
/* .line 66 */
} // :cond_3
} // :goto_1
v0 = this.mNotifyManager;
v1 = this.packageNameListNavNow;
int v2 = 0; // const/4 v2, 0x0
/* check-cast v1, Ljava/lang/String; */
(( com.android.server.location.NewNotifyManager ) v0 ).showNavigationNotification ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/location/NewNotifyManager;->showNavigationNotification(Ljava/lang/String;)V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 67 */
/* monitor-exit p0 */
return;
/* .line 78 */
} // :cond_4
try { // :try_start_1
v0 = this.mNotifyManager;
(( com.android.server.location.NewNotifyManager ) v0 ).removeNotification ( ); // invoke-virtual {v0}, Lcom/android/server/location/NewNotifyManager;->removeNotification()V
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 80 */
} // :goto_2
/* monitor-exit p0 */
return;
/* .line 61 */
} // :cond_5
} // :goto_3
/* monitor-exit p0 */
return;
/* .line 59 */
} // .end local p1 # "packageName":Ljava/lang/String;
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
public synchronized void handleUpdateGnssStatus ( ) {
/* .locals 5 */
/* monitor-enter p0 */
/* .line 35 */
try { // :try_start_0
v0 = /* invoke-direct {p0}, Lcom/android/server/location/NewGnssEventHandler;->getGnssEnableStatus()Z */
/* .line 36 */
/* .local v0, "gnssEnable":Z */
final String v1 = "GnssNps"; // const-string v1, "GnssNps"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "gnss status update: "; // const-string v3, "gnss status update: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v2 );
/* .line 37 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 38 */
v1 = v1 = this.packageNameListNavNow;
/* if-lez v1, :cond_2 */
/* .line 39 */
(( com.android.server.location.NewGnssEventHandler ) p0 ).getLastForegroundNavApp ( ); // invoke-virtual {p0}, Lcom/android/server/location/NewGnssEventHandler;->getLastForegroundNavApp()Ljava/lang/String;
/* .line 40 */
/* .local v1, "lastForegroundNavApp":Ljava/lang/String; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 41 */
v2 = this.mNotifyManager;
(( com.android.server.location.NewNotifyManager ) v2 ).showNavigationNotification ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/location/NewNotifyManager;->showNavigationNotification(Ljava/lang/String;)V
/* .line 43 */
} // .end local p0 # "this":Lcom/android/server/location/NewGnssEventHandler;
} // :cond_0
v2 = this.mNotifyManager;
v3 = this.packageNameListNavNow;
v4 = /* .line 44 */
/* add-int/lit8 v4, v4, -0x1 */
/* check-cast v3, Ljava/lang/String; */
/* .line 43 */
(( com.android.server.location.NewNotifyManager ) v2 ).showNavigationNotification ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/location/NewNotifyManager;->showNavigationNotification(Ljava/lang/String;)V
/* .line 46 */
} // .end local v1 # "lastForegroundNavApp":Ljava/lang/String;
} // :goto_0
/* .line 48 */
} // :cond_1
v1 = this.mNotifyManager;
(( com.android.server.location.NewNotifyManager ) v1 ).removeNotification ( ); // invoke-virtual {v1}, Lcom/android/server/location/NewNotifyManager;->removeNotification()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 50 */
} // :cond_2
} // :goto_1
/* monitor-exit p0 */
return;
/* .line 34 */
} // .end local v0 # "gnssEnable":Z
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* throw v0 */
} // .end method
public synchronized void handlerUpdateFixStatus ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "fixing" # Z */
/* monitor-enter p0 */
/* .line 83 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 84 */
try { // :try_start_0
v0 = this.mNotifyManager;
(( com.android.server.location.NewNotifyManager ) v0 ).removeNotification ( ); // invoke-virtual {v0}, Lcom/android/server/location/NewNotifyManager;->removeNotification()V
/* .line 86 */
} // .end local p0 # "this":Lcom/android/server/location/NewGnssEventHandler;
} // :cond_0
v0 = /* invoke-direct {p0}, Lcom/android/server/location/NewGnssEventHandler;->getGnssEnableStatus()Z */
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = v0 = this.packageNameListNavNow;
/* if-lez v0, :cond_2 */
/* .line 87 */
(( com.android.server.location.NewGnssEventHandler ) p0 ).getLastForegroundNavApp ( ); // invoke-virtual {p0}, Lcom/android/server/location/NewGnssEventHandler;->getLastForegroundNavApp()Ljava/lang/String;
/* .line 88 */
/* .local v0, "lastForegroundNavApp":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 89 */
v1 = this.mNotifyManager;
(( com.android.server.location.NewNotifyManager ) v1 ).showNavigationNotification ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/location/NewNotifyManager;->showNavigationNotification(Ljava/lang/String;)V
/* .line 91 */
} // :cond_1
v1 = this.mNotifyManager;
v2 = this.packageNameListNavNow;
v3 = /* .line 92 */
/* add-int/lit8 v3, v3, -0x1 */
/* check-cast v2, Ljava/lang/String; */
/* .line 91 */
(( com.android.server.location.NewNotifyManager ) v1 ).showNavigationNotification ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/location/NewNotifyManager;->showNavigationNotification(Ljava/lang/String;)V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 96 */
} // .end local v0 # "lastForegroundNavApp":Ljava/lang/String;
} // :cond_2
} // :goto_0
/* monitor-exit p0 */
return;
/* .line 82 */
} // .end local p1 # "fixing":Z
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
public Boolean moreThanOneNavApp ( ) {
/* .locals 6 */
/* .line 100 */
v0 = v0 = this.packageNameListNavNow;
int v1 = 0; // const/4 v1, 0x0
int v2 = 1; // const/4 v2, 0x1
/* if-gt v0, v2, :cond_0 */
/* .line 101 */
/* .line 103 */
} // :cond_0
v0 = this.packageNameListNavNow;
/* check-cast v0, Ljava/lang/String; */
/* .line 104 */
/* .local v0, "appNameFirst":Ljava/lang/String; */
int v3 = 1; // const/4 v3, 0x1
/* .local v3, "i":I */
} // :goto_0
v4 = v4 = this.packageNameListNavNow;
/* if-ge v3, v4, :cond_2 */
/* .line 105 */
v4 = this.packageNameListNavNow;
/* check-cast v4, Ljava/lang/String; */
v4 = (( java.lang.String ) v4 ).hashCode ( ); // invoke-virtual {v4}, Ljava/lang/String;->hashCode()I
v5 = (( java.lang.String ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/String;->hashCode()I
/* if-eq v4, v5, :cond_1 */
/* .line 106 */
/* .line 104 */
} // :cond_1
/* add-int/lit8 v3, v3, 0x1 */
/* .line 109 */
} // .end local v3 # "i":I
} // :cond_2
} // .end method
