.class public Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;
.super Ljava/lang/Object;
.source "GnssSmartSatelliteSwitchImpl.java"

# interfaces
.implements Lcom/android/server/location/GnssSmartSatelliteSwitchStub;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/location/GnssSmartSatelliteSwitchImpl$BlockListHandler;
    }
.end annotation


# static fields
.field private static final BLOCK_GLO_QZSS_NAV_GAL:Ljava/lang/String; = "3,0,4,0,6,0,7,0"

.field private static final BLOCK_NONE:Ljava/lang/String; = ""

.field private static final CLOUD_KEY_SWITCH_Enable:Ljava/lang/String; = "smartSatelliteSwitch"

.field private static final CLOUD_MODULE_GNSS_SMART_SATELLITE_SWITCH_CONFIG:Ljava/lang/String; = "mtkGnssConfig"

.field private static final EFFETCTIVE_NAVI_INTERVAL:J = 0x493e0L

.field private static final GNSS_CONFIG_SUPPORT_BLOCK_LIST_PROP:Ljava/lang/String; = "persist.sys.gps.support_block_list_prop"

.field private static final NAVI_STATUS_BLOCK_SATELLITE:I = 0x3

.field private static final NAVI_STATUS_NORMAL:I = 0x2

.field private static final NAVI_STATUS_START:I = 0x1

.field private static final NAVI_STATUS_STOP:I = 0x0

.field private static final SATTELLITE_POWERSAVE_LEVEL_0:I = 0x0

.field private static final SATTELLITE_POWERSAVE_LEVEL_1:I = 0x1

.field private static final TAG:Ljava/lang/String; = "GnssSmartSatelliteSwitchImpl"

.field private static final UPDATE_INTERVAL:J = 0x493e0L

.field private static controlledAppList:[Ljava/lang/String;

.field private static final isEnabled:Z


# instance fields
.field private volatile BDS_COLLECTOR:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private volatile GPS_COLLECTOR:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private effectiveTime:J

.field private isInControlledList:Z

.field private isInit:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mHandlerThread:Landroid/os/HandlerThread;

.field private mTimer:Ljava/util/Timer;

.field private mTimerTask:Ljava/util/TimerTask;

.field private naviTime:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private startControllerListener:Z

.field private totalNaviTime:J


# direct methods
.method static bridge synthetic -$$Nest$fgetBDS_COLLECTOR(Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;)Ljava/util/concurrent/ConcurrentHashMap;
    .locals 0

    iget-object p0, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->BDS_COLLECTOR:Ljava/util/concurrent/ConcurrentHashMap;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetGPS_COLLECTOR(Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;)Ljava/util/concurrent/ConcurrentHashMap;
    .locals 0

    iget-object p0, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->GPS_COLLECTOR:Ljava/util/concurrent/ConcurrentHashMap;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mgetAvgOfTopFourCN0(Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;Ljava/util/Map;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->getAvgOfTopFourCN0(Ljava/util/Map;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mupdateBlockList(Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->updateBlockList(Ljava/lang/String;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 35
    const-string v0, "com.autonavi.minimap"

    const-string v1, "com.tencent.map"

    const-string v2, "com.baidu.BaiduMap"

    filled-new-array {v2, v0, v1}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->controlledAppList:[Ljava/lang/String;

    .line 49
    invoke-static {}, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->isCnVersion()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 50
    const-string v0, "persist.sys.gps.support_block_list_prop"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    nop

    :goto_0
    sput-boolean v1, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->isEnabled:Z

    .line 49
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->GPS_COLLECTOR:Ljava/util/concurrent/ConcurrentHashMap;

    .line 47
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->BDS_COLLECTOR:Ljava/util/concurrent/ConcurrentHashMap;

    .line 51
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->isInit:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 52
    iput-boolean v1, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->startControllerListener:Z

    .line 65
    iput-boolean v1, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->isInControlledList:Z

    .line 67
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->naviTime:Landroid/util/SparseArray;

    .line 73
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->effectiveTime:J

    .line 74
    iput-wide v0, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->totalNaviTime:J

    return-void
.end method

.method private getAvgOfTopFourCN0(Ljava/util/Map;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Float;",
            ">;)Z"
        }
    .end annotation

    .line 155
    .local p1, "collector":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/Float;>;"
    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v0

    .line 156
    .local v0, "size":I
    const/4 v1, 0x0

    if-eqz v0, :cond_4

    const/4 v2, 0x4

    if-ge v0, v2, :cond_0

    goto :goto_2

    .line 158
    :cond_0
    const/4 v3, 0x0

    .line 159
    .local v3, "sum":F
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 160
    .local v4, "vals":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    .line 161
    .local v6, "key":Ljava/lang/Integer;
    invoke-interface {p1, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Float;

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 162
    .end local v6    # "key":Ljava/lang/Integer;
    goto :goto_0

    .line 163
    :cond_1
    new-instance v5, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl$2;

    invoke-direct {v5, p0}, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl$2;-><init>(Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;)V

    invoke-static {v4, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 169
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    if-ge v5, v2, :cond_2

    .line 170
    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Float;

    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v6

    add-float/2addr v3, v6

    .line 169
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 172
    .end local v5    # "i":I
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getAvgOfTopFourCN0: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/high16 v5, 0x40800000    # 4.0f

    div-float v6, v3, v5

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v6, "GnssSmartSatelliteSwitchImpl"

    invoke-static {v6, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    div-float v2, v3, v5

    const/high16 v5, 0x42200000    # 40.0f

    cmpl-float v2, v2, v5

    if-lez v2, :cond_3

    const/4 v1, 0x1

    :cond_3
    return v1

    .line 157
    .end local v3    # "sum":F
    .end local v4    # "vals":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
    :cond_4
    :goto_2
    return v1
.end method

.method private getEffectiveTime()J
    .locals 2

    .line 322
    iget-wide v0, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->effectiveTime:J

    return-wide v0
.end method

.method private handleGnssStatus(Landroid/location/GnssStatus;)V
    .locals 5
    .param p1, "status"    # Landroid/location/GnssStatus;

    .line 207
    iget-object v0, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->isInit:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 208
    return-void

    .line 210
    :cond_0
    const/4 v0, 0x0

    .line 211
    .local v0, "constellationType":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Landroid/location/GnssStatus;->getSatelliteCount()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 212
    invoke-virtual {p1, v1}, Landroid/location/GnssStatus;->getConstellationType(I)I

    move-result v0

    .line 213
    sparse-switch v0, :sswitch_data_0

    goto :goto_1

    .line 215
    :sswitch_0
    iget-object v2, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->BDS_COLLECTOR:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p1, v1}, Landroid/location/GnssStatus;->getSvid(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p1, v1}, Landroid/location/GnssStatus;->getCn0DbHz(I)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 216
    goto :goto_1

    .line 218
    :sswitch_1
    iget-object v2, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->GPS_COLLECTOR:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p1, v1}, Landroid/location/GnssStatus;->getSvid(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p1, v1}, Landroid/location/GnssStatus;->getCn0DbHz(I)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 219
    nop

    .line 211
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 224
    .end local v1    # "i":I
    :cond_1
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x5 -> :sswitch_0
    .end sparse-switch
.end method

.method private initCollector()V
    .locals 5

    .line 257
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 258
    .local v0, "currentTimemills":J
    iget-object v2, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->naviTime:Landroid/util/SparseArray;

    if-nez v2, :cond_0

    .line 259
    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    iput-object v2, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->naviTime:Landroid/util/SparseArray;

    .line 261
    :cond_0
    iget-object v2, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->naviTime:Landroid/util/SparseArray;

    const/4 v3, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 262
    iget-object v2, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->naviTime:Landroid/util/SparseArray;

    const/4 v3, 0x1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 263
    iget-object v2, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->naviTime:Landroid/util/SparseArray;

    const/4 v3, 0x2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 264
    iget-object v2, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->naviTime:Landroid/util/SparseArray;

    const/4 v3, 0x3

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 265
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->effectiveTime:J

    .line 266
    return-void
.end method

.method private initOnce()V
    .locals 9

    .line 228
    iget-object v0, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->isInit:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 229
    return-void

    .line 230
    :cond_0
    invoke-direct {p0}, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->initCollector()V

    .line 231
    iget-object v0, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->naviTime:Landroid/util/SparseArray;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 232
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "mGnssSmartSatelliteHandler"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->mHandlerThread:Landroid/os/HandlerThread;

    .line 233
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 234
    new-instance v0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl$BlockListHandler;

    iget-object v1, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl$BlockListHandler;-><init>(Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->mHandler:Landroid/os/Handler;

    .line 235
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->mTimer:Ljava/util/Timer;

    .line 236
    new-instance v4, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl$3;

    invoke-direct {v4, p0}, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl$3;-><init>(Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;)V

    iput-object v4, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->mTimerTask:Ljava/util/TimerTask;

    .line 251
    iget-object v3, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->mTimer:Ljava/util/Timer;

    const-wide/16 v5, 0x4e20

    const-wide/32 v7, 0x493e0

    invoke-virtual/range {v3 .. v8}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 252
    const-string v0, "GnssSmartSatelliteSwitchImpl"

    const-string v1, "init"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 253
    iget-object v0, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->isInit:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 254
    return-void
.end method

.method private static isCnVersion()Z
    .locals 2

    .line 119
    const-string v0, "ro.miui.build.region"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "CN"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private isEffective(JJ)Z
    .locals 4
    .param p1, "var1"    # J
    .param p3, "var2"    # J

    .line 299
    sub-long v0, p1, p3

    const-wide/32 v2, 0x493e0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 300
    const/4 v0, 0x1

    return v0

    .line 302
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private sendToGnssEnent()V
    .locals 10

    .line 328
    :try_start_0
    iget-object v0, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->naviTime:Landroid/util/SparseArray;

    invoke-direct {p0, v0}, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->updateEffectiveTime(Landroid/util/SparseArray;)V

    .line 329
    iget-wide v0, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->totalNaviTime:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 330
    invoke-static {}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->getInstance()Lcom/android/server/location/gnss/GnssEventTrackingStub;

    move-result-object v4

    iget-wide v5, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->totalNaviTime:J

    .line 331
    invoke-direct {p0}, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->getEffectiveTime()J

    move-result-wide v7

    const/4 v9, 0x0

    invoke-interface/range {v4 .. v9}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->recordSatelliteBlockListChanged(JJLjava/lang/String;)V

    .line 333
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->naviTime:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 334
    iput-wide v2, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->effectiveTime:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 337
    goto :goto_0

    .line 335
    :catch_0
    move-exception v0

    .line 336
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "GnssSmartSatelliteSwitchImpl"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 338
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private declared-synchronized updateBlockList(Ljava/lang/String;)V
    .locals 6
    .param p1, "mBlockList"    # Ljava/lang/String;

    monitor-enter p0

    .line 178
    :try_start_0
    const-string v0, ","

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 179
    .local v0, "blockLists":[Ljava/lang/String;
    array-length v1, v0

    const/4 v2, 0x2

    rem-int/2addr v1, v2

    if-eqz v1, :cond_0

    .line 180
    const-string v1, ""
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object p1, v1

    .line 183
    .end local p0    # "this":Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v3, "gnss_satellite_blocklist"

    invoke-static {v1, v3}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 185
    .local v1, "blockListNow":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 186
    const-string v3, ""

    move-object v1, v3

    .line 188
    :cond_1
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v3, :cond_2

    .line 189
    monitor-exit p0

    return-void

    .line 191
    .restart local p0    # "this":Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;
    :cond_2
    :try_start_2
    iget-object v3, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->mContext:Landroid/content/Context;

    .line 192
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "gnss_satellite_blocklist"

    .line 191
    invoke-static {v3, v4, p1}, Landroid/provider/Settings$Global;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 194
    const-string v3, "3,0,4,0,6,0,7,0"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 195
    iget-object v2, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->naviTime:Landroid/util/SparseArray;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const/4 v4, 0x3

    invoke-virtual {v2, v4, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    .line 197
    .end local p0    # "this":Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;
    :cond_3
    iget-object v3, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->naviTime:Landroid/util/SparseArray;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 199
    :goto_0
    const-string v2, "GnssSmartSatelliteSwitchImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "update blockList:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    iget-object v2, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->naviTime:Landroid/util/SparseArray;

    invoke-direct {p0, v2}, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->updateEffectiveTime(Landroid/util/SparseArray;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 203
    .end local v1    # "blockListNow":Ljava/lang/String;
    goto :goto_1

    .line 201
    :catch_0
    move-exception v1

    .line 202
    .local v1, "e":Ljava/lang/Exception;
    :try_start_3
    const-string v2, "GnssSmartSatelliteSwitchImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "update blocklist fail, cause :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 204
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_1
    monitor-exit p0

    return-void

    .line 177
    .end local v0    # "blockLists":[Ljava/lang/String;
    .end local p1    # "mBlockList":Ljava/lang/String;
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private declared-synchronized updateEffectiveTime(Landroid/util/SparseArray;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .local p1, "naviTimeCollector":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/Long;>;"
    monitor-enter p0

    .line 307
    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 308
    .local v0, "beginTime":J
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 309
    .local v2, "stopTime":J
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 310
    .local v4, "blockTime":J
    const/4 v6, 0x2

    invoke-virtual {p1, v6}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 311
    .local v6, "normalTime":J
    invoke-direct {p0, v2, v3, v0, v1}, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->isEffective(JJ)Z

    move-result v8

    if-eqz v8, :cond_0

    sub-long v8, v2, v0

    goto :goto_0

    :cond_0
    const-wide/16 v8, 0x0

    :goto_0
    iput-wide v8, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->totalNaviTime:J

    .line 312
    cmp-long v8, v6, v4

    if-ltz v8, :cond_1

    .line 313
    iget-wide v8, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->effectiveTime:J

    sub-long v10, v6, v4

    add-long/2addr v8, v10

    iput-wide v8, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->effectiveTime:J

    goto :goto_1

    .line 314
    .end local p0    # "this":Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;
    :cond_1
    sub-long v8, v4, v6

    const-wide/16 v10, 0x4e20

    cmp-long v8, v8, v10

    if-ltz v8, :cond_2

    .line 315
    iget-wide v8, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->effectiveTime:J

    sub-long v10, v2, v4

    add-long/2addr v8, v10

    iput-wide v8, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->effectiveTime:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 319
    :goto_1
    monitor-exit p0

    return-void

    .line 317
    :cond_2
    monitor-exit p0

    return-void

    .line 306
    .end local v0    # "beginTime":J
    .end local v2    # "stopTime":J
    .end local v4    # "blockTime":J
    .end local v6    # "normalTime":J
    .end local p1    # "naviTimeCollector":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/Long;>;"
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method


# virtual methods
.method public addCloudControllListener(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 78
    iget-boolean v0, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->startControllerListener:Z

    if-eqz v0, :cond_0

    .line 79
    return-void

    .line 81
    :cond_0
    if-nez p1, :cond_1

    .line 82
    return-void

    .line 84
    :cond_1
    iput-object p1, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->mContext:Landroid/content/Context;

    .line 85
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 86
    invoke-static {}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataNotifyUri()Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl$1;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl$1;-><init>(Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;Landroid/os/Handler;)V

    .line 85
    const/4 v3, 0x1

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 108
    const-string v0, "GnssSmartSatelliteSwitchImpl"

    const-string v1, "register cloud controller listener"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    iput-boolean v3, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->startControllerListener:Z

    .line 110
    return-void
.end method

.method public isControlled(Ljava/lang/String;)V
    .locals 3
    .param p1, "workSource"    # Ljava/lang/String;

    .line 139
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->controlledAppList:[Ljava/lang/String;

    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 140
    aget-object v1, v1, v0

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 141
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->isInControlledList:Z

    .line 139
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 144
    .end local v0    # "i":I
    :cond_1
    iget-boolean v0, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->isInControlledList:Z

    if-eqz v0, :cond_2

    .line 145
    invoke-direct {p0}, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->initOnce()V

    .line 147
    :cond_2
    return-void
.end method

.method public declared-synchronized resetBlockList()V
    .locals 4

    monitor-enter p0

    .line 270
    :try_start_0
    iget-object v0, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->isInit:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_3

    .line 272
    :try_start_1
    iget-object v0, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->naviTime:Landroid/util/SparseArray;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 273
    invoke-direct {p0}, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->sendToGnssEnent()V

    .line 274
    iget-object v0, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->mTimerTask:Ljava/util/TimerTask;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 275
    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    .line 276
    iput-object v1, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->mTimerTask:Ljava/util/TimerTask;

    .line 278
    .end local p0    # "this":Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_1

    .line 279
    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 280
    iget-object v0, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 281
    iput-object v1, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->mTimer:Ljava/util/Timer;

    .line 283
    :cond_1
    iget-object v0, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->mHandlerThread:Landroid/os/HandlerThread;

    if-eqz v0, :cond_2

    .line 284
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quitSafely()V

    .line 286
    :cond_2
    iget-object v0, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "gnss_satellite_blocklist"

    const-string v3, ""

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$Global;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 288
    const-string v0, "GnssSmartSatelliteSwitchImpl"

    const-string v1, "reset Blocklist"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    iput-boolean v2, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->isInControlledList:Z

    .line 290
    iget-object v0, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->isInit:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 293
    goto :goto_0

    .line 291
    :catch_0
    move-exception v0

    .line 292
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v1, "GnssSmartSatelliteSwitchImpl"

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 295
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_3
    :goto_0
    monitor-exit p0

    return-void

    .line 269
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public smartSatelliteSwitchMonitor(Landroid/location/GnssStatus;)V
    .locals 1
    .param p1, "gnssStatus"    # Landroid/location/GnssStatus;

    .line 124
    iget-object v0, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 125
    return-void

    .line 127
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->isInControlledList:Z

    if-nez v0, :cond_1

    .line 128
    return-void

    .line 130
    :cond_1
    invoke-direct {p0, p1}, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->handleGnssStatus(Landroid/location/GnssStatus;)V

    .line 131
    return-void
.end method

.method public supportConstellationBlockListFeature()Z
    .locals 1

    .line 114
    sget-boolean v0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->isEnabled:Z

    return v0
.end method
