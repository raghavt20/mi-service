public class com.android.server.location.GnssCollectDataDbHelper extends android.database.sqlite.SQLiteOpenHelper {
	 /* .source "GnssCollectDataDbHelper.java" */
	 /* # static fields */
	 private static final java.lang.String DB_NAME;
	 private static final Integer DB_VERSION;
	 private static final java.lang.String SQL_CREATE_ENTRIES;
	 private static final java.lang.String SQL_DELETE_ENTRIES;
	 private static final java.lang.String STR_COMMA;
	 private static final java.lang.String STR_INTEGER;
	 private static final java.lang.String STR_REAL;
	 private static com.android.server.location.GnssCollectDataDbHelper mGnssCollectDataHelper;
	 /* # direct methods */
	 private com.android.server.location.GnssCollectDataDbHelper ( ) {
		 /* .locals 3 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .line 42 */
		 int v0 = 0; // const/4 v0, 0x0
		 int v1 = 1; // const/4 v1, 0x1
		 final String v2 = "gnssCollectData.db"; // const-string v2, "gnssCollectData.db"
		 /* invoke-direct {p0, p1, v2, v0, v1}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V */
		 /* .line 43 */
		 return;
	 } // .end method
	 public static com.android.server.location.GnssCollectDataDbHelper getInstance ( android.content.Context p0 ) {
		 /* .locals 1 */
		 /* .param p0, "context" # Landroid/content/Context; */
		 /* .line 35 */
		 v0 = com.android.server.location.GnssCollectDataDbHelper.mGnssCollectDataHelper;
		 /* if-nez v0, :cond_0 */
		 /* .line 36 */
		 /* new-instance v0, Lcom/android/server/location/GnssCollectDataDbHelper; */
		 /* invoke-direct {v0, p0}, Lcom/android/server/location/GnssCollectDataDbHelper;-><init>(Landroid/content/Context;)V */
		 /* .line 38 */
	 } // :cond_0
	 v0 = com.android.server.location.GnssCollectDataDbHelper.mGnssCollectDataHelper;
} // .end method
/* # virtual methods */
public void onCreate ( android.database.sqlite.SQLiteDatabase p0 ) {
	 /* .locals 1 */
	 /* .param p1, "db" # Landroid/database/sqlite/SQLiteDatabase; */
	 /* .line 48 */
	 final String v0 = "CREATE TABLE IF NOT EXISTS GnssCollectData (_id INTEGER PRIMARY KEY,startTime INTEGER,TTFF INTEGER,runTime INTEGER,loseTimes INTEGER,SAPNumber INTEGER,PDRNumber INTEGER,totalNumber INTEGER,L1Top4MeanCn0 REAL,L5Top4MeanCn0 REAL,B1Top4MeanCn0 REAL)"; // const-string v0, "CREATE TABLE IF NOT EXISTS GnssCollectData (_id INTEGER PRIMARY KEY,startTime INTEGER,TTFF INTEGER,runTime INTEGER,loseTimes INTEGER,SAPNumber INTEGER,PDRNumber INTEGER,totalNumber INTEGER,L1Top4MeanCn0 REAL,L5Top4MeanCn0 REAL,B1Top4MeanCn0 REAL)"
	 (( android.database.sqlite.SQLiteDatabase ) p1 ).execSQL ( v0 ); // invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
	 /* .line 49 */
	 return;
} // .end method
public void onDowngrade ( android.database.sqlite.SQLiteDatabase p0, Integer p1, Integer p2 ) {
	 /* .locals 0 */
	 /* .param p1, "db" # Landroid/database/sqlite/SQLiteDatabase; */
	 /* .param p2, "oldVersion" # I */
	 /* .param p3, "newVersion" # I */
	 /* .line 61 */
	 (( com.android.server.location.GnssCollectDataDbHelper ) p0 ).onUpgrade ( p1, p2, p3 ); // invoke-virtual {p0, p1, p2, p3}, Lcom/android/server/location/GnssCollectDataDbHelper;->onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
	 /* .line 62 */
	 return;
} // .end method
public void onUpgrade ( android.database.sqlite.SQLiteDatabase p0, Integer p1, Integer p2 ) {
	 /* .locals 1 */
	 /* .param p1, "db" # Landroid/database/sqlite/SQLiteDatabase; */
	 /* .param p2, "oldVersion" # I */
	 /* .param p3, "newVersion" # I */
	 /* .line 55 */
	 final String v0 = "DROP TABLE IF EXISTS GnssCollectData"; // const-string v0, "DROP TABLE IF EXISTS GnssCollectData"
	 (( android.database.sqlite.SQLiteDatabase ) p1 ).execSQL ( v0 ); // invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
	 /* .line 56 */
	 (( com.android.server.location.GnssCollectDataDbHelper ) p0 ).onCreate ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/location/GnssCollectDataDbHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
	 /* .line 57 */
	 return;
} // .end method
