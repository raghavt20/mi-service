class com.android.server.location.MtkGnssPowerSaveImpl$BootCompletedReceiver extends android.content.BroadcastReceiver {
	 /* .source "MtkGnssPowerSaveImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/location/MtkGnssPowerSaveImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "BootCompletedReceiver" */
} // .end annotation
/* # instance fields */
final com.android.server.location.MtkGnssPowerSaveImpl this$0; //synthetic
/* # direct methods */
private com.android.server.location.MtkGnssPowerSaveImpl$BootCompletedReceiver ( ) {
/* .locals 0 */
/* .line 466 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
 com.android.server.location.MtkGnssPowerSaveImpl$BootCompletedReceiver ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/location/MtkGnssPowerSaveImpl$BootCompletedReceiver;-><init>(Lcom/android/server/location/MtkGnssPowerSaveImpl;)V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 469 */
final String v0 = "android.intent.action.BOOT_COMPLETED"; // const-string v0, "android.intent.action.BOOT_COMPLETED"
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_0 */
/* .line 470 */
return;
/* .line 472 */
} // :cond_0
v0 = this.this$0;
com.android.server.location.MtkGnssPowerSaveImpl .-$$Nest$fgetTAG ( v0 );
final String v1 = "receiver boot completed, start init listener"; // const-string v1, "receiver boot completed, start init listener"
android.util.Log .d ( v0,v1 );
/* .line 473 */
v0 = this.this$0;
com.android.server.location.MtkGnssPowerSaveImpl .-$$Nest$minitListenerAndRegisterIEmd ( v0 );
/* .line 476 */
v0 = this.this$0;
com.android.server.location.MtkGnssPowerSaveImpl .-$$Nest$fgetmContext ( v0 );
v1 = this.this$0;
com.android.server.location.MtkGnssPowerSaveImpl .-$$Nest$fgetbootCompletedReceiver ( v1 );
(( android.content.Context ) v0 ).unregisterReceiver ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
/* .line 477 */
return;
} // .end method
