class com.android.server.location.GnssSmartSatelliteSwitchImpl$1 extends android.database.ContentObserver {
	 /* .source "GnssSmartSatelliteSwitchImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/location/GnssSmartSatelliteSwitchImpl;->addCloudControllListener(Landroid/content/Context;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.location.GnssSmartSatelliteSwitchImpl this$0; //synthetic
/* # direct methods */
 com.android.server.location.GnssSmartSatelliteSwitchImpl$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/location/GnssSmartSatelliteSwitchImpl; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 87 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0 ) {
/* .locals 6 */
/* .param p1, "selfChange" # Z */
/* .line 90 */
int v0 = 0; // const/4 v0, 0x0
final String v1 = "persist.sys.gps.support_block_list_prop"; // const-string v1, "persist.sys.gps.support_block_list_prop"
v0 = android.os.SystemProperties .getBoolean ( v1,v0 );
/* .line 91 */
/* .local v0, "settingsNow":Z */
v2 = this.this$0;
com.android.server.location.GnssSmartSatelliteSwitchImpl .-$$Nest$fgetmContext ( v2 );
/* .line 92 */
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 91 */
final String v3 = "mtkGnssConfig"; // const-string v3, "mtkGnssConfig"
/* const-string/jumbo v4, "smartSatelliteSwitch" */
int v5 = 0; // const/4 v5, 0x0
android.provider.MiuiSettings$SettingsCloudData .getCloudDataString ( v2,v3,v4,v5 );
/* .line 96 */
/* .local v2, "newSettings":Ljava/lang/String; */
/* if-nez v2, :cond_0 */
/* .line 97 */
return;
/* .line 99 */
} // :cond_0
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "receiver new config, new value: "; // const-string v4, "receiver new config, new value: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ", settingsNow: "; // const-string v4, ", settingsNow: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "GnssSmartSatelliteSwitchImpl"; // const-string v4, "GnssSmartSatelliteSwitchImpl"
android.util.Log .i ( v4,v3 );
/* .line 101 */
v3 = java.lang.Boolean .parseBoolean ( v2 );
/* .line 102 */
/* .local v3, "newSettingsBool":Z */
/* if-ne v3, v0, :cond_1 */
/* .line 103 */
return;
/* .line 105 */
} // :cond_1
java.lang.String .valueOf ( v3 );
android.os.SystemProperties .set ( v1,v4 );
/* .line 106 */
return;
} // .end method
