class com.android.server.location.GnssEventHandler$NotifyManager$1 extends android.content.BroadcastReceiver {
	 /* .source "GnssEventHandler.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/location/GnssEventHandler$NotifyManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.location.GnssEventHandler$NotifyManager this$1; //synthetic
/* # direct methods */
 com.android.server.location.GnssEventHandler$NotifyManager$1 ( ) {
/* .locals 0 */
/* .param p1, "this$1" # Lcom/android/server/location/GnssEventHandler$NotifyManager; */
/* .line 186 */
this.this$1 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 7 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 189 */
(( android.content.Context ) p1 ).getApplicationContext ( ); // invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;
/* .line 190 */
/* .local v0, "mContext":Landroid/content/Context; */
(( android.content.Context ) v0 ).getPackageManager ( ); // invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 191 */
/* .local v1, "mPackageManager":Landroid/content/pm/PackageManager; */
final String v2 = "appops"; // const-string v2, "appops"
(( android.content.Context ) v0 ).getSystemService ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v2, Landroid/app/AppOpsManager; */
/* .line 193 */
/* .local v2, "appOpsManager":Landroid/app/AppOpsManager; */
int v3 = 0; // const/4 v3, 0x0
try { // :try_start_0
	 v4 = this.this$1;
	 v4 = this.this$0;
	 com.android.server.location.GnssEventHandler .-$$Nest$fgetpkgName ( v4 );
	 (( android.content.pm.PackageManager ) v1 ).getApplicationInfo ( v4, v3 ); // invoke-virtual {v1, v4, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
	 /* iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I */
	 /* .line 195 */
	 /* .local v4, "mUid":I */
	 v5 = this.this$1;
	 v5 = this.this$0;
	 com.android.server.location.GnssEventHandler .-$$Nest$fgetpkgName ( v5 );
	 /* const/16 v6, 0x2734 */
	 (( android.app.AppOpsManager ) v2 ).setMode ( v6, v4, v5, v3 ); // invoke-virtual {v2, v6, v4, v5, v3}, Landroid/app/AppOpsManager;->setMode(IILjava/lang/String;I)V
	 /* .line 196 */
	 v5 = this.this$1;
	 com.android.server.location.GnssEventHandler$NotifyManager .-$$Nest$fgetmCloseBlurLocationListener ( v5 );
	 if ( v5 != null) { // if-eqz v5, :cond_0
		 /* .line 197 */
		 v5 = this.this$1;
		 v5 = this.this$0;
		 com.android.server.location.GnssEventHandler .-$$Nest$fgetmAppContext ( v5 );
		 v6 = this.this$1;
		 com.android.server.location.GnssEventHandler$NotifyManager .-$$Nest$fgetmCloseBlurLocationListener ( v6 );
		 (( android.content.Context ) v5 ).unregisterReceiver ( v6 ); // invoke-virtual {v5, v6}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
		 /* :try_end_0 */
		 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 201 */
	 } // .end local v4 # "mUid":I
} // :cond_0
/* .line 199 */
/* :catch_0 */
/* move-exception v4 */
/* .line 200 */
/* .local v4, "e":Ljava/lang/Exception; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "exception in close blur location : "; // const-string v6, "exception in close blur location : "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
android.util.Log .getStackTraceString ( v4 );
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v6 = "GnssNps"; // const-string v6, "GnssNps"
android.util.Log .e ( v6,v5 );
/* .line 202 */
} // .end local v4 # "e":Ljava/lang/Exception;
} // :goto_0
v4 = this.this$1;
(( com.android.server.location.GnssEventHandler$NotifyManager ) v4 ).removeNotification ( ); // invoke-virtual {v4}, Lcom/android/server/location/GnssEventHandler$NotifyManager;->removeNotification()V
/* .line 203 */
v4 = this.this$1;
v4 = this.this$0;
com.android.server.location.GnssEventHandler .-$$Nest$fputmBlur ( v4,v3 );
/* .line 204 */
return;
} // .end method
