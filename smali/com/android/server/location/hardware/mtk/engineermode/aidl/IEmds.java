public abstract class com.android.server.location.hardware.mtk.engineermode.aidl.IEmds implements android.os.IInterface {
	 /* .source "IEmds.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;, */
	 /* Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Default; */
	 /* } */
} // .end annotation
/* # static fields */
public static final java.lang.String DESCRIPTOR;
public static final java.lang.String HASH;
public static final Integer VERSION;
/* # virtual methods */
public abstract Integer btDoTest ( Integer p0, Integer p1, Integer p2, Integer p3, Integer p4, Integer p5, Integer p6 ) {
	 /* .annotation system Ldalvik/annotation/Throws; */
	 /* value = { */
	 /* Landroid/os/RemoteException; */
	 /* } */
} // .end annotation
} // .end method
public abstract btEndNoSigRxTest ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract btHciCommandRun ( Object[] p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Integer btInit ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean btIsBLEEnhancedSupport ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Integer btIsBLESupport ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Integer btIsComboSupport ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Integer btIsEmSupport ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Integer btPollingStart ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Integer btPollingStop ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean btStartNoSigRxTest ( Integer p0, Integer p1, Integer p2, Integer p3 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Integer btStartRelayer ( Integer p0, Integer p1 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Integer btStopRelayer ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Integer btUninit ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean connect ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void disconnect ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean getFilePathListWithCallBack ( java.lang.String p0, com.android.server.location.hardware.mtk.engineermode.aidl.IEmCallbacks p1 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract java.lang.String getInterfaceHash ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Integer getInterfaceVersion ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Integer isGauge30Support ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Integer isNfcSupport ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean readFileConfigWithCallBack ( java.lang.String p0, com.android.server.location.hardware.mtk.engineermode.aidl.IEmCallbacks p1 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract readMnlConfigFile ( Integer[] p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void sendNfcRequest ( Integer p0, Object[] p1 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean sendToServer ( java.lang.String p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean sendToServerWithCallBack ( java.lang.String p0, com.android.server.location.hardware.mtk.engineermode.aidl.IEmCallbacks p1 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean setBypassDis ( java.lang.String p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean setBypassEn ( java.lang.String p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean setBypassService ( java.lang.String p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void setCallback ( com.android.server.location.hardware.mtk.engineermode.aidl.IEmCallbacks p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean setCtIREngMode ( java.lang.String p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean setDisableC2kCap ( java.lang.String p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean setDsbpSupport ( java.lang.String p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean setEmConfigure ( java.lang.String p0, java.lang.String p1 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean setEmUsbType ( java.lang.String p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean setEmUsbValue ( java.lang.String p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean setImsTestMode ( java.lang.String p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean setMdResetDelay ( java.lang.String p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean setModemWarningEnable ( java.lang.String p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean setMoms ( java.lang.String p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void setNfcResponseFunction ( com.android.server.location.hardware.mtk.engineermode.aidl.IEmResponses p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean setPreferGprsMode ( java.lang.String p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean setRadioCapabilitySwitchEnable ( java.lang.String p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean setSmsFormat ( java.lang.String p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean setTestSimCardType ( java.lang.String p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean setUsbOtgSwitch ( java.lang.String p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean setUsbPort ( java.lang.String p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean setVolteMalPctid ( java.lang.String p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean setWcnCoreDump ( java.lang.String p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean writeFileConfig ( java.lang.String p0, java.lang.String p1 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean writeMnlConfigFile ( Object[] p0, Integer[] p1 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean writeOtaFiles ( java.lang.String p0, Object[] p1 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
