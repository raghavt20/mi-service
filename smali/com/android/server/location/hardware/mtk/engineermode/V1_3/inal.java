public class inal implements com.android.server.location.hardware.mtk.engineermode.V1_3.IEmd {
	 /* .source "IEmd.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x19 */
/* name = "Proxy" */
} // .end annotation
/* # static fields */
public static final java.lang.String INTF_EMD_1_3;
/* # instance fields */
private android.os.IHwBinder mRemote;
/* # direct methods */
public inal ( ) {
/* .locals 0 */
/* .param p1, "iHwBinder" # Landroid/os/IHwBinder; */
/* .line 28 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 29 */
java.util.Objects .requireNonNull ( p1 );
/* .line 30 */
this.mRemote = p1;
/* .line 31 */
return;
} // .end method
/* # virtual methods */
public android.os.IHwBinder asBinder ( ) {
/* .locals 1 */
/* .line 35 */
v0 = this.mRemote;
} // .end method
public Integer btDoTest ( Integer p0, Integer p1, Integer p2, Integer p3, Integer p4, Integer p5, Integer p6 ) {
/* .locals 5 */
/* .param p1, "i" # I */
/* .param p2, "i2" # I */
/* .param p3, "i3" # I */
/* .param p4, "i4" # I */
/* .param p5, "i5" # I */
/* .param p6, "i6" # I */
/* .param p7, "i7" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 40 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 41 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 42 */
(( android.os.HwParcel ) v0 ).writeInt32 ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 43 */
(( android.os.HwParcel ) v0 ).writeInt32 ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 44 */
(( android.os.HwParcel ) v0 ).writeInt32 ( p3 ); // invoke-virtual {v0, p3}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 45 */
(( android.os.HwParcel ) v0 ).writeInt32 ( p4 ); // invoke-virtual {v0, p4}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 46 */
(( android.os.HwParcel ) v0 ).writeInt32 ( p5 ); // invoke-virtual {v0, p5}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 47 */
(( android.os.HwParcel ) v0 ).writeInt32 ( p6 ); // invoke-virtual {v0, p6}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 48 */
(( android.os.HwParcel ) v0 ).writeInt32 ( p7 ); // invoke-virtual {v0, p7}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 49 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 51 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0x13 */
int v4 = 0; // const/4 v4, 0x0
/* .line 52 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 53 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 54 */
v2 = (( android.os.HwParcel ) v1 ).readInt32 ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readInt32()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 56 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 54 */
/* .line 56 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 57 */
/* throw v2 */
} // .end method
public java.util.ArrayList btEndNoSigRxTest ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 62 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 63 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 64 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 66 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0x16 */
int v4 = 0; // const/4 v4, 0x0
/* .line 67 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 68 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 69 */
(( android.os.HwParcel ) v1 ).readInt32Vector ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readInt32Vector()Ljava/util/ArrayList;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 71 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 69 */
/* .line 71 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 72 */
/* throw v2 */
} // .end method
public java.util.ArrayList btHciCommandRun ( java.util.ArrayList p0 ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Byte;", */
/* ">;)", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Byte;", */
/* ">;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 77 */
/* .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;" */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 78 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 79 */
(( android.os.HwParcel ) v0 ).writeInt8Vector ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeInt8Vector(Ljava/util/ArrayList;)V
/* .line 80 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 82 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0x14 */
int v4 = 0; // const/4 v4, 0x0
/* .line 83 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 84 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 85 */
(( android.os.HwParcel ) v1 ).readInt8Vector ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readInt8Vector()Ljava/util/ArrayList;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 87 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 85 */
/* .line 87 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 88 */
/* throw v2 */
} // .end method
public Integer btInit ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 93 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 94 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 95 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 97 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0x11 */
int v4 = 0; // const/4 v4, 0x0
/* .line 98 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 99 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 100 */
v2 = (( android.os.HwParcel ) v1 ).readInt32 ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readInt32()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 102 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 100 */
/* .line 102 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 103 */
/* throw v2 */
} // .end method
public Boolean btIsBLEEnhancedSupport ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 108 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 109 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 110 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 112 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0x10 */
int v4 = 0; // const/4 v4, 0x0
/* .line 113 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 114 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 115 */
v2 = (( android.os.HwParcel ) v1 ).readBool ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 117 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 115 */
/* .line 117 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 118 */
/* throw v2 */
} // .end method
public Integer btIsBLESupport ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 123 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 124 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 125 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 127 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0xf */
int v4 = 0; // const/4 v4, 0x0
/* .line 128 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 129 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 130 */
v2 = (( android.os.HwParcel ) v1 ).readInt32 ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readInt32()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 132 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 130 */
/* .line 132 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 133 */
/* throw v2 */
} // .end method
public Integer btIsComboSupport ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 138 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 139 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 140 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 142 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0x17 */
int v4 = 0; // const/4 v4, 0x0
/* .line 143 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 144 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 145 */
v2 = (( android.os.HwParcel ) v1 ).readInt32 ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readInt32()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 147 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 145 */
/* .line 147 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 148 */
/* throw v2 */
} // .end method
public Integer btIsEmSupport ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 153 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 154 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.2::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 155 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 157 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0x2f */
int v4 = 0; // const/4 v4, 0x0
/* .line 158 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 159 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 160 */
v2 = (( android.os.HwParcel ) v1 ).readInt32 ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readInt32()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 162 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 160 */
/* .line 162 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 163 */
/* throw v2 */
} // .end method
public Integer btPollingStart ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 168 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 169 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 170 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 172 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0x18 */
int v4 = 0; // const/4 v4, 0x0
/* .line 173 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 174 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 175 */
v2 = (( android.os.HwParcel ) v1 ).readInt32 ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readInt32()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 177 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 175 */
/* .line 177 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 178 */
/* throw v2 */
} // .end method
public Integer btPollingStop ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 183 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 184 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 185 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 187 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0x19 */
int v4 = 0; // const/4 v4, 0x0
/* .line 188 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 189 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 190 */
v2 = (( android.os.HwParcel ) v1 ).readInt32 ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readInt32()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 192 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 190 */
/* .line 192 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 193 */
/* throw v2 */
} // .end method
public Boolean btStartNoSigRxTest ( Integer p0, Integer p1, Integer p2, Integer p3 ) {
/* .locals 5 */
/* .param p1, "i" # I */
/* .param p2, "i2" # I */
/* .param p3, "i3" # I */
/* .param p4, "i4" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 198 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 199 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 200 */
(( android.os.HwParcel ) v0 ).writeInt32 ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 201 */
(( android.os.HwParcel ) v0 ).writeInt32 ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 202 */
(( android.os.HwParcel ) v0 ).writeInt32 ( p3 ); // invoke-virtual {v0, p3}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 203 */
(( android.os.HwParcel ) v0 ).writeInt32 ( p4 ); // invoke-virtual {v0, p4}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 204 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 206 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0x15 */
int v4 = 0; // const/4 v4, 0x0
/* .line 207 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 208 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 209 */
v2 = (( android.os.HwParcel ) v1 ).readBool ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 211 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 209 */
/* .line 211 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 212 */
/* throw v2 */
} // .end method
public Integer btStartRelayer ( Integer p0, Integer p1 ) {
/* .locals 5 */
/* .param p1, "i" # I */
/* .param p2, "i2" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 217 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 218 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 219 */
(( android.os.HwParcel ) v0 ).writeInt32 ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 220 */
(( android.os.HwParcel ) v0 ).writeInt32 ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 221 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 223 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0xd */
int v4 = 0; // const/4 v4, 0x0
/* .line 224 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 225 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 226 */
v2 = (( android.os.HwParcel ) v1 ).readInt32 ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readInt32()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 228 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 226 */
/* .line 228 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 229 */
/* throw v2 */
} // .end method
public Integer btStopRelayer ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 234 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 235 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 236 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 238 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0xe */
int v4 = 0; // const/4 v4, 0x0
/* .line 239 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 240 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 241 */
v2 = (( android.os.HwParcel ) v1 ).readInt32 ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readInt32()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 243 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 241 */
/* .line 243 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 244 */
/* throw v2 */
} // .end method
public Integer btUninit ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 249 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 250 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 251 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 253 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0x12 */
int v4 = 0; // const/4 v4, 0x0
/* .line 254 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 255 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 256 */
v2 = (( android.os.HwParcel ) v1 ).readInt32 ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readInt32()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 258 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 256 */
/* .line 258 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 259 */
/* throw v2 */
} // .end method
public Boolean clearItemsforRsc ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 264 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 265 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 266 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 268 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0x2c */
int v4 = 0; // const/4 v4, 0x0
/* .line 269 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 270 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 271 */
v2 = (( android.os.HwParcel ) v1 ).readBool ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 273 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 271 */
/* .line 273 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 274 */
/* throw v2 */
} // .end method
public Boolean connect ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 279 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 280 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.3::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 281 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 283 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0x35 */
int v4 = 0; // const/4 v4, 0x0
/* .line 284 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 285 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 286 */
v2 = (( android.os.HwParcel ) v1 ).readBool ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 288 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 286 */
/* .line 288 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 289 */
/* throw v2 */
} // .end method
public void debug ( android.os.NativeHandle p0, java.util.ArrayList p1 ) {
/* .locals 5 */
/* .param p1, "nativeHandle" # Landroid/os/NativeHandle; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/os/NativeHandle;", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 294 */
/* .local p2, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 295 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
final String v1 = "android.hidl.base@1.0::IBase"; // const-string v1, "android.hidl.base@1.0::IBase"
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 296 */
(( android.os.HwParcel ) v0 ).writeNativeHandle ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeNativeHandle(Landroid/os/NativeHandle;)V
/* .line 297 */
(( android.os.HwParcel ) v0 ).writeStringVector ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/HwParcel;->writeStringVector(Ljava/util/ArrayList;)V
/* .line 298 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 300 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const v3, 0xf444247 */
int v4 = 0; // const/4 v4, 0x0
/* .line 301 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 302 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 304 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 305 */
/* nop */
/* .line 306 */
return;
/* .line 304 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 305 */
/* throw v2 */
} // .end method
public void disconnect ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 310 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 311 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.3::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 312 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 314 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0x36 */
int v4 = 0; // const/4 v4, 0x0
/* .line 315 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 316 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 318 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 319 */
/* nop */
/* .line 320 */
return;
/* .line 318 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 319 */
/* throw v2 */
} // .end method
public final Boolean equals ( java.lang.Object p0 ) {
/* .locals 1 */
/* .param p1, "obj" # Ljava/lang/Object; */
/* .line 323 */
v0 = android.os.HidlSupport .interfacesEqual ( p0,p1 );
} // .end method
public Boolean genMdLogFilter ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 5 */
/* .param p1, "str" # Ljava/lang/String; */
/* .param p2, "str2" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 328 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 329 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 330 */
(( android.os.HwParcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 331 */
(( android.os.HwParcel ) v0 ).writeString ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 332 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 334 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0x23 */
int v4 = 0; // const/4 v4, 0x0
/* .line 335 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 336 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 337 */
v2 = (( android.os.HwParcel ) v1 ).readBool ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 339 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 337 */
/* .line 339 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 340 */
/* throw v2 */
} // .end method
public android.hidl.base.V1_0.DebugInfo getDebugInfo ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 345 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 346 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
final String v1 = "android.hidl.base@1.0::IBase"; // const-string v1, "android.hidl.base@1.0::IBase"
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 347 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 349 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const v3, 0xf524546 */
int v4 = 0; // const/4 v4, 0x0
/* .line 350 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 351 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 352 */
/* new-instance v2, Landroid/hidl/base/V1_0/DebugInfo; */
/* invoke-direct {v2}, Landroid/hidl/base/V1_0/DebugInfo;-><init>()V */
/* .line 353 */
/* .local v2, "debugInfo":Landroid/hidl/base/V1_0/DebugInfo; */
(( android.hidl.base.V1_0.DebugInfo ) v2 ).readFromParcel ( v1 ); // invoke-virtual {v2, v1}, Landroid/hidl/base/V1_0/DebugInfo;->readFromParcel(Landroid/os/HwParcel;)V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 354 */
/* nop */
/* .line 356 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 354 */
/* .line 356 */
} // .end local v2 # "debugInfo":Landroid/hidl/base/V1_0/DebugInfo;
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 357 */
/* throw v2 */
} // .end method
public Boolean getFilePathListWithCallBack ( java.lang.String p0, com.android.server.location.hardware.mtk.engineermode.V1_0.IEmCallback p1 ) {
/* .locals 5 */
/* .param p1, "str" # Ljava/lang/String; */
/* .param p2, "iEmCallback" # Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 362 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 363 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 364 */
(( android.os.HwParcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 365 */
/* if-nez p2, :cond_0 */
int v1 = 0; // const/4 v1, 0x0
} // :cond_0
} // :goto_0
(( android.os.HwParcel ) v0 ).writeStrongBinder ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeStrongBinder(Landroid/os/IHwBinder;)V
/* .line 366 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 368 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0x2d */
int v4 = 0; // const/4 v4, 0x0
/* .line 369 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 370 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 371 */
v2 = (( android.os.HwParcel ) v1 ).readBool ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 373 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 371 */
/* .line 373 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 374 */
/* throw v2 */
} // .end method
public java.util.ArrayList getHashChain ( ) {
/* .locals 13 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "[B>;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 379 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 380 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
final String v1 = "android.hidl.base@1.0::IBase"; // const-string v1, "android.hidl.base@1.0::IBase"
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 381 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 383 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const v3, 0xf485348 */
int v4 = 0; // const/4 v4, 0x0
/* .line 384 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 385 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 386 */
/* new-instance v2, Ljava/util/ArrayList; */
/* invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V */
/* move-object v10, v2 */
/* .line 387 */
/* .local v10, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;" */
/* const-wide/16 v2, 0x10 */
(( android.os.HwParcel ) v1 ).readBuffer ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/os/HwParcel;->readBuffer(J)Landroid/os/HwBlob;
/* move-object v11, v2 */
/* .line 388 */
/* .local v11, "readBuffer":Landroid/os/HwBlob; */
/* const-wide/16 v2, 0x8 */
v2 = (( android.os.HwBlob ) v11 ).getInt32 ( v2, v3 ); // invoke-virtual {v11, v2, v3}, Landroid/os/HwBlob;->getInt32(J)I
/* move v12, v2 */
/* .line 389 */
/* .local v12, "int32":I */
/* mul-int/lit8 v2, v12, 0x20 */
/* int-to-long v3, v2 */
(( android.os.HwBlob ) v11 ).handle ( ); // invoke-virtual {v11}, Landroid/os/HwBlob;->handle()J
/* move-result-wide v5 */
/* const-wide/16 v7, 0x0 */
int v9 = 1; // const/4 v9, 0x1
/* move-object v2, v1 */
/* invoke-virtual/range {v2 ..v9}, Landroid/os/HwParcel;->readEmbeddedBuffer(JJJZ)Landroid/os/HwBlob; */
/* .line 390 */
/* .local v2, "readEmbeddedBuffer":Landroid/os/HwBlob; */
(( java.util.ArrayList ) v10 ).clear ( ); // invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V
/* .line 391 */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
/* if-ge v3, v12, :cond_0 */
/* .line 392 */
/* const/16 v4, 0x20 */
/* new-array v5, v4, [B */
/* .line 393 */
/* .local v5, "bArr":[B */
/* mul-int/lit8 v6, v3, 0x20 */
/* int-to-long v6, v6 */
(( android.os.HwBlob ) v2 ).copyToInt8Array ( v6, v7, v5, v4 ); // invoke-virtual {v2, v6, v7, v5, v4}, Landroid/os/HwBlob;->copyToInt8Array(J[BI)V
/* .line 394 */
(( java.util.ArrayList ) v10 ).add ( v5 ); // invoke-virtual {v10, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 391 */
/* nop */
} // .end local v5 # "bArr":[B
/* add-int/lit8 v3, v3, 0x1 */
/* .line 396 */
} // .end local v3 # "i":I
} // :cond_0
/* nop */
/* .line 398 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 396 */
/* .line 398 */
} // .end local v2 # "readEmbeddedBuffer":Landroid/os/HwBlob;
} // .end local v10 # "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
} // .end local v11 # "readBuffer":Landroid/os/HwBlob;
} // .end local v12 # "int32":I
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 399 */
/* throw v2 */
} // .end method
public final Integer hashCode ( ) {
/* .locals 1 */
/* .line 403 */
(( com.android.server.location.hardware.mtk.engineermode.V1_3.IEmd$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Proxy;->asBinder()Landroid/os/IHwBinder;
v0 = (( java.lang.Object ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I
} // .end method
public java.util.ArrayList interfaceChain ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 408 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 409 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
final String v1 = "android.hidl.base@1.0::IBase"; // const-string v1, "android.hidl.base@1.0::IBase"
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 410 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 412 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const v3, 0xf43484e */
int v4 = 0; // const/4 v4, 0x0
/* .line 413 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 414 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 415 */
(( android.os.HwParcel ) v1 ).readStringVector ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readStringVector()Ljava/util/ArrayList;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 417 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 415 */
/* .line 417 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 418 */
/* throw v2 */
} // .end method
public java.lang.String interfaceDescriptor ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 423 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 424 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
final String v1 = "android.hidl.base@1.0::IBase"; // const-string v1, "android.hidl.base@1.0::IBase"
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 425 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 427 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const v3, 0xf445343 */
int v4 = 0; // const/4 v4, 0x0
/* .line 428 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 429 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 430 */
(( android.os.HwParcel ) v1 ).readString ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 432 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 430 */
/* .line 432 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 433 */
/* throw v2 */
} // .end method
public Integer isGauge30Support ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 438 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 439 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.2::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 440 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 442 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0x31 */
int v4 = 0; // const/4 v4, 0x0
/* .line 443 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 444 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 445 */
v2 = (( android.os.HwParcel ) v1 ).readInt32 ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readInt32()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 447 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 445 */
/* .line 447 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 448 */
/* throw v2 */
} // .end method
public Integer isNfcSupport ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 453 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 454 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.2::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 455 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 457 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0x30 */
int v4 = 0; // const/4 v4, 0x0
/* .line 458 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 459 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 460 */
v2 = (( android.os.HwParcel ) v1 ).readInt32 ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readInt32()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 462 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 460 */
/* .line 462 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 463 */
/* throw v2 */
} // .end method
public Boolean linkToDeath ( android.os.IHwBinder$DeathRecipient p0, Long p1 ) {
/* .locals 1 */
/* .param p1, "deathRecipient" # Landroid/os/IHwBinder$DeathRecipient; */
/* .param p2, "j" # J */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 468 */
v0 = v0 = this.mRemote;
} // .end method
public void notifySyspropsChanged ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 473 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 474 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
final String v1 = "android.hidl.base@1.0::IBase"; // const-string v1, "android.hidl.base@1.0::IBase"
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 475 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 477 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const v3, 0xf535953 */
int v4 = 1; // const/4 v4, 0x1
/* .line 478 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 480 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 481 */
/* nop */
/* .line 482 */
return;
/* .line 480 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 481 */
/* throw v2 */
} // .end method
public void ping ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 486 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 487 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
final String v1 = "android.hidl.base@1.0::IBase"; // const-string v1, "android.hidl.base@1.0::IBase"
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 488 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 490 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const v3, 0xf504e47 */
int v4 = 0; // const/4 v4, 0x0
/* .line 491 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 492 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 494 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 495 */
/* nop */
/* .line 496 */
return;
/* .line 494 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 495 */
/* throw v2 */
} // .end method
public java.util.ArrayList readMnlConfigFile ( java.util.ArrayList p0 ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Integer;", */
/* ">;)", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Byte;", */
/* ">;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 500 */
/* .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 501 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.3::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 502 */
(( android.os.HwParcel ) v0 ).writeInt32Vector ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeInt32Vector(Ljava/util/ArrayList;)V
/* .line 503 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 505 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0x32 */
int v4 = 0; // const/4 v4, 0x0
/* .line 506 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 507 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 508 */
(( android.os.HwParcel ) v1 ).readInt8Vector ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readInt8Vector()Ljava/util/ArrayList;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 510 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 508 */
/* .line 510 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 511 */
/* throw v2 */
} // .end method
public void sendNfcRequest ( Integer p0, java.util.ArrayList p1 ) {
/* .locals 5 */
/* .param p1, "i" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Byte;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 516 */
/* .local p2, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;" */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 517 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.3::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 518 */
(( android.os.HwParcel ) v0 ).writeInt32 ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 519 */
(( android.os.HwParcel ) v0 ).writeInt8Vector ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/HwParcel;->writeInt8Vector(Ljava/util/ArrayList;)V
/* .line 520 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 522 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0x37 */
int v4 = 1; // const/4 v4, 0x1
/* .line 523 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 525 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 526 */
/* nop */
/* .line 527 */
return;
/* .line 525 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 526 */
/* throw v2 */
} // .end method
public Boolean sendToServer ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "str" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 531 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 532 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 533 */
(( android.os.HwParcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 534 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 536 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
int v3 = 2; // const/4 v3, 0x2
int v4 = 0; // const/4 v4, 0x0
/* .line 537 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 538 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 539 */
v2 = (( android.os.HwParcel ) v1 ).readBool ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 541 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 539 */
/* .line 541 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 542 */
/* throw v2 */
} // .end method
public Boolean sendToServerWithCallBack ( java.lang.String p0, com.android.server.location.hardware.mtk.engineermode.V1_0.IEmCallback p1 ) {
/* .locals 5 */
/* .param p1, "str" # Ljava/lang/String; */
/* .param p2, "iEmCallback" # Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 547 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 548 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 549 */
(( android.os.HwParcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 550 */
/* if-nez p2, :cond_0 */
int v1 = 0; // const/4 v1, 0x0
} // :cond_0
} // :goto_0
(( android.os.HwParcel ) v0 ).writeStrongBinder ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeStrongBinder(Landroid/os/IHwBinder;)V
/* .line 551 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 553 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
int v3 = 3; // const/4 v3, 0x3
int v4 = 0; // const/4 v4, 0x0
/* .line 554 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 555 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 556 */
v2 = (( android.os.HwParcel ) v1 ).readBool ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 558 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 556 */
/* .line 558 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 559 */
/* throw v2 */
} // .end method
public Boolean setBypassDis ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "str" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 564 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 565 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 566 */
(( android.os.HwParcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 567 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 569 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0x29 */
int v4 = 0; // const/4 v4, 0x0
/* .line 570 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 571 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 572 */
v2 = (( android.os.HwParcel ) v1 ).readBool ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 574 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 572 */
/* .line 574 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 575 */
/* throw v2 */
} // .end method
public Boolean setBypassEn ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "str" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 580 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 581 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 582 */
(( android.os.HwParcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 583 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 585 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0x28 */
int v4 = 0; // const/4 v4, 0x0
/* .line 586 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 587 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 588 */
v2 = (( android.os.HwParcel ) v1 ).readBool ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 590 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 588 */
/* .line 590 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 591 */
/* throw v2 */
} // .end method
public Boolean setBypassService ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "str" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 596 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 597 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 598 */
(( android.os.HwParcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 599 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 601 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0x2a */
int v4 = 0; // const/4 v4, 0x0
/* .line 602 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 603 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 604 */
v2 = (( android.os.HwParcel ) v1 ).readBool ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 606 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 604 */
/* .line 606 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 607 */
/* throw v2 */
} // .end method
public void setCallback ( com.android.server.location.hardware.mtk.engineermode.V1_0.IEmCallback p0 ) {
/* .locals 5 */
/* .param p1, "iEmCallback" # Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 612 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 613 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 614 */
/* if-nez p1, :cond_0 */
int v1 = 0; // const/4 v1, 0x0
} // :cond_0
} // :goto_0
(( android.os.HwParcel ) v0 ).writeStrongBinder ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeStrongBinder(Landroid/os/IHwBinder;)V
/* .line 615 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 617 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
int v3 = 1; // const/4 v3, 0x1
int v4 = 0; // const/4 v4, 0x0
/* .line 618 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 619 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 621 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 622 */
/* nop */
/* .line 623 */
return;
/* .line 621 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 622 */
/* throw v2 */
} // .end method
public Boolean setCtIREngMode ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "str" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 627 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 628 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 629 */
(( android.os.HwParcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 630 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 632 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
int v3 = 5; // const/4 v3, 0x5
int v4 = 0; // const/4 v4, 0x0
/* .line 633 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 634 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 635 */
v2 = (( android.os.HwParcel ) v1 ).readBool ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 637 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 635 */
/* .line 637 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 638 */
/* throw v2 */
} // .end method
public Boolean setDisableC2kCap ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "str" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 643 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 644 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 645 */
(( android.os.HwParcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 646 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 648 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0x9 */
int v4 = 0; // const/4 v4, 0x0
/* .line 649 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 650 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 651 */
v2 = (( android.os.HwParcel ) v1 ).readBool ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 653 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 651 */
/* .line 653 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 654 */
/* throw v2 */
} // .end method
public Boolean setDsbpSupport ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "str" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 659 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 660 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 661 */
(( android.os.HwParcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 662 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 664 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0xb */
int v4 = 0; // const/4 v4, 0x0
/* .line 665 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 666 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 667 */
v2 = (( android.os.HwParcel ) v1 ).readBool ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 669 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 667 */
/* .line 669 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 670 */
/* throw v2 */
} // .end method
public Boolean setEmConfigure ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 5 */
/* .param p1, "str" # Ljava/lang/String; */
/* .param p2, "str2" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 675 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 676 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.1::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 677 */
(( android.os.HwParcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 678 */
(( android.os.HwParcel ) v0 ).writeString ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 679 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 681 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0x2e */
int v4 = 0; // const/4 v4, 0x0
/* .line 682 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 683 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 684 */
v2 = (( android.os.HwParcel ) v1 ).readBool ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 686 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 684 */
/* .line 686 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 687 */
/* throw v2 */
} // .end method
public Boolean setEmUsbType ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "str" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 692 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 693 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 694 */
(( android.os.HwParcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 695 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 697 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0x27 */
int v4 = 0; // const/4 v4, 0x0
/* .line 698 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 699 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 700 */
v2 = (( android.os.HwParcel ) v1 ).readBool ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 702 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 700 */
/* .line 702 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 703 */
/* throw v2 */
} // .end method
public Boolean setEmUsbValue ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "str" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 708 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 709 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 710 */
(( android.os.HwParcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 711 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 713 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0x26 */
int v4 = 0; // const/4 v4, 0x0
/* .line 714 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 715 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 716 */
v2 = (( android.os.HwParcel ) v1 ).readBool ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 718 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 716 */
/* .line 718 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 719 */
/* throw v2 */
} // .end method
public void setHALInstrumentation ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 724 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 725 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
final String v1 = "android.hidl.base@1.0::IBase"; // const-string v1, "android.hidl.base@1.0::IBase"
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 726 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 728 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const v3, 0xf494e54 */
int v4 = 1; // const/4 v4, 0x1
/* .line 729 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 731 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 732 */
/* nop */
/* .line 733 */
return;
/* .line 731 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 732 */
/* throw v2 */
} // .end method
public Boolean setImsTestMode ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "str" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 737 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 738 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 739 */
(( android.os.HwParcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 740 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 742 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0xa */
int v4 = 0; // const/4 v4, 0x0
/* .line 743 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 744 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 745 */
v2 = (( android.os.HwParcel ) v1 ).readBool ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 747 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 745 */
/* .line 747 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 748 */
/* throw v2 */
} // .end method
public Boolean setMdResetDelay ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "str" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 753 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 754 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 755 */
(( android.os.HwParcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 756 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 758 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0x1a */
int v4 = 0; // const/4 v4, 0x0
/* .line 759 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 760 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 761 */
v2 = (( android.os.HwParcel ) v1 ).readBool ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 763 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 761 */
/* .line 763 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 764 */
/* throw v2 */
} // .end method
public Boolean setModemWarningEnable ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "str" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 769 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 770 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 771 */
(( android.os.HwParcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 772 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 774 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0x22 */
int v4 = 0; // const/4 v4, 0x0
/* .line 775 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 776 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 777 */
v2 = (( android.os.HwParcel ) v1 ).readBool ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 779 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 777 */
/* .line 779 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 780 */
/* throw v2 */
} // .end method
public Boolean setMoms ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "str" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 785 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 786 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 787 */
(( android.os.HwParcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 788 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 790 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0x2b */
int v4 = 0; // const/4 v4, 0x0
/* .line 791 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 792 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 793 */
v2 = (( android.os.HwParcel ) v1 ).readBool ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 795 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 793 */
/* .line 795 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 796 */
/* throw v2 */
} // .end method
public void setNfcResponseFunction ( com.android.server.location.hardware.mtk.engineermode.V1_3.IEmNfcResponse p0 ) {
/* .locals 5 */
/* .param p1, "iEmNfcResponse" # Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmNfcResponse; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 801 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 802 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.3::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 803 */
/* if-nez p1, :cond_0 */
int v1 = 0; // const/4 v1, 0x0
} // :cond_0
} // :goto_0
(( android.os.HwParcel ) v0 ).writeStrongBinder ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeStrongBinder(Landroid/os/IHwBinder;)V
/* .line 804 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 806 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0x34 */
int v4 = 0; // const/4 v4, 0x0
/* .line 807 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 808 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 810 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 811 */
/* nop */
/* .line 812 */
return;
/* .line 810 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 811 */
/* throw v2 */
} // .end method
public Boolean setOmxCoreLogEnable ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "str" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 816 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 817 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 818 */
(( android.os.HwParcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 819 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 821 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0x20 */
int v4 = 0; // const/4 v4, 0x0
/* .line 822 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 823 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 824 */
v2 = (( android.os.HwParcel ) v1 ).readBool ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 826 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 824 */
/* .line 826 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 827 */
/* throw v2 */
} // .end method
public Boolean setOmxVdecLogEnable ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "str" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 832 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 833 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 834 */
(( android.os.HwParcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 835 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 837 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0x1d */
int v4 = 0; // const/4 v4, 0x0
/* .line 838 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 839 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 840 */
v2 = (( android.os.HwParcel ) v1 ).readBool ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 842 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 840 */
/* .line 842 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 843 */
/* throw v2 */
} // .end method
public Boolean setOmxVencLogEnable ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "str" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 848 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 849 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 850 */
(( android.os.HwParcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 851 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 853 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0x1c */
int v4 = 0; // const/4 v4, 0x0
/* .line 854 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 855 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 856 */
v2 = (( android.os.HwParcel ) v1 ).readBool ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 858 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 856 */
/* .line 858 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 859 */
/* throw v2 */
} // .end method
public Boolean setPreferGprsMode ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "str" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 864 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 865 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 866 */
(( android.os.HwParcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 867 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 869 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
int v3 = 7; // const/4 v3, 0x7
int v4 = 0; // const/4 v4, 0x0
/* .line 870 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 871 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 872 */
v2 = (( android.os.HwParcel ) v1 ).readBool ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 874 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 872 */
/* .line 874 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 875 */
/* throw v2 */
} // .end method
public Boolean setRadioCapabilitySwitchEnable ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "str" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 880 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 881 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 882 */
(( android.os.HwParcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 883 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 885 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0x8 */
int v4 = 0; // const/4 v4, 0x0
/* .line 886 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 887 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 888 */
v2 = (( android.os.HwParcel ) v1 ).readBool ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 890 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 888 */
/* .line 890 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 891 */
/* throw v2 */
} // .end method
public Boolean setSmsFormat ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "str" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 896 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 897 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 898 */
(( android.os.HwParcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 899 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 901 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
int v3 = 4; // const/4 v3, 0x4
int v4 = 0; // const/4 v4, 0x0
/* .line 902 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 903 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 904 */
v2 = (( android.os.HwParcel ) v1 ).readBool ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 906 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 904 */
/* .line 906 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 907 */
/* throw v2 */
} // .end method
public Boolean setSvpLogEnable ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "str" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 912 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 913 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 914 */
(( android.os.HwParcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 915 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 917 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0x1f */
int v4 = 0; // const/4 v4, 0x0
/* .line 918 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 919 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 920 */
v2 = (( android.os.HwParcel ) v1 ).readBool ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 922 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 920 */
/* .line 922 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 923 */
/* throw v2 */
} // .end method
public Boolean setTestSimCardType ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "str" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 928 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 929 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 930 */
(( android.os.HwParcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 931 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 933 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
int v3 = 6; // const/4 v3, 0x6
int v4 = 0; // const/4 v4, 0x0
/* .line 934 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 935 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 936 */
v2 = (( android.os.HwParcel ) v1 ).readBool ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 938 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 936 */
/* .line 938 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 939 */
/* throw v2 */
} // .end method
public Boolean setUsbOtgSwitch ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "str" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 944 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 945 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 946 */
(( android.os.HwParcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 947 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 949 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0x25 */
int v4 = 0; // const/4 v4, 0x0
/* .line 950 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 951 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 952 */
v2 = (( android.os.HwParcel ) v1 ).readBool ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 954 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 952 */
/* .line 954 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 955 */
/* throw v2 */
} // .end method
public Boolean setUsbPort ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "str" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 960 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 961 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 962 */
(( android.os.HwParcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 963 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 965 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0x24 */
int v4 = 0; // const/4 v4, 0x0
/* .line 966 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 967 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 968 */
v2 = (( android.os.HwParcel ) v1 ).readBool ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 970 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 968 */
/* .line 970 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 971 */
/* throw v2 */
} // .end method
public Boolean setVdecDriverLogEnable ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "str" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 976 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 977 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 978 */
(( android.os.HwParcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 979 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 981 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0x1e */
int v4 = 0; // const/4 v4, 0x0
/* .line 982 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 983 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 984 */
v2 = (( android.os.HwParcel ) v1 ).readBool ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 986 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 984 */
/* .line 986 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 987 */
/* throw v2 */
} // .end method
public Boolean setVencDriverLogEnable ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "str" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 992 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 993 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 994 */
(( android.os.HwParcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 995 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 997 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0x21 */
int v4 = 0; // const/4 v4, 0x0
/* .line 998 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 999 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 1000 */
v2 = (( android.os.HwParcel ) v1 ).readBool ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1002 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 1000 */
/* .line 1002 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 1003 */
/* throw v2 */
} // .end method
public Boolean setVolteMalPctid ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "str" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1008 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 1009 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1010 */
(( android.os.HwParcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 1011 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 1013 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0xc */
int v4 = 0; // const/4 v4, 0x0
/* .line 1014 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 1015 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 1016 */
v2 = (( android.os.HwParcel ) v1 ).readBool ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1018 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 1016 */
/* .line 1018 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 1019 */
/* throw v2 */
} // .end method
public Boolean setWcnCoreDump ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "str" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1024 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 1025 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1026 */
(( android.os.HwParcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 1027 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 1029 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0x1b */
int v4 = 0; // const/4 v4, 0x0
/* .line 1030 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 1031 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 1032 */
v2 = (( android.os.HwParcel ) v1 ).readBool ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1034 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 1032 */
/* .line 1034 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 1035 */
/* throw v2 */
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 1040 */
try { // :try_start_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( com.android.server.location.hardware.mtk.engineermode.V1_3.IEmd$Proxy ) p0 ).interfaceDescriptor ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Proxy;->interfaceDescriptor()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "@Proxy"; // const-string v1, "@Proxy"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1041 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1042 */
/* .local v0, "e":Landroid/os/RemoteException; */
final String v1 = "[class or subclass of vendor.mediatek.hardware.engineermode@1.3::IEmd]@Proxy"; // const-string v1, "[class or subclass of vendor.mediatek.hardware.engineermode@1.3::IEmd]@Proxy"
} // .end method
public Boolean unlinkToDeath ( android.os.IHwBinder$DeathRecipient p0 ) {
/* .locals 1 */
/* .param p1, "deathRecipient" # Landroid/os/IHwBinder$DeathRecipient; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1048 */
v0 = v0 = this.mRemote;
} // .end method
public Boolean writeMnlConfigFile ( java.util.ArrayList p0, java.util.ArrayList p1 ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Byte;", */
/* ">;", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Integer;", */
/* ">;)Z" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1053 */
/* .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;" */
/* .local p2, "arrayList2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 1054 */
/* .local v0, "hwParcel":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.3::IEmd" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1055 */
(( android.os.HwParcel ) v0 ).writeInt8Vector ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeInt8Vector(Ljava/util/ArrayList;)V
/* .line 1056 */
(( android.os.HwParcel ) v0 ).writeInt32Vector ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/HwParcel;->writeInt32Vector(Ljava/util/ArrayList;)V
/* .line 1057 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 1059 */
/* .local v1, "hwParcel2":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0x33 */
int v4 = 0; // const/4 v4, 0x0
/* .line 1060 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 1061 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 1062 */
v2 = (( android.os.HwParcel ) v1 ).readBool ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1064 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 1062 */
/* .line 1064 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 1065 */
/* throw v2 */
} // .end method
