class com.android.server.location.hardware.mtk.engineermode.aidl.IEmCallbacks$Stub$Proxy implements com.android.server.location.hardware.mtk.engineermode.aidl.IEmCallbacks {
	 /* .source "IEmCallbacks.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks$Stub; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "Proxy" */
} // .end annotation
/* # instance fields */
private java.lang.String mCachedHash;
private Integer mCachedVersion;
private android.os.IBinder mRemote;
/* # direct methods */
 com.android.server.location.hardware.mtk.engineermode.aidl.IEmCallbacks$Stub$Proxy ( ) {
/* .locals 1 */
/* .param p1, "remote" # Landroid/os/IBinder; */
/* .line 110 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 107 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks$Stub$Proxy;->mCachedVersion:I */
/* .line 108 */
final String v0 = "-1"; // const-string v0, "-1"
this.mCachedHash = v0;
/* .line 111 */
this.mRemote = p1;
/* .line 112 */
return;
} // .end method
/* # virtual methods */
public android.os.IBinder asBinder ( ) {
/* .locals 1 */
/* .line 116 */
v0 = this.mRemote;
} // .end method
public Boolean callbackToClient ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "data" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 125 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmCallbacks$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 126 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 128 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmCallbacks" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 129 */
(( android.os.Parcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 130 */
v2 = this.mRemote;
int v3 = 1; // const/4 v3, 0x1
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 131 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
	 /* .line 134 */
	 (( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
	 /* .line 135 */
	 v3 = 	 (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* .line 136 */
	 /* .local v3, "_result":Z */
	 /* nop */
	 /* .line 138 */
	 (( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
	 /* .line 139 */
	 (( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
	 /* .line 136 */
	 /* .line 132 */
} // .end local v3 # "_result":Z
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method callbackToClient is unimplemented."; // const-string v4, "Method callbackToClient is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks$Stub$Proxy;
} // .end local p1 # "data":Ljava/lang/String;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 138 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks$Stub$Proxy; */
/* .restart local p1 # "data":Ljava/lang/String; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 139 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 140 */
/* throw v2 */
} // .end method
public java.lang.String getInterfaceDescriptor ( ) {
/* .locals 1 */
/* .line 120 */
/* const-string/jumbo v0, "vendor.mediatek.hardware.engineermode.IEmCallbacks" */
} // .end method
public synchronized java.lang.String getInterfaceHash ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* monitor-enter p0 */
/* .line 163 */
try { // :try_start_0
final String v0 = "-1"; // const-string v0, "-1"
v1 = this.mCachedHash;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 164 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmCallbacks$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 165 */
/* .local v0, "data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 166 */
/* .local v1, "reply":Landroid/os/Parcel; */
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmCallbacks" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 167 */
v2 = this.mRemote;
/* const v3, 0xfffffe */
int v4 = 0; // const/4 v4, 0x0
/* .line 168 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 169 */
(( android.os.Parcel ) v1 ).readString ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readString()Ljava/lang/String;
this.mCachedHash = v2;
/* .line 170 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 171 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 173 */
} // .end local v0 # "data":Landroid/os/Parcel;
} // .end local v1 # "reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks$Stub$Proxy;
} // :cond_0
v0 = this.mCachedHash;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* monitor-exit p0 */
/* .line 162 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* throw v0 */
} // .end method
public Integer getInterfaceVersion ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 145 */
/* iget v0, p0, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks$Stub$Proxy;->mCachedVersion:I */
int v1 = -1; // const/4 v1, -0x1
/* if-ne v0, v1, :cond_0 */
/* .line 146 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmCallbacks$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 147 */
/* .local v0, "data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 149 */
/* .local v1, "reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmCallbacks" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 150 */
v2 = this.mRemote;
/* const v3, 0xffffff */
int v4 = 0; // const/4 v4, 0x0
/* .line 151 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 152 */
v2 = (( android.os.Parcel ) v1 ).readInt ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
/* iput v2, p0, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks$Stub$Proxy;->mCachedVersion:I */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 154 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 155 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 156 */
/* .line 154 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 155 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 156 */
/* throw v2 */
/* .line 158 */
} // .end local v0 # "data":Landroid/os/Parcel;
} // .end local v1 # "reply":Landroid/os/Parcel;
} // :cond_0
} // :goto_0
/* iget v0, p0, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks$Stub$Proxy;->mCachedVersion:I */
} // .end method
