class com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy implements com.android.server.location.hardware.mtk.engineermode.aidl.IEmds {
	 /* .source "IEmds.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "Proxy" */
} // .end annotation
/* # instance fields */
private java.lang.String mCachedHash;
private Integer mCachedVersion;
private android.os.IBinder mRemote;
/* # direct methods */
 com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ( ) {
/* .locals 1 */
/* .param p1, "remote" # Landroid/os/IBinder; */
/* .line 829 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 826 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->mCachedVersion:I */
/* .line 827 */
final String v0 = "-1"; // const-string v0, "-1"
this.mCachedHash = v0;
/* .line 830 */
this.mRemote = p1;
/* .line 831 */
return;
} // .end method
/* # virtual methods */
public android.os.IBinder asBinder ( ) {
/* .locals 1 */
/* .line 835 */
v0 = this.mRemote;
} // .end method
public Integer btDoTest ( Integer p0, Integer p1, Integer p2, Integer p3, Integer p4, Integer p5, Integer p6 ) {
/* .locals 5 */
/* .param p1, "kind" # I */
/* .param p2, "pattern" # I */
/* .param p3, "channel" # I */
/* .param p4, "pocketType" # I */
/* .param p5, "pocketTypeLen" # I */
/* .param p6, "freq" # I */
/* .param p7, "power" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1123 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 1124 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1126 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1127 */
(( android.os.Parcel ) v0 ).writeInt ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 1128 */
(( android.os.Parcel ) v0 ).writeInt ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V
/* .line 1129 */
(( android.os.Parcel ) v0 ).writeInt ( p3 ); // invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V
/* .line 1130 */
(( android.os.Parcel ) v0 ).writeInt ( p4 ); // invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V
/* .line 1131 */
(( android.os.Parcel ) v0 ).writeInt ( p5 ); // invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeInt(I)V
/* .line 1132 */
(( android.os.Parcel ) v0 ).writeInt ( p6 ); // invoke-virtual {v0, p6}, Landroid/os/Parcel;->writeInt(I)V
/* .line 1133 */
(( android.os.Parcel ) v0 ).writeInt ( p7 ); // invoke-virtual {v0, p7}, Landroid/os/Parcel;->writeInt(I)V
/* .line 1134 */
v2 = this.mRemote;
/* const/16 v3, 0xf */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1135 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
	 /* .line 1138 */
	 (( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
	 /* .line 1139 */
	 v3 = 	 (( android.os.Parcel ) v1 ).readInt ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* .line 1140 */
	 /* .local v3, "_result":I */
	 /* nop */
	 /* .line 1142 */
	 (( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
	 /* .line 1143 */
	 (( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
	 /* .line 1140 */
	 /* .line 1136 */
} // .end local v3 # "_result":I
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method btDoTest is unimplemented."; // const-string v4, "Method btDoTest is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
} // .end local p1 # "kind":I
} // .end local p2 # "pattern":I
} // .end local p3 # "channel":I
} // .end local p4 # "pocketType":I
} // .end local p5 # "pocketTypeLen":I
} // .end local p6 # "freq":I
} // .end local p7 # "power":I
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1142 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* .restart local p1 # "kind":I */
/* .restart local p2 # "pattern":I */
/* .restart local p3 # "channel":I */
/* .restart local p4 # "pocketType":I */
/* .restart local p5 # "pocketTypeLen":I */
/* .restart local p6 # "freq":I */
/* .restart local p7 # "power":I */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1143 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1144 */
/* throw v2 */
} // .end method
public btEndNoSigRxTest ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1149 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 1150 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1152 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1153 */
v2 = this.mRemote;
/* const/16 v3, 0x10 */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1154 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1157 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1158 */
(( android.os.Parcel ) v1 ).createIntArray ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->createIntArray()[I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1159 */
/* .local v3, "_result":[I */
/* nop */
/* .line 1161 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1162 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1159 */
/* .line 1155 */
} // .end local v3 # "_result":[I
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method btEndNoSigRxTest is unimplemented."; // const-string v4, "Method btEndNoSigRxTest is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1161 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1162 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1163 */
/* throw v2 */
} // .end method
public btHciCommandRun ( Object[] p0 ) {
/* .locals 5 */
/* .param p1, "input" # [B */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1168 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 1169 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1171 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1172 */
(( android.os.Parcel ) v0 ).writeByteArray ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeByteArray([B)V
/* .line 1173 */
v2 = this.mRemote;
/* const/16 v3, 0x11 */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1174 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1177 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1178 */
(( android.os.Parcel ) v1 ).createByteArray ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->createByteArray()[B
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1179 */
/* .local v3, "_result":[B */
/* nop */
/* .line 1181 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1182 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1179 */
/* .line 1175 */
} // .end local v3 # "_result":[B
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method btHciCommandRun is unimplemented."; // const-string v4, "Method btHciCommandRun is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
} // .end local p1 # "input":[B
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1181 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* .restart local p1 # "input":[B */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1182 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1183 */
/* throw v2 */
} // .end method
public Integer btInit ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1188 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 1189 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1191 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1192 */
v2 = this.mRemote;
/* const/16 v3, 0x12 */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1193 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1196 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1197 */
v3 = (( android.os.Parcel ) v1 ).readInt ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1198 */
/* .local v3, "_result":I */
/* nop */
/* .line 1200 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1201 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1198 */
/* .line 1194 */
} // .end local v3 # "_result":I
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method btInit is unimplemented."; // const-string v4, "Method btInit is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1200 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1201 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1202 */
/* throw v2 */
} // .end method
public Boolean btIsBLEEnhancedSupport ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1207 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 1208 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1210 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1211 */
v2 = this.mRemote;
/* const/16 v3, 0x13 */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1212 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1215 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1216 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1217 */
/* .local v3, "_result":Z */
/* nop */
/* .line 1219 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1220 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1217 */
/* .line 1213 */
} // .end local v3 # "_result":Z
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method btIsBLEEnhancedSupport is unimplemented."; // const-string v4, "Method btIsBLEEnhancedSupport is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1219 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1220 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1221 */
/* throw v2 */
} // .end method
public Integer btIsBLESupport ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1226 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 1227 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1229 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1230 */
v2 = this.mRemote;
/* const/16 v3, 0x14 */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1231 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1234 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1235 */
v3 = (( android.os.Parcel ) v1 ).readInt ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1236 */
/* .local v3, "_result":I */
/* nop */
/* .line 1238 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1239 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1236 */
/* .line 1232 */
} // .end local v3 # "_result":I
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method btIsBLESupport is unimplemented."; // const-string v4, "Method btIsBLESupport is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1238 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1239 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1240 */
/* throw v2 */
} // .end method
public Integer btIsComboSupport ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1245 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 1246 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1248 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1249 */
v2 = this.mRemote;
/* const/16 v3, 0x15 */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1250 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1253 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1254 */
v3 = (( android.os.Parcel ) v1 ).readInt ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1255 */
/* .local v3, "_result":I */
/* nop */
/* .line 1257 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1258 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1255 */
/* .line 1251 */
} // .end local v3 # "_result":I
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method btIsComboSupport is unimplemented."; // const-string v4, "Method btIsComboSupport is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1257 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1258 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1259 */
/* throw v2 */
} // .end method
public Integer btIsEmSupport ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1264 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 1265 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1267 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1268 */
v2 = this.mRemote;
/* const/16 v3, 0x16 */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1269 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1272 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1273 */
v3 = (( android.os.Parcel ) v1 ).readInt ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1274 */
/* .local v3, "_result":I */
/* nop */
/* .line 1276 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1277 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1274 */
/* .line 1270 */
} // .end local v3 # "_result":I
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method btIsEmSupport is unimplemented."; // const-string v4, "Method btIsEmSupport is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1276 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1277 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1278 */
/* throw v2 */
} // .end method
public Integer btPollingStart ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1283 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 1284 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1286 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1287 */
v2 = this.mRemote;
/* const/16 v3, 0x17 */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1288 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1291 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1292 */
v3 = (( android.os.Parcel ) v1 ).readInt ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1293 */
/* .local v3, "_result":I */
/* nop */
/* .line 1295 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1296 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1293 */
/* .line 1289 */
} // .end local v3 # "_result":I
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method btPollingStart is unimplemented."; // const-string v4, "Method btPollingStart is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1295 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1296 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1297 */
/* throw v2 */
} // .end method
public Integer btPollingStop ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1302 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 1303 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1305 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1306 */
v2 = this.mRemote;
/* const/16 v3, 0x18 */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1307 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1310 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1311 */
v3 = (( android.os.Parcel ) v1 ).readInt ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1312 */
/* .local v3, "_result":I */
/* nop */
/* .line 1314 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1315 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1312 */
/* .line 1308 */
} // .end local v3 # "_result":I
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method btPollingStop is unimplemented."; // const-string v4, "Method btPollingStop is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1314 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1315 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1316 */
/* throw v2 */
} // .end method
public Boolean btStartNoSigRxTest ( Integer p0, Integer p1, Integer p2, Integer p3 ) {
/* .locals 5 */
/* .param p1, "pattern" # I */
/* .param p2, "pockettype" # I */
/* .param p3, "freq" # I */
/* .param p4, "address" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1321 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 1322 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1324 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1325 */
(( android.os.Parcel ) v0 ).writeInt ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 1326 */
(( android.os.Parcel ) v0 ).writeInt ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V
/* .line 1327 */
(( android.os.Parcel ) v0 ).writeInt ( p3 ); // invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V
/* .line 1328 */
(( android.os.Parcel ) v0 ).writeInt ( p4 ); // invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V
/* .line 1329 */
v2 = this.mRemote;
/* const/16 v3, 0x19 */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1330 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1333 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1334 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1335 */
/* .local v3, "_result":Z */
/* nop */
/* .line 1337 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1338 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1335 */
/* .line 1331 */
} // .end local v3 # "_result":Z
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method btStartNoSigRxTest is unimplemented."; // const-string v4, "Method btStartNoSigRxTest is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
} // .end local p1 # "pattern":I
} // .end local p2 # "pockettype":I
} // .end local p3 # "freq":I
} // .end local p4 # "address":I
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1337 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* .restart local p1 # "pattern":I */
/* .restart local p2 # "pockettype":I */
/* .restart local p3 # "freq":I */
/* .restart local p4 # "address":I */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1338 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1339 */
/* throw v2 */
} // .end method
public Integer btStartRelayer ( Integer p0, Integer p1 ) {
/* .locals 5 */
/* .param p1, "port" # I */
/* .param p2, "speed" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1344 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 1345 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1347 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1348 */
(( android.os.Parcel ) v0 ).writeInt ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 1349 */
(( android.os.Parcel ) v0 ).writeInt ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V
/* .line 1350 */
v2 = this.mRemote;
/* const/16 v3, 0x1a */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1351 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1354 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1355 */
v3 = (( android.os.Parcel ) v1 ).readInt ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1356 */
/* .local v3, "_result":I */
/* nop */
/* .line 1358 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1359 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1356 */
/* .line 1352 */
} // .end local v3 # "_result":I
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method btStartRelayer is unimplemented."; // const-string v4, "Method btStartRelayer is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
} // .end local p1 # "port":I
} // .end local p2 # "speed":I
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1358 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* .restart local p1 # "port":I */
/* .restart local p2 # "speed":I */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1359 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1360 */
/* throw v2 */
} // .end method
public Integer btStopRelayer ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1365 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 1366 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1368 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1369 */
v2 = this.mRemote;
/* const/16 v3, 0x1b */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1370 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1373 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1374 */
v3 = (( android.os.Parcel ) v1 ).readInt ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1375 */
/* .local v3, "_result":I */
/* nop */
/* .line 1377 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1378 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1375 */
/* .line 1371 */
} // .end local v3 # "_result":I
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method btStopRelayer is unimplemented."; // const-string v4, "Method btStopRelayer is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1377 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1378 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1379 */
/* throw v2 */
} // .end method
public Integer btUninit ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1384 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 1385 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1387 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1388 */
v2 = this.mRemote;
/* const/16 v3, 0x1c */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1389 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1392 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1393 */
v3 = (( android.os.Parcel ) v1 ).readInt ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1394 */
/* .local v3, "_result":I */
/* nop */
/* .line 1396 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1397 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1394 */
/* .line 1390 */
} // .end local v3 # "_result":I
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method btUninit is unimplemented."; // const-string v4, "Method btUninit is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1396 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1397 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1398 */
/* throw v2 */
} // .end method
public Boolean connect ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1722 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 1723 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1725 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1726 */
v2 = this.mRemote;
/* const/16 v3, 0x2d */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1727 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1730 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1731 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1732 */
/* .local v3, "_result":Z */
/* nop */
/* .line 1734 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1735 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1732 */
/* .line 1728 */
} // .end local v3 # "_result":Z
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method connect is unimplemented."; // const-string v4, "Method connect is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1734 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1735 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1736 */
/* throw v2 */
} // .end method
public void disconnect ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1741 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 1742 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1744 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1745 */
v2 = this.mRemote;
/* const/16 v3, 0x2e */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1746 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1749 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1751 */
} // .end local v2 # "_status":Z
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1752 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1753 */
/* nop */
/* .line 1754 */
return;
/* .line 1747 */
/* .restart local v2 # "_status":Z */
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method disconnect is unimplemented."; // const-string v4, "Method disconnect is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1751 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1752 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1753 */
/* throw v2 */
} // .end method
public Boolean getFilePathListWithCallBack ( java.lang.String p0, com.android.server.location.hardware.mtk.engineermode.aidl.IEmCallbacks p1 ) {
/* .locals 5 */
/* .param p1, "data" # Ljava/lang/String; */
/* .param p2, "callback" # Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1623 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 1624 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1626 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1627 */
(( android.os.Parcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 1628 */
(( android.os.Parcel ) v0 ).writeStrongInterface ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeStrongInterface(Landroid/os/IInterface;)V
/* .line 1629 */
v2 = this.mRemote;
/* const/16 v3, 0x28 */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1630 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1633 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1634 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1635 */
/* .local v3, "_result":Z */
/* nop */
/* .line 1637 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1638 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1635 */
/* .line 1631 */
} // .end local v3 # "_result":Z
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method getFilePathListWithCallBack is unimplemented."; // const-string v4, "Method getFilePathListWithCallBack is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
} // .end local p1 # "data":Ljava/lang/String;
} // .end local p2 # "callback":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1637 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* .restart local p1 # "data":Ljava/lang/String; */
/* .restart local p2 # "callback":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1638 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1639 */
/* throw v2 */
} // .end method
public java.lang.String getInterfaceDescriptor ( ) {
/* .locals 1 */
/* .line 839 */
/* const-string/jumbo v0, "vendor.mediatek.hardware.engineermode.IEmds" */
} // .end method
public synchronized java.lang.String getInterfaceHash ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* monitor-enter p0 */
/* .line 1855 */
try { // :try_start_0
final String v0 = "-1"; // const-string v0, "-1"
v1 = this.mCachedHash;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1856 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 1857 */
/* .local v0, "data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1858 */
/* .local v1, "reply":Landroid/os/Parcel; */
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1859 */
v2 = this.mRemote;
/* const v3, 0xfffffe */
int v4 = 0; // const/4 v4, 0x0
/* .line 1860 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1861 */
(( android.os.Parcel ) v1 ).readString ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readString()Ljava/lang/String;
this.mCachedHash = v2;
/* .line 1862 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1863 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1865 */
} // .end local v0 # "data":Landroid/os/Parcel;
} // .end local v1 # "reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
} // :cond_0
v0 = this.mCachedHash;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* monitor-exit p0 */
/* .line 1854 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* throw v0 */
} // .end method
public Integer getInterfaceVersion ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1837 */
/* iget v0, p0, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->mCachedVersion:I */
int v1 = -1; // const/4 v1, -0x1
/* if-ne v0, v1, :cond_0 */
/* .line 1838 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 1839 */
/* .local v0, "data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1841 */
/* .local v1, "reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1842 */
v2 = this.mRemote;
/* const v3, 0xffffff */
int v4 = 0; // const/4 v4, 0x0
/* .line 1843 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1844 */
v2 = (( android.os.Parcel ) v1 ).readInt ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
/* iput v2, p0, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->mCachedVersion:I */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1846 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1847 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1848 */
/* .line 1846 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1847 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1848 */
/* throw v2 */
/* .line 1850 */
} // .end local v0 # "data":Landroid/os/Parcel;
} // .end local v1 # "reply":Landroid/os/Parcel;
} // :cond_0
} // :goto_0
/* iget v0, p0, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->mCachedVersion:I */
} // .end method
public Integer isGauge30Support ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1104 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 1105 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1107 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1108 */
v2 = this.mRemote;
/* const/16 v3, 0xe */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1109 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1112 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1113 */
v3 = (( android.os.Parcel ) v1 ).readInt ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1114 */
/* .local v3, "_result":I */
/* nop */
/* .line 1116 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1117 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1114 */
/* .line 1110 */
} // .end local v3 # "_result":I
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method isGauge30Support is unimplemented."; // const-string v4, "Method isGauge30Support is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1116 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1117 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1118 */
/* throw v2 */
} // .end method
public Integer isNfcSupport ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1685 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 1686 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1688 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1689 */
v2 = this.mRemote;
/* const/16 v3, 0x2b */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1690 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1693 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1694 */
v3 = (( android.os.Parcel ) v1 ).readInt ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1695 */
/* .local v3, "_result":I */
/* nop */
/* .line 1697 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1698 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1695 */
/* .line 1691 */
} // .end local v3 # "_result":I
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method isNfcSupport is unimplemented."; // const-string v4, "Method isNfcSupport is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1697 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1698 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1699 */
/* throw v2 */
} // .end method
public Boolean readFileConfigWithCallBack ( java.lang.String p0, com.android.server.location.hardware.mtk.engineermode.aidl.IEmCallbacks p1 ) {
/* .locals 5 */
/* .param p1, "data" # Ljava/lang/String; */
/* .param p2, "callback" # Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1774 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 1775 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1777 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1778 */
(( android.os.Parcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 1779 */
(( android.os.Parcel ) v0 ).writeStrongInterface ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeStrongInterface(Landroid/os/IInterface;)V
/* .line 1780 */
v2 = this.mRemote;
/* const/16 v3, 0x30 */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1781 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1784 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1785 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1786 */
/* .local v3, "_result":Z */
/* nop */
/* .line 1788 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1789 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1786 */
/* .line 1782 */
} // .end local v3 # "_result":Z
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method readFileConfigWithCallBack is unimplemented."; // const-string v4, "Method readFileConfigWithCallBack is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
} // .end local p1 # "data":Ljava/lang/String;
} // .end local p2 # "callback":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1788 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* .restart local p1 # "data":Ljava/lang/String; */
/* .restart local p2 # "callback":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1789 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1790 */
/* throw v2 */
} // .end method
public readMnlConfigFile ( Integer[] p0 ) {
/* .locals 5 */
/* .param p1, "tag" # [I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1644 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 1645 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1647 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1648 */
(( android.os.Parcel ) v0 ).writeIntArray ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeIntArray([I)V
/* .line 1649 */
v2 = this.mRemote;
/* const/16 v3, 0x29 */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1650 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1653 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1654 */
(( android.os.Parcel ) v1 ).createByteArray ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->createByteArray()[B
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1655 */
/* .local v3, "_result":[B */
/* nop */
/* .line 1657 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1658 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1655 */
/* .line 1651 */
} // .end local v3 # "_result":[B
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method readMnlConfigFile is unimplemented."; // const-string v4, "Method readMnlConfigFile is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
} // .end local p1 # "tag":[I
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1657 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* .restart local p1 # "tag":[I */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1658 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1659 */
/* throw v2 */
} // .end method
public void sendNfcRequest ( Integer p0, Object[] p1 ) {
/* .locals 5 */
/* .param p1, "ins" # I */
/* .param p2, "detail" # [B */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1758 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 1760 */
/* .local v0, "_data":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1761 */
(( android.os.Parcel ) v0 ).writeInt ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 1762 */
(( android.os.Parcel ) v0 ).writeByteArray ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeByteArray([B)V
/* .line 1763 */
v1 = this.mRemote;
int v2 = 0; // const/4 v2, 0x0
int v3 = 1; // const/4 v3, 0x1
v1 = /* const/16 v4, 0x2f */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1764 */
/* .local v1, "_status":Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1768 */
} // .end local v1 # "_status":Z
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1769 */
/* nop */
/* .line 1770 */
return;
/* .line 1765 */
/* .restart local v1 # "_status":Z */
} // :cond_0
try { // :try_start_1
/* new-instance v2, Landroid/os/RemoteException; */
final String v3 = "Method sendNfcRequest is unimplemented."; // const-string v3, "Method sendNfcRequest is unimplemented."
/* invoke-direct {v2, v3}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
} // .end local p1 # "ins":I
} // .end local p2 # "detail":[B
/* throw v2 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1768 */
} // .end local v1 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* .restart local p1 # "ins":I */
/* .restart local p2 # "detail":[B */
/* :catchall_0 */
/* move-exception v1 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1769 */
/* throw v1 */
} // .end method
public Boolean sendToServer ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "data" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 862 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 863 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 865 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 866 */
(( android.os.Parcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 867 */
v2 = this.mRemote;
int v3 = 2; // const/4 v3, 0x2
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 868 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 871 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 872 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 873 */
/* .local v3, "_result":Z */
/* nop */
/* .line 875 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 876 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 873 */
/* .line 869 */
} // .end local v3 # "_result":Z
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method sendToServer is unimplemented."; // const-string v4, "Method sendToServer is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
} // .end local p1 # "data":Ljava/lang/String;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 875 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* .restart local p1 # "data":Ljava/lang/String; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 876 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 877 */
/* throw v2 */
} // .end method
public Boolean sendToServerWithCallBack ( java.lang.String p0, com.android.server.location.hardware.mtk.engineermode.aidl.IEmCallbacks p1 ) {
/* .locals 5 */
/* .param p1, "data" # Ljava/lang/String; */
/* .param p2, "callback" # Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 882 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 883 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 885 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 886 */
(( android.os.Parcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 887 */
(( android.os.Parcel ) v0 ).writeStrongInterface ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeStrongInterface(Landroid/os/IInterface;)V
/* .line 888 */
v2 = this.mRemote;
int v3 = 3; // const/4 v3, 0x3
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 889 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 892 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 893 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 894 */
/* .local v3, "_result":Z */
/* nop */
/* .line 896 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 897 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 894 */
/* .line 890 */
} // .end local v3 # "_result":Z
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method sendToServerWithCallBack is unimplemented."; // const-string v4, "Method sendToServerWithCallBack is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
} // .end local p1 # "data":Ljava/lang/String;
} // .end local p2 # "callback":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 896 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* .restart local p1 # "data":Ljava/lang/String; */
/* .restart local p2 # "callback":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 897 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 898 */
/* throw v2 */
} // .end method
public Boolean setBypassDis ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "data" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1543 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 1544 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1546 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1547 */
(( android.os.Parcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 1548 */
v2 = this.mRemote;
/* const/16 v3, 0x24 */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1549 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1552 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1553 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1554 */
/* .local v3, "_result":Z */
/* nop */
/* .line 1556 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1557 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1554 */
/* .line 1550 */
} // .end local v3 # "_result":Z
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method setBypassDis is unimplemented."; // const-string v4, "Method setBypassDis is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
} // .end local p1 # "data":Ljava/lang/String;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1556 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* .restart local p1 # "data":Ljava/lang/String; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1557 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1558 */
/* throw v2 */
} // .end method
public Boolean setBypassEn ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "data" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1563 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 1564 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1566 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1567 */
(( android.os.Parcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 1568 */
v2 = this.mRemote;
/* const/16 v3, 0x25 */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1569 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1572 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1573 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1574 */
/* .local v3, "_result":Z */
/* nop */
/* .line 1576 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1577 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1574 */
/* .line 1570 */
} // .end local v3 # "_result":Z
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method setBypassEn is unimplemented."; // const-string v4, "Method setBypassEn is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
} // .end local p1 # "data":Ljava/lang/String;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1576 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* .restart local p1 # "data":Ljava/lang/String; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1577 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1578 */
/* throw v2 */
} // .end method
public Boolean setBypassService ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "data" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1583 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 1584 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1586 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1587 */
(( android.os.Parcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 1588 */
v2 = this.mRemote;
/* const/16 v3, 0x26 */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1589 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1592 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1593 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1594 */
/* .local v3, "_result":Z */
/* nop */
/* .line 1596 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1597 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1594 */
/* .line 1590 */
} // .end local v3 # "_result":Z
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method setBypassService is unimplemented."; // const-string v4, "Method setBypassService is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
} // .end local p1 # "data":Ljava/lang/String;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1596 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* .restart local p1 # "data":Ljava/lang/String; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1597 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1598 */
/* throw v2 */
} // .end method
public void setCallback ( com.android.server.location.hardware.mtk.engineermode.aidl.IEmCallbacks p0 ) {
/* .locals 5 */
/* .param p1, "callback" # Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 844 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 845 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 847 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 848 */
(( android.os.Parcel ) v0 ).writeStrongInterface ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongInterface(Landroid/os/IInterface;)V
/* .line 849 */
v2 = this.mRemote;
int v3 = 1; // const/4 v3, 0x1
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 850 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 853 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 855 */
} // .end local v2 # "_status":Z
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 856 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 857 */
/* nop */
/* .line 858 */
return;
/* .line 851 */
/* .restart local v2 # "_status":Z */
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method setCallback is unimplemented."; // const-string v4, "Method setCallback is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
} // .end local p1 # "callback":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 855 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* .restart local p1 # "callback":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 856 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 857 */
/* throw v2 */
} // .end method
public Boolean setCtIREngMode ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "data" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 903 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 904 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 906 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 907 */
(( android.os.Parcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 908 */
v2 = this.mRemote;
int v3 = 4; // const/4 v3, 0x4
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 909 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 912 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 913 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 914 */
/* .local v3, "_result":Z */
/* nop */
/* .line 916 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 917 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 914 */
/* .line 910 */
} // .end local v3 # "_result":Z
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method setCtIREngMode is unimplemented."; // const-string v4, "Method setCtIREngMode is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
} // .end local p1 # "data":Ljava/lang/String;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 916 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* .restart local p1 # "data":Ljava/lang/String; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 917 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 918 */
/* throw v2 */
} // .end method
public Boolean setDisableC2kCap ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "data" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 923 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 924 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 926 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 927 */
(( android.os.Parcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 928 */
v2 = this.mRemote;
int v3 = 5; // const/4 v3, 0x5
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 929 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 932 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 933 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 934 */
/* .local v3, "_result":Z */
/* nop */
/* .line 936 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 937 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 934 */
/* .line 930 */
} // .end local v3 # "_result":Z
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method setDisableC2kCap is unimplemented."; // const-string v4, "Method setDisableC2kCap is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
} // .end local p1 # "data":Ljava/lang/String;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 936 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* .restart local p1 # "data":Ljava/lang/String; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 937 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 938 */
/* throw v2 */
} // .end method
public Boolean setDsbpSupport ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "data" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 943 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 944 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 946 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 947 */
(( android.os.Parcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 948 */
v2 = this.mRemote;
int v3 = 6; // const/4 v3, 0x6
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 949 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 952 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 953 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 954 */
/* .local v3, "_result":Z */
/* nop */
/* .line 956 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 957 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 954 */
/* .line 950 */
} // .end local v3 # "_result":Z
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method setDsbpSupport is unimplemented."; // const-string v4, "Method setDsbpSupport is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
} // .end local p1 # "data":Ljava/lang/String;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 956 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* .restart local p1 # "data":Ljava/lang/String; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 957 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 958 */
/* throw v2 */
} // .end method
public Boolean setEmConfigure ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 5 */
/* .param p1, "name" # Ljava/lang/String; */
/* .param p2, "value" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1083 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 1084 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1086 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1087 */
(( android.os.Parcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 1088 */
(( android.os.Parcel ) v0 ).writeString ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 1089 */
v2 = this.mRemote;
/* const/16 v3, 0xd */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1090 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1093 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1094 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1095 */
/* .local v3, "_result":Z */
/* nop */
/* .line 1097 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1098 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1095 */
/* .line 1091 */
} // .end local v3 # "_result":Z
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method setEmConfigure is unimplemented."; // const-string v4, "Method setEmConfigure is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
} // .end local p1 # "name":Ljava/lang/String;
} // .end local p2 # "value":Ljava/lang/String;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1097 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* .restart local p1 # "name":Ljava/lang/String; */
/* .restart local p2 # "value":Ljava/lang/String; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1098 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1099 */
/* throw v2 */
} // .end method
public Boolean setEmUsbType ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "data" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1503 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 1504 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1506 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1507 */
(( android.os.Parcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 1508 */
v2 = this.mRemote;
/* const/16 v3, 0x22 */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1509 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1512 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1513 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1514 */
/* .local v3, "_result":Z */
/* nop */
/* .line 1516 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1517 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1514 */
/* .line 1510 */
} // .end local v3 # "_result":Z
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method setEmUsbType is unimplemented."; // const-string v4, "Method setEmUsbType is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
} // .end local p1 # "data":Ljava/lang/String;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1516 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* .restart local p1 # "data":Ljava/lang/String; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1517 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1518 */
/* throw v2 */
} // .end method
public Boolean setEmUsbValue ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "data" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1523 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 1524 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1526 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1527 */
(( android.os.Parcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 1528 */
v2 = this.mRemote;
/* const/16 v3, 0x23 */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1529 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1532 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1533 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1534 */
/* .local v3, "_result":Z */
/* nop */
/* .line 1536 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1537 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1534 */
/* .line 1530 */
} // .end local v3 # "_result":Z
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method setEmUsbValue is unimplemented."; // const-string v4, "Method setEmUsbValue is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
} // .end local p1 # "data":Ljava/lang/String;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1536 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* .restart local p1 # "data":Ljava/lang/String; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1537 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1538 */
/* throw v2 */
} // .end method
public Boolean setImsTestMode ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "data" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1043 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 1044 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1046 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1047 */
(( android.os.Parcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 1048 */
v2 = this.mRemote;
/* const/16 v3, 0xb */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1049 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1052 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1053 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1054 */
/* .local v3, "_result":Z */
/* nop */
/* .line 1056 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1057 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1054 */
/* .line 1050 */
} // .end local v3 # "_result":Z
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method setImsTestMode is unimplemented."; // const-string v4, "Method setImsTestMode is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
} // .end local p1 # "data":Ljava/lang/String;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1056 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* .restart local p1 # "data":Ljava/lang/String; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1057 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1058 */
/* throw v2 */
} // .end method
public Boolean setMdResetDelay ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "data" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1403 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 1404 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1406 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1407 */
(( android.os.Parcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 1408 */
v2 = this.mRemote;
/* const/16 v3, 0x1d */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1409 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1412 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1413 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1414 */
/* .local v3, "_result":Z */
/* nop */
/* .line 1416 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1417 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1414 */
/* .line 1410 */
} // .end local v3 # "_result":Z
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method setMdResetDelay is unimplemented."; // const-string v4, "Method setMdResetDelay is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
} // .end local p1 # "data":Ljava/lang/String;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1416 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* .restart local p1 # "data":Ljava/lang/String; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1417 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1418 */
/* throw v2 */
} // .end method
public Boolean setModemWarningEnable ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "data" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1423 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 1424 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1426 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1427 */
(( android.os.Parcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 1428 */
v2 = this.mRemote;
/* const/16 v3, 0x1e */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1429 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1432 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1433 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1434 */
/* .local v3, "_result":Z */
/* nop */
/* .line 1436 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1437 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1434 */
/* .line 1430 */
} // .end local v3 # "_result":Z
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method setModemWarningEnable is unimplemented."; // const-string v4, "Method setModemWarningEnable is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
} // .end local p1 # "data":Ljava/lang/String;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1436 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* .restart local p1 # "data":Ljava/lang/String; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1437 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1438 */
/* throw v2 */
} // .end method
public Boolean setMoms ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "data" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1603 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 1604 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1606 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1607 */
(( android.os.Parcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 1608 */
v2 = this.mRemote;
/* const/16 v3, 0x27 */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1609 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1612 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1613 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1614 */
/* .local v3, "_result":Z */
/* nop */
/* .line 1616 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1617 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1614 */
/* .line 1610 */
} // .end local v3 # "_result":Z
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method setMoms is unimplemented."; // const-string v4, "Method setMoms is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
} // .end local p1 # "data":Ljava/lang/String;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1616 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* .restart local p1 # "data":Ljava/lang/String; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1617 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1618 */
/* throw v2 */
} // .end method
public void setNfcResponseFunction ( com.android.server.location.hardware.mtk.engineermode.aidl.IEmResponses p0 ) {
/* .locals 5 */
/* .param p1, "response" # Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmResponses; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1704 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 1705 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1707 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1708 */
(( android.os.Parcel ) v0 ).writeStrongInterface ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongInterface(Landroid/os/IInterface;)V
/* .line 1709 */
v2 = this.mRemote;
/* const/16 v3, 0x2c */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1710 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1713 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1715 */
} // .end local v2 # "_status":Z
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1716 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1717 */
/* nop */
/* .line 1718 */
return;
/* .line 1711 */
/* .restart local v2 # "_status":Z */
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method setNfcResponseFunction is unimplemented."; // const-string v4, "Method setNfcResponseFunction is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
} // .end local p1 # "response":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmResponses;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1715 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* .restart local p1 # "response":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmResponses; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1716 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1717 */
/* throw v2 */
} // .end method
public Boolean setPreferGprsMode ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "data" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 963 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 964 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 966 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 967 */
(( android.os.Parcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 968 */
v2 = this.mRemote;
int v3 = 7; // const/4 v3, 0x7
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 969 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 972 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 973 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 974 */
/* .local v3, "_result":Z */
/* nop */
/* .line 976 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 977 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 974 */
/* .line 970 */
} // .end local v3 # "_result":Z
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method setPreferGprsMode is unimplemented."; // const-string v4, "Method setPreferGprsMode is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
} // .end local p1 # "data":Ljava/lang/String;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 976 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* .restart local p1 # "data":Ljava/lang/String; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 977 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 978 */
/* throw v2 */
} // .end method
public Boolean setRadioCapabilitySwitchEnable ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "data" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 983 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 984 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 986 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 987 */
(( android.os.Parcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 988 */
v2 = this.mRemote;
/* const/16 v3, 0x8 */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 989 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 992 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 993 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 994 */
/* .local v3, "_result":Z */
/* nop */
/* .line 996 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 997 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 994 */
/* .line 990 */
} // .end local v3 # "_result":Z
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method setRadioCapabilitySwitchEnable is unimplemented."; // const-string v4, "Method setRadioCapabilitySwitchEnable is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
} // .end local p1 # "data":Ljava/lang/String;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 996 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* .restart local p1 # "data":Ljava/lang/String; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 997 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 998 */
/* throw v2 */
} // .end method
public Boolean setSmsFormat ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "data" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1003 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 1004 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1006 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1007 */
(( android.os.Parcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 1008 */
v2 = this.mRemote;
/* const/16 v3, 0x9 */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1009 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1012 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1013 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1014 */
/* .local v3, "_result":Z */
/* nop */
/* .line 1016 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1017 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1014 */
/* .line 1010 */
} // .end local v3 # "_result":Z
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method setSmsFormat is unimplemented."; // const-string v4, "Method setSmsFormat is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
} // .end local p1 # "data":Ljava/lang/String;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1016 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* .restart local p1 # "data":Ljava/lang/String; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1017 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1018 */
/* throw v2 */
} // .end method
public Boolean setTestSimCardType ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "data" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1023 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 1024 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1026 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1027 */
(( android.os.Parcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 1028 */
v2 = this.mRemote;
/* const/16 v3, 0xa */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1029 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1032 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1033 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1034 */
/* .local v3, "_result":Z */
/* nop */
/* .line 1036 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1037 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1034 */
/* .line 1030 */
} // .end local v3 # "_result":Z
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method setTestSimCardType is unimplemented."; // const-string v4, "Method setTestSimCardType is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
} // .end local p1 # "data":Ljava/lang/String;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1036 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* .restart local p1 # "data":Ljava/lang/String; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1037 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1038 */
/* throw v2 */
} // .end method
public Boolean setUsbOtgSwitch ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "data" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1483 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 1484 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1486 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1487 */
(( android.os.Parcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 1488 */
v2 = this.mRemote;
/* const/16 v3, 0x21 */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1489 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1492 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1493 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1494 */
/* .local v3, "_result":Z */
/* nop */
/* .line 1496 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1497 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1494 */
/* .line 1490 */
} // .end local v3 # "_result":Z
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method setUsbOtgSwitch is unimplemented."; // const-string v4, "Method setUsbOtgSwitch is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
} // .end local p1 # "data":Ljava/lang/String;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1496 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* .restart local p1 # "data":Ljava/lang/String; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1497 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1498 */
/* throw v2 */
} // .end method
public Boolean setUsbPort ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "data" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1463 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 1464 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1466 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1467 */
(( android.os.Parcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 1468 */
v2 = this.mRemote;
/* const/16 v3, 0x20 */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1469 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1472 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1473 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1474 */
/* .local v3, "_result":Z */
/* nop */
/* .line 1476 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1477 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1474 */
/* .line 1470 */
} // .end local v3 # "_result":Z
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method setUsbPort is unimplemented."; // const-string v4, "Method setUsbPort is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
} // .end local p1 # "data":Ljava/lang/String;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1476 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* .restart local p1 # "data":Ljava/lang/String; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1477 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1478 */
/* throw v2 */
} // .end method
public Boolean setVolteMalPctid ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "data" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1063 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 1064 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1066 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1067 */
(( android.os.Parcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 1068 */
v2 = this.mRemote;
/* const/16 v3, 0xc */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1069 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1072 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1073 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1074 */
/* .local v3, "_result":Z */
/* nop */
/* .line 1076 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1077 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1074 */
/* .line 1070 */
} // .end local v3 # "_result":Z
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method setVolteMalPctid is unimplemented."; // const-string v4, "Method setVolteMalPctid is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
} // .end local p1 # "data":Ljava/lang/String;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1076 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* .restart local p1 # "data":Ljava/lang/String; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1077 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1078 */
/* throw v2 */
} // .end method
public Boolean setWcnCoreDump ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "data" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1443 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 1444 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1446 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1447 */
(( android.os.Parcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 1448 */
v2 = this.mRemote;
/* const/16 v3, 0x1f */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1449 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1452 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1453 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1454 */
/* .local v3, "_result":Z */
/* nop */
/* .line 1456 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1457 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1454 */
/* .line 1450 */
} // .end local v3 # "_result":Z
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method setWcnCoreDump is unimplemented."; // const-string v4, "Method setWcnCoreDump is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
} // .end local p1 # "data":Ljava/lang/String;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1456 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* .restart local p1 # "data":Ljava/lang/String; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1457 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1458 */
/* throw v2 */
} // .end method
public Boolean writeFileConfig ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 5 */
/* .param p1, "data" # Ljava/lang/String; */
/* .param p2, "value" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1795 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 1796 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1798 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1799 */
(( android.os.Parcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 1800 */
(( android.os.Parcel ) v0 ).writeString ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 1801 */
v2 = this.mRemote;
/* const/16 v3, 0x31 */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1802 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1805 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1806 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1807 */
/* .local v3, "_result":Z */
/* nop */
/* .line 1809 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1810 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1807 */
/* .line 1803 */
} // .end local v3 # "_result":Z
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method writeFileConfig is unimplemented."; // const-string v4, "Method writeFileConfig is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
} // .end local p1 # "data":Ljava/lang/String;
} // .end local p2 # "value":Ljava/lang/String;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1809 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* .restart local p1 # "data":Ljava/lang/String; */
/* .restart local p2 # "value":Ljava/lang/String; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1810 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1811 */
/* throw v2 */
} // .end method
public Boolean writeMnlConfigFile ( Object[] p0, Integer[] p1 ) {
/* .locals 5 */
/* .param p1, "content" # [B */
/* .param p2, "tag" # [I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1664 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 1665 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1667 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1668 */
(( android.os.Parcel ) v0 ).writeByteArray ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeByteArray([B)V
/* .line 1669 */
(( android.os.Parcel ) v0 ).writeIntArray ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeIntArray([I)V
/* .line 1670 */
v2 = this.mRemote;
/* const/16 v3, 0x2a */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1671 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1674 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1675 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1676 */
/* .local v3, "_result":Z */
/* nop */
/* .line 1678 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1679 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1676 */
/* .line 1672 */
} // .end local v3 # "_result":Z
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method writeMnlConfigFile is unimplemented."; // const-string v4, "Method writeMnlConfigFile is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
} // .end local p1 # "content":[B
} // .end local p2 # "tag":[I
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1678 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* .restart local p1 # "content":[B */
/* .restart local p2 # "tag":[I */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1679 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1680 */
/* throw v2 */
} // .end method
public Boolean writeOtaFiles ( java.lang.String p0, Object[] p1 ) {
/* .locals 5 */
/* .param p1, "destFile" # Ljava/lang/String; */
/* .param p2, "content" # [B */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1816 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 1817 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 1819 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmds" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1820 */
(( android.os.Parcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 1821 */
(( android.os.Parcel ) v0 ).writeByteArray ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeByteArray([B)V
/* .line 1822 */
v2 = this.mRemote;
/* const/16 v3, 0x32 */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 1823 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1826 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 1827 */
v3 = (( android.os.Parcel ) v1 ).readBoolean ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readBoolean()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1828 */
/* .local v3, "_result":Z */
/* nop */
/* .line 1830 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1831 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1828 */
/* .line 1824 */
} // .end local v3 # "_result":Z
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method writeOtaFiles is unimplemented."; // const-string v4, "Method writeOtaFiles is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
} // .end local p1 # "destFile":Ljava/lang/String;
} // .end local p2 # "content":[B
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1830 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* .restart local p1 # "destFile":Ljava/lang/String; */
/* .restart local p2 # "content":[B */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1831 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1832 */
/* throw v2 */
} // .end method
