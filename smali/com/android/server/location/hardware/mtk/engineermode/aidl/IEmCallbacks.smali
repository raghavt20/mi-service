.class public interface abstract Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks;
.super Ljava/lang/Object;
.source "IEmCallbacks.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks$Stub;,
        Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks$Default;
    }
.end annotation


# static fields
.field public static final DESCRIPTOR:Ljava/lang/String; = "vendor.mediatek.hardware.engineermode.IEmCallbacks"

.field public static final HASH:Ljava/lang/String; = "de2f01ae4c46a25b30928e5a2edf9a1df3132225"

.field public static final VERSION:I = 0x1


# virtual methods
.method public abstract callbackToClient(Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getInterfaceHash()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getInterfaceVersion()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
