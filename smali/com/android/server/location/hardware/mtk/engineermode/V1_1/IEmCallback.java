public abstract class com.android.server.location.hardware.mtk.engineermode.V1_1.IEmCallback implements com.android.server.location.hardware.mtk.engineermode.V1_0.IEmCallback {
	 /* .source "IEmCallback.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmCallback$Proxy;, */
	 /* Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmCallback$Stub; */
	 /* } */
} // .end annotation
/* # direct methods */
public static com.android.server.location.hardware.mtk.engineermode.V1_1.IEmCallback asInterface ( android.os.IHwBinder p0 ) {
	 /* .locals 6 */
	 /* .param p0, "iHwBinder" # Landroid/os/IHwBinder; */
	 /* .line 357 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* if-nez p0, :cond_0 */
	 /* .line 358 */
	 /* .line 360 */
} // :cond_0
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.1::IEmCallback" */
/* check-cast v1, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmCallback; */
/* .line 361 */
/* .local v1, "queryLocalInterface":Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmCallback; */
if ( v1 != null) { // if-eqz v1, :cond_1
	 /* instance-of v2, v1, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmCallback; */
	 if ( v2 != null) { // if-eqz v2, :cond_1
		 /* .line 362 */
		 /* .line 364 */
	 } // :cond_1
	 /* new-instance v2, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmCallback$Proxy; */
	 /* invoke-direct {v2, p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmCallback$Proxy;-><init>(Landroid/os/IHwBinder;)V */
	 /* .line 366 */
	 /* .local v2, "proxy":Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmCallback$Proxy; */
	 try { // :try_start_0
		 (( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmCallback$Proxy ) v2 ).interfaceChain ( ); // invoke-virtual {v2}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmCallback$Proxy;->interfaceChain()Ljava/util/ArrayList;
		 (( java.util.ArrayList ) v3 ).iterator ( ); // invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
		 /* .line 367 */
		 /* .local v3, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;" */
	 v4 = 	 } // :cond_2
	 if ( v4 != null) { // if-eqz v4, :cond_3
		 /* .line 368 */
		 /* check-cast v4, Ljava/lang/String; */
		 final String v5 = "android.hidl.base@1.0::IBase"; // const-string v5, "android.hidl.base@1.0::IBase"
		 v4 = 		 (( java.lang.String ) v4 ).equals ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 /* :try_end_0 */
		 /* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
		 if ( v4 != null) { // if-eqz v4, :cond_2
			 /* .line 369 */
			 /* .line 374 */
		 } // .end local v3 # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
	 } // :cond_3
	 /* .line 372 */
	 /* :catch_0 */
	 /* move-exception v3 */
	 /* .line 373 */
	 /* .local v3, "e":Landroid/os/RemoteException; */
	 (( android.os.RemoteException ) v3 ).printStackTrace ( ); // invoke-virtual {v3}, Landroid/os/RemoteException;->printStackTrace()V
	 /* .line 375 */
} // .end local v3 # "e":Landroid/os/RemoteException;
} // :goto_0
} // .end method
public static com.android.server.location.hardware.mtk.engineermode.V1_1.IEmCallback castFrom ( android.os.IHwInterface p0 ) {
/* .locals 1 */
/* .param p0, "iHwInterface" # Landroid/os/IHwInterface; */
/* .line 379 */
/* if-nez p0, :cond_0 */
/* .line 380 */
int v0 = 0; // const/4 v0, 0x0
/* .line 382 */
} // :cond_0
com.android.server.location.hardware.mtk.engineermode.V1_1.IEmCallback .asInterface ( v0 );
} // .end method
public static com.android.server.location.hardware.mtk.engineermode.V1_1.IEmCallback getService ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 386 */
final String v0 = "default"; // const-string v0, "default"
com.android.server.location.hardware.mtk.engineermode.V1_1.IEmCallback .getService ( v0 );
} // .end method
public static com.android.server.location.hardware.mtk.engineermode.V1_1.IEmCallback getService ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p0, "str" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 390 */
/* const-string/jumbo v0, "vendor.mediatek.hardware.engineermode@1.1::IEmCallback" */
android.os.HwBinder .getService ( v0,p0 );
com.android.server.location.hardware.mtk.engineermode.V1_1.IEmCallback .asInterface ( v0 );
} // .end method
public static com.android.server.location.hardware.mtk.engineermode.V1_1.IEmCallback getService ( java.lang.String p0, Boolean p1 ) {
/* .locals 1 */
/* .param p0, "str" # Ljava/lang/String; */
/* .param p1, "z" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 394 */
/* const-string/jumbo v0, "vendor.mediatek.hardware.engineermode@1.1::IEmCallback" */
android.os.HwBinder .getService ( v0,p0,p1 );
com.android.server.location.hardware.mtk.engineermode.V1_1.IEmCallback .asInterface ( v0 );
} // .end method
public static com.android.server.location.hardware.mtk.engineermode.V1_1.IEmCallback getService ( Boolean p0 ) {
/* .locals 1 */
/* .param p0, "z" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 398 */
final String v0 = "default"; // const-string v0, "default"
com.android.server.location.hardware.mtk.engineermode.V1_1.IEmCallback .getService ( v0,p0 );
} // .end method
/* # virtual methods */
public abstract android.os.IHwBinder asBinder ( ) {
} // .end method
public abstract void debug ( android.os.NativeHandle p0, java.util.ArrayList p1 ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/os/NativeHandle;", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract android.hidl.base.V1_0.DebugInfo getDebugInfo ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract java.util.ArrayList getHashChain ( ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "[B>;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract java.util.ArrayList interfaceChain ( ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract java.lang.String interfaceDescriptor ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean linkToDeath ( android.os.IHwBinder$DeathRecipient p0, Long p1 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void notifySyspropsChanged ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void ping ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void setHALInstrumentation ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean unlinkToDeath ( android.os.IHwBinder$DeathRecipient p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
