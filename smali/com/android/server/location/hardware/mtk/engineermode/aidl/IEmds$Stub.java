public abstract class com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub extends android.os.Binder implements com.android.server.location.hardware.mtk.engineermode.aidl.IEmds {
	 /* .source "IEmds.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x409 */
/* name = "Stub" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* } */
} // .end annotation
/* # static fields */
static final Integer TRANSACTION_btDoTest;
static final Integer TRANSACTION_btEndNoSigRxTest;
static final Integer TRANSACTION_btHciCommandRun;
static final Integer TRANSACTION_btInit;
static final Integer TRANSACTION_btIsBLEEnhancedSupport;
static final Integer TRANSACTION_btIsBLESupport;
static final Integer TRANSACTION_btIsComboSupport;
static final Integer TRANSACTION_btIsEmSupport;
static final Integer TRANSACTION_btPollingStart;
static final Integer TRANSACTION_btPollingStop;
static final Integer TRANSACTION_btStartNoSigRxTest;
static final Integer TRANSACTION_btStartRelayer;
static final Integer TRANSACTION_btStopRelayer;
static final Integer TRANSACTION_btUninit;
static final Integer TRANSACTION_connect;
static final Integer TRANSACTION_disconnect;
static final Integer TRANSACTION_getFilePathListWithCallBack;
static final Integer TRANSACTION_getInterfaceHash;
static final Integer TRANSACTION_getInterfaceVersion;
static final Integer TRANSACTION_isGauge30Support;
static final Integer TRANSACTION_isNfcSupport;
static final Integer TRANSACTION_readFileConfigWithCallBack;
static final Integer TRANSACTION_readMnlConfigFile;
static final Integer TRANSACTION_sendNfcRequest;
static final Integer TRANSACTION_sendToServer;
static final Integer TRANSACTION_sendToServerWithCallBack;
static final Integer TRANSACTION_setBypassDis;
static final Integer TRANSACTION_setBypassEn;
static final Integer TRANSACTION_setBypassService;
static final Integer TRANSACTION_setCallback;
static final Integer TRANSACTION_setCtIREngMode;
static final Integer TRANSACTION_setDisableC2kCap;
static final Integer TRANSACTION_setDsbpSupport;
static final Integer TRANSACTION_setEmConfigure;
static final Integer TRANSACTION_setEmUsbType;
static final Integer TRANSACTION_setEmUsbValue;
static final Integer TRANSACTION_setImsTestMode;
static final Integer TRANSACTION_setMdResetDelay;
static final Integer TRANSACTION_setModemWarningEnable;
static final Integer TRANSACTION_setMoms;
static final Integer TRANSACTION_setNfcResponseFunction;
static final Integer TRANSACTION_setPreferGprsMode;
static final Integer TRANSACTION_setRadioCapabilitySwitchEnable;
static final Integer TRANSACTION_setSmsFormat;
static final Integer TRANSACTION_setTestSimCardType;
static final Integer TRANSACTION_setUsbOtgSwitch;
static final Integer TRANSACTION_setUsbPort;
static final Integer TRANSACTION_setVolteMalPctid;
static final Integer TRANSACTION_setWcnCoreDump;
static final Integer TRANSACTION_writeFileConfig;
static final Integer TRANSACTION_writeMnlConfigFile;
static final Integer TRANSACTION_writeOtaFiles;
/* # direct methods */
public com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub ( ) {
/* .locals 1 */
/* .line 440 */
/* invoke-direct {p0}, Landroid/os/Binder;-><init>()V */
/* .line 441 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub ) p0 ).markVintfStability ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->markVintfStability()V
/* .line 442 */
/* const-string/jumbo v0, "vendor.mediatek.hardware.engineermode.IEmds" */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub ) p0 ).attachInterface ( p0, v0 ); // invoke-virtual {p0, p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V
/* .line 443 */
return;
} // .end method
public static com.android.server.location.hardware.mtk.engineermode.aidl.IEmds asInterface ( android.os.IBinder p0 ) {
/* .locals 2 */
/* .param p0, "obj" # Landroid/os/IBinder; */
/* .line 446 */
/* if-nez p0, :cond_0 */
/* .line 447 */
int v0 = 0; // const/4 v0, 0x0
/* .line 449 */
} // :cond_0
/* const-string/jumbo v0, "vendor.mediatek.hardware.engineermode.IEmds" */
/* .line 450 */
/* .local v0, "iin":Landroid/os/IInterface; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* instance-of v1, v0, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds; */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 451 */
/* move-object v1, v0 */
/* check-cast v1, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds; */
/* .line 453 */
} // :cond_1
/* new-instance v1, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy; */
/* invoke-direct {v1, p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;-><init>(Landroid/os/IBinder;)V */
} // .end method
/* # virtual methods */
public android.os.IBinder asBinder ( ) {
/* .locals 0 */
/* .line 458 */
} // .end method
public Boolean onTransact ( Integer p0, android.os.Parcel p1, android.os.Parcel p2, Integer p3 ) {
/* .locals 21 */
/* .param p1, "code" # I */
/* .param p2, "data" # Landroid/os/Parcel; */
/* .param p3, "reply" # Landroid/os/Parcel; */
/* .param p4, "flags" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 463 */
/* move-object/from16 v8, p0 */
/* move/from16 v9, p1 */
/* move-object/from16 v10, p3 */
/* const-string/jumbo v11, "vendor.mediatek.hardware.engineermode.IEmds" */
/* .line 464 */
/* .local v11, "descriptor":Ljava/lang/String; */
int v12 = 1; // const/4 v12, 0x1
/* if-lt v9, v12, :cond_0 */
/* const v0, 0xffffff */
/* if-gt v9, v0, :cond_0 */
/* .line 465 */
/* move-object/from16 v13, p2 */
(( android.os.Parcel ) v13 ).enforceInterface ( v11 ); // invoke-virtual {v13, v11}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 464 */
} // :cond_0
/* move-object/from16 v13, p2 */
/* .line 467 */
} // :goto_0
/* sparse-switch v9, :sswitch_data_0 */
/* .line 480 */
/* packed-switch v9, :pswitch_data_0 */
/* .line 817 */
v0 = /* invoke-super/range {p0 ..p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z */
/* .line 477 */
/* :sswitch_0 */
(( android.os.Parcel ) v10 ).writeString ( v11 ); // invoke-virtual {v10, v11}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 478 */
/* .line 473 */
/* :sswitch_1 */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 474 */
v0 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->getInterfaceVersion()I */
(( android.os.Parcel ) v10 ).writeInt ( v0 ); // invoke-virtual {v10, v0}, Landroid/os/Parcel;->writeInt(I)V
/* .line 475 */
/* .line 469 */
/* :sswitch_2 */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 470 */
/* invoke-virtual/range {p0 ..p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->getInterfaceHash()Ljava/lang/String; */
(( android.os.Parcel ) v10 ).writeString ( v0 ); // invoke-virtual {v10, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 471 */
/* .line 809 */
/* :pswitch_0 */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readString()Ljava/lang/String; */
/* .line 810 */
/* .local v0, "_arg036":Ljava/lang/String; */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->createByteArray()[B */
/* .line 811 */
/* .local v1, "_arg111":[B */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->enforceNoDataAvail()V */
/* .line 812 */
v2 = (( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub ) v8 ).writeOtaFiles ( v0, v1 ); // invoke-virtual {v8, v0, v1}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->writeOtaFiles(Ljava/lang/String;[B)Z
/* .line 813 */
/* .local v2, "_result46":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 814 */
(( android.os.Parcel ) v10 ).writeBoolean ( v2 ); // invoke-virtual {v10, v2}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 815 */
/* goto/16 :goto_1 */
/* .line 801 */
} // .end local v0 # "_arg036":Ljava/lang/String;
} // .end local v1 # "_arg111":[B
} // .end local v2 # "_result46":Z
/* :pswitch_1 */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readString()Ljava/lang/String; */
/* .line 802 */
/* .local v0, "_arg035":Ljava/lang/String; */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readString()Ljava/lang/String; */
/* .line 803 */
/* .local v1, "_arg110":Ljava/lang/String; */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->enforceNoDataAvail()V */
/* .line 804 */
v2 = (( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub ) v8 ).writeFileConfig ( v0, v1 ); // invoke-virtual {v8, v0, v1}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->writeFileConfig(Ljava/lang/String;Ljava/lang/String;)Z
/* .line 805 */
/* .local v2, "_result45":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 806 */
(( android.os.Parcel ) v10 ).writeBoolean ( v2 ); // invoke-virtual {v10, v2}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 807 */
/* goto/16 :goto_1 */
/* .line 793 */
} // .end local v0 # "_arg035":Ljava/lang/String;
} // .end local v1 # "_arg110":Ljava/lang/String;
} // .end local v2 # "_result45":Z
/* :pswitch_2 */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readString()Ljava/lang/String; */
/* .line 794 */
/* .local v0, "_arg034":Ljava/lang/String; */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder; */
com.android.server.location.hardware.mtk.engineermode.aidl.IEmCallbacks$Stub .asInterface ( v1 );
/* .line 795 */
/* .local v1, "_arg19":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks; */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->enforceNoDataAvail()V */
/* .line 796 */
v2 = (( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub ) v8 ).readFileConfigWithCallBack ( v0, v1 ); // invoke-virtual {v8, v0, v1}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->readFileConfigWithCallBack(Ljava/lang/String;Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks;)Z
/* .line 797 */
/* .local v2, "_result44":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 798 */
(( android.os.Parcel ) v10 ).writeBoolean ( v2 ); // invoke-virtual {v10, v2}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 799 */
/* goto/16 :goto_1 */
/* .line 787 */
} // .end local v0 # "_arg034":Ljava/lang/String;
} // .end local v1 # "_arg19":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks;
} // .end local v2 # "_result44":Z
/* :pswitch_3 */
v0 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 788 */
/* .local v0, "_arg033":I */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->createByteArray()[B */
/* .line 789 */
/* .local v1, "_arg18":[B */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->enforceNoDataAvail()V */
/* .line 790 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub ) v8 ).sendNfcRequest ( v0, v1 ); // invoke-virtual {v8, v0, v1}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->sendNfcRequest(I[B)V
/* .line 791 */
/* goto/16 :goto_1 */
/* .line 783 */
} // .end local v0 # "_arg033":I
} // .end local v1 # "_arg18":[B
/* :pswitch_4 */
/* invoke-virtual/range {p0 ..p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->disconnect()V */
/* .line 784 */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 785 */
/* goto/16 :goto_1 */
/* .line 778 */
/* :pswitch_5 */
v0 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->connect()Z */
/* .line 779 */
/* .local v0, "_result43":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 780 */
(( android.os.Parcel ) v10 ).writeBoolean ( v0 ); // invoke-virtual {v10, v0}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 781 */
/* goto/16 :goto_1 */
/* .line 772 */
} // .end local v0 # "_result43":Z
/* :pswitch_6 */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder; */
com.android.server.location.hardware.mtk.engineermode.aidl.IEmResponses$Stub .asInterface ( v0 );
/* .line 773 */
/* .local v0, "_arg032":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmResponses; */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->enforceNoDataAvail()V */
/* .line 774 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub ) v8 ).setNfcResponseFunction ( v0 ); // invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->setNfcResponseFunction(Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmResponses;)V
/* .line 775 */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 776 */
/* goto/16 :goto_1 */
/* .line 767 */
} // .end local v0 # "_arg032":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmResponses;
/* :pswitch_7 */
v0 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->isNfcSupport()I */
/* .line 768 */
/* .local v0, "_result42":I */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 769 */
(( android.os.Parcel ) v10 ).writeInt ( v0 ); // invoke-virtual {v10, v0}, Landroid/os/Parcel;->writeInt(I)V
/* .line 770 */
/* goto/16 :goto_1 */
/* .line 759 */
} // .end local v0 # "_result42":I
/* :pswitch_8 */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->createByteArray()[B */
/* .line 760 */
/* .local v0, "_arg031":[B */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->createIntArray()[I */
/* .line 761 */
/* .local v1, "_arg17":[I */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->enforceNoDataAvail()V */
/* .line 762 */
v2 = (( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub ) v8 ).writeMnlConfigFile ( v0, v1 ); // invoke-virtual {v8, v0, v1}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->writeMnlConfigFile([B[I)Z
/* .line 763 */
/* .local v2, "_result41":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 764 */
(( android.os.Parcel ) v10 ).writeBoolean ( v2 ); // invoke-virtual {v10, v2}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 765 */
/* goto/16 :goto_1 */
/* .line 752 */
} // .end local v0 # "_arg031":[B
} // .end local v1 # "_arg17":[I
} // .end local v2 # "_result41":Z
/* :pswitch_9 */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->createIntArray()[I */
/* .line 753 */
/* .local v0, "_arg030":[I */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->enforceNoDataAvail()V */
/* .line 754 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub ) v8 ).readMnlConfigFile ( v0 ); // invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->readMnlConfigFile([I)[B
/* .line 755 */
/* .local v1, "_result40":[B */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 756 */
(( android.os.Parcel ) v10 ).writeByteArray ( v1 ); // invoke-virtual {v10, v1}, Landroid/os/Parcel;->writeByteArray([B)V
/* .line 757 */
/* goto/16 :goto_1 */
/* .line 744 */
} // .end local v0 # "_arg030":[I
} // .end local v1 # "_result40":[B
/* :pswitch_a */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readString()Ljava/lang/String; */
/* .line 745 */
/* .local v0, "_arg029":Ljava/lang/String; */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder; */
com.android.server.location.hardware.mtk.engineermode.aidl.IEmCallbacks$Stub .asInterface ( v1 );
/* .line 746 */
/* .local v1, "_arg16":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks; */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->enforceNoDataAvail()V */
/* .line 747 */
v2 = (( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub ) v8 ).getFilePathListWithCallBack ( v0, v1 ); // invoke-virtual {v8, v0, v1}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->getFilePathListWithCallBack(Ljava/lang/String;Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks;)Z
/* .line 748 */
/* .local v2, "_result39":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 749 */
(( android.os.Parcel ) v10 ).writeBoolean ( v2 ); // invoke-virtual {v10, v2}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 750 */
/* goto/16 :goto_1 */
/* .line 737 */
} // .end local v0 # "_arg029":Ljava/lang/String;
} // .end local v1 # "_arg16":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks;
} // .end local v2 # "_result39":Z
/* :pswitch_b */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readString()Ljava/lang/String; */
/* .line 738 */
/* .local v0, "_arg028":Ljava/lang/String; */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->enforceNoDataAvail()V */
/* .line 739 */
v1 = (( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub ) v8 ).setMoms ( v0 ); // invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->setMoms(Ljava/lang/String;)Z
/* .line 740 */
/* .local v1, "_result38":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 741 */
(( android.os.Parcel ) v10 ).writeBoolean ( v1 ); // invoke-virtual {v10, v1}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 742 */
/* goto/16 :goto_1 */
/* .line 730 */
} // .end local v0 # "_arg028":Ljava/lang/String;
} // .end local v1 # "_result38":Z
/* :pswitch_c */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readString()Ljava/lang/String; */
/* .line 731 */
/* .local v0, "_arg027":Ljava/lang/String; */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->enforceNoDataAvail()V */
/* .line 732 */
v1 = (( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub ) v8 ).setBypassService ( v0 ); // invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->setBypassService(Ljava/lang/String;)Z
/* .line 733 */
/* .local v1, "_result37":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 734 */
(( android.os.Parcel ) v10 ).writeBoolean ( v1 ); // invoke-virtual {v10, v1}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 735 */
/* goto/16 :goto_1 */
/* .line 723 */
} // .end local v0 # "_arg027":Ljava/lang/String;
} // .end local v1 # "_result37":Z
/* :pswitch_d */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readString()Ljava/lang/String; */
/* .line 724 */
/* .local v0, "_arg026":Ljava/lang/String; */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->enforceNoDataAvail()V */
/* .line 725 */
v1 = (( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub ) v8 ).setBypassEn ( v0 ); // invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->setBypassEn(Ljava/lang/String;)Z
/* .line 726 */
/* .local v1, "_result36":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 727 */
(( android.os.Parcel ) v10 ).writeBoolean ( v1 ); // invoke-virtual {v10, v1}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 728 */
/* goto/16 :goto_1 */
/* .line 716 */
} // .end local v0 # "_arg026":Ljava/lang/String;
} // .end local v1 # "_result36":Z
/* :pswitch_e */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readString()Ljava/lang/String; */
/* .line 717 */
/* .local v0, "_arg025":Ljava/lang/String; */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->enforceNoDataAvail()V */
/* .line 718 */
v1 = (( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub ) v8 ).setBypassDis ( v0 ); // invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->setBypassDis(Ljava/lang/String;)Z
/* .line 719 */
/* .local v1, "_result35":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 720 */
(( android.os.Parcel ) v10 ).writeBoolean ( v1 ); // invoke-virtual {v10, v1}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 721 */
/* goto/16 :goto_1 */
/* .line 709 */
} // .end local v0 # "_arg025":Ljava/lang/String;
} // .end local v1 # "_result35":Z
/* :pswitch_f */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readString()Ljava/lang/String; */
/* .line 710 */
/* .local v0, "_arg024":Ljava/lang/String; */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->enforceNoDataAvail()V */
/* .line 711 */
v1 = (( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub ) v8 ).setEmUsbValue ( v0 ); // invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->setEmUsbValue(Ljava/lang/String;)Z
/* .line 712 */
/* .local v1, "_result34":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 713 */
(( android.os.Parcel ) v10 ).writeBoolean ( v1 ); // invoke-virtual {v10, v1}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 714 */
/* goto/16 :goto_1 */
/* .line 702 */
} // .end local v0 # "_arg024":Ljava/lang/String;
} // .end local v1 # "_result34":Z
/* :pswitch_10 */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readString()Ljava/lang/String; */
/* .line 703 */
/* .local v0, "_arg023":Ljava/lang/String; */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->enforceNoDataAvail()V */
/* .line 704 */
v1 = (( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub ) v8 ).setEmUsbType ( v0 ); // invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->setEmUsbType(Ljava/lang/String;)Z
/* .line 705 */
/* .local v1, "_result33":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 706 */
(( android.os.Parcel ) v10 ).writeBoolean ( v1 ); // invoke-virtual {v10, v1}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 707 */
/* goto/16 :goto_1 */
/* .line 695 */
} // .end local v0 # "_arg023":Ljava/lang/String;
} // .end local v1 # "_result33":Z
/* :pswitch_11 */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readString()Ljava/lang/String; */
/* .line 696 */
/* .local v0, "_arg022":Ljava/lang/String; */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->enforceNoDataAvail()V */
/* .line 697 */
v1 = (( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub ) v8 ).setUsbOtgSwitch ( v0 ); // invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->setUsbOtgSwitch(Ljava/lang/String;)Z
/* .line 698 */
/* .local v1, "_result32":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 699 */
(( android.os.Parcel ) v10 ).writeBoolean ( v1 ); // invoke-virtual {v10, v1}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 700 */
/* goto/16 :goto_1 */
/* .line 688 */
} // .end local v0 # "_arg022":Ljava/lang/String;
} // .end local v1 # "_result32":Z
/* :pswitch_12 */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readString()Ljava/lang/String; */
/* .line 689 */
/* .local v0, "_arg021":Ljava/lang/String; */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->enforceNoDataAvail()V */
/* .line 690 */
v1 = (( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub ) v8 ).setUsbPort ( v0 ); // invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->setUsbPort(Ljava/lang/String;)Z
/* .line 691 */
/* .local v1, "_result31":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 692 */
(( android.os.Parcel ) v10 ).writeBoolean ( v1 ); // invoke-virtual {v10, v1}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 693 */
/* goto/16 :goto_1 */
/* .line 681 */
} // .end local v0 # "_arg021":Ljava/lang/String;
} // .end local v1 # "_result31":Z
/* :pswitch_13 */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readString()Ljava/lang/String; */
/* .line 682 */
/* .local v0, "_arg020":Ljava/lang/String; */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->enforceNoDataAvail()V */
/* .line 683 */
v1 = (( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub ) v8 ).setWcnCoreDump ( v0 ); // invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->setWcnCoreDump(Ljava/lang/String;)Z
/* .line 684 */
/* .local v1, "_result30":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 685 */
(( android.os.Parcel ) v10 ).writeBoolean ( v1 ); // invoke-virtual {v10, v1}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 686 */
/* goto/16 :goto_1 */
/* .line 674 */
} // .end local v0 # "_arg020":Ljava/lang/String;
} // .end local v1 # "_result30":Z
/* :pswitch_14 */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readString()Ljava/lang/String; */
/* .line 675 */
/* .local v0, "_arg019":Ljava/lang/String; */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->enforceNoDataAvail()V */
/* .line 676 */
v1 = (( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub ) v8 ).setModemWarningEnable ( v0 ); // invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->setModemWarningEnable(Ljava/lang/String;)Z
/* .line 677 */
/* .local v1, "_result29":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 678 */
(( android.os.Parcel ) v10 ).writeBoolean ( v1 ); // invoke-virtual {v10, v1}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 679 */
/* goto/16 :goto_1 */
/* .line 667 */
} // .end local v0 # "_arg019":Ljava/lang/String;
} // .end local v1 # "_result29":Z
/* :pswitch_15 */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readString()Ljava/lang/String; */
/* .line 668 */
/* .local v0, "_arg018":Ljava/lang/String; */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->enforceNoDataAvail()V */
/* .line 669 */
v1 = (( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub ) v8 ).setMdResetDelay ( v0 ); // invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->setMdResetDelay(Ljava/lang/String;)Z
/* .line 670 */
/* .local v1, "_result28":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 671 */
(( android.os.Parcel ) v10 ).writeBoolean ( v1 ); // invoke-virtual {v10, v1}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 672 */
/* goto/16 :goto_1 */
/* .line 662 */
} // .end local v0 # "_arg018":Ljava/lang/String;
} // .end local v1 # "_result28":Z
/* :pswitch_16 */
v0 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->btUninit()I */
/* .line 663 */
/* .local v0, "_result27":I */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 664 */
(( android.os.Parcel ) v10 ).writeInt ( v0 ); // invoke-virtual {v10, v0}, Landroid/os/Parcel;->writeInt(I)V
/* .line 665 */
/* goto/16 :goto_1 */
/* .line 657 */
} // .end local v0 # "_result27":I
/* :pswitch_17 */
v0 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->btStopRelayer()I */
/* .line 658 */
/* .local v0, "_result26":I */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 659 */
(( android.os.Parcel ) v10 ).writeInt ( v0 ); // invoke-virtual {v10, v0}, Landroid/os/Parcel;->writeInt(I)V
/* .line 660 */
/* goto/16 :goto_1 */
/* .line 649 */
} // .end local v0 # "_result26":I
/* :pswitch_18 */
v0 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 650 */
/* .local v0, "_arg017":I */
v1 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 651 */
/* .local v1, "_arg15":I */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->enforceNoDataAvail()V */
/* .line 652 */
v2 = (( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub ) v8 ).btStartRelayer ( v0, v1 ); // invoke-virtual {v8, v0, v1}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->btStartRelayer(II)I
/* .line 653 */
/* .local v2, "_result25":I */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 654 */
(( android.os.Parcel ) v10 ).writeInt ( v2 ); // invoke-virtual {v10, v2}, Landroid/os/Parcel;->writeInt(I)V
/* .line 655 */
/* goto/16 :goto_1 */
/* .line 639 */
} // .end local v0 # "_arg017":I
} // .end local v1 # "_arg15":I
} // .end local v2 # "_result25":I
/* :pswitch_19 */
v0 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 640 */
/* .local v0, "_arg016":I */
v1 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 641 */
/* .local v1, "_arg14":I */
v2 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 642 */
/* .local v2, "_arg22":I */
v3 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 643 */
/* .local v3, "_arg32":I */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->enforceNoDataAvail()V */
/* .line 644 */
v4 = (( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub ) v8 ).btStartNoSigRxTest ( v0, v1, v2, v3 ); // invoke-virtual {v8, v0, v1, v2, v3}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->btStartNoSigRxTest(IIII)Z
/* .line 645 */
/* .local v4, "_result24":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 646 */
(( android.os.Parcel ) v10 ).writeBoolean ( v4 ); // invoke-virtual {v10, v4}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 647 */
/* goto/16 :goto_1 */
/* .line 634 */
} // .end local v0 # "_arg016":I
} // .end local v1 # "_arg14":I
} // .end local v2 # "_arg22":I
} // .end local v3 # "_arg32":I
} // .end local v4 # "_result24":Z
/* :pswitch_1a */
v0 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->btPollingStop()I */
/* .line 635 */
/* .local v0, "_result23":I */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 636 */
(( android.os.Parcel ) v10 ).writeInt ( v0 ); // invoke-virtual {v10, v0}, Landroid/os/Parcel;->writeInt(I)V
/* .line 637 */
/* goto/16 :goto_1 */
/* .line 629 */
} // .end local v0 # "_result23":I
/* :pswitch_1b */
v0 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->btPollingStart()I */
/* .line 630 */
/* .local v0, "_result22":I */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 631 */
(( android.os.Parcel ) v10 ).writeInt ( v0 ); // invoke-virtual {v10, v0}, Landroid/os/Parcel;->writeInt(I)V
/* .line 632 */
/* goto/16 :goto_1 */
/* .line 624 */
} // .end local v0 # "_result22":I
/* :pswitch_1c */
v0 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->btIsEmSupport()I */
/* .line 625 */
/* .local v0, "_result21":I */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 626 */
(( android.os.Parcel ) v10 ).writeInt ( v0 ); // invoke-virtual {v10, v0}, Landroid/os/Parcel;->writeInt(I)V
/* .line 627 */
/* goto/16 :goto_1 */
/* .line 619 */
} // .end local v0 # "_result21":I
/* :pswitch_1d */
v0 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->btIsComboSupport()I */
/* .line 620 */
/* .local v0, "_result20":I */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 621 */
(( android.os.Parcel ) v10 ).writeInt ( v0 ); // invoke-virtual {v10, v0}, Landroid/os/Parcel;->writeInt(I)V
/* .line 622 */
/* goto/16 :goto_1 */
/* .line 614 */
} // .end local v0 # "_result20":I
/* :pswitch_1e */
v0 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->btIsBLESupport()I */
/* .line 615 */
/* .local v0, "_result19":I */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 616 */
(( android.os.Parcel ) v10 ).writeInt ( v0 ); // invoke-virtual {v10, v0}, Landroid/os/Parcel;->writeInt(I)V
/* .line 617 */
/* goto/16 :goto_1 */
/* .line 609 */
} // .end local v0 # "_result19":I
/* :pswitch_1f */
v0 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->btIsBLEEnhancedSupport()Z */
/* .line 610 */
/* .local v0, "_result18":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 611 */
(( android.os.Parcel ) v10 ).writeBoolean ( v0 ); // invoke-virtual {v10, v0}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 612 */
/* goto/16 :goto_1 */
/* .line 604 */
} // .end local v0 # "_result18":Z
/* :pswitch_20 */
v0 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->btInit()I */
/* .line 605 */
/* .local v0, "_result17":I */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 606 */
(( android.os.Parcel ) v10 ).writeInt ( v0 ); // invoke-virtual {v10, v0}, Landroid/os/Parcel;->writeInt(I)V
/* .line 607 */
/* goto/16 :goto_1 */
/* .line 597 */
} // .end local v0 # "_result17":I
/* :pswitch_21 */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->createByteArray()[B */
/* .line 598 */
/* .local v0, "_arg015":[B */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->enforceNoDataAvail()V */
/* .line 599 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub ) v8 ).btHciCommandRun ( v0 ); // invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->btHciCommandRun([B)[B
/* .line 600 */
/* .local v1, "_result16":[B */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 601 */
(( android.os.Parcel ) v10 ).writeByteArray ( v1 ); // invoke-virtual {v10, v1}, Landroid/os/Parcel;->writeByteArray([B)V
/* .line 602 */
/* goto/16 :goto_1 */
/* .line 592 */
} // .end local v0 # "_arg015":[B
} // .end local v1 # "_result16":[B
/* :pswitch_22 */
/* invoke-virtual/range {p0 ..p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->btEndNoSigRxTest()[I */
/* .line 593 */
/* .local v0, "_result15":[I */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 594 */
(( android.os.Parcel ) v10 ).writeIntArray ( v0 ); // invoke-virtual {v10, v0}, Landroid/os/Parcel;->writeIntArray([I)V
/* .line 595 */
/* goto/16 :goto_1 */
/* .line 579 */
} // .end local v0 # "_result15":[I
/* :pswitch_23 */
v14 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 580 */
/* .local v14, "_arg014":I */
v15 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 581 */
/* .local v15, "_arg13":I */
v16 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 582 */
/* .local v16, "_arg2":I */
v17 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 583 */
/* .local v17, "_arg3":I */
v18 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 584 */
/* .local v18, "_arg4":I */
v19 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 585 */
/* .local v19, "_arg5":I */
v20 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 586 */
/* .local v20, "_arg6":I */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->enforceNoDataAvail()V */
/* .line 587 */
/* move-object/from16 v0, p0 */
/* move v1, v14 */
/* move v2, v15 */
/* move/from16 v3, v16 */
/* move/from16 v4, v17 */
/* move/from16 v5, v18 */
/* move/from16 v6, v19 */
/* move/from16 v7, v20 */
v0 = /* invoke-virtual/range {v0 ..v7}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->btDoTest(IIIIIII)I */
/* .line 588 */
/* .local v0, "_result14":I */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 589 */
(( android.os.Parcel ) v10 ).writeInt ( v0 ); // invoke-virtual {v10, v0}, Landroid/os/Parcel;->writeInt(I)V
/* .line 590 */
/* goto/16 :goto_1 */
/* .line 574 */
} // .end local v0 # "_result14":I
} // .end local v14 # "_arg014":I
} // .end local v15 # "_arg13":I
} // .end local v16 # "_arg2":I
} // .end local v17 # "_arg3":I
} // .end local v18 # "_arg4":I
} // .end local v19 # "_arg5":I
} // .end local v20 # "_arg6":I
/* :pswitch_24 */
v0 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->isGauge30Support()I */
/* .line 575 */
/* .local v0, "_result13":I */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 576 */
(( android.os.Parcel ) v10 ).writeInt ( v0 ); // invoke-virtual {v10, v0}, Landroid/os/Parcel;->writeInt(I)V
/* .line 577 */
/* goto/16 :goto_1 */
/* .line 566 */
} // .end local v0 # "_result13":I
/* :pswitch_25 */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readString()Ljava/lang/String; */
/* .line 567 */
/* .local v0, "_arg013":Ljava/lang/String; */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readString()Ljava/lang/String; */
/* .line 568 */
/* .local v1, "_arg12":Ljava/lang/String; */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->enforceNoDataAvail()V */
/* .line 569 */
v2 = (( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub ) v8 ).setEmConfigure ( v0, v1 ); // invoke-virtual {v8, v0, v1}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->setEmConfigure(Ljava/lang/String;Ljava/lang/String;)Z
/* .line 570 */
/* .local v2, "_result12":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 571 */
(( android.os.Parcel ) v10 ).writeBoolean ( v2 ); // invoke-virtual {v10, v2}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 572 */
/* goto/16 :goto_1 */
/* .line 559 */
} // .end local v0 # "_arg013":Ljava/lang/String;
} // .end local v1 # "_arg12":Ljava/lang/String;
} // .end local v2 # "_result12":Z
/* :pswitch_26 */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readString()Ljava/lang/String; */
/* .line 560 */
/* .local v0, "_arg012":Ljava/lang/String; */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->enforceNoDataAvail()V */
/* .line 561 */
v1 = (( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub ) v8 ).setVolteMalPctid ( v0 ); // invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->setVolteMalPctid(Ljava/lang/String;)Z
/* .line 562 */
/* .local v1, "_result11":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 563 */
(( android.os.Parcel ) v10 ).writeBoolean ( v1 ); // invoke-virtual {v10, v1}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 564 */
/* goto/16 :goto_1 */
/* .line 552 */
} // .end local v0 # "_arg012":Ljava/lang/String;
} // .end local v1 # "_result11":Z
/* :pswitch_27 */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readString()Ljava/lang/String; */
/* .line 553 */
/* .local v0, "_arg011":Ljava/lang/String; */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->enforceNoDataAvail()V */
/* .line 554 */
v1 = (( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub ) v8 ).setImsTestMode ( v0 ); // invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->setImsTestMode(Ljava/lang/String;)Z
/* .line 555 */
/* .local v1, "_result10":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 556 */
(( android.os.Parcel ) v10 ).writeBoolean ( v1 ); // invoke-virtual {v10, v1}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 557 */
/* goto/16 :goto_1 */
/* .line 545 */
} // .end local v0 # "_arg011":Ljava/lang/String;
} // .end local v1 # "_result10":Z
/* :pswitch_28 */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readString()Ljava/lang/String; */
/* .line 546 */
/* .local v0, "_arg010":Ljava/lang/String; */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->enforceNoDataAvail()V */
/* .line 547 */
v1 = (( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub ) v8 ).setTestSimCardType ( v0 ); // invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->setTestSimCardType(Ljava/lang/String;)Z
/* .line 548 */
/* .local v1, "_result9":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 549 */
(( android.os.Parcel ) v10 ).writeBoolean ( v1 ); // invoke-virtual {v10, v1}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 550 */
/* goto/16 :goto_1 */
/* .line 538 */
} // .end local v0 # "_arg010":Ljava/lang/String;
} // .end local v1 # "_result9":Z
/* :pswitch_29 */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readString()Ljava/lang/String; */
/* .line 539 */
/* .local v0, "_arg09":Ljava/lang/String; */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->enforceNoDataAvail()V */
/* .line 540 */
v1 = (( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub ) v8 ).setSmsFormat ( v0 ); // invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->setSmsFormat(Ljava/lang/String;)Z
/* .line 541 */
/* .local v1, "_result8":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 542 */
(( android.os.Parcel ) v10 ).writeBoolean ( v1 ); // invoke-virtual {v10, v1}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 543 */
/* goto/16 :goto_1 */
/* .line 531 */
} // .end local v0 # "_arg09":Ljava/lang/String;
} // .end local v1 # "_result8":Z
/* :pswitch_2a */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readString()Ljava/lang/String; */
/* .line 532 */
/* .local v0, "_arg08":Ljava/lang/String; */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->enforceNoDataAvail()V */
/* .line 533 */
v1 = (( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub ) v8 ).setRadioCapabilitySwitchEnable ( v0 ); // invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->setRadioCapabilitySwitchEnable(Ljava/lang/String;)Z
/* .line 534 */
/* .local v1, "_result7":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 535 */
(( android.os.Parcel ) v10 ).writeBoolean ( v1 ); // invoke-virtual {v10, v1}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 536 */
/* goto/16 :goto_1 */
/* .line 524 */
} // .end local v0 # "_arg08":Ljava/lang/String;
} // .end local v1 # "_result7":Z
/* :pswitch_2b */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readString()Ljava/lang/String; */
/* .line 525 */
/* .local v0, "_arg07":Ljava/lang/String; */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->enforceNoDataAvail()V */
/* .line 526 */
v1 = (( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub ) v8 ).setPreferGprsMode ( v0 ); // invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->setPreferGprsMode(Ljava/lang/String;)Z
/* .line 527 */
/* .local v1, "_result6":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 528 */
(( android.os.Parcel ) v10 ).writeBoolean ( v1 ); // invoke-virtual {v10, v1}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 529 */
/* goto/16 :goto_1 */
/* .line 517 */
} // .end local v0 # "_arg07":Ljava/lang/String;
} // .end local v1 # "_result6":Z
/* :pswitch_2c */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readString()Ljava/lang/String; */
/* .line 518 */
/* .local v0, "_arg06":Ljava/lang/String; */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->enforceNoDataAvail()V */
/* .line 519 */
v1 = (( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub ) v8 ).setDsbpSupport ( v0 ); // invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->setDsbpSupport(Ljava/lang/String;)Z
/* .line 520 */
/* .local v1, "_result5":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 521 */
(( android.os.Parcel ) v10 ).writeBoolean ( v1 ); // invoke-virtual {v10, v1}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 522 */
/* .line 510 */
} // .end local v0 # "_arg06":Ljava/lang/String;
} // .end local v1 # "_result5":Z
/* :pswitch_2d */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readString()Ljava/lang/String; */
/* .line 511 */
/* .local v0, "_arg05":Ljava/lang/String; */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->enforceNoDataAvail()V */
/* .line 512 */
v1 = (( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub ) v8 ).setDisableC2kCap ( v0 ); // invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->setDisableC2kCap(Ljava/lang/String;)Z
/* .line 513 */
/* .local v1, "_result4":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 514 */
(( android.os.Parcel ) v10 ).writeBoolean ( v1 ); // invoke-virtual {v10, v1}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 515 */
/* .line 503 */
} // .end local v0 # "_arg05":Ljava/lang/String;
} // .end local v1 # "_result4":Z
/* :pswitch_2e */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readString()Ljava/lang/String; */
/* .line 504 */
/* .local v0, "_arg04":Ljava/lang/String; */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->enforceNoDataAvail()V */
/* .line 505 */
v1 = (( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub ) v8 ).setCtIREngMode ( v0 ); // invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->setCtIREngMode(Ljava/lang/String;)Z
/* .line 506 */
/* .local v1, "_result3":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 507 */
(( android.os.Parcel ) v10 ).writeBoolean ( v1 ); // invoke-virtual {v10, v1}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 508 */
/* .line 495 */
} // .end local v0 # "_arg04":Ljava/lang/String;
} // .end local v1 # "_result3":Z
/* :pswitch_2f */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readString()Ljava/lang/String; */
/* .line 496 */
/* .local v0, "_arg03":Ljava/lang/String; */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder; */
com.android.server.location.hardware.mtk.engineermode.aidl.IEmCallbacks$Stub .asInterface ( v1 );
/* .line 497 */
/* .local v1, "_arg1":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks; */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->enforceNoDataAvail()V */
/* .line 498 */
v2 = (( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub ) v8 ).sendToServerWithCallBack ( v0, v1 ); // invoke-virtual {v8, v0, v1}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->sendToServerWithCallBack(Ljava/lang/String;Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks;)Z
/* .line 499 */
/* .local v2, "_result2":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 500 */
(( android.os.Parcel ) v10 ).writeBoolean ( v2 ); // invoke-virtual {v10, v2}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 501 */
/* .line 488 */
} // .end local v0 # "_arg03":Ljava/lang/String;
} // .end local v1 # "_arg1":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks;
} // .end local v2 # "_result2":Z
/* :pswitch_30 */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readString()Ljava/lang/String; */
/* .line 489 */
/* .local v0, "_arg02":Ljava/lang/String; */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->enforceNoDataAvail()V */
/* .line 490 */
v1 = (( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub ) v8 ).sendToServer ( v0 ); // invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->sendToServer(Ljava/lang/String;)Z
/* .line 491 */
/* .local v1, "_result":Z */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 492 */
(( android.os.Parcel ) v10 ).writeBoolean ( v1 ); // invoke-virtual {v10, v1}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 493 */
/* .line 482 */
} // .end local v0 # "_arg02":Ljava/lang/String;
} // .end local v1 # "_result":Z
/* :pswitch_31 */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder; */
com.android.server.location.hardware.mtk.engineermode.aidl.IEmCallbacks$Stub .asInterface ( v0 );
/* .line 483 */
/* .local v0, "_arg0":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks; */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->enforceNoDataAvail()V */
/* .line 484 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Stub ) v8 ).setCallback ( v0 ); // invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->setCallback(Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks;)V
/* .line 485 */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 486 */
/* nop */
/* .line 819 */
} // .end local v0 # "_arg0":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks;
} // :goto_1
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0xfffffe -> :sswitch_2 */
/* 0xffffff -> :sswitch_1 */
/* 0x5f4e5446 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_31 */
/* :pswitch_30 */
/* :pswitch_2f */
/* :pswitch_2e */
/* :pswitch_2d */
/* :pswitch_2c */
/* :pswitch_2b */
/* :pswitch_2a */
/* :pswitch_29 */
/* :pswitch_28 */
/* :pswitch_27 */
/* :pswitch_26 */
/* :pswitch_25 */
/* :pswitch_24 */
/* :pswitch_23 */
/* :pswitch_22 */
/* :pswitch_21 */
/* :pswitch_20 */
/* :pswitch_1f */
/* :pswitch_1e */
/* :pswitch_1d */
/* :pswitch_1c */
/* :pswitch_1b */
/* :pswitch_1a */
/* :pswitch_19 */
/* :pswitch_18 */
/* :pswitch_17 */
/* :pswitch_16 */
/* :pswitch_15 */
/* :pswitch_14 */
/* :pswitch_13 */
/* :pswitch_12 */
/* :pswitch_11 */
/* :pswitch_10 */
/* :pswitch_f */
/* :pswitch_e */
/* :pswitch_d */
/* :pswitch_c */
/* :pswitch_b */
/* :pswitch_a */
/* :pswitch_9 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
