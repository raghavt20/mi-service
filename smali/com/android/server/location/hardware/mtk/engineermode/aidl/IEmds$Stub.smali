.class public abstract Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;
.super Landroid/os/Binder;
.source "IEmds.java"

# interfaces
.implements Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;
    }
.end annotation


# static fields
.field static final TRANSACTION_btDoTest:I = 0xf

.field static final TRANSACTION_btEndNoSigRxTest:I = 0x10

.field static final TRANSACTION_btHciCommandRun:I = 0x11

.field static final TRANSACTION_btInit:I = 0x12

.field static final TRANSACTION_btIsBLEEnhancedSupport:I = 0x13

.field static final TRANSACTION_btIsBLESupport:I = 0x14

.field static final TRANSACTION_btIsComboSupport:I = 0x15

.field static final TRANSACTION_btIsEmSupport:I = 0x16

.field static final TRANSACTION_btPollingStart:I = 0x17

.field static final TRANSACTION_btPollingStop:I = 0x18

.field static final TRANSACTION_btStartNoSigRxTest:I = 0x19

.field static final TRANSACTION_btStartRelayer:I = 0x1a

.field static final TRANSACTION_btStopRelayer:I = 0x1b

.field static final TRANSACTION_btUninit:I = 0x1c

.field static final TRANSACTION_connect:I = 0x2d

.field static final TRANSACTION_disconnect:I = 0x2e

.field static final TRANSACTION_getFilePathListWithCallBack:I = 0x28

.field static final TRANSACTION_getInterfaceHash:I = 0xfffffe

.field static final TRANSACTION_getInterfaceVersion:I = 0xffffff

.field static final TRANSACTION_isGauge30Support:I = 0xe

.field static final TRANSACTION_isNfcSupport:I = 0x2b

.field static final TRANSACTION_readFileConfigWithCallBack:I = 0x30

.field static final TRANSACTION_readMnlConfigFile:I = 0x29

.field static final TRANSACTION_sendNfcRequest:I = 0x2f

.field static final TRANSACTION_sendToServer:I = 0x2

.field static final TRANSACTION_sendToServerWithCallBack:I = 0x3

.field static final TRANSACTION_setBypassDis:I = 0x24

.field static final TRANSACTION_setBypassEn:I = 0x25

.field static final TRANSACTION_setBypassService:I = 0x26

.field static final TRANSACTION_setCallback:I = 0x1

.field static final TRANSACTION_setCtIREngMode:I = 0x4

.field static final TRANSACTION_setDisableC2kCap:I = 0x5

.field static final TRANSACTION_setDsbpSupport:I = 0x6

.field static final TRANSACTION_setEmConfigure:I = 0xd

.field static final TRANSACTION_setEmUsbType:I = 0x22

.field static final TRANSACTION_setEmUsbValue:I = 0x23

.field static final TRANSACTION_setImsTestMode:I = 0xb

.field static final TRANSACTION_setMdResetDelay:I = 0x1d

.field static final TRANSACTION_setModemWarningEnable:I = 0x1e

.field static final TRANSACTION_setMoms:I = 0x27

.field static final TRANSACTION_setNfcResponseFunction:I = 0x2c

.field static final TRANSACTION_setPreferGprsMode:I = 0x7

.field static final TRANSACTION_setRadioCapabilitySwitchEnable:I = 0x8

.field static final TRANSACTION_setSmsFormat:I = 0x9

.field static final TRANSACTION_setTestSimCardType:I = 0xa

.field static final TRANSACTION_setUsbOtgSwitch:I = 0x21

.field static final TRANSACTION_setUsbPort:I = 0x20

.field static final TRANSACTION_setVolteMalPctid:I = 0xc

.field static final TRANSACTION_setWcnCoreDump:I = 0x1f

.field static final TRANSACTION_writeFileConfig:I = 0x31

.field static final TRANSACTION_writeMnlConfigFile:I = 0x2a

.field static final TRANSACTION_writeOtaFiles:I = 0x32


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 440
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 441
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->markVintfStability()V

    .line 442
    const-string/jumbo v0, "vendor.mediatek.hardware.engineermode.IEmds"

    invoke-virtual {p0, p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 443
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .line 446
    if-nez p0, :cond_0

    .line 447
    const/4 v0, 0x0

    return-object v0

    .line 449
    :cond_0
    const-string/jumbo v0, "vendor.mediatek.hardware.engineermode.IEmds"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 450
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds;

    if-eqz v1, :cond_1

    .line 451
    move-object v1, v0

    check-cast v1, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds;

    return-object v1

    .line 453
    :cond_1
    new-instance v1, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;

    invoke-direct {v1, p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    return-object v1
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .line 458
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 21
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 463
    move-object/from16 v8, p0

    move/from16 v9, p1

    move-object/from16 v10, p3

    const-string/jumbo v11, "vendor.mediatek.hardware.engineermode.IEmds"

    .line 464
    .local v11, "descriptor":Ljava/lang/String;
    const/4 v12, 0x1

    if-lt v9, v12, :cond_0

    const v0, 0xffffff

    if-gt v9, v0, :cond_0

    .line 465
    move-object/from16 v13, p2

    invoke-virtual {v13, v11}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    goto :goto_0

    .line 464
    :cond_0
    move-object/from16 v13, p2

    .line 467
    :goto_0
    sparse-switch v9, :sswitch_data_0

    .line 480
    packed-switch v9, :pswitch_data_0

    .line 817
    invoke-super/range {p0 .. p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    return v0

    .line 477
    :sswitch_0
    invoke-virtual {v10, v11}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 478
    return v12

    .line 473
    :sswitch_1
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 474
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->getInterfaceVersion()I

    move-result v0

    invoke-virtual {v10, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 475
    return v12

    .line 469
    :sswitch_2
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 470
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->getInterfaceHash()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 471
    return v12

    .line 809
    :pswitch_0
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 810
    .local v0, "_arg036":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v1

    .line 811
    .local v1, "_arg111":[B
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 812
    invoke-virtual {v8, v0, v1}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->writeOtaFiles(Ljava/lang/String;[B)Z

    move-result v2

    .line 813
    .local v2, "_result46":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 814
    invoke-virtual {v10, v2}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 815
    goto/16 :goto_1

    .line 801
    .end local v0    # "_arg036":Ljava/lang/String;
    .end local v1    # "_arg111":[B
    .end local v2    # "_result46":Z
    :pswitch_1
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 802
    .local v0, "_arg035":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 803
    .local v1, "_arg110":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 804
    invoke-virtual {v8, v0, v1}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->writeFileConfig(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    .line 805
    .local v2, "_result45":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 806
    invoke-virtual {v10, v2}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 807
    goto/16 :goto_1

    .line 793
    .end local v0    # "_arg035":Ljava/lang/String;
    .end local v1    # "_arg110":Ljava/lang/String;
    .end local v2    # "_result45":Z
    :pswitch_2
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 794
    .local v0, "_arg034":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks;

    move-result-object v1

    .line 795
    .local v1, "_arg19":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 796
    invoke-virtual {v8, v0, v1}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->readFileConfigWithCallBack(Ljava/lang/String;Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks;)Z

    move-result v2

    .line 797
    .local v2, "_result44":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 798
    invoke-virtual {v10, v2}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 799
    goto/16 :goto_1

    .line 787
    .end local v0    # "_arg034":Ljava/lang/String;
    .end local v1    # "_arg19":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks;
    .end local v2    # "_result44":Z
    :pswitch_3
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 788
    .local v0, "_arg033":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v1

    .line 789
    .local v1, "_arg18":[B
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 790
    invoke-virtual {v8, v0, v1}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->sendNfcRequest(I[B)V

    .line 791
    goto/16 :goto_1

    .line 783
    .end local v0    # "_arg033":I
    .end local v1    # "_arg18":[B
    :pswitch_4
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->disconnect()V

    .line 784
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 785
    goto/16 :goto_1

    .line 778
    :pswitch_5
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->connect()Z

    move-result v0

    .line 779
    .local v0, "_result43":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 780
    invoke-virtual {v10, v0}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 781
    goto/16 :goto_1

    .line 772
    .end local v0    # "_result43":Z
    :pswitch_6
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmResponses$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmResponses;

    move-result-object v0

    .line 773
    .local v0, "_arg032":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmResponses;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 774
    invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->setNfcResponseFunction(Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmResponses;)V

    .line 775
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 776
    goto/16 :goto_1

    .line 767
    .end local v0    # "_arg032":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmResponses;
    :pswitch_7
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->isNfcSupport()I

    move-result v0

    .line 768
    .local v0, "_result42":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 769
    invoke-virtual {v10, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 770
    goto/16 :goto_1

    .line 759
    .end local v0    # "_result42":I
    :pswitch_8
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v0

    .line 760
    .local v0, "_arg031":[B
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createIntArray()[I

    move-result-object v1

    .line 761
    .local v1, "_arg17":[I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 762
    invoke-virtual {v8, v0, v1}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->writeMnlConfigFile([B[I)Z

    move-result v2

    .line 763
    .local v2, "_result41":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 764
    invoke-virtual {v10, v2}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 765
    goto/16 :goto_1

    .line 752
    .end local v0    # "_arg031":[B
    .end local v1    # "_arg17":[I
    .end local v2    # "_result41":Z
    :pswitch_9
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createIntArray()[I

    move-result-object v0

    .line 753
    .local v0, "_arg030":[I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 754
    invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->readMnlConfigFile([I)[B

    move-result-object v1

    .line 755
    .local v1, "_result40":[B
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 756
    invoke-virtual {v10, v1}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 757
    goto/16 :goto_1

    .line 744
    .end local v0    # "_arg030":[I
    .end local v1    # "_result40":[B
    :pswitch_a
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 745
    .local v0, "_arg029":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks;

    move-result-object v1

    .line 746
    .local v1, "_arg16":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 747
    invoke-virtual {v8, v0, v1}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->getFilePathListWithCallBack(Ljava/lang/String;Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks;)Z

    move-result v2

    .line 748
    .local v2, "_result39":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 749
    invoke-virtual {v10, v2}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 750
    goto/16 :goto_1

    .line 737
    .end local v0    # "_arg029":Ljava/lang/String;
    .end local v1    # "_arg16":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks;
    .end local v2    # "_result39":Z
    :pswitch_b
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 738
    .local v0, "_arg028":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 739
    invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->setMoms(Ljava/lang/String;)Z

    move-result v1

    .line 740
    .local v1, "_result38":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 741
    invoke-virtual {v10, v1}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 742
    goto/16 :goto_1

    .line 730
    .end local v0    # "_arg028":Ljava/lang/String;
    .end local v1    # "_result38":Z
    :pswitch_c
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 731
    .local v0, "_arg027":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 732
    invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->setBypassService(Ljava/lang/String;)Z

    move-result v1

    .line 733
    .local v1, "_result37":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 734
    invoke-virtual {v10, v1}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 735
    goto/16 :goto_1

    .line 723
    .end local v0    # "_arg027":Ljava/lang/String;
    .end local v1    # "_result37":Z
    :pswitch_d
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 724
    .local v0, "_arg026":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 725
    invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->setBypassEn(Ljava/lang/String;)Z

    move-result v1

    .line 726
    .local v1, "_result36":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 727
    invoke-virtual {v10, v1}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 728
    goto/16 :goto_1

    .line 716
    .end local v0    # "_arg026":Ljava/lang/String;
    .end local v1    # "_result36":Z
    :pswitch_e
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 717
    .local v0, "_arg025":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 718
    invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->setBypassDis(Ljava/lang/String;)Z

    move-result v1

    .line 719
    .local v1, "_result35":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 720
    invoke-virtual {v10, v1}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 721
    goto/16 :goto_1

    .line 709
    .end local v0    # "_arg025":Ljava/lang/String;
    .end local v1    # "_result35":Z
    :pswitch_f
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 710
    .local v0, "_arg024":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 711
    invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->setEmUsbValue(Ljava/lang/String;)Z

    move-result v1

    .line 712
    .local v1, "_result34":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 713
    invoke-virtual {v10, v1}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 714
    goto/16 :goto_1

    .line 702
    .end local v0    # "_arg024":Ljava/lang/String;
    .end local v1    # "_result34":Z
    :pswitch_10
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 703
    .local v0, "_arg023":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 704
    invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->setEmUsbType(Ljava/lang/String;)Z

    move-result v1

    .line 705
    .local v1, "_result33":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 706
    invoke-virtual {v10, v1}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 707
    goto/16 :goto_1

    .line 695
    .end local v0    # "_arg023":Ljava/lang/String;
    .end local v1    # "_result33":Z
    :pswitch_11
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 696
    .local v0, "_arg022":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 697
    invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->setUsbOtgSwitch(Ljava/lang/String;)Z

    move-result v1

    .line 698
    .local v1, "_result32":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 699
    invoke-virtual {v10, v1}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 700
    goto/16 :goto_1

    .line 688
    .end local v0    # "_arg022":Ljava/lang/String;
    .end local v1    # "_result32":Z
    :pswitch_12
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 689
    .local v0, "_arg021":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 690
    invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->setUsbPort(Ljava/lang/String;)Z

    move-result v1

    .line 691
    .local v1, "_result31":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 692
    invoke-virtual {v10, v1}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 693
    goto/16 :goto_1

    .line 681
    .end local v0    # "_arg021":Ljava/lang/String;
    .end local v1    # "_result31":Z
    :pswitch_13
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 682
    .local v0, "_arg020":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 683
    invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->setWcnCoreDump(Ljava/lang/String;)Z

    move-result v1

    .line 684
    .local v1, "_result30":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 685
    invoke-virtual {v10, v1}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 686
    goto/16 :goto_1

    .line 674
    .end local v0    # "_arg020":Ljava/lang/String;
    .end local v1    # "_result30":Z
    :pswitch_14
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 675
    .local v0, "_arg019":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 676
    invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->setModemWarningEnable(Ljava/lang/String;)Z

    move-result v1

    .line 677
    .local v1, "_result29":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 678
    invoke-virtual {v10, v1}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 679
    goto/16 :goto_1

    .line 667
    .end local v0    # "_arg019":Ljava/lang/String;
    .end local v1    # "_result29":Z
    :pswitch_15
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 668
    .local v0, "_arg018":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 669
    invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->setMdResetDelay(Ljava/lang/String;)Z

    move-result v1

    .line 670
    .local v1, "_result28":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 671
    invoke-virtual {v10, v1}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 672
    goto/16 :goto_1

    .line 662
    .end local v0    # "_arg018":Ljava/lang/String;
    .end local v1    # "_result28":Z
    :pswitch_16
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->btUninit()I

    move-result v0

    .line 663
    .local v0, "_result27":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 664
    invoke-virtual {v10, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 665
    goto/16 :goto_1

    .line 657
    .end local v0    # "_result27":I
    :pswitch_17
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->btStopRelayer()I

    move-result v0

    .line 658
    .local v0, "_result26":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 659
    invoke-virtual {v10, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 660
    goto/16 :goto_1

    .line 649
    .end local v0    # "_result26":I
    :pswitch_18
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 650
    .local v0, "_arg017":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 651
    .local v1, "_arg15":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 652
    invoke-virtual {v8, v0, v1}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->btStartRelayer(II)I

    move-result v2

    .line 653
    .local v2, "_result25":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 654
    invoke-virtual {v10, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 655
    goto/16 :goto_1

    .line 639
    .end local v0    # "_arg017":I
    .end local v1    # "_arg15":I
    .end local v2    # "_result25":I
    :pswitch_19
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 640
    .local v0, "_arg016":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 641
    .local v1, "_arg14":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 642
    .local v2, "_arg22":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 643
    .local v3, "_arg32":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 644
    invoke-virtual {v8, v0, v1, v2, v3}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->btStartNoSigRxTest(IIII)Z

    move-result v4

    .line 645
    .local v4, "_result24":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 646
    invoke-virtual {v10, v4}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 647
    goto/16 :goto_1

    .line 634
    .end local v0    # "_arg016":I
    .end local v1    # "_arg14":I
    .end local v2    # "_arg22":I
    .end local v3    # "_arg32":I
    .end local v4    # "_result24":Z
    :pswitch_1a
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->btPollingStop()I

    move-result v0

    .line 635
    .local v0, "_result23":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 636
    invoke-virtual {v10, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 637
    goto/16 :goto_1

    .line 629
    .end local v0    # "_result23":I
    :pswitch_1b
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->btPollingStart()I

    move-result v0

    .line 630
    .local v0, "_result22":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 631
    invoke-virtual {v10, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 632
    goto/16 :goto_1

    .line 624
    .end local v0    # "_result22":I
    :pswitch_1c
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->btIsEmSupport()I

    move-result v0

    .line 625
    .local v0, "_result21":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 626
    invoke-virtual {v10, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 627
    goto/16 :goto_1

    .line 619
    .end local v0    # "_result21":I
    :pswitch_1d
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->btIsComboSupport()I

    move-result v0

    .line 620
    .local v0, "_result20":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 621
    invoke-virtual {v10, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 622
    goto/16 :goto_1

    .line 614
    .end local v0    # "_result20":I
    :pswitch_1e
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->btIsBLESupport()I

    move-result v0

    .line 615
    .local v0, "_result19":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 616
    invoke-virtual {v10, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 617
    goto/16 :goto_1

    .line 609
    .end local v0    # "_result19":I
    :pswitch_1f
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->btIsBLEEnhancedSupport()Z

    move-result v0

    .line 610
    .local v0, "_result18":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 611
    invoke-virtual {v10, v0}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 612
    goto/16 :goto_1

    .line 604
    .end local v0    # "_result18":Z
    :pswitch_20
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->btInit()I

    move-result v0

    .line 605
    .local v0, "_result17":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 606
    invoke-virtual {v10, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 607
    goto/16 :goto_1

    .line 597
    .end local v0    # "_result17":I
    :pswitch_21
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v0

    .line 598
    .local v0, "_arg015":[B
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 599
    invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->btHciCommandRun([B)[B

    move-result-object v1

    .line 600
    .local v1, "_result16":[B
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 601
    invoke-virtual {v10, v1}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 602
    goto/16 :goto_1

    .line 592
    .end local v0    # "_arg015":[B
    .end local v1    # "_result16":[B
    :pswitch_22
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->btEndNoSigRxTest()[I

    move-result-object v0

    .line 593
    .local v0, "_result15":[I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 594
    invoke-virtual {v10, v0}, Landroid/os/Parcel;->writeIntArray([I)V

    .line 595
    goto/16 :goto_1

    .line 579
    .end local v0    # "_result15":[I
    :pswitch_23
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v14

    .line 580
    .local v14, "_arg014":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v15

    .line 581
    .local v15, "_arg13":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v16

    .line 582
    .local v16, "_arg2":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v17

    .line 583
    .local v17, "_arg3":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v18

    .line 584
    .local v18, "_arg4":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v19

    .line 585
    .local v19, "_arg5":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v20

    .line 586
    .local v20, "_arg6":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 587
    move-object/from16 v0, p0

    move v1, v14

    move v2, v15

    move/from16 v3, v16

    move/from16 v4, v17

    move/from16 v5, v18

    move/from16 v6, v19

    move/from16 v7, v20

    invoke-virtual/range {v0 .. v7}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->btDoTest(IIIIIII)I

    move-result v0

    .line 588
    .local v0, "_result14":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 589
    invoke-virtual {v10, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 590
    goto/16 :goto_1

    .line 574
    .end local v0    # "_result14":I
    .end local v14    # "_arg014":I
    .end local v15    # "_arg13":I
    .end local v16    # "_arg2":I
    .end local v17    # "_arg3":I
    .end local v18    # "_arg4":I
    .end local v19    # "_arg5":I
    .end local v20    # "_arg6":I
    :pswitch_24
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->isGauge30Support()I

    move-result v0

    .line 575
    .local v0, "_result13":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 576
    invoke-virtual {v10, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 577
    goto/16 :goto_1

    .line 566
    .end local v0    # "_result13":I
    :pswitch_25
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 567
    .local v0, "_arg013":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 568
    .local v1, "_arg12":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 569
    invoke-virtual {v8, v0, v1}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->setEmConfigure(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    .line 570
    .local v2, "_result12":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 571
    invoke-virtual {v10, v2}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 572
    goto/16 :goto_1

    .line 559
    .end local v0    # "_arg013":Ljava/lang/String;
    .end local v1    # "_arg12":Ljava/lang/String;
    .end local v2    # "_result12":Z
    :pswitch_26
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 560
    .local v0, "_arg012":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 561
    invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->setVolteMalPctid(Ljava/lang/String;)Z

    move-result v1

    .line 562
    .local v1, "_result11":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 563
    invoke-virtual {v10, v1}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 564
    goto/16 :goto_1

    .line 552
    .end local v0    # "_arg012":Ljava/lang/String;
    .end local v1    # "_result11":Z
    :pswitch_27
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 553
    .local v0, "_arg011":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 554
    invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->setImsTestMode(Ljava/lang/String;)Z

    move-result v1

    .line 555
    .local v1, "_result10":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 556
    invoke-virtual {v10, v1}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 557
    goto/16 :goto_1

    .line 545
    .end local v0    # "_arg011":Ljava/lang/String;
    .end local v1    # "_result10":Z
    :pswitch_28
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 546
    .local v0, "_arg010":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 547
    invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->setTestSimCardType(Ljava/lang/String;)Z

    move-result v1

    .line 548
    .local v1, "_result9":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 549
    invoke-virtual {v10, v1}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 550
    goto/16 :goto_1

    .line 538
    .end local v0    # "_arg010":Ljava/lang/String;
    .end local v1    # "_result9":Z
    :pswitch_29
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 539
    .local v0, "_arg09":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 540
    invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->setSmsFormat(Ljava/lang/String;)Z

    move-result v1

    .line 541
    .local v1, "_result8":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 542
    invoke-virtual {v10, v1}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 543
    goto/16 :goto_1

    .line 531
    .end local v0    # "_arg09":Ljava/lang/String;
    .end local v1    # "_result8":Z
    :pswitch_2a
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 532
    .local v0, "_arg08":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 533
    invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->setRadioCapabilitySwitchEnable(Ljava/lang/String;)Z

    move-result v1

    .line 534
    .local v1, "_result7":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 535
    invoke-virtual {v10, v1}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 536
    goto/16 :goto_1

    .line 524
    .end local v0    # "_arg08":Ljava/lang/String;
    .end local v1    # "_result7":Z
    :pswitch_2b
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 525
    .local v0, "_arg07":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 526
    invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->setPreferGprsMode(Ljava/lang/String;)Z

    move-result v1

    .line 527
    .local v1, "_result6":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 528
    invoke-virtual {v10, v1}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 529
    goto/16 :goto_1

    .line 517
    .end local v0    # "_arg07":Ljava/lang/String;
    .end local v1    # "_result6":Z
    :pswitch_2c
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 518
    .local v0, "_arg06":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 519
    invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->setDsbpSupport(Ljava/lang/String;)Z

    move-result v1

    .line 520
    .local v1, "_result5":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 521
    invoke-virtual {v10, v1}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 522
    goto :goto_1

    .line 510
    .end local v0    # "_arg06":Ljava/lang/String;
    .end local v1    # "_result5":Z
    :pswitch_2d
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 511
    .local v0, "_arg05":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 512
    invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->setDisableC2kCap(Ljava/lang/String;)Z

    move-result v1

    .line 513
    .local v1, "_result4":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 514
    invoke-virtual {v10, v1}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 515
    goto :goto_1

    .line 503
    .end local v0    # "_arg05":Ljava/lang/String;
    .end local v1    # "_result4":Z
    :pswitch_2e
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 504
    .local v0, "_arg04":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 505
    invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->setCtIREngMode(Ljava/lang/String;)Z

    move-result v1

    .line 506
    .local v1, "_result3":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 507
    invoke-virtual {v10, v1}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 508
    goto :goto_1

    .line 495
    .end local v0    # "_arg04":Ljava/lang/String;
    .end local v1    # "_result3":Z
    :pswitch_2f
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 496
    .local v0, "_arg03":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks;

    move-result-object v1

    .line 497
    .local v1, "_arg1":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 498
    invoke-virtual {v8, v0, v1}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->sendToServerWithCallBack(Ljava/lang/String;Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks;)Z

    move-result v2

    .line 499
    .local v2, "_result2":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 500
    invoke-virtual {v10, v2}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 501
    goto :goto_1

    .line 488
    .end local v0    # "_arg03":Ljava/lang/String;
    .end local v1    # "_arg1":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks;
    .end local v2    # "_result2":Z
    :pswitch_30
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 489
    .local v0, "_arg02":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 490
    invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->sendToServer(Ljava/lang/String;)Z

    move-result v1

    .line 491
    .local v1, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 492
    invoke-virtual {v10, v1}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 493
    goto :goto_1

    .line 482
    .end local v0    # "_arg02":Ljava/lang/String;
    .end local v1    # "_result":Z
    :pswitch_31
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks;

    move-result-object v0

    .line 483
    .local v0, "_arg0":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 484
    invoke-virtual {v8, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds$Stub;->setCallback(Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks;)V

    .line 485
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 486
    nop

    .line 819
    .end local v0    # "_arg0":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks;
    :goto_1
    return v12

    :sswitch_data_0
    .sparse-switch
        0xfffffe -> :sswitch_2
        0xffffff -> :sswitch_1
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_31
        :pswitch_30
        :pswitch_2f
        :pswitch_2e
        :pswitch_2d
        :pswitch_2c
        :pswitch_2b
        :pswitch_2a
        :pswitch_29
        :pswitch_28
        :pswitch_27
        :pswitch_26
        :pswitch_25
        :pswitch_24
        :pswitch_23
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
