class com.android.server.location.hardware.mtk.engineermode.aidl.IEmResponses$Stub$Proxy implements com.android.server.location.hardware.mtk.engineermode.aidl.IEmResponses {
	 /* .source "IEmResponses.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmResponses$Stub; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "Proxy" */
} // .end annotation
/* # instance fields */
private java.lang.String mCachedHash;
private Integer mCachedVersion;
private android.os.IBinder mRemote;
/* # direct methods */
 com.android.server.location.hardware.mtk.engineermode.aidl.IEmResponses$Stub$Proxy ( ) {
/* .locals 1 */
/* .param p1, "remote" # Landroid/os/IBinder; */
/* .line 107 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 104 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmResponses$Stub$Proxy;->mCachedVersion:I */
/* .line 105 */
final String v0 = "-1"; // const-string v0, "-1"
this.mCachedHash = v0;
/* .line 108 */
this.mRemote = p1;
/* .line 109 */
return;
} // .end method
/* # virtual methods */
public android.os.IBinder asBinder ( ) {
/* .locals 1 */
/* .line 113 */
v0 = this.mRemote;
} // .end method
public java.lang.String getInterfaceDescriptor ( ) {
/* .locals 1 */
/* .line 117 */
/* const-string/jumbo v0, "vendor.mediatek.hardware.engineermode.IEmResponses" */
} // .end method
public synchronized java.lang.String getInterfaceHash ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* monitor-enter p0 */
/* .line 155 */
try { // :try_start_0
final String v0 = "-1"; // const-string v0, "-1"
v1 = this.mCachedHash;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 156 */
	 (( com.android.server.location.hardware.mtk.engineermode.aidl.IEmResponses$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmResponses$Stub$Proxy;->asBinder()Landroid/os/IBinder;
	 android.os.Parcel .obtain ( v0 );
	 /* .line 157 */
	 /* .local v0, "data":Landroid/os/Parcel; */
	 android.os.Parcel .obtain ( );
	 /* .line 158 */
	 /* .local v1, "reply":Landroid/os/Parcel; */
	 /* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmResponses" */
	 (( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
	 /* .line 159 */
	 v2 = this.mRemote;
	 /* const v3, 0xfffffe */
	 int v4 = 0; // const/4 v4, 0x0
	 /* .line 160 */
	 (( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
	 /* .line 161 */
	 (( android.os.Parcel ) v1 ).readString ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readString()Ljava/lang/String;
	 this.mCachedHash = v2;
	 /* .line 162 */
	 (( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
	 /* .line 163 */
	 (( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
	 /* .line 165 */
} // .end local v0 # "data":Landroid/os/Parcel;
} // .end local v1 # "reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmResponses$Stub$Proxy;
} // :cond_0
v0 = this.mCachedHash;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* monitor-exit p0 */
/* .line 154 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* throw v0 */
} // .end method
public Integer getInterfaceVersion ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 137 */
/* iget v0, p0, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmResponses$Stub$Proxy;->mCachedVersion:I */
int v1 = -1; // const/4 v1, -0x1
/* if-ne v0, v1, :cond_0 */
/* .line 138 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmResponses$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmResponses$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 139 */
/* .local v0, "data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 141 */
/* .local v1, "reply":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode.IEmResponses" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 142 */
v2 = this.mRemote;
/* const v3, 0xffffff */
int v4 = 0; // const/4 v4, 0x0
/* .line 143 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 144 */
v2 = (( android.os.Parcel ) v1 ).readInt ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
/* iput v2, p0, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmResponses$Stub$Proxy;->mCachedVersion:I */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 146 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 147 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 148 */
/* .line 146 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 147 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 148 */
/* throw v2 */
/* .line 150 */
} // .end local v0 # "data":Landroid/os/Parcel;
} // .end local v1 # "reply":Landroid/os/Parcel;
} // :cond_0
} // :goto_0
/* iget v0, p0, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmResponses$Stub$Proxy;->mCachedVersion:I */
} // .end method
public void response ( Object[] p0 ) {
/* .locals 4 */
/* .param p1, "message" # [B */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 122 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmResponses$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmResponses$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 124 */
/* .local v0, "_data":Landroid/os/Parcel; */
try { // :try_start_0
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode.IEmResponses" */
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 125 */
(( android.os.Parcel ) v0 ).writeByteArray ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeByteArray([B)V
/* .line 126 */
v1 = this.mRemote;
int v2 = 0; // const/4 v2, 0x0
v1 = int v3 = 1; // const/4 v3, 0x1
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 127 */
/* .local v1, "_status":Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 131 */
} // .end local v1 # "_status":Z
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 132 */
/* nop */
/* .line 133 */
return;
/* .line 128 */
/* .restart local v1 # "_status":Z */
} // :cond_0
try { // :try_start_1
/* new-instance v2, Landroid/os/RemoteException; */
final String v3 = "Method response is unimplemented."; // const-string v3, "Method response is unimplemented."
/* invoke-direct {v2, v3}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmResponses$Stub$Proxy;
} // .end local p1 # "message":[B
/* throw v2 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 131 */
} // .end local v1 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmResponses$Stub$Proxy; */
/* .restart local p1 # "message":[B */
/* :catchall_0 */
/* move-exception v1 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 132 */
/* throw v1 */
} // .end method
