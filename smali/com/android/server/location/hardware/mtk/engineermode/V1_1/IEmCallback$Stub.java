public abstract class com.android.server.location.hardware.mtk.engineermode.V1_1.IEmCallback$Stub extends android.os.HwBinder implements com.android.server.location.hardware.mtk.engineermode.V1_1.IEmCallback {
	 /* .source "IEmCallback.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmCallback; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x409 */
/* name = "Stub" */
} // .end annotation
/* # direct methods */
public com.android.server.location.hardware.mtk.engineermode.V1_1.IEmCallback$Stub ( ) {
/* .locals 0 */
/* .line 207 */
/* invoke-direct {p0}, Landroid/os/HwBinder;-><init>()V */
return;
} // .end method
/* # virtual methods */
public android.os.IHwBinder asBinder ( ) {
/* .locals 0 */
/* .line 210 */
} // .end method
public void debug ( android.os.NativeHandle p0, java.util.ArrayList p1 ) {
/* .locals 0 */
/* .param p1, "nativeHandle" # Landroid/os/NativeHandle; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/os/NativeHandle;", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 215 */
/* .local p2, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
return;
} // .end method
public final android.hidl.base.V1_0.DebugInfo getDebugInfo ( ) {
/* .locals 3 */
/* .line 219 */
/* new-instance v0, Landroid/hidl/base/V1_0/DebugInfo; */
/* invoke-direct {v0}, Landroid/hidl/base/V1_0/DebugInfo;-><init>()V */
/* .line 220 */
/* .local v0, "debugInfo":Landroid/hidl/base/V1_0/DebugInfo; */
v1 = android.os.HidlSupport .getPidIfSharable ( );
/* iput v1, v0, Landroid/hidl/base/V1_0/DebugInfo;->pid:I */
/* .line 221 */
/* const-wide/16 v1, 0x0 */
/* iput-wide v1, v0, Landroid/hidl/base/V1_0/DebugInfo;->ptr:J */
/* .line 222 */
int v1 = 0; // const/4 v1, 0x0
/* iput v1, v0, Landroid/hidl/base/V1_0/DebugInfo;->arch:I */
/* .line 223 */
} // .end method
public final java.util.ArrayList getHashChain ( ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "[B>;" */
/* } */
} // .end annotation
/* .line 228 */
/* new-instance v0, Ljava/util/ArrayList; */
/* const/16 v1, 0x20 */
/* new-array v2, v1, [B */
/* fill-array-data v2, :array_0 */
/* new-array v3, v1, [B */
/* fill-array-data v3, :array_1 */
/* new-array v1, v1, [B */
/* fill-array-data v1, :array_2 */
/* filled-new-array {v2, v3, v1}, [[B */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* nop */
/* :array_0 */
/* .array-data 1 */
/* 0xet */
/* 0x53t */
/* 0x1ct */
/* -0x4t */
/* 0x68t */
/* 0x62t */
/* -0x4ft */
/* -0x53t */
/* 0x47t */
/* 0x5ct */
/* -0x51t */
/* 0x78t */
/* 0x40t */
/* 0x54t */
/* 0x1ct */
/* 0x5ct */
/* 0x3at */
/* 0x49t */
/* -0x3et */
/* -0x21t */
/* -0x3ft */
/* 0x78t */
/* 0x40t */
/* 0x9t */
/* -0x3ct */
/* 0x65t */
/* -0x2at */
/* 0x24t */
/* 0x22t */
/* 0x44t */
/* -0x3t */
/* -0x22t */
} // .end array-data
/* :array_1 */
/* .array-data 1 */
/* -0x1bt */
/* 0x70t */
/* 0x24t */
/* 0x71t */
/* -0x52t */
/* 0x14t */
/* -0x4bt */
/* -0x71t */
/* 0x26t */
/* -0x27t */
/* 0x65t */
/* -0x17t */
/* -0x80t */
/* -0x15t */
/* 0x77t */
/* 0x75t */
/* -0xat */
/* 0x28t */
/* -0x6ct */
/* 0x2at */
/* 0x1dt */
/* -0x53t */
/* 0x7t */
/* -0x6dt */
/* 0x64t */
/* 0x65t */
/* -0x5at */
/* 0x64t */
/* -0x37t */
/* 0x49t */
/* 0x5dt */
/* -0x69t */
} // .end array-data
/* :array_2 */
/* .array-data 1 */
/* -0x14t */
/* 0x7ft */
/* -0x29t */
/* -0x62t */
/* -0x30t */
/* 0x2dt */
/* -0x6t */
/* -0x7bt */
/* -0x44t */
/* 0x49t */
/* -0x6ct */
/* 0x26t */
/* -0x53t */
/* -0x52t */
/* 0x3et */
/* -0x42t */
/* 0x23t */
/* -0x11t */
/* 0x5t */
/* 0x24t */
/* -0xdt */
/* -0x33t */
/* 0x69t */
/* 0x57t */
/* 0x13t */
/* -0x6dt */
/* 0x24t */
/* -0x48t */
/* 0x3bt */
/* 0x18t */
/* -0x36t */
/* 0x4ct */
} // .end array-data
} // .end method
public final java.util.ArrayList interfaceChain ( ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 233 */
/* new-instance v0, Ljava/util/ArrayList; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmCallback" */
final String v2 = "android.hidl.base@1.0::IBase"; // const-string v2, "android.hidl.base@1.0::IBase"
/* const-string/jumbo v3, "vendor.mediatek.hardware.engineermode@1.1::IEmCallback" */
/* filled-new-array {v3, v1, v2}, [Ljava/lang/String; */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
} // .end method
public final java.lang.String interfaceDescriptor ( ) {
/* .locals 1 */
/* .line 238 */
/* const-string/jumbo v0, "vendor.mediatek.hardware.engineermode@1.1::IEmCallback" */
} // .end method
public final Boolean linkToDeath ( android.os.IHwBinder$DeathRecipient p0, Long p1 ) {
/* .locals 1 */
/* .param p1, "deathRecipient" # Landroid/os/IHwBinder$DeathRecipient; */
/* .param p2, "j" # J */
/* .line 243 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
public final void notifySyspropsChanged ( ) {
/* .locals 0 */
/* .line 248 */
android.os.HwBinder .enableInstrumentation ( );
/* .line 249 */
return;
} // .end method
public void onTransact ( Integer p0, android.os.HwParcel p1, android.os.HwParcel p2, Integer p3 ) {
/* .locals 10 */
/* .param p1, "i" # I */
/* .param p2, "hwParcel" # Landroid/os/HwParcel; */
/* .param p3, "hwParcel2" # Landroid/os/HwParcel; */
/* .param p4, "i2" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 252 */
final String v0 = "android.hidl.base@1.0::IBase"; // const-string v0, "android.hidl.base@1.0::IBase"
int v1 = 0; // const/4 v1, 0x0
/* sparse-switch p1, :sswitch_data_0 */
/* .line 323 */
return;
/* .line 319 */
/* :sswitch_0 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 320 */
(( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmCallback$Stub ) p0 ).notifySyspropsChanged ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmCallback$Stub;->notifySyspropsChanged()V
/* .line 321 */
return;
/* .line 312 */
/* :sswitch_1 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 313 */
(( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmCallback$Stub ) p0 ).getDebugInfo ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmCallback$Stub;->getDebugInfo()Landroid/hidl/base/V1_0/DebugInfo;
/* .line 314 */
/* .local v0, "debugInfo":Landroid/hidl/base/V1_0/DebugInfo; */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 315 */
(( android.hidl.base.V1_0.DebugInfo ) v0 ).writeToParcel ( p3 ); // invoke-virtual {v0, p3}, Landroid/hidl/base/V1_0/DebugInfo;->writeToParcel(Landroid/os/HwParcel;)V
/* .line 316 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 317 */
return;
/* .line 306 */
} // .end local v0 # "debugInfo":Landroid/hidl/base/V1_0/DebugInfo;
/* :sswitch_2 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 307 */
(( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmCallback$Stub ) p0 ).ping ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmCallback$Stub;->ping()V
/* .line 308 */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 309 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 310 */
return;
/* .line 302 */
/* :sswitch_3 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 303 */
(( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmCallback$Stub ) p0 ).setHALInstrumentation ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmCallback$Stub;->setHALInstrumentation()V
/* .line 304 */
return;
/* .line 281 */
/* :sswitch_4 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 282 */
(( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmCallback$Stub ) p0 ).getHashChain ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmCallback$Stub;->getHashChain()Ljava/util/ArrayList;
/* .line 283 */
/* .local v0, "hashChain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;" */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 284 */
/* new-instance v2, Landroid/os/HwBlob; */
/* const/16 v3, 0x10 */
/* invoke-direct {v2, v3}, Landroid/os/HwBlob;-><init>(I)V */
/* .line 285 */
/* .local v2, "hwBlob":Landroid/os/HwBlob; */
v3 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* .line 286 */
/* .local v3, "size":I */
/* const-wide/16 v4, 0x8 */
(( android.os.HwBlob ) v2 ).putInt32 ( v4, v5, v3 ); // invoke-virtual {v2, v4, v5, v3}, Landroid/os/HwBlob;->putInt32(JI)V
/* .line 287 */
/* const-wide/16 v4, 0xc */
(( android.os.HwBlob ) v2 ).putBool ( v4, v5, v1 ); // invoke-virtual {v2, v4, v5, v1}, Landroid/os/HwBlob;->putBool(JZ)V
/* .line 288 */
/* new-instance v1, Landroid/os/HwBlob; */
/* mul-int/lit8 v4, v3, 0x20 */
/* invoke-direct {v1, v4}, Landroid/os/HwBlob;-><init>(I)V */
/* .line 289 */
/* .local v1, "hwBlob2":Landroid/os/HwBlob; */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i3":I */
} // :goto_0
/* if-ge v4, v3, :cond_1 */
/* .line 290 */
/* mul-int/lit8 v5, v4, 0x20 */
/* int-to-long v5, v5 */
/* .line 291 */
/* .local v5, "j":J */
(( java.util.ArrayList ) v0 ).get ( v4 ); // invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v7, [B */
/* .line 292 */
/* .local v7, "bArr":[B */
if ( v7 != null) { // if-eqz v7, :cond_0
/* array-length v8, v7 */
/* const/16 v9, 0x20 */
/* if-ne v8, v9, :cond_0 */
/* .line 295 */
(( android.os.HwBlob ) v1 ).putInt8Array ( v5, v6, v7 ); // invoke-virtual {v1, v5, v6, v7}, Landroid/os/HwBlob;->putInt8Array(J[B)V
/* .line 289 */
} // .end local v5 # "j":J
} // .end local v7 # "bArr":[B
/* add-int/lit8 v4, v4, 0x1 */
/* .line 293 */
/* .restart local v5 # "j":J */
/* .restart local v7 # "bArr":[B */
} // :cond_0
/* new-instance v8, Ljava/lang/IllegalArgumentException; */
final String v9 = "Array element is not of the expected length"; // const-string v9, "Array element is not of the expected length"
/* invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v8 */
/* .line 297 */
} // .end local v4 # "i3":I
} // .end local v5 # "j":J
} // .end local v7 # "bArr":[B
} // :cond_1
/* const-wide/16 v4, 0x0 */
(( android.os.HwBlob ) v2 ).putBlob ( v4, v5, v1 ); // invoke-virtual {v2, v4, v5, v1}, Landroid/os/HwBlob;->putBlob(JLandroid/os/HwBlob;)V
/* .line 298 */
(( android.os.HwParcel ) p3 ).writeBuffer ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeBuffer(Landroid/os/HwBlob;)V
/* .line 299 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 300 */
return;
/* .line 274 */
} // .end local v0 # "hashChain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
} // .end local v1 # "hwBlob2":Landroid/os/HwBlob;
} // .end local v2 # "hwBlob":Landroid/os/HwBlob;
} // .end local v3 # "size":I
/* :sswitch_5 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 275 */
(( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmCallback$Stub ) p0 ).interfaceDescriptor ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmCallback$Stub;->interfaceDescriptor()Ljava/lang/String;
/* .line 276 */
/* .local v0, "interfaceDescriptor":Ljava/lang/String; */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 277 */
(( android.os.HwParcel ) p3 ).writeString ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 278 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 279 */
return;
/* .line 268 */
} // .end local v0 # "interfaceDescriptor":Ljava/lang/String;
/* :sswitch_6 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 269 */
(( android.os.HwParcel ) p2 ).readNativeHandle ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readNativeHandle()Landroid/os/NativeHandle;
(( android.os.HwParcel ) p2 ).readStringVector ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readStringVector()Ljava/util/ArrayList;
(( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmCallback$Stub ) p0 ).debug ( v0, v2 ); // invoke-virtual {p0, v0, v2}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmCallback$Stub;->debug(Landroid/os/NativeHandle;Ljava/util/ArrayList;)V
/* .line 270 */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 271 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 272 */
return;
/* .line 261 */
/* :sswitch_7 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 262 */
(( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmCallback$Stub ) p0 ).interfaceChain ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmCallback$Stub;->interfaceChain()Ljava/util/ArrayList;
/* .line 263 */
/* .local v0, "interfaceChain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 264 */
(( android.os.HwParcel ) p3 ).writeStringVector ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeStringVector(Ljava/util/ArrayList;)V
/* .line 265 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 266 */
return;
/* .line 254 */
} // .end local v0 # "interfaceChain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
/* :sswitch_8 */
/* const-string/jumbo v0, "vendor.mediatek.hardware.engineermode@1.0::IEmCallback" */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 255 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmCallback$Stub ) p0 ).callbackToClient ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmCallback$Stub;->callbackToClient(Ljava/lang/String;)Z
/* .line 256 */
/* .local v0, "callbackToClient":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 257 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 258 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 259 */
return;
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x1 -> :sswitch_8 */
/* 0xf43484e -> :sswitch_7 */
/* 0xf444247 -> :sswitch_6 */
/* 0xf445343 -> :sswitch_5 */
/* 0xf485348 -> :sswitch_4 */
/* 0xf494e54 -> :sswitch_3 */
/* 0xf504e47 -> :sswitch_2 */
/* 0xf524546 -> :sswitch_1 */
/* 0xf535953 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
public final void ping ( ) {
/* .locals 0 */
/* .line 329 */
return;
} // .end method
public android.os.IHwInterface queryLocalInterface ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "str" # Ljava/lang/String; */
/* .line 332 */
/* const-string/jumbo v0, "vendor.mediatek.hardware.engineermode@1.1::IEmCallback" */
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 333 */
/* .line 335 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void registerAsService ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "str" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 339 */
(( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmCallback$Stub ) p0 ).registerService ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmCallback$Stub;->registerService(Ljava/lang/String;)V
/* .line 340 */
return;
} // .end method
public final void setHALInstrumentation ( ) {
/* .locals 0 */
/* .line 344 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 347 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmCallback$Stub ) p0 ).interfaceDescriptor ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmCallback$Stub;->interfaceDescriptor()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "@Stub"; // const-string v1, "@Stub"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
public final Boolean unlinkToDeath ( android.os.IHwBinder$DeathRecipient p0 ) {
/* .locals 1 */
/* .param p1, "deathRecipient" # Landroid/os/IHwBinder$DeathRecipient; */
/* .line 352 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
