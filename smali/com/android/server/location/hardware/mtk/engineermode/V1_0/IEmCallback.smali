.class public interface abstract Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback;
.super Ljava/lang/Object;
.source "IEmCallback.java"

# interfaces
.implements Landroid/hidl/base/V1_0/IBase;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback$Proxy;,
        Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback$Stub;
    }
.end annotation


# direct methods
.method public static asInterface(Landroid/os/IHwBinder;)Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback;
    .locals 6
    .param p0, "iHwBinder"    # Landroid/os/IHwBinder;

    .line 355
    const/4 v0, 0x0

    if-nez p0, :cond_0

    .line 356
    return-object v0

    .line 358
    :cond_0
    const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmCallback"

    invoke-interface {p0, v1}, Landroid/os/IHwBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IHwInterface;

    move-result-object v1

    check-cast v1, Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback;

    .line 359
    .local v1, "queryLocalInterface":Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback;
    if-eqz v1, :cond_1

    instance-of v2, v1, Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback;

    if-eqz v2, :cond_1

    .line 360
    return-object v1

    .line 362
    :cond_1
    new-instance v2, Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback$Proxy;

    invoke-direct {v2, p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback$Proxy;-><init>(Landroid/os/IHwBinder;)V

    .line 364
    .local v2, "proxy":Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback$Proxy;
    :try_start_0
    invoke-virtual {v2}, Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback$Proxy;->interfaceChain()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 365
    .local v3, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 366
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const-string v5, "android.hidl.base@1.0::IBase"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v4, :cond_2

    .line 367
    return-object v2

    .line 372
    .end local v3    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_3
    goto :goto_0

    .line 370
    :catch_0
    move-exception v3

    .line 371
    .local v3, "e":Landroid/os/RemoteException;
    invoke-virtual {v3}, Landroid/os/RemoteException;->printStackTrace()V

    .line 373
    .end local v3    # "e":Landroid/os/RemoteException;
    :goto_0
    return-object v0
.end method

.method public static castFrom(Landroid/os/IHwInterface;)Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback;
    .locals 1
    .param p0, "iHwInterface"    # Landroid/os/IHwInterface;

    .line 377
    if-nez p0, :cond_0

    .line 378
    const/4 v0, 0x0

    return-object v0

    .line 380
    :cond_0
    invoke-interface {p0}, Landroid/os/IHwInterface;->asBinder()Landroid/os/IHwBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback;->asInterface(Landroid/os/IHwBinder;)Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback;

    move-result-object v0

    return-object v0
.end method

.method public static getService()Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 384
    const-string v0, "default"

    invoke-static {v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback;->getService(Ljava/lang/String;)Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback;

    move-result-object v0

    return-object v0
.end method

.method public static getService(Ljava/lang/String;)Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback;
    .locals 1
    .param p0, "str"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 388
    const-string/jumbo v0, "vendor.mediatek.hardware.engineermode@1.0::IEmCallback"

    invoke-static {v0, p0}, Landroid/os/HwBinder;->getService(Ljava/lang/String;Ljava/lang/String;)Landroid/os/IHwBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback;->asInterface(Landroid/os/IHwBinder;)Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback;

    move-result-object v0

    return-object v0
.end method

.method public static getService(Ljava/lang/String;Z)Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback;
    .locals 1
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "z"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 392
    const-string/jumbo v0, "vendor.mediatek.hardware.engineermode@1.0::IEmCallback"

    invoke-static {v0, p0, p1}, Landroid/os/HwBinder;->getService(Ljava/lang/String;Ljava/lang/String;Z)Landroid/os/IHwBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback;->asInterface(Landroid/os/IHwBinder;)Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback;

    move-result-object v0

    return-object v0
.end method

.method public static getService(Z)Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback;
    .locals 1
    .param p0, "z"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 396
    const-string v0, "default"

    invoke-static {v0, p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback;->getService(Ljava/lang/String;Z)Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public abstract asBinder()Landroid/os/IHwBinder;
.end method

.method public abstract callbackToClient(Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract debug(Landroid/os/NativeHandle;Ljava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/NativeHandle;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getDebugInfo()Landroid/hidl/base/V1_0/DebugInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getHashChain()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "[B>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract interfaceChain()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract interfaceDescriptor()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract linkToDeath(Landroid/os/IHwBinder$DeathRecipient;J)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract notifySyspropsChanged()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract ping()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract setHALInstrumentation()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract unlinkToDeath(Landroid/os/IHwBinder$DeathRecipient;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
