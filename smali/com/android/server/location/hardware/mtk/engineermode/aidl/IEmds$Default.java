public class com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Default implements com.android.server.location.hardware.mtk.engineermode.aidl.IEmds {
	 /* .source "IEmds.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmds; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "Default" */
} // .end annotation
/* # direct methods */
public com.android.server.location.hardware.mtk.engineermode.aidl.IEmds$Default ( ) {
/* .locals 0 */
/* .line 122 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public android.os.IBinder asBinder ( ) {
/* .locals 1 */
/* .line 381 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Integer btDoTest ( Integer p0, Integer p1, Integer p2, Integer p3, Integer p4, Integer p5, Integer p6 ) {
/* .locals 1 */
/* .param p1, "kind" # I */
/* .param p2, "pattern" # I */
/* .param p3, "channel" # I */
/* .param p4, "pocketType" # I */
/* .param p5, "pocketTypeLen" # I */
/* .param p6, "freq" # I */
/* .param p7, "power" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 194 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public btEndNoSigRxTest ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 199 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public btHciCommandRun ( Object[] p0 ) {
/* .locals 1 */
/* .param p1, "input" # [B */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 204 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Integer btInit ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 209 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean btIsBLEEnhancedSupport ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 214 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Integer btIsBLESupport ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 219 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Integer btIsComboSupport ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 224 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Integer btIsEmSupport ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 229 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Integer btPollingStart ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 234 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Integer btPollingStop ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 239 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean btStartNoSigRxTest ( Integer p0, Integer p1, Integer p2, Integer p3 ) {
/* .locals 1 */
/* .param p1, "pattern" # I */
/* .param p2, "pockettype" # I */
/* .param p3, "freq" # I */
/* .param p4, "address" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 244 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Integer btStartRelayer ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "port" # I */
/* .param p2, "speed" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 249 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Integer btStopRelayer ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 254 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Integer btUninit ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 259 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean connect ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 343 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void disconnect ( ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 348 */
return;
} // .end method
public Boolean getFilePathListWithCallBack ( java.lang.String p0, com.android.server.location.hardware.mtk.engineermode.aidl.IEmCallbacks p1 ) {
/* .locals 1 */
/* .param p1, "data" # Ljava/lang/String; */
/* .param p2, "callback" # Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 319 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public java.lang.String getInterfaceHash ( ) {
/* .locals 1 */
/* .line 376 */
final String v0 = ""; // const-string v0, ""
} // .end method
public Integer getInterfaceVersion ( ) {
/* .locals 1 */
/* .line 371 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Integer isGauge30Support ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 189 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Integer isNfcSupport ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 334 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean readFileConfigWithCallBack ( java.lang.String p0, com.android.server.location.hardware.mtk.engineermode.aidl.IEmCallbacks p1 ) {
/* .locals 1 */
/* .param p1, "data" # Ljava/lang/String; */
/* .param p2, "callback" # Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 356 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public readMnlConfigFile ( Integer[] p0 ) {
/* .locals 1 */
/* .param p1, "tag" # [I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 324 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void sendNfcRequest ( Integer p0, Object[] p1 ) {
/* .locals 0 */
/* .param p1, "ins" # I */
/* .param p2, "detail" # [B */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 352 */
return;
} // .end method
public Boolean sendToServer ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "data" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 129 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean sendToServerWithCallBack ( java.lang.String p0, com.android.server.location.hardware.mtk.engineermode.aidl.IEmCallbacks p1 ) {
/* .locals 1 */
/* .param p1, "data" # Ljava/lang/String; */
/* .param p2, "callback" # Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 134 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean setBypassDis ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "data" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 299 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean setBypassEn ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "data" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 304 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean setBypassService ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "data" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 309 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void setCallback ( com.android.server.location.hardware.mtk.engineermode.aidl.IEmCallbacks p0 ) {
/* .locals 0 */
/* .param p1, "callback" # Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmCallbacks; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 125 */
return;
} // .end method
public Boolean setCtIREngMode ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "data" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 139 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean setDisableC2kCap ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "data" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 144 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean setDsbpSupport ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "data" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 149 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean setEmConfigure ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "name" # Ljava/lang/String; */
/* .param p2, "value" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 184 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean setEmUsbType ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "data" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 289 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean setEmUsbValue ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "data" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 294 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean setImsTestMode ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "data" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 174 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean setMdResetDelay ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "data" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 264 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean setModemWarningEnable ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "data" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 269 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean setMoms ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "data" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 314 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void setNfcResponseFunction ( com.android.server.location.hardware.mtk.engineermode.aidl.IEmResponses p0 ) {
/* .locals 0 */
/* .param p1, "response" # Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmResponses; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 339 */
return;
} // .end method
public Boolean setPreferGprsMode ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "data" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 154 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean setRadioCapabilitySwitchEnable ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "data" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 159 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean setSmsFormat ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "data" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 164 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean setTestSimCardType ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "data" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 169 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean setUsbOtgSwitch ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "data" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 284 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean setUsbPort ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "data" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 279 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean setVolteMalPctid ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "data" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 179 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean setWcnCoreDump ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "data" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 274 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean writeFileConfig ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "data" # Ljava/lang/String; */
/* .param p2, "value" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 361 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean writeMnlConfigFile ( Object[] p0, Integer[] p1 ) {
/* .locals 1 */
/* .param p1, "content" # [B */
/* .param p2, "tag" # [I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 329 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean writeOtaFiles ( java.lang.String p0, Object[] p1 ) {
/* .locals 1 */
/* .param p1, "destFile" # Ljava/lang/String; */
/* .param p2, "content" # [B */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 366 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
