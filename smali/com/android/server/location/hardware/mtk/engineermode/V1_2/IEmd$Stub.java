public abstract class com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub extends android.os.HwBinder implements com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd {
	 /* .source "IEmd.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x409 */
/* name = "Stub" */
} // .end annotation
/* # direct methods */
public com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ( ) {
/* .locals 0 */
/* .line 977 */
/* invoke-direct {p0}, Landroid/os/HwBinder;-><init>()V */
return;
} // .end method
/* # virtual methods */
public android.os.IHwBinder asBinder ( ) {
/* .locals 0 */
/* .line 980 */
} // .end method
public void debug ( android.os.NativeHandle p0, java.util.ArrayList p1 ) {
/* .locals 0 */
/* .param p1, "nativeHandle" # Landroid/os/NativeHandle; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/os/NativeHandle;", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 985 */
/* .local p2, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
return;
} // .end method
public final android.hidl.base.V1_0.DebugInfo getDebugInfo ( ) {
/* .locals 3 */
/* .line 989 */
/* new-instance v0, Landroid/hidl/base/V1_0/DebugInfo; */
/* invoke-direct {v0}, Landroid/hidl/base/V1_0/DebugInfo;-><init>()V */
/* .line 990 */
/* .local v0, "debugInfo":Landroid/hidl/base/V1_0/DebugInfo; */
v1 = android.os.HidlSupport .getPidIfSharable ( );
/* iput v1, v0, Landroid/hidl/base/V1_0/DebugInfo;->pid:I */
/* .line 991 */
/* const-wide/16 v1, 0x0 */
/* iput-wide v1, v0, Landroid/hidl/base/V1_0/DebugInfo;->ptr:J */
/* .line 992 */
int v1 = 0; // const/4 v1, 0x0
/* iput v1, v0, Landroid/hidl/base/V1_0/DebugInfo;->arch:I */
/* .line 993 */
} // .end method
public final java.util.ArrayList getHashChain ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "[B>;" */
/* } */
} // .end annotation
/* .line 998 */
/* new-instance v0, Ljava/util/ArrayList; */
/* const/16 v1, 0x20 */
/* new-array v2, v1, [B */
/* fill-array-data v2, :array_0 */
/* new-array v3, v1, [B */
/* fill-array-data v3, :array_1 */
/* new-array v4, v1, [B */
/* fill-array-data v4, :array_2 */
/* new-array v1, v1, [B */
/* fill-array-data v1, :array_3 */
/* filled-new-array {v2, v3, v4, v1}, [[B */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* :array_0 */
/* .array-data 1 */
/* -0x1bt */
/* 0x15t */
/* -0x2bt */
/* -0x5at */
/* 0x51t */
/* 0x37t */
/* -0x7at */
/* 0xft */
/* -0x26t */
/* 0x29t */
/* 0x52t */
/* 0x59t */
/* 0x13t */
/* 0x16t */
/* -0xbt */
/* -0x26t */
/* 0x74t */
/* -0x15t */
/* -0x48t */
/* 0x6bt */
/* 0x6ct */
/* -0x14t */
/* 0x52t */
/* 0x11t */
/* -0x28t */
/* -0x75t */
/* 0x1dt */
/* 0x71t */
/* -0x20t */
/* -0x26t */
/* 0x37t */
/* -0x1at */
} // .end array-data
/* :array_1 */
/* .array-data 1 */
/* 0x63t */
/* -0x5dt */
/* 0x23t */
/* -0x55t */
/* -0x5t */
/* -0x19t */
/* 0x30t */
/* 0xdt */
/* -0x70t */
/* -0x7ft */
/* 0x66t */
/* -0x1at */
/* 0x47t */
/* 0x70t */
/* 0x20t */
/* -0x2bt */
/* 0x49t */
/* 0x3at */
/* 0x78t */
/* -0x11t */
/* -0x76t */
/* -0x54t */
/* 0x7bt */
/* -0xet */
/* -0x1bt */
/* -0x2at */
/* -0x60t */
/* 0x43t */
/* -0x41t */
/* -0x45t */
/* -0x5ct */
/* -0x71t */
} // .end array-data
/* :array_2 */
/* .array-data 1 */
/* -0x42t */
/* -0x1bt */
/* 0x12t */
/* -0x6bt */
/* 0x79t */
/* -0x73t */
/* -0x5t */
/* -0x71t */
/* -0xbt */
/* 0x1ct */
/* 0x77t */
/* -0x69t */
/* 0x79t */
/* -0x64t */
/* 0x69t */
/* -0x1dt */
/* 0x56t */
/* 0x69t */
/* -0x4ft */
/* 0x20t */
/* -0x9t */
/* 0x19t */
/* 0x36t */
/* -0x75t */
/* -0x5dt */
/* -0x61t */
/* -0x1dt */
/* -0x1et */
/* -0xat */
/* -0x30t */
/* 0x4ft */
/* -0x64t */
} // .end array-data
/* :array_3 */
/* .array-data 1 */
/* -0x14t */
/* 0x7ft */
/* -0x29t */
/* -0x62t */
/* -0x30t */
/* 0x2dt */
/* -0x6t */
/* -0x7bt */
/* -0x44t */
/* 0x49t */
/* -0x6ct */
/* 0x26t */
/* -0x53t */
/* -0x52t */
/* 0x3et */
/* -0x42t */
/* 0x23t */
/* -0x11t */
/* 0x5t */
/* 0x24t */
/* -0xdt */
/* -0x33t */
/* 0x69t */
/* 0x57t */
/* 0x13t */
/* -0x6dt */
/* 0x24t */
/* -0x48t */
/* 0x3bt */
/* 0x18t */
/* -0x36t */
/* 0x4ct */
} // .end array-data
} // .end method
public final java.util.ArrayList interfaceChain ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1003 */
/* new-instance v0, Ljava/util/ArrayList; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
final String v2 = "android.hidl.base@1.0::IBase"; // const-string v2, "android.hidl.base@1.0::IBase"
/* const-string/jumbo v3, "vendor.mediatek.hardware.engineermode@1.2::IEmd" */
/* const-string/jumbo v4, "vendor.mediatek.hardware.engineermode@1.1::IEmd" */
/* filled-new-array {v3, v4, v1, v2}, [Ljava/lang/String; */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
} // .end method
public final java.lang.String interfaceDescriptor ( ) {
/* .locals 1 */
/* .line 1008 */
/* const-string/jumbo v0, "vendor.mediatek.hardware.engineermode@1.2::IEmd" */
} // .end method
public final Boolean linkToDeath ( android.os.IHwBinder$DeathRecipient p0, Long p1 ) {
/* .locals 1 */
/* .param p1, "deathRecipient" # Landroid/os/IHwBinder$DeathRecipient; */
/* .param p2, "j" # J */
/* .line 1013 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
public final void notifySyspropsChanged ( ) {
/* .locals 0 */
/* .line 1018 */
android.os.HwBinder .enableInstrumentation ( );
/* .line 1019 */
return;
} // .end method
public void onTransact ( Integer p0, android.os.HwParcel p1, android.os.HwParcel p2, Integer p3 ) {
/* .locals 10 */
/* .param p1, "i" # I */
/* .param p2, "hwParcel" # Landroid/os/HwParcel; */
/* .param p3, "hwParcel2" # Landroid/os/HwParcel; */
/* .param p4, "i2" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1022 */
/* const-string/jumbo v0, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
int v1 = 0; // const/4 v1, 0x0
/* packed-switch p1, :pswitch_data_0 */
/* .line 1199 */
/* const-string/jumbo v2, "vendor.mediatek.hardware.engineermode@1.2::IEmd" */
/* packed-switch p1, :pswitch_data_1 */
/* .line 1369 */
final String v0 = "android.hidl.base@1.0::IBase"; // const-string v0, "android.hidl.base@1.0::IBase"
/* sparse-switch p1, :sswitch_data_0 */
/* .line 1433 */
return;
/* .line 1191 */
/* :pswitch_0 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1192 */
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).btPollingStop ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->btPollingStop()I
/* .line 1193 */
/* .local v0, "btPollingStop":I */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1194 */
(( android.os.HwParcel ) p3 ).writeInt32 ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 1195 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1196 */
return;
/* .line 1184 */
} // .end local v0 # "btPollingStop":I
/* :pswitch_1 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1185 */
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).btPollingStart ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->btPollingStart()I
/* .line 1186 */
/* .local v0, "btPollingStart":I */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1187 */
(( android.os.HwParcel ) p3 ).writeInt32 ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 1188 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1189 */
return;
/* .line 1177 */
} // .end local v0 # "btPollingStart":I
/* :pswitch_2 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1178 */
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).btIsComboSupport ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->btIsComboSupport()I
/* .line 1179 */
/* .local v0, "btIsComboSupport":I */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1180 */
(( android.os.HwParcel ) p3 ).writeInt32 ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 1181 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1182 */
return;
/* .line 1170 */
} // .end local v0 # "btIsComboSupport":I
/* :pswitch_3 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1171 */
(( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).btEndNoSigRxTest ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->btEndNoSigRxTest()Ljava/util/ArrayList;
/* .line 1172 */
/* .local v0, "btEndNoSigRxTest":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1173 */
(( android.os.HwParcel ) p3 ).writeInt32Vector ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32Vector(Ljava/util/ArrayList;)V
/* .line 1174 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1175 */
return;
/* .line 1163 */
} // .end local v0 # "btEndNoSigRxTest":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
/* :pswitch_4 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1164 */
v0 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
v2 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
v3 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
v4 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).btStartNoSigRxTest ( v0, v2, v3, v4 ); // invoke-virtual {p0, v0, v2, v3, v4}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->btStartNoSigRxTest(IIII)Z
/* .line 1165 */
/* .local v0, "btStartNoSigRxTest":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1166 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1167 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1168 */
return;
/* .line 1156 */
} // .end local v0 # "btStartNoSigRxTest":Z
/* :pswitch_5 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1157 */
(( android.os.HwParcel ) p2 ).readInt8Vector ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt8Vector()Ljava/util/ArrayList;
(( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).btHciCommandRun ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->btHciCommandRun(Ljava/util/ArrayList;)Ljava/util/ArrayList;
/* .line 1158 */
/* .local v0, "btHciCommandRun":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;" */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1159 */
(( android.os.HwParcel ) p3 ).writeInt8Vector ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt8Vector(Ljava/util/ArrayList;)V
/* .line 1160 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1161 */
return;
/* .line 1149 */
} // .end local v0 # "btHciCommandRun":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
/* :pswitch_6 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1150 */
v3 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
v4 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
v5 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
v6 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
v7 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
v8 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
v9 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
/* move-object v2, p0 */
v0 = /* invoke-virtual/range {v2 ..v9}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->btDoTest(IIIIIII)I */
/* .line 1151 */
/* .local v0, "btDoTest":I */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1152 */
(( android.os.HwParcel ) p3 ).writeInt32 ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 1153 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1154 */
return;
/* .line 1142 */
} // .end local v0 # "btDoTest":I
/* :pswitch_7 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1143 */
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).btUninit ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->btUninit()I
/* .line 1144 */
/* .local v0, "btUninit":I */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1145 */
(( android.os.HwParcel ) p3 ).writeInt32 ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 1146 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1147 */
return;
/* .line 1135 */
} // .end local v0 # "btUninit":I
/* :pswitch_8 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1136 */
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).btInit ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->btInit()I
/* .line 1137 */
/* .local v0, "btInit":I */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1138 */
(( android.os.HwParcel ) p3 ).writeInt32 ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 1139 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1140 */
return;
/* .line 1128 */
} // .end local v0 # "btInit":I
/* :pswitch_9 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1129 */
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).btIsBLEEnhancedSupport ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->btIsBLEEnhancedSupport()Z
/* .line 1130 */
/* .local v0, "btIsBLEEnhancedSupport":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1131 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1132 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1133 */
return;
/* .line 1121 */
} // .end local v0 # "btIsBLEEnhancedSupport":Z
/* :pswitch_a */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1122 */
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).btIsBLESupport ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->btIsBLESupport()I
/* .line 1123 */
/* .local v0, "btIsBLESupport":I */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1124 */
(( android.os.HwParcel ) p3 ).writeInt32 ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 1125 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1126 */
return;
/* .line 1114 */
} // .end local v0 # "btIsBLESupport":I
/* :pswitch_b */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1115 */
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).btStopRelayer ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->btStopRelayer()I
/* .line 1116 */
/* .local v0, "btStopRelayer":I */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1117 */
(( android.os.HwParcel ) p3 ).writeInt32 ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 1118 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1119 */
return;
/* .line 1107 */
} // .end local v0 # "btStopRelayer":I
/* :pswitch_c */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1108 */
v0 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
v2 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).btStartRelayer ( v0, v2 ); // invoke-virtual {p0, v0, v2}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->btStartRelayer(II)I
/* .line 1109 */
/* .local v0, "btStartRelayer":I */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1110 */
(( android.os.HwParcel ) p3 ).writeInt32 ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 1111 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1112 */
return;
/* .line 1100 */
} // .end local v0 # "btStartRelayer":I
/* :pswitch_d */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1101 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).setVolteMalPctid ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->setVolteMalPctid(Ljava/lang/String;)Z
/* .line 1102 */
/* .local v0, "volteMalPctid":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1103 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1104 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1105 */
return;
/* .line 1093 */
} // .end local v0 # "volteMalPctid":Z
/* :pswitch_e */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1094 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).setDsbpSupport ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->setDsbpSupport(Ljava/lang/String;)Z
/* .line 1095 */
/* .local v0, "dsbpSupport":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1096 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1097 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1098 */
return;
/* .line 1086 */
} // .end local v0 # "dsbpSupport":Z
/* :pswitch_f */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1087 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).setImsTestMode ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->setImsTestMode(Ljava/lang/String;)Z
/* .line 1088 */
/* .local v0, "imsTestMode":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1089 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1090 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1091 */
return;
/* .line 1079 */
} // .end local v0 # "imsTestMode":Z
/* :pswitch_10 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1080 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).setDisableC2kCap ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->setDisableC2kCap(Ljava/lang/String;)Z
/* .line 1081 */
/* .local v0, "disableC2kCap":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1082 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1083 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1084 */
return;
/* .line 1072 */
} // .end local v0 # "disableC2kCap":Z
/* :pswitch_11 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1073 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).setRadioCapabilitySwitchEnable ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->setRadioCapabilitySwitchEnable(Ljava/lang/String;)Z
/* .line 1074 */
/* .local v0, "radioCapabilitySwitchEnable":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1075 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1076 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1077 */
return;
/* .line 1065 */
} // .end local v0 # "radioCapabilitySwitchEnable":Z
/* :pswitch_12 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1066 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).setPreferGprsMode ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->setPreferGprsMode(Ljava/lang/String;)Z
/* .line 1067 */
/* .local v0, "preferGprsMode":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1068 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1069 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1070 */
return;
/* .line 1058 */
} // .end local v0 # "preferGprsMode":Z
/* :pswitch_13 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1059 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).setTestSimCardType ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->setTestSimCardType(Ljava/lang/String;)Z
/* .line 1060 */
/* .local v0, "testSimCardType":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1061 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1062 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1063 */
return;
/* .line 1051 */
} // .end local v0 # "testSimCardType":Z
/* :pswitch_14 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1052 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).setCtIREngMode ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->setCtIREngMode(Ljava/lang/String;)Z
/* .line 1053 */
/* .local v0, "ctIREngMode":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1054 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1055 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1056 */
return;
/* .line 1044 */
} // .end local v0 # "ctIREngMode":Z
/* :pswitch_15 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1045 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).setSmsFormat ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->setSmsFormat(Ljava/lang/String;)Z
/* .line 1046 */
/* .local v0, "smsFormat":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1047 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1048 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1049 */
return;
/* .line 1037 */
} // .end local v0 # "smsFormat":Z
/* :pswitch_16 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1038 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
(( android.os.HwParcel ) p2 ).readStrongBinder ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readStrongBinder()Landroid/os/IHwBinder;
com.android.server.location.hardware.mtk.engineermode.V1_0.IEmCallback .asInterface ( v2 );
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).sendToServerWithCallBack ( v0, v2 ); // invoke-virtual {p0, v0, v2}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->sendToServerWithCallBack(Ljava/lang/String;Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback;)Z
/* .line 1039 */
/* .local v0, "sendToServerWithCallBack":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1040 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1041 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1042 */
return;
/* .line 1030 */
} // .end local v0 # "sendToServerWithCallBack":Z
/* :pswitch_17 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1031 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).sendToServer ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->sendToServer(Ljava/lang/String;)Z
/* .line 1032 */
/* .local v0, "sendToServer":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1033 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1034 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1035 */
return;
/* .line 1024 */
} // .end local v0 # "sendToServer":Z
/* :pswitch_18 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1025 */
(( android.os.HwParcel ) p2 ).readStrongBinder ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readStrongBinder()Landroid/os/IHwBinder;
com.android.server.location.hardware.mtk.engineermode.V1_0.IEmCallback .asInterface ( v0 );
(( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).setCallback ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->setCallback(Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback;)V
/* .line 1026 */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1027 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1028 */
return;
/* .line 1362 */
/* :pswitch_19 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1363 */
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).isGauge30Support ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->isGauge30Support()I
/* .line 1364 */
/* .local v0, "isGauge30Support":I */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1365 */
(( android.os.HwParcel ) p3 ).writeInt32 ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 1366 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1367 */
return;
/* .line 1355 */
} // .end local v0 # "isGauge30Support":I
/* :pswitch_1a */
(( android.os.HwParcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1356 */
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).isNfcSupport ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->isNfcSupport()I
/* .line 1357 */
/* .local v0, "isNfcSupport":I */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1358 */
(( android.os.HwParcel ) p3 ).writeInt32 ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 1359 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1360 */
return;
/* .line 1348 */
} // .end local v0 # "isNfcSupport":I
/* :pswitch_1b */
(( android.os.HwParcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1349 */
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).btIsEmSupport ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->btIsEmSupport()I
/* .line 1350 */
/* .local v0, "btIsEmSupport":I */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1351 */
(( android.os.HwParcel ) p3 ).writeInt32 ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 1352 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1353 */
return;
/* .line 1341 */
} // .end local v0 # "btIsEmSupport":I
/* :pswitch_1c */
/* const-string/jumbo v0, "vendor.mediatek.hardware.engineermode@1.1::IEmd" */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1342 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).setEmConfigure ( v0, v2 ); // invoke-virtual {p0, v0, v2}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->setEmConfigure(Ljava/lang/String;Ljava/lang/String;)Z
/* .line 1343 */
/* .local v0, "emConfigure":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1344 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1345 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1346 */
return;
/* .line 1334 */
} // .end local v0 # "emConfigure":Z
/* :pswitch_1d */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1335 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
(( android.os.HwParcel ) p2 ).readStrongBinder ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readStrongBinder()Landroid/os/IHwBinder;
com.android.server.location.hardware.mtk.engineermode.V1_0.IEmCallback .asInterface ( v2 );
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).getFilePathListWithCallBack ( v0, v2 ); // invoke-virtual {p0, v0, v2}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->getFilePathListWithCallBack(Ljava/lang/String;Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback;)Z
/* .line 1336 */
/* .local v0, "filePathListWithCallBack":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1337 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1338 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1339 */
return;
/* .line 1327 */
} // .end local v0 # "filePathListWithCallBack":Z
/* :pswitch_1e */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1328 */
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).clearItemsforRsc ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->clearItemsforRsc()Z
/* .line 1329 */
/* .local v0, "clearItemsforRsc":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1330 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1331 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1332 */
return;
/* .line 1320 */
} // .end local v0 # "clearItemsforRsc":Z
/* :pswitch_1f */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1321 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).setMoms ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->setMoms(Ljava/lang/String;)Z
/* .line 1322 */
/* .local v0, "moms":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1323 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1324 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1325 */
return;
/* .line 1313 */
} // .end local v0 # "moms":Z
/* :pswitch_20 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1314 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).setBypassService ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->setBypassService(Ljava/lang/String;)Z
/* .line 1315 */
/* .local v0, "bypassService":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1316 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1317 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1318 */
return;
/* .line 1306 */
} // .end local v0 # "bypassService":Z
/* :pswitch_21 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1307 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).setBypassDis ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->setBypassDis(Ljava/lang/String;)Z
/* .line 1308 */
/* .local v0, "bypassDis":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1309 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1310 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1311 */
return;
/* .line 1299 */
} // .end local v0 # "bypassDis":Z
/* :pswitch_22 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1300 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).setBypassEn ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->setBypassEn(Ljava/lang/String;)Z
/* .line 1301 */
/* .local v0, "bypassEn":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1302 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1303 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1304 */
return;
/* .line 1292 */
} // .end local v0 # "bypassEn":Z
/* :pswitch_23 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1293 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).setEmUsbType ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->setEmUsbType(Ljava/lang/String;)Z
/* .line 1294 */
/* .local v0, "emUsbType":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1295 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1296 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1297 */
return;
/* .line 1285 */
} // .end local v0 # "emUsbType":Z
/* :pswitch_24 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1286 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).setEmUsbValue ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->setEmUsbValue(Ljava/lang/String;)Z
/* .line 1287 */
/* .local v0, "emUsbValue":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1288 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1289 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1290 */
return;
/* .line 1278 */
} // .end local v0 # "emUsbValue":Z
/* :pswitch_25 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1279 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).setUsbOtgSwitch ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->setUsbOtgSwitch(Ljava/lang/String;)Z
/* .line 1280 */
/* .local v0, "usbOtgSwitch":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1281 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1282 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1283 */
return;
/* .line 1271 */
} // .end local v0 # "usbOtgSwitch":Z
/* :pswitch_26 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1272 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).setUsbPort ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->setUsbPort(Ljava/lang/String;)Z
/* .line 1273 */
/* .local v0, "usbPort":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1274 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1275 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1276 */
return;
/* .line 1264 */
} // .end local v0 # "usbPort":Z
/* :pswitch_27 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1265 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).genMdLogFilter ( v0, v2 ); // invoke-virtual {p0, v0, v2}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->genMdLogFilter(Ljava/lang/String;Ljava/lang/String;)Z
/* .line 1266 */
/* .local v0, "genMdLogFilter":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1267 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1268 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1269 */
return;
/* .line 1257 */
} // .end local v0 # "genMdLogFilter":Z
/* :pswitch_28 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1258 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).setModemWarningEnable ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->setModemWarningEnable(Ljava/lang/String;)Z
/* .line 1259 */
/* .local v0, "modemWarningEnable":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1260 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1261 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1262 */
return;
/* .line 1250 */
} // .end local v0 # "modemWarningEnable":Z
/* :pswitch_29 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1251 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).setVencDriverLogEnable ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->setVencDriverLogEnable(Ljava/lang/String;)Z
/* .line 1252 */
/* .local v0, "vencDriverLogEnable":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1253 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1254 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1255 */
return;
/* .line 1243 */
} // .end local v0 # "vencDriverLogEnable":Z
/* :pswitch_2a */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1244 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).setOmxCoreLogEnable ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->setOmxCoreLogEnable(Ljava/lang/String;)Z
/* .line 1245 */
/* .local v0, "omxCoreLogEnable":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1246 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1247 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1248 */
return;
/* .line 1236 */
} // .end local v0 # "omxCoreLogEnable":Z
/* :pswitch_2b */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1237 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).setSvpLogEnable ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->setSvpLogEnable(Ljava/lang/String;)Z
/* .line 1238 */
/* .local v0, "svpLogEnable":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1239 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1240 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1241 */
return;
/* .line 1229 */
} // .end local v0 # "svpLogEnable":Z
/* :pswitch_2c */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1230 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).setVdecDriverLogEnable ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->setVdecDriverLogEnable(Ljava/lang/String;)Z
/* .line 1231 */
/* .local v0, "vdecDriverLogEnable":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1232 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1233 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1234 */
return;
/* .line 1222 */
} // .end local v0 # "vdecDriverLogEnable":Z
/* :pswitch_2d */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1223 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).setOmxVdecLogEnable ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->setOmxVdecLogEnable(Ljava/lang/String;)Z
/* .line 1224 */
/* .local v0, "omxVdecLogEnable":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1225 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1226 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1227 */
return;
/* .line 1215 */
} // .end local v0 # "omxVdecLogEnable":Z
/* :pswitch_2e */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1216 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).setOmxVencLogEnable ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->setOmxVencLogEnable(Ljava/lang/String;)Z
/* .line 1217 */
/* .local v0, "omxVencLogEnable":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1218 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1219 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1220 */
return;
/* .line 1208 */
} // .end local v0 # "omxVencLogEnable":Z
/* :pswitch_2f */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1209 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).setWcnCoreDump ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->setWcnCoreDump(Ljava/lang/String;)Z
/* .line 1210 */
/* .local v0, "wcnCoreDump":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1211 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1212 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1213 */
return;
/* .line 1201 */
} // .end local v0 # "wcnCoreDump":Z
/* :pswitch_30 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1202 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).setMdResetDelay ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->setMdResetDelay(Ljava/lang/String;)Z
/* .line 1203 */
/* .local v0, "mdResetDelay":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1204 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1205 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1206 */
return;
/* .line 1429 */
} // .end local v0 # "mdResetDelay":Z
/* :sswitch_0 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1430 */
(( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).notifySyspropsChanged ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->notifySyspropsChanged()V
/* .line 1431 */
return;
/* .line 1422 */
/* :sswitch_1 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1423 */
(( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).getDebugInfo ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->getDebugInfo()Landroid/hidl/base/V1_0/DebugInfo;
/* .line 1424 */
/* .local v0, "debugInfo":Landroid/hidl/base/V1_0/DebugInfo; */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1425 */
(( android.hidl.base.V1_0.DebugInfo ) v0 ).writeToParcel ( p3 ); // invoke-virtual {v0, p3}, Landroid/hidl/base/V1_0/DebugInfo;->writeToParcel(Landroid/os/HwParcel;)V
/* .line 1426 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1427 */
return;
/* .line 1416 */
} // .end local v0 # "debugInfo":Landroid/hidl/base/V1_0/DebugInfo;
/* :sswitch_2 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1417 */
(( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).ping ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->ping()V
/* .line 1418 */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1419 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1420 */
return;
/* .line 1412 */
/* :sswitch_3 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1413 */
(( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).setHALInstrumentation ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->setHALInstrumentation()V
/* .line 1414 */
return;
/* .line 1391 */
/* :sswitch_4 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1392 */
(( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).getHashChain ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->getHashChain()Ljava/util/ArrayList;
/* .line 1393 */
/* .local v0, "hashChain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;" */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1394 */
/* new-instance v2, Landroid/os/HwBlob; */
/* const/16 v3, 0x10 */
/* invoke-direct {v2, v3}, Landroid/os/HwBlob;-><init>(I)V */
/* .line 1395 */
/* .local v2, "hwBlob":Landroid/os/HwBlob; */
v3 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* .line 1396 */
/* .local v3, "size":I */
/* const-wide/16 v4, 0x8 */
(( android.os.HwBlob ) v2 ).putInt32 ( v4, v5, v3 ); // invoke-virtual {v2, v4, v5, v3}, Landroid/os/HwBlob;->putInt32(JI)V
/* .line 1397 */
/* const-wide/16 v4, 0xc */
(( android.os.HwBlob ) v2 ).putBool ( v4, v5, v1 ); // invoke-virtual {v2, v4, v5, v1}, Landroid/os/HwBlob;->putBool(JZ)V
/* .line 1398 */
/* new-instance v1, Landroid/os/HwBlob; */
/* mul-int/lit8 v4, v3, 0x20 */
/* invoke-direct {v1, v4}, Landroid/os/HwBlob;-><init>(I)V */
/* .line 1399 */
/* .local v1, "hwBlob2":Landroid/os/HwBlob; */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i3":I */
} // :goto_0
/* if-ge v4, v3, :cond_1 */
/* .line 1400 */
/* mul-int/lit8 v5, v4, 0x20 */
/* int-to-long v5, v5 */
/* .line 1401 */
/* .local v5, "j":J */
(( java.util.ArrayList ) v0 ).get ( v4 ); // invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v7, [B */
/* .line 1402 */
/* .local v7, "bArr":[B */
if ( v7 != null) { // if-eqz v7, :cond_0
/* array-length v8, v7 */
/* const/16 v9, 0x20 */
/* if-ne v8, v9, :cond_0 */
/* .line 1405 */
(( android.os.HwBlob ) v1 ).putInt8Array ( v5, v6, v7 ); // invoke-virtual {v1, v5, v6, v7}, Landroid/os/HwBlob;->putInt8Array(J[B)V
/* .line 1399 */
} // .end local v5 # "j":J
} // .end local v7 # "bArr":[B
/* add-int/lit8 v4, v4, 0x1 */
/* .line 1403 */
/* .restart local v5 # "j":J */
/* .restart local v7 # "bArr":[B */
} // :cond_0
/* new-instance v8, Ljava/lang/IllegalArgumentException; */
final String v9 = "Array element is not of the expected length"; // const-string v9, "Array element is not of the expected length"
/* invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v8 */
/* .line 1407 */
} // .end local v4 # "i3":I
} // .end local v5 # "j":J
} // .end local v7 # "bArr":[B
} // :cond_1
/* const-wide/16 v4, 0x0 */
(( android.os.HwBlob ) v2 ).putBlob ( v4, v5, v1 ); // invoke-virtual {v2, v4, v5, v1}, Landroid/os/HwBlob;->putBlob(JLandroid/os/HwBlob;)V
/* .line 1408 */
(( android.os.HwParcel ) p3 ).writeBuffer ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeBuffer(Landroid/os/HwBlob;)V
/* .line 1409 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1410 */
return;
/* .line 1384 */
} // .end local v0 # "hashChain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
} // .end local v1 # "hwBlob2":Landroid/os/HwBlob;
} // .end local v2 # "hwBlob":Landroid/os/HwBlob;
} // .end local v3 # "size":I
/* :sswitch_5 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1385 */
(( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).interfaceDescriptor ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->interfaceDescriptor()Ljava/lang/String;
/* .line 1386 */
/* .local v0, "interfaceDescriptor":Ljava/lang/String; */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1387 */
(( android.os.HwParcel ) p3 ).writeString ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 1388 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1389 */
return;
/* .line 1378 */
} // .end local v0 # "interfaceDescriptor":Ljava/lang/String;
/* :sswitch_6 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1379 */
(( android.os.HwParcel ) p2 ).readNativeHandle ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readNativeHandle()Landroid/os/NativeHandle;
(( android.os.HwParcel ) p2 ).readStringVector ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readStringVector()Ljava/util/ArrayList;
(( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).debug ( v0, v2 ); // invoke-virtual {p0, v0, v2}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->debug(Landroid/os/NativeHandle;Ljava/util/ArrayList;)V
/* .line 1380 */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1381 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1382 */
return;
/* .line 1371 */
/* :sswitch_7 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1372 */
(( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).interfaceChain ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->interfaceChain()Ljava/util/ArrayList;
/* .line 1373 */
/* .local v0, "interfaceChain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1374 */
(( android.os.HwParcel ) p3 ).writeStringVector ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeStringVector(Ljava/util/ArrayList;)V
/* .line 1375 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1376 */
return;
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_18 */
/* :pswitch_17 */
/* :pswitch_16 */
/* :pswitch_15 */
/* :pswitch_14 */
/* :pswitch_13 */
/* :pswitch_12 */
/* :pswitch_11 */
/* :pswitch_10 */
/* :pswitch_f */
/* :pswitch_e */
/* :pswitch_d */
/* :pswitch_c */
/* :pswitch_b */
/* :pswitch_a */
/* :pswitch_9 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
/* :pswitch_data_1 */
/* .packed-switch 0x1a */
/* :pswitch_30 */
/* :pswitch_2f */
/* :pswitch_2e */
/* :pswitch_2d */
/* :pswitch_2c */
/* :pswitch_2b */
/* :pswitch_2a */
/* :pswitch_29 */
/* :pswitch_28 */
/* :pswitch_27 */
/* :pswitch_26 */
/* :pswitch_25 */
/* :pswitch_24 */
/* :pswitch_23 */
/* :pswitch_22 */
/* :pswitch_21 */
/* :pswitch_20 */
/* :pswitch_1f */
/* :pswitch_1e */
/* :pswitch_1d */
/* :pswitch_1c */
/* :pswitch_1b */
/* :pswitch_1a */
/* :pswitch_19 */
} // .end packed-switch
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0xf43484e -> :sswitch_7 */
/* 0xf444247 -> :sswitch_6 */
/* 0xf445343 -> :sswitch_5 */
/* 0xf485348 -> :sswitch_4 */
/* 0xf494e54 -> :sswitch_3 */
/* 0xf504e47 -> :sswitch_2 */
/* 0xf524546 -> :sswitch_1 */
/* 0xf535953 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
public final void ping ( ) {
/* .locals 0 */
/* .line 1440 */
return;
} // .end method
public android.os.IHwInterface queryLocalInterface ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "str" # Ljava/lang/String; */
/* .line 1443 */
/* const-string/jumbo v0, "vendor.mediatek.hardware.engineermode@1.2::IEmd" */
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1444 */
/* .line 1446 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void registerAsService ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "str" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1450 */
(( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).registerService ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->registerService(Ljava/lang/String;)V
/* .line 1451 */
return;
} // .end method
public final void setHALInstrumentation ( ) {
/* .locals 0 */
/* .line 1455 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 1458 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Stub ) p0 ).interfaceDescriptor ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub;->interfaceDescriptor()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "@Stub"; // const-string v1, "@Stub"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
public final Boolean unlinkToDeath ( android.os.IHwBinder$DeathRecipient p0 ) {
/* .locals 1 */
/* .param p1, "deathRecipient" # Landroid/os/IHwBinder$DeathRecipient; */
/* .line 1463 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
