.class public abstract Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;
.super Landroid/os/HwBinder;
.source "IEmd.java"

# interfaces
.implements Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1069
    invoke-direct {p0}, Landroid/os/HwBinder;-><init>()V

    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IHwBinder;
    .locals 0

    .line 1072
    return-object p0
.end method

.method public debug(Landroid/os/NativeHandle;Ljava/util/ArrayList;)V
    .locals 0
    .param p1, "nativeHandle"    # Landroid/os/NativeHandle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/NativeHandle;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1077
    .local p2, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    return-void
.end method

.method public final getDebugInfo()Landroid/hidl/base/V1_0/DebugInfo;
    .locals 3

    .line 1081
    new-instance v0, Landroid/hidl/base/V1_0/DebugInfo;

    invoke-direct {v0}, Landroid/hidl/base/V1_0/DebugInfo;-><init>()V

    .line 1082
    .local v0, "debugInfo":Landroid/hidl/base/V1_0/DebugInfo;
    invoke-static {}, Landroid/os/HidlSupport;->getPidIfSharable()I

    move-result v1

    iput v1, v0, Landroid/hidl/base/V1_0/DebugInfo;->pid:I

    .line 1083
    const-wide/16 v1, 0x0

    iput-wide v1, v0, Landroid/hidl/base/V1_0/DebugInfo;->ptr:J

    .line 1084
    const/4 v1, 0x0

    iput v1, v0, Landroid/hidl/base/V1_0/DebugInfo;->arch:I

    .line 1085
    return-object v0
.end method

.method public final getHashChain()Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "[B>;"
        }
    .end annotation

    .line 1090
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x20

    new-array v2, v1, [B

    fill-array-data v2, :array_0

    new-array v3, v1, [B

    fill-array-data v3, :array_1

    new-array v4, v1, [B

    fill-array-data v4, :array_2

    new-array v5, v1, [B

    fill-array-data v5, :array_3

    new-array v1, v1, [B

    fill-array-data v1, :array_4

    filled-new-array {v2, v3, v4, v5, v1}, [[B

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0

    nop

    :array_0
    .array-data 1
        -0x16t
        0xct
        0x58t
        0x34t
        -0x74t
        -0x65t
        -0x23t
        -0x4ct
        -0x40t
        0x52t
        -0x6bt
        0x7dt
        -0x3dt
        0x54t
        -0x20t
        0x77t
        -0x45t
        -0x24t
        -0x2t
        -0x38t
        0x6et
        0x6bt
        -0x72t
        -0x75t
        -0x7t
        -0x22t
        0x3ct
        -0x24t
        0x3ft
        -0x3ct
        0xet
        -0x6dt
    .end array-data

    :array_1
    .array-data 1
        -0x1bt
        0x15t
        -0x2bt
        -0x5at
        0x51t
        0x37t
        -0x7at
        0xft
        -0x26t
        0x29t
        0x52t
        0x59t
        0x13t
        0x16t
        -0xbt
        -0x26t
        0x74t
        -0x15t
        -0x48t
        0x6bt
        0x6ct
        -0x14t
        0x52t
        0x11t
        -0x28t
        -0x75t
        0x1dt
        0x71t
        -0x20t
        -0x26t
        0x37t
        -0x1at
    .end array-data

    :array_2
    .array-data 1
        0x63t
        -0x5dt
        0x23t
        -0x55t
        -0x5t
        -0x19t
        0x30t
        0xdt
        -0x70t
        -0x7ft
        0x66t
        -0x1at
        0x47t
        0x70t
        0x20t
        -0x2bt
        0x49t
        0x3at
        0x78t
        -0x11t
        -0x76t
        -0x54t
        0x7bt
        -0xet
        -0x1bt
        -0x2at
        -0x60t
        0x43t
        -0x41t
        -0x45t
        -0x5ct
        -0x71t
    .end array-data

    :array_3
    .array-data 1
        -0x42t
        -0x1bt
        0x12t
        -0x6bt
        0x79t
        -0x73t
        -0x5t
        -0x71t
        -0xbt
        0x1ct
        0x77t
        -0x69t
        0x79t
        -0x64t
        0x69t
        -0x1dt
        0x56t
        0x69t
        -0x4ft
        0x20t
        -0x9t
        0x19t
        0x36t
        -0x75t
        -0x5dt
        -0x61t
        -0x1dt
        -0x1et
        -0xat
        -0x30t
        0x4ft
        -0x64t
    .end array-data

    :array_4
    .array-data 1
        -0x14t
        0x7ft
        -0x29t
        -0x62t
        -0x30t
        0x2dt
        -0x6t
        -0x7bt
        -0x44t
        0x49t
        -0x6ct
        0x26t
        -0x53t
        -0x52t
        0x3et
        -0x42t
        0x23t
        -0x11t
        0x5t
        0x24t
        -0xdt
        -0x33t
        0x69t
        0x57t
        0x13t
        -0x6dt
        0x24t
        -0x48t
        0x3bt
        0x18t
        -0x36t
        0x4ct
    .end array-data
.end method

.method public final interfaceChain()Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1095
    new-instance v0, Ljava/util/ArrayList;

    const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd"

    const-string v2, "android.hidl.base@1.0::IBase"

    const-string/jumbo v3, "vendor.mediatek.hardware.engineermode@1.3::IEmd"

    const-string/jumbo v4, "vendor.mediatek.hardware.engineermode@1.2::IEmd"

    const-string/jumbo v5, "vendor.mediatek.hardware.engineermode@1.1::IEmd"

    filled-new-array {v3, v4, v5, v1, v2}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public final interfaceDescriptor()Ljava/lang/String;
    .locals 1

    .line 1100
    const-string/jumbo v0, "vendor.mediatek.hardware.engineermode@1.3::IEmd"

    return-object v0
.end method

.method public final linkToDeath(Landroid/os/IHwBinder$DeathRecipient;J)Z
    .locals 1
    .param p1, "deathRecipient"    # Landroid/os/IHwBinder$DeathRecipient;
    .param p2, "j"    # J

    .line 1105
    const/4 v0, 0x1

    return v0
.end method

.method public final notifySyspropsChanged()V
    .locals 0

    .line 1110
    invoke-static {}, Landroid/os/HwBinder;->enableInstrumentation()V

    .line 1111
    return-void
.end method

.method public onTransact(ILandroid/os/HwParcel;Landroid/os/HwParcel;I)V
    .locals 10
    .param p1, "i"    # I
    .param p2, "hwParcel"    # Landroid/os/HwParcel;
    .param p3, "hwParcel2"    # Landroid/os/HwParcel;
    .param p4, "i2"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 1114
    const-string/jumbo v0, "vendor.mediatek.hardware.engineermode@1.0::IEmd"

    const/4 v1, 0x0

    packed-switch p1, :pswitch_data_0

    .line 1319
    const-string/jumbo v2, "vendor.mediatek.hardware.engineermode@1.2::IEmd"

    const-string/jumbo v3, "vendor.mediatek.hardware.engineermode@1.3::IEmd"

    packed-switch p1, :pswitch_data_1

    .line 1498
    const-string v0, "android.hidl.base@1.0::IBase"

    sparse-switch p1, :sswitch_data_0

    .line 1562
    return-void

    .line 1311
    :pswitch_0
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1312
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->setOmxVdecLogEnable(Ljava/lang/String;)Z

    move-result v0

    .line 1313
    .local v0, "omxVdecLogEnable":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1314
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1315
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1316
    return-void

    .line 1304
    .end local v0    # "omxVdecLogEnable":Z
    :pswitch_1
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1305
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->setOmxVencLogEnable(Ljava/lang/String;)Z

    move-result v0

    .line 1306
    .local v0, "omxVencLogEnable":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1307
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1308
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1309
    return-void

    .line 1297
    .end local v0    # "omxVencLogEnable":Z
    :pswitch_2
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1298
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->setWcnCoreDump(Ljava/lang/String;)Z

    move-result v0

    .line 1299
    .local v0, "wcnCoreDump":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1300
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1301
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1302
    return-void

    .line 1290
    .end local v0    # "wcnCoreDump":Z
    :pswitch_3
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1291
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->setMdResetDelay(Ljava/lang/String;)Z

    move-result v0

    .line 1292
    .local v0, "mdResetDelay":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1293
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1294
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1295
    return-void

    .line 1283
    .end local v0    # "mdResetDelay":Z
    :pswitch_4
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1284
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->btPollingStop()I

    move-result v0

    .line 1285
    .local v0, "btPollingStop":I
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1286
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V

    .line 1287
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1288
    return-void

    .line 1276
    .end local v0    # "btPollingStop":I
    :pswitch_5
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1277
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->btPollingStart()I

    move-result v0

    .line 1278
    .local v0, "btPollingStart":I
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1279
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V

    .line 1280
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1281
    return-void

    .line 1269
    .end local v0    # "btPollingStart":I
    :pswitch_6
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1270
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->btIsComboSupport()I

    move-result v0

    .line 1271
    .local v0, "btIsComboSupport":I
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1272
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V

    .line 1273
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1274
    return-void

    .line 1262
    .end local v0    # "btIsComboSupport":I
    :pswitch_7
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1263
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->btEndNoSigRxTest()Ljava/util/ArrayList;

    move-result-object v0

    .line 1264
    .local v0, "btEndNoSigRxTest":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1265
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32Vector(Ljava/util/ArrayList;)V

    .line 1266
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1267
    return-void

    .line 1255
    .end local v0    # "btEndNoSigRxTest":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :pswitch_8
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1256
    invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I

    move-result v0

    invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I

    move-result v2

    invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I

    move-result v3

    invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I

    move-result v4

    invoke-virtual {p0, v0, v2, v3, v4}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->btStartNoSigRxTest(IIII)Z

    move-result v0

    .line 1257
    .local v0, "btStartNoSigRxTest":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1258
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1259
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1260
    return-void

    .line 1248
    .end local v0    # "btStartNoSigRxTest":Z
    :pswitch_9
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1249
    invoke-virtual {p2}, Landroid/os/HwParcel;->readInt8Vector()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->btHciCommandRun(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1250
    .local v0, "btHciCommandRun":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1251
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt8Vector(Ljava/util/ArrayList;)V

    .line 1252
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1253
    return-void

    .line 1241
    .end local v0    # "btHciCommandRun":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
    :pswitch_a
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1242
    invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I

    move-result v3

    invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I

    move-result v4

    invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I

    move-result v5

    invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I

    move-result v6

    invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I

    move-result v7

    invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I

    move-result v8

    invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I

    move-result v9

    move-object v2, p0

    invoke-virtual/range {v2 .. v9}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->btDoTest(IIIIIII)I

    move-result v0

    .line 1243
    .local v0, "btDoTest":I
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1244
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V

    .line 1245
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1246
    return-void

    .line 1234
    .end local v0    # "btDoTest":I
    :pswitch_b
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1235
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->btUninit()I

    move-result v0

    .line 1236
    .local v0, "btUninit":I
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1237
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V

    .line 1238
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1239
    return-void

    .line 1227
    .end local v0    # "btUninit":I
    :pswitch_c
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1228
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->btInit()I

    move-result v0

    .line 1229
    .local v0, "btInit":I
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1230
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V

    .line 1231
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1232
    return-void

    .line 1220
    .end local v0    # "btInit":I
    :pswitch_d
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1221
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->btIsBLEEnhancedSupport()Z

    move-result v0

    .line 1222
    .local v0, "btIsBLEEnhancedSupport":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1223
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1224
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1225
    return-void

    .line 1213
    .end local v0    # "btIsBLEEnhancedSupport":Z
    :pswitch_e
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1214
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->btIsBLESupport()I

    move-result v0

    .line 1215
    .local v0, "btIsBLESupport":I
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1216
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V

    .line 1217
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1218
    return-void

    .line 1206
    .end local v0    # "btIsBLESupport":I
    :pswitch_f
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1207
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->btStopRelayer()I

    move-result v0

    .line 1208
    .local v0, "btStopRelayer":I
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1209
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V

    .line 1210
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1211
    return-void

    .line 1199
    .end local v0    # "btStopRelayer":I
    :pswitch_10
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1200
    invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I

    move-result v0

    invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I

    move-result v2

    invoke-virtual {p0, v0, v2}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->btStartRelayer(II)I

    move-result v0

    .line 1201
    .local v0, "btStartRelayer":I
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1202
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V

    .line 1203
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1204
    return-void

    .line 1192
    .end local v0    # "btStartRelayer":I
    :pswitch_11
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1193
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->setVolteMalPctid(Ljava/lang/String;)Z

    move-result v0

    .line 1194
    .local v0, "volteMalPctid":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1195
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1196
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1197
    return-void

    .line 1185
    .end local v0    # "volteMalPctid":Z
    :pswitch_12
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1186
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->setDsbpSupport(Ljava/lang/String;)Z

    move-result v0

    .line 1187
    .local v0, "dsbpSupport":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1188
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1189
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1190
    return-void

    .line 1178
    .end local v0    # "dsbpSupport":Z
    :pswitch_13
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1179
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->setImsTestMode(Ljava/lang/String;)Z

    move-result v0

    .line 1180
    .local v0, "imsTestMode":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1181
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1182
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1183
    return-void

    .line 1171
    .end local v0    # "imsTestMode":Z
    :pswitch_14
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1172
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->setDisableC2kCap(Ljava/lang/String;)Z

    move-result v0

    .line 1173
    .local v0, "disableC2kCap":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1174
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1175
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1176
    return-void

    .line 1164
    .end local v0    # "disableC2kCap":Z
    :pswitch_15
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1165
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->setRadioCapabilitySwitchEnable(Ljava/lang/String;)Z

    move-result v0

    .line 1166
    .local v0, "radioCapabilitySwitchEnable":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1167
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1168
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1169
    return-void

    .line 1157
    .end local v0    # "radioCapabilitySwitchEnable":Z
    :pswitch_16
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1158
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->setPreferGprsMode(Ljava/lang/String;)Z

    move-result v0

    .line 1159
    .local v0, "preferGprsMode":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1160
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1161
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1162
    return-void

    .line 1150
    .end local v0    # "preferGprsMode":Z
    :pswitch_17
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1151
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->setTestSimCardType(Ljava/lang/String;)Z

    move-result v0

    .line 1152
    .local v0, "testSimCardType":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1153
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1154
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1155
    return-void

    .line 1143
    .end local v0    # "testSimCardType":Z
    :pswitch_18
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1144
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->setCtIREngMode(Ljava/lang/String;)Z

    move-result v0

    .line 1145
    .local v0, "ctIREngMode":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1146
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1147
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1148
    return-void

    .line 1136
    .end local v0    # "ctIREngMode":Z
    :pswitch_19
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1137
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->setSmsFormat(Ljava/lang/String;)Z

    move-result v0

    .line 1138
    .local v0, "smsFormat":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1139
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1140
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1141
    return-void

    .line 1129
    .end local v0    # "smsFormat":Z
    :pswitch_1a
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1130
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Landroid/os/HwParcel;->readStrongBinder()Landroid/os/IHwBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback;->asInterface(Landroid/os/IHwBinder;)Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->sendToServerWithCallBack(Ljava/lang/String;Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback;)Z

    move-result v0

    .line 1131
    .local v0, "sendToServerWithCallBack":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1132
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1133
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1134
    return-void

    .line 1122
    .end local v0    # "sendToServerWithCallBack":Z
    :pswitch_1b
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1123
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->sendToServer(Ljava/lang/String;)Z

    move-result v0

    .line 1124
    .local v0, "sendToServer":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1125
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1126
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1127
    return-void

    .line 1116
    .end local v0    # "sendToServer":Z
    :pswitch_1c
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1117
    invoke-virtual {p2}, Landroid/os/HwParcel;->readStrongBinder()Landroid/os/IHwBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback;->asInterface(Landroid/os/IHwBinder;)Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->setCallback(Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback;)V

    .line 1118
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1119
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1120
    return-void

    .line 1494
    :pswitch_1d
    invoke-virtual {p2, v3}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1495
    invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I

    move-result v0

    invoke-virtual {p2}, Landroid/os/HwParcel;->readInt8Vector()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->sendNfcRequest(ILjava/util/ArrayList;)V

    .line 1496
    return-void

    .line 1488
    :pswitch_1e
    invoke-virtual {p2, v3}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1489
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->disconnect()V

    .line 1490
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1491
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1492
    return-void

    .line 1481
    :pswitch_1f
    invoke-virtual {p2, v3}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1482
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->connect()Z

    move-result v0

    .line 1483
    .local v0, "connect":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1484
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1485
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1486
    return-void

    .line 1475
    .end local v0    # "connect":Z
    :pswitch_20
    invoke-virtual {p2, v3}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1476
    invoke-virtual {p2}, Landroid/os/HwParcel;->readStrongBinder()Landroid/os/IHwBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmNfcResponse;->asInterface(Landroid/os/IHwBinder;)Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmNfcResponse;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->setNfcResponseFunction(Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmNfcResponse;)V

    .line 1477
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1478
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1479
    return-void

    .line 1468
    :pswitch_21
    invoke-virtual {p2, v3}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1469
    invoke-virtual {p2}, Landroid/os/HwParcel;->readInt8Vector()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32Vector()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->writeMnlConfigFile(Ljava/util/ArrayList;Ljava/util/ArrayList;)Z

    move-result v0

    .line 1470
    .local v0, "writeMnlConfigFile":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1471
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1472
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1473
    return-void

    .line 1461
    .end local v0    # "writeMnlConfigFile":Z
    :pswitch_22
    invoke-virtual {p2, v3}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1462
    invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32Vector()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->readMnlConfigFile(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1463
    .local v0, "readMnlConfigFile":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1464
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt8Vector(Ljava/util/ArrayList;)V

    .line 1465
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1466
    return-void

    .line 1454
    .end local v0    # "readMnlConfigFile":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
    :pswitch_23
    invoke-virtual {p2, v2}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1455
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->isGauge30Support()I

    move-result v0

    .line 1456
    .local v0, "isGauge30Support":I
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1457
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V

    .line 1458
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1459
    return-void

    .line 1447
    .end local v0    # "isGauge30Support":I
    :pswitch_24
    invoke-virtual {p2, v2}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1448
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->isNfcSupport()I

    move-result v0

    .line 1449
    .local v0, "isNfcSupport":I
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1450
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V

    .line 1451
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1452
    return-void

    .line 1440
    .end local v0    # "isNfcSupport":I
    :pswitch_25
    invoke-virtual {p2, v2}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1441
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->btIsEmSupport()I

    move-result v0

    .line 1442
    .local v0, "btIsEmSupport":I
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1443
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V

    .line 1444
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1445
    return-void

    .line 1433
    .end local v0    # "btIsEmSupport":I
    :pswitch_26
    const-string/jumbo v0, "vendor.mediatek.hardware.engineermode@1.1::IEmd"

    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1434
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->setEmConfigure(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 1435
    .local v0, "emConfigure":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1436
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1437
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1438
    return-void

    .line 1426
    .end local v0    # "emConfigure":Z
    :pswitch_27
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1427
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Landroid/os/HwParcel;->readStrongBinder()Landroid/os/IHwBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback;->asInterface(Landroid/os/IHwBinder;)Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->getFilePathListWithCallBack(Ljava/lang/String;Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback;)Z

    move-result v0

    .line 1428
    .local v0, "filePathListWithCallBack":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1429
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1430
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1431
    return-void

    .line 1419
    .end local v0    # "filePathListWithCallBack":Z
    :pswitch_28
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1420
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->clearItemsforRsc()Z

    move-result v0

    .line 1421
    .local v0, "clearItemsforRsc":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1422
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1423
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1424
    return-void

    .line 1412
    .end local v0    # "clearItemsforRsc":Z
    :pswitch_29
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1413
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->setMoms(Ljava/lang/String;)Z

    move-result v0

    .line 1414
    .local v0, "moms":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1415
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1416
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1417
    return-void

    .line 1405
    .end local v0    # "moms":Z
    :pswitch_2a
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1406
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->setBypassService(Ljava/lang/String;)Z

    move-result v0

    .line 1407
    .local v0, "bypassService":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1408
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1409
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1410
    return-void

    .line 1398
    .end local v0    # "bypassService":Z
    :pswitch_2b
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1399
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->setBypassDis(Ljava/lang/String;)Z

    move-result v0

    .line 1400
    .local v0, "bypassDis":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1401
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1402
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1403
    return-void

    .line 1391
    .end local v0    # "bypassDis":Z
    :pswitch_2c
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1392
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->setBypassEn(Ljava/lang/String;)Z

    move-result v0

    .line 1393
    .local v0, "bypassEn":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1394
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1395
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1396
    return-void

    .line 1384
    .end local v0    # "bypassEn":Z
    :pswitch_2d
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1385
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->setEmUsbType(Ljava/lang/String;)Z

    move-result v0

    .line 1386
    .local v0, "emUsbType":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1387
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1388
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1389
    return-void

    .line 1377
    .end local v0    # "emUsbType":Z
    :pswitch_2e
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1378
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->setEmUsbValue(Ljava/lang/String;)Z

    move-result v0

    .line 1379
    .local v0, "emUsbValue":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1380
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1381
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1382
    return-void

    .line 1370
    .end local v0    # "emUsbValue":Z
    :pswitch_2f
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1371
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->setUsbOtgSwitch(Ljava/lang/String;)Z

    move-result v0

    .line 1372
    .local v0, "usbOtgSwitch":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1373
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1374
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1375
    return-void

    .line 1363
    .end local v0    # "usbOtgSwitch":Z
    :pswitch_30
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1364
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->setUsbPort(Ljava/lang/String;)Z

    move-result v0

    .line 1365
    .local v0, "usbPort":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1366
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1367
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1368
    return-void

    .line 1356
    .end local v0    # "usbPort":Z
    :pswitch_31
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1357
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->genMdLogFilter(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 1358
    .local v0, "genMdLogFilter":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1359
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1360
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1361
    return-void

    .line 1349
    .end local v0    # "genMdLogFilter":Z
    :pswitch_32
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1350
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->setModemWarningEnable(Ljava/lang/String;)Z

    move-result v0

    .line 1351
    .local v0, "modemWarningEnable":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1352
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1353
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1354
    return-void

    .line 1342
    .end local v0    # "modemWarningEnable":Z
    :pswitch_33
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1343
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->setVencDriverLogEnable(Ljava/lang/String;)Z

    move-result v0

    .line 1344
    .local v0, "vencDriverLogEnable":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1345
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1346
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1347
    return-void

    .line 1335
    .end local v0    # "vencDriverLogEnable":Z
    :pswitch_34
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1336
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->setOmxCoreLogEnable(Ljava/lang/String;)Z

    move-result v0

    .line 1337
    .local v0, "omxCoreLogEnable":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1338
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1339
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1340
    return-void

    .line 1328
    .end local v0    # "omxCoreLogEnable":Z
    :pswitch_35
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1329
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->setSvpLogEnable(Ljava/lang/String;)Z

    move-result v0

    .line 1330
    .local v0, "svpLogEnable":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1331
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1332
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1333
    return-void

    .line 1321
    .end local v0    # "svpLogEnable":Z
    :pswitch_36
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1322
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->setVdecDriverLogEnable(Ljava/lang/String;)Z

    move-result v0

    .line 1323
    .local v0, "vdecDriverLogEnable":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1324
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1325
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1326
    return-void

    .line 1558
    .end local v0    # "vdecDriverLogEnable":Z
    :sswitch_0
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1559
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->notifySyspropsChanged()V

    .line 1560
    return-void

    .line 1551
    :sswitch_1
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1552
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->getDebugInfo()Landroid/hidl/base/V1_0/DebugInfo;

    move-result-object v0

    .line 1553
    .local v0, "debugInfo":Landroid/hidl/base/V1_0/DebugInfo;
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1554
    invoke-virtual {v0, p3}, Landroid/hidl/base/V1_0/DebugInfo;->writeToParcel(Landroid/os/HwParcel;)V

    .line 1555
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1556
    return-void

    .line 1545
    .end local v0    # "debugInfo":Landroid/hidl/base/V1_0/DebugInfo;
    :sswitch_2
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1546
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->ping()V

    .line 1547
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1548
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1549
    return-void

    .line 1541
    :sswitch_3
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1542
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->setHALInstrumentation()V

    .line 1543
    return-void

    .line 1520
    :sswitch_4
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1521
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->getHashChain()Ljava/util/ArrayList;

    move-result-object v0

    .line 1522
    .local v0, "hashChain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1523
    new-instance v2, Landroid/os/HwBlob;

    const/16 v3, 0x10

    invoke-direct {v2, v3}, Landroid/os/HwBlob;-><init>(I)V

    .line 1524
    .local v2, "hwBlob":Landroid/os/HwBlob;
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 1525
    .local v3, "size":I
    const-wide/16 v4, 0x8

    invoke-virtual {v2, v4, v5, v3}, Landroid/os/HwBlob;->putInt32(JI)V

    .line 1526
    const-wide/16 v4, 0xc

    invoke-virtual {v2, v4, v5, v1}, Landroid/os/HwBlob;->putBool(JZ)V

    .line 1527
    new-instance v1, Landroid/os/HwBlob;

    mul-int/lit8 v4, v3, 0x20

    invoke-direct {v1, v4}, Landroid/os/HwBlob;-><init>(I)V

    .line 1528
    .local v1, "hwBlob2":Landroid/os/HwBlob;
    const/4 v4, 0x0

    .local v4, "i3":I
    :goto_0
    if-ge v4, v3, :cond_1

    .line 1529
    mul-int/lit8 v5, v4, 0x20

    int-to-long v5, v5

    .line 1530
    .local v5, "j":J
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [B

    .line 1531
    .local v7, "bArr":[B
    if-eqz v7, :cond_0

    array-length v8, v7

    const/16 v9, 0x20

    if-ne v8, v9, :cond_0

    .line 1534
    invoke-virtual {v1, v5, v6, v7}, Landroid/os/HwBlob;->putInt8Array(J[B)V

    .line 1528
    .end local v5    # "j":J
    .end local v7    # "bArr":[B
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1532
    .restart local v5    # "j":J
    .restart local v7    # "bArr":[B
    :cond_0
    new-instance v8, Ljava/lang/IllegalArgumentException;

    const-string v9, "Array element is not of the expected length"

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 1536
    .end local v4    # "i3":I
    .end local v5    # "j":J
    .end local v7    # "bArr":[B
    :cond_1
    const-wide/16 v4, 0x0

    invoke-virtual {v2, v4, v5, v1}, Landroid/os/HwBlob;->putBlob(JLandroid/os/HwBlob;)V

    .line 1537
    invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeBuffer(Landroid/os/HwBlob;)V

    .line 1538
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1539
    return-void

    .line 1513
    .end local v0    # "hashChain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    .end local v1    # "hwBlob2":Landroid/os/HwBlob;
    .end local v2    # "hwBlob":Landroid/os/HwBlob;
    .end local v3    # "size":I
    :sswitch_5
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1514
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->interfaceDescriptor()Ljava/lang/String;

    move-result-object v0

    .line 1515
    .local v0, "interfaceDescriptor":Ljava/lang/String;
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1516
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V

    .line 1517
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1518
    return-void

    .line 1507
    .end local v0    # "interfaceDescriptor":Ljava/lang/String;
    :sswitch_6
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1508
    invoke-virtual {p2}, Landroid/os/HwParcel;->readNativeHandle()Landroid/os/NativeHandle;

    move-result-object v0

    invoke-virtual {p2}, Landroid/os/HwParcel;->readStringVector()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->debug(Landroid/os/NativeHandle;Ljava/util/ArrayList;)V

    .line 1509
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1510
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1511
    return-void

    .line 1500
    :sswitch_7
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1501
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->interfaceChain()Ljava/util/ArrayList;

    move-result-object v0

    .line 1502
    .local v0, "interfaceChain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1503
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeStringVector(Ljava/util/ArrayList;)V

    .line 1504
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1505
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1e
        :pswitch_36
        :pswitch_35
        :pswitch_34
        :pswitch_33
        :pswitch_32
        :pswitch_31
        :pswitch_30
        :pswitch_2f
        :pswitch_2e
        :pswitch_2d
        :pswitch_2c
        :pswitch_2b
        :pswitch_2a
        :pswitch_29
        :pswitch_28
        :pswitch_27
        :pswitch_26
        :pswitch_25
        :pswitch_24
        :pswitch_23
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0xf43484e -> :sswitch_7
        0xf444247 -> :sswitch_6
        0xf445343 -> :sswitch_5
        0xf485348 -> :sswitch_4
        0xf494e54 -> :sswitch_3
        0xf504e47 -> :sswitch_2
        0xf524546 -> :sswitch_1
        0xf535953 -> :sswitch_0
    .end sparse-switch
.end method

.method public final ping()V
    .locals 0

    .line 1569
    return-void
.end method

.method public queryLocalInterface(Ljava/lang/String;)Landroid/os/IHwInterface;
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .line 1572
    const-string/jumbo v0, "vendor.mediatek.hardware.engineermode@1.3::IEmd"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1573
    return-object p0

    .line 1575
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public registerAsService(Ljava/lang/String;)V
    .locals 0
    .param p1, "str"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 1579
    invoke-virtual {p0, p1}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->registerService(Ljava/lang/String;)V

    .line 1580
    return-void
.end method

.method public final setHALInstrumentation()V
    .locals 0

    .line 1584
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1587
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_3/IEmd$Stub;->interfaceDescriptor()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "@Stub"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final unlinkToDeath(Landroid/os/IHwBinder$DeathRecipient;)Z
    .locals 1
    .param p1, "deathRecipient"    # Landroid/os/IHwBinder$DeathRecipient;

    .line 1592
    const/4 v0, 0x1

    return v0
.end method
