public abstract class com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd implements com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd {
	 /* .source "IEmd.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Proxy;, */
	 /* Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Stub; */
	 /* } */
} // .end annotation
/* # direct methods */
public static com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd asInterface ( android.os.IHwBinder p0 ) {
	 /* .locals 6 */
	 /* .param p0, "iHwBinder" # Landroid/os/IHwBinder; */
	 /* .line 1468 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* if-nez p0, :cond_0 */
	 /* .line 1469 */
	 /* .line 1471 */
} // :cond_0
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.2::IEmd" */
/* check-cast v1, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd; */
/* .line 1472 */
/* .local v1, "queryLocalInterface":Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd; */
if ( v1 != null) { // if-eqz v1, :cond_1
	 /* instance-of v2, v1, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd; */
	 if ( v2 != null) { // if-eqz v2, :cond_1
		 /* .line 1473 */
		 /* .line 1475 */
	 } // :cond_1
	 /* new-instance v2, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Proxy; */
	 /* invoke-direct {v2, p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Proxy;-><init>(Landroid/os/IHwBinder;)V */
	 /* .line 1477 */
	 /* .local v2, "proxy":Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Proxy; */
	 try { // :try_start_0
		 (( com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd$Proxy ) v2 ).interfaceChain ( ); // invoke-virtual {v2}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmd$Proxy;->interfaceChain()Ljava/util/ArrayList;
		 (( java.util.ArrayList ) v3 ).iterator ( ); // invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
		 /* .line 1478 */
		 /* .local v3, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;" */
	 v4 = 	 } // :cond_2
	 if ( v4 != null) { // if-eqz v4, :cond_3
		 /* .line 1479 */
		 /* check-cast v4, Ljava/lang/String; */
		 final String v5 = "android.hidl.base@1.0::IBase"; // const-string v5, "android.hidl.base@1.0::IBase"
		 v4 = 		 (( java.lang.String ) v4 ).equals ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 /* :try_end_0 */
		 /* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
		 if ( v4 != null) { // if-eqz v4, :cond_2
			 /* .line 1480 */
			 /* .line 1485 */
		 } // .end local v3 # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
	 } // :cond_3
	 /* .line 1483 */
	 /* :catch_0 */
	 /* move-exception v3 */
	 /* .line 1484 */
	 /* .local v3, "e":Landroid/os/RemoteException; */
	 (( android.os.RemoteException ) v3 ).printStackTrace ( ); // invoke-virtual {v3}, Landroid/os/RemoteException;->printStackTrace()V
	 /* .line 1486 */
} // .end local v3 # "e":Landroid/os/RemoteException;
} // :goto_0
} // .end method
public static com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd castFrom ( android.os.IHwInterface p0 ) {
/* .locals 1 */
/* .param p0, "iHwInterface" # Landroid/os/IHwInterface; */
/* .line 1490 */
/* if-nez p0, :cond_0 */
/* .line 1491 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1493 */
} // :cond_0
com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd .asInterface ( v0 );
} // .end method
public static com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd getService ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1497 */
final String v0 = "default"; // const-string v0, "default"
com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd .getService ( v0 );
} // .end method
public static com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd getService ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p0, "str" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1501 */
/* const-string/jumbo v0, "vendor.mediatek.hardware.engineermode@1.2::IEmd" */
android.os.HwBinder .getService ( v0,p0 );
com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd .asInterface ( v0 );
} // .end method
public static com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd getService ( java.lang.String p0, Boolean p1 ) {
/* .locals 1 */
/* .param p0, "str" # Ljava/lang/String; */
/* .param p1, "z" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1505 */
/* const-string/jumbo v0, "vendor.mediatek.hardware.engineermode@1.2::IEmd" */
android.os.HwBinder .getService ( v0,p0,p1 );
com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd .asInterface ( v0 );
} // .end method
public static com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd getService ( Boolean p0 ) {
/* .locals 1 */
/* .param p0, "z" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1509 */
final String v0 = "default"; // const-string v0, "default"
com.android.server.location.hardware.mtk.engineermode.V1_2.IEmd .getService ( v0,p0 );
} // .end method
/* # virtual methods */
public abstract android.os.IHwBinder asBinder ( ) {
} // .end method
public abstract Integer btIsEmSupport ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void debug ( android.os.NativeHandle p0, java.util.ArrayList p1 ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/os/NativeHandle;", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract android.hidl.base.V1_0.DebugInfo getDebugInfo ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract java.util.ArrayList getHashChain ( ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "[B>;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract java.util.ArrayList interfaceChain ( ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract java.lang.String interfaceDescriptor ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Integer isGauge30Support ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Integer isNfcSupport ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean linkToDeath ( android.os.IHwBinder$DeathRecipient p0, Long p1 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void notifySyspropsChanged ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void ping ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void setHALInstrumentation ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean unlinkToDeath ( android.os.IHwBinder$DeathRecipient p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
