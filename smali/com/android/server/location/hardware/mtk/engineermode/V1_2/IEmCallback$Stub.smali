.class public abstract Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmCallback$Stub;
.super Landroid/os/HwBinder;
.source "IEmCallback.java"

# interfaces
.implements Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmCallback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 206
    invoke-direct {p0}, Landroid/os/HwBinder;-><init>()V

    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IHwBinder;
    .locals 0

    .line 209
    return-object p0
.end method

.method public debug(Landroid/os/NativeHandle;Ljava/util/ArrayList;)V
    .locals 0
    .param p1, "nativeHandle"    # Landroid/os/NativeHandle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/NativeHandle;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 214
    .local p2, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    return-void
.end method

.method public final getDebugInfo()Landroid/hidl/base/V1_0/DebugInfo;
    .locals 3

    .line 218
    new-instance v0, Landroid/hidl/base/V1_0/DebugInfo;

    invoke-direct {v0}, Landroid/hidl/base/V1_0/DebugInfo;-><init>()V

    .line 219
    .local v0, "debugInfo":Landroid/hidl/base/V1_0/DebugInfo;
    invoke-static {}, Landroid/os/HidlSupport;->getPidIfSharable()I

    move-result v1

    iput v1, v0, Landroid/hidl/base/V1_0/DebugInfo;->pid:I

    .line 220
    const-wide/16 v1, 0x0

    iput-wide v1, v0, Landroid/hidl/base/V1_0/DebugInfo;->ptr:J

    .line 221
    const/4 v1, 0x0

    iput v1, v0, Landroid/hidl/base/V1_0/DebugInfo;->arch:I

    .line 222
    return-object v0
.end method

.method public final getHashChain()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "[B>;"
        }
    .end annotation

    .line 227
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x20

    new-array v2, v1, [B

    fill-array-data v2, :array_0

    new-array v3, v1, [B

    fill-array-data v3, :array_1

    new-array v4, v1, [B

    fill-array-data v4, :array_2

    new-array v1, v1, [B

    fill-array-data v1, :array_3

    filled-new-array {v2, v3, v4, v1}, [[B

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0

    :array_0
    .array-data 1
        0x46t
        0x56t
        0x29t
        0x2at
        -0x51t
        -0xft
        -0x4et
        -0x11t
        0x1at
        -0x1et
        -0x4ct
        -0x2dt
        -0x3et
        0x5bt
        -0x3t
        -0x48t
        0x4bt
        0x3et
        -0x3ct
        0x7dt
        0x4at
        0x5ct
        0x5et
        0x6bt
        -0x6et
        -0x57t
        0x1t
        -0x1dt
        -0x64t
        -0x71t
        -0x80t
        -0x8t
    .end array-data

    :array_1
    .array-data 1
        0xet
        0x53t
        0x1ct
        -0x4t
        0x68t
        0x62t
        -0x4ft
        -0x53t
        0x47t
        0x5ct
        -0x51t
        0x78t
        0x40t
        0x54t
        0x1ct
        0x5ct
        0x3at
        0x49t
        -0x3et
        -0x21t
        -0x3ft
        0x78t
        0x40t
        0x9t
        -0x3ct
        0x65t
        -0x2at
        0x24t
        0x22t
        0x44t
        -0x3t
        -0x22t
    .end array-data

    :array_2
    .array-data 1
        -0x1bt
        0x70t
        0x24t
        0x71t
        -0x52t
        0x14t
        -0x4bt
        -0x71t
        0x26t
        -0x27t
        0x65t
        -0x17t
        -0x80t
        -0x15t
        0x77t
        0x75t
        -0xat
        0x28t
        -0x6ct
        0x2at
        0x1dt
        -0x53t
        0x7t
        -0x6dt
        0x64t
        0x65t
        -0x5at
        0x64t
        -0x37t
        0x49t
        0x5dt
        -0x69t
    .end array-data

    :array_3
    .array-data 1
        -0x14t
        0x7ft
        -0x29t
        -0x62t
        -0x30t
        0x2dt
        -0x6t
        -0x7bt
        -0x44t
        0x49t
        -0x6ct
        0x26t
        -0x53t
        -0x52t
        0x3et
        -0x42t
        0x23t
        -0x11t
        0x5t
        0x24t
        -0xdt
        -0x33t
        0x69t
        0x57t
        0x13t
        -0x6dt
        0x24t
        -0x48t
        0x3bt
        0x18t
        -0x36t
        0x4ct
    .end array-data
.end method

.method public final interfaceChain()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 232
    new-instance v0, Ljava/util/ArrayList;

    const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmCallback"

    const-string v2, "android.hidl.base@1.0::IBase"

    const-string/jumbo v3, "vendor.mediatek.hardware.engineermode@1.2::IEmCallback"

    const-string/jumbo v4, "vendor.mediatek.hardware.engineermode@1.1::IEmCallback"

    filled-new-array {v3, v4, v1, v2}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public final interfaceDescriptor()Ljava/lang/String;
    .locals 1

    .line 237
    const-string/jumbo v0, "vendor.mediatek.hardware.engineermode@1.2::IEmCallback"

    return-object v0
.end method

.method public final linkToDeath(Landroid/os/IHwBinder$DeathRecipient;J)Z
    .locals 1
    .param p1, "deathRecipient"    # Landroid/os/IHwBinder$DeathRecipient;
    .param p2, "j"    # J

    .line 242
    const/4 v0, 0x1

    return v0
.end method

.method public final notifySyspropsChanged()V
    .locals 0

    .line 247
    invoke-static {}, Landroid/os/HwBinder;->enableInstrumentation()V

    .line 248
    return-void
.end method

.method public onTransact(ILandroid/os/HwParcel;Landroid/os/HwParcel;I)V
    .locals 10
    .param p1, "i"    # I
    .param p2, "hwParcel"    # Landroid/os/HwParcel;
    .param p3, "hwParcel2"    # Landroid/os/HwParcel;
    .param p4, "i2"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 251
    const-string v0, "android.hidl.base@1.0::IBase"

    const/4 v1, 0x0

    sparse-switch p1, :sswitch_data_0

    .line 322
    return-void

    .line 318
    :sswitch_0
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 319
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmCallback$Stub;->notifySyspropsChanged()V

    .line 320
    return-void

    .line 311
    :sswitch_1
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 312
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmCallback$Stub;->getDebugInfo()Landroid/hidl/base/V1_0/DebugInfo;

    move-result-object v0

    .line 313
    .local v0, "debugInfo":Landroid/hidl/base/V1_0/DebugInfo;
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 314
    invoke-virtual {v0, p3}, Landroid/hidl/base/V1_0/DebugInfo;->writeToParcel(Landroid/os/HwParcel;)V

    .line 315
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 316
    return-void

    .line 305
    .end local v0    # "debugInfo":Landroid/hidl/base/V1_0/DebugInfo;
    :sswitch_2
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 306
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmCallback$Stub;->ping()V

    .line 307
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 308
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 309
    return-void

    .line 301
    :sswitch_3
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 302
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmCallback$Stub;->setHALInstrumentation()V

    .line 303
    return-void

    .line 280
    :sswitch_4
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 281
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmCallback$Stub;->getHashChain()Ljava/util/ArrayList;

    move-result-object v0

    .line 282
    .local v0, "hashChain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 283
    new-instance v2, Landroid/os/HwBlob;

    const/16 v3, 0x10

    invoke-direct {v2, v3}, Landroid/os/HwBlob;-><init>(I)V

    .line 284
    .local v2, "hwBlob":Landroid/os/HwBlob;
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 285
    .local v3, "size":I
    const-wide/16 v4, 0x8

    invoke-virtual {v2, v4, v5, v3}, Landroid/os/HwBlob;->putInt32(JI)V

    .line 286
    const-wide/16 v4, 0xc

    invoke-virtual {v2, v4, v5, v1}, Landroid/os/HwBlob;->putBool(JZ)V

    .line 287
    new-instance v1, Landroid/os/HwBlob;

    mul-int/lit8 v4, v3, 0x20

    invoke-direct {v1, v4}, Landroid/os/HwBlob;-><init>(I)V

    .line 288
    .local v1, "hwBlob2":Landroid/os/HwBlob;
    const/4 v4, 0x0

    .local v4, "i3":I
    :goto_0
    if-ge v4, v3, :cond_1

    .line 289
    mul-int/lit8 v5, v4, 0x20

    int-to-long v5, v5

    .line 290
    .local v5, "j":J
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [B

    .line 291
    .local v7, "bArr":[B
    if-eqz v7, :cond_0

    array-length v8, v7

    const/16 v9, 0x20

    if-ne v8, v9, :cond_0

    .line 294
    invoke-virtual {v1, v5, v6, v7}, Landroid/os/HwBlob;->putInt8Array(J[B)V

    .line 288
    .end local v5    # "j":J
    .end local v7    # "bArr":[B
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 292
    .restart local v5    # "j":J
    .restart local v7    # "bArr":[B
    :cond_0
    new-instance v8, Ljava/lang/IllegalArgumentException;

    const-string v9, "Array element is not of the expected length"

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 296
    .end local v4    # "i3":I
    .end local v5    # "j":J
    .end local v7    # "bArr":[B
    :cond_1
    const-wide/16 v4, 0x0

    invoke-virtual {v2, v4, v5, v1}, Landroid/os/HwBlob;->putBlob(JLandroid/os/HwBlob;)V

    .line 297
    invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeBuffer(Landroid/os/HwBlob;)V

    .line 298
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 299
    return-void

    .line 273
    .end local v0    # "hashChain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    .end local v1    # "hwBlob2":Landroid/os/HwBlob;
    .end local v2    # "hwBlob":Landroid/os/HwBlob;
    .end local v3    # "size":I
    :sswitch_5
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 274
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmCallback$Stub;->interfaceDescriptor()Ljava/lang/String;

    move-result-object v0

    .line 275
    .local v0, "interfaceDescriptor":Ljava/lang/String;
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 276
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V

    .line 277
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 278
    return-void

    .line 267
    .end local v0    # "interfaceDescriptor":Ljava/lang/String;
    :sswitch_6
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 268
    invoke-virtual {p2}, Landroid/os/HwParcel;->readNativeHandle()Landroid/os/NativeHandle;

    move-result-object v0

    invoke-virtual {p2}, Landroid/os/HwParcel;->readStringVector()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmCallback$Stub;->debug(Landroid/os/NativeHandle;Ljava/util/ArrayList;)V

    .line 269
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 270
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 271
    return-void

    .line 260
    :sswitch_7
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 261
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmCallback$Stub;->interfaceChain()Ljava/util/ArrayList;

    move-result-object v0

    .line 262
    .local v0, "interfaceChain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 263
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeStringVector(Ljava/util/ArrayList;)V

    .line 264
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 265
    return-void

    .line 253
    .end local v0    # "interfaceChain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :sswitch_8
    const-string/jumbo v0, "vendor.mediatek.hardware.engineermode@1.0::IEmCallback"

    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 254
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmCallback$Stub;->callbackToClient(Ljava/lang/String;)Z

    move-result v0

    .line 255
    .local v0, "callbackToClient":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 256
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 257
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 258
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_8
        0xf43484e -> :sswitch_7
        0xf444247 -> :sswitch_6
        0xf445343 -> :sswitch_5
        0xf485348 -> :sswitch_4
        0xf494e54 -> :sswitch_3
        0xf504e47 -> :sswitch_2
        0xf524546 -> :sswitch_1
        0xf535953 -> :sswitch_0
    .end sparse-switch
.end method

.method public final ping()V
    .locals 0

    .line 328
    return-void
.end method

.method public queryLocalInterface(Ljava/lang/String;)Landroid/os/IHwInterface;
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .line 331
    const-string/jumbo v0, "vendor.mediatek.hardware.engineermode@1.2::IEmCallback"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 332
    return-object p0

    .line 334
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public registerAsService(Ljava/lang/String;)V
    .locals 0
    .param p1, "str"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 338
    invoke-virtual {p0, p1}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmCallback$Stub;->registerService(Ljava/lang/String;)V

    .line 339
    return-void
.end method

.method public final setHALInstrumentation()V
    .locals 0

    .line 343
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 346
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_2/IEmCallback$Stub;->interfaceDescriptor()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "@Stub"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final unlinkToDeath(Landroid/os/IHwBinder$DeathRecipient;)Z
    .locals 1
    .param p1, "deathRecipient"    # Landroid/os/IHwBinder$DeathRecipient;

    .line 351
    const/4 v0, 0x1

    return v0
.end method
