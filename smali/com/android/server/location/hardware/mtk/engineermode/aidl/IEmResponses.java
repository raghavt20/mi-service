public abstract class com.android.server.location.hardware.mtk.engineermode.aidl.IEmResponses implements android.os.IInterface {
	 /* .source "IEmResponses.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmResponses$Stub;, */
	 /* Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmResponses$Default; */
	 /* } */
} // .end annotation
/* # static fields */
public static final java.lang.String DESCRIPTOR;
public static final java.lang.String HASH;
public static final Integer VERSION;
/* # virtual methods */
public abstract java.lang.String getInterfaceHash ( ) {
	 /* .annotation system Ldalvik/annotation/Throws; */
	 /* value = { */
	 /* Landroid/os/RemoteException; */
	 /* } */
} // .end annotation
} // .end method
public abstract Integer getInterfaceVersion ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void response ( Object[] p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
