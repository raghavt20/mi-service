public abstract class com.android.server.location.hardware.mtk.engineermode.aidl.IEmResponses$Stub extends android.os.Binder implements com.android.server.location.hardware.mtk.engineermode.aidl.IEmResponses {
	 /* .source "IEmResponses.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmResponses; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x409 */
/* name = "Stub" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmResponses$Stub$Proxy; */
/* } */
} // .end annotation
/* # static fields */
static final Integer TRANSACTION_getInterfaceHash;
static final Integer TRANSACTION_getInterfaceVersion;
static final Integer TRANSACTION_response;
/* # direct methods */
public com.android.server.location.hardware.mtk.engineermode.aidl.IEmResponses$Stub ( ) {
/* .locals 1 */
/* .line 49 */
/* invoke-direct {p0}, Landroid/os/Binder;-><init>()V */
/* .line 50 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmResponses$Stub ) p0 ).markVintfStability ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmResponses$Stub;->markVintfStability()V
/* .line 51 */
/* const-string/jumbo v0, "vendor.mediatek.hardware.engineermode.IEmResponses" */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmResponses$Stub ) p0 ).attachInterface ( p0, v0 ); // invoke-virtual {p0, p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmResponses$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V
/* .line 52 */
return;
} // .end method
public static com.android.server.location.hardware.mtk.engineermode.aidl.IEmResponses asInterface ( android.os.IBinder p0 ) {
/* .locals 2 */
/* .param p0, "obj" # Landroid/os/IBinder; */
/* .line 55 */
/* if-nez p0, :cond_0 */
/* .line 56 */
int v0 = 0; // const/4 v0, 0x0
/* .line 58 */
} // :cond_0
/* const-string/jumbo v0, "vendor.mediatek.hardware.engineermode.IEmResponses" */
/* .line 59 */
/* .local v0, "iin":Landroid/os/IInterface; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* instance-of v1, v0, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmResponses; */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 60 */
/* move-object v1, v0 */
/* check-cast v1, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmResponses; */
/* .line 62 */
} // :cond_1
/* new-instance v1, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmResponses$Stub$Proxy; */
/* invoke-direct {v1, p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmResponses$Stub$Proxy;-><init>(Landroid/os/IBinder;)V */
} // .end method
/* # virtual methods */
public android.os.IBinder asBinder ( ) {
/* .locals 0 */
/* .line 67 */
} // .end method
public Boolean onTransact ( Integer p0, android.os.Parcel p1, android.os.Parcel p2, Integer p3 ) {
/* .locals 3 */
/* .param p1, "code" # I */
/* .param p2, "data" # Landroid/os/Parcel; */
/* .param p3, "reply" # Landroid/os/Parcel; */
/* .param p4, "flags" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 72 */
/* const-string/jumbo v0, "vendor.mediatek.hardware.engineermode.IEmResponses" */
/* .line 73 */
/* .local v0, "descriptor":Ljava/lang/String; */
int v1 = 1; // const/4 v1, 0x1
/* if-lt p1, v1, :cond_0 */
/* const v2, 0xffffff */
/* if-gt p1, v2, :cond_0 */
/* .line 74 */
(( android.os.Parcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 76 */
} // :cond_0
/* sparse-switch p1, :sswitch_data_0 */
/* .line 89 */
/* packed-switch p1, :pswitch_data_0 */
/* .line 96 */
v1 = /* invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z */
/* .line 86 */
/* :sswitch_0 */
(( android.os.Parcel ) p3 ).writeString ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 87 */
/* .line 82 */
/* :sswitch_1 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 83 */
v2 = (( com.android.server.location.hardware.mtk.engineermode.aidl.IEmResponses$Stub ) p0 ).getInterfaceVersion ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmResponses$Stub;->getInterfaceVersion()I
(( android.os.Parcel ) p3 ).writeInt ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V
/* .line 84 */
/* .line 78 */
/* :sswitch_2 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 79 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmResponses$Stub ) p0 ).getInterfaceHash ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmResponses$Stub;->getInterfaceHash()Ljava/lang/String;
(( android.os.Parcel ) p3 ).writeString ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 80 */
/* .line 91 */
/* :pswitch_0 */
(( android.os.Parcel ) p2 ).createByteArray ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B
/* .line 92 */
/* .local v2, "_arg0":[B */
(( android.os.Parcel ) p2 ).enforceNoDataAvail ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->enforceNoDataAvail()V
/* .line 93 */
(( com.android.server.location.hardware.mtk.engineermode.aidl.IEmResponses$Stub ) p0 ).response ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/location/hardware/mtk/engineermode/aidl/IEmResponses$Stub;->response([B)V
/* .line 94 */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0xfffffe -> :sswitch_2 */
/* 0xffffff -> :sswitch_1 */
/* 0x5f4e5446 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
