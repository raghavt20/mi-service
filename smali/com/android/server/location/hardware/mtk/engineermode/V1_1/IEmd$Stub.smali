.class public abstract Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;
.super Landroid/os/HwBinder;
.source "IEmd.java"

# interfaces
.implements Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 932
    invoke-direct {p0}, Landroid/os/HwBinder;-><init>()V

    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IHwBinder;
    .locals 0

    .line 935
    return-object p0
.end method

.method public debug(Landroid/os/NativeHandle;Ljava/util/ArrayList;)V
    .locals 0
    .param p1, "nativeHandle"    # Landroid/os/NativeHandle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/NativeHandle;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 940
    .local p2, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    return-void
.end method

.method public final getDebugInfo()Landroid/hidl/base/V1_0/DebugInfo;
    .locals 3

    .line 944
    new-instance v0, Landroid/hidl/base/V1_0/DebugInfo;

    invoke-direct {v0}, Landroid/hidl/base/V1_0/DebugInfo;-><init>()V

    .line 945
    .local v0, "debugInfo":Landroid/hidl/base/V1_0/DebugInfo;
    invoke-static {}, Landroid/os/HidlSupport;->getPidIfSharable()I

    move-result v1

    iput v1, v0, Landroid/hidl/base/V1_0/DebugInfo;->pid:I

    .line 946
    const-wide/16 v1, 0x0

    iput-wide v1, v0, Landroid/hidl/base/V1_0/DebugInfo;->ptr:J

    .line 947
    const/4 v1, 0x0

    iput v1, v0, Landroid/hidl/base/V1_0/DebugInfo;->arch:I

    .line 948
    return-object v0
.end method

.method public final getHashChain()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "[B>;"
        }
    .end annotation

    .line 953
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x20

    new-array v2, v1, [B

    fill-array-data v2, :array_0

    new-array v3, v1, [B

    fill-array-data v3, :array_1

    new-array v1, v1, [B

    fill-array-data v1, :array_2

    filled-new-array {v2, v3, v1}, [[B

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0

    nop

    :array_0
    .array-data 1
        0x63t
        -0x5dt
        0x23t
        -0x55t
        -0x5t
        -0x19t
        0x30t
        0xdt
        -0x70t
        -0x7ft
        0x66t
        -0x1at
        0x47t
        0x70t
        0x20t
        -0x2bt
        0x49t
        0x3at
        0x78t
        -0x11t
        -0x76t
        -0x54t
        0x7bt
        -0xet
        -0x1bt
        -0x2at
        -0x60t
        0x43t
        -0x41t
        -0x45t
        -0x5ct
        -0x71t
    .end array-data

    :array_1
    .array-data 1
        -0x42t
        -0x1bt
        0x12t
        -0x6bt
        0x79t
        -0x73t
        -0x5t
        -0x71t
        -0xbt
        0x1ct
        0x77t
        -0x69t
        0x79t
        -0x64t
        0x69t
        -0x1dt
        0x56t
        0x69t
        -0x4ft
        0x20t
        -0x9t
        0x19t
        0x36t
        -0x75t
        -0x5dt
        -0x61t
        -0x1dt
        -0x1et
        -0xat
        -0x30t
        0x4ft
        -0x64t
    .end array-data

    :array_2
    .array-data 1
        -0x14t
        0x7ft
        -0x29t
        -0x62t
        -0x30t
        0x2dt
        -0x6t
        -0x7bt
        -0x44t
        0x49t
        -0x6ct
        0x26t
        -0x53t
        -0x52t
        0x3et
        -0x42t
        0x23t
        -0x11t
        0x5t
        0x24t
        -0xdt
        -0x33t
        0x69t
        0x57t
        0x13t
        -0x6dt
        0x24t
        -0x48t
        0x3bt
        0x18t
        -0x36t
        0x4ct
    .end array-data
.end method

.method public final interfaceChain()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 958
    new-instance v0, Ljava/util/ArrayList;

    const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd"

    const-string v2, "android.hidl.base@1.0::IBase"

    const-string/jumbo v3, "vendor.mediatek.hardware.engineermode@1.1::IEmd"

    filled-new-array {v3, v1, v2}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public final interfaceDescriptor()Ljava/lang/String;
    .locals 1

    .line 963
    const-string/jumbo v0, "vendor.mediatek.hardware.engineermode@1.1::IEmd"

    return-object v0
.end method

.method public final linkToDeath(Landroid/os/IHwBinder$DeathRecipient;J)Z
    .locals 1
    .param p1, "deathRecipient"    # Landroid/os/IHwBinder$DeathRecipient;
    .param p2, "j"    # J

    .line 968
    const/4 v0, 0x1

    return v0
.end method

.method public final notifySyspropsChanged()V
    .locals 0

    .line 973
    invoke-static {}, Landroid/os/HwBinder;->enableInstrumentation()V

    .line 974
    return-void
.end method

.method public onTransact(ILandroid/os/HwParcel;Landroid/os/HwParcel;I)V
    .locals 10
    .param p1, "i"    # I
    .param p2, "hwParcel"    # Landroid/os/HwParcel;
    .param p3, "hwParcel2"    # Landroid/os/HwParcel;
    .param p4, "i2"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 977
    const-string/jumbo v0, "vendor.mediatek.hardware.engineermode@1.0::IEmd"

    const/4 v1, 0x0

    packed-switch p1, :pswitch_data_0

    .line 1154
    packed-switch p1, :pswitch_data_1

    .line 1303
    const-string v0, "android.hidl.base@1.0::IBase"

    sparse-switch p1, :sswitch_data_0

    .line 1367
    return-void

    .line 1146
    :pswitch_0
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1147
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->btPollingStop()I

    move-result v0

    .line 1148
    .local v0, "btPollingStop":I
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1149
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V

    .line 1150
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1151
    return-void

    .line 1139
    .end local v0    # "btPollingStop":I
    :pswitch_1
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1140
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->btPollingStart()I

    move-result v0

    .line 1141
    .local v0, "btPollingStart":I
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1142
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V

    .line 1143
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1144
    return-void

    .line 1132
    .end local v0    # "btPollingStart":I
    :pswitch_2
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1133
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->btIsComboSupport()I

    move-result v0

    .line 1134
    .local v0, "btIsComboSupport":I
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1135
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V

    .line 1136
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1137
    return-void

    .line 1125
    .end local v0    # "btIsComboSupport":I
    :pswitch_3
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1126
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->btEndNoSigRxTest()Ljava/util/ArrayList;

    move-result-object v0

    .line 1127
    .local v0, "btEndNoSigRxTest":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1128
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32Vector(Ljava/util/ArrayList;)V

    .line 1129
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1130
    return-void

    .line 1118
    .end local v0    # "btEndNoSigRxTest":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :pswitch_4
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1119
    invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I

    move-result v0

    invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I

    move-result v2

    invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I

    move-result v3

    invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I

    move-result v4

    invoke-virtual {p0, v0, v2, v3, v4}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->btStartNoSigRxTest(IIII)Z

    move-result v0

    .line 1120
    .local v0, "btStartNoSigRxTest":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1121
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1122
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1123
    return-void

    .line 1111
    .end local v0    # "btStartNoSigRxTest":Z
    :pswitch_5
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1112
    invoke-virtual {p2}, Landroid/os/HwParcel;->readInt8Vector()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->btHciCommandRun(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1113
    .local v0, "btHciCommandRun":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1114
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt8Vector(Ljava/util/ArrayList;)V

    .line 1115
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1116
    return-void

    .line 1104
    .end local v0    # "btHciCommandRun":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
    :pswitch_6
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1105
    invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I

    move-result v3

    invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I

    move-result v4

    invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I

    move-result v5

    invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I

    move-result v6

    invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I

    move-result v7

    invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I

    move-result v8

    invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I

    move-result v9

    move-object v2, p0

    invoke-virtual/range {v2 .. v9}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->btDoTest(IIIIIII)I

    move-result v0

    .line 1106
    .local v0, "btDoTest":I
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1107
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V

    .line 1108
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1109
    return-void

    .line 1097
    .end local v0    # "btDoTest":I
    :pswitch_7
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1098
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->btUninit()I

    move-result v0

    .line 1099
    .local v0, "btUninit":I
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1100
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V

    .line 1101
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1102
    return-void

    .line 1090
    .end local v0    # "btUninit":I
    :pswitch_8
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1091
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->btInit()I

    move-result v0

    .line 1092
    .local v0, "btInit":I
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1093
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V

    .line 1094
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1095
    return-void

    .line 1083
    .end local v0    # "btInit":I
    :pswitch_9
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1084
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->btIsBLEEnhancedSupport()Z

    move-result v0

    .line 1085
    .local v0, "btIsBLEEnhancedSupport":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1086
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1087
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1088
    return-void

    .line 1076
    .end local v0    # "btIsBLEEnhancedSupport":Z
    :pswitch_a
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1077
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->btIsBLESupport()I

    move-result v0

    .line 1078
    .local v0, "btIsBLESupport":I
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1079
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V

    .line 1080
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1081
    return-void

    .line 1069
    .end local v0    # "btIsBLESupport":I
    :pswitch_b
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1070
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->btStopRelayer()I

    move-result v0

    .line 1071
    .local v0, "btStopRelayer":I
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1072
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V

    .line 1073
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1074
    return-void

    .line 1062
    .end local v0    # "btStopRelayer":I
    :pswitch_c
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1063
    invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I

    move-result v0

    invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I

    move-result v2

    invoke-virtual {p0, v0, v2}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->btStartRelayer(II)I

    move-result v0

    .line 1064
    .local v0, "btStartRelayer":I
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1065
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V

    .line 1066
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1067
    return-void

    .line 1055
    .end local v0    # "btStartRelayer":I
    :pswitch_d
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1056
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setVolteMalPctid(Ljava/lang/String;)Z

    move-result v0

    .line 1057
    .local v0, "volteMalPctid":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1058
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1059
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1060
    return-void

    .line 1048
    .end local v0    # "volteMalPctid":Z
    :pswitch_e
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1049
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setDsbpSupport(Ljava/lang/String;)Z

    move-result v0

    .line 1050
    .local v0, "dsbpSupport":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1051
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1052
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1053
    return-void

    .line 1041
    .end local v0    # "dsbpSupport":Z
    :pswitch_f
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1042
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setImsTestMode(Ljava/lang/String;)Z

    move-result v0

    .line 1043
    .local v0, "imsTestMode":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1044
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1045
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1046
    return-void

    .line 1034
    .end local v0    # "imsTestMode":Z
    :pswitch_10
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1035
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setDisableC2kCap(Ljava/lang/String;)Z

    move-result v0

    .line 1036
    .local v0, "disableC2kCap":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1037
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1038
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1039
    return-void

    .line 1027
    .end local v0    # "disableC2kCap":Z
    :pswitch_11
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1028
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setRadioCapabilitySwitchEnable(Ljava/lang/String;)Z

    move-result v0

    .line 1029
    .local v0, "radioCapabilitySwitchEnable":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1030
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1031
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1032
    return-void

    .line 1020
    .end local v0    # "radioCapabilitySwitchEnable":Z
    :pswitch_12
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1021
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setPreferGprsMode(Ljava/lang/String;)Z

    move-result v0

    .line 1022
    .local v0, "preferGprsMode":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1023
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1024
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1025
    return-void

    .line 1013
    .end local v0    # "preferGprsMode":Z
    :pswitch_13
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1014
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setTestSimCardType(Ljava/lang/String;)Z

    move-result v0

    .line 1015
    .local v0, "testSimCardType":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1016
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1017
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1018
    return-void

    .line 1006
    .end local v0    # "testSimCardType":Z
    :pswitch_14
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1007
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setCtIREngMode(Ljava/lang/String;)Z

    move-result v0

    .line 1008
    .local v0, "ctIREngMode":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1009
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1010
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1011
    return-void

    .line 999
    .end local v0    # "ctIREngMode":Z
    :pswitch_15
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1000
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setSmsFormat(Ljava/lang/String;)Z

    move-result v0

    .line 1001
    .local v0, "smsFormat":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1002
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1003
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1004
    return-void

    .line 992
    .end local v0    # "smsFormat":Z
    :pswitch_16
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 993
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Landroid/os/HwParcel;->readStrongBinder()Landroid/os/IHwBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback;->asInterface(Landroid/os/IHwBinder;)Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->sendToServerWithCallBack(Ljava/lang/String;Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback;)Z

    move-result v0

    .line 994
    .local v0, "sendToServerWithCallBack":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 995
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 996
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 997
    return-void

    .line 985
    .end local v0    # "sendToServerWithCallBack":Z
    :pswitch_17
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 986
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->sendToServer(Ljava/lang/String;)Z

    move-result v0

    .line 987
    .local v0, "sendToServer":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 988
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 989
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 990
    return-void

    .line 979
    .end local v0    # "sendToServer":Z
    :pswitch_18
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 980
    invoke-virtual {p2}, Landroid/os/HwParcel;->readStrongBinder()Landroid/os/IHwBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback;->asInterface(Landroid/os/IHwBinder;)Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setCallback(Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback;)V

    .line 981
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 982
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 983
    return-void

    .line 1296
    :pswitch_19
    const-string/jumbo v0, "vendor.mediatek.hardware.engineermode@1.1::IEmd"

    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1297
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setEmConfigure(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 1298
    .local v0, "emConfigure":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1299
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1300
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1301
    return-void

    .line 1289
    .end local v0    # "emConfigure":Z
    :pswitch_1a
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1290
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Landroid/os/HwParcel;->readStrongBinder()Landroid/os/IHwBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback;->asInterface(Landroid/os/IHwBinder;)Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->getFilePathListWithCallBack(Ljava/lang/String;Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback;)Z

    move-result v0

    .line 1291
    .local v0, "filePathListWithCallBack":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1292
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1293
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1294
    return-void

    .line 1282
    .end local v0    # "filePathListWithCallBack":Z
    :pswitch_1b
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1283
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->clearItemsforRsc()Z

    move-result v0

    .line 1284
    .local v0, "clearItemsforRsc":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1285
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1286
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1287
    return-void

    .line 1275
    .end local v0    # "clearItemsforRsc":Z
    :pswitch_1c
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1276
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setMoms(Ljava/lang/String;)Z

    move-result v0

    .line 1277
    .local v0, "moms":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1278
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1279
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1280
    return-void

    .line 1268
    .end local v0    # "moms":Z
    :pswitch_1d
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1269
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setBypassService(Ljava/lang/String;)Z

    move-result v0

    .line 1270
    .local v0, "bypassService":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1271
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1272
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1273
    return-void

    .line 1261
    .end local v0    # "bypassService":Z
    :pswitch_1e
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1262
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setBypassDis(Ljava/lang/String;)Z

    move-result v0

    .line 1263
    .local v0, "bypassDis":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1264
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1265
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1266
    return-void

    .line 1254
    .end local v0    # "bypassDis":Z
    :pswitch_1f
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1255
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setBypassEn(Ljava/lang/String;)Z

    move-result v0

    .line 1256
    .local v0, "bypassEn":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1257
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1258
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1259
    return-void

    .line 1247
    .end local v0    # "bypassEn":Z
    :pswitch_20
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1248
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setEmUsbType(Ljava/lang/String;)Z

    move-result v0

    .line 1249
    .local v0, "emUsbType":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1250
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1251
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1252
    return-void

    .line 1240
    .end local v0    # "emUsbType":Z
    :pswitch_21
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1241
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setEmUsbValue(Ljava/lang/String;)Z

    move-result v0

    .line 1242
    .local v0, "emUsbValue":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1243
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1244
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1245
    return-void

    .line 1233
    .end local v0    # "emUsbValue":Z
    :pswitch_22
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1234
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setUsbOtgSwitch(Ljava/lang/String;)Z

    move-result v0

    .line 1235
    .local v0, "usbOtgSwitch":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1236
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1237
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1238
    return-void

    .line 1226
    .end local v0    # "usbOtgSwitch":Z
    :pswitch_23
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1227
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setUsbPort(Ljava/lang/String;)Z

    move-result v0

    .line 1228
    .local v0, "usbPort":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1229
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1230
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1231
    return-void

    .line 1219
    .end local v0    # "usbPort":Z
    :pswitch_24
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1220
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->genMdLogFilter(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 1221
    .local v0, "genMdLogFilter":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1222
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1223
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1224
    return-void

    .line 1212
    .end local v0    # "genMdLogFilter":Z
    :pswitch_25
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1213
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setModemWarningEnable(Ljava/lang/String;)Z

    move-result v0

    .line 1214
    .local v0, "modemWarningEnable":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1215
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1216
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1217
    return-void

    .line 1205
    .end local v0    # "modemWarningEnable":Z
    :pswitch_26
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1206
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setVencDriverLogEnable(Ljava/lang/String;)Z

    move-result v0

    .line 1207
    .local v0, "vencDriverLogEnable":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1208
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1209
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1210
    return-void

    .line 1198
    .end local v0    # "vencDriverLogEnable":Z
    :pswitch_27
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1199
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setOmxCoreLogEnable(Ljava/lang/String;)Z

    move-result v0

    .line 1200
    .local v0, "omxCoreLogEnable":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1201
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1202
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1203
    return-void

    .line 1191
    .end local v0    # "omxCoreLogEnable":Z
    :pswitch_28
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1192
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setSvpLogEnable(Ljava/lang/String;)Z

    move-result v0

    .line 1193
    .local v0, "svpLogEnable":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1194
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1195
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1196
    return-void

    .line 1184
    .end local v0    # "svpLogEnable":Z
    :pswitch_29
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1185
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setVdecDriverLogEnable(Ljava/lang/String;)Z

    move-result v0

    .line 1186
    .local v0, "vdecDriverLogEnable":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1187
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1188
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1189
    return-void

    .line 1177
    .end local v0    # "vdecDriverLogEnable":Z
    :pswitch_2a
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1178
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setOmxVdecLogEnable(Ljava/lang/String;)Z

    move-result v0

    .line 1179
    .local v0, "omxVdecLogEnable":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1180
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1181
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1182
    return-void

    .line 1170
    .end local v0    # "omxVdecLogEnable":Z
    :pswitch_2b
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1171
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setOmxVencLogEnable(Ljava/lang/String;)Z

    move-result v0

    .line 1172
    .local v0, "omxVencLogEnable":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1173
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1174
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1175
    return-void

    .line 1163
    .end local v0    # "omxVencLogEnable":Z
    :pswitch_2c
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1164
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setWcnCoreDump(Ljava/lang/String;)Z

    move-result v0

    .line 1165
    .local v0, "wcnCoreDump":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1166
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1167
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1168
    return-void

    .line 1156
    .end local v0    # "wcnCoreDump":Z
    :pswitch_2d
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1157
    invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setMdResetDelay(Ljava/lang/String;)Z

    move-result v0

    .line 1158
    .local v0, "mdResetDelay":Z
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1159
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 1160
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1161
    return-void

    .line 1363
    .end local v0    # "mdResetDelay":Z
    :sswitch_0
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1364
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->notifySyspropsChanged()V

    .line 1365
    return-void

    .line 1356
    :sswitch_1
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1357
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->getDebugInfo()Landroid/hidl/base/V1_0/DebugInfo;

    move-result-object v0

    .line 1358
    .local v0, "debugInfo":Landroid/hidl/base/V1_0/DebugInfo;
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1359
    invoke-virtual {v0, p3}, Landroid/hidl/base/V1_0/DebugInfo;->writeToParcel(Landroid/os/HwParcel;)V

    .line 1360
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1361
    return-void

    .line 1350
    .end local v0    # "debugInfo":Landroid/hidl/base/V1_0/DebugInfo;
    :sswitch_2
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1351
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->ping()V

    .line 1352
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1353
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1354
    return-void

    .line 1346
    :sswitch_3
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1347
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setHALInstrumentation()V

    .line 1348
    return-void

    .line 1325
    :sswitch_4
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1326
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->getHashChain()Ljava/util/ArrayList;

    move-result-object v0

    .line 1327
    .local v0, "hashChain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1328
    new-instance v2, Landroid/os/HwBlob;

    const/16 v3, 0x10

    invoke-direct {v2, v3}, Landroid/os/HwBlob;-><init>(I)V

    .line 1329
    .local v2, "hwBlob":Landroid/os/HwBlob;
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 1330
    .local v3, "size":I
    const-wide/16 v4, 0x8

    invoke-virtual {v2, v4, v5, v3}, Landroid/os/HwBlob;->putInt32(JI)V

    .line 1331
    const-wide/16 v4, 0xc

    invoke-virtual {v2, v4, v5, v1}, Landroid/os/HwBlob;->putBool(JZ)V

    .line 1332
    new-instance v1, Landroid/os/HwBlob;

    mul-int/lit8 v4, v3, 0x20

    invoke-direct {v1, v4}, Landroid/os/HwBlob;-><init>(I)V

    .line 1333
    .local v1, "hwBlob2":Landroid/os/HwBlob;
    const/4 v4, 0x0

    .local v4, "i3":I
    :goto_0
    if-ge v4, v3, :cond_1

    .line 1334
    mul-int/lit8 v5, v4, 0x20

    int-to-long v5, v5

    .line 1335
    .local v5, "j":J
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [B

    .line 1336
    .local v7, "bArr":[B
    if-eqz v7, :cond_0

    array-length v8, v7

    const/16 v9, 0x20

    if-ne v8, v9, :cond_0

    .line 1339
    invoke-virtual {v1, v5, v6, v7}, Landroid/os/HwBlob;->putInt8Array(J[B)V

    .line 1333
    .end local v5    # "j":J
    .end local v7    # "bArr":[B
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1337
    .restart local v5    # "j":J
    .restart local v7    # "bArr":[B
    :cond_0
    new-instance v8, Ljava/lang/IllegalArgumentException;

    const-string v9, "Array element is not of the expected length"

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 1341
    .end local v4    # "i3":I
    .end local v5    # "j":J
    .end local v7    # "bArr":[B
    :cond_1
    const-wide/16 v4, 0x0

    invoke-virtual {v2, v4, v5, v1}, Landroid/os/HwBlob;->putBlob(JLandroid/os/HwBlob;)V

    .line 1342
    invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeBuffer(Landroid/os/HwBlob;)V

    .line 1343
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1344
    return-void

    .line 1318
    .end local v0    # "hashChain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    .end local v1    # "hwBlob2":Landroid/os/HwBlob;
    .end local v2    # "hwBlob":Landroid/os/HwBlob;
    .end local v3    # "size":I
    :sswitch_5
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1319
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->interfaceDescriptor()Ljava/lang/String;

    move-result-object v0

    .line 1320
    .local v0, "interfaceDescriptor":Ljava/lang/String;
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1321
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V

    .line 1322
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1323
    return-void

    .line 1312
    .end local v0    # "interfaceDescriptor":Ljava/lang/String;
    :sswitch_6
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1313
    invoke-virtual {p2}, Landroid/os/HwParcel;->readNativeHandle()Landroid/os/NativeHandle;

    move-result-object v0

    invoke-virtual {p2}, Landroid/os/HwParcel;->readStringVector()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->debug(Landroid/os/NativeHandle;Ljava/util/ArrayList;)V

    .line 1314
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1315
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1316
    return-void

    .line 1305
    :sswitch_7
    invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1306
    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->interfaceChain()Ljava/util/ArrayList;

    move-result-object v0

    .line 1307
    .local v0, "interfaceChain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 1308
    invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeStringVector(Ljava/util/ArrayList;)V

    .line 1309
    invoke-virtual {p3}, Landroid/os/HwParcel;->send()V

    .line 1310
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1a
        :pswitch_2d
        :pswitch_2c
        :pswitch_2b
        :pswitch_2a
        :pswitch_29
        :pswitch_28
        :pswitch_27
        :pswitch_26
        :pswitch_25
        :pswitch_24
        :pswitch_23
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0xf43484e -> :sswitch_7
        0xf444247 -> :sswitch_6
        0xf445343 -> :sswitch_5
        0xf485348 -> :sswitch_4
        0xf494e54 -> :sswitch_3
        0xf504e47 -> :sswitch_2
        0xf524546 -> :sswitch_1
        0xf535953 -> :sswitch_0
    .end sparse-switch
.end method

.method public final ping()V
    .locals 0

    .line 1374
    return-void
.end method

.method public queryLocalInterface(Ljava/lang/String;)Landroid/os/IHwInterface;
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .line 1377
    const-string/jumbo v0, "vendor.mediatek.hardware.engineermode@1.1::IEmd"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1378
    return-object p0

    .line 1380
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public registerAsService(Ljava/lang/String;)V
    .locals 0
    .param p1, "str"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 1384
    invoke-virtual {p0, p1}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->registerService(Ljava/lang/String;)V

    .line 1385
    return-void
.end method

.method public final setHALInstrumentation()V
    .locals 0

    .line 1389
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1392
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->interfaceDescriptor()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "@Stub"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final unlinkToDeath(Landroid/os/IHwBinder$DeathRecipient;)Z
    .locals 1
    .param p1, "deathRecipient"    # Landroid/os/IHwBinder$DeathRecipient;

    .line 1397
    const/4 v0, 0x1

    return v0
.end method
