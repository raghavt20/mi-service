public abstract class com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub extends android.os.HwBinder implements com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd {
	 /* .source "IEmd.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x409 */
/* name = "Stub" */
} // .end annotation
/* # direct methods */
public com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ( ) {
/* .locals 0 */
/* .line 932 */
/* invoke-direct {p0}, Landroid/os/HwBinder;-><init>()V */
return;
} // .end method
/* # virtual methods */
public android.os.IHwBinder asBinder ( ) {
/* .locals 0 */
/* .line 935 */
} // .end method
public void debug ( android.os.NativeHandle p0, java.util.ArrayList p1 ) {
/* .locals 0 */
/* .param p1, "nativeHandle" # Landroid/os/NativeHandle; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/os/NativeHandle;", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 940 */
/* .local p2, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
return;
} // .end method
public final android.hidl.base.V1_0.DebugInfo getDebugInfo ( ) {
/* .locals 3 */
/* .line 944 */
/* new-instance v0, Landroid/hidl/base/V1_0/DebugInfo; */
/* invoke-direct {v0}, Landroid/hidl/base/V1_0/DebugInfo;-><init>()V */
/* .line 945 */
/* .local v0, "debugInfo":Landroid/hidl/base/V1_0/DebugInfo; */
v1 = android.os.HidlSupport .getPidIfSharable ( );
/* iput v1, v0, Landroid/hidl/base/V1_0/DebugInfo;->pid:I */
/* .line 946 */
/* const-wide/16 v1, 0x0 */
/* iput-wide v1, v0, Landroid/hidl/base/V1_0/DebugInfo;->ptr:J */
/* .line 947 */
int v1 = 0; // const/4 v1, 0x0
/* iput v1, v0, Landroid/hidl/base/V1_0/DebugInfo;->arch:I */
/* .line 948 */
} // .end method
public final java.util.ArrayList getHashChain ( ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "[B>;" */
/* } */
} // .end annotation
/* .line 953 */
/* new-instance v0, Ljava/util/ArrayList; */
/* const/16 v1, 0x20 */
/* new-array v2, v1, [B */
/* fill-array-data v2, :array_0 */
/* new-array v3, v1, [B */
/* fill-array-data v3, :array_1 */
/* new-array v1, v1, [B */
/* fill-array-data v1, :array_2 */
/* filled-new-array {v2, v3, v1}, [[B */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* nop */
/* :array_0 */
/* .array-data 1 */
/* 0x63t */
/* -0x5dt */
/* 0x23t */
/* -0x55t */
/* -0x5t */
/* -0x19t */
/* 0x30t */
/* 0xdt */
/* -0x70t */
/* -0x7ft */
/* 0x66t */
/* -0x1at */
/* 0x47t */
/* 0x70t */
/* 0x20t */
/* -0x2bt */
/* 0x49t */
/* 0x3at */
/* 0x78t */
/* -0x11t */
/* -0x76t */
/* -0x54t */
/* 0x7bt */
/* -0xet */
/* -0x1bt */
/* -0x2at */
/* -0x60t */
/* 0x43t */
/* -0x41t */
/* -0x45t */
/* -0x5ct */
/* -0x71t */
} // .end array-data
/* :array_1 */
/* .array-data 1 */
/* -0x42t */
/* -0x1bt */
/* 0x12t */
/* -0x6bt */
/* 0x79t */
/* -0x73t */
/* -0x5t */
/* -0x71t */
/* -0xbt */
/* 0x1ct */
/* 0x77t */
/* -0x69t */
/* 0x79t */
/* -0x64t */
/* 0x69t */
/* -0x1dt */
/* 0x56t */
/* 0x69t */
/* -0x4ft */
/* 0x20t */
/* -0x9t */
/* 0x19t */
/* 0x36t */
/* -0x75t */
/* -0x5dt */
/* -0x61t */
/* -0x1dt */
/* -0x1et */
/* -0xat */
/* -0x30t */
/* 0x4ft */
/* -0x64t */
} // .end array-data
/* :array_2 */
/* .array-data 1 */
/* -0x14t */
/* 0x7ft */
/* -0x29t */
/* -0x62t */
/* -0x30t */
/* 0x2dt */
/* -0x6t */
/* -0x7bt */
/* -0x44t */
/* 0x49t */
/* -0x6ct */
/* 0x26t */
/* -0x53t */
/* -0x52t */
/* 0x3et */
/* -0x42t */
/* 0x23t */
/* -0x11t */
/* 0x5t */
/* 0x24t */
/* -0xdt */
/* -0x33t */
/* 0x69t */
/* 0x57t */
/* 0x13t */
/* -0x6dt */
/* 0x24t */
/* -0x48t */
/* 0x3bt */
/* 0x18t */
/* -0x36t */
/* 0x4ct */
} // .end array-data
} // .end method
public final java.util.ArrayList interfaceChain ( ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 958 */
/* new-instance v0, Ljava/util/ArrayList; */
/* const-string/jumbo v1, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
final String v2 = "android.hidl.base@1.0::IBase"; // const-string v2, "android.hidl.base@1.0::IBase"
/* const-string/jumbo v3, "vendor.mediatek.hardware.engineermode@1.1::IEmd" */
/* filled-new-array {v3, v1, v2}, [Ljava/lang/String; */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
} // .end method
public final java.lang.String interfaceDescriptor ( ) {
/* .locals 1 */
/* .line 963 */
/* const-string/jumbo v0, "vendor.mediatek.hardware.engineermode@1.1::IEmd" */
} // .end method
public final Boolean linkToDeath ( android.os.IHwBinder$DeathRecipient p0, Long p1 ) {
/* .locals 1 */
/* .param p1, "deathRecipient" # Landroid/os/IHwBinder$DeathRecipient; */
/* .param p2, "j" # J */
/* .line 968 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
public final void notifySyspropsChanged ( ) {
/* .locals 0 */
/* .line 973 */
android.os.HwBinder .enableInstrumentation ( );
/* .line 974 */
return;
} // .end method
public void onTransact ( Integer p0, android.os.HwParcel p1, android.os.HwParcel p2, Integer p3 ) {
/* .locals 10 */
/* .param p1, "i" # I */
/* .param p2, "hwParcel" # Landroid/os/HwParcel; */
/* .param p3, "hwParcel2" # Landroid/os/HwParcel; */
/* .param p4, "i2" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 977 */
/* const-string/jumbo v0, "vendor.mediatek.hardware.engineermode@1.0::IEmd" */
int v1 = 0; // const/4 v1, 0x0
/* packed-switch p1, :pswitch_data_0 */
/* .line 1154 */
/* packed-switch p1, :pswitch_data_1 */
/* .line 1303 */
final String v0 = "android.hidl.base@1.0::IBase"; // const-string v0, "android.hidl.base@1.0::IBase"
/* sparse-switch p1, :sswitch_data_0 */
/* .line 1367 */
return;
/* .line 1146 */
/* :pswitch_0 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1147 */
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).btPollingStop ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->btPollingStop()I
/* .line 1148 */
/* .local v0, "btPollingStop":I */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1149 */
(( android.os.HwParcel ) p3 ).writeInt32 ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 1150 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1151 */
return;
/* .line 1139 */
} // .end local v0 # "btPollingStop":I
/* :pswitch_1 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1140 */
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).btPollingStart ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->btPollingStart()I
/* .line 1141 */
/* .local v0, "btPollingStart":I */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1142 */
(( android.os.HwParcel ) p3 ).writeInt32 ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 1143 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1144 */
return;
/* .line 1132 */
} // .end local v0 # "btPollingStart":I
/* :pswitch_2 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1133 */
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).btIsComboSupport ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->btIsComboSupport()I
/* .line 1134 */
/* .local v0, "btIsComboSupport":I */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1135 */
(( android.os.HwParcel ) p3 ).writeInt32 ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 1136 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1137 */
return;
/* .line 1125 */
} // .end local v0 # "btIsComboSupport":I
/* :pswitch_3 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1126 */
(( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).btEndNoSigRxTest ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->btEndNoSigRxTest()Ljava/util/ArrayList;
/* .line 1127 */
/* .local v0, "btEndNoSigRxTest":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1128 */
(( android.os.HwParcel ) p3 ).writeInt32Vector ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32Vector(Ljava/util/ArrayList;)V
/* .line 1129 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1130 */
return;
/* .line 1118 */
} // .end local v0 # "btEndNoSigRxTest":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
/* :pswitch_4 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1119 */
v0 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
v2 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
v3 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
v4 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).btStartNoSigRxTest ( v0, v2, v3, v4 ); // invoke-virtual {p0, v0, v2, v3, v4}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->btStartNoSigRxTest(IIII)Z
/* .line 1120 */
/* .local v0, "btStartNoSigRxTest":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1121 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1122 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1123 */
return;
/* .line 1111 */
} // .end local v0 # "btStartNoSigRxTest":Z
/* :pswitch_5 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1112 */
(( android.os.HwParcel ) p2 ).readInt8Vector ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt8Vector()Ljava/util/ArrayList;
(( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).btHciCommandRun ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->btHciCommandRun(Ljava/util/ArrayList;)Ljava/util/ArrayList;
/* .line 1113 */
/* .local v0, "btHciCommandRun":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;" */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1114 */
(( android.os.HwParcel ) p3 ).writeInt8Vector ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt8Vector(Ljava/util/ArrayList;)V
/* .line 1115 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1116 */
return;
/* .line 1104 */
} // .end local v0 # "btHciCommandRun":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
/* :pswitch_6 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1105 */
v3 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
v4 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
v5 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
v6 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
v7 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
v8 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
v9 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
/* move-object v2, p0 */
v0 = /* invoke-virtual/range {v2 ..v9}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->btDoTest(IIIIIII)I */
/* .line 1106 */
/* .local v0, "btDoTest":I */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1107 */
(( android.os.HwParcel ) p3 ).writeInt32 ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 1108 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1109 */
return;
/* .line 1097 */
} // .end local v0 # "btDoTest":I
/* :pswitch_7 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1098 */
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).btUninit ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->btUninit()I
/* .line 1099 */
/* .local v0, "btUninit":I */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1100 */
(( android.os.HwParcel ) p3 ).writeInt32 ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 1101 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1102 */
return;
/* .line 1090 */
} // .end local v0 # "btUninit":I
/* :pswitch_8 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1091 */
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).btInit ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->btInit()I
/* .line 1092 */
/* .local v0, "btInit":I */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1093 */
(( android.os.HwParcel ) p3 ).writeInt32 ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 1094 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1095 */
return;
/* .line 1083 */
} // .end local v0 # "btInit":I
/* :pswitch_9 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1084 */
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).btIsBLEEnhancedSupport ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->btIsBLEEnhancedSupport()Z
/* .line 1085 */
/* .local v0, "btIsBLEEnhancedSupport":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1086 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1087 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1088 */
return;
/* .line 1076 */
} // .end local v0 # "btIsBLEEnhancedSupport":Z
/* :pswitch_a */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1077 */
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).btIsBLESupport ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->btIsBLESupport()I
/* .line 1078 */
/* .local v0, "btIsBLESupport":I */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1079 */
(( android.os.HwParcel ) p3 ).writeInt32 ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 1080 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1081 */
return;
/* .line 1069 */
} // .end local v0 # "btIsBLESupport":I
/* :pswitch_b */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1070 */
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).btStopRelayer ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->btStopRelayer()I
/* .line 1071 */
/* .local v0, "btStopRelayer":I */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1072 */
(( android.os.HwParcel ) p3 ).writeInt32 ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 1073 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1074 */
return;
/* .line 1062 */
} // .end local v0 # "btStopRelayer":I
/* :pswitch_c */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1063 */
v0 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
v2 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).btStartRelayer ( v0, v2 ); // invoke-virtual {p0, v0, v2}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->btStartRelayer(II)I
/* .line 1064 */
/* .local v0, "btStartRelayer":I */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1065 */
(( android.os.HwParcel ) p3 ).writeInt32 ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 1066 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1067 */
return;
/* .line 1055 */
} // .end local v0 # "btStartRelayer":I
/* :pswitch_d */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1056 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).setVolteMalPctid ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setVolteMalPctid(Ljava/lang/String;)Z
/* .line 1057 */
/* .local v0, "volteMalPctid":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1058 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1059 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1060 */
return;
/* .line 1048 */
} // .end local v0 # "volteMalPctid":Z
/* :pswitch_e */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1049 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).setDsbpSupport ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setDsbpSupport(Ljava/lang/String;)Z
/* .line 1050 */
/* .local v0, "dsbpSupport":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1051 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1052 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1053 */
return;
/* .line 1041 */
} // .end local v0 # "dsbpSupport":Z
/* :pswitch_f */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1042 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).setImsTestMode ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setImsTestMode(Ljava/lang/String;)Z
/* .line 1043 */
/* .local v0, "imsTestMode":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1044 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1045 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1046 */
return;
/* .line 1034 */
} // .end local v0 # "imsTestMode":Z
/* :pswitch_10 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1035 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).setDisableC2kCap ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setDisableC2kCap(Ljava/lang/String;)Z
/* .line 1036 */
/* .local v0, "disableC2kCap":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1037 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1038 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1039 */
return;
/* .line 1027 */
} // .end local v0 # "disableC2kCap":Z
/* :pswitch_11 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1028 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).setRadioCapabilitySwitchEnable ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setRadioCapabilitySwitchEnable(Ljava/lang/String;)Z
/* .line 1029 */
/* .local v0, "radioCapabilitySwitchEnable":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1030 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1031 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1032 */
return;
/* .line 1020 */
} // .end local v0 # "radioCapabilitySwitchEnable":Z
/* :pswitch_12 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1021 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).setPreferGprsMode ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setPreferGprsMode(Ljava/lang/String;)Z
/* .line 1022 */
/* .local v0, "preferGprsMode":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1023 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1024 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1025 */
return;
/* .line 1013 */
} // .end local v0 # "preferGprsMode":Z
/* :pswitch_13 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1014 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).setTestSimCardType ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setTestSimCardType(Ljava/lang/String;)Z
/* .line 1015 */
/* .local v0, "testSimCardType":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1016 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1017 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1018 */
return;
/* .line 1006 */
} // .end local v0 # "testSimCardType":Z
/* :pswitch_14 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1007 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).setCtIREngMode ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setCtIREngMode(Ljava/lang/String;)Z
/* .line 1008 */
/* .local v0, "ctIREngMode":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1009 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1010 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1011 */
return;
/* .line 999 */
} // .end local v0 # "ctIREngMode":Z
/* :pswitch_15 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1000 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).setSmsFormat ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setSmsFormat(Ljava/lang/String;)Z
/* .line 1001 */
/* .local v0, "smsFormat":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1002 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1003 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1004 */
return;
/* .line 992 */
} // .end local v0 # "smsFormat":Z
/* :pswitch_16 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 993 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
(( android.os.HwParcel ) p2 ).readStrongBinder ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readStrongBinder()Landroid/os/IHwBinder;
com.android.server.location.hardware.mtk.engineermode.V1_0.IEmCallback .asInterface ( v2 );
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).sendToServerWithCallBack ( v0, v2 ); // invoke-virtual {p0, v0, v2}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->sendToServerWithCallBack(Ljava/lang/String;Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback;)Z
/* .line 994 */
/* .local v0, "sendToServerWithCallBack":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 995 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 996 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 997 */
return;
/* .line 985 */
} // .end local v0 # "sendToServerWithCallBack":Z
/* :pswitch_17 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 986 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).sendToServer ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->sendToServer(Ljava/lang/String;)Z
/* .line 987 */
/* .local v0, "sendToServer":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 988 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 989 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 990 */
return;
/* .line 979 */
} // .end local v0 # "sendToServer":Z
/* :pswitch_18 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 980 */
(( android.os.HwParcel ) p2 ).readStrongBinder ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readStrongBinder()Landroid/os/IHwBinder;
com.android.server.location.hardware.mtk.engineermode.V1_0.IEmCallback .asInterface ( v0 );
(( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).setCallback ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setCallback(Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback;)V
/* .line 981 */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 982 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 983 */
return;
/* .line 1296 */
/* :pswitch_19 */
/* const-string/jumbo v0, "vendor.mediatek.hardware.engineermode@1.1::IEmd" */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1297 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).setEmConfigure ( v0, v2 ); // invoke-virtual {p0, v0, v2}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setEmConfigure(Ljava/lang/String;Ljava/lang/String;)Z
/* .line 1298 */
/* .local v0, "emConfigure":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1299 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1300 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1301 */
return;
/* .line 1289 */
} // .end local v0 # "emConfigure":Z
/* :pswitch_1a */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1290 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
(( android.os.HwParcel ) p2 ).readStrongBinder ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readStrongBinder()Landroid/os/IHwBinder;
com.android.server.location.hardware.mtk.engineermode.V1_0.IEmCallback .asInterface ( v2 );
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).getFilePathListWithCallBack ( v0, v2 ); // invoke-virtual {p0, v0, v2}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->getFilePathListWithCallBack(Ljava/lang/String;Lcom/android/server/location/hardware/mtk/engineermode/V1_0/IEmCallback;)Z
/* .line 1291 */
/* .local v0, "filePathListWithCallBack":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1292 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1293 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1294 */
return;
/* .line 1282 */
} // .end local v0 # "filePathListWithCallBack":Z
/* :pswitch_1b */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1283 */
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).clearItemsforRsc ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->clearItemsforRsc()Z
/* .line 1284 */
/* .local v0, "clearItemsforRsc":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1285 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1286 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1287 */
return;
/* .line 1275 */
} // .end local v0 # "clearItemsforRsc":Z
/* :pswitch_1c */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1276 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).setMoms ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setMoms(Ljava/lang/String;)Z
/* .line 1277 */
/* .local v0, "moms":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1278 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1279 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1280 */
return;
/* .line 1268 */
} // .end local v0 # "moms":Z
/* :pswitch_1d */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1269 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).setBypassService ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setBypassService(Ljava/lang/String;)Z
/* .line 1270 */
/* .local v0, "bypassService":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1271 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1272 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1273 */
return;
/* .line 1261 */
} // .end local v0 # "bypassService":Z
/* :pswitch_1e */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1262 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).setBypassDis ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setBypassDis(Ljava/lang/String;)Z
/* .line 1263 */
/* .local v0, "bypassDis":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1264 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1265 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1266 */
return;
/* .line 1254 */
} // .end local v0 # "bypassDis":Z
/* :pswitch_1f */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1255 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).setBypassEn ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setBypassEn(Ljava/lang/String;)Z
/* .line 1256 */
/* .local v0, "bypassEn":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1257 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1258 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1259 */
return;
/* .line 1247 */
} // .end local v0 # "bypassEn":Z
/* :pswitch_20 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1248 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).setEmUsbType ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setEmUsbType(Ljava/lang/String;)Z
/* .line 1249 */
/* .local v0, "emUsbType":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1250 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1251 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1252 */
return;
/* .line 1240 */
} // .end local v0 # "emUsbType":Z
/* :pswitch_21 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1241 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).setEmUsbValue ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setEmUsbValue(Ljava/lang/String;)Z
/* .line 1242 */
/* .local v0, "emUsbValue":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1243 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1244 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1245 */
return;
/* .line 1233 */
} // .end local v0 # "emUsbValue":Z
/* :pswitch_22 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1234 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).setUsbOtgSwitch ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setUsbOtgSwitch(Ljava/lang/String;)Z
/* .line 1235 */
/* .local v0, "usbOtgSwitch":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1236 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1237 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1238 */
return;
/* .line 1226 */
} // .end local v0 # "usbOtgSwitch":Z
/* :pswitch_23 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1227 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).setUsbPort ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setUsbPort(Ljava/lang/String;)Z
/* .line 1228 */
/* .local v0, "usbPort":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1229 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1230 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1231 */
return;
/* .line 1219 */
} // .end local v0 # "usbPort":Z
/* :pswitch_24 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1220 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).genMdLogFilter ( v0, v2 ); // invoke-virtual {p0, v0, v2}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->genMdLogFilter(Ljava/lang/String;Ljava/lang/String;)Z
/* .line 1221 */
/* .local v0, "genMdLogFilter":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1222 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1223 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1224 */
return;
/* .line 1212 */
} // .end local v0 # "genMdLogFilter":Z
/* :pswitch_25 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1213 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).setModemWarningEnable ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setModemWarningEnable(Ljava/lang/String;)Z
/* .line 1214 */
/* .local v0, "modemWarningEnable":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1215 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1216 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1217 */
return;
/* .line 1205 */
} // .end local v0 # "modemWarningEnable":Z
/* :pswitch_26 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1206 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).setVencDriverLogEnable ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setVencDriverLogEnable(Ljava/lang/String;)Z
/* .line 1207 */
/* .local v0, "vencDriverLogEnable":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1208 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1209 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1210 */
return;
/* .line 1198 */
} // .end local v0 # "vencDriverLogEnable":Z
/* :pswitch_27 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1199 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).setOmxCoreLogEnable ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setOmxCoreLogEnable(Ljava/lang/String;)Z
/* .line 1200 */
/* .local v0, "omxCoreLogEnable":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1201 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1202 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1203 */
return;
/* .line 1191 */
} // .end local v0 # "omxCoreLogEnable":Z
/* :pswitch_28 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1192 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).setSvpLogEnable ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setSvpLogEnable(Ljava/lang/String;)Z
/* .line 1193 */
/* .local v0, "svpLogEnable":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1194 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1195 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1196 */
return;
/* .line 1184 */
} // .end local v0 # "svpLogEnable":Z
/* :pswitch_29 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1185 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).setVdecDriverLogEnable ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setVdecDriverLogEnable(Ljava/lang/String;)Z
/* .line 1186 */
/* .local v0, "vdecDriverLogEnable":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1187 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1188 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1189 */
return;
/* .line 1177 */
} // .end local v0 # "vdecDriverLogEnable":Z
/* :pswitch_2a */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1178 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).setOmxVdecLogEnable ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setOmxVdecLogEnable(Ljava/lang/String;)Z
/* .line 1179 */
/* .local v0, "omxVdecLogEnable":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1180 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1181 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1182 */
return;
/* .line 1170 */
} // .end local v0 # "omxVdecLogEnable":Z
/* :pswitch_2b */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1171 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).setOmxVencLogEnable ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setOmxVencLogEnable(Ljava/lang/String;)Z
/* .line 1172 */
/* .local v0, "omxVencLogEnable":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1173 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1174 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1175 */
return;
/* .line 1163 */
} // .end local v0 # "omxVencLogEnable":Z
/* :pswitch_2c */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1164 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).setWcnCoreDump ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setWcnCoreDump(Ljava/lang/String;)Z
/* .line 1165 */
/* .local v0, "wcnCoreDump":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1166 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1167 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1168 */
return;
/* .line 1156 */
} // .end local v0 # "wcnCoreDump":Z
/* :pswitch_2d */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1157 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
v0 = (( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).setMdResetDelay ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setMdResetDelay(Ljava/lang/String;)Z
/* .line 1158 */
/* .local v0, "mdResetDelay":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1159 */
(( android.os.HwParcel ) p3 ).writeBool ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1160 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1161 */
return;
/* .line 1363 */
} // .end local v0 # "mdResetDelay":Z
/* :sswitch_0 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1364 */
(( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).notifySyspropsChanged ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->notifySyspropsChanged()V
/* .line 1365 */
return;
/* .line 1356 */
/* :sswitch_1 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1357 */
(( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).getDebugInfo ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->getDebugInfo()Landroid/hidl/base/V1_0/DebugInfo;
/* .line 1358 */
/* .local v0, "debugInfo":Landroid/hidl/base/V1_0/DebugInfo; */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1359 */
(( android.hidl.base.V1_0.DebugInfo ) v0 ).writeToParcel ( p3 ); // invoke-virtual {v0, p3}, Landroid/hidl/base/V1_0/DebugInfo;->writeToParcel(Landroid/os/HwParcel;)V
/* .line 1360 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1361 */
return;
/* .line 1350 */
} // .end local v0 # "debugInfo":Landroid/hidl/base/V1_0/DebugInfo;
/* :sswitch_2 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1351 */
(( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).ping ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->ping()V
/* .line 1352 */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1353 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1354 */
return;
/* .line 1346 */
/* :sswitch_3 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1347 */
(( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).setHALInstrumentation ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->setHALInstrumentation()V
/* .line 1348 */
return;
/* .line 1325 */
/* :sswitch_4 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1326 */
(( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).getHashChain ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->getHashChain()Ljava/util/ArrayList;
/* .line 1327 */
/* .local v0, "hashChain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;" */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1328 */
/* new-instance v2, Landroid/os/HwBlob; */
/* const/16 v3, 0x10 */
/* invoke-direct {v2, v3}, Landroid/os/HwBlob;-><init>(I)V */
/* .line 1329 */
/* .local v2, "hwBlob":Landroid/os/HwBlob; */
v3 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* .line 1330 */
/* .local v3, "size":I */
/* const-wide/16 v4, 0x8 */
(( android.os.HwBlob ) v2 ).putInt32 ( v4, v5, v3 ); // invoke-virtual {v2, v4, v5, v3}, Landroid/os/HwBlob;->putInt32(JI)V
/* .line 1331 */
/* const-wide/16 v4, 0xc */
(( android.os.HwBlob ) v2 ).putBool ( v4, v5, v1 ); // invoke-virtual {v2, v4, v5, v1}, Landroid/os/HwBlob;->putBool(JZ)V
/* .line 1332 */
/* new-instance v1, Landroid/os/HwBlob; */
/* mul-int/lit8 v4, v3, 0x20 */
/* invoke-direct {v1, v4}, Landroid/os/HwBlob;-><init>(I)V */
/* .line 1333 */
/* .local v1, "hwBlob2":Landroid/os/HwBlob; */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i3":I */
} // :goto_0
/* if-ge v4, v3, :cond_1 */
/* .line 1334 */
/* mul-int/lit8 v5, v4, 0x20 */
/* int-to-long v5, v5 */
/* .line 1335 */
/* .local v5, "j":J */
(( java.util.ArrayList ) v0 ).get ( v4 ); // invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v7, [B */
/* .line 1336 */
/* .local v7, "bArr":[B */
if ( v7 != null) { // if-eqz v7, :cond_0
/* array-length v8, v7 */
/* const/16 v9, 0x20 */
/* if-ne v8, v9, :cond_0 */
/* .line 1339 */
(( android.os.HwBlob ) v1 ).putInt8Array ( v5, v6, v7 ); // invoke-virtual {v1, v5, v6, v7}, Landroid/os/HwBlob;->putInt8Array(J[B)V
/* .line 1333 */
} // .end local v5 # "j":J
} // .end local v7 # "bArr":[B
/* add-int/lit8 v4, v4, 0x1 */
/* .line 1337 */
/* .restart local v5 # "j":J */
/* .restart local v7 # "bArr":[B */
} // :cond_0
/* new-instance v8, Ljava/lang/IllegalArgumentException; */
final String v9 = "Array element is not of the expected length"; // const-string v9, "Array element is not of the expected length"
/* invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v8 */
/* .line 1341 */
} // .end local v4 # "i3":I
} // .end local v5 # "j":J
} // .end local v7 # "bArr":[B
} // :cond_1
/* const-wide/16 v4, 0x0 */
(( android.os.HwBlob ) v2 ).putBlob ( v4, v5, v1 ); // invoke-virtual {v2, v4, v5, v1}, Landroid/os/HwBlob;->putBlob(JLandroid/os/HwBlob;)V
/* .line 1342 */
(( android.os.HwParcel ) p3 ).writeBuffer ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeBuffer(Landroid/os/HwBlob;)V
/* .line 1343 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1344 */
return;
/* .line 1318 */
} // .end local v0 # "hashChain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
} // .end local v1 # "hwBlob2":Landroid/os/HwBlob;
} // .end local v2 # "hwBlob":Landroid/os/HwBlob;
} // .end local v3 # "size":I
/* :sswitch_5 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1319 */
(( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).interfaceDescriptor ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->interfaceDescriptor()Ljava/lang/String;
/* .line 1320 */
/* .local v0, "interfaceDescriptor":Ljava/lang/String; */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1321 */
(( android.os.HwParcel ) p3 ).writeString ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 1322 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1323 */
return;
/* .line 1312 */
} // .end local v0 # "interfaceDescriptor":Ljava/lang/String;
/* :sswitch_6 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1313 */
(( android.os.HwParcel ) p2 ).readNativeHandle ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readNativeHandle()Landroid/os/NativeHandle;
(( android.os.HwParcel ) p2 ).readStringVector ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readStringVector()Ljava/util/ArrayList;
(( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).debug ( v0, v2 ); // invoke-virtual {p0, v0, v2}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->debug(Landroid/os/NativeHandle;Ljava/util/ArrayList;)V
/* .line 1314 */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1315 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1316 */
return;
/* .line 1305 */
/* :sswitch_7 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1306 */
(( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).interfaceChain ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->interfaceChain()Ljava/util/ArrayList;
/* .line 1307 */
/* .local v0, "interfaceChain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1308 */
(( android.os.HwParcel ) p3 ).writeStringVector ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeStringVector(Ljava/util/ArrayList;)V
/* .line 1309 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1310 */
return;
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_18 */
/* :pswitch_17 */
/* :pswitch_16 */
/* :pswitch_15 */
/* :pswitch_14 */
/* :pswitch_13 */
/* :pswitch_12 */
/* :pswitch_11 */
/* :pswitch_10 */
/* :pswitch_f */
/* :pswitch_e */
/* :pswitch_d */
/* :pswitch_c */
/* :pswitch_b */
/* :pswitch_a */
/* :pswitch_9 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
/* :pswitch_data_1 */
/* .packed-switch 0x1a */
/* :pswitch_2d */
/* :pswitch_2c */
/* :pswitch_2b */
/* :pswitch_2a */
/* :pswitch_29 */
/* :pswitch_28 */
/* :pswitch_27 */
/* :pswitch_26 */
/* :pswitch_25 */
/* :pswitch_24 */
/* :pswitch_23 */
/* :pswitch_22 */
/* :pswitch_21 */
/* :pswitch_20 */
/* :pswitch_1f */
/* :pswitch_1e */
/* :pswitch_1d */
/* :pswitch_1c */
/* :pswitch_1b */
/* :pswitch_1a */
/* :pswitch_19 */
} // .end packed-switch
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0xf43484e -> :sswitch_7 */
/* 0xf444247 -> :sswitch_6 */
/* 0xf445343 -> :sswitch_5 */
/* 0xf485348 -> :sswitch_4 */
/* 0xf494e54 -> :sswitch_3 */
/* 0xf504e47 -> :sswitch_2 */
/* 0xf524546 -> :sswitch_1 */
/* 0xf535953 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
public final void ping ( ) {
/* .locals 0 */
/* .line 1374 */
return;
} // .end method
public android.os.IHwInterface queryLocalInterface ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "str" # Ljava/lang/String; */
/* .line 1377 */
/* const-string/jumbo v0, "vendor.mediatek.hardware.engineermode@1.1::IEmd" */
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1378 */
/* .line 1380 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void registerAsService ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "str" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1384 */
(( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).registerService ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->registerService(Ljava/lang/String;)V
/* .line 1385 */
return;
} // .end method
public final void setHALInstrumentation ( ) {
/* .locals 0 */
/* .line 1389 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 1392 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( com.android.server.location.hardware.mtk.engineermode.V1_1.IEmd$Stub ) p0 ).interfaceDescriptor ( ); // invoke-virtual {p0}, Lcom/android/server/location/hardware/mtk/engineermode/V1_1/IEmd$Stub;->interfaceDescriptor()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "@Stub"; // const-string v1, "@Stub"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
public final Boolean unlinkToDeath ( android.os.IHwBinder$DeathRecipient p0 ) {
/* .locals 1 */
/* .param p1, "deathRecipient" # Landroid/os/IHwBinder$DeathRecipient; */
/* .line 1397 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
