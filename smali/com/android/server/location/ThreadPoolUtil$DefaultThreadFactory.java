class com.android.server.location.ThreadPoolUtil$DefaultThreadFactory implements java.util.concurrent.ThreadFactory {
	 /* .source "ThreadPoolUtil.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/location/ThreadPoolUtil; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "DefaultThreadFactory" */
} // .end annotation
/* # static fields */
private static final java.util.concurrent.atomic.AtomicInteger POOL_NUMBER;
/* # instance fields */
private final java.lang.ThreadGroup group;
private final java.lang.String namePrefix;
private final java.util.concurrent.atomic.AtomicInteger threadNumber;
private final Integer threadPriority;
/* # direct methods */
static com.android.server.location.ThreadPoolUtil$DefaultThreadFactory ( ) {
/* .locals 2 */
/* .line 131 */
/* new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger; */
int v1 = 1; // const/4 v1, 0x1
/* invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V */
return;
} // .end method
private com.android.server.location.ThreadPoolUtil$DefaultThreadFactory ( ) {
/* .locals 2 */
/* .param p1, "threadPriority" # I */
/* .param p2, "threadNamePrefix" # Ljava/lang/String; */
/* .line 142 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 136 */
/* new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger; */
int v1 = 1; // const/4 v1, 0x1
/* invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V */
this.threadNumber = v0;
/* .line 143 */
/* iput p1, p0, Lcom/android/server/location/ThreadPoolUtil$DefaultThreadFactory;->threadPriority:I */
/* .line 144 */
java.lang.Thread .currentThread ( );
(( java.lang.Thread ) v0 ).getThreadGroup ( ); // invoke-virtual {v0}, Ljava/lang/Thread;->getThreadGroup()Ljava/lang/ThreadGroup;
this.group = v0;
/* .line 145 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = com.android.server.location.ThreadPoolUtil$DefaultThreadFactory.POOL_NUMBER;
v1 = (( java.util.concurrent.atomic.AtomicInteger ) v1 ).getAndIncrement ( ); // invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = "-thread-"; // const-string v1, "-thread-"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
this.namePrefix = v0;
/* .line 146 */
return;
} // .end method
 com.android.server.location.ThreadPoolUtil$DefaultThreadFactory ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/location/ThreadPoolUtil$DefaultThreadFactory;-><init>(ILjava/lang/String;)V */
return;
} // .end method
/* # virtual methods */
public java.lang.Thread newThread ( java.lang.Runnable p0 ) {
/* .locals 7 */
/* .param p1, "runnable" # Ljava/lang/Runnable; */
/* .line 150 */
/* new-instance v6, Ljava/lang/Thread; */
v1 = this.group;
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
v2 = this.namePrefix;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.threadNumber;
v2 = (( java.util.concurrent.atomic.AtomicInteger ) v2 ).getAndIncrement ( ); // invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* const-wide/16 v4, 0x0 */
/* move-object v0, v6 */
/* move-object v2, p1 */
/* invoke-direct/range {v0 ..v5}, Ljava/lang/Thread;-><init>(Ljava/lang/ThreadGroup;Ljava/lang/Runnable;Ljava/lang/String;J)V */
/* .line 151 */
/* .local v0, "t":Ljava/lang/Thread; */
v1 = (( java.lang.Thread ) v0 ).isDaemon ( ); // invoke-virtual {v0}, Ljava/lang/Thread;->isDaemon()Z
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 152 */
	 int v1 = 0; // const/4 v1, 0x0
	 (( java.lang.Thread ) v0 ).setDaemon ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/Thread;->setDaemon(Z)V
	 /* .line 154 */
} // :cond_0
/* iget v1, p0, Lcom/android/server/location/ThreadPoolUtil$DefaultThreadFactory;->threadPriority:I */
(( java.lang.Thread ) v0 ).setPriority ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V
/* .line 155 */
} // .end method
