.class public Lcom/android/server/location/GnssCollectDataDbHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "GnssCollectDataDbHelper.java"


# static fields
.field private static final DB_NAME:Ljava/lang/String; = "gnssCollectData.db"

.field private static final DB_VERSION:I = 0x1

.field private static final SQL_CREATE_ENTRIES:Ljava/lang/String; = "CREATE TABLE IF NOT EXISTS GnssCollectData (_id INTEGER PRIMARY KEY,startTime INTEGER,TTFF INTEGER,runTime INTEGER,loseTimes INTEGER,SAPNumber INTEGER,PDRNumber INTEGER,totalNumber INTEGER,L1Top4MeanCn0 REAL,L5Top4MeanCn0 REAL,B1Top4MeanCn0 REAL)"

.field private static final SQL_DELETE_ENTRIES:Ljava/lang/String; = "DROP TABLE IF EXISTS GnssCollectData"

.field private static final STR_COMMA:Ljava/lang/String; = ","

.field private static final STR_INTEGER:Ljava/lang/String; = " INTEGER"

.field private static final STR_REAL:Ljava/lang/String; = " REAL"

.field private static mGnssCollectDataHelper:Lcom/android/server/location/GnssCollectDataDbHelper;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 42
    const/4 v0, 0x0

    const/4 v1, 0x1

    const-string v2, "gnssCollectData.db"

    invoke-direct {p0, p1, v2, v0, v1}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 43
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/android/server/location/GnssCollectDataDbHelper;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .line 35
    sget-object v0, Lcom/android/server/location/GnssCollectDataDbHelper;->mGnssCollectDataHelper:Lcom/android/server/location/GnssCollectDataDbHelper;

    if-nez v0, :cond_0

    .line 36
    new-instance v0, Lcom/android/server/location/GnssCollectDataDbHelper;

    invoke-direct {v0, p0}, Lcom/android/server/location/GnssCollectDataDbHelper;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/server/location/GnssCollectDataDbHelper;->mGnssCollectDataHelper:Lcom/android/server/location/GnssCollectDataDbHelper;

    .line 38
    :cond_0
    sget-object v0, Lcom/android/server/location/GnssCollectDataDbHelper;->mGnssCollectDataHelper:Lcom/android/server/location/GnssCollectDataDbHelper;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .line 48
    const-string v0, "CREATE TABLE IF NOT EXISTS GnssCollectData (_id INTEGER PRIMARY KEY,startTime INTEGER,TTFF INTEGER,runTime INTEGER,loseTimes INTEGER,SAPNumber INTEGER,PDRNumber INTEGER,totalNumber INTEGER,L1Top4MeanCn0 REAL,L5Top4MeanCn0 REAL,B1Top4MeanCn0 REAL)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 49
    return-void
.end method

.method public onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .line 61
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/server/location/GnssCollectDataDbHelper;->onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V

    .line 62
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .line 55
    const-string v0, "DROP TABLE IF EXISTS GnssCollectData"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 56
    invoke-virtual {p0, p1}, Lcom/android/server/location/GnssCollectDataDbHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 57
    return-void
.end method
