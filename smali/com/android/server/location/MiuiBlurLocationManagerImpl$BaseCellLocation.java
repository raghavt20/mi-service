class com.android.server.location.MiuiBlurLocationManagerImpl$BaseCellLocation {
	 /* .source "MiuiBlurLocationManagerImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/location/MiuiBlurLocationManagerImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "BaseCellLocation" */
} // .end annotation
/* # instance fields */
private Integer cid;
private Integer lac;
private Double latitude;
private Double longitude;
/* # direct methods */
static Integer -$$Nest$fgetcid ( com.android.server.location.MiuiBlurLocationManagerImpl$BaseCellLocation p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->cid:I */
} // .end method
static Integer -$$Nest$fgetlac ( com.android.server.location.MiuiBlurLocationManagerImpl$BaseCellLocation p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->lac:I */
} // .end method
static Double -$$Nest$fgetlatitude ( com.android.server.location.MiuiBlurLocationManagerImpl$BaseCellLocation p0 ) { //bridge//synthethic
/* .locals 2 */
/* iget-wide v0, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->latitude:D */
/* return-wide v0 */
} // .end method
static Double -$$Nest$fgetlongitude ( com.android.server.location.MiuiBlurLocationManagerImpl$BaseCellLocation p0 ) { //bridge//synthethic
/* .locals 2 */
/* iget-wide v0, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->longitude:D */
/* return-wide v0 */
} // .end method
private com.android.server.location.MiuiBlurLocationManagerImpl$BaseCellLocation ( ) {
/* .locals 0 */
/* .line 324 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
 com.android.server.location.MiuiBlurLocationManagerImpl$BaseCellLocation ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;-><init>()V */
return;
} // .end method
/* # virtual methods */
public Boolean isValidInfo ( ) {
/* .locals 4 */
/* .line 336 */
/* iget v0, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->lac:I */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* iget v0, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->cid:I */
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* iget-wide v0, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->longitude:D */
		 /* const-wide/16 v2, 0x0 */
		 /* cmpl-double v0, v0, v2 */
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* iget-wide v0, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->latitude:D */
			 /* cmpl-double v0, v0, v2 */
			 if ( v0 != null) { // if-eqz v0, :cond_0
				 int v0 = 1; // const/4 v0, 0x1
			 } // :cond_0
			 int v0 = 0; // const/4 v0, 0x0
		 } // :goto_0
	 } // .end method
	 public void set ( Integer p0, Integer p1, Double p2, Double p3 ) {
		 /* .locals 0 */
		 /* .param p1, "lac" # I */
		 /* .param p2, "cid" # I */
		 /* .param p3, "longitude" # D */
		 /* .param p5, "latitude" # D */
		 /* .line 329 */
		 /* iput p1, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->lac:I */
		 /* .line 330 */
		 /* iput p2, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->cid:I */
		 /* .line 331 */
		 /* iput-wide p3, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->longitude:D */
		 /* .line 332 */
		 /* iput-wide p5, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->latitude:D */
		 /* .line 333 */
		 return;
	 } // .end method
	 public java.lang.String toString ( ) {
		 /* .locals 3 */
		 /* .line 341 */
		 /* new-instance v0, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v1 = "BaseCellLocation{lac="; // const-string v1, "BaseCellLocation{lac="
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget v1, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->lac:I */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 final String v1 = ", cid="; // const-string v1, ", cid="
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget v1, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->cid:I */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 final String v1 = ", longitude="; // const-string v1, ", longitude="
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget-wide v1, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->longitude:D */
		 (( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
		 final String v1 = ", latitude="; // const-string v1, ", latitude="
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget-wide v1, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->latitude:D */
		 (( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
		 /* const/16 v1, 0x7d */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 } // .end method
