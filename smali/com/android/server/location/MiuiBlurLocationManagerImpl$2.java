class com.android.server.location.MiuiBlurLocationManagerImpl$2 extends android.os.Handler {
	 /* .source "MiuiBlurLocationManagerImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/location/MiuiBlurLocationManagerImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.location.MiuiBlurLocationManagerImpl this$0; //synthetic
/* # direct methods */
 com.android.server.location.MiuiBlurLocationManagerImpl$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/location/MiuiBlurLocationManagerImpl; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 471 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 3 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 474 */
v0 = this.obj;
/* instance-of v0, v0, Landroid/location/util/identity/CallerIdentity; */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 476 */
	 v0 = this.this$0;
	 v1 = this.obj;
	 /* check-cast v1, Landroid/location/util/identity/CallerIdentity; */
	 final String v2 = "gps"; // const-string v2, "gps"
	 (( com.android.server.location.MiuiBlurLocationManagerImpl ) v0 ).handleGpsLocationChangedLocked ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->handleGpsLocationChangedLocked(Ljava/lang/String;Landroid/location/util/identity/CallerIdentity;)V
	 /* .line 478 */
} // :cond_0
return;
} // .end method
