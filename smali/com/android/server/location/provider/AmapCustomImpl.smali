.class public Lcom/android/server/location/provider/AmapCustomImpl;
.super Ljava/lang/Object;
.source "AmapCustomImpl.java"

# interfaces
.implements Lcom/android/server/location/provider/AmapCustomStub;


# static fields
.field private static DEBUG:Z = false

.field private static final TAG:Ljava/lang/String; = "AmapCustomImpl"


# instance fields
.field private mCn0s:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mUpdateSvStatusTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 37
    sget-boolean v0, Landroid/os/Build;->IS_DEBUGGABLE:Z

    sput-boolean v0, Lcom/android/server/location/provider/AmapCustomImpl;->DEBUG:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/location/provider/AmapCustomImpl;->mCn0s:Ljava/util/ArrayList;

    .line 39
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/location/provider/AmapCustomImpl;->mUpdateSvStatusTime:J

    return-void
.end method

.method private fetchGnssState(Lcom/android/server/location/provider/AbstractLocationProvider;Landroid/os/Bundle;)V
    .locals 19
    .param p1, "provider"    # Lcom/android/server/location/provider/AbstractLocationProvider;
    .param p2, "extras"    # Landroid/os/Bundle;

    .line 176
    move-object/from16 v1, p0

    move-object/from16 v0, p1

    move-object/from16 v2, p2

    if-eqz v0, :cond_1

    instance-of v3, v0, Lcom/android/server/location/provider/DelegateLocationProvider;

    if-eqz v3, :cond_1

    .line 177
    const-string v3, "mDelegate"

    const-class v4, Lcom/android/server/location/provider/AbstractLocationProvider;

    invoke-static {v0, v3, v4}, Lmiui/util/ReflectionUtils;->tryGetObjectField(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;)Lmiui/util/ObjectReference;

    move-result-object v3

    .line 179
    .local v3, "deleRef":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Lcom/android/server/location/provider/AbstractLocationProvider;>;"
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lmiui/util/ObjectReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/location/provider/AbstractLocationProvider;

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    .line 180
    .local v4, "deleProvider":Lcom/android/server/location/provider/AbstractLocationProvider;
    :goto_0
    move-object v0, v4

    move-object v3, v0

    .end local p1    # "provider":Lcom/android/server/location/provider/AbstractLocationProvider;
    .local v0, "provider":Lcom/android/server/location/provider/AbstractLocationProvider;
    goto :goto_1

    .line 183
    .end local v0    # "provider":Lcom/android/server/location/provider/AbstractLocationProvider;
    .end local v3    # "deleRef":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Lcom/android/server/location/provider/AbstractLocationProvider;>;"
    .end local v4    # "deleProvider":Lcom/android/server/location/provider/AbstractLocationProvider;
    .restart local p1    # "provider":Lcom/android/server/location/provider/AbstractLocationProvider;
    :cond_1
    move-object v3, v0

    .end local p1    # "provider":Lcom/android/server/location/provider/AbstractLocationProvider;
    .local v3, "provider":Lcom/android/server/location/provider/AbstractLocationProvider;
    :goto_1
    if-eqz v3, :cond_7

    instance-of v0, v3, Lcom/android/server/location/gnss/GnssLocationProvider;

    if-nez v0, :cond_2

    move-object/from16 p1, v3

    goto/16 :goto_5

    .line 188
    :cond_2
    move-object v0, v3

    check-cast v0, Lcom/android/server/location/gnss/GnssLocationProvider;

    const-string v4, "mLastFixTime"

    const-class v5, Ljava/lang/Long;

    invoke-static {v0, v4, v5}, Lmiui/util/ReflectionUtils;->tryGetObjectField(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;)Lmiui/util/ObjectReference;

    move-result-object v4

    .line 190
    .local v4, "fixtime":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Ljava/lang/Long;>;"
    move-object v0, v3

    check-cast v0, Lcom/android/server/location/gnss/GnssLocationProvider;

    const-string v5, "mStarted"

    const-class v6, Ljava/lang/Boolean;

    invoke-static {v0, v5, v6}, Lmiui/util/ReflectionUtils;->tryGetObjectField(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;)Lmiui/util/ObjectReference;

    move-result-object v5

    .line 193
    .local v5, "started":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Ljava/lang/Boolean;>;"
    invoke-virtual {v5}, Lmiui/util/ObjectReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    move v6, v0

    .line 194
    .local v6, "isStart":I
    const-string v0, "gnss_status"

    invoke-virtual {v2, v0, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 195
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v7

    invoke-virtual {v4}, Lmiui/util/ObjectReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    sub-long/2addr v7, v9

    .line 196
    .local v7, "deltaTime":J
    const-string v0, "gnss_last_report_second"

    invoke-virtual {v2, v0, v7, v8}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 198
    iget-object v9, v1, Lcom/android/server/location/provider/AmapCustomImpl;->mCn0s:Ljava/util/ArrayList;

    monitor-enter v9

    .line 199
    const/4 v0, -0x1

    .line 200
    .local v0, "over0Cnt":I
    const/4 v10, -0x1

    .line 201
    .local v10, "over20Cnt":I
    const/4 v11, -0x1

    .line 202
    .local v11, "svCnt":I
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v12

    iget-wide v14, v1, Lcom/android/server/location/provider/AmapCustomImpl;->mUpdateSvStatusTime:J

    sub-long/2addr v12, v14

    .line 203
    .local v12, "deltaMs":J
    const/4 v14, 0x1

    if-ne v6, v14, :cond_6

    const-wide/16 v14, 0x1388

    cmp-long v14, v12, v14

    if-gtz v14, :cond_6

    .line 204
    const/4 v0, 0x0

    .line 205
    const/4 v10, 0x0

    .line 206
    iget-object v14, v1, Lcom/android/server/location/provider/AmapCustomImpl;->mCn0s:Ljava/util/ArrayList;

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v14

    move v11, v14

    .line 207
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_2
    if-ge v14, v11, :cond_5

    .line 208
    iget-object v15, v1, Lcom/android/server/location/provider/AmapCustomImpl;->mCn0s:Ljava/util/ArrayList;

    invoke-virtual {v15, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Float;

    invoke-virtual {v15}, Ljava/lang/Float;->floatValue()F

    move-result v15
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 209
    .local v15, "cn0":F
    move-object/from16 p1, v3

    move-object/from16 v16, v4

    .end local v3    # "provider":Lcom/android/server/location/provider/AbstractLocationProvider;
    .end local v4    # "fixtime":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Ljava/lang/Long;>;"
    .local v16, "fixtime":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Ljava/lang/Long;>;"
    .restart local p1    # "provider":Lcom/android/server/location/provider/AbstractLocationProvider;
    float-to-double v3, v15

    const-wide/16 v17, 0x0

    cmpl-double v3, v3, v17

    if-lez v3, :cond_3

    add-int/lit8 v0, v0, 0x1

    .line 210
    :cond_3
    float-to-double v3, v15

    const-wide/high16 v17, 0x4034000000000000L    # 20.0

    cmpl-double v3, v3, v17

    if-lez v3, :cond_4

    add-int/lit8 v10, v10, 0x1

    .line 207
    .end local v15    # "cn0":F
    :cond_4
    add-int/lit8 v14, v14, 0x1

    move-object/from16 v3, p1

    move-object/from16 v4, v16

    goto :goto_2

    .end local v16    # "fixtime":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Ljava/lang/Long;>;"
    .end local p1    # "provider":Lcom/android/server/location/provider/AbstractLocationProvider;
    .restart local v3    # "provider":Lcom/android/server/location/provider/AbstractLocationProvider;
    .restart local v4    # "fixtime":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Ljava/lang/Long;>;"
    :cond_5
    move-object/from16 p1, v3

    move-object/from16 v16, v4

    .end local v3    # "provider":Lcom/android/server/location/provider/AbstractLocationProvider;
    .end local v4    # "fixtime":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Ljava/lang/Long;>;"
    .restart local v16    # "fixtime":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Ljava/lang/Long;>;"
    .restart local p1    # "provider":Lcom/android/server/location/provider/AbstractLocationProvider;
    goto :goto_3

    .line 203
    .end local v14    # "i":I
    .end local v16    # "fixtime":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Ljava/lang/Long;>;"
    .end local p1    # "provider":Lcom/android/server/location/provider/AbstractLocationProvider;
    .restart local v3    # "provider":Lcom/android/server/location/provider/AbstractLocationProvider;
    .restart local v4    # "fixtime":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Ljava/lang/Long;>;"
    :cond_6
    move-object/from16 p1, v3

    move-object/from16 v16, v4

    .line 213
    .end local v3    # "provider":Lcom/android/server/location/provider/AbstractLocationProvider;
    .end local v4    # "fixtime":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Ljava/lang/Long;>;"
    .restart local v16    # "fixtime":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Ljava/lang/Long;>;"
    .restart local p1    # "provider":Lcom/android/server/location/provider/AbstractLocationProvider;
    :goto_3
    :try_start_1
    const-string v3, "satellite_all_count"

    invoke-virtual {v2, v3, v11}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 214
    const-string v3, "satellite_snr_over0_count"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 215
    const-string v3, "satellite_snr_over20_count"

    invoke-virtual {v2, v3, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 216
    .end local v0    # "over0Cnt":I
    .end local v10    # "over20Cnt":I
    .end local v11    # "svCnt":I
    .end local v12    # "deltaMs":J
    monitor-exit v9

    .line 217
    return-void

    .line 216
    .end local v16    # "fixtime":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Ljava/lang/Long;>;"
    .end local p1    # "provider":Lcom/android/server/location/provider/AbstractLocationProvider;
    .restart local v3    # "provider":Lcom/android/server/location/provider/AbstractLocationProvider;
    .restart local v4    # "fixtime":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Ljava/lang/Long;>;"
    :catchall_0
    move-exception v0

    move-object/from16 p1, v3

    move-object/from16 v16, v4

    .end local v3    # "provider":Lcom/android/server/location/provider/AbstractLocationProvider;
    .end local v4    # "fixtime":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Ljava/lang/Long;>;"
    .restart local v16    # "fixtime":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Ljava/lang/Long;>;"
    .restart local p1    # "provider":Lcom/android/server/location/provider/AbstractLocationProvider;
    :goto_4
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_4

    .line 183
    .end local v5    # "started":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Ljava/lang/Boolean;>;"
    .end local v6    # "isStart":I
    .end local v7    # "deltaTime":J
    .end local v16    # "fixtime":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Ljava/lang/Long;>;"
    .end local p1    # "provider":Lcom/android/server/location/provider/AbstractLocationProvider;
    .restart local v3    # "provider":Lcom/android/server/location/provider/AbstractLocationProvider;
    :cond_7
    move-object/from16 p1, v3

    .line 184
    .end local v3    # "provider":Lcom/android/server/location/provider/AbstractLocationProvider;
    .restart local p1    # "provider":Lcom/android/server/location/provider/AbstractLocationProvider;
    :goto_5
    const-string v0, "AmapCustomImpl"

    const-string v3, "Exception: bad argument for provider"

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    return-void
.end method

.method private fetchPowerPolicyState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "extras"    # Landroid/os/Bundle;

    .line 168
    const-string v0, "app_control"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 169
    const-string v0, "app_control_log"

    const-string v1, "The device is not supported"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    return-void
.end method

.method private fetchRegistrationState(Lcom/android/server/location/provider/LocationProviderManager;Landroid/os/Bundle;)V
    .locals 22
    .param p1, "manager"    # Lcom/android/server/location/provider/LocationProviderManager;
    .param p2, "extras"    # Landroid/os/Bundle;

    .line 92
    move-object/from16 v1, p2

    const-string v0, "mRegistrations"

    const-class v2, Landroid/util/ArrayMap;

    move-object/from16 v3, p1

    invoke-static {v3, v0, v2}, Lmiui/util/ReflectionUtils;->tryGetObjectField(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;)Lmiui/util/ObjectReference;

    move-result-object v2

    .line 94
    .local v2, "map":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Landroid/util/ArrayMap;>;"
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lmiui/util/ObjectReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/ArrayMap;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    move-object v4, v0

    .line 95
    .local v4, "registrations":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/Object;Lcom/android/server/location/provider/LocationProviderManager$Registration;>;"
    if-nez v4, :cond_1

    .line 96
    const-string v0, "AmapCustomImpl"

    const-string v5, "Exception: mRegistrations is null"

    invoke-static {v0, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    return-void

    .line 100
    :cond_1
    const/4 v5, -0x1

    .line 101
    .local v5, "isActive":I
    const/4 v6, -0x1

    .line 102
    .local v6, "permissionLevel":I
    const-wide/16 v7, -0x1

    .line 103
    .local v7, "lastReportTime":J
    const/4 v9, -0x1

    .line 104
    .local v9, "isUsingHighPower":I
    const/4 v10, -0x1

    .line 105
    .local v10, "isForeground":I
    monitor-enter v4

    .line 106
    :try_start_0
    invoke-virtual {v4}, Landroid/util/ArrayMap;->size()I

    move-result v0

    .line 107
    .local v0, "size":I
    const-string v11, "listenerHashcode"

    invoke-virtual {v1, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v11

    .line 108
    .local v11, "amapHashCode":I
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_1
    if-ge v12, v0, :cond_6

    .line 109
    invoke-virtual {v4, v12}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/android/server/location/provider/LocationProviderManager$Registration;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 110
    .local v13, "registration":Lcom/android/server/location/provider/LocationProviderManager$Registration;
    move-object/from16 v14, p0

    :try_start_1
    invoke-direct {v14, v13}, Lcom/android/server/location/provider/AmapCustomImpl;->listenerHashCode(Lcom/android/server/location/provider/LocationProviderManager$Registration;)I

    move-result v15

    if-ne v15, v11, :cond_5

    .line 111
    invoke-virtual {v13}, Lcom/android/server/location/provider/LocationProviderManager$Registration;->isActive()Z

    move-result v15

    const/16 v16, 0x0

    const/16 v17, 0x1

    if-eqz v15, :cond_2

    move/from16 v15, v17

    goto :goto_2

    :cond_2
    move/from16 v15, v16

    :goto_2
    move v5, v15

    .line 112
    invoke-virtual {v13}, Lcom/android/server/location/provider/LocationProviderManager$Registration;->getPermissionLevel()I

    move-result v15

    move v6, v15

    .line 114
    invoke-virtual {v13}, Lcom/android/server/location/provider/LocationProviderManager$Registration;->getLastDeliveredLocation()Landroid/location/Location;

    move-result-object v15
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 115
    .local v15, "lastLoc":Landroid/location/Location;
    if-eqz v15, :cond_3

    .line 116
    :try_start_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    invoke-virtual {v15}, Landroid/location/Location;->getTime()J

    move-result-wide v20
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    sub-long v7, v18, v20

    goto :goto_3

    .line 126
    .end local v0    # "size":I
    .end local v11    # "amapHashCode":I
    .end local v12    # "i":I
    .end local v13    # "registration":Lcom/android/server/location/provider/LocationProviderManager$Registration;
    .end local v15    # "lastLoc":Landroid/location/Location;
    :catchall_0
    move-exception v0

    move-object/from16 v19, v2

    goto :goto_6

    .line 119
    .restart local v0    # "size":I
    .restart local v11    # "amapHashCode":I
    .restart local v12    # "i":I
    .restart local v13    # "registration":Lcom/android/server/location/provider/LocationProviderManager$Registration;
    .restart local v15    # "lastLoc":Landroid/location/Location;
    :cond_3
    :goto_3
    move/from16 v18, v0

    .end local v0    # "size":I
    .local v18, "size":I
    :try_start_3
    const-string v0, "mIsUsingHighPower"
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-object/from16 v19, v2

    .end local v2    # "map":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Landroid/util/ArrayMap;>;"
    .local v19, "map":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Landroid/util/ArrayMap;>;"
    :try_start_4
    const-class v2, Ljava/lang/Boolean;

    invoke-static {v13, v0, v2}, Lmiui/util/ReflectionUtils;->tryGetObjectField(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;)Lmiui/util/ObjectReference;

    move-result-object v0

    .line 121
    .local v0, "highPower":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Ljava/lang/Boolean;>;"
    invoke-virtual {v0}, Lmiui/util/ObjectReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_4

    move/from16 v16, v17

    :cond_4
    move/from16 v9, v16

    .line 122
    invoke-virtual {v13}, Lcom/android/server/location/provider/LocationProviderManager$Registration;->isForeground()Z

    move-result v2

    .line 123
    .end local v10    # "isForeground":I
    .local v2, "isForeground":I
    move v10, v2

    goto :goto_4

    .line 110
    .end local v15    # "lastLoc":Landroid/location/Location;
    .end local v18    # "size":I
    .end local v19    # "map":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Landroid/util/ArrayMap;>;"
    .local v0, "size":I
    .local v2, "map":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Landroid/util/ArrayMap;>;"
    .restart local v10    # "isForeground":I
    :cond_5
    move/from16 v18, v0

    move-object/from16 v19, v2

    .line 108
    .end local v0    # "size":I
    .end local v2    # "map":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Landroid/util/ArrayMap;>;"
    .end local v13    # "registration":Lcom/android/server/location/provider/LocationProviderManager$Registration;
    .restart local v18    # "size":I
    .restart local v19    # "map":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Landroid/util/ArrayMap;>;"
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    .line 126
    .end local v11    # "amapHashCode":I
    .end local v12    # "i":I
    .end local v18    # "size":I
    .end local v19    # "map":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Landroid/util/ArrayMap;>;"
    .restart local v2    # "map":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Landroid/util/ArrayMap;>;"
    :catchall_1
    move-exception v0

    goto :goto_5

    .line 108
    .restart local v0    # "size":I
    .restart local v11    # "amapHashCode":I
    .restart local v12    # "i":I
    :cond_6
    move-object/from16 v14, p0

    move/from16 v18, v0

    move-object/from16 v19, v2

    .line 126
    .end local v0    # "size":I
    .end local v2    # "map":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Landroid/util/ArrayMap;>;"
    .end local v11    # "amapHashCode":I
    .end local v12    # "i":I
    .restart local v19    # "map":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Landroid/util/ArrayMap;>;"
    :goto_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 127
    const-string v0, "app_active"

    invoke-virtual {v1, v0, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 128
    const-string v0, "app_permission"

    invoke-virtual {v1, v0, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 129
    const-string v0, "app_last_report_second"

    invoke-virtual {v1, v0, v7, v8}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 130
    const-string v0, "app_power_mode"

    invoke-virtual {v1, v0, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 131
    const-string v0, "app_forground"

    invoke-virtual {v1, v0, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 132
    return-void

    .line 126
    .end local v19    # "map":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Landroid/util/ArrayMap;>;"
    .restart local v2    # "map":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Landroid/util/ArrayMap;>;"
    :catchall_2
    move-exception v0

    move-object/from16 v14, p0

    :goto_5
    move-object/from16 v19, v2

    .end local v2    # "map":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Landroid/util/ArrayMap;>;"
    .restart local v19    # "map":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Landroid/util/ArrayMap;>;"
    :goto_6
    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    throw v0

    :catchall_3
    move-exception v0

    goto :goto_6
.end method

.method private handleAmapGpsTimeoutCmd(Lcom/android/server/location/provider/LocationProviderManager;ILjava/lang/String;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "manager"    # Lcom/android/server/location/provider/LocationProviderManager;
    .param p2, "uid"    # I
    .param p3, "command"    # Ljava/lang/String;
    .param p4, "extras"    # Landroid/os/Bundle;

    .line 76
    invoke-direct {p0, p1, p4}, Lcom/android/server/location/provider/AmapCustomImpl;->fetchRegistrationState(Lcom/android/server/location/provider/LocationProviderManager;Landroid/os/Bundle;)V

    .line 77
    invoke-direct {p0, p4}, Lcom/android/server/location/provider/AmapCustomImpl;->fetchPowerPolicyState(Landroid/os/Bundle;)V

    .line 79
    const-string v0, "mProvider"

    const-class v1, Lcom/android/server/location/provider/MockableLocationProvider;

    invoke-static {p1, v0, v1}, Lmiui/util/ReflectionUtils;->tryGetObjectField(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;)Lmiui/util/ObjectReference;

    move-result-object v0

    .line 81
    .local v0, "mockRef":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Lcom/android/server/location/provider/MockableLocationProvider;>;"
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmiui/util/ObjectReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/location/provider/MockableLocationProvider;

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 82
    .local v1, "mockProvider":Lcom/android/server/location/provider/MockableLocationProvider;
    :goto_0
    if-eqz v1, :cond_1

    .line 83
    invoke-virtual {v1}, Lcom/android/server/location/provider/MockableLocationProvider;->isMock()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    const-string v3, "gnss_real"

    invoke-virtual {p4, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 84
    invoke-virtual {v1}, Lcom/android/server/location/provider/MockableLocationProvider;->getProvider()Lcom/android/server/location/provider/AbstractLocationProvider;

    move-result-object v2

    invoke-direct {p0, v2, p4}, Lcom/android/server/location/provider/AmapCustomImpl;->fetchGnssState(Lcom/android/server/location/provider/AbstractLocationProvider;Landroid/os/Bundle;)V

    .line 87
    :cond_1
    const-string/jumbo v2, "version"

    const-string/jumbo v3, "v2"

    invoke-virtual {p4, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    sget-boolean v2, Lcom/android/server/location/provider/AmapCustomImpl;->DEBUG:Z

    if-eqz v2, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "response: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p4}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "AmapCustomImpl"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    :cond_2
    return-void
.end method

.method private listenerHashCode(Lcom/android/server/location/provider/LocationProviderManager$Registration;)I
    .locals 7
    .param p1, "registration"    # Lcom/android/server/location/provider/LocationProviderManager$Registration;

    .line 135
    invoke-virtual {p1}, Lcom/android/server/location/provider/LocationProviderManager$Registration;->getIdentity()Landroid/location/util/identity/CallerIdentity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/location/util/identity/CallerIdentity;->getListenerId()Ljava/lang/String;

    move-result-object v0

    .line 136
    .local v0, "listenerId":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    .line 138
    .local v1, "length":I
    add-int/lit8 v2, v1, -0x1

    const/16 v3, 0x40

    invoke-virtual {v0, v3, v2}, Ljava/lang/String;->lastIndexOf(II)I

    move-result v2

    .line 139
    .local v2, "index":I
    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    sub-int v4, v1, v2

    const/4 v5, 0x1

    if-ge v4, v5, :cond_0

    goto :goto_1

    .line 143
    :cond_0
    const/4 v3, -0x1

    .line 145
    .local v3, "listenerHashCode":I
    add-int/lit8 v4, v2, 0x1

    :try_start_0
    invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v3, v4

    .line 148
    goto :goto_0

    .line 146
    :catch_0
    move-exception v4

    .line 147
    .local v4, "e":Ljava/lang/Exception;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "caught exception from Integer.parseInt "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "AmapCustomImpl"

    invoke-static {v6, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    .end local v4    # "e":Ljava/lang/Exception;
    :goto_0
    return v3

    .line 140
    .end local v3    # "listenerHashCode":I
    :cond_1
    :goto_1
    return v3
.end method


# virtual methods
.method public onSpecialExtraCommand(Lcom/android/server/location/provider/LocationProviderManager;ILjava/lang/String;Landroid/os/Bundle;)Z
    .locals 3
    .param p1, "manager"    # Lcom/android/server/location/provider/LocationProviderManager;
    .param p2, "uid"    # I
    .param p3, "command"    # Ljava/lang/String;
    .param p4, "extras"    # Landroid/os/Bundle;

    .line 55
    const-string v0, "AmapCustomImpl"

    if-eqz p3, :cond_3

    if-nez p4, :cond_0

    goto :goto_0

    .line 60
    :cond_0
    sget-boolean v1, Lcom/android/server/location/provider/AmapCustomImpl;->DEBUG:Z

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "provider "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/android/server/location/provider/LocationProviderManager;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    :cond_1
    const/4 v0, 0x0

    .line 63
    .local v0, "isHandled":Z
    invoke-static {p3, p4}, Lcom/android/server/location/gnss/map/AmapExtraCommand;->isSupported(Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 64
    const/4 v0, 0x1

    .line 66
    const-string v1, "gps"

    invoke-virtual {p1}, Lcom/android/server/location/provider/LocationProviderManager;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 67
    const-string/jumbo v1, "send_gps_timeout"

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 68
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/location/provider/AmapCustomImpl;->handleAmapGpsTimeoutCmd(Lcom/android/server/location/provider/LocationProviderManager;ILjava/lang/String;Landroid/os/Bundle;)V

    .line 71
    :cond_2
    return v0

    .line 56
    .end local v0    # "isHandled":Z
    :cond_3
    :goto_0
    const-string v1, "Exception: command/bundle is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    const/4 v0, 0x0

    return v0
.end method

.method public setLastSvStatus(Landroid/location/GnssStatus;)V
    .locals 4
    .param p1, "gnssStatus"    # Landroid/location/GnssStatus;

    .line 43
    iget-object v0, p0, Lcom/android/server/location/provider/AmapCustomImpl;->mCn0s:Ljava/util/ArrayList;

    monitor-enter v0

    .line 44
    :try_start_0
    iget-object v1, p0, Lcom/android/server/location/provider/AmapCustomImpl;->mCn0s:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 45
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Landroid/location/GnssStatus;->getSatelliteCount()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 46
    iget-object v2, p0, Lcom/android/server/location/provider/AmapCustomImpl;->mCn0s:Ljava/util/ArrayList;

    invoke-virtual {p1, v1}, Landroid/location/GnssStatus;->getCn0DbHz(I)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 45
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 48
    .end local v1    # "i":I
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/server/location/provider/AmapCustomImpl;->mUpdateSvStatusTime:J

    .line 49
    monitor-exit v0

    .line 50
    return-void

    .line 49
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
