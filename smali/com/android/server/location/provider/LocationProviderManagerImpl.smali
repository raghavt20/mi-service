.class public Lcom/android/server/location/provider/LocationProviderManagerImpl;
.super Ljava/lang/Object;
.source "LocationProviderManagerImpl.java"

# interfaces
.implements Lcom/android/server/location/provider/LocationProviderManagerStub;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static deliverToListeners(Lcom/android/server/location/provider/LocationProviderManager;Ljava/util/function/Function;)V
    .locals 5
    .param p0, "manager"    # Lcom/android/server/location/provider/LocationProviderManager;
    .param p1, "function"    # Ljava/util/function/Function;

    .line 152
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "deliverToListeners"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Ljava/util/function/Function;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lmiui/util/ReflectionUtils;->findMethodBestMatch(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 154
    .local v0, "method":Ljava/lang/reflect/Method;
    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 157
    nop

    .end local v0    # "method":Ljava/lang/reflect/Method;
    goto :goto_0

    .line 155
    :catch_0
    move-exception v0

    .line 156
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "LocationManagerService"

    const-string v2, "deliverToListeners exception!"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 158
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method static synthetic lambda$mayNotifyGpsBlurListener$1(Landroid/location/LocationResult;Lcom/android/server/location/provider/LocationProviderManager$Registration;)Lcom/android/internal/listeners/ListenerExecutor$ListenerOperation;
    .locals 3
    .param p0, "locationResult"    # Landroid/location/LocationResult;
    .param p1, "registration"    # Lcom/android/server/location/provider/LocationProviderManager$Registration;

    .line 139
    :try_start_0
    invoke-static {}, Lcom/android/server/location/MiuiBlurLocationManagerStub;->get()Lcom/android/server/location/MiuiBlurLocationManagerStub;

    move-result-object v0

    invoke-virtual {p1}, Lcom/android/server/location/provider/LocationProviderManager$Registration;->getIdentity()Landroid/location/util/identity/CallerIdentity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/location/MiuiBlurLocationManagerStub;->isBlurLocationMode(Landroid/location/util/identity/CallerIdentity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    invoke-virtual {p1, p0}, Lcom/android/server/location/provider/LocationProviderManager$Registration;->acceptLocationChange(Landroid/location/LocationResult;)Lcom/android/internal/listeners/ListenerExecutor$ListenerOperation;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 144
    :cond_0
    goto :goto_0

    .line 142
    :catch_0
    move-exception v0

    .line 143
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "LocationManagerService"

    const-string v2, "reflect exception!"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 145
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    const/4 v0, 0x0

    return-object v0
.end method

.method static synthetic lambda$onReportBlurLocation$0(Landroid/location/util/identity/CallerIdentity;[ZLandroid/location/LocationResult;Lcom/android/server/location/provider/LocationProviderManager$Registration;)Lcom/android/internal/listeners/ListenerExecutor$ListenerOperation;
    .locals 3
    .param p0, "identity"    # Landroid/location/util/identity/CallerIdentity;
    .param p1, "hasBlurApp"    # [Z
    .param p2, "locationResult"    # Landroid/location/LocationResult;
    .param p3, "registration"    # Lcom/android/server/location/provider/LocationProviderManager$Registration;

    .line 113
    :try_start_0
    invoke-virtual {p3}, Lcom/android/server/location/provider/LocationProviderManager$Registration;->getIdentity()Landroid/location/util/identity/CallerIdentity;

    move-result-object v0

    .line 114
    .local v0, "regIdentity":Landroid/location/util/identity/CallerIdentity;
    invoke-virtual {v0}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 115
    invoke-virtual {v0}, Landroid/location/util/identity/CallerIdentity;->getUid()I

    move-result v1

    invoke-virtual {p0}, Landroid/location/util/identity/CallerIdentity;->getUid()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 116
    const/4 v1, 0x0

    const/4 v2, 0x1

    aput-boolean v2, p1, v1

    .line 117
    invoke-virtual {p3, p2}, Lcom/android/server/location/provider/LocationProviderManager$Registration;->acceptLocationChange(Landroid/location/LocationResult;)Lcom/android/internal/listeners/ListenerExecutor$ListenerOperation;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 121
    .end local v0    # "regIdentity":Landroid/location/util/identity/CallerIdentity;
    :cond_0
    goto :goto_0

    .line 119
    :catch_0
    move-exception v0

    .line 120
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "LocationManagerService"

    const-string v2, "reflect exception!"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 122
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    const/4 v0, 0x0

    return-object v0
.end method


# virtual methods
.method public mayNotifyGpsBlurListener(Lcom/android/server/location/provider/LocationProviderManager;Landroid/location/LocationResult;Ljava/lang/Object;)V
    .locals 2
    .param p1, "manager"    # Lcom/android/server/location/provider/LocationProviderManager;
    .param p2, "locationResult"    # Landroid/location/LocationResult;
    .param p3, "lock"    # Ljava/lang/Object;

    .line 130
    const-string v0, "gps"

    invoke-virtual {p1}, Lcom/android/server/location/provider/LocationProviderManager;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 131
    return-void

    .line 133
    :cond_0
    sget-boolean v0, Landroid/os/Build;->IS_DEBUGGABLE:Z

    if-eqz v0, :cond_1

    .line 134
    invoke-static {p3}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/android/internal/util/Preconditions;->checkState(Z)V

    .line 137
    :cond_1
    new-instance v0, Lcom/android/server/location/provider/LocationProviderManagerImpl$$ExternalSyntheticLambda1;

    invoke-direct {v0, p2}, Lcom/android/server/location/provider/LocationProviderManagerImpl$$ExternalSyntheticLambda1;-><init>(Landroid/location/LocationResult;)V

    .line 147
    .local v0, "function":Ljava/util/function/Function;, "Ljava/util/function/Function<Lcom/android/server/location/provider/LocationProviderManager$Registration;Lcom/android/internal/listeners/ListenerExecutor$ListenerOperation<Lcom/android/server/location/provider/LocationProviderManager$LocationTransport;>;>;"
    invoke-static {p1, v0}, Lcom/android/server/location/provider/LocationProviderManagerImpl;->deliverToListeners(Lcom/android/server/location/provider/LocationProviderManager;Ljava/util/function/Function;)V

    .line 148
    return-void
.end method

.method public onForegroundChanged(IZLjava/lang/String;ZLandroid/location/util/identity/CallerIdentity;)V
    .locals 9
    .param p1, "uid"    # I
    .param p2, "foreground"    # Z
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "hasLocationPermissions"    # Z
    .param p5, "identity"    # Landroid/location/util/identity/CallerIdentity;

    .line 86
    invoke-static {}, Lcom/android/server/location/LocationDumpLogStub;->getInstance()Lcom/android/server/location/LocationDumpLogStub;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "request from uid "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 87
    invoke-virtual {p5}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is now "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 88
    if-eqz p2, :cond_0

    const-string v2, "foreground"

    goto :goto_0

    :cond_0
    const-string v2, "background"

    :goto_0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 86
    const/4 v2, 0x1

    invoke-interface {v0, v2, v1}, Lcom/android/server/location/LocationDumpLogStub;->addToBugreport(ILjava/lang/String;)V

    .line 90
    invoke-static {}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->getInstance()Lcom/android/server/location/gnss/GnssEventTrackingStub;

    move-result-object v3

    invoke-virtual {p5}, Landroid/location/util/identity/CallerIdentity;->getListenerId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v5

    .line 91
    invoke-virtual {p5}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;

    move-result-object v6

    .line 90
    move-object v4, p3

    move v7, p2

    move v8, p4

    invoke-interface/range {v3 .. v8}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->recordChangeToBackground(Ljava/lang/String;ILjava/lang/String;ZZ)V

    .line 93
    if-nez p2, :cond_1

    .line 94
    invoke-static {}, Lcom/android/server/location/gnss/GnssCollectDataStub;->getInstance()Lcom/android/server/location/gnss/GnssCollectDataStub;

    move-result-object v0

    invoke-virtual {p5}, Landroid/location/util/identity/CallerIdentity;->getUid()I

    move-result v1

    invoke-virtual {p5}, Landroid/location/util/identity/CallerIdentity;->getPid()I

    move-result v2

    .line 95
    invoke-virtual {p5}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 94
    invoke-interface {v0, v1, v2, v3}, Lcom/android/server/location/gnss/GnssCollectDataStub;->saveUngrantedBackPermission(IILjava/lang/String;)V

    .line 97
    :cond_1
    return-void
.end method

.method public onMockModeChanged(ZLjava/lang/String;Lcom/android/server/location/provider/MockableLocationProvider;Lcom/android/server/location/provider/MockLocationProvider;)Z
    .locals 1
    .param p1, "flag"    # Z
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "provider"    # Lcom/android/server/location/provider/MockableLocationProvider;
    .param p4, "testProvider"    # Lcom/android/server/location/provider/MockLocationProvider;

    .line 163
    invoke-static {}, Lcom/android/server/location/GnssMockLocationOptStub;->getInstance()Lcom/android/server/location/GnssMockLocationOptStub;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/android/server/location/GnssMockLocationOptStub;->recordOrderSchedule(ZLjava/lang/String;Lcom/android/server/location/provider/MockableLocationProvider;Lcom/android/server/location/provider/MockLocationProvider;)Z

    move-result v0

    return v0
.end method

.method public onRegister(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/android/server/location/provider/MockableLocationProvider;Landroid/location/util/identity/CallerIdentity;Landroid/location/LocationRequest;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "requestInfo"    # Ljava/lang/String;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "provider"    # Lcom/android/server/location/provider/MockableLocationProvider;
    .param p5, "identity"    # Landroid/location/util/identity/CallerIdentity;
    .param p6, "request"    # Landroid/location/LocationRequest;

    .line 40
    invoke-static {}, Lcom/android/server/location/LocationDumpLogStub;->getInstance()Lcom/android/server/location/LocationDumpLogStub;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1, p2}, Lcom/android/server/location/LocationDumpLogStub;->addToBugreport(ILjava/lang/String;)V

    .line 42
    const-string v0, "gps"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 43
    invoke-virtual {p4}, Lcom/android/server/location/provider/MockableLocationProvider;->isMock()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "to sent mock intent"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "LocationManagerService"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    invoke-static {}, Lcom/android/server/location/gnss/GnssLocationProviderStub;->getInstance()Lcom/android/server/location/gnss/GnssLocationProviderStub;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p5}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " gps is mock"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/android/server/location/gnss/GnssLocationProviderStub;->notifyCallerName(Ljava/lang/String;)V

    .line 46
    invoke-static {}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecoveryStub;->getInstance()Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecoveryStub;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecoveryStub;->diagnosticMockLocation()V

    goto :goto_0

    .line 48
    :cond_0
    invoke-static {}, Lcom/android/server/location/gnss/GnssLocationProviderStub;->getInstance()Lcom/android/server/location/gnss/GnssLocationProviderStub;

    move-result-object v0

    invoke-virtual {p5}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/android/server/location/gnss/GnssLocationProviderStub;->notifyCallerName(Ljava/lang/String;)V

    .line 51
    :cond_1
    :goto_0
    invoke-static {}, Lcom/android/server/location/MiuiBlurLocationManagerStub;->get()Lcom/android/server/location/MiuiBlurLocationManagerStub;

    move-result-object v0

    invoke-virtual {p5}, Landroid/location/util/identity/CallerIdentity;->getUid()I

    move-result v1

    invoke-virtual {p5}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/android/server/location/MiuiBlurLocationManagerStub;->isBlurLocationMode(ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 52
    invoke-static {}, Lcom/android/server/location/gnss/GnssLocationProviderStub;->getInstance()Lcom/android/server/location/gnss/GnssLocationProviderStub;

    move-result-object v0

    invoke-virtual {p5}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "blurLocation_notify is on"

    invoke-interface {v0, v1, v2}, Lcom/android/server/location/gnss/GnssLocationProviderStub;->notifyCallerName(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    :cond_2
    invoke-static {}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->getInstance()Lcom/android/server/location/gnss/GnssEventTrackingStub;

    move-result-object v3

    invoke-virtual {p5}, Landroid/location/util/identity/CallerIdentity;->getListenerId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v5

    .line 56
    invoke-virtual {p5}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p6}, Landroid/location/LocationRequest;->getIntervalMillis()J

    move-result-wide v7

    .line 55
    move-object v4, p3

    invoke-interface/range {v3 .. v8}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->recordRequest(Ljava/lang/String;ILjava/lang/String;J)V

    .line 58
    invoke-static {}, Lcom/android/server/location/GnssMockLocationOptStub;->getInstance()Lcom/android/server/location/GnssMockLocationOptStub;

    move-result-object v0

    invoke-interface {v0, p1, p5, p3, p4}, Lcom/android/server/location/GnssMockLocationOptStub;->handleNaviBatRegisteration(Landroid/content/Context;Landroid/location/util/identity/CallerIdentity;Ljava/lang/String;Lcom/android/server/location/provider/MockableLocationProvider;)V

    .line 59
    return-void
.end method

.method public onReportBlurLocation(Lcom/android/server/location/provider/LocationProviderManager;Landroid/location/LocationResult;Landroid/location/util/identity/CallerIdentity;Ljava/lang/Object;)Z
    .locals 3
    .param p1, "manager"    # Lcom/android/server/location/provider/LocationProviderManager;
    .param p2, "locationResult"    # Landroid/location/LocationResult;
    .param p3, "identity"    # Landroid/location/util/identity/CallerIdentity;
    .param p4, "lock"    # Ljava/lang/Object;

    .line 102
    const-string v0, "gps"

    invoke-virtual {p1}, Lcom/android/server/location/provider/LocationProviderManager;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 103
    return v1

    .line 105
    :cond_0
    sget-boolean v0, Landroid/os/Build;->IS_DEBUGGABLE:Z

    if-eqz v0, :cond_1

    .line 106
    invoke-static {p4}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/android/internal/util/Preconditions;->checkState(Z)V

    .line 109
    :cond_1
    const/4 v0, 0x1

    new-array v0, v0, [Z

    aput-boolean v1, v0, v1

    .line 111
    .local v0, "hasBlurApp":[Z
    new-instance v2, Lcom/android/server/location/provider/LocationProviderManagerImpl$$ExternalSyntheticLambda0;

    invoke-direct {v2, p3, v0, p2}, Lcom/android/server/location/provider/LocationProviderManagerImpl$$ExternalSyntheticLambda0;-><init>(Landroid/location/util/identity/CallerIdentity;[ZLandroid/location/LocationResult;)V

    .line 124
    .local v2, "function":Ljava/util/function/Function;, "Ljava/util/function/Function<Lcom/android/server/location/provider/LocationProviderManager$Registration;Lcom/android/internal/listeners/ListenerExecutor$ListenerOperation<Lcom/android/server/location/provider/LocationProviderManager$LocationTransport;>;>;"
    invoke-static {p1, v2}, Lcom/android/server/location/provider/LocationProviderManagerImpl;->deliverToListeners(Lcom/android/server/location/provider/LocationProviderManager;Ljava/util/function/Function;)V

    .line 125
    aget-boolean v1, v0, v1

    return v1
.end method

.method public onUnregister(Ljava/lang/String;Ljava/lang/String;Lcom/android/server/location/provider/MockableLocationProvider;Landroid/location/util/identity/CallerIdentity;ZZ)V
    .locals 4
    .param p1, "removeInfo"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "provider"    # Lcom/android/server/location/provider/MockableLocationProvider;
    .param p4, "identity"    # Landroid/location/util/identity/CallerIdentity;
    .param p5, "hasLocationPermissions"    # Z
    .param p6, "foreground"    # Z

    .line 65
    invoke-static {}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeStub;->getInstance()Lcom/android/server/location/gnss/hal/GnssPowerOptimizeStub;

    move-result-object v0

    invoke-virtual {p4}, Landroid/location/util/identity/CallerIdentity;->getListenerId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeStub;->removeLocationRequestId(I)V

    .line 67
    const-string v0, "gps"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p3}, Lcom/android/server/location/provider/MockableLocationProvider;->isMock()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "to sent remove mock intent"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "LocationManagerService"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    invoke-static {}, Lcom/android/server/location/gnss/GnssLocationProviderStub;->getInstance()Lcom/android/server/location/gnss/GnssLocationProviderStub;

    move-result-object v0

    const-string v1, "remove mock intent"

    invoke-interface {v0, v1}, Lcom/android/server/location/gnss/GnssLocationProviderStub;->notifyCallerName(Ljava/lang/String;)V

    .line 71
    :cond_0
    invoke-static {}, Lcom/android/server/location/MiuiBlurLocationManagerStub;->get()Lcom/android/server/location/MiuiBlurLocationManagerStub;

    move-result-object v0

    invoke-virtual {p4}, Landroid/location/util/identity/CallerIdentity;->getUid()I

    move-result v1

    invoke-virtual {p4}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/android/server/location/MiuiBlurLocationManagerStub;->isBlurLocationMode(ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 72
    invoke-static {}, Lcom/android/server/location/gnss/GnssLocationProviderStub;->getInstance()Lcom/android/server/location/gnss/GnssLocationProviderStub;

    move-result-object v0

    invoke-virtual {p4}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "blurLocation_notify is off"

    invoke-interface {v0, v1, v2}, Lcom/android/server/location/gnss/GnssLocationProviderStub;->notifyCallerName(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    :cond_1
    invoke-static {}, Lcom/android/server/location/LocationDumpLogStub;->getInstance()Lcom/android/server/location/LocationDumpLogStub;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1, p1}, Lcom/android/server/location/LocationDumpLogStub;->addToBugreport(ILjava/lang/String;)V

    .line 77
    invoke-static {}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->getInstance()Lcom/android/server/location/gnss/GnssEventTrackingStub;

    move-result-object v0

    invoke-virtual {p4}, Landroid/location/util/identity/CallerIdentity;->getListenerId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-interface {v0, p2, v1, p6, p5}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->recordRemove(Ljava/lang/String;IZZ)V

    .line 78
    invoke-static {}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub;->getInstance()Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub;

    move-result-object v0

    invoke-virtual {p4}, Landroid/location/util/identity/CallerIdentity;->getUid()I

    move-result v1

    invoke-virtual {p4}, Landroid/location/util/identity/CallerIdentity;->getPid()I

    move-result v2

    invoke-virtual {p4}, Landroid/location/util/identity/CallerIdentity;->getListenerId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, p2, v3}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub;->remove(IILjava/lang/String;Ljava/lang/String;)V

    .line 80
    invoke-static {}, Lcom/android/server/location/GnssMockLocationOptStub;->getInstance()Lcom/android/server/location/GnssMockLocationOptStub;

    move-result-object v0

    invoke-interface {v0, p4, p2, p3}, Lcom/android/server/location/GnssMockLocationOptStub;->handleNaviBatUnregisteration(Landroid/location/util/identity/CallerIdentity;Ljava/lang/String;Lcom/android/server/location/provider/MockableLocationProvider;)V

    .line 81
    return-void
.end method
