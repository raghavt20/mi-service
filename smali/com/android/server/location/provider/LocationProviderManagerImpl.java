public class com.android.server.location.provider.LocationProviderManagerImpl implements com.android.server.location.provider.LocationProviderManagerStub {
	 /* .source "LocationProviderManagerImpl.java" */
	 /* # interfaces */
	 /* # direct methods */
	 public com.android.server.location.provider.LocationProviderManagerImpl ( ) {
		 /* .locals 0 */
		 /* .line 35 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 private static void deliverToListeners ( com.android.server.location.provider.LocationProviderManager p0, java.util.function.Function p1 ) {
		 /* .locals 5 */
		 /* .param p0, "manager" # Lcom/android/server/location/provider/LocationProviderManager; */
		 /* .param p1, "function" # Ljava/util/function/Function; */
		 /* .line 152 */
		 try { // :try_start_0
			 (( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
			 (( java.lang.Class ) v0 ).getSuperclass ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;
			 final String v1 = "deliverToListeners"; // const-string v1, "deliverToListeners"
			 int v2 = 1; // const/4 v2, 0x1
			 /* new-array v2, v2, [Ljava/lang/Class; */
			 /* const-class v3, Ljava/util/function/Function; */
			 int v4 = 0; // const/4 v4, 0x0
			 /* aput-object v3, v2, v4 */
			 miui.util.ReflectionUtils .findMethodBestMatch ( v0,v1,v2 );
			 /* .line 154 */
			 /* .local v0, "method":Ljava/lang/reflect/Method; */
			 /* filled-new-array {p1}, [Ljava/lang/Object; */
			 (( java.lang.reflect.Method ) v0 ).invoke ( p0, v1 ); // invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
			 /* :try_end_0 */
			 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* .line 157 */
			 /* nop */
		 } // .end local v0 # "method":Ljava/lang/reflect/Method;
		 /* .line 155 */
		 /* :catch_0 */
		 /* move-exception v0 */
		 /* .line 156 */
		 /* .local v0, "e":Ljava/lang/Exception; */
		 final String v1 = "LocationManagerService"; // const-string v1, "LocationManagerService"
		 final String v2 = "deliverToListeners exception!"; // const-string v2, "deliverToListeners exception!"
		 android.util.Log .e ( v1,v2,v0 );
		 /* .line 158 */
	 } // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
static com.android.internal.listeners.ListenerExecutor$ListenerOperation lambda$mayNotifyGpsBlurListener$1 ( android.location.LocationResult p0, com.android.server.location.provider.LocationProviderManager$Registration p1 ) { //synthethic
/* .locals 3 */
/* .param p0, "locationResult" # Landroid/location/LocationResult; */
/* .param p1, "registration" # Lcom/android/server/location/provider/LocationProviderManager$Registration; */
/* .line 139 */
try { // :try_start_0
	 com.android.server.location.MiuiBlurLocationManagerStub .get ( );
	 (( com.android.server.location.provider.LocationProviderManager$Registration ) p1 ).getIdentity ( ); // invoke-virtual {p1}, Lcom/android/server/location/provider/LocationProviderManager$Registration;->getIdentity()Landroid/location/util/identity/CallerIdentity;
	 v0 = 	 (( com.android.server.location.MiuiBlurLocationManagerStub ) v0 ).isBlurLocationMode ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/location/MiuiBlurLocationManagerStub;->isBlurLocationMode(Landroid/location/util/identity/CallerIdentity;)Z
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 140 */
		 (( com.android.server.location.provider.LocationProviderManager$Registration ) p1 ).acceptLocationChange ( p0 ); // invoke-virtual {p1, p0}, Lcom/android/server/location/provider/LocationProviderManager$Registration;->acceptLocationChange(Landroid/location/LocationResult;)Lcom/android/internal/listeners/ListenerExecutor$ListenerOperation;
		 /* :try_end_0 */
		 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 144 */
	 } // :cond_0
	 /* .line 142 */
	 /* :catch_0 */
	 /* move-exception v0 */
	 /* .line 143 */
	 /* .local v0, "e":Ljava/lang/Exception; */
	 final String v1 = "LocationManagerService"; // const-string v1, "LocationManagerService"
	 final String v2 = "reflect exception!"; // const-string v2, "reflect exception!"
	 android.util.Log .e ( v1,v2,v0 );
	 /* .line 145 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
static com.android.internal.listeners.ListenerExecutor$ListenerOperation lambda$onReportBlurLocation$0 ( android.location.util.identity.CallerIdentity p0, Boolean[] p1, android.location.LocationResult p2, com.android.server.location.provider.LocationProviderManager$Registration p3 ) { //synthethic
/* .locals 3 */
/* .param p0, "identity" # Landroid/location/util/identity/CallerIdentity; */
/* .param p1, "hasBlurApp" # [Z */
/* .param p2, "locationResult" # Landroid/location/LocationResult; */
/* .param p3, "registration" # Lcom/android/server/location/provider/LocationProviderManager$Registration; */
/* .line 113 */
try { // :try_start_0
(( com.android.server.location.provider.LocationProviderManager$Registration ) p3 ).getIdentity ( ); // invoke-virtual {p3}, Lcom/android/server/location/provider/LocationProviderManager$Registration;->getIdentity()Landroid/location/util/identity/CallerIdentity;
/* .line 114 */
/* .local v0, "regIdentity":Landroid/location/util/identity/CallerIdentity; */
(( android.location.util.identity.CallerIdentity ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;
(( android.location.util.identity.CallerIdentity ) p0 ).getPackageName ( ); // invoke-virtual {p0}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;
v1 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 115 */
	 v1 = 	 (( android.location.util.identity.CallerIdentity ) v0 ).getUid ( ); // invoke-virtual {v0}, Landroid/location/util/identity/CallerIdentity;->getUid()I
	 v2 = 	 (( android.location.util.identity.CallerIdentity ) p0 ).getUid ( ); // invoke-virtual {p0}, Landroid/location/util/identity/CallerIdentity;->getUid()I
	 /* if-ne v1, v2, :cond_0 */
	 /* .line 116 */
	 int v1 = 0; // const/4 v1, 0x0
	 int v2 = 1; // const/4 v2, 0x1
	 /* aput-boolean v2, p1, v1 */
	 /* .line 117 */
	 (( com.android.server.location.provider.LocationProviderManager$Registration ) p3 ).acceptLocationChange ( p2 ); // invoke-virtual {p3, p2}, Lcom/android/server/location/provider/LocationProviderManager$Registration;->acceptLocationChange(Landroid/location/LocationResult;)Lcom/android/internal/listeners/ListenerExecutor$ListenerOperation;
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 121 */
} // .end local v0 # "regIdentity":Landroid/location/util/identity/CallerIdentity;
} // :cond_0
/* .line 119 */
/* :catch_0 */
/* move-exception v0 */
/* .line 120 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "LocationManagerService"; // const-string v1, "LocationManagerService"
final String v2 = "reflect exception!"; // const-string v2, "reflect exception!"
android.util.Log .e ( v1,v2,v0 );
/* .line 122 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
/* # virtual methods */
public void mayNotifyGpsBlurListener ( com.android.server.location.provider.LocationProviderManager p0, android.location.LocationResult p1, java.lang.Object p2 ) {
/* .locals 2 */
/* .param p1, "manager" # Lcom/android/server/location/provider/LocationProviderManager; */
/* .param p2, "locationResult" # Landroid/location/LocationResult; */
/* .param p3, "lock" # Ljava/lang/Object; */
/* .line 130 */
final String v0 = "gps"; // const-string v0, "gps"
(( com.android.server.location.provider.LocationProviderManager ) p1 ).getName ( ); // invoke-virtual {p1}, Lcom/android/server/location/provider/LocationProviderManager;->getName()Ljava/lang/String;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_0 */
/* .line 131 */
return;
/* .line 133 */
} // :cond_0
/* sget-boolean v0, Landroid/os/Build;->IS_DEBUGGABLE:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 134 */
v0 = java.lang.Thread .holdsLock ( p3 );
com.android.internal.util.Preconditions .checkState ( v0 );
/* .line 137 */
} // :cond_1
/* new-instance v0, Lcom/android/server/location/provider/LocationProviderManagerImpl$$ExternalSyntheticLambda1; */
/* invoke-direct {v0, p2}, Lcom/android/server/location/provider/LocationProviderManagerImpl$$ExternalSyntheticLambda1;-><init>(Landroid/location/LocationResult;)V */
/* .line 147 */
/* .local v0, "function":Ljava/util/function/Function;, "Ljava/util/function/Function<Lcom/android/server/location/provider/LocationProviderManager$Registration;Lcom/android/internal/listeners/ListenerExecutor$ListenerOperation<Lcom/android/server/location/provider/LocationProviderManager$LocationTransport;>;>;" */
com.android.server.location.provider.LocationProviderManagerImpl .deliverToListeners ( p1,v0 );
/* .line 148 */
return;
} // .end method
public void onForegroundChanged ( Integer p0, Boolean p1, java.lang.String p2, Boolean p3, android.location.util.identity.CallerIdentity p4 ) {
/* .locals 9 */
/* .param p1, "uid" # I */
/* .param p2, "foreground" # Z */
/* .param p3, "name" # Ljava/lang/String; */
/* .param p4, "hasLocationPermissions" # Z */
/* .param p5, "identity" # Landroid/location/util/identity/CallerIdentity; */
/* .line 86 */
com.android.server.location.LocationDumpLogStub .getInstance ( );
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "request from uid "; // const-string v2, "request from uid "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " "; // const-string v2, " "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 87 */
(( android.location.util.identity.CallerIdentity ) p5 ).getPackageName ( ); // invoke-virtual {p5}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " is now "; // const-string v2, " is now "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 88 */
if ( p2 != null) { // if-eqz p2, :cond_0
final String v2 = "foreground"; // const-string v2, "foreground"
} // :cond_0
final String v2 = "background"; // const-string v2, "background"
} // :goto_0
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 86 */
int v2 = 1; // const/4 v2, 0x1
/* .line 90 */
com.android.server.location.gnss.GnssEventTrackingStub .getInstance ( );
(( android.location.util.identity.CallerIdentity ) p5 ).getListenerId ( ); // invoke-virtual {p5}, Landroid/location/util/identity/CallerIdentity;->getListenerId()Ljava/lang/String;
v5 = (( java.lang.String ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/String;->hashCode()I
/* .line 91 */
(( android.location.util.identity.CallerIdentity ) p5 ).getPackageName ( ); // invoke-virtual {p5}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;
/* .line 90 */
/* move-object v4, p3 */
/* move v7, p2 */
/* move v8, p4 */
/* invoke-interface/range {v3 ..v8}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->recordChangeToBackground(Ljava/lang/String;ILjava/lang/String;ZZ)V */
/* .line 93 */
/* if-nez p2, :cond_1 */
/* .line 94 */
com.android.server.location.gnss.GnssCollectDataStub .getInstance ( );
v1 = (( android.location.util.identity.CallerIdentity ) p5 ).getUid ( ); // invoke-virtual {p5}, Landroid/location/util/identity/CallerIdentity;->getUid()I
v2 = (( android.location.util.identity.CallerIdentity ) p5 ).getPid ( ); // invoke-virtual {p5}, Landroid/location/util/identity/CallerIdentity;->getPid()I
/* .line 95 */
(( android.location.util.identity.CallerIdentity ) p5 ).getPackageName ( ); // invoke-virtual {p5}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;
/* .line 94 */
/* .line 97 */
} // :cond_1
return;
} // .end method
public Boolean onMockModeChanged ( Boolean p0, java.lang.String p1, com.android.server.location.provider.MockableLocationProvider p2, com.android.server.location.provider.MockLocationProvider p3 ) {
/* .locals 1 */
/* .param p1, "flag" # Z */
/* .param p2, "name" # Ljava/lang/String; */
/* .param p3, "provider" # Lcom/android/server/location/provider/MockableLocationProvider; */
/* .param p4, "testProvider" # Lcom/android/server/location/provider/MockLocationProvider; */
/* .line 163 */
v0 = com.android.server.location.GnssMockLocationOptStub .getInstance ( );
} // .end method
public void onRegister ( android.content.Context p0, java.lang.String p1, java.lang.String p2, com.android.server.location.provider.MockableLocationProvider p3, android.location.util.identity.CallerIdentity p4, android.location.LocationRequest p5 ) {
/* .locals 9 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "requestInfo" # Ljava/lang/String; */
/* .param p3, "name" # Ljava/lang/String; */
/* .param p4, "provider" # Lcom/android/server/location/provider/MockableLocationProvider; */
/* .param p5, "identity" # Landroid/location/util/identity/CallerIdentity; */
/* .param p6, "request" # Landroid/location/LocationRequest; */
/* .line 40 */
com.android.server.location.LocationDumpLogStub .getInstance ( );
int v1 = 1; // const/4 v1, 0x1
/* .line 42 */
final String v0 = "gps"; // const-string v0, "gps"
v0 = (( java.lang.String ) v0 ).equals ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 43 */
v0 = (( com.android.server.location.provider.MockableLocationProvider ) p4 ).isMock ( ); // invoke-virtual {p4}, Lcom/android/server/location/provider/MockableLocationProvider;->isMock()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 44 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "to sent mock intent" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "LocationManagerService"; // const-string v1, "LocationManagerService"
android.util.Log .d ( v1,v0 );
/* .line 45 */
com.android.server.location.gnss.GnssLocationProviderStub .getInstance ( );
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( android.location.util.identity.CallerIdentity ) p5 ).getPackageName ( ); // invoke-virtual {p5}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " gps is mock"; // const-string v2, " gps is mock"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 46 */
com.android.server.location.gnss.gnssSelfRecovery.GnssSelfRecoveryStub .getInstance ( );
/* .line 48 */
} // :cond_0
com.android.server.location.gnss.GnssLocationProviderStub .getInstance ( );
(( android.location.util.identity.CallerIdentity ) p5 ).getPackageName ( ); // invoke-virtual {p5}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;
/* .line 51 */
} // :cond_1
} // :goto_0
com.android.server.location.MiuiBlurLocationManagerStub .get ( );
v1 = (( android.location.util.identity.CallerIdentity ) p5 ).getUid ( ); // invoke-virtual {p5}, Landroid/location/util/identity/CallerIdentity;->getUid()I
(( android.location.util.identity.CallerIdentity ) p5 ).getPackageName ( ); // invoke-virtual {p5}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;
v0 = (( com.android.server.location.MiuiBlurLocationManagerStub ) v0 ).isBlurLocationMode ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/location/MiuiBlurLocationManagerStub;->isBlurLocationMode(ILjava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 52 */
com.android.server.location.gnss.GnssLocationProviderStub .getInstance ( );
(( android.location.util.identity.CallerIdentity ) p5 ).getPackageName ( ); // invoke-virtual {p5}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;
final String v2 = "blurLocation_notify is on"; // const-string v2, "blurLocation_notify is on"
/* .line 55 */
} // :cond_2
com.android.server.location.gnss.GnssEventTrackingStub .getInstance ( );
(( android.location.util.identity.CallerIdentity ) p5 ).getListenerId ( ); // invoke-virtual {p5}, Landroid/location/util/identity/CallerIdentity;->getListenerId()Ljava/lang/String;
v5 = (( java.lang.String ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/String;->hashCode()I
/* .line 56 */
(( android.location.util.identity.CallerIdentity ) p5 ).getPackageName ( ); // invoke-virtual {p5}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;
(( android.location.LocationRequest ) p6 ).getIntervalMillis ( ); // invoke-virtual {p6}, Landroid/location/LocationRequest;->getIntervalMillis()J
/* move-result-wide v7 */
/* .line 55 */
/* move-object v4, p3 */
/* invoke-interface/range {v3 ..v8}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->recordRequest(Ljava/lang/String;ILjava/lang/String;J)V */
/* .line 58 */
com.android.server.location.GnssMockLocationOptStub .getInstance ( );
/* .line 59 */
return;
} // .end method
public Boolean onReportBlurLocation ( com.android.server.location.provider.LocationProviderManager p0, android.location.LocationResult p1, android.location.util.identity.CallerIdentity p2, java.lang.Object p3 ) {
/* .locals 3 */
/* .param p1, "manager" # Lcom/android/server/location/provider/LocationProviderManager; */
/* .param p2, "locationResult" # Landroid/location/LocationResult; */
/* .param p3, "identity" # Landroid/location/util/identity/CallerIdentity; */
/* .param p4, "lock" # Ljava/lang/Object; */
/* .line 102 */
final String v0 = "gps"; // const-string v0, "gps"
(( com.android.server.location.provider.LocationProviderManager ) p1 ).getName ( ); // invoke-virtual {p1}, Lcom/android/server/location/provider/LocationProviderManager;->getName()Ljava/lang/String;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 103 */
/* .line 105 */
} // :cond_0
/* sget-boolean v0, Landroid/os/Build;->IS_DEBUGGABLE:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 106 */
v0 = java.lang.Thread .holdsLock ( p4 );
com.android.internal.util.Preconditions .checkState ( v0 );
/* .line 109 */
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
/* new-array v0, v0, [Z */
/* aput-boolean v1, v0, v1 */
/* .line 111 */
/* .local v0, "hasBlurApp":[Z */
/* new-instance v2, Lcom/android/server/location/provider/LocationProviderManagerImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v2, p3, v0, p2}, Lcom/android/server/location/provider/LocationProviderManagerImpl$$ExternalSyntheticLambda0;-><init>(Landroid/location/util/identity/CallerIdentity;[ZLandroid/location/LocationResult;)V */
/* .line 124 */
/* .local v2, "function":Ljava/util/function/Function;, "Ljava/util/function/Function<Lcom/android/server/location/provider/LocationProviderManager$Registration;Lcom/android/internal/listeners/ListenerExecutor$ListenerOperation<Lcom/android/server/location/provider/LocationProviderManager$LocationTransport;>;>;" */
com.android.server.location.provider.LocationProviderManagerImpl .deliverToListeners ( p1,v2 );
/* .line 125 */
/* aget-boolean v1, v0, v1 */
} // .end method
public void onUnregister ( java.lang.String p0, java.lang.String p1, com.android.server.location.provider.MockableLocationProvider p2, android.location.util.identity.CallerIdentity p3, Boolean p4, Boolean p5 ) {
/* .locals 4 */
/* .param p1, "removeInfo" # Ljava/lang/String; */
/* .param p2, "name" # Ljava/lang/String; */
/* .param p3, "provider" # Lcom/android/server/location/provider/MockableLocationProvider; */
/* .param p4, "identity" # Landroid/location/util/identity/CallerIdentity; */
/* .param p5, "hasLocationPermissions" # Z */
/* .param p6, "foreground" # Z */
/* .line 65 */
com.android.server.location.gnss.hal.GnssPowerOptimizeStub .getInstance ( );
(( android.location.util.identity.CallerIdentity ) p4 ).getListenerId ( ); // invoke-virtual {p4}, Landroid/location/util/identity/CallerIdentity;->getListenerId()Ljava/lang/String;
v1 = (( java.lang.String ) v1 ).hashCode ( ); // invoke-virtual {v1}, Ljava/lang/String;->hashCode()I
/* .line 67 */
final String v0 = "gps"; // const-string v0, "gps"
v0 = (( java.lang.String ) v0 ).equalsIgnoreCase ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = (( com.android.server.location.provider.MockableLocationProvider ) p3 ).isMock ( ); // invoke-virtual {p3}, Lcom/android/server/location/provider/MockableLocationProvider;->isMock()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 68 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "to sent remove mock intent" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "LocationManagerService"; // const-string v1, "LocationManagerService"
android.util.Log .d ( v1,v0 );
/* .line 69 */
com.android.server.location.gnss.GnssLocationProviderStub .getInstance ( );
final String v1 = "remove mock intent"; // const-string v1, "remove mock intent"
/* .line 71 */
} // :cond_0
com.android.server.location.MiuiBlurLocationManagerStub .get ( );
v1 = (( android.location.util.identity.CallerIdentity ) p4 ).getUid ( ); // invoke-virtual {p4}, Landroid/location/util/identity/CallerIdentity;->getUid()I
(( android.location.util.identity.CallerIdentity ) p4 ).getPackageName ( ); // invoke-virtual {p4}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;
v0 = (( com.android.server.location.MiuiBlurLocationManagerStub ) v0 ).isBlurLocationMode ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/location/MiuiBlurLocationManagerStub;->isBlurLocationMode(ILjava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 72 */
com.android.server.location.gnss.GnssLocationProviderStub .getInstance ( );
(( android.location.util.identity.CallerIdentity ) p4 ).getPackageName ( ); // invoke-virtual {p4}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;
final String v2 = "blurLocation_notify is off"; // const-string v2, "blurLocation_notify is off"
/* .line 75 */
} // :cond_1
com.android.server.location.LocationDumpLogStub .getInstance ( );
int v1 = 1; // const/4 v1, 0x1
/* .line 77 */
com.android.server.location.gnss.GnssEventTrackingStub .getInstance ( );
(( android.location.util.identity.CallerIdentity ) p4 ).getListenerId ( ); // invoke-virtual {p4}, Landroid/location/util/identity/CallerIdentity;->getListenerId()Ljava/lang/String;
v1 = (( java.lang.String ) v1 ).hashCode ( ); // invoke-virtual {v1}, Ljava/lang/String;->hashCode()I
/* .line 78 */
com.android.server.location.gnss.exp.GnssBackgroundUsageOptStub .getInstance ( );
v1 = (( android.location.util.identity.CallerIdentity ) p4 ).getUid ( ); // invoke-virtual {p4}, Landroid/location/util/identity/CallerIdentity;->getUid()I
v2 = (( android.location.util.identity.CallerIdentity ) p4 ).getPid ( ); // invoke-virtual {p4}, Landroid/location/util/identity/CallerIdentity;->getPid()I
(( android.location.util.identity.CallerIdentity ) p4 ).getListenerId ( ); // invoke-virtual {p4}, Landroid/location/util/identity/CallerIdentity;->getListenerId()Ljava/lang/String;
(( com.android.server.location.gnss.exp.GnssBackgroundUsageOptStub ) v0 ).remove ( v1, v2, p2, v3 ); // invoke-virtual {v0, v1, v2, p2, v3}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub;->remove(IILjava/lang/String;Ljava/lang/String;)V
/* .line 80 */
com.android.server.location.GnssMockLocationOptStub .getInstance ( );
/* .line 81 */
return;
} // .end method
