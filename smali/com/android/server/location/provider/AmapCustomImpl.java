public class com.android.server.location.provider.AmapCustomImpl implements com.android.server.location.provider.AmapCustomStub {
	 /* .source "AmapCustomImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static Boolean DEBUG;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private java.util.ArrayList mCn0s;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/ArrayList<", */
	 /* "Ljava/lang/Float;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private Long mUpdateSvStatusTime;
/* # direct methods */
static com.android.server.location.provider.AmapCustomImpl ( ) {
/* .locals 1 */
/* .line 37 */
/* sget-boolean v0, Landroid/os/Build;->IS_DEBUGGABLE:Z */
com.android.server.location.provider.AmapCustomImpl.DEBUG = (v0!= 0);
return;
} // .end method
public com.android.server.location.provider.AmapCustomImpl ( ) {
/* .locals 2 */
/* .line 34 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 38 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mCn0s = v0;
/* .line 39 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lcom/android/server/location/provider/AmapCustomImpl;->mUpdateSvStatusTime:J */
return;
} // .end method
private void fetchGnssState ( com.android.server.location.provider.AbstractLocationProvider p0, android.os.Bundle p1 ) {
/* .locals 19 */
/* .param p1, "provider" # Lcom/android/server/location/provider/AbstractLocationProvider; */
/* .param p2, "extras" # Landroid/os/Bundle; */
/* .line 176 */
/* move-object/from16 v1, p0 */
/* move-object/from16 v0, p1 */
/* move-object/from16 v2, p2 */
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* instance-of v3, v0, Lcom/android/server/location/provider/DelegateLocationProvider; */
	 if ( v3 != null) { // if-eqz v3, :cond_1
		 /* .line 177 */
		 final String v3 = "mDelegate"; // const-string v3, "mDelegate"
		 /* const-class v4, Lcom/android/server/location/provider/AbstractLocationProvider; */
		 miui.util.ReflectionUtils .tryGetObjectField ( v0,v3,v4 );
		 /* .line 179 */
		 /* .local v3, "deleRef":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Lcom/android/server/location/provider/AbstractLocationProvider;>;" */
		 if ( v3 != null) { // if-eqz v3, :cond_0
			 (( miui.util.ObjectReference ) v3 ).get ( ); // invoke-virtual {v3}, Lmiui/util/ObjectReference;->get()Ljava/lang/Object;
			 /* check-cast v4, Lcom/android/server/location/provider/AbstractLocationProvider; */
		 } // :cond_0
		 int v4 = 0; // const/4 v4, 0x0
		 /* .line 180 */
		 /* .local v4, "deleProvider":Lcom/android/server/location/provider/AbstractLocationProvider; */
	 } // :goto_0
	 /* move-object v0, v4 */
	 /* move-object v3, v0 */
} // .end local p1 # "provider":Lcom/android/server/location/provider/AbstractLocationProvider;
/* .local v0, "provider":Lcom/android/server/location/provider/AbstractLocationProvider; */
/* .line 183 */
} // .end local v0 # "provider":Lcom/android/server/location/provider/AbstractLocationProvider;
} // .end local v3 # "deleRef":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Lcom/android/server/location/provider/AbstractLocationProvider;>;"
} // .end local v4 # "deleProvider":Lcom/android/server/location/provider/AbstractLocationProvider;
/* .restart local p1 # "provider":Lcom/android/server/location/provider/AbstractLocationProvider; */
} // :cond_1
/* move-object v3, v0 */
} // .end local p1 # "provider":Lcom/android/server/location/provider/AbstractLocationProvider;
/* .local v3, "provider":Lcom/android/server/location/provider/AbstractLocationProvider; */
} // :goto_1
if ( v3 != null) { // if-eqz v3, :cond_7
/* instance-of v0, v3, Lcom/android/server/location/gnss/GnssLocationProvider; */
/* if-nez v0, :cond_2 */
/* move-object/from16 p1, v3 */
/* goto/16 :goto_5 */
/* .line 188 */
} // :cond_2
/* move-object v0, v3 */
/* check-cast v0, Lcom/android/server/location/gnss/GnssLocationProvider; */
final String v4 = "mLastFixTime"; // const-string v4, "mLastFixTime"
/* const-class v5, Ljava/lang/Long; */
miui.util.ReflectionUtils .tryGetObjectField ( v0,v4,v5 );
/* .line 190 */
/* .local v4, "fixtime":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Ljava/lang/Long;>;" */
/* move-object v0, v3 */
/* check-cast v0, Lcom/android/server/location/gnss/GnssLocationProvider; */
final String v5 = "mStarted"; // const-string v5, "mStarted"
/* const-class v6, Ljava/lang/Boolean; */
miui.util.ReflectionUtils .tryGetObjectField ( v0,v5,v6 );
/* .line 193 */
/* .local v5, "started":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Ljava/lang/Boolean;>;" */
(( miui.util.ObjectReference ) v5 ).get ( ); // invoke-virtual {v5}, Lmiui/util/ObjectReference;->get()Ljava/lang/Object;
/* check-cast v0, Ljava/lang/Boolean; */
v0 = (( java.lang.Boolean ) v0 ).booleanValue ( ); // invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
/* move v6, v0 */
/* .line 194 */
/* .local v6, "isStart":I */
final String v0 = "gnss_status"; // const-string v0, "gnss_status"
(( android.os.Bundle ) v2 ).putInt ( v0, v6 ); // invoke-virtual {v2, v0, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 195 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v7 */
(( miui.util.ObjectReference ) v4 ).get ( ); // invoke-virtual {v4}, Lmiui/util/ObjectReference;->get()Ljava/lang/Object;
/* check-cast v0, Ljava/lang/Long; */
(( java.lang.Long ) v0 ).longValue ( ); // invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
/* move-result-wide v9 */
/* sub-long/2addr v7, v9 */
/* .line 196 */
/* .local v7, "deltaTime":J */
final String v0 = "gnss_last_report_second"; // const-string v0, "gnss_last_report_second"
(( android.os.Bundle ) v2 ).putLong ( v0, v7, v8 ); // invoke-virtual {v2, v0, v7, v8}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V
/* .line 198 */
v9 = this.mCn0s;
/* monitor-enter v9 */
/* .line 199 */
int v0 = -1; // const/4 v0, -0x1
/* .line 200 */
/* .local v0, "over0Cnt":I */
int v10 = -1; // const/4 v10, -0x1
/* .line 201 */
/* .local v10, "over20Cnt":I */
int v11 = -1; // const/4 v11, -0x1
/* .line 202 */
/* .local v11, "svCnt":I */
try { // :try_start_0
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v12 */
/* iget-wide v14, v1, Lcom/android/server/location/provider/AmapCustomImpl;->mUpdateSvStatusTime:J */
/* sub-long/2addr v12, v14 */
/* .line 203 */
/* .local v12, "deltaMs":J */
int v14 = 1; // const/4 v14, 0x1
/* if-ne v6, v14, :cond_6 */
/* const-wide/16 v14, 0x1388 */
/* cmp-long v14, v12, v14 */
/* if-gtz v14, :cond_6 */
/* .line 204 */
int v0 = 0; // const/4 v0, 0x0
/* .line 205 */
int v10 = 0; // const/4 v10, 0x0
/* .line 206 */
v14 = this.mCn0s;
v14 = (( java.util.ArrayList ) v14 ).size ( ); // invoke-virtual {v14}, Ljava/util/ArrayList;->size()I
/* move v11, v14 */
/* .line 207 */
int v14 = 0; // const/4 v14, 0x0
/* .local v14, "i":I */
} // :goto_2
/* if-ge v14, v11, :cond_5 */
/* .line 208 */
v15 = this.mCn0s;
(( java.util.ArrayList ) v15 ).get ( v14 ); // invoke-virtual {v15, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v15, Ljava/lang/Float; */
v15 = (( java.lang.Float ) v15 ).floatValue ( ); // invoke-virtual {v15}, Ljava/lang/Float;->floatValue()F
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 209 */
/* .local v15, "cn0":F */
/* move-object/from16 p1, v3 */
/* move-object/from16 v16, v4 */
} // .end local v3 # "provider":Lcom/android/server/location/provider/AbstractLocationProvider;
} // .end local v4 # "fixtime":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Ljava/lang/Long;>;"
/* .local v16, "fixtime":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Ljava/lang/Long;>;" */
/* .restart local p1 # "provider":Lcom/android/server/location/provider/AbstractLocationProvider; */
/* float-to-double v3, v15 */
/* const-wide/16 v17, 0x0 */
/* cmpl-double v3, v3, v17 */
/* if-lez v3, :cond_3 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 210 */
} // :cond_3
/* float-to-double v3, v15 */
/* const-wide/high16 v17, 0x4034000000000000L # 20.0 */
/* cmpl-double v3, v3, v17 */
/* if-lez v3, :cond_4 */
/* add-int/lit8 v10, v10, 0x1 */
/* .line 207 */
} // .end local v15 # "cn0":F
} // :cond_4
/* add-int/lit8 v14, v14, 0x1 */
/* move-object/from16 v3, p1 */
/* move-object/from16 v4, v16 */
} // .end local v16 # "fixtime":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Ljava/lang/Long;>;"
} // .end local p1 # "provider":Lcom/android/server/location/provider/AbstractLocationProvider;
/* .restart local v3 # "provider":Lcom/android/server/location/provider/AbstractLocationProvider; */
/* .restart local v4 # "fixtime":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Ljava/lang/Long;>;" */
} // :cond_5
/* move-object/from16 p1, v3 */
/* move-object/from16 v16, v4 */
} // .end local v3 # "provider":Lcom/android/server/location/provider/AbstractLocationProvider;
} // .end local v4 # "fixtime":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Ljava/lang/Long;>;"
/* .restart local v16 # "fixtime":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Ljava/lang/Long;>;" */
/* .restart local p1 # "provider":Lcom/android/server/location/provider/AbstractLocationProvider; */
/* .line 203 */
} // .end local v14 # "i":I
} // .end local v16 # "fixtime":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Ljava/lang/Long;>;"
} // .end local p1 # "provider":Lcom/android/server/location/provider/AbstractLocationProvider;
/* .restart local v3 # "provider":Lcom/android/server/location/provider/AbstractLocationProvider; */
/* .restart local v4 # "fixtime":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Ljava/lang/Long;>;" */
} // :cond_6
/* move-object/from16 p1, v3 */
/* move-object/from16 v16, v4 */
/* .line 213 */
} // .end local v3 # "provider":Lcom/android/server/location/provider/AbstractLocationProvider;
} // .end local v4 # "fixtime":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Ljava/lang/Long;>;"
/* .restart local v16 # "fixtime":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Ljava/lang/Long;>;" */
/* .restart local p1 # "provider":Lcom/android/server/location/provider/AbstractLocationProvider; */
} // :goto_3
try { // :try_start_1
final String v3 = "satellite_all_count"; // const-string v3, "satellite_all_count"
(( android.os.Bundle ) v2 ).putInt ( v3, v11 ); // invoke-virtual {v2, v3, v11}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 214 */
final String v3 = "satellite_snr_over0_count"; // const-string v3, "satellite_snr_over0_count"
(( android.os.Bundle ) v2 ).putInt ( v3, v0 ); // invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 215 */
final String v3 = "satellite_snr_over20_count"; // const-string v3, "satellite_snr_over20_count"
(( android.os.Bundle ) v2 ).putInt ( v3, v10 ); // invoke-virtual {v2, v3, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 216 */
} // .end local v0 # "over0Cnt":I
} // .end local v10 # "over20Cnt":I
} // .end local v11 # "svCnt":I
} // .end local v12 # "deltaMs":J
/* monitor-exit v9 */
/* .line 217 */
return;
/* .line 216 */
} // .end local v16 # "fixtime":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Ljava/lang/Long;>;"
} // .end local p1 # "provider":Lcom/android/server/location/provider/AbstractLocationProvider;
/* .restart local v3 # "provider":Lcom/android/server/location/provider/AbstractLocationProvider; */
/* .restart local v4 # "fixtime":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Ljava/lang/Long;>;" */
/* :catchall_0 */
/* move-exception v0 */
/* move-object/from16 p1, v3 */
/* move-object/from16 v16, v4 */
} // .end local v3 # "provider":Lcom/android/server/location/provider/AbstractLocationProvider;
} // .end local v4 # "fixtime":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Ljava/lang/Long;>;"
/* .restart local v16 # "fixtime":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Ljava/lang/Long;>;" */
/* .restart local p1 # "provider":Lcom/android/server/location/provider/AbstractLocationProvider; */
} // :goto_4
/* monitor-exit v9 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* throw v0 */
/* :catchall_1 */
/* move-exception v0 */
/* .line 183 */
} // .end local v5 # "started":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Ljava/lang/Boolean;>;"
} // .end local v6 # "isStart":I
} // .end local v7 # "deltaTime":J
} // .end local v16 # "fixtime":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Ljava/lang/Long;>;"
} // .end local p1 # "provider":Lcom/android/server/location/provider/AbstractLocationProvider;
/* .restart local v3 # "provider":Lcom/android/server/location/provider/AbstractLocationProvider; */
} // :cond_7
/* move-object/from16 p1, v3 */
/* .line 184 */
} // .end local v3 # "provider":Lcom/android/server/location/provider/AbstractLocationProvider;
/* .restart local p1 # "provider":Lcom/android/server/location/provider/AbstractLocationProvider; */
} // :goto_5
final String v0 = "AmapCustomImpl"; // const-string v0, "AmapCustomImpl"
final String v3 = "Exception: bad argument for provider"; // const-string v3, "Exception: bad argument for provider"
android.util.Log .e ( v0,v3 );
/* .line 185 */
return;
} // .end method
private void fetchPowerPolicyState ( android.os.Bundle p0 ) {
/* .locals 2 */
/* .param p1, "extras" # Landroid/os/Bundle; */
/* .line 168 */
final String v0 = "app_control"; // const-string v0, "app_control"
int v1 = -1; // const/4 v1, -0x1
(( android.os.Bundle ) p1 ).putInt ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 169 */
final String v0 = "app_control_log"; // const-string v0, "app_control_log"
final String v1 = "The device is not supported"; // const-string v1, "The device is not supported"
(( android.os.Bundle ) p1 ).putString ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 173 */
return;
} // .end method
private void fetchRegistrationState ( com.android.server.location.provider.LocationProviderManager p0, android.os.Bundle p1 ) {
/* .locals 22 */
/* .param p1, "manager" # Lcom/android/server/location/provider/LocationProviderManager; */
/* .param p2, "extras" # Landroid/os/Bundle; */
/* .line 92 */
/* move-object/from16 v1, p2 */
final String v0 = "mRegistrations"; // const-string v0, "mRegistrations"
/* const-class v2, Landroid/util/ArrayMap; */
/* move-object/from16 v3, p1 */
miui.util.ReflectionUtils .tryGetObjectField ( v3,v0,v2 );
/* .line 94 */
/* .local v2, "map":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Landroid/util/ArrayMap;>;" */
if ( v2 != null) { // if-eqz v2, :cond_0
(( miui.util.ObjectReference ) v2 ).get ( ); // invoke-virtual {v2}, Lmiui/util/ObjectReference;->get()Ljava/lang/Object;
/* check-cast v0, Landroid/util/ArrayMap; */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
/* move-object v4, v0 */
/* .line 95 */
/* .local v4, "registrations":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/Object;Lcom/android/server/location/provider/LocationProviderManager$Registration;>;" */
/* if-nez v4, :cond_1 */
/* .line 96 */
final String v0 = "AmapCustomImpl"; // const-string v0, "AmapCustomImpl"
final String v5 = "Exception: mRegistrations is null"; // const-string v5, "Exception: mRegistrations is null"
android.util.Log .e ( v0,v5 );
/* .line 97 */
return;
/* .line 100 */
} // :cond_1
int v5 = -1; // const/4 v5, -0x1
/* .line 101 */
/* .local v5, "isActive":I */
int v6 = -1; // const/4 v6, -0x1
/* .line 102 */
/* .local v6, "permissionLevel":I */
/* const-wide/16 v7, -0x1 */
/* .line 103 */
/* .local v7, "lastReportTime":J */
int v9 = -1; // const/4 v9, -0x1
/* .line 104 */
/* .local v9, "isUsingHighPower":I */
int v10 = -1; // const/4 v10, -0x1
/* .line 105 */
/* .local v10, "isForeground":I */
/* monitor-enter v4 */
/* .line 106 */
try { // :try_start_0
v0 = (( android.util.ArrayMap ) v4 ).size ( ); // invoke-virtual {v4}, Landroid/util/ArrayMap;->size()I
/* .line 107 */
/* .local v0, "size":I */
final String v11 = "listenerHashcode"; // const-string v11, "listenerHashcode"
v11 = (( android.os.Bundle ) v1 ).getInt ( v11 ); // invoke-virtual {v1, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I
/* .line 108 */
/* .local v11, "amapHashCode":I */
int v12 = 0; // const/4 v12, 0x0
/* .local v12, "i":I */
} // :goto_1
/* if-ge v12, v0, :cond_6 */
/* .line 109 */
(( android.util.ArrayMap ) v4 ).valueAt ( v12 ); // invoke-virtual {v4, v12}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;
/* check-cast v13, Lcom/android/server/location/provider/LocationProviderManager$Registration; */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_2 */
/* .line 110 */
/* .local v13, "registration":Lcom/android/server/location/provider/LocationProviderManager$Registration; */
/* move-object/from16 v14, p0 */
try { // :try_start_1
v15 = /* invoke-direct {v14, v13}, Lcom/android/server/location/provider/AmapCustomImpl;->listenerHashCode(Lcom/android/server/location/provider/LocationProviderManager$Registration;)I */
/* if-ne v15, v11, :cond_5 */
/* .line 111 */
v15 = (( com.android.server.location.provider.LocationProviderManager$Registration ) v13 ).isActive ( ); // invoke-virtual {v13}, Lcom/android/server/location/provider/LocationProviderManager$Registration;->isActive()Z
/* const/16 v16, 0x0 */
/* const/16 v17, 0x1 */
if ( v15 != null) { // if-eqz v15, :cond_2
/* move/from16 v15, v17 */
} // :cond_2
/* move/from16 v15, v16 */
} // :goto_2
/* move v5, v15 */
/* .line 112 */
v15 = (( com.android.server.location.provider.LocationProviderManager$Registration ) v13 ).getPermissionLevel ( ); // invoke-virtual {v13}, Lcom/android/server/location/provider/LocationProviderManager$Registration;->getPermissionLevel()I
/* move v6, v15 */
/* .line 114 */
(( com.android.server.location.provider.LocationProviderManager$Registration ) v13 ).getLastDeliveredLocation ( ); // invoke-virtual {v13}, Lcom/android/server/location/provider/LocationProviderManager$Registration;->getLastDeliveredLocation()Landroid/location/Location;
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* .line 115 */
/* .local v15, "lastLoc":Landroid/location/Location; */
if ( v15 != null) { // if-eqz v15, :cond_3
/* .line 116 */
try { // :try_start_2
java.lang.System .currentTimeMillis ( );
/* move-result-wide v18 */
(( android.location.Location ) v15 ).getTime ( ); // invoke-virtual {v15}, Landroid/location/Location;->getTime()J
/* move-result-wide v20 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* sub-long v7, v18, v20 */
/* .line 126 */
} // .end local v0 # "size":I
} // .end local v11 # "amapHashCode":I
} // .end local v12 # "i":I
} // .end local v13 # "registration":Lcom/android/server/location/provider/LocationProviderManager$Registration;
} // .end local v15 # "lastLoc":Landroid/location/Location;
/* :catchall_0 */
/* move-exception v0 */
/* move-object/from16 v19, v2 */
/* .line 119 */
/* .restart local v0 # "size":I */
/* .restart local v11 # "amapHashCode":I */
/* .restart local v12 # "i":I */
/* .restart local v13 # "registration":Lcom/android/server/location/provider/LocationProviderManager$Registration; */
/* .restart local v15 # "lastLoc":Landroid/location/Location; */
} // :cond_3
} // :goto_3
/* move/from16 v18, v0 */
} // .end local v0 # "size":I
/* .local v18, "size":I */
try { // :try_start_3
final String v0 = "mIsUsingHighPower"; // const-string v0, "mIsUsingHighPower"
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* move-object/from16 v19, v2 */
} // .end local v2 # "map":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Landroid/util/ArrayMap;>;"
/* .local v19, "map":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Landroid/util/ArrayMap;>;" */
try { // :try_start_4
/* const-class v2, Ljava/lang/Boolean; */
miui.util.ReflectionUtils .tryGetObjectField ( v13,v0,v2 );
/* .line 121 */
/* .local v0, "highPower":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Ljava/lang/Boolean;>;" */
(( miui.util.ObjectReference ) v0 ).get ( ); // invoke-virtual {v0}, Lmiui/util/ObjectReference;->get()Ljava/lang/Object;
/* check-cast v2, Ljava/lang/Boolean; */
v2 = (( java.lang.Boolean ) v2 ).booleanValue ( ); // invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z
if ( v2 != null) { // if-eqz v2, :cond_4
/* move/from16 v16, v17 */
} // :cond_4
/* move/from16 v9, v16 */
/* .line 122 */
v2 = (( com.android.server.location.provider.LocationProviderManager$Registration ) v13 ).isForeground ( ); // invoke-virtual {v13}, Lcom/android/server/location/provider/LocationProviderManager$Registration;->isForeground()Z
/* .line 123 */
} // .end local v10 # "isForeground":I
/* .local v2, "isForeground":I */
/* move v10, v2 */
/* .line 110 */
} // .end local v15 # "lastLoc":Landroid/location/Location;
} // .end local v18 # "size":I
} // .end local v19 # "map":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Landroid/util/ArrayMap;>;"
/* .local v0, "size":I */
/* .local v2, "map":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Landroid/util/ArrayMap;>;" */
/* .restart local v10 # "isForeground":I */
} // :cond_5
/* move/from16 v18, v0 */
/* move-object/from16 v19, v2 */
/* .line 108 */
} // .end local v0 # "size":I
} // .end local v2 # "map":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Landroid/util/ArrayMap;>;"
} // .end local v13 # "registration":Lcom/android/server/location/provider/LocationProviderManager$Registration;
/* .restart local v18 # "size":I */
/* .restart local v19 # "map":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Landroid/util/ArrayMap;>;" */
/* add-int/lit8 v12, v12, 0x1 */
/* .line 126 */
} // .end local v11 # "amapHashCode":I
} // .end local v12 # "i":I
} // .end local v18 # "size":I
} // .end local v19 # "map":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Landroid/util/ArrayMap;>;"
/* .restart local v2 # "map":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Landroid/util/ArrayMap;>;" */
/* :catchall_1 */
/* move-exception v0 */
/* .line 108 */
/* .restart local v0 # "size":I */
/* .restart local v11 # "amapHashCode":I */
/* .restart local v12 # "i":I */
} // :cond_6
/* move-object/from16 v14, p0 */
/* move/from16 v18, v0 */
/* move-object/from16 v19, v2 */
/* .line 126 */
} // .end local v0 # "size":I
} // .end local v2 # "map":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Landroid/util/ArrayMap;>;"
} // .end local v11 # "amapHashCode":I
} // .end local v12 # "i":I
/* .restart local v19 # "map":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Landroid/util/ArrayMap;>;" */
} // :goto_4
/* monitor-exit v4 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_3 */
/* .line 127 */
final String v0 = "app_active"; // const-string v0, "app_active"
(( android.os.Bundle ) v1 ).putInt ( v0, v5 ); // invoke-virtual {v1, v0, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 128 */
final String v0 = "app_permission"; // const-string v0, "app_permission"
(( android.os.Bundle ) v1 ).putInt ( v0, v6 ); // invoke-virtual {v1, v0, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 129 */
final String v0 = "app_last_report_second"; // const-string v0, "app_last_report_second"
(( android.os.Bundle ) v1 ).putLong ( v0, v7, v8 ); // invoke-virtual {v1, v0, v7, v8}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V
/* .line 130 */
final String v0 = "app_power_mode"; // const-string v0, "app_power_mode"
(( android.os.Bundle ) v1 ).putInt ( v0, v9 ); // invoke-virtual {v1, v0, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 131 */
final String v0 = "app_forground"; // const-string v0, "app_forground"
(( android.os.Bundle ) v1 ).putInt ( v0, v10 ); // invoke-virtual {v1, v0, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 132 */
return;
/* .line 126 */
} // .end local v19 # "map":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Landroid/util/ArrayMap;>;"
/* .restart local v2 # "map":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Landroid/util/ArrayMap;>;" */
/* :catchall_2 */
/* move-exception v0 */
/* move-object/from16 v14, p0 */
} // :goto_5
/* move-object/from16 v19, v2 */
} // .end local v2 # "map":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Landroid/util/ArrayMap;>;"
/* .restart local v19 # "map":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Landroid/util/ArrayMap;>;" */
} // :goto_6
try { // :try_start_5
/* monitor-exit v4 */
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_3 */
/* throw v0 */
/* :catchall_3 */
/* move-exception v0 */
} // .end method
private void handleAmapGpsTimeoutCmd ( com.android.server.location.provider.LocationProviderManager p0, Integer p1, java.lang.String p2, android.os.Bundle p3 ) {
/* .locals 4 */
/* .param p1, "manager" # Lcom/android/server/location/provider/LocationProviderManager; */
/* .param p2, "uid" # I */
/* .param p3, "command" # Ljava/lang/String; */
/* .param p4, "extras" # Landroid/os/Bundle; */
/* .line 76 */
/* invoke-direct {p0, p1, p4}, Lcom/android/server/location/provider/AmapCustomImpl;->fetchRegistrationState(Lcom/android/server/location/provider/LocationProviderManager;Landroid/os/Bundle;)V */
/* .line 77 */
/* invoke-direct {p0, p4}, Lcom/android/server/location/provider/AmapCustomImpl;->fetchPowerPolicyState(Landroid/os/Bundle;)V */
/* .line 79 */
final String v0 = "mProvider"; // const-string v0, "mProvider"
/* const-class v1, Lcom/android/server/location/provider/MockableLocationProvider; */
miui.util.ReflectionUtils .tryGetObjectField ( p1,v0,v1 );
/* .line 81 */
/* .local v0, "mockRef":Lmiui/util/ObjectReference;, "Lmiui/util/ObjectReference<Lcom/android/server/location/provider/MockableLocationProvider;>;" */
if ( v0 != null) { // if-eqz v0, :cond_0
(( miui.util.ObjectReference ) v0 ).get ( ); // invoke-virtual {v0}, Lmiui/util/ObjectReference;->get()Ljava/lang/Object;
/* check-cast v1, Lcom/android/server/location/provider/MockableLocationProvider; */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 82 */
/* .local v1, "mockProvider":Lcom/android/server/location/provider/MockableLocationProvider; */
} // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 83 */
v2 = (( com.android.server.location.provider.MockableLocationProvider ) v1 ).isMock ( ); // invoke-virtual {v1}, Lcom/android/server/location/provider/MockableLocationProvider;->isMock()Z
/* xor-int/lit8 v2, v2, 0x1 */
final String v3 = "gnss_real"; // const-string v3, "gnss_real"
(( android.os.Bundle ) p4 ).putInt ( v3, v2 ); // invoke-virtual {p4, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 84 */
(( com.android.server.location.provider.MockableLocationProvider ) v1 ).getProvider ( ); // invoke-virtual {v1}, Lcom/android/server/location/provider/MockableLocationProvider;->getProvider()Lcom/android/server/location/provider/AbstractLocationProvider;
/* invoke-direct {p0, v2, p4}, Lcom/android/server/location/provider/AmapCustomImpl;->fetchGnssState(Lcom/android/server/location/provider/AbstractLocationProvider;Landroid/os/Bundle;)V */
/* .line 87 */
} // :cond_1
/* const-string/jumbo v2, "version" */
/* const-string/jumbo v3, "v2" */
(( android.os.Bundle ) p4 ).putString ( v2, v3 ); // invoke-virtual {p4, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 88 */
/* sget-boolean v2, Lcom/android/server/location/provider/AmapCustomImpl;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_2
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "response: "; // const-string v3, "response: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.os.Bundle ) p4 ).toString ( ); // invoke-virtual {p4}, Landroid/os/Bundle;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "AmapCustomImpl"; // const-string v3, "AmapCustomImpl"
android.util.Log .d ( v3,v2 );
/* .line 89 */
} // :cond_2
return;
} // .end method
private Integer listenerHashCode ( com.android.server.location.provider.LocationProviderManager$Registration p0 ) {
/* .locals 7 */
/* .param p1, "registration" # Lcom/android/server/location/provider/LocationProviderManager$Registration; */
/* .line 135 */
(( com.android.server.location.provider.LocationProviderManager$Registration ) p1 ).getIdentity ( ); // invoke-virtual {p1}, Lcom/android/server/location/provider/LocationProviderManager$Registration;->getIdentity()Landroid/location/util/identity/CallerIdentity;
(( android.location.util.identity.CallerIdentity ) v0 ).getListenerId ( ); // invoke-virtual {v0}, Landroid/location/util/identity/CallerIdentity;->getListenerId()Ljava/lang/String;
/* .line 136 */
/* .local v0, "listenerId":Ljava/lang/String; */
v1 = (( java.lang.String ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/String;->length()I
/* .line 138 */
/* .local v1, "length":I */
/* add-int/lit8 v2, v1, -0x1 */
/* const/16 v3, 0x40 */
v2 = (( java.lang.String ) v0 ).lastIndexOf ( v3, v2 ); // invoke-virtual {v0, v3, v2}, Ljava/lang/String;->lastIndexOf(II)I
/* .line 139 */
/* .local v2, "index":I */
int v3 = -1; // const/4 v3, -0x1
/* if-eq v2, v3, :cond_1 */
/* sub-int v4, v1, v2 */
int v5 = 1; // const/4 v5, 0x1
/* if-ge v4, v5, :cond_0 */
/* .line 143 */
} // :cond_0
int v3 = -1; // const/4 v3, -0x1
/* .line 145 */
/* .local v3, "listenerHashCode":I */
/* add-int/lit8 v4, v2, 0x1 */
try { // :try_start_0
(( java.lang.String ) v0 ).substring ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;
v4 = java.lang.Integer .parseInt ( v4 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* move v3, v4 */
/* .line 148 */
/* .line 146 */
/* :catch_0 */
/* move-exception v4 */
/* .line 147 */
/* .local v4, "e":Ljava/lang/Exception; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "caught exception from Integer.parseInt "; // const-string v6, "caught exception from Integer.parseInt "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v4 ).getMessage ( ); // invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v6 = "AmapCustomImpl"; // const-string v6, "AmapCustomImpl"
android.util.Log .e ( v6,v5 );
/* .line 149 */
} // .end local v4 # "e":Ljava/lang/Exception;
} // :goto_0
/* .line 140 */
} // .end local v3 # "listenerHashCode":I
} // :cond_1
} // :goto_1
} // .end method
/* # virtual methods */
public Boolean onSpecialExtraCommand ( com.android.server.location.provider.LocationProviderManager p0, Integer p1, java.lang.String p2, android.os.Bundle p3 ) {
/* .locals 3 */
/* .param p1, "manager" # Lcom/android/server/location/provider/LocationProviderManager; */
/* .param p2, "uid" # I */
/* .param p3, "command" # Ljava/lang/String; */
/* .param p4, "extras" # Landroid/os/Bundle; */
/* .line 55 */
final String v0 = "AmapCustomImpl"; // const-string v0, "AmapCustomImpl"
if ( p3 != null) { // if-eqz p3, :cond_3
/* if-nez p4, :cond_0 */
/* .line 60 */
} // :cond_0
/* sget-boolean v1, Lcom/android/server/location/provider/AmapCustomImpl;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "provider "; // const-string v2, "provider "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.location.provider.LocationProviderManager ) p1 ).getName ( ); // invoke-virtual {p1}, Lcom/android/server/location/provider/LocationProviderManager;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " , "; // const-string v2, " , "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v1 );
/* .line 62 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
/* .line 63 */
/* .local v0, "isHandled":Z */
v1 = com.android.server.location.gnss.map.AmapExtraCommand .isSupported ( p3,p4 );
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 64 */
int v0 = 1; // const/4 v0, 0x1
/* .line 66 */
final String v1 = "gps"; // const-string v1, "gps"
(( com.android.server.location.provider.LocationProviderManager ) p1 ).getName ( ); // invoke-virtual {p1}, Lcom/android/server/location/provider/LocationProviderManager;->getName()Ljava/lang/String;
v1 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 67 */
/* const-string/jumbo v1, "send_gps_timeout" */
v1 = (( java.lang.String ) v1 ).equals ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 68 */
/* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/location/provider/AmapCustomImpl;->handleAmapGpsTimeoutCmd(Lcom/android/server/location/provider/LocationProviderManager;ILjava/lang/String;Landroid/os/Bundle;)V */
/* .line 71 */
} // :cond_2
/* .line 56 */
} // .end local v0 # "isHandled":Z
} // :cond_3
} // :goto_0
final String v1 = "Exception: command/bundle is null"; // const-string v1, "Exception: command/bundle is null"
android.util.Log .e ( v0,v1 );
/* .line 57 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void setLastSvStatus ( android.location.GnssStatus p0 ) {
/* .locals 4 */
/* .param p1, "gnssStatus" # Landroid/location/GnssStatus; */
/* .line 43 */
v0 = this.mCn0s;
/* monitor-enter v0 */
/* .line 44 */
try { // :try_start_0
v1 = this.mCn0s;
(( java.util.ArrayList ) v1 ).clear ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V
/* .line 45 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
v2 = (( android.location.GnssStatus ) p1 ).getSatelliteCount ( ); // invoke-virtual {p1}, Landroid/location/GnssStatus;->getSatelliteCount()I
/* if-ge v1, v2, :cond_0 */
/* .line 46 */
v2 = this.mCn0s;
v3 = (( android.location.GnssStatus ) p1 ).getCn0DbHz ( v1 ); // invoke-virtual {p1, v1}, Landroid/location/GnssStatus;->getCn0DbHz(I)F
java.lang.Float .valueOf ( v3 );
(( java.util.ArrayList ) v2 ).add ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 45 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 48 */
} // .end local v1 # "i":I
} // :cond_0
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v1 */
/* iput-wide v1, p0, Lcom/android/server/location/provider/AmapCustomImpl;->mUpdateSvStatusTime:J */
/* .line 49 */
/* monitor-exit v0 */
/* .line 50 */
return;
/* .line 49 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
