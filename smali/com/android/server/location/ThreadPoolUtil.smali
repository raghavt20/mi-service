.class public Lcom/android/server/location/ThreadPoolUtil;
.super Ljava/lang/Object;
.source "ThreadPoolUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/location/ThreadPoolUtil$DefaultThreadFactory;
    }
.end annotation


# static fields
.field private static volatile mThreadPoolUtil:Lcom/android/server/location/ThreadPoolUtil;


# instance fields
.field private corePoolSize:I

.field private executor:Ljava/util/concurrent/ThreadPoolExecutor;

.field private keepAliveTime:J

.field private maximumPoolSize:I

.field private unit:Ljava/util/concurrent/TimeUnit;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 19
    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/location/ThreadPoolUtil;->mThreadPoolUtil:Lcom/android/server/location/ThreadPoolUtil;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const-wide/16 v0, 0x1

    iput-wide v0, p0, Lcom/android/server/location/ThreadPoolUtil;->keepAliveTime:J

    .line 46
    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    iput-object v0, p0, Lcom/android/server/location/ThreadPoolUtil;->unit:Ljava/util/concurrent/TimeUnit;

    .line 54
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/location/ThreadPoolUtil;->corePoolSize:I

    .line 56
    iput v0, p0, Lcom/android/server/location/ThreadPoolUtil;->maximumPoolSize:I

    .line 57
    invoke-direct {p0}, Lcom/android/server/location/ThreadPoolUtil;->initThreadPool()V

    .line 58
    return-void
.end method

.method public static getInstance()Lcom/android/server/location/ThreadPoolUtil;
    .locals 2

    .line 23
    sget-object v0, Lcom/android/server/location/ThreadPoolUtil;->mThreadPoolUtil:Lcom/android/server/location/ThreadPoolUtil;

    if-nez v0, :cond_1

    .line 24
    const-class v0, Lcom/android/server/location/ThreadPoolUtil;

    monitor-enter v0

    .line 25
    :try_start_0
    sget-object v1, Lcom/android/server/location/ThreadPoolUtil;->mThreadPoolUtil:Lcom/android/server/location/ThreadPoolUtil;

    if-nez v1, :cond_0

    .line 26
    new-instance v1, Lcom/android/server/location/ThreadPoolUtil;

    invoke-direct {v1}, Lcom/android/server/location/ThreadPoolUtil;-><init>()V

    sput-object v1, Lcom/android/server/location/ThreadPoolUtil;->mThreadPoolUtil:Lcom/android/server/location/ThreadPoolUtil;

    .line 28
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 30
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/location/ThreadPoolUtil;->mThreadPoolUtil:Lcom/android/server/location/ThreadPoolUtil;

    return-object v0
.end method

.method private initThreadPool()V
    .locals 11

    .line 64
    new-instance v9, Ljava/util/concurrent/ThreadPoolExecutor;

    iget v1, p0, Lcom/android/server/location/ThreadPoolUtil;->corePoolSize:I

    iget v2, p0, Lcom/android/server/location/ThreadPoolUtil;->maximumPoolSize:I

    iget-wide v3, p0, Lcom/android/server/location/ThreadPoolUtil;->keepAliveTime:J

    iget-object v5, p0, Lcom/android/server/location/ThreadPoolUtil;->unit:Ljava/util/concurrent/TimeUnit;

    new-instance v6, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v6}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    new-instance v7, Lcom/android/server/location/ThreadPoolUtil$DefaultThreadFactory;

    const-string v0, "mi-gnss-pool-"

    const/4 v8, 0x0

    const/4 v10, 0x5

    invoke-direct {v7, v10, v0, v8}, Lcom/android/server/location/ThreadPoolUtil$DefaultThreadFactory;-><init>(ILjava/lang/String;Lcom/android/server/location/ThreadPoolUtil$DefaultThreadFactory-IA;)V

    new-instance v8, Ljava/util/concurrent/ThreadPoolExecutor$AbortPolicy;

    invoke-direct {v8}, Ljava/util/concurrent/ThreadPoolExecutor$AbortPolicy;-><init>()V

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;Ljava/util/concurrent/RejectedExecutionHandler;)V

    iput-object v9, p0, Lcom/android/server/location/ThreadPoolUtil;->executor:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 80
    return-void
.end method


# virtual methods
.method public execute(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .line 86
    if-nez p1, :cond_0

    .line 87
    return-void

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/ThreadPoolUtil;->executor:Ljava/util/concurrent/ThreadPoolExecutor;

    if-nez v0, :cond_1

    .line 90
    invoke-direct {p0}, Lcom/android/server/location/ThreadPoolUtil;->initThreadPool()V

    .line 92
    :cond_1
    iget-object v0, p0, Lcom/android/server/location/ThreadPoolUtil;->executor:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    .line 93
    return-void
.end method

.method public remove(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .line 99
    if-nez p1, :cond_0

    .line 100
    return-void

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/ThreadPoolUtil;->executor:Ljava/util/concurrent/ThreadPoolExecutor;

    if-nez v0, :cond_1

    .line 103
    invoke-direct {p0}, Lcom/android/server/location/ThreadPoolUtil;->initThreadPool()V

    .line 105
    :cond_1
    iget-object v0, p0, Lcom/android/server/location/ThreadPoolUtil;->executor:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ThreadPoolExecutor;->remove(Ljava/lang/Runnable;)Z

    .line 106
    return-void
.end method

.method public setCorePoolSize(I)V
    .locals 0
    .param p1, "corePoolSize"    # I

    .line 109
    iput p1, p0, Lcom/android/server/location/ThreadPoolUtil;->corePoolSize:I

    .line 110
    return-void
.end method

.method public setKeepAliveTime(J)V
    .locals 0
    .param p1, "keepAliveTime"    # J

    .line 117
    iput-wide p1, p0, Lcom/android/server/location/ThreadPoolUtil;->keepAliveTime:J

    .line 118
    return-void
.end method

.method public setMaximumPoolSize(I)V
    .locals 0
    .param p1, "maximumPoolSize"    # I

    .line 113
    iput p1, p0, Lcom/android/server/location/ThreadPoolUtil;->maximumPoolSize:I

    .line 114
    return-void
.end method

.method public setUnit(Ljava/util/concurrent/TimeUnit;)V
    .locals 0
    .param p1, "unit"    # Ljava/util/concurrent/TimeUnit;

    .line 121
    iput-object p1, p0, Lcom/android/server/location/ThreadPoolUtil;->unit:Ljava/util/concurrent/TimeUnit;

    .line 122
    return-void
.end method
