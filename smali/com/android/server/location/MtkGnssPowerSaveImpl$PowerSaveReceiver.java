class com.android.server.location.MtkGnssPowerSaveImpl$PowerSaveReceiver extends android.content.BroadcastReceiver {
	 /* .source "MtkGnssPowerSaveImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/location/MtkGnssPowerSaveImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "PowerSaveReceiver" */
} // .end annotation
/* # instance fields */
final com.android.server.location.MtkGnssPowerSaveImpl this$0; //synthetic
/* # direct methods */
private com.android.server.location.MtkGnssPowerSaveImpl$PowerSaveReceiver ( ) {
/* .locals 0 */
/* .line 518 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
 com.android.server.location.MtkGnssPowerSaveImpl$PowerSaveReceiver ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/location/MtkGnssPowerSaveImpl$PowerSaveReceiver;-><init>(Lcom/android/server/location/MtkGnssPowerSaveImpl;)V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 6 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 521 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 522 */
/* .local v0, "action":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_4
	 v1 = 	 android.text.TextUtils .isEmpty ( v0 );
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 /* .line 525 */
	 } // :cond_0
	 /* new-instance v1, Landroid/os/Message; */
	 /* invoke-direct {v1}, Landroid/os/Message;-><init>()V */
	 /* .line 526 */
	 /* .local v1, "msg":Landroid/os/Message; */
	 int v2 = 0; // const/4 v2, 0x0
	 /* iput v2, v1, Landroid/os/Message;->what:I */
	 /* .line 527 */
	 final String v3 = "POWER_SAVE_MODE_OPEN"; // const-string v3, "POWER_SAVE_MODE_OPEN"
	 v3 = 	 (( android.content.Intent ) p2 ).getBooleanExtra ( v3, v2 ); // invoke-virtual {p2, v3, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z
	 /* .line 528 */
	 /* .local v3, "powerSaveStatus":Z */
	 v4 = this.this$0;
	 v4 = 	 com.android.server.location.MtkGnssPowerSaveImpl .-$$Nest$ml5Device ( v4 );
	 /* .line 529 */
	 /* .local v4, "isL5Device":Z */
	 final String v5 = "miui.intent.action.POWER_SAVE_MODE_CHANGED"; // const-string v5, "miui.intent.action.POWER_SAVE_MODE_CHANGED"
	 v5 = 	 (( java.lang.String ) v5 ).equals ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v5 != null) { // if-eqz v5, :cond_3
		 /* .line 530 */
		 if ( v4 != null) { // if-eqz v4, :cond_1
			 /* .line 531 */
			 int v2 = 2; // const/4 v2, 0x2
			 /* iput v2, v1, Landroid/os/Message;->what:I */
			 /* .line 532 */
		 } // :cond_1
		 if ( v3 != null) { // if-eqz v3, :cond_2
			 /* .line 533 */
			 int v2 = 1; // const/4 v2, 0x1
			 /* iput v2, v1, Landroid/os/Message;->what:I */
			 /* .line 535 */
		 } // :cond_2
		 /* iput v2, v1, Landroid/os/Message;->what:I */
		 /* .line 537 */
	 } // :goto_0
	 java.lang.Boolean .valueOf ( v3 );
	 this.obj = v2;
	 /* .line 541 */
	 v2 = this.this$0;
	 com.android.server.location.MtkGnssPowerSaveImpl .-$$Nest$fgetmHandler ( v2 );
	 (( android.os.Handler ) v2 ).sendMessage ( v1 ); // invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
	 /* .line 542 */
	 return;
	 /* .line 539 */
} // :cond_3
return;
/* .line 523 */
} // .end local v1 # "msg":Landroid/os/Message;
} // .end local v3 # "powerSaveStatus":Z
} // .end local v4 # "isL5Device":Z
} // :cond_4
} // :goto_1
return;
} // .end method
