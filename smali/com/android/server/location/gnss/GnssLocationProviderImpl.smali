.class public Lcom/android/server/location/gnss/GnssLocationProviderImpl;
.super Ljava/lang/Object;
.source "GnssLocationProviderImpl.java"

# interfaces
.implements Lcom/android/server/location/gnss/GnssLocationProviderStub;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/location/gnss/GnssLocationProviderImpl$BlurLocationItem;
    }
.end annotation


# static fields
.field private static final ACTION_MODEM_LOCATION:Ljava/lang/String; = "NFW GPS LOCATION FROM Modem"

.field private static final CALLER_PACKAGE_NAME_ACTION:Ljava/lang/String; = "com.xiaomi.bsp.gps.nps.callerName"

.field private static final CLASS_PACKAGE_CONNECTIVITY_NPI:Ljava/lang/String; = "com.xiaomi.bsp.gps.nps"

.field private static final DEBUG:Z

.field private static ENABLE_FULL_TRACKING:Z = false

.field private static final EXTRA_NPS_NEW_EVENT:Ljava/lang/String; = "com.xiaomi.bsp.gps.nps.NewEvent"

.field private static final EXTRA_NPS_PACKAGE_NAME:Ljava/lang/String; = "com.xiaomi.bsp.gps.nps.pkgNname"

.field private static final GET_EVENT_ACTION:Ljava/lang/String; = "com.xiaomi.bsp.gps.nps.GetEvent"

.field private static final MIUI_NFW_PROXY_APP:Ljava/lang/String; = "com.lbe.security.miui"

.field private static final RECEIVER_GNSS_CALLER_NAME_EVENT:Ljava/lang/String; = "com.xiaomi.bsp.gps.nps.GnssCallerNameEventReceiver"

.field private static final RECEIVER_GNSS_EVENT:Ljava/lang/String; = "com.xiaomi.bsp.gps.nps.GnssEventReceiver"

.field private static final STR_LOCATIONFILE:Ljava/lang/String; = "locationinformation.txt"

.field private static final TAG:Ljava/lang/String; = "GnssLocationProviderImpl"

.field private static final XM_HP_LOCATION:Ljava/lang/String; = "xiaomi_high_precise_location"

.field private static final XM_HP_LOCATION_OFF:I = 0x2

.field private static final mLocationFile:Ljava/io/File;

.field private static final mLocationInformationLock:Ljava/lang/Object;


# instance fields
.field private blurLocationCollector:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack<",
            "Lcom/android/server/location/gnss/GnssLocationProviderImpl$BlurLocationItem;",
            ">;"
        }
    .end annotation
.end field

.field private blurLocationItem:Lcom/android/server/location/gnss/GnssLocationProviderImpl$BlurLocationItem;

.field private mBlurLocation:Lcom/android/server/location/MiuiBlurLocationManagerImpl;

.field private mBlurPackageName:Ljava/lang/String;

.field private mBlurState:I

.field private mBlurUid:I

.field private mContext:Landroid/content/Context;

.field private mEdgnssUiSwitch:Z

.field private mEnableSendingState:Z

.field private mGnssEventHandler:Lcom/android/server/location/GnssEventHandler;

.field private mNoise:Z

.field private newGnssEventHandler:Lcom/android/server/location/NewGnssEventHandler;

.field private popBlurLocationItem:Lcom/android/server/location/gnss/GnssLocationProviderImpl$BlurLocationItem;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 52
    const-string v0, "GnssLocationProviderImpl"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->DEBUG:Z

    .line 65
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mLocationInformationLock:Ljava/lang/Object;

    .line 87
    const/4 v1, 0x0

    sput-boolean v1, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->ENABLE_FULL_TRACKING:Z

    .line 93
    monitor-enter v0

    .line 94
    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-static {}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->getSystemDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "locationinformation.txt"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 95
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 97
    :try_start_1
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 100
    goto :goto_0

    .line 98
    :catch_0
    move-exception v2

    .line 99
    .local v2, "e":Ljava/io/IOException;
    :try_start_2
    const-string v3, "IO error occurred!"

    invoke-static {v3}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->loge(Ljava/lang/String;)V

    .line 101
    .end local v2    # "e":Ljava/io/IOException;
    :goto_0
    new-instance v2, Ljava/io/File;

    invoke-static {}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->getSystemDir()Ljava/io/File;

    move-result-object v3

    const-string v4, "locationinformation.txt"

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    sput-object v2, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mLocationFile:Ljava/io/File;

    .line 102
    .end local v1    # "file":Ljava/io/File;
    monitor-exit v0

    .line 103
    return-void

    .line 102
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public constructor <init>()V
    .locals 2

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mEnableSendingState:Z

    .line 73
    iput-boolean v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mEdgnssUiSwitch:Z

    .line 74
    iput-boolean v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mNoise:Z

    .line 76
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mGnssEventHandler:Lcom/android/server/location/GnssEventHandler;

    .line 77
    iput-object v1, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->newGnssEventHandler:Lcom/android/server/location/NewGnssEventHandler;

    .line 80
    iput v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mBlurState:I

    return-void
.end method

.method private dataFormat(D)D
    .locals 4
    .param p1, "data"    # D

    .line 635
    :try_start_0
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "0.00000"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 636
    .local v0, "df":Ljava/text/DecimalFormat;
    invoke-virtual {v0, p1, p2}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    .line 638
    .local v1, "doubleNumAsString":Ljava/lang/String;
    const-string v2, ","

    const-string v3, "."

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-wide v2

    .line 639
    .end local v0    # "df":Ljava/text/DecimalFormat;
    .end local v1    # "doubleNumAsString":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 640
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception here, print locale: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->loge(Ljava/lang/String;)V

    .line 641
    return-wide p1
.end method

.method private deliverCallerNameIntent(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packageName"    # Ljava/lang/String;

    .line 419
    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->isChineseLanguage()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 420
    :cond_0
    invoke-direct {p0, p2}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->packageOnReceive(Ljava/lang/String;)V

    .line 421
    return-void

    .line 419
    :cond_1
    :goto_0
    return-void
.end method

.method private deliverIntent(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "event"    # I

    .line 409
    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->isChineseLanguage()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 410
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->packageEventOnReceive(ILjava/lang/String;)V

    .line 411
    return-void

    .line 409
    :cond_1
    :goto_0
    return-void
.end method

.method private deliverIntentWithPackageName(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "event"    # I

    .line 414
    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->isChineseLanguage()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 415
    :cond_0
    invoke-direct {p0, p3, p2}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->packageEventOnReceive(ILjava/lang/String;)V

    .line 416
    return-void

    .line 414
    :cond_1
    :goto_0
    return-void
.end method

.method private getCurrentTime()Ljava/lang/String;
    .locals 11

    .line 463
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 464
    .local v0, "mNow":J
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 465
    .local v2, "sb":Ljava/lang/StringBuilder;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v9

    .line 466
    .local v9, "c":Ljava/util/Calendar;
    invoke-virtual {v9, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 467
    const-string v10, "%tm-%td %tH:%tM:%tS.%tL"

    move-object v3, v9

    move-object v4, v9

    move-object v5, v9

    move-object v6, v9

    move-object v7, v9

    move-object v8, v9

    filled-new-array/range {v3 .. v8}, [Ljava/lang/Object;

    move-result-object v3

    invoke-static {v10, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 468
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private getLocationFilePath()Ljava/lang/String;
    .locals 2

    .line 457
    sget-object v0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mLocationInformationLock:Ljava/lang/Object;

    monitor-enter v0

    .line 458
    :try_start_0
    sget-object v1, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mLocationFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    monitor-exit v0

    return-object v1

    .line 459
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private static getSystemDir()Ljava/io/File;
    .locals 3

    .line 473
    new-instance v0, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v1

    const-string/jumbo v2, "system"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private isChineseLanguage()Z
    .locals 2

    .line 313
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    .line 314
    .local v0, "language":Ljava/lang/String;
    const-string/jumbo v1, "zh_CN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method private isEdgnssSwitchOn(Landroid/content/Context;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 501
    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 502
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "xiaomi_high_precise_location"

    const/4 v3, 0x2

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eq v1, v3, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method private logd(Ljava/lang/String;)V
    .locals 1
    .param p1, "string"    # Ljava/lang/String;

    .line 650
    sget-boolean v0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 651
    const-string v0, "GnssLocationProviderImpl"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 653
    :cond_0
    return-void
.end method

.method private static loge(Ljava/lang/String;)V
    .locals 1
    .param p0, "string"    # Ljava/lang/String;

    .line 646
    const-string v0, "GnssLocationProviderImpl"

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 647
    return-void
.end method

.method private packageEventOnReceive(ILjava/lang/String;)V
    .locals 17
    .param p1, "event"    # I
    .param p2, "packageName"    # Ljava/lang/String;

    .line 333
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    const/4 v2, 0x1

    .line 334
    .local v2, "ACTION_START":I
    const/4 v3, 0x2

    .line 335
    .local v3, "ACTION_STOP":I
    const/4 v4, 0x3

    .line 336
    .local v4, "ACTION_FIX":I
    const/4 v5, 0x4

    .line 337
    .local v5, "ACTION_LOSE":I
    const/4 v6, 0x5

    .line 339
    .local v6, "ACTION_RECOVER":I
    const/4 v7, 0x6

    .line 340
    .local v7, "ACTION_START_NEW":I
    const/4 v8, 0x7

    .line 341
    .local v8, "ACTION_STOP_NEW":I
    const/16 v9, 0x8

    .line 346
    .local v9, "ACTION_GNSS_ENABLE_UPDATE":I
    const-string v10, "com.xiaomi.bsp.gps.nps.NewEvent"

    .line 347
    .local v10, "NEW_EVENT_KEY":Ljava/lang/String;
    const-string v11, "com.xiaomi.bsp.gps.nps.PackageName"

    .line 348
    .local v11, "PACKAGE_NAME_KEY":Ljava/lang/String;
    iget-object v12, v0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mGnssEventHandler:Lcom/android/server/location/GnssEventHandler;

    if-nez v12, :cond_0

    .line 349
    iget-object v12, v0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mContext:Landroid/content/Context;

    invoke-static {v12}, Lcom/android/server/location/GnssEventHandler;->getInstance(Landroid/content/Context;)Lcom/android/server/location/GnssEventHandler;

    move-result-object v12

    iput-object v12, v0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mGnssEventHandler:Lcom/android/server/location/GnssEventHandler;

    .line 351
    :cond_0
    iget-object v12, v0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->newGnssEventHandler:Lcom/android/server/location/NewGnssEventHandler;

    if-nez v12, :cond_1

    .line 352
    iget-object v12, v0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mContext:Landroid/content/Context;

    invoke-static {v12}, Lcom/android/server/location/NewGnssEventHandler;->getInstance(Landroid/content/Context;)Lcom/android/server/location/NewGnssEventHandler;

    move-result-object v12

    iput-object v12, v0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->newGnssEventHandler:Lcom/android/server/location/NewGnssEventHandler;

    .line 354
    :cond_1
    move/from16 v12, p1

    .line 355
    .local v12, "intentExtra":I
    if-nez v12, :cond_2

    .line 356
    return-void

    .line 359
    :cond_2
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "receive event "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ""

    if-nez v1, :cond_3

    move/from16 v16, v2

    move-object v2, v14

    goto :goto_0

    :cond_3
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v16, v2

    .end local v2    # "ACTION_START":I
    .local v16, "ACTION_START":I
    const-string v2, ","

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_0
    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v13, "GnssLocationProviderImpl"

    invoke-static {v13, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    if-eqz v1, :cond_4

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 362
    packed-switch v12, :pswitch_data_0

    .line 383
    goto :goto_1

    .line 370
    :pswitch_0
    iget-object v2, v0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->newGnssEventHandler:Lcom/android/server/location/NewGnssEventHandler;

    invoke-virtual {v2}, Lcom/android/server/location/NewGnssEventHandler;->handleUpdateGnssStatus()V

    .line 371
    goto :goto_1

    .line 367
    :pswitch_1
    iget-object v2, v0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->newGnssEventHandler:Lcom/android/server/location/NewGnssEventHandler;

    invoke-virtual {v2, v1}, Lcom/android/server/location/NewGnssEventHandler;->handleStop(Ljava/lang/String;)V

    .line 368
    goto :goto_1

    .line 364
    :pswitch_2
    iget-object v2, v0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->newGnssEventHandler:Lcom/android/server/location/NewGnssEventHandler;

    invoke-virtual {v2, v1}, Lcom/android/server/location/NewGnssEventHandler;->handleStart(Ljava/lang/String;)V

    .line 365
    goto :goto_1

    .line 380
    :pswitch_3
    iget-object v2, v0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->newGnssEventHandler:Lcom/android/server/location/NewGnssEventHandler;

    const/4 v13, 0x0

    invoke-virtual {v2, v13}, Lcom/android/server/location/NewGnssEventHandler;->handlerUpdateFixStatus(Z)V

    .line 381
    goto :goto_1

    .line 376
    :pswitch_4
    iget-object v2, v0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->newGnssEventHandler:Lcom/android/server/location/NewGnssEventHandler;

    const/4 v13, 0x1

    invoke-virtual {v2, v13}, Lcom/android/server/location/NewGnssEventHandler;->handlerUpdateFixStatus(Z)V

    .line 377
    goto :goto_1

    .line 386
    :cond_4
    packed-switch v12, :pswitch_data_1

    goto :goto_1

    .line 401
    :pswitch_5
    goto :goto_1

    .line 398
    :pswitch_6
    goto :goto_1

    .line 395
    :pswitch_7
    goto :goto_1

    .line 391
    :pswitch_8
    iget-object v2, v0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mGnssEventHandler:Lcom/android/server/location/GnssEventHandler;

    invoke-virtual {v2}, Lcom/android/server/location/GnssEventHandler;->handleStop()V

    .line 392
    goto :goto_1

    .line 389
    :pswitch_9
    nop

    .line 406
    :goto_1
    return-void

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_4
        :pswitch_3
        :pswitch_4
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
    .end packed-switch
.end method

.method private packageOnReceive(Ljava/lang/String;)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .line 319
    new-instance v0, Ljava/util/ArrayList;

    const-string v1, "android"

    const-string v2, "com.xiaomi.location.fused"

    filled-new-array {v1, v2}, [Ljava/lang/String;

    move-result-object v1

    .line 320
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 321
    .local v0, "mIgnoreNotifyPackage":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mGnssEventHandler:Lcom/android/server/location/GnssEventHandler;

    if-nez v1, :cond_0

    .line 322
    iget-object v1, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/server/location/GnssEventHandler;->getInstance(Landroid/content/Context;)Lcom/android/server/location/GnssEventHandler;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mGnssEventHandler:Lcom/android/server/location/GnssEventHandler;

    .line 324
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "receive caller pkg name ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GnssLocationProviderImpl"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 326
    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    .line 329
    :cond_1
    iget-object v1, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mGnssEventHandler:Lcom/android/server/location/GnssEventHandler;

    invoke-virtual {v1, p1}, Lcom/android/server/location/GnssEventHandler;->handleCallerName(Ljava/lang/String;)V

    .line 330
    return-void

    .line 327
    :cond_2
    :goto_0
    return-void
.end method

.method private precisionProcess(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1, "data"    # Ljava/lang/String;

    .line 564
    const/16 v0, 0x2e

    invoke-virtual {p1, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 565
    .local v0, "leng":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 566
    return-object p1

    .line 568
    :cond_0
    add-int/lit8 v1, v0, -0x2

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 569
    .local v1, "dataNeedProcessed":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    const/4 v3, 0x2

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 570
    .local v2, "sf":Ljava/lang/StringBuilder;
    add-int/lit8 v3, v0, -0x2

    const/4 v4, 0x0

    invoke-virtual {p1, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 571
    .local v3, "dataInvariant":Ljava/lang/String;
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 572
    const/4 v5, 0x1

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    const-string v6, "0"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 574
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 576
    :cond_1
    invoke-static {v1}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    const-wide/high16 v8, 0x404e000000000000L    # 60.0

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v4

    const/4 v6, 0x5

    invoke-virtual {v4, v6, v5}, Ljava/math/BigDecimal;->setScale(II)Ljava/math/BigDecimal;

    move-result-object v4

    .line 577
    .local v4, "bigDecimal":Ljava/math/BigDecimal;
    new-instance v5, Ljava/math/BigDecimal;

    const/16 v6, 0x3c

    invoke-direct {v5, v6}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-virtual {v4, v5}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v5

    invoke-virtual {v5}, Ljava/math/BigDecimal;->doubleValue()D

    move-result-wide v5

    .line 578
    .local v5, "dataProcessed":D
    invoke-virtual {v2, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 579
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    return-object v7
.end method


# virtual methods
.method public dump(Ljava/io/PrintWriter;)V
    .locals 2
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 245
    invoke-virtual {p0}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->loadLocationInformation()Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    .line 247
    const-string v0, "GLP information:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 248
    invoke-static {}, Lcom/android/server/location/LocationDumpLogStub;->getInstance()Lcom/android/server/location/LocationDumpLogStub;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, v1, p1}, Lcom/android/server/location/LocationDumpLogStub;->dump(ILjava/io/PrintWriter;)V

    .line 250
    const-string v0, "NMEA information:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 251
    invoke-static {}, Lcom/android/server/location/LocationDumpLogStub;->getInstance()Lcom/android/server/location/LocationDumpLogStub;

    move-result-object v0

    const/4 v1, 0x4

    invoke-interface {v0, v1, p1}, Lcom/android/server/location/LocationDumpLogStub;->dump(ILjava/io/PrintWriter;)V

    .line 252
    return-void
.end method

.method public getConfig(Ljava/util/Properties;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "properties"    # Ljava/util/Properties;
    .param p2, "config"    # Ljava/lang/String;
    .param p3, "defaultConfig"    # Ljava/lang/String;

    .line 158
    invoke-virtual {p1, p2, p3}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLowerAccLocation(Landroid/location/Location;)Landroid/location/Location;
    .locals 2
    .param p1, "location"    # Landroid/location/Location;

    .line 584
    iget-boolean v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mEdgnssUiSwitch:Z

    if-nez v0, :cond_0

    .line 585
    return-object p1

    .line 587
    :cond_0
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->dataFormat(D)D

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/location/Location;->setLatitude(D)V

    .line 588
    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->dataFormat(D)D

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/location/Location;->setLongitude(D)V

    .line 589
    return-object p1
.end method

.method public getSendingSwitch()Z
    .locals 1

    .line 123
    iget-boolean v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mEnableSendingState:Z

    return v0
.end method

.method public handleDisable()V
    .locals 3

    .line 184
    invoke-static {}, Lcom/android/server/location/LocationDumpLogStub;->getInstance()Lcom/android/server/location/LocationDumpLogStub;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/server/location/LocationDumpLogStub;->clearData()V

    .line 186
    invoke-static {}, Lcom/android/server/location/LocationDumpLogStub;->getInstance()Lcom/android/server/location/LocationDumpLogStub;

    move-result-object v0

    const/4 v1, 0x2

    const-string v2, "disable gnss"

    invoke-interface {v0, v1, v2}, Lcom/android/server/location/LocationDumpLogStub;->addToBugreport(ILjava/lang/String;)V

    .line 188
    invoke-static {}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeStub;->getInstance()Lcom/android/server/location/gnss/hal/GnssPowerOptimizeStub;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeStub;->disableGnssSwitch()V

    .line 190
    invoke-virtual {p0, v2}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->writeLocationInformation(Ljava/lang/String;)V

    .line 191
    return-void
.end method

.method public handleEnable()V
    .locals 3

    .line 176
    invoke-static {}, Lcom/android/server/location/LocationDumpLogStub;->getInstance()Lcom/android/server/location/LocationDumpLogStub;

    move-result-object v0

    const/4 v1, 0x2

    const-string v2, "enable gnss"

    invoke-interface {v0, v1, v2}, Lcom/android/server/location/LocationDumpLogStub;->addToBugreport(ILjava/lang/String;)V

    .line 178
    invoke-virtual {p0, v2}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->writeLocationInformation(Ljava/lang/String;)V

    .line 179
    return-void
.end method

.method public handleInitialize(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 107
    iput-object p1, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mContext:Landroid/content/Context;

    .line 109
    invoke-static {}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeStub;->getInstance()Lcom/android/server/location/gnss/hal/GnssPowerOptimizeStub;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeStub;->init(Landroid/content/Context;)V

    .line 111
    invoke-static {}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->getInstance()Lcom/android/server/location/gnss/GnssEventTrackingStub;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->init(Landroid/content/Context;)V

    .line 113
    invoke-static {}, Lcom/android/server/location/LocationExtCooperateStub;->getInstance()Lcom/android/server/location/LocationExtCooperateStub;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/android/server/location/LocationExtCooperateStub;->init(Landroid/content/Context;)V

    .line 115
    invoke-static {}, Lcom/android/server/location/LocationDumpLogStub;->getInstance()Lcom/android/server/location/LocationDumpLogStub;

    move-result-object v0

    const/4 v1, 0x2

    const-string v2, "initialize glp"

    invoke-interface {v0, v1, v2}, Lcom/android/server/location/LocationDumpLogStub;->addToBugreport(ILjava/lang/String;)V

    .line 117
    invoke-static {}, Lcom/android/server/location/gnss/GnssCollectDataStub;->getInstance()Lcom/android/server/location/gnss/GnssCollectDataStub;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v3, 0x0

    invoke-interface {v0, v1, v3, p1}, Lcom/android/server/location/gnss/GnssCollectDataStub;->savePoint(ILjava/lang/String;Landroid/content/Context;)V

    .line 118
    invoke-virtual {p0, v2}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->writeLocationInformation(Ljava/lang/String;)V

    .line 119
    return-void
.end method

.method public handleReportLocation(ILandroid/location/Location;)V
    .locals 4
    .param p1, "ttff"    # I
    .param p2, "location"    # Landroid/location/Location;

    .line 295
    invoke-static {}, Lcom/android/server/location/LocationDumpLogStub;->getInstance()Lcom/android/server/location/LocationDumpLogStub;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TTFF is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {v0, v2, v1}, Lcom/android/server/location/LocationDumpLogStub;->addToBugreport(ILjava/lang/String;)V

    .line 297
    sget-boolean v0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->ENABLE_FULL_TRACKING:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 298
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->ENABLE_FULL_TRACKING:Z

    .line 301
    :cond_0
    invoke-static {}, Lcom/android/server/location/LocationDumpLogStub;->getInstance()Lcom/android/server/location/LocationDumpLogStub;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "the first location is "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x3

    invoke-interface {v0, v3, v1}, Lcom/android/server/location/LocationDumpLogStub;->addToBugreport(ILjava/lang/String;)V

    .line 304
    const-string v0, "fix location"

    invoke-virtual {p0, v0}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->writeLocationInformation(Ljava/lang/String;)V

    .line 308
    invoke-static {}, Lcom/android/server/location/gnss/GnssCollectDataStub;->getInstance()Lcom/android/server/location/gnss/GnssCollectDataStub;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v2, v1}, Lcom/android/server/location/gnss/GnssCollectDataStub;->savePoint(ILjava/lang/String;)V

    .line 310
    return-void
.end method

.method public handleReportSvStatus(Landroid/location/GnssStatus;JLandroid/location/provider/ProviderRequest;Lcom/android/server/location/gnss/hal/GnssNative;)V
    .locals 10
    .param p1, "gnssStatus"    # Landroid/location/GnssStatus;
    .param p2, "lastFixTime"    # J
    .param p4, "mProviderRequest"    # Landroid/location/provider/ProviderRequest;
    .param p5, "mGnssNative"    # Lcom/android/server/location/gnss/hal/GnssNative;

    .line 196
    invoke-virtual {p1}, Landroid/location/GnssStatus;->getSatelliteCount()I

    move-result v6

    .line 197
    .local v6, "svCount":I
    new-array v7, v6, [F

    .line 198
    .local v7, "cn0s":[F
    new-array v8, v6, [F

    .line 199
    .local v8, "svCarrierFre":[F
    new-array v9, v6, [F

    .line 200
    .local v9, "svConstellation":[F
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v6, :cond_1

    .line 201
    invoke-virtual {p1, v0}, Landroid/location/GnssStatus;->getCn0DbHz(I)F

    move-result v1

    aput v1, v7, v0

    .line 202
    invoke-virtual {p1, v0}, Landroid/location/GnssStatus;->getCarrierFrequencyHz(I)F

    move-result v1

    aput v1, v8, v0

    .line 203
    invoke-virtual {p1, v0}, Landroid/location/GnssStatus;->usedInFix(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 204
    invoke-virtual {p1, v0}, Landroid/location/GnssStatus;->getConstellationType(I)I

    move-result v1

    int-to-float v1, v1

    aput v1, v9, v0

    .line 200
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 207
    .end local v0    # "i":I
    :cond_1
    invoke-static {}, Lcom/android/server/location/gnss/GnssCollectDataStub;->getInstance()Lcom/android/server/location/gnss/GnssCollectDataStub;

    move-result-object v0

    const/16 v1, 0xa

    move-object v2, v7

    move v3, v6

    move-object v4, v8

    move-object v5, v9

    invoke-interface/range {v0 .. v5}, Lcom/android/server/location/gnss/GnssCollectDataStub;->savePoint(I[FI[F[F)V

    .line 211
    invoke-static {}, Lcom/android/server/location/provider/AmapCustomStub;->getInstance()Lcom/android/server/location/provider/AmapCustomStub;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/android/server/location/provider/AmapCustomStub;->setLastSvStatus(Landroid/location/GnssStatus;)V

    .line 214
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-eqz v0, :cond_4

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sub-long/2addr v0, p2

    const-wide/16 v2, 0xbb8

    cmp-long v0, v0, v2

    if-lez v0, :cond_4

    .line 215
    invoke-static {}, Lcom/android/server/location/LocationDumpLogStub;->getInstance()Lcom/android/server/location/LocationDumpLogStub;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/server/location/LocationDumpLogStub;->getRecordLoseLocation()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 216
    invoke-static {}, Lcom/android/server/location/LocationDumpLogStub;->getInstance()Lcom/android/server/location/LocationDumpLogStub;

    move-result-object v0

    const/4 v1, 0x2

    const-string v2, "lose location, record the latest SV status "

    invoke-interface {v0, v1, v2}, Lcom/android/server/location/LocationDumpLogStub;->addToBugreport(ILjava/lang/String;)V

    .line 218
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 219
    .local v0, "statusInfos":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {p1}, Landroid/location/GnssStatus;->getSatelliteCount()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 220
    invoke-virtual {p1, v1}, Landroid/location/GnssStatus;->getSvid(I)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1, v1}, Landroid/location/GnssStatus;->getCn0DbHz(I)F

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 219
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 222
    .end local v1    # "i":I
    :cond_2
    invoke-static {}, Lcom/android/server/location/LocationDumpLogStub;->getInstance()Lcom/android/server/location/LocationDumpLogStub;

    move-result-object v1

    .line 223
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 222
    const/4 v3, 0x3

    invoke-interface {v1, v3, v2}, Lcom/android/server/location/LocationDumpLogStub;->addToBugreport(ILjava/lang/String;)V

    .line 224
    sget-boolean v1, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->ENABLE_FULL_TRACKING:Z

    if-nez v1, :cond_3

    .line 225
    invoke-static {}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecoveryStub;->getInstance()Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecoveryStub;

    move-result-object v1

    invoke-virtual {p4}, Landroid/location/provider/ProviderRequest;->getWorkSource()Landroid/os/WorkSource;

    move-result-object v2

    invoke-interface {v1, v2, p5}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecoveryStub;->startDiagnostic(Landroid/os/WorkSource;Lcom/android/server/location/gnss/hal/GnssNative;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 226
    const/4 v1, 0x1

    sput-boolean v1, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->ENABLE_FULL_TRACKING:Z

    .line 229
    :cond_3
    invoke-static {}, Lcom/android/server/location/LocationDumpLogStub;->getInstance()Lcom/android/server/location/LocationDumpLogStub;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/android/server/location/LocationDumpLogStub;->setRecordLoseLocation(Z)V

    .line 231
    const-string v1, "lose location"

    invoke-virtual {p0, v1}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->writeLocationInformation(Ljava/lang/String;)V

    .line 232
    invoke-static {}, Lcom/android/server/location/gnss/GnssCollectDataStub;->getInstance()Lcom/android/server/location/gnss/GnssCollectDataStub;

    move-result-object v1

    const/4 v2, 0x4

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/android/server/location/gnss/GnssCollectDataStub;->savePoint(ILjava/lang/String;)V

    .line 239
    .end local v0    # "statusInfos":Ljava/lang/StringBuilder;
    :cond_4
    invoke-static {}, Lcom/android/server/location/GnssSmartSatelliteSwitchStub;->getInstance()Lcom/android/server/location/GnssSmartSatelliteSwitchStub;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/android/server/location/GnssSmartSatelliteSwitchStub;->smartSatelliteSwitchMonitor(Landroid/location/GnssStatus;)V

    .line 240
    return-void
.end method

.method public handleRequestLocation(ZLandroid/location/LocationRequest$Builder;Ljava/lang/String;)Landroid/location/LocationRequest$Builder;
    .locals 3
    .param p1, "canBypass"    # Z
    .param p2, "locationRequest"    # Landroid/location/LocationRequest$Builder;
    .param p3, "provider"    # Ljava/lang/String;

    .line 143
    invoke-static {}, Lcom/android/server/location/LocationDumpLogStub;->getInstance()Lcom/android/server/location/LocationDumpLogStub;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "request location from HAL using provider "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {v0, v2, v1}, Lcom/android/server/location/LocationDumpLogStub;->addToBugreport(ILjava/lang/String;)V

    .line 146
    if-eqz p1, :cond_0

    .line 147
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Landroid/location/LocationRequest$Builder;->setLocationSettingsIgnored(Z)Landroid/location/LocationRequest$Builder;

    .line 148
    invoke-virtual {p2, v2}, Landroid/location/LocationRequest$Builder;->setMaxUpdates(I)Landroid/location/LocationRequest$Builder;

    .line 149
    const-string v0, "Bypass All Modem request."

    .line 150
    .local v0, "passInfo":Ljava/lang/String;
    const-string v1, "GnssLocationProviderImpl"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    invoke-static {}, Lcom/android/server/location/LocationDumpLogStub;->getInstance()Lcom/android/server/location/LocationDumpLogStub;

    move-result-object v1

    invoke-interface {v1, v2, v0}, Lcom/android/server/location/LocationDumpLogStub;->addToBugreport(ILjava/lang/String;)V

    .line 153
    .end local v0    # "passInfo":Ljava/lang/String;
    :cond_0
    return-object p2
.end method

.method public hasLocationPermission(Landroid/content/pm/PackageManager;Ljava/lang/String;Landroid/content/Context;)Z
    .locals 4
    .param p1, "packageManager"    # Landroid/content/pm/PackageManager;
    .param p2, "pkgName"    # Ljava/lang/String;
    .param p3, "context"    # Landroid/content/Context;

    .line 487
    nop

    .line 488
    invoke-virtual {p3}, Landroid/content/Context;->getUser()Landroid/os/UserHandle;

    move-result-object v0

    .line 487
    const-string v1, "com.miui.securitycenter.permission.modem_location"

    invoke-virtual {p1, v1, p2, v0}, Landroid/content/pm/PackageManager;->getPermissionFlags(Ljava/lang/String;Ljava/lang/String;Landroid/os/UserHandle;)I

    move-result v0

    .line 489
    .local v0, "flags":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "flags in nfw app is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->loge(Ljava/lang/String;)V

    .line 490
    invoke-virtual {p1, v1, p2}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_1

    and-int/lit8 v1, v0, 0x2

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    return v1
.end method

.method public ifNoiseEnvironment(Ljava/lang/String;)V
    .locals 7
    .param p1, "nmea"    # Ljava/lang/String;

    .line 507
    const-string v0, "PQWM1"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 508
    return-void

    .line 510
    :cond_0
    const-string v1, ","

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 512
    .local v1, "nmeaSplit":[Ljava/lang/String;
    const/4 v2, 0x0

    aget-object v3, v1, v2

    invoke-virtual {v3, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v3, -0x1

    if-eq v0, v3, :cond_5

    .line 513
    array-length v0, v1

    const/4 v3, 0x3

    if-lt v0, v3, :cond_4

    const/16 v0, 0x9

    aget-object v3, v1, v0

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_0

    .line 516
    :cond_1
    aget-object v0, v1, v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 517
    .local v0, "value":I
    const/4 v3, 0x1

    const/16 v4, -0xc

    const/16 v5, 0x12

    if-eq v0, v5, :cond_2

    if-ne v0, v4, :cond_3

    :cond_2
    iget-boolean v6, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mNoise:Z

    if-nez v6, :cond_3

    .line 518
    const-string v2, "noise_environment"

    invoke-virtual {p0, v2}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->notifyCallerName(Ljava/lang/String;)V

    .line 519
    iput-boolean v3, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mNoise:Z

    goto :goto_1

    .line 520
    :cond_3
    iget-boolean v6, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mNoise:Z

    if-ne v6, v3, :cond_5

    if-eq v0, v5, :cond_5

    if-eq v0, v4, :cond_5

    .line 521
    iput-boolean v2, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mNoise:Z

    .line 522
    const-string v2, "normal_environment"

    invoke-virtual {p0, v2}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->notifyCallerName(Ljava/lang/String;)V

    goto :goto_1

    .line 514
    .end local v0    # "value":I
    :cond_4
    :goto_0
    return-void

    .line 525
    :cond_5
    :goto_1
    return-void
.end method

.method public loadLocationInformation()Ljava/lang/StringBuilder;
    .locals 6

    .line 439
    const-string v0, "loadLocationInformation"

    invoke-direct {p0, v0}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->logd(Ljava/lang/String;)V

    .line 440
    sget-object v0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mLocationInformationLock:Ljava/lang/Object;

    monitor-enter v0

    .line 441
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/FileReader;

    sget-object v3, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mLocationFile:Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 443
    .local v1, "reader":Ljava/io/BufferedReader;
    :try_start_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 444
    .local v2, "stirngBuidler":Ljava/lang/StringBuilder;
    const-string v3, "LocationInformation:"

    .line 446
    .local v3, "line":Ljava/lang/String;
    :cond_0
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 447
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v3, v4

    if-nez v4, :cond_0

    .line 448
    nop

    .line 449
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 448
    return-object v2

    .line 441
    .end local v2    # "stirngBuidler":Ljava/lang/StringBuilder;
    .end local v3    # "line":Ljava/lang/String;
    :catchall_0
    move-exception v2

    :try_start_4
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v3

    :try_start_5
    invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/location/gnss/GnssLocationProviderImpl;
    :goto_0
    throw v2
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 453
    .end local v1    # "reader":Ljava/io/BufferedReader;
    .restart local p0    # "this":Lcom/android/server/location/gnss/GnssLocationProviderImpl;
    :catchall_2
    move-exception v1

    goto :goto_1

    .line 449
    :catch_0
    move-exception v1

    .line 450
    .local v1, "e":Ljava/io/IOException;
    :try_start_6
    const-string v2, "IO exception"

    invoke-static {v2}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->loge(Ljava/lang/String;)V

    .line 452
    .end local v1    # "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    monitor-exit v0

    return-object v1

    .line 453
    :goto_1
    monitor-exit v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v1
.end method

.method public notifyCallerName(Ljava/lang/String;)V
    .locals 2
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 256
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "caller name: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->logd(Ljava/lang/String;)V

    .line 257
    iget-object v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 258
    invoke-direct {p0, v0, p1}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->deliverCallerNameIntent(Landroid/content/Context;Ljava/lang/String;)V

    .line 260
    iget-object v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->isEdgnssSwitchOn(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mEdgnssUiSwitch:Z

    .line 261
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Edgnss Switch now is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mEdgnssUiSwitch:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->logd(Ljava/lang/String;)V

    .line 263
    :cond_0
    return-void
.end method

.method public notifyCallerName(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "eventType"    # Ljava/lang/String;

    .line 267
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "caller name: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", eventType"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->logd(Ljava/lang/String;)V

    .line 268
    if-nez p2, :cond_0

    .line 269
    invoke-virtual {p0, p1}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->notifyCallerName(Ljava/lang/String;)V

    .line 270
    return-void

    .line 272
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_5

    .line 274
    :try_start_0
    iget-object v1, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mGnssEventHandler:Lcom/android/server/location/GnssEventHandler;

    if-nez v1, :cond_1

    .line 275
    invoke-static {v0}, Lcom/android/server/location/GnssEventHandler;->getInstance(Landroid/content/Context;)Lcom/android/server/location/GnssEventHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mGnssEventHandler:Lcom/android/server/location/GnssEventHandler;

    .line 277
    :cond_1
    iget-object v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->blurLocationCollector:Ljava/util/Stack;

    if-nez v0, :cond_2

    .line 278
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->blurLocationCollector:Ljava/util/Stack;

    .line 280
    :cond_2
    const-string v0, "blurLocation_notify is on"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    .line 281
    iget-object v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {p0, v0, p1}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->showBlurNotification(ILjava/lang/String;)V

    goto :goto_0

    .line 282
    :cond_3
    const-string v0, "blurLocation_notify is off"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 283
    iget-object v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {p0, v0, p1}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->removeBlurNotification(ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 287
    :cond_4
    :goto_0
    goto :goto_1

    .line 285
    :catch_0
    move-exception v0

    .line 286
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "exception"

    invoke-static {v1}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->loge(Ljava/lang/String;)V

    .line 290
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_5
    :goto_1
    return-void
.end method

.method public notifyState(I)V
    .locals 2
    .param p1, "event"    # I

    .line 163
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "gps now on "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->logd(Ljava/lang/String;)V

    .line 164
    iget-object v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0, p1}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->deliverIntent(Landroid/content/Context;I)V

    .line 165
    return-void
.end method

.method public notifyStateWithPackageName(Ljava/lang/String;I)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "event"    # I

    .line 169
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "notify state, event:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->logd(Ljava/lang/String;)V

    .line 170
    iget-object v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0, p1, p2}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->deliverIntentWithPackageName(Landroid/content/Context;Ljava/lang/String;I)V

    .line 171
    return-void
.end method

.method public precisionProcessByType(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "nmea"    # Ljava/lang/String;

    .line 535
    invoke-virtual {p0, p1}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->ifNoiseEnvironment(Ljava/lang/String;)V

    .line 536
    iget-boolean v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mEdgnssUiSwitch:Z

    if-nez v0, :cond_0

    .line 537
    return-object p1

    .line 540
    :cond_0
    const-string v0, ","

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 542
    .local v1, "nmeaSplit":[Ljava/lang/String;
    const/4 v2, 0x0

    aget-object v3, v1, v2

    const-string v4, "GGA"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v3, 0x2

    aget-object v5, v1, v3

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 544
    aget-object v5, v1, v3

    invoke-direct {p0, v5}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->precisionProcess(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v3

    .line 545
    const/4 v3, 0x4

    aget-object v5, v1, v3

    invoke-direct {p0, v5}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->precisionProcess(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v3

    .line 547
    :cond_1
    aget-object v3, v1, v2

    const-string v5, "RMC"

    invoke-virtual {v3, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v5, 0x3

    if-eq v3, v4, :cond_2

    aget-object v3, v1, v5

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 548
    aget-object v3, v1, v5

    invoke-direct {p0, v3}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->precisionProcess(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v5

    .line 549
    const/4 v3, 0x5

    aget-object v6, v1, v3

    invoke-direct {p0, v6}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->precisionProcess(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v1, v3

    .line 551
    :cond_2
    aget-object v2, v1, v2

    const-string v3, "GLL"

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v4, :cond_3

    const/4 v2, 0x1

    aget-object v3, v1, v2

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 552
    aget-object v3, v1, v2

    invoke-direct {p0, v3}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->precisionProcess(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 553
    aget-object v2, v1, v5

    invoke-direct {p0, v2}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->precisionProcess(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    .line 555
    :cond_3
    invoke-static {v0, v1}, Ljava/lang/String;->join(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 556
    .local v0, "nmeaEnd":Ljava/lang/String;
    return-object v0
.end method

.method public reloadGpsProperties(Lcom/android/server/location/gnss/GnssConfiguration;)V
    .locals 5
    .param p1, "gnssConfiguration"    # Lcom/android/server/location/gnss/GnssConfiguration;

    .line 133
    invoke-static {}, Lcom/android/server/location/LocationDumpLogStub;->getInstance()Lcom/android/server/location/LocationDumpLogStub;

    move-result-object v0

    .line 134
    invoke-static {}, Lcom/android/server/location/LocationDumpLogStub;->getInstance()Lcom/android/server/location/LocationDumpLogStub;

    move-result-object v1

    .line 135
    invoke-virtual {p1}, Lcom/android/server/location/gnss/GnssConfiguration;->getProperties()Ljava/util/Properties;

    move-result-object v2

    .line 134
    const-string v3, "NMEA_LEN"

    const-string v4, "20000"

    invoke-interface {v1, v2, v3, v4}, Lcom/android/server/location/LocationDumpLogStub;->getConfig(Ljava/util/Properties;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 133
    const/4 v2, 0x4

    invoke-interface {v0, v2, v1}, Lcom/android/server/location/LocationDumpLogStub;->setLength(II)V

    .line 137
    nop

    .line 138
    invoke-virtual {p1}, Lcom/android/server/location/gnss/GnssConfiguration;->getProperties()Ljava/util/Properties;

    move-result-object v0

    const-string v1, "ENABLE_NOTIFY"

    const-string v2, "false"

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->getConfig(Ljava/util/Properties;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 137
    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->setSendingSwitch(Z)V

    .line 139
    return-void
.end method

.method public removeBlurNotification(ILjava/lang/String;)V
    .locals 3
    .param p1, "uid"    # I
    .param p2, "packageName"    # Ljava/lang/String;

    .line 613
    iget-object v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mBlurPackageName:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "blurLocation_notify is off"

    if-nez v0, :cond_0

    .line 614
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mBlurState:I

    .line 615
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mBlurPackageName:Ljava/lang/String;

    .line 616
    iget-object v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->blurLocationCollector:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V

    .line 617
    iget-object v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mGnssEventHandler:Lcom/android/server/location/GnssEventHandler;

    invoke-virtual {v0, p2, v1}, Lcom/android/server/location/GnssEventHandler;->handleCallerName(Ljava/lang/String;Ljava/lang/String;)V

    .line 618
    return-void

    .line 620
    :cond_0
    iget v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mBlurState:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mBlurState:I

    .line 621
    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->blurLocationCollector:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 622
    iget-object v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->blurLocationCollector:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/location/gnss/GnssLocationProviderImpl$BlurLocationItem;

    iput-object v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->popBlurLocationItem:Lcom/android/server/location/gnss/GnssLocationProviderImpl$BlurLocationItem;

    .line 623
    invoke-virtual {v0}, Lcom/android/server/location/gnss/GnssLocationProviderImpl$BlurLocationItem;->getState()I

    move-result v0

    iput v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mBlurState:I

    .line 624
    iget-object v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->popBlurLocationItem:Lcom/android/server/location/gnss/GnssLocationProviderImpl$BlurLocationItem;

    invoke-virtual {v0}, Lcom/android/server/location/gnss/GnssLocationProviderImpl$BlurLocationItem;->getUid()I

    move-result v0

    iput v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mBlurUid:I

    .line 625
    iget-object v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->popBlurLocationItem:Lcom/android/server/location/gnss/GnssLocationProviderImpl$BlurLocationItem;

    invoke-virtual {v0}, Lcom/android/server/location/gnss/GnssLocationProviderImpl$BlurLocationItem;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mBlurPackageName:Ljava/lang/String;

    .line 626
    iget-object v1, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mGnssEventHandler:Lcom/android/server/location/GnssEventHandler;

    const-string v2, "blurLocation_notify is on"

    invoke-virtual {v1, v0, v2}, Lcom/android/server/location/GnssEventHandler;->handleCallerName(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 627
    :cond_1
    iget v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mBlurState:I

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->blurLocationCollector:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 628
    iget-object v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mGnssEventHandler:Lcom/android/server/location/GnssEventHandler;

    invoke-virtual {v0, p2, v1}, Lcom/android/server/location/GnssEventHandler;->handleCallerName(Ljava/lang/String;Ljava/lang/String;)V

    .line 631
    :cond_2
    :goto_0
    return-void
.end method

.method public sendNfwbroadcast(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 496
    new-instance v0, Landroid/content/Intent;

    const-string v1, "NFW GPS LOCATION FROM Modem"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 497
    .local v0, "intent":Landroid/content/Intent;
    sget-object v1, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 498
    return-void
.end method

.method public setNfwProxyAppConfig(Ljava/util/Properties;Ljava/lang/String;)V
    .locals 3
    .param p1, "properties"    # Ljava/util/Properties;
    .param p2, "config"    # Ljava/lang/String;

    .line 478
    const-string v0, "ro.boot.hwc"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 479
    .local v0, "country":Ljava/lang/String;
    const-string v1, "CN"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 480
    const-string v1, "com.lbe.security.miui"

    invoke-virtual {p1, p2, v1}, Ljava/util/Properties;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 481
    const-string v1, "GnssLocationProviderImpl"

    const-string v2, "NFW app is com.lbe.security.miui"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 483
    :cond_0
    return-void
.end method

.method public setSendingSwitch(Z)V
    .locals 0
    .param p1, "newValue"    # Z

    .line 128
    iput-boolean p1, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mEnableSendingState:Z

    .line 129
    return-void
.end method

.method public showBlurNotification(ILjava/lang/String;)V
    .locals 8
    .param p1, "uid"    # I
    .param p2, "packageName"    # Ljava/lang/String;

    .line 593
    iget-object v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mBlurPackageName:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_1

    iget v4, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mBlurState:I

    if-eqz v4, :cond_1

    iget-object v5, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mBlurPackageName:Ljava/lang/String;

    if-eqz v5, :cond_1

    .line 594
    new-instance v0, Lcom/android/server/location/gnss/GnssLocationProviderImpl$BlurLocationItem;

    iget v6, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mBlurUid:I

    const/4 v7, 0x0

    move-object v2, v0

    move-object v3, p0

    invoke-direct/range {v2 .. v7}, Lcom/android/server/location/gnss/GnssLocationProviderImpl$BlurLocationItem;-><init>(Lcom/android/server/location/gnss/GnssLocationProviderImpl;ILjava/lang/String;ILcom/android/server/location/gnss/GnssLocationProviderImpl$BlurLocationItem-IA;)V

    iput-object v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->blurLocationItem:Lcom/android/server/location/gnss/GnssLocationProviderImpl$BlurLocationItem;

    .line 595
    iget-object v2, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->blurLocationCollector:Ljava/util/Stack;

    invoke-virtual {v2, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 596
    iput v1, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mBlurState:I

    .line 597
    iget-object v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mBlurLocation:Lcom/android/server/location/MiuiBlurLocationManagerImpl;

    if-nez v0, :cond_0

    .line 598
    new-instance v0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;

    invoke-direct {v0}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;-><init>()V

    iput-object v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mBlurLocation:Lcom/android/server/location/MiuiBlurLocationManagerImpl;

    .line 600
    :cond_0
    invoke-static {}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->get()Lcom/android/server/location/MiuiBlurLocationManagerStub;

    move-result-object v0

    iget v2, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mBlurUid:I

    iget-object v3, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mBlurPackageName:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lcom/android/server/location/MiuiBlurLocationManagerStub;->isBlurLocationMode(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 601
    iput v1, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mBlurState:I

    .line 602
    iget-object v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->blurLocationCollector:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V

    goto :goto_0

    .line 605
    :cond_1
    iget v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mBlurState:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mBlurState:I

    .line 607
    :cond_2
    :goto_0
    iput p1, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mBlurUid:I

    .line 608
    iput-object p2, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mBlurPackageName:Ljava/lang/String;

    .line 609
    iget-object v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mGnssEventHandler:Lcom/android/server/location/GnssEventHandler;

    const-string v1, "blurLocation_notify is on"

    invoke-virtual {v0, p2, v1}, Lcom/android/server/location/GnssEventHandler;->handleCallerName(Ljava/lang/String;Ljava/lang/String;)V

    .line 610
    return-void
.end method

.method public writeLocationInformation(Ljava/lang/String;)V
    .locals 6
    .param p1, "event"    # Ljava/lang/String;

    .line 425
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "writeLocationInformation:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->logd(Ljava/lang/String;)V

    .line 426
    sget-object v0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mLocationInformationLock:Ljava/lang/Object;

    monitor-enter v0

    .line 427
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->getCurrentTime()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 428
    .local v1, "info":Ljava/lang/String;
    :try_start_1
    new-instance v2, Ljava/io/BufferedWriter;

    new-instance v3, Ljava/io/FileWriter;

    .line 429
    invoke-direct {p0}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->getLocationFilePath()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-direct {v3, v4, v5}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;Z)V

    invoke-direct {v2, v3}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 430
    .local v2, "writer":Ljava/io/BufferedWriter;
    :try_start_2
    invoke-virtual {v2, v1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 431
    :try_start_3
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 433
    .end local v2    # "writer":Ljava/io/BufferedWriter;
    goto :goto_1

    .line 428
    .restart local v2    # "writer":Ljava/io/BufferedWriter;
    :catchall_0
    move-exception v3

    :try_start_4
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v4

    :try_start_5
    invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v1    # "info":Ljava/lang/String;
    .end local p0    # "this":Lcom/android/server/location/gnss/GnssLocationProviderImpl;
    .end local p1    # "event":Ljava/lang/String;
    :goto_0
    throw v3
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 431
    .end local v2    # "writer":Ljava/io/BufferedWriter;
    .restart local v1    # "info":Ljava/lang/String;
    .restart local p0    # "this":Lcom/android/server/location/gnss/GnssLocationProviderImpl;
    .restart local p1    # "event":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 432
    .local v2, "e":Ljava/io/IOException;
    :try_start_6
    const-string v3, "IO exception"

    invoke-static {v3}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->loge(Ljava/lang/String;)V

    .line 434
    .end local v1    # "info":Ljava/lang/String;
    .end local v2    # "e":Ljava/io/IOException;
    :goto_1
    monitor-exit v0

    .line 435
    return-void

    .line 434
    :catchall_2
    move-exception v1

    monitor-exit v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v1
.end method
