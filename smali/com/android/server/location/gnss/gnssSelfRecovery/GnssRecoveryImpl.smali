.class public Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl;
.super Ljava/lang/Object;
.source "GnssRecoveryImpl.java"

# interfaces
.implements Lcom/android/server/location/gnss/gnssSelfRecovery/IGnssRecovery;


# static fields
.field private static final TAG:Ljava/lang/String; = "GnssRecoveryImpl"


# instance fields
.field private enablefulltracking:Z

.field private mContext:Landroid/content/Context;

.field private mFieldEnableCorrVecOutputs:Ljava/lang/reflect/Field;

.field private mFieldEnableFullTracking:Ljava/lang/reflect/Field;

.field private mFieldIntervalMillis:Ljava/lang/reflect/Field;

.field private mFieldRegisterGnssMeasurement:Ljava/lang/reflect/Field;

.field private final mFile:Ljava/io/File;

.field private mGnssHal:Ljava/lang/Object;

.field private mGnssNative:Lcom/android/server/location/gnss/hal/GnssNative;

.field private startMeasurementMethod:Ljava/lang/reflect/Method;

.field private startMethod:Ljava/lang/reflect/Method;

.field private stopMethod:Ljava/lang/reflect/Method;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "mContext"    # Landroid/content/Context;

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v2

    const-string/jumbo v3, "system"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const-string v2, "gnss_properties.xml"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl;->mFile:Ljava/io/File;

    .line 50
    iput-object p1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl;->mContext:Landroid/content/Context;

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/server/location/gnss/hal/GnssNative;)V
    .locals 4
    .param p1, "mContext"    # Landroid/content/Context;
    .param p2, "gnssNative"    # Lcom/android/server/location/gnss/hal/GnssNative;

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v2

    const-string/jumbo v3, "system"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const-string v2, "gnss_properties.xml"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl;->mFile:Ljava/io/File;

    .line 45
    iput-object p1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl;->mContext:Landroid/content/Context;

    .line 46
    iput-object p2, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl;->mGnssNative:Lcom/android/server/location/gnss/hal/GnssNative;

    .line 47
    return-void
.end method

.method private checkMockOpPermission()Z
    .locals 1

    .line 139
    const/4 v0, 0x0

    return v0
.end method

.method private doStartMeasurementByInstance()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/reflect/InvocationTargetException;
        }
    .end annotation

    .line 155
    iget-object v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl;->mFieldRegisterGnssMeasurement:Ljava/lang/reflect/Field;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl;->mGnssHal:Ljava/lang/Object;

    .line 156
    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->getBoolean(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 157
    const-string v0, "doStartMeasurementByInstance"

    const-string v1, "GnssRecoveryImpl"

    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    iget-object v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl;->startMeasurementMethod:Ljava/lang/reflect/Method;

    iget-object v2, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl;->mGnssHal:Ljava/lang/Object;

    iget-object v3, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl;->mFieldEnableFullTracking:Ljava/lang/reflect/Field;

    .line 159
    invoke-virtual {v3, v2}, Ljava/lang/reflect/Field;->getBoolean(Ljava/lang/Object;)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl;->mFieldEnableCorrVecOutputs:Ljava/lang/reflect/Field;

    iget-object v5, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl;->mGnssHal:Ljava/lang/Object;

    .line 160
    invoke-virtual {v4, v5}, Ljava/lang/reflect/Field;->getBoolean(Ljava/lang/Object;)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iget-object v5, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl;->mFieldIntervalMillis:Ljava/lang/reflect/Field;

    iget-object v6, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl;->mGnssHal:Ljava/lang/Object;

    .line 161
    invoke-virtual {v5, v6}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    filled-new-array {v3, v4, v5}, [Ljava/lang/Object;

    move-result-object v3

    .line 158
    invoke-virtual {v0, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 162
    iget-object v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl;->mFieldRegisterGnssMeasurement:Ljava/lang/reflect/Field;

    iget-object v2, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl;->mGnssHal:Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/lang/reflect/Field;->setBoolean(Ljava/lang/Object;Z)V

    .line 163
    const-string v0, "mGnssHal.startMeasurementCollection()"

    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    :cond_0
    return-void
.end method

.method private getGnssHal()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 143
    const-string v0, "com.android.server.location.gnss.hal.GnssNative$GnssHal"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 144
    .local v0, "innerClazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v1, 0x0

    new-array v2, v1, [Ljava/lang/Class;

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    new-array v3, v1, [Ljava/lang/Object;

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl;->mGnssHal:Ljava/lang/Object;

    .line 145
    const-string/jumbo v2, "start"

    new-array v3, v1, [Ljava/lang/Class;

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl;->startMethod:Ljava/lang/reflect/Method;

    .line 146
    const-string/jumbo v2, "stop"

    new-array v1, v1, [Ljava/lang/Class;

    invoke-virtual {v0, v2, v1}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl;->stopMethod:Ljava/lang/reflect/Method;

    .line 147
    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    sget-object v2, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    filled-new-array {v1, v2, v3}, [Ljava/lang/Class;

    move-result-object v1

    const-string/jumbo v2, "startMeasurementCollection"

    invoke-virtual {v0, v2, v1}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl;->startMeasurementMethod:Ljava/lang/reflect/Method;

    .line 148
    const-string v1, "mRegisterGnssMeasurement"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl;->mFieldRegisterGnssMeasurement:Ljava/lang/reflect/Field;

    .line 149
    const-string v1, "mEnableFullTracking"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl;->mFieldEnableFullTracking:Ljava/lang/reflect/Field;

    .line 150
    const-string v1, "mEnableCorrVecOutputs"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl;->mFieldEnableCorrVecOutputs:Ljava/lang/reflect/Field;

    .line 151
    const-string v1, "mIntervalMillis"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl;->mFieldIntervalMillis:Ljava/lang/reflect/Field;

    .line 152
    return-void
.end method

.method private grantMockOpPermission()V
    .locals 5

    .line 126
    iget-object v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 127
    return-void

    .line 129
    :cond_0
    const-string v1, "appops"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AppOpsManager;

    .line 131
    .local v0, "appOpsManager":Landroid/app/AppOpsManager;
    :try_start_0
    const-string v1, "android"

    const/4 v2, 0x0

    const/16 v3, 0x3a

    const/16 v4, 0x3e8

    invoke-virtual {v0, v3, v4, v1, v2}, Landroid/app/AppOpsManager;->setMode(IILjava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 134
    goto :goto_0

    .line 132
    :catch_0
    move-exception v1

    .line 133
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "grant mockLocationPermission failed, cause:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "GnssRecoveryImpl"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method static synthetic lambda$removeMockLocation$0(Landroid/location/LocationManager;)V
    .locals 2
    .param p0, "mLocationManager"    # Landroid/location/LocationManager;

    .line 108
    const/4 v0, 0x0

    sget-object v1, Landroid/os/UserHandle;->SYSTEM:Landroid/os/UserHandle;

    invoke-virtual {p0, v0, v1}, Landroid/location/LocationManager;->setLocationEnabledForUser(ZLandroid/os/UserHandle;)V

    .line 109
    const/4 v0, 0x1

    sget-object v1, Landroid/os/UserHandle;->SYSTEM:Landroid/os/UserHandle;

    invoke-virtual {p0, v0, v1}, Landroid/location/LocationManager;->setLocationEnabledForUser(ZLandroid/os/UserHandle;)V

    .line 110
    return-void
.end method

.method private removeMockAppPkgName()V
    .locals 3

    .line 117
    :try_start_0
    iget-object v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl;->mFile:Ljava/io/File;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/io/File;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 118
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "mockAppPackageName"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 119
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 122
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    goto :goto_0

    .line 120
    :catch_0
    move-exception v0

    .line 121
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "GnssRecoveryImpl"

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method


# virtual methods
.method public removeMockLocation()Z
    .locals 12

    .line 86
    const-string v0, "removeMockLocation()"

    const-string v1, "GnssRecoveryImpl"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    const/4 v0, 0x0

    .line 88
    .local v0, "hasMockOpPermission":Z
    invoke-direct {p0}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl;->checkMockOpPermission()Z

    move-result v2

    if-nez v2, :cond_0

    .line 89
    invoke-direct {p0}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl;->grantMockOpPermission()V

    .line 91
    :cond_0
    iget-object v2, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl;->mContext:Landroid/content/Context;

    .line 92
    const-string v3, "location"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/location/LocationManager;

    .line 93
    .local v2, "mLocationManager":Landroid/location/LocationManager;
    const-string v3, "network"

    const-string v4, "fused"

    const-string v5, "gps"

    filled-new-array {v5, v3, v4}, [Ljava/lang/String;

    move-result-object v3

    .line 96
    .local v3, "allProviders":[Ljava/lang/String;
    array-length v4, v3

    const/4 v6, 0x0

    move v7, v6

    :goto_0
    if-ge v7, v4, :cond_1

    aget-object v8, v3, v7

    .line 99
    .local v8, "provider":Ljava/lang/String;
    :try_start_0
    invoke-virtual {v2, v8}, Landroid/location/LocationManager;->removeTestProvider(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 102
    goto :goto_1

    .line 100
    :catch_0
    move-exception v9

    .line 101
    .local v9, "e":Ljava/lang/Exception;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "exception in remove mock providers : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {v9}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v1, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    .end local v8    # "provider":Ljava/lang/String;
    .end local v9    # "e":Ljava/lang/Exception;
    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 104
    :cond_1
    invoke-direct {p0}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl;->removeMockAppPkgName()V

    .line 105
    invoke-virtual {v2, v5}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v1

    .line 106
    .local v1, "gpsEnable":Z
    if-nez v1, :cond_2

    .line 107
    new-instance v4, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v5, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl$$ExternalSyntheticLambda0;

    invoke-direct {v5, v2}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl$$ExternalSyntheticLambda0;-><init>(Landroid/location/LocationManager;)V

    const-wide/16 v7, 0xc8

    invoke-virtual {v4, v5, v7, v8}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 112
    :cond_2
    return v6
.end method

.method public restartGnss()V
    .locals 5

    .line 55
    const-string v0, "GnssRecoveryImpl"

    iget-object v1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl;->mContext:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 56
    return-void

    .line 59
    :cond_0
    :try_start_0
    const-string v2, "location"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/location/LocationManager;

    .line 60
    .local v1, "locationManager":Landroid/location/LocationManager;
    const-string v2, "gps"

    invoke-virtual {v1, v2}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v2

    .line 61
    .local v2, "gpsEnable":Z
    if-nez v2, :cond_1

    .line 62
    const-string v3, "gnss is close, don\'t need restart"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    return-void

    .line 65
    :cond_1
    const-string v3, "restart gnss"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v4, v3}, Landroid/location/LocationManager;->setLocationEnabledForUser(ZLandroid/os/UserHandle;)V

    .line 67
    const-wide/16 v3, 0x64

    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V

    .line 68
    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v1, v4, v3}, Landroid/location/LocationManager;->setLocationEnabledForUser(ZLandroid/os/UserHandle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 71
    .end local v1    # "locationManager":Landroid/location/LocationManager;
    .end local v2    # "gpsEnable":Z
    goto :goto_0

    .line 69
    :catch_0
    move-exception v1

    .line 70
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "exception in restart gps : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public setFullTracking(Z)Z
    .locals 3
    .param p1, "enablefulltracking"    # Z

    .line 76
    iget-object v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl;->mGnssNative:Lcom/android/server/location/gnss/hal/GnssNative;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 77
    return v1

    .line 79
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "fulltracking: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "GnssRecoveryImpl"

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    iget-object v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl;->mGnssNative:Lcom/android/server/location/gnss/hal/GnssNative;

    const v2, 0x7fffffff

    invoke-virtual {v0, p1, v1, v2}, Lcom/android/server/location/gnss/hal/GnssNative;->startMeasurementCollection(ZZI)Z

    .line 81
    const/4 v0, 0x1

    return v0
.end method
