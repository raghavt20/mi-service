.class public Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;
.super Ljava/lang/Object;
.source "DiagnoticResult.java"


# instance fields
.field private diagnosticName:Ljava/lang/String;

.field private performance:D

.field private result:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 0
    .param p1, "diagnosticName"    # Ljava/lang/String;
    .param p2, "result"    # Z

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;->diagnosticName:Ljava/lang/String;

    .line 13
    iput-boolean p2, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;->result:Z

    .line 14
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ZD)V
    .locals 0
    .param p1, "diagnosticName"    # Ljava/lang/String;
    .param p2, "result"    # Z
    .param p3, "performance"    # D

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;->diagnosticName:Ljava/lang/String;

    .line 18
    iput-boolean p2, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;->result:Z

    .line 19
    iput-wide p3, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;->performance:D

    .line 20
    return-void
.end method


# virtual methods
.method public getDiagnosticName()Ljava/lang/String;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;->diagnosticName:Ljava/lang/String;

    return-object v0
.end method

.method public getPerformance()D
    .locals 2

    .line 39
    iget-wide v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;->performance:D

    return-wide v0
.end method

.method public getResult()Z
    .locals 1

    .line 31
    iget-boolean v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;->result:Z

    return v0
.end method

.method public setDiagnosticName(Ljava/lang/String;)V
    .locals 0
    .param p1, "diagnosticName"    # Ljava/lang/String;

    .line 27
    iput-object p1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;->diagnosticName:Ljava/lang/String;

    .line 28
    return-void
.end method

.method public setPerformance(D)V
    .locals 0
    .param p1, "performance"    # D

    .line 43
    iput-wide p1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;->performance:D

    .line 44
    return-void
.end method

.method public setResult(Z)V
    .locals 0
    .param p1, "result"    # Z

    .line 35
    iput-boolean p1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;->result:Z

    .line 36
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 48
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DiagnoticResult{diagnosticName=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;->diagnosticName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", result="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;->result:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", performance="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;->performance:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
