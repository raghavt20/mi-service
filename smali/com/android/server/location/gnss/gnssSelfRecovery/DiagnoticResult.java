public class com.android.server.location.gnss.gnssSelfRecovery.DiagnoticResult {
	 /* .source "DiagnoticResult.java" */
	 /* # instance fields */
	 private java.lang.String diagnosticName;
	 private Double performance;
	 private Boolean result;
	 /* # direct methods */
	 public com.android.server.location.gnss.gnssSelfRecovery.DiagnoticResult ( ) {
		 /* .locals 0 */
		 /* .line 8 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 9 */
		 return;
	 } // .end method
	 public com.android.server.location.gnss.gnssSelfRecovery.DiagnoticResult ( ) {
		 /* .locals 0 */
		 /* .param p1, "diagnosticName" # Ljava/lang/String; */
		 /* .param p2, "result" # Z */
		 /* .line 11 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 12 */
		 this.diagnosticName = p1;
		 /* .line 13 */
		 /* iput-boolean p2, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;->result:Z */
		 /* .line 14 */
		 return;
	 } // .end method
	 public com.android.server.location.gnss.gnssSelfRecovery.DiagnoticResult ( ) {
		 /* .locals 0 */
		 /* .param p1, "diagnosticName" # Ljava/lang/String; */
		 /* .param p2, "result" # Z */
		 /* .param p3, "performance" # D */
		 /* .line 16 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 17 */
		 this.diagnosticName = p1;
		 /* .line 18 */
		 /* iput-boolean p2, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;->result:Z */
		 /* .line 19 */
		 /* iput-wide p3, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;->performance:D */
		 /* .line 20 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public java.lang.String getDiagnosticName ( ) {
		 /* .locals 1 */
		 /* .line 23 */
		 v0 = this.diagnosticName;
	 } // .end method
	 public Double getPerformance ( ) {
		 /* .locals 2 */
		 /* .line 39 */
		 /* iget-wide v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;->performance:D */
		 /* return-wide v0 */
	 } // .end method
	 public Boolean getResult ( ) {
		 /* .locals 1 */
		 /* .line 31 */
		 /* iget-boolean v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;->result:Z */
	 } // .end method
	 public void setDiagnosticName ( java.lang.String p0 ) {
		 /* .locals 0 */
		 /* .param p1, "diagnosticName" # Ljava/lang/String; */
		 /* .line 27 */
		 this.diagnosticName = p1;
		 /* .line 28 */
		 return;
	 } // .end method
	 public void setPerformance ( Double p0 ) {
		 /* .locals 0 */
		 /* .param p1, "performance" # D */
		 /* .line 43 */
		 /* iput-wide p1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;->performance:D */
		 /* .line 44 */
		 return;
	 } // .end method
	 public void setResult ( Boolean p0 ) {
		 /* .locals 0 */
		 /* .param p1, "result" # Z */
		 /* .line 35 */
		 /* iput-boolean p1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;->result:Z */
		 /* .line 36 */
		 return;
	 } // .end method
	 public java.lang.String toString ( ) {
		 /* .locals 3 */
		 /* .line 48 */
		 /* new-instance v0, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v1 = "DiagnoticResult{diagnosticName=\'"; // const-string v1, "DiagnoticResult{diagnosticName=\'"
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 v1 = this.diagnosticName;
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* const/16 v1, 0x27 */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
		 final String v1 = ", result="; // const-string v1, ", result="
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget-boolean v1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;->result:Z */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
		 final String v1 = ", performance="; // const-string v1, ", performance="
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget-wide v1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;->performance:D */
		 (( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
		 /* const/16 v1, 0x7d */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 } // .end method
