.class public Lcom/android/server/location/gnss/gnssSelfRecovery/MockLocationDiagnostic;
.super Lcom/android/server/location/gnss/gnssSelfRecovery/GnssDiagnosticsBase;
.source "MockLocationDiagnostic.java"


# static fields
.field public static final DIAG_ITEM_NAME_MOCK:Ljava/lang/String; = "mockDiagsResult"

.field private static final TAG:Ljava/lang/String; = "MockLocationDiagnostic"


# instance fields
.field private isMockLocation:Z

.field private isRuning:Z

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mockDiagResult:Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;

.field private sharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 26
    invoke-direct {p0, p1, p2}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssDiagnosticsBase;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    .line 27
    iput-object p1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/MockLocationDiagnostic;->mContext:Landroid/content/Context;

    .line 28
    iput-object p2, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/MockLocationDiagnostic;->mHandler:Landroid/os/Handler;

    .line 29
    return-void
.end method

.method private checkIfMockAppAlive(Ljava/lang/String;)Z
    .locals 9
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 78
    iget-object v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/MockLocationDiagnostic;->mContext:Landroid/content/Context;

    .line 79
    const-string v1, "activity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 80
    .local v0, "mActivityManager":Landroid/app/ActivityManager;
    nop

    .line 81
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v1

    .line 82
    .local v1, "processInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x0

    if-ge v2, v3, :cond_2

    .line 83
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 84
    .local v3, "processInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    iget-object v5, v3, Landroid/app/ActivityManager$RunningAppProcessInfo;->pkgList:[Ljava/lang/String;

    .line 85
    .local v5, "pkgList":[Ljava/lang/String;
    array-length v6, v5

    :goto_1
    if-ge v4, v6, :cond_1

    aget-object v7, v5, v4

    .line 86
    .local v7, "packageName":Ljava/lang/String;
    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 87
    const-string v4, "MockLocationDiagnostic"

    const-string v6, "Service name"

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    const/4 v4, 0x1

    return v4

    .line 85
    .end local v7    # "packageName":Ljava/lang/String;
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 82
    .end local v3    # "processInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    .end local v5    # "pkgList":[Ljava/lang/String;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 92
    .end local v2    # "i":I
    :cond_2
    return v4
.end method

.method private getMockLocationPkgName(Landroid/content/Context;)Ljava/lang/String;
    .locals 5
    .param p1, "mContext"    # Landroid/content/Context;

    .line 64
    const-string v0, ""

    .line 65
    .local v0, "pkgName":Ljava/lang/String;
    if-nez p1, :cond_0

    return-object v0

    .line 67
    :cond_0
    :try_start_0
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v3

    const-string/jumbo v4, "system"

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const-string v3, "gnss_properties.xml"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 68
    .local v1, "mFile":Ljava/io/File;
    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/io/File;I)Landroid/content/SharedPreferences;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/MockLocationDiagnostic;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 69
    const-string v3, "mockAppPackageName"

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v2

    .line 72
    .end local v1    # "mFile":Ljava/io/File;
    goto :goto_0

    .line 70
    :catch_0
    move-exception v1

    .line 71
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "MockLocationDiagnostic"

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-object v0
.end method


# virtual methods
.method public diagnoseMockLocationInternel()V
    .locals 6

    .line 52
    const-string/jumbo v0, "start Diagnose mock location"

    const-string v1, "MockLocationDiagnostic"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    new-instance v0, Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;

    const-wide/16 v2, 0x0

    const-string v4, "mockDiagsResult"

    const/4 v5, 0x0

    invoke-direct {v0, v4, v5, v2, v3}, Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;-><init>(Ljava/lang/String;ZD)V

    iput-object v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/MockLocationDiagnostic;->mockDiagResult:Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;

    .line 54
    iget-object v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/MockLocationDiagnostic;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/android/server/location/gnss/gnssSelfRecovery/MockLocationDiagnostic;->getMockLocationPkgName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 55
    .local v0, "lastMockPkg":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/android/server/location/gnss/gnssSelfRecovery/MockLocationDiagnostic;->checkIfMockAppAlive(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 56
    iget-object v2, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/MockLocationDiagnostic;->mockDiagResult:Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;->setResult(Z)V

    goto :goto_0

    .line 58
    :cond_0
    iget-object v2, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/MockLocationDiagnostic;->mockDiagResult:Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;

    invoke-virtual {v2, v5}, Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;->setResult(Z)V

    .line 60
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "start Diagnose mock location result: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/MockLocationDiagnostic;->mockDiagResult:Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;

    invoke-virtual {v3}, Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    return-void
.end method

.method public finishDiagnostics()V
    .locals 0

    .line 97
    return-void
.end method

.method public getDiagnosticsResult()Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;
    .locals 1

    .line 102
    iget-object v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/MockLocationDiagnostic;->mockDiagResult:Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;

    return-object v0
.end method

.method public run()V
    .locals 2

    .line 33
    invoke-virtual {p0}, Lcom/android/server/location/gnss/gnssSelfRecovery/MockLocationDiagnostic;->diagnoseMockLocationInternel()V

    .line 34
    iget-object v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/MockLocationDiagnostic;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 35
    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 36
    .local v0, "message":Landroid/os/Message;
    const/16 v1, 0x65

    iput v1, v0, Landroid/os/Message;->what:I

    .line 37
    invoke-virtual {p0}, Lcom/android/server/location/gnss/gnssSelfRecovery/MockLocationDiagnostic;->getDiagnosticsResult()Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 38
    iget-object v1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/MockLocationDiagnostic;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 40
    .end local v0    # "message":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method public startDiagnostics()V
    .locals 2

    .line 44
    iget-boolean v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/MockLocationDiagnostic;->isRuning:Z

    if-nez v0, :cond_0

    .line 45
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/MockLocationDiagnostic;->isRuning:Z

    .line 46
    new-instance v0, Ljava/lang/Thread;

    const-string v1, "mockDiagsThread"

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 47
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 49
    .end local v0    # "thread":Ljava/lang/Thread;
    :cond_0
    return-void
.end method
