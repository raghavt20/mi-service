public class com.android.server.location.gnss.gnssSelfRecovery.GnssRecoveryImpl implements com.android.server.location.gnss.gnssSelfRecovery.IGnssRecovery {
	 /* .source "GnssRecoveryImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private Boolean enablefulltracking;
	 private android.content.Context mContext;
	 private java.lang.reflect.Field mFieldEnableCorrVecOutputs;
	 private java.lang.reflect.Field mFieldEnableFullTracking;
	 private java.lang.reflect.Field mFieldIntervalMillis;
	 private java.lang.reflect.Field mFieldRegisterGnssMeasurement;
	 private final java.io.File mFile;
	 private java.lang.Object mGnssHal;
	 private com.android.server.location.gnss.hal.GnssNative mGnssNative;
	 private java.lang.reflect.Method startMeasurementMethod;
	 private java.lang.reflect.Method startMethod;
	 private java.lang.reflect.Method stopMethod;
	 /* # direct methods */
	 public com.android.server.location.gnss.gnssSelfRecovery.GnssRecoveryImpl ( ) {
		 /* .locals 4 */
		 /* .param p1, "mContext" # Landroid/content/Context; */
		 /* .line 49 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 31 */
		 /* new-instance v0, Ljava/io/File; */
		 /* new-instance v1, Ljava/io/File; */
		 android.os.Environment .getDataDirectory ( );
		 /* const-string/jumbo v3, "system" */
		 /* invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
		 final String v2 = "gnss_properties.xml"; // const-string v2, "gnss_properties.xml"
		 /* invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
		 this.mFile = v0;
		 /* .line 50 */
		 this.mContext = p1;
		 /* .line 51 */
		 return;
	 } // .end method
	 public com.android.server.location.gnss.gnssSelfRecovery.GnssRecoveryImpl ( ) {
		 /* .locals 4 */
		 /* .param p1, "mContext" # Landroid/content/Context; */
		 /* .param p2, "gnssNative" # Lcom/android/server/location/gnss/hal/GnssNative; */
		 /* .line 44 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 31 */
		 /* new-instance v0, Ljava/io/File; */
		 /* new-instance v1, Ljava/io/File; */
		 android.os.Environment .getDataDirectory ( );
		 /* const-string/jumbo v3, "system" */
		 /* invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
		 final String v2 = "gnss_properties.xml"; // const-string v2, "gnss_properties.xml"
		 /* invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
		 this.mFile = v0;
		 /* .line 45 */
		 this.mContext = p1;
		 /* .line 46 */
		 this.mGnssNative = p2;
		 /* .line 47 */
		 return;
	 } // .end method
	 private Boolean checkMockOpPermission ( ) {
		 /* .locals 1 */
		 /* .line 139 */
		 int v0 = 0; // const/4 v0, 0x0
	 } // .end method
	 private void doStartMeasurementByInstance ( ) {
		 /* .locals 7 */
		 /* .annotation system Ldalvik/annotation/Throws; */
		 /* value = { */
		 /* Ljava/lang/IllegalAccessException;, */
		 /* Ljava/lang/reflect/InvocationTargetException; */
		 /* } */
	 } // .end annotation
	 /* .line 155 */
	 v0 = this.mFieldRegisterGnssMeasurement;
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 v1 = this.mGnssHal;
		 /* .line 156 */
		 v0 = 		 (( java.lang.reflect.Field ) v0 ).getBoolean ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->getBoolean(Ljava/lang/Object;)Z
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 157 */
			 final String v0 = "doStartMeasurementByInstance"; // const-string v0, "doStartMeasurementByInstance"
			 final String v1 = "GnssRecoveryImpl"; // const-string v1, "GnssRecoveryImpl"
			 android.util.Log .v ( v1,v0 );
			 /* .line 158 */
			 v0 = this.startMeasurementMethod;
			 v2 = this.mGnssHal;
			 v3 = this.mFieldEnableFullTracking;
			 /* .line 159 */
			 v3 = 			 (( java.lang.reflect.Field ) v3 ).getBoolean ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/reflect/Field;->getBoolean(Ljava/lang/Object;)Z
			 java.lang.Boolean .valueOf ( v3 );
			 v4 = this.mFieldEnableCorrVecOutputs;
			 v5 = this.mGnssHal;
			 /* .line 160 */
			 v4 = 			 (( java.lang.reflect.Field ) v4 ).getBoolean ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/reflect/Field;->getBoolean(Ljava/lang/Object;)Z
			 java.lang.Boolean .valueOf ( v4 );
			 v5 = this.mFieldIntervalMillis;
			 v6 = this.mGnssHal;
			 /* .line 161 */
			 v5 = 			 (( java.lang.reflect.Field ) v5 ).getInt ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I
			 java.lang.Integer .valueOf ( v5 );
			 /* filled-new-array {v3, v4, v5}, [Ljava/lang/Object; */
			 /* .line 158 */
			 (( java.lang.reflect.Method ) v0 ).invoke ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
			 /* .line 162 */
			 v0 = this.mFieldRegisterGnssMeasurement;
			 v2 = this.mGnssHal;
			 int v3 = 0; // const/4 v3, 0x0
			 (( java.lang.reflect.Field ) v0 ).setBoolean ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Ljava/lang/reflect/Field;->setBoolean(Ljava/lang/Object;Z)V
			 /* .line 163 */
			 final String v0 = "mGnssHal.startMeasurementCollection()"; // const-string v0, "mGnssHal.startMeasurementCollection()"
			 android.util.Log .v ( v1,v0 );
			 /* .line 165 */
		 } // :cond_0
		 return;
	 } // .end method
	 private void getGnssHal ( ) {
		 /* .locals 4 */
		 /* .annotation system Ldalvik/annotation/Throws; */
		 /* value = { */
		 /* Ljava/lang/Exception; */
		 /* } */
	 } // .end annotation
	 /* .line 143 */
	 final String v0 = "com.android.server.location.gnss.hal.GnssNative$GnssHal"; // const-string v0, "com.android.server.location.gnss.hal.GnssNative$GnssHal"
	 java.lang.Class .forName ( v0 );
	 /* .line 144 */
	 /* .local v0, "innerClazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
	 int v1 = 0; // const/4 v1, 0x0
	 /* new-array v2, v1, [Ljava/lang/Class; */
	 (( java.lang.Class ) v0 ).getDeclaredConstructor ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
	 /* new-array v3, v1, [Ljava/lang/Object; */
	 (( java.lang.reflect.Constructor ) v2 ).newInstance ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;
	 this.mGnssHal = v2;
	 /* .line 145 */
	 /* const-string/jumbo v2, "start" */
	 /* new-array v3, v1, [Ljava/lang/Class; */
	 (( java.lang.Class ) v0 ).getDeclaredMethod ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
	 this.startMethod = v2;
	 /* .line 146 */
	 /* const-string/jumbo v2, "stop" */
	 /* new-array v1, v1, [Ljava/lang/Class; */
	 (( java.lang.Class ) v0 ).getDeclaredMethod ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
	 this.stopMethod = v1;
	 /* .line 147 */
	 v1 = java.lang.Boolean.TYPE;
	 v2 = java.lang.Boolean.TYPE;
	 v3 = java.lang.Integer.TYPE;
	 /* filled-new-array {v1, v2, v3}, [Ljava/lang/Class; */
	 /* const-string/jumbo v2, "startMeasurementCollection" */
	 (( java.lang.Class ) v0 ).getDeclaredMethod ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
	 this.startMeasurementMethod = v1;
	 /* .line 148 */
	 final String v1 = "mRegisterGnssMeasurement"; // const-string v1, "mRegisterGnssMeasurement"
	 (( java.lang.Class ) v0 ).getDeclaredField ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
	 this.mFieldRegisterGnssMeasurement = v1;
	 /* .line 149 */
	 final String v1 = "mEnableFullTracking"; // const-string v1, "mEnableFullTracking"
	 (( java.lang.Class ) v0 ).getDeclaredField ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
	 this.mFieldEnableFullTracking = v1;
	 /* .line 150 */
	 final String v1 = "mEnableCorrVecOutputs"; // const-string v1, "mEnableCorrVecOutputs"
	 (( java.lang.Class ) v0 ).getDeclaredField ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
	 this.mFieldEnableCorrVecOutputs = v1;
	 /* .line 151 */
	 final String v1 = "mIntervalMillis"; // const-string v1, "mIntervalMillis"
	 (( java.lang.Class ) v0 ).getDeclaredField ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
	 this.mFieldIntervalMillis = v1;
	 /* .line 152 */
	 return;
} // .end method
private void grantMockOpPermission ( ) {
	 /* .locals 5 */
	 /* .line 126 */
	 v0 = this.mContext;
	 /* if-nez v0, :cond_0 */
	 /* .line 127 */
	 return;
	 /* .line 129 */
} // :cond_0
final String v1 = "appops"; // const-string v1, "appops"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/AppOpsManager; */
/* .line 131 */
/* .local v0, "appOpsManager":Landroid/app/AppOpsManager; */
try { // :try_start_0
	 final String v1 = "android"; // const-string v1, "android"
	 int v2 = 0; // const/4 v2, 0x0
	 /* const/16 v3, 0x3a */
	 /* const/16 v4, 0x3e8 */
	 (( android.app.AppOpsManager ) v0 ).setMode ( v3, v4, v1, v2 ); // invoke-virtual {v0, v3, v4, v1, v2}, Landroid/app/AppOpsManager;->setMode(IILjava/lang/String;I)V
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 134 */
	 /* .line 132 */
	 /* :catch_0 */
	 /* move-exception v1 */
	 /* .line 133 */
	 /* .local v1, "e":Ljava/lang/Exception; */
	 /* new-instance v2, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v3 = "grant mockLocationPermission failed, cause:"; // const-string v3, "grant mockLocationPermission failed, cause:"
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 android.util.Log .getStackTraceString ( v1 );
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v3 = "GnssRecoveryImpl"; // const-string v3, "GnssRecoveryImpl"
	 android.util.Log .d ( v3,v2 );
	 /* .line 136 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
static void lambda$removeMockLocation$0 ( android.location.LocationManager p0 ) { //synthethic
/* .locals 2 */
/* .param p0, "mLocationManager" # Landroid/location/LocationManager; */
/* .line 108 */
int v0 = 0; // const/4 v0, 0x0
v1 = android.os.UserHandle.SYSTEM;
(( android.location.LocationManager ) p0 ).setLocationEnabledForUser ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Landroid/location/LocationManager;->setLocationEnabledForUser(ZLandroid/os/UserHandle;)V
/* .line 109 */
int v0 = 1; // const/4 v0, 0x1
v1 = android.os.UserHandle.SYSTEM;
(( android.location.LocationManager ) p0 ).setLocationEnabledForUser ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Landroid/location/LocationManager;->setLocationEnabledForUser(ZLandroid/os/UserHandle;)V
/* .line 110 */
return;
} // .end method
private void removeMockAppPkgName ( ) {
/* .locals 3 */
/* .line 117 */
try { // :try_start_0
v0 = this.mContext;
v1 = this.mFile;
int v2 = 0; // const/4 v2, 0x0
(( android.content.Context ) v0 ).getSharedPreferences ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/io/File;I)Landroid/content/SharedPreferences;
/* .line 118 */
/* .local v0, "editor":Landroid/content/SharedPreferences$Editor; */
final String v1 = "mockAppPackageName"; // const-string v1, "mockAppPackageName"
final String v2 = ""; // const-string v2, ""
/* .line 119 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 122 */
} // .end local v0 # "editor":Landroid/content/SharedPreferences$Editor;
/* .line 120 */
/* :catch_0 */
/* move-exception v0 */
/* .line 121 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "GnssRecoveryImpl"; // const-string v1, "GnssRecoveryImpl"
android.util.Log .getStackTraceString ( v0 );
android.util.Log .e ( v1,v2 );
/* .line 123 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
/* # virtual methods */
public Boolean removeMockLocation ( ) {
/* .locals 12 */
/* .line 86 */
final String v0 = "removeMockLocation()"; // const-string v0, "removeMockLocation()"
final String v1 = "GnssRecoveryImpl"; // const-string v1, "GnssRecoveryImpl"
android.util.Log .d ( v1,v0 );
/* .line 87 */
int v0 = 0; // const/4 v0, 0x0
/* .line 88 */
/* .local v0, "hasMockOpPermission":Z */
v2 = /* invoke-direct {p0}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl;->checkMockOpPermission()Z */
/* if-nez v2, :cond_0 */
/* .line 89 */
/* invoke-direct {p0}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl;->grantMockOpPermission()V */
/* .line 91 */
} // :cond_0
v2 = this.mContext;
/* .line 92 */
final String v3 = "location"; // const-string v3, "location"
(( android.content.Context ) v2 ).getSystemService ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v2, Landroid/location/LocationManager; */
/* .line 93 */
/* .local v2, "mLocationManager":Landroid/location/LocationManager; */
final String v3 = "network"; // const-string v3, "network"
final String v4 = "fused"; // const-string v4, "fused"
final String v5 = "gps"; // const-string v5, "gps"
/* filled-new-array {v5, v3, v4}, [Ljava/lang/String; */
/* .line 96 */
/* .local v3, "allProviders":[Ljava/lang/String; */
/* array-length v4, v3 */
int v6 = 0; // const/4 v6, 0x0
/* move v7, v6 */
} // :goto_0
/* if-ge v7, v4, :cond_1 */
/* aget-object v8, v3, v7 */
/* .line 99 */
/* .local v8, "provider":Ljava/lang/String; */
try { // :try_start_0
(( android.location.LocationManager ) v2 ).removeTestProvider ( v8 ); // invoke-virtual {v2, v8}, Landroid/location/LocationManager;->removeTestProvider(Ljava/lang/String;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 102 */
/* .line 100 */
/* :catch_0 */
/* move-exception v9 */
/* .line 101 */
/* .local v9, "e":Ljava/lang/Exception; */
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
final String v11 = "exception in remove mock providers : "; // const-string v11, "exception in remove mock providers : "
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
android.util.Log .getStackTraceString ( v9 );
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v10 );
/* .line 96 */
} // .end local v8 # "provider":Ljava/lang/String;
} // .end local v9 # "e":Ljava/lang/Exception;
} // :goto_1
/* add-int/lit8 v7, v7, 0x1 */
/* .line 104 */
} // :cond_1
/* invoke-direct {p0}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl;->removeMockAppPkgName()V */
/* .line 105 */
v1 = (( android.location.LocationManager ) v2 ).isProviderEnabled ( v5 ); // invoke-virtual {v2, v5}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z
/* .line 106 */
/* .local v1, "gpsEnable":Z */
/* if-nez v1, :cond_2 */
/* .line 107 */
/* new-instance v4, Landroid/os/Handler; */
android.os.Looper .myLooper ( );
/* invoke-direct {v4, v5}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* new-instance v5, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v5, v2}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl$$ExternalSyntheticLambda0;-><init>(Landroid/location/LocationManager;)V */
/* const-wide/16 v7, 0xc8 */
(( android.os.Handler ) v4 ).postDelayed ( v5, v7, v8 ); // invoke-virtual {v4, v5, v7, v8}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 112 */
} // :cond_2
} // .end method
public void restartGnss ( ) {
/* .locals 5 */
/* .line 55 */
final String v0 = "GnssRecoveryImpl"; // const-string v0, "GnssRecoveryImpl"
v1 = this.mContext;
/* if-nez v1, :cond_0 */
/* .line 56 */
return;
/* .line 59 */
} // :cond_0
try { // :try_start_0
final String v2 = "location"; // const-string v2, "location"
(( android.content.Context ) v1 ).getSystemService ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v1, Landroid/location/LocationManager; */
/* .line 60 */
/* .local v1, "locationManager":Landroid/location/LocationManager; */
final String v2 = "gps"; // const-string v2, "gps"
v2 = (( android.location.LocationManager ) v1 ).isProviderEnabled ( v2 ); // invoke-virtual {v1, v2}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z
/* .line 61 */
/* .local v2, "gpsEnable":Z */
/* if-nez v2, :cond_1 */
/* .line 62 */
final String v3 = "gnss is close, don\'t need restart"; // const-string v3, "gnss is close, don\'t need restart"
android.util.Log .d ( v0,v3 );
/* .line 63 */
return;
/* .line 65 */
} // :cond_1
final String v3 = "restart gnss"; // const-string v3, "restart gnss"
android.util.Log .d ( v0,v3 );
/* .line 66 */
android.os.Process .myUserHandle ( );
int v4 = 0; // const/4 v4, 0x0
(( android.location.LocationManager ) v1 ).setLocationEnabledForUser ( v4, v3 ); // invoke-virtual {v1, v4, v3}, Landroid/location/LocationManager;->setLocationEnabledForUser(ZLandroid/os/UserHandle;)V
/* .line 67 */
/* const-wide/16 v3, 0x64 */
java.lang.Thread .sleep ( v3,v4 );
/* .line 68 */
android.os.Process .myUserHandle ( );
int v4 = 1; // const/4 v4, 0x1
(( android.location.LocationManager ) v1 ).setLocationEnabledForUser ( v4, v3 ); // invoke-virtual {v1, v4, v3}, Landroid/location/LocationManager;->setLocationEnabledForUser(ZLandroid/os/UserHandle;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 71 */
} // .end local v1 # "locationManager":Landroid/location/LocationManager;
} // .end local v2 # "gpsEnable":Z
/* .line 69 */
/* :catch_0 */
/* move-exception v1 */
/* .line 70 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "exception in restart gps : "; // const-string v3, "exception in restart gps : "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
android.util.Log .getStackTraceString ( v1 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v0,v2 );
/* .line 72 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public Boolean setFullTracking ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "enablefulltracking" # Z */
/* .line 76 */
v0 = this.mGnssNative;
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 77 */
/* .line 79 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "fulltracking: "; // const-string v2, "fulltracking: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "GnssRecoveryImpl"; // const-string v2, "GnssRecoveryImpl"
android.util.Log .d ( v2,v0 );
/* .line 80 */
v0 = this.mGnssNative;
/* const v2, 0x7fffffff */
(( com.android.server.location.gnss.hal.GnssNative ) v0 ).startMeasurementCollection ( p1, v1, v2 ); // invoke-virtual {v0, p1, v1, v2}, Lcom/android/server/location/gnss/hal/GnssNative;->startMeasurementCollection(ZZI)Z
/* .line 81 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
