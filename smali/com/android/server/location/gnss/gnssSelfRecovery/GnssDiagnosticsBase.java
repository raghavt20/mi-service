public abstract class com.android.server.location.gnss.gnssSelfRecovery.GnssDiagnosticsBase implements java.lang.Runnable {
	 /* .source "GnssDiagnosticsBase.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final Boolean DEBUG;
	 private static final Integer DIAGNOSTICS_TIME_OUT;
	 public static final java.lang.String LAST_MOCK_APP_PKG_NAME;
	 public static final Integer MSG_FINISHI_DIAGNOTIC;
	 public static final Integer MSG_MOCK_LOCATION;
	 public static final Integer MSG_WEAK_SIGNAL;
	 public static final Integer MSG_WEAK_SIGNAL_RECOVER;
	 public static final java.lang.String TAG;
	 /* # instance fields */
	 private Boolean isRunning;
	 private android.content.Context mContext;
	 private android.os.Handler mHandler;
	 /* # direct methods */
	 public com.android.server.location.gnss.gnssSelfRecovery.GnssDiagnosticsBase ( ) {
		 /* .locals 0 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .param p2, "handler" # Landroid/os/Handler; */
		 /* .line 24 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 25 */
		 this.mHandler = p2;
		 /* .line 26 */
		 this.mContext = p1;
		 /* .line 27 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public synchronized void finish ( ) {
		 /* .locals 1 */
		 /* monitor-enter p0 */
		 /* .line 41 */
		 try { // :try_start_0
			 /* iget-boolean v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssDiagnosticsBase;->isRunning:Z */
			 if ( v0 != null) { // if-eqz v0, :cond_0
				 /* .line 42 */
				 (( com.android.server.location.gnss.gnssSelfRecovery.GnssDiagnosticsBase ) p0 ).finishDiagnostics ( ); // invoke-virtual {p0}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssDiagnosticsBase;->finishDiagnostics()V
				 /* .line 43 */
				 int v0 = 0; // const/4 v0, 0x0
				 /* iput-boolean v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssDiagnosticsBase;->isRunning:Z */
				 /* :try_end_0 */
				 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
				 /* .line 45 */
			 } // .end local p0 # "this":Lcom/android/server/location/gnss/gnssSelfRecovery/GnssDiagnosticsBase;
		 } // :cond_0
		 /* monitor-exit p0 */
		 return;
		 /* .line 40 */
		 /* :catchall_0 */
		 /* move-exception v0 */
		 /* monitor-exit p0 */
		 /* throw v0 */
	 } // .end method
	 public abstract void finishDiagnostics ( ) {
	 } // .end method
	 public abstract com.android.server.location.gnss.gnssSelfRecovery.DiagnoticResult getDiagnosticsResult ( ) {
	 } // .end method
	 public void run ( ) {
		 /* .locals 1 */
		 /* .line 36 */
		 (( com.android.server.location.gnss.gnssSelfRecovery.GnssDiagnosticsBase ) p0 ).startDiagnostics ( ); // invoke-virtual {p0}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssDiagnosticsBase;->startDiagnostics()V
		 /* .line 37 */
		 int v0 = 1; // const/4 v0, 0x1
		 /* iput-boolean v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssDiagnosticsBase;->isRunning:Z */
		 /* .line 38 */
		 return;
	 } // .end method
	 public synchronized void start ( ) {
		 /* .locals 1 */
		 /* monitor-enter p0 */
		 /* .line 30 */
		 try { // :try_start_0
			 /* new-instance v0, Ljava/lang/Thread; */
			 /* invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V */
			 /* .line 31 */
			 /* .local v0, "thread":Ljava/lang/Thread; */
			 (( java.lang.Thread ) v0 ).start ( ); // invoke-virtual {v0}, Ljava/lang/Thread;->start()V
			 /* :try_end_0 */
			 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
			 /* .line 32 */
			 /* monitor-exit p0 */
			 return;
			 /* .line 29 */
		 } // .end local v0 # "thread":Ljava/lang/Thread;
	 } // .end local p0 # "this":Lcom/android/server/location/gnss/gnssSelfRecovery/GnssDiagnosticsBase;
	 /* :catchall_0 */
	 /* move-exception v0 */
	 /* monitor-exit p0 */
	 /* throw v0 */
} // .end method
public abstract void startDiagnostics ( ) {
} // .end method
