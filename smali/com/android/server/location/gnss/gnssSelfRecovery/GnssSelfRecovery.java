public class com.android.server.location.gnss.gnssSelfRecovery.GnssSelfRecovery implements com.android.server.location.gnss.gnssSelfRecovery.GnssSelfRecoveryStub {
	 /* .source "GnssSelfRecovery.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery$GnssDiagnoseHandler;, */
	 /* Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery$GnssRecoveryReason; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String GNSS_SELF_RECOVERY_PROP;
private static final Integer GPS_GSR_MAIN_BIT;
private static final Integer GPS_GSR_MOCK_BIT;
private static final Integer GPS_GSR_WEAK_SIGN_BIT;
public static final Integer REASON_MOCK_LOCATION;
public static final Integer REASON_WEAK_SIGNAL;
private static final java.lang.String TAG;
private static java.lang.String appList;
/* # instance fields */
private final java.util.ArrayList diagnosticsItemArrayList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Lcom/android/server/location/gnss/gnssSelfRecovery/GnssDiagnosticsBase;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.android.server.location.gnss.gnssSelfRecovery.IGnssRecovery gnssRecovery;
private com.android.server.location.gnss.gnssSelfRecovery.GnssWeakSignalDiagnostic gnssWeakSign;
private Boolean isEnableFullTracking;
private Boolean isMockFlag;
private final java.util.concurrent.atomic.AtomicBoolean isRunning;
private android.content.Context mContext;
private final java.io.File mFile;
private com.android.server.location.gnss.hal.GnssNative mGnssNative;
private android.os.Handler mHandler;
private android.os.HandlerThread mHandlerThread;
private Boolean mockStatus;
private Boolean weakSignStatus;
/* # direct methods */
static com.android.server.location.gnss.gnssSelfRecovery.IGnssRecovery -$$Nest$fgetgnssRecovery ( com.android.server.location.gnss.gnssSelfRecovery.GnssSelfRecovery p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.gnssRecovery;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.android.server.location.gnss.gnssSelfRecovery.GnssSelfRecovery p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static android.os.Handler -$$Nest$fgetmHandler ( com.android.server.location.gnss.gnssSelfRecovery.GnssSelfRecovery p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHandler;
} // .end method
static void -$$Nest$fputisMockFlag ( com.android.server.location.gnss.gnssSelfRecovery.GnssSelfRecovery p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->isMockFlag:Z */
return;
} // .end method
static com.android.server.location.gnss.gnssSelfRecovery.GnssSelfRecovery ( ) {
/* .locals 3 */
/* .line 42 */
final String v0 = "com.autonavi.minimap"; // const-string v0, "com.autonavi.minimap"
final String v1 = "com.tencent.map"; // const-string v1, "com.tencent.map"
final String v2 = "com.baidu.BaiduMap"; // const-string v2, "com.baidu.BaiduMap"
/* filled-new-array {v2, v0, v1}, [Ljava/lang/String; */
return;
} // .end method
public com.android.server.location.gnss.gnssSelfRecovery.GnssSelfRecovery ( ) {
/* .locals 4 */
/* .line 34 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 41 */
/* new-instance v0, Landroid/os/HandlerThread; */
final String v1 = "gnssSelfRecoveryThread"; // const-string v1, "gnssSelfRecoveryThread"
/* invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
this.mHandlerThread = v0;
/* .line 43 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.diagnosticsItemArrayList = v0;
/* .line 48 */
/* new-instance v0, Ljava/io/File; */
/* new-instance v1, Ljava/io/File; */
android.os.Environment .getDataDirectory ( );
/* const-string/jumbo v3, "system" */
/* invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
final String v2 = "gnss_properties.xml"; // const-string v2, "gnss_properties.xml"
/* invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
this.mFile = v0;
/* .line 49 */
/* new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V */
this.isRunning = v0;
/* .line 51 */
/* iput-boolean v1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->isMockFlag:Z */
/* .line 52 */
/* iput-boolean v1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->isEnableFullTracking:Z */
/* .line 53 */
/* iput-boolean v1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->mockStatus:Z */
/* .line 54 */
/* iput-boolean v1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->weakSignStatus:Z */
return;
} // .end method
public static java.lang.String getRecoveryReasonAsString ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "reason" # I */
/* .line 202 */
/* packed-switch p0, :pswitch_data_0 */
/* .line 208 */
final String v0 = "UNKNOWN"; // const-string v0, "UNKNOWN"
/* .line 206 */
/* :pswitch_0 */
/* const-string/jumbo v0, "weak GNSS signal" */
/* .line 204 */
/* :pswitch_1 */
final String v0 = "mock location"; // const-string v0, "mock location"
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private Boolean isUsingNaviApp ( android.os.WorkSource p0 ) {
/* .locals 6 */
/* .param p1, "mWorkSource" # Landroid/os/WorkSource; */
/* .line 159 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "workSource:" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "GnssSelfRecovery"; // const-string v1, "GnssSelfRecovery"
android.util.Log .d ( v1,v0 );
/* .line 160 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_0 */
/* .line 161 */
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
v3 = com.android.server.location.gnss.gnssSelfRecovery.GnssSelfRecovery.appList;
/* array-length v3, v3 */
/* if-ge v2, v3, :cond_3 */
/* .line 162 */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "j":I */
} // :goto_1
v4 = (( android.os.WorkSource ) p1 ).size ( ); // invoke-virtual {p1}, Landroid/os/WorkSource;->size()I
/* if-ge v3, v4, :cond_2 */
/* .line 163 */
v4 = com.android.server.location.gnss.gnssSelfRecovery.GnssSelfRecovery.appList;
/* aget-object v4, v4, v2 */
(( android.os.WorkSource ) p1 ).getPackageName ( v3 ); // invoke-virtual {p1, v3}, Landroid/os/WorkSource;->getPackageName(I)Ljava/lang/String;
v4 = (( java.lang.String ) v4 ).equals ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 164 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "is navic app :"; // const-string v4, "is navic app :"
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.os.WorkSource ) p1 ).getPackageName ( v3 ); // invoke-virtual {p1, v3}, Landroid/os/WorkSource;->getPackageName(I)Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v0 );
/* .line 165 */
int v0 = 1; // const/4 v0, 0x1
/* .line 162 */
} // :cond_1
/* add-int/lit8 v3, v3, 0x1 */
/* .line 161 */
} // .end local v3 # "j":I
} // :cond_2
/* add-int/lit8 v2, v2, 0x1 */
/* .line 169 */
} // .end local v2 # "i":I
} // :cond_3
} // .end method
/* # virtual methods */
public synchronized void deInit ( ) {
/* .locals 3 */
/* monitor-enter p0 */
/* .line 98 */
try { // :try_start_0
final String v0 = "GnssSelfRecovery"; // const-string v0, "GnssSelfRecovery"
final String v1 = "deInit"; // const-string v1, "deInit"
android.util.Log .i ( v0,v1 );
/* .line 99 */
v0 = this.mHandler;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 101 */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_1
(( android.os.Handler ) v0 ).removeCallbacksAndMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 104 */
/* .line 102 */
} // .end local p0 # "this":Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;
/* :catch_0 */
/* move-exception v0 */
/* .line 103 */
/* .local v0, "e":Ljava/lang/Exception; */
try { // :try_start_2
final String v1 = "GnssSelfRecovery"; // const-string v1, "GnssSelfRecovery"
android.util.Log .getStackTraceString ( v0 );
android.util.Log .e ( v1,v2 );
/* .line 106 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_0
} // :goto_0
v0 = this.diagnosticsItemArrayList;
(( java.util.ArrayList ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
/* .line 107 */
v0 = this.isRunning;
int v1 = 0; // const/4 v1, 0x0
(( java.util.concurrent.atomic.AtomicBoolean ) v0 ).set ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 108 */
/* monitor-exit p0 */
return;
/* .line 97 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* throw v0 */
} // .end method
public void diagnosticMockLocation ( ) {
/* .locals 3 */
/* .line 112 */
v0 = this.mHandler;
if ( v0 != null) { // if-eqz v0, :cond_2
/* iget-boolean v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->mockStatus:Z */
/* if-nez v0, :cond_0 */
/* .line 115 */
} // :cond_0
final String v0 = "GnssSelfRecovery"; // const-string v0, "GnssSelfRecovery"
/* const-string/jumbo v1, "start diagnostic mock location" */
android.util.Log .d ( v0,v1 );
/* .line 116 */
/* iget-boolean v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->isMockFlag:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 117 */
/* new-instance v0, Lcom/android/server/location/gnss/gnssSelfRecovery/MockLocationDiagnostic; */
v1 = this.mContext;
/* .line 118 */
(( android.content.Context ) v1 ).getApplicationContext ( ); // invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;
v2 = this.mHandler;
/* invoke-direct {v0, v1, v2}, Lcom/android/server/location/gnss/gnssSelfRecovery/MockLocationDiagnostic;-><init>(Landroid/content/Context;Landroid/os/Handler;)V */
/* .line 119 */
/* .local v0, "mMockLocationDiagnostic":Lcom/android/server/location/gnss/gnssSelfRecovery/GnssDiagnosticsBase; */
(( com.android.server.location.gnss.gnssSelfRecovery.GnssDiagnosticsBase ) v0 ).start ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssDiagnosticsBase;->start()V
/* .line 121 */
} // .end local v0 # "mMockLocationDiagnostic":Lcom/android/server/location/gnss/gnssSelfRecovery/GnssDiagnosticsBase;
} // :cond_1
return;
/* .line 113 */
} // :cond_2
} // :goto_0
return;
} // .end method
public void finishDiagnostic ( ) {
/* .locals 3 */
/* .line 174 */
final String v0 = "finishDiagnostic"; // const-string v0, "finishDiagnostic"
final String v1 = "GnssSelfRecovery"; // const-string v1, "GnssSelfRecovery"
android.util.Log .i ( v1,v0 );
/* .line 175 */
v0 = this.gnssWeakSign;
/* if-nez v0, :cond_0 */
/* .line 176 */
return;
/* .line 180 */
} // :cond_0
try { // :try_start_0
(( com.android.server.location.gnss.gnssSelfRecovery.GnssWeakSignalDiagnostic ) v0 ).finishDiagnostics ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssWeakSignalDiagnostic;->finishDiagnostics()V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 183 */
/* .line 181 */
/* :catch_0 */
/* move-exception v0 */
/* .line 182 */
/* .local v0, "e":Ljava/lang/Exception; */
android.util.Log .getStackTraceString ( v0 );
android.util.Log .e ( v1,v2 );
/* .line 184 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public void init ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 81 */
this.mContext = p1;
/* .line 82 */
v0 = this.isRunning;
v0 = (( java.util.concurrent.atomic.AtomicBoolean ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 83 */
return;
/* .line 85 */
} // :cond_0
final String v0 = "init"; // const-string v0, "init"
final String v1 = "GnssSelfRecovery"; // const-string v1, "GnssSelfRecovery"
android.util.Log .i ( v1,v0 );
/* .line 87 */
try { // :try_start_0
v0 = this.mHandlerThread;
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 88 */
/* new-instance v0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery$GnssDiagnoseHandler; */
v2 = this.mHandlerThread;
(( android.os.HandlerThread ) v2 ).getLooper ( ); // invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v2}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery$GnssDiagnoseHandler;-><init>(Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 89 */
(( com.android.server.location.gnss.gnssSelfRecovery.GnssSelfRecovery ) p0 ).setGsrConfig ( ); // invoke-virtual {p0}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->setGsrConfig()V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 92 */
/* .line 90 */
/* :catch_0 */
/* move-exception v0 */
/* .line 91 */
/* .local v0, "e":Ljava/lang/Exception; */
android.util.Log .getStackTraceString ( v0 );
android.util.Log .e ( v1,v2 );
/* .line 93 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
v0 = this.isRunning;
int v1 = 1; // const/4 v1, 0x1
(( java.util.concurrent.atomic.AtomicBoolean ) v0 ).set ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
/* .line 94 */
return;
} // .end method
public void setGsrConfig ( ) {
/* .locals 4 */
/* .line 66 */
final String v0 = "persist.sys.gps.selfRecovery"; // const-string v0, "persist.sys.gps.selfRecovery"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getInt ( v0,v1 );
/* .line 67 */
/* .local v0, "config":I */
/* and-int/lit8 v2, v0, 0x1 */
int v3 = 1; // const/4 v3, 0x1
if ( v2 != null) { // if-eqz v2, :cond_0
/* and-int/lit8 v2, v0, 0x2 */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 68 */
/* iput-boolean v3, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->mockStatus:Z */
/* .line 70 */
} // :cond_0
/* iput-boolean v1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->mockStatus:Z */
/* .line 72 */
} // :goto_0
/* and-int/lit8 v2, v0, 0x1 */
if ( v2 != null) { // if-eqz v2, :cond_1
/* and-int/lit8 v2, v0, 0x4 */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 73 */
/* iput-boolean v3, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->weakSignStatus:Z */
/* .line 75 */
} // :cond_1
/* iput-boolean v1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->weakSignStatus:Z */
/* .line 77 */
} // :goto_1
return;
} // .end method
public Boolean startDiagnostic ( android.os.WorkSource p0, com.android.server.location.gnss.hal.GnssNative p1 ) {
/* .locals 3 */
/* .param p1, "mWorkSource" # Landroid/os/WorkSource; */
/* .param p2, "gnssNative" # Lcom/android/server/location/gnss/hal/GnssNative; */
/* .line 125 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->isUsingNaviApp(Landroid/os/WorkSource;)Z */
if ( v0 != null) { // if-eqz v0, :cond_3
v0 = this.mHandler;
if ( v0 != null) { // if-eqz v0, :cond_3
/* iget-boolean v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->weakSignStatus:Z */
/* if-nez v0, :cond_0 */
/* .line 128 */
} // :cond_0
final String v0 = "GnssSelfRecovery"; // const-string v0, "GnssSelfRecovery"
/* const-string/jumbo v1, "start" */
android.util.Log .i ( v0,v1 );
/* .line 129 */
this.mGnssNative = p2;
/* .line 130 */
v0 = this.gnssRecovery;
/* if-nez v0, :cond_1 */
/* .line 131 */
/* new-instance v0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl; */
v1 = this.mContext;
/* invoke-direct {v0, v1, p2}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl;-><init>(Landroid/content/Context;Lcom/android/server/location/gnss/hal/GnssNative;)V */
this.gnssRecovery = v0;
/* .line 133 */
} // :cond_1
v0 = this.gnssWeakSign;
/* if-nez v0, :cond_2 */
/* .line 134 */
/* new-instance v0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssWeakSignalDiagnostic; */
v1 = this.mContext;
(( android.content.Context ) v1 ).getApplicationContext ( ); // invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;
v2 = this.mHandler;
/* invoke-direct {v0, v1, v2}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssWeakSignalDiagnostic;-><init>(Landroid/content/Context;Landroid/os/Handler;)V */
this.gnssWeakSign = v0;
/* .line 136 */
} // :cond_2
v0 = this.gnssWeakSign;
(( com.android.server.location.gnss.gnssSelfRecovery.GnssWeakSignalDiagnostic ) v0 ).startDiagnostics ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssWeakSignalDiagnostic;->startDiagnostics()V
/* .line 137 */
int v0 = 1; // const/4 v0, 0x1
/* .line 126 */
} // :cond_3
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void storeMockAppPkgName ( android.content.Context p0, java.lang.String p1 ) {
/* .locals 5 */
/* .param p1, "mContext" # Landroid/content/Context; */
/* .param p2, "pkgName" # Ljava/lang/String; */
/* .line 142 */
if ( p1 != null) { // if-eqz p1, :cond_1
/* iget-boolean v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->mockStatus:Z */
/* if-nez v0, :cond_0 */
/* .line 145 */
} // :cond_0
/* const-string/jumbo v0, "storeMockAppPkgName" */
final String v1 = "GnssSelfRecovery"; // const-string v1, "GnssSelfRecovery"
android.util.Log .d ( v1,v0 );
/* .line 146 */
v0 = this.mFile;
int v2 = 0; // const/4 v2, 0x0
(( android.content.Context ) p1 ).getSharedPreferences ( v0, v2 ); // invoke-virtual {p1, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/io/File;I)Landroid/content/SharedPreferences;
/* .line 147 */
/* .local v0, "sharedPreferences":Landroid/content/SharedPreferences; */
final String v2 = ""; // const-string v2, ""
final String v3 = "mockAppPackageName"; // const-string v3, "mockAppPackageName"
/* .line 149 */
/* .local v2, "lastPkgName":Ljava/lang/String; */
try { // :try_start_0
/* .line 150 */
/* .local v4, "editor":Landroid/content/SharedPreferences$Editor; */
/* .line 151 */
/* .line 152 */
int v3 = 1; // const/4 v3, 0x1
/* iput-boolean v3, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->isMockFlag:Z */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 155 */
} // .end local v4 # "editor":Landroid/content/SharedPreferences$Editor;
/* .line 153 */
/* :catch_0 */
/* move-exception v3 */
/* .line 154 */
/* .local v3, "e":Ljava/lang/Exception; */
android.util.Log .getStackTraceString ( v3 );
android.util.Log .e ( v1,v4 );
/* .line 156 */
} // .end local v3 # "e":Ljava/lang/Exception;
} // :goto_0
return;
/* .line 143 */
} // .end local v0 # "sharedPreferences":Landroid/content/SharedPreferences;
} // .end local v2 # "lastPkgName":Ljava/lang/String;
} // :cond_1
} // :goto_1
return;
} // .end method
public void weakSignRecover ( ) {
/* .locals 3 */
/* .line 188 */
final String v0 = "recove"; // const-string v0, "recove"
final String v1 = "GnssSelfRecovery"; // const-string v1, "GnssSelfRecovery"
android.util.Log .i ( v1,v0 );
/* .line 189 */
v0 = this.gnssWeakSign;
/* if-nez v0, :cond_0 */
/* .line 190 */
return;
/* .line 194 */
} // :cond_0
try { // :try_start_0
(( com.android.server.location.gnss.gnssSelfRecovery.GnssWeakSignalDiagnostic ) v0 ).weakSignRecovers ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssWeakSignalDiagnostic;->weakSignRecovers()V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 197 */
/* .line 195 */
/* :catch_0 */
/* move-exception v0 */
/* .line 196 */
/* .local v0, "e":Ljava/lang/Exception; */
android.util.Log .getStackTraceString ( v0 );
android.util.Log .e ( v1,v2 );
/* .line 199 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
