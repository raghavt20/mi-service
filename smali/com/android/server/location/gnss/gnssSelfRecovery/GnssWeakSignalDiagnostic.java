public class com.android.server.location.gnss.gnssSelfRecovery.GnssWeakSignalDiagnostic extends com.android.server.location.gnss.gnssSelfRecovery.GnssDiagnosticsBase {
	 /* .source "GnssWeakSignalDiagnostic.java" */
	 /* # static fields */
	 public static final java.lang.String DIAG_ITEM_NAME_WEAK_SIGNAL;
	 /* # instance fields */
	 private Boolean isRunning;
	 private android.content.Context mContext;
	 private android.os.Handler mHandler;
	 /* # direct methods */
	 public com.android.server.location.gnss.gnssSelfRecovery.GnssWeakSignalDiagnostic ( ) {
		 /* .locals 0 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .param p2, "handler" # Landroid/os/Handler; */
		 /* .line 15 */
		 /* invoke-direct {p0, p1, p2}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssDiagnosticsBase;-><init>(Landroid/content/Context;Landroid/os/Handler;)V */
		 /* .line 16 */
		 this.mContext = p1;
		 /* .line 17 */
		 this.mHandler = p2;
		 /* .line 18 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public void finishDiagnostics ( ) {
		 /* .locals 2 */
		 /* .line 50 */
		 v0 = this.mHandler;
		 /* if-nez v0, :cond_0 */
		 /* .line 51 */
		 final String v0 = "GnssDiagnosticsBase"; // const-string v0, "GnssDiagnosticsBase"
		 final String v1 = "mHandler is null"; // const-string v1, "mHandler is null"
		 android.util.Log .d ( v0,v1 );
		 /* .line 52 */
		 return;
		 /* .line 54 */
	 } // :cond_0
	 (( android.os.Handler ) v0 ).obtainMessage ( ); // invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;
	 /* .line 55 */
	 /* .local v0, "message":Landroid/os/Message; */
	 /* const/16 v1, 0x9 */
	 /* iput v1, v0, Landroid/os/Message;->what:I */
	 /* .line 56 */
	 v1 = this.mHandler;
	 (( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
	 /* .line 57 */
	 return;
} // .end method
public com.android.server.location.gnss.gnssSelfRecovery.DiagnoticResult getDiagnosticsResult ( ) {
	 /* .locals 1 */
	 /* .line 61 */
	 /* new-instance v0, Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult; */
	 /* invoke-direct {v0}, Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;-><init>()V */
} // .end method
public void run ( ) {
	 /* .locals 2 */
	 /* .line 23 */
	 v0 = this.mHandler;
	 /* if-nez v0, :cond_0 */
	 /* .line 24 */
	 final String v0 = "GnssDiagnosticsBase"; // const-string v0, "GnssDiagnosticsBase"
	 final String v1 = "mHandler is null"; // const-string v1, "mHandler is null"
	 android.util.Log .d ( v0,v1 );
	 /* .line 25 */
	 return;
	 /* .line 27 */
} // :cond_0
(( android.os.Handler ) v0 ).obtainMessage ( ); // invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;
/* .line 28 */
/* .local v0, "message":Landroid/os/Message; */
/* const/16 v1, 0x6e */
/* iput v1, v0, Landroid/os/Message;->what:I */
/* .line 29 */
v1 = this.mHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 30 */
return;
} // .end method
public void startDiagnostics ( ) {
/* .locals 1 */
/* .line 44 */
/* new-instance v0, Ljava/lang/Thread; */
/* invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V */
/* .line 45 */
/* .local v0, "mThread":Ljava/lang/Thread; */
(( java.lang.Thread ) v0 ).start ( ); // invoke-virtual {v0}, Ljava/lang/Thread;->start()V
/* .line 46 */
return;
} // .end method
public void weakSignRecovers ( ) {
/* .locals 2 */
/* .line 33 */
v0 = this.mHandler;
/* if-nez v0, :cond_0 */
/* .line 34 */
final String v0 = "GnssDiagnosticsBase"; // const-string v0, "GnssDiagnosticsBase"
final String v1 = "mHandler is null"; // const-string v1, "mHandler is null"
android.util.Log .d ( v0,v1 );
/* .line 35 */
return;
/* .line 37 */
} // :cond_0
(( android.os.Handler ) v0 ).obtainMessage ( ); // invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;
/* .line 38 */
/* .local v0, "message":Landroid/os/Message; */
int v1 = 1; // const/4 v1, 0x1
/* iput v1, v0, Landroid/os/Message;->what:I */
/* .line 39 */
v1 = this.mHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 40 */
return;
} // .end method
