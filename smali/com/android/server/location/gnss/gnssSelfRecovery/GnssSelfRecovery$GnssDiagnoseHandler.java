class com.android.server.location.gnss.gnssSelfRecovery.GnssSelfRecovery$GnssDiagnoseHandler extends android.os.Handler {
	 /* .source "GnssSelfRecovery.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "GnssDiagnoseHandler" */
} // .end annotation
/* # instance fields */
final com.android.server.location.gnss.gnssSelfRecovery.GnssSelfRecovery this$0; //synthetic
/* # direct methods */
public static void $r8$lambda$IYS4PdXsP-12oMkdH77RVXpGUDU ( com.android.server.location.gnss.gnssSelfRecovery.GnssSelfRecovery$GnssDiagnoseHandler p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery$GnssDiagnoseHandler;->lambda$handleMessage$0()V */
return;
} // .end method
public com.android.server.location.gnss.gnssSelfRecovery.GnssSelfRecovery$GnssDiagnoseHandler ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 213 */
this.this$0 = p1;
/* .line 214 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 215 */
return;
} // .end method
public com.android.server.location.gnss.gnssSelfRecovery.GnssSelfRecovery$GnssDiagnoseHandler ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .param p3, "callback" # Landroid/os/Handler$Callback; */
/* .line 216 */
this.this$0 = p1;
/* .line 217 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 218 */
return;
} // .end method
private void lambda$handleMessage$0 ( ) { //synthethic
/* .locals 1 */
/* .line 239 */
v0 = this.this$0;
(( com.android.server.location.gnss.gnssSelfRecovery.GnssSelfRecovery ) v0 ).finishDiagnostic ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->finishDiagnostic()V
/* .line 240 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 5 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 221 */
/* if-nez p1, :cond_0 */
return;
/* .line 222 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 223 */
/* .local v0, "diagResult":Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult; */
final String v1 = "handle receive result"; // const-string v1, "handle receive result"
final String v2 = "GnssSelfRecovery"; // const-string v2, "GnssSelfRecovery"
android.util.Log .i ( v2,v1 );
/* .line 224 */
/* iget v1, p1, Landroid/os/Message;->what:I */
int v3 = 0; // const/4 v3, 0x0
/* sparse-switch v1, :sswitch_data_0 */
/* .line 235 */
/* :sswitch_0 */
final String v1 = "receive diagnostic msg, type: WEAK_SIGNAL"; // const-string v1, "receive diagnostic msg, type: WEAK_SIGNAL"
android.util.Log .v ( v2,v1 );
/* .line 236 */
v1 = this.this$0;
com.android.server.location.gnss.gnssSelfRecovery.GnssSelfRecovery .-$$Nest$fgetgnssRecovery ( v1 );
int v2 = 1; // const/4 v2, 0x1
/* .line 238 */
v1 = this.this$0;
com.android.server.location.gnss.gnssSelfRecovery.GnssSelfRecovery .-$$Nest$fgetmHandler ( v1 );
/* new-instance v2, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery$GnssDiagnoseHandler$$ExternalSyntheticLambda0; */
/* invoke-direct {v2, p0}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery$GnssDiagnoseHandler$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery$GnssDiagnoseHandler;)V */
/* const-wide/32 v3, 0xea60 */
(( android.os.Handler ) v1 ).postDelayed ( v2, v3, v4 ); // invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 241 */
/* .line 226 */
/* :sswitch_1 */
final String v1 = "receive diagnostic msg, type: MOCK_LOCATION"; // const-string v1, "receive diagnostic msg, type: MOCK_LOCATION"
android.util.Log .v ( v2,v1 );
/* .line 227 */
/* new-instance v1, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl; */
v2 = this.this$0;
com.android.server.location.gnss.gnssSelfRecovery.GnssSelfRecovery .-$$Nest$fgetmContext ( v2 );
/* invoke-direct {v1, v2}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl;-><init>(Landroid/content/Context;)V */
/* .line 228 */
/* .local v1, "gnssMockRecovery":Lcom/android/server/location/gnss/gnssSelfRecovery/IGnssRecovery; */
v2 = this.obj;
/* move-object v0, v2 */
/* check-cast v0, Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult; */
/* .line 229 */
v2 = (( com.android.server.location.gnss.gnssSelfRecovery.DiagnoticResult ) v0 ).getResult ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;->getResult()Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 230 */
/* .line 231 */
v2 = this.this$0;
com.android.server.location.gnss.gnssSelfRecovery.GnssSelfRecovery .-$$Nest$fputisMockFlag ( v2,v3 );
/* .line 243 */
} // .end local v1 # "gnssMockRecovery":Lcom/android/server/location/gnss/gnssSelfRecovery/IGnssRecovery;
/* :sswitch_2 */
final String v1 = "receive diagnostic msg, type:FINISHI_DIAGNOTIC"; // const-string v1, "receive diagnostic msg, type:FINISHI_DIAGNOTIC"
android.util.Log .v ( v2,v1 );
/* .line 244 */
v1 = this.this$0;
com.android.server.location.gnss.gnssSelfRecovery.GnssSelfRecovery .-$$Nest$fgetgnssRecovery ( v1 );
/* .line 245 */
/* nop */
/* .line 248 */
} // :cond_1
} // :goto_0
return;
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x9 -> :sswitch_2 */
/* 0x65 -> :sswitch_1 */
/* 0x6e -> :sswitch_0 */
} // .end sparse-switch
} // .end method
