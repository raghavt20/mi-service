.class Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery$GnssDiagnoseHandler;
.super Landroid/os/Handler;
.source "GnssSelfRecovery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "GnssDiagnoseHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;


# direct methods
.method public static synthetic $r8$lambda$IYS4PdXsP-12oMkdH77RVXpGUDU(Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery$GnssDiagnoseHandler;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery$GnssDiagnoseHandler;->lambda$handleMessage$0()V

    return-void
.end method

.method public constructor <init>(Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;Landroid/os/Looper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 213
    iput-object p1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery$GnssDiagnoseHandler;->this$0:Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;

    .line 214
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 215
    return-void
.end method

.method public constructor <init>(Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;Landroid/os/Looper;Landroid/os/Handler$Callback;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;
    .param p2, "looper"    # Landroid/os/Looper;
    .param p3, "callback"    # Landroid/os/Handler$Callback;

    .line 216
    iput-object p1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery$GnssDiagnoseHandler;->this$0:Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;

    .line 217
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 218
    return-void
.end method

.method private synthetic lambda$handleMessage$0()V
    .locals 1

    .line 239
    iget-object v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery$GnssDiagnoseHandler;->this$0:Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;

    invoke-virtual {v0}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->finishDiagnostic()V

    .line 240
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .line 221
    if-nez p1, :cond_0

    return-void

    .line 222
    :cond_0
    const/4 v0, 0x0

    .line 223
    .local v0, "diagResult":Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;
    const-string v1, "handle receive result"

    const-string v2, "GnssSelfRecovery"

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    iget v1, p1, Landroid/os/Message;->what:I

    const/4 v3, 0x0

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    .line 235
    :sswitch_0
    const-string v1, "receive diagnostic msg, type: WEAK_SIGNAL"

    invoke-static {v2, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    iget-object v1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery$GnssDiagnoseHandler;->this$0:Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;

    invoke-static {v1}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->-$$Nest$fgetgnssRecovery(Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;)Lcom/android/server/location/gnss/gnssSelfRecovery/IGnssRecovery;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/android/server/location/gnss/gnssSelfRecovery/IGnssRecovery;->setFullTracking(Z)Z

    .line 238
    iget-object v1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery$GnssDiagnoseHandler;->this$0:Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;

    invoke-static {v1}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->-$$Nest$fgetmHandler(Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery$GnssDiagnoseHandler$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery$GnssDiagnoseHandler$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery$GnssDiagnoseHandler;)V

    const-wide/32 v3, 0xea60

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 241
    goto :goto_0

    .line 226
    :sswitch_1
    const-string v1, "receive diagnostic msg, type: MOCK_LOCATION"

    invoke-static {v2, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    new-instance v1, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl;

    iget-object v2, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery$GnssDiagnoseHandler;->this$0:Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;

    invoke-static {v2}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->-$$Nest$fgetmContext(Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl;-><init>(Landroid/content/Context;)V

    .line 228
    .local v1, "gnssMockRecovery":Lcom/android/server/location/gnss/gnssSelfRecovery/IGnssRecovery;
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object v0, v2

    check-cast v0, Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;

    .line 229
    invoke-virtual {v0}, Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;->getResult()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 230
    invoke-interface {v1}, Lcom/android/server/location/gnss/gnssSelfRecovery/IGnssRecovery;->removeMockLocation()Z

    .line 231
    iget-object v2, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery$GnssDiagnoseHandler;->this$0:Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;

    invoke-static {v2, v3}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->-$$Nest$fputisMockFlag(Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;Z)V

    goto :goto_0

    .line 243
    .end local v1    # "gnssMockRecovery":Lcom/android/server/location/gnss/gnssSelfRecovery/IGnssRecovery;
    :sswitch_2
    const-string v1, "receive diagnostic msg, type:FINISHI_DIAGNOTIC"

    invoke-static {v2, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    iget-object v1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery$GnssDiagnoseHandler;->this$0:Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;

    invoke-static {v1}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->-$$Nest$fgetgnssRecovery(Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;)Lcom/android/server/location/gnss/gnssSelfRecovery/IGnssRecovery;

    move-result-object v1

    invoke-interface {v1, v3}, Lcom/android/server/location/gnss/gnssSelfRecovery/IGnssRecovery;->setFullTracking(Z)Z

    .line 245
    nop

    .line 248
    :cond_1
    :goto_0
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_2
        0x65 -> :sswitch_1
        0x6e -> :sswitch_0
    .end sparse-switch
.end method
