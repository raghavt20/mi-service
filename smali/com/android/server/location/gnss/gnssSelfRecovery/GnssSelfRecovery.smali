.class public Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;
.super Ljava/lang/Object;
.source "GnssSelfRecovery.java"

# interfaces
.implements Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecoveryStub;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery$GnssDiagnoseHandler;,
        Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery$GnssRecoveryReason;
    }
.end annotation


# static fields
.field private static final GNSS_SELF_RECOVERY_PROP:Ljava/lang/String; = "persist.sys.gps.selfRecovery"

.field private static final GPS_GSR_MAIN_BIT:I = 0x1

.field private static final GPS_GSR_MOCK_BIT:I = 0x2

.field private static final GPS_GSR_WEAK_SIGN_BIT:I = 0x4

.field public static final REASON_MOCK_LOCATION:I = 0x0

.field public static final REASON_WEAK_SIGNAL:I = 0x1

.field private static final TAG:Ljava/lang/String; = "GnssSelfRecovery"

.field private static appList:[Ljava/lang/String;


# instance fields
.field private final diagnosticsItemArrayList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/android/server/location/gnss/gnssSelfRecovery/GnssDiagnosticsBase;",
            ">;"
        }
    .end annotation
.end field

.field private gnssRecovery:Lcom/android/server/location/gnss/gnssSelfRecovery/IGnssRecovery;

.field private gnssWeakSign:Lcom/android/server/location/gnss/gnssSelfRecovery/GnssWeakSignalDiagnostic;

.field private isEnableFullTracking:Z

.field private isMockFlag:Z

.field private final isRunning:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mContext:Landroid/content/Context;

.field private final mFile:Ljava/io/File;

.field private mGnssNative:Lcom/android/server/location/gnss/hal/GnssNative;

.field private mHandler:Landroid/os/Handler;

.field private mHandlerThread:Landroid/os/HandlerThread;

.field private mockStatus:Z

.field private weakSignStatus:Z


# direct methods
.method static bridge synthetic -$$Nest$fgetgnssRecovery(Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;)Lcom/android/server/location/gnss/gnssSelfRecovery/IGnssRecovery;
    .locals 0

    iget-object p0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->gnssRecovery:Lcom/android/server/location/gnss/gnssSelfRecovery/IGnssRecovery;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputisMockFlag(Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->isMockFlag:Z

    return-void
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 42
    const-string v0, "com.autonavi.minimap"

    const-string v1, "com.tencent.map"

    const-string v2, "com.baidu.BaiduMap"

    filled-new-array {v2, v0, v1}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->appList:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "gnssSelfRecoveryThread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->mHandlerThread:Landroid/os/HandlerThread;

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->diagnosticsItemArrayList:Ljava/util/ArrayList;

    .line 48
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v2

    const-string/jumbo v3, "system"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const-string v2, "gnss_properties.xml"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->mFile:Ljava/io/File;

    .line 49
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->isRunning:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 51
    iput-boolean v1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->isMockFlag:Z

    .line 52
    iput-boolean v1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->isEnableFullTracking:Z

    .line 53
    iput-boolean v1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->mockStatus:Z

    .line 54
    iput-boolean v1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->weakSignStatus:Z

    return-void
.end method

.method public static getRecoveryReasonAsString(I)Ljava/lang/String;
    .locals 1
    .param p0, "reason"    # I

    .line 202
    packed-switch p0, :pswitch_data_0

    .line 208
    const-string v0, "UNKNOWN"

    return-object v0

    .line 206
    :pswitch_0
    const-string/jumbo v0, "weak GNSS signal"

    return-object v0

    .line 204
    :pswitch_1
    const-string v0, "mock location"

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private isUsingNaviApp(Landroid/os/WorkSource;)Z
    .locals 6
    .param p1, "mWorkSource"    # Landroid/os/WorkSource;

    .line 159
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "workSource:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GnssSelfRecovery"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 161
    :cond_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    sget-object v3, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->appList:[Ljava/lang/String;

    array-length v3, v3

    if-ge v2, v3, :cond_3

    .line 162
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    invoke-virtual {p1}, Landroid/os/WorkSource;->size()I

    move-result v4

    if-ge v3, v4, :cond_2

    .line 163
    sget-object v4, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->appList:[Ljava/lang/String;

    aget-object v4, v4, v2

    invoke-virtual {p1, v3}, Landroid/os/WorkSource;->getPackageName(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 164
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "is navic app :"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1, v3}, Landroid/os/WorkSource;->getPackageName(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    const/4 v0, 0x1

    return v0

    .line 162
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 161
    .end local v3    # "j":I
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 169
    .end local v2    # "i":I
    :cond_3
    return v0
.end method


# virtual methods
.method public declared-synchronized deInit()V
    .locals 3

    monitor-enter p0

    .line 98
    :try_start_0
    const-string v0, "GnssSelfRecovery"

    const-string v1, "deInit"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    iget-object v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->mHandler:Landroid/os/Handler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 101
    const/4 v1, 0x0

    :try_start_1
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 104
    goto :goto_0

    .line 102
    .end local p0    # "this":Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;
    :catch_0
    move-exception v0

    .line 103
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v1, "GnssSelfRecovery"

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->diagnosticsItemArrayList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 107
    iget-object v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->isRunning:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 108
    monitor-exit p0

    return-void

    .line 97
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public diagnosticMockLocation()V
    .locals 3

    .line 112
    iget-object v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->mockStatus:Z

    if-nez v0, :cond_0

    goto :goto_0

    .line 115
    :cond_0
    const-string v0, "GnssSelfRecovery"

    const-string/jumbo v1, "start diagnostic mock location"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    iget-boolean v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->isMockFlag:Z

    if-eqz v0, :cond_1

    .line 117
    new-instance v0, Lcom/android/server/location/gnss/gnssSelfRecovery/MockLocationDiagnostic;

    iget-object v1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->mContext:Landroid/content/Context;

    .line 118
    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, v1, v2}, Lcom/android/server/location/gnss/gnssSelfRecovery/MockLocationDiagnostic;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    .line 119
    .local v0, "mMockLocationDiagnostic":Lcom/android/server/location/gnss/gnssSelfRecovery/GnssDiagnosticsBase;
    invoke-virtual {v0}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssDiagnosticsBase;->start()V

    .line 121
    .end local v0    # "mMockLocationDiagnostic":Lcom/android/server/location/gnss/gnssSelfRecovery/GnssDiagnosticsBase;
    :cond_1
    return-void

    .line 113
    :cond_2
    :goto_0
    return-void
.end method

.method public finishDiagnostic()V
    .locals 3

    .line 174
    const-string v0, "finishDiagnostic"

    const-string v1, "GnssSelfRecovery"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    iget-object v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->gnssWeakSign:Lcom/android/server/location/gnss/gnssSelfRecovery/GnssWeakSignalDiagnostic;

    if-nez v0, :cond_0

    .line 176
    return-void

    .line 180
    :cond_0
    :try_start_0
    invoke-virtual {v0}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssWeakSignalDiagnostic;->finishDiagnostics()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 183
    goto :goto_0

    .line 181
    :catch_0
    move-exception v0

    .line 182
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public init(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 81
    iput-object p1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->mContext:Landroid/content/Context;

    .line 82
    iget-object v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->isRunning:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    return-void

    .line 85
    :cond_0
    const-string v0, "init"

    const-string v1, "GnssSelfRecovery"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    :try_start_0
    iget-object v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 88
    new-instance v0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery$GnssDiagnoseHandler;

    iget-object v2, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery$GnssDiagnoseHandler;-><init>(Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->mHandler:Landroid/os/Handler;

    .line 89
    invoke-virtual {p0}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->setGsrConfig()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 92
    goto :goto_0

    .line 90
    :catch_0
    move-exception v0

    .line 91
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    iget-object v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->isRunning:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 94
    return-void
.end method

.method public setGsrConfig()V
    .locals 4

    .line 66
    const-string v0, "persist.sys.gps.selfRecovery"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 67
    .local v0, "config":I
    and-int/lit8 v2, v0, 0x1

    const/4 v3, 0x1

    if-eqz v2, :cond_0

    and-int/lit8 v2, v0, 0x2

    if-eqz v2, :cond_0

    .line 68
    iput-boolean v3, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->mockStatus:Z

    goto :goto_0

    .line 70
    :cond_0
    iput-boolean v1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->mockStatus:Z

    .line 72
    :goto_0
    and-int/lit8 v2, v0, 0x1

    if-eqz v2, :cond_1

    and-int/lit8 v2, v0, 0x4

    if-eqz v2, :cond_1

    .line 73
    iput-boolean v3, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->weakSignStatus:Z

    goto :goto_1

    .line 75
    :cond_1
    iput-boolean v1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->weakSignStatus:Z

    .line 77
    :goto_1
    return-void
.end method

.method public startDiagnostic(Landroid/os/WorkSource;Lcom/android/server/location/gnss/hal/GnssNative;)Z
    .locals 3
    .param p1, "mWorkSource"    # Landroid/os/WorkSource;
    .param p2, "gnssNative"    # Lcom/android/server/location/gnss/hal/GnssNative;

    .line 125
    invoke-direct {p0, p1}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->isUsingNaviApp(Landroid/os/WorkSource;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->weakSignStatus:Z

    if-nez v0, :cond_0

    goto :goto_0

    .line 128
    :cond_0
    const-string v0, "GnssSelfRecovery"

    const-string/jumbo v1, "start"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    iput-object p2, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->mGnssNative:Lcom/android/server/location/gnss/hal/GnssNative;

    .line 130
    iget-object v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->gnssRecovery:Lcom/android/server/location/gnss/gnssSelfRecovery/IGnssRecovery;

    if-nez v0, :cond_1

    .line 131
    new-instance v0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl;

    iget-object v1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p2}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssRecoveryImpl;-><init>(Landroid/content/Context;Lcom/android/server/location/gnss/hal/GnssNative;)V

    iput-object v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->gnssRecovery:Lcom/android/server/location/gnss/gnssSelfRecovery/IGnssRecovery;

    .line 133
    :cond_1
    iget-object v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->gnssWeakSign:Lcom/android/server/location/gnss/gnssSelfRecovery/GnssWeakSignalDiagnostic;

    if-nez v0, :cond_2

    .line 134
    new-instance v0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssWeakSignalDiagnostic;

    iget-object v1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, v1, v2}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssWeakSignalDiagnostic;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->gnssWeakSign:Lcom/android/server/location/gnss/gnssSelfRecovery/GnssWeakSignalDiagnostic;

    .line 136
    :cond_2
    iget-object v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->gnssWeakSign:Lcom/android/server/location/gnss/gnssSelfRecovery/GnssWeakSignalDiagnostic;

    invoke-virtual {v0}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssWeakSignalDiagnostic;->startDiagnostics()V

    .line 137
    const/4 v0, 0x1

    return v0

    .line 126
    :cond_3
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method public storeMockAppPkgName(Landroid/content/Context;Ljava/lang/String;)V
    .locals 5
    .param p1, "mContext"    # Landroid/content/Context;
    .param p2, "pkgName"    # Ljava/lang/String;

    .line 142
    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->mockStatus:Z

    if-nez v0, :cond_0

    goto :goto_1

    .line 145
    :cond_0
    const-string/jumbo v0, "storeMockAppPkgName"

    const-string v1, "GnssSelfRecovery"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    iget-object v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->mFile:Ljava/io/File;

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/io/File;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 147
    .local v0, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string v2, ""

    const-string v3, "mockAppPackageName"

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 149
    .local v2, "lastPkgName":Ljava/lang/String;
    :try_start_0
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 150
    .local v4, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v4, v3, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 151
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 152
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->isMockFlag:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 155
    .end local v4    # "editor":Landroid/content/SharedPreferences$Editor;
    goto :goto_0

    .line 153
    :catch_0
    move-exception v3

    .line 154
    .local v3, "e":Ljava/lang/Exception;
    invoke-static {v3}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    .end local v3    # "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 143
    .end local v0    # "sharedPreferences":Landroid/content/SharedPreferences;
    .end local v2    # "lastPkgName":Ljava/lang/String;
    :cond_1
    :goto_1
    return-void
.end method

.method public weakSignRecover()V
    .locals 3

    .line 188
    const-string v0, "recove"

    const-string v1, "GnssSelfRecovery"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    iget-object v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery;->gnssWeakSign:Lcom/android/server/location/gnss/gnssSelfRecovery/GnssWeakSignalDiagnostic;

    if-nez v0, :cond_0

    .line 190
    return-void

    .line 194
    :cond_0
    :try_start_0
    invoke-virtual {v0}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssWeakSignalDiagnostic;->weakSignRecovers()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 197
    goto :goto_0

    .line 195
    :catch_0
    move-exception v0

    .line 196
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method
