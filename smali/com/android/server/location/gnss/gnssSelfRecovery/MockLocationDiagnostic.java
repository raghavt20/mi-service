public class com.android.server.location.gnss.gnssSelfRecovery.MockLocationDiagnostic extends com.android.server.location.gnss.gnssSelfRecovery.GnssDiagnosticsBase {
	 /* .source "MockLocationDiagnostic.java" */
	 /* # static fields */
	 public static final java.lang.String DIAG_ITEM_NAME_MOCK;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private Boolean isMockLocation;
	 private Boolean isRuning;
	 private android.content.Context mContext;
	 private android.os.Handler mHandler;
	 private com.android.server.location.gnss.gnssSelfRecovery.DiagnoticResult mockDiagResult;
	 private android.content.SharedPreferences sharedPreferences;
	 /* # direct methods */
	 public com.android.server.location.gnss.gnssSelfRecovery.MockLocationDiagnostic ( ) {
		 /* .locals 0 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .param p2, "handler" # Landroid/os/Handler; */
		 /* .line 26 */
		 /* invoke-direct {p0, p1, p2}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssDiagnosticsBase;-><init>(Landroid/content/Context;Landroid/os/Handler;)V */
		 /* .line 27 */
		 this.mContext = p1;
		 /* .line 28 */
		 this.mHandler = p2;
		 /* .line 29 */
		 return;
	 } // .end method
	 private Boolean checkIfMockAppAlive ( java.lang.String p0 ) {
		 /* .locals 9 */
		 /* .param p1, "pkgName" # Ljava/lang/String; */
		 /* .line 78 */
		 v0 = this.mContext;
		 /* .line 79 */
		 final String v1 = "activity"; // const-string v1, "activity"
		 (( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
		 /* check-cast v0, Landroid/app/ActivityManager; */
		 /* .line 80 */
		 /* .local v0, "mActivityManager":Landroid/app/ActivityManager; */
		 /* nop */
		 /* .line 81 */
		 (( android.app.ActivityManager ) v0 ).getRunningAppProcesses ( ); // invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;
		 /* .line 82 */
		 /* .local v1, "processInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;" */
		 int v2 = 0; // const/4 v2, 0x0
		 /* .local v2, "i":I */
	 v3 = 	 } // :goto_0
	 int v4 = 0; // const/4 v4, 0x0
	 /* if-ge v2, v3, :cond_2 */
	 /* .line 83 */
	 /* check-cast v3, Landroid/app/ActivityManager$RunningAppProcessInfo; */
	 /* .line 84 */
	 /* .local v3, "processInfo":Landroid/app/ActivityManager$RunningAppProcessInfo; */
	 v5 = this.pkgList;
	 /* .line 85 */
	 /* .local v5, "pkgList":[Ljava/lang/String; */
	 /* array-length v6, v5 */
} // :goto_1
/* if-ge v4, v6, :cond_1 */
/* aget-object v7, v5, v4 */
/* .line 86 */
/* .local v7, "packageName":Ljava/lang/String; */
v8 = (( java.lang.String ) p1 ).equals ( v7 ); // invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v8 != null) { // if-eqz v8, :cond_0
	 /* .line 87 */
	 final String v4 = "MockLocationDiagnostic"; // const-string v4, "MockLocationDiagnostic"
	 final String v6 = "Service name"; // const-string v6, "Service name"
	 android.util.Log .d ( v4,v6 );
	 /* .line 88 */
	 int v4 = 1; // const/4 v4, 0x1
	 /* .line 85 */
} // .end local v7 # "packageName":Ljava/lang/String;
} // :cond_0
/* add-int/lit8 v4, v4, 0x1 */
/* .line 82 */
} // .end local v3 # "processInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
} // .end local v5 # "pkgList":[Ljava/lang/String;
} // :cond_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 92 */
} // .end local v2 # "i":I
} // :cond_2
} // .end method
private java.lang.String getMockLocationPkgName ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p1, "mContext" # Landroid/content/Context; */
/* .line 64 */
final String v0 = ""; // const-string v0, ""
/* .line 65 */
/* .local v0, "pkgName":Ljava/lang/String; */
/* if-nez p1, :cond_0 */
/* .line 67 */
} // :cond_0
try { // :try_start_0
/* new-instance v1, Ljava/io/File; */
/* new-instance v2, Ljava/io/File; */
android.os.Environment .getDataDirectory ( );
/* const-string/jumbo v4, "system" */
/* invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
final String v3 = "gnss_properties.xml"; // const-string v3, "gnss_properties.xml"
/* invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* .line 68 */
/* .local v1, "mFile":Ljava/io/File; */
int v2 = 0; // const/4 v2, 0x0
(( android.content.Context ) p1 ).getSharedPreferences ( v1, v2 ); // invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/io/File;I)Landroid/content/SharedPreferences;
this.sharedPreferences = v2;
/* .line 69 */
final String v3 = "mockAppPackageName"; // const-string v3, "mockAppPackageName"
final String v4 = ""; // const-string v4, ""
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object v0, v2 */
/* .line 72 */
} // .end local v1 # "mFile":Ljava/io/File;
/* .line 70 */
/* :catch_0 */
/* move-exception v1 */
/* .line 71 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v2 = "MockLocationDiagnostic"; // const-string v2, "MockLocationDiagnostic"
android.util.Log .getStackTraceString ( v1 );
android.util.Log .d ( v2,v3 );
/* .line 73 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
} // .end method
/* # virtual methods */
public void diagnoseMockLocationInternel ( ) {
/* .locals 6 */
/* .line 52 */
/* const-string/jumbo v0, "start Diagnose mock location" */
final String v1 = "MockLocationDiagnostic"; // const-string v1, "MockLocationDiagnostic"
android.util.Log .i ( v1,v0 );
/* .line 53 */
/* new-instance v0, Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult; */
/* const-wide/16 v2, 0x0 */
final String v4 = "mockDiagsResult"; // const-string v4, "mockDiagsResult"
int v5 = 0; // const/4 v5, 0x0
/* invoke-direct {v0, v4, v5, v2, v3}, Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;-><init>(Ljava/lang/String;ZD)V */
this.mockDiagResult = v0;
/* .line 54 */
v0 = this.mContext;
/* invoke-direct {p0, v0}, Lcom/android/server/location/gnss/gnssSelfRecovery/MockLocationDiagnostic;->getMockLocationPkgName(Landroid/content/Context;)Ljava/lang/String; */
/* .line 55 */
/* .local v0, "lastMockPkg":Ljava/lang/String; */
v2 = /* invoke-direct {p0, v0}, Lcom/android/server/location/gnss/gnssSelfRecovery/MockLocationDiagnostic;->checkIfMockAppAlive(Ljava/lang/String;)Z */
/* if-nez v2, :cond_0 */
/* .line 56 */
v2 = this.mockDiagResult;
int v3 = 1; // const/4 v3, 0x1
(( com.android.server.location.gnss.gnssSelfRecovery.DiagnoticResult ) v2 ).setResult ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;->setResult(Z)V
/* .line 58 */
} // :cond_0
v2 = this.mockDiagResult;
(( com.android.server.location.gnss.gnssSelfRecovery.DiagnoticResult ) v2 ).setResult ( v5 ); // invoke-virtual {v2, v5}, Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;->setResult(Z)V
/* .line 60 */
} // :goto_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "start Diagnose mock location result: " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mockDiagResult;
(( com.android.server.location.gnss.gnssSelfRecovery.DiagnoticResult ) v3 ).toString ( ); // invoke-virtual {v3}, Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v1,v2 );
/* .line 61 */
return;
} // .end method
public void finishDiagnostics ( ) {
/* .locals 0 */
/* .line 97 */
return;
} // .end method
public com.android.server.location.gnss.gnssSelfRecovery.DiagnoticResult getDiagnosticsResult ( ) {
/* .locals 1 */
/* .line 102 */
v0 = this.mockDiagResult;
} // .end method
public void run ( ) {
/* .locals 2 */
/* .line 33 */
(( com.android.server.location.gnss.gnssSelfRecovery.MockLocationDiagnostic ) p0 ).diagnoseMockLocationInternel ( ); // invoke-virtual {p0}, Lcom/android/server/location/gnss/gnssSelfRecovery/MockLocationDiagnostic;->diagnoseMockLocationInternel()V
/* .line 34 */
v0 = this.mHandler;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 35 */
(( android.os.Handler ) v0 ).obtainMessage ( ); // invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;
/* .line 36 */
/* .local v0, "message":Landroid/os/Message; */
/* const/16 v1, 0x65 */
/* iput v1, v0, Landroid/os/Message;->what:I */
/* .line 37 */
(( com.android.server.location.gnss.gnssSelfRecovery.MockLocationDiagnostic ) p0 ).getDiagnosticsResult ( ); // invoke-virtual {p0}, Lcom/android/server/location/gnss/gnssSelfRecovery/MockLocationDiagnostic;->getDiagnosticsResult()Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;
this.obj = v1;
/* .line 38 */
v1 = this.mHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 40 */
} // .end local v0 # "message":Landroid/os/Message;
} // :cond_0
return;
} // .end method
public void startDiagnostics ( ) {
/* .locals 2 */
/* .line 44 */
/* iget-boolean v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/MockLocationDiagnostic;->isRuning:Z */
/* if-nez v0, :cond_0 */
/* .line 45 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/MockLocationDiagnostic;->isRuning:Z */
/* .line 46 */
/* new-instance v0, Ljava/lang/Thread; */
final String v1 = "mockDiagsThread"; // const-string v1, "mockDiagsThread"
/* invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V */
/* .line 47 */
/* .local v0, "thread":Ljava/lang/Thread; */
(( java.lang.Thread ) v0 ).start ( ); // invoke-virtual {v0}, Ljava/lang/Thread;->start()V
/* .line 49 */
} // .end local v0 # "thread":Ljava/lang/Thread;
} // :cond_0
return;
} // .end method
