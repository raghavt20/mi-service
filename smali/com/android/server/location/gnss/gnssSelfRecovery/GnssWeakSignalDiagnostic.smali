.class public Lcom/android/server/location/gnss/gnssSelfRecovery/GnssWeakSignalDiagnostic;
.super Lcom/android/server/location/gnss/gnssSelfRecovery/GnssDiagnosticsBase;
.source "GnssWeakSignalDiagnostic.java"


# static fields
.field public static final DIAG_ITEM_NAME_WEAK_SIGNAL:Ljava/lang/String; = "weakSignalDiagsResult"


# instance fields
.field private isRunning:Z

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 15
    invoke-direct {p0, p1, p2}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssDiagnosticsBase;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    .line 16
    iput-object p1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssWeakSignalDiagnostic;->mContext:Landroid/content/Context;

    .line 17
    iput-object p2, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssWeakSignalDiagnostic;->mHandler:Landroid/os/Handler;

    .line 18
    return-void
.end method


# virtual methods
.method public finishDiagnostics()V
    .locals 2

    .line 50
    iget-object v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssWeakSignalDiagnostic;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 51
    const-string v0, "GnssDiagnosticsBase"

    const-string v1, "mHandler is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    return-void

    .line 54
    :cond_0
    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 55
    .local v0, "message":Landroid/os/Message;
    const/16 v1, 0x9

    iput v1, v0, Landroid/os/Message;->what:I

    .line 56
    iget-object v1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssWeakSignalDiagnostic;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 57
    return-void
.end method

.method public getDiagnosticsResult()Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;
    .locals 1

    .line 61
    new-instance v0, Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;

    invoke-direct {v0}, Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;-><init>()V

    return-object v0
.end method

.method public run()V
    .locals 2

    .line 23
    iget-object v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssWeakSignalDiagnostic;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 24
    const-string v0, "GnssDiagnosticsBase"

    const-string v1, "mHandler is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 25
    return-void

    .line 27
    :cond_0
    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 28
    .local v0, "message":Landroid/os/Message;
    const/16 v1, 0x6e

    iput v1, v0, Landroid/os/Message;->what:I

    .line 29
    iget-object v1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssWeakSignalDiagnostic;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 30
    return-void
.end method

.method public startDiagnostics()V
    .locals 1

    .line 44
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 45
    .local v0, "mThread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 46
    return-void
.end method

.method public weakSignRecovers()V
    .locals 2

    .line 33
    iget-object v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssWeakSignalDiagnostic;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 34
    const-string v0, "GnssDiagnosticsBase"

    const-string v1, "mHandler is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 35
    return-void

    .line 37
    :cond_0
    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 38
    .local v0, "message":Landroid/os/Message;
    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->what:I

    .line 39
    iget-object v1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssWeakSignalDiagnostic;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 40
    return-void
.end method
