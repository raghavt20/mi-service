.class public abstract Lcom/android/server/location/gnss/gnssSelfRecovery/GnssDiagnosticsBase;
.super Ljava/lang/Object;
.source "GnssDiagnosticsBase.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static final DEBUG:Z = false

.field private static final DIAGNOSTICS_TIME_OUT:I = 0xf4240

.field public static final LAST_MOCK_APP_PKG_NAME:Ljava/lang/String; = "mockAppPackageName"

.field public static final MSG_FINISHI_DIAGNOTIC:I = 0x9

.field public static final MSG_MOCK_LOCATION:I = 0x65

.field public static final MSG_WEAK_SIGNAL:I = 0x6e

.field public static final MSG_WEAK_SIGNAL_RECOVER:I = 0x1

.field public static final TAG:Ljava/lang/String; = "GnssDiagnosticsBase"


# instance fields
.field private isRunning:Z

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p2, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssDiagnosticsBase;->mHandler:Landroid/os/Handler;

    .line 26
    iput-object p1, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssDiagnosticsBase;->mContext:Landroid/content/Context;

    .line 27
    return-void
.end method


# virtual methods
.method public declared-synchronized finish()V
    .locals 1

    monitor-enter p0

    .line 41
    :try_start_0
    iget-boolean v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssDiagnosticsBase;->isRunning:Z

    if-eqz v0, :cond_0

    .line 42
    invoke-virtual {p0}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssDiagnosticsBase;->finishDiagnostics()V

    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssDiagnosticsBase;->isRunning:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 45
    .end local p0    # "this":Lcom/android/server/location/gnss/gnssSelfRecovery/GnssDiagnosticsBase;
    :cond_0
    monitor-exit p0

    return-void

    .line 40
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public abstract finishDiagnostics()V
.end method

.method public abstract getDiagnosticsResult()Lcom/android/server/location/gnss/gnssSelfRecovery/DiagnoticResult;
.end method

.method public run()V
    .locals 1

    .line 36
    invoke-virtual {p0}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssDiagnosticsBase;->startDiagnostics()V

    .line 37
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssDiagnosticsBase;->isRunning:Z

    .line 38
    return-void
.end method

.method public declared-synchronized start()V
    .locals 1

    monitor-enter p0

    .line 30
    :try_start_0
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 31
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 32
    monitor-exit p0

    return-void

    .line 29
    .end local v0    # "thread":Ljava/lang/Thread;
    .end local p0    # "this":Lcom/android/server/location/gnss/gnssSelfRecovery/GnssDiagnosticsBase;
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public abstract startDiagnostics()V
.end method
