public class com.android.server.location.gnss.monitor.LocationSettingsEventMonitorImpl implements com.android.server.location.gnss.monitor.LocationSettingsEventMonitorStub {
	 /* .source "LocationSettingsEventMonitorImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final java.lang.String DISABLED_BY_ANDROID;
	 private static final java.lang.String DISABLED_BY_SYSTEM;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private final Boolean D;
	 private android.content.Context mContext;
	 /* # direct methods */
	 com.android.server.location.gnss.monitor.LocationSettingsEventMonitorImpl ( ) {
		 /* .locals 2 */
		 /* .line 37 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 30 */
		 final String v0 = "persist.sys.gnss_dc.test"; // const-string v0, "persist.sys.gnss_dc.test"
		 int v1 = 0; // const/4 v1, 0x0
		 v0 = 		 android.os.SystemProperties .getBoolean ( v0,v1 );
		 /* iput-boolean v0, p0, Lcom/android/server/location/gnss/monitor/LocationSettingsEventMonitorImpl;->D:Z */
		 /* .line 39 */
		 return;
	 } // .end method
	 private java.lang.String getCallingPackageName ( Integer p0 ) {
		 /* .locals 6 */
		 /* .param p1, "pid" # I */
		 /* .line 60 */
		 v0 = this.mContext;
		 int v1 = 0; // const/4 v1, 0x0
		 /* if-nez v0, :cond_0 */
		 /* .line 61 */
	 } // :cond_0
	 final String v2 = "activity"; // const-string v2, "activity"
	 (( android.content.Context ) v0 ).getSystemService ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
	 /* check-cast v0, Landroid/app/ActivityManager; */
	 /* .line 62 */
	 /* .local v0, "activityManager":Landroid/app/ActivityManager; */
	 (( android.app.ActivityManager ) v0 ).getRunningAppProcesses ( ); // invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;
	 /* .line 63 */
	 /* .local v2, "runningAppProcessInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;" */
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_2
	 /* check-cast v4, Landroid/app/ActivityManager$RunningAppProcessInfo; */
	 /* .line 64 */
	 /* .local v4, "runningAppProcessInfo":Landroid/app/ActivityManager$RunningAppProcessInfo; */
	 /* iget v5, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I */
	 /* if-ne v5, p1, :cond_1 */
	 /* .line 65 */
	 v1 = this.processName;
	 /* .line 67 */
} // .end local v4 # "runningAppProcessInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
} // :cond_1
/* .line 68 */
} // :cond_2
} // .end method
/* # virtual methods */
public void dumpCallingInfo ( Integer p0, Integer p1, Boolean p2 ) {
/* .locals 4 */
/* .param p1, "userId" # I */
/* .param p2, "pid" # I */
/* .param p3, "enabled" # Z */
/* .line 49 */
/* invoke-direct {p0, p2}, Lcom/android/server/location/gnss/monitor/LocationSettingsEventMonitorImpl;->getCallingPackageName(I)Ljava/lang/String; */
/* .line 50 */
/* .local v0, "callingPackageName":Ljava/lang/String; */
final String v1 = "android"; // const-string v1, "android"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_3 */
/* const-string/jumbo v1, "system" */
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 52 */
} // :cond_0
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 53 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "[u"; // const-string v2, "[u"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = "] location enabled = "; // const-string v2, "] location enabled = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = ", because:"; // const-string v2, ", because:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 54 */
/* .local v1, "debugInfo":Ljava/lang/String; */
/* iget-boolean v2, p0, Lcom/android/server/location/gnss/monitor/LocationSettingsEventMonitorImpl;->D:Z */
if ( v2 != null) { // if-eqz v2, :cond_1
final String v2 = "LocationSettingsEventMonitor"; // const-string v2, "LocationSettingsEventMonitor"
android.util.Log .d ( v2,v1 );
/* .line 55 */
} // :cond_1
com.android.server.location.LocationDumpLogStub .getInstance ( );
int v3 = 1; // const/4 v3, 0x1
/* .line 57 */
} // .end local v1 # "debugInfo":Ljava/lang/String;
} // :cond_2
return;
/* .line 51 */
} // :cond_3
} // :goto_0
return;
} // .end method
public void initMonitor ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 43 */
this.mContext = p1;
/* .line 44 */
final String v0 = "LocationSettingsEventMonitor"; // const-string v0, "LocationSettingsEventMonitor"
final String v1 = "init LocationSettingsEventMonitor done"; // const-string v1, "init LocationSettingsEventMonitor done"
android.util.Log .d ( v0,v1 );
/* .line 45 */
return;
} // .end method
