.class public Lcom/android/server/location/gnss/monitor/LocationSettingsEventMonitorImpl;
.super Ljava/lang/Object;
.source "LocationSettingsEventMonitorImpl.java"

# interfaces
.implements Lcom/android/server/location/gnss/monitor/LocationSettingsEventMonitorStub;


# static fields
.field private static final DISABLED_BY_ANDROID:Ljava/lang/String; = "android"

.field private static final DISABLED_BY_SYSTEM:Ljava/lang/String; = "system"

.field private static final TAG:Ljava/lang/String; = "LocationSettingsEventMonitor"


# instance fields
.field private final D:Z

.field private mContext:Landroid/content/Context;


# direct methods
.method constructor <init>()V
    .locals 2

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const-string v0, "persist.sys.gnss_dc.test"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/location/gnss/monitor/LocationSettingsEventMonitorImpl;->D:Z

    .line 39
    return-void
.end method

.method private getCallingPackageName(I)Ljava/lang/String;
    .locals 6
    .param p1, "pid"    # I

    .line 60
    iget-object v0, p0, Lcom/android/server/location/gnss/monitor/LocationSettingsEventMonitorImpl;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 61
    :cond_0
    const-string v2, "activity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 62
    .local v0, "activityManager":Landroid/app/ActivityManager;
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v2

    .line 63
    .local v2, "runningAppProcessInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 64
    .local v4, "runningAppProcessInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    iget v5, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-ne v5, p1, :cond_1

    .line 65
    iget-object v1, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    return-object v1

    .line 67
    .end local v4    # "runningAppProcessInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    :cond_1
    goto :goto_0

    .line 68
    :cond_2
    return-object v1
.end method


# virtual methods
.method public dumpCallingInfo(IIZ)V
    .locals 4
    .param p1, "userId"    # I
    .param p2, "pid"    # I
    .param p3, "enabled"    # Z

    .line 49
    invoke-direct {p0, p2}, Lcom/android/server/location/gnss/monitor/LocationSettingsEventMonitorImpl;->getCallingPackageName(I)Ljava/lang/String;

    move-result-object v0

    .line 50
    .local v0, "callingPackageName":Ljava/lang/String;
    const-string v1, "android"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string/jumbo v1, "system"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 52
    :cond_0
    if-eqz v0, :cond_2

    .line 53
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[u"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] location enabled = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", because:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 54
    .local v1, "debugInfo":Ljava/lang/String;
    iget-boolean v2, p0, Lcom/android/server/location/gnss/monitor/LocationSettingsEventMonitorImpl;->D:Z

    if-eqz v2, :cond_1

    const-string v2, "LocationSettingsEventMonitor"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    :cond_1
    invoke-static {}, Lcom/android/server/location/LocationDumpLogStub;->getInstance()Lcom/android/server/location/LocationDumpLogStub;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v2, v3, v1}, Lcom/android/server/location/LocationDumpLogStub;->addToBugreport(ILjava/lang/String;)V

    .line 57
    .end local v1    # "debugInfo":Ljava/lang/String;
    :cond_2
    return-void

    .line 51
    :cond_3
    :goto_0
    return-void
.end method

.method public initMonitor(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 43
    iput-object p1, p0, Lcom/android/server/location/gnss/monitor/LocationSettingsEventMonitorImpl;->mContext:Landroid/content/Context;

    .line 44
    const-string v0, "LocationSettingsEventMonitor"

    const-string v1, "init LocationSettingsEventMonitor done"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    return-void
.end method
