.class Lcom/android/server/location/gnss/GnssLocationProviderImpl$BlurLocationItem;
.super Ljava/lang/Object;
.source "GnssLocationProviderImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/location/gnss/GnssLocationProviderImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BlurLocationItem"
.end annotation


# instance fields
.field private blurPackageName:Ljava/lang/String;

.field private blurState:I

.field private blurUid:I

.field final synthetic this$0:Lcom/android/server/location/gnss/GnssLocationProviderImpl;


# direct methods
.method private constructor <init>(Lcom/android/server/location/gnss/GnssLocationProviderImpl;ILjava/lang/String;I)V
    .locals 0
    .param p2, "state"    # I
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "uid"    # I

    .line 660
    iput-object p1, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl$BlurLocationItem;->this$0:Lcom/android/server/location/gnss/GnssLocationProviderImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 661
    iput p2, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl$BlurLocationItem;->blurState:I

    .line 662
    iput-object p3, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl$BlurLocationItem;->blurPackageName:Ljava/lang/String;

    .line 663
    iput p4, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl$BlurLocationItem;->blurUid:I

    .line 664
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/location/gnss/GnssLocationProviderImpl;ILjava/lang/String;ILcom/android/server/location/gnss/GnssLocationProviderImpl$BlurLocationItem-IA;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/location/gnss/GnssLocationProviderImpl$BlurLocationItem;-><init>(Lcom/android/server/location/gnss/GnssLocationProviderImpl;ILjava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public getPackageName()Ljava/lang/String;
    .locals 1

    .line 671
    iget-object v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl$BlurLocationItem;->blurPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public getState()I
    .locals 1

    .line 667
    iget v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl$BlurLocationItem;->blurState:I

    return v0
.end method

.method public getUid()I
    .locals 1

    .line 675
    iget v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl$BlurLocationItem;->blurUid:I

    return v0
.end method
