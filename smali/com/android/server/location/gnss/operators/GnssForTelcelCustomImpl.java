public class com.android.server.location.gnss.operators.GnssForTelcelCustomImpl implements com.android.server.location.gnss.operators.GnssForTelcelCustomStub {
	 /* .source "GnssForTelcelCustomImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final Boolean IS_MX_TELCEL_REGION;
	 private static final java.lang.String PKG_NAME_CMT;
	 private static final java.lang.String SP_CHILD_DIR;
	 private static final java.lang.String SP_CHILD_FILE;
	 private static final java.lang.String SP_KEY_CMT_PERMISSION;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private final java.io.File mCmtSpFile;
	 private Boolean mIsCmtHasDoPermission;
	 /* # direct methods */
	 static void -$$Nest$misCmtHasDeviceAdmin ( com.android.server.location.gnss.operators.GnssForTelcelCustomImpl p0, android.content.Context p1, android.app.admin.DevicePolicyManager p2 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0, p1, p2}, Lcom/android/server/location/gnss/operators/GnssForTelcelCustomImpl;->isCmtHasDeviceAdmin(Landroid/content/Context;Landroid/app/admin/DevicePolicyManager;)V */
		 return;
	 } // .end method
	 static com.android.server.location.gnss.operators.GnssForTelcelCustomImpl ( ) {
		 /* .locals 2 */
		 /* .line 33 */
		 /* nop */
		 /* .line 34 */
		 final String v0 = "ro.miui.customized.region"; // const-string v0, "ro.miui.customized.region"
		 android.os.SystemProperties .get ( v0 );
		 /* .line 33 */
		 final String v1 = "mx_telcel"; // const-string v1, "mx_telcel"
		 v0 = 		 (( java.lang.String ) v1 ).equalsIgnoreCase ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
		 com.android.server.location.gnss.operators.GnssForTelcelCustomImpl.IS_MX_TELCEL_REGION = (v0!= 0);
		 return;
	 } // .end method
	 public com.android.server.location.gnss.operators.GnssForTelcelCustomImpl ( ) {
		 /* .locals 4 */
		 /* .line 27 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 32 */
		 /* new-instance v0, Ljava/io/File; */
		 /* new-instance v1, Ljava/io/File; */
		 android.os.Environment .getDataDirectory ( );
		 /* const-string/jumbo v3, "system" */
		 /* invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
		 final String v2 = "gnss_cmt.xml"; // const-string v2, "gnss_cmt.xml"
		 /* invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
		 this.mCmtSpFile = v0;
		 return;
	 } // .end method
	 private void isCmtHasDeviceAdmin ( android.content.Context p0, android.app.admin.DevicePolicyManager p1 ) {
		 /* .locals 8 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .param p2, "dpm" # Landroid/app/admin/DevicePolicyManager; */
		 /* .line 72 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* iput-boolean v0, p0, Lcom/android/server/location/gnss/operators/GnssForTelcelCustomImpl;->mIsCmtHasDoPermission:Z */
		 /* .line 73 */
		 final String v1 = "com.controlmovil.telcel"; // const-string v1, "com.controlmovil.telcel"
		 v2 = 		 (( android.app.admin.DevicePolicyManager ) p2 ).isDeviceOwnerApp ( v1 ); // invoke-virtual {p2, v1}, Landroid/app/admin/DevicePolicyManager;->isDeviceOwnerApp(Ljava/lang/String;)Z
		 final String v3 = "GnssForTelcelCustomImpl"; // const-string v3, "GnssForTelcelCustomImpl"
		 int v4 = 1; // const/4 v4, 0x1
		 /* if-nez v2, :cond_3 */
		 /* .line 74 */
		 (( android.app.admin.DevicePolicyManager ) p2 ).getActiveAdmins ( ); // invoke-virtual {p2}, Landroid/app/admin/DevicePolicyManager;->getActiveAdmins()Ljava/util/List;
		 /* .line 75 */
		 /* .local v2, "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/ComponentName;>;" */
		 /* if-nez v2, :cond_0 */
		 return;
		 /* .line 76 */
	 } // :cond_0
v6 = } // :goto_0
if ( v6 != null) { // if-eqz v6, :cond_2
	 /* check-cast v6, Landroid/content/ComponentName; */
	 /* .line 77 */
	 /* .local v6, "componentName":Landroid/content/ComponentName; */
	 if ( v6 != null) { // if-eqz v6, :cond_1
		 (( android.content.ComponentName ) v6 ).getPackageName ( ); // invoke-virtual {v6}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
		 v7 = 		 (( java.lang.String ) v1 ).equals ( v7 ); // invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v7 != null) { // if-eqz v7, :cond_1
			 /* .line 78 */
			 /* iput-boolean v4, p0, Lcom/android/server/location/gnss/operators/GnssForTelcelCustomImpl;->mIsCmtHasDoPermission:Z */
			 /* .line 80 */
		 } // .end local v6 # "componentName":Landroid/content/ComponentName;
	 } // :cond_1
	 /* .line 81 */
} // :cond_2
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "receive device admin changed action , mx cmt has permission:"; // const-string v4, "receive device admin changed action , mx cmt has permission:"
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v4, p0, Lcom/android/server/location/gnss/operators/GnssForTelcelCustomImpl;->mIsCmtHasDoPermission:Z */
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v3,v1 );
/* .line 82 */
} // .end local v2 # "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/ComponentName;>;"
/* .line 83 */
} // :cond_3
/* iput-boolean v4, p0, Lcom/android/server/location/gnss/operators/GnssForTelcelCustomImpl;->mIsCmtHasDoPermission:Z */
/* .line 84 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "receive device owner changed action , mx cmt has permission:"; // const-string v2, "receive device owner changed action , mx cmt has permission:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/location/gnss/operators/GnssForTelcelCustomImpl;->mIsCmtHasDoPermission:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v3,v1 );
/* .line 86 */
} // :goto_1
v1 = this.mCmtSpFile;
(( android.content.Context ) p1 ).getSharedPreferences ( v1, v0 ); // invoke-virtual {p1, v1, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/io/File;I)Landroid/content/SharedPreferences;
/* .line 87 */
/* .local v0, "editor":Landroid/content/SharedPreferences$Editor; */
final String v1 = "key_cmt"; // const-string v1, "key_cmt"
/* iget-boolean v2, p0, Lcom/android/server/location/gnss/operators/GnssForTelcelCustomImpl;->mIsCmtHasDoPermission:Z */
/* .line 88 */
/* .line 89 */
return;
} // .end method
/* # virtual methods */
public void init ( android.content.Context p0 ) {
/* .locals 10 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 41 */
/* sget-boolean v0, Lcom/android/server/location/gnss/operators/GnssForTelcelCustomImpl;->IS_MX_TELCEL_REGION:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 42 */
final String v0 = "is mx region"; // const-string v0, "is mx region"
final String v1 = "GnssForTelcelCustomImpl"; // const-string v1, "GnssForTelcelCustomImpl"
android.util.Log .d ( v1,v0 );
/* .line 43 */
final String v0 = "device_policy"; // const-string v0, "device_policy"
(( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/admin/DevicePolicyManager; */
/* .line 45 */
/* .local v0, "dpm":Landroid/app/admin/DevicePolicyManager; */
(( android.content.Context ) p1 ).createDeviceProtectedStorageContext ( ); // invoke-virtual {p1}, Landroid/content/Context;->createDeviceProtectedStorageContext()Landroid/content/Context;
/* .line 46 */
/* .local v2, "directBootContext":Landroid/content/Context; */
v3 = this.mCmtSpFile;
int v4 = 0; // const/4 v4, 0x0
(( android.content.Context ) v2 ).getSharedPreferences ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/io/File;I)Landroid/content/SharedPreferences;
/* .line 47 */
/* .local v3, "sp":Landroid/content/SharedPreferences; */
v4 = final String v5 = "key_cmt"; // const-string v5, "key_cmt"
/* iput-boolean v4, p0, Lcom/android/server/location/gnss/operators/GnssForTelcelCustomImpl;->mIsCmtHasDoPermission:Z */
/* .line 48 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "CMT has device owner or admin permission when boot start:"; // const-string v5, "CMT has device owner or admin permission when boot start:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v5, p0, Lcom/android/server/location/gnss/operators/GnssForTelcelCustomImpl;->mIsCmtHasDoPermission:Z */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v4 );
/* .line 49 */
/* new-instance v1, Landroid/content/IntentFilter; */
/* invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V */
/* .line 50 */
/* .local v1, "intentFilter":Landroid/content/IntentFilter; */
final String v4 = "android.app.action.DEVICE_OWNER_CHANGED"; // const-string v4, "android.app.action.DEVICE_OWNER_CHANGED"
(( android.content.IntentFilter ) v1 ).addAction ( v4 ); // invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 51 */
final String v4 = "android.app.action.DEVICE_POLICY_MANAGER_STATE_CHANGED"; // const-string v4, "android.app.action.DEVICE_POLICY_MANAGER_STATE_CHANGED"
(( android.content.IntentFilter ) v1 ).addAction ( v4 ); // invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 52 */
/* new-instance v5, Lcom/android/server/location/gnss/operators/GnssForTelcelCustomImpl$1; */
/* invoke-direct {v5, p0, v0}, Lcom/android/server/location/gnss/operators/GnssForTelcelCustomImpl$1;-><init>(Lcom/android/server/location/gnss/operators/GnssForTelcelCustomImpl;Landroid/app/admin/DevicePolicyManager;)V */
v6 = android.os.UserHandle.ALL;
int v8 = 0; // const/4 v8, 0x0
/* .line 61 */
com.android.server.FgThread .getHandler ( );
/* .line 52 */
/* move-object v4, p1 */
/* move-object v7, v1 */
/* invoke-virtual/range {v4 ..v9}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent; */
/* .line 63 */
} // .end local v0 # "dpm":Landroid/app/admin/DevicePolicyManager;
} // .end local v1 # "intentFilter":Landroid/content/IntentFilter;
} // .end local v2 # "directBootContext":Landroid/content/Context;
} // .end local v3 # "sp":Landroid/content/SharedPreferences;
} // :cond_0
return;
} // .end method
public Boolean isCmtHasPermission ( ) {
/* .locals 1 */
/* .line 93 */
/* iget-boolean v0, p0, Lcom/android/server/location/gnss/operators/GnssForTelcelCustomImpl;->mIsCmtHasDoPermission:Z */
} // .end method
