public class com.android.server.location.gnss.operators.GnssForCommonOperatorImpl implements com.android.server.location.gnss.operators.GnssForOperatorCommonStub {
	 /* .source "GnssForCommonOperatorImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final java.lang.String CUSTOM_MCC_MNC;
	 /* # direct methods */
	 public com.android.server.location.gnss.operators.GnssForCommonOperatorImpl ( ) {
		 /* .locals 0 */
		 /* .line 11 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Boolean isNiWhiteListOperator ( ) {
		 /* .locals 3 */
		 /* .line 17 */
		 final String v0 = "persist.sys.mcc.mnc"; // const-string v0, "persist.sys.mcc.mnc"
		 final String v1 = ""; // const-string v1, ""
		 android.os.SystemProperties .get ( v0,v1 );
		 /* .line 19 */
		 /* .local v0, "mccmnc":Ljava/lang/String; */
		 /* new-instance v1, Ljava/util/ArrayList; */
		 final String v2 = "25020"; // const-string v2, "25020"
		 /* filled-new-array {v2}, [Ljava/lang/String; */
		 java.util.Arrays .asList ( v2 );
		 /* invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
		 /* .line 20 */
		 /* .local v1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
		 v2 = 		 (( java.util.ArrayList ) v1 ).contains ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
	 } // .end method
