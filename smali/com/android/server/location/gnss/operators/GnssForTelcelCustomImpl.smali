.class public Lcom/android/server/location/gnss/operators/GnssForTelcelCustomImpl;
.super Ljava/lang/Object;
.source "GnssForTelcelCustomImpl.java"

# interfaces
.implements Lcom/android/server/location/gnss/operators/GnssForTelcelCustomStub;


# static fields
.field private static final IS_MX_TELCEL_REGION:Z

.field private static final PKG_NAME_CMT:Ljava/lang/String; = "com.controlmovil.telcel"

.field private static final SP_CHILD_DIR:Ljava/lang/String; = "system"

.field private static final SP_CHILD_FILE:Ljava/lang/String; = "gnss_cmt.xml"

.field private static final SP_KEY_CMT_PERMISSION:Ljava/lang/String; = "key_cmt"

.field private static final TAG:Ljava/lang/String; = "GnssForTelcelCustomImpl"


# instance fields
.field private final mCmtSpFile:Ljava/io/File;

.field private mIsCmtHasDoPermission:Z


# direct methods
.method static bridge synthetic -$$Nest$misCmtHasDeviceAdmin(Lcom/android/server/location/gnss/operators/GnssForTelcelCustomImpl;Landroid/content/Context;Landroid/app/admin/DevicePolicyManager;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/location/gnss/operators/GnssForTelcelCustomImpl;->isCmtHasDeviceAdmin(Landroid/content/Context;Landroid/app/admin/DevicePolicyManager;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 33
    nop

    .line 34
    const-string v0, "ro.miui.customized.region"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 33
    const-string v1, "mx_telcel"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/location/gnss/operators/GnssForTelcelCustomImpl;->IS_MX_TELCEL_REGION:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v2

    const-string/jumbo v3, "system"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const-string v2, "gnss_cmt.xml"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/location/gnss/operators/GnssForTelcelCustomImpl;->mCmtSpFile:Ljava/io/File;

    return-void
.end method

.method private isCmtHasDeviceAdmin(Landroid/content/Context;Landroid/app/admin/DevicePolicyManager;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "dpm"    # Landroid/app/admin/DevicePolicyManager;

    .line 72
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/location/gnss/operators/GnssForTelcelCustomImpl;->mIsCmtHasDoPermission:Z

    .line 73
    const-string v1, "com.controlmovil.telcel"

    invoke-virtual {p2, v1}, Landroid/app/admin/DevicePolicyManager;->isDeviceOwnerApp(Ljava/lang/String;)Z

    move-result v2

    const-string v3, "GnssForTelcelCustomImpl"

    const/4 v4, 0x1

    if-nez v2, :cond_3

    .line 74
    invoke-virtual {p2}, Landroid/app/admin/DevicePolicyManager;->getActiveAdmins()Ljava/util/List;

    move-result-object v2

    .line 75
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/ComponentName;>;"
    if-nez v2, :cond_0

    return-void

    .line 76
    :cond_0
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/ComponentName;

    .line 77
    .local v6, "componentName":Landroid/content/ComponentName;
    if-eqz v6, :cond_1

    invoke-virtual {v6}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 78
    iput-boolean v4, p0, Lcom/android/server/location/gnss/operators/GnssForTelcelCustomImpl;->mIsCmtHasDoPermission:Z

    .line 80
    .end local v6    # "componentName":Landroid/content/ComponentName;
    :cond_1
    goto :goto_0

    .line 81
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "receive device admin changed action , mx cmt has permission:"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v4, p0, Lcom/android/server/location/gnss/operators/GnssForTelcelCustomImpl;->mIsCmtHasDoPermission:Z

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    .end local v2    # "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/ComponentName;>;"
    goto :goto_1

    .line 83
    :cond_3
    iput-boolean v4, p0, Lcom/android/server/location/gnss/operators/GnssForTelcelCustomImpl;->mIsCmtHasDoPermission:Z

    .line 84
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "receive device owner changed action , mx cmt has permission:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/location/gnss/operators/GnssForTelcelCustomImpl;->mIsCmtHasDoPermission:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    :goto_1
    iget-object v1, p0, Lcom/android/server/location/gnss/operators/GnssForTelcelCustomImpl;->mCmtSpFile:Ljava/io/File;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/io/File;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 87
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "key_cmt"

    iget-boolean v2, p0, Lcom/android/server/location/gnss/operators/GnssForTelcelCustomImpl;->mIsCmtHasDoPermission:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 88
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 89
    return-void
.end method


# virtual methods
.method public init(Landroid/content/Context;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;

    .line 41
    sget-boolean v0, Lcom/android/server/location/gnss/operators/GnssForTelcelCustomImpl;->IS_MX_TELCEL_REGION:Z

    if-eqz v0, :cond_0

    .line 42
    const-string v0, "is mx region"

    const-string v1, "GnssForTelcelCustomImpl"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    const-string v0, "device_policy"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    .line 45
    .local v0, "dpm":Landroid/app/admin/DevicePolicyManager;
    invoke-virtual {p1}, Landroid/content/Context;->createDeviceProtectedStorageContext()Landroid/content/Context;

    move-result-object v2

    .line 46
    .local v2, "directBootContext":Landroid/content/Context;
    iget-object v3, p0, Lcom/android/server/location/gnss/operators/GnssForTelcelCustomImpl;->mCmtSpFile:Ljava/io/File;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/io/File;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 47
    .local v3, "sp":Landroid/content/SharedPreferences;
    const-string v5, "key_cmt"

    invoke-interface {v3, v5, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    iput-boolean v4, p0, Lcom/android/server/location/gnss/operators/GnssForTelcelCustomImpl;->mIsCmtHasDoPermission:Z

    .line 48
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CMT has device owner or admin permission when boot start:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/android/server/location/gnss/operators/GnssForTelcelCustomImpl;->mIsCmtHasDoPermission:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 50
    .local v1, "intentFilter":Landroid/content/IntentFilter;
    const-string v4, "android.app.action.DEVICE_OWNER_CHANGED"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 51
    const-string v4, "android.app.action.DEVICE_POLICY_MANAGER_STATE_CHANGED"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 52
    new-instance v5, Lcom/android/server/location/gnss/operators/GnssForTelcelCustomImpl$1;

    invoke-direct {v5, p0, v0}, Lcom/android/server/location/gnss/operators/GnssForTelcelCustomImpl$1;-><init>(Lcom/android/server/location/gnss/operators/GnssForTelcelCustomImpl;Landroid/app/admin/DevicePolicyManager;)V

    sget-object v6, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const/4 v8, 0x0

    .line 61
    invoke-static {}, Lcom/android/server/FgThread;->getHandler()Landroid/os/Handler;

    move-result-object v9

    .line 52
    move-object v4, p1

    move-object v7, v1

    invoke-virtual/range {v4 .. v9}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 63
    .end local v0    # "dpm":Landroid/app/admin/DevicePolicyManager;
    .end local v1    # "intentFilter":Landroid/content/IntentFilter;
    .end local v2    # "directBootContext":Landroid/content/Context;
    .end local v3    # "sp":Landroid/content/SharedPreferences;
    :cond_0
    return-void
.end method

.method public isCmtHasPermission()Z
    .locals 1

    .line 93
    iget-boolean v0, p0, Lcom/android/server/location/gnss/operators/GnssForTelcelCustomImpl;->mIsCmtHasDoPermission:Z

    return v0
.end method
