.class public Lcom/android/server/location/gnss/operators/GnssForCommonOperatorImpl;
.super Ljava/lang/Object;
.source "GnssForCommonOperatorImpl.java"

# interfaces
.implements Lcom/android/server/location/gnss/operators/GnssForOperatorCommonStub;


# static fields
.field private static final CUSTOM_MCC_MNC:Ljava/lang/String; = "25020"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public isNiWhiteListOperator()Z
    .locals 3

    .line 17
    const-string v0, "persist.sys.mcc.mnc"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 19
    .local v0, "mccmnc":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    const-string v2, "25020"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 20
    .local v1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    return v2
.end method
