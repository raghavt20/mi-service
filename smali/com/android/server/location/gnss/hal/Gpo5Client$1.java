class com.android.server.location.gnss.hal.Gpo5Client$1 implements android.hardware.SensorEventListener {
	 /* .source "Gpo5Client.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/location/gnss/hal/Gpo5Client; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.location.gnss.hal.Gpo5Client this$0; //synthetic
/* # direct methods */
 com.android.server.location.gnss.hal.Gpo5Client$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/location/gnss/hal/Gpo5Client; */
/* .line 96 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onAccuracyChanged ( android.hardware.Sensor p0, Integer p1 ) {
/* .locals 0 */
/* .param p1, "sensor" # Landroid/hardware/Sensor; */
/* .param p2, "accuracy" # I */
/* .line 116 */
return;
} // .end method
public void onSensorChanged ( android.hardware.SensorEvent p0 ) {
/* .locals 4 */
/* .param p1, "event" # Landroid/hardware/SensorEvent; */
/* .line 99 */
v0 = this.this$0;
com.android.server.location.gnss.hal.Gpo5Client .-$$Nest$fgetmGpoUtil ( v0 );
v1 = this.values;
int v2 = 0; // const/4 v2, 0x0
/* aget v1, v1, v2 */
/* float-to-int v1, v1 */
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).setUserBehav ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->setUserBehav(I)V
/* .line 100 */
v0 = this.this$0;
com.android.server.location.gnss.hal.Gpo5Client .-$$Nest$fgetmGpoUtil ( v0 );
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Sensor report user behavior: "; // const-string v3, "Sensor report user behavior: "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.values;
/* aget v3, v3, v2 */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "Gpo5Client"; // const-string v3, "Gpo5Client"
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logv ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 101 */
v0 = this.values;
/* aget v0, v0, v2 */
/* const/high16 v1, 0x40800000 # 4.0f */
/* cmpl-float v0, v0, v1 */
/* if-nez v0, :cond_1 */
/* .line 103 */
v0 = this.this$0;
com.android.server.location.gnss.hal.Gpo5Client .-$$Nest$fgetmLastSensorTrafficTime ( v0 );
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v1 */
(( java.util.concurrent.atomic.AtomicLong ) v0 ).set ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V
/* .line 105 */
v0 = this.this$0;
com.android.server.location.gnss.hal.Gpo5Client .-$$Nest$fgetmGpoUtil ( v0 );
v0 = (( com.android.server.location.gnss.hal.GpoUtil ) v0 ).getEngineStatus ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->getEngineStatus()I
int v1 = 4; // const/4 v1, 0x4
/* if-ne v0, v1, :cond_0 */
/* .line 106 */
return;
/* .line 108 */
} // :cond_0
v0 = this.this$0;
com.android.server.location.gnss.hal.Gpo5Client .-$$Nest$fgetmGpoUtil ( v0 );
final String v1 = "User is on Traffic."; // const-string v1, "User is on Traffic."
int v2 = 1; // const/4 v2, 0x1
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logi ( v3, v1, v2 ); // invoke-virtual {v0, v3, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V
/* .line 109 */
v0 = this.this$0;
com.android.server.location.gnss.hal.Gpo5Client .-$$Nest$mhandleLeaveSmallArea ( v0 );
/* .line 111 */
} // :cond_1
return;
} // .end method
