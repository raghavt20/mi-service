class com.android.server.location.gnss.hal.GnssPowerOptimizeImpl$1 implements com.android.server.location.gnss.hal.GpoUtil$IDataEvent {
	 /* .source "GnssPowerOptimizeImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.location.gnss.hal.GnssPowerOptimizeImpl this$0; //synthetic
/* # direct methods */
 com.android.server.location.gnss.hal.GnssPowerOptimizeImpl$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl; */
/* .line 35 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void updateDefaultNetwork ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "type" # I */
/* .line 55 */
return;
} // .end method
public void updateFeatureSwitch ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "version" # I */
/* .line 38 */
v0 = this.this$0;
v0 = com.android.server.location.gnss.hal.GnssPowerOptimizeImpl .-$$Nest$fgetmGpoVersion ( v0 );
/* if-eq v0, p1, :cond_0 */
v0 = this.this$0;
v0 = (( com.android.server.location.gnss.hal.GnssPowerOptimizeImpl ) v0 ).getEngineStatus ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->getEngineStatus()I
int v1 = 4; // const/4 v1, 0x4
/* if-ne v0, v1, :cond_0 */
/* .line 40 */
v0 = this.this$0;
com.android.server.location.gnss.hal.GnssPowerOptimizeImpl .-$$Nest$fgetmGpoUtil ( v0 );
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Update Gpo Client"; // const-string v2, "Update Gpo Client"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v2 = 1; // const/4 v2, 0x1
final String v3 = "GnssPowerOptimize"; // const-string v3, "GnssPowerOptimize"
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logi ( v3, v1, v2 ); // invoke-virtual {v0, v3, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V
/* .line 41 */
v0 = this.this$0;
com.android.server.location.gnss.hal.GnssPowerOptimizeImpl .-$$Nest$fgetmContext ( v0 );
(( com.android.server.location.gnss.hal.GnssPowerOptimizeImpl ) v0 ).init ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->init(Landroid/content/Context;)V
/* .line 43 */
} // :cond_0
return;
} // .end method
public void updateGnssStatus ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "status" # I */
/* .line 59 */
v0 = this.this$0;
com.android.server.location.gnss.hal.GnssPowerOptimizeImpl .-$$Nest$fgetmGpo5Client ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.this$0;
com.android.server.location.gnss.hal.GnssPowerOptimizeImpl .-$$Nest$fgetmGpo5Client ( v0 );
(( com.android.server.location.gnss.hal.Gpo5Client ) v0 ).updateGnssStatus ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/location/gnss/hal/Gpo5Client;->updateGnssStatus(I)V
/* .line 60 */
} // :cond_0
return;
} // .end method
public void updateScreenState ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "on" # Z */
/* .line 47 */
v0 = this.this$0;
com.android.server.location.gnss.hal.GnssPowerOptimizeImpl .-$$Nest$fgetmGpoUtil ( v0 );
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Screen On ? "; // const-string v2, "Screen On ? "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v2 = 1; // const/4 v2, 0x1
final String v3 = "GnssPowerOptimize"; // const-string v3, "GnssPowerOptimize"
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logi ( v3, v1, v2 ); // invoke-virtual {v0, v3, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V
/* .line 48 */
v0 = this.this$0;
com.android.server.location.gnss.hal.GnssPowerOptimizeImpl .-$$Nest$fgetmGpo5Client ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.this$0;
com.android.server.location.gnss.hal.GnssPowerOptimizeImpl .-$$Nest$fgetmGpo5Client ( v0 );
(( com.android.server.location.gnss.hal.Gpo5Client ) v0 ).updateScreenState ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/location/gnss/hal/Gpo5Client;->updateScreenState(Z)V
/* .line 49 */
} // :cond_0
v0 = this.this$0;
com.android.server.location.gnss.hal.GnssPowerOptimizeImpl .-$$Nest$fgetmGpo4Client ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.this$0;
com.android.server.location.gnss.hal.GnssPowerOptimizeImpl .-$$Nest$fgetmGpo4Client ( v0 );
(( com.android.server.location.gnss.hal.Gpo4Client ) v0 ).updateScreenState ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/location/gnss/hal/Gpo4Client;->updateScreenState(Z)V
/* .line 50 */
} // :cond_1
} // :goto_0
return;
} // .end method
