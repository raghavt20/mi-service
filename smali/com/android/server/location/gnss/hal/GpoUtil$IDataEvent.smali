.class public interface abstract Lcom/android/server/location/gnss/hal/GpoUtil$IDataEvent;
.super Ljava/lang/Object;
.source "GpoUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/location/gnss/hal/GpoUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "IDataEvent"
.end annotation


# virtual methods
.method public abstract updateDefaultNetwork(I)V
.end method

.method public abstract updateFeatureSwitch(I)V
.end method

.method public abstract updateGnssStatus(I)V
.end method

.method public abstract updateScreenState(Z)V
.end method
