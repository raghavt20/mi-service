.class public Lcom/android/server/location/gnss/hal/GpoUtil;
.super Ljava/lang/Object;
.source "GpoUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/location/gnss/hal/GpoUtil$DataEventAdapter;,
        Lcom/android/server/location/gnss/hal/GpoUtil$IDataEvent;
    }
.end annotation


# static fields
.field private static final MAX_ENGINE_TIME:J = 0x36ee80L

.field private static final MAX_RECORD_ENGINE_SIZE:I = 0x5

.field private static final PERSIST_FOR_GPO_VERSION:Ljava/lang/String; = "persist.sys.gpo.version"

.field private static final SP_INDEX:Ljava/lang/String; = "index"

.field private static final TAG:Ljava/lang/String; = "GpoUtil"

.field private static final VERBOSE:Z

.field private static volatile instance:Lcom/android/server/location/gnss/hal/GpoUtil;


# instance fields
.field private final isHeavyUser:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private isUserUnlocked:Z

.field private mAdapter:Lcom/android/server/location/gnss/hal/GpoUtil$DataEventAdapter;

.field private mAppOps:Landroid/app/AppOpsManager;

.field private mConnectivityManager:Landroid/net/ConnectivityManager;

.field private mContext:Landroid/content/Context;

.field private final mDefaultNetwork:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final mGnssEngineStatus:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final mGnssEngineStoppedByInstance:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mGnssHal:Ljava/lang/Object;

.field private final mGpoVersion:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final mScreenUnlockReceiver:Landroid/content/BroadcastReceiver;

.field private final mSpFileGnssEngineUsageTime:Ljava/io/File;

.field private mUserBehavReport:I

.field private final mUserUnlockReceiver:Landroid/content/BroadcastReceiver;

.field private startMethod:Ljava/lang/reflect/Method;

.field private stopMethod:Ljava/lang/reflect/Method;


# direct methods
.method static bridge synthetic -$$Nest$fgetmAdapter(Lcom/android/server/location/gnss/hal/GpoUtil;)Lcom/android/server/location/gnss/hal/GpoUtil$DataEventAdapter;
    .locals 0

    iget-object p0, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mAdapter:Lcom/android/server/location/gnss/hal/GpoUtil$DataEventAdapter;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/location/gnss/hal/GpoUtil;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmUserUnlockReceiver(Lcom/android/server/location/gnss/hal/GpoUtil;)Landroid/content/BroadcastReceiver;
    .locals 0

    iget-object p0, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mUserUnlockReceiver:Landroid/content/BroadcastReceiver;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputisUserUnlocked(Lcom/android/server/location/gnss/hal/GpoUtil;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->isUserUnlocked:Z

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 34
    const-string v0, "GpoUtil"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/location/gnss/hal/GpoUtil;->VERBOSE:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mGnssEngineStatus:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 49
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mDefaultNetwork:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 51
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    .line 52
    const-string v1, "persist.sys.gpo.version"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mGpoVersion:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 54
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->isHeavyUser:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 55
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mGnssEngineStoppedByInstance:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 57
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/io/File;

    .line 58
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v3

    const-string/jumbo v4, "system"

    invoke-direct {v1, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const-string v3, "GnssEngineUsageTime.xml"

    invoke-direct {v0, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mSpFileGnssEngineUsageTime:Ljava/io/File;

    .line 67
    iput-boolean v2, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->isUserUnlocked:Z

    .line 68
    iput v2, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mUserBehavReport:I

    .line 147
    new-instance v0, Lcom/android/server/location/gnss/hal/GpoUtil$2;

    invoke-direct {v0, p0}, Lcom/android/server/location/gnss/hal/GpoUtil$2;-><init>(Lcom/android/server/location/gnss/hal/GpoUtil;)V

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mUserUnlockReceiver:Landroid/content/BroadcastReceiver;

    .line 162
    new-instance v0, Lcom/android/server/location/gnss/hal/GpoUtil$3;

    invoke-direct {v0, p0}, Lcom/android/server/location/gnss/hal/GpoUtil$3;-><init>(Lcom/android/server/location/gnss/hal/GpoUtil;)V

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mScreenUnlockReceiver:Landroid/content/BroadcastReceiver;

    .line 83
    return-void
.end method

.method private getGnssHal()V
    .locals 4

    .line 236
    :try_start_0
    const-string v0, "com.android.server.location.gnss.hal.GnssNative$GnssHal"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 237
    .local v0, "innerClazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v1, 0x0

    new-array v2, v1, [Ljava/lang/Class;

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    new-array v3, v1, [Ljava/lang/Object;

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mGnssHal:Ljava/lang/Object;

    .line 238
    const-string/jumbo v2, "start"

    new-array v3, v1, [Ljava/lang/Class;

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->startMethod:Ljava/lang/reflect/Method;

    .line 239
    const-string/jumbo v2, "stop"

    new-array v1, v1, [Ljava/lang/Class;

    invoke-virtual {v0, v2, v1}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->stopMethod:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 242
    .end local v0    # "innerClazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    goto :goto_0

    .line 240
    :catch_0
    move-exception v0

    .line 241
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 243
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public static getInstance()Lcom/android/server/location/gnss/hal/GpoUtil;
    .locals 2

    .line 73
    sget-object v0, Lcom/android/server/location/gnss/hal/GpoUtil;->instance:Lcom/android/server/location/gnss/hal/GpoUtil;

    if-nez v0, :cond_1

    .line 74
    const-class v0, Lcom/android/server/location/gnss/hal/GpoUtil;

    monitor-enter v0

    .line 75
    :try_start_0
    sget-object v1, Lcom/android/server/location/gnss/hal/GpoUtil;->instance:Lcom/android/server/location/gnss/hal/GpoUtil;

    if-nez v1, :cond_0

    .line 76
    new-instance v1, Lcom/android/server/location/gnss/hal/GpoUtil;

    invoke-direct {v1}, Lcom/android/server/location/gnss/hal/GpoUtil;-><init>()V

    sput-object v1, Lcom/android/server/location/gnss/hal/GpoUtil;->instance:Lcom/android/server/location/gnss/hal/GpoUtil;

    .line 78
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 80
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/location/gnss/hal/GpoUtil;->instance:Lcom/android/server/location/gnss/hal/GpoUtil;

    return-object v0
.end method

.method private registerBootCompletedRecv()V
    .locals 3

    .line 157
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 158
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 159
    iget-object v1, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mUserUnlockReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 160
    return-void
.end method

.method private registerNetworkListener()V
    .locals 2

    .line 101
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mConnectivityManager:Landroid/net/ConnectivityManager;

    new-instance v1, Lcom/android/server/location/gnss/hal/GpoUtil$1;

    invoke-direct {v1, p0}, Lcom/android/server/location/gnss/hal/GpoUtil$1;-><init>(Lcom/android/server/location/gnss/hal/GpoUtil;)V

    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->registerDefaultNetworkCallback(Landroid/net/ConnectivityManager$NetworkCallback;)V

    .line 124
    return-void
.end method

.method private registerScreenUnlockRecv()V
    .locals 3

    .line 170
    const-string v0, "GpoUtil"

    const-string v1, "registerScreenUnlockRecv"

    invoke-virtual {p0, v0, v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 172
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 173
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 174
    iget-object v1, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mScreenUnlockReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 175
    return-void
.end method


# virtual methods
.method public checkHeavyUser()Z
    .locals 1

    .line 231
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->isHeavyUser:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public convertNlp2Glp(Landroid/location/Location;)Landroid/location/Location;
    .locals 3
    .param p1, "nLocation"    # Landroid/location/Location;

    .line 292
    new-instance v0, Landroid/location/Location;

    const-string v1, "gps"

    invoke-direct {v0, v1}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 293
    .local v0, "gLocation":Landroid/location/Location;
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setLatitude(D)V

    .line 294
    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setLongitude(D)V

    .line 295
    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/location/Location;->setAccuracy(F)V

    .line 296
    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setAltitude(D)V

    .line 297
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/location/Location;->setSpeed(F)V

    .line 298
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setTime(J)V

    .line 299
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setElapsedRealtimeNanos(J)V

    .line 300
    return-object v0
.end method

.method public doStartEngineByInstance()Z
    .locals 6

    .line 261
    const-string v0, "GpoUtil"

    iget-object v1, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mGnssEngineStatus:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-ne v1, v2, :cond_0

    return v3

    .line 262
    :cond_0
    iget-object v1, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mGnssEngineStatus:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    const/4 v2, 0x4

    const/4 v4, 0x0

    if-ne v1, v2, :cond_1

    return v4

    .line 264
    :cond_1
    :try_start_0
    const-string v1, "doStartEngineByInstance()"

    invoke-virtual {p0, v0, v1, v3}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 265
    iget-object v1, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->startMethod:Ljava/lang/reflect/Method;

    iget-object v2, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mGnssHal:Ljava/lang/Object;

    new-array v5, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 269
    nop

    .line 270
    const-string v1, "mGnssHal.start()"

    invoke-virtual {p0, v0, v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    return v3

    .line 266
    :catch_0
    move-exception v1

    .line 267
    .local v1, "e":Ljava/lang/Exception;
    const-string/jumbo v2, "start engine failed."

    invoke-virtual {p0, v0, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->loge(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    return v4
.end method

.method public doStopEngineByInstance()V
    .locals 5

    .line 276
    const-string v0, "GpoUtil"

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mGnssEngineStoppedByInstance:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 277
    const-string v2, "doStopEngineByInstance()"

    invoke-virtual {p0, v0, v2, v3}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 278
    iget-object v2, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->stopMethod:Ljava/lang/reflect/Method;

    iget-object v3, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mGnssHal:Ljava/lang/Object;

    new-array v4, v1, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 281
    goto :goto_0

    .line 279
    :catch_0
    move-exception v2

    .line 280
    .local v2, "e":Ljava/lang/Exception;
    const-string/jumbo v3, "stop engine failed."

    invoke-virtual {p0, v0, v3}, Lcom/android/server/location/gnss/hal/GpoUtil;->loge(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_0
    iget-object v2, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mGnssEngineStoppedByInstance:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 284
    const-string v1, "mGnssHal.stop()"

    invoke-virtual {p0, v0, v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    return-void
.end method

.method public engineStoppedByGpo()Z
    .locals 1

    .line 288
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mGnssEngineStoppedByInstance:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public getCurUserBehav()I
    .locals 2

    .line 308
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Current user behav is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mUserBehavReport:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GpoUtil"

    invoke-virtual {p0, v1, v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    iget v0, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mUserBehavReport:I

    return v0
.end method

.method public getDefaultNetwork()I
    .locals 5

    .line 127
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 129
    .local v0, "networkInfo":Landroid/net/NetworkInfo;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 130
    :cond_0
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    .local v1, "curNetwork":I
    goto :goto_1

    .line 129
    .end local v1    # "curNetwork":I
    :cond_1
    :goto_0
    const/4 v1, -0x1

    .line 131
    .restart local v1    # "curNetwork":I
    :goto_1
    iget-object v2, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mDefaultNetwork:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    if-eq v2, v1, :cond_2

    .line 132
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Default Network: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    const-string v4, "GpoUtil"

    invoke-virtual {p0, v4, v2, v3}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 133
    iget-object v2, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mAdapter:Lcom/android/server/location/gnss/hal/GpoUtil$DataEventAdapter;

    invoke-virtual {v2, v1}, Lcom/android/server/location/gnss/hal/GpoUtil$DataEventAdapter;->notifyDefaultNetwork(I)V

    .line 134
    iget-object v2, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mDefaultNetwork:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 136
    :cond_2
    iget-object v2, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mDefaultNetwork:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    return v2
.end method

.method public getEngineStatus()I
    .locals 1

    .line 182
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mGnssEngineStatus:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    return v0
.end method

.method public getGpoVersion()I
    .locals 1

    .line 178
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mGpoVersion:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    return v0
.end method

.method public initOnce(Landroid/content/Context;Lcom/android/server/location/gnss/hal/GpoUtil$IDataEvent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "iDataEvent"    # Lcom/android/server/location/gnss/hal/GpoUtil$IDataEvent;

    .line 86
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 87
    iput-object p1, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mContext:Landroid/content/Context;

    .line 88
    const-class v0, Landroid/app/AppOpsManager;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AppOpsManager;

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mAppOps:Landroid/app/AppOpsManager;

    .line 89
    invoke-direct {p0}, Lcom/android/server/location/gnss/hal/GpoUtil;->getGnssHal()V

    .line 90
    invoke-direct {p0}, Lcom/android/server/location/gnss/hal/GpoUtil;->registerBootCompletedRecv()V

    .line 91
    invoke-direct {p0}, Lcom/android/server/location/gnss/hal/GpoUtil;->registerScreenUnlockRecv()V

    .line 92
    new-instance v0, Lcom/android/server/location/gnss/hal/GpoUtil$DataEventAdapter;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/server/location/gnss/hal/GpoUtil$DataEventAdapter;-><init>(Lcom/android/server/location/gnss/hal/GpoUtil;Lcom/android/server/location/gnss/hal/GpoUtil$DataEventAdapter-IA;)V

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mAdapter:Lcom/android/server/location/gnss/hal/GpoUtil$DataEventAdapter;

    .line 93
    invoke-virtual {v0, p2}, Lcom/android/server/location/gnss/hal/GpoUtil$DataEventAdapter;->attachEventListener(Lcom/android/server/location/gnss/hal/GpoUtil$IDataEvent;)V

    .line 94
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mContext:Landroid/content/Context;

    .line 95
    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 94
    invoke-static {v0}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mConnectivityManager:Landroid/net/ConnectivityManager;

    .line 96
    invoke-direct {p0}, Lcom/android/server/location/gnss/hal/GpoUtil;->registerNetworkListener()V

    .line 98
    :cond_0
    return-void
.end method

.method public isNetworkConnected()Z
    .locals 2

    .line 144
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mDefaultNetwork:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isWifiEnabled()Z
    .locals 2

    .line 140
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mDefaultNetwork:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public logEn(Ljava/lang/String;)V
    .locals 2
    .param p1, "info"    # Ljava/lang/String;

    .line 330
    invoke-static {}, Lcom/android/server/location/LocationDumpLogStub;->getInstance()Lcom/android/server/location/LocationDumpLogStub;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {v0, v1, p1}, Lcom/android/server/location/LocationDumpLogStub;->addToBugreport(ILjava/lang/String;)V

    .line 332
    return-void
.end method

.method public loge(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "info"    # Ljava/lang/String;

    .line 319
    invoke-static {p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    return-void
.end method

.method public logi(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "info"    # Ljava/lang/String;
    .param p3, "write2File"    # Z

    .line 323
    invoke-static {p1, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    if-eqz p3, :cond_0

    .line 325
    invoke-static {}, Lcom/android/server/location/gnss/GnssLocationProviderStub;->getInstance()Lcom/android/server/location/gnss/GnssLocationProviderStub;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/android/server/location/gnss/GnssLocationProviderStub;->writeLocationInformation(Ljava/lang/String;)V

    .line 327
    :cond_0
    return-void
.end method

.method public logv(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "info"    # Ljava/lang/String;

    .line 313
    sget-boolean v0, Lcom/android/server/location/gnss/hal/GpoUtil;->VERBOSE:Z

    if-eqz v0, :cond_0

    .line 314
    invoke-static {p1, p2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    :cond_0
    return-void
.end method

.method public readEngineUsageData()V
    .locals 8

    .line 214
    iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->isUserUnlocked:Z

    if-nez v0, :cond_0

    return-void

    .line 215
    :cond_0
    const-string v0, "readEngineUsageData"

    const-string v1, "GpoUtil"

    invoke-virtual {p0, v1, v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mSpFileGnssEngineUsageTime:Ljava/io/File;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/io/File;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 217
    .local v0, "pref":Landroid/content/SharedPreferences;
    const/4 v2, 0x0

    .line 218
    .local v2, "sum":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    const/4 v5, 0x5

    if-ge v4, v5, :cond_1

    .line 219
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "index"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v5, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    .line 220
    .local v5, "j":I
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v1, v6}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    add-int/2addr v2, v5

    .line 218
    .end local v5    # "j":I
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 223
    .end local v4    # "i":I
    :cond_1
    const/4 v4, 0x3

    const/4 v5, 0x1

    if-lt v2, v4, :cond_2

    move v3, v5

    .line 224
    .local v3, "newValue":Z
    :cond_2
    iget-object v4, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->isHeavyUser:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v4

    if-eq v4, v3, :cond_3

    .line 225
    iget-object v4, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->isHeavyUser:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v4, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 227
    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "is heavy user ? "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v6, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->isHeavyUser:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v1, v4, v5}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 228
    return-void
.end method

.method public recordEngineUsageDaily(J)V
    .locals 7
    .param p1, "time"    # J

    .line 201
    iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->isUserUnlocked:Z

    if-nez v0, :cond_0

    return-void

    .line 202
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "recordEngineUsageDaily: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GpoUtil"

    invoke-virtual {p0, v1, v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mSpFileGnssEngineUsageTime:Ljava/io/File;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/io/File;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 204
    .local v0, "perf":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 205
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    const/4 v3, 0x4

    .local v3, "i":I
    :goto_0
    if-lez v3, :cond_1

    .line 206
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "index"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    add-int/lit8 v6, v3, -0x1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v5, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 205
    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    .line 208
    .end local v3    # "i":I
    :cond_1
    const-wide/32 v3, 0x36ee80

    cmp-long v3, p1, v3

    if-ltz v3, :cond_2

    const/4 v2, 0x1

    :cond_2
    const-string v3, "index0"

    invoke-interface {v1, v3, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 209
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 210
    invoke-virtual {p0}, Lcom/android/server/location/gnss/hal/GpoUtil;->readEngineUsageData()V

    .line 211
    return-void
.end method

.method public setEngineStatus(I)V
    .locals 3
    .param p1, "status"    # I

    .line 186
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mGnssEngineStatus:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 187
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "gnss engine status: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mGnssEngineStatus:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    const-string v2, "GpoUtil"

    invoke-virtual {p0, v2, v0, v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 188
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mAdapter:Lcom/android/server/location/gnss/hal/GpoUtil$DataEventAdapter;

    invoke-virtual {v0, p1}, Lcom/android/server/location/gnss/hal/GpoUtil$DataEventAdapter;->notifyGnssStatus(I)V

    .line 189
    return-void
.end method

.method public setGpoVersionValue(I)V
    .locals 3
    .param p1, "newValue"    # I

    .line 192
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mGpoVersion:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-eq v0, p1, :cond_0

    .line 193
    const-string v0, "persist.sys.gpo.version"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setGpoVersionValue: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    const-string v2, "GpoUtil"

    invoke-virtual {p0, v2, v0, v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 195
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mGpoVersion:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 196
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mAdapter:Lcom/android/server/location/gnss/hal/GpoUtil$DataEventAdapter;

    iget-object v1, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mGpoVersion:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/server/location/gnss/hal/GpoUtil$DataEventAdapter;->notifyPersistUpdates(I)V

    .line 198
    :cond_0
    return-void
.end method

.method public setUserBehav(I)V
    .locals 0
    .param p1, "event"    # I

    .line 304
    iput p1, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mUserBehavReport:I

    .line 305
    return-void
.end method

.method public updateLocationIcon(ZILjava/lang/String;)V
    .locals 3
    .param p1, "displayLocationIcon"    # Z
    .param p2, "uid"    # I
    .param p3, "pkg"    # Ljava/lang/String;

    .line 246
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mAppOps:Landroid/app/AppOpsManager;

    if-nez v0, :cond_0

    return-void

    .line 247
    :cond_0
    const/16 v1, 0x2a

    if-eqz p1, :cond_1

    .line 248
    invoke-virtual {v0, v1, p2, p3}, Landroid/app/AppOpsManager;->startOpNoThrow(IILjava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_2

    .line 250
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mAppOps:Landroid/app/AppOpsManager;

    invoke-virtual {v0, v1, p2, p3}, Landroid/app/AppOpsManager;->finishOp(IILjava/lang/String;)V

    goto :goto_0

    .line 253
    :cond_1
    invoke-virtual {v0, v1, p2, p3}, Landroid/app/AppOpsManager;->finishOp(IILjava/lang/String;)V

    .line 255
    :cond_2
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "update Location Icon to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " for ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    const-string v2, "GpoUtil"

    invoke-virtual {p0, v2, v0, v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 257
    return-void
.end method
