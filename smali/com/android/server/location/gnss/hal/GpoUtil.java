public class com.android.server.location.gnss.hal.GpoUtil {
	 /* .source "GpoUtil.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/location/gnss/hal/GpoUtil$DataEventAdapter;, */
	 /* Lcom/android/server/location/gnss/hal/GpoUtil$IDataEvent; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Long MAX_ENGINE_TIME;
private static final Integer MAX_RECORD_ENGINE_SIZE;
private static final java.lang.String PERSIST_FOR_GPO_VERSION;
private static final java.lang.String SP_INDEX;
private static final java.lang.String TAG;
private static final Boolean VERBOSE;
private static volatile com.android.server.location.gnss.hal.GpoUtil instance;
/* # instance fields */
private final java.util.concurrent.atomic.AtomicBoolean isHeavyUser;
private Boolean isUserUnlocked;
private com.android.server.location.gnss.hal.GpoUtil$DataEventAdapter mAdapter;
private android.app.AppOpsManager mAppOps;
private android.net.ConnectivityManager mConnectivityManager;
private android.content.Context mContext;
private final java.util.concurrent.atomic.AtomicInteger mDefaultNetwork;
private final java.util.concurrent.atomic.AtomicInteger mGnssEngineStatus;
private final java.util.concurrent.atomic.AtomicBoolean mGnssEngineStoppedByInstance;
private java.lang.Object mGnssHal;
private final java.util.concurrent.atomic.AtomicInteger mGpoVersion;
private final android.content.BroadcastReceiver mScreenUnlockReceiver;
private final java.io.File mSpFileGnssEngineUsageTime;
private Integer mUserBehavReport;
private final android.content.BroadcastReceiver mUserUnlockReceiver;
private java.lang.reflect.Method startMethod;
private java.lang.reflect.Method stopMethod;
/* # direct methods */
static com.android.server.location.gnss.hal.GpoUtil$DataEventAdapter -$$Nest$fgetmAdapter ( com.android.server.location.gnss.hal.GpoUtil p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mAdapter;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.android.server.location.gnss.hal.GpoUtil p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mContext;
} // .end method
static android.content.BroadcastReceiver -$$Nest$fgetmUserUnlockReceiver ( com.android.server.location.gnss.hal.GpoUtil p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mUserUnlockReceiver;
} // .end method
static void -$$Nest$fputisUserUnlocked ( com.android.server.location.gnss.hal.GpoUtil p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->isUserUnlocked:Z */
	 return;
} // .end method
static com.android.server.location.gnss.hal.GpoUtil ( ) {
	 /* .locals 2 */
	 /* .line 34 */
	 final String v0 = "GpoUtil"; // const-string v0, "GpoUtil"
	 int v1 = 2; // const/4 v1, 0x2
	 v0 = 	 android.util.Log .isLoggable ( v0,v1 );
	 com.android.server.location.gnss.hal.GpoUtil.VERBOSE = (v0!= 0);
	 return;
} // .end method
private com.android.server.location.gnss.hal.GpoUtil ( ) {
	 /* .locals 5 */
	 /* .line 83 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 47 */
	 /* new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger; */
	 int v1 = 4; // const/4 v1, 0x4
	 /* invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V */
	 this.mGnssEngineStatus = v0;
	 /* .line 49 */
	 /* new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger; */
	 int v1 = -1; // const/4 v1, -0x1
	 /* invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V */
	 this.mDefaultNetwork = v0;
	 /* .line 51 */
	 /* new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger; */
	 /* .line 52 */
	 final String v1 = "persist.sys.gpo.version"; // const-string v1, "persist.sys.gpo.version"
	 int v2 = 0; // const/4 v2, 0x0
	 v1 = 	 android.os.SystemProperties .getInt ( v1,v2 );
	 /* invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V */
	 this.mGpoVersion = v0;
	 /* .line 54 */
	 /* new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean; */
	 int v1 = 1; // const/4 v1, 0x1
	 /* invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V */
	 this.isHeavyUser = v0;
	 /* .line 55 */
	 /* new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean; */
	 /* invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V */
	 this.mGnssEngineStoppedByInstance = v0;
	 /* .line 57 */
	 /* new-instance v0, Ljava/io/File; */
	 /* new-instance v1, Ljava/io/File; */
	 /* .line 58 */
	 android.os.Environment .getDataDirectory ( );
	 /* const-string/jumbo v4, "system" */
	 /* invoke-direct {v1, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
	 final String v3 = "GnssEngineUsageTime.xml"; // const-string v3, "GnssEngineUsageTime.xml"
	 /* invoke-direct {v0, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
	 this.mSpFileGnssEngineUsageTime = v0;
	 /* .line 67 */
	 /* iput-boolean v2, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->isUserUnlocked:Z */
	 /* .line 68 */
	 /* iput v2, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mUserBehavReport:I */
	 /* .line 147 */
	 /* new-instance v0, Lcom/android/server/location/gnss/hal/GpoUtil$2; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/location/gnss/hal/GpoUtil$2;-><init>(Lcom/android/server/location/gnss/hal/GpoUtil;)V */
	 this.mUserUnlockReceiver = v0;
	 /* .line 162 */
	 /* new-instance v0, Lcom/android/server/location/gnss/hal/GpoUtil$3; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/location/gnss/hal/GpoUtil$3;-><init>(Lcom/android/server/location/gnss/hal/GpoUtil;)V */
	 this.mScreenUnlockReceiver = v0;
	 /* .line 83 */
	 return;
} // .end method
private void getGnssHal ( ) {
	 /* .locals 4 */
	 /* .line 236 */
	 try { // :try_start_0
		 final String v0 = "com.android.server.location.gnss.hal.GnssNative$GnssHal"; // const-string v0, "com.android.server.location.gnss.hal.GnssNative$GnssHal"
		 java.lang.Class .forName ( v0 );
		 /* .line 237 */
		 /* .local v0, "innerClazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
		 int v1 = 0; // const/4 v1, 0x0
		 /* new-array v2, v1, [Ljava/lang/Class; */
		 (( java.lang.Class ) v0 ).getDeclaredConstructor ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
		 /* new-array v3, v1, [Ljava/lang/Object; */
		 (( java.lang.reflect.Constructor ) v2 ).newInstance ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;
		 this.mGnssHal = v2;
		 /* .line 238 */
		 /* const-string/jumbo v2, "start" */
		 /* new-array v3, v1, [Ljava/lang/Class; */
		 (( java.lang.Class ) v0 ).getDeclaredMethod ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
		 this.startMethod = v2;
		 /* .line 239 */
		 /* const-string/jumbo v2, "stop" */
		 /* new-array v1, v1, [Ljava/lang/Class; */
		 (( java.lang.Class ) v0 ).getDeclaredMethod ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
		 this.stopMethod = v1;
		 /* :try_end_0 */
		 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 242 */
	 } // .end local v0 # "innerClazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
	 /* .line 240 */
	 /* :catch_0 */
	 /* move-exception v0 */
	 /* .line 241 */
	 /* .local v0, "e":Ljava/lang/Exception; */
	 (( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
	 /* .line 243 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public static com.android.server.location.gnss.hal.GpoUtil getInstance ( ) {
/* .locals 2 */
/* .line 73 */
v0 = com.android.server.location.gnss.hal.GpoUtil.instance;
/* if-nez v0, :cond_1 */
/* .line 74 */
/* const-class v0, Lcom/android/server/location/gnss/hal/GpoUtil; */
/* monitor-enter v0 */
/* .line 75 */
try { // :try_start_0
v1 = com.android.server.location.gnss.hal.GpoUtil.instance;
/* if-nez v1, :cond_0 */
/* .line 76 */
/* new-instance v1, Lcom/android/server/location/gnss/hal/GpoUtil; */
/* invoke-direct {v1}, Lcom/android/server/location/gnss/hal/GpoUtil;-><init>()V */
/* .line 78 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 80 */
} // :cond_1
} // :goto_0
v0 = com.android.server.location.gnss.hal.GpoUtil.instance;
} // .end method
private void registerBootCompletedRecv ( ) {
/* .locals 3 */
/* .line 157 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 158 */
/* .local v0, "filter":Landroid/content/IntentFilter; */
final String v1 = "android.intent.action.BOOT_COMPLETED"; // const-string v1, "android.intent.action.BOOT_COMPLETED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 159 */
v1 = this.mContext;
v2 = this.mUserUnlockReceiver;
(( android.content.Context ) v1 ).registerReceiver ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 160 */
return;
} // .end method
private void registerNetworkListener ( ) {
/* .locals 2 */
/* .line 101 */
v0 = this.mConnectivityManager;
/* new-instance v1, Lcom/android/server/location/gnss/hal/GpoUtil$1; */
/* invoke-direct {v1, p0}, Lcom/android/server/location/gnss/hal/GpoUtil$1;-><init>(Lcom/android/server/location/gnss/hal/GpoUtil;)V */
(( android.net.ConnectivityManager ) v0 ).registerDefaultNetworkCallback ( v1 ); // invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->registerDefaultNetworkCallback(Landroid/net/ConnectivityManager$NetworkCallback;)V
/* .line 124 */
return;
} // .end method
private void registerScreenUnlockRecv ( ) {
/* .locals 3 */
/* .line 170 */
final String v0 = "GpoUtil"; // const-string v0, "GpoUtil"
final String v1 = "registerScreenUnlockRecv"; // const-string v1, "registerScreenUnlockRecv"
(( com.android.server.location.gnss.hal.GpoUtil ) p0 ).logv ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 171 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 172 */
/* .local v0, "filter":Landroid/content/IntentFilter; */
final String v1 = "android.intent.action.SCREEN_ON"; // const-string v1, "android.intent.action.SCREEN_ON"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 173 */
final String v1 = "android.intent.action.SCREEN_OFF"; // const-string v1, "android.intent.action.SCREEN_OFF"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 174 */
v1 = this.mContext;
v2 = this.mScreenUnlockReceiver;
(( android.content.Context ) v1 ).registerReceiver ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 175 */
return;
} // .end method
/* # virtual methods */
public Boolean checkHeavyUser ( ) {
/* .locals 1 */
/* .line 231 */
v0 = this.isHeavyUser;
v0 = (( java.util.concurrent.atomic.AtomicBoolean ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
} // .end method
public android.location.Location convertNlp2Glp ( android.location.Location p0 ) {
/* .locals 3 */
/* .param p1, "nLocation" # Landroid/location/Location; */
/* .line 292 */
/* new-instance v0, Landroid/location/Location; */
final String v1 = "gps"; // const-string v1, "gps"
/* invoke-direct {v0, v1}, Landroid/location/Location;-><init>(Ljava/lang/String;)V */
/* .line 293 */
/* .local v0, "gLocation":Landroid/location/Location; */
(( android.location.Location ) p1 ).getLatitude ( ); // invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D
/* move-result-wide v1 */
(( android.location.Location ) v0 ).setLatitude ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setLatitude(D)V
/* .line 294 */
(( android.location.Location ) p1 ).getLongitude ( ); // invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D
/* move-result-wide v1 */
(( android.location.Location ) v0 ).setLongitude ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setLongitude(D)V
/* .line 295 */
v1 = (( android.location.Location ) p1 ).getAccuracy ( ); // invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F
(( android.location.Location ) v0 ).setAccuracy ( v1 ); // invoke-virtual {v0, v1}, Landroid/location/Location;->setAccuracy(F)V
/* .line 296 */
/* const-wide/16 v1, 0x0 */
(( android.location.Location ) v0 ).setAltitude ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setAltitude(D)V
/* .line 297 */
int v1 = 0; // const/4 v1, 0x0
(( android.location.Location ) v0 ).setSpeed ( v1 ); // invoke-virtual {v0, v1}, Landroid/location/Location;->setSpeed(F)V
/* .line 298 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v1 */
(( android.location.Location ) v0 ).setTime ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setTime(J)V
/* .line 299 */
android.os.SystemClock .elapsedRealtimeNanos ( );
/* move-result-wide v1 */
(( android.location.Location ) v0 ).setElapsedRealtimeNanos ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setElapsedRealtimeNanos(J)V
/* .line 300 */
} // .end method
public Boolean doStartEngineByInstance ( ) {
/* .locals 6 */
/* .line 261 */
final String v0 = "GpoUtil"; // const-string v0, "GpoUtil"
v1 = this.mGnssEngineStatus;
v1 = (( java.util.concurrent.atomic.AtomicInteger ) v1 ).get ( ); // invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I
int v2 = 2; // const/4 v2, 0x2
int v3 = 1; // const/4 v3, 0x1
/* if-ne v1, v2, :cond_0 */
/* .line 262 */
} // :cond_0
v1 = this.mGnssEngineStatus;
v1 = (( java.util.concurrent.atomic.AtomicInteger ) v1 ).get ( ); // invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I
int v2 = 4; // const/4 v2, 0x4
int v4 = 0; // const/4 v4, 0x0
/* if-ne v1, v2, :cond_1 */
/* .line 264 */
} // :cond_1
try { // :try_start_0
final String v1 = "doStartEngineByInstance()"; // const-string v1, "doStartEngineByInstance()"
(( com.android.server.location.gnss.hal.GpoUtil ) p0 ).logi ( v0, v1, v3 ); // invoke-virtual {p0, v0, v1, v3}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V
/* .line 265 */
v1 = this.startMethod;
v2 = this.mGnssHal;
/* new-array v5, v4, [Ljava/lang/Object; */
(( java.lang.reflect.Method ) v1 ).invoke ( v2, v5 ); // invoke-virtual {v1, v2, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 269 */
/* nop */
/* .line 270 */
final String v1 = "mGnssHal.start()"; // const-string v1, "mGnssHal.start()"
(( com.android.server.location.gnss.hal.GpoUtil ) p0 ).logv ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 271 */
/* .line 266 */
/* :catch_0 */
/* move-exception v1 */
/* .line 267 */
/* .local v1, "e":Ljava/lang/Exception; */
/* const-string/jumbo v2, "start engine failed." */
(( com.android.server.location.gnss.hal.GpoUtil ) p0 ).loge ( v0, v2 ); // invoke-virtual {p0, v0, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->loge(Ljava/lang/String;Ljava/lang/String;)V
/* .line 268 */
} // .end method
public void doStopEngineByInstance ( ) {
/* .locals 5 */
/* .line 276 */
final String v0 = "GpoUtil"; // const-string v0, "GpoUtil"
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
v2 = this.mGnssEngineStoppedByInstance;
int v3 = 1; // const/4 v3, 0x1
(( java.util.concurrent.atomic.AtomicBoolean ) v2 ).set ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
/* .line 277 */
final String v2 = "doStopEngineByInstance()"; // const-string v2, "doStopEngineByInstance()"
(( com.android.server.location.gnss.hal.GpoUtil ) p0 ).logi ( v0, v2, v3 ); // invoke-virtual {p0, v0, v2, v3}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V
/* .line 278 */
v2 = this.stopMethod;
v3 = this.mGnssHal;
/* new-array v4, v1, [Ljava/lang/Object; */
(( java.lang.reflect.Method ) v2 ).invoke ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 281 */
/* .line 279 */
/* :catch_0 */
/* move-exception v2 */
/* .line 280 */
/* .local v2, "e":Ljava/lang/Exception; */
/* const-string/jumbo v3, "stop engine failed." */
(( com.android.server.location.gnss.hal.GpoUtil ) p0 ).loge ( v0, v3 ); // invoke-virtual {p0, v0, v3}, Lcom/android/server/location/gnss/hal/GpoUtil;->loge(Ljava/lang/String;Ljava/lang/String;)V
/* .line 283 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_0
v2 = this.mGnssEngineStoppedByInstance;
(( java.util.concurrent.atomic.AtomicBoolean ) v2 ).set ( v1 ); // invoke-virtual {v2, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
/* .line 284 */
final String v1 = "mGnssHal.stop()"; // const-string v1, "mGnssHal.stop()"
(( com.android.server.location.gnss.hal.GpoUtil ) p0 ).logv ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 285 */
return;
} // .end method
public Boolean engineStoppedByGpo ( ) {
/* .locals 1 */
/* .line 288 */
v0 = this.mGnssEngineStoppedByInstance;
v0 = (( java.util.concurrent.atomic.AtomicBoolean ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
} // .end method
public Integer getCurUserBehav ( ) {
/* .locals 2 */
/* .line 308 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Current user behav is "; // const-string v1, "Current user behav is "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mUserBehavReport:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "GpoUtil"; // const-string v1, "GpoUtil"
(( com.android.server.location.gnss.hal.GpoUtil ) p0 ).logv ( v1, v0 ); // invoke-virtual {p0, v1, v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 309 */
/* iget v0, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mUserBehavReport:I */
} // .end method
public Integer getDefaultNetwork ( ) {
/* .locals 5 */
/* .line 127 */
v0 = this.mConnectivityManager;
(( android.net.ConnectivityManager ) v0 ).getActiveNetworkInfo ( ); // invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;
/* .line 129 */
/* .local v0, "networkInfo":Landroid/net/NetworkInfo; */
if ( v0 != null) { // if-eqz v0, :cond_1
v1 = (( android.net.NetworkInfo ) v0 ).isConnected ( ); // invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z
/* if-nez v1, :cond_0 */
/* .line 130 */
} // :cond_0
v1 = (( android.net.NetworkInfo ) v0 ).getType ( ); // invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I
/* .local v1, "curNetwork":I */
/* .line 129 */
} // .end local v1 # "curNetwork":I
} // :cond_1
} // :goto_0
int v1 = -1; // const/4 v1, -0x1
/* .line 131 */
/* .restart local v1 # "curNetwork":I */
} // :goto_1
v2 = this.mDefaultNetwork;
v2 = (( java.util.concurrent.atomic.AtomicInteger ) v2 ).get ( ); // invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I
/* if-eq v2, v1, :cond_2 */
/* .line 132 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Default Network: "; // const-string v3, "Default Network: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v3 = 1; // const/4 v3, 0x1
final String v4 = "GpoUtil"; // const-string v4, "GpoUtil"
(( com.android.server.location.gnss.hal.GpoUtil ) p0 ).logi ( v4, v2, v3 ); // invoke-virtual {p0, v4, v2, v3}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V
/* .line 133 */
v2 = this.mAdapter;
(( com.android.server.location.gnss.hal.GpoUtil$DataEventAdapter ) v2 ).notifyDefaultNetwork ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/location/gnss/hal/GpoUtil$DataEventAdapter;->notifyDefaultNetwork(I)V
/* .line 134 */
v2 = this.mDefaultNetwork;
(( java.util.concurrent.atomic.AtomicInteger ) v2 ).set ( v1 ); // invoke-virtual {v2, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V
/* .line 136 */
} // :cond_2
v2 = this.mDefaultNetwork;
v2 = (( java.util.concurrent.atomic.AtomicInteger ) v2 ).get ( ); // invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I
} // .end method
public Integer getEngineStatus ( ) {
/* .locals 1 */
/* .line 182 */
v0 = this.mGnssEngineStatus;
v0 = (( java.util.concurrent.atomic.AtomicInteger ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I
} // .end method
public Integer getGpoVersion ( ) {
/* .locals 1 */
/* .line 178 */
v0 = this.mGpoVersion;
v0 = (( java.util.concurrent.atomic.AtomicInteger ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I
} // .end method
public void initOnce ( android.content.Context p0, com.android.server.location.gnss.hal.GpoUtil$IDataEvent p1 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "iDataEvent" # Lcom/android/server/location/gnss/hal/GpoUtil$IDataEvent; */
/* .line 86 */
v0 = this.mContext;
/* if-nez v0, :cond_0 */
/* .line 87 */
this.mContext = p1;
/* .line 88 */
/* const-class v0, Landroid/app/AppOpsManager; */
(( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/AppOpsManager; */
this.mAppOps = v0;
/* .line 89 */
/* invoke-direct {p0}, Lcom/android/server/location/gnss/hal/GpoUtil;->getGnssHal()V */
/* .line 90 */
/* invoke-direct {p0}, Lcom/android/server/location/gnss/hal/GpoUtil;->registerBootCompletedRecv()V */
/* .line 91 */
/* invoke-direct {p0}, Lcom/android/server/location/gnss/hal/GpoUtil;->registerScreenUnlockRecv()V */
/* .line 92 */
/* new-instance v0, Lcom/android/server/location/gnss/hal/GpoUtil$DataEventAdapter; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, p0, v1}, Lcom/android/server/location/gnss/hal/GpoUtil$DataEventAdapter;-><init>(Lcom/android/server/location/gnss/hal/GpoUtil;Lcom/android/server/location/gnss/hal/GpoUtil$DataEventAdapter-IA;)V */
this.mAdapter = v0;
/* .line 93 */
(( com.android.server.location.gnss.hal.GpoUtil$DataEventAdapter ) v0 ).attachEventListener ( p2 ); // invoke-virtual {v0, p2}, Lcom/android/server/location/gnss/hal/GpoUtil$DataEventAdapter;->attachEventListener(Lcom/android/server/location/gnss/hal/GpoUtil$IDataEvent;)V
/* .line 94 */
v0 = this.mContext;
/* .line 95 */
final String v1 = "connectivity"; // const-string v1, "connectivity"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/net/ConnectivityManager; */
/* .line 94 */
com.android.internal.util.Preconditions .checkNotNull ( v0 );
/* check-cast v0, Landroid/net/ConnectivityManager; */
this.mConnectivityManager = v0;
/* .line 96 */
/* invoke-direct {p0}, Lcom/android/server/location/gnss/hal/GpoUtil;->registerNetworkListener()V */
/* .line 98 */
} // :cond_0
return;
} // .end method
public Boolean isNetworkConnected ( ) {
/* .locals 2 */
/* .line 144 */
v0 = this.mDefaultNetwork;
v0 = (( java.util.concurrent.atomic.AtomicInteger ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I
int v1 = -1; // const/4 v1, -0x1
/* if-eq v0, v1, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean isWifiEnabled ( ) {
/* .locals 2 */
/* .line 140 */
v0 = this.mDefaultNetwork;
v0 = (( java.util.concurrent.atomic.AtomicInteger ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
} // .end method
public void logEn ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "info" # Ljava/lang/String; */
/* .line 330 */
com.android.server.location.LocationDumpLogStub .getInstance ( );
int v1 = 3; // const/4 v1, 0x3
/* .line 332 */
return;
} // .end method
public void loge ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 0 */
/* .param p1, "tag" # Ljava/lang/String; */
/* .param p2, "info" # Ljava/lang/String; */
/* .line 319 */
android.util.Log .e ( p1,p2 );
/* .line 320 */
return;
} // .end method
public void logi ( java.lang.String p0, java.lang.String p1, Boolean p2 ) {
/* .locals 1 */
/* .param p1, "tag" # Ljava/lang/String; */
/* .param p2, "info" # Ljava/lang/String; */
/* .param p3, "write2File" # Z */
/* .line 323 */
android.util.Log .i ( p1,p2 );
/* .line 324 */
if ( p3 != null) { // if-eqz p3, :cond_0
/* .line 325 */
com.android.server.location.gnss.GnssLocationProviderStub .getInstance ( );
/* .line 327 */
} // :cond_0
return;
} // .end method
public void logv ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "tag" # Ljava/lang/String; */
/* .param p2, "info" # Ljava/lang/String; */
/* .line 313 */
/* sget-boolean v0, Lcom/android/server/location/gnss/hal/GpoUtil;->VERBOSE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 314 */
android.util.Log .v ( p1,p2 );
/* .line 316 */
} // :cond_0
return;
} // .end method
public void readEngineUsageData ( ) {
/* .locals 8 */
/* .line 214 */
/* iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->isUserUnlocked:Z */
/* if-nez v0, :cond_0 */
return;
/* .line 215 */
} // :cond_0
final String v0 = "readEngineUsageData"; // const-string v0, "readEngineUsageData"
final String v1 = "GpoUtil"; // const-string v1, "GpoUtil"
(( com.android.server.location.gnss.hal.GpoUtil ) p0 ).logv ( v1, v0 ); // invoke-virtual {p0, v1, v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 216 */
v0 = this.mContext;
v2 = this.mSpFileGnssEngineUsageTime;
int v3 = 0; // const/4 v3, 0x0
(( android.content.Context ) v0 ).getSharedPreferences ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/io/File;I)Landroid/content/SharedPreferences;
/* .line 217 */
/* .local v0, "pref":Landroid/content/SharedPreferences; */
int v2 = 0; // const/4 v2, 0x0
/* .line 218 */
/* .local v2, "sum":I */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
} // :goto_0
int v5 = 5; // const/4 v5, 0x5
/* if-ge v4, v5, :cond_1 */
/* .line 219 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "index"; // const-string v6, "index"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
v5 = (( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 220 */
/* .local v5, "j":I */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v4 ); // invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = " is "; // const-string v7, " is "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.android.server.location.gnss.hal.GpoUtil ) p0 ).logv ( v1, v6 ); // invoke-virtual {p0, v1, v6}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 221 */
/* add-int/2addr v2, v5 */
/* .line 218 */
} // .end local v5 # "j":I
/* add-int/lit8 v4, v4, 0x1 */
/* .line 223 */
} // .end local v4 # "i":I
} // :cond_1
int v4 = 3; // const/4 v4, 0x3
int v5 = 1; // const/4 v5, 0x1
/* if-lt v2, v4, :cond_2 */
/* move v3, v5 */
/* .line 224 */
/* .local v3, "newValue":Z */
} // :cond_2
v4 = this.isHeavyUser;
v4 = (( java.util.concurrent.atomic.AtomicBoolean ) v4 ).get ( ); // invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
/* if-eq v4, v3, :cond_3 */
/* .line 225 */
v4 = this.isHeavyUser;
(( java.util.concurrent.atomic.AtomicBoolean ) v4 ).set ( v3 ); // invoke-virtual {v4, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
/* .line 227 */
} // :cond_3
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "is heavy user ? "; // const-string v6, "is heavy user ? "
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.isHeavyUser;
v6 = (( java.util.concurrent.atomic.AtomicBoolean ) v6 ).get ( ); // invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.android.server.location.gnss.hal.GpoUtil ) p0 ).logi ( v1, v4, v5 ); // invoke-virtual {p0, v1, v4, v5}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V
/* .line 228 */
return;
} // .end method
public void recordEngineUsageDaily ( Long p0 ) {
/* .locals 7 */
/* .param p1, "time" # J */
/* .line 201 */
/* iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->isUserUnlocked:Z */
/* if-nez v0, :cond_0 */
return;
/* .line 202 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "recordEngineUsageDaily: "; // const-string v1, "recordEngineUsageDaily: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "GpoUtil"; // const-string v1, "GpoUtil"
(( com.android.server.location.gnss.hal.GpoUtil ) p0 ).logv ( v1, v0 ); // invoke-virtual {p0, v1, v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 203 */
v0 = this.mContext;
v1 = this.mSpFileGnssEngineUsageTime;
int v2 = 0; // const/4 v2, 0x0
(( android.content.Context ) v0 ).getSharedPreferences ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/io/File;I)Landroid/content/SharedPreferences;
/* .line 204 */
/* .local v0, "perf":Landroid/content/SharedPreferences; */
/* .line 205 */
/* .local v1, "editor":Landroid/content/SharedPreferences$Editor; */
int v3 = 4; // const/4 v3, 0x4
/* .local v3, "i":I */
} // :goto_0
/* if-lez v3, :cond_1 */
/* .line 206 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "index"; // const-string v5, "index"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* add-int/lit8 v6, v3, -0x1 */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
v5 = (( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 205 */
/* add-int/lit8 v3, v3, -0x1 */
/* .line 208 */
} // .end local v3 # "i":I
} // :cond_1
/* const-wide/32 v3, 0x36ee80 */
/* cmp-long v3, p1, v3 */
/* if-ltz v3, :cond_2 */
int v2 = 1; // const/4 v2, 0x1
} // :cond_2
final String v3 = "index0"; // const-string v3, "index0"
/* .line 209 */
/* .line 210 */
(( com.android.server.location.gnss.hal.GpoUtil ) p0 ).readEngineUsageData ( ); // invoke-virtual {p0}, Lcom/android/server/location/gnss/hal/GpoUtil;->readEngineUsageData()V
/* .line 211 */
return;
} // .end method
public void setEngineStatus ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "status" # I */
/* .line 186 */
v0 = this.mGnssEngineStatus;
(( java.util.concurrent.atomic.AtomicInteger ) v0 ).set ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V
/* .line 187 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "gnss engine status: "; // const-string v1, "gnss engine status: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mGnssEngineStatus;
v1 = (( java.util.concurrent.atomic.AtomicInteger ) v1 ).get ( ); // invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v1 = 1; // const/4 v1, 0x1
final String v2 = "GpoUtil"; // const-string v2, "GpoUtil"
(( com.android.server.location.gnss.hal.GpoUtil ) p0 ).logi ( v2, v0, v1 ); // invoke-virtual {p0, v2, v0, v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V
/* .line 188 */
v0 = this.mAdapter;
(( com.android.server.location.gnss.hal.GpoUtil$DataEventAdapter ) v0 ).notifyGnssStatus ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/location/gnss/hal/GpoUtil$DataEventAdapter;->notifyGnssStatus(I)V
/* .line 189 */
return;
} // .end method
public void setGpoVersionValue ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "newValue" # I */
/* .line 192 */
v0 = this.mGpoVersion;
v0 = (( java.util.concurrent.atomic.AtomicInteger ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I
/* if-eq v0, p1, :cond_0 */
/* .line 193 */
final String v0 = "persist.sys.gpo.version"; // const-string v0, "persist.sys.gpo.version"
java.lang.String .valueOf ( p1 );
android.os.SystemProperties .set ( v0,v1 );
/* .line 194 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "setGpoVersionValue: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v1 = 1; // const/4 v1, 0x1
final String v2 = "GpoUtil"; // const-string v2, "GpoUtil"
(( com.android.server.location.gnss.hal.GpoUtil ) p0 ).logi ( v2, v0, v1 ); // invoke-virtual {p0, v2, v0, v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V
/* .line 195 */
v0 = this.mGpoVersion;
(( java.util.concurrent.atomic.AtomicInteger ) v0 ).set ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V
/* .line 196 */
v0 = this.mAdapter;
v1 = this.mGpoVersion;
v1 = (( java.util.concurrent.atomic.AtomicInteger ) v1 ).get ( ); // invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I
(( com.android.server.location.gnss.hal.GpoUtil$DataEventAdapter ) v0 ).notifyPersistUpdates ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/location/gnss/hal/GpoUtil$DataEventAdapter;->notifyPersistUpdates(I)V
/* .line 198 */
} // :cond_0
return;
} // .end method
public void setUserBehav ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "event" # I */
/* .line 304 */
/* iput p1, p0, Lcom/android/server/location/gnss/hal/GpoUtil;->mUserBehavReport:I */
/* .line 305 */
return;
} // .end method
public void updateLocationIcon ( Boolean p0, Integer p1, java.lang.String p2 ) {
/* .locals 3 */
/* .param p1, "displayLocationIcon" # Z */
/* .param p2, "uid" # I */
/* .param p3, "pkg" # Ljava/lang/String; */
/* .line 246 */
v0 = this.mAppOps;
/* if-nez v0, :cond_0 */
return;
/* .line 247 */
} // :cond_0
/* const/16 v1, 0x2a */
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 248 */
v0 = (( android.app.AppOpsManager ) v0 ).startOpNoThrow ( v1, p2, p3 ); // invoke-virtual {v0, v1, p2, p3}, Landroid/app/AppOpsManager;->startOpNoThrow(IILjava/lang/String;)I
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 250 */
v0 = this.mAppOps;
(( android.app.AppOpsManager ) v0 ).finishOp ( v1, p2, p3 ); // invoke-virtual {v0, v1, p2, p3}, Landroid/app/AppOpsManager;->finishOp(IILjava/lang/String;)V
/* .line 253 */
} // :cond_1
(( android.app.AppOpsManager ) v0 ).finishOp ( v1, p2, p3 ); // invoke-virtual {v0, v1, p2, p3}, Landroid/app/AppOpsManager;->finishOp(IILjava/lang/String;)V
/* .line 255 */
} // :cond_2
} // :goto_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "update Location Icon to " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " for ("; // const-string v1, " for ("
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ","; // const-string v1, ","
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ")"; // const-string v1, ")"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v1 = 1; // const/4 v1, 0x1
final String v2 = "GpoUtil"; // const-string v2, "GpoUtil"
(( com.android.server.location.gnss.hal.GpoUtil ) p0 ).logi ( v2, v0, v1 ); // invoke-virtual {p0, v2, v0, v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V
/* .line 257 */
return;
} // .end method
