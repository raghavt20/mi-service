public abstract class com.android.server.location.gnss.hal.GpoUtil$IDataEvent {
	 /* .source "GpoUtil.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/location/gnss/hal/GpoUtil; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x609 */
/* name = "IDataEvent" */
} // .end annotation
/* # virtual methods */
public abstract void updateDefaultNetwork ( Integer p0 ) {
} // .end method
public abstract void updateFeatureSwitch ( Integer p0 ) {
} // .end method
public abstract void updateGnssStatus ( Integer p0 ) {
} // .end method
public abstract void updateScreenState ( Boolean p0 ) {
} // .end method
