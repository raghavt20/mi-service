.class Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;
.super Ljava/lang/Object;
.source "Gpo4Client.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/location/gnss/hal/Gpo4Client;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LocationRequestRecorder"
.end annotation


# instance fields
.field private callbackType:Ljava/lang/Object;

.field private listenerHashCode:I

.field private nlpReturnTime:J

.field private nlpReturned:Z

.field private provider:Ljava/lang/String;

.field final synthetic this$0:Lcom/android/server/location/gnss/hal/Gpo4Client;

.field private uid:I


# direct methods
.method public constructor <init>(Lcom/android/server/location/gnss/hal/Gpo4Client;ILjava/lang/String;ILjava/lang/Object;ZJ)V
    .locals 0
    .param p2, "uid"    # I
    .param p3, "provider"    # Ljava/lang/String;
    .param p4, "listenerHashCode"    # I
    .param p5, "callbackType"    # Ljava/lang/Object;
    .param p6, "nlpReturned"    # Z
    .param p7, "nlpReturnTime"    # J

    .line 314
    iput-object p1, p0, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;->this$0:Lcom/android/server/location/gnss/hal/Gpo4Client;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 315
    iput p2, p0, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;->uid:I

    .line 316
    iput-object p3, p0, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;->provider:Ljava/lang/String;

    .line 317
    iput p4, p0, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;->listenerHashCode:I

    .line 318
    iput-object p5, p0, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;->callbackType:Ljava/lang/Object;

    .line 319
    iput-boolean p6, p0, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;->nlpReturned:Z

    .line 320
    iput-wide p7, p0, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;->nlpReturnTime:J

    .line 321
    return-void
.end method


# virtual methods
.method public existListenerHashCode(I)Z
    .locals 1
    .param p1, "value"    # I

    .line 336
    iget v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;->listenerHashCode:I

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public existUid(I)Z
    .locals 1
    .param p1, "value"    # I

    .line 324
    iget v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;->uid:I

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getCallbackType()Ljava/lang/Object;
    .locals 1

    .line 340
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;->callbackType:Ljava/lang/Object;

    return-object v0
.end method

.method public getNlpReturnTime()J
    .locals 2

    .line 356
    iget-wide v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;->nlpReturnTime:J

    return-wide v0
.end method

.method public getNlpReturned()Z
    .locals 1

    .line 348
    iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;->nlpReturned:Z

    return v0
.end method

.method public getProvider()Ljava/lang/String;
    .locals 1

    .line 332
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;->provider:Ljava/lang/String;

    return-object v0
.end method

.method public getUid()I
    .locals 1

    .line 328
    iget v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;->uid:I

    return v0
.end method

.method public setNlpReturnTime(J)V
    .locals 0
    .param p1, "value"    # J

    .line 352
    iput-wide p1, p0, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;->nlpReturnTime:J

    .line 353
    return-void
.end method

.method public setNlpReturned(Z)V
    .locals 0
    .param p1, "value"    # Z

    .line 344
    iput-boolean p1, p0, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;->nlpReturned:Z

    .line 345
    return-void
.end method
