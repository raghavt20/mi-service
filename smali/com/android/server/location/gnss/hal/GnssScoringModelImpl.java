public class com.android.server.location.gnss.hal.GnssScoringModelImpl implements com.android.server.location.gnss.hal.GnssScoringModelStub {
	 /* .source "GnssScoringModelImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final Long IGNORE_RUNNING_TIME;
	 private static final Integer NO_FIELD_SCORE;
	 private static final Long NO_FIELD_TIME;
	 private static final java.lang.String TAG;
	 private static final Long WEEK_FIELD_TIME;
	 /* # instance fields */
	 private Boolean mFeatureSwitch;
	 private com.android.server.location.gnss.hal.Gpo5Client mGpo5Client;
	 private final com.android.server.location.gnss.hal.GpoUtil mGpoUtil;
	 private Long mLastLocationTime;
	 private Boolean mModelRunning;
	 private Long mStartTime;
	 /* # direct methods */
	 public com.android.server.location.gnss.hal.GnssScoringModelImpl ( ) {
		 /* .locals 2 */
		 /* .line 22 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 24 */
		 com.android.server.location.gnss.hal.GpoUtil .getInstance ( );
		 this.mGpoUtil = v0;
		 /* .line 33 */
		 /* const-wide/16 v0, 0x0 */
		 /* iput-wide v0, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mStartTime:J */
		 /* .line 34 */
		 /* iput-wide v0, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mLastLocationTime:J */
		 /* .line 35 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* iput-boolean v0, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mFeatureSwitch:Z */
		 /* .line 36 */
		 /* iput-boolean v0, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mModelRunning:Z */
		 return;
	 } // .end method
	 private Long setLimitTime ( Integer p0 ) {
		 /* .locals 6 */
		 /* .param p1, "score" # I */
		 /* .line 98 */
		 v0 = this.mGpoUtil;
		 /* new-instance v1, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v2 = "The score is : "; // const-string v2, "The score is : "
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 final String v2 = "GnssScoringModel"; // const-string v2, "GnssScoringModel"
		 (( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logv ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
		 /* .line 100 */
		 int v0 = 4; // const/4 v0, 0x4
		 /* if-gt p1, v0, :cond_0 */
		 /* .line 101 */
		 /* const-wide/16 v0, 0x4a38 */
		 /* .local v0, "limitTime":J */
		 /* .line 103 */
	 } // .end local v0 # "limitTime":J
} // :cond_0
/* const-wide/32 v0, 0xee48 */
/* .line 105 */
/* .restart local v0 # "limitTime":J */
} // :goto_0
v3 = this.mGpoUtil;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "The Limit time is : "; // const-string v5, "The Limit time is : "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0, v1 ); // invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.android.server.location.gnss.hal.GpoUtil ) v3 ).logv ( v2, v4 ); // invoke-virtual {v3, v2, v4}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 106 */
/* return-wide v0 */
} // .end method
private void switchEngineStateWithScore ( Integer p0, Long p1 ) {
/* .locals 6 */
/* .param p1, "score" # I */
/* .param p2, "time" # J */
/* .line 76 */
/* invoke-direct {p0, p1}, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->setLimitTime(I)J */
/* move-result-wide v0 */
/* cmp-long v0, p2, v0 */
/* if-ltz v0, :cond_1 */
v0 = this.mGpoUtil;
v0 = (( com.android.server.location.gnss.hal.GpoUtil ) v0 ).getEngineStatus ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->getEngineStatus()I
int v1 = 2; // const/4 v1, 0x2
/* if-ne v0, v1, :cond_1 */
/* .line 81 */
v0 = this.mGpoUtil;
v0 = (( com.android.server.location.gnss.hal.GpoUtil ) v0 ).getCurUserBehav ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->getCurUserBehav()I
int v1 = 4; // const/4 v1, 0x4
int v2 = 1; // const/4 v2, 0x1
final String v3 = "GnssScoringModel"; // const-string v3, "GnssScoringModel"
/* if-ne v0, v1, :cond_0 */
/* .line 82 */
v0 = this.mGpoUtil;
final String v1 = "Re-run Scoring Model."; // const-string v1, "Re-run Scoring Model."
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logv ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 83 */
(( com.android.server.location.gnss.hal.GnssScoringModelImpl ) p0 ).startScoringModel ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->startScoringModel(Z)V
/* .line 84 */
return;
/* .line 86 */
} // :cond_0
v0 = this.mGpoUtil;
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).doStopEngineByInstance ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->doStopEngineByInstance()V
/* .line 87 */
v0 = this.mGpoUtil;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Gnss Score: "; // const-string v4, "Gnss Score: "
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = ", using time: "; // const-string v4, ", using time: "
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-wide/16 v4, 0x3e8 */
/* div-long v4, p2, v4 */
(( java.lang.StringBuilder ) v1 ).append ( v4, v5 ); // invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logi ( v3, v1, v2 ); // invoke-virtual {v0, v3, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V
/* .line 95 */
} // :cond_1
return;
} // .end method
/* # virtual methods */
public void init ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "state" # Z */
/* .line 40 */
/* iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mFeatureSwitch:Z */
/* if-ne v0, p1, :cond_0 */
return;
/* .line 41 */
} // :cond_0
/* iput-boolean p1, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mFeatureSwitch:Z */
/* .line 42 */
v0 = this.mGpoUtil;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "set GnssScoringModel running: " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v2 = 1; // const/4 v2, 0x1
final String v3 = "GnssScoringModel"; // const-string v3, "GnssScoringModel"
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logi ( v3, v1, v2 ); // invoke-virtual {v0, v3, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V
/* .line 43 */
com.android.server.location.gnss.hal.Gpo5Client .getInstance ( );
this.mGpo5Client = v0;
/* .line 44 */
return;
} // .end method
public void reportSvStatus2Score ( Float[] p0 ) {
/* .locals 6 */
/* .param p1, "basebandCn0DbHzs" # [F */
/* .line 67 */
/* iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mFeatureSwitch:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mModelRunning:Z */
/* if-nez v0, :cond_0 */
/* .line 68 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 69 */
/* .local v0, "sum":I */
/* array-length v1, p1 */
int v2 = 0; // const/4 v2, 0x0
/* move v3, v2 */
} // :goto_0
/* if-ge v3, v1, :cond_2 */
/* aget v4, p1, v3 */
/* .line 70 */
/* .local v4, "cn0":F */
int v5 = 0; // const/4 v5, 0x0
/* cmpl-float v5, v4, v5 */
/* if-lez v5, :cond_1 */
int v5 = 1; // const/4 v5, 0x1
} // :cond_1
/* move v5, v2 */
} // :goto_1
/* add-int/2addr v0, v5 */
/* .line 69 */
} // .end local v4 # "cn0":F
/* add-int/lit8 v3, v3, 0x1 */
/* .line 72 */
} // :cond_2
/* mul-int/lit8 v1, v0, 0x2 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v2 */
/* iget-wide v4, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mStartTime:J */
/* sub-long/2addr v2, v4 */
/* invoke-direct {p0, v1, v2, v3}, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->switchEngineStateWithScore(IJ)V */
/* .line 73 */
return;
/* .line 67 */
} // .end local v0 # "sum":I
} // :cond_3
} // :goto_2
return;
} // .end method
public void startScoringModel ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "on" # Z */
/* .line 48 */
/* iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mFeatureSwitch:Z */
/* if-nez v0, :cond_0 */
return;
/* .line 49 */
} // :cond_0
v0 = this.mGpoUtil;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "start Scoring Model ? " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "GnssScoringModel"; // const-string v2, "GnssScoringModel"
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logv ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 50 */
/* iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mModelRunning:Z */
/* if-nez v0, :cond_1 */
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 51 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mStartTime:J */
/* .line 53 */
} // :cond_1
if ( p1 != null) { // if-eqz p1, :cond_3
/* iget-wide v0, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mLastLocationTime:J */
/* const-wide/16 v2, 0x0 */
/* cmp-long v0, v0, v2 */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 54 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v0 */
/* iget-wide v2, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mLastLocationTime:J */
/* sub-long/2addr v0, v2 */
/* const-wide/32 v2, 0x493e0 */
/* cmp-long v0, v0, v2 */
/* if-lez v0, :cond_3 */
} // :cond_2
int v0 = 1; // const/4 v0, 0x1
} // :cond_3
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
/* iput-boolean v0, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mModelRunning:Z */
/* .line 55 */
return;
} // .end method
public void updateFixTime ( Long p0 ) {
/* .locals 3 */
/* .param p1, "time" # J */
/* .line 59 */
/* iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mFeatureSwitch:Z */
/* if-nez v0, :cond_0 */
return;
/* .line 60 */
} // :cond_0
v0 = this.mGpoUtil;
final String v1 = "GnssScoringModel"; // const-string v1, "GnssScoringModel"
/* const-string/jumbo v2, "update gnss fix time." */
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logv ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 61 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mModelRunning:Z */
/* .line 62 */
/* iput-wide p1, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mLastLocationTime:J */
/* .line 63 */
return;
} // .end method
