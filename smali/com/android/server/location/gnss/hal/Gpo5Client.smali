.class public Lcom/android/server/location/gnss/hal/Gpo5Client;
.super Ljava/lang/Object;
.source "Gpo5Client.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;
    }
.end annotation


# static fields
.field private static final MAX_RECOED_MOVING_TIME_SIZE:J = 0x493e0L

.field private static final MAX_RECORD_STEP_TIME_SIZE:I = 0x64

.field private static final MAX_STEPS:F = 5.0f

.field private static final MAX_TRAFFIC_SPEED:F = 3.0f

.field private static final MIN_RECOED_MOVING_TIME_SIZE:J = 0x2710L

.field private static final SENSOR_SENSITIVE:I = 0x0

.field private static final SENSOR_TYPE_OEM_USER_BEHAVIOR:I = 0x1fa2699

.field private static final TAG:Ljava/lang/String; = "Gpo5Client"

.field private static final TIME_CHECK_NAV_APP:J = 0xbb8L

.field private static final TIME_POST_DELAY_CHECK_NLP:I = 0xfa

.field private static final TIME_POST_TIMEOUT_OBTAIN_NLP:I = 0x7d0

.field private static final USER_BEHAVIOR_TRAFFIC:F = 4.0f

.field private static volatile instance:Lcom/android/server/location/gnss/hal/Gpo5Client;


# instance fields
.field private futureCheckNavigation1:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture<",
            "*>;"
        }
    .end annotation
.end field

.field private futureCheckNavigation2:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture<",
            "*>;"
        }
    .end annotation
.end field

.field private futureCheckRequest:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture<",
            "*>;"
        }
    .end annotation
.end field

.field private futureReturnFineLocation:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture<",
            "*>;"
        }
    .end annotation
.end field

.field private isClientAlive:Z

.field private final isFeatureEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final isInsideSmallArea:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final isScreenOn:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private isUserBehaviorSensorRegistered:Z

.field private final mCacheLocationCallbackMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private final mCurrentSteps:Ljava/util/concurrent/atomic/AtomicInteger;

.field private mFineLocation:Landroid/location/Location;

.field private final mGlpRequestMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;",
            ">;"
        }
    .end annotation
.end field

.field private final mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

.field private final mInitialSteps:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final mLastGnssTrafficTime:Ljava/util/concurrent/atomic/AtomicLong;

.field private mLastNlpLocation:Landroid/location/Location;

.field private final mLastNlpTime:Ljava/util/concurrent/atomic/AtomicLong;

.field private final mLastSensorTrafficTime:Ljava/util/concurrent/atomic/AtomicLong;

.field private mLocationManager:Landroid/location/LocationManager;

.field private final mNavAppInfo:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mNetworkLocationListener:Landroid/location/LocationListener;

.field private final mNlpRequestMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;",
            ">;"
        }
    .end annotation
.end field

.field private final mObtainNlpTime:Ljava/util/concurrent/atomic/AtomicLong;

.field private final mPassiveLocationListener:Landroid/location/LocationListener;

.field private final mRequestNlpTime:Ljava/util/concurrent/atomic/AtomicLong;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private final mStartEngineWhileScreenLocked:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final mStepTimeRecorder:Ljava/util/concurrent/BlockingDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingDeque<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mUserBehaviorDetector:Landroid/hardware/Sensor;

.field private final mUserBehaviorListener:Landroid/hardware/SensorEventListener;

.field private mUserStepsDetector:Landroid/hardware/Sensor;

.field private final mUserStepsListener:Landroid/hardware/SensorEventListener;

.field private scheduledThreadPoolExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;


# direct methods
.method public static synthetic $r8$lambda$8qAOQpqB-ZU_iVPk4B5LmFrM4Yo(Lcom/android/server/location/gnss/hal/Gpo5Client;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/location/gnss/hal/Gpo5Client;->lambda$saveLocationRequestId$2(II)V

    return-void
.end method

.method public static synthetic $r8$lambda$KKe_slwmNRqt3DI2t-6ADwTOFEw(Lcom/android/server/location/gnss/hal/Gpo5Client;)Ljava/lang/Boolean;
    .locals 0

    invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->lambda$registerNetworkLocationUpdates$5()Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$KSM108fFRhOGSQsrqH89yHVBmtk(Lcom/android/server/location/gnss/hal/Gpo5Client;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->lambda$saveLocationRequestId$1()V

    return-void
.end method

.method public static synthetic $r8$lambda$P7BJcDGM4MGPM2WIizDUYgDWuwI(Lcom/android/server/location/gnss/hal/Gpo5Client;Landroid/location/Location;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/location/gnss/hal/Gpo5Client;->lambda$new$3(Landroid/location/Location;)V

    return-void
.end method

.method public static synthetic $r8$lambda$QkFJwxbA-LNPoO-_7D4L2m8wcNo(Lcom/android/server/location/gnss/hal/Gpo5Client;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->lambda$checkAppUsage$7()V

    return-void
.end method

.method public static synthetic $r8$lambda$WHDDIxBnFXTAsfQAwlvemllYmcQ(Lcom/android/server/location/gnss/hal/Gpo5Client;Landroid/location/Location;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/location/gnss/hal/Gpo5Client;->lambda$new$4(Landroid/location/Location;)V

    return-void
.end method

.method public static synthetic $r8$lambda$_KGvJDv-9fO_fprUa5_UKqauhJM(Lcom/android/server/location/gnss/hal/Gpo5Client;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->lambda$unregisterNetworkLocationUpdates$6()V

    return-void
.end method

.method public static synthetic $r8$lambda$uVyDP5uqcDGtjyPlz5UVv8NG5oM(Lcom/android/server/location/gnss/hal/Gpo5Client;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->lambda$returnFineLocation2App$0()V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetisUserBehaviorSensorRegistered(Lcom/android/server/location/gnss/hal/Gpo5Client;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->isUserBehaviorSensorRegistered:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmCurrentSteps(Lcom/android/server/location/gnss/hal/Gpo5Client;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 0

    iget-object p0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mCurrentSteps:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmGpoUtil(Lcom/android/server/location/gnss/hal/Gpo5Client;)Lcom/android/server/location/gnss/hal/GpoUtil;
    .locals 0

    iget-object p0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmInitialSteps(Lcom/android/server/location/gnss/hal/Gpo5Client;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 0

    iget-object p0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mInitialSteps:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmLastSensorTrafficTime(Lcom/android/server/location/gnss/hal/Gpo5Client;)Ljava/util/concurrent/atomic/AtomicLong;
    .locals 0

    iget-object p0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mLastSensorTrafficTime:Ljava/util/concurrent/atomic/AtomicLong;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmStepTimeRecorder(Lcom/android/server/location/gnss/hal/Gpo5Client;)Ljava/util/concurrent/BlockingDeque;
    .locals 0

    iget-object p0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mStepTimeRecorder:Ljava/util/concurrent/BlockingDeque;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mhandleLeaveSmallArea(Lcom/android/server/location/gnss/hal/Gpo5Client;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->handleLeaveSmallArea()V

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .line 167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mFineLocation:Landroid/location/Location;

    .line 63
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->isUserBehaviorSensorRegistered:Z

    .line 67
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->isClientAlive:Z

    .line 70
    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mLastNlpLocation:Landroid/location/Location;

    .line 71
    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->futureCheckRequest:Ljava/util/concurrent/ScheduledFuture;

    .line 72
    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->futureCheckNavigation1:Ljava/util/concurrent/ScheduledFuture;

    .line 73
    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->futureCheckNavigation2:Ljava/util/concurrent/ScheduledFuture;

    .line 74
    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->futureReturnFineLocation:Ljava/util/concurrent/ScheduledFuture;

    .line 76
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGlpRequestMap:Ljava/util/Map;

    .line 77
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mNlpRequestMap:Ljava/util/Map;

    .line 78
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mNavAppInfo:Ljava/util/Map;

    .line 79
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mCacheLocationCallbackMap:Ljava/util/Map;

    .line 80
    invoke-static {}, Lcom/android/server/location/gnss/hal/GpoUtil;->getInstance()Lcom/android/server/location/gnss/hal/GpoUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    .line 82
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->isFeatureEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 83
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->isScreenOn:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 84
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mRequestNlpTime:Ljava/util/concurrent/atomic/AtomicLong;

    .line 85
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mObtainNlpTime:Ljava/util/concurrent/atomic/AtomicLong;

    .line 86
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mLastNlpTime:Ljava/util/concurrent/atomic/AtomicLong;

    .line 87
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mLastGnssTrafficTime:Ljava/util/concurrent/atomic/AtomicLong;

    .line 88
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mLastSensorTrafficTime:Ljava/util/concurrent/atomic/AtomicLong;

    .line 89
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mCurrentSteps:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 90
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mInitialSteps:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 91
    new-instance v0, Ljava/util/concurrent/LinkedBlockingDeque;

    const/16 v2, 0x64

    invoke-direct {v0, v2}, Ljava/util/concurrent/LinkedBlockingDeque;-><init>(I)V

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mStepTimeRecorder:Ljava/util/concurrent/BlockingDeque;

    .line 92
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->isInsideSmallArea:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 93
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mStartEngineWhileScreenLocked:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 96
    new-instance v0, Lcom/android/server/location/gnss/hal/Gpo5Client$1;

    invoke-direct {v0, p0}, Lcom/android/server/location/gnss/hal/Gpo5Client$1;-><init>(Lcom/android/server/location/gnss/hal/Gpo5Client;)V

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mUserBehaviorListener:Landroid/hardware/SensorEventListener;

    .line 119
    new-instance v0, Lcom/android/server/location/gnss/hal/Gpo5Client$2;

    invoke-direct {v0, p0}, Lcom/android/server/location/gnss/hal/Gpo5Client$2;-><init>(Lcom/android/server/location/gnss/hal/Gpo5Client;)V

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mUserStepsListener:Landroid/hardware/SensorEventListener;

    .line 389
    new-instance v0, Lcom/android/server/location/gnss/hal/Gpo5Client$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/android/server/location/gnss/hal/Gpo5Client$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/location/gnss/hal/Gpo5Client;)V

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mPassiveLocationListener:Landroid/location/LocationListener;

    .line 448
    new-instance v0, Lcom/android/server/location/gnss/hal/Gpo5Client$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0}, Lcom/android/server/location/gnss/hal/Gpo5Client$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/location/gnss/hal/Gpo5Client;)V

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mNetworkLocationListener:Landroid/location/LocationListener;

    .line 167
    return-void
.end method

.method private checkAppUsage(I)Ljava/util/concurrent/ScheduledFuture;
    .locals 5
    .param p1, "timeout"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/concurrent/ScheduledFuture<",
            "*>;"
        }
    .end annotation

    .line 518
    :try_start_0
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    const-string v1, "Gpo5Client"

    const-string v2, "checkAppUsage"

    invoke-virtual {v0, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 519
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->scheduledThreadPoolExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    new-instance v1, Lcom/android/server/location/gnss/hal/Gpo5Client$$ExternalSyntheticLambda4;

    invoke-direct {v1, p0}, Lcom/android/server/location/gnss/hal/Gpo5Client$$ExternalSyntheticLambda4;-><init>(Lcom/android/server/location/gnss/hal/Gpo5Client;)V

    int-to-long v2, p1

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 526
    :catch_0
    move-exception v0

    .line 527
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 529
    .end local v0    # "e":Ljava/lang/Exception;
    const/4 v0, 0x0

    return-object v0
.end method

.method private checkFeatureSwitch()V
    .locals 4

    .line 315
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGlpRequestMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 316
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->isFeatureEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

    iget-boolean v1, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->isUserBehaviorSensorRegistered:Z

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    invoke-virtual {v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->checkHeavyUser()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    .line 317
    invoke-virtual {v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->isNetworkConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->isScreenOn:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->isUserMovingRecently()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    move v1, v2

    .line 316
    :goto_0
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 318
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isGpoFeatureEnabled ? "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->isFeatureEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "Gpo5Client"

    invoke-virtual {v0, v3, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 319
    invoke-static {}, Lcom/android/server/location/gnss/hal/GnssScoringModelStub;->getInstance()Lcom/android/server/location/gnss/hal/GnssScoringModelStub;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->isFeatureEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/android/server/location/gnss/hal/GnssScoringModelStub;->init(Z)V

    .line 321
    :cond_1
    return-void
.end method

.method private delieverLocation(Ljava/lang/Object;Ljava/util/List;)V
    .locals 6
    .param p1, "callbackType"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/util/List<",
            "Landroid/location/Location;",
            ">;)V"
        }
    .end annotation

    .line 416
    .local p2, "locations":Ljava/util/List;, "Ljava/util/List<Landroid/location/Location;>;"
    instance-of v0, p1, Landroid/location/ILocationListener;

    const/4 v1, 0x1

    const-string v2, "Gpo5Client"

    if-eqz v0, :cond_0

    .line 418
    :try_start_0
    move-object v0, p1

    check-cast v0, Landroid/location/ILocationListener;

    const/4 v3, 0x0

    invoke-interface {v0, p2, v3}, Landroid/location/ILocationListener;->onLocationChanged(Ljava/util/List;Landroid/os/IRemoteCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 419
    :catch_0
    move-exception v0

    .line 420
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v3, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    const-string v4, "onLocationChanged RemoteException"

    invoke-virtual {v3, v2, v4, v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 421
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    goto :goto_1

    .line 423
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 426
    .local v0, "intent":Landroid/content/Intent;
    const/4 v3, 0x0

    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/Parcelable;

    const-string v5, "location"

    invoke-virtual {v0, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 428
    :try_start_1
    move-object v4, p1

    check-cast v4, Landroid/app/PendingIntent;

    iget-object v5, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v5, v3, v0}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V
    :try_end_1
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_1 .. :try_end_1} :catch_1

    .line 431
    goto :goto_1

    .line 429
    :catch_1
    move-exception v3

    .line 430
    .local v3, "e":Landroid/app/PendingIntent$CanceledException;
    iget-object v4, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    const-string v5, "PendingInent send CanceledException"

    invoke-virtual {v4, v2, v5, v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 433
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v3    # "e":Landroid/app/PendingIntent$CanceledException;
    :goto_1
    return-void
.end method

.method public static getInstance()Lcom/android/server/location/gnss/hal/Gpo5Client;
    .locals 2

    .line 153
    sget-object v0, Lcom/android/server/location/gnss/hal/Gpo5Client;->instance:Lcom/android/server/location/gnss/hal/Gpo5Client;

    if-nez v0, :cond_1

    .line 154
    const-class v0, Lcom/android/server/location/gnss/hal/Gpo5Client;

    monitor-enter v0

    .line 155
    :try_start_0
    sget-object v1, Lcom/android/server/location/gnss/hal/Gpo5Client;->instance:Lcom/android/server/location/gnss/hal/Gpo5Client;

    if-nez v1, :cond_0

    .line 156
    new-instance v1, Lcom/android/server/location/gnss/hal/Gpo5Client;

    invoke-direct {v1}, Lcom/android/server/location/gnss/hal/Gpo5Client;-><init>()V

    sput-object v1, Lcom/android/server/location/gnss/hal/Gpo5Client;->instance:Lcom/android/server/location/gnss/hal/Gpo5Client;

    .line 158
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 160
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/location/gnss/hal/Gpo5Client;->instance:Lcom/android/server/location/gnss/hal/Gpo5Client;

    return-object v0
.end method

.method private handleLeaveSmallArea()V
    .locals 2

    .line 209
    iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->isClientAlive:Z

    if-nez v0, :cond_0

    return-void

    .line 211
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mFineLocation:Landroid/location/Location;

    .line 213
    invoke-static {}, Lcom/android/server/location/gnss/hal/GnssScoringModelStub;->getInstance()Lcom/android/server/location/gnss/hal/GnssScoringModelStub;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/android/server/location/gnss/hal/GnssScoringModelStub;->startScoringModel(Z)V

    .line 216
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->getEngineStatus()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 221
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->doStartEngineByInstance()Z

    .line 223
    invoke-static {}, Lcom/android/server/location/LocationDumpLogStub;->getInstance()Lcom/android/server/location/LocationDumpLogStub;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/android/server/location/LocationDumpLogStub;->setRecordLoseLocation(Z)V

    .line 225
    :cond_1
    return-void
.end method

.method private isUserMovingRecently()Z
    .locals 12

    .line 324
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 325
    .local v0, "curTime":J
    iget-object v2, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mStepTimeRecorder:Ljava/util/concurrent/BlockingDeque;

    invoke-interface {v2}, Ljava/util/concurrent/BlockingDeque;->size()I

    move-result v2

    const/16 v3, 0x64

    const-wide/32 v4, 0x493e0

    const/4 v6, 0x0

    const/4 v7, 0x1

    if-lt v2, v3, :cond_0

    iget-object v2, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mStepTimeRecorder:Ljava/util/concurrent/BlockingDeque;

    .line 326
    invoke-interface {v2}, Ljava/util/concurrent/BlockingDeque;->getFirst()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sub-long v2, v0, v2

    cmp-long v2, v2, v4

    if-gtz v2, :cond_0

    iget-object v2, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mStepTimeRecorder:Ljava/util/concurrent/BlockingDeque;

    .line 327
    invoke-interface {v2}, Ljava/util/concurrent/BlockingDeque;->getFirst()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v8, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mStepTimeRecorder:Ljava/util/concurrent/BlockingDeque;

    invoke-interface {v8}, Ljava/util/concurrent/BlockingDeque;->getLast()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    sub-long/2addr v2, v8

    cmp-long v2, v2, v4

    if-gtz v2, :cond_0

    move v2, v7

    goto :goto_0

    :cond_0
    move v2, v6

    .line 332
    .local v2, "walkRecently":Z
    :goto_0
    iget-object v3, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mLastGnssTrafficTime:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v3, v8, v10

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mLastGnssTrafficTime:Ljava/util/concurrent/atomic/AtomicLong;

    .line 333
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v8

    sub-long v8, v0, v8

    cmp-long v3, v8, v4

    if-lez v3, :cond_2

    :cond_1
    iget-object v3, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mLastSensorTrafficTime:Ljava/util/concurrent/atomic/AtomicLong;

    .line 334
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v3

    cmp-long v3, v3, v10

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mLastSensorTrafficTime:Ljava/util/concurrent/atomic/AtomicLong;

    .line 335
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v3

    sub-long v3, v0, v3

    const-wide/16 v8, 0x2710

    cmp-long v3, v3, v8

    if-gtz v3, :cond_3

    :cond_2
    move v3, v7

    goto :goto_1

    :cond_3
    move v3, v6

    .line 336
    .local v3, "trafficRecently":Z
    :goto_1
    iget-object v4, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "CurrentTime is "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ", mLastGnssTrafficTime is "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v8, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mLastGnssTrafficTime:Ljava/util/concurrent/atomic/AtomicLong;

    .line 337
    invoke-virtual {v8}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ", mLastSensorTrafficTime is "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v8, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mLastSensorTrafficTime:Ljava/util/concurrent/atomic/AtomicLong;

    .line 338
    invoke-virtual {v8}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 336
    const-string v8, "Gpo5Client"

    invoke-virtual {v4, v8, v5}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    iget-object v4, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Recently user walk ? "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, ", traffic ? "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v8, v5, v7}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 340
    if-nez v2, :cond_4

    if-eqz v3, :cond_5

    :cond_4
    move v6, v7

    :cond_5
    return v6
.end method

.method private synthetic lambda$checkAppUsage$7()V
    .locals 3

    .line 521
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->getEngineStatus()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 522
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    const-string v1, "Gpo5Client"

    const-string v2, "navigating app, restart gnss engine"

    invoke-virtual {v0, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 523
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->doStartEngineByInstance()Z

    .line 525
    :cond_0
    return-void
.end method

.method private synthetic lambda$new$3(Landroid/location/Location;)V
    .locals 9
    .param p1, "location"    # Landroid/location/Location;

    .line 390
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->isFeatureEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "network"

    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    .line 391
    invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->getEngineStatus()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    .line 392
    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    const/high16 v1, 0x43160000    # 150.0f

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_2

    .line 394
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    invoke-virtual {v0, p1}, Lcom/android/server/location/gnss/hal/GpoUtil;->convertNlp2Glp(Landroid/location/Location;)Landroid/location/Location;

    move-result-object v0

    .line 395
    .local v0, "gLocation":Landroid/location/Location;
    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mLastNlpLocation:Landroid/location/Location;

    .line 396
    iget-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mLastNlpTime:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 397
    iget-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGlpRequestMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 398
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;

    invoke-virtual {v3}, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;->getNlpReturned()Z

    move-result v3

    if-eqz v3, :cond_0

    goto/16 :goto_1

    .line 399
    :cond_0
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;->setNlpReturned(Z)V

    .line 400
    filled-new-array {v0}, [Landroid/location/Location;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    .line 401
    .local v3, "locations":Ljava/util/List;, "Ljava/util/List<Landroid/location/Location;>;"
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "use nlp "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Landroid/location/Location;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", listenerHashCode is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 402
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", callbackType is ILocationListener "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 404
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;

    invoke-virtual {v6}, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;->getCallbackType()Ljava/lang/Object;

    move-result-object v6

    instance-of v6, v6, Landroid/location/ILocationListener;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 405
    .local v5, "logInfo":Ljava/lang/String;
    iget-object v6, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    const-string v7, "Gpo5Client"

    invoke-virtual {v6, v7, v5}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 406
    iget-object v6, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    invoke-virtual {v6, v5}, Lcom/android/server/location/gnss/hal/GpoUtil;->logEn(Ljava/lang/String;)V

    .line 407
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;

    invoke-virtual {v6}, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;->getCallbackType()Ljava/lang/Object;

    move-result-object v6

    invoke-direct {p0, v6, v3}, Lcom/android/server/location/gnss/hal/Gpo5Client;->delieverLocation(Ljava/lang/Object;Ljava/util/List;)V

    .line 408
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;->setNlpReturnTime(J)V

    .line 409
    const/16 v6, 0xfa

    invoke-direct {p0, v6}, Lcom/android/server/location/gnss/hal/Gpo5Client;->checkAppUsage(I)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v6

    iput-object v6, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->futureCheckNavigation2:Ljava/util/concurrent/ScheduledFuture;

    .line 410
    iget-object v6, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->futureCheckRequest:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v6, :cond_1

    invoke-interface {v6, v4}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 411
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;>;"
    .end local v3    # "locations":Ljava/util/List;, "Ljava/util/List<Landroid/location/Location;>;"
    .end local v5    # "logInfo":Ljava/lang/String;
    :cond_1
    goto/16 :goto_0

    .line 413
    .end local v0    # "gLocation":Landroid/location/Location;
    :cond_2
    :goto_1
    return-void
.end method

.method private synthetic lambda$new$4(Landroid/location/Location;)V
    .locals 3
    .param p1, "location"    # Landroid/location/Location;

    .line 449
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mObtainNlpTime:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    return-void
.end method

.method private synthetic lambda$registerNetworkLocationUpdates$5()Ljava/lang/Boolean;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 472
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    const-string v1, "Gpo5Client"

    const-string v2, "request NLP."

    invoke-virtual {v0, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 473
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mRequestNlpTime:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 474
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mLocationManager:Landroid/location/LocationManager;

    const-string v1, "network"

    const-wide/16 v2, 0x3e8

    const/high16 v4, 0x447a0000    # 1000.0f

    const/4 v5, 0x1

    invoke-static {v1, v2, v3, v4, v5}, Landroid/location/LocationRequest;->createFromDeprecatedProvider(Ljava/lang/String;JFZ)Landroid/location/LocationRequest;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mNetworkLocationListener:Landroid/location/LocationListener;

    iget-object v3, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mContext:Landroid/content/Context;

    .line 476
    invoke-virtual {v3}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    .line 474
    invoke-virtual {v0, v1, v2, v3}, Landroid/location/LocationManager;->requestLocationUpdates(Landroid/location/LocationRequest;Landroid/location/LocationListener;Landroid/os/Looper;)V

    .line 477
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method private synthetic lambda$returnFineLocation2App$0()V
    .locals 5

    .line 287
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mFineLocation:Landroid/location/Location;

    const/4 v1, 0x1

    const-string v2, "cancel scheduleAtFixedRate"

    const-string v3, "Gpo5Client"

    if-nez v0, :cond_0

    .line 288
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    invoke-virtual {v0, v3, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->futureReturnFineLocation:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 290
    return-void

    .line 293
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->isUserBehaviorSensorRegistered:Z

    if-nez v0, :cond_1

    .line 294
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    invoke-virtual {v0, v3, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->futureReturnFineLocation:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 296
    return-void

    .line 298
    :cond_1
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mCacheLocationCallbackMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 299
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Object;>;"
    iget-object v2, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    const-string v4, "return fine location to navigation app"

    invoke-virtual {v2, v3, v4}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    iget-object v4, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mFineLocation:Landroid/location/Location;

    filled-new-array {v4}, [Landroid/location/Location;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-direct {p0, v2, v4}, Lcom/android/server/location/gnss/hal/Gpo5Client;->delieverLocation(Ljava/lang/Object;Ljava/util/List;)V

    .line 301
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Object;>;"
    goto :goto_0

    .line 302
    :cond_2
    return-void
.end method

.method private synthetic lambda$saveLocationRequestId$1()V
    .locals 4

    .line 379
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mObtainNlpTime:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    iget-object v2, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mRequestNlpTime:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 380
    invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->unregisterNetworkLocationUpdates()V

    .line 382
    :cond_0
    return-void
.end method

.method private synthetic lambda$saveLocationRequestId$2(II)V
    .locals 6
    .param p1, "listenerHashCode"    # I
    .param p2, "uid"    # I

    .line 366
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGlpRequestMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 367
    const/4 v0, 0x0

    .line 368
    .local v0, "existNlp":Z
    iget-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mNlpRequestMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;

    .line 369
    .local v2, "r":Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;
    invoke-virtual {v2, p2}, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;->existUid(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 370
    const/4 v0, 0x1

    .line 371
    goto :goto_1

    .line 373
    .end local v2    # "r":Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;
    :cond_0
    goto :goto_0

    .line 374
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "LocationRequest of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " existNlp ? "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const-string v4, "Gpo5Client"

    invoke-virtual {v1, v4, v2, v3}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 376
    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->registerNetworkLocationUpdates()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 377
    iget-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->scheduledThreadPoolExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    new-instance v2, Lcom/android/server/location/gnss/hal/Gpo5Client$$ExternalSyntheticLambda2;

    invoke-direct {v2, p0}, Lcom/android/server/location/gnss/hal/Gpo5Client$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/location/gnss/hal/Gpo5Client;)V

    const-wide/16 v3, 0x7d0

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v4, v5}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->futureCheckRequest:Ljava/util/concurrent/ScheduledFuture;

    .line 385
    .end local v0    # "existNlp":Z
    :cond_2
    return-void
.end method

.method private synthetic lambda$unregisterNetworkLocationUpdates$6()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 483
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mLocationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mNetworkLocationListener:Landroid/location/LocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    return-void
.end method

.method private registerNetworkLocationUpdates()Z
    .locals 9

    .line 452
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->isNetworkConnected()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mLocationManager:Landroid/location/LocationManager;

    const-string v2, "network"

    invoke-virtual {v0, v2}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    goto/16 :goto_2

    .line 453
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mLastNlpLocation:Landroid/location/Location;

    if-eqz v0, :cond_4

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mLastNlpTime:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x7d0

    cmp-long v0, v2, v4

    if-gtz v0, :cond_4

    .line 454
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGlpRequestMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 455
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;

    invoke-virtual {v3}, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;->getNlpReturned()Z

    move-result v3

    if-eqz v3, :cond_1

    goto/16 :goto_1

    .line 456
    :cond_1
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;->setNlpReturned(Z)V

    .line 457
    iget-object v3, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mLastNlpLocation:Landroid/location/Location;

    filled-new-array {v3}, [Landroid/location/Location;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    .line 458
    .local v3, "locations":Ljava/util/List;, "Ljava/util/List<Landroid/location/Location;>;"
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "use cached nlp "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mLastNlpLocation:Landroid/location/Location;

    invoke-virtual {v6}, Landroid/location/Location;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", listenerHashCode is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 459
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", callbackType is ILocationListener "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 461
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;

    invoke-virtual {v6}, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;->getCallbackType()Ljava/lang/Object;

    move-result-object v6

    instance-of v6, v6, Landroid/location/ILocationListener;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 462
    .local v5, "logInfo":Ljava/lang/String;
    iget-object v6, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    const-string v7, "Gpo5Client"

    invoke-virtual {v6, v7, v5}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 463
    iget-object v6, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    invoke-virtual {v6, v5}, Lcom/android/server/location/gnss/hal/GpoUtil;->logEn(Ljava/lang/String;)V

    .line 464
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;

    invoke-virtual {v6}, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;->getCallbackType()Ljava/lang/Object;

    move-result-object v6

    invoke-direct {p0, v6, v3}, Lcom/android/server/location/gnss/hal/Gpo5Client;->delieverLocation(Ljava/lang/Object;Ljava/util/List;)V

    .line 465
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;->setNlpReturnTime(J)V

    .line 466
    const/16 v6, 0xfa

    invoke-direct {p0, v6}, Lcom/android/server/location/gnss/hal/Gpo5Client;->checkAppUsage(I)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v6

    iput-object v6, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->futureCheckNavigation2:Ljava/util/concurrent/ScheduledFuture;

    .line 467
    iget-object v6, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->futureCheckRequest:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v6, :cond_2

    invoke-interface {v6, v4}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 468
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;>;"
    .end local v3    # "locations":Ljava/util/List;, "Ljava/util/List<Landroid/location/Location;>;"
    .end local v5    # "logInfo":Ljava/lang/String;
    :cond_2
    goto/16 :goto_0

    .line 469
    :cond_3
    :goto_1
    return v1

    .line 471
    :cond_4
    new-instance v0, Lcom/android/server/location/gnss/hal/Gpo5Client$$ExternalSyntheticLambda7;

    invoke-direct {v0, p0}, Lcom/android/server/location/gnss/hal/Gpo5Client$$ExternalSyntheticLambda7;-><init>(Lcom/android/server/location/gnss/hal/Gpo5Client;)V

    invoke-static {v0}, Landroid/os/Binder;->withCleanCallingIdentity(Lcom/android/internal/util/FunctionalUtils$ThrowingSupplier;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    .line 452
    :cond_5
    :goto_2
    return v1
.end method

.method private registerPassiveLocationUpdates()V
    .locals 7

    .line 436
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mLocationManager:Landroid/location/LocationManager;

    if-eqz v0, :cond_0

    .line 437
    const-string v1, "passive"

    const-wide/16 v2, 0x3e8

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mPassiveLocationListener:Landroid/location/LocationListener;

    iget-object v6, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mContext:Landroid/content/Context;

    .line 438
    invoke-virtual {v6}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v6

    .line 437
    invoke-virtual/range {v0 .. v6}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;Landroid/os/Looper;)V

    .line 440
    :cond_0
    return-void
.end method

.method private registerUserBehaviorListener()V
    .locals 5

    .line 242
    iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->isClientAlive:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->isUserBehaviorSensorRegistered:Z

    if-eqz v0, :cond_0

    goto :goto_1

    .line 245
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mStartEngineWhileScreenLocked:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    const/4 v1, 0x0

    const-string v2, "Gpo5Client"

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->getEngineStatus()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    goto :goto_0

    .line 251
    :cond_1
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    const-string v3, "register User Behavior Listener"

    invoke-virtual {v0, v2, v3}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v3, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mUserBehaviorListener:Landroid/hardware/SensorEventListener;

    iget-object v4, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mUserBehaviorDetector:Landroid/hardware/Sensor;

    invoke-virtual {v0, v3, v4, v1}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 253
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->isUserBehaviorSensorRegistered:Z

    .line 255
    iget-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mInitialSteps:Ljava/util/concurrent/atomic/AtomicInteger;

    iget-object v3, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mCurrentSteps:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 256
    iget-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "initial steps is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mInitialSteps:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    iget-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->isInsideSmallArea:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 258
    return-void

    .line 246
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    const-string v3, "gnss engine has been started, do not control."

    invoke-virtual {v0, v2, v3}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mStartEngineWhileScreenLocked:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 248
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->isInsideSmallArea:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 249
    return-void

    .line 242
    :cond_3
    :goto_1
    return-void
.end method

.method private registerUserStepsListener()V
    .locals 4

    .line 272
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mSensorManager:Landroid/hardware/SensorManager;

    if-nez v0, :cond_0

    return-void

    .line 273
    :cond_0
    iget-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mUserStepsListener:Landroid/hardware/SensorEventListener;

    iget-object v2, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mUserStepsDetector:Landroid/hardware/Sensor;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 274
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    const-string v1, "Gpo5Client"

    const-string v2, "register User Steps Listener"

    invoke-virtual {v0, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    return-void
.end method

.method private returnFineLocation2App()V
    .locals 7

    .line 285
    :try_start_0
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->scheduledThreadPoolExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    new-instance v1, Lcom/android/server/location/gnss/hal/Gpo5Client$$ExternalSyntheticLambda5;

    invoke-direct {v1, p0}, Lcom/android/server/location/gnss/hal/Gpo5Client$$ExternalSyntheticLambda5;-><init>(Lcom/android/server/location/gnss/hal/Gpo5Client;)V

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x3e8

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual/range {v0 .. v6}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->futureReturnFineLocation:Ljava/util/concurrent/ScheduledFuture;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 305
    goto :goto_0

    .line 303
    :catch_0
    move-exception v0

    .line 304
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 306
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private unregisterNetworkLocationUpdates()V
    .locals 1

    .line 482
    new-instance v0, Lcom/android/server/location/gnss/hal/Gpo5Client$$ExternalSyntheticLambda6;

    invoke-direct {v0, p0}, Lcom/android/server/location/gnss/hal/Gpo5Client$$ExternalSyntheticLambda6;-><init>(Lcom/android/server/location/gnss/hal/Gpo5Client;)V

    invoke-static {v0}, Landroid/os/Binder;->withCleanCallingIdentity(Lcom/android/internal/util/FunctionalUtils$ThrowingRunnable;)V

    .line 485
    return-void
.end method

.method private unregisterPassiveLocationUpdates()V
    .locals 2

    .line 443
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mLocationManager:Landroid/location/LocationManager;

    if-eqz v0, :cond_0

    .line 444
    iget-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mPassiveLocationListener:Landroid/location/LocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 446
    :cond_0
    return-void
.end method

.method private unregisterUserBehaviorListener()V
    .locals 3

    .line 261
    iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->isUserBehaviorSensorRegistered:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->getEngineStatus()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 262
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    const-string v1, "Gpo5Client"

    const-string/jumbo v2, "unregister User Behavior Listener"

    invoke-virtual {v0, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mUserBehaviorListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 264
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->isUserBehaviorSensorRegistered:Z

    .line 265
    iget-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->isInsideSmallArea:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 266
    iget-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mStartEngineWhileScreenLocked:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 267
    iget-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    invoke-virtual {v1, v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->setUserBehav(I)V

    .line 269
    :cond_0
    return-void
.end method

.method private unregisterUserStepsListener()V
    .locals 3

    .line 278
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mSensorManager:Landroid/hardware/SensorManager;

    if-nez v0, :cond_0

    return-void

    .line 279
    :cond_0
    iget-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mUserStepsListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 280
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    const-string v1, "Gpo5Client"

    const-string/jumbo v2, "unregister User Steps Listener"

    invoke-virtual {v0, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    return-void
.end method


# virtual methods
.method public blockEngineStart()Z
    .locals 5

    .line 509
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->isFeatureEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    .line 510
    invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->getEngineStatus()I

    move-result v0

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v1

    .line 511
    .local v0, "res":Z
    :goto_0
    iget-object v2, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "blockEngineStart ? "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Gpo5Client"

    invoke-virtual {v2, v4, v3, v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 512
    const/16 v1, 0x7d0

    invoke-direct {p0, v1}, Lcom/android/server/location/gnss/hal/Gpo5Client;->checkAppUsage(I)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->futureCheckNavigation1:Ljava/util/concurrent/ScheduledFuture;

    .line 513
    return v0
.end method

.method public checkSensorSupport()Z
    .locals 5

    .line 187
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 189
    :cond_0
    const v2, 0x1fa2699

    invoke-virtual {v0, v2, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(IZ)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mUserBehaviorDetector:Landroid/hardware/Sensor;

    .line 190
    const/4 v2, 0x1

    if-eqz v0, :cond_1

    move v1, v2

    :cond_1
    move v0, v1

    .line 191
    .local v0, "sensorSupport":Z
    iget-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "This Device Support Sensor id 33171097 ? "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Gpo5Client"

    invoke-virtual {v1, v4, v3, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 193
    return v0
.end method

.method public clearLocationRequest()V
    .locals 3

    .line 533
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    const-string v1, "Gpo5Client"

    const-string v2, "clearLocationRequest"

    invoke-virtual {v0, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 534
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->futureCheckNavigation1:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 535
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->futureCheckNavigation2:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 536
    :cond_1
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->futureCheckRequest:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_2

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 537
    :cond_2
    return-void
.end method

.method public deinit()V
    .locals 3

    .line 197
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    const-string v1, "Gpo5Client"

    const-string v2, "deinit"

    invoke-virtual {v0, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->isClientAlive:Z

    .line 199
    invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->unregisterPassiveLocationUpdates()V

    .line 200
    iget-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->scheduledThreadPoolExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    invoke-virtual {v1}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->shutdown()V

    .line 201
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->scheduledThreadPoolExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    .line 203
    invoke-static {}, Lcom/android/server/location/gnss/hal/GnssScoringModelStub;->getInstance()Lcom/android/server/location/gnss/hal/GnssScoringModelStub;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/android/server/location/gnss/hal/GnssScoringModelStub;->init(Z)V

    .line 204
    invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->unregisterUserStepsListener()V

    .line 205
    invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->unregisterUserBehaviorListener()V

    .line 206
    return-void
.end method

.method public disableGnssSwitch()V
    .locals 2

    .line 228
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->isFeatureEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 229
    invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->unregisterUserBehaviorListener()V

    .line 230
    return-void
.end method

.method public getNavInfo()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 164
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mNavAppInfo:Ljava/util/Map;

    return-object v0
.end method

.method public init(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 170
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    const-string v1, "Gpo5Client"

    const-string v2, "init"

    invoke-virtual {v0, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->isClientAlive:Z

    .line 172
    iput-object p1, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mContext:Landroid/content/Context;

    .line 173
    nop

    .line 174
    const-string v1, "location"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/location/LocationManager;

    .line 173
    invoke-static {v1}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/location/LocationManager;

    iput-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mLocationManager:Landroid/location/LocationManager;

    .line 175
    invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->registerPassiveLocationUpdates()V

    .line 176
    new-instance v1, Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    invoke-direct {v1, v0}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;-><init>(I)V

    iput-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->scheduledThreadPoolExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    .line 177
    invoke-virtual {v1, v0}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->setRemoveOnCancelPolicy(Z)V

    .line 178
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->scheduledThreadPoolExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->setExecuteExistingDelayedTasksAfterShutdownPolicy(Z)V

    .line 179
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->scheduledThreadPoolExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->setContinueExistingPeriodicTasksAfterShutdownPolicy(Z)V

    .line 180
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mContext:Landroid/content/Context;

    .line 181
    const-string/jumbo v2, "sensor"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    .line 180
    invoke-static {v0}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mSensorManager:Landroid/hardware/SensorManager;

    .line 182
    const/16 v2, 0x13

    invoke-virtual {v0, v2, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(IZ)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mUserStepsDetector:Landroid/hardware/Sensor;

    .line 183
    invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->registerUserStepsListener()V

    .line 184
    return-void
.end method

.method public removeLocationRequestId(I)V
    .locals 8
    .param p1, "listenerHashCode"    # I

    .line 488
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mNlpRequestMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    const-string v2, "Gpo5Client"

    if-eqz v0, :cond_0

    .line 489
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "removeNLP: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3, v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 490
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mNlpRequestMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 492
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGlpRequestMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 493
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "removeGLP: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3, v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 494
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGlpRequestMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;

    .line 495
    .local v0, "recorder":Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;
    iget-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGlpRequestMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 496
    if-eqz v0, :cond_2

    .line 497
    invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;->getUid()I

    move-result v1

    .line 498
    .local v1, "uid":I
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;->getNlpReturnTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 499
    .local v2, "time":J
    invoke-static {}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->getInstance()Lcom/android/server/location/gnss/GnssEventTrackingStub;

    move-result-object v4

    iget-object v5, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mNavAppInfo:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 500
    const-wide/16 v6, 0xbb8

    cmp-long v6, v2, v6

    if-ltz v6, :cond_1

    move-wide v6, v2

    goto :goto_0

    :cond_1
    const-wide/16 v6, 0x0

    .line 499
    :goto_0
    invoke-interface {v4, v5, v6, v7}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->recordNavAppTime(Ljava/lang/String;J)V

    .line 501
    iget-object v4, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mNavAppInfo:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 502
    .end local v1    # "uid":I
    .end local v2    # "time":J
    goto :goto_1

    .line 503
    :cond_2
    iget-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mNavAppInfo:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 506
    .end local v0    # "recorder":Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;
    :cond_3
    :goto_1
    return-void
.end method

.method public reportLocation2Gpo(Landroid/location/Location;)V
    .locals 4
    .param p1, "location"    # Landroid/location/Location;

    .line 540
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 541
    .local v0, "time":J
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/location/Location;->isComplete()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Landroid/location/Location;->getSpeed()F

    move-result v2

    const/high16 v3, 0x40400000    # 3.0f

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_0

    .line 542
    iget-object v2, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->mLastGnssTrafficTime:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2, v0, v1}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 544
    :cond_0
    invoke-static {}, Lcom/android/server/location/gnss/hal/GnssScoringModelStub;->getInstance()Lcom/android/server/location/gnss/hal/GnssScoringModelStub;

    move-result-object v2

    invoke-interface {v2, v0, v1}, Lcom/android/server/location/gnss/hal/GnssScoringModelStub;->updateFixTime(J)V

    .line 545
    return-void
.end method

.method public saveLocationRequestId(ILjava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V
    .locals 18
    .param p1, "uid"    # I
    .param p2, "pkn"    # Ljava/lang/String;
    .param p3, "provider"    # Ljava/lang/String;
    .param p4, "listenerHashCode"    # I
    .param p5, "callbackType"    # Ljava/lang/Object;

    .line 345
    move-object/from16 v9, p0

    move-object/from16 v10, p3

    move/from16 v11, p4

    invoke-direct/range {p0 .. p0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->checkFeatureSwitch()V

    .line 346
    iget-object v0, v9, Lcom/android/server/location/gnss/hal/Gpo5Client;->isFeatureEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 347
    :cond_0
    const-string v0, "network"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    const/4 v12, 0x0

    const-string v13, "Gpo5Client"

    if-eqz v0, :cond_1

    iget-object v0, v9, Lcom/android/server/location/gnss/hal/Gpo5Client;->mNlpRequestMap:Ljava/util/Map;

    .line 348
    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 349
    iget-object v0, v9, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "saveNLP: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v13, v1, v12}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 350
    iget-object v14, v9, Lcom/android/server/location/gnss/hal/Gpo5Client;->mNlpRequestMap:Ljava/util/Map;

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    new-instance v7, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;

    const/4 v6, 0x0

    const-wide/16 v16, 0x0

    move-object v0, v7

    move-object/from16 v1, p0

    move/from16 v2, p1

    move-object/from16 v3, p3

    move/from16 v4, p4

    move-object/from16 v5, p5

    move-object v12, v7

    move-wide/from16 v7, v16

    invoke-direct/range {v0 .. v8}, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;-><init>(Lcom/android/server/location/gnss/hal/Gpo5Client;ILjava/lang/String;ILjava/lang/Object;ZJ)V

    invoke-interface {v14, v15, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 353
    :cond_1
    const-string v0, "gps"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, v9, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGlpRequestMap:Ljava/util/Map;

    .line 354
    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 356
    iget-object v0, v9, Lcom/android/server/location/gnss/hal/Gpo5Client;->mInitialSteps:Ljava/util/concurrent/atomic/AtomicInteger;

    iget-object v1, v9, Lcom/android/server/location/gnss/hal/Gpo5Client;->mCurrentSteps:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 357
    iget-object v0, v9, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "saveGLP: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v13, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 358
    iget-object v0, v9, Lcom/android/server/location/gnss/hal/Gpo5Client;->mNavAppInfo:Ljava/util/Map;

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v12, p2

    invoke-interface {v0, v1, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 359
    iget-object v13, v9, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGlpRequestMap:Ljava/util/Map;

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    new-instance v15, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;

    const/4 v6, 0x0

    const-wide/16 v7, 0x0

    move-object v0, v15

    move-object/from16 v1, p0

    move/from16 v2, p1

    move-object/from16 v3, p3

    move/from16 v4, p4

    move-object/from16 v5, p5

    invoke-direct/range {v0 .. v8}, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;-><init>(Lcom/android/server/location/gnss/hal/Gpo5Client;ILjava/lang/String;ILjava/lang/Object;ZJ)V

    invoke-interface {v13, v14, v15}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 361
    iget-object v0, v9, Lcom/android/server/location/gnss/hal/Gpo5Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->getEngineStatus()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 362
    iget-object v0, v9, Lcom/android/server/location/gnss/hal/Gpo5Client;->mLastNlpLocation:Landroid/location/Location;

    if-eqz v0, :cond_2

    filled-new-array {v0}, [Landroid/location/Location;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    move-object/from16 v1, p5

    invoke-direct {v9, v1, v0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->delieverLocation(Ljava/lang/Object;Ljava/util/List;)V

    goto :goto_0

    :cond_2
    move-object/from16 v1, p5

    .line 363
    :goto_0
    return-void

    .line 365
    :cond_3
    move-object/from16 v1, p5

    iget-object v0, v9, Lcom/android/server/location/gnss/hal/Gpo5Client;->scheduledThreadPoolExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    new-instance v2, Lcom/android/server/location/gnss/hal/Gpo5Client$$ExternalSyntheticLambda3;

    move/from16 v3, p1

    invoke-direct {v2, v9, v11, v3}, Lcom/android/server/location/gnss/hal/Gpo5Client$$ExternalSyntheticLambda3;-><init>(Lcom/android/server/location/gnss/hal/Gpo5Client;II)V

    const-wide/16 v4, 0xfa

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v4, v5, v6}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    goto :goto_1

    .line 354
    :cond_4
    move/from16 v3, p1

    move-object/from16 v12, p2

    move-object/from16 v1, p5

    goto :goto_1

    .line 353
    :cond_5
    move/from16 v3, p1

    move-object/from16 v12, p2

    move-object/from16 v1, p5

    .line 387
    :goto_1
    return-void
.end method

.method public updateGnssStatus(I)V
    .locals 1
    .param p1, "status"    # I

    .line 234
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->isScreenOn:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 235
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->isUserBehaviorSensorRegistered:Z

    if-eqz v0, :cond_0

    .line 237
    invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->unregisterUserBehaviorListener()V

    .line 239
    :cond_0
    return-void
.end method

.method public updateScreenState(Z)V
    .locals 1
    .param p1, "on"    # Z

    .line 309
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->isScreenOn:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 310
    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->registerUserBehaviorListener()V

    goto :goto_0

    .line 311
    :cond_0
    invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->unregisterUserBehaviorListener()V

    .line 312
    :goto_0
    return-void
.end method
