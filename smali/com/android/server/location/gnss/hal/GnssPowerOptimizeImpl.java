public class com.android.server.location.gnss.hal.GnssPowerOptimizeImpl implements com.android.server.location.gnss.hal.GnssPowerOptimizeStub {
	 /* .source "GnssPowerOptimizeImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final java.lang.String GPO_VERSION_KEY;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private Boolean isFeatureSupport;
	 private android.content.Context mContext;
	 private com.android.server.location.gnss.hal.Gpo4Client mGpo4Client;
	 private com.android.server.location.gnss.hal.Gpo5Client mGpo5Client;
	 private final com.android.server.location.gnss.hal.GpoUtil mGpoUtil;
	 private Integer mGpoVersion;
	 private android.os.Handler mHandler;
	 private final com.android.server.location.gnss.hal.GpoUtil$IDataEvent mIDataEvent;
	 /* # direct methods */
	 static android.content.Context -$$Nest$fgetmContext ( com.android.server.location.gnss.hal.GnssPowerOptimizeImpl p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 p0 = this.mContext;
	 } // .end method
	 static com.android.server.location.gnss.hal.Gpo4Client -$$Nest$fgetmGpo4Client ( com.android.server.location.gnss.hal.GnssPowerOptimizeImpl p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 p0 = this.mGpo4Client;
	 } // .end method
	 static com.android.server.location.gnss.hal.Gpo5Client -$$Nest$fgetmGpo5Client ( com.android.server.location.gnss.hal.GnssPowerOptimizeImpl p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 p0 = this.mGpo5Client;
	 } // .end method
	 static com.android.server.location.gnss.hal.GpoUtil -$$Nest$fgetmGpoUtil ( com.android.server.location.gnss.hal.GnssPowerOptimizeImpl p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 p0 = this.mGpoUtil;
	 } // .end method
	 static Integer -$$Nest$fgetmGpoVersion ( com.android.server.location.gnss.hal.GnssPowerOptimizeImpl p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* iget p0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpoVersion:I */
	 } // .end method
	 static void -$$Nest$mcontrastValue ( com.android.server.location.gnss.hal.GnssPowerOptimizeImpl p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->contrastValue()V */
		 return;
	 } // .end method
	 public com.android.server.location.gnss.hal.GnssPowerOptimizeImpl ( ) {
		 /* .locals 2 */
		 /* .line 21 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 26 */
		 com.android.server.location.gnss.hal.GpoUtil .getInstance ( );
		 this.mGpoUtil = v0;
		 /* .line 27 */
		 int v0 = 0; // const/4 v0, 0x0
		 this.mGpo5Client = v0;
		 /* .line 28 */
		 this.mGpo4Client = v0;
		 /* .line 30 */
		 /* new-instance v0, Landroid/os/Handler; */
		 android.os.Looper .getMainLooper ( );
		 /* invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
		 this.mHandler = v0;
		 /* .line 32 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* iput-boolean v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->isFeatureSupport:Z */
		 /* .line 33 */
		 /* iput v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpoVersion:I */
		 /* .line 35 */
		 /* new-instance v0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl$1; */
		 /* invoke-direct {v0, p0}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl$1;-><init>(Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;)V */
		 this.mIDataEvent = v0;
		 return;
	 } // .end method
	 private void contrastValue ( ) {
		 /* .locals 3 */
		 /* .line 121 */
		 v0 = this.mGpoUtil;
		 final String v1 = "GnssPowerOptimize"; // const-string v1, "GnssPowerOptimize"
		 final String v2 = "contrastValue"; // const-string v2, "contrastValue"
		 (( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logv ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
		 /* .line 122 */
		 v0 = this.mContext;
		 (( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
		 /* .line 123 */
		 /* .local v0, "resolver":Landroid/content/ContentResolver; */
		 /* const-string/jumbo v1, "select_gpo_version" */
		 int v2 = 0; // const/4 v2, 0x0
		 v1 = 		 android.provider.Settings$Global .getInt ( v0,v1,v2 );
		 /* .line 124 */
		 /* .local v1, "newValue":I */
		 /* iget v2, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpoVersion:I */
		 /* if-eq v2, v1, :cond_0 */
		 /* .line 125 */
		 (( com.android.server.location.gnss.hal.GnssPowerOptimizeImpl ) p0 ).setGpoVersionValue ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->setGpoVersionValue(I)V
		 /* .line 127 */
	 } // :cond_0
	 return;
} // .end method
private void deinitGpo4 ( ) {
	 /* .locals 1 */
	 /* .line 139 */
	 v0 = this.mGpo4Client;
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 140 */
		 (( com.android.server.location.gnss.hal.Gpo4Client ) v0 ).deinit ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/Gpo4Client;->deinit()V
		 /* .line 141 */
		 int v0 = 0; // const/4 v0, 0x0
		 this.mGpo4Client = v0;
		 /* .line 143 */
	 } // :cond_0
	 return;
} // .end method
private void deinitGpo5 ( ) {
	 /* .locals 2 */
	 /* .line 130 */
	 v0 = this.mGpo5Client;
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 131 */
		 (( com.android.server.location.gnss.hal.Gpo5Client ) v0 ).deinit ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->deinit()V
		 /* .line 132 */
		 int v0 = 0; // const/4 v0, 0x0
		 this.mGpo5Client = v0;
		 /* .line 135 */
	 } // :cond_0
	 com.android.server.location.gnss.hal.GnssScoringModelStub .getInstance ( );
	 int v1 = 0; // const/4 v1, 0x0
	 /* .line 136 */
	 return;
} // .end method
private void initOnce ( android.content.Context p0 ) {
	 /* .locals 5 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .line 105 */
	 v0 = this.mContext;
	 /* if-nez v0, :cond_0 */
	 /* .line 106 */
	 v0 = this.mGpoUtil;
	 final String v1 = "GnssPowerOptimize"; // const-string v1, "GnssPowerOptimize"
	 final String v2 = "initOnce"; // const-string v2, "initOnce"
	 (( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logv ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
	 /* .line 107 */
	 this.mContext = p1;
	 /* .line 109 */
	 (( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
	 /* .line 110 */
	 /* const-string/jumbo v1, "select_gpo_version" */
	 android.provider.Settings$Global .getUriFor ( v1 );
	 /* new-instance v2, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl$2; */
	 v3 = this.mHandler;
	 /* invoke-direct {v2, p0, v3}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl$2;-><init>(Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;Landroid/os/Handler;)V */
	 /* .line 109 */
	 int v3 = 1; // const/4 v3, 0x1
	 int v4 = -1; // const/4 v4, -0x1
	 (( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
	 /* .line 118 */
} // :cond_0
return;
} // .end method
/* # virtual methods */
public Boolean blockEngineStart ( ) {
/* .locals 4 */
/* .line 180 */
/* iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->isFeatureSupport:Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 181 */
} // :cond_0
v0 = this.mGpoUtil;
final String v2 = "GnssPowerOptimize"; // const-string v2, "GnssPowerOptimize"
final String v3 = "blockEngineStart"; // const-string v3, "blockEngineStart"
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logv ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 182 */
v0 = this.mGpo5Client;
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = (( com.android.server.location.gnss.hal.Gpo5Client ) v0 ).blockEngineStart ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->blockEngineStart()Z
/* .line 183 */
} // :cond_1
v0 = this.mGpo4Client;
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = (( com.android.server.location.gnss.hal.Gpo4Client ) v0 ).blockEngineStart ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/Gpo4Client;->blockEngineStart()Z
if ( v0 != null) { // if-eqz v0, :cond_2
	 int v1 = 1; // const/4 v1, 0x1
} // :cond_2
} // .end method
public void clearLocationRequest ( ) {
/* .locals 3 */
/* .line 188 */
/* iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->isFeatureSupport:Z */
/* if-nez v0, :cond_0 */
return;
/* .line 189 */
} // :cond_0
v0 = this.mGpoUtil;
final String v1 = "GnssPowerOptimize"; // const-string v1, "GnssPowerOptimize"
final String v2 = "clearLocationRequest"; // const-string v2, "clearLocationRequest"
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logv ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 190 */
v0 = this.mGpo5Client;
if ( v0 != null) { // if-eqz v0, :cond_1
(( com.android.server.location.gnss.hal.Gpo5Client ) v0 ).clearLocationRequest ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->clearLocationRequest()V
/* .line 191 */
} // :cond_1
v0 = this.mGpo4Client;
if ( v0 != null) { // if-eqz v0, :cond_2
(( com.android.server.location.gnss.hal.Gpo4Client ) v0 ).clearLocationRequest ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/Gpo4Client;->clearLocationRequest()V
/* .line 192 */
} // :cond_2
} // :goto_0
return;
} // .end method
public void disableGnssSwitch ( ) {
/* .locals 3 */
/* .line 147 */
/* iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->isFeatureSupport:Z */
/* if-nez v0, :cond_0 */
return;
/* .line 148 */
} // :cond_0
v0 = this.mGpoUtil;
final String v1 = "GnssPowerOptimize"; // const-string v1, "GnssPowerOptimize"
final String v2 = "disableGnssSwitch"; // const-string v2, "disableGnssSwitch"
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logv ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 149 */
v0 = this.mGpo5Client;
if ( v0 != null) { // if-eqz v0, :cond_1
(( com.android.server.location.gnss.hal.Gpo5Client ) v0 ).disableGnssSwitch ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->disableGnssSwitch()V
/* .line 150 */
} // :cond_1
v0 = this.mGpo4Client;
if ( v0 != null) { // if-eqz v0, :cond_2
(( com.android.server.location.gnss.hal.Gpo4Client ) v0 ).disableGnssSwitch ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/Gpo4Client;->disableGnssSwitch()V
/* .line 151 */
} // :cond_2
} // :goto_0
return;
} // .end method
public Boolean engineStoppedByGpo ( ) {
/* .locals 3 */
/* .line 220 */
/* iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->isFeatureSupport:Z */
/* if-nez v0, :cond_0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 221 */
} // :cond_0
v0 = this.mGpoUtil;
final String v1 = "GnssPowerOptimize"; // const-string v1, "GnssPowerOptimize"
final String v2 = "engineStoppedByGpo"; // const-string v2, "engineStoppedByGpo"
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logv ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 222 */
v0 = this.mGpoUtil;
v0 = (( com.android.server.location.gnss.hal.GpoUtil ) v0 ).engineStoppedByGpo ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->engineStoppedByGpo()Z
} // .end method
public Integer getEngineStatus ( ) {
/* .locals 1 */
/* .line 209 */
v0 = this.mGpoUtil;
v0 = (( com.android.server.location.gnss.hal.GpoUtil ) v0 ).getEngineStatus ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->getEngineStatus()I
} // .end method
public void init ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 65 */
final String v0 = "GnssPowerOptimize"; // const-string v0, "GnssPowerOptimize"
/* if-nez p1, :cond_0 */
/* .line 66 */
v1 = this.mGpoUtil;
final String v2 = "init transfer null context"; // const-string v2, "init transfer null context"
(( com.android.server.location.gnss.hal.GpoUtil ) v1 ).loge ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->loge(Ljava/lang/String;Ljava/lang/String;)V
/* .line 67 */
return;
/* .line 69 */
} // :cond_0
v1 = this.mGpoUtil;
final String v2 = "init "; // const-string v2, "init "
(( com.android.server.location.gnss.hal.GpoUtil ) v1 ).logv ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 70 */
/* invoke-direct {p0, p1}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->initOnce(Landroid/content/Context;)V */
/* .line 71 */
v1 = this.mGpoUtil;
v2 = this.mContext;
v3 = this.mIDataEvent;
(( com.android.server.location.gnss.hal.GpoUtil ) v1 ).initOnce ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lcom/android/server/location/gnss/hal/GpoUtil;->initOnce(Landroid/content/Context;Lcom/android/server/location/gnss/hal/GpoUtil$IDataEvent;)V
/* .line 72 */
v1 = this.mGpoUtil;
v1 = (( com.android.server.location.gnss.hal.GpoUtil ) v1 ).getGpoVersion ( ); // invoke-virtual {v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->getGpoVersion()I
/* iput v1, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpoVersion:I */
/* .line 73 */
v1 = this.mGpoUtil;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Gpo Version: "; // const-string v3, "Gpo Version: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpoVersion:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v3 = 1; // const/4 v3, 0x1
(( com.android.server.location.gnss.hal.GpoUtil ) v1 ).logi ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V
/* .line 74 */
/* iget v1, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpoVersion:I */
/* packed-switch v1, :pswitch_data_0 */
/* .line 96 */
v1 = this.mGpoUtil;
final String v2 = "Not Support GPO."; // const-string v2, "Not Support GPO."
(( com.android.server.location.gnss.hal.GpoUtil ) v1 ).logv ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 97 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->isFeatureSupport:Z */
/* .line 98 */
/* invoke-direct {p0}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->deinitGpo5()V */
/* .line 99 */
/* invoke-direct {p0}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->deinitGpo4()V */
/* .line 76 */
/* :pswitch_0 */
/* iput-boolean v3, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->isFeatureSupport:Z */
/* .line 77 */
com.android.server.location.gnss.hal.Gpo5Client .getInstance ( );
this.mGpo5Client = v1;
/* .line 78 */
v2 = this.mContext;
(( com.android.server.location.gnss.hal.Gpo5Client ) v1 ).init ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/location/gnss/hal/Gpo5Client;->init(Landroid/content/Context;)V
/* .line 79 */
v1 = this.mGpo5Client;
v1 = (( com.android.server.location.gnss.hal.Gpo5Client ) v1 ).checkSensorSupport ( ); // invoke-virtual {v1}, Lcom/android/server/location/gnss/hal/Gpo5Client;->checkSensorSupport()Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 81 */
com.android.server.location.gnss.hal.GnssScoringModelStub .getInstance ( );
/* .line 82 */
/* invoke-direct {p0}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->deinitGpo4()V */
/* .line 83 */
/* .line 86 */
} // :cond_1
/* iget v1, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpoVersion:I */
/* sub-int/2addr v1, v3 */
/* iput v1, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpoVersion:I */
/* .line 87 */
v1 = this.mGpoUtil;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Sensor do not support, downgrade to Version: "; // const-string v4, "Sensor do not support, downgrade to Version: "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpoVersion:I */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.android.server.location.gnss.hal.GpoUtil ) v1 ).logi ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V
/* .line 88 */
/* invoke-direct {p0}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->deinitGpo5()V */
/* .line 90 */
/* :pswitch_1 */
/* iput-boolean v3, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->isFeatureSupport:Z */
/* .line 91 */
com.android.server.location.gnss.hal.Gpo4Client .getInstance ( );
this.mGpo4Client = v0;
/* .line 92 */
v1 = this.mContext;
(( com.android.server.location.gnss.hal.Gpo4Client ) v0 ).init ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/location/gnss/hal/Gpo4Client;->init(Landroid/content/Context;)V
/* .line 93 */
/* invoke-direct {p0}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->deinitGpo5()V */
/* .line 94 */
/* nop */
/* .line 101 */
} // :goto_0
return;
/* :pswitch_data_0 */
/* .packed-switch 0x4 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public Boolean isBlackListControlEnabled ( ) {
/* .locals 2 */
/* .line 155 */
/* iget v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpoVersion:I */
int v1 = 2; // const/4 v1, 0x2
/* if-ne v0, v1, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public void recordEngineUsageDaily ( Long p0 ) {
/* .locals 3 */
/* .param p1, "time" # J */
/* .line 227 */
/* iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->isFeatureSupport:Z */
/* if-nez v0, :cond_0 */
return;
/* .line 228 */
} // :cond_0
v0 = this.mGpoUtil;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "recordEngineUsageDaily: "; // const-string v2, "recordEngineUsageDaily: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1, p2 ); // invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "GnssPowerOptimize"; // const-string v2, "GnssPowerOptimize"
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logv ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 229 */
v0 = this.mGpoUtil;
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).recordEngineUsageDaily ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/location/gnss/hal/GpoUtil;->recordEngineUsageDaily(J)V
/* .line 230 */
return;
} // .end method
public void removeLocationRequestId ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "listenerHashCode" # I */
/* .line 172 */
/* iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->isFeatureSupport:Z */
/* if-nez v0, :cond_0 */
return;
/* .line 173 */
} // :cond_0
v0 = this.mGpoUtil;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "removeLocationRequestId: "; // const-string v2, "removeLocationRequestId: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "GnssPowerOptimize"; // const-string v2, "GnssPowerOptimize"
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logv ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 174 */
v0 = this.mGpo5Client;
if ( v0 != null) { // if-eqz v0, :cond_1
(( com.android.server.location.gnss.hal.Gpo5Client ) v0 ).removeLocationRequestId ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/location/gnss/hal/Gpo5Client;->removeLocationRequestId(I)V
/* .line 175 */
} // :cond_1
v0 = this.mGpo4Client;
if ( v0 != null) { // if-eqz v0, :cond_2
(( com.android.server.location.gnss.hal.Gpo4Client ) v0 ).removeLocationRequestId ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/location/gnss/hal/Gpo4Client;->removeLocationRequestId(I)V
/* .line 176 */
} // :cond_2
} // :goto_0
return;
} // .end method
public void reportLocation2Gpo ( android.location.Location p0 ) {
/* .locals 3 */
/* .param p1, "location" # Landroid/location/Location; */
/* .line 196 */
/* iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->isFeatureSupport:Z */
/* if-nez v0, :cond_0 */
return;
/* .line 197 */
} // :cond_0
v0 = this.mGpoUtil;
final String v1 = "GnssPowerOptimize"; // const-string v1, "GnssPowerOptimize"
final String v2 = "reportLocation2Gpo"; // const-string v2, "reportLocation2Gpo"
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logv ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 198 */
v0 = this.mGpo5Client;
if ( v0 != null) { // if-eqz v0, :cond_1
(( com.android.server.location.gnss.hal.Gpo5Client ) v0 ).reportLocation2Gpo ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/location/gnss/hal/Gpo5Client;->reportLocation2Gpo(Landroid/location/Location;)V
/* .line 199 */
} // :cond_1
v0 = this.mGpo4Client;
if ( v0 != null) { // if-eqz v0, :cond_2
(( com.android.server.location.gnss.hal.Gpo4Client ) v0 ).reportLocation2Gpo ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/location/gnss/hal/Gpo4Client;->reportLocation2Gpo(Landroid/location/Location;)V
/* .line 200 */
} // :cond_2
} // :goto_0
return;
} // .end method
public void saveLocationRequestId ( Integer p0, java.lang.String p1, java.lang.String p2, Integer p3, java.lang.Object p4 ) {
/* .locals 10 */
/* .param p1, "uid" # I */
/* .param p2, "pkn" # Ljava/lang/String; */
/* .param p3, "provider" # Ljava/lang/String; */
/* .param p4, "listenerHashCode" # I */
/* .param p5, "callbackType" # Ljava/lang/Object; */
/* .line 161 */
/* iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->isFeatureSupport:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
final String v0 = "passive"; // const-string v0, "passive"
v0 = (( java.lang.String ) v0 ).equalsIgnoreCase ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 162 */
} // :cond_0
v0 = this.mGpoUtil;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "saveLocationRequestId: "; // const-string v2, "saveLocationRequestId: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p4 ); // invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ", "; // const-string v2, ", "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "GnssPowerOptimize"; // const-string v2, "GnssPowerOptimize"
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logv ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 163 */
v3 = this.mGpo5Client;
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 164 */
/* move v4, p1 */
/* move-object v5, p2 */
/* move-object v6, p3 */
/* move v7, p4 */
/* move-object v8, p5 */
/* invoke-virtual/range {v3 ..v8}, Lcom/android/server/location/gnss/hal/Gpo5Client;->saveLocationRequestId(ILjava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V */
/* .line 165 */
} // :cond_1
v4 = this.mGpo4Client;
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 166 */
/* move v5, p1 */
/* move-object v6, p2 */
/* move-object v7, p3 */
/* move v8, p4 */
/* move-object v9, p5 */
/* invoke-virtual/range {v4 ..v9}, Lcom/android/server/location/gnss/hal/Gpo4Client;->saveLocationRequestId(ILjava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V */
/* .line 168 */
} // :cond_2
} // :goto_0
return;
/* .line 161 */
} // :cond_3
} // :goto_1
return;
} // .end method
public void setEngineStatus ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "status" # I */
/* .line 214 */
v0 = this.mGpoUtil;
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).setEngineStatus ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/location/gnss/hal/GpoUtil;->setEngineStatus(I)V
/* .line 215 */
return;
} // .end method
public void setGpoVersionValue ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "newValue" # I */
/* .line 204 */
v0 = this.mGpoUtil;
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).setGpoVersionValue ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/location/gnss/hal/GpoUtil;->setGpoVersionValue(I)V
/* .line 205 */
return;
} // .end method
