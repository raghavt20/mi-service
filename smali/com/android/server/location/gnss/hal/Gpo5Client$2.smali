.class Lcom/android/server/location/gnss/hal/Gpo5Client$2;
.super Ljava/lang/Object;
.source "Gpo5Client.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/location/gnss/hal/Gpo5Client;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/location/gnss/hal/Gpo5Client;


# direct methods
.method constructor <init>(Lcom/android/server/location/gnss/hal/Gpo5Client;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/location/gnss/hal/Gpo5Client;

    .line 119
    iput-object p1, p0, Lcom/android/server/location/gnss/hal/Gpo5Client$2;->this$0:Lcom/android/server/location/gnss/hal/Gpo5Client;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .line 148
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .line 123
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client$2;->this$0:Lcom/android/server/location/gnss/hal/Gpo5Client;

    invoke-static {v0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->-$$Nest$fgetmCurrentSteps(Lcom/android/server/location/gnss/hal/Gpo5Client;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v2, 0x0

    aget v1, v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 125
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client$2;->this$0:Lcom/android/server/location/gnss/hal/Gpo5Client;

    invoke-static {v0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->-$$Nest$fgetisUserBehaviorSensorRegistered(Lcom/android/server/location/gnss/hal/Gpo5Client;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client$2;->this$0:Lcom/android/server/location/gnss/hal/Gpo5Client;

    invoke-static {v0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->-$$Nest$fgetmCurrentSteps(Lcom/android/server/location/gnss/hal/Gpo5Client;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    iget-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo5Client$2;->this$0:Lcom/android/server/location/gnss/hal/Gpo5Client;

    invoke-static {v1}, Lcom/android/server/location/gnss/hal/Gpo5Client;->-$$Nest$fgetmInitialSteps(Lcom/android/server/location/gnss/hal/Gpo5Client;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    sub-int/2addr v0, v1

    int-to-float v0, v0

    const/high16 v1, 0x40a00000    # 5.0f

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client$2;->this$0:Lcom/android/server/location/gnss/hal/Gpo5Client;

    invoke-static {v0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->-$$Nest$fgetmGpoUtil(Lcom/android/server/location/gnss/hal/Gpo5Client;)Lcom/android/server/location/gnss/hal/GpoUtil;

    move-result-object v0

    .line 126
    invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->getEngineStatus()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 128
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client$2;->this$0:Lcom/android/server/location/gnss/hal/Gpo5Client;

    invoke-static {v0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->-$$Nest$fgetmGpoUtil(Lcom/android/server/location/gnss/hal/Gpo5Client;)Lcom/android/server/location/gnss/hal/GpoUtil;

    move-result-object v0

    const-string v1, "User moves outside SmallArea."

    const/4 v2, 0x1

    const-string v3, "Gpo5Client"

    invoke-virtual {v0, v3, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 129
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client$2;->this$0:Lcom/android/server/location/gnss/hal/Gpo5Client;

    invoke-static {v0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->-$$Nest$mhandleLeaveSmallArea(Lcom/android/server/location/gnss/hal/Gpo5Client;)V

    .line 131
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client$2;->this$0:Lcom/android/server/location/gnss/hal/Gpo5Client;

    invoke-static {v0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->-$$Nest$fgetmInitialSteps(Lcom/android/server/location/gnss/hal/Gpo5Client;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo5Client$2;->this$0:Lcom/android/server/location/gnss/hal/Gpo5Client;

    invoke-static {v1}, Lcom/android/server/location/gnss/hal/Gpo5Client;->-$$Nest$fgetmCurrentSteps(Lcom/android/server/location/gnss/hal/Gpo5Client;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 134
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client$2;->this$0:Lcom/android/server/location/gnss/hal/Gpo5Client;

    invoke-static {v0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->-$$Nest$fgetmStepTimeRecorder(Lcom/android/server/location/gnss/hal/Gpo5Client;)Ljava/util/concurrent/BlockingDeque;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/BlockingDeque;->size()I

    move-result v0

    const/16 v1, 0x64

    if-lt v0, v1, :cond_1

    .line 135
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client$2;->this$0:Lcom/android/server/location/gnss/hal/Gpo5Client;

    invoke-static {v0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->-$$Nest$fgetmStepTimeRecorder(Lcom/android/server/location/gnss/hal/Gpo5Client;)Ljava/util/concurrent/BlockingDeque;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/BlockingDeque;->takeLast()Ljava/lang/Object;

    .line 137
    :cond_1
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client$2;->this$0:Lcom/android/server/location/gnss/hal/Gpo5Client;

    invoke-static {v0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->-$$Nest$fgetmStepTimeRecorder(Lcom/android/server/location/gnss/hal/Gpo5Client;)Ljava/util/concurrent/BlockingDeque;

    move-result-object v0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/BlockingDeque;->putFirst(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 142
    goto :goto_0

    .line 138
    :catch_0
    move-exception v0

    .line 139
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 141
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 143
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :goto_0
    return-void
.end method
