class com.android.server.location.gnss.hal.Gpo5Client$LocationRequestRecorder {
	 /* .source "Gpo5Client.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/location/gnss/hal/Gpo5Client; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "LocationRequestRecorder" */
} // .end annotation
/* # instance fields */
private java.lang.Object callbackType;
private Integer listenerHashCode;
private Long nlpReturnTime;
private Boolean nlpReturned;
private java.lang.String provider;
final com.android.server.location.gnss.hal.Gpo5Client this$0; //synthetic
private Integer uid;
/* # direct methods */
public com.android.server.location.gnss.hal.Gpo5Client$LocationRequestRecorder ( ) {
/* .locals 0 */
/* .param p2, "uid" # I */
/* .param p3, "provider" # Ljava/lang/String; */
/* .param p4, "listenerHashCode" # I */
/* .param p5, "callbackType" # Ljava/lang/Object; */
/* .param p6, "nlpReturned" # Z */
/* .param p7, "nlpReturnTime" # J */
/* .line 557 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 558 */
/* iput p2, p0, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;->uid:I */
/* .line 559 */
this.provider = p3;
/* .line 560 */
/* iput p4, p0, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;->listenerHashCode:I */
/* .line 561 */
this.callbackType = p5;
/* .line 562 */
/* iput-boolean p6, p0, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;->nlpReturned:Z */
/* .line 563 */
/* iput-wide p7, p0, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;->nlpReturnTime:J */
/* .line 564 */
return;
} // .end method
/* # virtual methods */
public Boolean existListenerHashCode ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "value" # I */
/* .line 579 */
/* iget v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;->listenerHashCode:I */
/* if-ne v0, p1, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean existUid ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "value" # I */
/* .line 567 */
/* iget v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;->uid:I */
/* if-ne v0, p1, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public java.lang.Object getCallbackType ( ) {
/* .locals 1 */
/* .line 583 */
v0 = this.callbackType;
} // .end method
public Long getNlpReturnTime ( ) {
/* .locals 2 */
/* .line 599 */
/* iget-wide v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;->nlpReturnTime:J */
/* return-wide v0 */
} // .end method
public Boolean getNlpReturned ( ) {
/* .locals 1 */
/* .line 591 */
/* iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;->nlpReturned:Z */
} // .end method
public java.lang.String getProvider ( ) {
/* .locals 1 */
/* .line 575 */
v0 = this.provider;
} // .end method
public Integer getUid ( ) {
/* .locals 1 */
/* .line 571 */
/* iget v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;->uid:I */
} // .end method
public void setNlpReturnTime ( Long p0 ) {
/* .locals 0 */
/* .param p1, "value" # J */
/* .line 595 */
/* iput-wide p1, p0, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;->nlpReturnTime:J */
/* .line 596 */
return;
} // .end method
public void setNlpReturned ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "value" # Z */
/* .line 587 */
/* iput-boolean p1, p0, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;->nlpReturned:Z */
/* .line 588 */
return;
} // .end method
