class com.android.server.location.gnss.hal.Gpo5Client$2 implements android.hardware.SensorEventListener {
	 /* .source "Gpo5Client.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/location/gnss/hal/Gpo5Client; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.location.gnss.hal.Gpo5Client this$0; //synthetic
/* # direct methods */
 com.android.server.location.gnss.hal.Gpo5Client$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/location/gnss/hal/Gpo5Client; */
/* .line 119 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onAccuracyChanged ( android.hardware.Sensor p0, Integer p1 ) {
/* .locals 0 */
/* .param p1, "sensor" # Landroid/hardware/Sensor; */
/* .param p2, "accuracy" # I */
/* .line 148 */
return;
} // .end method
public void onSensorChanged ( android.hardware.SensorEvent p0 ) {
/* .locals 4 */
/* .param p1, "event" # Landroid/hardware/SensorEvent; */
/* .line 123 */
v0 = this.this$0;
com.android.server.location.gnss.hal.Gpo5Client .-$$Nest$fgetmCurrentSteps ( v0 );
v1 = this.values;
int v2 = 0; // const/4 v2, 0x0
/* aget v1, v1, v2 */
/* float-to-int v1, v1 */
(( java.util.concurrent.atomic.AtomicInteger ) v0 ).set ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V
/* .line 125 */
v0 = this.this$0;
v0 = com.android.server.location.gnss.hal.Gpo5Client .-$$Nest$fgetisUserBehaviorSensorRegistered ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
	 v0 = this.this$0;
	 com.android.server.location.gnss.hal.Gpo5Client .-$$Nest$fgetmCurrentSteps ( v0 );
	 v0 = 	 (( java.util.concurrent.atomic.AtomicInteger ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I
	 v1 = this.this$0;
	 com.android.server.location.gnss.hal.Gpo5Client .-$$Nest$fgetmInitialSteps ( v1 );
	 v1 = 	 (( java.util.concurrent.atomic.AtomicInteger ) v1 ).get ( ); // invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I
	 /* sub-int/2addr v0, v1 */
	 /* int-to-float v0, v0 */
	 /* const/high16 v1, 0x40a00000 # 5.0f */
	 /* cmpl-float v0, v0, v1 */
	 /* if-ltz v0, :cond_0 */
	 v0 = this.this$0;
	 com.android.server.location.gnss.hal.Gpo5Client .-$$Nest$fgetmGpoUtil ( v0 );
	 /* .line 126 */
	 v0 = 	 (( com.android.server.location.gnss.hal.GpoUtil ) v0 ).getEngineStatus ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->getEngineStatus()I
	 int v1 = 4; // const/4 v1, 0x4
	 /* if-eq v0, v1, :cond_0 */
	 /* .line 128 */
	 v0 = this.this$0;
	 com.android.server.location.gnss.hal.Gpo5Client .-$$Nest$fgetmGpoUtil ( v0 );
	 final String v1 = "User moves outside SmallArea."; // const-string v1, "User moves outside SmallArea."
	 int v2 = 1; // const/4 v2, 0x1
	 final String v3 = "Gpo5Client"; // const-string v3, "Gpo5Client"
	 (( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logi ( v3, v1, v2 ); // invoke-virtual {v0, v3, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V
	 /* .line 129 */
	 v0 = this.this$0;
	 com.android.server.location.gnss.hal.Gpo5Client .-$$Nest$mhandleLeaveSmallArea ( v0 );
	 /* .line 131 */
	 v0 = this.this$0;
	 com.android.server.location.gnss.hal.Gpo5Client .-$$Nest$fgetmInitialSteps ( v0 );
	 v1 = this.this$0;
	 com.android.server.location.gnss.hal.Gpo5Client .-$$Nest$fgetmCurrentSteps ( v1 );
	 v1 = 	 (( java.util.concurrent.atomic.AtomicInteger ) v1 ).get ( ); // invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I
	 (( java.util.concurrent.atomic.AtomicInteger ) v0 ).set ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V
	 /* .line 134 */
} // :cond_0
try { // :try_start_0
	 v0 = this.this$0;
	 v0 = 	 com.android.server.location.gnss.hal.Gpo5Client .-$$Nest$fgetmStepTimeRecorder ( v0 );
	 /* const/16 v1, 0x64 */
	 /* if-lt v0, v1, :cond_1 */
	 /* .line 135 */
	 v0 = this.this$0;
	 com.android.server.location.gnss.hal.Gpo5Client .-$$Nest$fgetmStepTimeRecorder ( v0 );
	 /* .line 137 */
} // :cond_1
v0 = this.this$0;
com.android.server.location.gnss.hal.Gpo5Client .-$$Nest$fgetmStepTimeRecorder ( v0 );
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v1 */
java.lang.Long .valueOf ( v1,v2 );
/* :try_end_0 */
/* .catch Ljava/lang/InterruptedException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 142 */
/* .line 138 */
/* :catch_0 */
/* move-exception v0 */
/* .line 139 */
/* .local v0, "e":Ljava/lang/InterruptedException; */
(( java.lang.InterruptedException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V
/* .line 141 */
java.lang.Thread .currentThread ( );
(( java.lang.Thread ) v1 ).interrupt ( ); // invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V
/* .line 143 */
} // .end local v0 # "e":Ljava/lang/InterruptedException;
} // :goto_0
return;
} // .end method
