.class Lcom/android/server/location/gnss/hal/GpoUtil$2;
.super Landroid/content/BroadcastReceiver;
.source "GpoUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/location/gnss/hal/GpoUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/location/gnss/hal/GpoUtil;


# direct methods
.method constructor <init>(Lcom/android/server/location/gnss/hal/GpoUtil;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/location/gnss/hal/GpoUtil;

    .line 147
    iput-object p1, p0, Lcom/android/server/location/gnss/hal/GpoUtil$2;->this$0:Lcom/android/server/location/gnss/hal/GpoUtil;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 150
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GpoUtil$2;->this$0:Lcom/android/server/location/gnss/hal/GpoUtil;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->-$$Nest$fputisUserUnlocked(Lcom/android/server/location/gnss/hal/GpoUtil;Z)V

    .line 151
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GpoUtil$2;->this$0:Lcom/android/server/location/gnss/hal/GpoUtil;

    invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->readEngineUsageData()V

    .line 152
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GpoUtil$2;->this$0:Lcom/android/server/location/gnss/hal/GpoUtil;

    invoke-static {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->-$$Nest$fgetmContext(Lcom/android/server/location/gnss/hal/GpoUtil;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/location/gnss/hal/GpoUtil$2;->this$0:Lcom/android/server/location/gnss/hal/GpoUtil;

    invoke-static {v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->-$$Nest$fgetmUserUnlockReceiver(Lcom/android/server/location/gnss/hal/GpoUtil;)Landroid/content/BroadcastReceiver;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 153
    return-void
.end method
