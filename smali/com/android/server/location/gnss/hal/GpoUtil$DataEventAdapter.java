class com.android.server.location.gnss.hal.GpoUtil$DataEventAdapter {
	 /* .source "GpoUtil.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/location/gnss/hal/GpoUtil; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "DataEventAdapter" */
} // .end annotation
/* # instance fields */
private com.android.server.location.gnss.hal.GpoUtil$IDataEvent iDataEvent;
final com.android.server.location.gnss.hal.GpoUtil this$0; //synthetic
/* # direct methods */
private com.android.server.location.gnss.hal.GpoUtil$DataEventAdapter ( ) {
/* .locals 0 */
/* .line 341 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
 com.android.server.location.gnss.hal.GpoUtil$DataEventAdapter ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/location/gnss/hal/GpoUtil$DataEventAdapter;-><init>(Lcom/android/server/location/gnss/hal/GpoUtil;)V */
return;
} // .end method
/* # virtual methods */
public void attachEventListener ( com.android.server.location.gnss.hal.GpoUtil$IDataEvent p0 ) {
/* .locals 0 */
/* .param p1, "listener" # Lcom/android/server/location/gnss/hal/GpoUtil$IDataEvent; */
/* .line 345 */
this.iDataEvent = p1;
/* .line 346 */
return;
} // .end method
public void notifyDefaultNetwork ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "type" # I */
/* .line 361 */
v0 = this.iDataEvent;
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 362 */
	 /* .line 364 */
} // :cond_0
return;
} // .end method
public void notifyGnssStatus ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "status" # I */
/* .line 367 */
v0 = this.iDataEvent;
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 368 */
	 /* .line 370 */
} // :cond_0
return;
} // .end method
public void notifyPersistUpdates ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "version" # I */
/* .line 349 */
v0 = this.iDataEvent;
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 350 */
	 /* .line 352 */
} // :cond_0
return;
} // .end method
public void notifyScreenState ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "on" # Z */
/* .line 355 */
v0 = this.iDataEvent;
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 356 */
	 /* .line 358 */
} // :cond_0
return;
} // .end method
