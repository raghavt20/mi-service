.class public Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;
.super Ljava/lang/Object;
.source "GnssScoringModelImpl.java"

# interfaces
.implements Lcom/android/server/location/gnss/hal/GnssScoringModelStub;


# static fields
.field private static final IGNORE_RUNNING_TIME:J = 0x493e0L

.field private static final NO_FIELD_SCORE:I = 0x4

.field private static final NO_FIELD_TIME:J = 0x4a38L

.field private static final TAG:Ljava/lang/String; = "GnssScoringModel"

.field private static final WEEK_FIELD_TIME:J = 0xee48L


# instance fields
.field private mFeatureSwitch:Z

.field private mGpo5Client:Lcom/android/server/location/gnss/hal/Gpo5Client;

.field private final mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

.field private mLastLocationTime:J

.field private mModelRunning:Z

.field private mStartTime:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    invoke-static {}, Lcom/android/server/location/gnss/hal/GpoUtil;->getInstance()Lcom/android/server/location/gnss/hal/GpoUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    .line 33
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mStartTime:J

    .line 34
    iput-wide v0, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mLastLocationTime:J

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mFeatureSwitch:Z

    .line 36
    iput-boolean v0, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mModelRunning:Z

    return-void
.end method

.method private setLimitTime(I)J
    .locals 6
    .param p1, "score"    # I

    .line 98
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The score is : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GnssScoringModel"

    invoke-virtual {v0, v2, v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    const/4 v0, 0x4

    if-gt p1, v0, :cond_0

    .line 101
    const-wide/16 v0, 0x4a38

    .local v0, "limitTime":J
    goto :goto_0

    .line 103
    .end local v0    # "limitTime":J
    :cond_0
    const-wide/32 v0, 0xee48

    .line 105
    .restart local v0    # "limitTime":J
    :goto_0
    iget-object v3, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "The Limit time is :  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    return-wide v0
.end method

.method private switchEngineStateWithScore(IJ)V
    .locals 6
    .param p1, "score"    # I
    .param p2, "time"    # J

    .line 76
    invoke-direct {p0, p1}, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->setLimitTime(I)J

    move-result-wide v0

    cmp-long v0, p2, v0

    if-ltz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->getEngineStatus()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 81
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->getCurUserBehav()I

    move-result v0

    const/4 v1, 0x4

    const/4 v2, 0x1

    const-string v3, "GnssScoringModel"

    if-ne v0, v1, :cond_0

    .line 82
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    const-string v1, "Re-run Scoring Model."

    invoke-virtual {v0, v3, v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    invoke-virtual {p0, v2}, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->startScoringModel(Z)V

    .line 84
    return-void

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->doStopEngineByInstance()V

    .line 87
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Gnss Score: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", using time: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-wide/16 v4, 0x3e8

    div-long v4, p2, v4

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 95
    :cond_1
    return-void
.end method


# virtual methods
.method public init(Z)V
    .locals 4
    .param p1, "state"    # Z

    .line 40
    iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mFeatureSwitch:Z

    if-ne v0, p1, :cond_0

    return-void

    .line 41
    :cond_0
    iput-boolean p1, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mFeatureSwitch:Z

    .line 42
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "set GnssScoringModel running: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    const-string v3, "GnssScoringModel"

    invoke-virtual {v0, v3, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 43
    invoke-static {}, Lcom/android/server/location/gnss/hal/Gpo5Client;->getInstance()Lcom/android/server/location/gnss/hal/Gpo5Client;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mGpo5Client:Lcom/android/server/location/gnss/hal/Gpo5Client;

    .line 44
    return-void
.end method

.method public reportSvStatus2Score([F)V
    .locals 6
    .param p1, "basebandCn0DbHzs"    # [F

    .line 67
    iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mFeatureSwitch:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mModelRunning:Z

    if-nez v0, :cond_0

    goto :goto_2

    .line 68
    :cond_0
    const/4 v0, 0x0

    .line 69
    .local v0, "sum":I
    array-length v1, p1

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v1, :cond_2

    aget v4, p1, v3

    .line 70
    .local v4, "cn0":F
    const/4 v5, 0x0

    cmpl-float v5, v4, v5

    if-lez v5, :cond_1

    const/4 v5, 0x1

    goto :goto_1

    :cond_1
    move v5, v2

    :goto_1
    add-int/2addr v0, v5

    .line 69
    .end local v4    # "cn0":F
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 72
    :cond_2
    mul-int/lit8 v1, v0, 0x2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mStartTime:J

    sub-long/2addr v2, v4

    invoke-direct {p0, v1, v2, v3}, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->switchEngineStateWithScore(IJ)V

    .line 73
    return-void

    .line 67
    .end local v0    # "sum":I
    :cond_3
    :goto_2
    return-void
.end method

.method public startScoringModel(Z)V
    .locals 4
    .param p1, "on"    # Z

    .line 48
    iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mFeatureSwitch:Z

    if-nez v0, :cond_0

    return-void

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "start Scoring Model ? "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GnssScoringModel"

    invoke-virtual {v0, v2, v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mModelRunning:Z

    if-nez v0, :cond_1

    if-eqz p1, :cond_1

    .line 51
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mStartTime:J

    .line 53
    :cond_1
    if-eqz p1, :cond_3

    iget-wide v0, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mLastLocationTime:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    .line 54
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mLastLocationTime:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x493e0

    cmp-long v0, v0, v2

    if-lez v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mModelRunning:Z

    .line 55
    return-void
.end method

.method public updateFixTime(J)V
    .locals 3
    .param p1, "time"    # J

    .line 59
    iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mFeatureSwitch:Z

    if-nez v0, :cond_0

    return-void

    .line 60
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    const-string v1, "GnssScoringModel"

    const-string/jumbo v2, "update gnss fix time."

    invoke-virtual {v0, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mModelRunning:Z

    .line 62
    iput-wide p1, p0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl;->mLastLocationTime:J

    .line 63
    return-void
.end method
