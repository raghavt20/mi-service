public class com.android.server.location.gnss.hal.Gpo5Client {
	 /* .source "Gpo5Client.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Long MAX_RECOED_MOVING_TIME_SIZE;
private static final Integer MAX_RECORD_STEP_TIME_SIZE;
private static final Float MAX_STEPS;
private static final Float MAX_TRAFFIC_SPEED;
private static final Long MIN_RECOED_MOVING_TIME_SIZE;
private static final Integer SENSOR_SENSITIVE;
private static final Integer SENSOR_TYPE_OEM_USER_BEHAVIOR;
private static final java.lang.String TAG;
private static final Long TIME_CHECK_NAV_APP;
private static final Integer TIME_POST_DELAY_CHECK_NLP;
private static final Integer TIME_POST_TIMEOUT_OBTAIN_NLP;
private static final Float USER_BEHAVIOR_TRAFFIC;
private static volatile com.android.server.location.gnss.hal.Gpo5Client instance;
/* # instance fields */
private java.util.concurrent.ScheduledFuture futureCheckNavigation1;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/concurrent/ScheduledFuture<", */
/* "*>;" */
/* } */
} // .end annotation
} // .end field
private java.util.concurrent.ScheduledFuture futureCheckNavigation2;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/concurrent/ScheduledFuture<", */
/* "*>;" */
/* } */
} // .end annotation
} // .end field
private java.util.concurrent.ScheduledFuture futureCheckRequest;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/concurrent/ScheduledFuture<", */
/* "*>;" */
/* } */
} // .end annotation
} // .end field
private java.util.concurrent.ScheduledFuture futureReturnFineLocation;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/concurrent/ScheduledFuture<", */
/* "*>;" */
/* } */
} // .end annotation
} // .end field
private Boolean isClientAlive;
private final java.util.concurrent.atomic.AtomicBoolean isFeatureEnabled;
private final java.util.concurrent.atomic.AtomicBoolean isInsideSmallArea;
private final java.util.concurrent.atomic.AtomicBoolean isScreenOn;
private Boolean isUserBehaviorSensorRegistered;
private final java.util.Map mCacheLocationCallbackMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/lang/Object;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.content.Context mContext;
private final java.util.concurrent.atomic.AtomicInteger mCurrentSteps;
private android.location.Location mFineLocation;
private final java.util.Map mGlpRequestMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Integer;", */
/* "Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final com.android.server.location.gnss.hal.GpoUtil mGpoUtil;
private final java.util.concurrent.atomic.AtomicInteger mInitialSteps;
private final java.util.concurrent.atomic.AtomicLong mLastGnssTrafficTime;
private android.location.Location mLastNlpLocation;
private final java.util.concurrent.atomic.AtomicLong mLastNlpTime;
private final java.util.concurrent.atomic.AtomicLong mLastSensorTrafficTime;
private android.location.LocationManager mLocationManager;
private final java.util.Map mNavAppInfo;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final android.location.LocationListener mNetworkLocationListener;
private final java.util.Map mNlpRequestMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Integer;", */
/* "Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.util.concurrent.atomic.AtomicLong mObtainNlpTime;
private final android.location.LocationListener mPassiveLocationListener;
private final java.util.concurrent.atomic.AtomicLong mRequestNlpTime;
private android.hardware.SensorManager mSensorManager;
private final java.util.concurrent.atomic.AtomicBoolean mStartEngineWhileScreenLocked;
private final java.util.concurrent.BlockingDeque mStepTimeRecorder;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/concurrent/BlockingDeque<", */
/* "Ljava/lang/Long;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.hardware.Sensor mUserBehaviorDetector;
private final android.hardware.SensorEventListener mUserBehaviorListener;
private android.hardware.Sensor mUserStepsDetector;
private final android.hardware.SensorEventListener mUserStepsListener;
private java.util.concurrent.ScheduledThreadPoolExecutor scheduledThreadPoolExecutor;
/* # direct methods */
public static void $r8$lambda$8qAOQpqB-ZU_iVPk4B5LmFrM4Yo ( com.android.server.location.gnss.hal.Gpo5Client p0, Integer p1, Integer p2 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/location/gnss/hal/Gpo5Client;->lambda$saveLocationRequestId$2(II)V */
return;
} // .end method
public static java.lang.Boolean $r8$lambda$KKe_slwmNRqt3DI2t-6ADwTOFEw ( com.android.server.location.gnss.hal.Gpo5Client p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->lambda$registerNetworkLocationUpdates$5()Ljava/lang/Boolean; */
} // .end method
public static void $r8$lambda$KSM108fFRhOGSQsrqH89yHVBmtk ( com.android.server.location.gnss.hal.Gpo5Client p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->lambda$saveLocationRequestId$1()V */
return;
} // .end method
public static void $r8$lambda$P7BJcDGM4MGPM2WIizDUYgDWuwI ( com.android.server.location.gnss.hal.Gpo5Client p0, android.location.Location p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/location/gnss/hal/Gpo5Client;->lambda$new$3(Landroid/location/Location;)V */
return;
} // .end method
public static void $r8$lambda$QkFJwxbA-LNPoO-_7D4L2m8wcNo ( com.android.server.location.gnss.hal.Gpo5Client p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->lambda$checkAppUsage$7()V */
return;
} // .end method
public static void $r8$lambda$WHDDIxBnFXTAsfQAwlvemllYmcQ ( com.android.server.location.gnss.hal.Gpo5Client p0, android.location.Location p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/location/gnss/hal/Gpo5Client;->lambda$new$4(Landroid/location/Location;)V */
return;
} // .end method
public static void $r8$lambda$_KGvJDv-9fO_fprUa5_UKqauhJM ( com.android.server.location.gnss.hal.Gpo5Client p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->lambda$unregisterNetworkLocationUpdates$6()V */
return;
} // .end method
public static void $r8$lambda$uVyDP5uqcDGtjyPlz5UVv8NG5oM ( com.android.server.location.gnss.hal.Gpo5Client p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->lambda$returnFineLocation2App$0()V */
return;
} // .end method
static Boolean -$$Nest$fgetisUserBehaviorSensorRegistered ( com.android.server.location.gnss.hal.Gpo5Client p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->isUserBehaviorSensorRegistered:Z */
} // .end method
static java.util.concurrent.atomic.AtomicInteger -$$Nest$fgetmCurrentSteps ( com.android.server.location.gnss.hal.Gpo5Client p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mCurrentSteps;
} // .end method
static com.android.server.location.gnss.hal.GpoUtil -$$Nest$fgetmGpoUtil ( com.android.server.location.gnss.hal.Gpo5Client p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mGpoUtil;
} // .end method
static java.util.concurrent.atomic.AtomicInteger -$$Nest$fgetmInitialSteps ( com.android.server.location.gnss.hal.Gpo5Client p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mInitialSteps;
} // .end method
static java.util.concurrent.atomic.AtomicLong -$$Nest$fgetmLastSensorTrafficTime ( com.android.server.location.gnss.hal.Gpo5Client p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mLastSensorTrafficTime;
} // .end method
static java.util.concurrent.BlockingDeque -$$Nest$fgetmStepTimeRecorder ( com.android.server.location.gnss.hal.Gpo5Client p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mStepTimeRecorder;
} // .end method
static void -$$Nest$mhandleLeaveSmallArea ( com.android.server.location.gnss.hal.Gpo5Client p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->handleLeaveSmallArea()V */
return;
} // .end method
private com.android.server.location.gnss.hal.Gpo5Client ( ) {
/* .locals 4 */
/* .line 167 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 61 */
int v0 = 0; // const/4 v0, 0x0
this.mFineLocation = v0;
/* .line 63 */
int v1 = 0; // const/4 v1, 0x0
/* iput-boolean v1, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->isUserBehaviorSensorRegistered:Z */
/* .line 67 */
int v2 = 1; // const/4 v2, 0x1
/* iput-boolean v2, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->isClientAlive:Z */
/* .line 70 */
this.mLastNlpLocation = v0;
/* .line 71 */
this.futureCheckRequest = v0;
/* .line 72 */
this.futureCheckNavigation1 = v0;
/* .line 73 */
this.futureCheckNavigation2 = v0;
/* .line 74 */
this.futureReturnFineLocation = v0;
/* .line 76 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.mGlpRequestMap = v0;
/* .line 77 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.mNlpRequestMap = v0;
/* .line 78 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.mNavAppInfo = v0;
/* .line 79 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.mCacheLocationCallbackMap = v0;
/* .line 80 */
com.android.server.location.gnss.hal.GpoUtil .getInstance ( );
this.mGpoUtil = v0;
/* .line 82 */
/* new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean; */
/* invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V */
this.isFeatureEnabled = v0;
/* .line 83 */
/* new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean; */
/* invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V */
this.isScreenOn = v0;
/* .line 84 */
/* new-instance v0, Ljava/util/concurrent/atomic/AtomicLong; */
/* const-wide/16 v2, 0x0 */
/* invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V */
this.mRequestNlpTime = v0;
/* .line 85 */
/* new-instance v0, Ljava/util/concurrent/atomic/AtomicLong; */
/* invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V */
this.mObtainNlpTime = v0;
/* .line 86 */
/* new-instance v0, Ljava/util/concurrent/atomic/AtomicLong; */
/* invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V */
this.mLastNlpTime = v0;
/* .line 87 */
/* new-instance v0, Ljava/util/concurrent/atomic/AtomicLong; */
/* invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V */
this.mLastGnssTrafficTime = v0;
/* .line 88 */
/* new-instance v0, Ljava/util/concurrent/atomic/AtomicLong; */
/* invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V */
this.mLastSensorTrafficTime = v0;
/* .line 89 */
/* new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger; */
/* invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V */
this.mCurrentSteps = v0;
/* .line 90 */
/* new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger; */
/* invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V */
this.mInitialSteps = v0;
/* .line 91 */
/* new-instance v0, Ljava/util/concurrent/LinkedBlockingDeque; */
/* const/16 v2, 0x64 */
/* invoke-direct {v0, v2}, Ljava/util/concurrent/LinkedBlockingDeque;-><init>(I)V */
this.mStepTimeRecorder = v0;
/* .line 92 */
/* new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean; */
/* invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V */
this.isInsideSmallArea = v0;
/* .line 93 */
/* new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean; */
/* invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V */
this.mStartEngineWhileScreenLocked = v0;
/* .line 96 */
/* new-instance v0, Lcom/android/server/location/gnss/hal/Gpo5Client$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/location/gnss/hal/Gpo5Client$1;-><init>(Lcom/android/server/location/gnss/hal/Gpo5Client;)V */
this.mUserBehaviorListener = v0;
/* .line 119 */
/* new-instance v0, Lcom/android/server/location/gnss/hal/Gpo5Client$2; */
/* invoke-direct {v0, p0}, Lcom/android/server/location/gnss/hal/Gpo5Client$2;-><init>(Lcom/android/server/location/gnss/hal/Gpo5Client;)V */
this.mUserStepsListener = v0;
/* .line 389 */
/* new-instance v0, Lcom/android/server/location/gnss/hal/Gpo5Client$$ExternalSyntheticLambda0; */
/* invoke-direct {v0, p0}, Lcom/android/server/location/gnss/hal/Gpo5Client$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/location/gnss/hal/Gpo5Client;)V */
this.mPassiveLocationListener = v0;
/* .line 448 */
/* new-instance v0, Lcom/android/server/location/gnss/hal/Gpo5Client$$ExternalSyntheticLambda1; */
/* invoke-direct {v0, p0}, Lcom/android/server/location/gnss/hal/Gpo5Client$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/location/gnss/hal/Gpo5Client;)V */
this.mNetworkLocationListener = v0;
/* .line 167 */
return;
} // .end method
private java.util.concurrent.ScheduledFuture checkAppUsage ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "timeout" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I)", */
/* "Ljava/util/concurrent/ScheduledFuture<", */
/* "*>;" */
/* } */
} // .end annotation
/* .line 518 */
try { // :try_start_0
v0 = this.mGpoUtil;
final String v1 = "Gpo5Client"; // const-string v1, "Gpo5Client"
final String v2 = "checkAppUsage"; // const-string v2, "checkAppUsage"
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logv ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 519 */
v0 = this.scheduledThreadPoolExecutor;
/* new-instance v1, Lcom/android/server/location/gnss/hal/Gpo5Client$$ExternalSyntheticLambda4; */
/* invoke-direct {v1, p0}, Lcom/android/server/location/gnss/hal/Gpo5Client$$ExternalSyntheticLambda4;-><init>(Lcom/android/server/location/gnss/hal/Gpo5Client;)V */
/* int-to-long v2, p1 */
v4 = java.util.concurrent.TimeUnit.MILLISECONDS;
(( java.util.concurrent.ScheduledThreadPoolExecutor ) v0 ).schedule ( v1, v2, v3, v4 ); // invoke-virtual {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 526 */
/* :catch_0 */
/* move-exception v0 */
/* .line 527 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 529 */
} // .end local v0 # "e":Ljava/lang/Exception;
int v0 = 0; // const/4 v0, 0x0
} // .end method
private void checkFeatureSwitch ( ) {
/* .locals 4 */
/* .line 315 */
v0 = v0 = this.mGlpRequestMap;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 316 */
v0 = this.isFeatureEnabled;
/* iget-boolean v1, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->isUserBehaviorSensorRegistered:Z */
int v2 = 0; // const/4 v2, 0x0
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = this.mGpoUtil;
v1 = (( com.android.server.location.gnss.hal.GpoUtil ) v1 ).checkHeavyUser ( ); // invoke-virtual {v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->checkHeavyUser()Z
/* if-nez v1, :cond_0 */
v1 = this.mGpoUtil;
/* .line 317 */
v1 = (( com.android.server.location.gnss.hal.GpoUtil ) v1 ).isNetworkConnected ( ); // invoke-virtual {v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->isNetworkConnected()Z
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = this.isScreenOn;
v1 = (( java.util.concurrent.atomic.AtomicBoolean ) v1 ).get ( ); // invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = /* invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->isUserMovingRecently()Z */
/* if-nez v1, :cond_0 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
/* move v1, v2 */
/* .line 316 */
} // :goto_0
(( java.util.concurrent.atomic.AtomicBoolean ) v0 ).set ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
/* .line 318 */
v0 = this.mGpoUtil;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "isGpoFeatureEnabled ? "; // const-string v3, "isGpoFeatureEnabled ? "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.isFeatureEnabled;
v3 = (( java.util.concurrent.atomic.AtomicBoolean ) v3 ).get ( ); // invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "Gpo5Client"; // const-string v3, "Gpo5Client"
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logi ( v3, v1, v2 ); // invoke-virtual {v0, v3, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V
/* .line 319 */
com.android.server.location.gnss.hal.GnssScoringModelStub .getInstance ( );
v1 = this.isFeatureEnabled;
v1 = (( java.util.concurrent.atomic.AtomicBoolean ) v1 ).get ( ); // invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
/* .line 321 */
} // :cond_1
return;
} // .end method
private void delieverLocation ( java.lang.Object p0, java.util.List p1 ) {
/* .locals 6 */
/* .param p1, "callbackType" # Ljava/lang/Object; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/Object;", */
/* "Ljava/util/List<", */
/* "Landroid/location/Location;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 416 */
/* .local p2, "locations":Ljava/util/List;, "Ljava/util/List<Landroid/location/Location;>;" */
/* instance-of v0, p1, Landroid/location/ILocationListener; */
int v1 = 1; // const/4 v1, 0x1
final String v2 = "Gpo5Client"; // const-string v2, "Gpo5Client"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 418 */
try { // :try_start_0
/* move-object v0, p1 */
/* check-cast v0, Landroid/location/ILocationListener; */
int v3 = 0; // const/4 v3, 0x0
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 419 */
/* :catch_0 */
/* move-exception v0 */
/* .line 420 */
/* .local v0, "e":Landroid/os/RemoteException; */
v3 = this.mGpoUtil;
final String v4 = "onLocationChanged RemoteException"; // const-string v4, "onLocationChanged RemoteException"
(( com.android.server.location.gnss.hal.GpoUtil ) v3 ).logi ( v2, v4, v1 ); // invoke-virtual {v3, v2, v4, v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V
/* .line 421 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :goto_0
/* .line 423 */
} // :cond_0
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 426 */
/* .local v0, "intent":Landroid/content/Intent; */
int v3 = 0; // const/4 v3, 0x0
/* check-cast v4, Landroid/os/Parcelable; */
final String v5 = "location"; // const-string v5, "location"
(( android.content.Intent ) v0 ).putExtra ( v5, v4 ); // invoke-virtual {v0, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;
/* .line 428 */
try { // :try_start_1
/* move-object v4, p1 */
/* check-cast v4, Landroid/app/PendingIntent; */
v5 = this.mContext;
(( android.app.PendingIntent ) v4 ).send ( v5, v3, v0 ); // invoke-virtual {v4, v5, v3, v0}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V
/* :try_end_1 */
/* .catch Landroid/app/PendingIntent$CanceledException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 431 */
/* .line 429 */
/* :catch_1 */
/* move-exception v3 */
/* .line 430 */
/* .local v3, "e":Landroid/app/PendingIntent$CanceledException; */
v4 = this.mGpoUtil;
final String v5 = "PendingInent send CanceledException"; // const-string v5, "PendingInent send CanceledException"
(( com.android.server.location.gnss.hal.GpoUtil ) v4 ).logi ( v2, v5, v1 ); // invoke-virtual {v4, v2, v5, v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V
/* .line 433 */
} // .end local v0 # "intent":Landroid/content/Intent;
} // .end local v3 # "e":Landroid/app/PendingIntent$CanceledException;
} // :goto_1
return;
} // .end method
public static com.android.server.location.gnss.hal.Gpo5Client getInstance ( ) {
/* .locals 2 */
/* .line 153 */
v0 = com.android.server.location.gnss.hal.Gpo5Client.instance;
/* if-nez v0, :cond_1 */
/* .line 154 */
/* const-class v0, Lcom/android/server/location/gnss/hal/Gpo5Client; */
/* monitor-enter v0 */
/* .line 155 */
try { // :try_start_0
v1 = com.android.server.location.gnss.hal.Gpo5Client.instance;
/* if-nez v1, :cond_0 */
/* .line 156 */
/* new-instance v1, Lcom/android/server/location/gnss/hal/Gpo5Client; */
/* invoke-direct {v1}, Lcom/android/server/location/gnss/hal/Gpo5Client;-><init>()V */
/* .line 158 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 160 */
} // :cond_1
} // :goto_0
v0 = com.android.server.location.gnss.hal.Gpo5Client.instance;
} // .end method
private void handleLeaveSmallArea ( ) {
/* .locals 2 */
/* .line 209 */
/* iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->isClientAlive:Z */
/* if-nez v0, :cond_0 */
return;
/* .line 211 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
this.mFineLocation = v0;
/* .line 213 */
com.android.server.location.gnss.hal.GnssScoringModelStub .getInstance ( );
int v1 = 1; // const/4 v1, 0x1
/* .line 216 */
v0 = this.mGpoUtil;
v0 = (( com.android.server.location.gnss.hal.GpoUtil ) v0 ).getEngineStatus ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->getEngineStatus()I
int v1 = 3; // const/4 v1, 0x3
/* if-ne v0, v1, :cond_1 */
/* .line 221 */
v0 = this.mGpoUtil;
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).doStartEngineByInstance ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->doStartEngineByInstance()Z
/* .line 223 */
com.android.server.location.LocationDumpLogStub .getInstance ( );
int v1 = 0; // const/4 v1, 0x0
/* .line 225 */
} // :cond_1
return;
} // .end method
private Boolean isUserMovingRecently ( ) {
/* .locals 12 */
/* .line 324 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v0 */
/* .line 325 */
/* .local v0, "curTime":J */
v2 = v2 = this.mStepTimeRecorder;
/* const/16 v3, 0x64 */
/* const-wide/32 v4, 0x493e0 */
int v6 = 0; // const/4 v6, 0x0
int v7 = 1; // const/4 v7, 0x1
/* if-lt v2, v3, :cond_0 */
v2 = this.mStepTimeRecorder;
/* .line 326 */
/* check-cast v2, Ljava/lang/Long; */
(( java.lang.Long ) v2 ).longValue ( ); // invoke-virtual {v2}, Ljava/lang/Long;->longValue()J
/* move-result-wide v2 */
/* sub-long v2, v0, v2 */
/* cmp-long v2, v2, v4 */
/* if-gtz v2, :cond_0 */
v2 = this.mStepTimeRecorder;
/* .line 327 */
/* check-cast v2, Ljava/lang/Long; */
(( java.lang.Long ) v2 ).longValue ( ); // invoke-virtual {v2}, Ljava/lang/Long;->longValue()J
/* move-result-wide v2 */
v8 = this.mStepTimeRecorder;
/* check-cast v8, Ljava/lang/Long; */
(( java.lang.Long ) v8 ).longValue ( ); // invoke-virtual {v8}, Ljava/lang/Long;->longValue()J
/* move-result-wide v8 */
/* sub-long/2addr v2, v8 */
/* cmp-long v2, v2, v4 */
/* if-gtz v2, :cond_0 */
/* move v2, v7 */
} // :cond_0
/* move v2, v6 */
/* .line 332 */
/* .local v2, "walkRecently":Z */
} // :goto_0
v3 = this.mLastGnssTrafficTime;
(( java.util.concurrent.atomic.AtomicLong ) v3 ).get ( ); // invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicLong;->get()J
/* move-result-wide v8 */
/* const-wide/16 v10, 0x0 */
/* cmp-long v3, v8, v10 */
if ( v3 != null) { // if-eqz v3, :cond_1
v3 = this.mLastGnssTrafficTime;
/* .line 333 */
(( java.util.concurrent.atomic.AtomicLong ) v3 ).get ( ); // invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicLong;->get()J
/* move-result-wide v8 */
/* sub-long v8, v0, v8 */
/* cmp-long v3, v8, v4 */
/* if-lez v3, :cond_2 */
} // :cond_1
v3 = this.mLastSensorTrafficTime;
/* .line 334 */
(( java.util.concurrent.atomic.AtomicLong ) v3 ).get ( ); // invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicLong;->get()J
/* move-result-wide v3 */
/* cmp-long v3, v3, v10 */
if ( v3 != null) { // if-eqz v3, :cond_3
v3 = this.mLastSensorTrafficTime;
/* .line 335 */
(( java.util.concurrent.atomic.AtomicLong ) v3 ).get ( ); // invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicLong;->get()J
/* move-result-wide v3 */
/* sub-long v3, v0, v3 */
/* const-wide/16 v8, 0x2710 */
/* cmp-long v3, v3, v8 */
/* if-gtz v3, :cond_3 */
} // :cond_2
/* move v3, v7 */
} // :cond_3
/* move v3, v6 */
/* .line 336 */
/* .local v3, "trafficRecently":Z */
} // :goto_1
v4 = this.mGpoUtil;
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "CurrentTime is "; // const-string v8, "CurrentTime is "
(( java.lang.StringBuilder ) v5 ).append ( v8 ); // invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v0, v1 ); // invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v8 = ", mLastGnssTrafficTime is "; // const-string v8, ", mLastGnssTrafficTime is "
(( java.lang.StringBuilder ) v5 ).append ( v8 ); // invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v8 = this.mLastGnssTrafficTime;
/* .line 337 */
(( java.util.concurrent.atomic.AtomicLong ) v8 ).get ( ); // invoke-virtual {v8}, Ljava/util/concurrent/atomic/AtomicLong;->get()J
/* move-result-wide v8 */
(( java.lang.StringBuilder ) v5 ).append ( v8, v9 ); // invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v8 = ", mLastSensorTrafficTime is "; // const-string v8, ", mLastSensorTrafficTime is "
(( java.lang.StringBuilder ) v5 ).append ( v8 ); // invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v8 = this.mLastSensorTrafficTime;
/* .line 338 */
(( java.util.concurrent.atomic.AtomicLong ) v8 ).get ( ); // invoke-virtual {v8}, Ljava/util/concurrent/atomic/AtomicLong;->get()J
/* move-result-wide v8 */
(( java.lang.StringBuilder ) v5 ).append ( v8, v9 ); // invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 336 */
final String v8 = "Gpo5Client"; // const-string v8, "Gpo5Client"
(( com.android.server.location.gnss.hal.GpoUtil ) v4 ).logv ( v8, v5 ); // invoke-virtual {v4, v8, v5}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 339 */
v4 = this.mGpoUtil;
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "Recently user walk ? "; // const-string v9, "Recently user walk ? "
(( java.lang.StringBuilder ) v5 ).append ( v9 ); // invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v9 = ", traffic ? "; // const-string v9, ", traffic ? "
(( java.lang.StringBuilder ) v5 ).append ( v9 ); // invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.android.server.location.gnss.hal.GpoUtil ) v4 ).logi ( v8, v5, v7 ); // invoke-virtual {v4, v8, v5, v7}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V
/* .line 340 */
/* if-nez v2, :cond_4 */
if ( v3 != null) { // if-eqz v3, :cond_5
} // :cond_4
/* move v6, v7 */
} // :cond_5
} // .end method
private void lambda$checkAppUsage$7 ( ) { //synthethic
/* .locals 3 */
/* .line 521 */
v0 = this.mGpoUtil;
v0 = (( com.android.server.location.gnss.hal.GpoUtil ) v0 ).getEngineStatus ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->getEngineStatus()I
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* .line 522 */
v0 = this.mGpoUtil;
final String v1 = "Gpo5Client"; // const-string v1, "Gpo5Client"
final String v2 = "navigating app, restart gnss engine"; // const-string v2, "navigating app, restart gnss engine"
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logv ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 523 */
v0 = this.mGpoUtil;
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).doStartEngineByInstance ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->doStartEngineByInstance()Z
/* .line 525 */
} // :cond_0
return;
} // .end method
private void lambda$new$3 ( android.location.Location p0 ) { //synthethic
/* .locals 9 */
/* .param p1, "location" # Landroid/location/Location; */
/* .line 390 */
v0 = this.isFeatureEnabled;
v0 = (( java.util.concurrent.atomic.AtomicBoolean ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
if ( v0 != null) { // if-eqz v0, :cond_2
final String v0 = "network"; // const-string v0, "network"
(( android.location.Location ) p1 ).getProvider ( ); // invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = this.mGpoUtil;
/* .line 391 */
v0 = (( com.android.server.location.gnss.hal.GpoUtil ) v0 ).getEngineStatus ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->getEngineStatus()I
int v1 = 2; // const/4 v1, 0x2
/* if-eq v0, v1, :cond_2 */
/* .line 392 */
v0 = (( android.location.Location ) p1 ).getAccuracy ( ); // invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F
/* const/high16 v1, 0x43160000 # 150.0f */
/* cmpg-float v0, v0, v1 */
/* if-gtz v0, :cond_2 */
/* .line 394 */
v0 = this.mGpoUtil;
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).convertNlp2Glp ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/location/gnss/hal/GpoUtil;->convertNlp2Glp(Landroid/location/Location;)Landroid/location/Location;
/* .line 395 */
/* .local v0, "gLocation":Landroid/location/Location; */
this.mLastNlpLocation = v0;
/* .line 396 */
v1 = this.mLastNlpTime;
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v2 */
(( java.util.concurrent.atomic.AtomicLong ) v1 ).set ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V
/* .line 397 */
v1 = this.mGlpRequestMap;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Ljava/util/Map$Entry; */
/* .line 398 */
/* .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;>;" */
/* check-cast v3, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder; */
v3 = (( com.android.server.location.gnss.hal.Gpo5Client$LocationRequestRecorder ) v3 ).getNlpReturned ( ); // invoke-virtual {v3}, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;->getNlpReturned()Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* goto/16 :goto_1 */
/* .line 399 */
} // :cond_0
/* check-cast v3, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder; */
int v4 = 1; // const/4 v4, 0x1
(( com.android.server.location.gnss.hal.Gpo5Client$LocationRequestRecorder ) v3 ).setNlpReturned ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;->setNlpReturned(Z)V
/* .line 400 */
/* filled-new-array {v0}, [Landroid/location/Location; */
java.util.Arrays .asList ( v3 );
/* .line 401 */
/* .local v3, "locations":Ljava/util/List;, "Ljava/util/List<Landroid/location/Location;>;" */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v6, "use nlp " */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.location.Location ) v0 ).toString ( ); // invoke-virtual {v0}, Landroid/location/Location;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = ", listenerHashCode is "; // const-string v6, ", listenerHashCode is "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 402 */
/* check-cast v6, Ljava/lang/Integer; */
(( java.lang.Integer ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/Integer;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = ", callbackType is ILocationListener "; // const-string v6, ", callbackType is ILocationListener "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 404 */
/* check-cast v6, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder; */
(( com.android.server.location.gnss.hal.Gpo5Client$LocationRequestRecorder ) v6 ).getCallbackType ( ); // invoke-virtual {v6}, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;->getCallbackType()Ljava/lang/Object;
/* instance-of v6, v6, Landroid/location/ILocationListener; */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 405 */
/* .local v5, "logInfo":Ljava/lang/String; */
v6 = this.mGpoUtil;
final String v7 = "Gpo5Client"; // const-string v7, "Gpo5Client"
(( com.android.server.location.gnss.hal.GpoUtil ) v6 ).logv ( v7, v5 ); // invoke-virtual {v6, v7, v5}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 406 */
v6 = this.mGpoUtil;
(( com.android.server.location.gnss.hal.GpoUtil ) v6 ).logEn ( v5 ); // invoke-virtual {v6, v5}, Lcom/android/server/location/gnss/hal/GpoUtil;->logEn(Ljava/lang/String;)V
/* .line 407 */
/* check-cast v6, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder; */
(( com.android.server.location.gnss.hal.Gpo5Client$LocationRequestRecorder ) v6 ).getCallbackType ( ); // invoke-virtual {v6}, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;->getCallbackType()Ljava/lang/Object;
/* invoke-direct {p0, v6, v3}, Lcom/android/server/location/gnss/hal/Gpo5Client;->delieverLocation(Ljava/lang/Object;Ljava/util/List;)V */
/* .line 408 */
/* check-cast v6, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder; */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v7 */
(( com.android.server.location.gnss.hal.Gpo5Client$LocationRequestRecorder ) v6 ).setNlpReturnTime ( v7, v8 ); // invoke-virtual {v6, v7, v8}, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;->setNlpReturnTime(J)V
/* .line 409 */
/* const/16 v6, 0xfa */
/* invoke-direct {p0, v6}, Lcom/android/server/location/gnss/hal/Gpo5Client;->checkAppUsage(I)Ljava/util/concurrent/ScheduledFuture; */
this.futureCheckNavigation2 = v6;
/* .line 410 */
v6 = this.futureCheckRequest;
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 411 */
} // .end local v2 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;>;"
} // .end local v3 # "locations":Ljava/util/List;, "Ljava/util/List<Landroid/location/Location;>;"
} // .end local v5 # "logInfo":Ljava/lang/String;
} // :cond_1
/* goto/16 :goto_0 */
/* .line 413 */
} // .end local v0 # "gLocation":Landroid/location/Location;
} // :cond_2
} // :goto_1
return;
} // .end method
private void lambda$new$4 ( android.location.Location p0 ) { //synthethic
/* .locals 3 */
/* .param p1, "location" # Landroid/location/Location; */
/* .line 449 */
v0 = this.mObtainNlpTime;
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v1 */
(( java.util.concurrent.atomic.AtomicLong ) v0 ).set ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V
return;
} // .end method
private java.lang.Boolean lambda$registerNetworkLocationUpdates$5 ( ) { //synthethic
/* .locals 6 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 472 */
v0 = this.mGpoUtil;
final String v1 = "Gpo5Client"; // const-string v1, "Gpo5Client"
final String v2 = "request NLP."; // const-string v2, "request NLP."
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logv ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 473 */
v0 = this.mRequestNlpTime;
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v1 */
(( java.util.concurrent.atomic.AtomicLong ) v0 ).set ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V
/* .line 474 */
v0 = this.mLocationManager;
final String v1 = "network"; // const-string v1, "network"
/* const-wide/16 v2, 0x3e8 */
/* const/high16 v4, 0x447a0000 # 1000.0f */
int v5 = 1; // const/4 v5, 0x1
android.location.LocationRequest .createFromDeprecatedProvider ( v1,v2,v3,v4,v5 );
v2 = this.mNetworkLocationListener;
v3 = this.mContext;
/* .line 476 */
(( android.content.Context ) v3 ).getMainLooper ( ); // invoke-virtual {v3}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;
/* .line 474 */
(( android.location.LocationManager ) v0 ).requestLocationUpdates ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/location/LocationManager;->requestLocationUpdates(Landroid/location/LocationRequest;Landroid/location/LocationListener;Landroid/os/Looper;)V
/* .line 477 */
java.lang.Boolean .valueOf ( v5 );
} // .end method
private void lambda$returnFineLocation2App$0 ( ) { //synthethic
/* .locals 5 */
/* .line 287 */
v0 = this.mFineLocation;
int v1 = 1; // const/4 v1, 0x1
final String v2 = "cancel scheduleAtFixedRate"; // const-string v2, "cancel scheduleAtFixedRate"
final String v3 = "Gpo5Client"; // const-string v3, "Gpo5Client"
/* if-nez v0, :cond_0 */
/* .line 288 */
v0 = this.mGpoUtil;
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logv ( v3, v2 ); // invoke-virtual {v0, v3, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 289 */
v0 = this.futureReturnFineLocation;
/* .line 290 */
return;
/* .line 293 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->isUserBehaviorSensorRegistered:Z */
/* if-nez v0, :cond_1 */
/* .line 294 */
v0 = this.mGpoUtil;
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logv ( v3, v2 ); // invoke-virtual {v0, v3, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 295 */
v0 = this.futureReturnFineLocation;
/* .line 296 */
return;
/* .line 298 */
} // :cond_1
v0 = this.mCacheLocationCallbackMap;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_2
/* check-cast v1, Ljava/util/Map$Entry; */
/* .line 299 */
/* .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Object;>;" */
v2 = this.mGpoUtil;
final String v4 = "return fine location to navigation app"; // const-string v4, "return fine location to navigation app"
(( com.android.server.location.gnss.hal.GpoUtil ) v2 ).logv ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 300 */
v4 = this.mFineLocation;
/* filled-new-array {v4}, [Landroid/location/Location; */
java.util.Arrays .asList ( v4 );
/* invoke-direct {p0, v2, v4}, Lcom/android/server/location/gnss/hal/Gpo5Client;->delieverLocation(Ljava/lang/Object;Ljava/util/List;)V */
/* .line 301 */
} // .end local v1 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Object;>;"
/* .line 302 */
} // :cond_2
return;
} // .end method
private void lambda$saveLocationRequestId$1 ( ) { //synthethic
/* .locals 4 */
/* .line 379 */
v0 = this.mObtainNlpTime;
(( java.util.concurrent.atomic.AtomicLong ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J
/* move-result-wide v0 */
v2 = this.mRequestNlpTime;
(( java.util.concurrent.atomic.AtomicLong ) v2 ).get ( ); // invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicLong;->get()J
/* move-result-wide v2 */
/* cmp-long v0, v0, v2 */
/* if-gtz v0, :cond_0 */
/* .line 380 */
/* invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->unregisterNetworkLocationUpdates()V */
/* .line 382 */
} // :cond_0
return;
} // .end method
private void lambda$saveLocationRequestId$2 ( Integer p0, Integer p1 ) { //synthethic
/* .locals 6 */
/* .param p1, "listenerHashCode" # I */
/* .param p2, "uid" # I */
/* .line 366 */
v0 = this.mGlpRequestMap;
v0 = java.lang.Integer .valueOf ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 367 */
int v0 = 0; // const/4 v0, 0x0
/* .line 368 */
/* .local v0, "existNlp":Z */
v1 = this.mNlpRequestMap;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder; */
/* .line 369 */
/* .local v2, "r":Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder; */
v3 = (( com.android.server.location.gnss.hal.Gpo5Client$LocationRequestRecorder ) v2 ).existUid ( p2 ); // invoke-virtual {v2, p2}, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;->existUid(I)Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 370 */
int v0 = 1; // const/4 v0, 0x1
/* .line 371 */
/* .line 373 */
} // .end local v2 # "r":Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;
} // :cond_0
/* .line 374 */
} // :cond_1
} // :goto_1
v1 = this.mGpoUtil;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "LocationRequest of "; // const-string v3, "LocationRequest of "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " existNlp ? "; // const-string v3, " existNlp ? "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v3 = 0; // const/4 v3, 0x0
final String v4 = "Gpo5Client"; // const-string v4, "Gpo5Client"
(( com.android.server.location.gnss.hal.GpoUtil ) v1 ).logi ( v4, v2, v3 ); // invoke-virtual {v1, v4, v2, v3}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V
/* .line 376 */
/* if-nez v0, :cond_2 */
v1 = /* invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->registerNetworkLocationUpdates()Z */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 377 */
v1 = this.scheduledThreadPoolExecutor;
/* new-instance v2, Lcom/android/server/location/gnss/hal/Gpo5Client$$ExternalSyntheticLambda2; */
/* invoke-direct {v2, p0}, Lcom/android/server/location/gnss/hal/Gpo5Client$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/location/gnss/hal/Gpo5Client;)V */
/* const-wide/16 v3, 0x7d0 */
v5 = java.util.concurrent.TimeUnit.MILLISECONDS;
(( java.util.concurrent.ScheduledThreadPoolExecutor ) v1 ).schedule ( v2, v3, v4, v5 ); // invoke-virtual {v1, v2, v3, v4, v5}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;
this.futureCheckRequest = v1;
/* .line 385 */
} // .end local v0 # "existNlp":Z
} // :cond_2
return;
} // .end method
private void lambda$unregisterNetworkLocationUpdates$6 ( ) { //synthethic
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 483 */
v0 = this.mLocationManager;
v1 = this.mNetworkLocationListener;
(( android.location.LocationManager ) v0 ).removeUpdates ( v1 ); // invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V
return;
} // .end method
private Boolean registerNetworkLocationUpdates ( ) {
/* .locals 9 */
/* .line 452 */
v0 = this.mGpoUtil;
v0 = (( com.android.server.location.gnss.hal.GpoUtil ) v0 ).isNetworkConnected ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->isNetworkConnected()Z
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_5
v0 = this.mLocationManager;
final String v2 = "network"; // const-string v2, "network"
v0 = (( android.location.LocationManager ) v0 ).isProviderEnabled ( v2 ); // invoke-virtual {v0, v2}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z
/* if-nez v0, :cond_0 */
/* goto/16 :goto_2 */
/* .line 453 */
} // :cond_0
v0 = this.mLastNlpLocation;
if ( v0 != null) { // if-eqz v0, :cond_4
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v2 */
v0 = this.mLastNlpTime;
(( java.util.concurrent.atomic.AtomicLong ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J
/* move-result-wide v4 */
/* sub-long/2addr v2, v4 */
/* const-wide/16 v4, 0x7d0 */
/* cmp-long v0, v2, v4 */
/* if-gtz v0, :cond_4 */
/* .line 454 */
v0 = this.mGlpRequestMap;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_3
/* check-cast v2, Ljava/util/Map$Entry; */
/* .line 455 */
/* .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;>;" */
/* check-cast v3, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder; */
v3 = (( com.android.server.location.gnss.hal.Gpo5Client$LocationRequestRecorder ) v3 ).getNlpReturned ( ); // invoke-virtual {v3}, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;->getNlpReturned()Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* goto/16 :goto_1 */
/* .line 456 */
} // :cond_1
/* check-cast v3, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder; */
int v4 = 1; // const/4 v4, 0x1
(( com.android.server.location.gnss.hal.Gpo5Client$LocationRequestRecorder ) v3 ).setNlpReturned ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;->setNlpReturned(Z)V
/* .line 457 */
v3 = this.mLastNlpLocation;
/* filled-new-array {v3}, [Landroid/location/Location; */
java.util.Arrays .asList ( v3 );
/* .line 458 */
/* .local v3, "locations":Ljava/util/List;, "Ljava/util/List<Landroid/location/Location;>;" */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v6, "use cached nlp " */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.mLastNlpLocation;
(( android.location.Location ) v6 ).toString ( ); // invoke-virtual {v6}, Landroid/location/Location;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = ", listenerHashCode is "; // const-string v6, ", listenerHashCode is "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 459 */
/* check-cast v6, Ljava/lang/Integer; */
(( java.lang.Integer ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/Integer;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = ", callbackType is ILocationListener "; // const-string v6, ", callbackType is ILocationListener "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 461 */
/* check-cast v6, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder; */
(( com.android.server.location.gnss.hal.Gpo5Client$LocationRequestRecorder ) v6 ).getCallbackType ( ); // invoke-virtual {v6}, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;->getCallbackType()Ljava/lang/Object;
/* instance-of v6, v6, Landroid/location/ILocationListener; */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 462 */
/* .local v5, "logInfo":Ljava/lang/String; */
v6 = this.mGpoUtil;
final String v7 = "Gpo5Client"; // const-string v7, "Gpo5Client"
(( com.android.server.location.gnss.hal.GpoUtil ) v6 ).logv ( v7, v5 ); // invoke-virtual {v6, v7, v5}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 463 */
v6 = this.mGpoUtil;
(( com.android.server.location.gnss.hal.GpoUtil ) v6 ).logEn ( v5 ); // invoke-virtual {v6, v5}, Lcom/android/server/location/gnss/hal/GpoUtil;->logEn(Ljava/lang/String;)V
/* .line 464 */
/* check-cast v6, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder; */
(( com.android.server.location.gnss.hal.Gpo5Client$LocationRequestRecorder ) v6 ).getCallbackType ( ); // invoke-virtual {v6}, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;->getCallbackType()Ljava/lang/Object;
/* invoke-direct {p0, v6, v3}, Lcom/android/server/location/gnss/hal/Gpo5Client;->delieverLocation(Ljava/lang/Object;Ljava/util/List;)V */
/* .line 465 */
/* check-cast v6, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder; */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v7 */
(( com.android.server.location.gnss.hal.Gpo5Client$LocationRequestRecorder ) v6 ).setNlpReturnTime ( v7, v8 ); // invoke-virtual {v6, v7, v8}, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;->setNlpReturnTime(J)V
/* .line 466 */
/* const/16 v6, 0xfa */
/* invoke-direct {p0, v6}, Lcom/android/server/location/gnss/hal/Gpo5Client;->checkAppUsage(I)Ljava/util/concurrent/ScheduledFuture; */
this.futureCheckNavigation2 = v6;
/* .line 467 */
v6 = this.futureCheckRequest;
if ( v6 != null) { // if-eqz v6, :cond_2
/* .line 468 */
} // .end local v2 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;>;"
} // .end local v3 # "locations":Ljava/util/List;, "Ljava/util/List<Landroid/location/Location;>;"
} // .end local v5 # "logInfo":Ljava/lang/String;
} // :cond_2
/* goto/16 :goto_0 */
/* .line 469 */
} // :cond_3
} // :goto_1
/* .line 471 */
} // :cond_4
/* new-instance v0, Lcom/android/server/location/gnss/hal/Gpo5Client$$ExternalSyntheticLambda7; */
/* invoke-direct {v0, p0}, Lcom/android/server/location/gnss/hal/Gpo5Client$$ExternalSyntheticLambda7;-><init>(Lcom/android/server/location/gnss/hal/Gpo5Client;)V */
android.os.Binder .withCleanCallingIdentity ( v0 );
/* check-cast v0, Ljava/lang/Boolean; */
v0 = (( java.lang.Boolean ) v0 ).booleanValue ( ); // invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
/* .line 452 */
} // :cond_5
} // :goto_2
} // .end method
private void registerPassiveLocationUpdates ( ) {
/* .locals 7 */
/* .line 436 */
v0 = this.mLocationManager;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 437 */
final String v1 = "passive"; // const-string v1, "passive"
/* const-wide/16 v2, 0x3e8 */
int v4 = 0; // const/4 v4, 0x0
v5 = this.mPassiveLocationListener;
v6 = this.mContext;
/* .line 438 */
(( android.content.Context ) v6 ).getMainLooper ( ); // invoke-virtual {v6}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;
/* .line 437 */
/* invoke-virtual/range {v0 ..v6}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;Landroid/os/Looper;)V */
/* .line 440 */
} // :cond_0
return;
} // .end method
private void registerUserBehaviorListener ( ) {
/* .locals 5 */
/* .line 242 */
/* iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->isClientAlive:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->isUserBehaviorSensorRegistered:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 245 */
} // :cond_0
v0 = this.mStartEngineWhileScreenLocked;
v0 = (( java.util.concurrent.atomic.AtomicBoolean ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
int v1 = 0; // const/4 v1, 0x0
final String v2 = "Gpo5Client"; // const-string v2, "Gpo5Client"
/* if-nez v0, :cond_2 */
v0 = this.mGpoUtil;
v0 = (( com.android.server.location.gnss.hal.GpoUtil ) v0 ).getEngineStatus ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->getEngineStatus()I
int v3 = 2; // const/4 v3, 0x2
/* if-ne v0, v3, :cond_1 */
/* .line 251 */
} // :cond_1
v0 = this.mGpoUtil;
final String v3 = "register User Behavior Listener"; // const-string v3, "register User Behavior Listener"
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logv ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 252 */
v0 = this.mSensorManager;
v3 = this.mUserBehaviorListener;
v4 = this.mUserBehaviorDetector;
(( android.hardware.SensorManager ) v0 ).registerListener ( v3, v4, v1 ); // invoke-virtual {v0, v3, v4, v1}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z
/* .line 253 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->isUserBehaviorSensorRegistered:Z */
/* .line 255 */
v1 = this.mInitialSteps;
v3 = this.mCurrentSteps;
v3 = (( java.util.concurrent.atomic.AtomicInteger ) v3 ).get ( ); // invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I
(( java.util.concurrent.atomic.AtomicInteger ) v1 ).set ( v3 ); // invoke-virtual {v1, v3}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V
/* .line 256 */
v1 = this.mGpoUtil;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "initial steps is "; // const-string v4, "initial steps is "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.mInitialSteps;
v4 = (( java.util.concurrent.atomic.AtomicInteger ) v4 ).get ( ); // invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.android.server.location.gnss.hal.GpoUtil ) v1 ).logv ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 257 */
v1 = this.isInsideSmallArea;
(( java.util.concurrent.atomic.AtomicBoolean ) v1 ).set ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
/* .line 258 */
return;
/* .line 246 */
} // :cond_2
} // :goto_0
v0 = this.mGpoUtil;
final String v3 = "gnss engine has been started, do not control."; // const-string v3, "gnss engine has been started, do not control."
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logv ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 247 */
v0 = this.mStartEngineWhileScreenLocked;
(( java.util.concurrent.atomic.AtomicBoolean ) v0 ).set ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
/* .line 248 */
v0 = this.isInsideSmallArea;
(( java.util.concurrent.atomic.AtomicBoolean ) v0 ).set ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
/* .line 249 */
return;
/* .line 242 */
} // :cond_3
} // :goto_1
return;
} // .end method
private void registerUserStepsListener ( ) {
/* .locals 4 */
/* .line 272 */
v0 = this.mSensorManager;
/* if-nez v0, :cond_0 */
return;
/* .line 273 */
} // :cond_0
v1 = this.mUserStepsListener;
v2 = this.mUserStepsDetector;
int v3 = 0; // const/4 v3, 0x0
(( android.hardware.SensorManager ) v0 ).registerListener ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z
/* .line 274 */
v0 = this.mGpoUtil;
final String v1 = "Gpo5Client"; // const-string v1, "Gpo5Client"
final String v2 = "register User Steps Listener"; // const-string v2, "register User Steps Listener"
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logv ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 275 */
return;
} // .end method
private void returnFineLocation2App ( ) {
/* .locals 7 */
/* .line 285 */
try { // :try_start_0
v0 = this.scheduledThreadPoolExecutor;
/* new-instance v1, Lcom/android/server/location/gnss/hal/Gpo5Client$$ExternalSyntheticLambda5; */
/* invoke-direct {v1, p0}, Lcom/android/server/location/gnss/hal/Gpo5Client$$ExternalSyntheticLambda5;-><init>(Lcom/android/server/location/gnss/hal/Gpo5Client;)V */
/* const-wide/16 v2, 0x0 */
/* const-wide/16 v4, 0x3e8 */
v6 = java.util.concurrent.TimeUnit.MILLISECONDS;
/* invoke-virtual/range {v0 ..v6}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture; */
this.futureReturnFineLocation = v0;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 305 */
/* .line 303 */
/* :catch_0 */
/* move-exception v0 */
/* .line 304 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 306 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void unregisterNetworkLocationUpdates ( ) {
/* .locals 1 */
/* .line 482 */
/* new-instance v0, Lcom/android/server/location/gnss/hal/Gpo5Client$$ExternalSyntheticLambda6; */
/* invoke-direct {v0, p0}, Lcom/android/server/location/gnss/hal/Gpo5Client$$ExternalSyntheticLambda6;-><init>(Lcom/android/server/location/gnss/hal/Gpo5Client;)V */
android.os.Binder .withCleanCallingIdentity ( v0 );
/* .line 485 */
return;
} // .end method
private void unregisterPassiveLocationUpdates ( ) {
/* .locals 2 */
/* .line 443 */
v0 = this.mLocationManager;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 444 */
v1 = this.mPassiveLocationListener;
(( android.location.LocationManager ) v0 ).removeUpdates ( v1 ); // invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V
/* .line 446 */
} // :cond_0
return;
} // .end method
private void unregisterUserBehaviorListener ( ) {
/* .locals 3 */
/* .line 261 */
/* iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->isUserBehaviorSensorRegistered:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mGpoUtil;
v0 = (( com.android.server.location.gnss.hal.GpoUtil ) v0 ).getEngineStatus ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->getEngineStatus()I
int v1 = 4; // const/4 v1, 0x4
/* if-ne v0, v1, :cond_0 */
/* .line 262 */
v0 = this.mGpoUtil;
final String v1 = "Gpo5Client"; // const-string v1, "Gpo5Client"
/* const-string/jumbo v2, "unregister User Behavior Listener" */
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logv ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 263 */
v0 = this.mSensorManager;
v1 = this.mUserBehaviorListener;
(( android.hardware.SensorManager ) v0 ).unregisterListener ( v1 ); // invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V
/* .line 264 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->isUserBehaviorSensorRegistered:Z */
/* .line 265 */
v1 = this.isInsideSmallArea;
(( java.util.concurrent.atomic.AtomicBoolean ) v1 ).set ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
/* .line 266 */
v1 = this.mStartEngineWhileScreenLocked;
(( java.util.concurrent.atomic.AtomicBoolean ) v1 ).set ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
/* .line 267 */
v1 = this.mGpoUtil;
(( com.android.server.location.gnss.hal.GpoUtil ) v1 ).setUserBehav ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->setUserBehav(I)V
/* .line 269 */
} // :cond_0
return;
} // .end method
private void unregisterUserStepsListener ( ) {
/* .locals 3 */
/* .line 278 */
v0 = this.mSensorManager;
/* if-nez v0, :cond_0 */
return;
/* .line 279 */
} // :cond_0
v1 = this.mUserStepsListener;
(( android.hardware.SensorManager ) v0 ).unregisterListener ( v1 ); // invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V
/* .line 280 */
v0 = this.mGpoUtil;
final String v1 = "Gpo5Client"; // const-string v1, "Gpo5Client"
/* const-string/jumbo v2, "unregister User Steps Listener" */
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logv ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 281 */
return;
} // .end method
/* # virtual methods */
public Boolean blockEngineStart ( ) {
/* .locals 5 */
/* .line 509 */
v0 = this.isFeatureEnabled;
v0 = (( java.util.concurrent.atomic.AtomicBoolean ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mGpoUtil;
/* .line 510 */
v0 = (( com.android.server.location.gnss.hal.GpoUtil ) v0 ).getEngineStatus ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->getEngineStatus()I
int v2 = 2; // const/4 v2, 0x2
/* if-eq v0, v2, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
/* move v0, v1 */
/* .line 511 */
/* .local v0, "res":Z */
} // :goto_0
v2 = this.mGpoUtil;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "blockEngineStart ? "; // const-string v4, "blockEngineStart ? "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "Gpo5Client"; // const-string v4, "Gpo5Client"
(( com.android.server.location.gnss.hal.GpoUtil ) v2 ).logi ( v4, v3, v1 ); // invoke-virtual {v2, v4, v3, v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V
/* .line 512 */
/* const/16 v1, 0x7d0 */
/* invoke-direct {p0, v1}, Lcom/android/server/location/gnss/hal/Gpo5Client;->checkAppUsage(I)Ljava/util/concurrent/ScheduledFuture; */
this.futureCheckNavigation1 = v1;
/* .line 513 */
} // .end method
public Boolean checkSensorSupport ( ) {
/* .locals 5 */
/* .line 187 */
v0 = this.mSensorManager;
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 189 */
} // :cond_0
/* const v2, 0x1fa2699 */
(( android.hardware.SensorManager ) v0 ).getDefaultSensor ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(IZ)Landroid/hardware/Sensor;
this.mUserBehaviorDetector = v0;
/* .line 190 */
int v2 = 1; // const/4 v2, 0x1
if ( v0 != null) { // if-eqz v0, :cond_1
/* move v1, v2 */
} // :cond_1
/* move v0, v1 */
/* .line 191 */
/* .local v0, "sensorSupport":Z */
v1 = this.mGpoUtil;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "This Device Support Sensor id 33171097 ? "; // const-string v4, "This Device Support Sensor id 33171097 ? "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "Gpo5Client"; // const-string v4, "Gpo5Client"
(( com.android.server.location.gnss.hal.GpoUtil ) v1 ).logi ( v4, v3, v2 ); // invoke-virtual {v1, v4, v3, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V
/* .line 193 */
} // .end method
public void clearLocationRequest ( ) {
/* .locals 3 */
/* .line 533 */
v0 = this.mGpoUtil;
final String v1 = "Gpo5Client"; // const-string v1, "Gpo5Client"
final String v2 = "clearLocationRequest"; // const-string v2, "clearLocationRequest"
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logv ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 534 */
v0 = this.futureCheckNavigation1;
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 535 */
} // :cond_0
v0 = this.futureCheckNavigation2;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 536 */
} // :cond_1
v0 = this.futureCheckRequest;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 537 */
} // :cond_2
return;
} // .end method
public void deinit ( ) {
/* .locals 3 */
/* .line 197 */
v0 = this.mGpoUtil;
final String v1 = "Gpo5Client"; // const-string v1, "Gpo5Client"
final String v2 = "deinit"; // const-string v2, "deinit"
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logv ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 198 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->isClientAlive:Z */
/* .line 199 */
/* invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->unregisterPassiveLocationUpdates()V */
/* .line 200 */
v1 = this.scheduledThreadPoolExecutor;
(( java.util.concurrent.ScheduledThreadPoolExecutor ) v1 ).shutdown ( ); // invoke-virtual {v1}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->shutdown()V
/* .line 201 */
int v1 = 0; // const/4 v1, 0x0
this.scheduledThreadPoolExecutor = v1;
/* .line 203 */
com.android.server.location.gnss.hal.GnssScoringModelStub .getInstance ( );
/* .line 204 */
/* invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->unregisterUserStepsListener()V */
/* .line 205 */
/* invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->unregisterUserBehaviorListener()V */
/* .line 206 */
return;
} // .end method
public void disableGnssSwitch ( ) {
/* .locals 2 */
/* .line 228 */
v0 = this.isFeatureEnabled;
int v1 = 0; // const/4 v1, 0x0
(( java.util.concurrent.atomic.AtomicBoolean ) v0 ).set ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
/* .line 229 */
/* invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->unregisterUserBehaviorListener()V */
/* .line 230 */
return;
} // .end method
public java.util.Map getNavInfo ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 164 */
v0 = this.mNavAppInfo;
} // .end method
public void init ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 170 */
v0 = this.mGpoUtil;
final String v1 = "Gpo5Client"; // const-string v1, "Gpo5Client"
final String v2 = "init"; // const-string v2, "init"
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logv ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 171 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->isClientAlive:Z */
/* .line 172 */
this.mContext = p1;
/* .line 173 */
/* nop */
/* .line 174 */
final String v1 = "location"; // const-string v1, "location"
(( android.content.Context ) p1 ).getSystemService ( v1 ); // invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v1, Landroid/location/LocationManager; */
/* .line 173 */
com.android.internal.util.Preconditions .checkNotNull ( v1 );
/* check-cast v1, Landroid/location/LocationManager; */
this.mLocationManager = v1;
/* .line 175 */
/* invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->registerPassiveLocationUpdates()V */
/* .line 176 */
/* new-instance v1, Ljava/util/concurrent/ScheduledThreadPoolExecutor; */
/* invoke-direct {v1, v0}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;-><init>(I)V */
this.scheduledThreadPoolExecutor = v1;
/* .line 177 */
(( java.util.concurrent.ScheduledThreadPoolExecutor ) v1 ).setRemoveOnCancelPolicy ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->setRemoveOnCancelPolicy(Z)V
/* .line 178 */
v0 = this.scheduledThreadPoolExecutor;
int v1 = 0; // const/4 v1, 0x0
(( java.util.concurrent.ScheduledThreadPoolExecutor ) v0 ).setExecuteExistingDelayedTasksAfterShutdownPolicy ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->setExecuteExistingDelayedTasksAfterShutdownPolicy(Z)V
/* .line 179 */
v0 = this.scheduledThreadPoolExecutor;
(( java.util.concurrent.ScheduledThreadPoolExecutor ) v0 ).setContinueExistingPeriodicTasksAfterShutdownPolicy ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->setContinueExistingPeriodicTasksAfterShutdownPolicy(Z)V
/* .line 180 */
v0 = this.mContext;
/* .line 181 */
/* const-string/jumbo v2, "sensor" */
(( android.content.Context ) v0 ).getSystemService ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/hardware/SensorManager; */
/* .line 180 */
com.android.internal.util.Preconditions .checkNotNull ( v0 );
/* check-cast v0, Landroid/hardware/SensorManager; */
this.mSensorManager = v0;
/* .line 182 */
/* const/16 v2, 0x13 */
(( android.hardware.SensorManager ) v0 ).getDefaultSensor ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(IZ)Landroid/hardware/Sensor;
this.mUserStepsDetector = v0;
/* .line 183 */
/* invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->registerUserStepsListener()V */
/* .line 184 */
return;
} // .end method
public void removeLocationRequestId ( Integer p0 ) {
/* .locals 8 */
/* .param p1, "listenerHashCode" # I */
/* .line 488 */
v0 = this.mNlpRequestMap;
v0 = java.lang.Integer .valueOf ( p1 );
int v1 = 0; // const/4 v1, 0x0
final String v2 = "Gpo5Client"; // const-string v2, "Gpo5Client"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 489 */
v0 = this.mGpoUtil;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "removeNLP: "; // const-string v4, "removeNLP: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logi ( v2, v3, v1 ); // invoke-virtual {v0, v2, v3, v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V
/* .line 490 */
v0 = this.mNlpRequestMap;
java.lang.Integer .valueOf ( p1 );
/* .line 492 */
} // :cond_0
v0 = this.mGlpRequestMap;
v0 = java.lang.Integer .valueOf ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 493 */
v0 = this.mGpoUtil;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "removeGLP: "; // const-string v4, "removeGLP: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logi ( v2, v3, v1 ); // invoke-virtual {v0, v2, v3, v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V
/* .line 494 */
v0 = this.mGlpRequestMap;
java.lang.Integer .valueOf ( p1 );
/* check-cast v0, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder; */
/* .line 495 */
/* .local v0, "recorder":Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder; */
v1 = this.mGlpRequestMap;
java.lang.Integer .valueOf ( p1 );
/* .line 496 */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 497 */
v1 = (( com.android.server.location.gnss.hal.Gpo5Client$LocationRequestRecorder ) v0 ).getUid ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;->getUid()I
/* .line 498 */
/* .local v1, "uid":I */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v2 */
(( com.android.server.location.gnss.hal.Gpo5Client$LocationRequestRecorder ) v0 ).getNlpReturnTime ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;->getNlpReturnTime()J
/* move-result-wide v4 */
/* sub-long/2addr v2, v4 */
/* .line 499 */
/* .local v2, "time":J */
com.android.server.location.gnss.GnssEventTrackingStub .getInstance ( );
v5 = this.mNavAppInfo;
java.lang.Integer .valueOf ( v1 );
/* check-cast v5, Ljava/lang/String; */
/* .line 500 */
/* const-wide/16 v6, 0xbb8 */
/* cmp-long v6, v2, v6 */
/* if-ltz v6, :cond_1 */
/* move-wide v6, v2 */
} // :cond_1
/* const-wide/16 v6, 0x0 */
/* .line 499 */
} // :goto_0
/* .line 501 */
v4 = this.mNavAppInfo;
java.lang.Integer .valueOf ( v1 );
/* .line 502 */
} // .end local v1 # "uid":I
} // .end local v2 # "time":J
/* .line 503 */
} // :cond_2
v1 = this.mNavAppInfo;
/* .line 506 */
} // .end local v0 # "recorder":Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;
} // :cond_3
} // :goto_1
return;
} // .end method
public void reportLocation2Gpo ( android.location.Location p0 ) {
/* .locals 4 */
/* .param p1, "location" # Landroid/location/Location; */
/* .line 540 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v0 */
/* .line 541 */
/* .local v0, "time":J */
if ( p1 != null) { // if-eqz p1, :cond_0
v2 = (( android.location.Location ) p1 ).isComplete ( ); // invoke-virtual {p1}, Landroid/location/Location;->isComplete()Z
if ( v2 != null) { // if-eqz v2, :cond_0
v2 = (( android.location.Location ) p1 ).getSpeed ( ); // invoke-virtual {p1}, Landroid/location/Location;->getSpeed()F
/* const/high16 v3, 0x40400000 # 3.0f */
/* cmpl-float v2, v2, v3 */
/* if-ltz v2, :cond_0 */
/* .line 542 */
v2 = this.mLastGnssTrafficTime;
(( java.util.concurrent.atomic.AtomicLong ) v2 ).set ( v0, v1 ); // invoke-virtual {v2, v0, v1}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V
/* .line 544 */
} // :cond_0
com.android.server.location.gnss.hal.GnssScoringModelStub .getInstance ( );
/* .line 545 */
return;
} // .end method
public void saveLocationRequestId ( Integer p0, java.lang.String p1, java.lang.String p2, Integer p3, java.lang.Object p4 ) {
/* .locals 18 */
/* .param p1, "uid" # I */
/* .param p2, "pkn" # Ljava/lang/String; */
/* .param p3, "provider" # Ljava/lang/String; */
/* .param p4, "listenerHashCode" # I */
/* .param p5, "callbackType" # Ljava/lang/Object; */
/* .line 345 */
/* move-object/from16 v9, p0 */
/* move-object/from16 v10, p3 */
/* move/from16 v11, p4 */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->checkFeatureSwitch()V */
/* .line 346 */
v0 = this.isFeatureEnabled;
v0 = (( java.util.concurrent.atomic.AtomicBoolean ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
/* if-nez v0, :cond_0 */
return;
/* .line 347 */
} // :cond_0
final String v0 = "network"; // const-string v0, "network"
v0 = (( java.lang.String ) v0 ).equalsIgnoreCase ( v10 ); // invoke-virtual {v0, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
int v12 = 0; // const/4 v12, 0x0
final String v13 = "Gpo5Client"; // const-string v13, "Gpo5Client"
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.mNlpRequestMap;
/* .line 348 */
v0 = /* invoke-static/range {p4 ..p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer; */
/* if-nez v0, :cond_1 */
/* .line 349 */
v0 = this.mGpoUtil;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "saveNLP: "; // const-string v2, "saveNLP: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v11 ); // invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logi ( v13, v1, v12 ); // invoke-virtual {v0, v13, v1, v12}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V
/* .line 350 */
v14 = this.mNlpRequestMap;
/* invoke-static/range {p4 ..p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer; */
/* new-instance v7, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder; */
int v6 = 0; // const/4 v6, 0x0
/* const-wide/16 v16, 0x0 */
/* move-object v0, v7 */
/* move-object/from16 v1, p0 */
/* move/from16 v2, p1 */
/* move-object/from16 v3, p3 */
/* move/from16 v4, p4 */
/* move-object/from16 v5, p5 */
/* move-object v12, v7 */
/* move-wide/from16 v7, v16 */
/* invoke-direct/range {v0 ..v8}, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;-><init>(Lcom/android/server/location/gnss/hal/Gpo5Client;ILjava/lang/String;ILjava/lang/Object;ZJ)V */
/* .line 353 */
} // :cond_1
final String v0 = "gps"; // const-string v0, "gps"
v0 = (( java.lang.String ) v0 ).equalsIgnoreCase ( v10 ); // invoke-virtual {v0, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_5
v0 = this.mGlpRequestMap;
/* .line 354 */
v0 = /* invoke-static/range {p4 ..p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer; */
/* if-nez v0, :cond_4 */
/* .line 356 */
v0 = this.mInitialSteps;
v1 = this.mCurrentSteps;
v1 = (( java.util.concurrent.atomic.AtomicInteger ) v1 ).get ( ); // invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I
(( java.util.concurrent.atomic.AtomicInteger ) v0 ).set ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V
/* .line 357 */
v0 = this.mGpoUtil;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "saveGLP: "; // const-string v2, "saveGLP: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v11 ); // invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v2 = 0; // const/4 v2, 0x0
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logi ( v13, v1, v2 ); // invoke-virtual {v0, v13, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V
/* .line 358 */
v0 = this.mNavAppInfo;
/* invoke-static/range {p1 ..p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer; */
/* move-object/from16 v12, p2 */
/* .line 359 */
v13 = this.mGlpRequestMap;
/* invoke-static/range {p4 ..p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer; */
/* new-instance v15, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder; */
int v6 = 0; // const/4 v6, 0x0
/* const-wide/16 v7, 0x0 */
/* move-object v0, v15 */
/* move-object/from16 v1, p0 */
/* move/from16 v2, p1 */
/* move-object/from16 v3, p3 */
/* move/from16 v4, p4 */
/* move-object/from16 v5, p5 */
/* invoke-direct/range {v0 ..v8}, Lcom/android/server/location/gnss/hal/Gpo5Client$LocationRequestRecorder;-><init>(Lcom/android/server/location/gnss/hal/Gpo5Client;ILjava/lang/String;ILjava/lang/Object;ZJ)V */
/* .line 361 */
v0 = this.mGpoUtil;
v0 = (( com.android.server.location.gnss.hal.GpoUtil ) v0 ).getEngineStatus ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->getEngineStatus()I
int v1 = 3; // const/4 v1, 0x3
/* if-ne v0, v1, :cond_3 */
/* .line 362 */
v0 = this.mLastNlpLocation;
if ( v0 != null) { // if-eqz v0, :cond_2
/* filled-new-array {v0}, [Landroid/location/Location; */
java.util.Arrays .asList ( v0 );
/* move-object/from16 v1, p5 */
/* invoke-direct {v9, v1, v0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->delieverLocation(Ljava/lang/Object;Ljava/util/List;)V */
} // :cond_2
/* move-object/from16 v1, p5 */
/* .line 363 */
} // :goto_0
return;
/* .line 365 */
} // :cond_3
/* move-object/from16 v1, p5 */
v0 = this.scheduledThreadPoolExecutor;
/* new-instance v2, Lcom/android/server/location/gnss/hal/Gpo5Client$$ExternalSyntheticLambda3; */
/* move/from16 v3, p1 */
/* invoke-direct {v2, v9, v11, v3}, Lcom/android/server/location/gnss/hal/Gpo5Client$$ExternalSyntheticLambda3;-><init>(Lcom/android/server/location/gnss/hal/Gpo5Client;II)V */
/* const-wide/16 v4, 0xfa */
v6 = java.util.concurrent.TimeUnit.MILLISECONDS;
(( java.util.concurrent.ScheduledThreadPoolExecutor ) v0 ).schedule ( v2, v4, v5, v6 ); // invoke-virtual {v0, v2, v4, v5, v6}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;
/* .line 354 */
} // :cond_4
/* move/from16 v3, p1 */
/* move-object/from16 v12, p2 */
/* move-object/from16 v1, p5 */
/* .line 353 */
} // :cond_5
/* move/from16 v3, p1 */
/* move-object/from16 v12, p2 */
/* move-object/from16 v1, p5 */
/* .line 387 */
} // :goto_1
return;
} // .end method
public void updateGnssStatus ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "status" # I */
/* .line 234 */
int v0 = 4; // const/4 v0, 0x4
/* if-ne p1, v0, :cond_0 */
v0 = this.isScreenOn;
/* .line 235 */
v0 = (( java.util.concurrent.atomic.AtomicBoolean ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
/* if-nez v0, :cond_0 */
/* iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client;->isUserBehaviorSensorRegistered:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 237 */
/* invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->unregisterUserBehaviorListener()V */
/* .line 239 */
} // :cond_0
return;
} // .end method
public void updateScreenState ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "on" # Z */
/* .line 309 */
v0 = this.isScreenOn;
(( java.util.concurrent.atomic.AtomicBoolean ) v0 ).set ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
/* .line 310 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->registerUserBehaviorListener()V */
/* .line 311 */
} // :cond_0
/* invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->unregisterUserBehaviorListener()V */
/* .line 312 */
} // :goto_0
return;
} // .end method
