.class Lcom/android/server/location/gnss/hal/GpoUtil$DataEventAdapter;
.super Ljava/lang/Object;
.source "GpoUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/location/gnss/hal/GpoUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DataEventAdapter"
.end annotation


# instance fields
.field private iDataEvent:Lcom/android/server/location/gnss/hal/GpoUtil$IDataEvent;

.field final synthetic this$0:Lcom/android/server/location/gnss/hal/GpoUtil;


# direct methods
.method private constructor <init>(Lcom/android/server/location/gnss/hal/GpoUtil;)V
    .locals 0

    .line 341
    iput-object p1, p0, Lcom/android/server/location/gnss/hal/GpoUtil$DataEventAdapter;->this$0:Lcom/android/server/location/gnss/hal/GpoUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/location/gnss/hal/GpoUtil;Lcom/android/server/location/gnss/hal/GpoUtil$DataEventAdapter-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/location/gnss/hal/GpoUtil$DataEventAdapter;-><init>(Lcom/android/server/location/gnss/hal/GpoUtil;)V

    return-void
.end method


# virtual methods
.method public attachEventListener(Lcom/android/server/location/gnss/hal/GpoUtil$IDataEvent;)V
    .locals 0
    .param p1, "listener"    # Lcom/android/server/location/gnss/hal/GpoUtil$IDataEvent;

    .line 345
    iput-object p1, p0, Lcom/android/server/location/gnss/hal/GpoUtil$DataEventAdapter;->iDataEvent:Lcom/android/server/location/gnss/hal/GpoUtil$IDataEvent;

    .line 346
    return-void
.end method

.method public notifyDefaultNetwork(I)V
    .locals 1
    .param p1, "type"    # I

    .line 361
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GpoUtil$DataEventAdapter;->iDataEvent:Lcom/android/server/location/gnss/hal/GpoUtil$IDataEvent;

    if-eqz v0, :cond_0

    .line 362
    invoke-interface {v0, p1}, Lcom/android/server/location/gnss/hal/GpoUtil$IDataEvent;->updateDefaultNetwork(I)V

    .line 364
    :cond_0
    return-void
.end method

.method public notifyGnssStatus(I)V
    .locals 1
    .param p1, "status"    # I

    .line 367
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GpoUtil$DataEventAdapter;->iDataEvent:Lcom/android/server/location/gnss/hal/GpoUtil$IDataEvent;

    if-eqz v0, :cond_0

    .line 368
    invoke-interface {v0, p1}, Lcom/android/server/location/gnss/hal/GpoUtil$IDataEvent;->updateGnssStatus(I)V

    .line 370
    :cond_0
    return-void
.end method

.method public notifyPersistUpdates(I)V
    .locals 1
    .param p1, "version"    # I

    .line 349
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GpoUtil$DataEventAdapter;->iDataEvent:Lcom/android/server/location/gnss/hal/GpoUtil$IDataEvent;

    if-eqz v0, :cond_0

    .line 350
    invoke-interface {v0, p1}, Lcom/android/server/location/gnss/hal/GpoUtil$IDataEvent;->updateFeatureSwitch(I)V

    .line 352
    :cond_0
    return-void
.end method

.method public notifyScreenState(Z)V
    .locals 1
    .param p1, "on"    # Z

    .line 355
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GpoUtil$DataEventAdapter;->iDataEvent:Lcom/android/server/location/gnss/hal/GpoUtil$IDataEvent;

    if-eqz v0, :cond_0

    .line 356
    invoke-interface {v0, p1}, Lcom/android/server/location/gnss/hal/GpoUtil$IDataEvent;->updateScreenState(Z)V

    .line 358
    :cond_0
    return-void
.end method
