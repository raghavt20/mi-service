.class Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl$1;
.super Ljava/lang/Object;
.source "GnssPowerOptimizeImpl.java"

# interfaces
.implements Lcom/android/server/location/gnss/hal/GpoUtil$IDataEvent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;


# direct methods
.method constructor <init>(Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;

    .line 35
    iput-object p1, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl$1;->this$0:Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public updateDefaultNetwork(I)V
    .locals 0
    .param p1, "type"    # I

    .line 55
    return-void
.end method

.method public updateFeatureSwitch(I)V
    .locals 4
    .param p1, "version"    # I

    .line 38
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl$1;->this$0:Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;

    invoke-static {v0}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->-$$Nest$fgetmGpoVersion(Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;)I

    move-result v0

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl$1;->this$0:Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;

    invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->getEngineStatus()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 40
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl$1;->this$0:Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;

    invoke-static {v0}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->-$$Nest$fgetmGpoUtil(Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;)Lcom/android/server/location/gnss/hal/GpoUtil;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Update Gpo Client"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    const-string v3, "GnssPowerOptimize"

    invoke-virtual {v0, v3, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 41
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl$1;->this$0:Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;

    invoke-static {v0}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->-$$Nest$fgetmContext(Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->init(Landroid/content/Context;)V

    .line 43
    :cond_0
    return-void
.end method

.method public updateGnssStatus(I)V
    .locals 1
    .param p1, "status"    # I

    .line 59
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl$1;->this$0:Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;

    invoke-static {v0}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->-$$Nest$fgetmGpo5Client(Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;)Lcom/android/server/location/gnss/hal/Gpo5Client;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl$1;->this$0:Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;

    invoke-static {v0}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->-$$Nest$fgetmGpo5Client(Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;)Lcom/android/server/location/gnss/hal/Gpo5Client;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/location/gnss/hal/Gpo5Client;->updateGnssStatus(I)V

    .line 60
    :cond_0
    return-void
.end method

.method public updateScreenState(Z)V
    .locals 4
    .param p1, "on"    # Z

    .line 47
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl$1;->this$0:Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;

    invoke-static {v0}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->-$$Nest$fgetmGpoUtil(Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;)Lcom/android/server/location/gnss/hal/GpoUtil;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Screen On ? "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    const-string v3, "GnssPowerOptimize"

    invoke-virtual {v0, v3, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 48
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl$1;->this$0:Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;

    invoke-static {v0}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->-$$Nest$fgetmGpo5Client(Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;)Lcom/android/server/location/gnss/hal/Gpo5Client;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl$1;->this$0:Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;

    invoke-static {v0}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->-$$Nest$fgetmGpo5Client(Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;)Lcom/android/server/location/gnss/hal/Gpo5Client;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/location/gnss/hal/Gpo5Client;->updateScreenState(Z)V

    goto :goto_0

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl$1;->this$0:Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;

    invoke-static {v0}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->-$$Nest$fgetmGpo4Client(Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;)Lcom/android/server/location/gnss/hal/Gpo4Client;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl$1;->this$0:Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;

    invoke-static {v0}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->-$$Nest$fgetmGpo4Client(Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;)Lcom/android/server/location/gnss/hal/Gpo4Client;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/location/gnss/hal/Gpo4Client;->updateScreenState(Z)V

    .line 50
    :cond_1
    :goto_0
    return-void
.end method
