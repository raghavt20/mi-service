.class public Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;
.super Ljava/lang/Object;
.source "GnssPowerOptimizeImpl.java"

# interfaces
.implements Lcom/android/server/location/gnss/hal/GnssPowerOptimizeStub;


# static fields
.field private static final GPO_VERSION_KEY:Ljava/lang/String; = "select_gpo_version"

.field private static final TAG:Ljava/lang/String; = "GnssPowerOptimize"


# instance fields
.field private isFeatureSupport:Z

.field private mContext:Landroid/content/Context;

.field private mGpo4Client:Lcom/android/server/location/gnss/hal/Gpo4Client;

.field private mGpo5Client:Lcom/android/server/location/gnss/hal/Gpo5Client;

.field private final mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

.field private mGpoVersion:I

.field private mHandler:Landroid/os/Handler;

.field private final mIDataEvent:Lcom/android/server/location/gnss/hal/GpoUtil$IDataEvent;


# direct methods
.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmGpo4Client(Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;)Lcom/android/server/location/gnss/hal/Gpo4Client;
    .locals 0

    iget-object p0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpo4Client:Lcom/android/server/location/gnss/hal/Gpo4Client;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmGpo5Client(Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;)Lcom/android/server/location/gnss/hal/Gpo5Client;
    .locals 0

    iget-object p0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpo5Client:Lcom/android/server/location/gnss/hal/Gpo5Client;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmGpoUtil(Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;)Lcom/android/server/location/gnss/hal/GpoUtil;
    .locals 0

    iget-object p0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmGpoVersion(Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;)I
    .locals 0

    iget p0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpoVersion:I

    return p0
.end method

.method static bridge synthetic -$$Nest$mcontrastValue(Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->contrastValue()V

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    invoke-static {}, Lcom/android/server/location/gnss/hal/GpoUtil;->getInstance()Lcom/android/server/location/gnss/hal/GpoUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpo5Client:Lcom/android/server/location/gnss/hal/Gpo5Client;

    .line 28
    iput-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpo4Client:Lcom/android/server/location/gnss/hal/Gpo4Client;

    .line 30
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mHandler:Landroid/os/Handler;

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->isFeatureSupport:Z

    .line 33
    iput v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpoVersion:I

    .line 35
    new-instance v0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl$1;

    invoke-direct {v0, p0}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl$1;-><init>(Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;)V

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mIDataEvent:Lcom/android/server/location/gnss/hal/GpoUtil$IDataEvent;

    return-void
.end method

.method private contrastValue()V
    .locals 3

    .line 121
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    const-string v1, "GnssPowerOptimize"

    const-string v2, "contrastValue"

    invoke-virtual {v0, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 123
    .local v0, "resolver":Landroid/content/ContentResolver;
    const-string/jumbo v1, "select_gpo_version"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 124
    .local v1, "newValue":I
    iget v2, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpoVersion:I

    if-eq v2, v1, :cond_0

    .line 125
    invoke-virtual {p0, v1}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->setGpoVersionValue(I)V

    .line 127
    :cond_0
    return-void
.end method

.method private deinitGpo4()V
    .locals 1

    .line 139
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpo4Client:Lcom/android/server/location/gnss/hal/Gpo4Client;

    if-eqz v0, :cond_0

    .line 140
    invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/Gpo4Client;->deinit()V

    .line 141
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpo4Client:Lcom/android/server/location/gnss/hal/Gpo4Client;

    .line 143
    :cond_0
    return-void
.end method

.method private deinitGpo5()V
    .locals 2

    .line 130
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpo5Client:Lcom/android/server/location/gnss/hal/Gpo5Client;

    if-eqz v0, :cond_0

    .line 131
    invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->deinit()V

    .line 132
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpo5Client:Lcom/android/server/location/gnss/hal/Gpo5Client;

    .line 135
    :cond_0
    invoke-static {}, Lcom/android/server/location/gnss/hal/GnssScoringModelStub;->getInstance()Lcom/android/server/location/gnss/hal/GnssScoringModelStub;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/android/server/location/gnss/hal/GnssScoringModelStub;->init(Z)V

    .line 136
    return-void
.end method

.method private initOnce(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 105
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    const-string v1, "GnssPowerOptimize"

    const-string v2, "initOnce"

    invoke-virtual {v0, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    iput-object p1, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mContext:Landroid/content/Context;

    .line 109
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 110
    const-string/jumbo v1, "select_gpo_version"

    invoke-static {v1}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl$2;

    iget-object v3, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mHandler:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl$2;-><init>(Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;Landroid/os/Handler;)V

    .line 109
    const/4 v3, 0x1

    const/4 v4, -0x1

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 118
    :cond_0
    return-void
.end method


# virtual methods
.method public blockEngineStart()Z
    .locals 4

    .line 180
    iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->isFeatureSupport:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 181
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    const-string v2, "GnssPowerOptimize"

    const-string v3, "blockEngineStart"

    invoke-virtual {v0, v2, v3}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpo5Client:Lcom/android/server/location/gnss/hal/Gpo5Client;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->blockEngineStart()Z

    move-result v0

    return v0

    .line 183
    :cond_1
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpo4Client:Lcom/android/server/location/gnss/hal/Gpo4Client;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/Gpo4Client;->blockEngineStart()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    :cond_2
    return v1
.end method

.method public clearLocationRequest()V
    .locals 3

    .line 188
    iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->isFeatureSupport:Z

    if-nez v0, :cond_0

    return-void

    .line 189
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    const-string v1, "GnssPowerOptimize"

    const-string v2, "clearLocationRequest"

    invoke-virtual {v0, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpo5Client:Lcom/android/server/location/gnss/hal/Gpo5Client;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->clearLocationRequest()V

    goto :goto_0

    .line 191
    :cond_1
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpo4Client:Lcom/android/server/location/gnss/hal/Gpo4Client;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/Gpo4Client;->clearLocationRequest()V

    .line 192
    :cond_2
    :goto_0
    return-void
.end method

.method public disableGnssSwitch()V
    .locals 3

    .line 147
    iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->isFeatureSupport:Z

    if-nez v0, :cond_0

    return-void

    .line 148
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    const-string v1, "GnssPowerOptimize"

    const-string v2, "disableGnssSwitch"

    invoke-virtual {v0, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpo5Client:Lcom/android/server/location/gnss/hal/Gpo5Client;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->disableGnssSwitch()V

    goto :goto_0

    .line 150
    :cond_1
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpo4Client:Lcom/android/server/location/gnss/hal/Gpo4Client;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/Gpo4Client;->disableGnssSwitch()V

    .line 151
    :cond_2
    :goto_0
    return-void
.end method

.method public engineStoppedByGpo()Z
    .locals 3

    .line 220
    iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->isFeatureSupport:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 221
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    const-string v1, "GnssPowerOptimize"

    const-string v2, "engineStoppedByGpo"

    invoke-virtual {v0, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->engineStoppedByGpo()Z

    move-result v0

    return v0
.end method

.method public getEngineStatus()I
    .locals 1

    .line 209
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->getEngineStatus()I

    move-result v0

    return v0
.end method

.method public init(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 65
    const-string v0, "GnssPowerOptimize"

    if-nez p1, :cond_0

    .line 66
    iget-object v1, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    const-string v2, "init transfer null context"

    invoke-virtual {v1, v0, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->loge(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    return-void

    .line 69
    :cond_0
    iget-object v1, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    const-string v2, "init "

    invoke-virtual {v1, v0, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    invoke-direct {p0, p1}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->initOnce(Landroid/content/Context;)V

    .line 71
    iget-object v1, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    iget-object v2, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mIDataEvent:Lcom/android/server/location/gnss/hal/GpoUtil$IDataEvent;

    invoke-virtual {v1, v2, v3}, Lcom/android/server/location/gnss/hal/GpoUtil;->initOnce(Landroid/content/Context;Lcom/android/server/location/gnss/hal/GpoUtil$IDataEvent;)V

    .line 72
    iget-object v1, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    invoke-virtual {v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->getGpoVersion()I

    move-result v1

    iput v1, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpoVersion:I

    .line 73
    iget-object v1, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Gpo Version: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpoVersion:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 74
    iget v1, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpoVersion:I

    packed-switch v1, :pswitch_data_0

    .line 96
    iget-object v1, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    const-string v2, "Not Support GPO."

    invoke-virtual {v1, v0, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->isFeatureSupport:Z

    .line 98
    invoke-direct {p0}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->deinitGpo5()V

    .line 99
    invoke-direct {p0}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->deinitGpo4()V

    goto :goto_0

    .line 76
    :pswitch_0
    iput-boolean v3, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->isFeatureSupport:Z

    .line 77
    invoke-static {}, Lcom/android/server/location/gnss/hal/Gpo5Client;->getInstance()Lcom/android/server/location/gnss/hal/Gpo5Client;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpo5Client:Lcom/android/server/location/gnss/hal/Gpo5Client;

    .line 78
    iget-object v2, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/android/server/location/gnss/hal/Gpo5Client;->init(Landroid/content/Context;)V

    .line 79
    iget-object v1, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpo5Client:Lcom/android/server/location/gnss/hal/Gpo5Client;

    invoke-virtual {v1}, Lcom/android/server/location/gnss/hal/Gpo5Client;->checkSensorSupport()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 81
    invoke-static {}, Lcom/android/server/location/gnss/hal/GnssScoringModelStub;->getInstance()Lcom/android/server/location/gnss/hal/GnssScoringModelStub;

    move-result-object v0

    invoke-interface {v0, v3}, Lcom/android/server/location/gnss/hal/GnssScoringModelStub;->init(Z)V

    .line 82
    invoke-direct {p0}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->deinitGpo4()V

    .line 83
    goto :goto_0

    .line 86
    :cond_1
    iget v1, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpoVersion:I

    sub-int/2addr v1, v3

    iput v1, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpoVersion:I

    .line 87
    iget-object v1, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Sensor do not support, downgrade to Version: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v4, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpoVersion:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2, v3}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 88
    invoke-direct {p0}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->deinitGpo5()V

    .line 90
    :pswitch_1
    iput-boolean v3, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->isFeatureSupport:Z

    .line 91
    invoke-static {}, Lcom/android/server/location/gnss/hal/Gpo4Client;->getInstance()Lcom/android/server/location/gnss/hal/Gpo4Client;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpo4Client:Lcom/android/server/location/gnss/hal/Gpo4Client;

    .line 92
    iget-object v1, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/android/server/location/gnss/hal/Gpo4Client;->init(Landroid/content/Context;)V

    .line 93
    invoke-direct {p0}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->deinitGpo5()V

    .line 94
    nop

    .line 101
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public isBlackListControlEnabled()Z
    .locals 2

    .line 155
    iget v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpoVersion:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public recordEngineUsageDaily(J)V
    .locals 3
    .param p1, "time"    # J

    .line 227
    iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->isFeatureSupport:Z

    if-nez v0, :cond_0

    return-void

    .line 228
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "recordEngineUsageDaily: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GnssPowerOptimize"

    invoke-virtual {v0, v2, v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/location/gnss/hal/GpoUtil;->recordEngineUsageDaily(J)V

    .line 230
    return-void
.end method

.method public removeLocationRequestId(I)V
    .locals 3
    .param p1, "listenerHashCode"    # I

    .line 172
    iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->isFeatureSupport:Z

    if-nez v0, :cond_0

    return-void

    .line 173
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "removeLocationRequestId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GnssPowerOptimize"

    invoke-virtual {v0, v2, v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpo5Client:Lcom/android/server/location/gnss/hal/Gpo5Client;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Lcom/android/server/location/gnss/hal/Gpo5Client;->removeLocationRequestId(I)V

    goto :goto_0

    .line 175
    :cond_1
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpo4Client:Lcom/android/server/location/gnss/hal/Gpo4Client;

    if-eqz v0, :cond_2

    invoke-virtual {v0, p1}, Lcom/android/server/location/gnss/hal/Gpo4Client;->removeLocationRequestId(I)V

    .line 176
    :cond_2
    :goto_0
    return-void
.end method

.method public reportLocation2Gpo(Landroid/location/Location;)V
    .locals 3
    .param p1, "location"    # Landroid/location/Location;

    .line 196
    iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->isFeatureSupport:Z

    if-nez v0, :cond_0

    return-void

    .line 197
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    const-string v1, "GnssPowerOptimize"

    const-string v2, "reportLocation2Gpo"

    invoke-virtual {v0, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpo5Client:Lcom/android/server/location/gnss/hal/Gpo5Client;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Lcom/android/server/location/gnss/hal/Gpo5Client;->reportLocation2Gpo(Landroid/location/Location;)V

    goto :goto_0

    .line 199
    :cond_1
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpo4Client:Lcom/android/server/location/gnss/hal/Gpo4Client;

    if-eqz v0, :cond_2

    invoke-virtual {v0, p1}, Lcom/android/server/location/gnss/hal/Gpo4Client;->reportLocation2Gpo(Landroid/location/Location;)V

    .line 200
    :cond_2
    :goto_0
    return-void
.end method

.method public saveLocationRequestId(ILjava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V
    .locals 10
    .param p1, "uid"    # I
    .param p2, "pkn"    # Ljava/lang/String;
    .param p3, "provider"    # Ljava/lang/String;
    .param p4, "listenerHashCode"    # I
    .param p5, "callbackType"    # Ljava/lang/Object;

    .line 161
    iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->isFeatureSupport:Z

    if-eqz v0, :cond_3

    const-string v0, "passive"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 162
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "saveLocationRequestId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GnssPowerOptimize"

    invoke-virtual {v0, v2, v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    iget-object v3, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpo5Client:Lcom/android/server/location/gnss/hal/Gpo5Client;

    if-eqz v3, :cond_1

    .line 164
    move v4, p1

    move-object v5, p2

    move-object v6, p3

    move v7, p4

    move-object v8, p5

    invoke-virtual/range {v3 .. v8}, Lcom/android/server/location/gnss/hal/Gpo5Client;->saveLocationRequestId(ILjava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    goto :goto_0

    .line 165
    :cond_1
    iget-object v4, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpo4Client:Lcom/android/server/location/gnss/hal/Gpo4Client;

    if-eqz v4, :cond_2

    .line 166
    move v5, p1

    move-object v6, p2

    move-object v7, p3

    move v8, p4

    move-object v9, p5

    invoke-virtual/range {v4 .. v9}, Lcom/android/server/location/gnss/hal/Gpo4Client;->saveLocationRequestId(ILjava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    .line 168
    :cond_2
    :goto_0
    return-void

    .line 161
    :cond_3
    :goto_1
    return-void
.end method

.method public setEngineStatus(I)V
    .locals 1
    .param p1, "status"    # I

    .line 214
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    invoke-virtual {v0, p1}, Lcom/android/server/location/gnss/hal/GpoUtil;->setEngineStatus(I)V

    .line 215
    return-void
.end method

.method public setGpoVersionValue(I)V
    .locals 1
    .param p1, "newValue"    # I

    .line 204
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    invoke-virtual {v0, p1}, Lcom/android/server/location/gnss/hal/GpoUtil;->setGpoVersionValue(I)V

    .line 205
    return-void
.end method
