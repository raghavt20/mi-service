.class Lcom/android/server/location/gnss/hal/Gpo5Client$1;
.super Ljava/lang/Object;
.source "Gpo5Client.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/location/gnss/hal/Gpo5Client;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/location/gnss/hal/Gpo5Client;


# direct methods
.method constructor <init>(Lcom/android/server/location/gnss/hal/Gpo5Client;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/location/gnss/hal/Gpo5Client;

    .line 96
    iput-object p1, p0, Lcom/android/server/location/gnss/hal/Gpo5Client$1;->this$0:Lcom/android/server/location/gnss/hal/Gpo5Client;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .line 116
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .line 99
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client$1;->this$0:Lcom/android/server/location/gnss/hal/Gpo5Client;

    invoke-static {v0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->-$$Nest$fgetmGpoUtil(Lcom/android/server/location/gnss/hal/Gpo5Client;)Lcom/android/server/location/gnss/hal/GpoUtil;

    move-result-object v0

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v2, 0x0

    aget v1, v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->setUserBehav(I)V

    .line 100
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client$1;->this$0:Lcom/android/server/location/gnss/hal/Gpo5Client;

    invoke-static {v0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->-$$Nest$fgetmGpoUtil(Lcom/android/server/location/gnss/hal/Gpo5Client;)Lcom/android/server/location/gnss/hal/GpoUtil;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Sensor report user behavior: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v3, v3, v2

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "Gpo5Client"

    invoke-virtual {v0, v3, v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v0, v2

    const/high16 v1, 0x40800000    # 4.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 103
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client$1;->this$0:Lcom/android/server/location/gnss/hal/Gpo5Client;

    invoke-static {v0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->-$$Nest$fgetmLastSensorTrafficTime(Lcom/android/server/location/gnss/hal/Gpo5Client;)Ljava/util/concurrent/atomic/AtomicLong;

    move-result-object v0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 105
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client$1;->this$0:Lcom/android/server/location/gnss/hal/Gpo5Client;

    invoke-static {v0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->-$$Nest$fgetmGpoUtil(Lcom/android/server/location/gnss/hal/Gpo5Client;)Lcom/android/server/location/gnss/hal/GpoUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->getEngineStatus()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 106
    return-void

    .line 108
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client$1;->this$0:Lcom/android/server/location/gnss/hal/Gpo5Client;

    invoke-static {v0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->-$$Nest$fgetmGpoUtil(Lcom/android/server/location/gnss/hal/Gpo5Client;)Lcom/android/server/location/gnss/hal/GpoUtil;

    move-result-object v0

    const-string v1, "User is on Traffic."

    const/4 v2, 0x1

    invoke-virtual {v0, v3, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 109
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo5Client$1;->this$0:Lcom/android/server/location/gnss/hal/Gpo5Client;

    invoke-static {v0}, Lcom/android/server/location/gnss/hal/Gpo5Client;->-$$Nest$mhandleLeaveSmallArea(Lcom/android/server/location/gnss/hal/Gpo5Client;)V

    .line 111
    :cond_1
    return-void
.end method
