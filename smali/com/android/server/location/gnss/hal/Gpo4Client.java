public class com.android.server.location.gnss.hal.Gpo4Client {
	 /* .source "Gpo4Client.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String TAG;
private static final Long TIME_CHECK_NAV_APP;
private static final Long TIME_MAX_TRAFFIC_IGNORE;
private static final Integer TIME_POST_DELAY_CHECK_NLP;
private static final Integer TIME_POST_TIMEOUT_OBTAIN_NLP;
private static volatile com.android.server.location.gnss.hal.Gpo4Client instance;
/* # instance fields */
private java.util.concurrent.ScheduledFuture futureCheckNavigation1;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/concurrent/ScheduledFuture<", */
/* "*>;" */
/* } */
} // .end annotation
} // .end field
private java.util.concurrent.ScheduledFuture futureCheckNavigation2;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/concurrent/ScheduledFuture<", */
/* "*>;" */
/* } */
} // .end annotation
} // .end field
private java.util.concurrent.ScheduledFuture futureCheckRequest;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/concurrent/ScheduledFuture<", */
/* "*>;" */
/* } */
} // .end annotation
} // .end field
private final java.util.concurrent.atomic.AtomicBoolean isFeatureEnabled;
private final java.util.concurrent.atomic.AtomicBoolean isScreenOn;
private android.content.Context mContext;
private final java.util.Map mGlpRequestMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Integer;", */
/* "Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final com.android.server.location.gnss.hal.GpoUtil mGpoUtil;
private android.location.Location mLastNlpLocation;
private final java.util.concurrent.atomic.AtomicLong mLastNlpTime;
private final java.util.concurrent.atomic.AtomicLong mLastTrafficTime;
private android.location.LocationManager mLocationManager;
private final java.util.Map mNavAppInfo;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final android.location.LocationListener mNetworkLocationListener;
private final java.util.Map mNlpRequestMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Integer;", */
/* "Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.util.concurrent.atomic.AtomicLong mObtainNlpTime;
private final android.location.LocationListener mPassiveLocationListener;
private final java.util.concurrent.atomic.AtomicLong mRequestNlpTime;
private java.util.concurrent.ScheduledThreadPoolExecutor scheduledThreadPoolExecutor;
/* # direct methods */
public static void $r8$lambda$-XGP8cAHfNg3ENKjODZwP8C80Kk ( com.android.server.location.gnss.hal.Gpo4Client p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo4Client;->lambda$saveLocationRequestId$0()V */
return;
} // .end method
public static void $r8$lambda$4pV_jCN4PfmJzUBiaAu74n5iawk ( com.android.server.location.gnss.hal.Gpo4Client p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo4Client;->lambda$checkAppUsage$6()V */
return;
} // .end method
public static void $r8$lambda$75TPzP_NztHwaGVZbO20HtTxwTY ( com.android.server.location.gnss.hal.Gpo4Client p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo4Client;->lambda$unregisterNetworkLocationUpdates$5()V */
return;
} // .end method
public static java.lang.Boolean $r8$lambda$alKhd4AfgH5cOhYbGCx3v0DKc5s ( com.android.server.location.gnss.hal.Gpo4Client p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo4Client;->lambda$registerNetworkLocationUpdates$4()Ljava/lang/Boolean; */
} // .end method
public static void $r8$lambda$fbmXz5iO6O8eJRg7KyNRPVcWz_A ( com.android.server.location.gnss.hal.Gpo4Client p0, android.location.Location p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/location/gnss/hal/Gpo4Client;->lambda$new$2(Landroid/location/Location;)V */
return;
} // .end method
public static void $r8$lambda$j9DcByIdrTCXHSd2ObGiyOUcQm0 ( com.android.server.location.gnss.hal.Gpo4Client p0, Integer p1, Integer p2 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/location/gnss/hal/Gpo4Client;->lambda$saveLocationRequestId$1(II)V */
return;
} // .end method
public static void $r8$lambda$y0E58QqoFXDKH97D3YUmF32a-aM ( com.android.server.location.gnss.hal.Gpo4Client p0, android.location.Location p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/location/gnss/hal/Gpo4Client;->lambda$new$3(Landroid/location/Location;)V */
return;
} // .end method
private com.android.server.location.gnss.hal.Gpo4Client ( ) {
/* .locals 3 */
/* .line 71 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 38 */
com.android.server.location.gnss.hal.GpoUtil .getInstance ( );
this.mGpoUtil = v0;
/* .line 42 */
int v0 = 0; // const/4 v0, 0x0
this.mLastNlpLocation = v0;
/* .line 43 */
this.futureCheckRequest = v0;
/* .line 44 */
this.futureCheckNavigation1 = v0;
/* .line 45 */
this.futureCheckNavigation2 = v0;
/* .line 47 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.mGlpRequestMap = v0;
/* .line 48 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.mNlpRequestMap = v0;
/* .line 49 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.mNavAppInfo = v0;
/* .line 51 */
/* new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V */
this.isFeatureEnabled = v0;
/* .line 52 */
/* new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean; */
int v1 = 1; // const/4 v1, 0x1
/* invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V */
this.isScreenOn = v0;
/* .line 53 */
/* new-instance v0, Ljava/util/concurrent/atomic/AtomicLong; */
/* const-wide/16 v1, 0x0 */
/* invoke-direct {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V */
this.mRequestNlpTime = v0;
/* .line 54 */
/* new-instance v0, Ljava/util/concurrent/atomic/AtomicLong; */
/* invoke-direct {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V */
this.mObtainNlpTime = v0;
/* .line 55 */
/* new-instance v0, Ljava/util/concurrent/atomic/AtomicLong; */
/* invoke-direct {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V */
this.mLastTrafficTime = v0;
/* .line 56 */
/* new-instance v0, Ljava/util/concurrent/atomic/AtomicLong; */
/* invoke-direct {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V */
this.mLastNlpTime = v0;
/* .line 149 */
/* new-instance v0, Lcom/android/server/location/gnss/hal/Gpo4Client$$ExternalSyntheticLambda3; */
/* invoke-direct {v0, p0}, Lcom/android/server/location/gnss/hal/Gpo4Client$$ExternalSyntheticLambda3;-><init>(Lcom/android/server/location/gnss/hal/Gpo4Client;)V */
this.mPassiveLocationListener = v0;
/* .line 206 */
/* new-instance v0, Lcom/android/server/location/gnss/hal/Gpo4Client$$ExternalSyntheticLambda4; */
/* invoke-direct {v0, p0}, Lcom/android/server/location/gnss/hal/Gpo4Client$$ExternalSyntheticLambda4;-><init>(Lcom/android/server/location/gnss/hal/Gpo4Client;)V */
this.mNetworkLocationListener = v0;
/* .line 71 */
return;
} // .end method
private java.util.concurrent.ScheduledFuture checkAppUsage ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "timeout" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I)", */
/* "Ljava/util/concurrent/ScheduledFuture<", */
/* "*>;" */
/* } */
} // .end annotation
/* .line 277 */
try { // :try_start_0
v0 = this.mGpoUtil;
final String v1 = "Gpo4Client"; // const-string v1, "Gpo4Client"
final String v2 = "checkAppUsage"; // const-string v2, "checkAppUsage"
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logv ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 278 */
v0 = this.scheduledThreadPoolExecutor;
/* new-instance v1, Lcom/android/server/location/gnss/hal/Gpo4Client$$ExternalSyntheticLambda1; */
/* invoke-direct {v1, p0}, Lcom/android/server/location/gnss/hal/Gpo4Client$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/location/gnss/hal/Gpo4Client;)V */
/* int-to-long v2, p1 */
v4 = java.util.concurrent.TimeUnit.MILLISECONDS;
(( java.util.concurrent.ScheduledThreadPoolExecutor ) v0 ).schedule ( v1, v2, v3, v4 ); // invoke-virtual {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 285 */
/* :catch_0 */
/* move-exception v0 */
/* .line 286 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 288 */
} // .end local v0 # "e":Ljava/lang/Exception;
int v0 = 0; // const/4 v0, 0x0
} // .end method
private void checkFeatureSwitch ( ) {
/* .locals 7 */
/* .line 101 */
v0 = v0 = this.mGlpRequestMap;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 102 */
v0 = this.isFeatureEnabled;
v1 = this.mGpoUtil;
v1 = (( com.android.server.location.gnss.hal.GpoUtil ) v1 ).checkHeavyUser ( ); // invoke-virtual {v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->checkHeavyUser()Z
int v2 = 0; // const/4 v2, 0x0
/* if-nez v1, :cond_1 */
v1 = this.mGpoUtil;
v1 = (( com.android.server.location.gnss.hal.GpoUtil ) v1 ).isNetworkConnected ( ); // invoke-virtual {v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->isNetworkConnected()Z
if ( v1 != null) { // if-eqz v1, :cond_1
v1 = this.isScreenOn;
v1 = (( java.util.concurrent.atomic.AtomicBoolean ) v1 ).get ( ); // invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
if ( v1 != null) { // if-eqz v1, :cond_1
v1 = this.mLastTrafficTime;
/* .line 103 */
(( java.util.concurrent.atomic.AtomicLong ) v1 ).get ( ); // invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicLong;->get()J
/* move-result-wide v3 */
/* const-wide/16 v5, 0x0 */
/* cmp-long v1, v3, v5 */
if ( v1 != null) { // if-eqz v1, :cond_0
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v3 */
v1 = this.mLastTrafficTime;
/* .line 104 */
(( java.util.concurrent.atomic.AtomicLong ) v1 ).get ( ); // invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicLong;->get()J
/* move-result-wide v5 */
/* sub-long/2addr v3, v5 */
/* const-wide/32 v5, 0x493e0 */
/* cmp-long v1, v3, v5 */
/* if-ltz v1, :cond_1 */
} // :cond_0
int v1 = 1; // const/4 v1, 0x1
} // :cond_1
/* move v1, v2 */
/* .line 102 */
} // :goto_0
(( java.util.concurrent.atomic.AtomicBoolean ) v0 ).set ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
/* .line 105 */
v0 = this.mGpoUtil;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "isGpoFeatureEnabled ? "; // const-string v3, "isGpoFeatureEnabled ? "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.isFeatureEnabled;
v3 = (( java.util.concurrent.atomic.AtomicBoolean ) v3 ).get ( ); // invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "Gpo4Client"; // const-string v3, "Gpo4Client"
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logi ( v3, v1, v2 ); // invoke-virtual {v0, v3, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V
/* .line 107 */
} // :cond_2
return;
} // .end method
private void delieverLocation ( java.lang.Object p0, java.util.List p1 ) {
/* .locals 5 */
/* .param p1, "callbackType" # Ljava/lang/Object; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/Object;", */
/* "Ljava/util/List<", */
/* "Landroid/location/Location;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 176 */
/* .local p2, "locations":Ljava/util/List;, "Ljava/util/List<Landroid/location/Location;>;" */
/* instance-of v0, p1, Landroid/location/ILocationListener; */
final String v1 = "Gpo4Client"; // const-string v1, "Gpo4Client"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 178 */
try { // :try_start_0
/* move-object v0, p1 */
/* check-cast v0, Landroid/location/ILocationListener; */
int v2 = 0; // const/4 v2, 0x0
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 179 */
/* :catch_0 */
/* move-exception v0 */
/* .line 180 */
/* .local v0, "e":Landroid/os/RemoteException; */
v2 = this.mGlpRequestMap;
/* .line 181 */
v2 = this.mGpoUtil;
final String v3 = "onLocationChanged RemoteException"; // const-string v3, "onLocationChanged RemoteException"
(( com.android.server.location.gnss.hal.GpoUtil ) v2 ).loge ( v1, v3 ); // invoke-virtual {v2, v1, v3}, Lcom/android/server/location/gnss/hal/GpoUtil;->loge(Ljava/lang/String;Ljava/lang/String;)V
/* .line 182 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :goto_0
/* .line 184 */
} // :cond_0
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 187 */
/* .local v0, "intent":Landroid/content/Intent; */
int v2 = 0; // const/4 v2, 0x0
/* check-cast v3, Landroid/os/Parcelable; */
final String v4 = "location"; // const-string v4, "location"
(( android.content.Intent ) v0 ).putExtra ( v4, v3 ); // invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;
/* .line 189 */
try { // :try_start_1
/* move-object v3, p1 */
/* check-cast v3, Landroid/app/PendingIntent; */
v4 = this.mContext;
(( android.app.PendingIntent ) v3 ).send ( v4, v2, v0 ); // invoke-virtual {v3, v4, v2, v0}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V
/* :try_end_1 */
/* .catch Landroid/app/PendingIntent$CanceledException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 193 */
/* .line 190 */
/* :catch_1 */
/* move-exception v2 */
/* .line 191 */
/* .local v2, "e":Landroid/app/PendingIntent$CanceledException; */
v3 = this.mGlpRequestMap;
/* .line 192 */
v3 = this.mGpoUtil;
final String v4 = "PendingInent send CanceledException"; // const-string v4, "PendingInent send CanceledException"
(( com.android.server.location.gnss.hal.GpoUtil ) v3 ).loge ( v1, v4 ); // invoke-virtual {v3, v1, v4}, Lcom/android/server/location/gnss/hal/GpoUtil;->loge(Ljava/lang/String;Ljava/lang/String;)V
/* .line 195 */
} // .end local v0 # "intent":Landroid/content/Intent;
} // .end local v2 # "e":Landroid/app/PendingIntent$CanceledException;
} // :goto_1
return;
} // .end method
public static com.android.server.location.gnss.hal.Gpo4Client getInstance ( ) {
/* .locals 2 */
/* .line 61 */
v0 = com.android.server.location.gnss.hal.Gpo4Client.instance;
/* if-nez v0, :cond_1 */
/* .line 62 */
/* const-class v0, Lcom/android/server/location/gnss/hal/Gpo4Client; */
/* monitor-enter v0 */
/* .line 63 */
try { // :try_start_0
v1 = com.android.server.location.gnss.hal.Gpo4Client.instance;
/* if-nez v1, :cond_0 */
/* .line 64 */
/* new-instance v1, Lcom/android/server/location/gnss/hal/Gpo4Client; */
/* invoke-direct {v1}, Lcom/android/server/location/gnss/hal/Gpo4Client;-><init>()V */
/* .line 66 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 68 */
} // :cond_1
} // :goto_0
v0 = com.android.server.location.gnss.hal.Gpo4Client.instance;
} // .end method
private void lambda$checkAppUsage$6 ( ) { //synthethic
/* .locals 3 */
/* .line 280 */
v0 = this.mGpoUtil;
v0 = (( com.android.server.location.gnss.hal.GpoUtil ) v0 ).getEngineStatus ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->getEngineStatus()I
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* .line 281 */
v0 = this.mGpoUtil;
final String v1 = "Gpo4Client"; // const-string v1, "Gpo4Client"
final String v2 = "navigating app, restart gnss engine"; // const-string v2, "navigating app, restart gnss engine"
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logv ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 282 */
v0 = this.mGpoUtil;
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).doStartEngineByInstance ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->doStartEngineByInstance()Z
/* .line 284 */
} // :cond_0
return;
} // .end method
private void lambda$new$2 ( android.location.Location p0 ) { //synthethic
/* .locals 9 */
/* .param p1, "location" # Landroid/location/Location; */
/* .line 150 */
v0 = this.isFeatureEnabled;
v0 = (( java.util.concurrent.atomic.AtomicBoolean ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
if ( v0 != null) { // if-eqz v0, :cond_2
final String v0 = "network"; // const-string v0, "network"
(( android.location.Location ) p1 ).getProvider ( ); // invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = this.mGpoUtil;
/* .line 151 */
v0 = (( com.android.server.location.gnss.hal.GpoUtil ) v0 ).getEngineStatus ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->getEngineStatus()I
int v1 = 2; // const/4 v1, 0x2
/* if-eq v0, v1, :cond_2 */
/* .line 152 */
v0 = (( android.location.Location ) p1 ).getAccuracy ( ); // invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F
/* const/high16 v1, 0x43160000 # 150.0f */
/* cmpg-float v0, v0, v1 */
/* if-gtz v0, :cond_2 */
/* .line 154 */
v0 = this.mGpoUtil;
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).convertNlp2Glp ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/location/gnss/hal/GpoUtil;->convertNlp2Glp(Landroid/location/Location;)Landroid/location/Location;
/* .line 155 */
/* .local v0, "gLocation":Landroid/location/Location; */
this.mLastNlpLocation = v0;
/* .line 156 */
v1 = this.mLastNlpTime;
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v2 */
(( java.util.concurrent.atomic.AtomicLong ) v1 ).set ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V
/* .line 157 */
v1 = this.mGlpRequestMap;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Ljava/util/Map$Entry; */
/* .line 158 */
/* .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;>;" */
/* check-cast v3, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder; */
v3 = (( com.android.server.location.gnss.hal.Gpo4Client$LocationRequestRecorder ) v3 ).getNlpReturned ( ); // invoke-virtual {v3}, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;->getNlpReturned()Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* goto/16 :goto_1 */
/* .line 159 */
} // :cond_0
/* check-cast v3, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder; */
int v4 = 1; // const/4 v4, 0x1
(( com.android.server.location.gnss.hal.Gpo4Client$LocationRequestRecorder ) v3 ).setNlpReturned ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;->setNlpReturned(Z)V
/* .line 160 */
/* filled-new-array {v0}, [Landroid/location/Location; */
java.util.Arrays .asList ( v3 );
/* .line 161 */
/* .local v3, "locations":Ljava/util/List;, "Ljava/util/List<Landroid/location/Location;>;" */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v6, "use nlp " */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.location.Location ) v0 ).toString ( ); // invoke-virtual {v0}, Landroid/location/Location;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = ", listenerHashCode is "; // const-string v6, ", listenerHashCode is "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 162 */
/* check-cast v6, Ljava/lang/Integer; */
(( java.lang.Integer ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/Integer;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = ", callbackType is ILocationListener "; // const-string v6, ", callbackType is ILocationListener "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 164 */
/* check-cast v6, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder; */
(( com.android.server.location.gnss.hal.Gpo4Client$LocationRequestRecorder ) v6 ).getCallbackType ( ); // invoke-virtual {v6}, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;->getCallbackType()Ljava/lang/Object;
/* instance-of v6, v6, Landroid/location/ILocationListener; */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 165 */
/* .local v5, "logInfo":Ljava/lang/String; */
v6 = this.mGpoUtil;
final String v7 = "Gpo4Client"; // const-string v7, "Gpo4Client"
(( com.android.server.location.gnss.hal.GpoUtil ) v6 ).logv ( v7, v5 ); // invoke-virtual {v6, v7, v5}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 166 */
v6 = this.mGpoUtil;
(( com.android.server.location.gnss.hal.GpoUtil ) v6 ).logEn ( v5 ); // invoke-virtual {v6, v5}, Lcom/android/server/location/gnss/hal/GpoUtil;->logEn(Ljava/lang/String;)V
/* .line 167 */
/* check-cast v6, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder; */
(( com.android.server.location.gnss.hal.Gpo4Client$LocationRequestRecorder ) v6 ).getCallbackType ( ); // invoke-virtual {v6}, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;->getCallbackType()Ljava/lang/Object;
/* invoke-direct {p0, v6, v3}, Lcom/android/server/location/gnss/hal/Gpo4Client;->delieverLocation(Ljava/lang/Object;Ljava/util/List;)V */
/* .line 168 */
/* check-cast v6, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder; */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v7 */
(( com.android.server.location.gnss.hal.Gpo4Client$LocationRequestRecorder ) v6 ).setNlpReturnTime ( v7, v8 ); // invoke-virtual {v6, v7, v8}, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;->setNlpReturnTime(J)V
/* .line 169 */
/* const/16 v6, 0xfa */
/* invoke-direct {p0, v6}, Lcom/android/server/location/gnss/hal/Gpo4Client;->checkAppUsage(I)Ljava/util/concurrent/ScheduledFuture; */
this.futureCheckNavigation2 = v6;
/* .line 170 */
v6 = this.futureCheckRequest;
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 171 */
} // .end local v2 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;>;"
} // .end local v3 # "locations":Ljava/util/List;, "Ljava/util/List<Landroid/location/Location;>;"
} // .end local v5 # "logInfo":Ljava/lang/String;
} // :cond_1
/* goto/16 :goto_0 */
/* .line 173 */
} // .end local v0 # "gLocation":Landroid/location/Location;
} // :cond_2
} // :goto_1
return;
} // .end method
private void lambda$new$3 ( android.location.Location p0 ) { //synthethic
/* .locals 3 */
/* .param p1, "location" # Landroid/location/Location; */
/* .line 207 */
v0 = this.mObtainNlpTime;
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v1 */
(( java.util.concurrent.atomic.AtomicLong ) v0 ).set ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V
return;
} // .end method
private java.lang.Boolean lambda$registerNetworkLocationUpdates$4 ( ) { //synthethic
/* .locals 6 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 230 */
v0 = this.mGpoUtil;
final String v1 = "Gpo4Client"; // const-string v1, "Gpo4Client"
final String v2 = "request NLP."; // const-string v2, "request NLP."
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logv ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 231 */
v0 = this.mRequestNlpTime;
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v1 */
(( java.util.concurrent.atomic.AtomicLong ) v0 ).set ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V
/* .line 232 */
v0 = this.mLocationManager;
final String v1 = "network"; // const-string v1, "network"
/* const-wide/16 v2, 0x3e8 */
/* const/high16 v4, 0x447a0000 # 1000.0f */
int v5 = 1; // const/4 v5, 0x1
android.location.LocationRequest .createFromDeprecatedProvider ( v1,v2,v3,v4,v5 );
v2 = this.mNetworkLocationListener;
v3 = this.mContext;
/* .line 234 */
(( android.content.Context ) v3 ).getMainLooper ( ); // invoke-virtual {v3}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;
/* .line 232 */
(( android.location.LocationManager ) v0 ).requestLocationUpdates ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/location/LocationManager;->requestLocationUpdates(Landroid/location/LocationRequest;Landroid/location/LocationListener;Landroid/os/Looper;)V
/* .line 235 */
java.lang.Boolean .valueOf ( v5 );
} // .end method
private void lambda$saveLocationRequestId$0 ( ) { //synthethic
/* .locals 4 */
/* .line 139 */
v0 = this.mObtainNlpTime;
(( java.util.concurrent.atomic.AtomicLong ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J
/* move-result-wide v0 */
v2 = this.mRequestNlpTime;
(( java.util.concurrent.atomic.AtomicLong ) v2 ).get ( ); // invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicLong;->get()J
/* move-result-wide v2 */
/* cmp-long v0, v0, v2 */
/* if-gtz v0, :cond_0 */
/* .line 140 */
/* invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo4Client;->unregisterNetworkLocationUpdates()V */
/* .line 142 */
} // :cond_0
return;
} // .end method
private void lambda$saveLocationRequestId$1 ( Integer p0, Integer p1 ) { //synthethic
/* .locals 6 */
/* .param p1, "listenerHashCode" # I */
/* .param p2, "uid" # I */
/* .line 126 */
v0 = this.mGlpRequestMap;
v0 = java.lang.Integer .valueOf ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 127 */
int v0 = 0; // const/4 v0, 0x0
/* .line 128 */
/* .local v0, "existNlp":Z */
v1 = this.mNlpRequestMap;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder; */
/* .line 129 */
/* .local v2, "r":Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder; */
v3 = (( com.android.server.location.gnss.hal.Gpo4Client$LocationRequestRecorder ) v2 ).existUid ( p2 ); // invoke-virtual {v2, p2}, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;->existUid(I)Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 130 */
int v0 = 1; // const/4 v0, 0x1
/* .line 131 */
/* .line 133 */
} // .end local v2 # "r":Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;
} // :cond_0
/* .line 134 */
} // :cond_1
} // :goto_1
v1 = this.mGpoUtil;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "LocationRequest of "; // const-string v3, "LocationRequest of "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " existNlp ? "; // const-string v3, " existNlp ? "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v3 = 0; // const/4 v3, 0x0
final String v4 = "Gpo4Client"; // const-string v4, "Gpo4Client"
(( com.android.server.location.gnss.hal.GpoUtil ) v1 ).logi ( v4, v2, v3 ); // invoke-virtual {v1, v4, v2, v3}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V
/* .line 136 */
/* if-nez v0, :cond_2 */
v1 = /* invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo4Client;->registerNetworkLocationUpdates()Z */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 137 */
v1 = this.scheduledThreadPoolExecutor;
/* new-instance v2, Lcom/android/server/location/gnss/hal/Gpo4Client$$ExternalSyntheticLambda6; */
/* invoke-direct {v2, p0}, Lcom/android/server/location/gnss/hal/Gpo4Client$$ExternalSyntheticLambda6;-><init>(Lcom/android/server/location/gnss/hal/Gpo4Client;)V */
/* const-wide/16 v3, 0x7d0 */
v5 = java.util.concurrent.TimeUnit.MILLISECONDS;
(( java.util.concurrent.ScheduledThreadPoolExecutor ) v1 ).schedule ( v2, v3, v4, v5 ); // invoke-virtual {v1, v2, v3, v4, v5}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;
this.futureCheckRequest = v1;
/* .line 145 */
} // .end local v0 # "existNlp":Z
} // :cond_2
return;
} // .end method
private void lambda$unregisterNetworkLocationUpdates$5 ( ) { //synthethic
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 241 */
v0 = this.mLocationManager;
v1 = this.mNetworkLocationListener;
(( android.location.LocationManager ) v0 ).removeUpdates ( v1 ); // invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V
return;
} // .end method
private Boolean registerNetworkLocationUpdates ( ) {
/* .locals 9 */
/* .line 210 */
v0 = this.mGpoUtil;
v0 = (( com.android.server.location.gnss.hal.GpoUtil ) v0 ).isNetworkConnected ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->isNetworkConnected()Z
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_5
v0 = this.mLocationManager;
final String v2 = "network"; // const-string v2, "network"
v0 = (( android.location.LocationManager ) v0 ).isProviderEnabled ( v2 ); // invoke-virtual {v0, v2}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z
/* if-nez v0, :cond_0 */
/* goto/16 :goto_2 */
/* .line 211 */
} // :cond_0
v0 = this.mLastNlpLocation;
if ( v0 != null) { // if-eqz v0, :cond_4
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v2 */
v0 = this.mLastNlpTime;
(( java.util.concurrent.atomic.AtomicLong ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J
/* move-result-wide v4 */
/* sub-long/2addr v2, v4 */
/* const-wide/16 v4, 0x7d0 */
/* cmp-long v0, v2, v4 */
/* if-gtz v0, :cond_4 */
/* .line 212 */
v0 = this.mGlpRequestMap;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_3
/* check-cast v2, Ljava/util/Map$Entry; */
/* .line 213 */
/* .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;>;" */
/* check-cast v3, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder; */
v3 = (( com.android.server.location.gnss.hal.Gpo4Client$LocationRequestRecorder ) v3 ).getNlpReturned ( ); // invoke-virtual {v3}, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;->getNlpReturned()Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* goto/16 :goto_1 */
/* .line 214 */
} // :cond_1
/* check-cast v3, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder; */
int v4 = 1; // const/4 v4, 0x1
(( com.android.server.location.gnss.hal.Gpo4Client$LocationRequestRecorder ) v3 ).setNlpReturned ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;->setNlpReturned(Z)V
/* .line 215 */
v3 = this.mLastNlpLocation;
/* filled-new-array {v3}, [Landroid/location/Location; */
java.util.Arrays .asList ( v3 );
/* .line 216 */
/* .local v3, "locations":Ljava/util/List;, "Ljava/util/List<Landroid/location/Location;>;" */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v6, "use cached nlp " */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.mLastNlpLocation;
(( android.location.Location ) v6 ).toString ( ); // invoke-virtual {v6}, Landroid/location/Location;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = ", listenerHashCode is "; // const-string v6, ", listenerHashCode is "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 217 */
/* check-cast v6, Ljava/lang/Integer; */
(( java.lang.Integer ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/Integer;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = ", callbackType is ILocationListener "; // const-string v6, ", callbackType is ILocationListener "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 219 */
/* check-cast v6, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder; */
(( com.android.server.location.gnss.hal.Gpo4Client$LocationRequestRecorder ) v6 ).getCallbackType ( ); // invoke-virtual {v6}, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;->getCallbackType()Ljava/lang/Object;
/* instance-of v6, v6, Landroid/location/ILocationListener; */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 220 */
/* .local v5, "logInfo":Ljava/lang/String; */
v6 = this.mGpoUtil;
final String v7 = "Gpo4Client"; // const-string v7, "Gpo4Client"
(( com.android.server.location.gnss.hal.GpoUtil ) v6 ).logv ( v7, v5 ); // invoke-virtual {v6, v7, v5}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 221 */
v6 = this.mGpoUtil;
(( com.android.server.location.gnss.hal.GpoUtil ) v6 ).logEn ( v5 ); // invoke-virtual {v6, v5}, Lcom/android/server/location/gnss/hal/GpoUtil;->logEn(Ljava/lang/String;)V
/* .line 222 */
/* check-cast v6, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder; */
(( com.android.server.location.gnss.hal.Gpo4Client$LocationRequestRecorder ) v6 ).getCallbackType ( ); // invoke-virtual {v6}, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;->getCallbackType()Ljava/lang/Object;
/* invoke-direct {p0, v6, v3}, Lcom/android/server/location/gnss/hal/Gpo4Client;->delieverLocation(Ljava/lang/Object;Ljava/util/List;)V */
/* .line 223 */
/* check-cast v6, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder; */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v7 */
(( com.android.server.location.gnss.hal.Gpo4Client$LocationRequestRecorder ) v6 ).setNlpReturnTime ( v7, v8 ); // invoke-virtual {v6, v7, v8}, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;->setNlpReturnTime(J)V
/* .line 224 */
/* const/16 v6, 0xfa */
/* invoke-direct {p0, v6}, Lcom/android/server/location/gnss/hal/Gpo4Client;->checkAppUsage(I)Ljava/util/concurrent/ScheduledFuture; */
this.futureCheckNavigation2 = v6;
/* .line 225 */
v6 = this.futureCheckRequest;
if ( v6 != null) { // if-eqz v6, :cond_2
/* .line 226 */
} // .end local v2 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;>;"
} // .end local v3 # "locations":Ljava/util/List;, "Ljava/util/List<Landroid/location/Location;>;"
} // .end local v5 # "logInfo":Ljava/lang/String;
} // :cond_2
/* goto/16 :goto_0 */
/* .line 227 */
} // :cond_3
} // :goto_1
/* .line 229 */
} // :cond_4
/* new-instance v0, Lcom/android/server/location/gnss/hal/Gpo4Client$$ExternalSyntheticLambda5; */
/* invoke-direct {v0, p0}, Lcom/android/server/location/gnss/hal/Gpo4Client$$ExternalSyntheticLambda5;-><init>(Lcom/android/server/location/gnss/hal/Gpo4Client;)V */
android.os.Binder .withCleanCallingIdentity ( v0 );
/* check-cast v0, Ljava/lang/Boolean; */
v0 = (( java.lang.Boolean ) v0 ).booleanValue ( ); // invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
/* .line 210 */
} // :cond_5
} // :goto_2
} // .end method
private void registerPassiveLocationUpdates ( ) {
/* .locals 7 */
/* .line 198 */
v0 = this.mLocationManager;
final String v1 = "passive"; // const-string v1, "passive"
/* const-wide/16 v2, 0x3e8 */
int v4 = 0; // const/4 v4, 0x0
v5 = this.mPassiveLocationListener;
v6 = this.mContext;
/* .line 199 */
(( android.content.Context ) v6 ).getMainLooper ( ); // invoke-virtual {v6}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;
/* .line 198 */
/* invoke-virtual/range {v0 ..v6}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;Landroid/os/Looper;)V */
/* .line 200 */
return;
} // .end method
private void unregisterNetworkLocationUpdates ( ) {
/* .locals 1 */
/* .line 240 */
/* new-instance v0, Lcom/android/server/location/gnss/hal/Gpo4Client$$ExternalSyntheticLambda2; */
/* invoke-direct {v0, p0}, Lcom/android/server/location/gnss/hal/Gpo4Client$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/location/gnss/hal/Gpo4Client;)V */
android.os.Binder .withCleanCallingIdentity ( v0 );
/* .line 243 */
return;
} // .end method
private void unregisterPassiveLocationUpdates ( ) {
/* .locals 2 */
/* .line 203 */
v0 = this.mLocationManager;
v1 = this.mPassiveLocationListener;
(( android.location.LocationManager ) v0 ).removeUpdates ( v1 ); // invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V
/* .line 204 */
return;
} // .end method
/* # virtual methods */
public Boolean blockEngineStart ( ) {
/* .locals 5 */
/* .line 268 */
v0 = this.isFeatureEnabled;
v0 = (( java.util.concurrent.atomic.AtomicBoolean ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mGpoUtil;
/* .line 269 */
v0 = (( com.android.server.location.gnss.hal.GpoUtil ) v0 ).getEngineStatus ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->getEngineStatus()I
int v2 = 2; // const/4 v2, 0x2
/* if-eq v0, v2, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
/* move v0, v1 */
/* .line 270 */
/* .local v0, "res":Z */
} // :goto_0
v2 = this.mGpoUtil;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "blockEngineStart ? "; // const-string v4, "blockEngineStart ? "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "Gpo4Client"; // const-string v4, "Gpo4Client"
(( com.android.server.location.gnss.hal.GpoUtil ) v2 ).logi ( v4, v3, v1 ); // invoke-virtual {v2, v4, v3, v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V
/* .line 271 */
/* const/16 v1, 0x7d0 */
/* invoke-direct {p0, v1}, Lcom/android/server/location/gnss/hal/Gpo4Client;->checkAppUsage(I)Ljava/util/concurrent/ScheduledFuture; */
this.futureCheckNavigation1 = v1;
/* .line 272 */
} // .end method
public void clearLocationRequest ( ) {
/* .locals 3 */
/* .line 292 */
v0 = this.mGpoUtil;
final String v1 = "Gpo4Client"; // const-string v1, "Gpo4Client"
final String v2 = "clearLocationRequest"; // const-string v2, "clearLocationRequest"
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logv ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 293 */
v0 = this.futureCheckNavigation1;
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 294 */
} // :cond_0
v0 = this.futureCheckNavigation2;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 295 */
} // :cond_1
v0 = this.futureCheckRequest;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 296 */
} // :cond_2
return;
} // .end method
public void deinit ( ) {
/* .locals 3 */
/* .line 86 */
v0 = this.mGpoUtil;
final String v1 = "Gpo4Client"; // const-string v1, "Gpo4Client"
final String v2 = "deinit"; // const-string v2, "deinit"
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logv ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 87 */
/* invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo4Client;->unregisterPassiveLocationUpdates()V */
/* .line 88 */
v0 = this.scheduledThreadPoolExecutor;
(( java.util.concurrent.ScheduledThreadPoolExecutor ) v0 ).shutdown ( ); // invoke-virtual {v0}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->shutdown()V
/* .line 89 */
int v0 = 0; // const/4 v0, 0x0
this.scheduledThreadPoolExecutor = v0;
/* .line 90 */
return;
} // .end method
public void disableGnssSwitch ( ) {
/* .locals 2 */
/* .line 93 */
v0 = this.isFeatureEnabled;
int v1 = 0; // const/4 v1, 0x0
(( java.util.concurrent.atomic.AtomicBoolean ) v0 ).set ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
/* .line 94 */
return;
} // .end method
public void init ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 74 */
v0 = this.mGpoUtil;
final String v1 = "Gpo4Client"; // const-string v1, "Gpo4Client"
final String v2 = "init"; // const-string v2, "init"
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logv ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V
/* .line 75 */
this.mContext = p1;
/* .line 76 */
/* nop */
/* .line 77 */
final String v0 = "location"; // const-string v0, "location"
(( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/location/LocationManager; */
/* .line 76 */
com.android.internal.util.Preconditions .checkNotNull ( v0 );
/* check-cast v0, Landroid/location/LocationManager; */
this.mLocationManager = v0;
/* .line 78 */
/* invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo4Client;->registerPassiveLocationUpdates()V */
/* .line 79 */
/* new-instance v0, Ljava/util/concurrent/ScheduledThreadPoolExecutor; */
int v1 = 1; // const/4 v1, 0x1
/* invoke-direct {v0, v1}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;-><init>(I)V */
this.scheduledThreadPoolExecutor = v0;
/* .line 80 */
(( java.util.concurrent.ScheduledThreadPoolExecutor ) v0 ).setRemoveOnCancelPolicy ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->setRemoveOnCancelPolicy(Z)V
/* .line 81 */
v0 = this.scheduledThreadPoolExecutor;
int v1 = 0; // const/4 v1, 0x0
(( java.util.concurrent.ScheduledThreadPoolExecutor ) v0 ).setExecuteExistingDelayedTasksAfterShutdownPolicy ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->setExecuteExistingDelayedTasksAfterShutdownPolicy(Z)V
/* .line 82 */
v0 = this.scheduledThreadPoolExecutor;
(( java.util.concurrent.ScheduledThreadPoolExecutor ) v0 ).setContinueExistingPeriodicTasksAfterShutdownPolicy ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->setContinueExistingPeriodicTasksAfterShutdownPolicy(Z)V
/* .line 83 */
return;
} // .end method
public void removeLocationRequestId ( Integer p0 ) {
/* .locals 8 */
/* .param p1, "listenerHashCode" # I */
/* .line 246 */
v0 = this.mNlpRequestMap;
v0 = java.lang.Integer .valueOf ( p1 );
int v1 = 0; // const/4 v1, 0x0
final String v2 = "Gpo4Client"; // const-string v2, "Gpo4Client"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 247 */
v0 = this.mGpoUtil;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "removeNLP: "; // const-string v4, "removeNLP: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logi ( v2, v3, v1 ); // invoke-virtual {v0, v2, v3, v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V
/* .line 248 */
v0 = this.mNlpRequestMap;
java.lang.Integer .valueOf ( p1 );
/* .line 250 */
} // :cond_0
v0 = this.mGlpRequestMap;
v0 = java.lang.Integer .valueOf ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 251 */
v0 = this.mGpoUtil;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "removeGLP: "; // const-string v4, "removeGLP: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logi ( v2, v3, v1 ); // invoke-virtual {v0, v2, v3, v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V
/* .line 252 */
v0 = this.mGlpRequestMap;
java.lang.Integer .valueOf ( p1 );
/* check-cast v0, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder; */
/* .line 253 */
/* .local v0, "recorder":Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder; */
v1 = this.mGlpRequestMap;
java.lang.Integer .valueOf ( p1 );
/* .line 254 */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 255 */
v1 = (( com.android.server.location.gnss.hal.Gpo4Client$LocationRequestRecorder ) v0 ).getUid ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;->getUid()I
/* .line 256 */
/* .local v1, "uid":I */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v2 */
(( com.android.server.location.gnss.hal.Gpo4Client$LocationRequestRecorder ) v0 ).getNlpReturnTime ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;->getNlpReturnTime()J
/* move-result-wide v4 */
/* sub-long/2addr v2, v4 */
/* .line 257 */
/* .local v2, "time":J */
com.android.server.location.gnss.GnssEventTrackingStub .getInstance ( );
v5 = this.mNavAppInfo;
java.lang.Integer .valueOf ( v1 );
/* check-cast v5, Ljava/lang/String; */
/* .line 258 */
/* const-wide/16 v6, 0xbb8 */
/* cmp-long v6, v2, v6 */
/* if-ltz v6, :cond_1 */
/* move-wide v6, v2 */
} // :cond_1
/* const-wide/16 v6, 0x0 */
/* .line 257 */
} // :goto_0
/* .line 259 */
v4 = this.mNavAppInfo;
java.lang.Integer .valueOf ( v1 );
/* .line 260 */
} // .end local v1 # "uid":I
} // .end local v2 # "time":J
/* .line 261 */
} // :cond_2
v1 = this.mNavAppInfo;
/* .line 265 */
} // .end local v0 # "recorder":Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;
} // :cond_3
} // :goto_1
return;
} // .end method
public void reportLocation2Gpo ( android.location.Location p0 ) {
/* .locals 3 */
/* .param p1, "location" # Landroid/location/Location; */
/* .line 299 */
if ( p1 != null) { // if-eqz p1, :cond_0
v0 = (( android.location.Location ) p1 ).isComplete ( ); // invoke-virtual {p1}, Landroid/location/Location;->isComplete()Z
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = (( android.location.Location ) p1 ).getSpeed ( ); // invoke-virtual {p1}, Landroid/location/Location;->getSpeed()F
/* const/high16 v1, 0x40400000 # 3.0f */
/* cmpl-float v0, v0, v1 */
/* if-ltz v0, :cond_0 */
/* .line 300 */
v0 = this.mLastTrafficTime;
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v1 */
(( java.util.concurrent.atomic.AtomicLong ) v0 ).set ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V
/* .line 302 */
} // :cond_0
return;
} // .end method
public void saveLocationRequestId ( Integer p0, java.lang.String p1, java.lang.String p2, Integer p3, java.lang.Object p4 ) {
/* .locals 18 */
/* .param p1, "uid" # I */
/* .param p2, "pkn" # Ljava/lang/String; */
/* .param p3, "provider" # Ljava/lang/String; */
/* .param p4, "listenerHashCode" # I */
/* .param p5, "callbackType" # Ljava/lang/Object; */
/* .line 111 */
/* move-object/from16 v9, p0 */
/* move-object/from16 v10, p3 */
/* move/from16 v11, p4 */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/location/gnss/hal/Gpo4Client;->checkFeatureSwitch()V */
/* .line 112 */
v0 = this.isFeatureEnabled;
v0 = (( java.util.concurrent.atomic.AtomicBoolean ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
/* if-nez v0, :cond_0 */
return;
/* .line 113 */
} // :cond_0
final String v0 = "network"; // const-string v0, "network"
v0 = (( java.lang.String ) v0 ).equalsIgnoreCase ( v10 ); // invoke-virtual {v0, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
int v12 = 0; // const/4 v12, 0x0
final String v13 = "Gpo4Client"; // const-string v13, "Gpo4Client"
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.mNlpRequestMap;
/* .line 114 */
v0 = /* invoke-static/range {p4 ..p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer; */
/* if-nez v0, :cond_1 */
/* .line 115 */
v0 = this.mGpoUtil;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "saveNLP: "; // const-string v2, "saveNLP: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v11 ); // invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logi ( v13, v1, v12 ); // invoke-virtual {v0, v13, v1, v12}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V
/* .line 116 */
v14 = this.mNlpRequestMap;
/* invoke-static/range {p4 ..p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer; */
/* new-instance v7, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder; */
int v6 = 0; // const/4 v6, 0x0
/* const-wide/16 v16, 0x0 */
/* move-object v0, v7 */
/* move-object/from16 v1, p0 */
/* move/from16 v2, p1 */
/* move-object/from16 v3, p3 */
/* move/from16 v4, p4 */
/* move-object/from16 v5, p5 */
/* move-object v12, v7 */
/* move-wide/from16 v7, v16 */
/* invoke-direct/range {v0 ..v8}, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;-><init>(Lcom/android/server/location/gnss/hal/Gpo4Client;ILjava/lang/String;ILjava/lang/Object;ZJ)V */
/* .line 119 */
} // :cond_1
final String v0 = "gps"; // const-string v0, "gps"
v0 = (( java.lang.String ) v0 ).equalsIgnoreCase ( v10 ); // invoke-virtual {v0, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_3
v0 = this.mGlpRequestMap;
/* .line 120 */
v0 = /* invoke-static/range {p4 ..p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer; */
/* if-nez v0, :cond_2 */
/* .line 121 */
v0 = this.mGpoUtil;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "saveGLP: "; // const-string v2, "saveGLP: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v11 ); // invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v2 = 0; // const/4 v2, 0x0
(( com.android.server.location.gnss.hal.GpoUtil ) v0 ).logi ( v13, v1, v2 ); // invoke-virtual {v0, v13, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V
/* .line 122 */
v0 = this.mNavAppInfo;
/* invoke-static/range {p1 ..p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer; */
/* move-object/from16 v12, p2 */
/* .line 123 */
v13 = this.mGlpRequestMap;
/* invoke-static/range {p4 ..p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer; */
/* new-instance v15, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder; */
int v6 = 0; // const/4 v6, 0x0
/* const-wide/16 v7, 0x0 */
/* move-object v0, v15 */
/* move-object/from16 v1, p0 */
/* move/from16 v2, p1 */
/* move-object/from16 v3, p3 */
/* move/from16 v4, p4 */
/* move-object/from16 v5, p5 */
/* invoke-direct/range {v0 ..v8}, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;-><init>(Lcom/android/server/location/gnss/hal/Gpo4Client;ILjava/lang/String;ILjava/lang/Object;ZJ)V */
/* .line 125 */
v0 = this.scheduledThreadPoolExecutor;
/* new-instance v1, Lcom/android/server/location/gnss/hal/Gpo4Client$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, v9, v11, v2}, Lcom/android/server/location/gnss/hal/Gpo4Client$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/location/gnss/hal/Gpo4Client;II)V */
/* const-wide/16 v3, 0xfa */
v5 = java.util.concurrent.TimeUnit.MILLISECONDS;
(( java.util.concurrent.ScheduledThreadPoolExecutor ) v0 ).schedule ( v1, v3, v4, v5 ); // invoke-virtual {v0, v1, v3, v4, v5}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;
/* .line 120 */
} // :cond_2
/* move/from16 v2, p1 */
/* move-object/from16 v12, p2 */
/* .line 119 */
} // :cond_3
/* move/from16 v2, p1 */
/* move-object/from16 v12, p2 */
/* .line 147 */
} // :goto_0
return;
} // .end method
public void updateScreenState ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "on" # Z */
/* .line 97 */
v0 = this.isScreenOn;
(( java.util.concurrent.atomic.AtomicBoolean ) v0 ).set ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
/* .line 98 */
return;
} // .end method
