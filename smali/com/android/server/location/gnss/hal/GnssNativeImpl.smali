.class public Lcom/android/server/location/gnss/hal/GnssNativeImpl;
.super Ljava/lang/Object;
.source "GnssNativeImpl.java"

# interfaces
.implements Lcom/android/server/location/gnss/hal/GnssNativeStub;


# instance fields
.field private mNmeaStatus:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/location/gnss/hal/GnssNativeImpl;->mNmeaStatus:Z

    return-void
.end method


# virtual methods
.method public nativeStart()Z
    .locals 5

    .line 36
    invoke-static {}, Lcom/android/server/location/LocationDumpLogStub;->getInstance()Lcom/android/server/location/LocationDumpLogStub;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/android/server/location/LocationDumpLogStub;->setRecordLoseLocation(Z)V

    .line 38
    invoke-static {}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeStub;->getInstance()Lcom/android/server/location/gnss/hal/GnssPowerOptimizeStub;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeStub;->getEngineStatus()I

    move-result v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    .line 39
    return v1

    .line 41
    :cond_0
    invoke-static {}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeStub;->getInstance()Lcom/android/server/location/gnss/hal/GnssPowerOptimizeStub;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeStub;->setEngineStatus(I)V

    .line 42
    invoke-static {}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->getInstance()Lcom/android/server/location/gnss/GnssEventTrackingStub;

    move-result-object v0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    invoke-interface {v0, v2, v3, v4}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->recordEngineUsage(IJ)V

    .line 43
    const/4 v0, 0x0

    return v0
.end method

.method public nativeStop()Z
    .locals 5

    .line 49
    invoke-static {}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeStub;->getInstance()Lcom/android/server/location/gnss/hal/GnssPowerOptimizeStub;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeStub;->engineStoppedByGpo()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 52
    invoke-static {}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeStub;->getInstance()Lcom/android/server/location/gnss/hal/GnssPowerOptimizeStub;

    move-result-object v0

    const/4 v2, 0x3

    invoke-interface {v0, v2}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeStub;->setEngineStatus(I)V

    .line 53
    invoke-static {}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->getInstance()Lcom/android/server/location/gnss/GnssEventTrackingStub;

    move-result-object v0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    invoke-interface {v0, v2, v3, v4}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->recordEngineUsage(IJ)V

    .line 54
    return v1

    .line 57
    :cond_0
    invoke-static {}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeStub;->getInstance()Lcom/android/server/location/gnss/hal/GnssPowerOptimizeStub;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeStub;->clearLocationRequest()V

    .line 59
    invoke-static {}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeStub;->getInstance()Lcom/android/server/location/gnss/hal/GnssPowerOptimizeStub;

    move-result-object v0

    const/4 v2, 0x4

    invoke-interface {v0, v2}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeStub;->setEngineStatus(I)V

    .line 60
    invoke-static {}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->getInstance()Lcom/android/server/location/gnss/GnssEventTrackingStub;

    move-result-object v0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    invoke-interface {v0, v2, v3, v4}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->recordEngineUsage(IJ)V

    .line 61
    return v1
.end method

.method public reportNmea(Ljava/lang/String;)Z
    .locals 2
    .param p1, "nmea"    # Ljava/lang/String;

    .line 94
    invoke-static {}, Lcom/android/server/location/gnss/GnssLocationProviderStub;->getInstance()Lcom/android/server/location/gnss/GnssLocationProviderStub;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/android/server/location/gnss/GnssLocationProviderStub;->precisionProcessByType(Ljava/lang/String;)Ljava/lang/String;

    .line 96
    invoke-static {}, Lcom/android/server/location/LocationDumpLogStub;->getInstance()Lcom/android/server/location/LocationDumpLogStub;

    move-result-object v0

    const/4 v1, 0x4

    invoke-interface {v0, v1, p1}, Lcom/android/server/location/LocationDumpLogStub;->addToBugreport(ILjava/lang/String;)V

    .line 98
    invoke-static {}, Lcom/android/server/location/gnss/GnssCollectDataStub;->getInstance()Lcom/android/server/location/gnss/GnssCollectDataStub;

    move-result-object v0

    const/4 v1, 0x6

    invoke-interface {v0, v1, p1}, Lcom/android/server/location/gnss/GnssCollectDataStub;->savePoint(ILjava/lang/String;)V

    .line 100
    const-string v0, "$P"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/GnssNativeImpl;->mNmeaStatus:Z

    if-nez v0, :cond_0

    goto :goto_0

    .line 103
    :cond_0
    const/4 v0, 0x1

    return v0

    .line 101
    :cond_1
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method public reportStatus(I)V
    .locals 5
    .param p1, "gnssStatus"    # I

    .line 66
    const/4 v0, 0x0

    const-string v1, "GnssManager"

    const/4 v2, 0x2

    const/4 v3, 0x3

    if-ne p1, v3, :cond_1

    .line 67
    const-string v3, "gnss engine report: on"

    .line 68
    .local v3, "dumpInfo":Ljava/lang/String;
    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    invoke-static {}, Lcom/android/server/location/gnss/GnssCollectDataStub;->getInstance()Lcom/android/server/location/gnss/GnssCollectDataStub;

    move-result-object v1

    const/4 v4, 0x1

    invoke-interface {v1, v4, v0}, Lcom/android/server/location/gnss/GnssCollectDataStub;->savePoint(ILjava/lang/String;)V

    .line 71
    invoke-static {}, Lcom/android/server/location/LocationDumpLogStub;->getInstance()Lcom/android/server/location/LocationDumpLogStub;

    move-result-object v0

    invoke-interface {v0, v2, v3}, Lcom/android/server/location/LocationDumpLogStub;->addToBugreport(ILjava/lang/String;)V

    .line 73
    invoke-static {}, Lcom/android/server/location/gnss/GnssLocationProviderStub;->getInstance()Lcom/android/server/location/gnss/GnssLocationProviderStub;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/server/location/gnss/GnssLocationProviderStub;->getSendingSwitch()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    invoke-static {}, Lcom/android/server/location/gnss/GnssLocationProviderStub;->getInstance()Lcom/android/server/location/gnss/GnssLocationProviderStub;

    move-result-object v0

    invoke-interface {v0, v4}, Lcom/android/server/location/gnss/GnssLocationProviderStub;->notifyState(I)V

    .line 76
    :cond_0
    invoke-static {}, Lcom/android/server/location/gnss/hal/GnssScoringModelStub;->getInstance()Lcom/android/server/location/gnss/hal/GnssScoringModelStub;

    move-result-object v0

    invoke-interface {v0, v4}, Lcom/android/server/location/gnss/hal/GnssScoringModelStub;->startScoringModel(Z)V

    .end local v3    # "dumpInfo":Ljava/lang/String;
    goto :goto_0

    .line 77
    :cond_1
    const/4 v4, 0x4

    if-ne p1, v4, :cond_3

    .line 78
    const-string v4, "gnss engine report: off"

    .line 79
    .local v4, "dumpInfo":Ljava/lang/String;
    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    invoke-static {}, Lcom/android/server/location/gnss/GnssCollectDataStub;->getInstance()Lcom/android/server/location/gnss/GnssCollectDataStub;

    move-result-object v1

    invoke-interface {v1, v3, v0}, Lcom/android/server/location/gnss/GnssCollectDataStub;->savePoint(ILjava/lang/String;)V

    .line 82
    invoke-static {}, Lcom/android/server/location/LocationDumpLogStub;->getInstance()Lcom/android/server/location/LocationDumpLogStub;

    move-result-object v0

    invoke-interface {v0, v2, v4}, Lcom/android/server/location/LocationDumpLogStub;->addToBugreport(ILjava/lang/String;)V

    .line 84
    invoke-static {}, Lcom/android/server/location/gnss/GnssLocationProviderStub;->getInstance()Lcom/android/server/location/gnss/GnssLocationProviderStub;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/server/location/gnss/GnssLocationProviderStub;->getSendingSwitch()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 85
    invoke-static {}, Lcom/android/server/location/gnss/GnssLocationProviderStub;->getInstance()Lcom/android/server/location/gnss/GnssLocationProviderStub;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/android/server/location/gnss/GnssLocationProviderStub;->notifyState(I)V

    .line 87
    :cond_2
    invoke-static {}, Lcom/android/server/location/gnss/hal/GnssScoringModelStub;->getInstance()Lcom/android/server/location/gnss/hal/GnssScoringModelStub;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/android/server/location/gnss/hal/GnssScoringModelStub;->startScoringModel(Z)V

    goto :goto_1

    .line 77
    .end local v4    # "dumpInfo":Ljava/lang/String;
    :cond_3
    :goto_0
    nop

    .line 89
    :goto_1
    return-void
.end method

.method public setNmeaStatus(Z)V
    .locals 0
    .param p1, "nmeaStatus"    # Z

    .line 108
    iput-boolean p1, p0, Lcom/android/server/location/gnss/hal/GnssNativeImpl;->mNmeaStatus:Z

    .line 109
    return-void
.end method

.method public start()Z
    .locals 4

    .line 24
    invoke-static {}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeStub;->getInstance()Lcom/android/server/location/gnss/hal/GnssPowerOptimizeStub;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeStub;->blockEngineStart()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 26
    invoke-static {}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeStub;->getInstance()Lcom/android/server/location/gnss/hal/GnssPowerOptimizeStub;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeStub;->setEngineStatus(I)V

    .line 27
    invoke-static {}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->getInstance()Lcom/android/server/location/gnss/GnssEventTrackingStub;

    move-result-object v0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->recordEngineUsage(IJ)V

    .line 28
    return v1

    .line 30
    :cond_0
    const/4 v0, 0x0

    return v0
.end method
