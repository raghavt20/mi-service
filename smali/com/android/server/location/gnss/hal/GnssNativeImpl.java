public class com.android.server.location.gnss.hal.GnssNativeImpl implements com.android.server.location.gnss.hal.GnssNativeStub {
	 /* .source "GnssNativeImpl.java" */
	 /* # interfaces */
	 /* # instance fields */
	 private Boolean mNmeaStatus;
	 /* # direct methods */
	 public com.android.server.location.gnss.hal.GnssNativeImpl ( ) {
		 /* .locals 1 */
		 /* .line 16 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 18 */
		 int v0 = 1; // const/4 v0, 0x1
		 /* iput-boolean v0, p0, Lcom/android/server/location/gnss/hal/GnssNativeImpl;->mNmeaStatus:Z */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Boolean nativeStart ( ) {
		 /* .locals 5 */
		 /* .line 36 */
		 com.android.server.location.LocationDumpLogStub .getInstance ( );
		 int v1 = 1; // const/4 v1, 0x1
		 /* .line 38 */
		 v0 = 		 com.android.server.location.gnss.hal.GnssPowerOptimizeStub .getInstance ( );
		 int v2 = 2; // const/4 v2, 0x2
		 /* if-ne v0, v2, :cond_0 */
		 /* .line 39 */
		 /* .line 41 */
	 } // :cond_0
	 com.android.server.location.gnss.hal.GnssPowerOptimizeStub .getInstance ( );
	 /* .line 42 */
	 com.android.server.location.gnss.GnssEventTrackingStub .getInstance ( );
	 android.os.SystemClock .elapsedRealtime ( );
	 /* move-result-wide v3 */
	 /* .line 43 */
	 int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean nativeStop ( ) {
	 /* .locals 5 */
	 /* .line 49 */
	 v0 = 	 com.android.server.location.gnss.hal.GnssPowerOptimizeStub .getInstance ( );
	 int v1 = 0; // const/4 v1, 0x0
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 52 */
		 com.android.server.location.gnss.hal.GnssPowerOptimizeStub .getInstance ( );
		 int v2 = 3; // const/4 v2, 0x3
		 /* .line 53 */
		 com.android.server.location.gnss.GnssEventTrackingStub .getInstance ( );
		 android.os.SystemClock .elapsedRealtime ( );
		 /* move-result-wide v3 */
		 /* .line 54 */
		 /* .line 57 */
	 } // :cond_0
	 com.android.server.location.gnss.hal.GnssPowerOptimizeStub .getInstance ( );
	 /* .line 59 */
	 com.android.server.location.gnss.hal.GnssPowerOptimizeStub .getInstance ( );
	 int v2 = 4; // const/4 v2, 0x4
	 /* .line 60 */
	 com.android.server.location.gnss.GnssEventTrackingStub .getInstance ( );
	 android.os.SystemClock .elapsedRealtime ( );
	 /* move-result-wide v3 */
	 /* .line 61 */
} // .end method
public Boolean reportNmea ( java.lang.String p0 ) {
	 /* .locals 2 */
	 /* .param p1, "nmea" # Ljava/lang/String; */
	 /* .line 94 */
	 com.android.server.location.gnss.GnssLocationProviderStub .getInstance ( );
	 /* .line 96 */
	 com.android.server.location.LocationDumpLogStub .getInstance ( );
	 int v1 = 4; // const/4 v1, 0x4
	 /* .line 98 */
	 com.android.server.location.gnss.GnssCollectDataStub .getInstance ( );
	 int v1 = 6; // const/4 v1, 0x6
	 /* .line 100 */
	 final String v0 = "$P"; // const-string v0, "$P"
	 v0 = 	 (( java.lang.String ) p1 ).startsWith ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
	 /* if-nez v0, :cond_1 */
	 /* iget-boolean v0, p0, Lcom/android/server/location/gnss/hal/GnssNativeImpl;->mNmeaStatus:Z */
	 /* if-nez v0, :cond_0 */
	 /* .line 103 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* .line 101 */
} // :cond_1
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void reportStatus ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "gnssStatus" # I */
/* .line 66 */
int v0 = 0; // const/4 v0, 0x0
final String v1 = "GnssManager"; // const-string v1, "GnssManager"
int v2 = 2; // const/4 v2, 0x2
int v3 = 3; // const/4 v3, 0x3
/* if-ne p1, v3, :cond_1 */
/* .line 67 */
final String v3 = "gnss engine report: on"; // const-string v3, "gnss engine report: on"
/* .line 68 */
/* .local v3, "dumpInfo":Ljava/lang/String; */
android.util.Log .d ( v1,v3 );
/* .line 70 */
com.android.server.location.gnss.GnssCollectDataStub .getInstance ( );
int v4 = 1; // const/4 v4, 0x1
/* .line 71 */
com.android.server.location.LocationDumpLogStub .getInstance ( );
/* .line 73 */
v0 = com.android.server.location.gnss.GnssLocationProviderStub .getInstance ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 74 */
com.android.server.location.gnss.GnssLocationProviderStub .getInstance ( );
/* .line 76 */
} // :cond_0
com.android.server.location.gnss.hal.GnssScoringModelStub .getInstance ( );
} // .end local v3 # "dumpInfo":Ljava/lang/String;
/* .line 77 */
} // :cond_1
int v4 = 4; // const/4 v4, 0x4
/* if-ne p1, v4, :cond_3 */
/* .line 78 */
final String v4 = "gnss engine report: off"; // const-string v4, "gnss engine report: off"
/* .line 79 */
/* .local v4, "dumpInfo":Ljava/lang/String; */
android.util.Log .d ( v1,v4 );
/* .line 81 */
com.android.server.location.gnss.GnssCollectDataStub .getInstance ( );
/* .line 82 */
com.android.server.location.LocationDumpLogStub .getInstance ( );
/* .line 84 */
v0 = com.android.server.location.gnss.GnssLocationProviderStub .getInstance ( );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 85 */
com.android.server.location.gnss.GnssLocationProviderStub .getInstance ( );
/* .line 87 */
} // :cond_2
com.android.server.location.gnss.hal.GnssScoringModelStub .getInstance ( );
int v1 = 0; // const/4 v1, 0x0
/* .line 77 */
} // .end local v4 # "dumpInfo":Ljava/lang/String;
} // :cond_3
} // :goto_0
/* nop */
/* .line 89 */
} // :goto_1
return;
} // .end method
public void setNmeaStatus ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "nmeaStatus" # Z */
/* .line 108 */
/* iput-boolean p1, p0, Lcom/android/server/location/gnss/hal/GnssNativeImpl;->mNmeaStatus:Z */
/* .line 109 */
return;
} // .end method
public Boolean start ( ) {
/* .locals 4 */
/* .line 24 */
v0 = com.android.server.location.gnss.hal.GnssPowerOptimizeStub .getInstance ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 26 */
com.android.server.location.gnss.hal.GnssPowerOptimizeStub .getInstance ( );
int v1 = 1; // const/4 v1, 0x1
/* .line 27 */
com.android.server.location.gnss.GnssEventTrackingStub .getInstance ( );
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v2 */
/* .line 28 */
/* .line 30 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
