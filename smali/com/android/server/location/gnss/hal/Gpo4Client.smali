.class public Lcom/android/server/location/gnss/hal/Gpo4Client;
.super Ljava/lang/Object;
.source "Gpo4Client.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Gpo4Client"

.field private static final TIME_CHECK_NAV_APP:J = 0xbb8L

.field private static final TIME_MAX_TRAFFIC_IGNORE:J = 0x493e0L

.field private static final TIME_POST_DELAY_CHECK_NLP:I = 0xfa

.field private static final TIME_POST_TIMEOUT_OBTAIN_NLP:I = 0x7d0

.field private static volatile instance:Lcom/android/server/location/gnss/hal/Gpo4Client;


# instance fields
.field private futureCheckNavigation1:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture<",
            "*>;"
        }
    .end annotation
.end field

.field private futureCheckNavigation2:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture<",
            "*>;"
        }
    .end annotation
.end field

.field private futureCheckRequest:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture<",
            "*>;"
        }
    .end annotation
.end field

.field private final isFeatureEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final isScreenOn:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mContext:Landroid/content/Context;

.field private final mGlpRequestMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;",
            ">;"
        }
    .end annotation
.end field

.field private final mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

.field private mLastNlpLocation:Landroid/location/Location;

.field private final mLastNlpTime:Ljava/util/concurrent/atomic/AtomicLong;

.field private final mLastTrafficTime:Ljava/util/concurrent/atomic/AtomicLong;

.field private mLocationManager:Landroid/location/LocationManager;

.field private final mNavAppInfo:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mNetworkLocationListener:Landroid/location/LocationListener;

.field private final mNlpRequestMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;",
            ">;"
        }
    .end annotation
.end field

.field private final mObtainNlpTime:Ljava/util/concurrent/atomic/AtomicLong;

.field private final mPassiveLocationListener:Landroid/location/LocationListener;

.field private final mRequestNlpTime:Ljava/util/concurrent/atomic/AtomicLong;

.field private scheduledThreadPoolExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;


# direct methods
.method public static synthetic $r8$lambda$-XGP8cAHfNg3ENKjODZwP8C80Kk(Lcom/android/server/location/gnss/hal/Gpo4Client;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo4Client;->lambda$saveLocationRequestId$0()V

    return-void
.end method

.method public static synthetic $r8$lambda$4pV_jCN4PfmJzUBiaAu74n5iawk(Lcom/android/server/location/gnss/hal/Gpo4Client;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo4Client;->lambda$checkAppUsage$6()V

    return-void
.end method

.method public static synthetic $r8$lambda$75TPzP_NztHwaGVZbO20HtTxwTY(Lcom/android/server/location/gnss/hal/Gpo4Client;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo4Client;->lambda$unregisterNetworkLocationUpdates$5()V

    return-void
.end method

.method public static synthetic $r8$lambda$alKhd4AfgH5cOhYbGCx3v0DKc5s(Lcom/android/server/location/gnss/hal/Gpo4Client;)Ljava/lang/Boolean;
    .locals 0

    invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo4Client;->lambda$registerNetworkLocationUpdates$4()Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$fbmXz5iO6O8eJRg7KyNRPVcWz_A(Lcom/android/server/location/gnss/hal/Gpo4Client;Landroid/location/Location;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/location/gnss/hal/Gpo4Client;->lambda$new$2(Landroid/location/Location;)V

    return-void
.end method

.method public static synthetic $r8$lambda$j9DcByIdrTCXHSd2ObGiyOUcQm0(Lcom/android/server/location/gnss/hal/Gpo4Client;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/location/gnss/hal/Gpo4Client;->lambda$saveLocationRequestId$1(II)V

    return-void
.end method

.method public static synthetic $r8$lambda$y0E58QqoFXDKH97D3YUmF32a-aM(Lcom/android/server/location/gnss/hal/Gpo4Client;Landroid/location/Location;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/location/gnss/hal/Gpo4Client;->lambda$new$3(Landroid/location/Location;)V

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-static {}, Lcom/android/server/location/gnss/hal/GpoUtil;->getInstance()Lcom/android/server/location/gnss/hal/GpoUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mLastNlpLocation:Landroid/location/Location;

    .line 43
    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->futureCheckRequest:Ljava/util/concurrent/ScheduledFuture;

    .line 44
    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->futureCheckNavigation1:Ljava/util/concurrent/ScheduledFuture;

    .line 45
    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->futureCheckNavigation2:Ljava/util/concurrent/ScheduledFuture;

    .line 47
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mGlpRequestMap:Ljava/util/Map;

    .line 48
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mNlpRequestMap:Ljava/util/Map;

    .line 49
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mNavAppInfo:Ljava/util/Map;

    .line 51
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->isFeatureEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 52
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->isScreenOn:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 53
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v1, 0x0

    invoke-direct {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mRequestNlpTime:Ljava/util/concurrent/atomic/AtomicLong;

    .line 54
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mObtainNlpTime:Ljava/util/concurrent/atomic/AtomicLong;

    .line 55
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mLastTrafficTime:Ljava/util/concurrent/atomic/AtomicLong;

    .line 56
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mLastNlpTime:Ljava/util/concurrent/atomic/AtomicLong;

    .line 149
    new-instance v0, Lcom/android/server/location/gnss/hal/Gpo4Client$$ExternalSyntheticLambda3;

    invoke-direct {v0, p0}, Lcom/android/server/location/gnss/hal/Gpo4Client$$ExternalSyntheticLambda3;-><init>(Lcom/android/server/location/gnss/hal/Gpo4Client;)V

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mPassiveLocationListener:Landroid/location/LocationListener;

    .line 206
    new-instance v0, Lcom/android/server/location/gnss/hal/Gpo4Client$$ExternalSyntheticLambda4;

    invoke-direct {v0, p0}, Lcom/android/server/location/gnss/hal/Gpo4Client$$ExternalSyntheticLambda4;-><init>(Lcom/android/server/location/gnss/hal/Gpo4Client;)V

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mNetworkLocationListener:Landroid/location/LocationListener;

    .line 71
    return-void
.end method

.method private checkAppUsage(I)Ljava/util/concurrent/ScheduledFuture;
    .locals 5
    .param p1, "timeout"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/concurrent/ScheduledFuture<",
            "*>;"
        }
    .end annotation

    .line 277
    :try_start_0
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    const-string v1, "Gpo4Client"

    const-string v2, "checkAppUsage"

    invoke-virtual {v0, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->scheduledThreadPoolExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    new-instance v1, Lcom/android/server/location/gnss/hal/Gpo4Client$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0}, Lcom/android/server/location/gnss/hal/Gpo4Client$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/location/gnss/hal/Gpo4Client;)V

    int-to-long v2, p1

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 285
    :catch_0
    move-exception v0

    .line 286
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 288
    .end local v0    # "e":Ljava/lang/Exception;
    const/4 v0, 0x0

    return-object v0
.end method

.method private checkFeatureSwitch()V
    .locals 7

    .line 101
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mGlpRequestMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 102
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->isFeatureEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

    iget-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    invoke-virtual {v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->checkHeavyUser()Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    invoke-virtual {v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->isNetworkConnected()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->isScreenOn:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mLastTrafficTime:Ljava/util/concurrent/atomic/AtomicLong;

    .line 103
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v1, v3, v5

    if-eqz v1, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    iget-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mLastTrafficTime:Ljava/util/concurrent/atomic/AtomicLong;

    .line 104
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v5

    sub-long/2addr v3, v5

    const-wide/32 v5, 0x493e0

    cmp-long v1, v3, v5

    if-ltz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    move v1, v2

    .line 102
    :goto_0
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 105
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isGpoFeatureEnabled ? "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->isFeatureEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "Gpo4Client"

    invoke-virtual {v0, v3, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 107
    :cond_2
    return-void
.end method

.method private delieverLocation(Ljava/lang/Object;Ljava/util/List;)V
    .locals 5
    .param p1, "callbackType"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/util/List<",
            "Landroid/location/Location;",
            ">;)V"
        }
    .end annotation

    .line 176
    .local p2, "locations":Ljava/util/List;, "Ljava/util/List<Landroid/location/Location;>;"
    instance-of v0, p1, Landroid/location/ILocationListener;

    const-string v1, "Gpo4Client"

    if-eqz v0, :cond_0

    .line 178
    :try_start_0
    move-object v0, p1

    check-cast v0, Landroid/location/ILocationListener;

    const/4 v2, 0x0

    invoke-interface {v0, p2, v2}, Landroid/location/ILocationListener;->onLocationChanged(Ljava/util/List;Landroid/os/IRemoteCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 179
    :catch_0
    move-exception v0

    .line 180
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v2, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mGlpRequestMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    .line 181
    iget-object v2, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    const-string v3, "onLocationChanged RemoteException"

    invoke-virtual {v2, v1, v3}, Lcom/android/server/location/gnss/hal/GpoUtil;->loge(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    goto :goto_1

    .line 184
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 187
    .local v0, "intent":Landroid/content/Intent;
    const/4 v2, 0x0

    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Parcelable;

    const-string v4, "location"

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 189
    :try_start_1
    move-object v3, p1

    check-cast v3, Landroid/app/PendingIntent;

    iget-object v4, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v4, v2, v0}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V
    :try_end_1
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_1 .. :try_end_1} :catch_1

    .line 193
    goto :goto_1

    .line 190
    :catch_1
    move-exception v2

    .line 191
    .local v2, "e":Landroid/app/PendingIntent$CanceledException;
    iget-object v3, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mGlpRequestMap:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    .line 192
    iget-object v3, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    const-string v4, "PendingInent send CanceledException"

    invoke-virtual {v3, v1, v4}, Lcom/android/server/location/gnss/hal/GpoUtil;->loge(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v2    # "e":Landroid/app/PendingIntent$CanceledException;
    :goto_1
    return-void
.end method

.method public static getInstance()Lcom/android/server/location/gnss/hal/Gpo4Client;
    .locals 2

    .line 61
    sget-object v0, Lcom/android/server/location/gnss/hal/Gpo4Client;->instance:Lcom/android/server/location/gnss/hal/Gpo4Client;

    if-nez v0, :cond_1

    .line 62
    const-class v0, Lcom/android/server/location/gnss/hal/Gpo4Client;

    monitor-enter v0

    .line 63
    :try_start_0
    sget-object v1, Lcom/android/server/location/gnss/hal/Gpo4Client;->instance:Lcom/android/server/location/gnss/hal/Gpo4Client;

    if-nez v1, :cond_0

    .line 64
    new-instance v1, Lcom/android/server/location/gnss/hal/Gpo4Client;

    invoke-direct {v1}, Lcom/android/server/location/gnss/hal/Gpo4Client;-><init>()V

    sput-object v1, Lcom/android/server/location/gnss/hal/Gpo4Client;->instance:Lcom/android/server/location/gnss/hal/Gpo4Client;

    .line 66
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 68
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/location/gnss/hal/Gpo4Client;->instance:Lcom/android/server/location/gnss/hal/Gpo4Client;

    return-object v0
.end method

.method private synthetic lambda$checkAppUsage$6()V
    .locals 3

    .line 280
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->getEngineStatus()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 281
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    const-string v1, "Gpo4Client"

    const-string v2, "navigating app, restart gnss engine"

    invoke-virtual {v0, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->doStartEngineByInstance()Z

    .line 284
    :cond_0
    return-void
.end method

.method private synthetic lambda$new$2(Landroid/location/Location;)V
    .locals 9
    .param p1, "location"    # Landroid/location/Location;

    .line 150
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->isFeatureEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "network"

    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    .line 151
    invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->getEngineStatus()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    .line 152
    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    const/high16 v1, 0x43160000    # 150.0f

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_2

    .line 154
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    invoke-virtual {v0, p1}, Lcom/android/server/location/gnss/hal/GpoUtil;->convertNlp2Glp(Landroid/location/Location;)Landroid/location/Location;

    move-result-object v0

    .line 155
    .local v0, "gLocation":Landroid/location/Location;
    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mLastNlpLocation:Landroid/location/Location;

    .line 156
    iget-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mLastNlpTime:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 157
    iget-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mGlpRequestMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 158
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;

    invoke-virtual {v3}, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;->getNlpReturned()Z

    move-result v3

    if-eqz v3, :cond_0

    goto/16 :goto_1

    .line 159
    :cond_0
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;->setNlpReturned(Z)V

    .line 160
    filled-new-array {v0}, [Landroid/location/Location;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    .line 161
    .local v3, "locations":Ljava/util/List;, "Ljava/util/List<Landroid/location/Location;>;"
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "use nlp "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Landroid/location/Location;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", listenerHashCode is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 162
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", callbackType is ILocationListener "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 164
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;

    invoke-virtual {v6}, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;->getCallbackType()Ljava/lang/Object;

    move-result-object v6

    instance-of v6, v6, Landroid/location/ILocationListener;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 165
    .local v5, "logInfo":Ljava/lang/String;
    iget-object v6, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    const-string v7, "Gpo4Client"

    invoke-virtual {v6, v7, v5}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    iget-object v6, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    invoke-virtual {v6, v5}, Lcom/android/server/location/gnss/hal/GpoUtil;->logEn(Ljava/lang/String;)V

    .line 167
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;

    invoke-virtual {v6}, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;->getCallbackType()Ljava/lang/Object;

    move-result-object v6

    invoke-direct {p0, v6, v3}, Lcom/android/server/location/gnss/hal/Gpo4Client;->delieverLocation(Ljava/lang/Object;Ljava/util/List;)V

    .line 168
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;->setNlpReturnTime(J)V

    .line 169
    const/16 v6, 0xfa

    invoke-direct {p0, v6}, Lcom/android/server/location/gnss/hal/Gpo4Client;->checkAppUsage(I)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v6

    iput-object v6, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->futureCheckNavigation2:Ljava/util/concurrent/ScheduledFuture;

    .line 170
    iget-object v6, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->futureCheckRequest:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v6, :cond_1

    invoke-interface {v6, v4}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 171
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;>;"
    .end local v3    # "locations":Ljava/util/List;, "Ljava/util/List<Landroid/location/Location;>;"
    .end local v5    # "logInfo":Ljava/lang/String;
    :cond_1
    goto/16 :goto_0

    .line 173
    .end local v0    # "gLocation":Landroid/location/Location;
    :cond_2
    :goto_1
    return-void
.end method

.method private synthetic lambda$new$3(Landroid/location/Location;)V
    .locals 3
    .param p1, "location"    # Landroid/location/Location;

    .line 207
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mObtainNlpTime:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    return-void
.end method

.method private synthetic lambda$registerNetworkLocationUpdates$4()Ljava/lang/Boolean;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 230
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    const-string v1, "Gpo4Client"

    const-string v2, "request NLP."

    invoke-virtual {v0, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mRequestNlpTime:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 232
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mLocationManager:Landroid/location/LocationManager;

    const-string v1, "network"

    const-wide/16 v2, 0x3e8

    const/high16 v4, 0x447a0000    # 1000.0f

    const/4 v5, 0x1

    invoke-static {v1, v2, v3, v4, v5}, Landroid/location/LocationRequest;->createFromDeprecatedProvider(Ljava/lang/String;JFZ)Landroid/location/LocationRequest;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mNetworkLocationListener:Landroid/location/LocationListener;

    iget-object v3, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mContext:Landroid/content/Context;

    .line 234
    invoke-virtual {v3}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    .line 232
    invoke-virtual {v0, v1, v2, v3}, Landroid/location/LocationManager;->requestLocationUpdates(Landroid/location/LocationRequest;Landroid/location/LocationListener;Landroid/os/Looper;)V

    .line 235
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method private synthetic lambda$saveLocationRequestId$0()V
    .locals 4

    .line 139
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mObtainNlpTime:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    iget-object v2, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mRequestNlpTime:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 140
    invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo4Client;->unregisterNetworkLocationUpdates()V

    .line 142
    :cond_0
    return-void
.end method

.method private synthetic lambda$saveLocationRequestId$1(II)V
    .locals 6
    .param p1, "listenerHashCode"    # I
    .param p2, "uid"    # I

    .line 126
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mGlpRequestMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 127
    const/4 v0, 0x0

    .line 128
    .local v0, "existNlp":Z
    iget-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mNlpRequestMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;

    .line 129
    .local v2, "r":Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;
    invoke-virtual {v2, p2}, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;->existUid(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 130
    const/4 v0, 0x1

    .line 131
    goto :goto_1

    .line 133
    .end local v2    # "r":Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;
    :cond_0
    goto :goto_0

    .line 134
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "LocationRequest of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " existNlp ? "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const-string v4, "Gpo4Client"

    invoke-virtual {v1, v4, v2, v3}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 136
    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo4Client;->registerNetworkLocationUpdates()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 137
    iget-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->scheduledThreadPoolExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    new-instance v2, Lcom/android/server/location/gnss/hal/Gpo4Client$$ExternalSyntheticLambda6;

    invoke-direct {v2, p0}, Lcom/android/server/location/gnss/hal/Gpo4Client$$ExternalSyntheticLambda6;-><init>(Lcom/android/server/location/gnss/hal/Gpo4Client;)V

    const-wide/16 v3, 0x7d0

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v4, v5}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->futureCheckRequest:Ljava/util/concurrent/ScheduledFuture;

    .line 145
    .end local v0    # "existNlp":Z
    :cond_2
    return-void
.end method

.method private synthetic lambda$unregisterNetworkLocationUpdates$5()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 241
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mLocationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mNetworkLocationListener:Landroid/location/LocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    return-void
.end method

.method private registerNetworkLocationUpdates()Z
    .locals 9

    .line 210
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->isNetworkConnected()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mLocationManager:Landroid/location/LocationManager;

    const-string v2, "network"

    invoke-virtual {v0, v2}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    goto/16 :goto_2

    .line 211
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mLastNlpLocation:Landroid/location/Location;

    if-eqz v0, :cond_4

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mLastNlpTime:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x7d0

    cmp-long v0, v2, v4

    if-gtz v0, :cond_4

    .line 212
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mGlpRequestMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 213
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;

    invoke-virtual {v3}, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;->getNlpReturned()Z

    move-result v3

    if-eqz v3, :cond_1

    goto/16 :goto_1

    .line 214
    :cond_1
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;->setNlpReturned(Z)V

    .line 215
    iget-object v3, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mLastNlpLocation:Landroid/location/Location;

    filled-new-array {v3}, [Landroid/location/Location;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    .line 216
    .local v3, "locations":Ljava/util/List;, "Ljava/util/List<Landroid/location/Location;>;"
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "use cached nlp "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mLastNlpLocation:Landroid/location/Location;

    invoke-virtual {v6}, Landroid/location/Location;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", listenerHashCode is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 217
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", callbackType is ILocationListener "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 219
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;

    invoke-virtual {v6}, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;->getCallbackType()Ljava/lang/Object;

    move-result-object v6

    instance-of v6, v6, Landroid/location/ILocationListener;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 220
    .local v5, "logInfo":Ljava/lang/String;
    iget-object v6, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    const-string v7, "Gpo4Client"

    invoke-virtual {v6, v7, v5}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    iget-object v6, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    invoke-virtual {v6, v5}, Lcom/android/server/location/gnss/hal/GpoUtil;->logEn(Ljava/lang/String;)V

    .line 222
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;

    invoke-virtual {v6}, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;->getCallbackType()Ljava/lang/Object;

    move-result-object v6

    invoke-direct {p0, v6, v3}, Lcom/android/server/location/gnss/hal/Gpo4Client;->delieverLocation(Ljava/lang/Object;Ljava/util/List;)V

    .line 223
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;->setNlpReturnTime(J)V

    .line 224
    const/16 v6, 0xfa

    invoke-direct {p0, v6}, Lcom/android/server/location/gnss/hal/Gpo4Client;->checkAppUsage(I)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v6

    iput-object v6, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->futureCheckNavigation2:Ljava/util/concurrent/ScheduledFuture;

    .line 225
    iget-object v6, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->futureCheckRequest:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v6, :cond_2

    invoke-interface {v6, v4}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 226
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;>;"
    .end local v3    # "locations":Ljava/util/List;, "Ljava/util/List<Landroid/location/Location;>;"
    .end local v5    # "logInfo":Ljava/lang/String;
    :cond_2
    goto/16 :goto_0

    .line 227
    :cond_3
    :goto_1
    return v1

    .line 229
    :cond_4
    new-instance v0, Lcom/android/server/location/gnss/hal/Gpo4Client$$ExternalSyntheticLambda5;

    invoke-direct {v0, p0}, Lcom/android/server/location/gnss/hal/Gpo4Client$$ExternalSyntheticLambda5;-><init>(Lcom/android/server/location/gnss/hal/Gpo4Client;)V

    invoke-static {v0}, Landroid/os/Binder;->withCleanCallingIdentity(Lcom/android/internal/util/FunctionalUtils$ThrowingSupplier;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    .line 210
    :cond_5
    :goto_2
    return v1
.end method

.method private registerPassiveLocationUpdates()V
    .locals 7

    .line 198
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mLocationManager:Landroid/location/LocationManager;

    const-string v1, "passive"

    const-wide/16 v2, 0x3e8

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mPassiveLocationListener:Landroid/location/LocationListener;

    iget-object v6, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mContext:Landroid/content/Context;

    .line 199
    invoke-virtual {v6}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v6

    .line 198
    invoke-virtual/range {v0 .. v6}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;Landroid/os/Looper;)V

    .line 200
    return-void
.end method

.method private unregisterNetworkLocationUpdates()V
    .locals 1

    .line 240
    new-instance v0, Lcom/android/server/location/gnss/hal/Gpo4Client$$ExternalSyntheticLambda2;

    invoke-direct {v0, p0}, Lcom/android/server/location/gnss/hal/Gpo4Client$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/location/gnss/hal/Gpo4Client;)V

    invoke-static {v0}, Landroid/os/Binder;->withCleanCallingIdentity(Lcom/android/internal/util/FunctionalUtils$ThrowingRunnable;)V

    .line 243
    return-void
.end method

.method private unregisterPassiveLocationUpdates()V
    .locals 2

    .line 203
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mLocationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mPassiveLocationListener:Landroid/location/LocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 204
    return-void
.end method


# virtual methods
.method public blockEngineStart()Z
    .locals 5

    .line 268
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->isFeatureEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    .line 269
    invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->getEngineStatus()I

    move-result v0

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v1

    .line 270
    .local v0, "res":Z
    :goto_0
    iget-object v2, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "blockEngineStart ? "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Gpo4Client"

    invoke-virtual {v2, v4, v3, v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 271
    const/16 v1, 0x7d0

    invoke-direct {p0, v1}, Lcom/android/server/location/gnss/hal/Gpo4Client;->checkAppUsage(I)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->futureCheckNavigation1:Ljava/util/concurrent/ScheduledFuture;

    .line 272
    return v0
.end method

.method public clearLocationRequest()V
    .locals 3

    .line 292
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    const-string v1, "Gpo4Client"

    const-string v2, "clearLocationRequest"

    invoke-virtual {v0, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->futureCheckNavigation1:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 294
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->futureCheckNavigation2:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 295
    :cond_1
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->futureCheckRequest:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_2

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 296
    :cond_2
    return-void
.end method

.method public deinit()V
    .locals 3

    .line 86
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    const-string v1, "Gpo4Client"

    const-string v2, "deinit"

    invoke-virtual {v0, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo4Client;->unregisterPassiveLocationUpdates()V

    .line 88
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->scheduledThreadPoolExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    invoke-virtual {v0}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->shutdown()V

    .line 89
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->scheduledThreadPoolExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    .line 90
    return-void
.end method

.method public disableGnssSwitch()V
    .locals 2

    .line 93
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->isFeatureEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 94
    return-void
.end method

.method public init(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 74
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    const-string v1, "Gpo4Client"

    const-string v2, "init"

    invoke-virtual {v0, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    iput-object p1, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mContext:Landroid/content/Context;

    .line 76
    nop

    .line 77
    const-string v0, "location"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 76
    invoke-static {v0}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mLocationManager:Landroid/location/LocationManager;

    .line 78
    invoke-direct {p0}, Lcom/android/server/location/gnss/hal/Gpo4Client;->registerPassiveLocationUpdates()V

    .line 79
    new-instance v0, Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;-><init>(I)V

    iput-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->scheduledThreadPoolExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    .line 80
    invoke-virtual {v0, v1}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->setRemoveOnCancelPolicy(Z)V

    .line 81
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->scheduledThreadPoolExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->setExecuteExistingDelayedTasksAfterShutdownPolicy(Z)V

    .line 82
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->scheduledThreadPoolExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->setContinueExistingPeriodicTasksAfterShutdownPolicy(Z)V

    .line 83
    return-void
.end method

.method public removeLocationRequestId(I)V
    .locals 8
    .param p1, "listenerHashCode"    # I

    .line 246
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mNlpRequestMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    const-string v2, "Gpo4Client"

    if-eqz v0, :cond_0

    .line 247
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "removeNLP: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3, v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 248
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mNlpRequestMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mGlpRequestMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 251
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "removeGLP: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3, v1}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 252
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mGlpRequestMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;

    .line 253
    .local v0, "recorder":Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;
    iget-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mGlpRequestMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 254
    if-eqz v0, :cond_2

    .line 255
    invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;->getUid()I

    move-result v1

    .line 256
    .local v1, "uid":I
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;->getNlpReturnTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 257
    .local v2, "time":J
    invoke-static {}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->getInstance()Lcom/android/server/location/gnss/GnssEventTrackingStub;

    move-result-object v4

    iget-object v5, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mNavAppInfo:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 258
    const-wide/16 v6, 0xbb8

    cmp-long v6, v2, v6

    if-ltz v6, :cond_1

    move-wide v6, v2

    goto :goto_0

    :cond_1
    const-wide/16 v6, 0x0

    .line 257
    :goto_0
    invoke-interface {v4, v5, v6, v7}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->recordNavAppTime(Ljava/lang/String;J)V

    .line 259
    iget-object v4, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mNavAppInfo:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 260
    .end local v1    # "uid":I
    .end local v2    # "time":J
    goto :goto_1

    .line 261
    :cond_2
    iget-object v1, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mNavAppInfo:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 265
    .end local v0    # "recorder":Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;
    :cond_3
    :goto_1
    return-void
.end method

.method public reportLocation2Gpo(Landroid/location/Location;)V
    .locals 3
    .param p1, "location"    # Landroid/location/Location;

    .line 299
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/location/Location;->isComplete()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/location/Location;->getSpeed()F

    move-result v0

    const/high16 v1, 0x40400000    # 3.0f

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    .line 300
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->mLastTrafficTime:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 302
    :cond_0
    return-void
.end method

.method public saveLocationRequestId(ILjava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V
    .locals 18
    .param p1, "uid"    # I
    .param p2, "pkn"    # Ljava/lang/String;
    .param p3, "provider"    # Ljava/lang/String;
    .param p4, "listenerHashCode"    # I
    .param p5, "callbackType"    # Ljava/lang/Object;

    .line 111
    move-object/from16 v9, p0

    move-object/from16 v10, p3

    move/from16 v11, p4

    invoke-direct/range {p0 .. p0}, Lcom/android/server/location/gnss/hal/Gpo4Client;->checkFeatureSwitch()V

    .line 112
    iget-object v0, v9, Lcom/android/server/location/gnss/hal/Gpo4Client;->isFeatureEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 113
    :cond_0
    const-string v0, "network"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    const/4 v12, 0x0

    const-string v13, "Gpo4Client"

    if-eqz v0, :cond_1

    iget-object v0, v9, Lcom/android/server/location/gnss/hal/Gpo4Client;->mNlpRequestMap:Ljava/util/Map;

    .line 114
    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 115
    iget-object v0, v9, Lcom/android/server/location/gnss/hal/Gpo4Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "saveNLP: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v13, v1, v12}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 116
    iget-object v14, v9, Lcom/android/server/location/gnss/hal/Gpo4Client;->mNlpRequestMap:Ljava/util/Map;

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    new-instance v7, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;

    const/4 v6, 0x0

    const-wide/16 v16, 0x0

    move-object v0, v7

    move-object/from16 v1, p0

    move/from16 v2, p1

    move-object/from16 v3, p3

    move/from16 v4, p4

    move-object/from16 v5, p5

    move-object v12, v7

    move-wide/from16 v7, v16

    invoke-direct/range {v0 .. v8}, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;-><init>(Lcom/android/server/location/gnss/hal/Gpo4Client;ILjava/lang/String;ILjava/lang/Object;ZJ)V

    invoke-interface {v14, v15, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    :cond_1
    const-string v0, "gps"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, v9, Lcom/android/server/location/gnss/hal/Gpo4Client;->mGlpRequestMap:Ljava/util/Map;

    .line 120
    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 121
    iget-object v0, v9, Lcom/android/server/location/gnss/hal/Gpo4Client;->mGpoUtil:Lcom/android/server/location/gnss/hal/GpoUtil;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "saveGLP: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v13, v1, v2}, Lcom/android/server/location/gnss/hal/GpoUtil;->logi(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 122
    iget-object v0, v9, Lcom/android/server/location/gnss/hal/Gpo4Client;->mNavAppInfo:Ljava/util/Map;

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v12, p2

    invoke-interface {v0, v1, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    iget-object v13, v9, Lcom/android/server/location/gnss/hal/Gpo4Client;->mGlpRequestMap:Ljava/util/Map;

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    new-instance v15, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;

    const/4 v6, 0x0

    const-wide/16 v7, 0x0

    move-object v0, v15

    move-object/from16 v1, p0

    move/from16 v2, p1

    move-object/from16 v3, p3

    move/from16 v4, p4

    move-object/from16 v5, p5

    invoke-direct/range {v0 .. v8}, Lcom/android/server/location/gnss/hal/Gpo4Client$LocationRequestRecorder;-><init>(Lcom/android/server/location/gnss/hal/Gpo4Client;ILjava/lang/String;ILjava/lang/Object;ZJ)V

    invoke-interface {v13, v14, v15}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    iget-object v0, v9, Lcom/android/server/location/gnss/hal/Gpo4Client;->scheduledThreadPoolExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    new-instance v1, Lcom/android/server/location/gnss/hal/Gpo4Client$$ExternalSyntheticLambda0;

    invoke-direct {v1, v9, v11, v2}, Lcom/android/server/location/gnss/hal/Gpo4Client$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/location/gnss/hal/Gpo4Client;II)V

    const-wide/16 v3, 0xfa

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v3, v4, v5}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    goto :goto_0

    .line 120
    :cond_2
    move/from16 v2, p1

    move-object/from16 v12, p2

    goto :goto_0

    .line 119
    :cond_3
    move/from16 v2, p1

    move-object/from16 v12, p2

    .line 147
    :goto_0
    return-void
.end method

.method public updateScreenState(Z)V
    .locals 1
    .param p1, "on"    # Z

    .line 97
    iget-object v0, p0, Lcom/android/server/location/gnss/hal/Gpo4Client;->isScreenOn:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 98
    return-void
.end method
