public class com.android.server.location.gnss.map.AmapExtraCommand {
	 /* .source "AmapExtraCommand.java" */
	 /* # static fields */
	 public static final java.lang.String APP_ACTIVE_KEY;
	 public static final java.lang.String APP_CTRL_KEY;
	 public static final java.lang.String APP_CTRL_LOG_KEY;
	 public static final java.lang.String APP_FG_KEY;
	 public static final java.lang.String APP_LAST_RPT_TIME_KEY;
	 public static final java.lang.String APP_PERM_KEY;
	 public static final java.lang.String APP_PWR_MODE_KEY;
	 public static final java.lang.String GNSS_LAST_RPT_TIME_KEY;
	 public static final java.lang.String GNSS_REAL_KEY;
	 public static final java.lang.String GNSS_STATUS_KEY;
	 public static final java.lang.String GPS_TIMEOUT_CMD;
	 public static final java.lang.String LISTENER_HASHCODE_KEY;
	 public static final java.lang.String ORIGIN_KEY;
	 public static final java.lang.String OWNER;
	 public static final java.lang.String SAT_ALL_CNT_KEY;
	 public static final java.lang.String SAT_SNR_OVER0_CNT_KEY;
	 public static final java.lang.String SAT_SNR_OVER20_CNT_KEY;
	 public static final Integer SV_STATUS_INTERVAL_MILLIS;
	 public static final java.lang.String VERSION_KEY;
	 public static final java.lang.String VERSION_NAME;
	 private static final java.util.ArrayList sSupportedCommands;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/ArrayList<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
/* # direct methods */
static com.android.server.location.gnss.map.AmapExtraCommand ( ) {
/* .locals 2 */
/* .line 39 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 42 */
/* const-string/jumbo v1, "send_gps_timeout" */
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 43 */
return;
} // .end method
private com.android.server.location.gnss.map.AmapExtraCommand ( ) {
/* .locals 0 */
/* .line 45 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 47 */
return;
} // .end method
public static Boolean isSupported ( java.lang.String p0, android.os.Bundle p1 ) {
/* .locals 2 */
/* .param p0, "command" # Ljava/lang/String; */
/* .param p1, "extras" # Landroid/os/Bundle; */
/* .line 50 */
final String v0 = "origin"; // const-string v0, "origin"
(( android.os.Bundle ) p1 ).getString ( v0 ); // invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
final String v1 = "amap"; // const-string v1, "amap"
v0 = (( java.lang.String ) v1 ).equalsIgnoreCase ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
/* if-nez v0, :cond_0 */
/* .line 51 */
int v0 = 0; // const/4 v0, 0x0
/* .line 54 */
} // :cond_0
v0 = com.android.server.location.gnss.map.AmapExtraCommand.sSupportedCommands;
v0 = (( java.util.ArrayList ) v0 ).contains ( p0 ); // invoke-virtual {v0, p0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
} // .end method
