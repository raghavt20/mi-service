.class public Lcom/android/server/location/gnss/map/AmapExtraCommand;
.super Ljava/lang/Object;
.source "AmapExtraCommand.java"


# static fields
.field public static final APP_ACTIVE_KEY:Ljava/lang/String; = "app_active"

.field public static final APP_CTRL_KEY:Ljava/lang/String; = "app_control"

.field public static final APP_CTRL_LOG_KEY:Ljava/lang/String; = "app_control_log"

.field public static final APP_FG_KEY:Ljava/lang/String; = "app_forground"

.field public static final APP_LAST_RPT_TIME_KEY:Ljava/lang/String; = "app_last_report_second"

.field public static final APP_PERM_KEY:Ljava/lang/String; = "app_permission"

.field public static final APP_PWR_MODE_KEY:Ljava/lang/String; = "app_power_mode"

.field public static final GNSS_LAST_RPT_TIME_KEY:Ljava/lang/String; = "gnss_last_report_second"

.field public static final GNSS_REAL_KEY:Ljava/lang/String; = "gnss_real"

.field public static final GNSS_STATUS_KEY:Ljava/lang/String; = "gnss_status"

.field public static final GPS_TIMEOUT_CMD:Ljava/lang/String; = "send_gps_timeout"

.field public static final LISTENER_HASHCODE_KEY:Ljava/lang/String; = "listenerHashcode"

.field public static final ORIGIN_KEY:Ljava/lang/String; = "origin"

.field public static final OWNER:Ljava/lang/String; = "amap"

.field public static final SAT_ALL_CNT_KEY:Ljava/lang/String; = "satellite_all_count"

.field public static final SAT_SNR_OVER0_CNT_KEY:Ljava/lang/String; = "satellite_snr_over0_count"

.field public static final SAT_SNR_OVER20_CNT_KEY:Ljava/lang/String; = "satellite_snr_over20_count"

.field public static final SV_STATUS_INTERVAL_MILLIS:I = 0x1388

.field public static final VERSION_KEY:Ljava/lang/String; = "version"

.field public static final VERSION_NAME:Ljava/lang/String; = "v2"

.field private static final sSupportedCommands:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/location/gnss/map/AmapExtraCommand;->sSupportedCommands:Ljava/util/ArrayList;

    .line 42
    const-string/jumbo v1, "send_gps_timeout"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 43
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    return-void
.end method

.method public static isSupported(Ljava/lang/String;Landroid/os/Bundle;)Z
    .locals 2
    .param p0, "command"    # Ljava/lang/String;
    .param p1, "extras"    # Landroid/os/Bundle;

    .line 50
    const-string v0, "origin"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "amap"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 51
    const/4 v0, 0x0

    return v0

    .line 54
    :cond_0
    sget-object v0, Lcom/android/server/location/gnss/map/AmapExtraCommand;->sSupportedCommands:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
