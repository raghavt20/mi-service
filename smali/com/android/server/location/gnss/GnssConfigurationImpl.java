public class com.android.server.location.gnss.GnssConfigurationImpl implements com.android.server.location.gnss.GnssConfigurationStub {
	 /* .source "GnssConfigurationImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final java.lang.String CONFIG_NFW_PROXY_APPS;
	 private static final java.lang.String CONFIG_SUPL_HOST;
	 private static final java.lang.String QX_SUPL_ADDRESS;
	 private static final java.lang.String XTY_SUPL_ADDRESS;
	 /* # direct methods */
	 public com.android.server.location.gnss.GnssConfigurationImpl ( ) {
		 /* .locals 0 */
		 /* .line 9 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public void loadPropertiesFromCarrierConfig ( java.util.Properties p0 ) {
		 /* .locals 4 */
		 /* .param p1, "properties" # Ljava/util/Properties; */
		 /* .line 19 */
		 v0 = 		 com.android.server.location.gnss.GnssCollectDataStub .getInstance ( );
		 if ( v0 != null) { // if-eqz v0, :cond_1
			 /* .line 21 */
			 v0 = 			 com.android.server.location.gnss.GnssCollectDataStub .getInstance ( );
			 /* .line 22 */
			 /* .local v0, "CaictState":Z */
			 com.android.server.location.LocationDumpLogStub .getInstance ( );
			 /* new-instance v2, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
			 final String v3 = "Caictstate is : "; // const-string v3, "Caictstate is : "
			 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 int v3 = 2; // const/4 v3, 0x2
			 /* .line 24 */
			 final String v1 = "SUPL_HOST"; // const-string v1, "SUPL_HOST"
			 if ( v0 != null) { // if-eqz v0, :cond_0
				 /* .line 25 */
				 /* const-string/jumbo v2, "supl.bd-caict.com" */
				 (( java.util.Properties ) p1 ).setProperty ( v1, v2 ); // invoke-virtual {p1, v1, v2}, Ljava/util/Properties;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
				 /* .line 27 */
			 } // :cond_0
			 /* const-string/jumbo v2, "supl.qxwz.com" */
			 (( java.util.Properties ) p1 ).setProperty ( v1, v2 ); // invoke-virtual {p1, v1, v2}, Ljava/util/Properties;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
			 /* .line 31 */
		 } // .end local v0 # "CaictState":Z
	 } // :cond_1
} // :goto_0
com.android.server.location.gnss.GnssLocationProviderStub .getInstance ( );
final String v1 = "NFW_PROXY_APPS"; // const-string v1, "NFW_PROXY_APPS"
/* .line 32 */
return;
} // .end method
