.class public Lcom/android/server/location/gnss/GnssConfigurationImpl;
.super Ljava/lang/Object;
.source "GnssConfigurationImpl.java"

# interfaces
.implements Lcom/android/server/location/gnss/GnssConfigurationStub;


# static fields
.field private static final CONFIG_NFW_PROXY_APPS:Ljava/lang/String; = "NFW_PROXY_APPS"

.field private static final CONFIG_SUPL_HOST:Ljava/lang/String; = "SUPL_HOST"

.field private static final QX_SUPL_ADDRESS:Ljava/lang/String; = "supl.qxwz.com"

.field private static final XTY_SUPL_ADDRESS:Ljava/lang/String; = "supl.bd-caict.com"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public loadPropertiesFromCarrierConfig(Ljava/util/Properties;)V
    .locals 4
    .param p1, "properties"    # Ljava/util/Properties;

    .line 19
    invoke-static {}, Lcom/android/server/location/gnss/GnssCollectDataStub;->getInstance()Lcom/android/server/location/gnss/GnssCollectDataStub;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/server/location/gnss/GnssCollectDataStub;->isCnSimInserted()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 21
    invoke-static {}, Lcom/android/server/location/gnss/GnssCollectDataStub;->getInstance()Lcom/android/server/location/gnss/GnssCollectDataStub;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/server/location/gnss/GnssCollectDataStub;->getSuplState()Z

    move-result v0

    .line 22
    .local v0, "CaictState":Z
    invoke-static {}, Lcom/android/server/location/LocationDumpLogStub;->getInstance()Lcom/android/server/location/LocationDumpLogStub;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Caictstate is : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    invoke-interface {v1, v3, v2}, Lcom/android/server/location/LocationDumpLogStub;->addToBugreport(ILjava/lang/String;)V

    .line 24
    const-string v1, "SUPL_HOST"

    if-eqz v0, :cond_0

    .line 25
    const-string/jumbo v2, "supl.bd-caict.com"

    invoke-virtual {p1, v1, v2}, Ljava/util/Properties;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    goto :goto_0

    .line 27
    :cond_0
    const-string/jumbo v2, "supl.qxwz.com"

    invoke-virtual {p1, v1, v2}, Ljava/util/Properties;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 31
    .end local v0    # "CaictState":Z
    :cond_1
    :goto_0
    invoke-static {}, Lcom/android/server/location/gnss/GnssLocationProviderStub;->getInstance()Lcom/android/server/location/gnss/GnssLocationProviderStub;

    move-result-object v0

    const-string v1, "NFW_PROXY_APPS"

    invoke-interface {v0, p1, v1}, Lcom/android/server/location/gnss/GnssLocationProviderStub;->setNfwProxyAppConfig(Ljava/util/Properties;Ljava/lang/String;)V

    .line 32
    return-void
.end method
