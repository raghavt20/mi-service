.class Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl$1;
.super Landroid/app/IProcessObserver$Stub;
.source "GnssBackgroundUsageOptImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->registerProcessObserver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;


# direct methods
.method constructor <init>(Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;

    .line 368
    iput-object p1, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl$1;->this$0:Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;

    invoke-direct {p0}, Landroid/app/IProcessObserver$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onForegroundActivitiesChanged(IIZ)V
    .locals 0
    .param p1, "pid"    # I
    .param p2, "uid"    # I
    .param p3, "foregroundActivities"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 372
    return-void
.end method

.method public onForegroundServicesChanged(III)V
    .locals 0
    .param p1, "pid"    # I
    .param p2, "uid"    # I
    .param p3, "serviceTypes"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 377
    return-void
.end method

.method public onProcessDied(II)V
    .locals 7
    .param p1, "pid"    # I
    .param p2, "uid"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 382
    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl$1;->this$0:Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;

    invoke-static {v1}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->-$$Nest$fgetmRequestMap(Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;)Ljava/util/Map;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 383
    .local v0, "reqMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/location/gnss/exp/GnssRequestBean;>;"
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 384
    .local v2, "map":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/location/gnss/exp/GnssRequestBean;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;

    iget-object v3, v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;->identity:Landroid/location/util/identity/CallerIdentity;

    invoke-virtual {v3}, Landroid/location/util/identity/CallerIdentity;->getUid()I

    move-result v3

    if-ne p2, v3, :cond_2

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;

    iget-object v3, v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;->identity:Landroid/location/util/identity/CallerIdentity;

    invoke-virtual {v3}, Landroid/location/util/identity/CallerIdentity;->getPid()I

    move-result v3

    if-ne p1, v3, :cond_2

    .line 386
    iget-object v3, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl$1;->this$0:Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;

    invoke-static {v3, p1}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->-$$Nest$misProcessAlive(Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;I)Z

    move-result v3

    const-string v4, "GnssBackgroundUsageOpt"

    if-eqz v3, :cond_0

    .line 387
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pid:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " is still alive, do not need clear map"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 388
    return-void

    .line 390
    :cond_0
    iget-object v3, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl$1;->this$0:Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;

    invoke-static {v3}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->-$$Nest$fgetmRequestMap(Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;)Ljava/util/Map;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-dupReq"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 391
    iget-object v3, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl$1;->this$0:Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;

    invoke-static {v3}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->-$$Nest$fgetmRequestMap(Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 392
    iget-object v3, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl$1;->this$0:Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;

    invoke-static {v3}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->-$$Nest$fgetmRemoveThreadMap(Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Thread;

    .line 393
    .local v3, "removeThread":Ljava/lang/Thread;
    if-eqz v3, :cond_1

    .line 394
    invoke-virtual {v3}, Ljava/lang/Thread;->interrupt()V

    .line 395
    const-string v5, "onProcessDied, remove Thread not null, interrupt it..."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onProcessDied remove "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 401
    .end local v2    # "map":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/location/gnss/exp/GnssRequestBean;>;"
    .end local v3    # "removeThread":Ljava/lang/Thread;
    :cond_2
    goto/16 :goto_0

    .line 402
    :cond_3
    return-void
.end method
