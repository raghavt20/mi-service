public class inal implements com.miui.base.MiuiStubRegistry$ImplProviderManifest {
	 /* .source "GnssBackgroundUsageOptStub$$.java" */
	 /* # interfaces */
	 /* # direct methods */
	 public inal ( ) {
		 /* .locals 0 */
		 /* .line 34 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public final void collectImplProviders ( java.util.Map p0 ) {
		 /* .locals 2 */
		 /* .annotation system Ldalvik/annotation/Signature; */
		 /* value = { */
		 /* "(", */
		 /* "Ljava/util/Map<", */
		 /* "Ljava/lang/String;", */
		 /* "Lcom/miui/base/MiuiStubRegistry$ImplProvider<", */
		 /* "*>;>;)V" */
		 /* } */
	 } // .end annotation
	 /* .line 38 */
	 /* .local p1, "outProviders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/miui/base/MiuiStubRegistry$ImplProvider<*>;>;" */
	 /* new-instance v0, Lcom/android/server/location/gnss/operators/GnssForCommonOperatorImpl$Provider; */
	 /* invoke-direct {v0}, Lcom/android/server/location/gnss/operators/GnssForCommonOperatorImpl$Provider;-><init>()V */
	 final String v1 = "com.android.server.location.gnss.operators.GnssForOperatorCommonStub"; // const-string v1, "com.android.server.location.gnss.operators.GnssForOperatorCommonStub"
	 /* .line 39 */
	 /* new-instance v0, Lcom/android/server/location/GnssMockLocationOptImpl$Provider; */
	 /* invoke-direct {v0}, Lcom/android/server/location/GnssMockLocationOptImpl$Provider;-><init>()V */
	 final String v1 = "com.android.server.location.GnssMockLocationOptStub"; // const-string v1, "com.android.server.location.GnssMockLocationOptStub"
	 /* .line 40 */
	 /* new-instance v0, Lcom/android/server/location/gnss/GnssLocationProviderImpl$Provider; */
	 /* invoke-direct {v0}, Lcom/android/server/location/gnss/GnssLocationProviderImpl$Provider;-><init>()V */
	 final String v1 = "com.android.server.location.gnss.GnssLocationProviderStub"; // const-string v1, "com.android.server.location.gnss.GnssLocationProviderStub"
	 /* .line 41 */
	 /* new-instance v0, Lcom/android/server/location/MtkGnssPowerSaveImpl$Provider; */
	 /* invoke-direct {v0}, Lcom/android/server/location/MtkGnssPowerSaveImpl$Provider;-><init>()V */
	 final String v1 = "com.android.server.location.MtkGnssPowerSaveStub"; // const-string v1, "com.android.server.location.MtkGnssPowerSaveStub"
	 /* .line 42 */
	 /* new-instance v0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl$Provider; */
	 /* invoke-direct {v0}, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl$Provider;-><init>()V */
	 final String v1 = "com.android.server.location.GnssSmartSatelliteSwitchStub"; // const-string v1, "com.android.server.location.GnssSmartSatelliteSwitchStub"
	 /* .line 43 */
	 /* new-instance v0, Lcom/android/server/location/MiuiBlurLocationManagerImpl$Provider; */
	 /* invoke-direct {v0}, Lcom/android/server/location/MiuiBlurLocationManagerImpl$Provider;-><init>()V */
	 final String v1 = "com.android.server.location.MiuiBlurLocationManagerStub"; // const-string v1, "com.android.server.location.MiuiBlurLocationManagerStub"
	 /* .line 44 */
	 /* new-instance v0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$Provider; */
	 /* invoke-direct {v0}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$Provider;-><init>()V */
	 final String v1 = "com.android.server.location.gnss.GnssEventTrackingStub"; // const-string v1, "com.android.server.location.gnss.GnssEventTrackingStub"
	 /* .line 45 */
	 /* new-instance v0, Lcom/android/server/location/LocationExtCooperateImpl$Provider; */
	 /* invoke-direct {v0}, Lcom/android/server/location/LocationExtCooperateImpl$Provider;-><init>()V */
	 final String v1 = "com.android.server.location.LocationExtCooperateStub"; // const-string v1, "com.android.server.location.LocationExtCooperateStub"
	 /* .line 46 */
	 /* new-instance v0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl$Provider; */
	 /* invoke-direct {v0}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl$Provider;-><init>()V */
	 final String v1 = "com.android.server.location.gnss.exp.GnssBackgroundUsageOptStub"; // const-string v1, "com.android.server.location.gnss.exp.GnssBackgroundUsageOptStub"
	 /* .line 47 */
	 /* new-instance v0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl$Provider; */
	 /* invoke-direct {v0}, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl$Provider;-><init>()V */
	 final String v1 = "com.android.server.location.gnss.hal.GnssScoringModelStub"; // const-string v1, "com.android.server.location.gnss.hal.GnssScoringModelStub"
	 /* .line 48 */
	 /* new-instance v0, Lcom/android/server/location/gnss/operators/GnssForKtCustomImpl$Provider; */
	 /* invoke-direct {v0}, Lcom/android/server/location/gnss/operators/GnssForKtCustomImpl$Provider;-><init>()V */
	 final String v1 = "com.android.server.location.gnss.operators.GnssForKtCustomStub"; // const-string v1, "com.android.server.location.gnss.operators.GnssForKtCustomStub"
	 /* .line 49 */
	 /* new-instance v0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl$Provider; */
	 /* invoke-direct {v0}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl$Provider;-><init>()V */
	 final String v1 = "com.android.server.location.gnss.hal.GnssPowerOptimizeStub"; // const-string v1, "com.android.server.location.gnss.hal.GnssPowerOptimizeStub"
	 /* .line 50 */
	 /* new-instance v0, Lcom/android/server/location/LocationDumpLogImpl$Provider; */
	 /* invoke-direct {v0}, Lcom/android/server/location/LocationDumpLogImpl$Provider;-><init>()V */
	 final String v1 = "com.android.server.location.LocationDumpLogStub"; // const-string v1, "com.android.server.location.LocationDumpLogStub"
	 /* .line 51 */
	 /* new-instance v0, Lcom/android/server/location/gnss/operators/GnssForTelcelCustomImpl$Provider; */
	 /* invoke-direct {v0}, Lcom/android/server/location/gnss/operators/GnssForTelcelCustomImpl$Provider;-><init>()V */
	 final String v1 = "com.android.server.location.gnss.operators.GnssForTelcelCustomStub"; // const-string v1, "com.android.server.location.gnss.operators.GnssForTelcelCustomStub"
	 /* .line 52 */
	 /* new-instance v0, Lcom/android/server/location/provider/LocationProviderManagerImpl$Provider; */
	 /* invoke-direct {v0}, Lcom/android/server/location/provider/LocationProviderManagerImpl$Provider;-><init>()V */
	 final String v1 = "com.android.server.location.provider.LocationProviderManagerStub"; // const-string v1, "com.android.server.location.provider.LocationProviderManagerStub"
	 /* .line 53 */
	 /* new-instance v0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery$Provider; */
	 /* invoke-direct {v0}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery$Provider;-><init>()V */
	 final String v1 = "com.android.server.location.gnss.gnssSelfRecovery.GnssSelfRecoveryStub"; // const-string v1, "com.android.server.location.gnss.gnssSelfRecovery.GnssSelfRecoveryStub"
	 /* .line 54 */
	 /* new-instance v0, Lcom/android/server/location/gnss/monitor/LocationSettingsEventMonitorImpl$Provider; */
	 /* invoke-direct {v0}, Lcom/android/server/location/gnss/monitor/LocationSettingsEventMonitorImpl$Provider;-><init>()V */
	 final String v1 = "com.android.server.location.gnss.monitor.LocationSettingsEventMonitorStub"; // const-string v1, "com.android.server.location.gnss.monitor.LocationSettingsEventMonitorStub"
	 /* .line 55 */
	 /* new-instance v0, Lcom/android/server/location/gnss/hal/GnssNativeImpl$Provider; */
	 /* invoke-direct {v0}, Lcom/android/server/location/gnss/hal/GnssNativeImpl$Provider;-><init>()V */
	 final String v1 = "com.android.server.location.gnss.hal.GnssNativeStub"; // const-string v1, "com.android.server.location.gnss.hal.GnssNativeStub"
	 /* .line 56 */
	 /* new-instance v0, Lcom/android/server/location/provider/AmapCustomImpl$Provider; */
	 /* invoke-direct {v0}, Lcom/android/server/location/provider/AmapCustomImpl$Provider;-><init>()V */
	 final String v1 = "com.android.server.location.provider.AmapCustomStub"; // const-string v1, "com.android.server.location.provider.AmapCustomStub"
	 /* .line 57 */
	 /* new-instance v0, Lcom/android/server/location/GnssCollectDataImpl$Provider; */
	 /* invoke-direct {v0}, Lcom/android/server/location/GnssCollectDataImpl$Provider;-><init>()V */
	 final String v1 = "com.android.server.location.gnss.GnssCollectDataStub"; // const-string v1, "com.android.server.location.gnss.GnssCollectDataStub"
	 /* .line 58 */
	 /* new-instance v0, Lcom/android/server/location/gnss/GnssConfigurationImpl$Provider; */
	 /* invoke-direct {v0}, Lcom/android/server/location/gnss/GnssConfigurationImpl$Provider;-><init>()V */
	 final String v1 = "com.android.server.location.gnss.GnssConfigurationStub"; // const-string v1, "com.android.server.location.gnss.GnssConfigurationStub"
	 /* .line 59 */
	 return;
} // .end method
