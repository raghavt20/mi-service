class com.android.server.location.gnss.exp.GnssBackgroundUsageOptImpl$1 extends android.app.IProcessObserver$Stub {
	 /* .source "GnssBackgroundUsageOptImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->registerProcessObserver()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.location.gnss.exp.GnssBackgroundUsageOptImpl this$0; //synthetic
/* # direct methods */
 com.android.server.location.gnss.exp.GnssBackgroundUsageOptImpl$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl; */
/* .line 368 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/app/IProcessObserver$Stub;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onForegroundActivitiesChanged ( Integer p0, Integer p1, Boolean p2 ) {
/* .locals 0 */
/* .param p1, "pid" # I */
/* .param p2, "uid" # I */
/* .param p3, "foregroundActivities" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 372 */
return;
} // .end method
public void onForegroundServicesChanged ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 0 */
/* .param p1, "pid" # I */
/* .param p2, "uid" # I */
/* .param p3, "serviceTypes" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 377 */
return;
} // .end method
public void onProcessDied ( Integer p0, Integer p1 ) {
/* .locals 7 */
/* .param p1, "pid" # I */
/* .param p2, "uid" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 382 */
/* new-instance v0, Ljava/util/HashMap; */
v1 = this.this$0;
com.android.server.location.gnss.exp.GnssBackgroundUsageOptImpl .-$$Nest$fgetmRequestMap ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V */
/* .line 383 */
/* .local v0, "reqMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/location/gnss/exp/GnssRequestBean;>;" */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_3
/* check-cast v2, Ljava/util/Map$Entry; */
/* .line 384 */
/* .local v2, "map":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/location/gnss/exp/GnssRequestBean;>;" */
/* check-cast v3, Lcom/android/server/location/gnss/exp/GnssRequestBean; */
v3 = this.identity;
v3 = (( android.location.util.identity.CallerIdentity ) v3 ).getUid ( ); // invoke-virtual {v3}, Landroid/location/util/identity/CallerIdentity;->getUid()I
/* if-ne p2, v3, :cond_2 */
/* check-cast v3, Lcom/android/server/location/gnss/exp/GnssRequestBean; */
v3 = this.identity;
v3 = (( android.location.util.identity.CallerIdentity ) v3 ).getPid ( ); // invoke-virtual {v3}, Landroid/location/util/identity/CallerIdentity;->getPid()I
/* if-ne p1, v3, :cond_2 */
/* .line 386 */
v3 = this.this$0;
v3 = com.android.server.location.gnss.exp.GnssBackgroundUsageOptImpl .-$$Nest$misProcessAlive ( v3,p1 );
final String v4 = "GnssBackgroundUsageOpt"; // const-string v4, "GnssBackgroundUsageOpt"
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 387 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "pid:"; // const-string v3, "pid:"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " is still alive, do not need clear map"; // const-string v3, " is still alive, do not need clear map"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v4,v1 );
/* .line 388 */
return;
/* .line 390 */
} // :cond_0
v3 = this.this$0;
com.android.server.location.gnss.exp.GnssBackgroundUsageOptImpl .-$$Nest$fgetmRequestMap ( v3 );
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* check-cast v6, Ljava/lang/String; */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = "-dupReq"; // const-string v6, "-dupReq"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 391 */
v3 = this.this$0;
com.android.server.location.gnss.exp.GnssBackgroundUsageOptImpl .-$$Nest$fgetmRequestMap ( v3 );
/* .line 392 */
v3 = this.this$0;
com.android.server.location.gnss.exp.GnssBackgroundUsageOptImpl .-$$Nest$fgetmRemoveThreadMap ( v3 );
/* check-cast v3, Ljava/lang/Thread; */
/* .line 393 */
/* .local v3, "removeThread":Ljava/lang/Thread; */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 394 */
(( java.lang.Thread ) v3 ).interrupt ( ); // invoke-virtual {v3}, Ljava/lang/Thread;->interrupt()V
/* .line 395 */
final String v5 = "onProcessDied, remove Thread not null, interrupt it..."; // const-string v5, "onProcessDied, remove Thread not null, interrupt it..."
android.util.Log .d ( v4,v5 );
/* .line 398 */
} // :cond_1
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "onProcessDied remove "; // const-string v6, "onProcessDied remove "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* check-cast v6, Ljava/lang/String; */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v4,v5 );
/* .line 401 */
} // .end local v2 # "map":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/location/gnss/exp/GnssRequestBean;>;"
} // .end local v3 # "removeThread":Ljava/lang/Thread;
} // :cond_2
/* goto/16 :goto_0 */
/* .line 402 */
} // :cond_3
return;
} // .end method
