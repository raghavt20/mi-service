public class com.android.server.location.gnss.exp.GnssBackgroundUsageOptImpl extends com.android.server.location.gnss.exp.GnssBackgroundUsageOptStub {
	 /* .source "GnssBackgroundUsageOptImpl.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.location.gnss.exp.GnssBackgroundUsageOptStub$$" */
} // .end annotation
/* # static fields */
private static final Integer BACKGROUND_USAGE_OPT_REMOVE_IMMEDIATELY;
private static final Integer BACKGROUND_USAGE_OPT_TIME;
private static final Integer DEF_UID;
private static final java.lang.String REQ_KEY_SUFFIX_DUP;
private static final java.lang.String TAG;
private static android.content.Context mContext;
/* # instance fields */
private final Boolean D;
private Boolean mAlreadyLoadDataFromSP;
private final java.util.Map mBackOpt3Map;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/lang/Long;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.io.File mCloudSpFile;
private Boolean mCurrentForeground;
private final Boolean mDefaultFeatureStatus;
private android.app.IProcessObserver$Stub mIProcessObserverStub;
private com.android.server.location.gnss.exp.GnssBackgroundUsageOptStub$IRemoveRequest mIRemoveRequest;
private com.android.server.location.gnss.exp.GnssBackgroundUsageOptStub$IRestoreRequest mIRestoreRequest;
private volatile Boolean mIsBackFromGsco;
private final java.util.concurrent.atomic.AtomicBoolean mIsInSatelliteCallMode;
private final java.util.concurrent.atomic.AtomicBoolean mIsSpecifiedDevice;
private volatile Boolean mIsUseUidCtl;
private Boolean mOldForeground;
private Integer mOldUid;
private final java.util.Map mRemoveThreadMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Thread;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Integer mRemovedUid;
private final java.util.Map mRequestMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Lcom/android/server/location/gnss/exp/GnssRequestBean;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.HashSet mSatelliteCallAppUidSet;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashSet<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.HashSet mSatelliteCallPkgSet;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashSet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Long mScStartTime;
private final java.util.Map mUidForMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/lang/Boolean;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
public static void $r8$lambda$2RXna1m8T_aYV6u3o8VpfiYheZA ( com.android.server.location.gnss.exp.GnssBackgroundUsageOptImpl p0, java.lang.String p1, Integer p2, java.lang.String p3 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->lambda$remove$2(Ljava/lang/String;ILjava/lang/String;)V */
return;
} // .end method
public static void $r8$lambda$fCWvbKOx5RX1u9kG8aORe42auv0 ( com.android.server.location.gnss.exp.GnssBackgroundUsageOptImpl p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->lambda$unRegisterSatelliteCallMode$1()V */
return;
} // .end method
public static void $r8$lambda$wH6cmURY5LEvZfPpymXklJErtxE ( com.android.server.location.gnss.exp.GnssBackgroundUsageOptImpl p0, java.util.HashSet p1, java.lang.String p2 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->lambda$registerSatelliteCallMode$0(Ljava/util/HashSet;Ljava/lang/String;)V */
return;
} // .end method
static java.util.Map -$$Nest$fgetmRemoveThreadMap ( com.android.server.location.gnss.exp.GnssBackgroundUsageOptImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mRemoveThreadMap;
} // .end method
static java.util.Map -$$Nest$fgetmRequestMap ( com.android.server.location.gnss.exp.GnssBackgroundUsageOptImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mRequestMap;
} // .end method
static Boolean -$$Nest$misProcessAlive ( com.android.server.location.gnss.exp.GnssBackgroundUsageOptImpl p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->isProcessAlive(I)Z */
} // .end method
 com.android.server.location.gnss.exp.GnssBackgroundUsageOptImpl ( ) {
/* .locals 5 */
/* .line 71 */
/* invoke-direct {p0}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub;-><init>()V */
/* .line 46 */
/* new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V */
this.mIsSpecifiedDevice = v0;
/* .line 48 */
/* new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean; */
/* invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V */
this.mIsInSatelliteCallMode = v0;
/* .line 50 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.mUidForMap = v0;
/* .line 53 */
/* new-instance v0, Ljava/io/File; */
/* new-instance v2, Ljava/io/File; */
android.os.Environment .getDataDirectory ( );
/* const-string/jumbo v4, "system" */
/* invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
final String v3 = "IsSpecifiedDevice.xml"; // const-string v3, "IsSpecifiedDevice.xml"
/* invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
this.mCloudSpFile = v0;
/* .line 54 */
final String v0 = "persist.sys.gnss_back.opt"; // const-string v0, "persist.sys.gnss_back.opt"
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
/* iput-boolean v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mDefaultFeatureStatus:Z */
/* .line 55 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->D:Z */
/* .line 56 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.mRequestMap = v0;
/* .line 57 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.mRemoveThreadMap = v0;
/* .line 58 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.mBackOpt3Map = v0;
/* .line 65 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.mSatelliteCallAppUidSet = v0;
/* .line 66 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.mSatelliteCallPkgSet = v0;
/* .line 72 */
return;
} // .end method
private java.lang.String createReqKey ( Integer p0, Integer p1, java.lang.String p2, java.lang.String p3 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "providerName" # Ljava/lang/String; */
/* .param p4, "listenerId" # Ljava/lang/String; */
/* .line 614 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
java.lang.Integer .toString ( p1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.Integer .toString ( p2 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p4 ); // invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
private Integer getForegroundProcessUid ( ) {
/* .locals 7 */
/* .line 481 */
v0 = com.android.server.location.gnss.exp.GnssBackgroundUsageOptImpl.mContext;
int v1 = -1; // const/4 v1, -0x1
/* if-nez v0, :cond_0 */
/* .line 482 */
} // :cond_0
final String v2 = "activity"; // const-string v2, "activity"
(( android.content.Context ) v0 ).getSystemService ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/ActivityManager; */
/* .line 483 */
/* .local v0, "activityManager":Landroid/app/ActivityManager; */
(( android.app.ActivityManager ) v0 ).getRunningAppProcesses ( ); // invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;
/* .line 484 */
/* .local v2, "runningAppProcesses":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;" */
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_2
/* check-cast v4, Landroid/app/ActivityManager$RunningAppProcessInfo; */
/* .line 485 */
/* .local v4, "processInfo":Landroid/app/ActivityManager$RunningAppProcessInfo; */
/* iget v5, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I */
/* const/16 v6, 0x64 */
/* if-ne v5, v6, :cond_1 */
/* .line 486 */
/* iget v1, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->uid:I */
/* .line 488 */
} // .end local v4 # "processInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
} // :cond_1
/* .line 489 */
} // :cond_2
} // .end method
private Integer getUidFromKey ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p1, "key" # Ljava/lang/String; */
/* .line 458 */
int v0 = -1; // const/4 v0, -0x1
/* .line 459 */
/* .local v0, "uid":I */
final String v1 = "\\d+"; // const-string v1, "\\d+"
java.util.regex.Pattern .compile ( v1 );
/* .line 460 */
/* .local v1, "pattern":Ljava/util/regex/Pattern; */
(( java.util.regex.Pattern ) v1 ).matcher ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
/* .line 461 */
/* .local v2, "matcher":Ljava/util/regex/Matcher; */
v3 = (( java.util.regex.Matcher ) v2 ).find ( ); // invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 463 */
try { // :try_start_0
(( java.util.regex.Matcher ) v2 ).group ( ); // invoke-virtual {v2}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;
v3 = java.lang.Integer .parseInt ( v3 );
/* :try_end_0 */
/* .catch Ljava/lang/NumberFormatException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move v0, v3 */
/* .line 466 */
/* .line 464 */
/* :catch_0 */
/* move-exception v3 */
/* .line 465 */
/* .local v3, "e":Ljava/lang/NumberFormatException; */
final String v4 = "GnssBackgroundUsageOpt"; // const-string v4, "GnssBackgroundUsageOpt"
/* const-string/jumbo v5, "uid parseInt NumberFormatException..." */
android.util.Log .e ( v4,v5 );
/* .line 468 */
} // .end local v3 # "e":Ljava/lang/NumberFormatException;
} // :cond_0
} // :goto_0
} // .end method
private Boolean isForegroundService ( Integer p0, Integer p1 ) {
/* .locals 6 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .line 581 */
v0 = com.android.server.location.gnss.exp.GnssBackgroundUsageOptImpl.mContext;
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 582 */
} // :cond_0
final String v2 = "activity"; // const-string v2, "activity"
(( android.content.Context ) v0 ).getSystemService ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/ActivityManager; */
/* .line 583 */
/* .local v0, "activityManager":Landroid/app/ActivityManager; */
/* const v2, 0x7fffffff */
(( android.app.ActivityManager ) v0 ).getRunningServices ( v2 ); // invoke-virtual {v0, v2}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;
/* .line 584 */
/* .local v2, "runningServices":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningServiceInfo;>;" */
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_2
/* check-cast v4, Landroid/app/ActivityManager$RunningServiceInfo; */
/* .line 585 */
/* .local v4, "service":Landroid/app/ActivityManager$RunningServiceInfo; */
/* iget v5, v4, Landroid/app/ActivityManager$RunningServiceInfo;->uid:I */
/* if-ne v5, p1, :cond_1 */
/* iget v5, v4, Landroid/app/ActivityManager$RunningServiceInfo;->pid:I */
/* if-ne v5, p2, :cond_1 */
/* .line 586 */
/* iget-boolean v5, v4, Landroid/app/ActivityManager$RunningServiceInfo;->foreground:Z */
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 588 */
int v1 = 1; // const/4 v1, 0x1
/* .line 591 */
} // .end local v4 # "service":Landroid/app/ActivityManager$RunningServiceInfo;
} // :cond_1
/* .line 592 */
} // :cond_2
} // .end method
private Boolean isProcessAlive ( Integer p0 ) {
/* .locals 6 */
/* .param p1, "pid" # I */
/* .line 493 */
v0 = com.android.server.location.gnss.exp.GnssBackgroundUsageOptImpl.mContext;
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 494 */
} // :cond_0
final String v2 = "activity"; // const-string v2, "activity"
(( android.content.Context ) v0 ).getSystemService ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/ActivityManager; */
/* .line 495 */
/* .local v0, "activityManager":Landroid/app/ActivityManager; */
(( android.app.ActivityManager ) v0 ).getRunningAppProcesses ( ); // invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;
/* .line 496 */
/* .local v2, "processList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;" */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 497 */
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_2
/* check-cast v4, Landroid/app/ActivityManager$RunningAppProcessInfo; */
/* .line 498 */
/* .local v4, "processInfo":Landroid/app/ActivityManager$RunningAppProcessInfo; */
/* iget v5, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I */
/* if-ne v5, p1, :cond_1 */
/* .line 499 */
int v1 = 1; // const/4 v1, 0x1
/* .line 501 */
} // .end local v4 # "processInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
} // :cond_1
/* .line 503 */
} // :cond_2
} // .end method
private void lambda$registerSatelliteCallMode$0 ( java.util.HashSet p0, java.lang.String p1 ) { //synthethic
/* .locals 4 */
/* .param p1, "pkgSet" # Ljava/util/HashSet; */
/* .param p2, "key" # Ljava/lang/String; */
/* .line 228 */
/* const-class v0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl; */
/* monitor-enter v0 */
/* .line 230 */
try { // :try_start_0
final String v1 = "GnssBackgroundUsageOpt"; // const-string v1, "GnssBackgroundUsageOpt"
final String v2 = "=================registerSatelliteCallMode, Start ================="; // const-string v2, "=================registerSatelliteCallMode, Start ================="
android.util.Log .d ( v1,v2 );
/* .line 231 */
v1 = this.mIsInSatelliteCallMode;
v1 = (( java.util.concurrent.atomic.AtomicBoolean ) v1 ).get ( ); // invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 232 */
final String v1 = "GnssBackgroundUsageOpt"; // const-string v1, "GnssBackgroundUsageOpt"
final String v2 = "is in Satellite Call Mode do not need register..."; // const-string v2, "is in Satellite Call Mode do not need register..."
android.util.Log .d ( v1,v2 );
/* .line 233 */
/* monitor-exit v0 */
return;
/* .line 235 */
} // :cond_0
final String v1 = "GnssBackgroundUsageOpt"; // const-string v1, "GnssBackgroundUsageOpt"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "removeAndBlockAllRequestExPkg\uff0cpkg:"; // const-string v3, "removeAndBlockAllRequestExPkg\uff0cpkg:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.util.HashSet ) p1 ).toString ( ); // invoke-virtual {p1}, Ljava/util/HashSet;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = ",key:"; // const-string v3, ",key:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v1,v2 );
/* .line 236 */
v1 = this.KEY_API_USE;
/* if-eq v1, p2, :cond_1 */
/* .line 237 */
final String v1 = "GnssBackgroundUsageOpt"; // const-string v1, "GnssBackgroundUsageOpt"
final String v2 = "removeAndBlockAllRequestExPkg key is invalid..."; // const-string v2, "removeAndBlockAllRequestExPkg key is invalid..."
android.util.Log .e ( v1,v2 );
/* .line 238 */
/* monitor-exit v0 */
return;
/* .line 241 */
} // :cond_1
v1 = this.mIsSpecifiedDevice;
int v2 = 0; // const/4 v2, 0x0
(( java.util.concurrent.atomic.AtomicBoolean ) v1 ).set ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
/* .line 243 */
v1 = this.mIsInSatelliteCallMode;
int v2 = 1; // const/4 v2, 0x1
(( java.util.concurrent.atomic.AtomicBoolean ) v1 ).set ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
/* .line 245 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v1 */
/* iput-wide v1, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mScStartTime:J */
/* .line 246 */
/* invoke-direct {p0, p1}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->removeCurrentAllRequestExPkgSet(Ljava/util/HashSet;)V */
/* .line 247 */
/* monitor-exit v0 */
/* .line 248 */
return;
/* .line 247 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void lambda$remove$2 ( java.lang.String p0, Integer p1, java.lang.String p2 ) { //synthethic
/* .locals 5 */
/* .param p1, "key" # Ljava/lang/String; */
/* .param p2, "delayTime" # I */
/* .param p3, "providerName" # Ljava/lang/String; */
/* .line 315 */
final String v0 = " delay:"; // const-string v0, " delay:"
v1 = this.mRequestMap;
/* check-cast v1, Lcom/android/server/location/gnss/exp/GnssRequestBean; */
/* .line 316 */
/* .local v1, "bean":Lcom/android/server/location/gnss/exp/GnssRequestBean; */
final String v2 = "GnssBackgroundUsageOpt"; // const-string v2, "GnssBackgroundUsageOpt"
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 319 */
try { // :try_start_0
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "remove key sleep before: "; // const-string v4, "remove key sleep before: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p2 ); // invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v3 );
/* .line 320 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 321 */
/* int-to-long v3, p2 */
java.lang.Thread .sleep ( v3,v4 );
/* .line 322 */
} // :cond_0
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "remove key sleep after: "; // const-string v4, "remove key sleep after: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v0 );
/* .line 323 */
v0 = this.identity;
v0 = (( android.location.util.identity.CallerIdentity ) v0 ).getUid ( ); // invoke-virtual {v0}, Landroid/location/util/identity/CallerIdentity;->getUid()I
v3 = this.identity;
v3 = (( android.location.util.identity.CallerIdentity ) v3 ).getPid ( ); // invoke-virtual {v3}, Landroid/location/util/identity/CallerIdentity;->getPid()I
v0 = /* invoke-direct {p0, v0, v3}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->isForegroundService(II)Z */
/* iput-boolean v0, v1, Lcom/android/server/location/gnss/exp/GnssRequestBean;->isForegroundService:Z */
/* .line 324 */
v0 = this.mIRemoveRequest;
v3 = this.callbackType;
/* .line 325 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, v1, Lcom/android/server/location/gnss/exp/GnssRequestBean;->removeByOpt:Z */
/* .line 326 */
/* invoke-direct {p0, p1, v1, v0}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->putIntoRequestMap(Ljava/lang/String;Lcom/android/server/location/gnss/exp/GnssRequestBean;Z)V */
/* .line 328 */
v0 = this.mIsInSatelliteCallMode;
v0 = (( java.util.concurrent.atomic.AtomicBoolean ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 329 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "remove by GSCO opt, key:"; // const-string v3, "remove by GSCO opt, key:"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v0 );
/* .line 331 */
com.android.server.location.gnss.GnssEventTrackingStub .getInstance ( );
/* .line 333 */
} // :cond_1
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "remove by GBO opt, key:"; // const-string v3, "remove by GBO opt, key:"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v0 );
/* .line 334 */
com.android.server.location.gnss.GnssEventTrackingStub .getInstance ( );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 336 */
/* :catch_0 */
/* move-exception v0 */
/* .line 337 */
/* .local v0, "e":Ljava/lang/Exception; */
/* instance-of v3, v0, Ljava/lang/InterruptedException; */
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 338 */
final String v3 = "current remove thread has been interrupted..."; // const-string v3, "current remove thread has been interrupted..."
android.util.Log .e ( v2,v3 );
/* .line 339 */
v2 = this.mRemoveThreadMap;
/* .line 340 */
return;
/* .line 342 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_2
} // :goto_0
/* .line 345 */
} // :cond_3
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "remove by opt interrupt, key:"; // const-string v3, "remove by opt interrupt, key:"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v0 );
/* .line 347 */
} // :goto_1
v0 = this.mRemoveThreadMap;
/* .line 348 */
return;
} // .end method
private void lambda$unRegisterSatelliteCallMode$1 ( ) { //synthethic
/* .locals 7 */
/* .line 254 */
/* const-class v0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl; */
/* monitor-enter v0 */
/* .line 256 */
try { // :try_start_0
v1 = this.mIsInSatelliteCallMode;
v1 = (( java.util.concurrent.atomic.AtomicBoolean ) v1 ).get ( ); // invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
/* if-nez v1, :cond_0 */
/* .line 257 */
final String v1 = "GnssBackgroundUsageOpt"; // const-string v1, "GnssBackgroundUsageOpt"
final String v2 = "is not in Satellite Call Mode do not need unRegister..."; // const-string v2, "is not in Satellite Call Mode do not need unRegister..."
android.util.Log .d ( v1,v2 );
/* .line 258 */
/* monitor-exit v0 */
return;
/* .line 260 */
} // :cond_0
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mIsBackFromGsco:Z */
/* .line 262 */
/* invoke-direct {p0}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->loadFeatureSwitch()V */
/* .line 264 */
v1 = this.mIsInSatelliteCallMode;
int v2 = 0; // const/4 v2, 0x0
(( java.util.concurrent.atomic.AtomicBoolean ) v1 ).set ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
/* .line 266 */
/* iget-wide v3, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mScStartTime:J */
/* const-wide/16 v5, 0x0 */
/* cmp-long v1, v3, v5 */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 268 */
com.android.server.location.gnss.GnssEventTrackingStub .getInstance ( );
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v3 */
/* iget-wide v5, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mScStartTime:J */
/* sub-long/2addr v3, v5 */
/* .line 271 */
} // :cond_1
/* invoke-direct {p0}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->restoreGsco()V */
/* .line 272 */
/* iput-boolean v2, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mIsBackFromGsco:Z */
/* .line 274 */
final String v1 = "GnssBackgroundUsageOpt"; // const-string v1, "GnssBackgroundUsageOpt"
final String v2 = "=================unRegisterSatelliteCallMode, End ================="; // const-string v2, "=================unRegisterSatelliteCallMode, End ================="
android.util.Log .d ( v1,v2 );
/* .line 275 */
/* monitor-exit v0 */
/* .line 276 */
return;
/* .line 275 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private Boolean loadCloudDataFromSP ( ) {
/* .locals 5 */
/* .line 440 */
final String v0 = "load mIsSpecifiedDevice running..."; // const-string v0, "load mIsSpecifiedDevice running..."
final String v1 = "GnssBackgroundUsageOpt"; // const-string v1, "GnssBackgroundUsageOpt"
android.util.Log .d ( v1,v0 );
/* .line 442 */
try { // :try_start_0
v0 = com.android.server.location.gnss.exp.GnssBackgroundUsageOptImpl.mContext;
/* if-nez v0, :cond_0 */
/* .line 443 */
/* iget-boolean v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mDefaultFeatureStatus:Z */
/* .line 445 */
} // :cond_0
(( android.content.Context ) v0 ).createDeviceProtectedStorageContext ( ); // invoke-virtual {v0}, Landroid/content/Context;->createDeviceProtectedStorageContext()Landroid/content/Context;
/* .line 446 */
/* .local v0, "directBootContext":Landroid/content/Context; */
v2 = this.mCloudSpFile;
int v3 = 0; // const/4 v3, 0x0
(( android.content.Context ) v0 ).getSharedPreferences ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/io/File;I)Landroid/content/SharedPreferences;
/* .line 447 */
/* .local v2, "editor":Landroid/content/SharedPreferences; */
final String v3 = "mIsSpecifiedDevice"; // const-string v3, "mIsSpecifiedDevice"
v3 = /* iget-boolean v4, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mDefaultFeatureStatus:Z */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* move v0, v3 */
/* .line 451 */
} // .end local v2 # "editor":Landroid/content/SharedPreferences;
/* .local v0, "status":Z */
/* nop */
/* .line 452 */
int v2 = 1; // const/4 v2, 0x1
/* iput-boolean v2, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mAlreadyLoadDataFromSP:Z */
/* .line 453 */
final String v2 = "Success to load mIsSpecifiedDevice..."; // const-string v2, "Success to load mIsSpecifiedDevice..."
android.util.Log .d ( v1,v2 );
/* .line 454 */
/* .line 448 */
} // .end local v0 # "status":Z
/* :catch_0 */
/* move-exception v0 */
/* .line 449 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Failed to load mIsSpecifiedDevice..., "; // const-string v3, "Failed to load mIsSpecifiedDevice..., "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v2 );
/* .line 450 */
/* iget-boolean v1, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mDefaultFeatureStatus:Z */
} // .end method
private void loadFeatureSwitch ( ) {
/* .locals 3 */
/* .line 472 */
v0 = this.mIsSpecifiedDevice;
v1 = /* invoke-direct {p0}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->loadCloudDataFromSP()Z */
(( java.util.concurrent.atomic.AtomicBoolean ) v0 ).set ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
/* .line 473 */
/* iget-boolean v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mAlreadyLoadDataFromSP:Z */
final String v1 = "GnssBackgroundUsageOpt"; // const-string v1, "GnssBackgroundUsageOpt"
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mIsSpecifiedDevice;
v0 = (( java.util.concurrent.atomic.AtomicBoolean ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 474 */
/* invoke-direct {p0}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->registerProcessObserver()V */
/* .line 475 */
final String v0 = "Has Register Process Observer..."; // const-string v0, "Has Register Process Observer..."
android.util.Log .d ( v1,v0 );
/* .line 477 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Is Specified Device:"; // const-string v2, "Is Specified Device:"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mIsSpecifiedDevice;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v0 );
/* .line 478 */
return;
} // .end method
private com.android.server.location.gnss.exp.GnssRequestBean putGnssRequestBean ( android.location.util.identity.CallerIdentity p0, java.lang.String p1, android.location.LocationRequest p2, Boolean p3, Integer p4, java.lang.Object p5 ) {
/* .locals 3 */
/* .param p1, "identity" # Landroid/location/util/identity/CallerIdentity; */
/* .param p2, "provider" # Ljava/lang/String; */
/* .param p3, "locationRequest" # Landroid/location/LocationRequest; */
/* .param p4, "foreground" # Z */
/* .param p5, "permissionLevel" # I */
/* .param p6, "callbackType" # Ljava/lang/Object; */
/* .line 509 */
/* new-instance v0, Lcom/android/server/location/gnss/exp/GnssRequestBean; */
/* invoke-direct {v0}, Lcom/android/server/location/gnss/exp/GnssRequestBean;-><init>()V */
/* .line 510 */
/* .local v0, "requestBean":Lcom/android/server/location/gnss/exp/GnssRequestBean; */
this.identity = p1;
/* .line 511 */
this.callbackType = p6;
/* .line 512 */
this.provider = p2;
/* .line 513 */
int v1 = 0; // const/4 v1, 0x0
/* iput-boolean v1, v0, Lcom/android/server/location/gnss/exp/GnssRequestBean;->removeByOpt:Z */
/* .line 514 */
this.locationRequest = p3;
/* .line 515 */
/* iput p5, v0, Lcom/android/server/location/gnss/exp/GnssRequestBean;->permissionLevel:I */
/* .line 516 */
v1 = (( android.location.util.identity.CallerIdentity ) p1 ).getUid ( ); // invoke-virtual {p1}, Landroid/location/util/identity/CallerIdentity;->getUid()I
v2 = (( android.location.util.identity.CallerIdentity ) p1 ).getPid ( ); // invoke-virtual {p1}, Landroid/location/util/identity/CallerIdentity;->getPid()I
v1 = /* invoke-direct {p0, v1, v2}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->isForegroundService(II)Z */
/* iput-boolean v1, v0, Lcom/android/server/location/gnss/exp/GnssRequestBean;->isForegroundService:Z */
/* .line 517 */
/* iput-boolean p4, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mOldForeground:Z */
/* .line 518 */
} // .end method
private void putIntoRequestMap ( java.lang.String p0, com.android.server.location.gnss.exp.GnssRequestBean p1, Boolean p2 ) {
/* .locals 4 */
/* .param p1, "key" # Ljava/lang/String; */
/* .param p2, "value" # Lcom/android/server/location/gnss/exp/GnssRequestBean; */
/* .param p3, "isModify" # Z */
/* .line 596 */
if ( p3 != null) { // if-eqz p3, :cond_0
/* .line 597 */
v0 = this.mRequestMap;
/* .line 598 */
return;
/* .line 601 */
} // :cond_0
v0 = this.mRequestMap;
/* check-cast v0, Lcom/android/server/location/gnss/exp/GnssRequestBean; */
/* .line 602 */
/* .local v0, "bean":Lcom/android/server/location/gnss/exp/GnssRequestBean; */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 603 */
v1 = this.callbackType;
/* instance-of v1, v1, Landroid/location/ILocationListener; */
if ( v1 != null) { // if-eqz v1, :cond_1
v1 = this.callbackType;
/* check-cast v1, Landroid/location/ILocationListener; */
/* .line 604 */
v2 = this.callbackType;
/* check-cast v2, Landroid/location/ILocationListener; */
/* if-eq v1, v2, :cond_2 */
} // :cond_1
v1 = this.callbackType;
v2 = this.callbackType;
/* .line 605 */
v1 = (( java.lang.Object ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 606 */
} // :cond_2
v1 = this.mRequestMap;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "-dupReq"; // const-string v3, "-dupReq"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 607 */
return;
/* .line 610 */
} // :cond_3
v1 = this.mRequestMap;
/* .line 611 */
return;
} // .end method
private void registerProcessObserver ( ) {
/* .locals 3 */
/* .line 368 */
/* new-instance v0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl$1;-><init>(Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;)V */
this.mIProcessObserverStub = v0;
/* .line 405 */
try { // :try_start_0
android.app.ActivityManager .getService ( );
v1 = this.mIProcessObserverStub;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 408 */
/* .line 406 */
/* :catch_0 */
/* move-exception v0 */
/* .line 407 */
/* .local v0, "e":Landroid/os/RemoteException; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "ActivityManager registerProcessObserver RemoteException...:"; // const-string v2, "ActivityManager registerProcessObserver RemoteException...:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "GnssBackgroundUsageOpt"; // const-string v2, "GnssBackgroundUsageOpt"
android.util.Log .e ( v2,v1 );
/* .line 409 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :goto_0
return;
} // .end method
private void remove ( java.lang.String p0, java.lang.String p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "providerName" # Ljava/lang/String; */
/* .param p2, "key" # Ljava/lang/String; */
/* .param p3, "delayTime" # I */
/* .line 312 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "remove key: "; // const-string v1, "remove key: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "GnssBackgroundUsageOpt"; // const-string v1, "GnssBackgroundUsageOpt"
android.util.Log .d ( v1,v0 );
/* .line 313 */
v0 = this.mRemoveThreadMap;
/* if-nez v0, :cond_0 */
/* .line 314 */
/* new-instance v0, Ljava/lang/Thread; */
/* new-instance v1, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl$$ExternalSyntheticLambda1; */
/* invoke-direct {v1, p0, p2, p3, p1}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;Ljava/lang/String;ILjava/lang/String;)V */
/* invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V */
/* .line 349 */
/* .local v0, "thread":Ljava/lang/Thread; */
(( java.lang.Thread ) v0 ).start ( ); // invoke-virtual {v0}, Ljava/lang/Thread;->start()V
/* .line 350 */
v1 = this.mRemoveThreadMap;
/* .line 352 */
} // .end local v0 # "thread":Ljava/lang/Thread;
} // :cond_0
return;
} // .end method
private void removeCurrentAllRequestExPkgByPkgSet ( java.util.HashSet p0 ) {
/* .locals 6 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/HashSet<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 554 */
/* .local p1, "pkgSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;" */
/* if-nez p1, :cond_0 */
return;
/* .line 556 */
} // :cond_0
/* new-instance v0, Ljava/util/HashMap; */
v1 = this.mRequestMap;
/* invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V */
/* .line 557 */
/* .local v0, "reqMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/location/gnss/exp/GnssRequestBean;>;" */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Ljava/util/Map$Entry; */
/* .line 558 */
/* .local v2, "map":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/location/gnss/exp/GnssRequestBean;>;" */
/* check-cast v3, Lcom/android/server/location/gnss/exp/GnssRequestBean; */
v3 = this.identity;
(( android.location.util.identity.CallerIdentity ) v3 ).getPackageName ( ); // invoke-virtual {v3}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;
v3 = (( java.util.HashSet ) p1 ).contains ( v3 ); // invoke-virtual {p1, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
/* if-nez v3, :cond_1 */
/* .line 559 */
/* check-cast v3, Lcom/android/server/location/gnss/exp/GnssRequestBean; */
v3 = this.provider;
/* check-cast v4, Ljava/lang/String; */
int v5 = 0; // const/4 v5, 0x0
/* invoke-direct {p0, v3, v4, v5}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->remove(Ljava/lang/String;Ljava/lang/String;I)V */
/* .line 560 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "removeCurrentAllRequestExPkgByPkg, uid:"; // const-string v4, "removeCurrentAllRequestExPkgByPkg, uid:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* check-cast v4, Lcom/android/server/location/gnss/exp/GnssRequestBean; */
v4 = this.identity;
v4 = (( android.location.util.identity.CallerIdentity ) v4 ).getUid ( ); // invoke-virtual {v4}, Landroid/location/util/identity/CallerIdentity;->getUid()I
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = ", pkg:"; // const-string v4, ", pkg:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 561 */
/* check-cast v4, Lcom/android/server/location/gnss/exp/GnssRequestBean; */
v4 = this.identity;
(( android.location.util.identity.CallerIdentity ) v4 ).getPackageName ( ); // invoke-virtual {v4}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = "\nNow :"; // const-string v4, "\nNow :"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* check-cast v4, Lcom/android/server/location/gnss/exp/GnssRequestBean; */
v4 = this.locationRequest;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 560 */
final String v4 = "GnssBackgroundUsageOpt"; // const-string v4, "GnssBackgroundUsageOpt"
android.util.Log .i ( v4,v3 );
/* .line 563 */
} // .end local v2 # "map":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/location/gnss/exp/GnssRequestBean;>;"
} // :cond_1
/* .line 564 */
} // :cond_2
return;
} // .end method
private void removeCurrentAllRequestExPkgByUidSet ( java.util.HashSet p0 ) {
/* .locals 6 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/HashSet<", */
/* "Ljava/lang/Integer;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 567 */
/* .local p1, "uidSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;" */
/* if-nez p1, :cond_0 */
return;
/* .line 569 */
} // :cond_0
/* new-instance v0, Ljava/util/HashMap; */
v1 = this.mRequestMap;
/* invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V */
/* .line 570 */
/* .local v0, "reqMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/location/gnss/exp/GnssRequestBean;>;" */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Ljava/util/Map$Entry; */
/* .line 571 */
/* .local v2, "map":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/location/gnss/exp/GnssRequestBean;>;" */
/* check-cast v3, Lcom/android/server/location/gnss/exp/GnssRequestBean; */
v3 = this.identity;
v3 = (( android.location.util.identity.CallerIdentity ) v3 ).getUid ( ); // invoke-virtual {v3}, Landroid/location/util/identity/CallerIdentity;->getUid()I
java.lang.Integer .valueOf ( v3 );
v3 = (( java.util.HashSet ) p1 ).contains ( v3 ); // invoke-virtual {p1, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
/* if-nez v3, :cond_1 */
/* .line 573 */
/* check-cast v3, Lcom/android/server/location/gnss/exp/GnssRequestBean; */
v3 = this.provider;
/* check-cast v4, Ljava/lang/String; */
int v5 = 0; // const/4 v5, 0x0
/* invoke-direct {p0, v3, v4, v5}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->remove(Ljava/lang/String;Ljava/lang/String;I)V */
/* .line 574 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "removeCurrentAllRequestExPkgByUid, key:"; // const-string v4, "removeCurrentAllRequestExPkgByUid, key:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* check-cast v4, Ljava/lang/String; */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ", pkg:"; // const-string v4, ", pkg:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 575 */
/* check-cast v4, Lcom/android/server/location/gnss/exp/GnssRequestBean; */
v4 = this.identity;
(( android.location.util.identity.CallerIdentity ) v4 ).getPackageName ( ); // invoke-virtual {v4}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = "\n Now mRequestMap:"; // const-string v4, "\n Now mRequestMap:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* check-cast v4, Lcom/android/server/location/gnss/exp/GnssRequestBean; */
v4 = this.locationRequest;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 574 */
final String v4 = "GnssBackgroundUsageOpt"; // const-string v4, "GnssBackgroundUsageOpt"
android.util.Log .i ( v4,v3 );
/* .line 577 */
} // .end local v2 # "map":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/location/gnss/exp/GnssRequestBean;>;"
} // :cond_1
/* .line 578 */
} // :cond_2
return;
} // .end method
private void removeCurrentAllRequestExPkgSet ( java.util.HashSet p0 ) {
/* .locals 7 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/HashSet<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 522 */
/* .local p1, "pkgSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;" */
final String v0 = "GnssBackgroundUsageOpt"; // const-string v0, "GnssBackgroundUsageOpt"
if ( p1 != null) { // if-eqz p1, :cond_5
v1 = (( java.util.HashSet ) p1 ).isEmpty ( ); // invoke-virtual {p1}, Ljava/util/HashSet;->isEmpty()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 526 */
} // :cond_0
v1 = this.mSatelliteCallAppUidSet;
(( java.util.HashSet ) v1 ).clear ( ); // invoke-virtual {v1}, Ljava/util/HashSet;->clear()V
/* .line 527 */
this.mSatelliteCallPkgSet = p1;
/* .line 528 */
int v1 = 0; // const/4 v1, 0x0
/* .line 529 */
/* .local v1, "isException":Z */
(( java.util.HashSet ) p1 ).iterator ( ); // invoke-virtual {p1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;
v3 = } // :goto_0
int v4 = 0; // const/4 v4, 0x0
if ( v3 != null) { // if-eqz v3, :cond_2
/* check-cast v3, Ljava/lang/String; */
/* .line 531 */
/* .local v3, "p":Ljava/lang/String; */
try { // :try_start_0
v5 = com.android.server.location.gnss.exp.GnssBackgroundUsageOptImpl.mContext;
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 532 */
v6 = this.mSatelliteCallAppUidSet;
(( android.content.Context ) v5 ).getPackageManager ( ); // invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
v4 = (( android.content.pm.PackageManager ) v5 ).getPackageUid ( v3, v4 ); // invoke-virtual {v5, v3, v4}, Landroid/content/pm/PackageManager;->getPackageUid(Ljava/lang/String;I)I
java.lang.Integer .valueOf ( v4 );
(( java.util.HashSet ) v6 ).add ( v4 ); // invoke-virtual {v6, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 537 */
} // :cond_1
/* .line 534 */
/* :catch_0 */
/* move-exception v4 */
/* .line 535 */
/* .local v4, "e":Ljava/lang/Exception; */
int v1 = 1; // const/4 v1, 0x1
/* .line 536 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "getPackageManager Exception:"; // const-string v6, "getPackageManager Exception:"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v0,v5 );
/* .line 538 */
} // .end local v3 # "p":Ljava/lang/String;
} // .end local v4 # "e":Ljava/lang/Exception;
} // :goto_1
/* .line 539 */
} // :cond_2
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 541 */
/* iput-boolean v4, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mIsUseUidCtl:Z */
/* .line 542 */
/* invoke-direct {p0, p1}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->removeCurrentAllRequestExPkgByPkgSet(Ljava/util/HashSet;)V */
/* .line 545 */
} // :cond_3
v0 = this.mSatelliteCallAppUidSet;
v0 = (( java.util.HashSet ) v0 ).isEmpty ( ); // invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z
/* if-nez v0, :cond_4 */
/* .line 547 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mIsUseUidCtl:Z */
/* .line 548 */
v0 = this.mSatelliteCallAppUidSet;
/* invoke-direct {p0, v0}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->removeCurrentAllRequestExPkgByUidSet(Ljava/util/HashSet;)V */
/* .line 551 */
} // :cond_4
} // :goto_2
return;
/* .line 523 */
} // .end local v1 # "isException":Z
} // :cond_5
} // :goto_3
final String v1 = "removeCurrentAllRequestExPkg pkg is invalid..."; // const-string v1, "removeCurrentAllRequestExPkg pkg is invalid..."
android.util.Log .e ( v0,v1 );
/* .line 524 */
return;
} // .end method
private void restore ( java.lang.String p0, java.lang.String p1, android.location.LocationRequest p2, android.location.util.identity.CallerIdentity p3, Integer p4, java.lang.Object p5 ) {
/* .locals 8 */
/* .param p1, "key" # Ljava/lang/String; */
/* .param p2, "providerName" # Ljava/lang/String; */
/* .param p3, "request" # Landroid/location/LocationRequest; */
/* .param p4, "identity" # Landroid/location/util/identity/CallerIdentity; */
/* .param p5, "permissionLevel" # I */
/* .param p6, "callbackType" # Ljava/lang/Object; */
/* .line 357 */
final String v0 = "GnssBackgroundUsageOpt"; // const-string v0, "GnssBackgroundUsageOpt"
try { // :try_start_0
v1 = this.mRequestMap;
/* .line 358 */
v2 = this.mIRestoreRequest;
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 359 */
/* move-object v3, p2 */
/* move-object v4, p3 */
/* move-object v5, p4 */
/* move v6, p5 */
/* move-object v7, p6 */
/* invoke-interface/range {v2 ..v7}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub$IRestoreRequest;->onRestore(Ljava/lang/String;Landroid/location/LocationRequest;Landroid/location/util/identity/CallerIdentity;ILjava/lang/Object;)V */
/* .line 361 */
} // :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "restore by opt,uid:"; // const-string v2, "restore by opt,uid:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = (( android.location.util.identity.CallerIdentity ) p4 ).getUid ( ); // invoke-virtual {p4}, Landroid/location/util/identity/CallerIdentity;->getUid()I
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " pkg:"; // const-string v2, " pkg:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.location.util.identity.CallerIdentity ) p4 ).getPackageName ( ); // invoke-virtual {p4}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v1 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 364 */
/* .line 362 */
/* :catch_0 */
/* move-exception v1 */
/* .line 363 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "restore exception-->"; // const-string v3, "restore exception-->"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v0,v2 );
/* .line 365 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void restoreGsco ( ) {
/* .locals 12 */
/* .line 619 */
/* new-instance v0, Ljava/util/HashMap; */
v1 = this.mRequestMap;
/* invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V */
/* .line 620 */
/* .local v0, "reqMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/location/gnss/exp/GnssRequestBean;>;" */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_3
/* check-cast v2, Ljava/util/Map$Entry; */
/* .line 621 */
/* .local v2, "map":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/location/gnss/exp/GnssRequestBean;>;" */
/* check-cast v3, Lcom/android/server/location/gnss/exp/GnssRequestBean; */
/* .line 622 */
/* .local v3, "bean":Lcom/android/server/location/gnss/exp/GnssRequestBean; */
/* if-nez v3, :cond_0 */
/* .line 623 */
/* .line 626 */
} // :cond_0
/* iget-boolean v4, v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;->isForegroundService:Z */
final String v5 = "key:"; // const-string v5, "key:"
final String v6 = " pkg:"; // const-string v6, " pkg:"
final String v7 = "GnssBackgroundUsageOpt"; // const-string v7, "GnssBackgroundUsageOpt"
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 627 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* check-cast v5, Ljava/lang/String; */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.identity;
(( android.location.util.identity.CallerIdentity ) v5 ).getPackageName ( ); // invoke-virtual {v5}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " is isForegroundService"; // const-string v5, " is isForegroundService"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v7,v4 );
/* .line 628 */
/* move-object v6, v4 */
/* check-cast v6, Ljava/lang/String; */
v7 = this.provider;
v8 = this.locationRequest;
v9 = this.identity;
/* iget v10, v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;->permissionLevel:I */
v11 = this.callbackType;
/* move-object v5, p0 */
/* invoke-direct/range {v5 ..v11}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->restore(Ljava/lang/String;Ljava/lang/String;Landroid/location/LocationRequest;Landroid/location/util/identity/CallerIdentity;ILjava/lang/Object;)V */
/* goto/16 :goto_1 */
/* .line 630 */
} // :cond_1
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* check-cast v5, Ljava/lang/String; */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.identity;
(( android.location.util.identity.CallerIdentity ) v5 ).getPackageName ( ); // invoke-virtual {v5}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " is not isForegroundService Foreground:"; // const-string v5, " is not isForegroundService Foreground:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.mUidForMap;
v8 = this.identity;
v8 = (( android.location.util.identity.CallerIdentity ) v8 ).getUid ( ); // invoke-virtual {v8}, Landroid/location/util/identity/CallerIdentity;->getUid()I
java.lang.Integer .valueOf ( v8 );
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v5 = " mUidForMap.containsKey:"; // const-string v5, " mUidForMap.containsKey:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.mUidForMap;
v8 = this.identity;
v8 = (( android.location.util.identity.CallerIdentity ) v8 ).getUid ( ); // invoke-virtual {v8}, Landroid/location/util/identity/CallerIdentity;->getUid()I
v5 = java.lang.Integer .valueOf ( v8 );
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v7,v4 );
/* .line 631 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "!mSatelliteCallAppUidSet.contains(bean.identity.getUid()):"; // const-string v5, "!mSatelliteCallAppUidSet.contains(bean.identity.getUid()):"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.mSatelliteCallAppUidSet;
v8 = this.identity;
v8 = (( android.location.util.identity.CallerIdentity ) v8 ).getUid ( ); // invoke-virtual {v8}, Landroid/location/util/identity/CallerIdentity;->getUid()I
java.lang.Integer .valueOf ( v8 );
v5 = (( java.util.HashSet ) v5 ).contains ( v8 ); // invoke-virtual {v5, v8}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
/* xor-int/lit8 v5, v5, 0x1 */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v5 = " bean.identity.getUid():"; // const-string v5, " bean.identity.getUid():"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.identity;
/* .line 632 */
v5 = (( android.location.util.identity.CallerIdentity ) v5 ).getUid ( ); // invoke-virtual {v5}, Landroid/location/util/identity/CallerIdentity;->getUid()I
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 631 */
android.util.Log .d ( v7,v4 );
/* .line 635 */
v4 = this.mSatelliteCallAppUidSet;
v5 = this.identity;
v5 = (( android.location.util.identity.CallerIdentity ) v5 ).getUid ( ); // invoke-virtual {v5}, Landroid/location/util/identity/CallerIdentity;->getUid()I
java.lang.Integer .valueOf ( v5 );
v4 = (( java.util.HashSet ) v4 ).contains ( v5 ); // invoke-virtual {v4, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
/* if-nez v4, :cond_2 */
v4 = this.mUidForMap;
v5 = this.identity;
/* .line 636 */
v5 = (( android.location.util.identity.CallerIdentity ) v5 ).getUid ( ); // invoke-virtual {v5}, Landroid/location/util/identity/CallerIdentity;->getUid()I
v4 = java.lang.Integer .valueOf ( v5 );
if ( v4 != null) { // if-eqz v4, :cond_2
v4 = this.mUidForMap;
v5 = this.identity;
/* .line 637 */
v5 = (( android.location.util.identity.CallerIdentity ) v5 ).getUid ( ); // invoke-virtual {v5}, Landroid/location/util/identity/CallerIdentity;->getUid()I
java.lang.Integer .valueOf ( v5 );
/* check-cast v4, Ljava/lang/Boolean; */
v4 = (( java.lang.Boolean ) v4 ).booleanValue ( ); // invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 639 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "change to foreground remove by GSCO opt and now restore key:"; // const-string v5, "change to foreground remove by GSCO opt and now restore key:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 640 */
/* check-cast v5, Ljava/lang/String; */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.identity;
(( android.location.util.identity.CallerIdentity ) v5 ).getPackageName ( ); // invoke-virtual {v5}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " isForegroundService:"; // const-string v5, " isForegroundService:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v5, v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;->isForegroundService:Z */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 639 */
android.util.Log .d ( v7,v4 );
/* .line 641 */
/* move-object v6, v4 */
/* check-cast v6, Ljava/lang/String; */
v7 = this.provider;
v8 = this.locationRequest;
v9 = this.identity;
/* iget v10, v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;->permissionLevel:I */
v11 = this.callbackType;
/* move-object v5, p0 */
/* invoke-direct/range {v5 ..v11}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->restore(Ljava/lang/String;Ljava/lang/String;Landroid/location/LocationRequest;Landroid/location/util/identity/CallerIdentity;ILjava/lang/Object;)V */
/* .line 644 */
} // .end local v2 # "map":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/location/gnss/exp/GnssRequestBean;>;"
} // .end local v3 # "bean":Lcom/android/server/location/gnss/exp/GnssRequestBean;
} // :cond_2
} // :goto_1
/* goto/16 :goto_0 */
/* .line 645 */
} // :cond_3
return;
} // .end method
private void saveCloudDataToSP ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "status" # Z */
/* .line 423 */
final String v0 = "Save mIsSpecifiedDevice running..."; // const-string v0, "Save mIsSpecifiedDevice running..."
final String v1 = "GnssBackgroundUsageOpt"; // const-string v1, "GnssBackgroundUsageOpt"
android.util.Log .d ( v1,v0 );
/* .line 425 */
try { // :try_start_0
v0 = com.android.server.location.gnss.exp.GnssBackgroundUsageOptImpl.mContext;
/* if-nez v0, :cond_0 */
/* .line 426 */
return;
/* .line 428 */
} // :cond_0
v2 = this.mCloudSpFile;
int v3 = 0; // const/4 v3, 0x0
(( android.content.Context ) v0 ).getSharedPreferences ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/io/File;I)Landroid/content/SharedPreferences;
/* .line 429 */
/* .local v0, "editor":Landroid/content/SharedPreferences$Editor; */
final String v2 = "mIsSpecifiedDevice"; // const-string v2, "mIsSpecifiedDevice"
/* .line 430 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 434 */
} // .end local v0 # "editor":Landroid/content/SharedPreferences$Editor;
/* nop */
/* .line 435 */
final String v0 = "Success to save mIsSpecifiedDevice..."; // const-string v0, "Success to save mIsSpecifiedDevice..."
android.util.Log .d ( v1,v0 );
/* .line 436 */
return;
/* .line 431 */
/* :catch_0 */
/* move-exception v0 */
/* .line 432 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Failed to save mIsSpecifiedDevice..., "; // const-string v3, "Failed to save mIsSpecifiedDevice..., "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v2 );
/* .line 433 */
return;
} // .end method
private void unRegisterProcessObserver ( ) {
/* .locals 4 */
/* .line 412 */
final String v0 = "GnssBackgroundUsageOpt"; // const-string v0, "GnssBackgroundUsageOpt"
v1 = this.mIProcessObserverStub;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 414 */
try { // :try_start_0
android.app.ActivityManager .getService ( );
v2 = this.mIProcessObserverStub;
/* .line 415 */
/* const-string/jumbo v1, "unRegisterProcessObserver..." */
android.util.Log .d ( v0,v1 );
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 418 */
/* .line 416 */
/* :catch_0 */
/* move-exception v1 */
/* .line 417 */
/* .local v1, "e":Landroid/os/RemoteException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "ActivityManager unRegisterProcessObserver RemoteException...:"; // const-string v3, "ActivityManager unRegisterProcessObserver RemoteException...:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v0,v2 );
/* .line 420 */
} // .end local v1 # "e":Landroid/os/RemoteException;
} // :cond_0
} // :goto_0
return;
} // .end method
/* # virtual methods */
public Boolean getSatelliteCallMode ( ) {
/* .locals 1 */
/* .line 308 */
v0 = this.mIsInSatelliteCallMode;
v0 = (( java.util.concurrent.atomic.AtomicBoolean ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
} // .end method
public void onAppForegroundChanged ( Integer p0, Boolean p1 ) {
/* .locals 13 */
/* .param p1, "uid" # I */
/* .param p2, "foreground" # Z */
/* .line 130 */
/* if-nez p2, :cond_0 */
/* .line 131 */
v0 = this.mBackOpt3Map;
java.lang.Integer .valueOf ( p1 );
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v2 */
java.lang.Long .valueOf ( v2,v3 );
/* .line 133 */
} // :cond_0
v0 = this.mBackOpt3Map;
java.lang.Integer .valueOf ( p1 );
/* .line 135 */
} // :goto_0
/* iget v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mOldUid:I */
/* if-ne v0, p1, :cond_1 */
/* iget-boolean v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mOldForeground:Z */
/* if-ne v0, p2, :cond_1 */
/* .line 136 */
return;
/* .line 138 */
} // :cond_1
v0 = this.mUidForMap;
java.lang.Integer .valueOf ( p1 );
java.lang.Boolean .valueOf ( p2 );
/* .line 139 */
/* iput p1, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mOldUid:I */
/* .line 140 */
/* iput-boolean p2, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mOldForeground:Z */
/* .line 145 */
v0 = this.mIsSpecifiedDevice;
v0 = (( java.util.concurrent.atomic.AtomicBoolean ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
if ( v0 != null) { // if-eqz v0, :cond_9
v0 = this.mIsInSatelliteCallMode;
v0 = (( java.util.concurrent.atomic.AtomicBoolean ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
/* if-nez v0, :cond_9 */
/* iget-boolean v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mIsBackFromGsco:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* goto/16 :goto_3 */
/* .line 149 */
} // :cond_2
/* new-instance v0, Ljava/util/HashMap; */
v1 = this.mRequestMap;
/* invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V */
/* .line 150 */
/* .local v0, "reqMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/location/gnss/exp/GnssRequestBean;>;" */
} // :cond_3
v2 = } // :goto_1
if ( v2 != null) { // if-eqz v2, :cond_8
/* check-cast v2, Ljava/util/Map$Entry; */
/* .line 151 */
/* .local v2, "map":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/location/gnss/exp/GnssRequestBean;>;" */
/* check-cast v3, Lcom/android/server/location/gnss/exp/GnssRequestBean; */
v3 = this.identity;
v3 = (( android.location.util.identity.CallerIdentity ) v3 ).getUid ( ); // invoke-virtual {v3}, Landroid/location/util/identity/CallerIdentity;->getUid()I
/* if-ne p1, v3, :cond_7 */
/* .line 152 */
/* check-cast v3, Lcom/android/server/location/gnss/exp/GnssRequestBean; */
/* .line 153 */
/* .local v3, "bean":Lcom/android/server/location/gnss/exp/GnssRequestBean; */
if ( v3 != null) { // if-eqz v3, :cond_3
final String v4 = "gps"; // const-string v4, "gps"
v5 = this.provider;
v4 = (( java.lang.String ) v4 ).equals ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v4, :cond_4 */
/* .line 154 */
/* .line 157 */
} // :cond_4
v4 = this.mRemoveThreadMap;
/* check-cast v4, Ljava/lang/Thread; */
/* .line 158 */
/* .local v4, "removeThread":Ljava/lang/Thread; */
/* if-nez p2, :cond_5 */
/* .line 159 */
v5 = this.provider;
/* check-cast v6, Ljava/lang/String; */
/* const/16 v7, 0x2710 */
/* invoke-direct {p0, v5, v6, v7}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->remove(Ljava/lang/String;Ljava/lang/String;I)V */
/* .line 160 */
} // :cond_5
/* iget-boolean v5, v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;->removeByOpt:Z */
final String v6 = "GnssBackgroundUsageOpt"; // const-string v6, "GnssBackgroundUsageOpt"
if ( v5 != null) { // if-eqz v5, :cond_6
/* .line 161 */
final String v5 = "change to foreground remove by opt and now restore..."; // const-string v5, "change to foreground remove by opt and now restore..."
android.util.Log .d ( v6,v5 );
/* .line 162 */
/* move-object v7, v5 */
/* check-cast v7, Ljava/lang/String; */
v8 = this.provider;
v9 = this.locationRequest;
v10 = this.identity;
/* iget v11, v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;->permissionLevel:I */
v12 = this.callbackType;
/* move-object v6, p0 */
/* invoke-direct/range {v6 ..v12}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->restore(Ljava/lang/String;Ljava/lang/String;Landroid/location/LocationRequest;Landroid/location/util/identity/CallerIdentity;ILjava/lang/Object;)V */
/* .line 163 */
} // :cond_6
if ( v4 != null) { // if-eqz v4, :cond_7
/* .line 164 */
(( java.lang.Thread ) v4 ).interrupt ( ); // invoke-virtual {v4}, Ljava/lang/Thread;->interrupt()V
/* .line 165 */
final String v5 = "remove Thread not null, interrupt it..."; // const-string v5, "remove Thread not null, interrupt it..."
android.util.Log .d ( v6,v5 );
/* .line 168 */
} // .end local v2 # "map":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/location/gnss/exp/GnssRequestBean;>;"
} // .end local v3 # "bean":Lcom/android/server/location/gnss/exp/GnssRequestBean;
} // .end local v4 # "removeThread":Ljava/lang/Thread;
} // :cond_7
} // :goto_2
/* .line 169 */
} // :cond_8
return;
/* .line 146 */
} // .end local v0 # "reqMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/location/gnss/exp/GnssRequestBean;>;"
} // :cond_9
} // :goto_3
return;
} // .end method
public void registerRequestCallback ( com.android.server.location.gnss.exp.GnssBackgroundUsageOptStub$IRemoveRequest p0 ) {
/* .locals 0 */
/* .param p1, "iRemoveRequest" # Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub$IRemoveRequest; */
/* .line 282 */
this.mIRemoveRequest = p1;
/* .line 283 */
return;
} // .end method
public void registerRestoreCallback ( com.android.server.location.gnss.exp.GnssBackgroundUsageOptStub$IRestoreRequest p0 ) {
/* .locals 0 */
/* .param p1, "iRestoreRequest" # Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub$IRestoreRequest; */
/* .line 287 */
this.mIRestoreRequest = p1;
/* .line 288 */
return;
} // .end method
public void registerSatelliteCallMode ( java.util.HashSet p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p2, "key" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/HashSet<", */
/* "Ljava/lang/String;", */
/* ">;", */
/* "Ljava/lang/String;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 227 */
/* .local p1, "pkgSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;" */
/* new-instance v0, Ljava/lang/Thread; */
/* new-instance v1, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0, p1, p2}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;Ljava/util/HashSet;Ljava/lang/String;)V */
/* invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V */
/* .line 248 */
(( java.lang.Thread ) v0 ).start ( ); // invoke-virtual {v0}, Ljava/lang/Thread;->start()V
/* .line 249 */
return;
} // .end method
public void remove ( Integer p0, Integer p1, java.lang.String p2, java.lang.String p3 ) {
/* .locals 5 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "provider" # Ljava/lang/String; */
/* .param p4, "listenerId" # Ljava/lang/String; */
/* .line 204 */
/* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->createReqKey(IILjava/lang/String;Ljava/lang/String;)Ljava/lang/String; */
/* .line 205 */
/* .local v0, "removeKey":Ljava/lang/String; */
v1 = this.mRequestMap;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "-dupReq"; // const-string v3, "-dupReq"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "GnssBackgroundUsageOpt"; // const-string v2, "GnssBackgroundUsageOpt"
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 207 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 208 */
final String v1 = "same req, return dup first..."; // const-string v1, "same req, return dup first..."
android.util.Log .i ( v2,v1 );
/* .line 210 */
} // :cond_0
v1 = this.mRequestMap;
/* check-cast v1, Lcom/android/server/location/gnss/exp/GnssRequestBean; */
/* .line 211 */
/* .local v1, "bean":Lcom/android/server/location/gnss/exp/GnssRequestBean; */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 212 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "remove normal: key:"; // const-string v4, "remove normal: key:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " "; // const-string v4, " "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.callbackType;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v3 );
/* .line 216 */
} // .end local v1 # "bean":Lcom/android/server/location/gnss/exp/GnssRequestBean;
} // :cond_1
} // :goto_0
v1 = this.mRequestMap;
/* .line 217 */
v1 = this.mRemoveThreadMap;
/* check-cast v1, Ljava/lang/Thread; */
/* .line 218 */
/* .local v1, "thread":Ljava/lang/Thread; */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 220 */
(( java.lang.Thread ) v1 ).interrupt ( ); // invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V
/* .line 222 */
} // :cond_2
return;
} // .end method
public void removeByLmsUser ( Integer p0, Integer p1, java.lang.Object p2 ) {
/* .locals 7 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "callbackType" # Ljava/lang/Object; */
/* .line 178 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "removeByLmsUser"; // const-string v1, "removeByLmsUser"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.Integer .toString ( p1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.Integer .toString ( p2 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "GnssBackgroundUsageOpt"; // const-string v1, "GnssBackgroundUsageOpt"
android.util.Log .d ( v1,v0 );
/* .line 179 */
v0 = this.mIsInSatelliteCallMode;
v0 = (( java.util.concurrent.atomic.AtomicBoolean ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 180 */
/* if-nez p3, :cond_0 */
return;
/* .line 182 */
} // :cond_0
/* new-instance v0, Ljava/util/HashMap; */
v2 = this.mRequestMap;
/* invoke-direct {v0, v2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V */
/* .line 183 */
/* .local v0, "reqMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/location/gnss/exp/GnssRequestBean;>;" */
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_6
/* check-cast v3, Ljava/util/Map$Entry; */
/* .line 184 */
/* .local v3, "map":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/location/gnss/exp/GnssRequestBean;>;" */
/* check-cast v4, Lcom/android/server/location/gnss/exp/GnssRequestBean; */
v4 = this.callbackType;
/* instance-of v4, v4, Landroid/location/ILocationListener; */
if ( v4 != null) { // if-eqz v4, :cond_1
/* instance-of v4, p3, Landroid/location/ILocationListener; */
if ( v4 != null) { // if-eqz v4, :cond_1
/* move-object v4, p3 */
/* check-cast v4, Landroid/location/ILocationListener; */
/* .line 186 */
/* check-cast v5, Lcom/android/server/location/gnss/exp/GnssRequestBean; */
v5 = this.callbackType;
/* check-cast v5, Landroid/location/ILocationListener; */
/* if-eq v4, v5, :cond_2 */
/* .line 187 */
} // :cond_1
/* check-cast v4, Lcom/android/server/location/gnss/exp/GnssRequestBean; */
v4 = this.callbackType;
v4 = (( java.lang.Object ) p3 ).equals ( v4 ); // invoke-virtual {p3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_5
/* .line 188 */
} // :cond_2
/* check-cast v4, Lcom/android/server/location/gnss/exp/GnssRequestBean; */
v4 = this.identity;
v4 = (( android.location.util.identity.CallerIdentity ) v4 ).getUid ( ); // invoke-virtual {v4}, Landroid/location/util/identity/CallerIdentity;->getUid()I
/* if-ne v4, p1, :cond_4 */
/* check-cast v4, Lcom/android/server/location/gnss/exp/GnssRequestBean; */
v4 = this.identity;
v4 = (( android.location.util.identity.CallerIdentity ) v4 ).getPid ( ); // invoke-virtual {v4}, Landroid/location/util/identity/CallerIdentity;->getPid()I
/* if-ne v4, p2, :cond_4 */
/* .line 190 */
v4 = this.mRequestMap;
/* .line 191 */
v4 = this.mRemoveThreadMap;
/* check-cast v4, Ljava/lang/Thread; */
/* .line 192 */
/* .local v4, "thread":Ljava/lang/Thread; */
if ( v4 != null) { // if-eqz v4, :cond_3
(( java.lang.Thread ) v4 ).interrupt ( ); // invoke-virtual {v4}, Ljava/lang/Thread;->interrupt()V
/* .line 193 */
} // :cond_3
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "removeByLmsUser: key:"; // const-string v6, "removeByLmsUser: key:"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* check-cast v6, Ljava/lang/String; */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v5 );
/* .line 194 */
} // .end local v4 # "thread":Ljava/lang/Thread;
/* .line 195 */
} // :cond_4
final String v4 = "callbackType seem but not true uid pid.."; // const-string v4, "callbackType seem but not true uid pid.."
android.util.Log .e ( v1,v4 );
/* .line 198 */
} // .end local v3 # "map":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/location/gnss/exp/GnssRequestBean;>;"
} // :cond_5
} // :goto_1
/* goto/16 :goto_0 */
/* .line 200 */
} // .end local v0 # "reqMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/location/gnss/exp/GnssRequestBean;>;"
} // :cond_6
return;
} // .end method
public Boolean request ( android.content.Context p0, java.lang.String p1, android.location.util.identity.CallerIdentity p2, android.location.LocationRequest p3, Boolean p4, Integer p5, Boolean p6, java.lang.Object p7 ) {
/* .locals 14 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "provider" # Ljava/lang/String; */
/* .param p3, "identity" # Landroid/location/util/identity/CallerIdentity; */
/* .param p4, "locationRequest" # Landroid/location/LocationRequest; */
/* .param p5, "foreground" # Z */
/* .param p6, "permissionLevel" # I */
/* .param p7, "hasLocationPermissions" # Z */
/* .param p8, "callbackType" # Ljava/lang/Object; */
/* .line 76 */
/* move-object v7, p0 */
/* move-object/from16 v8, p2 */
/* .line 77 */
/* iget-boolean v0, v7, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mAlreadyLoadDataFromSP:Z */
/* if-nez v0, :cond_0 */
/* .line 78 */
/* invoke-direct {p0}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->loadFeatureSwitch()V */
/* .line 81 */
} // :cond_0
v0 = /* invoke-virtual/range {p3 ..p3}, Landroid/location/util/identity/CallerIdentity;->getUid()I */
v1 = /* invoke-virtual/range {p3 ..p3}, Landroid/location/util/identity/CallerIdentity;->getPid()I */
/* invoke-virtual/range {p3 ..p3}, Landroid/location/util/identity/CallerIdentity;->getListenerId()Ljava/lang/String; */
/* invoke-direct {p0, v0, v1, v8, v2}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->createReqKey(IILjava/lang/String;Ljava/lang/String;)Ljava/lang/String; */
/* .line 82 */
/* .local v9, "requestKey":Ljava/lang/String; */
/* nop */
/* .line 83 */
/* move-object v0, p0 */
/* move-object/from16 v1, p3 */
/* move-object/from16 v2, p2 */
/* move-object/from16 v3, p4 */
/* move/from16 v4, p5 */
/* move/from16 v5, p6 */
/* move-object/from16 v6, p8 */
/* invoke-direct/range {v0 ..v6}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->putGnssRequestBean(Landroid/location/util/identity/CallerIdentity;Ljava/lang/String;Landroid/location/LocationRequest;ZILjava/lang/Object;)Lcom/android/server/location/gnss/exp/GnssRequestBean; */
/* .line 82 */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {p0, v9, v0, v1}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->putIntoRequestMap(Ljava/lang/String;Lcom/android/server/location/gnss/exp/GnssRequestBean;Z)V */
/* .line 85 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "request: mRequestMap add: "; // const-string v2, "request: mRequestMap add: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "\nNow"; // const-string v2, "\nNow"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-object/from16 v2, p4 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v3 = " callType:"; // const-string v3, " callType:"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-object/from16 v3, p8 */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v4 = " identity:"; // const-string v4, " identity:"
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-object/from16 v4, p3 */
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "GnssBackgroundUsageOpt"; // const-string v5, "GnssBackgroundUsageOpt"
android.util.Log .d ( v5,v0 );
/* .line 86 */
v0 = this.mIsSpecifiedDevice;
v0 = (( java.util.concurrent.atomic.AtomicBoolean ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
int v6 = 1; // const/4 v6, 0x1
if ( v0 != null) { // if-eqz v0, :cond_4
final String v0 = "gps"; // const-string v0, "gps"
v0 = (( java.lang.String ) v0 ).equals ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 87 */
v0 = this.mBackOpt3Map;
v10 = /* invoke-virtual/range {p3 ..p3}, Landroid/location/util/identity/CallerIdentity;->getUid()I */
java.lang.Integer .valueOf ( v10 );
/* check-cast v0, Ljava/lang/Long; */
/* .line 88 */
/* .local v0, "backTime":Ljava/lang/Long; */
/* if-nez p5, :cond_1 */
if ( v0 != null) { // if-eqz v0, :cond_1
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v10 */
(( java.lang.Long ) v0 ).longValue ( ); // invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
/* move-result-wide v12 */
/* sub-long/2addr v10, v12 */
/* const-wide/16 v12, 0x2710 */
/* cmp-long v10, v10, v12 */
/* if-lez v10, :cond_1 */
/* .line 90 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "remove by back opt 3.0:"; // const-string v10, "remove by back opt 3.0:"
(( java.lang.StringBuilder ) v1 ).append ( v10 ); // invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v9 ); // invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v5,v1 );
/* .line 92 */
com.android.server.location.gnss.GnssEventTrackingStub .getInstance ( );
/* .line 94 */
v1 = this.mRequestMap;
/* .line 95 */
/* .line 96 */
} // :cond_1
if ( p5 != null) { // if-eqz p5, :cond_2
/* .line 97 */
v6 = this.mBackOpt3Map;
v10 = /* invoke-virtual/range {p3 ..p3}, Landroid/location/util/identity/CallerIdentity;->getUid()I */
java.lang.Integer .valueOf ( v10 );
/* .line 100 */
} // :cond_2
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "normal request location key:"; // const-string v10, "normal request location key:"
(( java.lang.StringBuilder ) v6 ).append ( v10 ); // invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v9 ); // invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v5,v6 );
/* .line 102 */
/* if-nez p5, :cond_3 */
/* .line 103 */
/* const/16 v5, 0x2710 */
/* invoke-direct {p0, v8, v9, v5}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->remove(Ljava/lang/String;Ljava/lang/String;I)V */
/* .line 105 */
} // .end local v0 # "backTime":Ljava/lang/Long;
} // :cond_3
/* goto/16 :goto_0 */
} // :cond_4
v0 = this.mIsInSatelliteCallMode;
v0 = (( java.util.concurrent.atomic.AtomicBoolean ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 106 */
/* iget-boolean v0, v7, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mIsUseUidCtl:Z */
final String v10 = " need to return..."; // const-string v10, " need to return..."
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 107 */
v0 = this.mSatelliteCallAppUidSet;
v11 = /* invoke-virtual/range {p3 ..p3}, Landroid/location/util/identity/CallerIdentity;->getUid()I */
java.lang.Integer .valueOf ( v11 );
v0 = (( java.util.HashSet ) v0 ).contains ( v11 ); // invoke-virtual {v0, v11}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
/* if-nez v0, :cond_7 */
/* .line 110 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "request location by uid:"; // const-string v1, "request location by uid:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v10 ); // invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v5,v0 );
/* .line 112 */
/* .line 114 */
} // :cond_5
v0 = this.mSatelliteCallPkgSet;
/* invoke-virtual/range {p3 ..p3}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String; */
v0 = (( java.util.HashSet ) v0 ).contains ( v11 ); // invoke-virtual {v0, v11}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
/* if-nez v0, :cond_7 */
/* .line 117 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "request location by pkg:"; // const-string v1, "request location by pkg:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v10 ); // invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v5,v0 );
/* .line 119 */
/* .line 122 */
} // :cond_6
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "request provider:"; // const-string v6, "request provider:"
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = " key:"; // const-string v6, " key:"
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v5,v0 );
/* .line 124 */
} // :cond_7
} // :goto_0
} // .end method
public void setBackgroundOptStatus ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "status" # Z */
/* .line 293 */
v0 = this.mIsSpecifiedDevice;
v0 = (( java.util.concurrent.atomic.AtomicBoolean ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
final String v1 = "GnssBackgroundUsageOpt"; // const-string v1, "GnssBackgroundUsageOpt"
/* if-nez v0, :cond_0 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 294 */
/* invoke-direct {p0}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->registerProcessObserver()V */
/* .line 295 */
final String v0 = "Has Register Process Observer by cloud..."; // const-string v0, "Has Register Process Observer by cloud..."
android.util.Log .d ( v1,v0 );
/* .line 296 */
v0 = this.mIsSpecifiedDevice;
int v1 = 1; // const/4 v1, 0x1
(( java.util.concurrent.atomic.AtomicBoolean ) v0 ).set ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
/* .line 297 */
v0 = this.mIsSpecifiedDevice;
v0 = (( java.util.concurrent.atomic.AtomicBoolean ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
/* invoke-direct {p0, v0}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->saveCloudDataToSP(Z)V */
/* .line 298 */
} // :cond_0
v0 = this.mIsSpecifiedDevice;
v0 = (( java.util.concurrent.atomic.AtomicBoolean ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* if-nez p1, :cond_1 */
/* .line 299 */
/* invoke-direct {p0}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->unRegisterProcessObserver()V */
/* .line 300 */
final String v0 = "Has unRegister Process Observer by cloud..."; // const-string v0, "Has unRegister Process Observer by cloud..."
android.util.Log .d ( v1,v0 );
/* .line 301 */
v0 = this.mIsSpecifiedDevice;
int v1 = 0; // const/4 v1, 0x0
(( java.util.concurrent.atomic.AtomicBoolean ) v0 ).set ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
/* .line 302 */
v0 = this.mIsSpecifiedDevice;
v0 = (( java.util.concurrent.atomic.AtomicBoolean ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
/* invoke-direct {p0, v0}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->saveCloudDataToSP(Z)V */
/* .line 304 */
} // :cond_1
} // :goto_0
return;
} // .end method
public void unRegisterSatelliteCallMode ( ) {
/* .locals 2 */
/* .line 253 */
/* new-instance v0, Ljava/lang/Thread; */
/* new-instance v1, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl$$ExternalSyntheticLambda2; */
/* invoke-direct {v1, p0}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;)V */
/* invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V */
/* .line 276 */
(( java.lang.Thread ) v0 ).start ( ); // invoke-virtual {v0}, Ljava/lang/Thread;->start()V
/* .line 277 */
return;
} // .end method
