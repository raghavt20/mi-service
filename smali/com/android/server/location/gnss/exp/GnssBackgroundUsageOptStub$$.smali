.class public final Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub$$;
.super Ljava/lang/Object;
.source "GnssBackgroundUsageOptStub$$.java"

# interfaces
.implements Lcom/miui/base/MiuiStubRegistry$ImplProviderManifest;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final collectImplProviders(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/miui/base/MiuiStubRegistry$ImplProvider<",
            "*>;>;)V"
        }
    .end annotation

    .line 38
    .local p1, "outProviders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/miui/base/MiuiStubRegistry$ImplProvider<*>;>;"
    new-instance v0, Lcom/android/server/location/gnss/operators/GnssForCommonOperatorImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/location/gnss/operators/GnssForCommonOperatorImpl$Provider;-><init>()V

    const-string v1, "com.android.server.location.gnss.operators.GnssForOperatorCommonStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    new-instance v0, Lcom/android/server/location/GnssMockLocationOptImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/location/GnssMockLocationOptImpl$Provider;-><init>()V

    const-string v1, "com.android.server.location.GnssMockLocationOptStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    new-instance v0, Lcom/android/server/location/gnss/GnssLocationProviderImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/location/gnss/GnssLocationProviderImpl$Provider;-><init>()V

    const-string v1, "com.android.server.location.gnss.GnssLocationProviderStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    new-instance v0, Lcom/android/server/location/MtkGnssPowerSaveImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/location/MtkGnssPowerSaveImpl$Provider;-><init>()V

    const-string v1, "com.android.server.location.MtkGnssPowerSaveStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    new-instance v0, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/location/GnssSmartSatelliteSwitchImpl$Provider;-><init>()V

    const-string v1, "com.android.server.location.GnssSmartSatelliteSwitchStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    new-instance v0, Lcom/android/server/location/MiuiBlurLocationManagerImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/location/MiuiBlurLocationManagerImpl$Provider;-><init>()V

    const-string v1, "com.android.server.location.MiuiBlurLocationManagerStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    new-instance v0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$Provider;-><init>()V

    const-string v1, "com.android.server.location.gnss.GnssEventTrackingStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    new-instance v0, Lcom/android/server/location/LocationExtCooperateImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/location/LocationExtCooperateImpl$Provider;-><init>()V

    const-string v1, "com.android.server.location.LocationExtCooperateStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    new-instance v0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl$Provider;-><init>()V

    const-string v1, "com.android.server.location.gnss.exp.GnssBackgroundUsageOptStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    new-instance v0, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/location/gnss/hal/GnssScoringModelImpl$Provider;-><init>()V

    const-string v1, "com.android.server.location.gnss.hal.GnssScoringModelStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    new-instance v0, Lcom/android/server/location/gnss/operators/GnssForKtCustomImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/location/gnss/operators/GnssForKtCustomImpl$Provider;-><init>()V

    const-string v1, "com.android.server.location.gnss.operators.GnssForKtCustomStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    new-instance v0, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeImpl$Provider;-><init>()V

    const-string v1, "com.android.server.location.gnss.hal.GnssPowerOptimizeStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    new-instance v0, Lcom/android/server/location/LocationDumpLogImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/location/LocationDumpLogImpl$Provider;-><init>()V

    const-string v1, "com.android.server.location.LocationDumpLogStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    new-instance v0, Lcom/android/server/location/gnss/operators/GnssForTelcelCustomImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/location/gnss/operators/GnssForTelcelCustomImpl$Provider;-><init>()V

    const-string v1, "com.android.server.location.gnss.operators.GnssForTelcelCustomStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    new-instance v0, Lcom/android/server/location/provider/LocationProviderManagerImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/location/provider/LocationProviderManagerImpl$Provider;-><init>()V

    const-string v1, "com.android.server.location.provider.LocationProviderManagerStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    new-instance v0, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery$Provider;

    invoke-direct {v0}, Lcom/android/server/location/gnss/gnssSelfRecovery/GnssSelfRecovery$Provider;-><init>()V

    const-string v1, "com.android.server.location.gnss.gnssSelfRecovery.GnssSelfRecoveryStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    new-instance v0, Lcom/android/server/location/gnss/monitor/LocationSettingsEventMonitorImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/location/gnss/monitor/LocationSettingsEventMonitorImpl$Provider;-><init>()V

    const-string v1, "com.android.server.location.gnss.monitor.LocationSettingsEventMonitorStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    new-instance v0, Lcom/android/server/location/gnss/hal/GnssNativeImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/location/gnss/hal/GnssNativeImpl$Provider;-><init>()V

    const-string v1, "com.android.server.location.gnss.hal.GnssNativeStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    new-instance v0, Lcom/android/server/location/provider/AmapCustomImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/location/provider/AmapCustomImpl$Provider;-><init>()V

    const-string v1, "com.android.server.location.provider.AmapCustomStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    new-instance v0, Lcom/android/server/location/GnssCollectDataImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/location/GnssCollectDataImpl$Provider;-><init>()V

    const-string v1, "com.android.server.location.gnss.GnssCollectDataStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    new-instance v0, Lcom/android/server/location/gnss/GnssConfigurationImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/location/gnss/GnssConfigurationImpl$Provider;-><init>()V

    const-string v1, "com.android.server.location.gnss.GnssConfigurationStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    return-void
.end method
