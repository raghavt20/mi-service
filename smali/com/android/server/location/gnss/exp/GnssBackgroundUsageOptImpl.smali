.class public Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;
.super Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub;
.source "GnssBackgroundUsageOptImpl.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.location.gnss.exp.GnssBackgroundUsageOptStub$$"
.end annotation


# static fields
.field private static final BACKGROUND_USAGE_OPT_REMOVE_IMMEDIATELY:I = 0x0

.field private static final BACKGROUND_USAGE_OPT_TIME:I = 0x2710

.field private static final DEF_UID:I = -0x1

.field private static final REQ_KEY_SUFFIX_DUP:Ljava/lang/String; = "-dupReq"

.field private static final TAG:Ljava/lang/String; = "GnssBackgroundUsageOpt"

.field private static mContext:Landroid/content/Context;


# instance fields
.field private final D:Z

.field private mAlreadyLoadDataFromSP:Z

.field private final mBackOpt3Map:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final mCloudSpFile:Ljava/io/File;

.field private mCurrentForeground:Z

.field private final mDefaultFeatureStatus:Z

.field private mIProcessObserverStub:Landroid/app/IProcessObserver$Stub;

.field private mIRemoveRequest:Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub$IRemoveRequest;

.field private mIRestoreRequest:Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub$IRestoreRequest;

.field private volatile mIsBackFromGsco:Z

.field private final mIsInSatelliteCallMode:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final mIsSpecifiedDevice:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private volatile mIsUseUidCtl:Z

.field private mOldForeground:Z

.field private mOldUid:I

.field private final mRemoveThreadMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Thread;",
            ">;"
        }
    .end annotation
.end field

.field private mRemovedUid:I

.field private final mRequestMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/android/server/location/gnss/exp/GnssRequestBean;",
            ">;"
        }
    .end annotation
.end field

.field private mSatelliteCallAppUidSet:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSatelliteCallPkgSet:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mScStartTime:J

.field private final mUidForMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static synthetic $r8$lambda$2RXna1m8T_aYV6u3o8VpfiYheZA(Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->lambda$remove$2(Ljava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method public static synthetic $r8$lambda$fCWvbKOx5RX1u9kG8aORe42auv0(Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->lambda$unRegisterSatelliteCallMode$1()V

    return-void
.end method

.method public static synthetic $r8$lambda$wH6cmURY5LEvZfPpymXklJErtxE(Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;Ljava/util/HashSet;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->lambda$registerSatelliteCallMode$0(Ljava/util/HashSet;Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmRemoveThreadMap(Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mRemoveThreadMap:Ljava/util/Map;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmRequestMap(Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mRequestMap:Ljava/util/Map;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$misProcessAlive(Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;I)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->isProcessAlive(I)Z

    move-result p0

    return p0
.end method

.method constructor <init>()V
    .locals 5

    .line 71
    invoke-direct {p0}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub;-><init>()V

    .line 46
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mIsSpecifiedDevice:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 48
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mIsInSatelliteCallMode:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 50
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mUidForMap:Ljava/util/Map;

    .line 53
    new-instance v0, Ljava/io/File;

    new-instance v2, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v3

    const-string/jumbo v4, "system"

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const-string v3, "IsSpecifiedDevice.xml"

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mCloudSpFile:Ljava/io/File;

    .line 54
    const-string v0, "persist.sys.gnss_back.opt"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mDefaultFeatureStatus:Z

    .line 55
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->D:Z

    .line 56
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mRequestMap:Ljava/util/Map;

    .line 57
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mRemoveThreadMap:Ljava/util/Map;

    .line 58
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mBackOpt3Map:Ljava/util/Map;

    .line 65
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mSatelliteCallAppUidSet:Ljava/util/HashSet;

    .line 66
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mSatelliteCallPkgSet:Ljava/util/HashSet;

    .line 72
    return-void
.end method

.method private createReqKey(IILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "providerName"    # Ljava/lang/String;
    .param p4, "listenerId"    # Ljava/lang/String;

    .line 614
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getForegroundProcessUid()I
    .locals 7

    .line 481
    sget-object v0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mContext:Landroid/content/Context;

    const/4 v1, -0x1

    if-nez v0, :cond_0

    return v1

    .line 482
    :cond_0
    const-string v2, "activity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 483
    .local v0, "activityManager":Landroid/app/ActivityManager;
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v2

    .line 484
    .local v2, "runningAppProcesses":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 485
    .local v4, "processInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    iget v5, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v6, 0x64

    if-ne v5, v6, :cond_1

    .line 486
    iget v1, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->uid:I

    return v1

    .line 488
    .end local v4    # "processInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    :cond_1
    goto :goto_0

    .line 489
    :cond_2
    return v1
.end method

.method private getUidFromKey(Ljava/lang/String;)I
    .locals 6
    .param p1, "key"    # Ljava/lang/String;

    .line 458
    const/4 v0, -0x1

    .line 459
    .local v0, "uid":I
    const-string v1, "\\d+"

    invoke-static {v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    .line 460
    .local v1, "pattern":Ljava/util/regex/Pattern;
    invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 461
    .local v2, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 463
    :try_start_0
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v3

    .line 466
    goto :goto_0

    .line 464
    :catch_0
    move-exception v3

    .line 465
    .local v3, "e":Ljava/lang/NumberFormatException;
    const-string v4, "GnssBackgroundUsageOpt"

    const-string/jumbo v5, "uid parseInt NumberFormatException..."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 468
    .end local v3    # "e":Ljava/lang/NumberFormatException;
    :cond_0
    :goto_0
    return v0
.end method

.method private isForegroundService(II)Z
    .locals 6
    .param p1, "uid"    # I
    .param p2, "pid"    # I

    .line 581
    sget-object v0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 582
    :cond_0
    const-string v2, "activity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 583
    .local v0, "activityManager":Landroid/app/ActivityManager;
    const v2, 0x7fffffff

    invoke-virtual {v0, v2}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v2

    .line 584
    .local v2, "runningServices":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager$RunningServiceInfo;

    .line 585
    .local v4, "service":Landroid/app/ActivityManager$RunningServiceInfo;
    iget v5, v4, Landroid/app/ActivityManager$RunningServiceInfo;->uid:I

    if-ne v5, p1, :cond_1

    iget v5, v4, Landroid/app/ActivityManager$RunningServiceInfo;->pid:I

    if-ne v5, p2, :cond_1

    .line 586
    iget-boolean v5, v4, Landroid/app/ActivityManager$RunningServiceInfo;->foreground:Z

    if-eqz v5, :cond_1

    .line 588
    const/4 v1, 0x1

    return v1

    .line 591
    .end local v4    # "service":Landroid/app/ActivityManager$RunningServiceInfo;
    :cond_1
    goto :goto_0

    .line 592
    :cond_2
    return v1
.end method

.method private isProcessAlive(I)Z
    .locals 6
    .param p1, "pid"    # I

    .line 493
    sget-object v0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 494
    :cond_0
    const-string v2, "activity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 495
    .local v0, "activityManager":Landroid/app/ActivityManager;
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v2

    .line 496
    .local v2, "processList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    if-eqz v2, :cond_2

    .line 497
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 498
    .local v4, "processInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    iget v5, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-ne v5, p1, :cond_1

    .line 499
    const/4 v1, 0x1

    return v1

    .line 501
    .end local v4    # "processInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    :cond_1
    goto :goto_0

    .line 503
    :cond_2
    return v1
.end method

.method private synthetic lambda$registerSatelliteCallMode$0(Ljava/util/HashSet;Ljava/lang/String;)V
    .locals 4
    .param p1, "pkgSet"    # Ljava/util/HashSet;
    .param p2, "key"    # Ljava/lang/String;

    .line 228
    const-class v0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;

    monitor-enter v0

    .line 230
    :try_start_0
    const-string v1, "GnssBackgroundUsageOpt"

    const-string v2, "=================registerSatelliteCallMode, Start ================="

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    iget-object v1, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mIsInSatelliteCallMode:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 232
    const-string v1, "GnssBackgroundUsageOpt"

    const-string v2, "is in Satellite Call Mode do not need register..."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    monitor-exit v0

    return-void

    .line 235
    :cond_0
    const-string v1, "GnssBackgroundUsageOpt"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "removeAndBlockAllRequestExPkg\uff0cpkg:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/util/HashSet;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",key:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    iget-object v1, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->KEY_API_USE:Ljava/lang/String;

    if-eq v1, p2, :cond_1

    .line 237
    const-string v1, "GnssBackgroundUsageOpt"

    const-string v2, "removeAndBlockAllRequestExPkg key is invalid..."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    monitor-exit v0

    return-void

    .line 241
    :cond_1
    iget-object v1, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mIsSpecifiedDevice:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 243
    iget-object v1, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mIsInSatelliteCallMode:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 245
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mScStartTime:J

    .line 246
    invoke-direct {p0, p1}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->removeCurrentAllRequestExPkgSet(Ljava/util/HashSet;)V

    .line 247
    monitor-exit v0

    .line 248
    return-void

    .line 247
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private synthetic lambda$remove$2(Ljava/lang/String;ILjava/lang/String;)V
    .locals 5
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "delayTime"    # I
    .param p3, "providerName"    # Ljava/lang/String;

    .line 315
    const-string v0, " delay:"

    iget-object v1, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mRequestMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/location/gnss/exp/GnssRequestBean;

    .line 316
    .local v1, "bean":Lcom/android/server/location/gnss/exp/GnssRequestBean;
    const-string v2, "GnssBackgroundUsageOpt"

    if-eqz v1, :cond_3

    .line 319
    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "remove key sleep before: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    if-eqz p2, :cond_0

    .line 321
    int-to-long v3, p2

    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V

    .line 322
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "remove key sleep after: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    iget-object v0, v1, Lcom/android/server/location/gnss/exp/GnssRequestBean;->identity:Landroid/location/util/identity/CallerIdentity;

    invoke-virtual {v0}, Landroid/location/util/identity/CallerIdentity;->getUid()I

    move-result v0

    iget-object v3, v1, Lcom/android/server/location/gnss/exp/GnssRequestBean;->identity:Landroid/location/util/identity/CallerIdentity;

    invoke-virtual {v3}, Landroid/location/util/identity/CallerIdentity;->getPid()I

    move-result v3

    invoke-direct {p0, v0, v3}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->isForegroundService(II)Z

    move-result v0

    iput-boolean v0, v1, Lcom/android/server/location/gnss/exp/GnssRequestBean;->isForegroundService:Z

    .line 324
    iget-object v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mIRemoveRequest:Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub$IRemoveRequest;

    iget-object v3, v1, Lcom/android/server/location/gnss/exp/GnssRequestBean;->callbackType:Ljava/lang/Object;

    invoke-interface {v0, p3, v3}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub$IRemoveRequest;->onRemoveListener(Ljava/lang/String;Ljava/lang/Object;)V

    .line 325
    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/android/server/location/gnss/exp/GnssRequestBean;->removeByOpt:Z

    .line 326
    invoke-direct {p0, p1, v1, v0}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->putIntoRequestMap(Ljava/lang/String;Lcom/android/server/location/gnss/exp/GnssRequestBean;Z)V

    .line 328
    iget-object v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mIsInSatelliteCallMode:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 329
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "remove by GSCO opt, key:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    invoke-static {}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->getInstance()Lcom/android/server/location/gnss/GnssEventTrackingStub;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->recordGnssSatelliteCallOptCnt()V

    goto :goto_0

    .line 333
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "remove by GBO opt, key:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    invoke-static {}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->getInstance()Lcom/android/server/location/gnss/GnssEventTrackingStub;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->recordGnssBackgroundOpt2Time()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 336
    :catch_0
    move-exception v0

    .line 337
    .local v0, "e":Ljava/lang/Exception;
    instance-of v3, v0, Ljava/lang/InterruptedException;

    if-eqz v3, :cond_2

    .line 338
    const-string v3, "current remove thread has been interrupted..."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 339
    iget-object v2, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mRemoveThreadMap:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 340
    return-void

    .line 342
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    :goto_0
    goto :goto_1

    .line 345
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "remove by opt interrupt, key:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 347
    :goto_1
    iget-object v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mRemoveThreadMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 348
    return-void
.end method

.method private synthetic lambda$unRegisterSatelliteCallMode$1()V
    .locals 7

    .line 254
    const-class v0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;

    monitor-enter v0

    .line 256
    :try_start_0
    iget-object v1, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mIsInSatelliteCallMode:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-nez v1, :cond_0

    .line 257
    const-string v1, "GnssBackgroundUsageOpt"

    const-string v2, "is not in Satellite Call Mode do not need unRegister..."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    monitor-exit v0

    return-void

    .line 260
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mIsBackFromGsco:Z

    .line 262
    invoke-direct {p0}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->loadFeatureSwitch()V

    .line 264
    iget-object v1, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mIsInSatelliteCallMode:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 266
    iget-wide v3, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mScStartTime:J

    const-wide/16 v5, 0x0

    cmp-long v1, v3, v5

    if-eqz v1, :cond_1

    .line 268
    invoke-static {}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->getInstance()Lcom/android/server/location/gnss/GnssEventTrackingStub;

    move-result-object v1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    iget-wide v5, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mScStartTime:J

    sub-long/2addr v3, v5

    invoke-interface {v1, v3, v4}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->recordGnssSatelliteCallOptDuring(J)V

    .line 271
    :cond_1
    invoke-direct {p0}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->restoreGsco()V

    .line 272
    iput-boolean v2, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mIsBackFromGsco:Z

    .line 274
    const-string v1, "GnssBackgroundUsageOpt"

    const-string v2, "=================unRegisterSatelliteCallMode, End ================="

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    monitor-exit v0

    .line 276
    return-void

    .line 275
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private loadCloudDataFromSP()Z
    .locals 5

    .line 440
    const-string v0, "load mIsSpecifiedDevice running..."

    const-string v1, "GnssBackgroundUsageOpt"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 442
    :try_start_0
    sget-object v0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 443
    iget-boolean v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mDefaultFeatureStatus:Z

    return v0

    .line 445
    :cond_0
    invoke-virtual {v0}, Landroid/content/Context;->createDeviceProtectedStorageContext()Landroid/content/Context;

    move-result-object v0

    .line 446
    .local v0, "directBootContext":Landroid/content/Context;
    iget-object v2, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mCloudSpFile:Ljava/io/File;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/io/File;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 447
    .local v2, "editor":Landroid/content/SharedPreferences;
    const-string v3, "mIsSpecifiedDevice"

    iget-boolean v4, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mDefaultFeatureStatus:Z

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v3

    .line 451
    .end local v2    # "editor":Landroid/content/SharedPreferences;
    .local v0, "status":Z
    nop

    .line 452
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mAlreadyLoadDataFromSP:Z

    .line 453
    const-string v2, "Success to load mIsSpecifiedDevice..."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 454
    return v0

    .line 448
    .end local v0    # "status":Z
    :catch_0
    move-exception v0

    .line 449
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to load mIsSpecifiedDevice..., "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 450
    iget-boolean v1, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mDefaultFeatureStatus:Z

    return v1
.end method

.method private loadFeatureSwitch()V
    .locals 3

    .line 472
    iget-object v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mIsSpecifiedDevice:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {p0}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->loadCloudDataFromSP()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 473
    iget-boolean v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mAlreadyLoadDataFromSP:Z

    const-string v1, "GnssBackgroundUsageOpt"

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mIsSpecifiedDevice:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 474
    invoke-direct {p0}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->registerProcessObserver()V

    .line 475
    const-string v0, "Has Register Process Observer..."

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 477
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Is Specified Device:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mIsSpecifiedDevice:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 478
    return-void
.end method

.method private putGnssRequestBean(Landroid/location/util/identity/CallerIdentity;Ljava/lang/String;Landroid/location/LocationRequest;ZILjava/lang/Object;)Lcom/android/server/location/gnss/exp/GnssRequestBean;
    .locals 3
    .param p1, "identity"    # Landroid/location/util/identity/CallerIdentity;
    .param p2, "provider"    # Ljava/lang/String;
    .param p3, "locationRequest"    # Landroid/location/LocationRequest;
    .param p4, "foreground"    # Z
    .param p5, "permissionLevel"    # I
    .param p6, "callbackType"    # Ljava/lang/Object;

    .line 509
    new-instance v0, Lcom/android/server/location/gnss/exp/GnssRequestBean;

    invoke-direct {v0}, Lcom/android/server/location/gnss/exp/GnssRequestBean;-><init>()V

    .line 510
    .local v0, "requestBean":Lcom/android/server/location/gnss/exp/GnssRequestBean;
    iput-object p1, v0, Lcom/android/server/location/gnss/exp/GnssRequestBean;->identity:Landroid/location/util/identity/CallerIdentity;

    .line 511
    iput-object p6, v0, Lcom/android/server/location/gnss/exp/GnssRequestBean;->callbackType:Ljava/lang/Object;

    .line 512
    iput-object p2, v0, Lcom/android/server/location/gnss/exp/GnssRequestBean;->provider:Ljava/lang/String;

    .line 513
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/android/server/location/gnss/exp/GnssRequestBean;->removeByOpt:Z

    .line 514
    iput-object p3, v0, Lcom/android/server/location/gnss/exp/GnssRequestBean;->locationRequest:Landroid/location/LocationRequest;

    .line 515
    iput p5, v0, Lcom/android/server/location/gnss/exp/GnssRequestBean;->permissionLevel:I

    .line 516
    invoke-virtual {p1}, Landroid/location/util/identity/CallerIdentity;->getUid()I

    move-result v1

    invoke-virtual {p1}, Landroid/location/util/identity/CallerIdentity;->getPid()I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->isForegroundService(II)Z

    move-result v1

    iput-boolean v1, v0, Lcom/android/server/location/gnss/exp/GnssRequestBean;->isForegroundService:Z

    .line 517
    iput-boolean p4, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mOldForeground:Z

    .line 518
    return-object v0
.end method

.method private putIntoRequestMap(Ljava/lang/String;Lcom/android/server/location/gnss/exp/GnssRequestBean;Z)V
    .locals 4
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Lcom/android/server/location/gnss/exp/GnssRequestBean;
    .param p3, "isModify"    # Z

    .line 596
    if-eqz p3, :cond_0

    .line 597
    iget-object v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mRequestMap:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 598
    return-void

    .line 601
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mRequestMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/location/gnss/exp/GnssRequestBean;

    .line 602
    .local v0, "bean":Lcom/android/server/location/gnss/exp/GnssRequestBean;
    if-eqz v0, :cond_3

    .line 603
    iget-object v1, p2, Lcom/android/server/location/gnss/exp/GnssRequestBean;->callbackType:Ljava/lang/Object;

    instance-of v1, v1, Landroid/location/ILocationListener;

    if-eqz v1, :cond_1

    iget-object v1, p2, Lcom/android/server/location/gnss/exp/GnssRequestBean;->callbackType:Ljava/lang/Object;

    check-cast v1, Landroid/location/ILocationListener;

    .line 604
    invoke-interface {v1}, Landroid/location/ILocationListener;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    iget-object v2, v0, Lcom/android/server/location/gnss/exp/GnssRequestBean;->callbackType:Ljava/lang/Object;

    check-cast v2, Landroid/location/ILocationListener;

    invoke-interface {v2}, Landroid/location/ILocationListener;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    if-eq v1, v2, :cond_2

    :cond_1
    iget-object v1, p2, Lcom/android/server/location/gnss/exp/GnssRequestBean;->callbackType:Ljava/lang/Object;

    iget-object v2, v0, Lcom/android/server/location/gnss/exp/GnssRequestBean;->callbackType:Ljava/lang/Object;

    .line 605
    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 606
    :cond_2
    iget-object v1, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mRequestMap:Ljava/util/Map;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-dupReq"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 607
    return-void

    .line 610
    :cond_3
    iget-object v1, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mRequestMap:Ljava/util/Map;

    invoke-interface {v1, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 611
    return-void
.end method

.method private registerProcessObserver()V
    .locals 3

    .line 368
    new-instance v0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl$1;

    invoke-direct {v0, p0}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl$1;-><init>(Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;)V

    iput-object v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mIProcessObserverStub:Landroid/app/IProcessObserver$Stub;

    .line 405
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManager;->getService()Landroid/app/IActivityManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mIProcessObserverStub:Landroid/app/IProcessObserver$Stub;

    invoke-interface {v0, v1}, Landroid/app/IActivityManager;->registerProcessObserver(Landroid/app/IProcessObserver;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 408
    goto :goto_0

    .line 406
    :catch_0
    move-exception v0

    .line 407
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ActivityManager registerProcessObserver RemoteException...:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GnssBackgroundUsageOpt"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 409
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method

.method private remove(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2
    .param p1, "providerName"    # Ljava/lang/String;
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "delayTime"    # I

    .line 312
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "remove key: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GnssBackgroundUsageOpt"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    iget-object v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mRemoveThreadMap:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 314
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0, p2, p3, p1}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;Ljava/lang/String;ILjava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 349
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 350
    iget-object v1, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mRemoveThreadMap:Ljava/util/Map;

    invoke-interface {v1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 352
    .end local v0    # "thread":Ljava/lang/Thread;
    :cond_0
    return-void
.end method

.method private removeCurrentAllRequestExPkgByPkgSet(Ljava/util/HashSet;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 554
    .local p1, "pkgSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    if-nez p1, :cond_0

    return-void

    .line 556
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mRequestMap:Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 557
    .local v0, "reqMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/location/gnss/exp/GnssRequestBean;>;"
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 558
    .local v2, "map":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/location/gnss/exp/GnssRequestBean;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;

    iget-object v3, v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;->identity:Landroid/location/util/identity/CallerIdentity;

    invoke-virtual {v3}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 559
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;

    iget-object v3, v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;->provider:Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct {p0, v3, v4, v5}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->remove(Ljava/lang/String;Ljava/lang/String;I)V

    .line 560
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "removeCurrentAllRequestExPkgByPkg, uid:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/location/gnss/exp/GnssRequestBean;

    iget-object v4, v4, Lcom/android/server/location/gnss/exp/GnssRequestBean;->identity:Landroid/location/util/identity/CallerIdentity;

    invoke-virtual {v4}, Landroid/location/util/identity/CallerIdentity;->getUid()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", pkg:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 561
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/location/gnss/exp/GnssRequestBean;

    iget-object v4, v4, Lcom/android/server/location/gnss/exp/GnssRequestBean;->identity:Landroid/location/util/identity/CallerIdentity;

    invoke-virtual {v4}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\nNow :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/location/gnss/exp/GnssRequestBean;

    iget-object v4, v4, Lcom/android/server/location/gnss/exp/GnssRequestBean;->locationRequest:Landroid/location/LocationRequest;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 560
    const-string v4, "GnssBackgroundUsageOpt"

    invoke-static {v4, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 563
    .end local v2    # "map":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/location/gnss/exp/GnssRequestBean;>;"
    :cond_1
    goto :goto_0

    .line 564
    :cond_2
    return-void
.end method

.method private removeCurrentAllRequestExPkgByUidSet(Ljava/util/HashSet;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 567
    .local p1, "uidSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    if-nez p1, :cond_0

    return-void

    .line 569
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mRequestMap:Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 570
    .local v0, "reqMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/location/gnss/exp/GnssRequestBean;>;"
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 571
    .local v2, "map":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/location/gnss/exp/GnssRequestBean;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;

    iget-object v3, v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;->identity:Landroid/location/util/identity/CallerIdentity;

    invoke-virtual {v3}, Landroid/location/util/identity/CallerIdentity;->getUid()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 573
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;

    iget-object v3, v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;->provider:Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct {p0, v3, v4, v5}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->remove(Ljava/lang/String;Ljava/lang/String;I)V

    .line 574
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "removeCurrentAllRequestExPkgByUid, key:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", pkg:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 575
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/location/gnss/exp/GnssRequestBean;

    iget-object v4, v4, Lcom/android/server/location/gnss/exp/GnssRequestBean;->identity:Landroid/location/util/identity/CallerIdentity;

    invoke-virtual {v4}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n Now mRequestMap:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/location/gnss/exp/GnssRequestBean;

    iget-object v4, v4, Lcom/android/server/location/gnss/exp/GnssRequestBean;->locationRequest:Landroid/location/LocationRequest;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 574
    const-string v4, "GnssBackgroundUsageOpt"

    invoke-static {v4, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 577
    .end local v2    # "map":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/location/gnss/exp/GnssRequestBean;>;"
    :cond_1
    goto :goto_0

    .line 578
    :cond_2
    return-void
.end method

.method private removeCurrentAllRequestExPkgSet(Ljava/util/HashSet;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 522
    .local p1, "pkgSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    const-string v0, "GnssBackgroundUsageOpt"

    if-eqz p1, :cond_5

    invoke-virtual {p1}, Ljava/util/HashSet;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_3

    .line 526
    :cond_0
    iget-object v1, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mSatelliteCallAppUidSet:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->clear()V

    .line 527
    iput-object p1, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mSatelliteCallPkgSet:Ljava/util/HashSet;

    .line 528
    const/4 v1, 0x0

    .line 529
    .local v1, "isException":Z
    invoke-virtual {p1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, 0x0

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 531
    .local v3, "p":Ljava/lang/String;
    :try_start_0
    sget-object v5, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mContext:Landroid/content/Context;

    if-eqz v5, :cond_1

    .line 532
    iget-object v6, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mSatelliteCallAppUidSet:Ljava/util/HashSet;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {v5, v3, v4}, Landroid/content/pm/PackageManager;->getPackageUid(Ljava/lang/String;I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 537
    :cond_1
    goto :goto_1

    .line 534
    :catch_0
    move-exception v4

    .line 535
    .local v4, "e":Ljava/lang/Exception;
    const/4 v1, 0x1

    .line 536
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getPackageManager Exception:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 538
    .end local v3    # "p":Ljava/lang/String;
    .end local v4    # "e":Ljava/lang/Exception;
    :goto_1
    goto :goto_0

    .line 539
    :cond_2
    if-eqz v1, :cond_3

    .line 541
    iput-boolean v4, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mIsUseUidCtl:Z

    .line 542
    invoke-direct {p0, p1}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->removeCurrentAllRequestExPkgByPkgSet(Ljava/util/HashSet;)V

    goto :goto_2

    .line 545
    :cond_3
    iget-object v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mSatelliteCallAppUidSet:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 547
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mIsUseUidCtl:Z

    .line 548
    iget-object v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mSatelliteCallAppUidSet:Ljava/util/HashSet;

    invoke-direct {p0, v0}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->removeCurrentAllRequestExPkgByUidSet(Ljava/util/HashSet;)V

    .line 551
    :cond_4
    :goto_2
    return-void

    .line 523
    .end local v1    # "isException":Z
    :cond_5
    :goto_3
    const-string v1, "removeCurrentAllRequestExPkg pkg is invalid..."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 524
    return-void
.end method

.method private restore(Ljava/lang/String;Ljava/lang/String;Landroid/location/LocationRequest;Landroid/location/util/identity/CallerIdentity;ILjava/lang/Object;)V
    .locals 8
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "providerName"    # Ljava/lang/String;
    .param p3, "request"    # Landroid/location/LocationRequest;
    .param p4, "identity"    # Landroid/location/util/identity/CallerIdentity;
    .param p5, "permissionLevel"    # I
    .param p6, "callbackType"    # Ljava/lang/Object;

    .line 357
    const-string v0, "GnssBackgroundUsageOpt"

    :try_start_0
    iget-object v1, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mRequestMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 358
    iget-object v2, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mIRestoreRequest:Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub$IRestoreRequest;

    if-eqz v2, :cond_0

    .line 359
    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    move-object v7, p6

    invoke-interface/range {v2 .. v7}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub$IRestoreRequest;->onRestore(Ljava/lang/String;Landroid/location/LocationRequest;Landroid/location/util/identity/CallerIdentity;ILjava/lang/Object;)V

    .line 361
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "restore by opt,uid:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p4}, Landroid/location/util/identity/CallerIdentity;->getUid()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " pkg:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p4}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 364
    goto :goto_0

    .line 362
    :catch_0
    move-exception v1

    .line 363
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "restore exception-->"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private restoreGsco()V
    .locals 12

    .line 619
    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mRequestMap:Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 620
    .local v0, "reqMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/location/gnss/exp/GnssRequestBean;>;"
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 621
    .local v2, "map":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/location/gnss/exp/GnssRequestBean;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;

    .line 622
    .local v3, "bean":Lcom/android/server/location/gnss/exp/GnssRequestBean;
    if-nez v3, :cond_0

    .line 623
    goto :goto_0

    .line 626
    :cond_0
    iget-boolean v4, v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;->isForegroundService:Z

    const-string v5, "key:"

    const-string v6, " pkg:"

    const-string v7, "GnssBackgroundUsageOpt"

    if-eqz v4, :cond_1

    .line 627
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;->identity:Landroid/location/util/identity/CallerIdentity;

    invoke-virtual {v5}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " is isForegroundService"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v7, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 628
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    move-object v6, v4

    check-cast v6, Ljava/lang/String;

    iget-object v7, v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;->provider:Ljava/lang/String;

    iget-object v8, v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;->locationRequest:Landroid/location/LocationRequest;

    iget-object v9, v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;->identity:Landroid/location/util/identity/CallerIdentity;

    iget v10, v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;->permissionLevel:I

    iget-object v11, v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;->callbackType:Ljava/lang/Object;

    move-object v5, p0

    invoke-direct/range {v5 .. v11}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->restore(Ljava/lang/String;Ljava/lang/String;Landroid/location/LocationRequest;Landroid/location/util/identity/CallerIdentity;ILjava/lang/Object;)V

    goto/16 :goto_1

    .line 630
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;->identity:Landroid/location/util/identity/CallerIdentity;

    invoke-virtual {v5}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " is not isForegroundService Foreground:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mUidForMap:Ljava/util/Map;

    iget-object v8, v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;->identity:Landroid/location/util/identity/CallerIdentity;

    invoke-virtual {v8}, Landroid/location/util/identity/CallerIdentity;->getUid()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v5, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mUidForMap.containsKey:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mUidForMap:Ljava/util/Map;

    iget-object v8, v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;->identity:Landroid/location/util/identity/CallerIdentity;

    invoke-virtual {v8}, Landroid/location/util/identity/CallerIdentity;->getUid()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v5, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v7, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 631
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "!mSatelliteCallAppUidSet.contains(bean.identity.getUid()):"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mSatelliteCallAppUidSet:Ljava/util/HashSet;

    iget-object v8, v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;->identity:Landroid/location/util/identity/CallerIdentity;

    invoke-virtual {v8}, Landroid/location/util/identity/CallerIdentity;->getUid()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    xor-int/lit8 v5, v5, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " bean.identity.getUid():"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;->identity:Landroid/location/util/identity/CallerIdentity;

    .line 632
    invoke-virtual {v5}, Landroid/location/util/identity/CallerIdentity;->getUid()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 631
    invoke-static {v7, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 635
    iget-object v4, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mSatelliteCallAppUidSet:Ljava/util/HashSet;

    iget-object v5, v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;->identity:Landroid/location/util/identity/CallerIdentity;

    invoke-virtual {v5}, Landroid/location/util/identity/CallerIdentity;->getUid()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mUidForMap:Ljava/util/Map;

    iget-object v5, v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;->identity:Landroid/location/util/identity/CallerIdentity;

    .line 636
    invoke-virtual {v5}, Landroid/location/util/identity/CallerIdentity;->getUid()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mUidForMap:Ljava/util/Map;

    iget-object v5, v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;->identity:Landroid/location/util/identity/CallerIdentity;

    .line 637
    invoke-virtual {v5}, Landroid/location/util/identity/CallerIdentity;->getUid()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 639
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "change to foreground remove by GSCO opt and now restore key:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 640
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;->identity:Landroid/location/util/identity/CallerIdentity;

    invoke-virtual {v5}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " isForegroundService:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;->isForegroundService:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 639
    invoke-static {v7, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 641
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    move-object v6, v4

    check-cast v6, Ljava/lang/String;

    iget-object v7, v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;->provider:Ljava/lang/String;

    iget-object v8, v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;->locationRequest:Landroid/location/LocationRequest;

    iget-object v9, v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;->identity:Landroid/location/util/identity/CallerIdentity;

    iget v10, v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;->permissionLevel:I

    iget-object v11, v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;->callbackType:Ljava/lang/Object;

    move-object v5, p0

    invoke-direct/range {v5 .. v11}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->restore(Ljava/lang/String;Ljava/lang/String;Landroid/location/LocationRequest;Landroid/location/util/identity/CallerIdentity;ILjava/lang/Object;)V

    .line 644
    .end local v2    # "map":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/location/gnss/exp/GnssRequestBean;>;"
    .end local v3    # "bean":Lcom/android/server/location/gnss/exp/GnssRequestBean;
    :cond_2
    :goto_1
    goto/16 :goto_0

    .line 645
    :cond_3
    return-void
.end method

.method private saveCloudDataToSP(Z)V
    .locals 4
    .param p1, "status"    # Z

    .line 423
    const-string v0, "Save mIsSpecifiedDevice running..."

    const-string v1, "GnssBackgroundUsageOpt"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 425
    :try_start_0
    sget-object v0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 426
    return-void

    .line 428
    :cond_0
    iget-object v2, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mCloudSpFile:Ljava/io/File;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/io/File;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 429
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "mIsSpecifiedDevice"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 430
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 434
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    nop

    .line 435
    const-string v0, "Success to save mIsSpecifiedDevice..."

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 436
    return-void

    .line 431
    :catch_0
    move-exception v0

    .line 432
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to save mIsSpecifiedDevice..., "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 433
    return-void
.end method

.method private unRegisterProcessObserver()V
    .locals 4

    .line 412
    const-string v0, "GnssBackgroundUsageOpt"

    iget-object v1, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mIProcessObserverStub:Landroid/app/IProcessObserver$Stub;

    if-eqz v1, :cond_0

    .line 414
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManager;->getService()Landroid/app/IActivityManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mIProcessObserverStub:Landroid/app/IProcessObserver$Stub;

    invoke-interface {v1, v2}, Landroid/app/IActivityManager;->unregisterProcessObserver(Landroid/app/IProcessObserver;)V

    .line 415
    const-string/jumbo v1, "unRegisterProcessObserver..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 418
    goto :goto_0

    .line 416
    :catch_0
    move-exception v1

    .line 417
    .local v1, "e":Landroid/os/RemoteException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ActivityManager unRegisterProcessObserver RemoteException...:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 420
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_0
    :goto_0
    return-void
.end method


# virtual methods
.method public getSatelliteCallMode()Z
    .locals 1

    .line 308
    iget-object v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mIsInSatelliteCallMode:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public onAppForegroundChanged(IZ)V
    .locals 13
    .param p1, "uid"    # I
    .param p2, "foreground"    # Z

    .line 130
    if-nez p2, :cond_0

    .line 131
    iget-object v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mBackOpt3Map:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 133
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mBackOpt3Map:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    :goto_0
    iget v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mOldUid:I

    if-ne v0, p1, :cond_1

    iget-boolean v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mOldForeground:Z

    if-ne v0, p2, :cond_1

    .line 136
    return-void

    .line 138
    :cond_1
    iget-object v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mUidForMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    iput p1, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mOldUid:I

    .line 140
    iput-boolean p2, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mOldForeground:Z

    .line 145
    iget-object v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mIsSpecifiedDevice:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mIsInSatelliteCallMode:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_9

    iget-boolean v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mIsBackFromGsco:Z

    if-eqz v0, :cond_2

    goto/16 :goto_3

    .line 149
    :cond_2
    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mRequestMap:Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 150
    .local v0, "reqMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/location/gnss/exp/GnssRequestBean;>;"
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 151
    .local v2, "map":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/location/gnss/exp/GnssRequestBean;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;

    iget-object v3, v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;->identity:Landroid/location/util/identity/CallerIdentity;

    invoke-virtual {v3}, Landroid/location/util/identity/CallerIdentity;->getUid()I

    move-result v3

    if-ne p1, v3, :cond_7

    .line 152
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;

    .line 153
    .local v3, "bean":Lcom/android/server/location/gnss/exp/GnssRequestBean;
    if-eqz v3, :cond_3

    const-string v4, "gps"

    iget-object v5, v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;->provider:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 154
    goto :goto_1

    .line 157
    :cond_4
    iget-object v4, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mRemoveThreadMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Thread;

    .line 158
    .local v4, "removeThread":Ljava/lang/Thread;
    if-nez p2, :cond_5

    .line 159
    iget-object v5, v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;->provider:Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    const/16 v7, 0x2710

    invoke-direct {p0, v5, v6, v7}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->remove(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_2

    .line 160
    :cond_5
    iget-boolean v5, v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;->removeByOpt:Z

    const-string v6, "GnssBackgroundUsageOpt"

    if-eqz v5, :cond_6

    .line 161
    const-string v5, "change to foreground remove by opt and now restore..."

    invoke-static {v6, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    move-object v7, v5

    check-cast v7, Ljava/lang/String;

    iget-object v8, v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;->provider:Ljava/lang/String;

    iget-object v9, v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;->locationRequest:Landroid/location/LocationRequest;

    iget-object v10, v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;->identity:Landroid/location/util/identity/CallerIdentity;

    iget v11, v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;->permissionLevel:I

    iget-object v12, v3, Lcom/android/server/location/gnss/exp/GnssRequestBean;->callbackType:Ljava/lang/Object;

    move-object v6, p0

    invoke-direct/range {v6 .. v12}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->restore(Ljava/lang/String;Ljava/lang/String;Landroid/location/LocationRequest;Landroid/location/util/identity/CallerIdentity;ILjava/lang/Object;)V

    goto :goto_2

    .line 163
    :cond_6
    if-eqz v4, :cond_7

    .line 164
    invoke-virtual {v4}, Ljava/lang/Thread;->interrupt()V

    .line 165
    const-string v5, "remove Thread not null, interrupt it..."

    invoke-static {v6, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    .end local v2    # "map":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/location/gnss/exp/GnssRequestBean;>;"
    .end local v3    # "bean":Lcom/android/server/location/gnss/exp/GnssRequestBean;
    .end local v4    # "removeThread":Ljava/lang/Thread;
    :cond_7
    :goto_2
    goto :goto_1

    .line 169
    :cond_8
    return-void

    .line 146
    .end local v0    # "reqMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/location/gnss/exp/GnssRequestBean;>;"
    :cond_9
    :goto_3
    return-void
.end method

.method public registerRequestCallback(Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub$IRemoveRequest;)V
    .locals 0
    .param p1, "iRemoveRequest"    # Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub$IRemoveRequest;

    .line 282
    iput-object p1, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mIRemoveRequest:Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub$IRemoveRequest;

    .line 283
    return-void
.end method

.method public registerRestoreCallback(Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub$IRestoreRequest;)V
    .locals 0
    .param p1, "iRestoreRequest"    # Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub$IRestoreRequest;

    .line 287
    iput-object p1, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mIRestoreRequest:Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub$IRestoreRequest;

    .line 288
    return-void
.end method

.method public registerSatelliteCallMode(Ljava/util/HashSet;Ljava/lang/String;)V
    .locals 2
    .param p2, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 227
    .local p1, "pkgSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;Ljava/util/HashSet;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 248
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 249
    return-void
.end method

.method public remove(IILjava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "provider"    # Ljava/lang/String;
    .param p4, "listenerId"    # Ljava/lang/String;

    .line 204
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->createReqKey(IILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 205
    .local v0, "removeKey":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mRequestMap:Ljava/util/Map;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-dupReq"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    const-string v2, "GnssBackgroundUsageOpt"

    if-eqz v1, :cond_0

    .line 207
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 208
    const-string v1, "same req, return dup first..."

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 210
    :cond_0
    iget-object v1, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mRequestMap:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/location/gnss/exp/GnssRequestBean;

    .line 211
    .local v1, "bean":Lcom/android/server/location/gnss/exp/GnssRequestBean;
    if-eqz v1, :cond_1

    .line 212
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "remove normal: key:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lcom/android/server/location/gnss/exp/GnssRequestBean;->callbackType:Ljava/lang/Object;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    .end local v1    # "bean":Lcom/android/server/location/gnss/exp/GnssRequestBean;
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mRequestMap:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 217
    iget-object v1, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mRemoveThreadMap:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Thread;

    .line 218
    .local v1, "thread":Ljava/lang/Thread;
    if-eqz v1, :cond_2

    .line 220
    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 222
    :cond_2
    return-void
.end method

.method public removeByLmsUser(IILjava/lang/Object;)V
    .locals 7
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "callbackType"    # Ljava/lang/Object;

    .line 178
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "removeByLmsUser"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GnssBackgroundUsageOpt"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    iget-object v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mIsInSatelliteCallMode:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 180
    if-nez p3, :cond_0

    return-void

    .line 182
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    iget-object v2, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mRequestMap:Ljava/util/Map;

    invoke-direct {v0, v2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 183
    .local v0, "reqMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/location/gnss/exp/GnssRequestBean;>;"
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 184
    .local v3, "map":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/location/gnss/exp/GnssRequestBean;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/location/gnss/exp/GnssRequestBean;

    iget-object v4, v4, Lcom/android/server/location/gnss/exp/GnssRequestBean;->callbackType:Ljava/lang/Object;

    instance-of v4, v4, Landroid/location/ILocationListener;

    if-eqz v4, :cond_1

    instance-of v4, p3, Landroid/location/ILocationListener;

    if-eqz v4, :cond_1

    move-object v4, p3

    check-cast v4, Landroid/location/ILocationListener;

    .line 186
    invoke-interface {v4}, Landroid/location/ILocationListener;->asBinder()Landroid/os/IBinder;

    move-result-object v4

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/server/location/gnss/exp/GnssRequestBean;

    iget-object v5, v5, Lcom/android/server/location/gnss/exp/GnssRequestBean;->callbackType:Ljava/lang/Object;

    check-cast v5, Landroid/location/ILocationListener;

    invoke-interface {v5}, Landroid/location/ILocationListener;->asBinder()Landroid/os/IBinder;

    move-result-object v5

    if-eq v4, v5, :cond_2

    .line 187
    :cond_1
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/location/gnss/exp/GnssRequestBean;

    iget-object v4, v4, Lcom/android/server/location/gnss/exp/GnssRequestBean;->callbackType:Ljava/lang/Object;

    invoke-virtual {p3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 188
    :cond_2
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/location/gnss/exp/GnssRequestBean;

    iget-object v4, v4, Lcom/android/server/location/gnss/exp/GnssRequestBean;->identity:Landroid/location/util/identity/CallerIdentity;

    invoke-virtual {v4}, Landroid/location/util/identity/CallerIdentity;->getUid()I

    move-result v4

    if-ne v4, p1, :cond_4

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/location/gnss/exp/GnssRequestBean;

    iget-object v4, v4, Lcom/android/server/location/gnss/exp/GnssRequestBean;->identity:Landroid/location/util/identity/CallerIdentity;

    invoke-virtual {v4}, Landroid/location/util/identity/CallerIdentity;->getPid()I

    move-result v4

    if-ne v4, p2, :cond_4

    .line 190
    iget-object v4, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mRequestMap:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 191
    iget-object v4, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mRemoveThreadMap:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Thread;

    .line 192
    .local v4, "thread":Ljava/lang/Thread;
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Ljava/lang/Thread;->interrupt()V

    .line 193
    :cond_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "removeByLmsUser: key:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    .end local v4    # "thread":Ljava/lang/Thread;
    goto :goto_1

    .line 195
    :cond_4
    const-string v4, "callbackType seem but not true uid pid.."

    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    .end local v3    # "map":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/location/gnss/exp/GnssRequestBean;>;"
    :cond_5
    :goto_1
    goto/16 :goto_0

    .line 200
    .end local v0    # "reqMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/location/gnss/exp/GnssRequestBean;>;"
    :cond_6
    return-void
.end method

.method public request(Landroid/content/Context;Ljava/lang/String;Landroid/location/util/identity/CallerIdentity;Landroid/location/LocationRequest;ZIZLjava/lang/Object;)Z
    .locals 14
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "provider"    # Ljava/lang/String;
    .param p3, "identity"    # Landroid/location/util/identity/CallerIdentity;
    .param p4, "locationRequest"    # Landroid/location/LocationRequest;
    .param p5, "foreground"    # Z
    .param p6, "permissionLevel"    # I
    .param p7, "hasLocationPermissions"    # Z
    .param p8, "callbackType"    # Ljava/lang/Object;

    .line 76
    move-object v7, p0

    move-object/from16 v8, p2

    sput-object p1, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mContext:Landroid/content/Context;

    .line 77
    iget-boolean v0, v7, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mAlreadyLoadDataFromSP:Z

    if-nez v0, :cond_0

    .line 78
    invoke-direct {p0}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->loadFeatureSwitch()V

    .line 81
    :cond_0
    invoke-virtual/range {p3 .. p3}, Landroid/location/util/identity/CallerIdentity;->getUid()I

    move-result v0

    invoke-virtual/range {p3 .. p3}, Landroid/location/util/identity/CallerIdentity;->getPid()I

    move-result v1

    invoke-virtual/range {p3 .. p3}, Landroid/location/util/identity/CallerIdentity;->getListenerId()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v8, v2}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->createReqKey(IILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 82
    .local v9, "requestKey":Ljava/lang/String;
    nop

    .line 83
    move-object v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p2

    move-object/from16 v3, p4

    move/from16 v4, p5

    move/from16 v5, p6

    move-object/from16 v6, p8

    invoke-direct/range {v0 .. v6}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->putGnssRequestBean(Landroid/location/util/identity/CallerIdentity;Ljava/lang/String;Landroid/location/LocationRequest;ZILjava/lang/Object;)Lcom/android/server/location/gnss/exp/GnssRequestBean;

    move-result-object v0

    .line 82
    const/4 v1, 0x0

    invoke-direct {p0, v9, v0, v1}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->putIntoRequestMap(Ljava/lang/String;Lcom/android/server/location/gnss/exp/GnssRequestBean;Z)V

    .line 85
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "request: mRequestMap add: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\nNow"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    move-object/from16 v2, p4

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " callType:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    move-object/from16 v3, p8

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " identity:"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    move-object/from16 v4, p3

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v5, "GnssBackgroundUsageOpt"

    invoke-static {v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    iget-object v0, v7, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mIsSpecifiedDevice:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    const/4 v6, 0x1

    if-eqz v0, :cond_4

    const-string v0, "gps"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 87
    iget-object v0, v7, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mBackOpt3Map:Ljava/util/Map;

    invoke-virtual/range {p3 .. p3}, Landroid/location/util/identity/CallerIdentity;->getUid()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 88
    .local v0, "backTime":Ljava/lang/Long;
    if-nez p5, :cond_1

    if-eqz v0, :cond_1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    sub-long/2addr v10, v12

    const-wide/16 v12, 0x2710

    cmp-long v10, v10, v12

    if-lez v10, :cond_1

    .line 90
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "remove by back opt 3.0:"

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    invoke-static {}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->getInstance()Lcom/android/server/location/gnss/GnssEventTrackingStub;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->recordGnssBackgroundOpt3Time()V

    .line 94
    iget-object v1, v7, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mRequestMap:Ljava/util/Map;

    invoke-interface {v1, v9}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    return v6

    .line 96
    :cond_1
    if-eqz p5, :cond_2

    .line 97
    iget-object v6, v7, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mBackOpt3Map:Ljava/util/Map;

    invoke-virtual/range {p3 .. p3}, Landroid/location/util/identity/CallerIdentity;->getUid()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v6, v10}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    :cond_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "normal request location key:"

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    if-nez p5, :cond_3

    .line 103
    const/16 v5, 0x2710

    invoke-direct {p0, v8, v9, v5}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->remove(Ljava/lang/String;Ljava/lang/String;I)V

    .line 105
    .end local v0    # "backTime":Ljava/lang/Long;
    :cond_3
    goto/16 :goto_0

    :cond_4
    iget-object v0, v7, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mIsInSatelliteCallMode:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 106
    iget-boolean v0, v7, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mIsUseUidCtl:Z

    const-string v10, " need to return..."

    if-eqz v0, :cond_5

    .line 107
    iget-object v0, v7, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mSatelliteCallAppUidSet:Ljava/util/HashSet;

    invoke-virtual/range {p3 .. p3}, Landroid/location/util/identity/CallerIdentity;->getUid()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v0, v11}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 110
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "request location by uid:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    return v6

    .line 114
    :cond_5
    iget-object v0, v7, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mSatelliteCallPkgSet:Ljava/util/HashSet;

    invoke-virtual/range {p3 .. p3}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v11}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 117
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "request location by pkg:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    return v6

    .line 122
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "request provider:"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, " key:"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    :cond_7
    :goto_0
    return v1
.end method

.method public setBackgroundOptStatus(Z)V
    .locals 2
    .param p1, "status"    # Z

    .line 293
    iget-object v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mIsSpecifiedDevice:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    const-string v1, "GnssBackgroundUsageOpt"

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 294
    invoke-direct {p0}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->registerProcessObserver()V

    .line 295
    const-string v0, "Has Register Process Observer by cloud..."

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    iget-object v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mIsSpecifiedDevice:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 297
    iget-object v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mIsSpecifiedDevice:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->saveCloudDataToSP(Z)V

    goto :goto_0

    .line 298
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mIsSpecifiedDevice:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez p1, :cond_1

    .line 299
    invoke-direct {p0}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->unRegisterProcessObserver()V

    .line 300
    const-string v0, "Has unRegister Process Observer by cloud..."

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    iget-object v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mIsSpecifiedDevice:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 302
    iget-object v0, p0, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->mIsSpecifiedDevice:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;->saveCloudDataToSP(Z)V

    .line 304
    :cond_1
    :goto_0
    return-void
.end method

.method public unRegisterSatelliteCallMode()V
    .locals 2

    .line 253
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl$$ExternalSyntheticLambda2;

    invoke-direct {v1, p0}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptImpl;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 276
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 277
    return-void
.end method
