public class com.android.server.location.gnss.GnssLocationProviderImpl implements com.android.server.location.gnss.GnssLocationProviderStub {
	 /* .source "GnssLocationProviderImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/location/gnss/GnssLocationProviderImpl$BlurLocationItem; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String ACTION_MODEM_LOCATION;
private static final java.lang.String CALLER_PACKAGE_NAME_ACTION;
private static final java.lang.String CLASS_PACKAGE_CONNECTIVITY_NPI;
private static final Boolean DEBUG;
private static Boolean ENABLE_FULL_TRACKING;
private static final java.lang.String EXTRA_NPS_NEW_EVENT;
private static final java.lang.String EXTRA_NPS_PACKAGE_NAME;
private static final java.lang.String GET_EVENT_ACTION;
private static final java.lang.String MIUI_NFW_PROXY_APP;
private static final java.lang.String RECEIVER_GNSS_CALLER_NAME_EVENT;
private static final java.lang.String RECEIVER_GNSS_EVENT;
private static final java.lang.String STR_LOCATIONFILE;
private static final java.lang.String TAG;
private static final java.lang.String XM_HP_LOCATION;
private static final Integer XM_HP_LOCATION_OFF;
private static final java.io.File mLocationFile;
private static final java.lang.Object mLocationInformationLock;
/* # instance fields */
private java.util.Stack blurLocationCollector;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Stack<", */
/* "Lcom/android/server/location/gnss/GnssLocationProviderImpl$BlurLocationItem;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.android.server.location.gnss.GnssLocationProviderImpl$BlurLocationItem blurLocationItem;
private com.android.server.location.MiuiBlurLocationManagerImpl mBlurLocation;
private java.lang.String mBlurPackageName;
private Integer mBlurState;
private Integer mBlurUid;
private android.content.Context mContext;
private Boolean mEdgnssUiSwitch;
private Boolean mEnableSendingState;
private com.android.server.location.GnssEventHandler mGnssEventHandler;
private Boolean mNoise;
private com.android.server.location.NewGnssEventHandler newGnssEventHandler;
private com.android.server.location.gnss.GnssLocationProviderImpl$BlurLocationItem popBlurLocationItem;
/* # direct methods */
static com.android.server.location.gnss.GnssLocationProviderImpl ( ) {
/* .locals 5 */
/* .line 52 */
final String v0 = "GnssLocationProviderImpl"; // const-string v0, "GnssLocationProviderImpl"
int v1 = 3; // const/4 v1, 0x3
v0 = android.util.Log .isLoggable ( v0,v1 );
com.android.server.location.gnss.GnssLocationProviderImpl.DEBUG = (v0!= 0);
/* .line 65 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
/* .line 87 */
int v1 = 0; // const/4 v1, 0x0
com.android.server.location.gnss.GnssLocationProviderImpl.ENABLE_FULL_TRACKING = (v1!= 0);
/* .line 93 */
/* monitor-enter v0 */
/* .line 94 */
try { // :try_start_0
/* new-instance v1, Ljava/io/File; */
com.android.server.location.gnss.GnssLocationProviderImpl .getSystemDir ( );
final String v3 = "locationinformation.txt"; // const-string v3, "locationinformation.txt"
/* invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* .line 95 */
/* .local v1, "file":Ljava/io/File; */
(( java.io.File ) v1 ).delete ( ); // invoke-virtual {v1}, Ljava/io/File;->delete()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 97 */
try { // :try_start_1
	 (( java.io.File ) v1 ).createNewFile ( ); // invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z
	 /* :try_end_1 */
	 /* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
	 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
	 /* .line 100 */
	 /* .line 98 */
	 /* :catch_0 */
	 /* move-exception v2 */
	 /* .line 99 */
	 /* .local v2, "e":Ljava/io/IOException; */
	 try { // :try_start_2
		 final String v3 = "IO error occurred!"; // const-string v3, "IO error occurred!"
		 com.android.server.location.gnss.GnssLocationProviderImpl .loge ( v3 );
		 /* .line 101 */
	 } // .end local v2 # "e":Ljava/io/IOException;
} // :goto_0
/* new-instance v2, Ljava/io/File; */
com.android.server.location.gnss.GnssLocationProviderImpl .getSystemDir ( );
final String v4 = "locationinformation.txt"; // const-string v4, "locationinformation.txt"
/* invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* .line 102 */
} // .end local v1 # "file":Ljava/io/File;
/* monitor-exit v0 */
/* .line 103 */
return;
/* .line 102 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
} // .end method
public com.android.server.location.gnss.GnssLocationProviderImpl ( ) {
/* .locals 2 */
/* .line 50 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 71 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mEnableSendingState:Z */
/* .line 73 */
/* iput-boolean v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mEdgnssUiSwitch:Z */
/* .line 74 */
/* iput-boolean v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mNoise:Z */
/* .line 76 */
int v1 = 0; // const/4 v1, 0x0
this.mGnssEventHandler = v1;
/* .line 77 */
this.newGnssEventHandler = v1;
/* .line 80 */
/* iput v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mBlurState:I */
return;
} // .end method
private Double dataFormat ( Double p0 ) {
/* .locals 4 */
/* .param p1, "data" # D */
/* .line 635 */
try { // :try_start_0
/* new-instance v0, Ljava/text/DecimalFormat; */
final String v1 = "0.00000"; // const-string v1, "0.00000"
/* invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V */
/* .line 636 */
/* .local v0, "df":Ljava/text/DecimalFormat; */
(( java.text.DecimalFormat ) v0 ).format ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;
/* .line 638 */
/* .local v1, "doubleNumAsString":Ljava/lang/String; */
final String v2 = ","; // const-string v2, ","
final String v3 = "."; // const-string v3, "."
(( java.lang.String ) v1 ).replace ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
java.lang.Double .valueOf ( v2 );
(( java.lang.Double ) v2 ).doubleValue ( ); // invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D
/* move-result-wide v2 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* return-wide v2 */
/* .line 639 */
} // .end local v0 # "df":Ljava/text/DecimalFormat;
} // .end local v1 # "doubleNumAsString":Ljava/lang/String;
/* :catch_0 */
/* move-exception v0 */
/* .line 640 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Exception here, print locale: "; // const-string v2, "Exception here, print locale: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.util.Locale .getDefault ( );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.location.gnss.GnssLocationProviderImpl .loge ( v1 );
/* .line 641 */
/* return-wide p1 */
} // .end method
private void deliverCallerNameIntent ( android.content.Context p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 419 */
if ( p1 != null) { // if-eqz p1, :cond_1
v0 = /* invoke-direct {p0}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->isChineseLanguage()Z */
/* if-nez v0, :cond_0 */
/* .line 420 */
} // :cond_0
/* invoke-direct {p0, p2}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->packageOnReceive(Ljava/lang/String;)V */
/* .line 421 */
return;
/* .line 419 */
} // :cond_1
} // :goto_0
return;
} // .end method
private void deliverIntent ( android.content.Context p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "event" # I */
/* .line 409 */
if ( p1 != null) { // if-eqz p1, :cond_1
v0 = /* invoke-direct {p0}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->isChineseLanguage()Z */
/* if-nez v0, :cond_0 */
/* .line 410 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, p2, v0}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->packageEventOnReceive(ILjava/lang/String;)V */
/* .line 411 */
return;
/* .line 409 */
} // :cond_1
} // :goto_0
return;
} // .end method
private void deliverIntentWithPackageName ( android.content.Context p0, java.lang.String p1, Integer p2 ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "event" # I */
/* .line 414 */
if ( p1 != null) { // if-eqz p1, :cond_1
v0 = /* invoke-direct {p0}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->isChineseLanguage()Z */
/* if-nez v0, :cond_0 */
/* .line 415 */
} // :cond_0
/* invoke-direct {p0, p3, p2}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->packageEventOnReceive(ILjava/lang/String;)V */
/* .line 416 */
return;
/* .line 414 */
} // :cond_1
} // :goto_0
return;
} // .end method
private java.lang.String getCurrentTime ( ) {
/* .locals 11 */
/* .line 463 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* .line 464 */
/* .local v0, "mNow":J */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 465 */
/* .local v2, "sb":Ljava/lang/StringBuilder; */
java.util.Calendar .getInstance ( );
/* .line 466 */
/* .local v9, "c":Ljava/util/Calendar; */
(( java.util.Calendar ) v9 ).setTimeInMillis ( v0, v1 ); // invoke-virtual {v9, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V
/* .line 467 */
final String v10 = "%tm-%td %tH:%tM:%tS.%tL"; // const-string v10, "%tm-%td %tH:%tM:%tS.%tL"
/* move-object v3, v9 */
/* move-object v4, v9 */
/* move-object v5, v9 */
/* move-object v6, v9 */
/* move-object v7, v9 */
/* move-object v8, v9 */
/* filled-new-array/range {v3 ..v8}, [Ljava/lang/Object; */
java.lang.String .format ( v10,v3 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 468 */
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
private java.lang.String getLocationFilePath ( ) {
/* .locals 2 */
/* .line 457 */
v0 = com.android.server.location.gnss.GnssLocationProviderImpl.mLocationInformationLock;
/* monitor-enter v0 */
/* .line 458 */
try { // :try_start_0
v1 = com.android.server.location.gnss.GnssLocationProviderImpl.mLocationFile;
(( java.io.File ) v1 ).getAbsolutePath ( ); // invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
/* monitor-exit v0 */
/* .line 459 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private static java.io.File getSystemDir ( ) {
/* .locals 3 */
/* .line 473 */
/* new-instance v0, Ljava/io/File; */
android.os.Environment .getDataDirectory ( );
/* const-string/jumbo v2, "system" */
/* invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
} // .end method
private Boolean isChineseLanguage ( ) {
/* .locals 2 */
/* .line 313 */
java.util.Locale .getDefault ( );
(( java.util.Locale ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;
/* .line 314 */
/* .local v0, "language":Ljava/lang/String; */
/* const-string/jumbo v1, "zh_CN" */
v1 = (( java.lang.String ) v0 ).endsWith ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z
} // .end method
private Boolean isEdgnssSwitchOn ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 501 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_0 */
/* .line 502 */
} // :cond_0
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v2, "xiaomi_high_precise_location" */
int v3 = 2; // const/4 v3, 0x2
v1 = android.provider.Settings$Secure .getInt ( v1,v2,v3 );
/* if-eq v1, v3, :cond_1 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
} // .end method
private void logd ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "string" # Ljava/lang/String; */
/* .line 650 */
/* sget-boolean v0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 651 */
final String v0 = "GnssLocationProviderImpl"; // const-string v0, "GnssLocationProviderImpl"
android.util.Log .d ( v0,p1 );
/* .line 653 */
} // :cond_0
return;
} // .end method
private static void loge ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p0, "string" # Ljava/lang/String; */
/* .line 646 */
final String v0 = "GnssLocationProviderImpl"; // const-string v0, "GnssLocationProviderImpl"
android.util.Log .e ( v0,p0 );
/* .line 647 */
return;
} // .end method
private void packageEventOnReceive ( Integer p0, java.lang.String p1 ) {
/* .locals 17 */
/* .param p1, "event" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 333 */
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p2 */
int v2 = 1; // const/4 v2, 0x1
/* .line 334 */
/* .local v2, "ACTION_START":I */
int v3 = 2; // const/4 v3, 0x2
/* .line 335 */
/* .local v3, "ACTION_STOP":I */
int v4 = 3; // const/4 v4, 0x3
/* .line 336 */
/* .local v4, "ACTION_FIX":I */
int v5 = 4; // const/4 v5, 0x4
/* .line 337 */
/* .local v5, "ACTION_LOSE":I */
int v6 = 5; // const/4 v6, 0x5
/* .line 339 */
/* .local v6, "ACTION_RECOVER":I */
int v7 = 6; // const/4 v7, 0x6
/* .line 340 */
/* .local v7, "ACTION_START_NEW":I */
int v8 = 7; // const/4 v8, 0x7
/* .line 341 */
/* .local v8, "ACTION_STOP_NEW":I */
/* const/16 v9, 0x8 */
/* .line 346 */
/* .local v9, "ACTION_GNSS_ENABLE_UPDATE":I */
final String v10 = "com.xiaomi.bsp.gps.nps.NewEvent"; // const-string v10, "com.xiaomi.bsp.gps.nps.NewEvent"
/* .line 347 */
/* .local v10, "NEW_EVENT_KEY":Ljava/lang/String; */
final String v11 = "com.xiaomi.bsp.gps.nps.PackageName"; // const-string v11, "com.xiaomi.bsp.gps.nps.PackageName"
/* .line 348 */
/* .local v11, "PACKAGE_NAME_KEY":Ljava/lang/String; */
v12 = this.mGnssEventHandler;
/* if-nez v12, :cond_0 */
/* .line 349 */
v12 = this.mContext;
com.android.server.location.GnssEventHandler .getInstance ( v12 );
this.mGnssEventHandler = v12;
/* .line 351 */
} // :cond_0
v12 = this.newGnssEventHandler;
/* if-nez v12, :cond_1 */
/* .line 352 */
v12 = this.mContext;
com.android.server.location.NewGnssEventHandler .getInstance ( v12 );
this.newGnssEventHandler = v12;
/* .line 354 */
} // :cond_1
/* move/from16 v12, p1 */
/* .line 355 */
/* .local v12, "intentExtra":I */
/* if-nez v12, :cond_2 */
/* .line 356 */
return;
/* .line 359 */
} // :cond_2
/* new-instance v13, Ljava/lang/StringBuilder; */
/* invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V */
final String v14 = "receive event "; // const-string v14, "receive event "
(( java.lang.StringBuilder ) v13 ).append ( v14 ); // invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).append ( v12 ); // invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v14 = ""; // const-string v14, ""
/* if-nez v1, :cond_3 */
/* move/from16 v16, v2 */
/* move-object v2, v14 */
} // :cond_3
/* new-instance v15, Ljava/lang/StringBuilder; */
/* invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V */
/* move/from16 v16, v2 */
} // .end local v2 # "ACTION_START":I
/* .local v16, "ACTION_START":I */
final String v2 = ","; // const-string v2, ","
(( java.lang.StringBuilder ) v15 ).append ( v2 ); // invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // :goto_0
(( java.lang.StringBuilder ) v13 ).append ( v2 ); // invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v13 = "GnssLocationProviderImpl"; // const-string v13, "GnssLocationProviderImpl"
android.util.Log .d ( v13,v2 );
/* .line 361 */
if ( v1 != null) { // if-eqz v1, :cond_4
/* invoke-virtual/range {p2 ..p2}, Ljava/lang/String;->trim()Ljava/lang/String; */
v2 = (( java.lang.String ) v2 ).equals ( v14 ); // invoke-virtual {v2, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_4 */
/* .line 362 */
/* packed-switch v12, :pswitch_data_0 */
/* .line 383 */
/* .line 370 */
/* :pswitch_0 */
v2 = this.newGnssEventHandler;
(( com.android.server.location.NewGnssEventHandler ) v2 ).handleUpdateGnssStatus ( ); // invoke-virtual {v2}, Lcom/android/server/location/NewGnssEventHandler;->handleUpdateGnssStatus()V
/* .line 371 */
/* .line 367 */
/* :pswitch_1 */
v2 = this.newGnssEventHandler;
(( com.android.server.location.NewGnssEventHandler ) v2 ).handleStop ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/location/NewGnssEventHandler;->handleStop(Ljava/lang/String;)V
/* .line 368 */
/* .line 364 */
/* :pswitch_2 */
v2 = this.newGnssEventHandler;
(( com.android.server.location.NewGnssEventHandler ) v2 ).handleStart ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/location/NewGnssEventHandler;->handleStart(Ljava/lang/String;)V
/* .line 365 */
/* .line 380 */
/* :pswitch_3 */
v2 = this.newGnssEventHandler;
int v13 = 0; // const/4 v13, 0x0
(( com.android.server.location.NewGnssEventHandler ) v2 ).handlerUpdateFixStatus ( v13 ); // invoke-virtual {v2, v13}, Lcom/android/server/location/NewGnssEventHandler;->handlerUpdateFixStatus(Z)V
/* .line 381 */
/* .line 376 */
/* :pswitch_4 */
v2 = this.newGnssEventHandler;
int v13 = 1; // const/4 v13, 0x1
(( com.android.server.location.NewGnssEventHandler ) v2 ).handlerUpdateFixStatus ( v13 ); // invoke-virtual {v2, v13}, Lcom/android/server/location/NewGnssEventHandler;->handlerUpdateFixStatus(Z)V
/* .line 377 */
/* .line 386 */
} // :cond_4
/* packed-switch v12, :pswitch_data_1 */
/* .line 401 */
/* :pswitch_5 */
/* .line 398 */
/* :pswitch_6 */
/* .line 395 */
/* :pswitch_7 */
/* .line 391 */
/* :pswitch_8 */
v2 = this.mGnssEventHandler;
(( com.android.server.location.GnssEventHandler ) v2 ).handleStop ( ); // invoke-virtual {v2}, Lcom/android/server/location/GnssEventHandler;->handleStop()V
/* .line 392 */
/* .line 389 */
/* :pswitch_9 */
/* nop */
/* .line 406 */
} // :goto_1
return;
/* :pswitch_data_0 */
/* .packed-switch 0x3 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_4 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
/* :pswitch_data_1 */
/* .packed-switch 0x1 */
/* :pswitch_9 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
} // .end packed-switch
} // .end method
private void packageOnReceive ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 319 */
/* new-instance v0, Ljava/util/ArrayList; */
final String v1 = "android"; // const-string v1, "android"
final String v2 = "com.xiaomi.location.fused"; // const-string v2, "com.xiaomi.location.fused"
/* filled-new-array {v1, v2}, [Ljava/lang/String; */
/* .line 320 */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* .line 321 */
/* .local v0, "mIgnoreNotifyPackage":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v1 = this.mGnssEventHandler;
/* if-nez v1, :cond_0 */
/* .line 322 */
v1 = this.mContext;
com.android.server.location.GnssEventHandler .getInstance ( v1 );
this.mGnssEventHandler = v1;
/* .line 324 */
} // :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "receive caller pkg name ="; // const-string v2, "receive caller pkg name ="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "GnssLocationProviderImpl"; // const-string v2, "GnssLocationProviderImpl"
android.util.Log .d ( v2,v1 );
/* .line 325 */
v1 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v1, :cond_2 */
v1 = /* .line 326 */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 329 */
} // :cond_1
v1 = this.mGnssEventHandler;
(( com.android.server.location.GnssEventHandler ) v1 ).handleCallerName ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/location/GnssEventHandler;->handleCallerName(Ljava/lang/String;)V
/* .line 330 */
return;
/* .line 327 */
} // :cond_2
} // :goto_0
return;
} // .end method
private java.lang.String precisionProcess ( java.lang.String p0 ) {
/* .locals 10 */
/* .param p1, "data" # Ljava/lang/String; */
/* .line 564 */
/* const/16 v0, 0x2e */
v0 = (( java.lang.String ) p1 ).lastIndexOf ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->lastIndexOf(I)I
/* .line 565 */
/* .local v0, "leng":I */
int v1 = -1; // const/4 v1, -0x1
/* if-ne v0, v1, :cond_0 */
/* .line 566 */
/* .line 568 */
} // :cond_0
/* add-int/lit8 v1, v0, -0x2 */
(( java.lang.String ) p1 ).substring ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;
/* .line 569 */
/* .local v1, "dataNeedProcessed":Ljava/lang/String; */
/* new-instance v2, Ljava/lang/StringBuilder; */
int v3 = 2; // const/4 v3, 0x2
/* invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V */
/* .line 570 */
/* .local v2, "sf":Ljava/lang/StringBuilder; */
/* add-int/lit8 v3, v0, -0x2 */
int v4 = 0; // const/4 v4, 0x0
(( java.lang.String ) p1 ).substring ( v4, v3 ); // invoke-virtual {p1, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;
/* .line 571 */
/* .local v3, "dataInvariant":Ljava/lang/String; */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 572 */
int v5 = 1; // const/4 v5, 0x1
(( java.lang.String ) v1 ).substring ( v4, v5 ); // invoke-virtual {v1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;
final String v6 = "0"; // const-string v6, "0"
v4 = (( java.lang.String ) v4 ).equals ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 574 */
(( java.lang.StringBuilder ) v2 ).append ( v6 ); // invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 576 */
} // :cond_1
java.lang.Double .valueOf ( v1 );
(( java.lang.Double ) v4 ).doubleValue ( ); // invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D
/* move-result-wide v6 */
/* const-wide/high16 v8, 0x404e000000000000L # 60.0 */
/* div-double/2addr v6, v8 */
java.math.BigDecimal .valueOf ( v6,v7 );
int v6 = 5; // const/4 v6, 0x5
(( java.math.BigDecimal ) v4 ).setScale ( v6, v5 ); // invoke-virtual {v4, v6, v5}, Ljava/math/BigDecimal;->setScale(II)Ljava/math/BigDecimal;
/* .line 577 */
/* .local v4, "bigDecimal":Ljava/math/BigDecimal; */
/* new-instance v5, Ljava/math/BigDecimal; */
/* const/16 v6, 0x3c */
/* invoke-direct {v5, v6}, Ljava/math/BigDecimal;-><init>(I)V */
(( java.math.BigDecimal ) v4 ).multiply ( v5 ); // invoke-virtual {v4, v5}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;
(( java.math.BigDecimal ) v5 ).doubleValue ( ); // invoke-virtual {v5}, Ljava/math/BigDecimal;->doubleValue()D
/* move-result-wide v5 */
/* .line 578 */
/* .local v5, "dataProcessed":D */
(( java.lang.StringBuilder ) v2 ).append ( v5, v6 ); // invoke-virtual {v2, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
/* .line 579 */
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
/* # virtual methods */
public void dump ( java.io.PrintWriter p0 ) {
/* .locals 2 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 245 */
(( com.android.server.location.gnss.GnssLocationProviderImpl ) p0 ).loadLocationInformation ( ); // invoke-virtual {p0}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->loadLocationInformation()Ljava/lang/StringBuilder;
(( java.io.PrintWriter ) p1 ).append ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;
/* .line 247 */
final String v0 = "GLP information:"; // const-string v0, "GLP information:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 248 */
com.android.server.location.LocationDumpLogStub .getInstance ( );
int v1 = 2; // const/4 v1, 0x2
/* .line 250 */
final String v0 = "NMEA information:"; // const-string v0, "NMEA information:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 251 */
com.android.server.location.LocationDumpLogStub .getInstance ( );
int v1 = 4; // const/4 v1, 0x4
/* .line 252 */
return;
} // .end method
public java.lang.String getConfig ( java.util.Properties p0, java.lang.String p1, java.lang.String p2 ) {
/* .locals 1 */
/* .param p1, "properties" # Ljava/util/Properties; */
/* .param p2, "config" # Ljava/lang/String; */
/* .param p3, "defaultConfig" # Ljava/lang/String; */
/* .line 158 */
(( java.util.Properties ) p1 ).getProperty ( p2, p3 ); // invoke-virtual {p1, p2, p3}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
} // .end method
public android.location.Location getLowerAccLocation ( android.location.Location p0 ) {
/* .locals 2 */
/* .param p1, "location" # Landroid/location/Location; */
/* .line 584 */
/* iget-boolean v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mEdgnssUiSwitch:Z */
/* if-nez v0, :cond_0 */
/* .line 585 */
/* .line 587 */
} // :cond_0
(( android.location.Location ) p1 ).getLatitude ( ); // invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D
/* move-result-wide v0 */
/* invoke-direct {p0, v0, v1}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->dataFormat(D)D */
/* move-result-wide v0 */
(( android.location.Location ) p1 ).setLatitude ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Landroid/location/Location;->setLatitude(D)V
/* .line 588 */
(( android.location.Location ) p1 ).getLongitude ( ); // invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D
/* move-result-wide v0 */
/* invoke-direct {p0, v0, v1}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->dataFormat(D)D */
/* move-result-wide v0 */
(( android.location.Location ) p1 ).setLongitude ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Landroid/location/Location;->setLongitude(D)V
/* .line 589 */
} // .end method
public Boolean getSendingSwitch ( ) {
/* .locals 1 */
/* .line 123 */
/* iget-boolean v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mEnableSendingState:Z */
} // .end method
public void handleDisable ( ) {
/* .locals 3 */
/* .line 184 */
com.android.server.location.LocationDumpLogStub .getInstance ( );
/* .line 186 */
com.android.server.location.LocationDumpLogStub .getInstance ( );
int v1 = 2; // const/4 v1, 0x2
final String v2 = "disable gnss"; // const-string v2, "disable gnss"
/* .line 188 */
com.android.server.location.gnss.hal.GnssPowerOptimizeStub .getInstance ( );
/* .line 190 */
(( com.android.server.location.gnss.GnssLocationProviderImpl ) p0 ).writeLocationInformation ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->writeLocationInformation(Ljava/lang/String;)V
/* .line 191 */
return;
} // .end method
public void handleEnable ( ) {
/* .locals 3 */
/* .line 176 */
com.android.server.location.LocationDumpLogStub .getInstance ( );
int v1 = 2; // const/4 v1, 0x2
final String v2 = "enable gnss"; // const-string v2, "enable gnss"
/* .line 178 */
(( com.android.server.location.gnss.GnssLocationProviderImpl ) p0 ).writeLocationInformation ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->writeLocationInformation(Ljava/lang/String;)V
/* .line 179 */
return;
} // .end method
public void handleInitialize ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 107 */
this.mContext = p1;
/* .line 109 */
com.android.server.location.gnss.hal.GnssPowerOptimizeStub .getInstance ( );
/* .line 111 */
com.android.server.location.gnss.GnssEventTrackingStub .getInstance ( );
/* .line 113 */
com.android.server.location.LocationExtCooperateStub .getInstance ( );
/* .line 115 */
com.android.server.location.LocationDumpLogStub .getInstance ( );
int v1 = 2; // const/4 v1, 0x2
final String v2 = "initialize glp"; // const-string v2, "initialize glp"
/* .line 117 */
com.android.server.location.gnss.GnssCollectDataStub .getInstance ( );
int v1 = 0; // const/4 v1, 0x0
int v3 = 0; // const/4 v3, 0x0
/* .line 118 */
(( com.android.server.location.gnss.GnssLocationProviderImpl ) p0 ).writeLocationInformation ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->writeLocationInformation(Ljava/lang/String;)V
/* .line 119 */
return;
} // .end method
public void handleReportLocation ( Integer p0, android.location.Location p1 ) {
/* .locals 4 */
/* .param p1, "ttff" # I */
/* .param p2, "location" # Landroid/location/Location; */
/* .line 295 */
com.android.server.location.LocationDumpLogStub .getInstance ( );
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "TTFF is "; // const-string v2, "TTFF is "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v2 = 2; // const/4 v2, 0x2
/* .line 297 */
/* sget-boolean v0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->ENABLE_FULL_TRACKING:Z */
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* .line 298 */
int v0 = 0; // const/4 v0, 0x0
com.android.server.location.gnss.GnssLocationProviderImpl.ENABLE_FULL_TRACKING = (v0!= 0);
/* .line 301 */
} // :cond_0
com.android.server.location.LocationDumpLogStub .getInstance ( );
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "the first location is " */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v3 = 3; // const/4 v3, 0x3
/* .line 304 */
final String v0 = "fix location"; // const-string v0, "fix location"
(( com.android.server.location.gnss.GnssLocationProviderImpl ) p0 ).writeLocationInformation ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->writeLocationInformation(Ljava/lang/String;)V
/* .line 308 */
com.android.server.location.gnss.GnssCollectDataStub .getInstance ( );
int v1 = 0; // const/4 v1, 0x0
/* .line 310 */
return;
} // .end method
public void handleReportSvStatus ( android.location.GnssStatus p0, Long p1, android.location.provider.ProviderRequest p2, com.android.server.location.gnss.hal.GnssNative p3 ) {
/* .locals 10 */
/* .param p1, "gnssStatus" # Landroid/location/GnssStatus; */
/* .param p2, "lastFixTime" # J */
/* .param p4, "mProviderRequest" # Landroid/location/provider/ProviderRequest; */
/* .param p5, "mGnssNative" # Lcom/android/server/location/gnss/hal/GnssNative; */
/* .line 196 */
v6 = (( android.location.GnssStatus ) p1 ).getSatelliteCount ( ); // invoke-virtual {p1}, Landroid/location/GnssStatus;->getSatelliteCount()I
/* .line 197 */
/* .local v6, "svCount":I */
/* new-array v7, v6, [F */
/* .line 198 */
/* .local v7, "cn0s":[F */
/* new-array v8, v6, [F */
/* .line 199 */
/* .local v8, "svCarrierFre":[F */
/* new-array v9, v6, [F */
/* .line 200 */
/* .local v9, "svConstellation":[F */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
/* if-ge v0, v6, :cond_1 */
/* .line 201 */
v1 = (( android.location.GnssStatus ) p1 ).getCn0DbHz ( v0 ); // invoke-virtual {p1, v0}, Landroid/location/GnssStatus;->getCn0DbHz(I)F
/* aput v1, v7, v0 */
/* .line 202 */
v1 = (( android.location.GnssStatus ) p1 ).getCarrierFrequencyHz ( v0 ); // invoke-virtual {p1, v0}, Landroid/location/GnssStatus;->getCarrierFrequencyHz(I)F
/* aput v1, v8, v0 */
/* .line 203 */
v1 = (( android.location.GnssStatus ) p1 ).usedInFix ( v0 ); // invoke-virtual {p1, v0}, Landroid/location/GnssStatus;->usedInFix(I)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 204 */
v1 = (( android.location.GnssStatus ) p1 ).getConstellationType ( v0 ); // invoke-virtual {p1, v0}, Landroid/location/GnssStatus;->getConstellationType(I)I
/* int-to-float v1, v1 */
/* aput v1, v9, v0 */
/* .line 200 */
} // :cond_0
/* add-int/lit8 v0, v0, 0x1 */
/* .line 207 */
} // .end local v0 # "i":I
} // :cond_1
com.android.server.location.gnss.GnssCollectDataStub .getInstance ( );
/* const/16 v1, 0xa */
/* move-object v2, v7 */
/* move v3, v6 */
/* move-object v4, v8 */
/* move-object v5, v9 */
/* invoke-interface/range {v0 ..v5}, Lcom/android/server/location/gnss/GnssCollectDataStub;->savePoint(I[FI[F[F)V */
/* .line 211 */
com.android.server.location.provider.AmapCustomStub .getInstance ( );
/* .line 214 */
/* const-wide/16 v0, 0x0 */
/* cmp-long v0, p2, v0 */
if ( v0 != null) { // if-eqz v0, :cond_4
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v0 */
/* sub-long/2addr v0, p2 */
/* const-wide/16 v2, 0xbb8 */
/* cmp-long v0, v0, v2 */
/* if-lez v0, :cond_4 */
/* .line 215 */
v0 = com.android.server.location.LocationDumpLogStub .getInstance ( );
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 216 */
com.android.server.location.LocationDumpLogStub .getInstance ( );
int v1 = 2; // const/4 v1, 0x2
final String v2 = "lose location, record the latest SV status "; // const-string v2, "lose location, record the latest SV status "
/* .line 218 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 219 */
/* .local v0, "statusInfos":Ljava/lang/StringBuilder; */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_1
v2 = (( android.location.GnssStatus ) p1 ).getSatelliteCount ( ); // invoke-virtual {p1}, Landroid/location/GnssStatus;->getSatelliteCount()I
/* if-ge v1, v2, :cond_2 */
/* .line 220 */
v2 = (( android.location.GnssStatus ) p1 ).getSvid ( v1 ); // invoke-virtual {p1, v1}, Landroid/location/GnssStatus;->getSvid(I)I
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ","; // const-string v3, ","
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = (( android.location.GnssStatus ) p1 ).getCn0DbHz ( v1 ); // invoke-virtual {p1, v1}, Landroid/location/GnssStatus;->getCn0DbHz(I)F
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 219 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 222 */
} // .end local v1 # "i":I
} // :cond_2
com.android.server.location.LocationDumpLogStub .getInstance ( );
/* .line 223 */
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 222 */
int v3 = 3; // const/4 v3, 0x3
/* .line 224 */
/* sget-boolean v1, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->ENABLE_FULL_TRACKING:Z */
/* if-nez v1, :cond_3 */
/* .line 225 */
com.android.server.location.gnss.gnssSelfRecovery.GnssSelfRecoveryStub .getInstance ( );
v1 = (( android.location.provider.ProviderRequest ) p4 ).getWorkSource ( ); // invoke-virtual {p4}, Landroid/location/provider/ProviderRequest;->getWorkSource()Landroid/os/WorkSource;
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 226 */
int v1 = 1; // const/4 v1, 0x1
com.android.server.location.gnss.GnssLocationProviderImpl.ENABLE_FULL_TRACKING = (v1!= 0);
/* .line 229 */
} // :cond_3
com.android.server.location.LocationDumpLogStub .getInstance ( );
int v2 = 0; // const/4 v2, 0x0
/* .line 231 */
final String v1 = "lose location"; // const-string v1, "lose location"
(( com.android.server.location.gnss.GnssLocationProviderImpl ) p0 ).writeLocationInformation ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->writeLocationInformation(Ljava/lang/String;)V
/* .line 232 */
com.android.server.location.gnss.GnssCollectDataStub .getInstance ( );
int v2 = 4; // const/4 v2, 0x4
int v3 = 0; // const/4 v3, 0x0
/* .line 239 */
} // .end local v0 # "statusInfos":Ljava/lang/StringBuilder;
} // :cond_4
com.android.server.location.GnssSmartSatelliteSwitchStub .getInstance ( );
/* .line 240 */
return;
} // .end method
public android.location.LocationRequest$Builder handleRequestLocation ( Boolean p0, android.location.LocationRequest$Builder p1, java.lang.String p2 ) {
/* .locals 3 */
/* .param p1, "canBypass" # Z */
/* .param p2, "locationRequest" # Landroid/location/LocationRequest$Builder; */
/* .param p3, "provider" # Ljava/lang/String; */
/* .line 143 */
com.android.server.location.LocationDumpLogStub .getInstance ( );
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "request location from HAL using provider "; // const-string v2, "request location from HAL using provider "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v2 = 2; // const/4 v2, 0x2
/* .line 146 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 147 */
int v0 = 1; // const/4 v0, 0x1
(( android.location.LocationRequest$Builder ) p2 ).setLocationSettingsIgnored ( v0 ); // invoke-virtual {p2, v0}, Landroid/location/LocationRequest$Builder;->setLocationSettingsIgnored(Z)Landroid/location/LocationRequest$Builder;
/* .line 148 */
(( android.location.LocationRequest$Builder ) p2 ).setMaxUpdates ( v2 ); // invoke-virtual {p2, v2}, Landroid/location/LocationRequest$Builder;->setMaxUpdates(I)Landroid/location/LocationRequest$Builder;
/* .line 149 */
final String v0 = "Bypass All Modem request."; // const-string v0, "Bypass All Modem request."
/* .line 150 */
/* .local v0, "passInfo":Ljava/lang/String; */
final String v1 = "GnssLocationProviderImpl"; // const-string v1, "GnssLocationProviderImpl"
android.util.Log .i ( v1,v0 );
/* .line 151 */
com.android.server.location.LocationDumpLogStub .getInstance ( );
/* .line 153 */
} // .end local v0 # "passInfo":Ljava/lang/String;
} // :cond_0
} // .end method
public Boolean hasLocationPermission ( android.content.pm.PackageManager p0, java.lang.String p1, android.content.Context p2 ) {
/* .locals 4 */
/* .param p1, "packageManager" # Landroid/content/pm/PackageManager; */
/* .param p2, "pkgName" # Ljava/lang/String; */
/* .param p3, "context" # Landroid/content/Context; */
/* .line 487 */
/* nop */
/* .line 488 */
(( android.content.Context ) p3 ).getUser ( ); // invoke-virtual {p3}, Landroid/content/Context;->getUser()Landroid/os/UserHandle;
/* .line 487 */
final String v1 = "com.miui.securitycenter.permission.modem_location"; // const-string v1, "com.miui.securitycenter.permission.modem_location"
v0 = (( android.content.pm.PackageManager ) p1 ).getPermissionFlags ( v1, p2, v0 ); // invoke-virtual {p1, v1, p2, v0}, Landroid/content/pm/PackageManager;->getPermissionFlags(Ljava/lang/String;Ljava/lang/String;Landroid/os/UserHandle;)I
/* .line 489 */
/* .local v0, "flags":I */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "flags in nfw app is "; // const-string v3, "flags in nfw app is "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.location.gnss.GnssLocationProviderImpl .loge ( v2 );
/* .line 490 */
v1 = (( android.content.pm.PackageManager ) p1 ).checkPermission ( v1, p2 ); // invoke-virtual {p1, v1, p2}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I
if ( v1 != null) { // if-eqz v1, :cond_1
/* and-int/lit8 v1, v0, 0x2 */
/* if-nez v1, :cond_0 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :cond_1
} // :goto_0
int v1 = 1; // const/4 v1, 0x1
} // :goto_1
} // .end method
public void ifNoiseEnvironment ( java.lang.String p0 ) {
/* .locals 7 */
/* .param p1, "nmea" # Ljava/lang/String; */
/* .line 507 */
final String v0 = "PQWM1"; // const-string v0, "PQWM1"
v1 = (( java.lang.String ) p1 ).contains ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v1, :cond_0 */
/* .line 508 */
return;
/* .line 510 */
} // :cond_0
final String v1 = ","; // const-string v1, ","
(( java.lang.String ) p1 ).split ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 512 */
/* .local v1, "nmeaSplit":[Ljava/lang/String; */
int v2 = 0; // const/4 v2, 0x0
/* aget-object v3, v1, v2 */
v0 = (( java.lang.String ) v3 ).indexOf ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
int v3 = -1; // const/4 v3, -0x1
/* if-eq v0, v3, :cond_5 */
/* .line 513 */
/* array-length v0, v1 */
int v3 = 3; // const/4 v3, 0x3
/* if-lt v0, v3, :cond_4 */
/* const/16 v0, 0x9 */
/* aget-object v3, v1, v0 */
v3 = android.text.TextUtils .isEmpty ( v3 );
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 516 */
} // :cond_1
/* aget-object v0, v1, v0 */
v0 = java.lang.Integer .parseInt ( v0 );
/* .line 517 */
/* .local v0, "value":I */
int v3 = 1; // const/4 v3, 0x1
/* const/16 v4, -0xc */
/* const/16 v5, 0x12 */
/* if-eq v0, v5, :cond_2 */
/* if-ne v0, v4, :cond_3 */
} // :cond_2
/* iget-boolean v6, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mNoise:Z */
/* if-nez v6, :cond_3 */
/* .line 518 */
final String v2 = "noise_environment"; // const-string v2, "noise_environment"
(( com.android.server.location.gnss.GnssLocationProviderImpl ) p0 ).notifyCallerName ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->notifyCallerName(Ljava/lang/String;)V
/* .line 519 */
/* iput-boolean v3, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mNoise:Z */
/* .line 520 */
} // :cond_3
/* iget-boolean v6, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mNoise:Z */
/* if-ne v6, v3, :cond_5 */
/* if-eq v0, v5, :cond_5 */
/* if-eq v0, v4, :cond_5 */
/* .line 521 */
/* iput-boolean v2, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mNoise:Z */
/* .line 522 */
final String v2 = "normal_environment"; // const-string v2, "normal_environment"
(( com.android.server.location.gnss.GnssLocationProviderImpl ) p0 ).notifyCallerName ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->notifyCallerName(Ljava/lang/String;)V
/* .line 514 */
} // .end local v0 # "value":I
} // :cond_4
} // :goto_0
return;
/* .line 525 */
} // :cond_5
} // :goto_1
return;
} // .end method
public java.lang.StringBuilder loadLocationInformation ( ) {
/* .locals 6 */
/* .line 439 */
final String v0 = "loadLocationInformation"; // const-string v0, "loadLocationInformation"
/* invoke-direct {p0, v0}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->logd(Ljava/lang/String;)V */
/* .line 440 */
v0 = com.android.server.location.gnss.GnssLocationProviderImpl.mLocationInformationLock;
/* monitor-enter v0 */
/* .line 441 */
try { // :try_start_0
/* new-instance v1, Ljava/io/BufferedReader; */
/* new-instance v2, Ljava/io/FileReader; */
v3 = com.android.server.location.gnss.GnssLocationProviderImpl.mLocationFile;
/* invoke-direct {v2, v3}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V */
/* invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_2 */
/* .line 443 */
/* .local v1, "reader":Ljava/io/BufferedReader; */
try { // :try_start_1
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 444 */
/* .local v2, "stirngBuidler":Ljava/lang/StringBuilder; */
final String v3 = "LocationInformation:"; // const-string v3, "LocationInformation:"
/* .line 446 */
/* .local v3, "line":Ljava/lang/String; */
} // :cond_0
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "\n"; // const-string v5, "\n"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 447 */
(( java.io.BufferedReader ) v1 ).readLine ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* move-object v3, v4 */
/* if-nez v4, :cond_0 */
/* .line 448 */
/* nop */
/* .line 449 */
try { // :try_start_2
(( java.io.BufferedReader ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_2 */
try { // :try_start_3
/* monitor-exit v0 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_2 */
/* .line 448 */
/* .line 441 */
} // .end local v2 # "stirngBuidler":Ljava/lang/StringBuilder;
} // .end local v3 # "line":Ljava/lang/String;
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_4
(( java.io.BufferedReader ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* :catchall_1 */
/* move-exception v3 */
try { // :try_start_5
(( java.lang.Throwable ) v2 ).addSuppressed ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local p0 # "this":Lcom/android/server/location/gnss/GnssLocationProviderImpl;
} // :goto_0
/* throw v2 */
/* :try_end_5 */
/* .catch Ljava/io/IOException; {:try_start_5 ..:try_end_5} :catch_0 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_2 */
/* .line 453 */
} // .end local v1 # "reader":Ljava/io/BufferedReader;
/* .restart local p0 # "this":Lcom/android/server/location/gnss/GnssLocationProviderImpl; */
/* :catchall_2 */
/* move-exception v1 */
/* .line 449 */
/* :catch_0 */
/* move-exception v1 */
/* .line 450 */
/* .local v1, "e":Ljava/io/IOException; */
try { // :try_start_6
final String v2 = "IO exception"; // const-string v2, "IO exception"
com.android.server.location.gnss.GnssLocationProviderImpl .loge ( v2 );
/* .line 452 */
} // .end local v1 # "e":Ljava/io/IOException;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* monitor-exit v0 */
/* .line 453 */
} // :goto_1
/* monitor-exit v0 */
/* :try_end_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_2 */
/* throw v1 */
} // .end method
public void notifyCallerName ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .line 256 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "caller name: "; // const-string v1, "caller name: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->logd(Ljava/lang/String;)V */
/* .line 257 */
v0 = this.mContext;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 258 */
/* invoke-direct {p0, v0, p1}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->deliverCallerNameIntent(Landroid/content/Context;Ljava/lang/String;)V */
/* .line 260 */
v0 = this.mContext;
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->isEdgnssSwitchOn(Landroid/content/Context;)Z */
/* iput-boolean v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mEdgnssUiSwitch:Z */
/* .line 261 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Edgnss Switch now is "; // const-string v1, "Edgnss Switch now is "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mEdgnssUiSwitch:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->logd(Ljava/lang/String;)V */
/* .line 263 */
} // :cond_0
return;
} // .end method
public void notifyCallerName ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .param p2, "eventType" # Ljava/lang/String; */
/* .line 267 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "caller name: "; // const-string v1, "caller name: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", eventType"; // const-string v1, ", eventType"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->logd(Ljava/lang/String;)V */
/* .line 268 */
/* if-nez p2, :cond_0 */
/* .line 269 */
(( com.android.server.location.gnss.GnssLocationProviderImpl ) p0 ).notifyCallerName ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->notifyCallerName(Ljava/lang/String;)V
/* .line 270 */
return;
/* .line 272 */
} // :cond_0
v0 = this.mContext;
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 274 */
try { // :try_start_0
v1 = this.mGnssEventHandler;
/* if-nez v1, :cond_1 */
/* .line 275 */
com.android.server.location.GnssEventHandler .getInstance ( v0 );
this.mGnssEventHandler = v0;
/* .line 277 */
} // :cond_1
v0 = this.blurLocationCollector;
/* if-nez v0, :cond_2 */
/* .line 278 */
/* new-instance v0, Ljava/util/Stack; */
/* invoke-direct {v0}, Ljava/util/Stack;-><init>()V */
this.blurLocationCollector = v0;
/* .line 280 */
} // :cond_2
final String v0 = "blurLocation_notify is on"; // const-string v0, "blurLocation_notify is on"
v0 = (( java.lang.String ) p2 ).equals ( v0 ); // invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 281 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getPackageManager ( ); // invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
(( android.content.pm.PackageManager ) v0 ).getApplicationInfo ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
/* iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I */
(( com.android.server.location.gnss.GnssLocationProviderImpl ) p0 ).showBlurNotification ( v0, p1 ); // invoke-virtual {p0, v0, p1}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->showBlurNotification(ILjava/lang/String;)V
/* .line 282 */
} // :cond_3
final String v0 = "blurLocation_notify is off"; // const-string v0, "blurLocation_notify is off"
v0 = (( java.lang.String ) p2 ).equals ( v0 ); // invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 283 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getPackageManager ( ); // invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
(( android.content.pm.PackageManager ) v0 ).getApplicationInfo ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
/* iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I */
(( com.android.server.location.gnss.GnssLocationProviderImpl ) p0 ).removeBlurNotification ( v0, p1 ); // invoke-virtual {p0, v0, p1}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->removeBlurNotification(ILjava/lang/String;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 287 */
} // :cond_4
} // :goto_0
/* .line 285 */
/* :catch_0 */
/* move-exception v0 */
/* .line 286 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "exception"; // const-string v1, "exception"
com.android.server.location.gnss.GnssLocationProviderImpl .loge ( v1 );
/* .line 290 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_5
} // :goto_1
return;
} // .end method
public void notifyState ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "event" # I */
/* .line 163 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "gps now on "; // const-string v1, "gps now on "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->logd(Ljava/lang/String;)V */
/* .line 164 */
v0 = this.mContext;
/* invoke-direct {p0, v0, p1}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->deliverIntent(Landroid/content/Context;I)V */
/* .line 165 */
return;
} // .end method
public void notifyStateWithPackageName ( java.lang.String p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "event" # I */
/* .line 169 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "notify state, event:"; // const-string v1, "notify state, event:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", "; // const-string v1, ", "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->logd(Ljava/lang/String;)V */
/* .line 170 */
v0 = this.mContext;
/* invoke-direct {p0, v0, p1, p2}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->deliverIntentWithPackageName(Landroid/content/Context;Ljava/lang/String;I)V */
/* .line 171 */
return;
} // .end method
public java.lang.String precisionProcessByType ( java.lang.String p0 ) {
/* .locals 7 */
/* .param p1, "nmea" # Ljava/lang/String; */
/* .line 535 */
(( com.android.server.location.gnss.GnssLocationProviderImpl ) p0 ).ifNoiseEnvironment ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->ifNoiseEnvironment(Ljava/lang/String;)V
/* .line 536 */
/* iget-boolean v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mEdgnssUiSwitch:Z */
/* if-nez v0, :cond_0 */
/* .line 537 */
/* .line 540 */
} // :cond_0
final String v0 = ","; // const-string v0, ","
(( java.lang.String ) p1 ).split ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 542 */
/* .local v1, "nmeaSplit":[Ljava/lang/String; */
int v2 = 0; // const/4 v2, 0x0
/* aget-object v3, v1, v2 */
final String v4 = "GGA"; // const-string v4, "GGA"
v3 = (( java.lang.String ) v3 ).indexOf ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
int v4 = -1; // const/4 v4, -0x1
/* if-eq v3, v4, :cond_1 */
int v3 = 2; // const/4 v3, 0x2
/* aget-object v5, v1, v3 */
v5 = android.text.TextUtils .isEmpty ( v5 );
/* if-nez v5, :cond_1 */
/* .line 544 */
/* aget-object v5, v1, v3 */
/* invoke-direct {p0, v5}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->precisionProcess(Ljava/lang/String;)Ljava/lang/String; */
/* aput-object v5, v1, v3 */
/* .line 545 */
int v3 = 4; // const/4 v3, 0x4
/* aget-object v5, v1, v3 */
/* invoke-direct {p0, v5}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->precisionProcess(Ljava/lang/String;)Ljava/lang/String; */
/* aput-object v5, v1, v3 */
/* .line 547 */
} // :cond_1
/* aget-object v3, v1, v2 */
final String v5 = "RMC"; // const-string v5, "RMC"
v3 = (( java.lang.String ) v3 ).indexOf ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
int v5 = 3; // const/4 v5, 0x3
/* if-eq v3, v4, :cond_2 */
/* aget-object v3, v1, v5 */
v3 = android.text.TextUtils .isEmpty ( v3 );
/* if-nez v3, :cond_2 */
/* .line 548 */
/* aget-object v3, v1, v5 */
/* invoke-direct {p0, v3}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->precisionProcess(Ljava/lang/String;)Ljava/lang/String; */
/* aput-object v3, v1, v5 */
/* .line 549 */
int v3 = 5; // const/4 v3, 0x5
/* aget-object v6, v1, v3 */
/* invoke-direct {p0, v6}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->precisionProcess(Ljava/lang/String;)Ljava/lang/String; */
/* aput-object v6, v1, v3 */
/* .line 551 */
} // :cond_2
/* aget-object v2, v1, v2 */
final String v3 = "GLL"; // const-string v3, "GLL"
v2 = (( java.lang.String ) v2 ).indexOf ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
/* if-eq v2, v4, :cond_3 */
int v2 = 1; // const/4 v2, 0x1
/* aget-object v3, v1, v2 */
v3 = android.text.TextUtils .isEmpty ( v3 );
/* if-nez v3, :cond_3 */
/* .line 552 */
/* aget-object v3, v1, v2 */
/* invoke-direct {p0, v3}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->precisionProcess(Ljava/lang/String;)Ljava/lang/String; */
/* aput-object v3, v1, v2 */
/* .line 553 */
/* aget-object v2, v1, v5 */
/* invoke-direct {p0, v2}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->precisionProcess(Ljava/lang/String;)Ljava/lang/String; */
/* aput-object v2, v1, v5 */
/* .line 555 */
} // :cond_3
java.lang.String .join ( v0,v1 );
/* .line 556 */
/* .local v0, "nmeaEnd":Ljava/lang/String; */
} // .end method
public void reloadGpsProperties ( com.android.server.location.gnss.GnssConfiguration p0 ) {
/* .locals 5 */
/* .param p1, "gnssConfiguration" # Lcom/android/server/location/gnss/GnssConfiguration; */
/* .line 133 */
com.android.server.location.LocationDumpLogStub .getInstance ( );
/* .line 134 */
com.android.server.location.LocationDumpLogStub .getInstance ( );
/* .line 135 */
(( com.android.server.location.gnss.GnssConfiguration ) p1 ).getProperties ( ); // invoke-virtual {p1}, Lcom/android/server/location/gnss/GnssConfiguration;->getProperties()Ljava/util/Properties;
/* .line 134 */
final String v3 = "NMEA_LEN"; // const-string v3, "NMEA_LEN"
final String v4 = "20000"; // const-string v4, "20000"
v1 = java.lang.Integer .parseInt ( v1 );
/* .line 133 */
int v2 = 4; // const/4 v2, 0x4
/* .line 137 */
/* nop */
/* .line 138 */
(( com.android.server.location.gnss.GnssConfiguration ) p1 ).getProperties ( ); // invoke-virtual {p1}, Lcom/android/server/location/gnss/GnssConfiguration;->getProperties()Ljava/util/Properties;
final String v1 = "ENABLE_NOTIFY"; // const-string v1, "ENABLE_NOTIFY"
final String v2 = "false"; // const-string v2, "false"
(( com.android.server.location.gnss.GnssLocationProviderImpl ) p0 ).getConfig ( v0, v1, v2 ); // invoke-virtual {p0, v0, v1, v2}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->getConfig(Ljava/util/Properties;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
/* .line 137 */
v0 = java.lang.Boolean .parseBoolean ( v0 );
(( com.android.server.location.gnss.GnssLocationProviderImpl ) p0 ).setSendingSwitch ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->setSendingSwitch(Z)V
/* .line 139 */
return;
} // .end method
public void removeBlurNotification ( Integer p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 613 */
v0 = this.mBlurPackageName;
v0 = (( java.lang.String ) p2 ).equals ( v0 ); // invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
final String v1 = "blurLocation_notify is off"; // const-string v1, "blurLocation_notify is off"
/* if-nez v0, :cond_0 */
/* .line 614 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mBlurState:I */
/* .line 615 */
int v0 = 0; // const/4 v0, 0x0
this.mBlurPackageName = v0;
/* .line 616 */
v0 = this.blurLocationCollector;
(( java.util.Stack ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/Stack;->clear()V
/* .line 617 */
v0 = this.mGnssEventHandler;
(( com.android.server.location.GnssEventHandler ) v0 ).handleCallerName ( p2, v1 ); // invoke-virtual {v0, p2, v1}, Lcom/android/server/location/GnssEventHandler;->handleCallerName(Ljava/lang/String;Ljava/lang/String;)V
/* .line 618 */
return;
/* .line 620 */
} // :cond_0
/* iget v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mBlurState:I */
/* add-int/lit8 v0, v0, -0x1 */
/* iput v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mBlurState:I */
/* .line 621 */
/* if-nez v0, :cond_1 */
v0 = this.blurLocationCollector;
v0 = (( java.util.Stack ) v0 ).empty ( ); // invoke-virtual {v0}, Ljava/util/Stack;->empty()Z
/* if-nez v0, :cond_1 */
/* .line 622 */
v0 = this.blurLocationCollector;
(( java.util.Stack ) v0 ).pop ( ); // invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/location/gnss/GnssLocationProviderImpl$BlurLocationItem; */
this.popBlurLocationItem = v0;
/* .line 623 */
v0 = (( com.android.server.location.gnss.GnssLocationProviderImpl$BlurLocationItem ) v0 ).getState ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/GnssLocationProviderImpl$BlurLocationItem;->getState()I
/* iput v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mBlurState:I */
/* .line 624 */
v0 = this.popBlurLocationItem;
v0 = (( com.android.server.location.gnss.GnssLocationProviderImpl$BlurLocationItem ) v0 ).getUid ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/GnssLocationProviderImpl$BlurLocationItem;->getUid()I
/* iput v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mBlurUid:I */
/* .line 625 */
v0 = this.popBlurLocationItem;
(( com.android.server.location.gnss.GnssLocationProviderImpl$BlurLocationItem ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/GnssLocationProviderImpl$BlurLocationItem;->getPackageName()Ljava/lang/String;
this.mBlurPackageName = v0;
/* .line 626 */
v1 = this.mGnssEventHandler;
final String v2 = "blurLocation_notify is on"; // const-string v2, "blurLocation_notify is on"
(( com.android.server.location.GnssEventHandler ) v1 ).handleCallerName ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Lcom/android/server/location/GnssEventHandler;->handleCallerName(Ljava/lang/String;Ljava/lang/String;)V
/* .line 627 */
} // :cond_1
/* iget v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mBlurState:I */
/* if-nez v0, :cond_2 */
v0 = this.blurLocationCollector;
v0 = (( java.util.Stack ) v0 ).empty ( ); // invoke-virtual {v0}, Ljava/util/Stack;->empty()Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 628 */
v0 = this.mGnssEventHandler;
(( com.android.server.location.GnssEventHandler ) v0 ).handleCallerName ( p2, v1 ); // invoke-virtual {v0, p2, v1}, Lcom/android/server/location/GnssEventHandler;->handleCallerName(Ljava/lang/String;Ljava/lang/String;)V
/* .line 631 */
} // :cond_2
} // :goto_0
return;
} // .end method
public void sendNfwbroadcast ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 496 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "NFW GPS LOCATION FROM Modem"; // const-string v1, "NFW GPS LOCATION FROM Modem"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 497 */
/* .local v0, "intent":Landroid/content/Intent; */
v1 = android.os.UserHandle.ALL;
(( android.content.Context ) p1 ).sendBroadcastAsUser ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
/* .line 498 */
return;
} // .end method
public void setNfwProxyAppConfig ( java.util.Properties p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "properties" # Ljava/util/Properties; */
/* .param p2, "config" # Ljava/lang/String; */
/* .line 478 */
final String v0 = "ro.boot.hwc"; // const-string v0, "ro.boot.hwc"
android.os.SystemProperties .get ( v0 );
/* .line 479 */
/* .local v0, "country":Ljava/lang/String; */
final String v1 = "CN"; // const-string v1, "CN"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_0 */
/* .line 480 */
final String v1 = "com.lbe.security.miui"; // const-string v1, "com.lbe.security.miui"
(( java.util.Properties ) p1 ).setProperty ( p2, v1 ); // invoke-virtual {p1, p2, v1}, Ljava/util/Properties;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
/* .line 481 */
final String v1 = "GnssLocationProviderImpl"; // const-string v1, "GnssLocationProviderImpl"
final String v2 = "NFW app is com.lbe.security.miui"; // const-string v2, "NFW app is com.lbe.security.miui"
android.util.Log .d ( v1,v2 );
/* .line 483 */
} // :cond_0
return;
} // .end method
public void setSendingSwitch ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "newValue" # Z */
/* .line 128 */
/* iput-boolean p1, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mEnableSendingState:Z */
/* .line 129 */
return;
} // .end method
public void showBlurNotification ( Integer p0, java.lang.String p1 ) {
/* .locals 8 */
/* .param p1, "uid" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 593 */
v0 = this.mBlurPackageName;
v0 = (( java.lang.String ) p2 ).equals ( v0 ); // invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v1 = 1; // const/4 v1, 0x1
/* if-nez v0, :cond_1 */
/* iget v4, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mBlurState:I */
if ( v4 != null) { // if-eqz v4, :cond_1
v5 = this.mBlurPackageName;
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 594 */
/* new-instance v0, Lcom/android/server/location/gnss/GnssLocationProviderImpl$BlurLocationItem; */
/* iget v6, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mBlurUid:I */
int v7 = 0; // const/4 v7, 0x0
/* move-object v2, v0 */
/* move-object v3, p0 */
/* invoke-direct/range {v2 ..v7}, Lcom/android/server/location/gnss/GnssLocationProviderImpl$BlurLocationItem;-><init>(Lcom/android/server/location/gnss/GnssLocationProviderImpl;ILjava/lang/String;ILcom/android/server/location/gnss/GnssLocationProviderImpl$BlurLocationItem-IA;)V */
this.blurLocationItem = v0;
/* .line 595 */
v2 = this.blurLocationCollector;
(( java.util.Stack ) v2 ).push ( v0 ); // invoke-virtual {v2, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 596 */
/* iput v1, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mBlurState:I */
/* .line 597 */
v0 = this.mBlurLocation;
/* if-nez v0, :cond_0 */
/* .line 598 */
/* new-instance v0, Lcom/android/server/location/MiuiBlurLocationManagerImpl; */
/* invoke-direct {v0}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;-><init>()V */
this.mBlurLocation = v0;
/* .line 600 */
} // :cond_0
com.android.server.location.MiuiBlurLocationManagerImpl .get ( );
/* iget v2, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mBlurUid:I */
v3 = this.mBlurPackageName;
v0 = (( com.android.server.location.MiuiBlurLocationManagerStub ) v0 ).isBlurLocationMode ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lcom/android/server/location/MiuiBlurLocationManagerStub;->isBlurLocationMode(ILjava/lang/String;)Z
/* if-nez v0, :cond_2 */
/* .line 601 */
/* iput v1, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mBlurState:I */
/* .line 602 */
v0 = this.blurLocationCollector;
(( java.util.Stack ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/Stack;->clear()V
/* .line 605 */
} // :cond_1
/* iget v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mBlurState:I */
/* add-int/2addr v0, v1 */
/* iput v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mBlurState:I */
/* .line 607 */
} // :cond_2
} // :goto_0
/* iput p1, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->mBlurUid:I */
/* .line 608 */
this.mBlurPackageName = p2;
/* .line 609 */
v0 = this.mGnssEventHandler;
final String v1 = "blurLocation_notify is on"; // const-string v1, "blurLocation_notify is on"
(( com.android.server.location.GnssEventHandler ) v0 ).handleCallerName ( p2, v1 ); // invoke-virtual {v0, p2, v1}, Lcom/android/server/location/GnssEventHandler;->handleCallerName(Ljava/lang/String;Ljava/lang/String;)V
/* .line 610 */
return;
} // .end method
public void writeLocationInformation ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p1, "event" # Ljava/lang/String; */
/* .line 425 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "writeLocationInformation:" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->logd(Ljava/lang/String;)V */
/* .line 426 */
v0 = com.android.server.location.gnss.GnssLocationProviderImpl.mLocationInformationLock;
/* monitor-enter v0 */
/* .line 427 */
try { // :try_start_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* invoke-direct {p0}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->getCurrentTime()Ljava/lang/String; */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ": "; // const-string v2, ": "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "\n"; // const-string v2, "\n"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_2 */
/* .line 428 */
/* .local v1, "info":Ljava/lang/String; */
try { // :try_start_1
/* new-instance v2, Ljava/io/BufferedWriter; */
/* new-instance v3, Ljava/io/FileWriter; */
/* .line 429 */
/* invoke-direct {p0}, Lcom/android/server/location/gnss/GnssLocationProviderImpl;->getLocationFilePath()Ljava/lang/String; */
int v5 = 1; // const/4 v5, 0x1
/* invoke-direct {v3, v4, v5}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;Z)V */
/* invoke-direct {v2, v3}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V */
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_2 */
/* .line 430 */
/* .local v2, "writer":Ljava/io/BufferedWriter; */
try { // :try_start_2
(( java.io.BufferedWriter ) v2 ).write ( v1 ); // invoke-virtual {v2, v1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 431 */
try { // :try_start_3
(( java.io.BufferedWriter ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/BufferedWriter;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_0 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_2 */
/* .line 433 */
} // .end local v2 # "writer":Ljava/io/BufferedWriter;
/* .line 428 */
/* .restart local v2 # "writer":Ljava/io/BufferedWriter; */
/* :catchall_0 */
/* move-exception v3 */
try { // :try_start_4
(( java.io.BufferedWriter ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/BufferedWriter;->close()V
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* :catchall_1 */
/* move-exception v4 */
try { // :try_start_5
(( java.lang.Throwable ) v3 ).addSuppressed ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v1 # "info":Ljava/lang/String;
} // .end local p0 # "this":Lcom/android/server/location/gnss/GnssLocationProviderImpl;
} // .end local p1 # "event":Ljava/lang/String;
} // :goto_0
/* throw v3 */
/* :try_end_5 */
/* .catch Ljava/io/IOException; {:try_start_5 ..:try_end_5} :catch_0 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_2 */
/* .line 431 */
} // .end local v2 # "writer":Ljava/io/BufferedWriter;
/* .restart local v1 # "info":Ljava/lang/String; */
/* .restart local p0 # "this":Lcom/android/server/location/gnss/GnssLocationProviderImpl; */
/* .restart local p1 # "event":Ljava/lang/String; */
/* :catch_0 */
/* move-exception v2 */
/* .line 432 */
/* .local v2, "e":Ljava/io/IOException; */
try { // :try_start_6
final String v3 = "IO exception"; // const-string v3, "IO exception"
com.android.server.location.gnss.GnssLocationProviderImpl .loge ( v3 );
/* .line 434 */
} // .end local v1 # "info":Ljava/lang/String;
} // .end local v2 # "e":Ljava/io/IOException;
} // :goto_1
/* monitor-exit v0 */
/* .line 435 */
return;
/* .line 434 */
/* :catchall_2 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_2 */
/* throw v1 */
} // .end method
