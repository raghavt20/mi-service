class com.android.server.location.gnss.GnssLocationProviderImpl$BlurLocationItem {
	 /* .source "GnssLocationProviderImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/location/gnss/GnssLocationProviderImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "BlurLocationItem" */
} // .end annotation
/* # instance fields */
private java.lang.String blurPackageName;
private Integer blurState;
private Integer blurUid;
final com.android.server.location.gnss.GnssLocationProviderImpl this$0; //synthetic
/* # direct methods */
private com.android.server.location.gnss.GnssLocationProviderImpl$BlurLocationItem ( ) {
/* .locals 0 */
/* .param p2, "state" # I */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .param p4, "uid" # I */
/* .line 660 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 661 */
/* iput p2, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl$BlurLocationItem;->blurState:I */
/* .line 662 */
this.blurPackageName = p3;
/* .line 663 */
/* iput p4, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl$BlurLocationItem;->blurUid:I */
/* .line 664 */
return;
} // .end method
 com.android.server.location.gnss.GnssLocationProviderImpl$BlurLocationItem ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/location/gnss/GnssLocationProviderImpl$BlurLocationItem;-><init>(Lcom/android/server/location/gnss/GnssLocationProviderImpl;ILjava/lang/String;I)V */
return;
} // .end method
/* # virtual methods */
public java.lang.String getPackageName ( ) {
/* .locals 1 */
/* .line 671 */
v0 = this.blurPackageName;
} // .end method
public Integer getState ( ) {
/* .locals 1 */
/* .line 667 */
/* iget v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl$BlurLocationItem;->blurState:I */
} // .end method
public Integer getUid ( ) {
/* .locals 1 */
/* .line 675 */
/* iget v0, p0, Lcom/android/server/location/gnss/GnssLocationProviderImpl$BlurLocationItem;->blurUid:I */
} // .end method
