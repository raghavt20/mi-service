class com.android.server.location.gnss.datacollect.GnssEventTrackingImpl$GnssEngineUsage {
	 /* .source "GnssEventTrackingImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "GnssEngineUsage" */
} // .end annotation
/* # instance fields */
private Long gnssEngineActuallyUseTime;
private Long gnssEngineShouldUseTime;
final com.android.server.location.gnss.datacollect.GnssEventTrackingImpl this$0; //synthetic
/* # direct methods */
private com.android.server.location.gnss.datacollect.GnssEventTrackingImpl$GnssEngineUsage ( ) {
/* .locals 0 */
/* .param p2, "gnssEngineShouldUseTime" # J */
/* .param p4, "gnssEngineActuallyUseTime" # J */
/* .line 625 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 626 */
/* iput-wide p2, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$GnssEngineUsage;->gnssEngineShouldUseTime:J */
/* .line 627 */
/* iput-wide p4, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$GnssEngineUsage;->gnssEngineActuallyUseTime:J */
/* .line 628 */
return;
} // .end method
 com.android.server.location.gnss.datacollect.GnssEventTrackingImpl$GnssEngineUsage ( ) { //synthethic
/* .locals 0 */
/* invoke-direct/range {p0 ..p5}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$GnssEngineUsage;-><init>(Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;JJ)V */
return;
} // .end method
/* # virtual methods */
public Long getGnssEngineActuallyUseTime ( ) {
/* .locals 2 */
/* .line 635 */
/* iget-wide v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$GnssEngineUsage;->gnssEngineActuallyUseTime:J */
/* return-wide v0 */
} // .end method
public Integer getGnssEngineControlState ( ) {
/* .locals 4 */
/* .line 639 */
/* iget-wide v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$GnssEngineUsage;->gnssEngineActuallyUseTime:J */
/* const-wide/16 v2, 0x0 */
/* cmp-long v2, v0, v2 */
/* if-nez v2, :cond_0 */
/* .line 640 */
int v0 = 0; // const/4 v0, 0x0
/* .line 641 */
} // :cond_0
/* iget-wide v2, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$GnssEngineUsage;->gnssEngineShouldUseTime:J */
/* cmp-long v0, v2, v0 */
/* if-nez v0, :cond_1 */
/* .line 642 */
int v0 = 2; // const/4 v0, 0x2
/* .line 644 */
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
} // .end method
public Long getGnssEngineShouldUseTime ( ) {
/* .locals 2 */
/* .line 631 */
/* iget-wide v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$GnssEngineUsage;->gnssEngineShouldUseTime:J */
/* return-wide v0 */
} // .end method
