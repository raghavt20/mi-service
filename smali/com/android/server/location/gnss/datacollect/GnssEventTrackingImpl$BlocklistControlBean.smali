.class Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;
.super Ljava/lang/Object;
.source "GnssEventTrackingImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BlocklistControlBean"
.end annotation


# instance fields
.field private blockedTime:J

.field private pkg:Ljava/lang/String;

.field final synthetic this$0:Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;

.field private totalNaviTime:J


# direct methods
.method public constructor <init>(Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;JJ)V
    .locals 0
    .param p2, "totalNaviTime"    # J
    .param p4, "blockedTime"    # J

    .line 677
    iput-object p1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;->this$0:Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 678
    iput-wide p2, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;->totalNaviTime:J

    .line 679
    iput-wide p4, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;->blockedTime:J

    .line 680
    return-void
.end method

.method public constructor <init>(Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;JJLjava/lang/String;)V
    .locals 0
    .param p2, "totalNaviTime"    # J
    .param p4, "blockedTime"    # J
    .param p6, "pkg"    # Ljava/lang/String;

    .line 682
    iput-object p1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;->this$0:Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 683
    iput-wide p2, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;->totalNaviTime:J

    .line 684
    iput-wide p4, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;->blockedTime:J

    .line 685
    iput-object p6, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;->pkg:Ljava/lang/String;

    .line 686
    return-void
.end method


# virtual methods
.method public getBlockedTime()J
    .locals 2

    .line 697
    iget-wide v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;->blockedTime:J

    return-wide v0
.end method

.method public getPkg()Ljava/lang/String;
    .locals 1

    .line 705
    iget-object v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;->pkg:Ljava/lang/String;

    return-object v0
.end method

.method public getTotalNaviTime()J
    .locals 2

    .line 689
    iget-wide v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;->totalNaviTime:J

    return-wide v0
.end method

.method public setBlockedTime(J)V
    .locals 0
    .param p1, "blockedTime"    # J

    .line 701
    iput-wide p1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;->blockedTime:J

    .line 702
    return-void
.end method

.method public setPkg(Ljava/lang/String;)V
    .locals 0
    .param p1, "pkg"    # Ljava/lang/String;

    .line 709
    iput-object p1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;->pkg:Ljava/lang/String;

    .line 710
    return-void
.end method

.method public setTotalNaviTime(J)V
    .locals 0
    .param p1, "totalNaviTime"    # J

    .line 693
    iput-wide p1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;->totalNaviTime:J

    .line 694
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 714
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "recordGnssBlocklistUsage,totalNaviTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;->totalNaviTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", blockedTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;->blockedTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", pkg="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;->pkg:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
