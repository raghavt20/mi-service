.class public Lcom/android/server/location/gnss/datacollect/GnssPowerBean;
.super Ljava/lang/Object;
.source "GnssPowerBean.java"


# instance fields
.field start_power:Ljava/lang/String;

.field start_time:Ljava/lang/String;

.field stop_power:Ljava/lang/String;

.field stop_time:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .line 22
    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    .line 23
    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 24
    :cond_1
    move-object v2, p1

    check-cast v2, Lcom/android/server/location/gnss/datacollect/GnssPowerBean;

    .line 25
    .local v2, "bean":Lcom/android/server/location/gnss/datacollect/GnssPowerBean;
    iget-object v3, p0, Lcom/android/server/location/gnss/datacollect/GnssPowerBean;->start_time:Ljava/lang/String;

    iget-object v4, v2, Lcom/android/server/location/gnss/datacollect/GnssPowerBean;->start_time:Ljava/lang/String;

    invoke-static {v3, v4}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/server/location/gnss/datacollect/GnssPowerBean;->stop_time:Ljava/lang/String;

    iget-object v4, v2, Lcom/android/server/location/gnss/datacollect/GnssPowerBean;->stop_time:Ljava/lang/String;

    .line 26
    invoke-static {v3, v4}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/server/location/gnss/datacollect/GnssPowerBean;->start_power:Ljava/lang/String;

    iget-object v4, v2, Lcom/android/server/location/gnss/datacollect/GnssPowerBean;->start_power:Ljava/lang/String;

    .line 27
    invoke-static {v3, v4}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/server/location/gnss/datacollect/GnssPowerBean;->stop_power:Ljava/lang/String;

    iget-object v4, v2, Lcom/android/server/location/gnss/datacollect/GnssPowerBean;->stop_power:Ljava/lang/String;

    .line 28
    invoke-static {v3, v4}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    :cond_2
    move v0, v1

    .line 25
    :goto_0
    return v0

    .line 23
    .end local v2    # "bean":Lcom/android/server/location/gnss/datacollect/GnssPowerBean;
    :cond_3
    :goto_1
    return v1
.end method

.method public hashCode()I
    .locals 4

    .line 33
    iget-object v0, p0, Lcom/android/server/location/gnss/datacollect/GnssPowerBean;->start_time:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/server/location/gnss/datacollect/GnssPowerBean;->stop_time:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/server/location/gnss/datacollect/GnssPowerBean;->start_power:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/server/location/gnss/datacollect/GnssPowerBean;->stop_power:Ljava/lang/String;

    filled-new-array {v0, v1, v2, v3}, [Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
