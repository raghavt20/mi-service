.class public Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;
.super Ljava/lang/Object;
.source "UseGnssAppBean.java"


# instance fields
.field public changeToBackTime:J

.field public changeToforeTime:J

.field public glpDuring:J

.field public isAppForeground:Z

.field public isRunning:Z

.field public packageName:Ljava/lang/String;

.field public provider:Ljava/lang/String;

.field public removeTime:J

.field public reportInterval:Ljava/lang/String;

.field public requestTime:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 7
    .param p1, "o"    # Ljava/lang/Object;

    .line 34
    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    .line 35
    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 36
    :cond_1
    move-object v2, p1

    check-cast v2, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;

    .line 37
    .local v2, "that":Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;
    iget-wide v3, p0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->requestTime:J

    iget-wide v5, v2, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->requestTime:J

    cmp-long v3, v3, v5

    if-nez v3, :cond_2

    iget-wide v3, p0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->changeToforeTime:J

    iget-wide v5, v2, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->changeToforeTime:J

    cmp-long v3, v3, v5

    if-nez v3, :cond_2

    iget-wide v3, p0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->changeToBackTime:J

    iget-wide v5, v2, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->changeToBackTime:J

    cmp-long v3, v3, v5

    if-nez v3, :cond_2

    iget-wide v3, p0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->removeTime:J

    iget-wide v5, v2, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->removeTime:J

    cmp-long v3, v3, v5

    if-nez v3, :cond_2

    iget-wide v3, p0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->glpDuring:J

    iget-wide v5, v2, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->glpDuring:J

    cmp-long v3, v3, v5

    if-nez v3, :cond_2

    iget-boolean v3, p0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->isAppForeground:Z

    iget-boolean v4, v2, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->isAppForeground:Z

    if-ne v3, v4, :cond_2

    iget-boolean v3, p0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->isRunning:Z

    iget-boolean v4, v2, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->isRunning:Z

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->packageName:Ljava/lang/String;

    iget-object v4, v2, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->packageName:Ljava/lang/String;

    invoke-static {v3, v4}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->reportInterval:Ljava/lang/String;

    iget-object v4, v2, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->reportInterval:Ljava/lang/String;

    invoke-static {v3, v4}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->provider:Ljava/lang/String;

    iget-object v4, v2, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->provider:Ljava/lang/String;

    invoke-static {v3, v4}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    :cond_2
    move v0, v1

    :goto_0
    return v0

    .line 35
    .end local v2    # "that":Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;
    :cond_3
    :goto_1
    return v1
.end method

.method public hashCode()I
    .locals 10

    .line 42
    iget-object v0, p0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->packageName:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->reportInterval:Ljava/lang/String;

    iget-wide v2, p0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->requestTime:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-wide v3, p0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->changeToforeTime:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iget-wide v4, p0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->changeToBackTime:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iget-wide v5, p0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->removeTime:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    iget-wide v6, p0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->glpDuring:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    iget-boolean v7, p0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->isAppForeground:Z

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    iget-boolean v8, p0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->isRunning:Z

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    iget-object v9, p0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->provider:Ljava/lang/String;

    filled-new-array/range {v0 .. v9}, [Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
