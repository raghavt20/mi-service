public class com.android.server.location.gnss.datacollect.GnssEventTrackingImpl implements com.android.server.location.gnss.GnssEventTrackingStub {
	 /* .source "GnssEventTrackingImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl;, */
	 /* Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$GnssEngineUsage;, */
	 /* Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String ACTION_UPLOAD_DATA;
private static final java.lang.String APP_REQUEST_GLP_CNT_AFT_GPO;
private static final java.lang.String APP_REQUEST_GLP_CNT_BEF_GPO;
private static final java.lang.String EVENT_APP_REQUEST_GLP_CNT;
private static final java.lang.String EVENT_BLOCK_LIST_USAGE;
private static final java.lang.String EVENT_GNSS_ENGINE_USAGE;
private static final java.lang.String EVENT_GPS_USE_APP;
private static final java.lang.String EVENT_NAME;
private static final java.lang.String GMO_POSITION_INTERVAL;
private static final java.lang.String GMO_POSITION_TIMES;
private static final java.lang.String GNSS_BACKGROUND_OPT;
private static final java.lang.String GNSS_MOCK_LOCATION_OPT;
private static final java.lang.String GNSS_SATELLITE_CALL_OPT;
private static final java.lang.String GPO3_CTRL_TYPE_ALL;
private static final java.lang.String GPO3_CTRL_TYPE_NONE;
private static final java.lang.String GPO3_CTRL_TYPE_PART;
private static final java.lang.String GPO3_TIME_AFT;
private static final java.lang.String GPO3_TIME_BEF;
private static final java.lang.String NAV_APP_TIME;
private static final java.lang.String PACKAGE_NAME;
private static final Integer REQUEST_CODE_GNSS_ENGINE_USAGE;
private static final Integer REQUEST_CODE_SATELLITE_CALL_OPT;
private static final Integer REQUEST_CODE_USE_APP;
private static final java.lang.String TAG;
private static final Integer TYPE_GNSS_ENGINE_CONTROL_ALL;
private static final Integer TYPE_GNSS_ENGINE_CONTROL_NONE;
private static final Integer TYPE_GNSS_ENGINE_CONTROL_PART;
/* # instance fields */
private final Boolean D;
private Integer START_INTERVAL;
private Integer UPLOAD_REPEAT_TIME;
private Boolean hasStartUploadData;
private final Boolean isSavePoint;
private final java.util.Map mAppRequestCtlMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.concurrent.atomic.AtomicInteger mBackgroundOpt2Cnt;
private java.util.concurrent.atomic.AtomicInteger mBackgroundOpt3Cnt;
private final java.util.List mBlocklistControlBeanList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Long mEngineBlockTime;
private Long mEngineControlTime;
private Long mEngineStartTime;
private Long mEngineStopTime;
private Long mEngineTimeAftGpo3;
private Long mEngineTimeBefGpo3;
private Long mGlpBackTime;
private android.content.Context mGlpContext;
private final java.util.Map mGlpDuringBackground;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Long;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.util.Map mGlpDuringMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Long;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Long mGlpForeTime;
private java.util.concurrent.atomic.AtomicLong mGnssMockLocationInterval;
private java.util.concurrent.atomic.AtomicLong mGnssMockLocationTimes;
private Boolean mIsGnssPowerRecord;
private Integer mLastEngineStatus;
private final java.util.List mListEngineUsage;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$GnssEngineUsage;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.util.List mListUseApp;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.util.Map mNavAppTimeMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Long;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final android.content.BroadcastReceiver mReceiver;
private final java.util.Map mRequestMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Integer;", */
/* "Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.concurrent.atomic.AtomicInteger mSatelliteCallCnt;
private Long mSatelliteCallDuring;
/* # direct methods */
public static void $r8$lambda$iLpkYKjax4NPB2bo6yMTIy161lg ( com.android.server.location.gnss.datacollect.GnssEventTrackingImpl p0, android.content.Context p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->lambda$startUploadGnssData$0(Landroid/content/Context;)V */
return;
} // .end method
static void -$$Nest$mstartUploadGnssData ( com.android.server.location.gnss.datacollect.GnssEventTrackingImpl p0, android.content.Context p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->startUploadGnssData(Landroid/content/Context;)V */
return;
} // .end method
 com.android.server.location.gnss.datacollect.GnssEventTrackingImpl ( ) {
/* .locals 5 */
/* .line 111 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 62 */
final String v0 = "persist.sys.miui_gnss_dc"; // const-string v0, "persist.sys.miui_gnss_dc"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
/* iput-boolean v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->isSavePoint:Z */
/* .line 63 */
final String v2 = "persist.sys.gnss_dc.test"; // const-string v2, "persist.sys.gnss_dc.test"
v2 = android.os.SystemProperties .getBoolean ( v2,v1 );
/* iput-boolean v2, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z */
/* .line 75 */
/* const v3, 0x5265c00 */
/* iput v3, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->UPLOAD_REPEAT_TIME:I */
/* .line 76 */
/* const v3, 0x493e0 */
/* iput v3, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->START_INTERVAL:I */
/* .line 79 */
int v3 = 1; // const/4 v3, 0x1
/* iput-boolean v3, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mIsGnssPowerRecord:Z */
/* .line 91 */
int v3 = 4; // const/4 v3, 0x4
/* iput v3, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mLastEngineStatus:I */
/* .line 94 */
/* new-instance v3, Ljava/util/concurrent/CopyOnWriteArrayList; */
/* invoke-direct {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V */
this.mListUseApp = v3;
/* .line 95 */
/* new-instance v3, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v3}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.mRequestMap = v3;
/* .line 96 */
/* new-instance v3, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v3}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.mGlpDuringMap = v3;
/* .line 97 */
/* new-instance v3, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v3}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.mGlpDuringBackground = v3;
/* .line 98 */
/* new-instance v3, Ljava/util/concurrent/CopyOnWriteArrayList; */
/* invoke-direct {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V */
this.mListEngineUsage = v3;
/* .line 99 */
/* new-instance v3, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v3}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.mAppRequestCtlMap = v3;
/* .line 100 */
/* new-instance v3, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v3}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.mNavAppTimeMap = v3;
/* .line 101 */
/* new-instance v3, Ljava/util/concurrent/CopyOnWriteArrayList; */
/* invoke-direct {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V */
this.mBlocklistControlBeanList = v3;
/* .line 102 */
/* new-instance v3, Ljava/util/concurrent/atomic/AtomicInteger; */
/* invoke-direct {v3, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V */
this.mBackgroundOpt2Cnt = v3;
/* .line 103 */
/* new-instance v3, Ljava/util/concurrent/atomic/AtomicInteger; */
/* invoke-direct {v3, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V */
this.mBackgroundOpt3Cnt = v3;
/* .line 104 */
/* new-instance v3, Ljava/util/concurrent/atomic/AtomicInteger; */
/* invoke-direct {v3, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V */
this.mSatelliteCallCnt = v3;
/* .line 105 */
/* new-instance v1, Ljava/util/concurrent/atomic/AtomicLong; */
/* const-wide/16 v3, 0x0 */
/* invoke-direct {v1, v3, v4}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V */
this.mGnssMockLocationTimes = v1;
/* .line 106 */
/* new-instance v1, Ljava/util/concurrent/atomic/AtomicLong; */
/* invoke-direct {v1, v3, v4}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V */
this.mGnssMockLocationInterval = v1;
/* .line 125 */
/* new-instance v1, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$1; */
/* invoke-direct {v1, p0}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$1;-><init>(Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;)V */
this.mReceiver = v1;
/* .line 112 */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 113 */
/* const/16 v1, 0xbb8 */
/* iput v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->UPLOAD_REPEAT_TIME:I */
/* .line 114 */
/* iput v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->START_INTERVAL:I */
/* .line 116 */
} // :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Is specified platform:"; // const-string v2, "Is specified platform:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " upload repeat time:"; // const-string v1, " upload repeat time:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->UPLOAD_REPEAT_TIME:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " start interval:"; // const-string v1, " start interval:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->START_INTERVAL:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "GnssSavePoint"; // const-string v1, "GnssSavePoint"
android.util.Log .d ( v1,v0 );
/* .line 118 */
return;
} // .end method
private void calCurEngineUsage ( Long p0 ) {
/* .locals 11 */
/* .param p1, "milliseconds" # J */
/* .line 332 */
/* const-class v0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl; */
/* monitor-enter v0 */
/* .line 333 */
try { // :try_start_0
/* iput-wide p1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineStopTime:J */
/* .line 336 */
/* iget-wide v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineControlTime:J */
/* const-wide/16 v3, 0x0 */
/* cmp-long v1, v1, v3 */
/* if-nez v1, :cond_0 */
/* .line 337 */
/* iput-wide p1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineControlTime:J */
/* .line 340 */
} // :cond_0
/* iget-wide v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineStartTime:J */
/* cmp-long v5, v1, v3 */
/* if-nez v5, :cond_1 */
/* .line 341 */
/* iput-wide v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineControlTime:J */
/* .line 344 */
} // :cond_1
/* iget-wide v5, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineBlockTime:J */
/* sub-long v5, p1, v5 */
/* iput-wide v5, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineTimeBefGpo3:J */
/* .line 346 */
/* iget-wide v5, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineTimeAftGpo3:J */
/* iget-wide v7, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineControlTime:J */
/* sub-long/2addr v7, v1 */
/* add-long/2addr v5, v7 */
/* iput-wide v5, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineTimeAftGpo3:J */
/* .line 347 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "recordEngineUsage, mEngineTimeBefGpo3="; // const-string v2, "recordEngineUsage, mEngineTimeBefGpo3="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v5, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineTimeBefGpo3:J */
(( java.lang.StringBuilder ) v1 ).append ( v5, v6 ); // invoke-virtual {v1, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v2 = ", mEngineTimeAftGpo3="; // const-string v2, ", mEngineTimeAftGpo3="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v5, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineTimeAftGpo3:J */
(( java.lang.StringBuilder ) v1 ).append ( v5, v6 ); // invoke-virtual {v1, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v2 = ", saved milliseconds is "; // const-string v2, ", saved milliseconds is "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v5, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineTimeBefGpo3:J */
/* iget-wide v7, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineTimeAftGpo3:J */
/* sub-long/2addr v5, v7 */
(( java.lang.StringBuilder ) v1 ).append ( v5, v6 ); // invoke-virtual {v1, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 350 */
/* .local v1, "dumpInfo":Ljava/lang/String; */
/* iget-boolean v2, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 351 */
final String v2 = "GnssSavePoint"; // const-string v2, "GnssSavePoint"
android.util.Log .d ( v2,v1 );
/* .line 353 */
} // :cond_2
/* iget-wide v5, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineTimeBefGpo3:J */
/* iget-wide v7, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineTimeAftGpo3:J */
/* cmp-long v2, v5, v7 */
/* if-ltz v2, :cond_3 */
/* cmp-long v2, v5, v3 */
/* if-lez v2, :cond_3 */
/* .line 354 */
/* new-instance v9, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$GnssEngineUsage; */
int v10 = 0; // const/4 v10, 0x0
/* move-object v2, v9 */
/* move-object v3, p0 */
/* move-wide v4, v5 */
/* move-wide v6, v7 */
/* move-object v8, v10 */
/* invoke-direct/range {v2 ..v8}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$GnssEngineUsage;-><init>(Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;JJLcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$GnssEngineUsage-IA;)V */
/* move-object v2, v9 */
/* .line 355 */
/* .local v2, "gnssEngineUsage":Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$GnssEngineUsage; */
v3 = this.mListEngineUsage;
/* .line 356 */
com.android.server.location.gnss.GnssLocationProviderStub .getInstance ( );
/* .line 358 */
} // .end local v2 # "gnssEngineUsage":Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$GnssEngineUsage;
} // :cond_3
/* invoke-direct {p0}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->resetGpo3Var()V */
/* .line 359 */
} // .end local v1 # "dumpInfo":Ljava/lang/String;
/* monitor-exit v0 */
/* .line 360 */
return;
/* .line 359 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void lambda$startUploadGnssData$0 ( android.content.Context p0 ) { //synthethic
/* .locals 0 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 136 */
/* invoke-direct {p0, p1}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->startUploadUseApp(Landroid/content/Context;)V */
/* .line 137 */
/* invoke-direct {p0, p1}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->startUploadAppRequest(Landroid/content/Context;)V */
/* .line 138 */
/* invoke-direct {p0, p1}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->startUploadGnssEngineUsage(Landroid/content/Context;)V */
/* .line 139 */
/* invoke-direct {p0, p1}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->startUploadBlockListUsage(Landroid/content/Context;)V */
/* .line 140 */
/* invoke-direct {p0, p1}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->startUploadBackgroundOpt(Landroid/content/Context;)V */
/* .line 141 */
/* invoke-direct {p0, p1}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->startUploadGnssMockLocationOpt(Landroid/content/Context;)V */
/* .line 142 */
/* invoke-direct {p0, p1}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->startUploadGnssSatelliteCallOpt(Landroid/content/Context;)V */
/* .line 143 */
return;
} // .end method
private void putIntoAppRequestCtlMap ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .line 272 */
v0 = v0 = this.mAppRequestCtlMap;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 273 */
v0 = this.mAppRequestCtlMap;
/* check-cast v0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl; */
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.location.gnss.datacollect.GnssEventTrackingImpl$AppRequestCtl ) v0 ).addRequestCnt ( v1, v1 ); // invoke-virtual {v0, v1, v1}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl;->addRequestCnt(II)V
/* .line 274 */
return;
/* .line 276 */
} // :cond_0
v0 = this.mAppRequestCtlMap;
/* new-instance v1, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl; */
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {v1, p0, v2}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl;-><init>(Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl-IA;)V */
/* .line 277 */
return;
} // .end method
private void recordAppRemove ( com.android.server.location.gnss.datacollect.UseGnssAppBean p0 ) {
/* .locals 3 */
/* .param p1, "bean" # Lcom/android/server/location/gnss/datacollect/UseGnssAppBean; */
/* .line 280 */
/* if-nez p1, :cond_0 */
return;
/* .line 281 */
} // :cond_0
v0 = this.mAppRequestCtlMap;
v1 = this.packageName;
/* check-cast v0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl; */
/* .line 282 */
/* .local v0, "appRequestCtl":Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mLastEngineStatus:I */
int v2 = 1; // const/4 v2, 0x1
/* if-ne v1, v2, :cond_1 */
/* .line 283 */
int v1 = 0; // const/4 v1, 0x0
int v2 = -1; // const/4 v2, -0x1
(( com.android.server.location.gnss.datacollect.GnssEventTrackingImpl$AppRequestCtl ) v0 ).addRequestCnt ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl;->addRequestCnt(II)V
/* .line 285 */
} // :cond_1
return;
} // .end method
private void recordCallerGnssApp ( android.content.Context p0, java.lang.String p1, java.lang.String p2, Long p3, Long p4 ) {
/* .locals 7 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "interval" # Ljava/lang/String; */
/* .param p4, "glpDuring" # J */
/* .param p6, "glpBackgroundDuring" # J */
/* .line 183 */
if ( p1 != null) { // if-eqz p1, :cond_6
if ( p2 != null) { // if-eqz p2, :cond_6
/* if-nez p3, :cond_0 */
/* goto/16 :goto_0 */
/* .line 186 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->isSavePoint:Z */
final String v1 = "GnssSavePoint"; // const-string v1, "GnssSavePoint"
/* if-nez v0, :cond_2 */
/* .line 187 */
/* iget-boolean v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
final String v0 = "record Use Gnss App----> Not the specified platform"; // const-string v0, "record Use Gnss App----> Not the specified platform"
android.util.Log .d ( v1,v0 );
/* .line 188 */
} // :cond_1
return;
/* .line 190 */
} // :cond_2
v0 = v0 = this.mListUseApp;
/* const v2, 0x7fffffff */
/* if-lt v0, v2, :cond_3 */
/* .line 191 */
/* const-string/jumbo v0, "use app list.size() >= Integer.MAX_VALUE----> return" */
android.util.Log .v ( v1,v0 );
/* .line 192 */
return;
/* .line 194 */
} // :cond_3
/* new-instance v0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean; */
/* invoke-direct {v0}, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;-><init>()V */
/* .line 195 */
/* .local v0, "bean":Lcom/android/server/location/gnss/datacollect/UseGnssAppBean; */
this.packageName = p2;
/* .line 196 */
this.reportInterval = p3;
/* .line 197 */
v2 = this.mGlpDuringMap;
/* const-wide/16 v3, 0x0 */
java.lang.Long .valueOf ( v3,v4 );
/* check-cast v5, Ljava/lang/Long; */
(( java.lang.Long ) v5 ).longValue ( ); // invoke-virtual {v5}, Ljava/lang/Long;->longValue()J
/* move-result-wide v5 */
/* add-long/2addr v5, p4 */
java.lang.Long .valueOf ( v5,v6 );
/* .line 198 */
v2 = this.mGlpDuringBackground;
java.lang.Long .valueOf ( v3,v4 );
/* check-cast v3, Ljava/lang/Long; */
(( java.lang.Long ) v3 ).longValue ( ); // invoke-virtual {v3}, Ljava/lang/Long;->longValue()J
/* move-result-wide v3 */
/* add-long/2addr v3, p6 */
java.lang.Long .valueOf ( v3,v4 );
/* .line 199 */
v2 = this.mListUseApp;
/* .line 200 */
/* iget-boolean v2, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z */
if ( v2 != null) { // if-eqz v2, :cond_4
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "record Use Gnss App----> "; // const-string v3, "record Use Gnss App----> "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v2 );
/* .line 201 */
} // :cond_4
/* iget-boolean v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->hasStartUploadData:Z */
/* if-nez v1, :cond_5 */
/* .line 202 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->hasStartUploadData:Z */
/* .line 203 */
final String v1 = "action upload data"; // const-string v1, "action upload data"
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {p0, p1, v1, v2}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->setAlarm(Landroid/content/Context;Ljava/lang/String;I)V */
/* .line 205 */
} // :cond_5
return;
/* .line 184 */
} // .end local v0 # "bean":Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;
} // :cond_6
} // :goto_0
return;
} // .end method
private void resetGpo3Var ( ) {
/* .locals 3 */
/* .line 385 */
/* const-class v0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl; */
/* monitor-enter v0 */
/* .line 386 */
/* const-wide/16 v1, 0x0 */
try { // :try_start_0
/* iput-wide v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineBlockTime:J */
/* .line 387 */
/* iput-wide v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineStartTime:J */
/* .line 388 */
/* iput-wide v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineStopTime:J */
/* .line 389 */
/* iput-wide v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineControlTime:J */
/* .line 390 */
/* iput-wide v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineTimeBefGpo3:J */
/* .line 391 */
/* iput-wide v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineTimeAftGpo3:J */
/* .line 392 */
/* monitor-exit v0 */
/* .line 393 */
return;
/* .line 392 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void setAlarm ( android.content.Context p0, java.lang.String p1, Integer p2 ) {
/* .locals 16 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "action" # Ljava/lang/String; */
/* .param p3, "requestCode" # I */
/* .line 604 */
/* move-object/from16 v1, p0 */
/* move-object/from16 v2, p1 */
/* move-object/from16 v3, p2 */
if ( v2 != null) { // if-eqz v2, :cond_1
/* if-nez v3, :cond_0 */
/* move/from16 v14, p3 */
/* .line 606 */
} // :cond_0
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* move-object v4, v0 */
/* .line 607 */
/* .local v4, "filter":Landroid/content/IntentFilter; */
(( android.content.IntentFilter ) v4 ).addAction ( v3 ); // invoke-virtual {v4, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 608 */
v0 = this.mReceiver;
(( android.content.Context ) v2 ).registerReceiver ( v0, v4 ); // invoke-virtual {v2, v0, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 609 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v5 */
/* .line 611 */
/* .local v5, "token":J */
try { // :try_start_0
final String v0 = "alarm"; // const-string v0, "alarm"
(( android.content.Context ) v2 ).getSystemService ( v0 ); // invoke-virtual {v2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* move-object v7, v0 */
/* check-cast v7, Landroid/app/AlarmManager; */
/* .line 612 */
/* .local v7, "alarmManager":Landroid/app/AlarmManager; */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 613 */
/* .local v0, "intent":Landroid/content/Intent; */
/* const/high16 v8, 0x4000000 */
/* move/from16 v14, p3 */
try { // :try_start_1
android.app.PendingIntent .getBroadcast ( v2,v14,v0,v8 );
/* .line 614 */
/* .local v13, "p":Landroid/app/PendingIntent; */
int v8 = 2; // const/4 v8, 0x2
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v9 */
/* iget v11, v1, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->UPLOAD_REPEAT_TIME:I */
/* move-object v15, v0 */
} // .end local v0 # "intent":Landroid/content/Intent;
/* .local v15, "intent":Landroid/content/Intent; */
/* int-to-long v0, v11 */
/* add-long/2addr v9, v0 */
/* int-to-long v11, v11 */
/* invoke-virtual/range {v7 ..v13}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 616 */
} // .end local v7 # "alarmManager":Landroid/app/AlarmManager;
} // .end local v13 # "p":Landroid/app/PendingIntent;
} // .end local v15 # "intent":Landroid/content/Intent;
android.os.Binder .restoreCallingIdentity ( v5,v6 );
/* .line 617 */
/* nop */
/* .line 618 */
return;
/* .line 616 */
/* :catchall_0 */
/* move-exception v0 */
/* :catchall_1 */
/* move-exception v0 */
/* move/from16 v14, p3 */
} // :goto_0
android.os.Binder .restoreCallingIdentity ( v5,v6 );
/* .line 617 */
/* throw v0 */
/* .line 604 */
} // .end local v4 # "filter":Landroid/content/IntentFilter;
} // .end local v5 # "token":J
} // :cond_1
/* move/from16 v14, p3 */
} // :goto_1
return;
} // .end method
private void startUploadAppRequest ( android.content.Context p0 ) {
/* .locals 8 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 487 */
/* iget-boolean v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 488 */
final String v0 = "GnssSavePoint"; // const-string v0, "GnssSavePoint"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "startUploadAppRequest schedule current thread---->" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.Thread .currentThread ( );
(( java.lang.Thread ) v2 ).getName ( ); // invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v1 );
/* .line 489 */
} // :cond_0
v0 = v0 = this.mAppRequestCtlMap;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 490 */
/* iget-boolean v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
final String v0 = "GnssSavePoint"; // const-string v0, "GnssSavePoint"
final String v1 = "No App Request Data, skip upload."; // const-string v1, "No App Request Data, skip upload."
android.util.Log .d ( v0,v1 );
/* .line 491 */
} // :cond_1
return;
/* .line 493 */
} // :cond_2
/* iget v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mLastEngineStatus:I */
int v1 = 4; // const/4 v1, 0x4
/* if-eq v0, v1, :cond_4 */
/* .line 494 */
/* iget-boolean v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
final String v0 = "GnssSavePoint"; // const-string v0, "GnssSavePoint"
final String v1 = "Gnss engine is working, skip upload."; // const-string v1, "Gnss engine is working, skip upload."
android.util.Log .d ( v0,v1 );
/* .line 495 */
} // :cond_3
return;
/* .line 497 */
} // :cond_4
/* const-class v0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl; */
/* monitor-enter v0 */
/* .line 499 */
try { // :try_start_0
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 500 */
/* .local v1, "dataList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v2 = this.mAppRequestCtlMap;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_6
/* check-cast v3, Ljava/util/Map$Entry; */
/* .line 501 */
/* .local v3, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl;>;" */
/* check-cast v4, Ljava/lang/String; */
/* .line 502 */
/* .local v4, "pkg":Ljava/lang/String; */
/* new-instance v5, Lorg/json/JSONObject; */
/* invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V */
/* .line 503 */
/* .local v5, "jsonObject":Lorg/json/JSONObject; */
final String v6 = "EVENT_NAME"; // const-string v6, "EVENT_NAME"
final String v7 = "AppRequestGlpCnt"; // const-string v7, "AppRequestGlpCnt"
(( org.json.JSONObject ) v5 ).put ( v6, v7 ); // invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 504 */
final String v6 = "packageName"; // const-string v6, "packageName"
(( org.json.JSONObject ) v5 ).put ( v6, v4 ); // invoke-virtual {v5, v6, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 505 */
final String v6 = "AppRequestGlpCntBefGpo"; // const-string v6, "AppRequestGlpCntBefGpo"
v7 = this.mAppRequestCtlMap;
/* check-cast v7, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl; */
v7 = (( com.android.server.location.gnss.datacollect.GnssEventTrackingImpl$AppRequestCtl ) v7 ).getBefCtlCnt ( ); // invoke-virtual {v7}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl;->getBefCtlCnt()I
(( org.json.JSONObject ) v5 ).put ( v6, v7 ); // invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 506 */
final String v6 = "AppRequestGlpCntAftGpo"; // const-string v6, "AppRequestGlpCntAftGpo"
v7 = this.mAppRequestCtlMap;
/* check-cast v7, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl; */
v7 = (( com.android.server.location.gnss.datacollect.GnssEventTrackingImpl$AppRequestCtl ) v7 ).getAftCtlCnt ( ); // invoke-virtual {v7}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl;->getAftCtlCnt()I
(( org.json.JSONObject ) v5 ).put ( v6, v7 ); // invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 507 */
v6 = v6 = this.mNavAppTimeMap;
if ( v6 != null) { // if-eqz v6, :cond_5
/* .line 508 */
final String v6 = "NavAppTime"; // const-string v6, "NavAppTime"
v7 = this.mNavAppTimeMap;
(( org.json.JSONObject ) v5 ).put ( v6, v7 ); // invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 510 */
} // :cond_5
(( org.json.JSONObject ) v5 ).toString ( ); // invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
/* .line 511 */
/* nop */
} // .end local v3 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl;>;"
} // .end local v4 # "pkg":Ljava/lang/String;
} // .end local v5 # "jsonObject":Lorg/json/JSONObject;
/* .line 513 */
} // :cond_6
com.android.server.location.gnss.datacollect.GnssOneTrackManager .getInstance ( );
/* .line 514 */
/* .local v2, "instance":Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager; */
(( com.android.server.location.gnss.datacollect.GnssOneTrackManager ) v2 ).init ( p1 ); // invoke-virtual {v2, p1}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->init(Landroid/content/Context;)V
/* .line 515 */
(( com.android.server.location.gnss.datacollect.GnssOneTrackManager ) v2 ).track ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->track(Ljava/util/List;)V
/* .line 516 */
/* iget-boolean v3, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z */
if ( v3 != null) { // if-eqz v3, :cond_7
final String v3 = "GnssSavePoint"; // const-string v3, "GnssSavePoint"
/* const-string/jumbo v4, "startUploadAppRequest track success" */
android.util.Log .d ( v3,v4 );
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 521 */
} // .end local v1 # "dataList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // .end local v2 # "instance":Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;
} // :cond_7
/* .line 524 */
/* :catchall_0 */
/* move-exception v1 */
/* .line 519 */
/* :catch_0 */
/* move-exception v1 */
/* .line 520 */
/* .local v1, "e":Landroid/os/RemoteException; */
try { // :try_start_1
final String v2 = "GnssSavePoint"; // const-string v2, "GnssSavePoint"
/* const-string/jumbo v3, "startUploadAppRequest track RemoteException" */
android.util.Log .e ( v2,v3 );
/* .line 517 */
} // .end local v1 # "e":Landroid/os/RemoteException;
/* :catch_1 */
/* move-exception v1 */
/* .line 518 */
/* .local v1, "e":Lorg/json/JSONException; */
final String v2 = "GnssSavePoint"; // const-string v2, "GnssSavePoint"
/* const-string/jumbo v3, "startUploadAppRequest JSONException!" */
android.util.Log .e ( v2,v3 );
/* .line 521 */
/* nop */
/* .line 522 */
} // .end local v1 # "e":Lorg/json/JSONException;
} // :goto_1
v1 = this.mAppRequestCtlMap;
/* .line 523 */
v1 = this.mNavAppTimeMap;
/* .line 524 */
/* monitor-exit v0 */
/* .line 525 */
return;
/* .line 524 */
} // :goto_2
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
private void startUploadBackgroundOpt ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 401 */
/* const-class v0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl; */
/* monitor-enter v0 */
/* .line 402 */
try { // :try_start_0
/* new-instance v1, Lorg/json/JSONObject; */
/* invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 404 */
/* .local v1, "jsonObject":Lorg/json/JSONObject; */
try { // :try_start_1
final String v2 = "EVENT_NAME"; // const-string v2, "EVENT_NAME"
final String v3 = "GNSS_BACKGROUND_OPT"; // const-string v3, "GNSS_BACKGROUND_OPT"
(( org.json.JSONObject ) v1 ).put ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 405 */
final String v2 = "count2"; // const-string v2, "count2"
v3 = this.mBackgroundOpt2Cnt;
v3 = (( java.util.concurrent.atomic.AtomicInteger ) v3 ).get ( ); // invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I
(( org.json.JSONObject ) v1 ).put ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 406 */
final String v2 = "count3"; // const-string v2, "count3"
v3 = this.mBackgroundOpt3Cnt;
v3 = (( java.util.concurrent.atomic.AtomicInteger ) v3 ).get ( ); // invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I
(( org.json.JSONObject ) v1 ).put ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 407 */
com.android.server.location.gnss.datacollect.GnssOneTrackManager .getInstance ( );
/* .line 408 */
/* .local v2, "instance":Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager; */
(( com.android.server.location.gnss.datacollect.GnssOneTrackManager ) v2 ).init ( p1 ); // invoke-virtual {v2, p1}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->init(Landroid/content/Context;)V
/* .line 409 */
(( com.android.server.location.gnss.datacollect.GnssOneTrackManager ) v2 ).track ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->track(Lorg/json/JSONObject;)V
/* :try_end_1 */
/* .catch Lorg/json/JSONException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 414 */
} // .end local v2 # "instance":Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;
/* .line 412 */
/* :catch_0 */
/* move-exception v2 */
/* .line 413 */
/* .local v2, "e":Landroid/os/RemoteException; */
try { // :try_start_2
final String v3 = "GnssSavePoint"; // const-string v3, "GnssSavePoint"
/* const-string/jumbo v4, "startUploadBackgroundOpt RemoteException!" */
android.util.Log .e ( v3,v4 );
/* .line 410 */
} // .end local v2 # "e":Landroid/os/RemoteException;
/* :catch_1 */
/* move-exception v2 */
/* .line 411 */
/* .local v2, "e":Lorg/json/JSONException; */
final String v3 = "GnssSavePoint"; // const-string v3, "GnssSavePoint"
/* const-string/jumbo v4, "startUploadBackgroundOpt JSONException!" */
android.util.Log .e ( v3,v4 );
/* .line 414 */
/* nop */
/* .line 415 */
} // .end local v2 # "e":Lorg/json/JSONException;
} // :goto_0
v2 = this.mBackgroundOpt2Cnt;
int v3 = 0; // const/4 v3, 0x0
(( java.util.concurrent.atomic.AtomicInteger ) v2 ).set ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V
/* .line 416 */
v2 = this.mBackgroundOpt3Cnt;
(( java.util.concurrent.atomic.AtomicInteger ) v2 ).set ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V
/* .line 417 */
} // .end local v1 # "jsonObject":Lorg/json/JSONObject;
/* monitor-exit v0 */
/* .line 418 */
return;
/* .line 417 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
} // .end method
private void startUploadBlockListUsage ( android.content.Context p0 ) {
/* .locals 10 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 570 */
/* iget-boolean v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 571 */
final String v0 = "GnssSavePoint"; // const-string v0, "GnssSavePoint"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "startUploadBlockListUsage schedule current thread---->" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.Thread .currentThread ( );
(( java.lang.Thread ) v2 ).getName ( ); // invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v1 );
/* .line 572 */
} // :cond_0
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 573 */
/* .local v0, "dataList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v1 = v1 = this.mBlocklistControlBeanList;
/* .line 574 */
/* .local v1, "mCount":I */
/* if-nez v1, :cond_1 */
return;
/* .line 575 */
} // :cond_1
/* monitor-enter p0 */
/* .line 576 */
/* const-wide/16 v2, 0x0 */
/* .line 577 */
/* .local v2, "mTotalTime":J */
/* const-wide/16 v4, 0x0 */
/* .line 579 */
/* .local v4, "mEffectiveTime":J */
try { // :try_start_0
v6 = this.mBlocklistControlBeanList;
v7 = } // :goto_0
if ( v7 != null) { // if-eqz v7, :cond_2
/* check-cast v7, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean; */
/* .line 580 */
/* .local v7, "mBLControlBean":Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean; */
(( com.android.server.location.gnss.datacollect.GnssEventTrackingImpl$BlocklistControlBean ) v7 ).getTotalNaviTime ( ); // invoke-virtual {v7}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;->getTotalNaviTime()J
/* move-result-wide v8 */
/* add-long/2addr v2, v8 */
/* .line 581 */
(( com.android.server.location.gnss.datacollect.GnssEventTrackingImpl$BlocklistControlBean ) v7 ).getBlockedTime ( ); // invoke-virtual {v7}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;->getBlockedTime()J
/* move-result-wide v8 */
/* move-wide v4, v8 */
/* .line 582 */
} // .end local v7 # "mBLControlBean":Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;
/* .line 583 */
} // :cond_2
/* new-instance v6, Lorg/json/JSONObject; */
/* invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V */
/* .line 584 */
/* .local v6, "jsonObject":Lorg/json/JSONObject; */
final String v7 = "EVENT_NAME"; // const-string v7, "EVENT_NAME"
} // .end local v6 # "jsonObject":Lorg/json/JSONObject;
final String v8 = "GNSS_BLOCK_LIST_USAGE"; // const-string v8, "GNSS_BLOCK_LIST_USAGE"
(( org.json.JSONObject ) v6 ).put ( v7, v8 ); // invoke-virtual {v6, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 585 */
final String v7 = "navi_times"; // const-string v7, "navi_times"
(( org.json.JSONObject ) v6 ).put ( v7, v1 ); // invoke-virtual {v6, v7, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 586 */
/* const-string/jumbo v7, "total_navi_time" */
(( org.json.JSONObject ) v6 ).put ( v7, v2, v3 ); // invoke-virtual {v6, v7, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
/* .line 587 */
final String v7 = "effective_time"; // const-string v7, "effective_time"
(( org.json.JSONObject ) v6 ).put ( v7, v4, v5 ); // invoke-virtual {v6, v7, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
/* .line 588 */
(( org.json.JSONObject ) v6 ).toString ( ); // invoke-virtual {v6}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
/* .line 589 */
com.android.server.location.gnss.datacollect.GnssOneTrackManager .getInstance ( );
/* .line 590 */
/* .local v6, "instance":Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager; */
(( com.android.server.location.gnss.datacollect.GnssOneTrackManager ) v6 ).init ( p1 ); // invoke-virtual {v6, p1}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->init(Landroid/content/Context;)V
/* .line 591 */
(( com.android.server.location.gnss.datacollect.GnssOneTrackManager ) v6 ).track ( v0 ); // invoke-virtual {v6, v0}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->track(Ljava/util/List;)V
/* .line 592 */
/* iget-boolean v7, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z */
if ( v7 != null) { // if-eqz v7, :cond_3
final String v7 = "GnssSavePoint"; // const-string v7, "GnssSavePoint"
/* const-string/jumbo v8, "startUploadBlockListUsage track success" */
android.util.Log .d ( v7,v8 );
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 593 */
} // :cond_3
/* nop */
/* .line 598 */
} // .end local v6 # "instance":Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;
/* .line 600 */
} // .end local v2 # "mTotalTime":J
} // .end local v4 # "mEffectiveTime":J
/* :catchall_0 */
/* move-exception v2 */
/* .line 596 */
/* .restart local v2 # "mTotalTime":J */
/* .restart local v4 # "mEffectiveTime":J */
/* :catch_0 */
/* move-exception v6 */
/* .line 597 */
/* .local v6, "e":Landroid/os/RemoteException; */
try { // :try_start_1
final String v7 = "GnssSavePoint"; // const-string v7, "GnssSavePoint"
/* const-string/jumbo v8, "startUploadUseApp track RemoteException" */
android.util.Log .e ( v7,v8 );
/* .line 594 */
} // .end local v6 # "e":Landroid/os/RemoteException;
/* :catch_1 */
/* move-exception v6 */
/* .line 595 */
/* .local v6, "e":Lorg/json/JSONException; */
final String v7 = "GnssSavePoint"; // const-string v7, "GnssSavePoint"
final String v8 = "recordUseGnssApp JSONException!"; // const-string v8, "recordUseGnssApp JSONException!"
android.util.Log .e ( v7,v8 );
/* .line 598 */
/* nop */
/* .line 599 */
} // .end local v6 # "e":Lorg/json/JSONException;
} // :goto_1
v6 = this.mBlocklistControlBeanList;
/* .line 600 */
} // .end local v2 # "mTotalTime":J
} // .end local v4 # "mEffectiveTime":J
/* monitor-exit p0 */
/* .line 601 */
return;
/* .line 600 */
} // :goto_2
/* monitor-exit p0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v2 */
} // .end method
private void startUploadGnssData ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 135 */
com.android.server.location.ThreadPoolUtil .getInstance ( );
/* new-instance v1, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;Landroid/content/Context;)V */
(( com.android.server.location.ThreadPoolUtil ) v0 ).execute ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/location/ThreadPoolUtil;->execute(Ljava/lang/Runnable;)V
/* .line 144 */
return;
} // .end method
private void startUploadGnssEngineUsage ( android.content.Context p0 ) {
/* .locals 17 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 426 */
/* move-object/from16 v1, p0 */
/* const-class v2, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl; */
/* monitor-enter v2 */
/* .line 427 */
try { // :try_start_0
/* iget-boolean v0, v1, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
final String v0 = "GnssSavePoint"; // const-string v0, "GnssSavePoint"
/* const-string/jumbo v3, "startUploadGnssEngineUsage" */
android.util.Log .d ( v0,v3 );
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 429 */
} // :cond_0
/* const-wide/16 v3, 0x0 */
/* .line 430 */
/* .local v3, "timeBef":J */
/* const-wide/16 v5, 0x0 */
/* .line 431 */
/* .local v5, "timeAft":J */
/* const-wide/16 v7, 0x0 */
/* .line 432 */
/* .local v7, "typeCtrlAll":J */
/* const-wide/16 v9, 0x0 */
/* .line 433 */
/* .local v9, "typeCtrlPart":J */
/* const-wide/16 v11, 0x0 */
/* .line 434 */
/* .local v11, "typeCtrlNone":J */
try { // :try_start_1
v0 = this.mListEngineUsage;
v13 = } // :goto_0
if ( v13 != null) { // if-eqz v13, :cond_1
/* check-cast v13, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$GnssEngineUsage; */
/* .line 436 */
/* .local v13, "usage":Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$GnssEngineUsage; */
(( com.android.server.location.gnss.datacollect.GnssEventTrackingImpl$GnssEngineUsage ) v13 ).getGnssEngineShouldUseTime ( ); // invoke-virtual {v13}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$GnssEngineUsage;->getGnssEngineShouldUseTime()J
/* move-result-wide v14 */
/* add-long/2addr v3, v14 */
/* .line 437 */
(( com.android.server.location.gnss.datacollect.GnssEventTrackingImpl$GnssEngineUsage ) v13 ).getGnssEngineActuallyUseTime ( ); // invoke-virtual {v13}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$GnssEngineUsage;->getGnssEngineActuallyUseTime()J
/* move-result-wide v14 */
/* add-long/2addr v5, v14 */
/* .line 438 */
v14 = (( com.android.server.location.gnss.datacollect.GnssEventTrackingImpl$GnssEngineUsage ) v13 ).getGnssEngineControlState ( ); // invoke-virtual {v13}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$GnssEngineUsage;->getGnssEngineControlState()I
/* const-wide/16 v15, 0x1 */
/* packed-switch v14, :pswitch_data_0 */
/* .line 446 */
/* :pswitch_0 */
/* add-long/2addr v9, v15 */
/* .line 443 */
/* :pswitch_1 */
/* add-long/2addr v11, v15 */
/* .line 444 */
/* .line 440 */
/* :pswitch_2 */
/* add-long/2addr v7, v15 */
/* .line 441 */
/* nop */
/* .line 448 */
} // .end local v13 # "usage":Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$GnssEngineUsage;
} // :goto_1
/* .line 449 */
} // :cond_1
/* const-wide/16 v13, 0x0 */
/* cmp-long v0, v3, v13 */
/* if-nez v0, :cond_3 */
/* .line 450 */
/* iget-boolean v0, v1, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
final String v0 = "GnssSavePoint"; // const-string v0, "GnssSavePoint"
final String v13 = "No gnss data, do not upload GnssEngineUsage"; // const-string v13, "No gnss data, do not upload GnssEngineUsage"
android.util.Log .d ( v0,v13 );
/* :try_end_1 */
/* .catch Lorg/json/JSONException; {:try_start_1 ..:try_end_1} :catch_3 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_2 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 451 */
} // :cond_2
try { // :try_start_2
/* monitor-exit v2 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
return;
/* .line 453 */
} // :cond_3
try { // :try_start_3
com.android.server.location.gnss.hal.GpoUtil .getInstance ( );
v0 = (( com.android.server.location.gnss.hal.GpoUtil ) v0 ).getGpoVersion ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->getGpoVersion()I
int v13 = 3; // const/4 v13, 0x3
/* if-lt v0, v13, :cond_6 */
com.android.server.location.gnss.hal.GpoUtil .getInstance ( );
v0 = (( com.android.server.location.gnss.hal.GpoUtil ) v0 ).checkHeavyUser ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->checkHeavyUser()Z
if ( v0 != null) { // if-eqz v0, :cond_4
/* move-object/from16 v14, p1 */
/* .line 460 */
} // :cond_4
com.android.server.location.gnss.hal.GnssPowerOptimizeStub .getInstance ( );
/* .line 461 */
/* new-instance v0, Lorg/json/JSONObject; */
/* invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V */
/* .line 462 */
/* .local v0, "jsonObject":Lorg/json/JSONObject; */
final String v13 = "EVENT_NAME"; // const-string v13, "EVENT_NAME"
final String v14 = "GNSS_ENGINE_USAGE"; // const-string v14, "GNSS_ENGINE_USAGE"
(( org.json.JSONObject ) v0 ).put ( v13, v14 ); // invoke-virtual {v0, v13, v14}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 463 */
final String v13 = "gpo3TimeBef"; // const-string v13, "gpo3TimeBef"
(( org.json.JSONObject ) v0 ).put ( v13, v3, v4 ); // invoke-virtual {v0, v13, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
/* .line 464 */
final String v13 = "gpo3TimeAft"; // const-string v13, "gpo3TimeAft"
(( org.json.JSONObject ) v0 ).put ( v13, v5, v6 ); // invoke-virtual {v0, v13, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
/* .line 465 */
final String v13 = "gpo3CtrlTypeAll"; // const-string v13, "gpo3CtrlTypeAll"
(( org.json.JSONObject ) v0 ).put ( v13, v7, v8 ); // invoke-virtual {v0, v13, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
/* .line 466 */
final String v13 = "gpo3CtrlTypePart"; // const-string v13, "gpo3CtrlTypePart"
(( org.json.JSONObject ) v0 ).put ( v13, v9, v10 ); // invoke-virtual {v0, v13, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
/* .line 467 */
final String v13 = "gpo3CtrlTypeNone"; // const-string v13, "gpo3CtrlTypeNone"
(( org.json.JSONObject ) v0 ).put ( v13, v11, v12 ); // invoke-virtual {v0, v13, v11, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
/* .line 468 */
com.android.server.location.gnss.datacollect.GnssOneTrackManager .getInstance ( );
/* :try_end_3 */
/* .catch Lorg/json/JSONException; {:try_start_3 ..:try_end_3} :catch_3 */
/* .catch Landroid/os/RemoteException; {:try_start_3 ..:try_end_3} :catch_2 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 469 */
/* .local v13, "instance":Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager; */
/* move-object/from16 v14, p1 */
try { // :try_start_4
(( com.android.server.location.gnss.datacollect.GnssOneTrackManager ) v13 ).init ( v14 ); // invoke-virtual {v13, v14}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->init(Landroid/content/Context;)V
/* .line 470 */
(( com.android.server.location.gnss.datacollect.GnssOneTrackManager ) v13 ).track ( v0 ); // invoke-virtual {v13, v0}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->track(Lorg/json/JSONObject;)V
/* .line 471 */
/* iget-boolean v15, v1, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z */
if ( v15 != null) { // if-eqz v15, :cond_5
final String v15 = "GnssSavePoint"; // const-string v15, "GnssSavePoint"
/* move-object/from16 v16, v0 */
} // .end local v0 # "jsonObject":Lorg/json/JSONObject;
/* .local v16, "jsonObject":Lorg/json/JSONObject; */
/* const-string/jumbo v0, "startUploadGnssEngineUsage track success" */
android.util.Log .d ( v15,v0 );
} // .end local v16 # "jsonObject":Lorg/json/JSONObject;
/* .restart local v0 # "jsonObject":Lorg/json/JSONObject; */
} // :cond_5
/* move-object/from16 v16, v0 */
/* .line 476 */
} // .end local v0 # "jsonObject":Lorg/json/JSONObject;
} // .end local v3 # "timeBef":J
} // .end local v5 # "timeAft":J
} // .end local v7 # "typeCtrlAll":J
} // .end local v9 # "typeCtrlPart":J
} // .end local v11 # "typeCtrlNone":J
} // .end local v13 # "instance":Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;
} // :goto_2
/* .line 453 */
/* .restart local v3 # "timeBef":J */
/* .restart local v5 # "timeAft":J */
/* .restart local v7 # "typeCtrlAll":J */
/* .restart local v9 # "typeCtrlPart":J */
/* .restart local v11 # "typeCtrlNone":J */
} // :cond_6
/* move-object/from16 v14, p1 */
/* .line 456 */
} // :goto_3
/* iget-boolean v0, v1, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z */
if ( v0 != null) { // if-eqz v0, :cond_7
final String v0 = "GnssSavePoint"; // const-string v0, "GnssSavePoint"
final String v13 = "Not enable gpo or isHeavyUser"; // const-string v13, "Not enable gpo or isHeavyUser"
android.util.Log .d ( v0,v13 );
/* .line 457 */
} // :cond_7
com.android.server.location.gnss.hal.GnssPowerOptimizeStub .getInstance ( );
/* :try_end_4 */
/* .catch Lorg/json/JSONException; {:try_start_4 ..:try_end_4} :catch_1 */
/* .catch Landroid/os/RemoteException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* .line 458 */
try { // :try_start_5
/* monitor-exit v2 */
return;
/* .line 474 */
} // .end local v3 # "timeBef":J
} // .end local v5 # "timeAft":J
} // .end local v7 # "typeCtrlAll":J
} // .end local v9 # "typeCtrlPart":J
} // .end local v11 # "typeCtrlNone":J
/* :catch_0 */
/* move-exception v0 */
/* .line 472 */
/* :catch_1 */
/* move-exception v0 */
/* .line 474 */
/* :catch_2 */
/* move-exception v0 */
/* move-object/from16 v14, p1 */
/* .line 475 */
/* .local v0, "e":Landroid/os/RemoteException; */
} // :goto_4
final String v3 = "GnssSavePoint"; // const-string v3, "GnssSavePoint"
/* const-string/jumbo v4, "startUploadGnssEngineUsage track RemoteException" */
android.util.Log .e ( v3,v4 );
/* .line 472 */
} // .end local v0 # "e":Landroid/os/RemoteException;
/* :catch_3 */
/* move-exception v0 */
/* move-object/from16 v14, p1 */
/* .line 473 */
/* .local v0, "e":Lorg/json/JSONException; */
} // :goto_5
final String v3 = "GnssSavePoint"; // const-string v3, "GnssSavePoint"
/* const-string/jumbo v4, "startUploadGnssEngineUsage JSONException!" */
android.util.Log .e ( v3,v4 );
/* .line 476 */
/* nop */
/* .line 477 */
} // .end local v0 # "e":Lorg/json/JSONException;
} // :goto_6
v0 = this.mListEngineUsage;
/* .line 478 */
/* monitor-exit v2 */
/* .line 479 */
return;
/* .line 478 */
/* :catchall_0 */
/* move-exception v0 */
/* move-object/from16 v14, p1 */
} // :goto_7
/* monitor-exit v2 */
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_1 */
/* throw v0 */
/* :catchall_1 */
/* move-exception v0 */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_2 */
/* :pswitch_0 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
private void startUploadGnssMockLocationOpt ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 734 */
/* const-class v0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl; */
/* monitor-enter v0 */
/* .line 735 */
try { // :try_start_0
/* new-instance v1, Lorg/json/JSONObject; */
/* invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 737 */
/* .local v1, "jsonObject":Lorg/json/JSONObject; */
try { // :try_start_1
final String v2 = "EVENT_NAME"; // const-string v2, "EVENT_NAME"
final String v3 = "GNSS_MOCK_LOCATION_OPT"; // const-string v3, "GNSS_MOCK_LOCATION_OPT"
(( org.json.JSONObject ) v1 ).put ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 739 */
final String v2 = "count"; // const-string v2, "count"
v3 = this.mGnssMockLocationTimes;
(( org.json.JSONObject ) v1 ).put ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 740 */
final String v2 = "gmo1_during"; // const-string v2, "gmo1_during"
v3 = this.mGnssMockLocationInterval;
(( org.json.JSONObject ) v1 ).put ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 742 */
com.android.server.location.gnss.datacollect.GnssOneTrackManager .getInstance ( );
/* .line 743 */
/* .local v2, "instance":Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager; */
(( com.android.server.location.gnss.datacollect.GnssOneTrackManager ) v2 ).init ( p1 ); // invoke-virtual {v2, p1}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->init(Landroid/content/Context;)V
/* .line 744 */
(( com.android.server.location.gnss.datacollect.GnssOneTrackManager ) v2 ).track ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->track(Lorg/json/JSONObject;)V
/* :try_end_1 */
/* .catch Lorg/json/JSONException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 749 */
} // .end local v2 # "instance":Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;
/* .line 747 */
/* :catch_0 */
/* move-exception v2 */
/* .line 748 */
/* .local v2, "e":Landroid/os/RemoteException; */
try { // :try_start_2
final String v3 = "GnssSavePoint"; // const-string v3, "GnssSavePoint"
/* const-string/jumbo v4, "startUploadGnssMockLocationOpt RemoteException!" */
android.util.Log .e ( v3,v4 );
/* .line 745 */
} // .end local v2 # "e":Landroid/os/RemoteException;
/* :catch_1 */
/* move-exception v2 */
/* .line 746 */
/* .local v2, "e":Lorg/json/JSONException; */
final String v3 = "GnssSavePoint"; // const-string v3, "GnssSavePoint"
/* const-string/jumbo v4, "startUploadGnssMockLocationOpt JSONException!" */
android.util.Log .e ( v3,v4 );
/* .line 749 */
/* nop */
/* .line 750 */
} // .end local v2 # "e":Lorg/json/JSONException;
} // :goto_0
v2 = this.mGnssMockLocationTimes;
/* const-wide/16 v3, 0x0 */
(( java.util.concurrent.atomic.AtomicLong ) v2 ).set ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V
/* .line 751 */
v2 = this.mGnssMockLocationInterval;
(( java.util.concurrent.atomic.AtomicLong ) v2 ).set ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V
/* .line 752 */
} // .end local v1 # "jsonObject":Lorg/json/JSONObject;
/* monitor-exit v0 */
/* .line 753 */
return;
/* .line 752 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
} // .end method
private void startUploadGnssSatelliteCallOpt ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 163 */
/* const-class v0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl; */
/* monitor-enter v0 */
/* .line 164 */
try { // :try_start_0
/* new-instance v1, Lorg/json/JSONObject; */
/* invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 166 */
/* .local v1, "jsonObject":Lorg/json/JSONObject; */
try { // :try_start_1
final String v2 = "EVENT_NAME"; // const-string v2, "EVENT_NAME"
final String v3 = "GNSS_SATELLITE_CALL_OPT"; // const-string v3, "GNSS_SATELLITE_CALL_OPT"
(( org.json.JSONObject ) v1 ).put ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 167 */
final String v2 = "count"; // const-string v2, "count"
v3 = this.mSatelliteCallCnt;
v3 = (( java.util.concurrent.atomic.AtomicInteger ) v3 ).get ( ); // invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I
(( org.json.JSONObject ) v1 ).put ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 168 */
final String v2 = "during"; // const-string v2, "during"
/* iget-wide v3, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mSatelliteCallDuring:J */
(( org.json.JSONObject ) v1 ).put ( v2, v3, v4 ); // invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
/* .line 169 */
com.android.server.location.gnss.datacollect.GnssOneTrackManager .getInstance ( );
/* .line 170 */
/* .local v2, "instance":Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager; */
(( com.android.server.location.gnss.datacollect.GnssOneTrackManager ) v2 ).init ( p1 ); // invoke-virtual {v2, p1}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->init(Landroid/content/Context;)V
/* .line 171 */
(( com.android.server.location.gnss.datacollect.GnssOneTrackManager ) v2 ).track ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->track(Lorg/json/JSONObject;)V
/* :try_end_1 */
/* .catch Lorg/json/JSONException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 176 */
} // .end local v2 # "instance":Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;
/* .line 174 */
/* :catch_0 */
/* move-exception v2 */
/* .line 175 */
/* .local v2, "e":Landroid/os/RemoteException; */
try { // :try_start_2
final String v3 = "GnssSavePoint"; // const-string v3, "GnssSavePoint"
/* const-string/jumbo v4, "startUploadGnssSatelliteCallOpt RemoteException!" */
android.util.Log .e ( v3,v4 );
/* .line 172 */
} // .end local v2 # "e":Landroid/os/RemoteException;
/* :catch_1 */
/* move-exception v2 */
/* .line 173 */
/* .local v2, "e":Lorg/json/JSONException; */
final String v3 = "GnssSavePoint"; // const-string v3, "GnssSavePoint"
/* const-string/jumbo v4, "startUploadGnssSatelliteCallOpt JSONException!" */
android.util.Log .e ( v3,v4 );
/* .line 176 */
/* nop */
/* .line 177 */
} // .end local v2 # "e":Lorg/json/JSONException;
} // :goto_0
v2 = this.mSatelliteCallCnt;
int v3 = 0; // const/4 v3, 0x0
(( java.util.concurrent.atomic.AtomicInteger ) v2 ).set ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V
/* .line 178 */
/* const-wide/16 v2, 0x0 */
/* iput-wide v2, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mSatelliteCallDuring:J */
/* .line 179 */
} // .end local v1 # "jsonObject":Lorg/json/JSONObject;
/* monitor-exit v0 */
/* .line 180 */
return;
/* .line 179 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
} // .end method
private void startUploadUseApp ( android.content.Context p0 ) {
/* .locals 10 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 533 */
/* iget-boolean v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 534 */
final String v0 = "GnssSavePoint"; // const-string v0, "GnssSavePoint"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "startUploadUseAppTimer schedule current thread---->" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.Thread .currentThread ( );
(( java.lang.Thread ) v2 ).getName ( ); // invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v1 );
/* .line 535 */
} // :cond_0
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 536 */
/* .local v0, "dataList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* new-instance v1, Ljava/util/HashMap; */
/* invoke-direct {v1}, Ljava/util/HashMap;-><init>()V */
/* .line 537 */
/* .local v1, "map":Ljava/util/Map;, "Ljava/util/Map<Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;Ljava/lang/Integer;>;" */
/* const-class v2, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl; */
/* monitor-enter v2 */
/* .line 538 */
try { // :try_start_0
v3 = this.mListUseApp;
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_1
/* check-cast v4, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean; */
/* .line 539 */
/* .local v4, "bean":Lcom/android/server/location/gnss/datacollect/UseGnssAppBean; */
int v5 = 0; // const/4 v5, 0x0
java.lang.Integer .valueOf ( v5 );
/* check-cast v5, Ljava/lang/Integer; */
v5 = (( java.lang.Integer ) v5 ).intValue ( ); // invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I
/* add-int/lit8 v5, v5, 0x1 */
java.lang.Integer .valueOf ( v5 );
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 540 */
/* nop */
} // .end local v4 # "bean":Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;
/* .line 542 */
} // :cond_1
try { // :try_start_1
v4 = } // :goto_1
if ( v4 != null) { // if-eqz v4, :cond_2
/* check-cast v4, Ljava/util/Map$Entry; */
/* .line 543 */
/* .local v4, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;Ljava/lang/Integer;>;" */
/* check-cast v5, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean; */
v5 = this.packageName;
/* .line 544 */
/* .local v5, "pkg":Ljava/lang/String; */
/* new-instance v6, Lorg/json/JSONObject; */
/* invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V */
/* .line 545 */
/* .local v6, "jsonObject":Lorg/json/JSONObject; */
final String v7 = "EVENT_NAME"; // const-string v7, "EVENT_NAME"
final String v8 = "GPS_USE_APP"; // const-string v8, "GPS_USE_APP"
(( org.json.JSONObject ) v6 ).put ( v7, v8 ); // invoke-virtual {v6, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 546 */
final String v7 = "packageName"; // const-string v7, "packageName"
(( org.json.JSONObject ) v6 ).put ( v7, v5 ); // invoke-virtual {v6, v7, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 547 */
final String v7 = "report_interval"; // const-string v7, "report_interval"
/* check-cast v8, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean; */
v8 = this.reportInterval;
(( org.json.JSONObject ) v6 ).put ( v7, v8 ); // invoke-virtual {v6, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 548 */
final String v7 = "glp_during"; // const-string v7, "glp_during"
v8 = this.mGlpDuringMap;
(( org.json.JSONObject ) v6 ).put ( v7, v8 ); // invoke-virtual {v6, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 549 */
final String v7 = "glp_during_background"; // const-string v7, "glp_during_background"
v8 = this.mGlpDuringBackground;
(( org.json.JSONObject ) v6 ).put ( v7, v8 ); // invoke-virtual {v6, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 550 */
final String v7 = "count"; // const-string v7, "count"
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = ""; // const-string v9, ""
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( org.json.JSONObject ) v6 ).put ( v7, v8 ); // invoke-virtual {v6, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 551 */
(( org.json.JSONObject ) v6 ).toString ( ); // invoke-virtual {v6}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
/* .line 552 */
/* nop */
} // .end local v4 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;Ljava/lang/Integer;>;"
} // .end local v5 # "pkg":Ljava/lang/String;
} // .end local v6 # "jsonObject":Lorg/json/JSONObject;
/* .line 554 */
} // :cond_2
com.android.server.location.gnss.datacollect.GnssOneTrackManager .getInstance ( );
/* .line 555 */
/* .local v3, "instance":Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager; */
(( com.android.server.location.gnss.datacollect.GnssOneTrackManager ) v3 ).init ( p1 ); // invoke-virtual {v3, p1}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->init(Landroid/content/Context;)V
/* .line 556 */
(( com.android.server.location.gnss.datacollect.GnssOneTrackManager ) v3 ).track ( v0 ); // invoke-virtual {v3, v0}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->track(Ljava/util/List;)V
/* .line 557 */
/* iget-boolean v4, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z */
if ( v4 != null) { // if-eqz v4, :cond_3
final String v4 = "GnssSavePoint"; // const-string v4, "GnssSavePoint"
/* const-string/jumbo v5, "startUploadUseApp track success" */
android.util.Log .d ( v4,v5 );
/* :try_end_1 */
/* .catch Lorg/json/JSONException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 562 */
} // .end local v3 # "instance":Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;
} // :cond_3
/* .line 560 */
/* :catch_0 */
/* move-exception v3 */
/* .line 561 */
/* .local v3, "e":Landroid/os/RemoteException; */
try { // :try_start_2
final String v4 = "GnssSavePoint"; // const-string v4, "GnssSavePoint"
/* const-string/jumbo v5, "startUploadUseApp track RemoteException" */
android.util.Log .e ( v4,v5 );
/* .line 558 */
} // .end local v3 # "e":Landroid/os/RemoteException;
/* :catch_1 */
/* move-exception v3 */
/* .line 559 */
/* .local v3, "e":Lorg/json/JSONException; */
final String v4 = "GnssSavePoint"; // const-string v4, "GnssSavePoint"
final String v5 = "recordUseGnssApp JSONException!"; // const-string v5, "recordUseGnssApp JSONException!"
android.util.Log .e ( v4,v5 );
/* .line 562 */
/* nop */
/* .line 563 */
} // .end local v3 # "e":Lorg/json/JSONException;
} // :goto_2
v3 = this.mListUseApp;
/* .line 564 */
v3 = this.mGlpDuringMap;
/* .line 565 */
v3 = this.mGlpDuringBackground;
/* .line 566 */
/* monitor-exit v2 */
/* .line 567 */
return;
/* .line 566 */
/* :catchall_0 */
/* move-exception v3 */
/* monitor-exit v2 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v3 */
} // .end method
/* # virtual methods */
public void init ( android.content.Context p0 ) {
/* .locals 0 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 122 */
this.mGlpContext = p1;
/* .line 123 */
return;
} // .end method
public void recordChangeToBackground ( java.lang.String p0, Integer p1, java.lang.String p2, Boolean p3, Boolean p4 ) {
/* .locals 7 */
/* .param p1, "provider" # Ljava/lang/String; */
/* .param p2, "callingIdentityHashCode" # I */
/* .param p3, "pkgName" # Ljava/lang/String; */
/* .param p4, "foreground" # Z */
/* .param p5, "hasLocationPermissions" # Z */
/* .line 225 */
if ( p5 != null) { // if-eqz p5, :cond_5
final String v0 = "gps"; // const-string v0, "gps"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 226 */
v0 = this.mRequestMap;
java.lang.Integer .valueOf ( p2 );
/* check-cast v0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean; */
/* .line 227 */
/* .local v0, "bean":Lcom/android/server/location/gnss/datacollect/UseGnssAppBean; */
if ( v0 != null) { // if-eqz v0, :cond_4
/* iget-wide v1, v0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->requestTime:J */
/* const-wide/16 v3, 0x0 */
/* cmp-long v1, v1, v3 */
/* if-nez v1, :cond_0 */
/* goto/16 :goto_2 */
/* .line 228 */
} // :cond_0
if ( p4 != null) { // if-eqz p4, :cond_1
/* .line 229 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v1 */
/* iput-wide v1, v0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->changeToforeTime:J */
/* .line 230 */
/* iget-wide v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGlpBackTime:J */
/* iget-wide v3, v0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->changeToforeTime:J */
/* iget-wide v5, v0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->changeToBackTime:J */
/* sub-long/2addr v3, v5 */
/* add-long/2addr v1, v3 */
/* iput-wide v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGlpBackTime:J */
/* .line 232 */
} // :cond_1
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v1 */
/* iput-wide v1, v0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->changeToBackTime:J */
/* .line 233 */
/* iget-wide v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGlpForeTime:J */
/* iget-wide v3, v0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->changeToBackTime:J */
/* iget-wide v5, v0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->changeToforeTime:J */
/* sub-long/2addr v3, v5 */
/* add-long/2addr v1, v3 */
/* iput-wide v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGlpForeTime:J */
/* .line 235 */
} // :goto_0
/* iget-boolean v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 236 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " recordChangeToBackground callingIdentity:"; // const-string v2, " recordChangeToBackground callingIdentity:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = "pkgName:"; // const-string v2, "pkgName:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 237 */
if ( p4 != null) { // if-eqz p4, :cond_2
final String v2 = " foreground"; // const-string v2, " foreground"
} // :cond_2
final String v2 = " background"; // const-string v2, " background"
} // :goto_1
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " GlpForeTime:"; // const-string v2, " GlpForeTime:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v2, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGlpForeTime:J */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v2 = " GlpBackTime:"; // const-string v2, " GlpBackTime:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v2, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGlpBackTime:J */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 236 */
final String v2 = "GnssSavePoint"; // const-string v2, "GnssSavePoint"
android.util.Log .d ( v2,v1 );
/* .line 239 */
} // :cond_3
v1 = this.mRequestMap;
java.lang.Integer .valueOf ( p2 );
/* .line 227 */
} // :cond_4
} // :goto_2
return;
/* .line 241 */
} // .end local v0 # "bean":Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;
} // :cond_5
} // :goto_3
return;
} // .end method
public void recordEngineUsage ( Integer p0, Long p1 ) {
/* .locals 9 */
/* .param p1, "type" # I */
/* .param p2, "milliseconds" # J */
/* .line 289 */
/* const-class v0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl; */
/* monitor-enter v0 */
/* .line 290 */
try { // :try_start_0
/* iget v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mLastEngineStatus:I */
/* if-ne v1, p1, :cond_0 */
/* monitor-exit v0 */
return;
/* .line 291 */
} // :cond_0
/* iput p1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mLastEngineStatus:I */
/* .line 292 */
/* iget-boolean v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
final String v1 = "GnssSavePoint"; // const-string v1, "GnssSavePoint"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "recordEngineUsage, type="; // const-string v3, "recordEngineUsage, type="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ", currentTime="; // const-string v3, ", currentTime="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2, p3 ); // invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v2 );
/* .line 293 */
} // :cond_1
/* packed-switch p1, :pswitch_data_0 */
/* .line 316 */
/* invoke-direct {p0}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->resetGpo3Var()V */
/* .line 313 */
/* :pswitch_0 */
/* invoke-direct {p0, p2, p3}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->calCurEngineUsage(J)V */
/* .line 314 */
/* .line 310 */
/* :pswitch_1 */
/* iput-wide p2, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineControlTime:J */
/* .line 311 */
/* .line 300 */
/* :pswitch_2 */
/* iget-wide v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineStartTime:J */
/* const-wide/16 v3, 0x0 */
/* cmp-long v5, v1, v3 */
if ( v5 != null) { // if-eqz v5, :cond_2
/* iget-wide v5, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineControlTime:J */
/* cmp-long v7, v5, v3 */
if ( v7 != null) { // if-eqz v7, :cond_2
/* .line 301 */
/* iget-wide v7, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineTimeAftGpo3:J */
/* sub-long/2addr v5, v1 */
/* add-long/2addr v7, v5 */
/* iput-wide v7, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineTimeAftGpo3:J */
/* .line 302 */
/* iput-wide v3, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineControlTime:J */
/* .line 304 */
} // :cond_2
/* iput-wide p2, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineStartTime:J */
/* .line 305 */
/* iget-wide v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineBlockTime:J */
/* cmp-long v1, v1, v3 */
/* if-nez v1, :cond_3 */
/* .line 306 */
/* iput-wide p2, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineBlockTime:J */
/* .line 297 */
/* :pswitch_3 */
/* iput-wide p2, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineBlockTime:J */
/* .line 298 */
/* nop */
/* .line 318 */
} // :cond_3
} // :goto_0
/* iget-boolean v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->hasStartUploadData:Z */
/* if-nez v1, :cond_4 */
/* .line 319 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->hasStartUploadData:Z */
/* .line 320 */
v1 = this.mGlpContext;
final String v2 = "action upload data"; // const-string v2, "action upload data"
int v3 = 3; // const/4 v3, 0x3
/* invoke-direct {p0, v1, v2, v3}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->setAlarm(Landroid/content/Context;Ljava/lang/String;I)V */
/* .line 322 */
} // :cond_4
/* monitor-exit v0 */
/* .line 323 */
return;
/* .line 322 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public void recordGnssBackgroundOpt2Time ( ) {
/* .locals 3 */
/* .line 374 */
v0 = this.mBackgroundOpt2Cnt;
v0 = (( java.util.concurrent.atomic.AtomicInteger ) v0 ).incrementAndGet ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I
/* .line 375 */
/* .local v0, "cnt":I */
/* iget-boolean v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "recordGnssBackgroundOptTime:"; // const-string v2, "recordGnssBackgroundOptTime:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "GnssSavePoint"; // const-string v2, "GnssSavePoint"
android.util.Log .d ( v2,v1 );
/* .line 376 */
} // :cond_0
return;
} // .end method
public void recordGnssBackgroundOpt3Time ( ) {
/* .locals 3 */
/* .line 380 */
v0 = this.mBackgroundOpt3Cnt;
v0 = (( java.util.concurrent.atomic.AtomicInteger ) v0 ).incrementAndGet ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I
/* .line 381 */
/* .local v0, "cnt":I */
/* iget-boolean v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "recordGnssBackgroundOptTime:"; // const-string v2, "recordGnssBackgroundOptTime:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "GnssSavePoint"; // const-string v2, "GnssSavePoint"
android.util.Log .d ( v2,v1 );
/* .line 382 */
} // :cond_0
return;
} // .end method
public void recordGnssSatelliteCallOptCnt ( ) {
/* .locals 4 */
/* .line 148 */
v0 = this.mSatelliteCallCnt;
v0 = (( java.util.concurrent.atomic.AtomicInteger ) v0 ).incrementAndGet ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I
/* .line 149 */
/* .local v0, "cnt":I */
/* iget-boolean v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "recordGnssSatelliteCallOptCnt:"; // const-string v2, "recordGnssSatelliteCallOptCnt:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "GnssSavePoint"; // const-string v2, "GnssSavePoint"
android.util.Log .d ( v2,v1 );
/* .line 150 */
} // :cond_0
/* iget-boolean v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->hasStartUploadData:Z */
/* if-nez v1, :cond_1 */
v1 = this.mGlpContext;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 151 */
int v2 = 1; // const/4 v2, 0x1
/* iput-boolean v2, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->hasStartUploadData:Z */
/* .line 152 */
final String v2 = "action upload data"; // const-string v2, "action upload data"
int v3 = 4; // const/4 v3, 0x4
/* invoke-direct {p0, v1, v2, v3}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->setAlarm(Landroid/content/Context;Ljava/lang/String;I)V */
/* .line 154 */
} // :cond_1
return;
} // .end method
public void recordGnssSatelliteCallOptDuring ( Long p0 ) {
/* .locals 3 */
/* .param p1, "during" # J */
/* .line 158 */
/* iget-wide v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mSatelliteCallDuring:J */
/* add-long/2addr v0, p1 */
/* iput-wide v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mSatelliteCallDuring:J */
/* .line 159 */
/* iget-boolean v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "recordGnssSatelliteCallOptDuring:"; // const-string v1, "recordGnssSatelliteCallOptDuring:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mSatelliteCallDuring:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "GnssSavePoint"; // const-string v1, "GnssSavePoint"
android.util.Log .d ( v1,v0 );
/* .line 160 */
} // :cond_0
return;
} // .end method
public void recordNavAppTime ( java.lang.String p0, Long p1 ) {
/* .locals 3 */
/* .param p1, "pkn" # Ljava/lang/String; */
/* .param p2, "time" # J */
/* .line 327 */
if ( p1 != null) { // if-eqz p1, :cond_1
/* const-wide/16 v0, 0x0 */
/* cmp-long v2, p2, v0 */
/* if-gtz v2, :cond_0 */
/* .line 328 */
} // :cond_0
v2 = this.mNavAppTimeMap;
java.lang.Long .valueOf ( v0,v1 );
/* check-cast v0, Ljava/lang/Long; */
(( java.lang.Long ) v0 ).longValue ( ); // invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
/* move-result-wide v0 */
/* add-long/2addr v0, p2 */
java.lang.Long .valueOf ( v0,v1 );
/* .line 329 */
return;
/* .line 327 */
} // :cond_1
} // :goto_0
return;
} // .end method
public void recordRemove ( java.lang.String p0, Integer p1, Boolean p2, Boolean p3 ) {
/* .locals 16 */
/* .param p1, "provider" # Ljava/lang/String; */
/* .param p2, "callingIdentityHashCode" # I */
/* .param p3, "foreground" # Z */
/* .param p4, "hasLocationPermissions" # Z */
/* .line 245 */
/* move-object/from16 v8, p0 */
final String v0 = "gps"; // const-string v0, "gps"
/* move-object/from16 v9, p1 */
v0 = (( java.lang.String ) v0 ).equals ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 246 */
v0 = this.mRequestMap;
/* invoke-static/range {p2 ..p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer; */
/* move-object v10, v0 */
/* check-cast v10, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean; */
/* .line 247 */
/* .local v10, "bean":Lcom/android/server/location/gnss/datacollect/UseGnssAppBean; */
/* invoke-direct {v8, v10}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->recordAppRemove(Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;)V */
/* .line 248 */
/* const-wide/16 v11, 0x0 */
if ( v10 != null) { // if-eqz v10, :cond_3
/* iget-wide v0, v10, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->requestTime:J */
/* cmp-long v0, v0, v11 */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 249 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v0 */
/* iget-wide v2, v10, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->requestTime:J */
/* sub-long v13, v0, v2 */
/* .line 250 */
/* .local v13, "glpDuring":J */
final String v15 = "GnssSavePoint"; // const-string v15, "GnssSavePoint"
if ( p4 != null) { // if-eqz p4, :cond_1
/* .line 251 */
/* iget-wide v0, v10, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->changeToforeTime:J */
/* iget-wide v2, v10, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->changeToBackTime:J */
/* cmp-long v0, v0, v2 */
/* if-gez v0, :cond_0 */
/* .line 252 */
/* iget-wide v0, v8, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGlpForeTime:J */
/* sub-long v0, v13, v0 */
/* iput-wide v0, v8, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGlpBackTime:J */
/* .line 253 */
/* iget-boolean v0, v8, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 254 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "remove on the background and true BackTime is "; // const-string v1, "remove on the background and true BackTime is "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, v8, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGlpBackTime:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v15,v0 );
/* .line 255 */
} // :cond_0
/* iget-wide v0, v10, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->changeToforeTime:J */
/* iget-wide v2, v10, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->changeToBackTime:J */
/* cmp-long v0, v0, v2 */
/* if-nez v0, :cond_2 */
/* if-nez p3, :cond_2 */
/* .line 256 */
/* iput-wide v13, v8, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGlpBackTime:J */
/* .line 259 */
} // :cond_1
final String v0 = "do not have Location Permissions, do not need record background time..."; // const-string v0, "do not have Location Permissions, do not need record background time..."
android.util.Log .d ( v15,v0 );
/* .line 261 */
} // :cond_2
} // :goto_0
v1 = this.mGlpContext;
v2 = this.packageName;
v3 = this.reportInterval;
/* iget-wide v6, v8, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGlpBackTime:J */
/* move-object/from16 v0, p0 */
/* move-wide v4, v13 */
/* invoke-direct/range {v0 ..v7}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->recordCallerGnssApp(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JJ)V */
/* .line 262 */
/* iget-boolean v0, v8, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 263 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "packageName:"; // const-string v1, "packageName:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.packageName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " glpDuring:"; // const-string v1, " glpDuring:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v13, v14 ); // invoke-virtual {v0, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = " GlpBackTime:"; // const-string v1, " GlpBackTime:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, v8, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGlpBackTime:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v15,v0 );
/* .line 265 */
} // .end local v13 # "glpDuring":J
} // :cond_3
v0 = this.mRequestMap;
/* invoke-static/range {p2 ..p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer; */
/* .line 266 */
/* iput-wide v11, v8, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGlpBackTime:J */
/* .line 267 */
/* iput-wide v11, v8, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGlpForeTime:J */
/* .line 269 */
} // .end local v10 # "bean":Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;
} // :cond_4
return;
} // .end method
public void recordRequest ( java.lang.String p0, Integer p1, java.lang.String p2, Long p3 ) {
/* .locals 3 */
/* .param p1, "provider" # Ljava/lang/String; */
/* .param p2, "callingIdentityHashCode" # I */
/* .param p3, "pkgName" # Ljava/lang/String; */
/* .param p4, "intervalMs" # J */
/* .line 209 */
final String v0 = "gps"; // const-string v0, "gps"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 210 */
/* new-instance v0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean; */
/* invoke-direct {v0}, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;-><init>()V */
/* .line 211 */
/* .local v0, "bean":Lcom/android/server/location/gnss/datacollect/UseGnssAppBean; */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v1 */
/* iput-wide v1, v0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->requestTime:J */
/* .line 212 */
this.packageName = p3;
/* .line 213 */
java.lang.String .valueOf ( p4,p5 );
this.reportInterval = v1;
/* .line 214 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v1 */
/* iput-wide v1, v0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->changeToBackTime:J */
/* .line 215 */
/* iget-wide v1, v0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->changeToBackTime:J */
/* iput-wide v1, v0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->changeToforeTime:J */
/* .line 216 */
v1 = this.mRequestMap;
java.lang.Integer .valueOf ( p2 );
/* .line 217 */
/* invoke-direct {p0, p3}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->putIntoAppRequestCtlMap(Ljava/lang/String;)V */
/* .line 218 */
/* iget-boolean v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 219 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " recordRequest callingIdentity:"; // const-string v2, " recordRequest callingIdentity:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "GnssSavePoint"; // const-string v2, "GnssSavePoint"
android.util.Log .d ( v2,v1 );
/* .line 221 */
} // .end local v0 # "bean":Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;
} // :cond_0
return;
} // .end method
public void recordSatelliteBlockListChanged ( Long p0, Long p1, java.lang.String p2 ) {
/* .locals 8 */
/* .param p1, "mTotalNaviTime" # J */
/* .param p3, "mEffectiveTime" # J */
/* .param p5, "mPkn" # Ljava/lang/String; */
/* .line 364 */
/* const-wide/16 v0, 0x0 */
/* cmp-long v2, p1, v0 */
/* if-lez v2, :cond_0 */
/* cmp-long v0, p3, v0 */
/* if-lez v0, :cond_0 */
/* .line 365 */
/* new-instance v0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean; */
/* move-object v1, v0 */
/* move-object v2, p0 */
/* move-wide v3, p1 */
/* move-wide v5, p3 */
/* move-object v7, p5 */
/* invoke-direct/range {v1 ..v7}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;-><init>(Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;JJLjava/lang/String;)V */
/* .line 366 */
/* .local v0, "mBlocklistControlBean":Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean; */
v1 = this.mBlocklistControlBeanList;
/* .line 367 */
com.android.server.location.gnss.GnssLocationProviderStub .getInstance ( );
(( com.android.server.location.gnss.datacollect.GnssEventTrackingImpl$BlocklistControlBean ) v0 ).toString ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;->toString()Ljava/lang/String;
/* .line 368 */
/* iget-boolean v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
final String v1 = "TAG"; // const-string v1, "TAG"
(( com.android.server.location.gnss.datacollect.GnssEventTrackingImpl$BlocklistControlBean ) v0 ).toString ( ); // invoke-virtual {v0}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v2 );
/* .line 370 */
} // .end local v0 # "mBlocklistControlBean":Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;
} // :cond_0
return;
} // .end method
public void usingBatInMockModeInterval ( Long p0 ) {
/* .locals 4 */
/* .param p1, "interval" # J */
/* .line 729 */
v0 = this.mGnssMockLocationInterval;
(( java.util.concurrent.atomic.AtomicLong ) v0 ).addAndGet ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J
/* move-result-wide v0 */
/* .line 730 */
/* .local v0, "cnt":J */
/* iget-boolean v2, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "usingBatInMockModeInterval: " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0, v1 ); // invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "GnssSavePoint"; // const-string v3, "GnssSavePoint"
android.util.Log .d ( v3,v2 );
/* .line 731 */
} // :cond_0
return;
} // .end method
public void usingBatInMockModeTimes ( Long p0 ) {
/* .locals 4 */
/* .param p1, "times" # J */
/* .line 723 */
v0 = this.mGnssMockLocationTimes;
(( java.util.concurrent.atomic.AtomicLong ) v0 ).addAndGet ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J
/* move-result-wide v0 */
/* .line 724 */
/* .local v0, "cnt":J */
/* iget-boolean v2, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "usingBatInMockModeTimes: " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0, v1 ); // invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "GnssSavePoint"; // const-string v3, "GnssSavePoint"
android.util.Log .d ( v3,v2 );
/* .line 725 */
} // :cond_0
return;
} // .end method
