.class Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl;
.super Ljava/lang/Object;
.source "GnssEventTrackingImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AppRequestCtl"
.end annotation


# instance fields
.field private aftCtl:I

.field private befCtl:I

.field final synthetic this$0:Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;


# direct methods
.method private constructor <init>(Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;)V
    .locals 0

    .line 653
    iput-object p1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl;->this$0:Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 654
    const/4 p1, 0x1

    iput p1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl;->befCtl:I

    .line 655
    iput p1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl;->aftCtl:I

    .line 656
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl;-><init>(Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;)V

    return-void
.end method


# virtual methods
.method public addRequestCnt(II)V
    .locals 1
    .param p1, "befDeltaAdd"    # I
    .param p2, "aftDeltaAdd"    # I

    .line 659
    iget v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl;->befCtl:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl;->befCtl:I

    .line 660
    iget v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl;->aftCtl:I

    add-int/2addr v0, p2

    iput v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl;->aftCtl:I

    .line 661
    return-void
.end method

.method public getAftCtlCnt()I
    .locals 1

    .line 668
    iget v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl;->aftCtl:I

    return v0
.end method

.method public getBefCtlCnt()I
    .locals 1

    .line 664
    iget v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl;->befCtl:I

    return v0
.end method
