.class public Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;
.super Ljava/lang/Object;
.source "GnssEventTrackingImpl.java"

# interfaces
.implements Lcom/android/server/location/gnss/GnssEventTrackingStub;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl;,
        Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$GnssEngineUsage;,
        Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;
    }
.end annotation


# static fields
.field private static final ACTION_UPLOAD_DATA:Ljava/lang/String; = "action upload data"

.field private static final APP_REQUEST_GLP_CNT_AFT_GPO:Ljava/lang/String; = "AppRequestGlpCntAftGpo"

.field private static final APP_REQUEST_GLP_CNT_BEF_GPO:Ljava/lang/String; = "AppRequestGlpCntBefGpo"

.field private static final EVENT_APP_REQUEST_GLP_CNT:Ljava/lang/String; = "AppRequestGlpCnt"

.field private static final EVENT_BLOCK_LIST_USAGE:Ljava/lang/String; = "GNSS_BLOCK_LIST_USAGE"

.field private static final EVENT_GNSS_ENGINE_USAGE:Ljava/lang/String; = "GNSS_ENGINE_USAGE"

.field private static final EVENT_GPS_USE_APP:Ljava/lang/String; = "GPS_USE_APP"

.field private static final EVENT_NAME:Ljava/lang/String; = "EVENT_NAME"

.field private static final GMO_POSITION_INTERVAL:Ljava/lang/String; = "gmo1_during"

.field private static final GMO_POSITION_TIMES:Ljava/lang/String; = "count"

.field private static final GNSS_BACKGROUND_OPT:Ljava/lang/String; = "GNSS_BACKGROUND_OPT"

.field private static final GNSS_MOCK_LOCATION_OPT:Ljava/lang/String; = "GNSS_MOCK_LOCATION_OPT"

.field private static final GNSS_SATELLITE_CALL_OPT:Ljava/lang/String; = "GNSS_SATELLITE_CALL_OPT"

.field private static final GPO3_CTRL_TYPE_ALL:Ljava/lang/String; = "gpo3CtrlTypeAll"

.field private static final GPO3_CTRL_TYPE_NONE:Ljava/lang/String; = "gpo3CtrlTypeNone"

.field private static final GPO3_CTRL_TYPE_PART:Ljava/lang/String; = "gpo3CtrlTypePart"

.field private static final GPO3_TIME_AFT:Ljava/lang/String; = "gpo3TimeAft"

.field private static final GPO3_TIME_BEF:Ljava/lang/String; = "gpo3TimeBef"

.field private static final NAV_APP_TIME:Ljava/lang/String; = "NavAppTime"

.field private static final PACKAGE_NAME:Ljava/lang/String; = "packageName"

.field private static final REQUEST_CODE_GNSS_ENGINE_USAGE:I = 0x3

.field private static final REQUEST_CODE_SATELLITE_CALL_OPT:I = 0x4

.field private static final REQUEST_CODE_USE_APP:I = 0x0

.field private static final TAG:Ljava/lang/String; = "GnssSavePoint"

.field private static final TYPE_GNSS_ENGINE_CONTROL_ALL:I = 0x0

.field private static final TYPE_GNSS_ENGINE_CONTROL_NONE:I = 0x2

.field private static final TYPE_GNSS_ENGINE_CONTROL_PART:I = 0x1


# instance fields
.field private final D:Z

.field private START_INTERVAL:I

.field private UPLOAD_REPEAT_TIME:I

.field private hasStartUploadData:Z

.field private final isSavePoint:Z

.field private final mAppRequestCtlMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl;",
            ">;"
        }
    .end annotation
.end field

.field private mBackgroundOpt2Cnt:Ljava/util/concurrent/atomic/AtomicInteger;

.field private mBackgroundOpt3Cnt:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final mBlocklistControlBeanList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;",
            ">;"
        }
    .end annotation
.end field

.field private mEngineBlockTime:J

.field private mEngineControlTime:J

.field private mEngineStartTime:J

.field private mEngineStopTime:J

.field private mEngineTimeAftGpo3:J

.field private mEngineTimeBefGpo3:J

.field private mGlpBackTime:J

.field private mGlpContext:Landroid/content/Context;

.field private final mGlpDuringBackground:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final mGlpDuringMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mGlpForeTime:J

.field private mGnssMockLocationInterval:Ljava/util/concurrent/atomic/AtomicLong;

.field private mGnssMockLocationTimes:Ljava/util/concurrent/atomic/AtomicLong;

.field private mIsGnssPowerRecord:Z

.field private mLastEngineStatus:I

.field private final mListEngineUsage:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$GnssEngineUsage;",
            ">;"
        }
    .end annotation
.end field

.field private final mListUseApp:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;",
            ">;"
        }
    .end annotation
.end field

.field private final mNavAppTimeMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private final mRequestMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;",
            ">;"
        }
    .end annotation
.end field

.field private mSatelliteCallCnt:Ljava/util/concurrent/atomic/AtomicInteger;

.field private mSatelliteCallDuring:J


# direct methods
.method public static synthetic $r8$lambda$iLpkYKjax4NPB2bo6yMTIy161lg(Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->lambda$startUploadGnssData$0(Landroid/content/Context;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mstartUploadGnssData(Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->startUploadGnssData(Landroid/content/Context;)V

    return-void
.end method

.method constructor <init>()V
    .locals 5

    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    const-string v0, "persist.sys.miui_gnss_dc"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->isSavePoint:Z

    .line 63
    const-string v2, "persist.sys.gnss_dc.test"

    invoke-static {v2, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z

    .line 75
    const v3, 0x5265c00

    iput v3, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->UPLOAD_REPEAT_TIME:I

    .line 76
    const v3, 0x493e0

    iput v3, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->START_INTERVAL:I

    .line 79
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mIsGnssPowerRecord:Z

    .line 91
    const/4 v3, 0x4

    iput v3, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mLastEngineStatus:I

    .line 94
    new-instance v3, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v3, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mListUseApp:Ljava/util/List;

    .line 95
    new-instance v3, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v3}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v3, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mRequestMap:Ljava/util/Map;

    .line 96
    new-instance v3, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v3}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v3, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGlpDuringMap:Ljava/util/Map;

    .line 97
    new-instance v3, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v3}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v3, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGlpDuringBackground:Ljava/util/Map;

    .line 98
    new-instance v3, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v3, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mListEngineUsage:Ljava/util/List;

    .line 99
    new-instance v3, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v3}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v3, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mAppRequestCtlMap:Ljava/util/Map;

    .line 100
    new-instance v3, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v3}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v3, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mNavAppTimeMap:Ljava/util/Map;

    .line 101
    new-instance v3, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v3, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mBlocklistControlBeanList:Ljava/util/List;

    .line 102
    new-instance v3, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v3, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v3, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mBackgroundOpt2Cnt:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 103
    new-instance v3, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v3, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v3, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mBackgroundOpt3Cnt:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 104
    new-instance v3, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v3, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v3, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mSatelliteCallCnt:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 105
    new-instance v1, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v3, 0x0

    invoke-direct {v1, v3, v4}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGnssMockLocationTimes:Ljava/util/concurrent/atomic/AtomicLong;

    .line 106
    new-instance v1, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v1, v3, v4}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGnssMockLocationInterval:Ljava/util/concurrent/atomic/AtomicLong;

    .line 125
    new-instance v1, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$1;

    invoke-direct {v1, p0}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$1;-><init>(Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;)V

    iput-object v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 112
    if-eqz v2, :cond_0

    .line 113
    const/16 v1, 0xbb8

    iput v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->UPLOAD_REPEAT_TIME:I

    .line 114
    iput v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->START_INTERVAL:I

    .line 116
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Is specified platform:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  upload repeat time:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->UPLOAD_REPEAT_TIME:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  start interval:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->START_INTERVAL:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GnssSavePoint"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    return-void
.end method

.method private calCurEngineUsage(J)V
    .locals 11
    .param p1, "milliseconds"    # J

    .line 332
    const-class v0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;

    monitor-enter v0

    .line 333
    :try_start_0
    iput-wide p1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineStopTime:J

    .line 336
    iget-wide v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineControlTime:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    .line 337
    iput-wide p1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineControlTime:J

    .line 340
    :cond_0
    iget-wide v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineStartTime:J

    cmp-long v5, v1, v3

    if-nez v5, :cond_1

    .line 341
    iput-wide v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineControlTime:J

    .line 344
    :cond_1
    iget-wide v5, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineBlockTime:J

    sub-long v5, p1, v5

    iput-wide v5, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineTimeBefGpo3:J

    .line 346
    iget-wide v5, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineTimeAftGpo3:J

    iget-wide v7, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineControlTime:J

    sub-long/2addr v7, v1

    add-long/2addr v5, v7

    iput-wide v5, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineTimeAftGpo3:J

    .line 347
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "recordEngineUsage, mEngineTimeBefGpo3="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v5, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineTimeBefGpo3:J

    invoke-virtual {v1, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mEngineTimeAftGpo3="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v5, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineTimeAftGpo3:J

    invoke-virtual {v1, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", saved milliseconds is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v5, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineTimeBefGpo3:J

    iget-wide v7, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineTimeAftGpo3:J

    sub-long/2addr v5, v7

    invoke-virtual {v1, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 350
    .local v1, "dumpInfo":Ljava/lang/String;
    iget-boolean v2, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z

    if-eqz v2, :cond_2

    .line 351
    const-string v2, "GnssSavePoint"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    :cond_2
    iget-wide v5, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineTimeBefGpo3:J

    iget-wide v7, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineTimeAftGpo3:J

    cmp-long v2, v5, v7

    if-ltz v2, :cond_3

    cmp-long v2, v5, v3

    if-lez v2, :cond_3

    .line 354
    new-instance v9, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$GnssEngineUsage;

    const/4 v10, 0x0

    move-object v2, v9

    move-object v3, p0

    move-wide v4, v5

    move-wide v6, v7

    move-object v8, v10

    invoke-direct/range {v2 .. v8}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$GnssEngineUsage;-><init>(Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;JJLcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$GnssEngineUsage-IA;)V

    move-object v2, v9

    .line 355
    .local v2, "gnssEngineUsage":Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$GnssEngineUsage;
    iget-object v3, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mListEngineUsage:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 356
    invoke-static {}, Lcom/android/server/location/gnss/GnssLocationProviderStub;->getInstance()Lcom/android/server/location/gnss/GnssLocationProviderStub;

    move-result-object v3

    invoke-interface {v3, v1}, Lcom/android/server/location/gnss/GnssLocationProviderStub;->writeLocationInformation(Ljava/lang/String;)V

    .line 358
    .end local v2    # "gnssEngineUsage":Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$GnssEngineUsage;
    :cond_3
    invoke-direct {p0}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->resetGpo3Var()V

    .line 359
    .end local v1    # "dumpInfo":Ljava/lang/String;
    monitor-exit v0

    .line 360
    return-void

    .line 359
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private synthetic lambda$startUploadGnssData$0(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .line 136
    invoke-direct {p0, p1}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->startUploadUseApp(Landroid/content/Context;)V

    .line 137
    invoke-direct {p0, p1}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->startUploadAppRequest(Landroid/content/Context;)V

    .line 138
    invoke-direct {p0, p1}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->startUploadGnssEngineUsage(Landroid/content/Context;)V

    .line 139
    invoke-direct {p0, p1}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->startUploadBlockListUsage(Landroid/content/Context;)V

    .line 140
    invoke-direct {p0, p1}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->startUploadBackgroundOpt(Landroid/content/Context;)V

    .line 141
    invoke-direct {p0, p1}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->startUploadGnssMockLocationOpt(Landroid/content/Context;)V

    .line 142
    invoke-direct {p0, p1}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->startUploadGnssSatelliteCallOpt(Landroid/content/Context;)V

    .line 143
    return-void
.end method

.method private putIntoAppRequestCtlMap(Ljava/lang/String;)V
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 272
    iget-object v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mAppRequestCtlMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 273
    iget-object v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mAppRequestCtlMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v1}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl;->addRequestCnt(II)V

    .line 274
    return-void

    .line 276
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mAppRequestCtlMap:Ljava/util/Map;

    new-instance v1, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl;-><init>(Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl-IA;)V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 277
    return-void
.end method

.method private recordAppRemove(Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;)V
    .locals 3
    .param p1, "bean"    # Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;

    .line 280
    if-nez p1, :cond_0

    return-void

    .line 281
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mAppRequestCtlMap:Ljava/util/Map;

    iget-object v1, p1, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->packageName:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl;

    .line 282
    .local v0, "appRequestCtl":Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl;
    if-eqz v0, :cond_1

    iget v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mLastEngineStatus:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 283
    const/4 v1, 0x0

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl;->addRequestCnt(II)V

    .line 285
    :cond_1
    return-void
.end method

.method private recordCallerGnssApp(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JJ)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "interval"    # Ljava/lang/String;
    .param p4, "glpDuring"    # J
    .param p6, "glpBackgroundDuring"    # J

    .line 183
    if-eqz p1, :cond_6

    if-eqz p2, :cond_6

    if-nez p3, :cond_0

    goto/16 :goto_0

    .line 186
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->isSavePoint:Z

    const-string v1, "GnssSavePoint"

    if-nez v0, :cond_2

    .line 187
    iget-boolean v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z

    if-eqz v0, :cond_1

    const-string v0, "record Use Gnss App----> Not the specified platform"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    :cond_1
    return-void

    .line 190
    :cond_2
    iget-object v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mListUseApp:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const v2, 0x7fffffff

    if-lt v0, v2, :cond_3

    .line 191
    const-string/jumbo v0, "use app list.size() >= Integer.MAX_VALUE----> return"

    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    return-void

    .line 194
    :cond_3
    new-instance v0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;

    invoke-direct {v0}, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;-><init>()V

    .line 195
    .local v0, "bean":Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;
    iput-object p2, v0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->packageName:Ljava/lang/String;

    .line 196
    iput-object p3, v0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->reportInterval:Ljava/lang/String;

    .line 197
    iget-object v2, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGlpDuringMap:Ljava/util/Map;

    const-wide/16 v3, 0x0

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v2, p2, v5}, Ljava/util/Map;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    add-long/2addr v5, p4

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v2, p2, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    iget-object v2, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGlpDuringBackground:Ljava/util/Map;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, p2, v3}, Ljava/util/Map;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    add-long/2addr v3, p6

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, p2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 199
    iget-object v2, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mListUseApp:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 200
    iget-boolean v2, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z

    if-eqz v2, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "record Use Gnss App----> "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    :cond_4
    iget-boolean v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->hasStartUploadData:Z

    if-nez v1, :cond_5

    .line 202
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->hasStartUploadData:Z

    .line 203
    const-string v1, "action upload data"

    const/4 v2, 0x0

    invoke-direct {p0, p1, v1, v2}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->setAlarm(Landroid/content/Context;Ljava/lang/String;I)V

    .line 205
    :cond_5
    return-void

    .line 184
    .end local v0    # "bean":Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;
    :cond_6
    :goto_0
    return-void
.end method

.method private resetGpo3Var()V
    .locals 3

    .line 385
    const-class v0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;

    monitor-enter v0

    .line 386
    const-wide/16 v1, 0x0

    :try_start_0
    iput-wide v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineBlockTime:J

    .line 387
    iput-wide v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineStartTime:J

    .line 388
    iput-wide v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineStopTime:J

    .line 389
    iput-wide v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineControlTime:J

    .line 390
    iput-wide v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineTimeBefGpo3:J

    .line 391
    iput-wide v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineTimeAftGpo3:J

    .line 392
    monitor-exit v0

    .line 393
    return-void

    .line 392
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private setAlarm(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 16
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "action"    # Ljava/lang/String;
    .param p3, "requestCode"    # I

    .line 604
    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    if-eqz v2, :cond_1

    if-nez v3, :cond_0

    move/from16 v14, p3

    goto :goto_1

    .line 606
    :cond_0
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    move-object v4, v0

    .line 607
    .local v4, "filter":Landroid/content/IntentFilter;
    invoke-virtual {v4, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 608
    iget-object v0, v1, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v0, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 609
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v5

    .line 611
    .local v5, "token":J
    :try_start_0
    const-string v0, "alarm"

    invoke-virtual {v2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/app/AlarmManager;

    .line 612
    .local v7, "alarmManager":Landroid/app/AlarmManager;
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 613
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v8, 0x4000000

    move/from16 v14, p3

    :try_start_1
    invoke-static {v2, v14, v0, v8}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v13

    .line 614
    .local v13, "p":Landroid/app/PendingIntent;
    const/4 v8, 0x2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v9

    iget v11, v1, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->UPLOAD_REPEAT_TIME:I

    move-object v15, v0

    .end local v0    # "intent":Landroid/content/Intent;
    .local v15, "intent":Landroid/content/Intent;
    int-to-long v0, v11

    add-long/2addr v9, v0

    int-to-long v11, v11

    invoke-virtual/range {v7 .. v13}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 616
    .end local v7    # "alarmManager":Landroid/app/AlarmManager;
    .end local v13    # "p":Landroid/app/PendingIntent;
    .end local v15    # "intent":Landroid/content/Intent;
    invoke-static {v5, v6}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 617
    nop

    .line 618
    return-void

    .line 616
    :catchall_0
    move-exception v0

    goto :goto_0

    :catchall_1
    move-exception v0

    move/from16 v14, p3

    :goto_0
    invoke-static {v5, v6}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 617
    throw v0

    .line 604
    .end local v4    # "filter":Landroid/content/IntentFilter;
    .end local v5    # "token":J
    :cond_1
    move/from16 v14, p3

    :goto_1
    return-void
.end method

.method private startUploadAppRequest(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .line 487
    iget-boolean v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z

    if-eqz v0, :cond_0

    .line 488
    const-string v0, "GnssSavePoint"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "startUploadAppRequest schedule current thread---->"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 489
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mAppRequestCtlMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 490
    iget-boolean v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z

    if-eqz v0, :cond_1

    const-string v0, "GnssSavePoint"

    const-string v1, "No App Request Data, skip upload."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 491
    :cond_1
    return-void

    .line 493
    :cond_2
    iget v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mLastEngineStatus:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_4

    .line 494
    iget-boolean v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z

    if-eqz v0, :cond_3

    const-string v0, "GnssSavePoint"

    const-string v1, "Gnss engine is working, skip upload."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 495
    :cond_3
    return-void

    .line 497
    :cond_4
    const-class v0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;

    monitor-enter v0

    .line 499
    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 500
    .local v1, "dataList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mAppRequestCtlMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 501
    .local v3, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 502
    .local v4, "pkg":Ljava/lang/String;
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 503
    .local v5, "jsonObject":Lorg/json/JSONObject;
    const-string v6, "EVENT_NAME"

    const-string v7, "AppRequestGlpCnt"

    invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 504
    const-string v6, "packageName"

    invoke-virtual {v5, v6, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 505
    const-string v6, "AppRequestGlpCntBefGpo"

    iget-object v7, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mAppRequestCtlMap:Ljava/util/Map;

    invoke-interface {v7, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl;

    invoke-virtual {v7}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl;->getBefCtlCnt()I

    move-result v7

    invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 506
    const-string v6, "AppRequestGlpCntAftGpo"

    iget-object v7, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mAppRequestCtlMap:Ljava/util/Map;

    invoke-interface {v7, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl;

    invoke-virtual {v7}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl;->getAftCtlCnt()I

    move-result v7

    invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 507
    iget-object v6, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mNavAppTimeMap:Ljava/util/Map;

    invoke-interface {v6, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 508
    const-string v6, "NavAppTime"

    iget-object v7, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mNavAppTimeMap:Ljava/util/Map;

    invoke-interface {v7, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 510
    :cond_5
    invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 511
    nop

    .end local v3    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl;>;"
    .end local v4    # "pkg":Ljava/lang/String;
    .end local v5    # "jsonObject":Lorg/json/JSONObject;
    goto :goto_0

    .line 513
    :cond_6
    invoke-static {}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->getInstance()Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;

    move-result-object v2

    .line 514
    .local v2, "instance":Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;
    invoke-virtual {v2, p1}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->init(Landroid/content/Context;)V

    .line 515
    invoke-virtual {v2, v1}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->track(Ljava/util/List;)V

    .line 516
    iget-boolean v3, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z

    if-eqz v3, :cond_7

    const-string v3, "GnssSavePoint"

    const-string/jumbo v4, "startUploadAppRequest track success"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 521
    .end local v1    # "dataList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v2    # "instance":Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;
    :cond_7
    goto :goto_1

    .line 524
    :catchall_0
    move-exception v1

    goto :goto_2

    .line 519
    :catch_0
    move-exception v1

    .line 520
    .local v1, "e":Landroid/os/RemoteException;
    :try_start_1
    const-string v2, "GnssSavePoint"

    const-string/jumbo v3, "startUploadAppRequest track RemoteException"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 517
    .end local v1    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v1

    .line 518
    .local v1, "e":Lorg/json/JSONException;
    const-string v2, "GnssSavePoint"

    const-string/jumbo v3, "startUploadAppRequest JSONException!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 521
    nop

    .line 522
    .end local v1    # "e":Lorg/json/JSONException;
    :goto_1
    iget-object v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mAppRequestCtlMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 523
    iget-object v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mNavAppTimeMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 524
    monitor-exit v0

    .line 525
    return-void

    .line 524
    :goto_2
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private startUploadBackgroundOpt(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 401
    const-class v0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;

    monitor-enter v0

    .line 402
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 404
    .local v1, "jsonObject":Lorg/json/JSONObject;
    :try_start_1
    const-string v2, "EVENT_NAME"

    const-string v3, "GNSS_BACKGROUND_OPT"

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 405
    const-string v2, "count2"

    iget-object v3, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mBackgroundOpt2Cnt:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 406
    const-string v2, "count3"

    iget-object v3, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mBackgroundOpt3Cnt:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 407
    invoke-static {}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->getInstance()Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;

    move-result-object v2

    .line 408
    .local v2, "instance":Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;
    invoke-virtual {v2, p1}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->init(Landroid/content/Context;)V

    .line 409
    invoke-virtual {v2, v1}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->track(Lorg/json/JSONObject;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 414
    .end local v2    # "instance":Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;
    goto :goto_0

    .line 412
    :catch_0
    move-exception v2

    .line 413
    .local v2, "e":Landroid/os/RemoteException;
    :try_start_2
    const-string v3, "GnssSavePoint"

    const-string/jumbo v4, "startUploadBackgroundOpt RemoteException!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 410
    .end local v2    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v2

    .line 411
    .local v2, "e":Lorg/json/JSONException;
    const-string v3, "GnssSavePoint"

    const-string/jumbo v4, "startUploadBackgroundOpt JSONException!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 414
    nop

    .line 415
    .end local v2    # "e":Lorg/json/JSONException;
    :goto_0
    iget-object v2, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mBackgroundOpt2Cnt:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 416
    iget-object v2, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mBackgroundOpt3Cnt:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 417
    .end local v1    # "jsonObject":Lorg/json/JSONObject;
    monitor-exit v0

    .line 418
    return-void

    .line 417
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method private startUploadBlockListUsage(Landroid/content/Context;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;

    .line 570
    iget-boolean v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z

    if-eqz v0, :cond_0

    .line 571
    const-string v0, "GnssSavePoint"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "startUploadBlockListUsage schedule current thread---->"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 572
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 573
    .local v0, "dataList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mBlocklistControlBeanList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    .line 574
    .local v1, "mCount":I
    if-nez v1, :cond_1

    return-void

    .line 575
    :cond_1
    monitor-enter p0

    .line 576
    const-wide/16 v2, 0x0

    .line 577
    .local v2, "mTotalTime":J
    const-wide/16 v4, 0x0

    .line 579
    .local v4, "mEffectiveTime":J
    :try_start_0
    iget-object v6, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mBlocklistControlBeanList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;

    .line 580
    .local v7, "mBLControlBean":Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;
    invoke-virtual {v7}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;->getTotalNaviTime()J

    move-result-wide v8

    add-long/2addr v2, v8

    .line 581
    invoke-virtual {v7}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;->getBlockedTime()J

    move-result-wide v8

    move-wide v4, v8

    .line 582
    .end local v7    # "mBLControlBean":Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;
    goto :goto_0

    .line 583
    :cond_2
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V

    .line 584
    .local v6, "jsonObject":Lorg/json/JSONObject;
    const-string v7, "EVENT_NAME"

    .end local v6    # "jsonObject":Lorg/json/JSONObject;
    const-string v8, "GNSS_BLOCK_LIST_USAGE"

    invoke-virtual {v6, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 585
    const-string v7, "navi_times"

    invoke-virtual {v6, v7, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 586
    const-string/jumbo v7, "total_navi_time"

    invoke-virtual {v6, v7, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 587
    const-string v7, "effective_time"

    invoke-virtual {v6, v7, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 588
    invoke-virtual {v6}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 589
    invoke-static {}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->getInstance()Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;

    move-result-object v6

    .line 590
    .local v6, "instance":Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;
    invoke-virtual {v6, p1}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->init(Landroid/content/Context;)V

    .line 591
    invoke-virtual {v6, v0}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->track(Ljava/util/List;)V

    .line 592
    iget-boolean v7, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z

    if-eqz v7, :cond_3

    const-string v7, "GnssSavePoint"

    const-string/jumbo v8, "startUploadBlockListUsage track success"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 593
    :cond_3
    nop

    .line 598
    .end local v6    # "instance":Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;
    goto :goto_1

    .line 600
    .end local v2    # "mTotalTime":J
    .end local v4    # "mEffectiveTime":J
    :catchall_0
    move-exception v2

    goto :goto_2

    .line 596
    .restart local v2    # "mTotalTime":J
    .restart local v4    # "mEffectiveTime":J
    :catch_0
    move-exception v6

    .line 597
    .local v6, "e":Landroid/os/RemoteException;
    :try_start_1
    const-string v7, "GnssSavePoint"

    const-string/jumbo v8, "startUploadUseApp track RemoteException"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 594
    .end local v6    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v6

    .line 595
    .local v6, "e":Lorg/json/JSONException;
    const-string v7, "GnssSavePoint"

    const-string v8, "recordUseGnssApp JSONException!"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 598
    nop

    .line 599
    .end local v6    # "e":Lorg/json/JSONException;
    :goto_1
    iget-object v6, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mBlocklistControlBeanList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->clear()V

    .line 600
    .end local v2    # "mTotalTime":J
    .end local v4    # "mEffectiveTime":J
    monitor-exit p0

    .line 601
    return-void

    .line 600
    :goto_2
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method private startUploadGnssData(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 135
    invoke-static {}, Lcom/android/server/location/ThreadPoolUtil;->getInstance()Lcom/android/server/location/ThreadPoolUtil;

    move-result-object v0

    new-instance v1, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p1}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/android/server/location/ThreadPoolUtil;->execute(Ljava/lang/Runnable;)V

    .line 144
    return-void
.end method

.method private startUploadGnssEngineUsage(Landroid/content/Context;)V
    .locals 17
    .param p1, "context"    # Landroid/content/Context;

    .line 426
    move-object/from16 v1, p0

    const-class v2, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;

    monitor-enter v2

    .line 427
    :try_start_0
    iget-boolean v0, v1, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z

    if-eqz v0, :cond_0

    const-string v0, "GnssSavePoint"

    const-string/jumbo v3, "startUploadGnssEngineUsage"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 429
    :cond_0
    const-wide/16 v3, 0x0

    .line 430
    .local v3, "timeBef":J
    const-wide/16 v5, 0x0

    .line 431
    .local v5, "timeAft":J
    const-wide/16 v7, 0x0

    .line 432
    .local v7, "typeCtrlAll":J
    const-wide/16 v9, 0x0

    .line 433
    .local v9, "typeCtrlPart":J
    const-wide/16 v11, 0x0

    .line 434
    .local v11, "typeCtrlNone":J
    :try_start_1
    iget-object v0, v1, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mListEngineUsage:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$GnssEngineUsage;

    .line 436
    .local v13, "usage":Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$GnssEngineUsage;
    invoke-virtual {v13}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$GnssEngineUsage;->getGnssEngineShouldUseTime()J

    move-result-wide v14

    add-long/2addr v3, v14

    .line 437
    invoke-virtual {v13}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$GnssEngineUsage;->getGnssEngineActuallyUseTime()J

    move-result-wide v14

    add-long/2addr v5, v14

    .line 438
    invoke-virtual {v13}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$GnssEngineUsage;->getGnssEngineControlState()I

    move-result v14

    const-wide/16 v15, 0x1

    packed-switch v14, :pswitch_data_0

    .line 446
    :pswitch_0
    add-long/2addr v9, v15

    goto :goto_1

    .line 443
    :pswitch_1
    add-long/2addr v11, v15

    .line 444
    goto :goto_1

    .line 440
    :pswitch_2
    add-long/2addr v7, v15

    .line 441
    nop

    .line 448
    .end local v13    # "usage":Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$GnssEngineUsage;
    :goto_1
    goto :goto_0

    .line 449
    :cond_1
    const-wide/16 v13, 0x0

    cmp-long v0, v3, v13

    if-nez v0, :cond_3

    .line 450
    iget-boolean v0, v1, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z

    if-eqz v0, :cond_2

    const-string v0, "GnssSavePoint"

    const-string v13, "No gnss data, do not upload GnssEngineUsage"

    invoke-static {v0, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 451
    :cond_2
    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    return-void

    .line 453
    :cond_3
    :try_start_3
    invoke-static {}, Lcom/android/server/location/gnss/hal/GpoUtil;->getInstance()Lcom/android/server/location/gnss/hal/GpoUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->getGpoVersion()I

    move-result v0

    const/4 v13, 0x3

    if-lt v0, v13, :cond_6

    invoke-static {}, Lcom/android/server/location/gnss/hal/GpoUtil;->getInstance()Lcom/android/server/location/gnss/hal/GpoUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/location/gnss/hal/GpoUtil;->checkHeavyUser()Z

    move-result v0

    if-eqz v0, :cond_4

    move-object/from16 v14, p1

    goto :goto_3

    .line 460
    :cond_4
    invoke-static {}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeStub;->getInstance()Lcom/android/server/location/gnss/hal/GnssPowerOptimizeStub;

    move-result-object v0

    invoke-interface {v0, v5, v6}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeStub;->recordEngineUsageDaily(J)V

    .line 461
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 462
    .local v0, "jsonObject":Lorg/json/JSONObject;
    const-string v13, "EVENT_NAME"

    const-string v14, "GNSS_ENGINE_USAGE"

    invoke-virtual {v0, v13, v14}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 463
    const-string v13, "gpo3TimeBef"

    invoke-virtual {v0, v13, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 464
    const-string v13, "gpo3TimeAft"

    invoke-virtual {v0, v13, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 465
    const-string v13, "gpo3CtrlTypeAll"

    invoke-virtual {v0, v13, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 466
    const-string v13, "gpo3CtrlTypePart"

    invoke-virtual {v0, v13, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 467
    const-string v13, "gpo3CtrlTypeNone"

    invoke-virtual {v0, v13, v11, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 468
    invoke-static {}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->getInstance()Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;

    move-result-object v13
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 469
    .local v13, "instance":Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;
    move-object/from16 v14, p1

    :try_start_4
    invoke-virtual {v13, v14}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->init(Landroid/content/Context;)V

    .line 470
    invoke-virtual {v13, v0}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->track(Lorg/json/JSONObject;)V

    .line 471
    iget-boolean v15, v1, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z

    if-eqz v15, :cond_5

    const-string v15, "GnssSavePoint"

    move-object/from16 v16, v0

    .end local v0    # "jsonObject":Lorg/json/JSONObject;
    .local v16, "jsonObject":Lorg/json/JSONObject;
    const-string/jumbo v0, "startUploadGnssEngineUsage track success"

    invoke-static {v15, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .end local v16    # "jsonObject":Lorg/json/JSONObject;
    .restart local v0    # "jsonObject":Lorg/json/JSONObject;
    :cond_5
    move-object/from16 v16, v0

    .line 476
    .end local v0    # "jsonObject":Lorg/json/JSONObject;
    .end local v3    # "timeBef":J
    .end local v5    # "timeAft":J
    .end local v7    # "typeCtrlAll":J
    .end local v9    # "typeCtrlPart":J
    .end local v11    # "typeCtrlNone":J
    .end local v13    # "instance":Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;
    :goto_2
    goto :goto_6

    .line 453
    .restart local v3    # "timeBef":J
    .restart local v5    # "timeAft":J
    .restart local v7    # "typeCtrlAll":J
    .restart local v9    # "typeCtrlPart":J
    .restart local v11    # "typeCtrlNone":J
    :cond_6
    move-object/from16 v14, p1

    .line 456
    :goto_3
    iget-boolean v0, v1, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z

    if-eqz v0, :cond_7

    const-string v0, "GnssSavePoint"

    const-string v13, "Not enable gpo or isHeavyUser"

    invoke-static {v0, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    :cond_7
    invoke-static {}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeStub;->getInstance()Lcom/android/server/location/gnss/hal/GnssPowerOptimizeStub;

    move-result-object v0

    invoke-interface {v0, v5, v6}, Lcom/android/server/location/gnss/hal/GnssPowerOptimizeStub;->recordEngineUsageDaily(J)V
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 458
    :try_start_5
    monitor-exit v2

    return-void

    .line 474
    .end local v3    # "timeBef":J
    .end local v5    # "timeAft":J
    .end local v7    # "typeCtrlAll":J
    .end local v9    # "typeCtrlPart":J
    .end local v11    # "typeCtrlNone":J
    :catch_0
    move-exception v0

    goto :goto_4

    .line 472
    :catch_1
    move-exception v0

    goto :goto_5

    .line 474
    :catch_2
    move-exception v0

    move-object/from16 v14, p1

    .line 475
    .local v0, "e":Landroid/os/RemoteException;
    :goto_4
    const-string v3, "GnssSavePoint"

    const-string/jumbo v4, "startUploadGnssEngineUsage track RemoteException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    .line 472
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_3
    move-exception v0

    move-object/from16 v14, p1

    .line 473
    .local v0, "e":Lorg/json/JSONException;
    :goto_5
    const-string v3, "GnssSavePoint"

    const-string/jumbo v4, "startUploadGnssEngineUsage JSONException!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 476
    nop

    .line 477
    .end local v0    # "e":Lorg/json/JSONException;
    :goto_6
    iget-object v0, v1, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mListEngineUsage:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 478
    monitor-exit v2

    .line 479
    return-void

    .line 478
    :catchall_0
    move-exception v0

    move-object/from16 v14, p1

    :goto_7
    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_7

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private startUploadGnssMockLocationOpt(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 734
    const-class v0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;

    monitor-enter v0

    .line 735
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 737
    .local v1, "jsonObject":Lorg/json/JSONObject;
    :try_start_1
    const-string v2, "EVENT_NAME"

    const-string v3, "GNSS_MOCK_LOCATION_OPT"

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 739
    const-string v2, "count"

    iget-object v3, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGnssMockLocationTimes:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 740
    const-string v2, "gmo1_during"

    iget-object v3, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGnssMockLocationInterval:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 742
    invoke-static {}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->getInstance()Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;

    move-result-object v2

    .line 743
    .local v2, "instance":Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;
    invoke-virtual {v2, p1}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->init(Landroid/content/Context;)V

    .line 744
    invoke-virtual {v2, v1}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->track(Lorg/json/JSONObject;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 749
    .end local v2    # "instance":Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;
    goto :goto_0

    .line 747
    :catch_0
    move-exception v2

    .line 748
    .local v2, "e":Landroid/os/RemoteException;
    :try_start_2
    const-string v3, "GnssSavePoint"

    const-string/jumbo v4, "startUploadGnssMockLocationOpt RemoteException!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 745
    .end local v2    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v2

    .line 746
    .local v2, "e":Lorg/json/JSONException;
    const-string v3, "GnssSavePoint"

    const-string/jumbo v4, "startUploadGnssMockLocationOpt JSONException!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 749
    nop

    .line 750
    .end local v2    # "e":Lorg/json/JSONException;
    :goto_0
    iget-object v2, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGnssMockLocationTimes:Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v3, 0x0

    invoke-virtual {v2, v3, v4}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 751
    iget-object v2, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGnssMockLocationInterval:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2, v3, v4}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 752
    .end local v1    # "jsonObject":Lorg/json/JSONObject;
    monitor-exit v0

    .line 753
    return-void

    .line 752
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method private startUploadGnssSatelliteCallOpt(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 163
    const-class v0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;

    monitor-enter v0

    .line 164
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 166
    .local v1, "jsonObject":Lorg/json/JSONObject;
    :try_start_1
    const-string v2, "EVENT_NAME"

    const-string v3, "GNSS_SATELLITE_CALL_OPT"

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 167
    const-string v2, "count"

    iget-object v3, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mSatelliteCallCnt:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 168
    const-string v2, "during"

    iget-wide v3, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mSatelliteCallDuring:J

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 169
    invoke-static {}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->getInstance()Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;

    move-result-object v2

    .line 170
    .local v2, "instance":Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;
    invoke-virtual {v2, p1}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->init(Landroid/content/Context;)V

    .line 171
    invoke-virtual {v2, v1}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->track(Lorg/json/JSONObject;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 176
    .end local v2    # "instance":Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;
    goto :goto_0

    .line 174
    :catch_0
    move-exception v2

    .line 175
    .local v2, "e":Landroid/os/RemoteException;
    :try_start_2
    const-string v3, "GnssSavePoint"

    const-string/jumbo v4, "startUploadGnssSatelliteCallOpt RemoteException!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 172
    .end local v2    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v2

    .line 173
    .local v2, "e":Lorg/json/JSONException;
    const-string v3, "GnssSavePoint"

    const-string/jumbo v4, "startUploadGnssSatelliteCallOpt JSONException!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    nop

    .line 177
    .end local v2    # "e":Lorg/json/JSONException;
    :goto_0
    iget-object v2, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mSatelliteCallCnt:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 178
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mSatelliteCallDuring:J

    .line 179
    .end local v1    # "jsonObject":Lorg/json/JSONObject;
    monitor-exit v0

    .line 180
    return-void

    .line 179
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method private startUploadUseApp(Landroid/content/Context;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;

    .line 533
    iget-boolean v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z

    if-eqz v0, :cond_0

    .line 534
    const-string v0, "GnssSavePoint"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "startUploadUseAppTimer schedule current thread---->"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 535
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 536
    .local v0, "dataList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 537
    .local v1, "map":Ljava/util/Map;, "Ljava/util/Map<Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;Ljava/lang/Integer;>;"
    const-class v2, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;

    monitor-enter v2

    .line 538
    :try_start_0
    iget-object v3, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mListUseApp:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;

    .line 539
    .local v4, "bean":Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;
    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v1, v4, v5}, Ljava/util/Map;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v1, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 540
    nop

    .end local v4    # "bean":Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;
    goto :goto_0

    .line 542
    :cond_1
    :try_start_1
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 543
    .local v4, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;Ljava/lang/Integer;>;"
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;

    iget-object v5, v5, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->packageName:Ljava/lang/String;

    .line 544
    .local v5, "pkg":Ljava/lang/String;
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V

    .line 545
    .local v6, "jsonObject":Lorg/json/JSONObject;
    const-string v7, "EVENT_NAME"

    const-string v8, "GPS_USE_APP"

    invoke-virtual {v6, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 546
    const-string v7, "packageName"

    invoke-virtual {v6, v7, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 547
    const-string v7, "report_interval"

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;

    iget-object v8, v8, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->reportInterval:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 548
    const-string v7, "glp_during"

    iget-object v8, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGlpDuringMap:Ljava/util/Map;

    invoke-interface {v8, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 549
    const-string v7, "glp_during_background"

    iget-object v8, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGlpDuringBackground:Ljava/util/Map;

    invoke-interface {v8, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 550
    const-string v7, "count"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 551
    invoke-virtual {v6}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 552
    nop

    .end local v4    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;Ljava/lang/Integer;>;"
    .end local v5    # "pkg":Ljava/lang/String;
    .end local v6    # "jsonObject":Lorg/json/JSONObject;
    goto :goto_1

    .line 554
    :cond_2
    invoke-static {}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->getInstance()Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;

    move-result-object v3

    .line 555
    .local v3, "instance":Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;
    invoke-virtual {v3, p1}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->init(Landroid/content/Context;)V

    .line 556
    invoke-virtual {v3, v0}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->track(Ljava/util/List;)V

    .line 557
    iget-boolean v4, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z

    if-eqz v4, :cond_3

    const-string v4, "GnssSavePoint"

    const-string/jumbo v5, "startUploadUseApp track success"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 562
    .end local v3    # "instance":Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;
    :cond_3
    goto :goto_2

    .line 560
    :catch_0
    move-exception v3

    .line 561
    .local v3, "e":Landroid/os/RemoteException;
    :try_start_2
    const-string v4, "GnssSavePoint"

    const-string/jumbo v5, "startUploadUseApp track RemoteException"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 558
    .end local v3    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v3

    .line 559
    .local v3, "e":Lorg/json/JSONException;
    const-string v4, "GnssSavePoint"

    const-string v5, "recordUseGnssApp JSONException!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 562
    nop

    .line 563
    .end local v3    # "e":Lorg/json/JSONException;
    :goto_2
    iget-object v3, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mListUseApp:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 564
    iget-object v3, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGlpDuringMap:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    .line 565
    iget-object v3, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGlpDuringBackground:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    .line 566
    monitor-exit v2

    .line 567
    return-void

    .line 566
    :catchall_0
    move-exception v3

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3
.end method


# virtual methods
.method public init(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .line 122
    iput-object p1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGlpContext:Landroid/content/Context;

    .line 123
    return-void
.end method

.method public recordChangeToBackground(Ljava/lang/String;ILjava/lang/String;ZZ)V
    .locals 7
    .param p1, "provider"    # Ljava/lang/String;
    .param p2, "callingIdentityHashCode"    # I
    .param p3, "pkgName"    # Ljava/lang/String;
    .param p4, "foreground"    # Z
    .param p5, "hasLocationPermissions"    # Z

    .line 225
    if-eqz p5, :cond_5

    const-string v0, "gps"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 226
    iget-object v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mRequestMap:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;

    .line 227
    .local v0, "bean":Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;
    if-eqz v0, :cond_4

    iget-wide v1, v0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->requestTime:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    goto/16 :goto_2

    .line 228
    :cond_0
    if-eqz p4, :cond_1

    .line 229
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->changeToforeTime:J

    .line 230
    iget-wide v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGlpBackTime:J

    iget-wide v3, v0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->changeToforeTime:J

    iget-wide v5, v0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->changeToBackTime:J

    sub-long/2addr v3, v5

    add-long/2addr v1, v3

    iput-wide v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGlpBackTime:J

    goto :goto_0

    .line 232
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->changeToBackTime:J

    .line 233
    iget-wide v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGlpForeTime:J

    iget-wide v3, v0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->changeToBackTime:J

    iget-wide v5, v0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->changeToforeTime:J

    sub-long/2addr v3, v5

    add-long/2addr v1, v3

    iput-wide v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGlpForeTime:J

    .line 235
    :goto_0
    iget-boolean v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z

    if-eqz v1, :cond_3

    .line 236
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " recordChangeToBackground callingIdentity:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "pkgName:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 237
    if-eqz p4, :cond_2

    const-string v2, " foreground"

    goto :goto_1

    :cond_2
    const-string v2, " background"

    :goto_1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " GlpForeTime:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGlpForeTime:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " GlpBackTime:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGlpBackTime:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 236
    const-string v2, "GnssSavePoint"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    :cond_3
    iget-object v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mRequestMap:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 227
    :cond_4
    :goto_2
    return-void

    .line 241
    .end local v0    # "bean":Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;
    :cond_5
    :goto_3
    return-void
.end method

.method public recordEngineUsage(IJ)V
    .locals 9
    .param p1, "type"    # I
    .param p2, "milliseconds"    # J

    .line 289
    const-class v0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;

    monitor-enter v0

    .line 290
    :try_start_0
    iget v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mLastEngineStatus:I

    if-ne v1, p1, :cond_0

    monitor-exit v0

    return-void

    .line 291
    :cond_0
    iput p1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mLastEngineStatus:I

    .line 292
    iget-boolean v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z

    if-eqz v1, :cond_1

    const-string v1, "GnssSavePoint"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "recordEngineUsage, type="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", currentTime="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    :cond_1
    packed-switch p1, :pswitch_data_0

    .line 316
    invoke-direct {p0}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->resetGpo3Var()V

    goto :goto_0

    .line 313
    :pswitch_0
    invoke-direct {p0, p2, p3}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->calCurEngineUsage(J)V

    .line 314
    goto :goto_0

    .line 310
    :pswitch_1
    iput-wide p2, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineControlTime:J

    .line 311
    goto :goto_0

    .line 300
    :pswitch_2
    iget-wide v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineStartTime:J

    const-wide/16 v3, 0x0

    cmp-long v5, v1, v3

    if-eqz v5, :cond_2

    iget-wide v5, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineControlTime:J

    cmp-long v7, v5, v3

    if-eqz v7, :cond_2

    .line 301
    iget-wide v7, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineTimeAftGpo3:J

    sub-long/2addr v5, v1

    add-long/2addr v7, v5

    iput-wide v7, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineTimeAftGpo3:J

    .line 302
    iput-wide v3, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineControlTime:J

    .line 304
    :cond_2
    iput-wide p2, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineStartTime:J

    .line 305
    iget-wide v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineBlockTime:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_3

    .line 306
    iput-wide p2, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineBlockTime:J

    goto :goto_0

    .line 297
    :pswitch_3
    iput-wide p2, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mEngineBlockTime:J

    .line 298
    nop

    .line 318
    :cond_3
    :goto_0
    iget-boolean v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->hasStartUploadData:Z

    if-nez v1, :cond_4

    .line 319
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->hasStartUploadData:Z

    .line 320
    iget-object v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGlpContext:Landroid/content/Context;

    const-string v2, "action upload data"

    const/4 v3, 0x3

    invoke-direct {p0, v1, v2, v3}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->setAlarm(Landroid/content/Context;Ljava/lang/String;I)V

    .line 322
    :cond_4
    monitor-exit v0

    .line 323
    return-void

    .line 322
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public recordGnssBackgroundOpt2Time()V
    .locals 3

    .line 374
    iget-object v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mBackgroundOpt2Cnt:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    .line 375
    .local v0, "cnt":I
    iget-boolean v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "recordGnssBackgroundOptTime:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GnssSavePoint"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 376
    :cond_0
    return-void
.end method

.method public recordGnssBackgroundOpt3Time()V
    .locals 3

    .line 380
    iget-object v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mBackgroundOpt3Cnt:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    .line 381
    .local v0, "cnt":I
    iget-boolean v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "recordGnssBackgroundOptTime:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GnssSavePoint"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 382
    :cond_0
    return-void
.end method

.method public recordGnssSatelliteCallOptCnt()V
    .locals 4

    .line 148
    iget-object v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mSatelliteCallCnt:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    .line 149
    .local v0, "cnt":I
    iget-boolean v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "recordGnssSatelliteCallOptCnt:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GnssSavePoint"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    :cond_0
    iget-boolean v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->hasStartUploadData:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGlpContext:Landroid/content/Context;

    if-eqz v1, :cond_1

    .line 151
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->hasStartUploadData:Z

    .line 152
    const-string v2, "action upload data"

    const/4 v3, 0x4

    invoke-direct {p0, v1, v2, v3}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->setAlarm(Landroid/content/Context;Ljava/lang/String;I)V

    .line 154
    :cond_1
    return-void
.end method

.method public recordGnssSatelliteCallOptDuring(J)V
    .locals 3
    .param p1, "during"    # J

    .line 158
    iget-wide v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mSatelliteCallDuring:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mSatelliteCallDuring:J

    .line 159
    iget-boolean v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "recordGnssSatelliteCallOptDuring:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mSatelliteCallDuring:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GnssSavePoint"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    :cond_0
    return-void
.end method

.method public recordNavAppTime(Ljava/lang/String;J)V
    .locals 3
    .param p1, "pkn"    # Ljava/lang/String;
    .param p2, "time"    # J

    .line 327
    if-eqz p1, :cond_1

    const-wide/16 v0, 0x0

    cmp-long v2, p2, v0

    if-gtz v2, :cond_0

    goto :goto_0

    .line 328
    :cond_0
    iget-object v2, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mNavAppTimeMap:Ljava/util/Map;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    add-long/2addr v0, p2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 329
    return-void

    .line 327
    :cond_1
    :goto_0
    return-void
.end method

.method public recordRemove(Ljava/lang/String;IZZ)V
    .locals 16
    .param p1, "provider"    # Ljava/lang/String;
    .param p2, "callingIdentityHashCode"    # I
    .param p3, "foreground"    # Z
    .param p4, "hasLocationPermissions"    # Z

    .line 245
    move-object/from16 v8, p0

    const-string v0, "gps"

    move-object/from16 v9, p1

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 246
    iget-object v0, v8, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mRequestMap:Ljava/util/Map;

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;

    .line 247
    .local v10, "bean":Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;
    invoke-direct {v8, v10}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->recordAppRemove(Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;)V

    .line 248
    const-wide/16 v11, 0x0

    if-eqz v10, :cond_3

    iget-wide v0, v10, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->requestTime:J

    cmp-long v0, v0, v11

    if-eqz v0, :cond_3

    .line 249
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, v10, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->requestTime:J

    sub-long v13, v0, v2

    .line 250
    .local v13, "glpDuring":J
    const-string v15, "GnssSavePoint"

    if-eqz p4, :cond_1

    .line 251
    iget-wide v0, v10, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->changeToforeTime:J

    iget-wide v2, v10, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->changeToBackTime:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 252
    iget-wide v0, v8, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGlpForeTime:J

    sub-long v0, v13, v0

    iput-wide v0, v8, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGlpBackTime:J

    .line 253
    iget-boolean v0, v8, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z

    if-eqz v0, :cond_2

    .line 254
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "remove on the background and true BackTime is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, v8, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGlpBackTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v15, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 255
    :cond_0
    iget-wide v0, v10, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->changeToforeTime:J

    iget-wide v2, v10, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->changeToBackTime:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    .line 256
    iput-wide v13, v8, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGlpBackTime:J

    goto :goto_0

    .line 259
    :cond_1
    const-string v0, "do not have Location Permissions, do not need record background time..."

    invoke-static {v15, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    :cond_2
    :goto_0
    iget-object v1, v8, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGlpContext:Landroid/content/Context;

    iget-object v2, v10, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->packageName:Ljava/lang/String;

    iget-object v3, v10, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->reportInterval:Ljava/lang/String;

    iget-wide v6, v8, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGlpBackTime:J

    move-object/from16 v0, p0

    move-wide v4, v13

    invoke-direct/range {v0 .. v7}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->recordCallerGnssApp(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JJ)V

    .line 262
    iget-boolean v0, v8, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z

    if-eqz v0, :cond_3

    .line 263
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "packageName:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v10, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " glpDuring:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " GlpBackTime:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, v8, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGlpBackTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v15, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    .end local v13    # "glpDuring":J
    :cond_3
    iget-object v0, v8, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mRequestMap:Ljava/util/Map;

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 266
    iput-wide v11, v8, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGlpBackTime:J

    .line 267
    iput-wide v11, v8, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGlpForeTime:J

    .line 269
    .end local v10    # "bean":Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;
    :cond_4
    return-void
.end method

.method public recordRequest(Ljava/lang/String;ILjava/lang/String;J)V
    .locals 3
    .param p1, "provider"    # Ljava/lang/String;
    .param p2, "callingIdentityHashCode"    # I
    .param p3, "pkgName"    # Ljava/lang/String;
    .param p4, "intervalMs"    # J

    .line 209
    const-string v0, "gps"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210
    new-instance v0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;

    invoke-direct {v0}, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;-><init>()V

    .line 211
    .local v0, "bean":Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->requestTime:J

    .line 212
    iput-object p3, v0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->packageName:Ljava/lang/String;

    .line 213
    invoke-static {p4, p5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->reportInterval:Ljava/lang/String;

    .line 214
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->changeToBackTime:J

    .line 215
    iget-wide v1, v0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->changeToBackTime:J

    iput-wide v1, v0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->changeToforeTime:J

    .line 216
    iget-object v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mRequestMap:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 217
    invoke-direct {p0, p3}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->putIntoAppRequestCtlMap(Ljava/lang/String;)V

    .line 218
    iget-boolean v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z

    if-eqz v1, :cond_0

    .line 219
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " recordRequest callingIdentity:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GnssSavePoint"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    .end local v0    # "bean":Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;
    :cond_0
    return-void
.end method

.method public recordSatelliteBlockListChanged(JJLjava/lang/String;)V
    .locals 8
    .param p1, "mTotalNaviTime"    # J
    .param p3, "mEffectiveTime"    # J
    .param p5, "mPkn"    # Ljava/lang/String;

    .line 364
    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-lez v2, :cond_0

    cmp-long v0, p3, v0

    if-lez v0, :cond_0

    .line 365
    new-instance v0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;

    move-object v1, v0

    move-object v2, p0

    move-wide v3, p1

    move-wide v5, p3

    move-object v7, p5

    invoke-direct/range {v1 .. v7}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;-><init>(Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;JJLjava/lang/String;)V

    .line 366
    .local v0, "mBlocklistControlBean":Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;
    iget-object v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mBlocklistControlBeanList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 367
    invoke-static {}, Lcom/android/server/location/gnss/GnssLocationProviderStub;->getInstance()Lcom/android/server/location/gnss/GnssLocationProviderStub;

    move-result-object v1

    invoke-virtual {v0}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/android/server/location/gnss/GnssLocationProviderStub;->writeLocationInformation(Ljava/lang/String;)V

    .line 368
    iget-boolean v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z

    if-eqz v1, :cond_0

    const-string v1, "TAG"

    invoke-virtual {v0}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 370
    .end local v0    # "mBlocklistControlBean":Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;
    :cond_0
    return-void
.end method

.method public usingBatInMockModeInterval(J)V
    .locals 4
    .param p1, "interval"    # J

    .line 729
    iget-object v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGnssMockLocationInterval:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    move-result-wide v0

    .line 730
    .local v0, "cnt":J
    iget-boolean v2, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "usingBatInMockModeInterval: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "GnssSavePoint"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 731
    :cond_0
    return-void
.end method

.method public usingBatInMockModeTimes(J)V
    .locals 4
    .param p1, "times"    # J

    .line 723
    iget-object v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->mGnssMockLocationTimes:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    move-result-wide v0

    .line 724
    .local v0, "cnt":J
    iget-boolean v2, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;->D:Z

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "usingBatInMockModeTimes: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "GnssSavePoint"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 725
    :cond_0
    return-void
.end method
