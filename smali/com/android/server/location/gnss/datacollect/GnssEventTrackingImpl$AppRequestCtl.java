class com.android.server.location.gnss.datacollect.GnssEventTrackingImpl$AppRequestCtl {
	 /* .source "GnssEventTrackingImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "AppRequestCtl" */
} // .end annotation
/* # instance fields */
private Integer aftCtl;
private Integer befCtl;
final com.android.server.location.gnss.datacollect.GnssEventTrackingImpl this$0; //synthetic
/* # direct methods */
private com.android.server.location.gnss.datacollect.GnssEventTrackingImpl$AppRequestCtl ( ) {
/* .locals 0 */
/* .line 653 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 654 */
int p1 = 1; // const/4 p1, 0x1
/* iput p1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl;->befCtl:I */
/* .line 655 */
/* iput p1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl;->aftCtl:I */
/* .line 656 */
return;
} // .end method
 com.android.server.location.gnss.datacollect.GnssEventTrackingImpl$AppRequestCtl ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl;-><init>(Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;)V */
return;
} // .end method
/* # virtual methods */
public void addRequestCnt ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "befDeltaAdd" # I */
/* .param p2, "aftDeltaAdd" # I */
/* .line 659 */
/* iget v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl;->befCtl:I */
/* add-int/2addr v0, p1 */
/* iput v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl;->befCtl:I */
/* .line 660 */
/* iget v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl;->aftCtl:I */
/* add-int/2addr v0, p2 */
/* iput v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl;->aftCtl:I */
/* .line 661 */
return;
} // .end method
public Integer getAftCtlCnt ( ) {
/* .locals 1 */
/* .line 668 */
/* iget v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl;->aftCtl:I */
} // .end method
public Integer getBefCtlCnt ( ) {
/* .locals 1 */
/* .line 664 */
/* iget v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$AppRequestCtl;->befCtl:I */
} // .end method
