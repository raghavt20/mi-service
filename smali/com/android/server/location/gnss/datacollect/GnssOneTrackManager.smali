.class Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;
.super Ljava/lang/Object;
.source "GnssOneTrackManager.java"


# static fields
.field private static final APP_ID:Ljava/lang/String; = "2882303761518758754"

.field private static final CALL_INIT_FIRST:Ljava/lang/String; = "please check whether call the init method first!"

.field private static final CAN_NOT_RUN_IN_MAINTHREAD:Ljava/lang/String; = "Can not run in main thread!"

.field private static final FLAG_NON_ANONYMOUS:I = 0x2

.field private static final FLAG_NOT_LIMITED_BY_USER_EXPERIENCE_PLAN:I = 0x1

.field private static final ONETRACK_TRACK_EXCEPTION:Ljava/lang/String; = "OneTrack Track Exception"

.field private static final ONETRACK_TRACK_SUCCESS:Ljava/lang/String; = "OneTrack Track Success"

.field private static final PKG_NAME:Ljava/lang/String; = "com.miui.analytics"

.field private static final SERVER_CLASS_NAME:Ljava/lang/String; = "com.miui.analytics.onetrack.TrackService"

.field private static final TAG:Ljava/lang/String; = "GnssOneTrackManager"

.field private static volatile mGnssOneTrackManager:Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;


# instance fields
.field private final D:Z

.field private final conn:Landroid/content/ServiceConnection;

.field private final intent:Landroid/content/Intent;

.field private isServiceBind:Z

.field private mBindContext:Landroid/content/Context;

.field private mITrackBinder:Lcom/miui/analytics/ITrackBinder;


# direct methods
.method static bridge synthetic -$$Nest$fgetconn(Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;)Landroid/content/ServiceConnection;
    .locals 0

    iget-object p0, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->conn:Landroid/content/ServiceConnection;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmBindContext(Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->mBindContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputisServiceBind(Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->isServiceBind:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmITrackBinder(Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;Lcom/miui/analytics/ITrackBinder;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->mITrackBinder:Lcom/miui/analytics/ITrackBinder;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const-string v0, "persist.sys.gnss_dc.test"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->D:Z

    .line 62
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.miui.analytics"

    const-string v2, "com.miui.analytics.onetrack.TrackService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->intent:Landroid/content/Intent;

    .line 63
    new-instance v0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager$1;

    invoke-direct {v0, p0}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager$1;-><init>(Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;)V

    iput-object v0, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->conn:Landroid/content/ServiceConnection;

    .line 49
    return-void
.end method

.method public static getInstance()Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;
    .locals 2

    .line 52
    sget-object v0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->mGnssOneTrackManager:Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;

    if-nez v0, :cond_1

    .line 53
    const-class v0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;

    monitor-enter v0

    .line 54
    :try_start_0
    sget-object v1, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->mGnssOneTrackManager:Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;

    if-nez v1, :cond_0

    .line 55
    new-instance v1, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;

    invoke-direct {v1}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;-><init>()V

    sput-object v1, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->mGnssOneTrackManager:Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;

    .line 57
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 59
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->mGnssOneTrackManager:Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;

    return-object v0
.end method


# virtual methods
.method public getGnssOneTrackBinder()Landroid/os/IBinder;
    .locals 3

    .line 226
    const-string v0, "GnssOneTrackManager"

    const-string v1, "getOneTrackService() on called"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    const-class v0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;

    monitor-enter v0

    .line 228
    :try_start_0
    iget-object v1, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->mITrackBinder:Lcom/miui/analytics/ITrackBinder;

    if-eqz v1, :cond_0

    .line 229
    const-string v1, "GnssOneTrackManager"

    const-string v2, "Return the IBinder"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    iget-object v1, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->mITrackBinder:Lcom/miui/analytics/ITrackBinder;

    invoke-interface {v1}, Lcom/miui/analytics/ITrackBinder;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    monitor-exit v0

    return-object v1

    .line 232
    :cond_0
    const-string v1, "GnssOneTrackManager"

    const-string v2, "getOneTrackService() failed: no binder!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    monitor-exit v0

    const/4 v0, 0x0

    return-object v0

    .line 235
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public init(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 86
    if-eqz p1, :cond_1

    .line 89
    const-class v0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;

    monitor-enter v0

    .line 90
    :try_start_0
    iget-object v1, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->mBindContext:Landroid/content/Context;

    if-eq p1, v1, :cond_0

    iget-boolean v2, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->isServiceBind:Z

    if-eqz v2, :cond_0

    .line 91
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->mITrackBinder:Lcom/miui/analytics/ITrackBinder;

    .line 92
    iget-object v2, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->conn:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 94
    :cond_0
    iput-object p1, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->mBindContext:Landroid/content/Context;

    .line 95
    monitor-exit v0

    .line 96
    return-void

    .line 95
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 87
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Init Context == null"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public track(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "eventName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 148
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-eq v0, v1, :cond_6

    .line 151
    const-class v0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;

    monitor-enter v0

    .line 152
    :try_start_0
    iget-object v1, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->mBindContext:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 153
    const-string v1, "GnssOneTrackManager"

    const-string v2, "please check whether call the init method first!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    monitor-exit v0

    return-void

    .line 156
    :cond_0
    iget-object v1, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->mITrackBinder:Lcom/miui/analytics/ITrackBinder;

    if-eqz v1, :cond_2

    .line 157
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 159
    .local v1, "jsonData":Lorg/json/JSONObject;
    :try_start_1
    const-string v2, "EVENT_NAME"

    invoke-virtual {v1, v2, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 160
    invoke-virtual {v1, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 163
    goto :goto_0

    .line 161
    :catch_0
    move-exception v2

    .line 162
    .local v2, "e":Lorg/json/JSONException;
    :try_start_2
    const-string v3, "GnssOneTrackManager"

    const-string v4, "OneTrack Track Exception:track JSONException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    .end local v2    # "e":Lorg/json/JSONException;
    :goto_0
    iget-boolean v2, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->D:Z

    if-eqz v2, :cond_1

    const-string v2, "GnssOneTrackManager"

    const-string v3, "kv call trackEvent"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    :cond_1
    iget-object v2, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->mITrackBinder:Lcom/miui/analytics/ITrackBinder;

    const-string v3, "2882303761518758754"

    const-string v4, "com.miui.analytics"

    .line 166
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    .line 165
    const/4 v6, 0x3

    invoke-interface {v2, v3, v4, v5, v6}, Lcom/miui/analytics/ITrackBinder;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 167
    const-string v2, "GnssOneTrackManager"

    const-string v3, "OneTrack Track Success"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    monitor-exit v0

    return-void

    .line 170
    .end local v1    # "jsonData":Lorg/json/JSONObject;
    :cond_2
    iget-boolean v1, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->D:Z

    if-eqz v1, :cond_3

    const-string v1, "GnssOneTrackManager"

    const-string v2, "kv No TrackBinder, begin to bindService"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    :cond_3
    iget-object v1, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->mBindContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->intent:Landroid/content/Intent;

    iget-object v3, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->conn:Landroid/content/ServiceConnection;

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 173
    .local v1, "bindResult":Z
    :try_start_3
    const-class v2, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;

    const-wide/16 v3, 0x3e8

    invoke-virtual {v2, v3, v4}, Ljava/lang/Object;->wait(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 177
    goto :goto_1

    .line 174
    :catch_1
    move-exception v2

    .line 175
    .local v2, "e":Ljava/lang/InterruptedException;
    :try_start_4
    const-string v3, "GnssOneTrackManager"

    const-string v4, "OneTrack Track Exception"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 178
    .end local v2    # "e":Ljava/lang/InterruptedException;
    :goto_1
    if-eqz v1, :cond_5

    iget-object v2, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->mITrackBinder:Lcom/miui/analytics/ITrackBinder;

    if-eqz v2, :cond_5

    .line 179
    iget-boolean v2, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->D:Z

    if-eqz v2, :cond_4

    const-string v2, "GnssOneTrackManager"

    const-string v3, "bind success kv call trackEvent"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    :cond_4
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->track(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 182
    :cond_5
    const-string v2, "GnssOneTrackManager"

    const-string v3, "OneTrack Track Exception"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    .end local v1    # "bindResult":Z
    :goto_2
    monitor-exit v0

    .line 185
    return-void

    .line 184
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v1

    .line 149
    :cond_6
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Can not run in main thread!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public track(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 193
    .local p1, "dataList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-eq v0, v1, :cond_6

    .line 196
    const-class v0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;

    monitor-enter v0

    .line 197
    :try_start_0
    iget-object v1, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->mBindContext:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 198
    const-string v1, "GnssOneTrackManager"

    const-string v2, "please check whether call the init method first!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    monitor-exit v0

    return-void

    .line 201
    :cond_0
    iget-object v1, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->mITrackBinder:Lcom/miui/analytics/ITrackBinder;

    if-eqz v1, :cond_2

    .line 202
    iget-boolean v1, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->D:Z

    if-eqz v1, :cond_1

    const-string v1, "GnssOneTrackManager"

    const-string v2, "data list call trackEvent"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    :cond_1
    iget-object v1, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->mITrackBinder:Lcom/miui/analytics/ITrackBinder;

    const-string v2, "2882303761518758754"

    const-string v3, "com.miui.analytics"

    const/4 v4, 0x3

    invoke-interface {v1, v2, v3, p1, v4}, Lcom/miui/analytics/ITrackBinder;->trackEvents(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;I)V

    .line 205
    const-string v1, "GnssOneTrackManager"

    const-string v2, "OneTrack Track Success"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    monitor-exit v0

    return-void

    .line 208
    :cond_2
    iget-boolean v1, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->D:Z

    if-eqz v1, :cond_3

    const-string v1, "GnssOneTrackManager"

    const-string v2, "data list No TrackBinder, begin to bindService"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    :cond_3
    iget-object v1, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->mBindContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->intent:Landroid/content/Intent;

    iget-object v3, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->conn:Landroid/content/ServiceConnection;

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 211
    .local v1, "bindResult":Z
    :try_start_1
    const-class v2, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;

    const-wide/16 v3, 0x3e8

    invoke-virtual {v2, v3, v4}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 215
    goto :goto_0

    .line 212
    :catch_0
    move-exception v2

    .line 213
    .local v2, "e":Ljava/lang/InterruptedException;
    :try_start_2
    const-string v3, "GnssOneTrackManager"

    const-string v4, "OneTrack Track Exception"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 216
    .end local v2    # "e":Ljava/lang/InterruptedException;
    :goto_0
    if-eqz v1, :cond_5

    iget-object v2, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->mITrackBinder:Lcom/miui/analytics/ITrackBinder;

    if-eqz v2, :cond_5

    .line 217
    iget-boolean v2, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->D:Z

    if-eqz v2, :cond_4

    const-string v2, "GnssOneTrackManager"

    const-string v3, "bind success data list call trackEvent"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    :cond_4
    invoke-virtual {p0, p1}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->track(Ljava/util/List;)V

    goto :goto_1

    .line 220
    :cond_5
    const-string v2, "GnssOneTrackManager"

    const-string v3, "OneTrack Track Exception"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    .end local v1    # "bindResult":Z
    :goto_1
    monitor-exit v0

    .line 223
    return-void

    .line 222
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 194
    :cond_6
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Can not run in main thread!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public track(Lorg/json/JSONObject;)V
    .locals 6
    .param p1, "jsonData"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 104
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-eq v0, v1, :cond_8

    .line 107
    const-class v0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;

    monitor-enter v0

    .line 108
    :try_start_0
    iget-object v1, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->mBindContext:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 109
    const-string v1, "GnssOneTrackManager"

    const-string v2, "please check whether call the init method first!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    monitor-exit v0

    return-void

    .line 112
    :cond_0
    if-nez p1, :cond_2

    .line 113
    iget-boolean v1, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->D:Z

    if-eqz v1, :cond_1

    const-string v1, "GnssOneTrackManager"

    const-string v2, "jsonData == null"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    :cond_1
    monitor-exit v0

    return-void

    .line 116
    :cond_2
    iget-object v1, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->mITrackBinder:Lcom/miui/analytics/ITrackBinder;

    if-eqz v1, :cond_4

    .line 117
    iget-boolean v1, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->D:Z

    if-eqz v1, :cond_3

    const-string v1, "GnssOneTrackManager"

    const-string v2, "json data call trackEvent"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    :cond_3
    iget-object v1, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->mITrackBinder:Lcom/miui/analytics/ITrackBinder;

    const-string v2, "2882303761518758754"

    const-string v3, "com.miui.analytics"

    .line 119
    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    .line 118
    const/4 v5, 0x3

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/miui/analytics/ITrackBinder;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 120
    const-string v1, "GnssOneTrackManager"

    const-string v2, "OneTrack Track Success"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    monitor-exit v0

    return-void

    .line 123
    :cond_4
    iget-boolean v1, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->D:Z

    if-eqz v1, :cond_5

    const-string v1, "GnssOneTrackManager"

    const-string v2, "json data No TrackBinder, begin to bindService"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    :cond_5
    iget-object v1, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->mBindContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->intent:Landroid/content/Intent;

    iget-object v3, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->conn:Landroid/content/ServiceConnection;

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 126
    .local v1, "bindResult":Z
    :try_start_1
    const-class v2, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;

    const-wide/16 v3, 0x3e8

    invoke-virtual {v2, v3, v4}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 130
    goto :goto_0

    .line 127
    :catch_0
    move-exception v2

    .line 128
    .local v2, "e":Ljava/lang/InterruptedException;
    :try_start_2
    const-string v3, "GnssOneTrackManager"

    const-string v4, "OneTrack Track Exception"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 131
    .end local v2    # "e":Ljava/lang/InterruptedException;
    :goto_0
    if-eqz v1, :cond_7

    iget-object v2, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->mITrackBinder:Lcom/miui/analytics/ITrackBinder;

    if-eqz v2, :cond_7

    .line 132
    iget-boolean v2, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->D:Z

    if-eqz v2, :cond_6

    const-string v2, "GnssOneTrackManager"

    const-string v3, "bind success and json data call trackEvent"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    :cond_6
    invoke-virtual {p0, p1}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->track(Lorg/json/JSONObject;)V

    goto :goto_1

    .line 135
    :cond_7
    const-string v2, "GnssOneTrackManager"

    const-string v3, "OneTrack Track Exception"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    .end local v1    # "bindResult":Z
    :goto_1
    monitor-exit v0

    .line 138
    return-void

    .line 137
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 105
    :cond_8
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Can not run in main thread!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
