class com.android.server.location.gnss.datacollect.GnssEventTrackingImpl$BlocklistControlBean {
	 /* .source "GnssEventTrackingImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "BlocklistControlBean" */
} // .end annotation
/* # instance fields */
private Long blockedTime;
private java.lang.String pkg;
final com.android.server.location.gnss.datacollect.GnssEventTrackingImpl this$0; //synthetic
private Long totalNaviTime;
/* # direct methods */
public com.android.server.location.gnss.datacollect.GnssEventTrackingImpl$BlocklistControlBean ( ) {
/* .locals 0 */
/* .param p2, "totalNaviTime" # J */
/* .param p4, "blockedTime" # J */
/* .line 677 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 678 */
/* iput-wide p2, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;->totalNaviTime:J */
/* .line 679 */
/* iput-wide p4, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;->blockedTime:J */
/* .line 680 */
return;
} // .end method
public com.android.server.location.gnss.datacollect.GnssEventTrackingImpl$BlocklistControlBean ( ) {
/* .locals 0 */
/* .param p2, "totalNaviTime" # J */
/* .param p4, "blockedTime" # J */
/* .param p6, "pkg" # Ljava/lang/String; */
/* .line 682 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 683 */
/* iput-wide p2, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;->totalNaviTime:J */
/* .line 684 */
/* iput-wide p4, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;->blockedTime:J */
/* .line 685 */
this.pkg = p6;
/* .line 686 */
return;
} // .end method
/* # virtual methods */
public Long getBlockedTime ( ) {
/* .locals 2 */
/* .line 697 */
/* iget-wide v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;->blockedTime:J */
/* return-wide v0 */
} // .end method
public java.lang.String getPkg ( ) {
/* .locals 1 */
/* .line 705 */
v0 = this.pkg;
} // .end method
public Long getTotalNaviTime ( ) {
/* .locals 2 */
/* .line 689 */
/* iget-wide v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;->totalNaviTime:J */
/* return-wide v0 */
} // .end method
public void setBlockedTime ( Long p0 ) {
/* .locals 0 */
/* .param p1, "blockedTime" # J */
/* .line 701 */
/* iput-wide p1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;->blockedTime:J */
/* .line 702 */
return;
} // .end method
public void setPkg ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .line 709 */
this.pkg = p1;
/* .line 710 */
return;
} // .end method
public void setTotalNaviTime ( Long p0 ) {
/* .locals 0 */
/* .param p1, "totalNaviTime" # J */
/* .line 693 */
/* iput-wide p1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;->totalNaviTime:J */
/* .line 694 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 3 */
/* .line 714 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "recordGnssBlocklistUsage,totalNaviTime="; // const-string v1, "recordGnssBlocklistUsage,totalNaviTime="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;->totalNaviTime:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = ", blockedTime="; // const-string v1, ", blockedTime="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$BlocklistControlBean;->blockedTime:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = ", pkg="; // const-string v1, ", pkg="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.pkg;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
