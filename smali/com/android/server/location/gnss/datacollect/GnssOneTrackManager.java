class com.android.server.location.gnss.datacollect.GnssOneTrackManager {
	 /* .source "GnssOneTrackManager.java" */
	 /* # static fields */
	 private static final java.lang.String APP_ID;
	 private static final java.lang.String CALL_INIT_FIRST;
	 private static final java.lang.String CAN_NOT_RUN_IN_MAINTHREAD;
	 private static final Integer FLAG_NON_ANONYMOUS;
	 private static final Integer FLAG_NOT_LIMITED_BY_USER_EXPERIENCE_PLAN;
	 private static final java.lang.String ONETRACK_TRACK_EXCEPTION;
	 private static final java.lang.String ONETRACK_TRACK_SUCCESS;
	 private static final java.lang.String PKG_NAME;
	 private static final java.lang.String SERVER_CLASS_NAME;
	 private static final java.lang.String TAG;
	 private static volatile com.android.server.location.gnss.datacollect.GnssOneTrackManager mGnssOneTrackManager;
	 /* # instance fields */
	 private final Boolean D;
	 private final android.content.ServiceConnection conn;
	 private final android.content.Intent intent;
	 private Boolean isServiceBind;
	 private android.content.Context mBindContext;
	 private com.miui.analytics.ITrackBinder mITrackBinder;
	 /* # direct methods */
	 static android.content.ServiceConnection -$$Nest$fgetconn ( com.android.server.location.gnss.datacollect.GnssOneTrackManager p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 p0 = this.conn;
	 } // .end method
	 static android.content.Context -$$Nest$fgetmBindContext ( com.android.server.location.gnss.datacollect.GnssOneTrackManager p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 p0 = this.mBindContext;
	 } // .end method
	 static void -$$Nest$fputisServiceBind ( com.android.server.location.gnss.datacollect.GnssOneTrackManager p0, Boolean p1 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* iput-boolean p1, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->isServiceBind:Z */
		 return;
	 } // .end method
	 static void -$$Nest$fputmITrackBinder ( com.android.server.location.gnss.datacollect.GnssOneTrackManager p0, com.miui.analytics.ITrackBinder p1 ) { //bridge//synthethic
		 /* .locals 0 */
		 this.mITrackBinder = p1;
		 return;
	 } // .end method
	 private com.android.server.location.gnss.datacollect.GnssOneTrackManager ( ) {
		 /* .locals 3 */
		 /* .line 48 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 41 */
		 final String v0 = "persist.sys.gnss_dc.test"; // const-string v0, "persist.sys.gnss_dc.test"
		 int v1 = 0; // const/4 v1, 0x0
		 v0 = 		 android.os.SystemProperties .getBoolean ( v0,v1 );
		 /* iput-boolean v0, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->D:Z */
		 /* .line 62 */
		 /* new-instance v0, Landroid/content/Intent; */
		 /* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
		 final String v1 = "com.miui.analytics"; // const-string v1, "com.miui.analytics"
		 final String v2 = "com.miui.analytics.onetrack.TrackService"; // const-string v2, "com.miui.analytics.onetrack.TrackService"
		 (( android.content.Intent ) v0 ).setClassName ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
		 this.intent = v0;
		 /* .line 63 */
		 /* new-instance v0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager$1; */
		 /* invoke-direct {v0, p0}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager$1;-><init>(Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;)V */
		 this.conn = v0;
		 /* .line 49 */
		 return;
	 } // .end method
	 public static com.android.server.location.gnss.datacollect.GnssOneTrackManager getInstance ( ) {
		 /* .locals 2 */
		 /* .line 52 */
		 v0 = com.android.server.location.gnss.datacollect.GnssOneTrackManager.mGnssOneTrackManager;
		 /* if-nez v0, :cond_1 */
		 /* .line 53 */
		 /* const-class v0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager; */
		 /* monitor-enter v0 */
		 /* .line 54 */
		 try { // :try_start_0
			 v1 = com.android.server.location.gnss.datacollect.GnssOneTrackManager.mGnssOneTrackManager;
			 /* if-nez v1, :cond_0 */
			 /* .line 55 */
			 /* new-instance v1, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager; */
			 /* invoke-direct {v1}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;-><init>()V */
			 /* .line 57 */
		 } // :cond_0
		 /* monitor-exit v0 */
		 /* :catchall_0 */
		 /* move-exception v1 */
		 /* monitor-exit v0 */
		 /* :try_end_0 */
		 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
		 /* throw v1 */
		 /* .line 59 */
	 } // :cond_1
} // :goto_0
v0 = com.android.server.location.gnss.datacollect.GnssOneTrackManager.mGnssOneTrackManager;
} // .end method
/* # virtual methods */
public android.os.IBinder getGnssOneTrackBinder ( ) {
/* .locals 3 */
/* .line 226 */
final String v0 = "GnssOneTrackManager"; // const-string v0, "GnssOneTrackManager"
final String v1 = "getOneTrackService() on called"; // const-string v1, "getOneTrackService() on called"
android.util.Log .d ( v0,v1 );
/* .line 227 */
/* const-class v0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager; */
/* monitor-enter v0 */
/* .line 228 */
try { // :try_start_0
	 v1 = this.mITrackBinder;
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 /* .line 229 */
		 final String v1 = "GnssOneTrackManager"; // const-string v1, "GnssOneTrackManager"
		 final String v2 = "Return the IBinder"; // const-string v2, "Return the IBinder"
		 android.util.Log .d ( v1,v2 );
		 /* .line 230 */
		 v1 = this.mITrackBinder;
		 /* monitor-exit v0 */
		 /* .line 232 */
	 } // :cond_0
	 final String v1 = "GnssOneTrackManager"; // const-string v1, "GnssOneTrackManager"
	 final String v2 = "getOneTrackService() failed: no binder!"; // const-string v2, "getOneTrackService() failed: no binder!"
	 android.util.Log .d ( v1,v2 );
	 /* .line 233 */
	 /* monitor-exit v0 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* .line 235 */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 /* monitor-exit v0 */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* throw v1 */
} // .end method
public void init ( android.content.Context p0 ) {
	 /* .locals 3 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .line 86 */
	 if ( p1 != null) { // if-eqz p1, :cond_1
		 /* .line 89 */
		 /* const-class v0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager; */
		 /* monitor-enter v0 */
		 /* .line 90 */
		 try { // :try_start_0
			 v1 = this.mBindContext;
			 /* if-eq p1, v1, :cond_0 */
			 /* iget-boolean v2, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->isServiceBind:Z */
			 if ( v2 != null) { // if-eqz v2, :cond_0
				 /* .line 91 */
				 int v2 = 0; // const/4 v2, 0x0
				 this.mITrackBinder = v2;
				 /* .line 92 */
				 v2 = this.conn;
				 (( android.content.Context ) v1 ).unbindService ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
				 /* .line 94 */
			 } // :cond_0
			 this.mBindContext = p1;
			 /* .line 95 */
			 /* monitor-exit v0 */
			 /* .line 96 */
			 return;
			 /* .line 95 */
			 /* :catchall_0 */
			 /* move-exception v1 */
			 /* monitor-exit v0 */
			 /* :try_end_0 */
			 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
			 /* throw v1 */
			 /* .line 87 */
		 } // :cond_1
		 /* new-instance v0, Ljava/lang/RuntimeException; */
		 final String v1 = "Init Context == null"; // const-string v1, "Init Context == null"
		 /* invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V */
		 /* throw v0 */
	 } // .end method
	 public void track ( java.lang.String p0, java.lang.String p1, java.lang.String p2 ) {
		 /* .locals 7 */
		 /* .param p1, "key" # Ljava/lang/String; */
		 /* .param p2, "value" # Ljava/lang/String; */
		 /* .param p3, "eventName" # Ljava/lang/String; */
		 /* .annotation system Ldalvik/annotation/Throws; */
		 /* value = { */
		 /* Landroid/os/RemoteException; */
		 /* } */
	 } // .end annotation
	 /* .line 148 */
	 android.os.Looper .getMainLooper ( );
	 (( android.os.Looper ) v0 ).getThread ( ); // invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;
	 java.lang.Thread .currentThread ( );
	 /* if-eq v0, v1, :cond_6 */
	 /* .line 151 */
	 /* const-class v0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager; */
	 /* monitor-enter v0 */
	 /* .line 152 */
	 try { // :try_start_0
		 v1 = this.mBindContext;
		 /* if-nez v1, :cond_0 */
		 /* .line 153 */
		 final String v1 = "GnssOneTrackManager"; // const-string v1, "GnssOneTrackManager"
		 final String v2 = "please check whether call the init method first!"; // const-string v2, "please check whether call the init method first!"
		 android.util.Log .e ( v1,v2 );
		 /* .line 154 */
		 /* monitor-exit v0 */
		 return;
		 /* .line 156 */
	 } // :cond_0
	 v1 = this.mITrackBinder;
	 if ( v1 != null) { // if-eqz v1, :cond_2
		 /* .line 157 */
		 /* new-instance v1, Lorg/json/JSONObject; */
		 /* invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V */
		 /* :try_end_0 */
		 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
		 /* .line 159 */
		 /* .local v1, "jsonData":Lorg/json/JSONObject; */
		 try { // :try_start_1
			 final String v2 = "EVENT_NAME"; // const-string v2, "EVENT_NAME"
			 (( org.json.JSONObject ) v1 ).put ( v2, p3 ); // invoke-virtual {v1, v2, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
			 /* .line 160 */
			 (( org.json.JSONObject ) v1 ).put ( p1, p2 ); // invoke-virtual {v1, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
			 /* :try_end_1 */
			 /* .catch Lorg/json/JSONException; {:try_start_1 ..:try_end_1} :catch_0 */
			 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
			 /* .line 163 */
			 /* .line 161 */
			 /* :catch_0 */
			 /* move-exception v2 */
			 /* .line 162 */
			 /* .local v2, "e":Lorg/json/JSONException; */
			 try { // :try_start_2
				 final String v3 = "GnssOneTrackManager"; // const-string v3, "GnssOneTrackManager"
				 final String v4 = "OneTrack Track Exception:track JSONException"; // const-string v4, "OneTrack Track Exception:track JSONException"
				 android.util.Log .e ( v3,v4 );
				 /* .line 164 */
			 } // .end local v2 # "e":Lorg/json/JSONException;
		 } // :goto_0
		 /* iget-boolean v2, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->D:Z */
		 if ( v2 != null) { // if-eqz v2, :cond_1
			 final String v2 = "GnssOneTrackManager"; // const-string v2, "GnssOneTrackManager"
			 final String v3 = "kv call trackEvent"; // const-string v3, "kv call trackEvent"
			 android.util.Log .d ( v2,v3 );
			 /* .line 165 */
		 } // :cond_1
		 v2 = this.mITrackBinder;
		 final String v3 = "2882303761518758754"; // const-string v3, "2882303761518758754"
		 final String v4 = "com.miui.analytics"; // const-string v4, "com.miui.analytics"
		 /* .line 166 */
		 (( org.json.JSONObject ) v1 ).toString ( ); // invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
		 /* .line 165 */
		 int v6 = 3; // const/4 v6, 0x3
		 /* .line 167 */
		 final String v2 = "GnssOneTrackManager"; // const-string v2, "GnssOneTrackManager"
		 final String v3 = "OneTrack Track Success"; // const-string v3, "OneTrack Track Success"
		 android.util.Log .i ( v2,v3 );
		 /* .line 168 */
		 /* monitor-exit v0 */
		 return;
		 /* .line 170 */
	 } // .end local v1 # "jsonData":Lorg/json/JSONObject;
} // :cond_2
/* iget-boolean v1, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->D:Z */
if ( v1 != null) { // if-eqz v1, :cond_3
	 final String v1 = "GnssOneTrackManager"; // const-string v1, "GnssOneTrackManager"
	 final String v2 = "kv No TrackBinder, begin to bindService"; // const-string v2, "kv No TrackBinder, begin to bindService"
	 android.util.Log .d ( v1,v2 );
	 /* .line 171 */
} // :cond_3
v1 = this.mBindContext;
v2 = this.intent;
v3 = this.conn;
int v4 = 1; // const/4 v4, 0x1
v1 = (( android.content.Context ) v1 ).bindService ( v2, v3, v4 ); // invoke-virtual {v1, v2, v3, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 173 */
/* .local v1, "bindResult":Z */
try { // :try_start_3
	 /* const-class v2, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager; */
	 /* const-wide/16 v3, 0x3e8 */
	 (( java.lang.Object ) v2 ).wait ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Ljava/lang/Object;->wait(J)V
	 /* :try_end_3 */
	 /* .catch Ljava/lang/InterruptedException; {:try_start_3 ..:try_end_3} :catch_1 */
	 /* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
	 /* .line 177 */
	 /* .line 174 */
	 /* :catch_1 */
	 /* move-exception v2 */
	 /* .line 175 */
	 /* .local v2, "e":Ljava/lang/InterruptedException; */
	 try { // :try_start_4
		 final String v3 = "GnssOneTrackManager"; // const-string v3, "GnssOneTrackManager"
		 final String v4 = "OneTrack Track Exception"; // const-string v4, "OneTrack Track Exception"
		 android.util.Log .e ( v3,v4 );
		 /* .line 176 */
		 (( java.lang.InterruptedException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V
		 /* .line 178 */
	 } // .end local v2 # "e":Ljava/lang/InterruptedException;
} // :goto_1
if ( v1 != null) { // if-eqz v1, :cond_5
	 v2 = this.mITrackBinder;
	 if ( v2 != null) { // if-eqz v2, :cond_5
		 /* .line 179 */
		 /* iget-boolean v2, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->D:Z */
		 if ( v2 != null) { // if-eqz v2, :cond_4
			 final String v2 = "GnssOneTrackManager"; // const-string v2, "GnssOneTrackManager"
			 final String v3 = "bind success kv call trackEvent"; // const-string v3, "bind success kv call trackEvent"
			 android.util.Log .d ( v2,v3 );
			 /* .line 180 */
		 } // :cond_4
		 (( com.android.server.location.gnss.datacollect.GnssOneTrackManager ) p0 ).track ( p1, p2, p3 ); // invoke-virtual {p0, p1, p2, p3}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->track(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
		 /* .line 182 */
	 } // :cond_5
	 final String v2 = "GnssOneTrackManager"; // const-string v2, "GnssOneTrackManager"
	 final String v3 = "OneTrack Track Exception"; // const-string v3, "OneTrack Track Exception"
	 android.util.Log .e ( v2,v3 );
	 /* .line 184 */
} // .end local v1 # "bindResult":Z
} // :goto_2
/* monitor-exit v0 */
/* .line 185 */
return;
/* .line 184 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* throw v1 */
/* .line 149 */
} // :cond_6
/* new-instance v0, Ljava/lang/RuntimeException; */
final String v1 = "Can not run in main thread!"; // const-string v1, "Can not run in main thread!"
/* invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
public void track ( java.util.List p0 ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 193 */
/* .local p1, "dataList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
android.os.Looper .getMainLooper ( );
(( android.os.Looper ) v0 ).getThread ( ); // invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;
java.lang.Thread .currentThread ( );
/* if-eq v0, v1, :cond_6 */
/* .line 196 */
/* const-class v0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager; */
/* monitor-enter v0 */
/* .line 197 */
try { // :try_start_0
v1 = this.mBindContext;
/* if-nez v1, :cond_0 */
/* .line 198 */
final String v1 = "GnssOneTrackManager"; // const-string v1, "GnssOneTrackManager"
final String v2 = "please check whether call the init method first!"; // const-string v2, "please check whether call the init method first!"
android.util.Log .e ( v1,v2 );
/* .line 199 */
/* monitor-exit v0 */
return;
/* .line 201 */
} // :cond_0
v1 = this.mITrackBinder;
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 202 */
/* iget-boolean v1, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->D:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
final String v1 = "GnssOneTrackManager"; // const-string v1, "GnssOneTrackManager"
final String v2 = "data list call trackEvent"; // const-string v2, "data list call trackEvent"
android.util.Log .d ( v1,v2 );
/* .line 203 */
} // :cond_1
v1 = this.mITrackBinder;
final String v2 = "2882303761518758754"; // const-string v2, "2882303761518758754"
final String v3 = "com.miui.analytics"; // const-string v3, "com.miui.analytics"
int v4 = 3; // const/4 v4, 0x3
/* .line 205 */
final String v1 = "GnssOneTrackManager"; // const-string v1, "GnssOneTrackManager"
final String v2 = "OneTrack Track Success"; // const-string v2, "OneTrack Track Success"
android.util.Log .i ( v1,v2 );
/* .line 206 */
/* monitor-exit v0 */
return;
/* .line 208 */
} // :cond_2
/* iget-boolean v1, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->D:Z */
if ( v1 != null) { // if-eqz v1, :cond_3
final String v1 = "GnssOneTrackManager"; // const-string v1, "GnssOneTrackManager"
final String v2 = "data list No TrackBinder, begin to bindService"; // const-string v2, "data list No TrackBinder, begin to bindService"
android.util.Log .d ( v1,v2 );
/* .line 209 */
} // :cond_3
v1 = this.mBindContext;
v2 = this.intent;
v3 = this.conn;
int v4 = 1; // const/4 v4, 0x1
v1 = (( android.content.Context ) v1 ).bindService ( v2, v3, v4 ); // invoke-virtual {v1, v2, v3, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 211 */
/* .local v1, "bindResult":Z */
try { // :try_start_1
/* const-class v2, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager; */
/* const-wide/16 v3, 0x3e8 */
(( java.lang.Object ) v2 ).wait ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Ljava/lang/Object;->wait(J)V
/* :try_end_1 */
/* .catch Ljava/lang/InterruptedException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 215 */
/* .line 212 */
/* :catch_0 */
/* move-exception v2 */
/* .line 213 */
/* .local v2, "e":Ljava/lang/InterruptedException; */
try { // :try_start_2
final String v3 = "GnssOneTrackManager"; // const-string v3, "GnssOneTrackManager"
final String v4 = "OneTrack Track Exception"; // const-string v4, "OneTrack Track Exception"
android.util.Log .e ( v3,v4 );
/* .line 214 */
(( java.lang.InterruptedException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V
/* .line 216 */
} // .end local v2 # "e":Ljava/lang/InterruptedException;
} // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_5
v2 = this.mITrackBinder;
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 217 */
/* iget-boolean v2, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->D:Z */
if ( v2 != null) { // if-eqz v2, :cond_4
final String v2 = "GnssOneTrackManager"; // const-string v2, "GnssOneTrackManager"
final String v3 = "bind success data list call trackEvent"; // const-string v3, "bind success data list call trackEvent"
android.util.Log .d ( v2,v3 );
/* .line 218 */
} // :cond_4
(( com.android.server.location.gnss.datacollect.GnssOneTrackManager ) p0 ).track ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->track(Ljava/util/List;)V
/* .line 220 */
} // :cond_5
final String v2 = "GnssOneTrackManager"; // const-string v2, "GnssOneTrackManager"
final String v3 = "OneTrack Track Exception"; // const-string v3, "OneTrack Track Exception"
android.util.Log .e ( v2,v3 );
/* .line 222 */
} // .end local v1 # "bindResult":Z
} // :goto_1
/* monitor-exit v0 */
/* .line 223 */
return;
/* .line 222 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
/* .line 194 */
} // :cond_6
/* new-instance v0, Ljava/lang/RuntimeException; */
final String v1 = "Can not run in main thread!"; // const-string v1, "Can not run in main thread!"
/* invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
public void track ( org.json.JSONObject p0 ) {
/* .locals 6 */
/* .param p1, "jsonData" # Lorg/json/JSONObject; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 104 */
android.os.Looper .getMainLooper ( );
(( android.os.Looper ) v0 ).getThread ( ); // invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;
java.lang.Thread .currentThread ( );
/* if-eq v0, v1, :cond_8 */
/* .line 107 */
/* const-class v0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager; */
/* monitor-enter v0 */
/* .line 108 */
try { // :try_start_0
v1 = this.mBindContext;
/* if-nez v1, :cond_0 */
/* .line 109 */
final String v1 = "GnssOneTrackManager"; // const-string v1, "GnssOneTrackManager"
final String v2 = "please check whether call the init method first!"; // const-string v2, "please check whether call the init method first!"
android.util.Log .e ( v1,v2 );
/* .line 110 */
/* monitor-exit v0 */
return;
/* .line 112 */
} // :cond_0
/* if-nez p1, :cond_2 */
/* .line 113 */
/* iget-boolean v1, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->D:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
final String v1 = "GnssOneTrackManager"; // const-string v1, "GnssOneTrackManager"
final String v2 = "jsonData == null"; // const-string v2, "jsonData == null"
android.util.Log .d ( v1,v2 );
/* .line 114 */
} // :cond_1
/* monitor-exit v0 */
return;
/* .line 116 */
} // :cond_2
v1 = this.mITrackBinder;
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 117 */
/* iget-boolean v1, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->D:Z */
if ( v1 != null) { // if-eqz v1, :cond_3
final String v1 = "GnssOneTrackManager"; // const-string v1, "GnssOneTrackManager"
final String v2 = "json data call trackEvent"; // const-string v2, "json data call trackEvent"
android.util.Log .d ( v1,v2 );
/* .line 118 */
} // :cond_3
v1 = this.mITrackBinder;
final String v2 = "2882303761518758754"; // const-string v2, "2882303761518758754"
final String v3 = "com.miui.analytics"; // const-string v3, "com.miui.analytics"
/* .line 119 */
(( org.json.JSONObject ) p1 ).toString ( ); // invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
/* .line 118 */
int v5 = 3; // const/4 v5, 0x3
/* .line 120 */
final String v1 = "GnssOneTrackManager"; // const-string v1, "GnssOneTrackManager"
final String v2 = "OneTrack Track Success"; // const-string v2, "OneTrack Track Success"
android.util.Log .i ( v1,v2 );
/* .line 121 */
/* monitor-exit v0 */
return;
/* .line 123 */
} // :cond_4
/* iget-boolean v1, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->D:Z */
if ( v1 != null) { // if-eqz v1, :cond_5
final String v1 = "GnssOneTrackManager"; // const-string v1, "GnssOneTrackManager"
final String v2 = "json data No TrackBinder, begin to bindService"; // const-string v2, "json data No TrackBinder, begin to bindService"
android.util.Log .d ( v1,v2 );
/* .line 124 */
} // :cond_5
v1 = this.mBindContext;
v2 = this.intent;
v3 = this.conn;
int v4 = 1; // const/4 v4, 0x1
v1 = (( android.content.Context ) v1 ).bindService ( v2, v3, v4 ); // invoke-virtual {v1, v2, v3, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 126 */
/* .local v1, "bindResult":Z */
try { // :try_start_1
/* const-class v2, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager; */
/* const-wide/16 v3, 0x3e8 */
(( java.lang.Object ) v2 ).wait ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Ljava/lang/Object;->wait(J)V
/* :try_end_1 */
/* .catch Ljava/lang/InterruptedException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 130 */
/* .line 127 */
/* :catch_0 */
/* move-exception v2 */
/* .line 128 */
/* .local v2, "e":Ljava/lang/InterruptedException; */
try { // :try_start_2
final String v3 = "GnssOneTrackManager"; // const-string v3, "GnssOneTrackManager"
final String v4 = "OneTrack Track Exception"; // const-string v4, "OneTrack Track Exception"
android.util.Log .e ( v3,v4 );
/* .line 129 */
(( java.lang.InterruptedException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V
/* .line 131 */
} // .end local v2 # "e":Ljava/lang/InterruptedException;
} // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_7
v2 = this.mITrackBinder;
if ( v2 != null) { // if-eqz v2, :cond_7
/* .line 132 */
/* iget-boolean v2, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->D:Z */
if ( v2 != null) { // if-eqz v2, :cond_6
final String v2 = "GnssOneTrackManager"; // const-string v2, "GnssOneTrackManager"
final String v3 = "bind success and json data call trackEvent"; // const-string v3, "bind success and json data call trackEvent"
android.util.Log .d ( v2,v3 );
/* .line 133 */
} // :cond_6
(( com.android.server.location.gnss.datacollect.GnssOneTrackManager ) p0 ).track ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->track(Lorg/json/JSONObject;)V
/* .line 135 */
} // :cond_7
final String v2 = "GnssOneTrackManager"; // const-string v2, "GnssOneTrackManager"
final String v3 = "OneTrack Track Exception"; // const-string v3, "OneTrack Track Exception"
android.util.Log .e ( v2,v3 );
/* .line 137 */
} // .end local v1 # "bindResult":Z
} // :goto_1
/* monitor-exit v0 */
/* .line 138 */
return;
/* .line 137 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
/* .line 105 */
} // :cond_8
/* new-instance v0, Ljava/lang/RuntimeException; */
final String v1 = "Can not run in main thread!"; // const-string v1, "Can not run in main thread!"
/* invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
