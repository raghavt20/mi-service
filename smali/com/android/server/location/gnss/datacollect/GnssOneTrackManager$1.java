class com.android.server.location.gnss.datacollect.GnssOneTrackManager$1 implements android.content.ServiceConnection {
	 /* .source "GnssOneTrackManager.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.location.gnss.datacollect.GnssOneTrackManager this$0; //synthetic
/* # direct methods */
 com.android.server.location.gnss.datacollect.GnssOneTrackManager$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager; */
/* .line 63 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onServiceConnected ( android.content.ComponentName p0, android.os.IBinder p1 ) {
/* .locals 3 */
/* .param p1, "name" # Landroid/content/ComponentName; */
/* .param p2, "service" # Landroid/os/IBinder; */
/* .line 66 */
final String v0 = "GnssOneTrackManager"; // const-string v0, "GnssOneTrackManager"
final String v1 = "BindOneTrackService Success"; // const-string v1, "BindOneTrackService Success"
android.util.Log .i ( v0,v1 );
/* .line 67 */
/* const-class v0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager; */
/* monitor-enter v0 */
/* .line 68 */
try { // :try_start_0
	 v1 = this.this$0;
	 com.miui.analytics.ITrackBinder$Stub .asInterface ( p2 );
	 com.android.server.location.gnss.datacollect.GnssOneTrackManager .-$$Nest$fputmITrackBinder ( v1,v2 );
	 /* .line 69 */
	 /* const-class v1, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager; */
	 (( java.lang.Object ) v1 ).notifyAll ( ); // invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V
	 /* .line 70 */
	 v1 = this.this$0;
	 int v2 = 1; // const/4 v2, 0x1
	 com.android.server.location.gnss.datacollect.GnssOneTrackManager .-$$Nest$fputisServiceBind ( v1,v2 );
	 /* .line 71 */
	 /* monitor-exit v0 */
	 /* .line 72 */
	 return;
	 /* .line 71 */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 /* monitor-exit v0 */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* throw v1 */
} // .end method
public void onServiceDisconnected ( android.content.ComponentName p0 ) {
	 /* .locals 3 */
	 /* .param p1, "name" # Landroid/content/ComponentName; */
	 /* .line 76 */
	 final String v0 = "GnssOneTrackManager"; // const-string v0, "GnssOneTrackManager"
	 final String v1 = "Onetrack Service Disconnected"; // const-string v1, "Onetrack Service Disconnected"
	 android.util.Log .i ( v0,v1 );
	 /* .line 77 */
	 /* const-class v0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager; */
	 /* monitor-enter v0 */
	 /* .line 78 */
	 try { // :try_start_0
		 v1 = this.this$0;
		 int v2 = 0; // const/4 v2, 0x0
		 com.android.server.location.gnss.datacollect.GnssOneTrackManager .-$$Nest$fputmITrackBinder ( v1,v2 );
		 /* .line 79 */
		 v1 = this.this$0;
		 com.android.server.location.gnss.datacollect.GnssOneTrackManager .-$$Nest$fgetmBindContext ( v1 );
		 v2 = this.this$0;
		 com.android.server.location.gnss.datacollect.GnssOneTrackManager .-$$Nest$fgetconn ( v2 );
		 (( android.content.Context ) v1 ).unbindService ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
		 /* .line 80 */
		 v1 = this.this$0;
		 int v2 = 0; // const/4 v2, 0x0
		 com.android.server.location.gnss.datacollect.GnssOneTrackManager .-$$Nest$fputisServiceBind ( v1,v2 );
		 /* .line 81 */
		 /* monitor-exit v0 */
		 /* .line 82 */
		 return;
		 /* .line 81 */
		 /* :catchall_0 */
		 /* move-exception v1 */
		 /* monitor-exit v0 */
		 /* :try_end_0 */
		 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
		 /* throw v1 */
	 } // .end method
