.class Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager$1;
.super Ljava/lang/Object;
.source "GnssOneTrackManager.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;


# direct methods
.method constructor <init>(Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;

    .line 63
    iput-object p1, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager$1;->this$0:Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .line 66
    const-string v0, "GnssOneTrackManager"

    const-string v1, "BindOneTrackService Success"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    const-class v0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;

    monitor-enter v0

    .line 68
    :try_start_0
    iget-object v1, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager$1;->this$0:Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;

    invoke-static {p2}, Lcom/miui/analytics/ITrackBinder$Stub;->asInterface(Landroid/os/IBinder;)Lcom/miui/analytics/ITrackBinder;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->-$$Nest$fputmITrackBinder(Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;Lcom/miui/analytics/ITrackBinder;)V

    .line 69
    const-class v1, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 70
    iget-object v1, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager$1;->this$0:Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->-$$Nest$fputisServiceBind(Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;Z)V

    .line 71
    monitor-exit v0

    .line 72
    return-void

    .line 71
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3
    .param p1, "name"    # Landroid/content/ComponentName;

    .line 76
    const-string v0, "GnssOneTrackManager"

    const-string v1, "Onetrack Service Disconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    const-class v0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;

    monitor-enter v0

    .line 78
    :try_start_0
    iget-object v1, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager$1;->this$0:Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->-$$Nest$fputmITrackBinder(Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;Lcom/miui/analytics/ITrackBinder;)V

    .line 79
    iget-object v1, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager$1;->this$0:Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;

    invoke-static {v1}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->-$$Nest$fgetmBindContext(Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager$1;->this$0:Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;

    invoke-static {v2}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->-$$Nest$fgetconn(Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;)Landroid/content/ServiceConnection;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 80
    iget-object v1, p0, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager$1;->this$0:Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;->-$$Nest$fputisServiceBind(Lcom/android/server/location/gnss/datacollect/GnssOneTrackManager;Z)V

    .line 81
    monitor-exit v0

    .line 82
    return-void

    .line 81
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
