.class Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$GnssEngineUsage;
.super Ljava/lang/Object;
.source "GnssEventTrackingImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GnssEngineUsage"
.end annotation


# instance fields
.field private gnssEngineActuallyUseTime:J

.field private gnssEngineShouldUseTime:J

.field final synthetic this$0:Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;


# direct methods
.method private constructor <init>(Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;JJ)V
    .locals 0
    .param p2, "gnssEngineShouldUseTime"    # J
    .param p4, "gnssEngineActuallyUseTime"    # J

    .line 625
    iput-object p1, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$GnssEngineUsage;->this$0:Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 626
    iput-wide p2, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$GnssEngineUsage;->gnssEngineShouldUseTime:J

    .line 627
    iput-wide p4, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$GnssEngineUsage;->gnssEngineActuallyUseTime:J

    .line 628
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;JJLcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$GnssEngineUsage-IA;)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$GnssEngineUsage;-><init>(Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl;JJ)V

    return-void
.end method


# virtual methods
.method public getGnssEngineActuallyUseTime()J
    .locals 2

    .line 635
    iget-wide v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$GnssEngineUsage;->gnssEngineActuallyUseTime:J

    return-wide v0
.end method

.method public getGnssEngineControlState()I
    .locals 4

    .line 639
    iget-wide v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$GnssEngineUsage;->gnssEngineActuallyUseTime:J

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 640
    const/4 v0, 0x0

    return v0

    .line 641
    :cond_0
    iget-wide v2, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$GnssEngineUsage;->gnssEngineShouldUseTime:J

    cmp-long v0, v2, v0

    if-nez v0, :cond_1

    .line 642
    const/4 v0, 0x2

    return v0

    .line 644
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public getGnssEngineShouldUseTime()J
    .locals 2

    .line 631
    iget-wide v0, p0, Lcom/android/server/location/gnss/datacollect/GnssEventTrackingImpl$GnssEngineUsage;->gnssEngineShouldUseTime:J

    return-wide v0
.end method
