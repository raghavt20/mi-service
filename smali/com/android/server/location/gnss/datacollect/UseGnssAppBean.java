public class com.android.server.location.gnss.datacollect.UseGnssAppBean {
	 /* .source "UseGnssAppBean.java" */
	 /* # instance fields */
	 public Long changeToBackTime;
	 public Long changeToforeTime;
	 public Long glpDuring;
	 public Boolean isAppForeground;
	 public Boolean isRunning;
	 public java.lang.String packageName;
	 public java.lang.String provider;
	 public Long removeTime;
	 public java.lang.String reportInterval;
	 public Long requestTime;
	 /* # direct methods */
	 public com.android.server.location.gnss.datacollect.UseGnssAppBean ( ) {
		 /* .locals 0 */
		 /* .line 10 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Boolean equals ( java.lang.Object p0 ) {
		 /* .locals 7 */
		 /* .param p1, "o" # Ljava/lang/Object; */
		 /* .line 34 */
		 int v0 = 1; // const/4 v0, 0x1
		 /* if-ne p0, p1, :cond_0 */
		 /* .line 35 */
	 } // :cond_0
	 int v1 = 0; // const/4 v1, 0x0
	 if ( p1 != null) { // if-eqz p1, :cond_3
		 (( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
		 (( java.lang.Object ) p1 ).getClass ( ); // invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
		 /* if-eq v2, v3, :cond_1 */
		 /* .line 36 */
	 } // :cond_1
	 /* move-object v2, p1 */
	 /* check-cast v2, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean; */
	 /* .line 37 */
	 /* .local v2, "that":Lcom/android/server/location/gnss/datacollect/UseGnssAppBean; */
	 /* iget-wide v3, p0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->requestTime:J */
	 /* iget-wide v5, v2, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->requestTime:J */
	 /* cmp-long v3, v3, v5 */
	 /* if-nez v3, :cond_2 */
	 /* iget-wide v3, p0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->changeToforeTime:J */
	 /* iget-wide v5, v2, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->changeToforeTime:J */
	 /* cmp-long v3, v3, v5 */
	 /* if-nez v3, :cond_2 */
	 /* iget-wide v3, p0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->changeToBackTime:J */
	 /* iget-wide v5, v2, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->changeToBackTime:J */
	 /* cmp-long v3, v3, v5 */
	 /* if-nez v3, :cond_2 */
	 /* iget-wide v3, p0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->removeTime:J */
	 /* iget-wide v5, v2, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->removeTime:J */
	 /* cmp-long v3, v3, v5 */
	 /* if-nez v3, :cond_2 */
	 /* iget-wide v3, p0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->glpDuring:J */
	 /* iget-wide v5, v2, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->glpDuring:J */
	 /* cmp-long v3, v3, v5 */
	 /* if-nez v3, :cond_2 */
	 /* iget-boolean v3, p0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->isAppForeground:Z */
	 /* iget-boolean v4, v2, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->isAppForeground:Z */
	 /* if-ne v3, v4, :cond_2 */
	 /* iget-boolean v3, p0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->isRunning:Z */
	 /* iget-boolean v4, v2, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->isRunning:Z */
	 /* if-ne v3, v4, :cond_2 */
	 v3 = this.packageName;
	 v4 = this.packageName;
	 v3 = 	 java.util.Objects .equals ( v3,v4 );
	 if ( v3 != null) { // if-eqz v3, :cond_2
		 v3 = this.reportInterval;
		 v4 = this.reportInterval;
		 v3 = 		 java.util.Objects .equals ( v3,v4 );
		 if ( v3 != null) { // if-eqz v3, :cond_2
			 v3 = this.provider;
			 v4 = this.provider;
			 v3 = 			 java.util.Objects .equals ( v3,v4 );
			 if ( v3 != null) { // if-eqz v3, :cond_2
			 } // :cond_2
			 /* move v0, v1 */
		 } // :goto_0
		 /* .line 35 */
	 } // .end local v2 # "that":Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;
} // :cond_3
} // :goto_1
} // .end method
public Integer hashCode ( ) {
/* .locals 10 */
/* .line 42 */
v0 = this.packageName;
v1 = this.reportInterval;
/* iget-wide v2, p0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->requestTime:J */
java.lang.Long .valueOf ( v2,v3 );
/* iget-wide v3, p0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->changeToforeTime:J */
java.lang.Long .valueOf ( v3,v4 );
/* iget-wide v4, p0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->changeToBackTime:J */
java.lang.Long .valueOf ( v4,v5 );
/* iget-wide v5, p0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->removeTime:J */
java.lang.Long .valueOf ( v5,v6 );
/* iget-wide v6, p0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->glpDuring:J */
java.lang.Long .valueOf ( v6,v7 );
/* iget-boolean v7, p0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->isAppForeground:Z */
java.lang.Boolean .valueOf ( v7 );
/* iget-boolean v8, p0, Lcom/android/server/location/gnss/datacollect/UseGnssAppBean;->isRunning:Z */
java.lang.Boolean .valueOf ( v8 );
v9 = this.provider;
/* filled-new-array/range {v0 ..v9}, [Ljava/lang/Object; */
v0 = java.util.Objects .hash ( v0 );
} // .end method
