public class com.android.server.location.gnss.datacollect.GnssPowerBean {
	 /* .source "GnssPowerBean.java" */
	 /* # instance fields */
	 java.lang.String start_power;
	 java.lang.String start_time;
	 java.lang.String stop_power;
	 java.lang.String stop_time;
	 /* # direct methods */
	 public com.android.server.location.gnss.datacollect.GnssPowerBean ( ) {
		 /* .locals 0 */
		 /* .line 10 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Boolean equals ( java.lang.Object p0 ) {
		 /* .locals 5 */
		 /* .param p1, "o" # Ljava/lang/Object; */
		 /* .line 22 */
		 int v0 = 1; // const/4 v0, 0x1
		 /* if-ne p0, p1, :cond_0 */
		 /* .line 23 */
	 } // :cond_0
	 int v1 = 0; // const/4 v1, 0x0
	 if ( p1 != null) { // if-eqz p1, :cond_3
		 (( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
		 (( java.lang.Object ) p1 ).getClass ( ); // invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
		 /* if-eq v2, v3, :cond_1 */
		 /* .line 24 */
	 } // :cond_1
	 /* move-object v2, p1 */
	 /* check-cast v2, Lcom/android/server/location/gnss/datacollect/GnssPowerBean; */
	 /* .line 25 */
	 /* .local v2, "bean":Lcom/android/server/location/gnss/datacollect/GnssPowerBean; */
	 v3 = this.start_time;
	 v4 = this.start_time;
	 v3 = 	 java.util.Objects .equals ( v3,v4 );
	 if ( v3 != null) { // if-eqz v3, :cond_2
		 v3 = this.stop_time;
		 v4 = this.stop_time;
		 /* .line 26 */
		 v3 = 		 java.util.Objects .equals ( v3,v4 );
		 if ( v3 != null) { // if-eqz v3, :cond_2
			 v3 = this.start_power;
			 v4 = this.start_power;
			 /* .line 27 */
			 v3 = 			 java.util.Objects .equals ( v3,v4 );
			 if ( v3 != null) { // if-eqz v3, :cond_2
				 v3 = this.stop_power;
				 v4 = this.stop_power;
				 /* .line 28 */
				 v3 = 				 java.util.Objects .equals ( v3,v4 );
				 if ( v3 != null) { // if-eqz v3, :cond_2
				 } // :cond_2
				 /* move v0, v1 */
				 /* .line 25 */
			 } // :goto_0
			 /* .line 23 */
		 } // .end local v2 # "bean":Lcom/android/server/location/gnss/datacollect/GnssPowerBean;
	 } // :cond_3
} // :goto_1
} // .end method
public Integer hashCode ( ) {
/* .locals 4 */
/* .line 33 */
v0 = this.start_time;
v1 = this.stop_time;
v2 = this.start_power;
v3 = this.stop_power;
/* filled-new-array {v0, v1, v2, v3}, [Ljava/lang/Object; */
v0 = java.util.Objects .hash ( v0 );
} // .end method
