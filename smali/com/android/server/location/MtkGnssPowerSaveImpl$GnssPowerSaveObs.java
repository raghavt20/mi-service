class com.android.server.location.MtkGnssPowerSaveImpl$GnssPowerSaveObs extends android.database.ContentObserver {
	 /* .source "MtkGnssPowerSaveImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/location/MtkGnssPowerSaveImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "GnssPowerSaveObs" */
} // .end annotation
/* # instance fields */
private android.content.Context mContext;
final com.android.server.location.MtkGnssPowerSaveImpl this$0; //synthetic
/* # direct methods */
public com.android.server.location.MtkGnssPowerSaveImpl$GnssPowerSaveObs ( ) {
/* .locals 0 */
/* .param p2, "mContext" # Landroid/content/Context; */
/* .param p3, "handler" # Landroid/os/Handler; */
/* .line 555 */
this.this$0 = p1;
/* .line 556 */
/* invoke-direct {p0, p3}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
/* .line 557 */
this.mContext = p2;
/* .line 558 */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "selfChange" # Z */
/* .line 562 */
/* invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V */
/* .line 563 */
final String v0 = "persist.sys.gps.support_disable_l5_config"; // const-string v0, "persist.sys.gps.support_disable_l5_config"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
/* if-nez v0, :cond_0 */
/* .line 564 */
return;
/* .line 566 */
} // :cond_0
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v1, "xiaomi_gnss_config_disable_l5" */
int v2 = 2; // const/4 v2, 0x2
v0 = android.provider.Settings$Secure .getInt ( v0,v1,v2 );
/* .line 569 */
/* .local v0, "state":I */
v1 = this.this$0;
com.android.server.location.MtkGnssPowerSaveImpl .-$$Nest$fgetTAG ( v1 );
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "xiaomi_mtk_gnss_power_save_feature had set to " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v2 );
/* .line 570 */
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_1 */
/* .line 571 */
v1 = this.this$0;
(( com.android.server.location.MtkGnssPowerSaveImpl ) v1 ).disableL5AndDisableGlp ( ); // invoke-virtual {v1}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->disableL5AndDisableGlp()V
/* .line 573 */
} // :cond_1
v1 = this.this$0;
(( com.android.server.location.MtkGnssPowerSaveImpl ) v1 ).enableL5AndEnableGlp ( ); // invoke-virtual {v1}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->enableL5AndEnableGlp()V
/* .line 575 */
} // :goto_0
return;
} // .end method
