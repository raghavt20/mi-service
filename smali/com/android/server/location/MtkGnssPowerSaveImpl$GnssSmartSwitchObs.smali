.class Lcom/android/server/location/MtkGnssPowerSaveImpl$GnssSmartSwitchObs;
.super Landroid/database/ContentObserver;
.source "MtkGnssPowerSaveImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/location/MtkGnssPowerSaveImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GnssSmartSwitchObs"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/android/server/location/MtkGnssPowerSaveImpl;


# direct methods
.method public constructor <init>(Lcom/android/server/location/MtkGnssPowerSaveImpl;Landroid/content/Context;Landroid/os/Handler;)V
    .locals 0
    .param p2, "mContext"    # Landroid/content/Context;
    .param p3, "handler"    # Landroid/os/Handler;

    .line 581
    iput-object p1, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl$GnssSmartSwitchObs;->this$0:Lcom/android/server/location/MtkGnssPowerSaveImpl;

    .line 582
    invoke-direct {p0, p3}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 583
    iput-object p2, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl$GnssSmartSwitchObs;->mContext:Landroid/content/Context;

    .line 584
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 5
    .param p1, "selfChange"    # Z

    .line 588
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 589
    const-string v0, "persist.sys.gps.support_disable_satellite_config"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 590
    return-void

    .line 592
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl$GnssSmartSwitchObs;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v2, "xiaomi_gnss_smart_switch"

    const/4 v3, 0x2

    invoke-static {v0, v2, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 595
    .local v0, "state":I
    iget-object v2, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl$GnssSmartSwitchObs;->this$0:Lcom/android/server/location/MtkGnssPowerSaveImpl;

    invoke-static {v2}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->-$$Nest$fgetTAG(Lcom/android/server/location/MtkGnssPowerSaveImpl;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "xiaomi_mtk_gnss_smart_switch_feature had set to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 596
    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    .line 598
    iget-object v1, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl$GnssSmartSwitchObs;->this$0:Lcom/android/server/location/MtkGnssPowerSaveImpl;

    invoke-virtual {v1, v2}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->activeSmartSwitch(I)V

    .line 599
    iget-object v1, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl$GnssSmartSwitchObs;->this$0:Lcom/android/server/location/MtkGnssPowerSaveImpl;

    invoke-static {v1}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->-$$Nest$fgetTAG(Lcom/android/server/location/MtkGnssPowerSaveImpl;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v3, "start smart switch"

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 600
    iget-object v1, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl$GnssSmartSwitchObs;->this$0:Lcom/android/server/location/MtkGnssPowerSaveImpl;

    invoke-static {v1, v2}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->-$$Nest$fputisSmartSwitchEnableFlag(Lcom/android/server/location/MtkGnssPowerSaveImpl;Z)V

    goto :goto_0

    .line 602
    :cond_1
    iget-object v2, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl$GnssSmartSwitchObs;->this$0:Lcom/android/server/location/MtkGnssPowerSaveImpl;

    invoke-static {v2}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->-$$Nest$fgetisSmartSwitchEnableFlag(Lcom/android/server/location/MtkGnssPowerSaveImpl;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 603
    iget-object v1, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl$GnssSmartSwitchObs;->this$0:Lcom/android/server/location/MtkGnssPowerSaveImpl;

    invoke-static {v1}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->-$$Nest$fgetTAG(Lcom/android/server/location/MtkGnssPowerSaveImpl;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "smartSatellite is not actived,no need to modify"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 604
    return-void

    .line 606
    :cond_2
    iget-object v2, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl$GnssSmartSwitchObs;->this$0:Lcom/android/server/location/MtkGnssPowerSaveImpl;

    invoke-virtual {v2}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->recoveryAllSatellite()V

    .line 607
    iget-object v2, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl$GnssSmartSwitchObs;->this$0:Lcom/android/server/location/MtkGnssPowerSaveImpl;

    invoke-static {v2, v1}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->-$$Nest$fputisSmartSwitchEnableFlag(Lcom/android/server/location/MtkGnssPowerSaveImpl;Z)V

    .line 608
    iget-object v1, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl$GnssSmartSwitchObs;->this$0:Lcom/android/server/location/MtkGnssPowerSaveImpl;

    invoke-static {v1}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->-$$Nest$fgetTAG(Lcom/android/server/location/MtkGnssPowerSaveImpl;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "start recovery all satellite system"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 610
    :goto_0
    return-void
.end method
