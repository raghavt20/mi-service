.class Lcom/android/server/location/MtkGnssPowerSaveImpl$SmartSatelliteHandler;
.super Landroid/os/Handler;
.source "MtkGnssPowerSaveImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/location/MtkGnssPowerSaveImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SmartSatelliteHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/location/MtkGnssPowerSaveImpl;


# direct methods
.method public constructor <init>(Lcom/android/server/location/MtkGnssPowerSaveImpl;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 614
    iput-object p1, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl$SmartSatelliteHandler;->this$0:Lcom/android/server/location/MtkGnssPowerSaveImpl;

    .line 615
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 616
    return-void
.end method

.method public constructor <init>(Lcom/android/server/location/MtkGnssPowerSaveImpl;Landroid/os/Looper;Landroid/os/Handler$Callback;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;
    .param p3, "callback"    # Landroid/os/Handler$Callback;

    .line 618
    iput-object p1, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl$SmartSatelliteHandler;->this$0:Lcom/android/server/location/MtkGnssPowerSaveImpl;

    .line 619
    invoke-direct {p0, p2, p3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    .line 620
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .line 624
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 625
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 626
    return-void

    .line 628
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x1

    const/4 v2, 0x2

    const-string/jumbo v3, "xiaomi_gnss_smart_switch"

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 630
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 631
    .local v0, "powerSaveMode":Z
    const-string/jumbo v4, "xiaomi_gnss_config_disable_l5"

    if-eqz v0, :cond_1

    .line 632
    iget-object v2, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl$SmartSatelliteHandler;->this$0:Lcom/android/server/location/MtkGnssPowerSaveImpl;

    invoke-static {v2}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->-$$Nest$fgetmContext(Lcom/android/server/location/MtkGnssPowerSaveImpl;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v2, v4, v1}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 634
    iget-object v2, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl$SmartSatelliteHandler;->this$0:Lcom/android/server/location/MtkGnssPowerSaveImpl;

    invoke-static {v2}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->-$$Nest$fgetmContext(Lcom/android/server/location/MtkGnssPowerSaveImpl;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    .line 637
    :cond_1
    iget-object v1, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl$SmartSatelliteHandler;->this$0:Lcom/android/server/location/MtkGnssPowerSaveImpl;

    invoke-static {v1}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->-$$Nest$fgetmContext(Lcom/android/server/location/MtkGnssPowerSaveImpl;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, v4, v2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 639
    iget-object v1, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl$SmartSatelliteHandler;->this$0:Lcom/android/server/location/MtkGnssPowerSaveImpl;

    invoke-static {v1}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->-$$Nest$fgetmContext(Lcom/android/server/location/MtkGnssPowerSaveImpl;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, v3, v2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 644
    goto :goto_0

    .line 646
    .end local v0    # "powerSaveMode":Z
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl$SmartSatelliteHandler;->this$0:Lcom/android/server/location/MtkGnssPowerSaveImpl;

    invoke-static {v0}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->-$$Nest$fgetmContext(Lcom/android/server/location/MtkGnssPowerSaveImpl;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, v3, v1}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 648
    goto :goto_0

    .line 650
    :pswitch_2
    iget-object v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl$SmartSatelliteHandler;->this$0:Lcom/android/server/location/MtkGnssPowerSaveImpl;

    invoke-static {v0}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->-$$Nest$fgetmContext(Lcom/android/server/location/MtkGnssPowerSaveImpl;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 652
    nop

    .line 656
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
