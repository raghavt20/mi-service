public class com.android.server.location.MiuiBlurLocationManagerImpl extends com.android.server.location.MiuiBlurLocationManagerStub {
	 /* .source "MiuiBlurLocationManagerImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Boolean DEBUG;
private static final java.lang.String KEY_CALLBACK;
private static final java.lang.String KEY_CID;
private static final java.lang.String KEY_LAC;
private static final java.lang.String KEY_LAST_BLUR_LATITUDE;
private static final java.lang.String KEY_LAST_BLUR_LONGITUDE;
private static final java.lang.String KEY_LATITUDE;
private static final java.lang.String KEY_LONGITUDE;
private static final java.lang.String KEY_MCC;
private static final java.lang.String KEY_MNC;
private static final Integer MSG_BLUR_LOCATION;
private static final Integer MSG_UPDATE_CELL_INFO;
private static final Integer MSG_UPDATE_SATELLITE;
private static final Integer MSG_UPDATE_SATELLITE_SILENT;
private static final java.lang.String TAG;
/* # instance fields */
private android.app.AppOpsManager mAppOps;
private final android.os.Handler mBlurGpsHandler;
private volatile com.android.internal.app.ILocationBlurry mBlurLocationManager;
private final android.os.Messenger mBlurMessenger;
private android.content.Context mContext;
private final android.os.Handler mHandler;
private volatile android.location.Location mLastBlurLocation;
private com.android.server.location.LocationManagerService mLocationService;
private volatile com.android.server.location.MiuiBlurLocationManagerImpl$BaseCellLocation mStashBlurLocationInfo;
private miui.security.SvStatusData mSvStatusData;
/* # direct methods */
static com.android.server.location.MiuiBlurLocationManagerImpl$BaseCellLocation -$$Nest$fgetmStashBlurLocationInfo ( com.android.server.location.MiuiBlurLocationManagerImpl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mStashBlurLocationInfo;
} // .end method
static void -$$Nest$fputmSvStatusData ( com.android.server.location.MiuiBlurLocationManagerImpl p0, miui.security.SvStatusData p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 this.mSvStatusData = p1;
	 return;
} // .end method
public com.android.server.location.MiuiBlurLocationManagerImpl ( ) {
	 /* .locals 2 */
	 /* .line 59 */
	 /* invoke-direct {p0}, Lcom/android/server/location/MiuiBlurLocationManagerStub;-><init>()V */
	 /* .line 83 */
	 /* new-instance v0, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation; */
	 int v1 = 0; // const/4 v1, 0x0
	 /* invoke-direct {v0, v1}, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;-><init>(Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation-IA;)V */
	 this.mStashBlurLocationInfo = v0;
	 /* .line 86 */
	 /* new-instance v0, Lcom/android/server/location/MiuiBlurLocationManagerImpl$1; */
	 com.android.internal.os.BackgroundThread .get ( );
	 (( com.android.internal.os.BackgroundThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Lcom/android/internal/os/BackgroundThread;->getLooper()Landroid/os/Looper;
	 /* invoke-direct {v0, p0, v1}, Lcom/android/server/location/MiuiBlurLocationManagerImpl$1;-><init>(Lcom/android/server/location/MiuiBlurLocationManagerImpl;Landroid/os/Looper;)V */
	 this.mHandler = v0;
	 /* .line 119 */
	 /* new-instance v1, Landroid/os/Messenger; */
	 /* invoke-direct {v1, v0}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V */
	 this.mBlurMessenger = v1;
	 /* .line 471 */
	 /* new-instance v0, Lcom/android/server/location/MiuiBlurLocationManagerImpl$2; */
	 com.android.internal.os.BackgroundThread .get ( );
	 (( com.android.internal.os.BackgroundThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Lcom/android/internal/os/BackgroundThread;->getLooper()Landroid/os/Looper;
	 /* invoke-direct {v0, p0, v1}, Lcom/android/server/location/MiuiBlurLocationManagerImpl$2;-><init>(Lcom/android/server/location/MiuiBlurLocationManagerImpl;Landroid/os/Looper;)V */
	 this.mBlurGpsHandler = v0;
	 return;
} // .end method
private android.app.AppOpsManager getAppOps ( ) {
	 /* .locals 2 */
	 /* .line 351 */
	 v0 = this.mAppOps;
	 /* if-nez v0, :cond_0 */
	 /* .line 352 */
	 v0 = this.mContext;
	 final String v1 = "appops"; // const-string v1, "appops"
	 (( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
	 /* check-cast v0, Landroid/app/AppOpsManager; */
	 this.mAppOps = v0;
	 /* .line 354 */
} // :cond_0
v0 = this.mAppOps;
} // .end method
private java.lang.String getS ( java.lang.CharSequence p0 ) {
/* .locals 1 */
/* .param p1, "charSequence" # Ljava/lang/CharSequence; */
/* .line 170 */
/* if-nez p1, :cond_0 */
int v0 = 0; // const/4 v0, 0x0
} // :cond_0
} // :goto_0
} // .end method
private void initBlurLocationData ( ) {
/* .locals 5 */
/* .line 135 */
v0 = this.mBlurLocationManager;
final String v1 = "MiuiBlurLocationManager"; // const-string v1, "MiuiBlurLocationManager"
/* if-nez v0, :cond_0 */
/* .line 136 */
final String v0 = "ILocationBlurry has not been register yet"; // const-string v0, "ILocationBlurry has not been register yet"
android.util.Log .i ( v1,v0 );
/* .line 137 */
return;
/* .line 139 */
} // :cond_0
v0 = this.mHandler;
/* const v2, 0x29cc2 */
v0 = (( android.os.Handler ) v0 ).hasMessages ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 140 */
final String v0 = "blurLocationData return for too frequently."; // const-string v0, "blurLocationData return for too frequently."
android.util.Log .i ( v1,v0 );
/* .line 141 */
return;
/* .line 143 */
} // :cond_1
v0 = this.mHandler;
/* const-wide/32 v3, 0x493e0 */
(( android.os.Handler ) v0 ).sendEmptyMessageDelayed ( v2, v3, v4 ); // invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
/* .line 145 */
try { // :try_start_0
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 146 */
/* .local v0, "input":Landroid/os/Bundle; */
final String v2 = "key_callback"; // const-string v2, "key_callback"
v3 = this.mBlurMessenger;
(( android.os.Messenger ) v3 ).getBinder ( ); // invoke-virtual {v3}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;
(( android.os.Bundle ) v0 ).putBinder ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBinder(Ljava/lang/String;Landroid/os/IBinder;)V
/* .line 147 */
v2 = this.mBlurLocationManager;
/* .line 148 */
final String v2 = "MIUILOG- getBlurryCellLocation now"; // const-string v2, "MIUILOG- getBlurryCellLocation now"
android.util.Log .i ( v1,v2 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 151 */
/* nop */
} // .end local v0 # "input":Landroid/os/Bundle;
/* .line 149 */
/* :catch_0 */
/* move-exception v0 */
/* .line 150 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v2 = "MIUILOG- getBlurryCellLocation exception"; // const-string v2, "MIUILOG- getBlurryCellLocation exception"
android.util.Log .e ( v1,v2,v0 );
/* .line 152 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void logIfDebug ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "info" # Ljava/lang/String; */
/* .line 359 */
return;
} // .end method
private Boolean noteOpUseMyIdentity ( Integer p0, java.lang.String p1 ) {
/* .locals 8 */
/* .param p1, "uid" # I */
/* .param p2, "pkgName" # Ljava/lang/String; */
/* .line 371 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 373 */
/* .local v0, "identity":J */
try { // :try_start_0
/* invoke-direct {p0}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->getAppOps()Landroid/app/AppOpsManager; */
/* const/16 v3, 0x2734 */
final String v6 = "MiuiBlueLocationManager#noteOpUseMyIdentity"; // const-string v6, "MiuiBlueLocationManager#noteOpUseMyIdentity"
int v7 = 0; // const/4 v7, 0x0
/* move v4, p1 */
/* move-object v5, p2 */
v2 = /* invoke-virtual/range {v2 ..v7}, Landroid/app/AppOpsManager;->noteOpNoThrow(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
if ( v2 != null) { // if-eqz v2, :cond_0
int v2 = 1; // const/4 v2, 0x1
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
/* .line 376 */
} // :goto_0
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 373 */
/* .line 376 */
/* :catchall_0 */
/* move-exception v2 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 377 */
/* throw v2 */
} // .end method
/* # virtual methods */
public java.util.List getBlurryCellInfos ( java.util.List p0 ) {
/* .locals 31 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Landroid/telephony/CellInfo;", */
/* ">;)", */
/* "Ljava/util/List<", */
/* "Landroid/telephony/CellInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 245 */
/* .local p1, "cellInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/telephony/CellInfo;>;" */
/* move-object/from16 v0, p0 */
/* sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
/* if-nez v1, :cond_7 */
if ( p1 != null) { // if-eqz p1, :cond_7
v1 = /* invoke-interface/range {p1 ..p1}, Ljava/util/List;->size()I */
if ( v1 != null) { // if-eqz v1, :cond_7
v1 = this.mBlurLocationManager;
if ( v1 != null) { // if-eqz v1, :cond_7
/* .line 246 */
v1 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->initBlurLocationReady()Z */
/* if-nez v1, :cond_0 */
/* goto/16 :goto_2 */
/* .line 250 */
} // :cond_0
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 251 */
/* .local v1, "results":Ljava/util/List;, "Ljava/util/List<Landroid/telephony/CellInfo;>;" */
/* invoke-interface/range {p1 ..p1}, Ljava/util/List;->iterator()Ljava/util/Iterator; */
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_6
/* check-cast v3, Landroid/telephony/CellInfo; */
/* .line 252 */
/* .local v3, "info":Landroid/telephony/CellInfo; */
/* instance-of v4, v3, Landroid/telephony/CellInfoGsm; */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 253 */
/* move-object v4, v3 */
/* check-cast v4, Landroid/telephony/CellInfoGsm; */
(( android.telephony.CellInfoGsm ) v4 ).getCellIdentity ( ); // invoke-virtual {v4}, Landroid/telephony/CellInfoGsm;->getCellIdentity()Landroid/telephony/CellIdentityGsm;
/* .line 254 */
/* .local v4, "identityGsm":Landroid/telephony/CellIdentityGsm; */
/* new-instance v5, Landroid/telephony/CellInfoGsm; */
/* move-object v6, v3 */
/* check-cast v6, Landroid/telephony/CellInfoGsm; */
/* invoke-direct {v5, v6}, Landroid/telephony/CellInfoGsm;-><init>(Landroid/telephony/CellInfoGsm;)V */
/* .line 256 */
/* .local v5, "result":Landroid/telephony/CellInfoGsm; */
/* new-instance v15, Landroid/telephony/CellIdentityGsm; */
v6 = this.mStashBlurLocationInfo;
v7 = com.android.server.location.MiuiBlurLocationManagerImpl$BaseCellLocation .-$$Nest$fgetlac ( v6 );
v6 = this.mStashBlurLocationInfo;
v8 = com.android.server.location.MiuiBlurLocationManagerImpl$BaseCellLocation .-$$Nest$fgetcid ( v6 );
/* .line 258 */
v9 = (( android.telephony.CellIdentityGsm ) v4 ).getArfcn ( ); // invoke-virtual {v4}, Landroid/telephony/CellIdentityGsm;->getArfcn()I
v10 = (( android.telephony.CellIdentityGsm ) v4 ).getBsic ( ); // invoke-virtual {v4}, Landroid/telephony/CellIdentityGsm;->getBsic()I
(( android.telephony.CellIdentityGsm ) v4 ).getMccString ( ); // invoke-virtual {v4}, Landroid/telephony/CellIdentityGsm;->getMccString()Ljava/lang/String;
/* .line 259 */
(( android.telephony.CellIdentityGsm ) v4 ).getMncString ( ); // invoke-virtual {v4}, Landroid/telephony/CellIdentityGsm;->getMncString()Ljava/lang/String;
(( android.telephony.CellIdentityGsm ) v4 ).getOperatorAlphaLong ( ); // invoke-virtual {v4}, Landroid/telephony/CellIdentityGsm;->getOperatorAlphaLong()Ljava/lang/CharSequence;
/* invoke-direct {v0, v6}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->getS(Ljava/lang/CharSequence;)Ljava/lang/String; */
/* .line 260 */
(( android.telephony.CellIdentityGsm ) v4 ).getOperatorAlphaShort ( ); // invoke-virtual {v4}, Landroid/telephony/CellIdentityGsm;->getOperatorAlphaShort()Ljava/lang/CharSequence;
/* invoke-direct {v0, v6}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->getS(Ljava/lang/CharSequence;)Ljava/lang/String; */
(( android.telephony.CellIdentityGsm ) v4 ).getAdditionalPlmns ( ); // invoke-virtual {v4}, Landroid/telephony/CellIdentityGsm;->getAdditionalPlmns()Ljava/util/Set;
/* move-object v6, v15 */
/* move-object/from16 v17, v2 */
/* move-object v2, v15 */
/* move-object/from16 v15, v16 */
/* invoke-direct/range {v6 ..v15}, Landroid/telephony/CellIdentityGsm;-><init>(IIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;)V */
/* .line 256 */
(( android.telephony.CellInfoGsm ) v5 ).setCellIdentity ( v2 ); // invoke-virtual {v5, v2}, Landroid/telephony/CellInfoGsm;->setCellIdentity(Landroid/telephony/CellIdentityGsm;)V
/* .line 261 */
/* .line 263 */
} // .end local v4 # "identityGsm":Landroid/telephony/CellIdentityGsm;
} // .end local v5 # "result":Landroid/telephony/CellInfoGsm;
/* goto/16 :goto_1 */
} // :cond_1
/* move-object/from16 v17, v2 */
/* instance-of v2, v3, Landroid/telephony/CellInfoCdma; */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 264 */
/* move-object v2, v3 */
/* check-cast v2, Landroid/telephony/CellInfoCdma; */
(( android.telephony.CellInfoCdma ) v2 ).getCellIdentity ( ); // invoke-virtual {v2}, Landroid/telephony/CellInfoCdma;->getCellIdentity()Landroid/telephony/CellIdentityCdma;
/* .line 265 */
/* .local v2, "identityCdma":Landroid/telephony/CellIdentityCdma; */
/* new-instance v4, Landroid/telephony/CellInfoCdma; */
/* move-object v5, v3 */
/* check-cast v5, Landroid/telephony/CellInfoCdma; */
/* invoke-direct {v4, v5}, Landroid/telephony/CellInfoCdma;-><init>(Landroid/telephony/CellInfoCdma;)V */
/* .line 267 */
/* .local v4, "result":Landroid/telephony/CellInfoCdma; */
/* new-instance v13, Landroid/telephony/CellIdentityCdma; */
v5 = this.mStashBlurLocationInfo;
v6 = com.android.server.location.MiuiBlurLocationManagerImpl$BaseCellLocation .-$$Nest$fgetlac ( v5 );
/* .line 268 */
v7 = (( android.telephony.CellIdentityCdma ) v2 ).getSystemId ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityCdma;->getSystemId()I
v5 = this.mStashBlurLocationInfo;
v8 = com.android.server.location.MiuiBlurLocationManagerImpl$BaseCellLocation .-$$Nest$fgetcid ( v5 );
/* .line 270 */
v9 = (( android.telephony.CellIdentityCdma ) v2 ).getLongitude ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityCdma;->getLongitude()I
v10 = (( android.telephony.CellIdentityCdma ) v2 ).getLatitude ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityCdma;->getLatitude()I
/* .line 271 */
(( android.telephony.CellIdentityCdma ) v2 ).getOperatorAlphaLong ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityCdma;->getOperatorAlphaLong()Ljava/lang/CharSequence;
/* invoke-direct {v0, v5}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->getS(Ljava/lang/CharSequence;)Ljava/lang/String; */
/* .line 272 */
(( android.telephony.CellIdentityCdma ) v2 ).getOperatorAlphaShort ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityCdma;->getOperatorAlphaShort()Ljava/lang/CharSequence;
/* invoke-direct {v0, v5}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->getS(Ljava/lang/CharSequence;)Ljava/lang/String; */
/* move-object v5, v13 */
/* invoke-direct/range {v5 ..v12}, Landroid/telephony/CellIdentityCdma;-><init>(IIIIILjava/lang/String;Ljava/lang/String;)V */
/* .line 267 */
(( android.telephony.CellInfoCdma ) v4 ).setCellIdentity ( v13 ); // invoke-virtual {v4, v13}, Landroid/telephony/CellInfoCdma;->setCellIdentity(Landroid/telephony/CellIdentityCdma;)V
/* .line 273 */
/* .line 274 */
} // .end local v2 # "identityCdma":Landroid/telephony/CellIdentityCdma;
} // .end local v4 # "result":Landroid/telephony/CellInfoCdma;
/* goto/16 :goto_1 */
} // :cond_2
/* instance-of v2, v3, Landroid/telephony/CellInfoLte; */
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 275 */
/* move-object v2, v3 */
/* check-cast v2, Landroid/telephony/CellInfoLte; */
(( android.telephony.CellInfoLte ) v2 ).getCellIdentity ( ); // invoke-virtual {v2}, Landroid/telephony/CellInfoLte;->getCellIdentity()Landroid/telephony/CellIdentityLte;
/* .line 276 */
/* .local v2, "identityLte":Landroid/telephony/CellIdentityLte; */
/* new-instance v4, Landroid/telephony/CellInfoLte; */
/* move-object v5, v3 */
/* check-cast v5, Landroid/telephony/CellInfoLte; */
/* invoke-direct {v4, v5}, Landroid/telephony/CellInfoLte;-><init>(Landroid/telephony/CellInfoLte;)V */
/* .line 278 */
/* .local v4, "result":Landroid/telephony/CellInfoLte; */
/* new-instance v5, Landroid/telephony/CellIdentityLte; */
v6 = this.mStashBlurLocationInfo;
v19 = com.android.server.location.MiuiBlurLocationManagerImpl$BaseCellLocation .-$$Nest$fgetcid ( v6 );
/* .line 279 */
v20 = (( android.telephony.CellIdentityLte ) v2 ).getPci ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityLte;->getPci()I
v6 = this.mStashBlurLocationInfo;
v21 = com.android.server.location.MiuiBlurLocationManagerImpl$BaseCellLocation .-$$Nest$fgetlac ( v6 );
/* .line 280 */
v22 = (( android.telephony.CellIdentityLte ) v2 ).getEarfcn ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityLte;->getEarfcn()I
/* .line 281 */
(( android.telephony.CellIdentityLte ) v2 ).getBands ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityLte;->getBands()[I
v24 = (( android.telephony.CellIdentityLte ) v2 ).getBandwidth ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityLte;->getBandwidth()I
(( android.telephony.CellIdentityLte ) v2 ).getMccString ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityLte;->getMccString()Ljava/lang/String;
/* .line 282 */
(( android.telephony.CellIdentityLte ) v2 ).getMncString ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityLte;->getMncString()Ljava/lang/String;
(( android.telephony.CellIdentityLte ) v2 ).getOperatorAlphaLong ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityLte;->getOperatorAlphaLong()Ljava/lang/CharSequence;
/* invoke-direct {v0, v6}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->getS(Ljava/lang/CharSequence;)Ljava/lang/String; */
/* .line 283 */
(( android.telephony.CellIdentityLte ) v2 ).getOperatorAlphaShort ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityLte;->getOperatorAlphaShort()Ljava/lang/CharSequence;
/* invoke-direct {v0, v6}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->getS(Ljava/lang/CharSequence;)Ljava/lang/String; */
(( android.telephony.CellIdentityLte ) v2 ).getAdditionalPlmns ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityLte;->getAdditionalPlmns()Ljava/util/Set;
/* .line 284 */
(( android.telephony.CellIdentityLte ) v2 ).getClosedSubscriberGroupInfo ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityLte;->getClosedSubscriberGroupInfo()Landroid/telephony/ClosedSubscriberGroupInfo;
/* move-object/from16 v18, v5 */
/* invoke-direct/range {v18 ..v30}, Landroid/telephony/CellIdentityLte;-><init>(IIII[IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;Landroid/telephony/ClosedSubscriberGroupInfo;)V */
/* .line 278 */
(( android.telephony.CellInfoLte ) v4 ).setCellIdentity ( v5 ); // invoke-virtual {v4, v5}, Landroid/telephony/CellInfoLte;->setCellIdentity(Landroid/telephony/CellIdentityLte;)V
/* .line 285 */
/* .line 286 */
} // .end local v2 # "identityLte":Landroid/telephony/CellIdentityLte;
} // .end local v4 # "result":Landroid/telephony/CellInfoLte;
/* goto/16 :goto_1 */
} // :cond_3
/* instance-of v2, v3, Landroid/telephony/CellInfoWcdma; */
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 287 */
/* move-object v2, v3 */
/* check-cast v2, Landroid/telephony/CellInfoWcdma; */
(( android.telephony.CellInfoWcdma ) v2 ).getCellIdentity ( ); // invoke-virtual {v2}, Landroid/telephony/CellInfoWcdma;->getCellIdentity()Landroid/telephony/CellIdentityWcdma;
/* .line 288 */
/* .local v2, "identityWcdma":Landroid/telephony/CellIdentityWcdma; */
/* new-instance v4, Landroid/telephony/CellInfoWcdma; */
/* move-object v5, v3 */
/* check-cast v5, Landroid/telephony/CellInfoWcdma; */
/* invoke-direct {v4, v5}, Landroid/telephony/CellInfoWcdma;-><init>(Landroid/telephony/CellInfoWcdma;)V */
/* .line 289 */
/* .local v4, "result":Landroid/telephony/CellInfoWcdma; */
/* new-instance v15, Landroid/telephony/CellIdentityWcdma; */
v5 = this.mStashBlurLocationInfo;
v6 = com.android.server.location.MiuiBlurLocationManagerImpl$BaseCellLocation .-$$Nest$fgetlac ( v5 );
v5 = this.mStashBlurLocationInfo;
v7 = com.android.server.location.MiuiBlurLocationManagerImpl$BaseCellLocation .-$$Nest$fgetcid ( v5 );
/* .line 291 */
v8 = (( android.telephony.CellIdentityWcdma ) v2 ).getPsc ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityWcdma;->getPsc()I
v9 = (( android.telephony.CellIdentityWcdma ) v2 ).getUarfcn ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityWcdma;->getUarfcn()I
/* .line 292 */
(( android.telephony.CellIdentityWcdma ) v2 ).getMccString ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityWcdma;->getMccString()Ljava/lang/String;
(( android.telephony.CellIdentityWcdma ) v2 ).getMncString ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityWcdma;->getMncString()Ljava/lang/String;
/* .line 293 */
(( android.telephony.CellIdentityWcdma ) v2 ).getOperatorAlphaLong ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityWcdma;->getOperatorAlphaLong()Ljava/lang/CharSequence;
/* invoke-direct {v0, v5}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->getS(Ljava/lang/CharSequence;)Ljava/lang/String; */
/* .line 294 */
(( android.telephony.CellIdentityWcdma ) v2 ).getOperatorAlphaShort ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityWcdma;->getOperatorAlphaShort()Ljava/lang/CharSequence;
/* invoke-direct {v0, v5}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->getS(Ljava/lang/CharSequence;)Ljava/lang/String; */
/* .line 295 */
(( android.telephony.CellIdentityWcdma ) v2 ).getAdditionalPlmns ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityWcdma;->getAdditionalPlmns()Ljava/util/Set;
/* .line 296 */
(( android.telephony.CellIdentityWcdma ) v2 ).getClosedSubscriberGroupInfo ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityWcdma;->getClosedSubscriberGroupInfo()Landroid/telephony/ClosedSubscriberGroupInfo;
/* move-object v5, v15 */
/* move-object/from16 v18, v2 */
/* move-object v2, v15 */
} // .end local v2 # "identityWcdma":Landroid/telephony/CellIdentityWcdma;
/* .local v18, "identityWcdma":Landroid/telephony/CellIdentityWcdma; */
/* move-object/from16 v15, v16 */
/* invoke-direct/range {v5 ..v15}, Landroid/telephony/CellIdentityWcdma;-><init>(IIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;Landroid/telephony/ClosedSubscriberGroupInfo;)V */
/* .line 289 */
(( android.telephony.CellInfoWcdma ) v4 ).setCellIdentity ( v2 ); // invoke-virtual {v4, v2}, Landroid/telephony/CellInfoWcdma;->setCellIdentity(Landroid/telephony/CellIdentityWcdma;)V
/* .line 297 */
/* .line 298 */
} // .end local v4 # "result":Landroid/telephony/CellInfoWcdma;
} // .end local v18 # "identityWcdma":Landroid/telephony/CellIdentityWcdma;
} // :cond_4
/* instance-of v2, v3, Landroid/telephony/CellInfoNr; */
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 299 */
/* move-object v2, v3 */
/* check-cast v2, Landroid/telephony/CellInfoNr; */
/* .line 300 */
/* .local v2, "infoNr":Landroid/telephony/CellInfoNr; */
(( android.telephony.CellInfoNr ) v2 ).getCellIdentity ( ); // invoke-virtual {v2}, Landroid/telephony/CellInfoNr;->getCellIdentity()Landroid/telephony/CellIdentity;
/* check-cast v4, Landroid/telephony/CellIdentityNr; */
/* .line 301 */
/* .local v4, "identityNr":Landroid/telephony/CellIdentityNr; */
/* new-instance v18, Landroid/telephony/CellIdentityNr; */
v5 = this.mStashBlurLocationInfo;
v6 = com.android.server.location.MiuiBlurLocationManagerImpl$BaseCellLocation .-$$Nest$fgetcid ( v5 );
v5 = this.mStashBlurLocationInfo;
v7 = com.android.server.location.MiuiBlurLocationManagerImpl$BaseCellLocation .-$$Nest$fgetlac ( v5 );
/* .line 302 */
v8 = (( android.telephony.CellIdentityNr ) v4 ).getNrarfcn ( ); // invoke-virtual {v4}, Landroid/telephony/CellIdentityNr;->getNrarfcn()I
(( android.telephony.CellIdentityNr ) v4 ).getBands ( ); // invoke-virtual {v4}, Landroid/telephony/CellIdentityNr;->getBands()[I
(( android.telephony.CellIdentityNr ) v4 ).getMccString ( ); // invoke-virtual {v4}, Landroid/telephony/CellIdentityNr;->getMccString()Ljava/lang/String;
/* .line 303 */
(( android.telephony.CellIdentityNr ) v4 ).getMncString ( ); // invoke-virtual {v4}, Landroid/telephony/CellIdentityNr;->getMncString()Ljava/lang/String;
(( android.telephony.CellIdentityNr ) v4 ).getNci ( ); // invoke-virtual {v4}, Landroid/telephony/CellIdentityNr;->getNci()J
/* move-result-wide v12 */
(( android.telephony.CellIdentityNr ) v4 ).getOperatorAlphaLong ( ); // invoke-virtual {v4}, Landroid/telephony/CellIdentityNr;->getOperatorAlphaLong()Ljava/lang/CharSequence;
/* invoke-direct {v0, v5}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->getS(Ljava/lang/CharSequence;)Ljava/lang/String; */
/* .line 304 */
(( android.telephony.CellIdentityNr ) v4 ).getOperatorAlphaShort ( ); // invoke-virtual {v4}, Landroid/telephony/CellIdentityNr;->getOperatorAlphaShort()Ljava/lang/CharSequence;
/* invoke-direct {v0, v5}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->getS(Ljava/lang/CharSequence;)Ljava/lang/String; */
(( android.telephony.CellIdentityNr ) v4 ).getAdditionalPlmns ( ); // invoke-virtual {v4}, Landroid/telephony/CellIdentityNr;->getAdditionalPlmns()Ljava/util/Set;
/* move-object/from16 v5, v18 */
/* invoke-direct/range {v5 ..v16}, Landroid/telephony/CellIdentityNr;-><init>(III[ILjava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/util/Collection;)V */
/* move-object/from16 v10, v18 */
/* .line 305 */
/* .local v10, "resultNr":Landroid/telephony/CellIdentityNr; */
/* new-instance v12, Landroid/telephony/CellInfoNr; */
v6 = (( android.telephony.CellInfoNr ) v2 ).getCellConnectionStatus ( ); // invoke-virtual {v2}, Landroid/telephony/CellInfoNr;->getCellConnectionStatus()I
/* .line 306 */
v7 = (( android.telephony.CellInfoNr ) v2 ).isRegistered ( ); // invoke-virtual {v2}, Landroid/telephony/CellInfoNr;->isRegistered()Z
(( android.telephony.CellInfoNr ) v2 ).getTimeStamp ( ); // invoke-virtual {v2}, Landroid/telephony/CellInfoNr;->getTimeStamp()J
/* move-result-wide v8 */
/* .line 307 */
(( android.telephony.CellInfoNr ) v2 ).getCellSignalStrength ( ); // invoke-virtual {v2}, Landroid/telephony/CellInfoNr;->getCellSignalStrength()Landroid/telephony/CellSignalStrength;
/* move-object v11, v5 */
/* check-cast v11, Landroid/telephony/CellSignalStrengthNr; */
/* move-object v5, v12 */
/* invoke-direct/range {v5 ..v11}, Landroid/telephony/CellInfoNr;-><init>(IZJLandroid/telephony/CellIdentityNr;Landroid/telephony/CellSignalStrengthNr;)V */
/* .line 308 */
/* .local v5, "result":Landroid/telephony/CellInfoNr; */
/* .line 309 */
} // .end local v2 # "infoNr":Landroid/telephony/CellInfoNr;
} // .end local v4 # "identityNr":Landroid/telephony/CellIdentityNr;
} // .end local v5 # "result":Landroid/telephony/CellInfoNr;
} // .end local v10 # "resultNr":Landroid/telephony/CellIdentityNr;
/* .line 310 */
} // :cond_5
/* .line 312 */
} // .end local v3 # "info":Landroid/telephony/CellInfo;
} // :goto_1
/* move-object/from16 v2, v17 */
/* goto/16 :goto_0 */
/* .line 313 */
} // :cond_6
/* .line 247 */
} // .end local v1 # "results":Ljava/util/List;, "Ljava/util/List<Landroid/telephony/CellInfo;>;"
} // :cond_7
} // :goto_2
} // .end method
public java.util.List getBlurryCellInfos ( java.util.List p0, Integer p1, java.lang.String p2 ) {
/* .locals 1 */
/* .param p2, "uid" # I */
/* .param p3, "pkgName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Landroid/telephony/CellInfo;", */
/* ">;I", */
/* "Ljava/lang/String;", */
/* ")", */
/* "Ljava/util/List<", */
/* "Landroid/telephony/CellInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 318 */
/* .local p1, "location":Ljava/util/List;, "Ljava/util/List<Landroid/telephony/CellInfo;>;" */
v0 = if ( p1 != null) { // if-eqz p1, :cond_1
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = (( com.android.server.location.MiuiBlurLocationManagerImpl ) p0 ).isBlurLocationMode ( p2, p3 ); // invoke-virtual {p0, p2, p3}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->isBlurLocationMode(ILjava/lang/String;)Z
/* if-nez v0, :cond_0 */
/* .line 321 */
} // :cond_0
(( com.android.server.location.MiuiBlurLocationManagerImpl ) p0 ).getBlurryCellInfos ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->getBlurryCellInfos(Ljava/util/List;)Ljava/util/List;
/* .line 319 */
} // :cond_1
} // :goto_0
} // .end method
public android.telephony.CellIdentity getBlurryCellLocation ( android.telephony.CellIdentity p0 ) {
/* .locals 17 */
/* .param p1, "location" # Landroid/telephony/CellIdentity; */
/* .line 187 */
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p1 */
/* sget-boolean v2, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
/* if-nez v2, :cond_6 */
if ( v1 != null) { // if-eqz v1, :cond_6
v2 = this.mBlurLocationManager;
if ( v2 != null) { // if-eqz v2, :cond_6
v2 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->initBlurLocationReady()Z */
/* if-nez v2, :cond_0 */
/* goto/16 :goto_0 */
/* .line 190 */
} // :cond_0
/* instance-of v2, v1, Landroid/telephony/CellIdentityGsm; */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 191 */
/* move-object v2, v1 */
/* check-cast v2, Landroid/telephony/CellIdentityGsm; */
/* .line 192 */
/* .local v2, "identityGsm":Landroid/telephony/CellIdentityGsm; */
/* new-instance v13, Landroid/telephony/CellIdentityGsm; */
v3 = this.mStashBlurLocationInfo;
v4 = com.android.server.location.MiuiBlurLocationManagerImpl$BaseCellLocation .-$$Nest$fgetlac ( v3 );
v3 = this.mStashBlurLocationInfo;
v5 = com.android.server.location.MiuiBlurLocationManagerImpl$BaseCellLocation .-$$Nest$fgetcid ( v3 );
/* .line 194 */
v6 = (( android.telephony.CellIdentityGsm ) v2 ).getArfcn ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityGsm;->getArfcn()I
v7 = (( android.telephony.CellIdentityGsm ) v2 ).getBsic ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityGsm;->getBsic()I
(( android.telephony.CellIdentityGsm ) v2 ).getMccString ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityGsm;->getMccString()Ljava/lang/String;
/* .line 195 */
(( android.telephony.CellIdentityGsm ) v2 ).getMncString ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityGsm;->getMncString()Ljava/lang/String;
(( android.telephony.CellIdentityGsm ) v2 ).getOperatorAlphaLong ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityGsm;->getOperatorAlphaLong()Ljava/lang/CharSequence;
/* invoke-direct {v0, v3}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->getS(Ljava/lang/CharSequence;)Ljava/lang/String; */
/* .line 196 */
(( android.telephony.CellIdentityGsm ) v2 ).getOperatorAlphaShort ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityGsm;->getOperatorAlphaShort()Ljava/lang/CharSequence;
/* invoke-direct {v0, v3}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->getS(Ljava/lang/CharSequence;)Ljava/lang/String; */
(( android.telephony.CellIdentityGsm ) v2 ).getAdditionalPlmns ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityGsm;->getAdditionalPlmns()Ljava/util/Set;
/* move-object v3, v13 */
/* invoke-direct/range {v3 ..v12}, Landroid/telephony/CellIdentityGsm;-><init>(IIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;)V */
/* .line 192 */
/* .line 197 */
} // .end local v2 # "identityGsm":Landroid/telephony/CellIdentityGsm;
} // :cond_1
/* instance-of v2, v1, Landroid/telephony/CellIdentityCdma; */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 198 */
/* move-object v2, v1 */
/* check-cast v2, Landroid/telephony/CellIdentityCdma; */
/* .line 199 */
/* .local v2, "identityCdma":Landroid/telephony/CellIdentityCdma; */
/* new-instance v11, Landroid/telephony/CellIdentityCdma; */
v3 = this.mStashBlurLocationInfo;
v4 = com.android.server.location.MiuiBlurLocationManagerImpl$BaseCellLocation .-$$Nest$fgetlac ( v3 );
/* .line 200 */
v5 = (( android.telephony.CellIdentityCdma ) v2 ).getSystemId ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityCdma;->getSystemId()I
v3 = this.mStashBlurLocationInfo;
v6 = com.android.server.location.MiuiBlurLocationManagerImpl$BaseCellLocation .-$$Nest$fgetcid ( v3 );
/* .line 202 */
v7 = (( android.telephony.CellIdentityCdma ) v2 ).getLongitude ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityCdma;->getLongitude()I
v8 = (( android.telephony.CellIdentityCdma ) v2 ).getLatitude ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityCdma;->getLatitude()I
/* .line 203 */
(( android.telephony.CellIdentityCdma ) v2 ).getOperatorAlphaLong ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityCdma;->getOperatorAlphaLong()Ljava/lang/CharSequence;
/* invoke-direct {v0, v3}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->getS(Ljava/lang/CharSequence;)Ljava/lang/String; */
/* .line 204 */
(( android.telephony.CellIdentityCdma ) v2 ).getOperatorAlphaShort ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityCdma;->getOperatorAlphaShort()Ljava/lang/CharSequence;
/* invoke-direct {v0, v3}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->getS(Ljava/lang/CharSequence;)Ljava/lang/String; */
/* move-object v3, v11 */
/* invoke-direct/range {v3 ..v10}, Landroid/telephony/CellIdentityCdma;-><init>(IIIIILjava/lang/String;Ljava/lang/String;)V */
/* .line 199 */
/* .line 205 */
} // .end local v2 # "identityCdma":Landroid/telephony/CellIdentityCdma;
} // :cond_2
/* instance-of v2, v1, Landroid/telephony/CellIdentityLte; */
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 206 */
/* move-object v2, v1 */
/* check-cast v2, Landroid/telephony/CellIdentityLte; */
/* .line 207 */
/* .local v2, "identityLte":Landroid/telephony/CellIdentityLte; */
/* new-instance v16, Landroid/telephony/CellIdentityLte; */
v3 = this.mStashBlurLocationInfo;
v4 = com.android.server.location.MiuiBlurLocationManagerImpl$BaseCellLocation .-$$Nest$fgetcid ( v3 );
/* .line 208 */
v5 = (( android.telephony.CellIdentityLte ) v2 ).getPci ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityLte;->getPci()I
v3 = this.mStashBlurLocationInfo;
v6 = com.android.server.location.MiuiBlurLocationManagerImpl$BaseCellLocation .-$$Nest$fgetlac ( v3 );
/* .line 209 */
v7 = (( android.telephony.CellIdentityLte ) v2 ).getEarfcn ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityLte;->getEarfcn()I
/* .line 210 */
(( android.telephony.CellIdentityLte ) v2 ).getBands ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityLte;->getBands()[I
v9 = (( android.telephony.CellIdentityLte ) v2 ).getBandwidth ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityLte;->getBandwidth()I
(( android.telephony.CellIdentityLte ) v2 ).getMccString ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityLte;->getMccString()Ljava/lang/String;
/* .line 211 */
(( android.telephony.CellIdentityLte ) v2 ).getMncString ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityLte;->getMncString()Ljava/lang/String;
(( android.telephony.CellIdentityLte ) v2 ).getOperatorAlphaLong ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityLte;->getOperatorAlphaLong()Ljava/lang/CharSequence;
/* invoke-direct {v0, v3}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->getS(Ljava/lang/CharSequence;)Ljava/lang/String; */
/* .line 212 */
(( android.telephony.CellIdentityLte ) v2 ).getOperatorAlphaShort ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityLte;->getOperatorAlphaShort()Ljava/lang/CharSequence;
/* invoke-direct {v0, v3}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->getS(Ljava/lang/CharSequence;)Ljava/lang/String; */
(( android.telephony.CellIdentityLte ) v2 ).getAdditionalPlmns ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityLte;->getAdditionalPlmns()Ljava/util/Set;
/* .line 213 */
(( android.telephony.CellIdentityLte ) v2 ).getClosedSubscriberGroupInfo ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityLte;->getClosedSubscriberGroupInfo()Landroid/telephony/ClosedSubscriberGroupInfo;
/* move-object/from16 v3, v16 */
/* invoke-direct/range {v3 ..v15}, Landroid/telephony/CellIdentityLte;-><init>(IIII[IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;Landroid/telephony/ClosedSubscriberGroupInfo;)V */
/* .line 207 */
/* .line 214 */
} // .end local v2 # "identityLte":Landroid/telephony/CellIdentityLte;
} // :cond_3
/* instance-of v2, v1, Landroid/telephony/CellIdentityWcdma; */
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 215 */
/* move-object v2, v1 */
/* check-cast v2, Landroid/telephony/CellIdentityWcdma; */
/* .line 216 */
/* .local v2, "identityWcdma":Landroid/telephony/CellIdentityWcdma; */
/* new-instance v14, Landroid/telephony/CellIdentityWcdma; */
v3 = this.mStashBlurLocationInfo;
v4 = com.android.server.location.MiuiBlurLocationManagerImpl$BaseCellLocation .-$$Nest$fgetlac ( v3 );
v3 = this.mStashBlurLocationInfo;
v5 = com.android.server.location.MiuiBlurLocationManagerImpl$BaseCellLocation .-$$Nest$fgetcid ( v3 );
/* .line 218 */
v6 = (( android.telephony.CellIdentityWcdma ) v2 ).getPsc ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityWcdma;->getPsc()I
v7 = (( android.telephony.CellIdentityWcdma ) v2 ).getUarfcn ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityWcdma;->getUarfcn()I
/* .line 219 */
(( android.telephony.CellIdentityWcdma ) v2 ).getMccString ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityWcdma;->getMccString()Ljava/lang/String;
(( android.telephony.CellIdentityWcdma ) v2 ).getMncString ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityWcdma;->getMncString()Ljava/lang/String;
/* .line 220 */
(( android.telephony.CellIdentityWcdma ) v2 ).getOperatorAlphaLong ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityWcdma;->getOperatorAlphaLong()Ljava/lang/CharSequence;
/* invoke-direct {v0, v3}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->getS(Ljava/lang/CharSequence;)Ljava/lang/String; */
/* .line 221 */
(( android.telephony.CellIdentityWcdma ) v2 ).getOperatorAlphaShort ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityWcdma;->getOperatorAlphaShort()Ljava/lang/CharSequence;
/* invoke-direct {v0, v3}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->getS(Ljava/lang/CharSequence;)Ljava/lang/String; */
/* .line 222 */
(( android.telephony.CellIdentityWcdma ) v2 ).getAdditionalPlmns ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityWcdma;->getAdditionalPlmns()Ljava/util/Set;
/* .line 223 */
(( android.telephony.CellIdentityWcdma ) v2 ).getClosedSubscriberGroupInfo ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityWcdma;->getClosedSubscriberGroupInfo()Landroid/telephony/ClosedSubscriberGroupInfo;
/* move-object v3, v14 */
/* invoke-direct/range {v3 ..v13}, Landroid/telephony/CellIdentityWcdma;-><init>(IIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;Landroid/telephony/ClosedSubscriberGroupInfo;)V */
/* .line 216 */
/* .line 224 */
} // .end local v2 # "identityWcdma":Landroid/telephony/CellIdentityWcdma;
} // :cond_4
/* instance-of v2, v1, Landroid/telephony/CellIdentityNr; */
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 225 */
/* move-object v2, v1 */
/* check-cast v2, Landroid/telephony/CellIdentityNr; */
/* .line 226 */
/* .local v2, "identityNr":Landroid/telephony/CellIdentityNr; */
/* new-instance v15, Landroid/telephony/CellIdentityNr; */
v3 = this.mStashBlurLocationInfo;
v4 = com.android.server.location.MiuiBlurLocationManagerImpl$BaseCellLocation .-$$Nest$fgetcid ( v3 );
v3 = this.mStashBlurLocationInfo;
v5 = com.android.server.location.MiuiBlurLocationManagerImpl$BaseCellLocation .-$$Nest$fgetlac ( v3 );
/* .line 227 */
v6 = (( android.telephony.CellIdentityNr ) v2 ).getNrarfcn ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityNr;->getNrarfcn()I
(( android.telephony.CellIdentityNr ) v2 ).getBands ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityNr;->getBands()[I
(( android.telephony.CellIdentityNr ) v2 ).getMccString ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityNr;->getMccString()Ljava/lang/String;
/* .line 228 */
(( android.telephony.CellIdentityNr ) v2 ).getMncString ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityNr;->getMncString()Ljava/lang/String;
(( android.telephony.CellIdentityNr ) v2 ).getNci ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityNr;->getNci()J
/* move-result-wide v10 */
(( android.telephony.CellIdentityNr ) v2 ).getOperatorAlphaLong ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityNr;->getOperatorAlphaLong()Ljava/lang/CharSequence;
/* invoke-direct {v0, v3}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->getS(Ljava/lang/CharSequence;)Ljava/lang/String; */
/* .line 229 */
(( android.telephony.CellIdentityNr ) v2 ).getOperatorAlphaShort ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityNr;->getOperatorAlphaShort()Ljava/lang/CharSequence;
/* invoke-direct {v0, v3}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->getS(Ljava/lang/CharSequence;)Ljava/lang/String; */
(( android.telephony.CellIdentityNr ) v2 ).getAdditionalPlmns ( ); // invoke-virtual {v2}, Landroid/telephony/CellIdentityNr;->getAdditionalPlmns()Ljava/util/Set;
/* move-object v3, v15 */
/* invoke-direct/range {v3 ..v14}, Landroid/telephony/CellIdentityNr;-><init>(III[ILjava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/util/Collection;)V */
/* .line 226 */
/* .line 231 */
} // .end local v2 # "identityNr":Landroid/telephony/CellIdentityNr;
} // :cond_5
/* .line 188 */
} // :cond_6
} // :goto_0
} // .end method
public android.telephony.CellIdentity getBlurryCellLocation ( android.telephony.CellIdentity p0, Integer p1, java.lang.String p2 ) {
/* .locals 1 */
/* .param p1, "location" # Landroid/telephony/CellIdentity; */
/* .param p2, "uid" # I */
/* .param p3, "pkgName" # Ljava/lang/String; */
/* .line 237 */
if ( p1 != null) { // if-eqz p1, :cond_1
v0 = (( com.android.server.location.MiuiBlurLocationManagerImpl ) p0 ).isBlurLocationMode ( p2, p3 ); // invoke-virtual {p0, p2, p3}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->isBlurLocationMode(ILjava/lang/String;)Z
/* if-nez v0, :cond_0 */
/* .line 240 */
} // :cond_0
(( com.android.server.location.MiuiBlurLocationManagerImpl ) p0 ).getBlurryCellLocation ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->getBlurryCellLocation(Landroid/telephony/CellIdentity;)Landroid/telephony/CellIdentity;
/* .line 238 */
} // :cond_1
} // :goto_0
} // .end method
public android.location.Location getBlurryLocation ( android.location.Location p0, Integer p1, java.lang.String p2 ) {
/* .locals 2 */
/* .param p1, "location" # Landroid/location/Location; */
/* .param p2, "uid" # I */
/* .param p3, "pkgName" # Ljava/lang/String; */
/* .line 175 */
if ( p1 != null) { // if-eqz p1, :cond_1
v0 = (( com.android.server.location.MiuiBlurLocationManagerImpl ) p0 ).isBlurLocationMode ( p2, p3 ); // invoke-virtual {p0, p2, p3}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->isBlurLocationMode(ILjava/lang/String;)Z
/* if-nez v0, :cond_0 */
/* .line 178 */
} // :cond_0
v0 = this.mStashBlurLocationInfo;
com.android.server.location.MiuiBlurLocationManagerImpl$BaseCellLocation .-$$Nest$fgetlatitude ( v0 );
/* move-result-wide v0 */
(( android.location.Location ) p1 ).setLatitude ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Landroid/location/Location;->setLatitude(D)V
/* .line 179 */
v0 = this.mStashBlurLocationInfo;
com.android.server.location.MiuiBlurLocationManagerImpl$BaseCellLocation .-$$Nest$fgetlongitude ( v0 );
/* move-result-wide v0 */
(( android.location.Location ) p1 ).setLongitude ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Landroid/location/Location;->setLongitude(D)V
/* .line 180 */
int v0 = 0; // const/4 v0, 0x0
(( android.location.Location ) p1 ).setExtras ( v0 ); // invoke-virtual {p1, v0}, Landroid/location/Location;->setExtras(Landroid/os/Bundle;)V
/* .line 181 */
this.mLastBlurLocation = p1;
/* .line 182 */
/* .line 176 */
} // :cond_1
} // :goto_0
} // .end method
public android.location.LocationResult getBlurryLocation ( android.location.LocationResult p0, android.location.util.identity.CallerIdentity p1 ) {
/* .locals 6 */
/* .param p1, "location" # Landroid/location/LocationResult; */
/* .param p2, "identity" # Landroid/location/util/identity/CallerIdentity; */
/* .line 515 */
if ( p1 != null) { // if-eqz p1, :cond_2
v0 = (( android.location.util.identity.CallerIdentity ) p2 ).getUid ( ); // invoke-virtual {p2}, Landroid/location/util/identity/CallerIdentity;->getUid()I
(( android.location.util.identity.CallerIdentity ) p2 ).getPackageName ( ); // invoke-virtual {p2}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;
v0 = (( com.android.server.location.MiuiBlurLocationManagerImpl ) p0 ).isBlurLocationMode ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->isBlurLocationMode(ILjava/lang/String;)Z
/* if-nez v0, :cond_0 */
/* .line 518 */
} // :cond_0
(( android.location.LocationResult ) p1 ).deepCopy ( ); // invoke-virtual {p1}, Landroid/location/LocationResult;->deepCopy()Landroid/location/LocationResult;
/* .line 519 */
/* .local v0, "blurResult":Landroid/location/LocationResult; */
v1 = (( android.location.LocationResult ) v0 ).size ( ); // invoke-virtual {v0}, Landroid/location/LocationResult;->size()I
/* .line 520 */
/* .local v1, "size":I */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
/* if-ge v2, v1, :cond_1 */
/* .line 521 */
(( android.location.LocationResult ) v0 ).get ( v2 ); // invoke-virtual {v0, v2}, Landroid/location/LocationResult;->get(I)Landroid/location/Location;
/* .line 522 */
/* .local v3, "blur":Landroid/location/Location; */
v4 = this.mStashBlurLocationInfo;
com.android.server.location.MiuiBlurLocationManagerImpl$BaseCellLocation .-$$Nest$fgetlatitude ( v4 );
/* move-result-wide v4 */
(( android.location.Location ) v3 ).setLatitude ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Landroid/location/Location;->setLatitude(D)V
/* .line 523 */
v4 = this.mStashBlurLocationInfo;
com.android.server.location.MiuiBlurLocationManagerImpl$BaseCellLocation .-$$Nest$fgetlongitude ( v4 );
/* move-result-wide v4 */
(( android.location.Location ) v3 ).setLongitude ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Landroid/location/Location;->setLongitude(D)V
/* .line 520 */
} // .end local v3 # "blur":Landroid/location/Location;
/* add-int/lit8 v2, v2, 0x1 */
/* .line 526 */
} // .end local v2 # "i":I
} // :cond_1
/* .line 516 */
} // .end local v0 # "blurResult":Landroid/location/LocationResult;
} // .end local v1 # "size":I
} // :cond_2
} // :goto_1
} // .end method
public android.location.Location getLocationIfBlurMode ( android.location.LastLocationRequest p0, android.location.util.identity.CallerIdentity p1, Integer p2 ) {
/* .locals 5 */
/* .param p1, "request" # Landroid/location/LastLocationRequest; */
/* .param p2, "identity" # Landroid/location/util/identity/CallerIdentity; */
/* .param p3, "permissionLevel" # I */
/* .line 449 */
v0 = (( com.android.server.location.MiuiBlurLocationManagerImpl ) p0 ).isBlurLocationMode ( p2 ); // invoke-virtual {p0, p2}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->isBlurLocationMode(Landroid/location/util/identity/CallerIdentity;)Z
/* if-nez v0, :cond_0 */
/* .line 450 */
int v0 = 0; // const/4 v0, 0x0
/* .line 452 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 453 */
/* .local v0, "blurGpsLocation":Landroid/location/Location; */
v1 = this.mLocationService;
v1 = this.mProviderManagers;
(( java.util.concurrent.CopyOnWriteArrayList ) v1 ).iterator ( ); // invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;
v2 = } // :goto_0
final String v3 = "gps"; // const-string v3, "gps"
if ( v2 != null) { // if-eqz v2, :cond_3
/* check-cast v2, Lcom/android/server/location/provider/LocationProviderManager; */
/* .line 454 */
/* .local v2, "lpm":Lcom/android/server/location/provider/LocationProviderManager; */
(( com.android.server.location.provider.LocationProviderManager ) v2 ).getName ( ); // invoke-virtual {v2}, Lcom/android/server/location/provider/LocationProviderManager;->getName()Ljava/lang/String;
v4 = (( java.lang.String ) v3 ).equals ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 455 */
/* .line 457 */
} // :cond_1
(( com.android.server.location.provider.LocationProviderManager ) v2 ).getLastLocation ( p1, p2, p3 ); // invoke-virtual {v2, p1, p2, p3}, Lcom/android/server/location/provider/LocationProviderManager;->getLastLocation(Landroid/location/LastLocationRequest;Landroid/location/util/identity/CallerIdentity;I)Landroid/location/Location;
/* .line 458 */
/* .local v4, "current":Landroid/location/Location; */
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 459 */
/* new-instance v1, Landroid/location/Location; */
/* invoke-direct {v1, v4}, Landroid/location/Location;-><init>(Landroid/location/Location;)V */
/* move-object v0, v1 */
/* .line 460 */
/* .line 462 */
} // .end local v2 # "lpm":Lcom/android/server/location/provider/LocationProviderManager;
} // .end local v4 # "current":Landroid/location/Location;
} // :cond_2
/* .line 463 */
} // :cond_3
} // :goto_1
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 464 */
(( android.location.Location ) v0 ).setProvider ( v3 ); // invoke-virtual {v0, v3}, Landroid/location/Location;->setProvider(Ljava/lang/String;)V
/* .line 465 */
this.mLastBlurLocation = v0;
/* .line 468 */
} // :cond_4
} // .end method
public miui.security.SvStatusData getSvStatusData ( ) {
/* .locals 1 */
/* .line 421 */
v0 = this.mSvStatusData;
} // .end method
public void handleGpsLocationChangedLocked ( java.lang.String p0, android.location.util.identity.CallerIdentity p1 ) {
/* .locals 8 */
/* .param p1, "provider" # Ljava/lang/String; */
/* .param p2, "identity" # Landroid/location/util/identity/CallerIdentity; */
/* .line 483 */
final String v0 = "gps"; // const-string v0, "gps"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_4
v0 = this.mBlurGpsHandler;
/* .line 484 */
v1 = (( android.location.util.identity.CallerIdentity ) p2 ).getUid ( ); // invoke-virtual {p2}, Landroid/location/util/identity/CallerIdentity;->getUid()I
v0 = (( android.os.Handler ) v0 ).hasMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z
/* if-nez v0, :cond_4 */
/* .line 485 */
v0 = (( android.location.util.identity.CallerIdentity ) p2 ).getUid ( ); // invoke-virtual {p2}, Landroid/location/util/identity/CallerIdentity;->getUid()I
(( android.location.util.identity.CallerIdentity ) p2 ).getPackageName ( ); // invoke-virtual {p2}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;
v0 = (( com.android.server.location.MiuiBlurLocationManagerImpl ) p0 ).isBlurLocationMode ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->isBlurLocationMode(ILjava/lang/String;)Z
/* if-nez v0, :cond_0 */
/* .line 488 */
} // :cond_0
v0 = this.mLocationService;
final String v1 = "gps"; // const-string v1, "gps"
(( com.android.server.location.LocationManagerService ) v0 ).getLocationProviderManager ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/location/LocationManagerService;->getLocationProviderManager(Ljava/lang/String;)Lcom/android/server/location/provider/LocationProviderManager;
/* .line 489 */
/* .local v0, "gpsManager":Lcom/android/server/location/provider/LocationProviderManager; */
/* if-nez v0, :cond_1 */
/* .line 490 */
return;
/* .line 492 */
} // :cond_1
v1 = this.mLastBlurLocation;
/* if-nez v1, :cond_2 */
/* .line 493 */
return;
/* .line 495 */
} // :cond_2
v1 = this.mLastBlurLocation;
final String v2 = "gps"; // const-string v2, "gps"
(( android.location.Location ) v1 ).setProvider ( v2 ); // invoke-virtual {v1, v2}, Landroid/location/Location;->setProvider(Ljava/lang/String;)V
/* .line 496 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v1 */
/* .line 498 */
/* .local v1, "clearCallingIdentity":J */
try { // :try_start_0
final String v3 = "mMultiplexerLock"; // const-string v3, "mMultiplexerLock"
/* const-class v4, Ljava/lang/Object; */
miui.util.ReflectionUtils .getObjectField ( v0,v3,v4 );
/* .line 499 */
/* .local v3, "lock":Ljava/lang/Object; */
/* monitor-enter v3 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 500 */
try { // :try_start_1
com.android.server.location.provider.LocationProviderManagerStub .getInstance ( );
int v5 = 1; // const/4 v5, 0x1
/* new-array v5, v5, [Landroid/location/Location; */
v6 = this.mLastBlurLocation;
int v7 = 0; // const/4 v7, 0x0
/* aput-object v6, v5, v7 */
/* .line 501 */
android.location.LocationResult .create ( v5 );
v4 = /* .line 500 */
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 502 */
v4 = this.mBlurGpsHandler;
/* .line 503 */
v5 = (( android.location.util.identity.CallerIdentity ) p2 ).getUid ( ); // invoke-virtual {p2}, Landroid/location/util/identity/CallerIdentity;->getUid()I
/* .line 502 */
(( android.os.Handler ) v4 ).obtainMessage ( v5, p2 ); // invoke-virtual {v4, v5, p2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* const-wide/16 v6, 0xbb8 */
(( android.os.Handler ) v4 ).sendMessageDelayed ( v5, v6, v7 ); // invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 505 */
} // :cond_3
/* monitor-exit v3 */
/* .line 509 */
} // .end local v3 # "lock":Ljava/lang/Object;
/* .line 505 */
/* .restart local v3 # "lock":Ljava/lang/Object; */
/* :catchall_0 */
/* move-exception v4 */
/* monitor-exit v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
} // .end local v0 # "gpsManager":Lcom/android/server/location/provider/LocationProviderManager;
} // .end local v1 # "clearCallingIdentity":J
} // .end local p0 # "this":Lcom/android/server/location/MiuiBlurLocationManagerImpl;
} // .end local p1 # "provider":Ljava/lang/String;
} // .end local p2 # "identity":Landroid/location/util/identity/CallerIdentity;
try { // :try_start_2
/* throw v4 */
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* .line 509 */
} // .end local v3 # "lock":Ljava/lang/Object;
/* .restart local v0 # "gpsManager":Lcom/android/server/location/provider/LocationProviderManager; */
/* .restart local v1 # "clearCallingIdentity":J */
/* .restart local p0 # "this":Lcom/android/server/location/MiuiBlurLocationManagerImpl; */
/* .restart local p1 # "provider":Ljava/lang/String; */
/* .restart local p2 # "identity":Landroid/location/util/identity/CallerIdentity; */
/* :catchall_1 */
/* move-exception v3 */
/* .line 506 */
/* :catch_0 */
/* move-exception v3 */
/* .line 507 */
/* .local v3, "e":Ljava/lang/Exception; */
try { // :try_start_3
final String v4 = "MiuiBlurLocationManager"; // const-string v4, "MiuiBlurLocationManager"
final String v5 = "onReportBlurLocation exception!"; // const-string v5, "onReportBlurLocation exception!"
android.util.Log .e ( v4,v5,v3 );
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* .line 509 */
/* nop */
} // .end local v3 # "e":Ljava/lang/Exception;
} // :goto_0
android.os.Binder .restoreCallingIdentity ( v1,v2 );
/* .line 510 */
/* nop */
/* .line 511 */
return;
/* .line 509 */
} // :goto_1
android.os.Binder .restoreCallingIdentity ( v1,v2 );
/* .line 510 */
/* throw v3 */
/* .line 486 */
} // .end local v0 # "gpsManager":Lcom/android/server/location/provider/LocationProviderManager;
} // .end local v1 # "clearCallingIdentity":J
} // :cond_4
} // :goto_2
return;
} // .end method
public void init ( com.android.server.location.LocationManagerService p0, android.content.Context p1 ) {
/* .locals 1 */
/* .param p1, "service" # Lcom/android/server/location/LocationManagerService; */
/* .param p2, "context" # Landroid/content/Context; */
/* .line 123 */
if ( p1 != null) { // if-eqz p1, :cond_0
v0 = this.mLocationService;
/* if-nez v0, :cond_0 */
/* .line 124 */
this.mLocationService = p1;
/* .line 126 */
} // :cond_0
if ( p2 != null) { // if-eqz p2, :cond_1
v0 = this.mContext;
/* if-nez v0, :cond_1 */
/* .line 127 */
this.mContext = p2;
/* .line 129 */
} // :cond_1
return;
} // .end method
public Boolean initBlurLocationReady ( ) {
/* .locals 3 */
/* .line 381 */
v0 = this.mStashBlurLocationInfo;
v0 = (( com.android.server.location.MiuiBlurLocationManagerImpl$BaseCellLocation ) v0 ).isValidInfo ( ); // invoke-virtual {v0}, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->isValidInfo()Z
/* if-nez v0, :cond_0 */
/* .line 382 */
/* invoke-direct {p0}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->initBlurLocationData()V */
/* .line 384 */
} // :cond_0
v0 = this.mStashBlurLocationInfo;
v0 = (( com.android.server.location.MiuiBlurLocationManagerImpl$BaseCellLocation ) v0 ).isValidInfo ( ); // invoke-virtual {v0}, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->isValidInfo()Z
/* .line 385 */
/* .local v0, "validInfo":Z */
/* if-nez v0, :cond_1 */
/* .line 386 */
v1 = this.mHandler;
/* const v2, 0x29cc2 */
(( android.os.Handler ) v1 ).removeMessages ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V
/* .line 388 */
} // :cond_1
} // .end method
public Boolean isBlurLocationMode ( Integer p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "pkgName" # Ljava/lang/String; */
/* .line 363 */
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
/* if-nez v0, :cond_0 */
/* .line 364 */
v0 = android.os.UserHandle .getAppId ( p1 );
/* const/16 v1, 0x2710 */
/* if-lt v0, v1, :cond_0 */
v0 = this.mBlurLocationManager;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 366 */
v0 = /* invoke-direct {p0, p1, p2}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->noteOpUseMyIdentity(ILjava/lang/String;)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 367 */
v0 = (( com.android.server.location.MiuiBlurLocationManagerImpl ) p0 ).initBlurLocationReady ( ); // invoke-virtual {p0}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->initBlurLocationReady()Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 363 */
} // :goto_0
} // .end method
public void mayNotifyGpsListener ( java.lang.String p0, android.location.LocationResult p1 ) {
/* .locals 6 */
/* .param p1, "currentProvider" # Ljava/lang/String; */
/* .param p2, "location" # Landroid/location/LocationResult; */
/* .line 426 */
final String v0 = "gps"; // const-string v0, "gps"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_2 */
final String v0 = "passive"; // const-string v0, "passive"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 429 */
} // :cond_0
v0 = this.mLocationService;
final String v1 = "gps"; // const-string v1, "gps"
(( com.android.server.location.LocationManagerService ) v0 ).getLocationProviderManager ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/location/LocationManagerService;->getLocationProviderManager(Ljava/lang/String;)Lcom/android/server/location/provider/LocationProviderManager;
/* .line 430 */
/* .local v0, "manager":Lcom/android/server/location/provider/LocationProviderManager; */
/* if-nez v0, :cond_1 */
/* .line 431 */
return;
/* .line 434 */
} // :cond_1
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v1 */
/* .line 436 */
/* .local v1, "clearCallingIdentity":J */
try { // :try_start_0
final String v3 = "mMultiplexerLock"; // const-string v3, "mMultiplexerLock"
/* const-class v4, Ljava/lang/Object; */
miui.util.ReflectionUtils .getObjectField ( v0,v3,v4 );
/* .line 437 */
/* .local v3, "lock":Ljava/lang/Object; */
/* monitor-enter v3 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 438 */
try { // :try_start_1
com.android.server.location.provider.LocationProviderManagerStub .getInstance ( );
/* .line 439 */
/* monitor-exit v3 */
/* .line 443 */
} // .end local v3 # "lock":Ljava/lang/Object;
/* .line 439 */
/* .restart local v3 # "lock":Ljava/lang/Object; */
/* :catchall_0 */
/* move-exception v4 */
/* monitor-exit v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
} // .end local v0 # "manager":Lcom/android/server/location/provider/LocationProviderManager;
} // .end local v1 # "clearCallingIdentity":J
} // .end local p0 # "this":Lcom/android/server/location/MiuiBlurLocationManagerImpl;
} // .end local p1 # "currentProvider":Ljava/lang/String;
} // .end local p2 # "location":Landroid/location/LocationResult;
try { // :try_start_2
/* throw v4 */
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* .line 443 */
} // .end local v3 # "lock":Ljava/lang/Object;
/* .restart local v0 # "manager":Lcom/android/server/location/provider/LocationProviderManager; */
/* .restart local v1 # "clearCallingIdentity":J */
/* .restart local p0 # "this":Lcom/android/server/location/MiuiBlurLocationManagerImpl; */
/* .restart local p1 # "currentProvider":Ljava/lang/String; */
/* .restart local p2 # "location":Landroid/location/LocationResult; */
/* :catchall_1 */
/* move-exception v3 */
/* .line 440 */
/* :catch_0 */
/* move-exception v3 */
/* .line 441 */
/* .local v3, "e":Ljava/lang/Exception; */
try { // :try_start_3
final String v4 = "MiuiBlurLocationManager"; // const-string v4, "MiuiBlurLocationManager"
final String v5 = "mayNotifyGpsListener exception!"; // const-string v5, "mayNotifyGpsListener exception!"
android.util.Log .e ( v4,v5,v3 );
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* .line 443 */
/* nop */
} // .end local v3 # "e":Ljava/lang/Exception;
} // :goto_0
android.os.Binder .restoreCallingIdentity ( v1,v2 );
/* .line 444 */
/* nop */
/* .line 445 */
return;
/* .line 443 */
} // :goto_1
android.os.Binder .restoreCallingIdentity ( v1,v2 );
/* .line 444 */
/* throw v3 */
/* .line 427 */
} // .end local v0 # "manager":Lcom/android/server/location/provider/LocationProviderManager;
} // .end local v1 # "clearCallingIdentity":J
} // :cond_2
} // :goto_2
return;
} // .end method
public void registerLocationBlurryManager ( com.android.internal.app.ILocationBlurry p0 ) {
/* .locals 2 */
/* .param p1, "manager" # Lcom/android/internal/app/ILocationBlurry; */
/* .line 159 */
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 160 */
return;
/* .line 162 */
} // :cond_0
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 163 */
final String v0 = "MiuiBlurLocationManager"; // const-string v0, "MiuiBlurLocationManager"
final String v1 = "registerLocationBlurryManager invoke"; // const-string v1, "registerLocationBlurryManager invoke"
android.util.Log .i ( v0,v1 );
/* .line 164 */
this.mBlurLocationManager = p1;
/* .line 165 */
/* invoke-direct {p0}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->initBlurLocationData()V */
/* .line 167 */
} // :cond_1
return;
} // .end method
public void updateSvStatusData ( Integer p0, Integer[] p1, Float[] p2, Float[] p3, Float[] p4, Float[] p5, Float[] p6 ) {
/* .locals 13 */
/* .param p1, "svCount" # I */
/* .param p2, "svidWithFlags" # [I */
/* .param p3, "cn0s" # [F */
/* .param p4, "svElevations" # [F */
/* .param p5, "svAzimuths" # [F */
/* .param p6, "svCarrierFreqs" # [F */
/* .param p7, "basebandCn0s" # [F */
/* .line 394 */
/* move-object v1, p0 */
/* move v10, p1 */
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
/* if-nez v0, :cond_1 */
v0 = this.mBlurLocationManager;
if ( v0 != null) { // if-eqz v0, :cond_1
/* const/16 v0, 0x14 */
/* if-lt v10, v0, :cond_1 */
v0 = this.mHandler;
/* .line 395 */
int v11 = 3; // const/4 v11, 0x3
v0 = (( android.os.Handler ) v0 ).hasMessages ( v11 ); // invoke-virtual {v0, v11}, Landroid/os/Handler;->hasMessages(I)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* move-object v3, p2 */
/* move-object/from16 v4, p3 */
/* move-object/from16 v5, p4 */
/* move-object/from16 v6, p5 */
/* move-object/from16 v7, p6 */
/* move-object/from16 v8, p7 */
/* goto/16 :goto_1 */
/* .line 398 */
} // :cond_0
/* new-instance v0, Lmiui/security/SvStatusData; */
/* move-object v2, v0 */
/* move v3, p1 */
/* move-object v4, p2 */
/* move-object/from16 v5, p3 */
/* move-object/from16 v6, p4 */
/* move-object/from16 v7, p5 */
/* move-object/from16 v8, p6 */
/* move-object/from16 v9, p7 */
/* invoke-direct/range {v2 ..v9}, Lmiui/security/SvStatusData;-><init>(I[I[F[F[F[F[F)V */
this.mSvStatusData = v0;
/* .line 400 */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* move-object v2, v0 */
/* .line 401 */
/* .local v2, "data":Landroid/os/Bundle; */
final String v0 = "key_svcount"; // const-string v0, "key_svcount"
(( android.os.Bundle ) v2 ).putInt ( v0, p1 ); // invoke-virtual {v2, v0, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 402 */
final String v0 = "key_svidWithFlags"; // const-string v0, "key_svidWithFlags"
/* move-object v3, p2 */
(( android.os.Bundle ) v2 ).putIntArray ( v0, p2 ); // invoke-virtual {v2, v0, p2}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V
/* .line 403 */
final String v0 = "key_cn0s"; // const-string v0, "key_cn0s"
/* move-object/from16 v4, p3 */
(( android.os.Bundle ) v2 ).putFloatArray ( v0, v4 ); // invoke-virtual {v2, v0, v4}, Landroid/os/Bundle;->putFloatArray(Ljava/lang/String;[F)V
/* .line 404 */
final String v0 = "key_svElevations"; // const-string v0, "key_svElevations"
/* move-object/from16 v5, p4 */
(( android.os.Bundle ) v2 ).putFloatArray ( v0, v5 ); // invoke-virtual {v2, v0, v5}, Landroid/os/Bundle;->putFloatArray(Ljava/lang/String;[F)V
/* .line 405 */
final String v0 = "key_svAzimuths"; // const-string v0, "key_svAzimuths"
/* move-object/from16 v6, p5 */
(( android.os.Bundle ) v2 ).putFloatArray ( v0, v6 ); // invoke-virtual {v2, v0, v6}, Landroid/os/Bundle;->putFloatArray(Ljava/lang/String;[F)V
/* .line 406 */
final String v0 = "key_svCarrierFreqs"; // const-string v0, "key_svCarrierFreqs"
/* move-object/from16 v7, p6 */
(( android.os.Bundle ) v2 ).putFloatArray ( v0, v7 ); // invoke-virtual {v2, v0, v7}, Landroid/os/Bundle;->putFloatArray(Ljava/lang/String;[F)V
/* .line 407 */
final String v0 = "key_basebandCn0s"; // const-string v0, "key_basebandCn0s"
/* move-object/from16 v8, p7 */
(( android.os.Bundle ) v2 ).putFloatArray ( v0, v8 ); // invoke-virtual {v2, v0, v8}, Landroid/os/Bundle;->putFloatArray(Ljava/lang/String;[F)V
/* .line 408 */
v0 = this.mBlurMessenger;
(( android.os.Messenger ) v0 ).getBinder ( ); // invoke-virtual {v0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;
final String v9 = "key_callback"; // const-string v9, "key_callback"
(( android.os.Bundle ) v2 ).putBinder ( v9, v0 ); // invoke-virtual {v2, v9, v0}, Landroid/os/Bundle;->putBinder(Ljava/lang/String;Landroid/os/IBinder;)V
/* .line 410 */
try { // :try_start_0
v0 = this.mBlurLocationManager;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 413 */
/* .line 411 */
/* :catch_0 */
/* move-exception v0 */
/* .line 414 */
} // :goto_0
v0 = this.mHandler;
(( android.os.Handler ) v0 ).obtainMessage ( v11 ); // invoke-virtual {v0, v11}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* const-wide/32 v11, 0xdbba0 */
(( android.os.Handler ) v0 ).sendMessageDelayed ( v9, v11, v12 ); // invoke-virtual {v0, v9, v11, v12}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 416 */
return;
/* .line 394 */
} // .end local v2 # "data":Landroid/os/Bundle;
} // :cond_1
/* move-object v3, p2 */
/* move-object/from16 v4, p3 */
/* move-object/from16 v5, p4 */
/* move-object/from16 v6, p5 */
/* move-object/from16 v7, p6 */
/* move-object/from16 v8, p7 */
/* .line 396 */
} // :goto_1
return;
} // .end method
