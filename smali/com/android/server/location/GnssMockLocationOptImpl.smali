.class public Lcom/android/server/location/GnssMockLocationOptImpl;
.super Ljava/lang/Object;
.source "GnssMockLocationOptImpl.java"

# interfaces
.implements Lcom/android/server/location/GnssMockLocationOptStub;


# static fields
.field private static final ALWAYS_MOCK_MODE:I = 0x2

.field private static final ALWAYS_NONMOCK_MODE:I = 0x2

.field private static final MOCK_REMOVE:Ljava/lang/String; = "0"

.field private static final MOCK_SETUP:Ljava/lang/String; = "1"

.field private static final ONLY_MOCK_ONECE:I = 0x1

.field private static final PERSIST_FOR_GMO_VERSION:Ljava/lang/String; = "persist.sys.gmo.version"

.field private static final TAG:Ljava/lang/String; = "GnssMockLocationOpt"

.field private static mContext:Landroid/content/Context;

.field private static final sBatList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sFusedProviderStatus:Z

.field private static sGpsProviderStatus:Z

.field private static sLastFlag:Z

.field private static sLastMockAppPid:I

.field private static sMockFlagSetByUser:Z

.field private static sNetworkProviderStatus:Z


# instance fields
.field private final D:Z

.field private mAlreadyLoadControlFlag:Z

.field private mBatUsing:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mCallerIdentity:Landroid/location/util/identity/CallerIdentity;

.field private mChangeModeInNaviCondition:Z

.field private mCountRemoveTimes:J

.field private mEnableGmoControlStatus:Z

.field private mEnableGnssMockLocationOpt:Z

.field private mFusedProvider:Lcom/android/server/location/provider/MockableLocationProvider;

.field private final mGmoCloudFlagFile:Ljava/io/File;

.field private final mGmoVersion:Ljava/util/concurrent/atomic/AtomicInteger;

.field private mGpsProvider:Lcom/android/server/location/provider/MockableLocationProvider;

.field private mIsMockMode:Z

.field private mIsValidInterval:Z

.field private mLatestMockApp:Ljava/lang/String;

.field private mName:Ljava/lang/String;

.field private mNetworkProvider:Lcom/android/server/location/provider/MockableLocationProvider;

.field private mPermittedRunning:Z

.field private mProvider:Lcom/android/server/location/provider/MockableLocationProvider;

.field private mRecordMockSchedule:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mRemovedBySystem:Z

.field private mTimeInterval:J

.field private mTimeStart:J

.field private mTimeStop:J


# direct methods
.method static bridge synthetic -$$Nest$fgetD(Lcom/android/server/location/GnssMockLocationOptImpl;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmRecordMockSchedule(Lcom/android/server/location/GnssMockLocationOptImpl;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mRecordMockSchedule:Ljava/util/Map;

    return-object p0
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 75
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/android/server/location/GnssMockLocationOptImpl;->sBatList:Ljava/util/HashSet;

    .line 78
    const-string v1, "com.baidu.BaiduMap"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 79
    const-string v1, "com.autonavi.minimap"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 80
    const-string v1, "com.tencent.map"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 109
    const/16 v0, 0x3e8

    sput v0, Lcom/android/server/location/GnssMockLocationOptImpl;->sLastMockAppPid:I

    return-void
.end method

.method constructor <init>()V
    .locals 5

    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mEnableGnssMockLocationOpt:Z

    .line 83
    invoke-static {}, Ljava/util/concurrent/ConcurrentHashMap;->newKeySet()Ljava/util/concurrent/ConcurrentHashMap$KeySetView;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mBatUsing:Ljava/util/Set;

    .line 88
    const-string v1, "persist.sys.gnss_dc.test"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z

    .line 95
    new-instance v1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v1, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mRecordMockSchedule:Ljava/util/Map;

    .line 121
    iput-boolean v0, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mPermittedRunning:Z

    .line 125
    iput-boolean v0, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mEnableGmoControlStatus:Z

    .line 126
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v3

    const-string/jumbo v4, "system"

    invoke-direct {v1, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const-string v3, "GmoCloudFlagFile.xml"

    invoke-direct {v0, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mGmoCloudFlagFile:Ljava/io/File;

    .line 130
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    .line 131
    const-string v1, "persist.sys.gmo.version"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mGmoVersion:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 138
    new-instance v0, Lcom/android/server/location/GnssMockLocationOptImpl$1;

    invoke-direct {v0, p0}, Lcom/android/server/location/GnssMockLocationOptImpl$1;-><init>(Lcom/android/server/location/GnssMockLocationOptImpl;)V

    invoke-static {v0}, Lcom/android/server/location/Order;->setOnChangeListener(Lcom/android/server/location/Order$OnChangeListener;)V

    .line 146
    return-void
.end method

.method private calculate(Ljava/util/Map;Z)V
    .locals 12
    .param p2, "flag"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .line 574
    .local p1, "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Ljava/lang/String;>;"
    const/4 v0, 0x0

    .line 575
    .local v0, "cnt":I
    const/4 v1, 0x0

    .line 576
    .local v1, "cntOdd":I
    const/4 v2, 0x0

    .line 578
    .local v2, "cntEven":I
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 579
    .local v3, "oddTimePoint":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 581
    .local v4, "evenTimePoint":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 583
    .local v6, "key":J
    rem-int/lit8 v8, v0, 0x2

    if-nez v8, :cond_0

    .line 584
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 585
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 587
    :cond_0
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 588
    add-int/lit8 v1, v1, 0x1

    .line 590
    :goto_1
    nop

    .end local v6    # "key":J
    add-int/lit8 v0, v0, 0x1

    .line 591
    goto :goto_0

    .line 592
    :cond_1
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_2
    if-ge v5, v1, :cond_2

    .line 593
    iget-wide v6, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeInterval:J

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Long;

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    sub-long/2addr v8, v10

    add-long/2addr v6, v8

    iput-wide v6, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeInterval:J

    .line 592
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 596
    .end local v5    # "i":I
    :cond_2
    if-nez p2, :cond_3

    .line 597
    iget-wide v5, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeStop:J

    iget-wide v7, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeStart:J

    sub-long/2addr v5, v7

    iget-wide v7, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeInterval:J

    sub-long/2addr v5, v7

    iput-wide v5, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeInterval:J

    .line 599
    :cond_3
    if-eqz p2, :cond_4

    .line 600
    invoke-static {}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->getInstance()Lcom/android/server/location/gnss/GnssEventTrackingStub;

    move-result-object v5

    int-to-long v6, v1

    invoke-interface {v5, v6, v7}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->usingBatInMockModeTimes(J)V

    goto :goto_3

    .line 602
    :cond_4
    invoke-static {}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->getInstance()Lcom/android/server/location/gnss/GnssEventTrackingStub;

    move-result-object v5

    add-int/lit8 v6, v2, -0x1

    int-to-long v6, v6

    invoke-interface {v5, v6, v7}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->usingBatInMockModeTimes(J)V

    .line 604
    :goto_3
    return-void
.end method

.method private dealingProcess()V
    .locals 11

    .line 489
    const-string v0, "1"

    const-string v1, "GnssMockLocationOpt"

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 490
    .local v2, "preResult":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mRecordMockSchedule:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 491
    .local v4, "key":J
    iget-wide v6, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeStart:J

    cmp-long v6, v4, v6

    if-ltz v6, :cond_0

    iget-wide v6, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeStop:J

    cmp-long v6, v4, v6

    if-gtz v6, :cond_0

    .line 492
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    iget-object v7, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mRecordMockSchedule:Ljava/util/Map;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-interface {v2, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 494
    .end local v4    # "key":J
    :cond_0
    goto :goto_0

    .line 496
    :cond_1
    new-instance v3, Ljava/util/LinkedHashMap;

    invoke-direct {v3}, Ljava/util/LinkedHashMap;-><init>()V

    .line 497
    .local v3, "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->stream()Ljava/util/stream/Stream;

    move-result-object v4

    invoke-static {}, Ljava/util/Map$Entry;->comparingByKey()Ljava/util/Comparator;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/stream/Stream;->sorted(Ljava/util/Comparator;)Ljava/util/stream/Stream;

    move-result-object v4

    new-instance v5, Lcom/android/server/location/GnssMockLocationOptImpl$$ExternalSyntheticLambda0;

    invoke-direct {v5, v3}, Lcom/android/server/location/GnssMockLocationOptImpl$$ExternalSyntheticLambda0;-><init>(Ljava/util/Map;)V

    invoke-interface {v4, v5}, Ljava/util/stream/Stream;->forEachOrdered(Ljava/util/function/Consumer;)V

    .line 502
    :try_start_0
    iget-wide v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeStart:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const-string v7, "ms"

    if-eqz v4, :cond_5

    :try_start_1
    iget-wide v8, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeStop:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 503
    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v0

    if-ne v0, v6, :cond_3

    .line 504
    iget-wide v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeStop:J

    iget-wide v8, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeStart:J

    sub-long/2addr v4, v8

    iput-wide v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeInterval:J

    .line 505
    iget-boolean v0, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z

    if-eqz v0, :cond_2

    .line 506
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "always mock mode, time interval is "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeInterval:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 508
    :cond_2
    invoke-static {}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->getInstance()Lcom/android/server/location/gnss/GnssEventTrackingStub;

    move-result-object v0

    const-wide/16 v4, 0x1

    invoke-interface {v0, v4, v5}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->usingBatInMockModeTimes(J)V

    .line 509
    invoke-static {}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->getInstance()Lcom/android/server/location/gnss/GnssEventTrackingStub;

    move-result-object v0

    iget-wide v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeInterval:J

    invoke-interface {v0, v4, v5}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->usingBatInMockModeInterval(J)V

    .line 510
    invoke-direct {p0}, Lcom/android/server/location/GnssMockLocationOptImpl;->resetCondition()V

    .line 511
    return-void

    .line 513
    :cond_3
    invoke-direct {p0, v3, v5}, Lcom/android/server/location/GnssMockLocationOptImpl;->calculate(Ljava/util/Map;Z)V

    .line 514
    iget-boolean v0, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z

    if-eqz v0, :cond_4

    .line 515
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mock to mock mode, time interval is "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeInterval:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 517
    :cond_4
    invoke-static {}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->getInstance()Lcom/android/server/location/gnss/GnssEventTrackingStub;

    move-result-object v0

    iget-wide v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeInterval:J

    invoke-interface {v0, v4, v5}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->usingBatInMockModeInterval(J)V

    .line 518
    invoke-direct {p0}, Lcom/android/server/location/GnssMockLocationOptImpl;->resetCondition()V

    .line 519
    return-void

    .line 523
    :cond_5
    iget-wide v8, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeStart:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    const-string v8, "0"

    if-eqz v4, :cond_7

    :try_start_2
    iget-wide v9, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeStop:J

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 524
    invoke-direct {p0, v3, v5}, Lcom/android/server/location/GnssMockLocationOptImpl;->calculate(Ljava/util/Map;Z)V

    .line 525
    iget-boolean v0, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z

    if-eqz v0, :cond_6

    .line 526
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mock to nonmock mode, time interval is "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeInterval:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 528
    :cond_6
    invoke-static {}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->getInstance()Lcom/android/server/location/gnss/GnssEventTrackingStub;

    move-result-object v0

    iget-wide v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeInterval:J

    invoke-interface {v0, v4, v5}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->usingBatInMockModeInterval(J)V

    .line 529
    invoke-direct {p0}, Lcom/android/server/location/GnssMockLocationOptImpl;->resetCondition()V

    .line 530
    return-void

    .line 534
    :cond_7
    iget-wide v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeStart:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    const/4 v5, 0x0

    if-eqz v4, :cond_b

    iget-wide v9, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeStop:J

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 535
    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v0

    if-ne v0, v6, :cond_9

    .line 536
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeInterval:J

    .line 537
    iget-boolean v0, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z

    if-eqz v0, :cond_8

    .line 538
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "always nonmock mode, time interval is "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeInterval:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 539
    :cond_8
    invoke-direct {p0}, Lcom/android/server/location/GnssMockLocationOptImpl;->resetCondition()V

    .line 540
    return-void

    .line 542
    :cond_9
    invoke-direct {p0, v3, v5}, Lcom/android/server/location/GnssMockLocationOptImpl;->calculate(Ljava/util/Map;Z)V

    .line 543
    iget-boolean v0, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z

    if-eqz v0, :cond_a

    .line 544
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "nonmock to nonmock mode, time interval is "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeInterval:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 546
    :cond_a
    invoke-static {}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->getInstance()Lcom/android/server/location/gnss/GnssEventTrackingStub;

    move-result-object v0

    iget-wide v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeInterval:J

    invoke-interface {v0, v4, v5}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->usingBatInMockModeInterval(J)V

    .line 547
    invoke-direct {p0}, Lcom/android/server/location/GnssMockLocationOptImpl;->resetCondition()V

    .line 548
    return-void

    .line 552
    :cond_b
    iget-wide v9, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeStart:J

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    iget-wide v8, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeStop:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 553
    invoke-direct {p0, v3, v5}, Lcom/android/server/location/GnssMockLocationOptImpl;->calculate(Ljava/util/Map;Z)V

    .line 554
    iget-boolean v0, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z

    if-eqz v0, :cond_c

    .line 555
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "nonmock to mock mode, time interval is "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeInterval:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 557
    :cond_c
    invoke-static {}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->getInstance()Lcom/android/server/location/gnss/GnssEventTrackingStub;

    move-result-object v0

    iget-wide v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeInterval:J

    invoke-interface {v0, v4, v5}, Lcom/android/server/location/gnss/GnssEventTrackingStub;->usingBatInMockModeInterval(J)V

    .line 558
    invoke-direct {p0}, Lcom/android/server/location/GnssMockLocationOptImpl;->resetCondition()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 559
    return-void

    .line 563
    :cond_d
    goto :goto_1

    .line 561
    :catch_0
    move-exception v0

    .line 562
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 564
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    return-void
.end method

.method private getLatestMockAppName(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 608
    if-nez p1, :cond_0

    return-void

    .line 609
    :cond_0
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    .line 610
    .local v0, "pid":I
    sget v1, Lcom/android/server/location/GnssMockLocationOptImpl;->sLastMockAppPid:I

    const-string v2, "GnssMockLocationOpt"

    if-ne v0, v1, :cond_2

    .line 611
    iget-boolean v1, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "duplicate, no need to record, "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 612
    :cond_1
    return-void

    .line 614
    :cond_2
    sput v0, Lcom/android/server/location/GnssMockLocationOptImpl;->sLastMockAppPid:I

    .line 616
    invoke-direct {p0, p1, v0}, Lcom/android/server/location/GnssMockLocationOptImpl;->getProcessName(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mLatestMockApp:Ljava/lang/String;

    .line 617
    iget-boolean v1, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z

    if-eqz v1, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mLatestMockApp:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " calling"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 618
    :cond_3
    return-void
.end method

.method private getProcessName(Landroid/content/Context;I)Ljava/lang/String;
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "pid"    # I

    .line 621
    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    .line 622
    :cond_0
    const-string v1, "activity"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    .line 623
    .local v1, "activityManager":Landroid/app/ActivityManager;
    invoke-virtual {v1}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v2

    .line 624
    .local v2, "list":Ljava/util/List;
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 625
    .local v3, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 626
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 628
    .local v4, "info":Landroid/app/ActivityManager$RunningAppProcessInfo;
    :try_start_0
    iget v5, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-ne v5, p2, :cond_1

    .line 629
    iget-object v0, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 633
    :cond_1
    goto :goto_1

    .line 631
    :catch_0
    move-exception v5

    .line 632
    .local v5, "e":Ljava/lang/Exception;
    const-string v6, "GnssMockLocationOpt"

    invoke-virtual {v5}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 634
    .end local v4    # "info":Landroid/app/ActivityManager$RunningAppProcessInfo;
    .end local v5    # "e":Ljava/lang/Exception;
    :goto_1
    goto :goto_0

    .line 635
    :cond_2
    return-object v0
.end method

.method private grantMockLocationPermission(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 479
    const-string v0, "appops"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AppOpsManager;

    .line 481
    .local v0, "appOpsManager":Landroid/app/AppOpsManager;
    :try_start_0
    const-string v1, "android"

    const/4 v2, 0x0

    const/16 v3, 0x3a

    const/16 v4, 0x3e8

    invoke-virtual {v0, v3, v4, v1, v2}, Landroid/app/AppOpsManager;->setMode(IILjava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 484
    goto :goto_0

    .line 482
    :catch_0
    move-exception v1

    .line 483
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "grant mockLocationPermission failed, cause: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "GnssMockLocationOpt"

    invoke-static {v3, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 485
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private isMockInfluence(Ljava/lang/String;Lcom/android/server/location/provider/MockableLocationProvider;)Z
    .locals 5
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "provider"    # Lcom/android/server/location/provider/MockableLocationProvider;

    .line 272
    iget-boolean v0, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z

    const-string v1, "GnssMockLocationOpt"

    if-eqz v0, :cond_0

    invoke-static {v1, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    :cond_0
    const-string v0, "gps"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v0, :cond_2

    .line 275
    iput-object p2, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mGpsProvider:Lcom/android/server/location/provider/MockableLocationProvider;

    .line 276
    invoke-virtual {p2}, Lcom/android/server/location/provider/MockableLocationProvider;->isMock()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 277
    sput-boolean v3, Lcom/android/server/location/GnssMockLocationOptImpl;->sGpsProviderStatus:Z

    goto :goto_0

    .line 279
    :cond_1
    sput-boolean v2, Lcom/android/server/location/GnssMockLocationOptImpl;->sGpsProviderStatus:Z

    .line 282
    :cond_2
    :goto_0
    const-string v0, "fused"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 283
    iput-object p2, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mFusedProvider:Lcom/android/server/location/provider/MockableLocationProvider;

    .line 284
    invoke-virtual {p2}, Lcom/android/server/location/provider/MockableLocationProvider;->isMock()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 285
    sput-boolean v3, Lcom/android/server/location/GnssMockLocationOptImpl;->sFusedProviderStatus:Z

    goto :goto_1

    .line 287
    :cond_3
    sput-boolean v2, Lcom/android/server/location/GnssMockLocationOptImpl;->sFusedProviderStatus:Z

    .line 290
    :cond_4
    :goto_1
    const-string v0, "network"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 291
    iput-object p2, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mNetworkProvider:Lcom/android/server/location/provider/MockableLocationProvider;

    .line 292
    invoke-virtual {p2}, Lcom/android/server/location/provider/MockableLocationProvider;->isMock()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 293
    sput-boolean v3, Lcom/android/server/location/GnssMockLocationOptImpl;->sNetworkProviderStatus:Z

    goto :goto_2

    .line 295
    :cond_5
    sput-boolean v2, Lcom/android/server/location/GnssMockLocationOptImpl;->sNetworkProviderStatus:Z

    .line 298
    :cond_6
    :goto_2
    sget-boolean v0, Lcom/android/server/location/GnssMockLocationOptImpl;->sGpsProviderStatus:Z

    if-eqz v0, :cond_7

    iget-boolean v0, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z

    if-eqz v0, :cond_7

    const-string v0, "gps is mock"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    :cond_7
    sget-boolean v0, Lcom/android/server/location/GnssMockLocationOptImpl;->sFusedProviderStatus:Z

    if-eqz v0, :cond_8

    iget-boolean v0, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z

    if-eqz v0, :cond_8

    const-string v0, "fused is mock"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 300
    :cond_8
    sget-boolean v0, Lcom/android/server/location/GnssMockLocationOptImpl;->sNetworkProviderStatus:Z

    if-eqz v0, :cond_9

    iget-boolean v0, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z

    if-eqz v0, :cond_9

    const-string v0, "network is mock"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    :cond_9
    sget-boolean v0, Lcom/android/server/location/GnssMockLocationOptImpl;->sGpsProviderStatus:Z

    if-nez v0, :cond_c

    sget-boolean v1, Lcom/android/server/location/GnssMockLocationOptImpl;->sFusedProviderStatus:Z

    if-nez v1, :cond_c

    sget-boolean v4, Lcom/android/server/location/GnssMockLocationOptImpl;->sNetworkProviderStatus:Z

    if-eqz v4, :cond_a

    goto :goto_3

    .line 304
    :cond_a
    if-nez v0, :cond_b

    if-nez v1, :cond_b

    if-nez v4, :cond_b

    .line 305
    return v2

    .line 307
    :cond_b
    return v3

    .line 302
    :cond_c
    :goto_3
    return v3
.end method

.method private isNaviBat(Ljava/lang/String;)Z
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .line 261
    sget-object v0, Lcom/android/server/location/GnssMockLocationOptImpl;->sBatList:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 262
    iget-boolean v0, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z

    if-eqz v0, :cond_0

    .line 263
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is navi app"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GnssMockLocationOpt"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    :cond_0
    const/4 v0, 0x1

    return v0

    .line 267
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method static synthetic lambda$dealingProcess$0(Ljava/util/Map;Ljava/util/Map$Entry;)V
    .locals 2
    .param p0, "result"    # Ljava/util/Map;
    .param p1, "e"    # Ljava/util/Map$Entry;

    .line 497
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private loadCloudDataFromSP(Landroid/content/Context;)Z
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .line 658
    const/4 v0, 0x0

    const-string v1, "GnssMockLocationOpt"

    if-nez p1, :cond_0

    .line 659
    :try_start_0
    const-string v2, "null context object"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 660
    return v0

    .line 662
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->createDeviceProtectedStorageContext()Landroid/content/Context;

    move-result-object v2

    .line 663
    .local v2, "directBootContext":Landroid/content/Context;
    iget-object v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mGmoCloudFlagFile:Ljava/io/File;

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/io/File;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 664
    .local v3, "editor":Landroid/content/SharedPreferences;
    const-string v4, "mEnableGmoControlStatus"

    const/4 v5, 0x1

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 668
    .end local v2    # "directBootContext":Landroid/content/Context;
    .end local v3    # "editor":Landroid/content/SharedPreferences;
    .local v0, "status":Z
    nop

    .line 669
    iput-boolean v5, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mAlreadyLoadControlFlag:Z

    .line 670
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "success to load mEnableGmoControlStatus, "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 671
    return v0

    .line 665
    .end local v0    # "status":Z
    :catch_0
    move-exception v2

    .line 666
    .local v2, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "failed to load mEnableGmoControlStatus, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 667
    return v0
.end method

.method private removeMock()V
    .locals 6

    .line 400
    const-class v0, Lcom/android/server/location/GnssMockLocationOptImpl;

    monitor-enter v0

    .line 401
    :try_start_0
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 403
    .local v1, "identity":J
    :try_start_1
    sget-object v3, Lcom/android/server/location/GnssMockLocationOptImpl;->mContext:Landroid/content/Context;

    invoke-direct {p0, v3}, Lcom/android/server/location/GnssMockLocationOptImpl;->grantMockLocationPermission(Landroid/content/Context;)V

    .line 405
    iget-object v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mGpsProvider:Lcom/android/server/location/provider/MockableLocationProvider;

    const/4 v4, 0x0

    if-eqz v3, :cond_0

    .line 406
    invoke-virtual {v3}, Lcom/android/server/location/provider/MockableLocationProvider;->isMock()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 407
    const-string v3, "GnssMockLocationOpt"

    const-string v5, "remove gps test provider"

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 408
    iget-object v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mGpsProvider:Lcom/android/server/location/provider/MockableLocationProvider;

    invoke-virtual {v3, v4}, Lcom/android/server/location/provider/MockableLocationProvider;->setMockProvider(Lcom/android/server/location/provider/MockLocationProvider;)V

    .line 412
    :cond_0
    iget-object v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mFusedProvider:Lcom/android/server/location/provider/MockableLocationProvider;

    if-eqz v3, :cond_1

    .line 413
    invoke-virtual {v3}, Lcom/android/server/location/provider/MockableLocationProvider;->isMock()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 414
    const-string v3, "GnssMockLocationOpt"

    const-string v5, "remove fused test provider"

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 415
    iget-object v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mFusedProvider:Lcom/android/server/location/provider/MockableLocationProvider;

    invoke-virtual {v3, v4}, Lcom/android/server/location/provider/MockableLocationProvider;->setMockProvider(Lcom/android/server/location/provider/MockLocationProvider;)V

    .line 418
    :cond_1
    iget-object v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mNetworkProvider:Lcom/android/server/location/provider/MockableLocationProvider;

    if-eqz v3, :cond_2

    .line 419
    invoke-virtual {v3}, Lcom/android/server/location/provider/MockableLocationProvider;->isMock()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 420
    const-string v3, "GnssMockLocationOpt"

    const-string v5, "remove network test provider"

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 421
    iget-object v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mNetworkProvider:Lcom/android/server/location/provider/MockableLocationProvider;

    invoke-virtual {v3, v4}, Lcom/android/server/location/provider/MockableLocationProvider;->setMockProvider(Lcom/android/server/location/provider/MockLocationProvider;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 427
    :cond_2
    :try_start_2
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 428
    const-string v3, "GnssMockLocationOpt"

    const-string v4, "remove mock provider in system process"
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 427
    :catchall_0
    move-exception v3

    goto :goto_1

    .line 424
    :catch_0
    move-exception v3

    .line 425
    .local v3, "e":Ljava/lang/Exception;
    :try_start_3
    const-string v4, "GnssMockLocationOpt"

    invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 427
    .end local v3    # "e":Ljava/lang/Exception;
    :try_start_4
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 428
    const-string v3, "GnssMockLocationOpt"

    const-string v4, "remove mock provider in system process"

    :goto_0
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 429
    nop

    .line 430
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mRemovedBySystem:Z

    .line 431
    const-wide/16 v3, 0x1

    iput-wide v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mCountRemoveTimes:J

    .line 432
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mPermittedRunning:Z

    .line 433
    .end local v1    # "identity":J
    monitor-exit v0

    .line 434
    return-void

    .line 427
    .restart local v1    # "identity":J
    :goto_1
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 428
    const-string v4, "GnssMockLocationOpt"

    const-string v5, "remove mock provider in system process"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 429
    nop

    .end local p0    # "this":Lcom/android/server/location/GnssMockLocationOptImpl;
    throw v3

    .line 433
    .end local v1    # "identity":J
    .restart local p0    # "this":Lcom/android/server/location/GnssMockLocationOptImpl;
    :catchall_1
    move-exception v1

    monitor-exit v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v1
.end method

.method private resetCondition()V
    .locals 3

    .line 567
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeInterval:J

    .line 568
    iget-object v2, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mRecordMockSchedule:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    .line 569
    iput-wide v0, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeStart:J

    .line 570
    iput-wide v0, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeStop:J

    .line 571
    return-void
.end method

.method private restoreMock()V
    .locals 19

    .line 438
    const-class v1, Lcom/android/server/location/GnssMockLocationOptImpl;

    monitor-enter v1

    .line 440
    const/4 v2, 0x1

    .line 441
    .local v2, "forceRecoverAllTestProvider":Z
    :try_start_0
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 443
    .local v3, "identity":J
    :try_start_1
    sget-object v0, Lcom/android/server/location/GnssMockLocationOptImpl;->mContext:Landroid/content/Context;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object/from16 v5, p0

    :try_start_2
    invoke-direct {v5, v0}, Lcom/android/server/location/GnssMockLocationOptImpl;->grantMockLocationPermission(Landroid/content/Context;)V

    .line 444
    sget-object v0, Lcom/android/server/location/GnssMockLocationOptImpl;->mContext:Landroid/content/Context;

    const-string v6, "location"

    invoke-virtual {v0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/location/LocationManager;

    .line 445
    .local v6, "locationManager":Landroid/location/LocationManager;
    const-string v7, "gps"

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x1

    const/4 v13, 0x1

    const/4 v14, 0x1

    const/4 v15, 0x1

    const/16 v16, 0x1

    invoke-virtual/range {v6 .. v16}, Landroid/location/LocationManager;->addTestProvider(Ljava/lang/String;ZZZZZZZII)V

    .line 449
    const-string v0, "GnssMockLocationOpt"

    const-string v7, "recover gps test provider"

    invoke-static {v0, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 451
    sget-boolean v0, Lcom/android/server/location/GnssMockLocationOptImpl;->sFusedProviderStatus:Z

    if-nez v0, :cond_1

    if-eqz v2, :cond_0

    goto :goto_0

    .line 458
    :cond_0
    const-string v0, "GnssMockLocationOpt"

    const-string v7, "fused flag false"

    invoke-static {v0, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 452
    :cond_1
    :goto_0
    const-string v0, "GnssMockLocationOpt"

    const-string v7, "recover fused test provider"

    invoke-static {v0, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 453
    const-string v9, "fused"

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x1

    const/4 v15, 0x1

    const/16 v16, 0x1

    const/16 v17, 0x1

    const/16 v18, 0x1

    move-object v8, v6

    invoke-virtual/range {v8 .. v18}, Landroid/location/LocationManager;->addTestProvider(Ljava/lang/String;ZZZZZZZII)V

    .line 460
    :goto_1
    sget-boolean v0, Lcom/android/server/location/GnssMockLocationOptImpl;->sNetworkProviderStatus:Z

    if-nez v0, :cond_3

    if-eqz v2, :cond_2

    goto :goto_2

    .line 467
    :cond_2
    const-string v0, "GnssMockLocationOpt"

    const-string v7, "network flag false"

    invoke-static {v0, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 461
    :cond_3
    :goto_2
    const-string v0, "GnssMockLocationOpt"

    const-string v7, "recover network test provider"

    invoke-static {v0, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 462
    const-string v9, "network"

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x1

    const/4 v15, 0x1

    const/16 v16, 0x1

    const/16 v17, 0x1

    const/16 v18, 0x1

    move-object v8, v6

    invoke-virtual/range {v8 .. v18}, Landroid/location/LocationManager;->addTestProvider(Ljava/lang/String;ZZZZZZZII)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 472
    .end local v6    # "locationManager":Landroid/location/LocationManager;
    :goto_3
    :try_start_3
    invoke-static {v3, v4}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 473
    const-string v0, "GnssMockLocationOpt"

    const-string v6, "restore mock provider in system process"
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    goto :goto_5

    .line 472
    :catchall_0
    move-exception v0

    goto :goto_6

    .line 469
    :catch_0
    move-exception v0

    goto :goto_4

    .line 472
    :catchall_1
    move-exception v0

    move-object/from16 v5, p0

    goto :goto_6

    .line 469
    :catch_1
    move-exception v0

    move-object/from16 v5, p0

    .line 470
    .local v0, "e":Ljava/lang/Exception;
    :goto_4
    :try_start_4
    const-string v6, "GnssMockLocationOpt"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 472
    .end local v0    # "e":Ljava/lang/Exception;
    :try_start_5
    invoke-static {v3, v4}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 473
    const-string v0, "GnssMockLocationOpt"

    const-string v6, "restore mock provider in system process"

    :goto_5
    invoke-static {v0, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 474
    nop

    .line 475
    .end local v2    # "forceRecoverAllTestProvider":Z
    .end local v3    # "identity":J
    monitor-exit v1

    .line 476
    return-void

    .line 472
    .restart local v2    # "forceRecoverAllTestProvider":Z
    .restart local v3    # "identity":J
    :goto_6
    invoke-static {v3, v4}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 473
    const-string v6, "GnssMockLocationOpt"

    const-string v7, "restore mock provider in system process"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 474
    nop

    .end local p0    # "this":Lcom/android/server/location/GnssMockLocationOptImpl;
    throw v0

    .line 475
    .end local v2    # "forceRecoverAllTestProvider":Z
    .end local v3    # "identity":J
    .restart local p0    # "this":Lcom/android/server/location/GnssMockLocationOptImpl;
    :catchall_2
    move-exception v0

    move-object/from16 v5, p0

    :goto_7
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    throw v0

    :catchall_3
    move-exception v0

    goto :goto_7
.end method

.method private saveCloudDataToSP(Landroid/content/Context;Z)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "status"    # Z

    .line 641
    const-string v0, "GnssMockLocationOpt"

    if-nez p1, :cond_0

    .line 642
    :try_start_0
    const-string v1, "null context object"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 643
    return-void

    .line 645
    :cond_0
    iget-object v1, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mGmoCloudFlagFile:Ljava/io/File;

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/io/File;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 646
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "mEnableGmoControlStatus"

    invoke-interface {v1, v2, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 647
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 651
    .end local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    nop

    .line 652
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "success to save mEnableGmoControlStatus, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 653
    return-void

    .line 648
    :catch_0
    move-exception v1

    .line 649
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "failed to save mEnableGmoControlStatus, "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 650
    return-void
.end method


# virtual methods
.method public handleNaviBatRegisteration(Landroid/content/Context;Landroid/location/util/identity/CallerIdentity;Ljava/lang/String;Lcom/android/server/location/provider/MockableLocationProvider;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "callerIdentity"    # Landroid/location/util/identity/CallerIdentity;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "provider"    # Lcom/android/server/location/provider/MockableLocationProvider;

    .line 152
    const/4 v0, 0x0

    .line 153
    .local v0, "isStart":Z
    iget-boolean v1, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mEnableGnssMockLocationOpt:Z

    if-nez v1, :cond_0

    return-void

    .line 155
    :cond_0
    const/4 v1, 0x0

    if-nez p1, :cond_1

    .line 157
    iget-boolean v2, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z

    if-eqz v2, :cond_4

    const-string v2, "GnssMockLocationOpt"

    const-string v3, "context is null"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 159
    :cond_1
    sput-object p1, Lcom/android/server/location/GnssMockLocationOptImpl;->mContext:Landroid/content/Context;

    .line 160
    iput-object p4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mProvider:Lcom/android/server/location/provider/MockableLocationProvider;

    .line 161
    iput-object p3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mName:Ljava/lang/String;

    .line 163
    iget-boolean v2, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mAlreadyLoadControlFlag:Z

    if-nez v2, :cond_4

    .line 164
    iget-object v2, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mGmoVersion:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    move v2, v3

    goto :goto_0

    :cond_2
    move v2, v1

    .line 165
    .local v2, "gmoVersionEnabled":Z
    :goto_0
    sget-object v4, Lcom/android/server/location/GnssMockLocationOptImpl;->mContext:Landroid/content/Context;

    invoke-direct {p0, v4}, Lcom/android/server/location/GnssMockLocationOptImpl;->loadCloudDataFromSP(Landroid/content/Context;)Z

    move-result v4

    .line 166
    .local v4, "cloudControlFlag":Z
    if-eqz v4, :cond_3

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_3
    move v3, v1

    :goto_1
    iput-boolean v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mEnableGmoControlStatus:Z

    .line 167
    const-string v3, "GnssMockLocationOpt"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "GMO version: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mGmoVersion:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", cloud control flag: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 168
    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 167
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    .end local v2    # "gmoVersionEnabled":Z
    .end local v4    # "cloudControlFlag":Z
    :cond_4
    :goto_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 173
    .local v2, "identity":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/android/server/location/GnssMockLocationOptImpl;->isNaviBat(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_5

    return-void

    .line 174
    :cond_5
    iget-boolean v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z

    if-eqz v3, :cond_6

    .line 175
    const-string v3, "GnssMockLocationOpt"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "handleNaviBatRegisteration, add "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " done"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    :cond_6
    iget-object v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mBatUsing:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 178
    const/4 v0, 0x1

    goto :goto_3

    .line 180
    :cond_7
    const/4 v0, 0x0

    .line 182
    :goto_3
    iget-object v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mBatUsing:Ljava/util/Set;

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 184
    if-nez v0, :cond_8

    return-void

    .line 186
    :cond_8
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeStart:J

    .line 188
    sget-boolean v3, Lcom/android/server/location/GnssMockLocationOptImpl;->sMockFlagSetByUser:Z

    if-eqz v3, :cond_a

    .line 189
    iget-boolean v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z

    if-eqz v3, :cond_9

    const-string v3, "GnssMockLocationOpt"

    const-string v4, "axis set begin with mock"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    :cond_9
    iget-object v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mRecordMockSchedule:Ljava/util/Map;

    iget-wide v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeStart:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    const-string v5, "1"

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 192
    :cond_a
    iget-boolean v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z

    if-eqz v3, :cond_b

    const-string v3, "GnssMockLocationOpt"

    const-string v4, "axis set begin with nonmock"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    :cond_b
    iget-object v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mRecordMockSchedule:Ljava/util/Map;

    iget-wide v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeStart:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    const-string v5, "0"

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    :goto_4
    iget-boolean v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mEnableGmoControlStatus:Z

    if-nez v3, :cond_c

    return-void

    .line 197
    :cond_c
    const-class v3, Lcom/android/server/location/GnssMockLocationOptImpl;

    monitor-enter v3

    .line 198
    :try_start_0
    sget-boolean v4, Lcom/android/server/location/GnssMockLocationOptImpl;->sLastFlag:Z

    if-eqz v4, :cond_d

    .line 199
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mCountRemoveTimes:J

    .line 200
    invoke-direct {p0}, Lcom/android/server/location/GnssMockLocationOptImpl;->removeMock()V

    .line 201
    const-string v1, "GnssMockLocationOpt"

    const-string v4, "begin navigation with mock, remove mock process"

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 203
    :cond_d
    iput-boolean v1, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mRemovedBySystem:Z

    .line 204
    const-string v1, "GnssMockLocationOpt"

    const-string v4, "begin navigation with nonmock, nothing to do"

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    :goto_5
    monitor-exit v3

    .line 207
    return-void

    .line 206
    :catchall_0
    move-exception v1

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public handleNaviBatUnregisteration(Landroid/location/util/identity/CallerIdentity;Ljava/lang/String;Lcom/android/server/location/provider/MockableLocationProvider;)V
    .locals 5
    .param p1, "callerIdentity"    # Landroid/location/util/identity/CallerIdentity;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "provider"    # Lcom/android/server/location/provider/MockableLocationProvider;

    .line 213
    iget-boolean v0, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mEnableGnssMockLocationOpt:Z

    if-nez v0, :cond_0

    return-void

    .line 214
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 215
    .local v0, "identity":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/server/location/GnssMockLocationOptImpl;->isNaviBat(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    return-void

    .line 217
    :cond_1
    iget-boolean v1, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z

    if-eqz v1, :cond_2

    .line 218
    const-string v1, "GnssMockLocationOpt"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleNaviBatUnregisteration, remove "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " done"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    :cond_2
    iget-object v1, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mBatUsing:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 222
    iget-object v1, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mBatUsing:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    return-void

    .line 224
    :cond_3
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeStop:J

    .line 226
    sget-boolean v1, Lcom/android/server/location/GnssMockLocationOptImpl;->sMockFlagSetByUser:Z

    if-eqz v1, :cond_5

    .line 227
    iget-boolean v1, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z

    if-eqz v1, :cond_4

    const-string v1, "GnssMockLocationOpt"

    const-string v2, "axis set end with mock"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    :cond_4
    iget-object v1, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mRecordMockSchedule:Ljava/util/Map;

    iget-wide v2, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeStop:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const-string v3, "1"

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 230
    :cond_5
    iget-boolean v1, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z

    if-eqz v1, :cond_6

    const-string v1, "GnssMockLocationOpt"

    const-string v2, "axis set end with nonmock"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    :cond_6
    iget-object v1, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mRecordMockSchedule:Ljava/util/Map;

    iget-wide v2, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeStop:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const-string v3, "0"

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 234
    :goto_0
    const-class v1, Lcom/android/server/location/GnssMockLocationOptImpl;

    monitor-enter v1

    .line 235
    :try_start_0
    iget-boolean v2, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mEnableGmoControlStatus:Z

    if-nez v2, :cond_7

    .line 237
    invoke-direct {p0}, Lcom/android/server/location/GnssMockLocationOptImpl;->dealingProcess()V

    .line 238
    monitor-exit v1

    return-void

    .line 240
    :cond_7
    iget-boolean v2, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mChangeModeInNaviCondition:Z

    const/4 v3, 0x1

    if-nez v2, :cond_9

    .line 241
    iget-boolean v2, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mRemovedBySystem:Z

    const/4 v4, 0x0

    if-eqz v2, :cond_8

    .line 242
    invoke-direct {p0}, Lcom/android/server/location/GnssMockLocationOptImpl;->restoreMock()V

    .line 243
    sput-boolean v3, Lcom/android/server/location/GnssMockLocationOptImpl;->sLastFlag:Z

    .line 244
    iput-boolean v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mRemovedBySystem:Z

    .line 245
    const-string v2, "GnssMockLocationOpt"

    const-string v4, "restore begin mock, and no interrupt by third app"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 247
    :cond_8
    sput-boolean v4, Lcom/android/server/location/GnssMockLocationOptImpl;->sLastFlag:Z

    goto :goto_1

    .line 250
    :cond_9
    invoke-direct {p0}, Lcom/android/server/location/GnssMockLocationOptImpl;->restoreMock()V

    .line 251
    sput-boolean v3, Lcom/android/server/location/GnssMockLocationOptImpl;->sLastFlag:Z

    .line 252
    const-string v2, "GnssMockLocationOpt"

    const-string v4, "force restore command"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    :goto_1
    iput-boolean v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mPermittedRunning:Z

    .line 256
    invoke-direct {p0}, Lcom/android/server/location/GnssMockLocationOptImpl;->dealingProcess()V

    .line 257
    monitor-exit v1

    .line 258
    return-void

    .line 257
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public recordOrderSchedule(ZLjava/lang/String;Lcom/android/server/location/provider/MockableLocationProvider;Lcom/android/server/location/provider/MockLocationProvider;)Z
    .locals 8
    .param p1, "flag"    # Z
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "provider"    # Lcom/android/server/location/provider/MockableLocationProvider;
    .param p4, "testProvider"    # Lcom/android/server/location/provider/MockLocationProvider;

    .line 313
    sget-boolean v0, Lcom/android/server/location/GnssMockLocationOptImpl;->sLastFlag:Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne v0, p1, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v2

    .line 316
    .local v0, "done":Z
    :goto_0
    iget-boolean v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mEnableGmoControlStatus:Z

    if-eqz v3, :cond_12

    .line 318
    iget-boolean v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z

    if-eqz v3, :cond_1

    .line 319
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v3

    .line 320
    .local v3, "pid":I
    const-string v4, "GnssMockLocationOpt"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "pid: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", android os identity: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    .end local v3    # "pid":I
    :cond_1
    sget-object v3, Lcom/android/server/location/GnssMockLocationOptImpl;->mContext:Landroid/content/Context;

    invoke-direct {p0, v3}, Lcom/android/server/location/GnssMockLocationOptImpl;->getLatestMockAppName(Landroid/content/Context;)V

    .line 325
    if-nez p1, :cond_3

    if-eqz p4, :cond_3

    iget-object v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mBatUsing:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    .line 326
    iget-boolean v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z

    if-eqz v3, :cond_2

    const-string v3, "GnssMockLocationOpt"

    const-string/jumbo v4, "third app add test provider"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 328
    :cond_2
    invoke-direct {p0, p2, p3}, Lcom/android/server/location/GnssMockLocationOptImpl;->isMockInfluence(Ljava/lang/String;Lcom/android/server/location/provider/MockableLocationProvider;)Z

    .line 330
    iput-boolean v1, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mChangeModeInNaviCondition:Z

    .line 331
    return v2

    .line 334
    :cond_3
    if-nez p1, :cond_4

    if-nez p4, :cond_4

    iget-object v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mBatUsing:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_4

    .line 337
    iput-boolean v2, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mChangeModeInNaviCondition:Z

    .line 339
    :cond_4
    if-nez p1, :cond_6

    if-eqz p4, :cond_6

    iget-object v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mBatUsing:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 340
    iget-boolean v2, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z

    if-eqz v2, :cond_5

    const-string v2, "GnssMockLocationOpt"

    const-string v3, "recover add test provider"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 341
    :cond_5
    return v1

    .line 345
    :cond_6
    iget-boolean v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z

    if-eqz v3, :cond_8

    .line 346
    const-string v3, "GnssMockLocationOpt"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "flag: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", provider: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-nez p4, :cond_7

    move v5, v1

    goto :goto_1

    :cond_7
    move v5, v2

    :goto_1
    invoke-static {v5}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 348
    :cond_8
    if-nez p1, :cond_e

    if-nez p4, :cond_e

    .line 349
    iget-boolean v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z

    if-eqz v3, :cond_9

    .line 350
    const-string v3, "GnssMockLocationOpt"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "system process: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mLatestMockApp:Ljava/lang/String;

    const-string/jumbo v6, "system"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", fact process: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mLatestMockApp:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", judge flag: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mRemovedBySystem:Z

    .line 351
    invoke-static {v5}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 350
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 354
    :cond_9
    const-class v3, Lcom/android/server/location/GnssMockLocationOptImpl;

    monitor-enter v3

    .line 355
    :try_start_0
    iget-boolean v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mRemovedBySystem:Z

    if-eqz v4, :cond_b

    .line 356
    iget-wide v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mCountRemoveTimes:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mCountRemoveTimes:J

    cmp-long v4, v4, v6

    if-eqz v4, :cond_b

    .line 357
    iget-boolean v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z

    if-eqz v4, :cond_a

    const-string v4, "GnssMockLocationOpt"

    const-string v5, "interrupt by third app"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    :cond_a
    iput-boolean v2, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mRemovedBySystem:Z

    .line 359
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mCountRemoveTimes:J

    .line 362
    :cond_b
    iget-boolean v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mRemovedBySystem:Z

    if-nez v4, :cond_d

    .line 363
    iget-boolean v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z

    if-eqz v4, :cond_c

    const-string v4, "GnssMockLocationOpt"

    const-string v5, "reset all provider"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    :cond_c
    sput-boolean v2, Lcom/android/server/location/GnssMockLocationOptImpl;->sNetworkProviderStatus:Z

    .line 365
    sput-boolean v2, Lcom/android/server/location/GnssMockLocationOptImpl;->sGpsProviderStatus:Z

    .line 366
    sput-boolean v2, Lcom/android/server/location/GnssMockLocationOptImpl;->sFusedProviderStatus:Z

    .line 368
    :cond_d
    monitor-exit v3

    goto :goto_2

    :catchall_0
    move-exception v1

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 371
    :cond_e
    :goto_2
    iget-boolean v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z

    if-eqz v3, :cond_f

    .line 372
    const-string v3, "GnssMockLocationOpt"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "bat null: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mBatUsing:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->isEmpty()Z

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", permitted: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mPermittedRunning:Z

    invoke-static {v5}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    :cond_f
    iget-object v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mBatUsing:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_10

    iget-boolean v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mPermittedRunning:Z

    if-eqz v3, :cond_10

    .line 374
    iget-boolean v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z

    if-eqz v3, :cond_12

    const-string v3, "GnssMockLocationOpt"

    const-string v4, "mock permitted"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 376
    :cond_10
    iget-boolean v1, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z

    if-eqz v1, :cond_11

    const-string v1, "GnssMockLocationOpt"

    const-string v3, "mock denied"

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 377
    :cond_11
    return v2

    .line 381
    :cond_12
    :goto_3
    if-nez v0, :cond_15

    .line 382
    if-eqz p1, :cond_13

    const-string v3, "1"

    goto :goto_4

    :cond_13
    const-string v3, "0"

    .line 383
    .local v3, "flage":Ljava/lang/String;
    :goto_4
    invoke-static {v3}, Lcom/android/server/location/Order;->setFlag(Ljava/lang/String;)V

    .line 385
    const-string v4, "GnssMockLocationOpt"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mode switch to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 386
    const-string v4, "1"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_14

    .line 387
    sput-boolean v1, Lcom/android/server/location/GnssMockLocationOptImpl;->sMockFlagSetByUser:Z

    .line 388
    invoke-direct {p0, p2, p3}, Lcom/android/server/location/GnssMockLocationOptImpl;->isMockInfluence(Ljava/lang/String;Lcom/android/server/location/provider/MockableLocationProvider;)Z

    .line 390
    :cond_14
    const-string v4, "0"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_15

    .line 391
    sput-boolean v2, Lcom/android/server/location/GnssMockLocationOptImpl;->sMockFlagSetByUser:Z

    .line 394
    .end local v3    # "flage":Ljava/lang/String;
    :cond_15
    sput-boolean p1, Lcom/android/server/location/GnssMockLocationOptImpl;->sLastFlag:Z

    .line 395
    return v1
.end method

.method public setMockLocationOptStatus(Z)V
    .locals 2
    .param p1, "status"    # Z

    .line 676
    const-string v0, "GnssMockLocationOpt"

    if-eqz p1, :cond_0

    .line 677
    sget-object v1, Lcom/android/server/location/GnssMockLocationOptImpl;->mContext:Landroid/content/Context;

    invoke-direct {p0, v1, p1}, Lcom/android/server/location/GnssMockLocationOptImpl;->saveCloudDataToSP(Landroid/content/Context;Z)V

    .line 678
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mEnableGmoControlStatus:Z

    .line 679
    const-string v1, "enable GMO by cloud"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 681
    :cond_0
    sget-object v1, Lcom/android/server/location/GnssMockLocationOptImpl;->mContext:Landroid/content/Context;

    invoke-direct {p0, v1, p1}, Lcom/android/server/location/GnssMockLocationOptImpl;->saveCloudDataToSP(Landroid/content/Context;Z)V

    .line 682
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mEnableGmoControlStatus:Z

    .line 683
    const-string v1, "disable GMO by cloud"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 685
    :goto_0
    return-void
.end method
