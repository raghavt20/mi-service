.class public Lcom/android/server/location/EnCode;
.super Ljava/lang/Object;
.source "EnCode.java"


# static fields
.field private static mkeyRandom:[B

.field private static moutPutString:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/location/EnCode;->mkeyRandom:[B

    .line 21
    sput-object v0, Lcom/android/server/location/EnCode;->moutPutString:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final encBytes([B[B[B)[B
    .locals 4
    .param p0, "srcBytes"    # [B
    .param p1, "key"    # [B
    .param p2, "newIv"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 52
    const-string v0, "AES/CBC/PKCS5Padding"

    invoke-static {v0}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 53
    .local v0, "cipher":Ljavax/crypto/Cipher;
    new-instance v1, Ljavax/crypto/spec/SecretKeySpec;

    const-string v2, "AES"

    invoke-direct {v1, p1, v2}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 54
    .local v1, "skeySpec":Ljavax/crypto/spec/SecretKeySpec;
    new-instance v2, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v2, p2}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 55
    .local v2, "iv":Ljavax/crypto/spec/IvParameterSpec;
    const/4 v3, 0x1

    invoke-virtual {v0, v3, v1, v2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 56
    invoke-virtual {v0, p0}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v3

    .line 57
    .local v3, "encrypted":[B
    return-object v3
.end method

.method public static final encText(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "sSrc"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 62
    const-string v0, ""

    const/4 v1, 0x0

    .line 63
    .local v1, "outPut":Ljava/lang/String;
    const/4 v2, 0x0

    .line 64
    .local v2, "outPutTxt":Ljava/lang/String;
    const/16 v3, 0x10

    new-array v3, v3, [B

    fill-array-data v3, :array_0

    .line 66
    .local v3, "ivk":[B
    :try_start_0
    const-string v4, "UTF-8"

    invoke-virtual {p0, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v4

    .line 67
    .local v4, "srcBytes":[B
    sget-object v5, Lcom/android/server/location/EnCode;->mkeyRandom:[B

    if-nez v5, :cond_0

    .line 68
    const/4 v0, 0x0

    return-object v0

    .line 70
    :cond_0
    invoke-static {v4, v5, v3}, Lcom/android/server/location/EnCode;->encBytes([B[B[B)[B

    move-result-object v5

    .line 71
    .local v5, "encryptedTxt":[B
    const/4 v6, 0x0

    invoke-static {v5, v6}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v6

    move-object v2, v6

    .line 72
    const-string v6, "\n"

    invoke-virtual {v2, v6, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "\r"

    invoke-virtual {v6, v7, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 77
    .end local v2    # "outPutTxt":Ljava/lang/String;
    .end local v4    # "srcBytes":[B
    .end local v5    # "encryptedTxt":[B
    .local v0, "outPutTxt":Ljava/lang/String;
    goto :goto_0

    .line 75
    .end local v0    # "outPutTxt":Ljava/lang/String;
    .restart local v2    # "outPutTxt":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 76
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v2

    goto :goto_0

    .line 73
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 74
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 77
    .end local v0    # "e":Ljava/io/UnsupportedEncodingException;
    move-object v0, v2

    .line 79
    .end local v2    # "outPutTxt":Ljava/lang/String;
    .local v0, "outPutTxt":Ljava/lang/String;
    :goto_0
    return-object v0

    :array_0
    .array-data 1
        0x30t
        0x30t
        0x30t
        0x30t
        0x30t
        0x30t
        0x30t
        0x30t
        0x30t
        0x30t
        0x30t
        0x30t
        0x30t
        0x30t
        0x30t
        0x30t
    .end array-data
.end method

.method public static final initRandom()Ljava/lang/String;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 24
    const-string v0, ""

    sget-object v1, Lcom/android/server/location/EnCode;->mkeyRandom:[B

    if-nez v1, :cond_1

    .line 25
    const/4 v1, 0x0

    .line 26
    .local v1, "outPutRandom":Ljava/lang/String;
    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    .line 27
    .local v2, "ranDom":Ljava/util/Random;
    const/16 v3, 0x10

    new-array v4, v3, [B

    .line 29
    .local v4, "keyRandom":[B
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-ge v5, v3, :cond_0

    .line 30
    const/16 v6, 0x14

    invoke-virtual {v2, v6}, Ljava/util/Random;->nextInt(I)I

    move-result v6

    .line 31
    .local v6, "number":I
    add-int/lit8 v7, v6, 0x61

    int-to-byte v7, v7

    aput-byte v7, v4, v5

    .line 29
    .end local v6    # "number":I
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 34
    .end local v5    # "i":I
    :cond_0
    sput-object v4, Lcom/android/server/location/EnCode;->mkeyRandom:[B

    .line 37
    :try_start_0
    invoke-static {v4}, Lcom/android/server/location/RsaUtil;->encryptByPublicKey([B)[B

    move-result-object v3

    .line 38
    .local v3, "encryptedRandom":[B
    const/4 v5, 0x0

    invoke-static {v3, v5}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v5

    move-object v1, v5

    .line 39
    const-string v5, "\n"

    invoke-virtual {v1, v5, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "\r"

    invoke-virtual {v5, v6, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 40
    sput-object v1, Lcom/android/server/location/EnCode;->moutPutString:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    .end local v3    # "encryptedRandom":[B
    goto :goto_1

    .line 41
    :catch_0
    move-exception v0

    .line 42
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 44
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "outPutRandom":Ljava/lang/String;
    .end local v2    # "ranDom":Ljava/util/Random;
    .end local v4    # "keyRandom":[B
    :goto_1
    goto :goto_2

    .line 45
    :cond_1
    const-string/jumbo v0, "test"

    const-string v1, "mkeyRandom init done  here\n"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    :goto_2
    sget-object v0, Lcom/android/server/location/EnCode;->moutPutString:Ljava/lang/String;

    return-object v0
.end method
