.class public Lcom/android/server/location/MiuiBlurLocationManagerImpl;
.super Lcom/android/server/location/MiuiBlurLocationManagerStub;
.source "MiuiBlurLocationManagerImpl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final KEY_CALLBACK:Ljava/lang/String; = "key_callback"

.field private static final KEY_CID:Ljava/lang/String; = "key_cid"

.field private static final KEY_LAC:Ljava/lang/String; = "key_lac"

.field private static final KEY_LAST_BLUR_LATITUDE:Ljava/lang/String; = "key_last_blur_latitude"

.field private static final KEY_LAST_BLUR_LONGITUDE:Ljava/lang/String; = "key_last_blur_longitude"

.field private static final KEY_LATITUDE:Ljava/lang/String; = "key_latitude"

.field private static final KEY_LONGITUDE:Ljava/lang/String; = "key_longitude"

.field private static final KEY_MCC:Ljava/lang/String; = "key_mcc"

.field private static final KEY_MNC:Ljava/lang/String; = "key_mnc"

.field private static final MSG_BLUR_LOCATION:I = 0x29cc2

.field private static final MSG_UPDATE_CELL_INFO:I = 0x1

.field private static final MSG_UPDATE_SATELLITE:I = 0x2

.field private static final MSG_UPDATE_SATELLITE_SILENT:I = 0x3

.field private static final TAG:Ljava/lang/String; = "MiuiBlurLocationManager"


# instance fields
.field private mAppOps:Landroid/app/AppOpsManager;

.field private final mBlurGpsHandler:Landroid/os/Handler;

.field private volatile mBlurLocationManager:Lcom/android/internal/app/ILocationBlurry;

.field private final mBlurMessenger:Landroid/os/Messenger;

.field private mContext:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;

.field private volatile mLastBlurLocation:Landroid/location/Location;

.field private mLocationService:Lcom/android/server/location/LocationManagerService;

.field private volatile mStashBlurLocationInfo:Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;

.field private mSvStatusData:Lmiui/security/SvStatusData;


# direct methods
.method static bridge synthetic -$$Nest$fgetmStashBlurLocationInfo(Lcom/android/server/location/MiuiBlurLocationManagerImpl;)Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;
    .locals 0

    iget-object p0, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mStashBlurLocationInfo:Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmSvStatusData(Lcom/android/server/location/MiuiBlurLocationManagerImpl;Lmiui/security/SvStatusData;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mSvStatusData:Lmiui/security/SvStatusData;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 59
    invoke-direct {p0}, Lcom/android/server/location/MiuiBlurLocationManagerStub;-><init>()V

    .line 83
    new-instance v0, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;-><init>(Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation-IA;)V

    iput-object v0, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mStashBlurLocationInfo:Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;

    .line 86
    new-instance v0, Lcom/android/server/location/MiuiBlurLocationManagerImpl$1;

    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->get()Lcom/android/internal/os/BackgroundThread;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/os/BackgroundThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/server/location/MiuiBlurLocationManagerImpl$1;-><init>(Lcom/android/server/location/MiuiBlurLocationManagerImpl;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mHandler:Landroid/os/Handler;

    .line 119
    new-instance v1, Landroid/os/Messenger;

    invoke-direct {v1, v0}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mBlurMessenger:Landroid/os/Messenger;

    .line 471
    new-instance v0, Lcom/android/server/location/MiuiBlurLocationManagerImpl$2;

    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->get()Lcom/android/internal/os/BackgroundThread;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/os/BackgroundThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/server/location/MiuiBlurLocationManagerImpl$2;-><init>(Lcom/android/server/location/MiuiBlurLocationManagerImpl;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mBlurGpsHandler:Landroid/os/Handler;

    return-void
.end method

.method private getAppOps()Landroid/app/AppOpsManager;
    .locals 2

    .line 351
    iget-object v0, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mAppOps:Landroid/app/AppOpsManager;

    if-nez v0, :cond_0

    .line 352
    iget-object v0, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mContext:Landroid/content/Context;

    const-string v1, "appops"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AppOpsManager;

    iput-object v0, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mAppOps:Landroid/app/AppOpsManager;

    .line 354
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mAppOps:Landroid/app/AppOpsManager;

    return-object v0
.end method

.method private getS(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 1
    .param p1, "charSequence"    # Ljava/lang/CharSequence;

    .line 170
    if-nez p1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method private initBlurLocationData()V
    .locals 5

    .line 135
    iget-object v0, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mBlurLocationManager:Lcom/android/internal/app/ILocationBlurry;

    const-string v1, "MiuiBlurLocationManager"

    if-nez v0, :cond_0

    .line 136
    const-string v0, "ILocationBlurry has not been register yet"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    return-void

    .line 139
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mHandler:Landroid/os/Handler;

    const v2, 0x29cc2

    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 140
    const-string v0, "blurLocationData return for too frequently."

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    return-void

    .line 143
    :cond_1
    iget-object v0, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mHandler:Landroid/os/Handler;

    const-wide/32 v3, 0x493e0

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 145
    :try_start_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 146
    .local v0, "input":Landroid/os/Bundle;
    const-string v2, "key_callback"

    iget-object v3, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mBlurMessenger:Landroid/os/Messenger;

    invoke-virtual {v3}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBinder(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 147
    iget-object v2, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mBlurLocationManager:Lcom/android/internal/app/ILocationBlurry;

    invoke-interface {v2, v0}, Lcom/android/internal/app/ILocationBlurry;->pushBlurryCellLocation(Landroid/os/Bundle;)V

    .line 148
    const-string v2, "MIUILOG- getBlurryCellLocation now"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 151
    nop

    .end local v0    # "input":Landroid/os/Bundle;
    goto :goto_0

    .line 149
    :catch_0
    move-exception v0

    .line 150
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "MIUILOG- getBlurryCellLocation exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 152
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private logIfDebug(Ljava/lang/String;)V
    .locals 0
    .param p1, "info"    # Ljava/lang/String;

    .line 359
    return-void
.end method

.method private noteOpUseMyIdentity(ILjava/lang/String;)Z
    .locals 8
    .param p1, "uid"    # I
    .param p2, "pkgName"    # Ljava/lang/String;

    .line 371
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 373
    .local v0, "identity":J
    :try_start_0
    invoke-direct {p0}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->getAppOps()Landroid/app/AppOpsManager;

    move-result-object v2

    const/16 v3, 0x2734

    const-string v6, "MiuiBlueLocationManager#noteOpUseMyIdentity"

    const/4 v7, 0x0

    move v4, p1

    move-object v5, p2

    invoke-virtual/range {v2 .. v7}, Landroid/app/AppOpsManager;->noteOpNoThrow(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 376
    :goto_0
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 373
    return v2

    .line 376
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 377
    throw v2
.end method


# virtual methods
.method public getBlurryCellInfos(Ljava/util/List;)Ljava/util/List;
    .locals 31
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/telephony/CellInfo;",
            ">;)",
            "Ljava/util/List<",
            "Landroid/telephony/CellInfo;",
            ">;"
        }
    .end annotation

    .line 245
    .local p1, "cellInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/telephony/CellInfo;>;"
    move-object/from16 v0, p0

    sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v1, :cond_7

    if-eqz p1, :cond_7

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, v0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mBlurLocationManager:Lcom/android/internal/app/ILocationBlurry;

    if-eqz v1, :cond_7

    .line 246
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->initBlurLocationReady()Z

    move-result v1

    if-nez v1, :cond_0

    goto/16 :goto_2

    .line 250
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 251
    .local v1, "results":Ljava/util/List;, "Ljava/util/List<Landroid/telephony/CellInfo;>;"
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telephony/CellInfo;

    .line 252
    .local v3, "info":Landroid/telephony/CellInfo;
    instance-of v4, v3, Landroid/telephony/CellInfoGsm;

    if-eqz v4, :cond_1

    .line 253
    move-object v4, v3

    check-cast v4, Landroid/telephony/CellInfoGsm;

    invoke-virtual {v4}, Landroid/telephony/CellInfoGsm;->getCellIdentity()Landroid/telephony/CellIdentityGsm;

    move-result-object v4

    .line 254
    .local v4, "identityGsm":Landroid/telephony/CellIdentityGsm;
    new-instance v5, Landroid/telephony/CellInfoGsm;

    move-object v6, v3

    check-cast v6, Landroid/telephony/CellInfoGsm;

    invoke-direct {v5, v6}, Landroid/telephony/CellInfoGsm;-><init>(Landroid/telephony/CellInfoGsm;)V

    .line 256
    .local v5, "result":Landroid/telephony/CellInfoGsm;
    new-instance v15, Landroid/telephony/CellIdentityGsm;

    iget-object v6, v0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mStashBlurLocationInfo:Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;

    invoke-static {v6}, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->-$$Nest$fgetlac(Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;)I

    move-result v7

    iget-object v6, v0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mStashBlurLocationInfo:Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;

    invoke-static {v6}, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->-$$Nest$fgetcid(Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;)I

    move-result v8

    .line 258
    invoke-virtual {v4}, Landroid/telephony/CellIdentityGsm;->getArfcn()I

    move-result v9

    invoke-virtual {v4}, Landroid/telephony/CellIdentityGsm;->getBsic()I

    move-result v10

    invoke-virtual {v4}, Landroid/telephony/CellIdentityGsm;->getMccString()Ljava/lang/String;

    move-result-object v11

    .line 259
    invoke-virtual {v4}, Landroid/telephony/CellIdentityGsm;->getMncString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v4}, Landroid/telephony/CellIdentityGsm;->getOperatorAlphaLong()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-direct {v0, v6}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->getS(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v13

    .line 260
    invoke-virtual {v4}, Landroid/telephony/CellIdentityGsm;->getOperatorAlphaShort()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-direct {v0, v6}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->getS(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v4}, Landroid/telephony/CellIdentityGsm;->getAdditionalPlmns()Ljava/util/Set;

    move-result-object v16

    move-object v6, v15

    move-object/from16 v17, v2

    move-object v2, v15

    move-object/from16 v15, v16

    invoke-direct/range {v6 .. v15}, Landroid/telephony/CellIdentityGsm;-><init>(IIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;)V

    .line 256
    invoke-virtual {v5, v2}, Landroid/telephony/CellInfoGsm;->setCellIdentity(Landroid/telephony/CellIdentityGsm;)V

    .line 261
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 263
    .end local v4    # "identityGsm":Landroid/telephony/CellIdentityGsm;
    .end local v5    # "result":Landroid/telephony/CellInfoGsm;
    goto/16 :goto_1

    :cond_1
    move-object/from16 v17, v2

    instance-of v2, v3, Landroid/telephony/CellInfoCdma;

    if-eqz v2, :cond_2

    .line 264
    move-object v2, v3

    check-cast v2, Landroid/telephony/CellInfoCdma;

    invoke-virtual {v2}, Landroid/telephony/CellInfoCdma;->getCellIdentity()Landroid/telephony/CellIdentityCdma;

    move-result-object v2

    .line 265
    .local v2, "identityCdma":Landroid/telephony/CellIdentityCdma;
    new-instance v4, Landroid/telephony/CellInfoCdma;

    move-object v5, v3

    check-cast v5, Landroid/telephony/CellInfoCdma;

    invoke-direct {v4, v5}, Landroid/telephony/CellInfoCdma;-><init>(Landroid/telephony/CellInfoCdma;)V

    .line 267
    .local v4, "result":Landroid/telephony/CellInfoCdma;
    new-instance v13, Landroid/telephony/CellIdentityCdma;

    iget-object v5, v0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mStashBlurLocationInfo:Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;

    invoke-static {v5}, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->-$$Nest$fgetlac(Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;)I

    move-result v6

    .line 268
    invoke-virtual {v2}, Landroid/telephony/CellIdentityCdma;->getSystemId()I

    move-result v7

    iget-object v5, v0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mStashBlurLocationInfo:Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;

    invoke-static {v5}, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->-$$Nest$fgetcid(Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;)I

    move-result v8

    .line 270
    invoke-virtual {v2}, Landroid/telephony/CellIdentityCdma;->getLongitude()I

    move-result v9

    invoke-virtual {v2}, Landroid/telephony/CellIdentityCdma;->getLatitude()I

    move-result v10

    .line 271
    invoke-virtual {v2}, Landroid/telephony/CellIdentityCdma;->getOperatorAlphaLong()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-direct {v0, v5}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->getS(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v11

    .line 272
    invoke-virtual {v2}, Landroid/telephony/CellIdentityCdma;->getOperatorAlphaShort()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-direct {v0, v5}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->getS(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v12

    move-object v5, v13

    invoke-direct/range {v5 .. v12}, Landroid/telephony/CellIdentityCdma;-><init>(IIIIILjava/lang/String;Ljava/lang/String;)V

    .line 267
    invoke-virtual {v4, v13}, Landroid/telephony/CellInfoCdma;->setCellIdentity(Landroid/telephony/CellIdentityCdma;)V

    .line 273
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 274
    .end local v2    # "identityCdma":Landroid/telephony/CellIdentityCdma;
    .end local v4    # "result":Landroid/telephony/CellInfoCdma;
    goto/16 :goto_1

    :cond_2
    instance-of v2, v3, Landroid/telephony/CellInfoLte;

    if-eqz v2, :cond_3

    .line 275
    move-object v2, v3

    check-cast v2, Landroid/telephony/CellInfoLte;

    invoke-virtual {v2}, Landroid/telephony/CellInfoLte;->getCellIdentity()Landroid/telephony/CellIdentityLte;

    move-result-object v2

    .line 276
    .local v2, "identityLte":Landroid/telephony/CellIdentityLte;
    new-instance v4, Landroid/telephony/CellInfoLte;

    move-object v5, v3

    check-cast v5, Landroid/telephony/CellInfoLte;

    invoke-direct {v4, v5}, Landroid/telephony/CellInfoLte;-><init>(Landroid/telephony/CellInfoLte;)V

    .line 278
    .local v4, "result":Landroid/telephony/CellInfoLte;
    new-instance v5, Landroid/telephony/CellIdentityLte;

    iget-object v6, v0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mStashBlurLocationInfo:Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;

    invoke-static {v6}, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->-$$Nest$fgetcid(Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;)I

    move-result v19

    .line 279
    invoke-virtual {v2}, Landroid/telephony/CellIdentityLte;->getPci()I

    move-result v20

    iget-object v6, v0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mStashBlurLocationInfo:Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;

    invoke-static {v6}, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->-$$Nest$fgetlac(Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;)I

    move-result v21

    .line 280
    invoke-virtual {v2}, Landroid/telephony/CellIdentityLte;->getEarfcn()I

    move-result v22

    .line 281
    invoke-virtual {v2}, Landroid/telephony/CellIdentityLte;->getBands()[I

    move-result-object v23

    invoke-virtual {v2}, Landroid/telephony/CellIdentityLte;->getBandwidth()I

    move-result v24

    invoke-virtual {v2}, Landroid/telephony/CellIdentityLte;->getMccString()Ljava/lang/String;

    move-result-object v25

    .line 282
    invoke-virtual {v2}, Landroid/telephony/CellIdentityLte;->getMncString()Ljava/lang/String;

    move-result-object v26

    invoke-virtual {v2}, Landroid/telephony/CellIdentityLte;->getOperatorAlphaLong()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-direct {v0, v6}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->getS(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v27

    .line 283
    invoke-virtual {v2}, Landroid/telephony/CellIdentityLte;->getOperatorAlphaShort()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-direct {v0, v6}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->getS(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v28

    invoke-virtual {v2}, Landroid/telephony/CellIdentityLte;->getAdditionalPlmns()Ljava/util/Set;

    move-result-object v29

    .line 284
    invoke-virtual {v2}, Landroid/telephony/CellIdentityLte;->getClosedSubscriberGroupInfo()Landroid/telephony/ClosedSubscriberGroupInfo;

    move-result-object v30

    move-object/from16 v18, v5

    invoke-direct/range {v18 .. v30}, Landroid/telephony/CellIdentityLte;-><init>(IIII[IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;Landroid/telephony/ClosedSubscriberGroupInfo;)V

    .line 278
    invoke-virtual {v4, v5}, Landroid/telephony/CellInfoLte;->setCellIdentity(Landroid/telephony/CellIdentityLte;)V

    .line 285
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 286
    .end local v2    # "identityLte":Landroid/telephony/CellIdentityLte;
    .end local v4    # "result":Landroid/telephony/CellInfoLte;
    goto/16 :goto_1

    :cond_3
    instance-of v2, v3, Landroid/telephony/CellInfoWcdma;

    if-eqz v2, :cond_4

    .line 287
    move-object v2, v3

    check-cast v2, Landroid/telephony/CellInfoWcdma;

    invoke-virtual {v2}, Landroid/telephony/CellInfoWcdma;->getCellIdentity()Landroid/telephony/CellIdentityWcdma;

    move-result-object v2

    .line 288
    .local v2, "identityWcdma":Landroid/telephony/CellIdentityWcdma;
    new-instance v4, Landroid/telephony/CellInfoWcdma;

    move-object v5, v3

    check-cast v5, Landroid/telephony/CellInfoWcdma;

    invoke-direct {v4, v5}, Landroid/telephony/CellInfoWcdma;-><init>(Landroid/telephony/CellInfoWcdma;)V

    .line 289
    .local v4, "result":Landroid/telephony/CellInfoWcdma;
    new-instance v15, Landroid/telephony/CellIdentityWcdma;

    iget-object v5, v0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mStashBlurLocationInfo:Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;

    invoke-static {v5}, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->-$$Nest$fgetlac(Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;)I

    move-result v6

    iget-object v5, v0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mStashBlurLocationInfo:Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;

    invoke-static {v5}, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->-$$Nest$fgetcid(Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;)I

    move-result v7

    .line 291
    invoke-virtual {v2}, Landroid/telephony/CellIdentityWcdma;->getPsc()I

    move-result v8

    invoke-virtual {v2}, Landroid/telephony/CellIdentityWcdma;->getUarfcn()I

    move-result v9

    .line 292
    invoke-virtual {v2}, Landroid/telephony/CellIdentityWcdma;->getMccString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2}, Landroid/telephony/CellIdentityWcdma;->getMncString()Ljava/lang/String;

    move-result-object v11

    .line 293
    invoke-virtual {v2}, Landroid/telephony/CellIdentityWcdma;->getOperatorAlphaLong()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-direct {v0, v5}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->getS(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v12

    .line 294
    invoke-virtual {v2}, Landroid/telephony/CellIdentityWcdma;->getOperatorAlphaShort()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-direct {v0, v5}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->getS(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v13

    .line 295
    invoke-virtual {v2}, Landroid/telephony/CellIdentityWcdma;->getAdditionalPlmns()Ljava/util/Set;

    move-result-object v14

    .line 296
    invoke-virtual {v2}, Landroid/telephony/CellIdentityWcdma;->getClosedSubscriberGroupInfo()Landroid/telephony/ClosedSubscriberGroupInfo;

    move-result-object v16

    move-object v5, v15

    move-object/from16 v18, v2

    move-object v2, v15

    .end local v2    # "identityWcdma":Landroid/telephony/CellIdentityWcdma;
    .local v18, "identityWcdma":Landroid/telephony/CellIdentityWcdma;
    move-object/from16 v15, v16

    invoke-direct/range {v5 .. v15}, Landroid/telephony/CellIdentityWcdma;-><init>(IIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;Landroid/telephony/ClosedSubscriberGroupInfo;)V

    .line 289
    invoke-virtual {v4, v2}, Landroid/telephony/CellInfoWcdma;->setCellIdentity(Landroid/telephony/CellIdentityWcdma;)V

    .line 297
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 298
    .end local v4    # "result":Landroid/telephony/CellInfoWcdma;
    .end local v18    # "identityWcdma":Landroid/telephony/CellIdentityWcdma;
    goto :goto_1

    :cond_4
    instance-of v2, v3, Landroid/telephony/CellInfoNr;

    if-eqz v2, :cond_5

    .line 299
    move-object v2, v3

    check-cast v2, Landroid/telephony/CellInfoNr;

    .line 300
    .local v2, "infoNr":Landroid/telephony/CellInfoNr;
    invoke-virtual {v2}, Landroid/telephony/CellInfoNr;->getCellIdentity()Landroid/telephony/CellIdentity;

    move-result-object v4

    check-cast v4, Landroid/telephony/CellIdentityNr;

    .line 301
    .local v4, "identityNr":Landroid/telephony/CellIdentityNr;
    new-instance v18, Landroid/telephony/CellIdentityNr;

    iget-object v5, v0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mStashBlurLocationInfo:Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;

    invoke-static {v5}, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->-$$Nest$fgetcid(Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;)I

    move-result v6

    iget-object v5, v0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mStashBlurLocationInfo:Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;

    invoke-static {v5}, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->-$$Nest$fgetlac(Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;)I

    move-result v7

    .line 302
    invoke-virtual {v4}, Landroid/telephony/CellIdentityNr;->getNrarfcn()I

    move-result v8

    invoke-virtual {v4}, Landroid/telephony/CellIdentityNr;->getBands()[I

    move-result-object v9

    invoke-virtual {v4}, Landroid/telephony/CellIdentityNr;->getMccString()Ljava/lang/String;

    move-result-object v10

    .line 303
    invoke-virtual {v4}, Landroid/telephony/CellIdentityNr;->getMncString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v4}, Landroid/telephony/CellIdentityNr;->getNci()J

    move-result-wide v12

    invoke-virtual {v4}, Landroid/telephony/CellIdentityNr;->getOperatorAlphaLong()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-direct {v0, v5}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->getS(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v14

    .line 304
    invoke-virtual {v4}, Landroid/telephony/CellIdentityNr;->getOperatorAlphaShort()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-direct {v0, v5}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->getS(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v4}, Landroid/telephony/CellIdentityNr;->getAdditionalPlmns()Ljava/util/Set;

    move-result-object v16

    move-object/from16 v5, v18

    invoke-direct/range {v5 .. v16}, Landroid/telephony/CellIdentityNr;-><init>(III[ILjava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/util/Collection;)V

    move-object/from16 v10, v18

    .line 305
    .local v10, "resultNr":Landroid/telephony/CellIdentityNr;
    new-instance v12, Landroid/telephony/CellInfoNr;

    invoke-virtual {v2}, Landroid/telephony/CellInfoNr;->getCellConnectionStatus()I

    move-result v6

    .line 306
    invoke-virtual {v2}, Landroid/telephony/CellInfoNr;->isRegistered()Z

    move-result v7

    invoke-virtual {v2}, Landroid/telephony/CellInfoNr;->getTimeStamp()J

    move-result-wide v8

    .line 307
    invoke-virtual {v2}, Landroid/telephony/CellInfoNr;->getCellSignalStrength()Landroid/telephony/CellSignalStrength;

    move-result-object v5

    move-object v11, v5

    check-cast v11, Landroid/telephony/CellSignalStrengthNr;

    move-object v5, v12

    invoke-direct/range {v5 .. v11}, Landroid/telephony/CellInfoNr;-><init>(IZJLandroid/telephony/CellIdentityNr;Landroid/telephony/CellSignalStrengthNr;)V

    .line 308
    .local v5, "result":Landroid/telephony/CellInfoNr;
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 309
    .end local v2    # "infoNr":Landroid/telephony/CellInfoNr;
    .end local v4    # "identityNr":Landroid/telephony/CellIdentityNr;
    .end local v5    # "result":Landroid/telephony/CellInfoNr;
    .end local v10    # "resultNr":Landroid/telephony/CellIdentityNr;
    goto :goto_1

    .line 310
    :cond_5
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 312
    .end local v3    # "info":Landroid/telephony/CellInfo;
    :goto_1
    move-object/from16 v2, v17

    goto/16 :goto_0

    .line 313
    :cond_6
    return-object v1

    .line 247
    .end local v1    # "results":Ljava/util/List;, "Ljava/util/List<Landroid/telephony/CellInfo;>;"
    :cond_7
    :goto_2
    return-object p1
.end method

.method public getBlurryCellInfos(Ljava/util/List;ILjava/lang/String;)Ljava/util/List;
    .locals 1
    .param p2, "uid"    # I
    .param p3, "pkgName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/telephony/CellInfo;",
            ">;I",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Landroid/telephony/CellInfo;",
            ">;"
        }
    .end annotation

    .line 318
    .local p1, "location":Ljava/util/List;, "Ljava/util/List<Landroid/telephony/CellInfo;>;"
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, p2, p3}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->isBlurLocationMode(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 321
    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->getBlurryCellInfos(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 319
    :cond_1
    :goto_0
    return-object p1
.end method

.method public getBlurryCellLocation(Landroid/telephony/CellIdentity;)Landroid/telephony/CellIdentity;
    .locals 17
    .param p1, "location"    # Landroid/telephony/CellIdentity;

    .line 187
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    sget-boolean v2, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v2, :cond_6

    if-eqz v1, :cond_6

    iget-object v2, v0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mBlurLocationManager:Lcom/android/internal/app/ILocationBlurry;

    if-eqz v2, :cond_6

    invoke-virtual/range {p0 .. p0}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->initBlurLocationReady()Z

    move-result v2

    if-nez v2, :cond_0

    goto/16 :goto_0

    .line 190
    :cond_0
    instance-of v2, v1, Landroid/telephony/CellIdentityGsm;

    if-eqz v2, :cond_1

    .line 191
    move-object v2, v1

    check-cast v2, Landroid/telephony/CellIdentityGsm;

    .line 192
    .local v2, "identityGsm":Landroid/telephony/CellIdentityGsm;
    new-instance v13, Landroid/telephony/CellIdentityGsm;

    iget-object v3, v0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mStashBlurLocationInfo:Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;

    invoke-static {v3}, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->-$$Nest$fgetlac(Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;)I

    move-result v4

    iget-object v3, v0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mStashBlurLocationInfo:Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;

    invoke-static {v3}, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->-$$Nest$fgetcid(Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;)I

    move-result v5

    .line 194
    invoke-virtual {v2}, Landroid/telephony/CellIdentityGsm;->getArfcn()I

    move-result v6

    invoke-virtual {v2}, Landroid/telephony/CellIdentityGsm;->getBsic()I

    move-result v7

    invoke-virtual {v2}, Landroid/telephony/CellIdentityGsm;->getMccString()Ljava/lang/String;

    move-result-object v8

    .line 195
    invoke-virtual {v2}, Landroid/telephony/CellIdentityGsm;->getMncString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2}, Landroid/telephony/CellIdentityGsm;->getOperatorAlphaLong()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->getS(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    .line 196
    invoke-virtual {v2}, Landroid/telephony/CellIdentityGsm;->getOperatorAlphaShort()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->getS(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2}, Landroid/telephony/CellIdentityGsm;->getAdditionalPlmns()Ljava/util/Set;

    move-result-object v12

    move-object v3, v13

    invoke-direct/range {v3 .. v12}, Landroid/telephony/CellIdentityGsm;-><init>(IIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;)V

    .line 192
    return-object v13

    .line 197
    .end local v2    # "identityGsm":Landroid/telephony/CellIdentityGsm;
    :cond_1
    instance-of v2, v1, Landroid/telephony/CellIdentityCdma;

    if-eqz v2, :cond_2

    .line 198
    move-object v2, v1

    check-cast v2, Landroid/telephony/CellIdentityCdma;

    .line 199
    .local v2, "identityCdma":Landroid/telephony/CellIdentityCdma;
    new-instance v11, Landroid/telephony/CellIdentityCdma;

    iget-object v3, v0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mStashBlurLocationInfo:Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;

    invoke-static {v3}, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->-$$Nest$fgetlac(Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;)I

    move-result v4

    .line 200
    invoke-virtual {v2}, Landroid/telephony/CellIdentityCdma;->getSystemId()I

    move-result v5

    iget-object v3, v0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mStashBlurLocationInfo:Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;

    invoke-static {v3}, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->-$$Nest$fgetcid(Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;)I

    move-result v6

    .line 202
    invoke-virtual {v2}, Landroid/telephony/CellIdentityCdma;->getLongitude()I

    move-result v7

    invoke-virtual {v2}, Landroid/telephony/CellIdentityCdma;->getLatitude()I

    move-result v8

    .line 203
    invoke-virtual {v2}, Landroid/telephony/CellIdentityCdma;->getOperatorAlphaLong()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->getS(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v9

    .line 204
    invoke-virtual {v2}, Landroid/telephony/CellIdentityCdma;->getOperatorAlphaShort()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->getS(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    move-object v3, v11

    invoke-direct/range {v3 .. v10}, Landroid/telephony/CellIdentityCdma;-><init>(IIIIILjava/lang/String;Ljava/lang/String;)V

    .line 199
    return-object v11

    .line 205
    .end local v2    # "identityCdma":Landroid/telephony/CellIdentityCdma;
    :cond_2
    instance-of v2, v1, Landroid/telephony/CellIdentityLte;

    if-eqz v2, :cond_3

    .line 206
    move-object v2, v1

    check-cast v2, Landroid/telephony/CellIdentityLte;

    .line 207
    .local v2, "identityLte":Landroid/telephony/CellIdentityLte;
    new-instance v16, Landroid/telephony/CellIdentityLte;

    iget-object v3, v0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mStashBlurLocationInfo:Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;

    invoke-static {v3}, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->-$$Nest$fgetcid(Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;)I

    move-result v4

    .line 208
    invoke-virtual {v2}, Landroid/telephony/CellIdentityLte;->getPci()I

    move-result v5

    iget-object v3, v0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mStashBlurLocationInfo:Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;

    invoke-static {v3}, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->-$$Nest$fgetlac(Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;)I

    move-result v6

    .line 209
    invoke-virtual {v2}, Landroid/telephony/CellIdentityLte;->getEarfcn()I

    move-result v7

    .line 210
    invoke-virtual {v2}, Landroid/telephony/CellIdentityLte;->getBands()[I

    move-result-object v8

    invoke-virtual {v2}, Landroid/telephony/CellIdentityLte;->getBandwidth()I

    move-result v9

    invoke-virtual {v2}, Landroid/telephony/CellIdentityLte;->getMccString()Ljava/lang/String;

    move-result-object v10

    .line 211
    invoke-virtual {v2}, Landroid/telephony/CellIdentityLte;->getMncString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2}, Landroid/telephony/CellIdentityLte;->getOperatorAlphaLong()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->getS(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v12

    .line 212
    invoke-virtual {v2}, Landroid/telephony/CellIdentityLte;->getOperatorAlphaShort()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->getS(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2}, Landroid/telephony/CellIdentityLte;->getAdditionalPlmns()Ljava/util/Set;

    move-result-object v14

    .line 213
    invoke-virtual {v2}, Landroid/telephony/CellIdentityLte;->getClosedSubscriberGroupInfo()Landroid/telephony/ClosedSubscriberGroupInfo;

    move-result-object v15

    move-object/from16 v3, v16

    invoke-direct/range {v3 .. v15}, Landroid/telephony/CellIdentityLte;-><init>(IIII[IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;Landroid/telephony/ClosedSubscriberGroupInfo;)V

    .line 207
    return-object v16

    .line 214
    .end local v2    # "identityLte":Landroid/telephony/CellIdentityLte;
    :cond_3
    instance-of v2, v1, Landroid/telephony/CellIdentityWcdma;

    if-eqz v2, :cond_4

    .line 215
    move-object v2, v1

    check-cast v2, Landroid/telephony/CellIdentityWcdma;

    .line 216
    .local v2, "identityWcdma":Landroid/telephony/CellIdentityWcdma;
    new-instance v14, Landroid/telephony/CellIdentityWcdma;

    iget-object v3, v0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mStashBlurLocationInfo:Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;

    invoke-static {v3}, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->-$$Nest$fgetlac(Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;)I

    move-result v4

    iget-object v3, v0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mStashBlurLocationInfo:Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;

    invoke-static {v3}, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->-$$Nest$fgetcid(Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;)I

    move-result v5

    .line 218
    invoke-virtual {v2}, Landroid/telephony/CellIdentityWcdma;->getPsc()I

    move-result v6

    invoke-virtual {v2}, Landroid/telephony/CellIdentityWcdma;->getUarfcn()I

    move-result v7

    .line 219
    invoke-virtual {v2}, Landroid/telephony/CellIdentityWcdma;->getMccString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2}, Landroid/telephony/CellIdentityWcdma;->getMncString()Ljava/lang/String;

    move-result-object v9

    .line 220
    invoke-virtual {v2}, Landroid/telephony/CellIdentityWcdma;->getOperatorAlphaLong()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->getS(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    .line 221
    invoke-virtual {v2}, Landroid/telephony/CellIdentityWcdma;->getOperatorAlphaShort()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->getS(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v11

    .line 222
    invoke-virtual {v2}, Landroid/telephony/CellIdentityWcdma;->getAdditionalPlmns()Ljava/util/Set;

    move-result-object v12

    .line 223
    invoke-virtual {v2}, Landroid/telephony/CellIdentityWcdma;->getClosedSubscriberGroupInfo()Landroid/telephony/ClosedSubscriberGroupInfo;

    move-result-object v13

    move-object v3, v14

    invoke-direct/range {v3 .. v13}, Landroid/telephony/CellIdentityWcdma;-><init>(IIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;Landroid/telephony/ClosedSubscriberGroupInfo;)V

    .line 216
    return-object v14

    .line 224
    .end local v2    # "identityWcdma":Landroid/telephony/CellIdentityWcdma;
    :cond_4
    instance-of v2, v1, Landroid/telephony/CellIdentityNr;

    if-eqz v2, :cond_5

    .line 225
    move-object v2, v1

    check-cast v2, Landroid/telephony/CellIdentityNr;

    .line 226
    .local v2, "identityNr":Landroid/telephony/CellIdentityNr;
    new-instance v15, Landroid/telephony/CellIdentityNr;

    iget-object v3, v0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mStashBlurLocationInfo:Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;

    invoke-static {v3}, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->-$$Nest$fgetcid(Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;)I

    move-result v4

    iget-object v3, v0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mStashBlurLocationInfo:Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;

    invoke-static {v3}, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->-$$Nest$fgetlac(Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;)I

    move-result v5

    .line 227
    invoke-virtual {v2}, Landroid/telephony/CellIdentityNr;->getNrarfcn()I

    move-result v6

    invoke-virtual {v2}, Landroid/telephony/CellIdentityNr;->getBands()[I

    move-result-object v7

    invoke-virtual {v2}, Landroid/telephony/CellIdentityNr;->getMccString()Ljava/lang/String;

    move-result-object v8

    .line 228
    invoke-virtual {v2}, Landroid/telephony/CellIdentityNr;->getMncString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2}, Landroid/telephony/CellIdentityNr;->getNci()J

    move-result-wide v10

    invoke-virtual {v2}, Landroid/telephony/CellIdentityNr;->getOperatorAlphaLong()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->getS(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v12

    .line 229
    invoke-virtual {v2}, Landroid/telephony/CellIdentityNr;->getOperatorAlphaShort()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->getS(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2}, Landroid/telephony/CellIdentityNr;->getAdditionalPlmns()Ljava/util/Set;

    move-result-object v14

    move-object v3, v15

    invoke-direct/range {v3 .. v14}, Landroid/telephony/CellIdentityNr;-><init>(III[ILjava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/util/Collection;)V

    .line 226
    return-object v15

    .line 231
    .end local v2    # "identityNr":Landroid/telephony/CellIdentityNr;
    :cond_5
    return-object v1

    .line 188
    :cond_6
    :goto_0
    return-object v1
.end method

.method public getBlurryCellLocation(Landroid/telephony/CellIdentity;ILjava/lang/String;)Landroid/telephony/CellIdentity;
    .locals 1
    .param p1, "location"    # Landroid/telephony/CellIdentity;
    .param p2, "uid"    # I
    .param p3, "pkgName"    # Ljava/lang/String;

    .line 237
    if-eqz p1, :cond_1

    invoke-virtual {p0, p2, p3}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->isBlurLocationMode(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 240
    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->getBlurryCellLocation(Landroid/telephony/CellIdentity;)Landroid/telephony/CellIdentity;

    move-result-object v0

    return-object v0

    .line 238
    :cond_1
    :goto_0
    return-object p1
.end method

.method public getBlurryLocation(Landroid/location/Location;ILjava/lang/String;)Landroid/location/Location;
    .locals 2
    .param p1, "location"    # Landroid/location/Location;
    .param p2, "uid"    # I
    .param p3, "pkgName"    # Ljava/lang/String;

    .line 175
    if-eqz p1, :cond_1

    invoke-virtual {p0, p2, p3}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->isBlurLocationMode(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 178
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mStashBlurLocationInfo:Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;

    invoke-static {v0}, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->-$$Nest$fgetlatitude(Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;)D

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/location/Location;->setLatitude(D)V

    .line 179
    iget-object v0, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mStashBlurLocationInfo:Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;

    invoke-static {v0}, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->-$$Nest$fgetlongitude(Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;)D

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/location/Location;->setLongitude(D)V

    .line 180
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/location/Location;->setExtras(Landroid/os/Bundle;)V

    .line 181
    iput-object p1, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mLastBlurLocation:Landroid/location/Location;

    .line 182
    return-object p1

    .line 176
    :cond_1
    :goto_0
    return-object p1
.end method

.method public getBlurryLocation(Landroid/location/LocationResult;Landroid/location/util/identity/CallerIdentity;)Landroid/location/LocationResult;
    .locals 6
    .param p1, "location"    # Landroid/location/LocationResult;
    .param p2, "identity"    # Landroid/location/util/identity/CallerIdentity;

    .line 515
    if-eqz p1, :cond_2

    invoke-virtual {p2}, Landroid/location/util/identity/CallerIdentity;->getUid()I

    move-result v0

    invoke-virtual {p2}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->isBlurLocationMode(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 518
    :cond_0
    invoke-virtual {p1}, Landroid/location/LocationResult;->deepCopy()Landroid/location/LocationResult;

    move-result-object v0

    .line 519
    .local v0, "blurResult":Landroid/location/LocationResult;
    invoke-virtual {v0}, Landroid/location/LocationResult;->size()I

    move-result v1

    .line 520
    .local v1, "size":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_1

    .line 521
    invoke-virtual {v0, v2}, Landroid/location/LocationResult;->get(I)Landroid/location/Location;

    move-result-object v3

    .line 522
    .local v3, "blur":Landroid/location/Location;
    iget-object v4, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mStashBlurLocationInfo:Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;

    invoke-static {v4}, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->-$$Nest$fgetlatitude(Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;)D

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Landroid/location/Location;->setLatitude(D)V

    .line 523
    iget-object v4, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mStashBlurLocationInfo:Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;

    invoke-static {v4}, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->-$$Nest$fgetlongitude(Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;)D

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Landroid/location/Location;->setLongitude(D)V

    .line 520
    .end local v3    # "blur":Landroid/location/Location;
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 526
    .end local v2    # "i":I
    :cond_1
    return-object v0

    .line 516
    .end local v0    # "blurResult":Landroid/location/LocationResult;
    .end local v1    # "size":I
    :cond_2
    :goto_1
    return-object p1
.end method

.method public getLocationIfBlurMode(Landroid/location/LastLocationRequest;Landroid/location/util/identity/CallerIdentity;I)Landroid/location/Location;
    .locals 5
    .param p1, "request"    # Landroid/location/LastLocationRequest;
    .param p2, "identity"    # Landroid/location/util/identity/CallerIdentity;
    .param p3, "permissionLevel"    # I

    .line 449
    invoke-virtual {p0, p2}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->isBlurLocationMode(Landroid/location/util/identity/CallerIdentity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 450
    const/4 v0, 0x0

    return-object v0

    .line 452
    :cond_0
    const/4 v0, 0x0

    .line 453
    .local v0, "blurGpsLocation":Landroid/location/Location;
    iget-object v1, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mLocationService:Lcom/android/server/location/LocationManagerService;

    iget-object v1, v1, Lcom/android/server/location/LocationManagerService;->mProviderManagers:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const-string v3, "gps"

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/location/provider/LocationProviderManager;

    .line 454
    .local v2, "lpm":Lcom/android/server/location/provider/LocationProviderManager;
    invoke-virtual {v2}, Lcom/android/server/location/provider/LocationProviderManager;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 455
    goto :goto_0

    .line 457
    :cond_1
    invoke-virtual {v2, p1, p2, p3}, Lcom/android/server/location/provider/LocationProviderManager;->getLastLocation(Landroid/location/LastLocationRequest;Landroid/location/util/identity/CallerIdentity;I)Landroid/location/Location;

    move-result-object v4

    .line 458
    .local v4, "current":Landroid/location/Location;
    if-eqz v4, :cond_2

    .line 459
    new-instance v1, Landroid/location/Location;

    invoke-direct {v1, v4}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    move-object v0, v1

    .line 460
    goto :goto_1

    .line 462
    .end local v2    # "lpm":Lcom/android/server/location/provider/LocationProviderManager;
    .end local v4    # "current":Landroid/location/Location;
    :cond_2
    goto :goto_0

    .line 463
    :cond_3
    :goto_1
    if-eqz v0, :cond_4

    .line 464
    invoke-virtual {v0, v3}, Landroid/location/Location;->setProvider(Ljava/lang/String;)V

    .line 465
    iput-object v0, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mLastBlurLocation:Landroid/location/Location;

    .line 468
    :cond_4
    return-object v0
.end method

.method public getSvStatusData()Lmiui/security/SvStatusData;
    .locals 1

    .line 421
    iget-object v0, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mSvStatusData:Lmiui/security/SvStatusData;

    return-object v0
.end method

.method public handleGpsLocationChangedLocked(Ljava/lang/String;Landroid/location/util/identity/CallerIdentity;)V
    .locals 8
    .param p1, "provider"    # Ljava/lang/String;
    .param p2, "identity"    # Landroid/location/util/identity/CallerIdentity;

    .line 483
    const-string v0, "gps"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mBlurGpsHandler:Landroid/os/Handler;

    .line 484
    invoke-virtual {p2}, Landroid/location/util/identity/CallerIdentity;->getUid()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 485
    invoke-virtual {p2}, Landroid/location/util/identity/CallerIdentity;->getUid()I

    move-result v0

    invoke-virtual {p2}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->isBlurLocationMode(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_2

    .line 488
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mLocationService:Lcom/android/server/location/LocationManagerService;

    const-string v1, "gps"

    invoke-virtual {v0, v1}, Lcom/android/server/location/LocationManagerService;->getLocationProviderManager(Ljava/lang/String;)Lcom/android/server/location/provider/LocationProviderManager;

    move-result-object v0

    .line 489
    .local v0, "gpsManager":Lcom/android/server/location/provider/LocationProviderManager;
    if-nez v0, :cond_1

    .line 490
    return-void

    .line 492
    :cond_1
    iget-object v1, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mLastBlurLocation:Landroid/location/Location;

    if-nez v1, :cond_2

    .line 493
    return-void

    .line 495
    :cond_2
    iget-object v1, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mLastBlurLocation:Landroid/location/Location;

    const-string v2, "gps"

    invoke-virtual {v1, v2}, Landroid/location/Location;->setProvider(Ljava/lang/String;)V

    .line 496
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v1

    .line 498
    .local v1, "clearCallingIdentity":J
    :try_start_0
    const-string v3, "mMultiplexerLock"

    const-class v4, Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lmiui/util/ReflectionUtils;->getObjectField(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    .line 499
    .local v3, "lock":Ljava/lang/Object;
    monitor-enter v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 500
    :try_start_1
    invoke-static {}, Lcom/android/server/location/provider/LocationProviderManagerStub;->getInstance()Lcom/android/server/location/provider/LocationProviderManagerStub;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Landroid/location/Location;

    iget-object v6, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mLastBlurLocation:Landroid/location/Location;

    const/4 v7, 0x0

    aput-object v6, v5, v7

    .line 501
    invoke-static {v5}, Landroid/location/LocationResult;->create([Landroid/location/Location;)Landroid/location/LocationResult;

    move-result-object v5

    .line 500
    invoke-interface {v4, v0, v5, p2, v3}, Lcom/android/server/location/provider/LocationProviderManagerStub;->onReportBlurLocation(Lcom/android/server/location/provider/LocationProviderManager;Landroid/location/LocationResult;Landroid/location/util/identity/CallerIdentity;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 502
    iget-object v4, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mBlurGpsHandler:Landroid/os/Handler;

    .line 503
    invoke-virtual {p2}, Landroid/location/util/identity/CallerIdentity;->getUid()I

    move-result v5

    .line 502
    invoke-virtual {v4, v5, p2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v5

    const-wide/16 v6, 0xbb8

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 505
    :cond_3
    monitor-exit v3

    .line 509
    .end local v3    # "lock":Ljava/lang/Object;
    goto :goto_0

    .line 505
    .restart local v3    # "lock":Ljava/lang/Object;
    :catchall_0
    move-exception v4

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local v0    # "gpsManager":Lcom/android/server/location/provider/LocationProviderManager;
    .end local v1    # "clearCallingIdentity":J
    .end local p0    # "this":Lcom/android/server/location/MiuiBlurLocationManagerImpl;
    .end local p1    # "provider":Ljava/lang/String;
    .end local p2    # "identity":Landroid/location/util/identity/CallerIdentity;
    :try_start_2
    throw v4
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 509
    .end local v3    # "lock":Ljava/lang/Object;
    .restart local v0    # "gpsManager":Lcom/android/server/location/provider/LocationProviderManager;
    .restart local v1    # "clearCallingIdentity":J
    .restart local p0    # "this":Lcom/android/server/location/MiuiBlurLocationManagerImpl;
    .restart local p1    # "provider":Ljava/lang/String;
    .restart local p2    # "identity":Landroid/location/util/identity/CallerIdentity;
    :catchall_1
    move-exception v3

    goto :goto_1

    .line 506
    :catch_0
    move-exception v3

    .line 507
    .local v3, "e":Ljava/lang/Exception;
    :try_start_3
    const-string v4, "MiuiBlurLocationManager"

    const-string v5, "onReportBlurLocation exception!"

    invoke-static {v4, v5, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 509
    nop

    .end local v3    # "e":Ljava/lang/Exception;
    :goto_0
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 510
    nop

    .line 511
    return-void

    .line 509
    :goto_1
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 510
    throw v3

    .line 486
    .end local v0    # "gpsManager":Lcom/android/server/location/provider/LocationProviderManager;
    .end local v1    # "clearCallingIdentity":J
    :cond_4
    :goto_2
    return-void
.end method

.method public init(Lcom/android/server/location/LocationManagerService;Landroid/content/Context;)V
    .locals 1
    .param p1, "service"    # Lcom/android/server/location/LocationManagerService;
    .param p2, "context"    # Landroid/content/Context;

    .line 123
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mLocationService:Lcom/android/server/location/LocationManagerService;

    if-nez v0, :cond_0

    .line 124
    iput-object p1, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mLocationService:Lcom/android/server/location/LocationManagerService;

    .line 126
    :cond_0
    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 127
    iput-object p2, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mContext:Landroid/content/Context;

    .line 129
    :cond_1
    return-void
.end method

.method public initBlurLocationReady()Z
    .locals 3

    .line 381
    iget-object v0, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mStashBlurLocationInfo:Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;

    invoke-virtual {v0}, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->isValidInfo()Z

    move-result v0

    if-nez v0, :cond_0

    .line 382
    invoke-direct {p0}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->initBlurLocationData()V

    .line 384
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mStashBlurLocationInfo:Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;

    invoke-virtual {v0}, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->isValidInfo()Z

    move-result v0

    .line 385
    .local v0, "validInfo":Z
    if-nez v0, :cond_1

    .line 386
    iget-object v1, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mHandler:Landroid/os/Handler;

    const v2, 0x29cc2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 388
    :cond_1
    return v0
.end method

.method public isBlurLocationMode(ILjava/lang/String;)Z
    .locals 2
    .param p1, "uid"    # I
    .param p2, "pkgName"    # Ljava/lang/String;

    .line 363
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v0, :cond_0

    .line 364
    invoke-static {p1}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v0

    const/16 v1, 0x2710

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mBlurLocationManager:Lcom/android/internal/app/ILocationBlurry;

    if-eqz v0, :cond_0

    .line 366
    invoke-direct {p0, p1, p2}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->noteOpUseMyIdentity(ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 367
    invoke-virtual {p0}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->initBlurLocationReady()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 363
    :goto_0
    return v0
.end method

.method public mayNotifyGpsListener(Ljava/lang/String;Landroid/location/LocationResult;)V
    .locals 6
    .param p1, "currentProvider"    # Ljava/lang/String;
    .param p2, "location"    # Landroid/location/LocationResult;

    .line 426
    const-string v0, "gps"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "passive"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_2

    .line 429
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mLocationService:Lcom/android/server/location/LocationManagerService;

    const-string v1, "gps"

    invoke-virtual {v0, v1}, Lcom/android/server/location/LocationManagerService;->getLocationProviderManager(Ljava/lang/String;)Lcom/android/server/location/provider/LocationProviderManager;

    move-result-object v0

    .line 430
    .local v0, "manager":Lcom/android/server/location/provider/LocationProviderManager;
    if-nez v0, :cond_1

    .line 431
    return-void

    .line 434
    :cond_1
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v1

    .line 436
    .local v1, "clearCallingIdentity":J
    :try_start_0
    const-string v3, "mMultiplexerLock"

    const-class v4, Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lmiui/util/ReflectionUtils;->getObjectField(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    .line 437
    .local v3, "lock":Ljava/lang/Object;
    monitor-enter v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 438
    :try_start_1
    invoke-static {}, Lcom/android/server/location/provider/LocationProviderManagerStub;->getInstance()Lcom/android/server/location/provider/LocationProviderManagerStub;

    move-result-object v4

    invoke-interface {v4, v0, p2, v3}, Lcom/android/server/location/provider/LocationProviderManagerStub;->mayNotifyGpsBlurListener(Lcom/android/server/location/provider/LocationProviderManager;Landroid/location/LocationResult;Ljava/lang/Object;)V

    .line 439
    monitor-exit v3

    .line 443
    .end local v3    # "lock":Ljava/lang/Object;
    goto :goto_0

    .line 439
    .restart local v3    # "lock":Ljava/lang/Object;
    :catchall_0
    move-exception v4

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local v0    # "manager":Lcom/android/server/location/provider/LocationProviderManager;
    .end local v1    # "clearCallingIdentity":J
    .end local p0    # "this":Lcom/android/server/location/MiuiBlurLocationManagerImpl;
    .end local p1    # "currentProvider":Ljava/lang/String;
    .end local p2    # "location":Landroid/location/LocationResult;
    :try_start_2
    throw v4
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 443
    .end local v3    # "lock":Ljava/lang/Object;
    .restart local v0    # "manager":Lcom/android/server/location/provider/LocationProviderManager;
    .restart local v1    # "clearCallingIdentity":J
    .restart local p0    # "this":Lcom/android/server/location/MiuiBlurLocationManagerImpl;
    .restart local p1    # "currentProvider":Ljava/lang/String;
    .restart local p2    # "location":Landroid/location/LocationResult;
    :catchall_1
    move-exception v3

    goto :goto_1

    .line 440
    :catch_0
    move-exception v3

    .line 441
    .local v3, "e":Ljava/lang/Exception;
    :try_start_3
    const-string v4, "MiuiBlurLocationManager"

    const-string v5, "mayNotifyGpsListener exception!"

    invoke-static {v4, v5, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 443
    nop

    .end local v3    # "e":Ljava/lang/Exception;
    :goto_0
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 444
    nop

    .line 445
    return-void

    .line 443
    :goto_1
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 444
    throw v3

    .line 427
    .end local v0    # "manager":Lcom/android/server/location/provider/LocationProviderManager;
    .end local v1    # "clearCallingIdentity":J
    :cond_2
    :goto_2
    return-void
.end method

.method public registerLocationBlurryManager(Lcom/android/internal/app/ILocationBlurry;)V
    .locals 2
    .param p1, "manager"    # Lcom/android/internal/app/ILocationBlurry;

    .line 159
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_0

    .line 160
    return-void

    .line 162
    :cond_0
    if-eqz p1, :cond_1

    .line 163
    const-string v0, "MiuiBlurLocationManager"

    const-string v1, "registerLocationBlurryManager invoke"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    iput-object p1, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mBlurLocationManager:Lcom/android/internal/app/ILocationBlurry;

    .line 165
    invoke-direct {p0}, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->initBlurLocationData()V

    .line 167
    :cond_1
    return-void
.end method

.method public updateSvStatusData(I[I[F[F[F[F[F)V
    .locals 13
    .param p1, "svCount"    # I
    .param p2, "svidWithFlags"    # [I
    .param p3, "cn0s"    # [F
    .param p4, "svElevations"    # [F
    .param p5, "svAzimuths"    # [F
    .param p6, "svCarrierFreqs"    # [F
    .param p7, "basebandCn0s"    # [F

    .line 394
    move-object v1, p0

    move v10, p1

    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v0, :cond_1

    iget-object v0, v1, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mBlurLocationManager:Lcom/android/internal/app/ILocationBlurry;

    if-eqz v0, :cond_1

    const/16 v0, 0x14

    if-lt v10, v0, :cond_1

    iget-object v0, v1, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mHandler:Landroid/os/Handler;

    .line 395
    const/4 v11, 0x3

    invoke-virtual {v0, v11}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    goto/16 :goto_1

    .line 398
    :cond_0
    new-instance v0, Lmiui/security/SvStatusData;

    move-object v2, v0

    move v3, p1

    move-object v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    invoke-direct/range {v2 .. v9}, Lmiui/security/SvStatusData;-><init>(I[I[F[F[F[F[F)V

    iput-object v0, v1, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mSvStatusData:Lmiui/security/SvStatusData;

    .line 400
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    move-object v2, v0

    .line 401
    .local v2, "data":Landroid/os/Bundle;
    const-string v0, "key_svcount"

    invoke-virtual {v2, v0, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 402
    const-string v0, "key_svidWithFlags"

    move-object v3, p2

    invoke-virtual {v2, v0, p2}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 403
    const-string v0, "key_cn0s"

    move-object/from16 v4, p3

    invoke-virtual {v2, v0, v4}, Landroid/os/Bundle;->putFloatArray(Ljava/lang/String;[F)V

    .line 404
    const-string v0, "key_svElevations"

    move-object/from16 v5, p4

    invoke-virtual {v2, v0, v5}, Landroid/os/Bundle;->putFloatArray(Ljava/lang/String;[F)V

    .line 405
    const-string v0, "key_svAzimuths"

    move-object/from16 v6, p5

    invoke-virtual {v2, v0, v6}, Landroid/os/Bundle;->putFloatArray(Ljava/lang/String;[F)V

    .line 406
    const-string v0, "key_svCarrierFreqs"

    move-object/from16 v7, p6

    invoke-virtual {v2, v0, v7}, Landroid/os/Bundle;->putFloatArray(Ljava/lang/String;[F)V

    .line 407
    const-string v0, "key_basebandCn0s"

    move-object/from16 v8, p7

    invoke-virtual {v2, v0, v8}, Landroid/os/Bundle;->putFloatArray(Ljava/lang/String;[F)V

    .line 408
    iget-object v0, v1, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mBlurMessenger:Landroid/os/Messenger;

    invoke-virtual {v0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    const-string v9, "key_callback"

    invoke-virtual {v2, v9, v0}, Landroid/os/Bundle;->putBinder(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 410
    :try_start_0
    iget-object v0, v1, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mBlurLocationManager:Lcom/android/internal/app/ILocationBlurry;

    invoke-interface {v0, v2}, Lcom/android/internal/app/ILocationBlurry;->sendSvStatusData(Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 413
    goto :goto_0

    .line 411
    :catch_0
    move-exception v0

    .line 414
    :goto_0
    iget-object v0, v1, Lcom/android/server/location/MiuiBlurLocationManagerImpl;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v11}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v9

    const-wide/32 v11, 0xdbba0

    invoke-virtual {v0, v9, v11, v12}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 416
    return-void

    .line 394
    .end local v2    # "data":Landroid/os/Bundle;
    :cond_1
    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    .line 396
    :goto_1
    return-void
.end method
