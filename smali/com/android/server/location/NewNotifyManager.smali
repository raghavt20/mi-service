.class public Lcom/android/server/location/NewNotifyManager;
.super Ljava/lang/Object;
.source "NewNotifyManager.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "GnssNps-NotifyManager"


# instance fields
.field private final CHANNEL_ID:Ljava/lang/String;

.field private final NOTIFY_ID:I

.field private intentToSettings:Landroid/app/PendingIntent;

.field private mAppContext:Landroid/content/Context;

.field private mBuilder:Landroid/app/Notification$Builder;

.field private mNotificationManager:Landroid/app/NotificationManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const-string v0, "GPS_STATUS_MONITOR_ID"

    iput-object v0, p0, Lcom/android/server/location/NewNotifyManager;->CHANNEL_ID:Ljava/lang/String;

    .line 28
    const/4 v1, 0x1

    iput v1, p0, Lcom/android/server/location/NewNotifyManager;->NOTIFY_ID:I

    .line 37
    iput-object p1, p0, Lcom/android/server/location/NewNotifyManager;->mAppContext:Landroid/content/Context;

    .line 38
    const-string v1, "notification"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    iput-object v1, p0, Lcom/android/server/location/NewNotifyManager;->mNotificationManager:Landroid/app/NotificationManager;

    .line 39
    new-instance v1, Landroid/app/Notification$Builder;

    iget-object v2, p0, Lcom/android/server/location/NewNotifyManager;->mAppContext:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/server/location/NewNotifyManager;->mBuilder:Landroid/app/Notification$Builder;

    .line 41
    invoke-virtual {p0}, Lcom/android/server/location/NewNotifyManager;->initNotifyChannel()V

    .line 43
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 44
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.android.settings"

    const-string v2, "com.android.settings.Settings$LocationSettingsActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 45
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 46
    iget-object v1, p0, Lcom/android/server/location/NewNotifyManager;->mAppContext:Landroid/content/Context;

    const/4 v2, 0x0

    const/high16 v3, 0xc000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/location/NewNotifyManager;->intentToSettings:Landroid/app/PendingIntent;

    .line 47
    return-void
.end method

.method private getAppIcon(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;
    .locals 11
    .param p1, "draw"    # Landroid/graphics/drawable/Drawable;

    .line 96
    instance-of v0, p1, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_0

    .line 97
    move-object v0, p1

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0

    .line 98
    :cond_0
    instance-of v0, p1, Landroid/graphics/drawable/AdaptiveIconDrawable;

    if-eqz v0, :cond_1

    .line 99
    move-object v0, p1

    check-cast v0, Landroid/graphics/drawable/AdaptiveIconDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AdaptiveIconDrawable;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 100
    .local v0, "backgroundDr":Landroid/graphics/drawable/Drawable;
    move-object v1, p1

    check-cast v1, Landroid/graphics/drawable/AdaptiveIconDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/AdaptiveIconDrawable;->getForeground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 101
    .local v1, "foregroundDr":Landroid/graphics/drawable/Drawable;
    const/4 v2, 0x2

    new-array v2, v2, [Landroid/graphics/drawable/Drawable;

    .line 102
    .local v2, "drr":[Landroid/graphics/drawable/Drawable;
    const/4 v3, 0x0

    aput-object v0, v2, v3

    .line 103
    const/4 v4, 0x1

    aput-object v1, v2, v4

    .line 104
    new-instance v4, Landroid/graphics/drawable/LayerDrawable;

    invoke-direct {v4, v2}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 105
    .local v4, "layerDrawable":Landroid/graphics/drawable/LayerDrawable;
    invoke-virtual {v4}, Landroid/graphics/drawable/LayerDrawable;->getIntrinsicWidth()I

    move-result v5

    .line 106
    .local v5, "width":I
    invoke-virtual {v4}, Landroid/graphics/drawable/LayerDrawable;->getIntrinsicHeight()I

    move-result v6

    .line 107
    .local v6, "height":I
    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v6, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 108
    .local v7, "bitmap":Landroid/graphics/Bitmap;
    new-instance v8, Landroid/graphics/Canvas;

    invoke-direct {v8, v7}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 109
    .local v8, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v8}, Landroid/graphics/Canvas;->getWidth()I

    move-result v9

    invoke-virtual {v8}, Landroid/graphics/Canvas;->getHeight()I

    move-result v10

    invoke-virtual {v4, v3, v3, v9, v10}, Landroid/graphics/drawable/LayerDrawable;->setBounds(IIII)V

    .line 110
    invoke-virtual {v4, v8}, Landroid/graphics/drawable/LayerDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 111
    return-object v7

    .line 113
    .end local v0    # "backgroundDr":Landroid/graphics/drawable/Drawable;
    .end local v1    # "foregroundDr":Landroid/graphics/drawable/Drawable;
    .end local v2    # "drr":[Landroid/graphics/drawable/Drawable;
    .end local v4    # "layerDrawable":Landroid/graphics/drawable/LayerDrawable;
    .end local v5    # "width":I
    .end local v6    # "height":I
    .end local v7    # "bitmap":Landroid/graphics/Bitmap;
    .end local v8    # "canvas":Landroid/graphics/Canvas;
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method private isChineseLanguage()Z
    .locals 2

    .line 117
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    .line 118
    .local v0, "language":Ljava/lang/String;
    const-string/jumbo v1, "zh_CN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    return v1
.end method


# virtual methods
.method public initNotifyChannel()V
    .locals 6

    .line 122
    iget-object v0, p0, Lcom/android/server/location/NewNotifyManager;->mNotificationManager:Landroid/app/NotificationManager;

    if-nez v0, :cond_0

    .line 123
    return-void

    .line 125
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/NewNotifyManager;->mAppContext:Landroid/content/Context;

    const v1, 0x110f02bd

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 126
    .local v0, "description":Ljava/lang/String;
    new-instance v1, Landroid/app/NotificationChannel;

    const-string v2, "GPS_STATUS_MONITOR_ID"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v0, v3}, Landroid/app/NotificationChannel;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V

    .line 127
    .local v1, "channel":Landroid/app/NotificationChannel;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/NotificationChannel;->setLockscreenVisibility(I)V

    .line 128
    const/4 v3, 0x0

    invoke-virtual {v1, v3, v3}, Landroid/app/NotificationChannel;->setSound(Landroid/net/Uri;Landroid/media/AudioAttributes;)V

    .line 129
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/app/NotificationChannel;->enableVibration(Z)V

    .line 130
    new-array v2, v2, [J

    const-wide/16 v4, 0x0

    aput-wide v4, v2, v3

    invoke-virtual {v1, v2}, Landroid/app/NotificationChannel;->setVibrationPattern([J)V

    .line 131
    iget-object v2, p0, Lcom/android/server/location/NewNotifyManager;->mNotificationManager:Landroid/app/NotificationManager;

    invoke-virtual {v2, v1}, Landroid/app/NotificationManager;->createNotificationChannel(Landroid/app/NotificationChannel;)V

    .line 132
    return-void
.end method

.method public removeNotification()V
    .locals 2

    .line 50
    const-string v0, "GnssNps-NotifyManager"

    const-string v1, "removeNotification"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    :try_start_0
    iget-object v0, p0, Lcom/android/server/location/NewNotifyManager;->mNotificationManager:Landroid/app/NotificationManager;

    if-eqz v0, :cond_0

    .line 53
    invoke-virtual {v0}, Landroid/app/NotificationManager;->cancelAll()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 57
    :cond_0
    goto :goto_0

    .line 55
    :catch_0
    move-exception v0

    .line 56
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 58
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public showNavigationNotification(Ljava/lang/String;)V
    .locals 8
    .param p1, "packageName"    # Ljava/lang/String;

    .line 61
    invoke-direct {p0}, Lcom/android/server/location/NewNotifyManager;->isChineseLanguage()Z

    move-result v0

    const-string v1, "GnssNps-NotifyManager"

    if-nez v0, :cond_0

    .line 62
    const-string/jumbo v0, "show notification only in CHINESE language"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    return-void

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/NewNotifyManager;->mAppContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 69
    .local v0, "pm":Landroid/content/pm/PackageManager;
    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {v0, p1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    .line 70
    .local v2, "ai":Landroid/content/pm/ApplicationInfo;
    invoke-virtual {v2, v0}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 71
    .local v3, "appName":Ljava/lang/String;
    invoke-virtual {v0, p1}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 72
    .local v4, "draw":Landroid/graphics/drawable/Drawable;
    invoke-direct {p0, v4}, Lcom/android/server/location/NewNotifyManager;->getAppIcon(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 73
    .local v5, "iconBmp":Landroid/graphics/Bitmap;
    if-nez v5, :cond_1

    .line 74
    iget-object v6, p0, Lcom/android/server/location/NewNotifyManager;->mAppContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x110801b8

    invoke-static {v6, v7}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    move-object v5, v1

    .line 80
    .end local v2    # "ai":Landroid/content/pm/ApplicationInfo;
    .end local v4    # "draw":Landroid/graphics/drawable/Drawable;
    :cond_1
    nop

    .line 81
    iget-object v1, p0, Lcom/android/server/location/NewNotifyManager;->mBuilder:Landroid/app/Notification$Builder;

    invoke-virtual {v1, v5}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    .line 82
    iget-object v1, p0, Lcom/android/server/location/NewNotifyManager;->mBuilder:Landroid/app/Notification$Builder;

    const v2, 0x110801b9

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    .line 84
    :try_start_1
    iget-object v1, p0, Lcom/android/server/location/NewNotifyManager;->mBuilder:Landroid/app/Notification$Builder;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/android/server/location/NewNotifyManager;->mAppContext:Landroid/content/Context;

    const v6, 0x110f02b8

    invoke-virtual {v4, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/location/NewNotifyManager;->mAppContext:Landroid/content/Context;

    .line 85
    const v4, 0x110f02b7

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/location/NewNotifyManager;->intentToSettings:Landroid/app/PendingIntent;

    .line 86
    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v1

    .line 87
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v1

    .line 88
    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    .line 89
    iget-object v1, p0, Lcom/android/server/location/NewNotifyManager;->mNotificationManager:Landroid/app/NotificationManager;

    iget-object v4, p0, Lcom/android/server/location/NewNotifyManager;->mBuilder:Landroid/app/Notification$Builder;

    invoke-virtual {v4}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 92
    goto :goto_0

    .line 90
    :catch_0
    move-exception v1

    .line 91
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 93
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 77
    .end local v3    # "appName":Ljava/lang/String;
    .end local v5    # "iconBmp":Landroid/graphics/Bitmap;
    :catch_1
    move-exception v2

    .line 78
    .local v2, "nameNotFoundException":Landroid/content/pm/PackageManager$NameNotFoundException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "No such application for this name:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    return-void
.end method
