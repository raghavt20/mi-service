class com.android.server.location.GnssSessionInfo$PQWP6 {
	 /* .source "GnssSessionInfo.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/location/GnssSessionInfo; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "PQWP6" */
} // .end annotation
/* # instance fields */
private java.lang.String mNmea;
private Boolean pdrEnaged;
private Boolean sapEnaged;
private java.lang.String sub1;
private java.lang.String sub2;
final com.android.server.location.GnssSessionInfo this$0; //synthetic
/* # direct methods */
public com.android.server.location.GnssSessionInfo$PQWP6 ( ) {
/* .locals 0 */
/* .param p2, "nmea" # Ljava/lang/String; */
/* .line 244 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 241 */
int p1 = 0; // const/4 p1, 0x0
/* iput-boolean p1, p0, Lcom/android/server/location/GnssSessionInfo$PQWP6;->sapEnaged:Z */
/* .line 242 */
/* iput-boolean p1, p0, Lcom/android/server/location/GnssSessionInfo$PQWP6;->pdrEnaged:Z */
/* .line 245 */
this.mNmea = p2;
/* .line 246 */
return;
} // .end method
/* # virtual methods */
public Boolean isPdrEngaged ( ) {
/* .locals 1 */
/* .line 264 */
/* iget-boolean v0, p0, Lcom/android/server/location/GnssSessionInfo$PQWP6;->pdrEnaged:Z */
} // .end method
public Boolean isSapEnaged ( ) {
/* .locals 1 */
/* .line 260 */
/* iget-boolean v0, p0, Lcom/android/server/location/GnssSessionInfo$PQWP6;->sapEnaged:Z */
} // .end method
public void parse ( ) {
/* .locals 5 */
/* .line 249 */
v0 = this.mNmea;
final String v1 = ","; // const-string v1, ","
(( java.lang.String ) v0 ).split ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
this.sub1 = v0;
/* .line 250 */
/* array-length v1, v0 */
int v2 = 3; // const/4 v2, 0x3
/* if-eq v1, v2, :cond_0 */
/* .line 251 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Error nmea is: "; // const-string v1, "Error nmea is: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mNmea;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "GnssSessionInfo"; // const-string v1, "GnssSessionInfo"
android.util.Log .e ( v1,v0 );
/* .line 252 */
return;
/* .line 254 */
} // :cond_0
int v1 = 2; // const/4 v1, 0x2
/* aget-object v0, v0, v1 */
final String v2 = "\\*"; // const-string v2, "\\*"
(( java.lang.String ) v0 ).split ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
this.sub2 = v0;
/* .line 255 */
int v2 = 0; // const/4 v2, 0x0
/* aget-object v0, v0, v2 */
/* const/16 v3, 0x10 */
v0 = java.lang.Integer .parseInt ( v0,v3 );
int v4 = 1; // const/4 v4, 0x1
/* shr-int/2addr v0, v4 */
/* and-int/2addr v0, v4 */
/* if-ne v0, v4, :cond_1 */
/* move v0, v4 */
} // :cond_1
/* move v0, v2 */
} // :goto_0
/* iput-boolean v0, p0, Lcom/android/server/location/GnssSessionInfo$PQWP6;->sapEnaged:Z */
/* .line 256 */
v0 = this.sub2;
/* aget-object v0, v0, v2 */
v0 = java.lang.Integer .parseInt ( v0,v3 );
/* shr-int/2addr v0, v1 */
/* and-int/2addr v0, v4 */
/* if-ne v0, v4, :cond_2 */
/* move v2, v4 */
} // :cond_2
/* iput-boolean v2, p0, Lcom/android/server/location/GnssSessionInfo$PQWP6;->pdrEnaged:Z */
/* .line 257 */
return;
} // .end method
