public class com.android.server.location.GnssMockLocationOptImpl implements com.android.server.location.GnssMockLocationOptStub {
	 /* .source "GnssMockLocationOptImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final Integer ALWAYS_MOCK_MODE;
	 private static final Integer ALWAYS_NONMOCK_MODE;
	 private static final java.lang.String MOCK_REMOVE;
	 private static final java.lang.String MOCK_SETUP;
	 private static final Integer ONLY_MOCK_ONECE;
	 private static final java.lang.String PERSIST_FOR_GMO_VERSION;
	 private static final java.lang.String TAG;
	 private static android.content.Context mContext;
	 private static final java.util.HashSet sBatList;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/HashSet<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private static Boolean sFusedProviderStatus;
private static Boolean sGpsProviderStatus;
private static Boolean sLastFlag;
private static Integer sLastMockAppPid;
private static Boolean sMockFlagSetByUser;
private static Boolean sNetworkProviderStatus;
/* # instance fields */
private final Boolean D;
private Boolean mAlreadyLoadControlFlag;
private java.util.Set mBatUsing;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.location.util.identity.CallerIdentity mCallerIdentity;
private Boolean mChangeModeInNaviCondition;
private Long mCountRemoveTimes;
private Boolean mEnableGmoControlStatus;
private Boolean mEnableGnssMockLocationOpt;
private com.android.server.location.provider.MockableLocationProvider mFusedProvider;
private final java.io.File mGmoCloudFlagFile;
private final java.util.concurrent.atomic.AtomicInteger mGmoVersion;
private com.android.server.location.provider.MockableLocationProvider mGpsProvider;
private Boolean mIsMockMode;
private Boolean mIsValidInterval;
private java.lang.String mLatestMockApp;
private java.lang.String mName;
private com.android.server.location.provider.MockableLocationProvider mNetworkProvider;
private Boolean mPermittedRunning;
private com.android.server.location.provider.MockableLocationProvider mProvider;
private java.util.Map mRecordMockSchedule;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Long;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Boolean mRemovedBySystem;
private Long mTimeInterval;
private Long mTimeStart;
private Long mTimeStop;
/* # direct methods */
static Boolean -$$Nest$fgetD ( com.android.server.location.GnssMockLocationOptImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z */
} // .end method
static java.util.Map -$$Nest$fgetmRecordMockSchedule ( com.android.server.location.GnssMockLocationOptImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mRecordMockSchedule;
} // .end method
static com.android.server.location.GnssMockLocationOptImpl ( ) {
/* .locals 2 */
/* .line 75 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 78 */
final String v1 = "com.baidu.BaiduMap"; // const-string v1, "com.baidu.BaiduMap"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 79 */
final String v1 = "com.autonavi.minimap"; // const-string v1, "com.autonavi.minimap"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 80 */
final String v1 = "com.tencent.map"; // const-string v1, "com.tencent.map"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 109 */
/* const/16 v0, 0x3e8 */
return;
} // .end method
 com.android.server.location.GnssMockLocationOptImpl ( ) {
/* .locals 5 */
/* .line 135 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 74 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mEnableGnssMockLocationOpt:Z */
/* .line 83 */
java.util.concurrent.ConcurrentHashMap .newKeySet ( );
this.mBatUsing = v1;
/* .line 88 */
final String v1 = "persist.sys.gnss_dc.test"; // const-string v1, "persist.sys.gnss_dc.test"
int v2 = 0; // const/4 v2, 0x0
v1 = android.os.SystemProperties .getBoolean ( v1,v2 );
/* iput-boolean v1, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z */
/* .line 95 */
/* new-instance v1, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.mRecordMockSchedule = v1;
/* .line 121 */
/* iput-boolean v0, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mPermittedRunning:Z */
/* .line 125 */
/* iput-boolean v0, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mEnableGmoControlStatus:Z */
/* .line 126 */
/* new-instance v0, Ljava/io/File; */
/* new-instance v1, Ljava/io/File; */
android.os.Environment .getDataDirectory ( );
/* const-string/jumbo v4, "system" */
/* invoke-direct {v1, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
final String v3 = "GmoCloudFlagFile.xml"; // const-string v3, "GmoCloudFlagFile.xml"
/* invoke-direct {v0, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
this.mGmoCloudFlagFile = v0;
/* .line 130 */
/* new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger; */
/* .line 131 */
final String v1 = "persist.sys.gmo.version"; // const-string v1, "persist.sys.gmo.version"
v1 = android.os.SystemProperties .getInt ( v1,v2 );
/* invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V */
this.mGmoVersion = v0;
/* .line 138 */
/* new-instance v0, Lcom/android/server/location/GnssMockLocationOptImpl$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/location/GnssMockLocationOptImpl$1;-><init>(Lcom/android/server/location/GnssMockLocationOptImpl;)V */
com.android.server.location.Order .setOnChangeListener ( v0 );
/* .line 146 */
return;
} // .end method
private void calculate ( java.util.Map p0, Boolean p1 ) {
/* .locals 12 */
/* .param p2, "flag" # Z */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Long;", */
/* "Ljava/lang/String;", */
/* ">;Z)V" */
/* } */
} // .end annotation
/* .line 574 */
/* .local p1, "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Ljava/lang/String;>;" */
int v0 = 0; // const/4 v0, 0x0
/* .line 575 */
/* .local v0, "cnt":I */
int v1 = 0; // const/4 v1, 0x0
/* .line 576 */
/* .local v1, "cntOdd":I */
int v2 = 0; // const/4 v2, 0x0
/* .line 578 */
/* .local v2, "cntEven":I */
/* new-instance v3, Ljava/util/ArrayList; */
/* invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V */
/* .line 579 */
/* .local v3, "oddTimePoint":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;" */
/* new-instance v4, Ljava/util/ArrayList; */
/* invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V */
/* .line 581 */
/* .local v4, "evenTimePoint":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;" */
v6 = } // :goto_0
if ( v6 != null) { // if-eqz v6, :cond_1
/* check-cast v6, Ljava/lang/Long; */
(( java.lang.Long ) v6 ).longValue ( ); // invoke-virtual {v6}, Ljava/lang/Long;->longValue()J
/* move-result-wide v6 */
/* .line 583 */
/* .local v6, "key":J */
/* rem-int/lit8 v8, v0, 0x2 */
/* if-nez v8, :cond_0 */
/* .line 584 */
java.lang.Long .valueOf ( v6,v7 );
(( java.util.ArrayList ) v4 ).add ( v8 ); // invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 585 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 587 */
} // :cond_0
java.lang.Long .valueOf ( v6,v7 );
(( java.util.ArrayList ) v3 ).add ( v8 ); // invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 588 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 590 */
} // :goto_1
/* nop */
} // .end local v6 # "key":J
/* add-int/lit8 v0, v0, 0x1 */
/* .line 591 */
/* .line 592 */
} // :cond_1
int v5 = 0; // const/4 v5, 0x0
/* .local v5, "i":I */
} // :goto_2
/* if-ge v5, v1, :cond_2 */
/* .line 593 */
/* iget-wide v6, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeInterval:J */
(( java.util.ArrayList ) v3 ).get ( v5 ); // invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v8, Ljava/lang/Long; */
(( java.lang.Long ) v8 ).longValue ( ); // invoke-virtual {v8}, Ljava/lang/Long;->longValue()J
/* move-result-wide v8 */
(( java.util.ArrayList ) v4 ).get ( v5 ); // invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v10, Ljava/lang/Long; */
(( java.lang.Long ) v10 ).longValue ( ); // invoke-virtual {v10}, Ljava/lang/Long;->longValue()J
/* move-result-wide v10 */
/* sub-long/2addr v8, v10 */
/* add-long/2addr v6, v8 */
/* iput-wide v6, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeInterval:J */
/* .line 592 */
/* add-int/lit8 v5, v5, 0x1 */
/* .line 596 */
} // .end local v5 # "i":I
} // :cond_2
/* if-nez p2, :cond_3 */
/* .line 597 */
/* iget-wide v5, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeStop:J */
/* iget-wide v7, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeStart:J */
/* sub-long/2addr v5, v7 */
/* iget-wide v7, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeInterval:J */
/* sub-long/2addr v5, v7 */
/* iput-wide v5, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeInterval:J */
/* .line 599 */
} // :cond_3
if ( p2 != null) { // if-eqz p2, :cond_4
/* .line 600 */
com.android.server.location.gnss.GnssEventTrackingStub .getInstance ( );
/* int-to-long v6, v1 */
/* .line 602 */
} // :cond_4
com.android.server.location.gnss.GnssEventTrackingStub .getInstance ( );
/* add-int/lit8 v6, v2, -0x1 */
/* int-to-long v6, v6 */
/* .line 604 */
} // :goto_3
return;
} // .end method
private void dealingProcess ( ) {
/* .locals 11 */
/* .line 489 */
final String v0 = "1"; // const-string v0, "1"
final String v1 = "GnssMockLocationOpt"; // const-string v1, "GnssMockLocationOpt"
/* new-instance v2, Ljava/util/HashMap; */
/* invoke-direct {v2}, Ljava/util/HashMap;-><init>()V */
/* .line 490 */
/* .local v2, "preResult":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Ljava/lang/String;>;" */
v3 = this.mRecordMockSchedule;
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_1
/* check-cast v4, Ljava/lang/Long; */
(( java.lang.Long ) v4 ).longValue ( ); // invoke-virtual {v4}, Ljava/lang/Long;->longValue()J
/* move-result-wide v4 */
/* .line 491 */
/* .local v4, "key":J */
/* iget-wide v6, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeStart:J */
/* cmp-long v6, v4, v6 */
/* if-ltz v6, :cond_0 */
/* iget-wide v6, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeStop:J */
/* cmp-long v6, v4, v6 */
/* if-gtz v6, :cond_0 */
/* .line 492 */
java.lang.Long .valueOf ( v4,v5 );
v7 = this.mRecordMockSchedule;
java.lang.Long .valueOf ( v4,v5 );
/* check-cast v7, Ljava/lang/String; */
/* .line 494 */
} // .end local v4 # "key":J
} // :cond_0
/* .line 496 */
} // :cond_1
/* new-instance v3, Ljava/util/LinkedHashMap; */
/* invoke-direct {v3}, Ljava/util/LinkedHashMap;-><init>()V */
/* .line 497 */
/* .local v3, "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Ljava/lang/String;>;" */
java.util.Map$Entry .comparingByKey ( );
/* new-instance v5, Lcom/android/server/location/GnssMockLocationOptImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v5, v3}, Lcom/android/server/location/GnssMockLocationOptImpl$$ExternalSyntheticLambda0;-><init>(Ljava/util/Map;)V */
/* .line 502 */
try { // :try_start_0
/* iget-wide v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeStart:J */
java.lang.Long .valueOf ( v4,v5 );
v4 = (( java.lang.String ) v0 ).equals ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
int v5 = 1; // const/4 v5, 0x1
int v6 = 2; // const/4 v6, 0x2
final String v7 = "ms"; // const-string v7, "ms"
if ( v4 != null) { // if-eqz v4, :cond_5
try { // :try_start_1
/* iget-wide v8, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeStop:J */
java.lang.Long .valueOf ( v8,v9 );
v4 = (( java.lang.String ) v0 ).equals ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_5
v0 = /* .line 503 */
/* if-ne v0, v6, :cond_3 */
/* .line 504 */
/* iget-wide v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeStop:J */
/* iget-wide v8, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeStart:J */
/* sub-long/2addr v4, v8 */
/* iput-wide v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeInterval:J */
/* .line 505 */
/* iget-boolean v0, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 506 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "always mock mode, time interval is "; // const-string v4, "always mock mode, time interval is "
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeInterval:J */
java.lang.String .valueOf ( v4,v5 );
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v0 );
/* .line 508 */
} // :cond_2
com.android.server.location.gnss.GnssEventTrackingStub .getInstance ( );
/* const-wide/16 v4, 0x1 */
/* .line 509 */
com.android.server.location.gnss.GnssEventTrackingStub .getInstance ( );
/* iget-wide v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeInterval:J */
/* .line 510 */
/* invoke-direct {p0}, Lcom/android/server/location/GnssMockLocationOptImpl;->resetCondition()V */
/* .line 511 */
return;
/* .line 513 */
} // :cond_3
/* invoke-direct {p0, v3, v5}, Lcom/android/server/location/GnssMockLocationOptImpl;->calculate(Ljava/util/Map;Z)V */
/* .line 514 */
/* iget-boolean v0, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 515 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "mock to mock mode, time interval is "; // const-string v4, "mock to mock mode, time interval is "
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeInterval:J */
java.lang.String .valueOf ( v4,v5 );
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v0 );
/* .line 517 */
} // :cond_4
com.android.server.location.gnss.GnssEventTrackingStub .getInstance ( );
/* iget-wide v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeInterval:J */
/* .line 518 */
/* invoke-direct {p0}, Lcom/android/server/location/GnssMockLocationOptImpl;->resetCondition()V */
/* .line 519 */
return;
/* .line 523 */
} // :cond_5
/* iget-wide v8, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeStart:J */
java.lang.Long .valueOf ( v8,v9 );
v4 = (( java.lang.String ) v0 ).equals ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
final String v8 = "0"; // const-string v8, "0"
if ( v4 != null) { // if-eqz v4, :cond_7
try { // :try_start_2
/* iget-wide v9, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeStop:J */
java.lang.Long .valueOf ( v9,v10 );
v4 = (( java.lang.String ) v8 ).equals ( v4 ); // invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_7
/* .line 524 */
/* invoke-direct {p0, v3, v5}, Lcom/android/server/location/GnssMockLocationOptImpl;->calculate(Ljava/util/Map;Z)V */
/* .line 525 */
/* iget-boolean v0, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z */
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 526 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "mock to nonmock mode, time interval is "; // const-string v4, "mock to nonmock mode, time interval is "
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeInterval:J */
java.lang.String .valueOf ( v4,v5 );
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v0 );
/* .line 528 */
} // :cond_6
com.android.server.location.gnss.GnssEventTrackingStub .getInstance ( );
/* iget-wide v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeInterval:J */
/* .line 529 */
/* invoke-direct {p0}, Lcom/android/server/location/GnssMockLocationOptImpl;->resetCondition()V */
/* .line 530 */
return;
/* .line 534 */
} // :cond_7
/* iget-wide v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeStart:J */
java.lang.Long .valueOf ( v4,v5 );
v4 = (( java.lang.String ) v8 ).equals ( v4 ); // invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v5 = 0; // const/4 v5, 0x0
if ( v4 != null) { // if-eqz v4, :cond_b
/* iget-wide v9, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeStop:J */
java.lang.Long .valueOf ( v9,v10 );
v4 = (( java.lang.String ) v8 ).equals ( v4 ); // invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_b
v0 = /* .line 535 */
/* if-ne v0, v6, :cond_9 */
/* .line 536 */
/* const-wide/16 v4, 0x0 */
/* iput-wide v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeInterval:J */
/* .line 537 */
/* iget-boolean v0, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z */
if ( v0 != null) { // if-eqz v0, :cond_8
/* .line 538 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "always nonmock mode, time interval is "; // const-string v4, "always nonmock mode, time interval is "
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeInterval:J */
java.lang.String .valueOf ( v4,v5 );
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v0 );
/* .line 539 */
} // :cond_8
/* invoke-direct {p0}, Lcom/android/server/location/GnssMockLocationOptImpl;->resetCondition()V */
/* .line 540 */
return;
/* .line 542 */
} // :cond_9
/* invoke-direct {p0, v3, v5}, Lcom/android/server/location/GnssMockLocationOptImpl;->calculate(Ljava/util/Map;Z)V */
/* .line 543 */
/* iget-boolean v0, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z */
if ( v0 != null) { // if-eqz v0, :cond_a
/* .line 544 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "nonmock to nonmock mode, time interval is "; // const-string v4, "nonmock to nonmock mode, time interval is "
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeInterval:J */
java.lang.String .valueOf ( v4,v5 );
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v0 );
/* .line 546 */
} // :cond_a
com.android.server.location.gnss.GnssEventTrackingStub .getInstance ( );
/* iget-wide v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeInterval:J */
/* .line 547 */
/* invoke-direct {p0}, Lcom/android/server/location/GnssMockLocationOptImpl;->resetCondition()V */
/* .line 548 */
return;
/* .line 552 */
} // :cond_b
/* iget-wide v9, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeStart:J */
java.lang.Long .valueOf ( v9,v10 );
v4 = (( java.lang.String ) v8 ).equals ( v4 ); // invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_d
/* iget-wide v8, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeStop:J */
java.lang.Long .valueOf ( v8,v9 );
v0 = (( java.lang.String ) v0 ).equals ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_d
/* .line 553 */
/* invoke-direct {p0, v3, v5}, Lcom/android/server/location/GnssMockLocationOptImpl;->calculate(Ljava/util/Map;Z)V */
/* .line 554 */
/* iget-boolean v0, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z */
if ( v0 != null) { // if-eqz v0, :cond_c
/* .line 555 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "nonmock to mock mode, time interval is "; // const-string v4, "nonmock to mock mode, time interval is "
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeInterval:J */
java.lang.String .valueOf ( v4,v5 );
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v0 );
/* .line 557 */
} // :cond_c
com.android.server.location.gnss.GnssEventTrackingStub .getInstance ( );
/* iget-wide v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeInterval:J */
/* .line 558 */
/* invoke-direct {p0}, Lcom/android/server/location/GnssMockLocationOptImpl;->resetCondition()V */
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 559 */
return;
/* .line 563 */
} // :cond_d
/* .line 561 */
/* :catch_0 */
/* move-exception v0 */
/* .line 562 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v4 );
/* .line 564 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
return;
} // .end method
private void getLatestMockAppName ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 608 */
/* if-nez p1, :cond_0 */
return;
/* .line 609 */
} // :cond_0
v0 = android.os.Binder .getCallingPid ( );
/* .line 610 */
/* .local v0, "pid":I */
final String v2 = "GnssMockLocationOpt"; // const-string v2, "GnssMockLocationOpt"
/* if-ne v0, v1, :cond_2 */
/* .line 611 */
/* iget-boolean v1, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "duplicate, no need to record, "; // const-string v3, "duplicate, no need to record, "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.String .valueOf ( v0 );
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v1 );
/* .line 612 */
} // :cond_1
return;
/* .line 614 */
} // :cond_2
/* .line 616 */
/* invoke-direct {p0, p1, v0}, Lcom/android/server/location/GnssMockLocationOptImpl;->getProcessName(Landroid/content/Context;I)Ljava/lang/String; */
this.mLatestMockApp = v1;
/* .line 617 */
/* iget-boolean v1, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z */
if ( v1 != null) { // if-eqz v1, :cond_3
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
v3 = this.mLatestMockApp;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " calling"; // const-string v3, " calling"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v1 );
/* .line 618 */
} // :cond_3
return;
} // .end method
private java.lang.String getProcessName ( android.content.Context p0, Integer p1 ) {
/* .locals 8 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "pid" # I */
/* .line 621 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_0 */
/* .line 622 */
} // :cond_0
final String v1 = "activity"; // const-string v1, "activity"
(( android.content.Context ) p1 ).getSystemService ( v1 ); // invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v1, Landroid/app/ActivityManager; */
/* .line 623 */
/* .local v1, "activityManager":Landroid/app/ActivityManager; */
(( android.app.ActivityManager ) v1 ).getRunningAppProcesses ( ); // invoke-virtual {v1}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;
/* .line 624 */
/* .local v2, "list":Ljava/util/List; */
/* .line 625 */
/* .local v3, "i":Ljava/util/Iterator; */
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 626 */
/* check-cast v4, Landroid/app/ActivityManager$RunningAppProcessInfo; */
/* .line 628 */
/* .local v4, "info":Landroid/app/ActivityManager$RunningAppProcessInfo; */
try { // :try_start_0
/* iget v5, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I */
/* if-ne v5, p2, :cond_1 */
/* .line 629 */
v0 = this.processName;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 633 */
} // :cond_1
/* .line 631 */
/* :catch_0 */
/* move-exception v5 */
/* .line 632 */
/* .local v5, "e":Ljava/lang/Exception; */
final String v6 = "GnssMockLocationOpt"; // const-string v6, "GnssMockLocationOpt"
(( java.lang.Exception ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/Exception;->toString()Ljava/lang/String;
android.util.Log .e ( v6,v7 );
/* .line 634 */
} // .end local v4 # "info":Landroid/app/ActivityManager$RunningAppProcessInfo;
} // .end local v5 # "e":Ljava/lang/Exception;
} // :goto_1
/* .line 635 */
} // :cond_2
} // .end method
private void grantMockLocationPermission ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 479 */
final String v0 = "appops"; // const-string v0, "appops"
(( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/AppOpsManager; */
/* .line 481 */
/* .local v0, "appOpsManager":Landroid/app/AppOpsManager; */
try { // :try_start_0
final String v1 = "android"; // const-string v1, "android"
int v2 = 0; // const/4 v2, 0x0
/* const/16 v3, 0x3a */
/* const/16 v4, 0x3e8 */
(( android.app.AppOpsManager ) v0 ).setMode ( v3, v4, v1, v2 ); // invoke-virtual {v0, v3, v4, v1, v2}, Landroid/app/AppOpsManager;->setMode(IILjava/lang/String;I)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 484 */
/* .line 482 */
/* :catch_0 */
/* move-exception v1 */
/* .line 483 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "grant mockLocationPermission failed, cause: "; // const-string v3, "grant mockLocationPermission failed, cause: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
android.util.Log .getStackTraceString ( v1 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "GnssMockLocationOpt"; // const-string v3, "GnssMockLocationOpt"
android.util.Log .e ( v3,v2 );
/* .line 485 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private Boolean isMockInfluence ( java.lang.String p0, com.android.server.location.provider.MockableLocationProvider p1 ) {
/* .locals 5 */
/* .param p1, "name" # Ljava/lang/String; */
/* .param p2, "provider" # Lcom/android/server/location/provider/MockableLocationProvider; */
/* .line 272 */
/* iget-boolean v0, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z */
final String v1 = "GnssMockLocationOpt"; // const-string v1, "GnssMockLocationOpt"
if ( v0 != null) { // if-eqz v0, :cond_0
android.util.Log .d ( v1,p1 );
/* .line 274 */
} // :cond_0
final String v0 = "gps"; // const-string v0, "gps"
v0 = (( java.lang.String ) v0 ).equalsIgnoreCase ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
int v2 = 0; // const/4 v2, 0x0
int v3 = 1; // const/4 v3, 0x1
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 275 */
this.mGpsProvider = p2;
/* .line 276 */
v0 = (( com.android.server.location.provider.MockableLocationProvider ) p2 ).isMock ( ); // invoke-virtual {p2}, Lcom/android/server/location/provider/MockableLocationProvider;->isMock()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 277 */
com.android.server.location.GnssMockLocationOptImpl.sGpsProviderStatus = (v3!= 0);
/* .line 279 */
} // :cond_1
com.android.server.location.GnssMockLocationOptImpl.sGpsProviderStatus = (v2!= 0);
/* .line 282 */
} // :cond_2
} // :goto_0
final String v0 = "fused"; // const-string v0, "fused"
v0 = (( java.lang.String ) v0 ).equalsIgnoreCase ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 283 */
this.mFusedProvider = p2;
/* .line 284 */
v0 = (( com.android.server.location.provider.MockableLocationProvider ) p2 ).isMock ( ); // invoke-virtual {p2}, Lcom/android/server/location/provider/MockableLocationProvider;->isMock()Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 285 */
com.android.server.location.GnssMockLocationOptImpl.sFusedProviderStatus = (v3!= 0);
/* .line 287 */
} // :cond_3
com.android.server.location.GnssMockLocationOptImpl.sFusedProviderStatus = (v2!= 0);
/* .line 290 */
} // :cond_4
} // :goto_1
final String v0 = "network"; // const-string v0, "network"
v0 = (( java.lang.String ) v0 ).equalsIgnoreCase ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 291 */
this.mNetworkProvider = p2;
/* .line 292 */
v0 = (( com.android.server.location.provider.MockableLocationProvider ) p2 ).isMock ( ); // invoke-virtual {p2}, Lcom/android/server/location/provider/MockableLocationProvider;->isMock()Z
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 293 */
com.android.server.location.GnssMockLocationOptImpl.sNetworkProviderStatus = (v3!= 0);
/* .line 295 */
} // :cond_5
com.android.server.location.GnssMockLocationOptImpl.sNetworkProviderStatus = (v2!= 0);
/* .line 298 */
} // :cond_6
} // :goto_2
/* sget-boolean v0, Lcom/android/server/location/GnssMockLocationOptImpl;->sGpsProviderStatus:Z */
if ( v0 != null) { // if-eqz v0, :cond_7
/* iget-boolean v0, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z */
if ( v0 != null) { // if-eqz v0, :cond_7
final String v0 = "gps is mock"; // const-string v0, "gps is mock"
android.util.Log .d ( v1,v0 );
/* .line 299 */
} // :cond_7
/* sget-boolean v0, Lcom/android/server/location/GnssMockLocationOptImpl;->sFusedProviderStatus:Z */
if ( v0 != null) { // if-eqz v0, :cond_8
/* iget-boolean v0, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z */
if ( v0 != null) { // if-eqz v0, :cond_8
final String v0 = "fused is mock"; // const-string v0, "fused is mock"
android.util.Log .d ( v1,v0 );
/* .line 300 */
} // :cond_8
/* sget-boolean v0, Lcom/android/server/location/GnssMockLocationOptImpl;->sNetworkProviderStatus:Z */
if ( v0 != null) { // if-eqz v0, :cond_9
/* iget-boolean v0, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z */
if ( v0 != null) { // if-eqz v0, :cond_9
final String v0 = "network is mock"; // const-string v0, "network is mock"
android.util.Log .d ( v1,v0 );
/* .line 301 */
} // :cond_9
/* sget-boolean v0, Lcom/android/server/location/GnssMockLocationOptImpl;->sGpsProviderStatus:Z */
/* if-nez v0, :cond_c */
/* sget-boolean v1, Lcom/android/server/location/GnssMockLocationOptImpl;->sFusedProviderStatus:Z */
/* if-nez v1, :cond_c */
/* sget-boolean v4, Lcom/android/server/location/GnssMockLocationOptImpl;->sNetworkProviderStatus:Z */
if ( v4 != null) { // if-eqz v4, :cond_a
/* .line 304 */
} // :cond_a
/* if-nez v0, :cond_b */
/* if-nez v1, :cond_b */
/* if-nez v4, :cond_b */
/* .line 305 */
/* .line 307 */
} // :cond_b
/* .line 302 */
} // :cond_c
} // :goto_3
} // .end method
private Boolean isNaviBat ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "name" # Ljava/lang/String; */
/* .line 261 */
v0 = com.android.server.location.GnssMockLocationOptImpl.sBatList;
v0 = (( java.util.HashSet ) v0 ).contains ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 262 */
/* iget-boolean v0, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 263 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " is navi app"; // const-string v1, " is navi app"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "GnssMockLocationOpt"; // const-string v1, "GnssMockLocationOpt"
android.util.Log .d ( v1,v0 );
/* .line 265 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* .line 267 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
static void lambda$dealingProcess$0 ( java.util.Map p0, java.util.Map$Entry p1 ) { //synthethic
/* .locals 2 */
/* .param p0, "result" # Ljava/util/Map; */
/* .param p1, "e" # Ljava/util/Map$Entry; */
/* .line 497 */
/* check-cast v0, Ljava/lang/Long; */
/* check-cast v1, Ljava/lang/String; */
return;
} // .end method
private Boolean loadCloudDataFromSP ( android.content.Context p0 ) {
/* .locals 6 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 658 */
int v0 = 0; // const/4 v0, 0x0
final String v1 = "GnssMockLocationOpt"; // const-string v1, "GnssMockLocationOpt"
/* if-nez p1, :cond_0 */
/* .line 659 */
try { // :try_start_0
final String v2 = "null context object"; // const-string v2, "null context object"
android.util.Log .d ( v1,v2 );
/* .line 660 */
/* .line 662 */
} // :cond_0
(( android.content.Context ) p1 ).createDeviceProtectedStorageContext ( ); // invoke-virtual {p1}, Landroid/content/Context;->createDeviceProtectedStorageContext()Landroid/content/Context;
/* .line 663 */
/* .local v2, "directBootContext":Landroid/content/Context; */
v3 = this.mGmoCloudFlagFile;
(( android.content.Context ) v2 ).getSharedPreferences ( v3, v0 ); // invoke-virtual {v2, v3, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/io/File;I)Landroid/content/SharedPreferences;
/* .line 664 */
/* .local v3, "editor":Landroid/content/SharedPreferences; */
final String v4 = "mEnableGmoControlStatus"; // const-string v4, "mEnableGmoControlStatus"
v0 = int v5 = 1; // const/4 v5, 0x1
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 668 */
} // .end local v2 # "directBootContext":Landroid/content/Context;
} // .end local v3 # "editor":Landroid/content/SharedPreferences;
/* .local v0, "status":Z */
/* nop */
/* .line 669 */
/* iput-boolean v5, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mAlreadyLoadControlFlag:Z */
/* .line 670 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "success to load mEnableGmoControlStatus, " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.String .valueOf ( v0 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v2 );
/* .line 671 */
/* .line 665 */
} // .end local v0 # "status":Z
/* :catch_0 */
/* move-exception v2 */
/* .line 666 */
/* .local v2, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "failed to load mEnableGmoControlStatus, "; // const-string v4, "failed to load mEnableGmoControlStatus, "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v3 );
/* .line 667 */
} // .end method
private void removeMock ( ) {
/* .locals 6 */
/* .line 400 */
/* const-class v0, Lcom/android/server/location/GnssMockLocationOptImpl; */
/* monitor-enter v0 */
/* .line 401 */
try { // :try_start_0
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 403 */
/* .local v1, "identity":J */
try { // :try_start_1
v3 = com.android.server.location.GnssMockLocationOptImpl.mContext;
/* invoke-direct {p0, v3}, Lcom/android/server/location/GnssMockLocationOptImpl;->grantMockLocationPermission(Landroid/content/Context;)V */
/* .line 405 */
v3 = this.mGpsProvider;
int v4 = 0; // const/4 v4, 0x0
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 406 */
v3 = (( com.android.server.location.provider.MockableLocationProvider ) v3 ).isMock ( ); // invoke-virtual {v3}, Lcom/android/server/location/provider/MockableLocationProvider;->isMock()Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 407 */
final String v3 = "GnssMockLocationOpt"; // const-string v3, "GnssMockLocationOpt"
final String v5 = "remove gps test provider"; // const-string v5, "remove gps test provider"
android.util.Log .d ( v3,v5 );
/* .line 408 */
v3 = this.mGpsProvider;
(( com.android.server.location.provider.MockableLocationProvider ) v3 ).setMockProvider ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/location/provider/MockableLocationProvider;->setMockProvider(Lcom/android/server/location/provider/MockLocationProvider;)V
/* .line 412 */
} // :cond_0
v3 = this.mFusedProvider;
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 413 */
v3 = (( com.android.server.location.provider.MockableLocationProvider ) v3 ).isMock ( ); // invoke-virtual {v3}, Lcom/android/server/location/provider/MockableLocationProvider;->isMock()Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 414 */
final String v3 = "GnssMockLocationOpt"; // const-string v3, "GnssMockLocationOpt"
final String v5 = "remove fused test provider"; // const-string v5, "remove fused test provider"
android.util.Log .d ( v3,v5 );
/* .line 415 */
v3 = this.mFusedProvider;
(( com.android.server.location.provider.MockableLocationProvider ) v3 ).setMockProvider ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/location/provider/MockableLocationProvider;->setMockProvider(Lcom/android/server/location/provider/MockLocationProvider;)V
/* .line 418 */
} // :cond_1
v3 = this.mNetworkProvider;
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 419 */
v3 = (( com.android.server.location.provider.MockableLocationProvider ) v3 ).isMock ( ); // invoke-virtual {v3}, Lcom/android/server/location/provider/MockableLocationProvider;->isMock()Z
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 420 */
final String v3 = "GnssMockLocationOpt"; // const-string v3, "GnssMockLocationOpt"
final String v5 = "remove network test provider"; // const-string v5, "remove network test provider"
android.util.Log .d ( v3,v5 );
/* .line 421 */
v3 = this.mNetworkProvider;
(( com.android.server.location.provider.MockableLocationProvider ) v3 ).setMockProvider ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/location/provider/MockableLocationProvider;->setMockProvider(Lcom/android/server/location/provider/MockLocationProvider;)V
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 427 */
} // :cond_2
try { // :try_start_2
android.os.Binder .restoreCallingIdentity ( v1,v2 );
/* .line 428 */
final String v3 = "GnssMockLocationOpt"; // const-string v3, "GnssMockLocationOpt"
final String v4 = "remove mock provider in system process"; // const-string v4, "remove mock provider in system process"
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* .line 427 */
/* :catchall_0 */
/* move-exception v3 */
/* .line 424 */
/* :catch_0 */
/* move-exception v3 */
/* .line 425 */
/* .local v3, "e":Ljava/lang/Exception; */
try { // :try_start_3
final String v4 = "GnssMockLocationOpt"; // const-string v4, "GnssMockLocationOpt"
(( java.lang.Exception ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;
android.util.Log .e ( v4,v5 );
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 427 */
} // .end local v3 # "e":Ljava/lang/Exception;
try { // :try_start_4
android.os.Binder .restoreCallingIdentity ( v1,v2 );
/* .line 428 */
final String v3 = "GnssMockLocationOpt"; // const-string v3, "GnssMockLocationOpt"
final String v4 = "remove mock provider in system process"; // const-string v4, "remove mock provider in system process"
} // :goto_0
android.util.Log .d ( v3,v4 );
/* .line 429 */
/* nop */
/* .line 430 */
int v3 = 1; // const/4 v3, 0x1
/* iput-boolean v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mRemovedBySystem:Z */
/* .line 431 */
/* const-wide/16 v3, 0x1 */
/* iput-wide v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mCountRemoveTimes:J */
/* .line 432 */
int v3 = 0; // const/4 v3, 0x0
/* iput-boolean v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mPermittedRunning:Z */
/* .line 433 */
} // .end local v1 # "identity":J
/* monitor-exit v0 */
/* .line 434 */
return;
/* .line 427 */
/* .restart local v1 # "identity":J */
} // :goto_1
android.os.Binder .restoreCallingIdentity ( v1,v2 );
/* .line 428 */
final String v4 = "GnssMockLocationOpt"; // const-string v4, "GnssMockLocationOpt"
final String v5 = "remove mock provider in system process"; // const-string v5, "remove mock provider in system process"
android.util.Log .d ( v4,v5 );
/* .line 429 */
/* nop */
} // .end local p0 # "this":Lcom/android/server/location/GnssMockLocationOptImpl;
/* throw v3 */
/* .line 433 */
} // .end local v1 # "identity":J
/* .restart local p0 # "this":Lcom/android/server/location/GnssMockLocationOptImpl; */
/* :catchall_1 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* throw v1 */
} // .end method
private void resetCondition ( ) {
/* .locals 3 */
/* .line 567 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeInterval:J */
/* .line 568 */
v2 = this.mRecordMockSchedule;
/* .line 569 */
/* iput-wide v0, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeStart:J */
/* .line 570 */
/* iput-wide v0, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeStop:J */
/* .line 571 */
return;
} // .end method
private void restoreMock ( ) {
/* .locals 19 */
/* .line 438 */
/* const-class v1, Lcom/android/server/location/GnssMockLocationOptImpl; */
/* monitor-enter v1 */
/* .line 440 */
int v2 = 1; // const/4 v2, 0x1
/* .line 441 */
/* .local v2, "forceRecoverAllTestProvider":Z */
try { // :try_start_0
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v3 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_2 */
/* .line 443 */
/* .local v3, "identity":J */
try { // :try_start_1
v0 = com.android.server.location.GnssMockLocationOptImpl.mContext;
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* move-object/from16 v5, p0 */
try { // :try_start_2
/* invoke-direct {v5, v0}, Lcom/android/server/location/GnssMockLocationOptImpl;->grantMockLocationPermission(Landroid/content/Context;)V */
/* .line 444 */
v0 = com.android.server.location.GnssMockLocationOptImpl.mContext;
final String v6 = "location"; // const-string v6, "location"
(( android.content.Context ) v0 ).getSystemService ( v6 ); // invoke-virtual {v0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* move-object v6, v0 */
/* check-cast v6, Landroid/location/LocationManager; */
/* .line 445 */
/* .local v6, "locationManager":Landroid/location/LocationManager; */
final String v7 = "gps"; // const-string v7, "gps"
int v8 = 0; // const/4 v8, 0x0
int v9 = 0; // const/4 v9, 0x0
int v10 = 0; // const/4 v10, 0x0
int v11 = 0; // const/4 v11, 0x0
int v12 = 1; // const/4 v12, 0x1
int v13 = 1; // const/4 v13, 0x1
int v14 = 1; // const/4 v14, 0x1
int v15 = 1; // const/4 v15, 0x1
/* const/16 v16, 0x1 */
/* invoke-virtual/range {v6 ..v16}, Landroid/location/LocationManager;->addTestProvider(Ljava/lang/String;ZZZZZZZII)V */
/* .line 449 */
final String v0 = "GnssMockLocationOpt"; // const-string v0, "GnssMockLocationOpt"
final String v7 = "recover gps test provider"; // const-string v7, "recover gps test provider"
android.util.Log .d ( v0,v7 );
/* .line 451 */
/* sget-boolean v0, Lcom/android/server/location/GnssMockLocationOptImpl;->sFusedProviderStatus:Z */
/* if-nez v0, :cond_1 */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 458 */
} // :cond_0
final String v0 = "GnssMockLocationOpt"; // const-string v0, "GnssMockLocationOpt"
final String v7 = "fused flag false"; // const-string v7, "fused flag false"
android.util.Log .d ( v0,v7 );
/* .line 452 */
} // :cond_1
} // :goto_0
final String v0 = "GnssMockLocationOpt"; // const-string v0, "GnssMockLocationOpt"
final String v7 = "recover fused test provider"; // const-string v7, "recover fused test provider"
android.util.Log .d ( v0,v7 );
/* .line 453 */
final String v9 = "fused"; // const-string v9, "fused"
int v10 = 0; // const/4 v10, 0x0
int v11 = 0; // const/4 v11, 0x0
int v12 = 0; // const/4 v12, 0x0
int v13 = 0; // const/4 v13, 0x0
int v14 = 1; // const/4 v14, 0x1
int v15 = 1; // const/4 v15, 0x1
/* const/16 v16, 0x1 */
/* const/16 v17, 0x1 */
/* const/16 v18, 0x1 */
/* move-object v8, v6 */
/* invoke-virtual/range {v8 ..v18}, Landroid/location/LocationManager;->addTestProvider(Ljava/lang/String;ZZZZZZZII)V */
/* .line 460 */
} // :goto_1
/* sget-boolean v0, Lcom/android/server/location/GnssMockLocationOptImpl;->sNetworkProviderStatus:Z */
/* if-nez v0, :cond_3 */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 467 */
} // :cond_2
final String v0 = "GnssMockLocationOpt"; // const-string v0, "GnssMockLocationOpt"
final String v7 = "network flag false"; // const-string v7, "network flag false"
android.util.Log .d ( v0,v7 );
/* .line 461 */
} // :cond_3
} // :goto_2
final String v0 = "GnssMockLocationOpt"; // const-string v0, "GnssMockLocationOpt"
final String v7 = "recover network test provider"; // const-string v7, "recover network test provider"
android.util.Log .d ( v0,v7 );
/* .line 462 */
final String v9 = "network"; // const-string v9, "network"
int v10 = 0; // const/4 v10, 0x0
int v11 = 0; // const/4 v11, 0x0
int v12 = 0; // const/4 v12, 0x0
int v13 = 0; // const/4 v13, 0x0
int v14 = 1; // const/4 v14, 0x1
int v15 = 1; // const/4 v15, 0x1
/* const/16 v16, 0x1 */
/* const/16 v17, 0x1 */
/* const/16 v18, 0x1 */
/* move-object v8, v6 */
/* invoke-virtual/range {v8 ..v18}, Landroid/location/LocationManager;->addTestProvider(Ljava/lang/String;ZZZZZZZII)V */
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 472 */
} // .end local v6 # "locationManager":Landroid/location/LocationManager;
} // :goto_3
try { // :try_start_3
android.os.Binder .restoreCallingIdentity ( v3,v4 );
/* .line 473 */
final String v0 = "GnssMockLocationOpt"; // const-string v0, "GnssMockLocationOpt"
final String v6 = "restore mock provider in system process"; // const-string v6, "restore mock provider in system process"
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_3 */
/* .line 472 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 469 */
/* :catch_0 */
/* move-exception v0 */
/* .line 472 */
/* :catchall_1 */
/* move-exception v0 */
/* move-object/from16 v5, p0 */
/* .line 469 */
/* :catch_1 */
/* move-exception v0 */
/* move-object/from16 v5, p0 */
/* .line 470 */
/* .local v0, "e":Ljava/lang/Exception; */
} // :goto_4
try { // :try_start_4
final String v6 = "GnssMockLocationOpt"; // const-string v6, "GnssMockLocationOpt"
(( java.lang.Exception ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;
android.util.Log .e ( v6,v7 );
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* .line 472 */
} // .end local v0 # "e":Ljava/lang/Exception;
try { // :try_start_5
android.os.Binder .restoreCallingIdentity ( v3,v4 );
/* .line 473 */
final String v0 = "GnssMockLocationOpt"; // const-string v0, "GnssMockLocationOpt"
final String v6 = "restore mock provider in system process"; // const-string v6, "restore mock provider in system process"
} // :goto_5
android.util.Log .d ( v0,v6 );
/* .line 474 */
/* nop */
/* .line 475 */
} // .end local v2 # "forceRecoverAllTestProvider":Z
} // .end local v3 # "identity":J
/* monitor-exit v1 */
/* .line 476 */
return;
/* .line 472 */
/* .restart local v2 # "forceRecoverAllTestProvider":Z */
/* .restart local v3 # "identity":J */
} // :goto_6
android.os.Binder .restoreCallingIdentity ( v3,v4 );
/* .line 473 */
final String v6 = "GnssMockLocationOpt"; // const-string v6, "GnssMockLocationOpt"
final String v7 = "restore mock provider in system process"; // const-string v7, "restore mock provider in system process"
android.util.Log .d ( v6,v7 );
/* .line 474 */
/* nop */
} // .end local p0 # "this":Lcom/android/server/location/GnssMockLocationOptImpl;
/* throw v0 */
/* .line 475 */
} // .end local v2 # "forceRecoverAllTestProvider":Z
} // .end local v3 # "identity":J
/* .restart local p0 # "this":Lcom/android/server/location/GnssMockLocationOptImpl; */
/* :catchall_2 */
/* move-exception v0 */
/* move-object/from16 v5, p0 */
} // :goto_7
/* monitor-exit v1 */
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_3 */
/* throw v0 */
/* :catchall_3 */
/* move-exception v0 */
} // .end method
private void saveCloudDataToSP ( android.content.Context p0, Boolean p1 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "status" # Z */
/* .line 641 */
final String v0 = "GnssMockLocationOpt"; // const-string v0, "GnssMockLocationOpt"
/* if-nez p1, :cond_0 */
/* .line 642 */
try { // :try_start_0
final String v1 = "null context object"; // const-string v1, "null context object"
android.util.Log .d ( v0,v1 );
/* .line 643 */
return;
/* .line 645 */
} // :cond_0
v1 = this.mGmoCloudFlagFile;
int v2 = 0; // const/4 v2, 0x0
(( android.content.Context ) p1 ).getSharedPreferences ( v1, v2 ); // invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/io/File;I)Landroid/content/SharedPreferences;
/* .line 646 */
/* .local v1, "editor":Landroid/content/SharedPreferences$Editor; */
final String v2 = "mEnableGmoControlStatus"; // const-string v2, "mEnableGmoControlStatus"
/* .line 647 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 651 */
} // .end local v1 # "editor":Landroid/content/SharedPreferences$Editor;
/* nop */
/* .line 652 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "success to save mEnableGmoControlStatus, " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.String .valueOf ( p2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v1 );
/* .line 653 */
return;
/* .line 648 */
/* :catch_0 */
/* move-exception v1 */
/* .line 649 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "failed to save mEnableGmoControlStatus, "; // const-string v3, "failed to save mEnableGmoControlStatus, "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v0,v2 );
/* .line 650 */
return;
} // .end method
/* # virtual methods */
public void handleNaviBatRegisteration ( android.content.Context p0, android.location.util.identity.CallerIdentity p1, java.lang.String p2, com.android.server.location.provider.MockableLocationProvider p3 ) {
/* .locals 7 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "callerIdentity" # Landroid/location/util/identity/CallerIdentity; */
/* .param p3, "name" # Ljava/lang/String; */
/* .param p4, "provider" # Lcom/android/server/location/provider/MockableLocationProvider; */
/* .line 152 */
int v0 = 0; // const/4 v0, 0x0
/* .line 153 */
/* .local v0, "isStart":Z */
/* iget-boolean v1, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mEnableGnssMockLocationOpt:Z */
/* if-nez v1, :cond_0 */
return;
/* .line 155 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* if-nez p1, :cond_1 */
/* .line 157 */
/* iget-boolean v2, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z */
if ( v2 != null) { // if-eqz v2, :cond_4
final String v2 = "GnssMockLocationOpt"; // const-string v2, "GnssMockLocationOpt"
final String v3 = "context is null"; // const-string v3, "context is null"
android.util.Log .d ( v2,v3 );
/* .line 159 */
} // :cond_1
/* .line 160 */
this.mProvider = p4;
/* .line 161 */
this.mName = p3;
/* .line 163 */
/* iget-boolean v2, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mAlreadyLoadControlFlag:Z */
/* if-nez v2, :cond_4 */
/* .line 164 */
v2 = this.mGmoVersion;
v2 = (( java.util.concurrent.atomic.AtomicInteger ) v2 ).get ( ); // invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I
int v3 = 1; // const/4 v3, 0x1
if ( v2 != null) { // if-eqz v2, :cond_2
/* move v2, v3 */
} // :cond_2
/* move v2, v1 */
/* .line 165 */
/* .local v2, "gmoVersionEnabled":Z */
} // :goto_0
v4 = com.android.server.location.GnssMockLocationOptImpl.mContext;
v4 = /* invoke-direct {p0, v4}, Lcom/android/server/location/GnssMockLocationOptImpl;->loadCloudDataFromSP(Landroid/content/Context;)Z */
/* .line 166 */
/* .local v4, "cloudControlFlag":Z */
if ( v4 != null) { // if-eqz v4, :cond_3
if ( v2 != null) { // if-eqz v2, :cond_3
} // :cond_3
/* move v3, v1 */
} // :goto_1
/* iput-boolean v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mEnableGmoControlStatus:Z */
/* .line 167 */
final String v3 = "GnssMockLocationOpt"; // const-string v3, "GnssMockLocationOpt"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "GMO version: "; // const-string v6, "GMO version: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.mGmoVersion;
v6 = (( java.util.concurrent.atomic.AtomicInteger ) v6 ).get ( ); // invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I
java.lang.String .valueOf ( v6 );
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = ", cloud control flag: "; // const-string v6, ", cloud control flag: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 168 */
java.lang.String .valueOf ( v4 );
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 167 */
android.util.Log .d ( v3,v5 );
/* .line 172 */
} // .end local v2 # "gmoVersionEnabled":Z
} // .end local v4 # "cloudControlFlag":Z
} // :cond_4
} // :goto_2
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "["; // const-string v3, "["
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p3 ); // invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "-"; // const-string v3, "-"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v3 = "]"; // const-string v3, "]"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 173 */
/* .local v2, "identity":Ljava/lang/String; */
(( android.location.util.identity.CallerIdentity ) p2 ).getPackageName ( ); // invoke-virtual {p2}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;
v3 = /* invoke-direct {p0, v3}, Lcom/android/server/location/GnssMockLocationOptImpl;->isNaviBat(Ljava/lang/String;)Z */
/* if-nez v3, :cond_5 */
return;
/* .line 174 */
} // :cond_5
/* iget-boolean v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z */
if ( v3 != null) { // if-eqz v3, :cond_6
/* .line 175 */
final String v3 = "GnssMockLocationOpt"; // const-string v3, "GnssMockLocationOpt"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "handleNaviBatRegisteration, add "; // const-string v5, "handleNaviBatRegisteration, add "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " done"; // const-string v5, " done"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v3,v4 );
/* .line 177 */
} // :cond_6
v3 = v3 = this.mBatUsing;
if ( v3 != null) { // if-eqz v3, :cond_7
/* .line 178 */
int v0 = 1; // const/4 v0, 0x1
/* .line 180 */
} // :cond_7
int v0 = 0; // const/4 v0, 0x0
/* .line 182 */
} // :goto_3
v3 = this.mBatUsing;
/* .line 184 */
/* if-nez v0, :cond_8 */
return;
/* .line 186 */
} // :cond_8
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v3 */
/* iput-wide v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeStart:J */
/* .line 188 */
/* sget-boolean v3, Lcom/android/server/location/GnssMockLocationOptImpl;->sMockFlagSetByUser:Z */
if ( v3 != null) { // if-eqz v3, :cond_a
/* .line 189 */
/* iget-boolean v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z */
if ( v3 != null) { // if-eqz v3, :cond_9
final String v3 = "GnssMockLocationOpt"; // const-string v3, "GnssMockLocationOpt"
final String v4 = "axis set begin with mock"; // const-string v4, "axis set begin with mock"
android.util.Log .d ( v3,v4 );
/* .line 190 */
} // :cond_9
v3 = this.mRecordMockSchedule;
/* iget-wide v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeStart:J */
java.lang.Long .valueOf ( v4,v5 );
final String v5 = "1"; // const-string v5, "1"
/* .line 192 */
} // :cond_a
/* iget-boolean v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z */
if ( v3 != null) { // if-eqz v3, :cond_b
final String v3 = "GnssMockLocationOpt"; // const-string v3, "GnssMockLocationOpt"
final String v4 = "axis set begin with nonmock"; // const-string v4, "axis set begin with nonmock"
android.util.Log .d ( v3,v4 );
/* .line 193 */
} // :cond_b
v3 = this.mRecordMockSchedule;
/* iget-wide v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeStart:J */
java.lang.Long .valueOf ( v4,v5 );
final String v5 = "0"; // const-string v5, "0"
/* .line 196 */
} // :goto_4
/* iget-boolean v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mEnableGmoControlStatus:Z */
/* if-nez v3, :cond_c */
return;
/* .line 197 */
} // :cond_c
/* const-class v3, Lcom/android/server/location/GnssMockLocationOptImpl; */
/* monitor-enter v3 */
/* .line 198 */
try { // :try_start_0
/* sget-boolean v4, Lcom/android/server/location/GnssMockLocationOptImpl;->sLastFlag:Z */
if ( v4 != null) { // if-eqz v4, :cond_d
/* .line 199 */
/* const-wide/16 v4, 0x0 */
/* iput-wide v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mCountRemoveTimes:J */
/* .line 200 */
/* invoke-direct {p0}, Lcom/android/server/location/GnssMockLocationOptImpl;->removeMock()V */
/* .line 201 */
final String v1 = "GnssMockLocationOpt"; // const-string v1, "GnssMockLocationOpt"
final String v4 = "begin navigation with mock, remove mock process"; // const-string v4, "begin navigation with mock, remove mock process"
android.util.Log .d ( v1,v4 );
/* .line 203 */
} // :cond_d
/* iput-boolean v1, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mRemovedBySystem:Z */
/* .line 204 */
final String v1 = "GnssMockLocationOpt"; // const-string v1, "GnssMockLocationOpt"
final String v4 = "begin navigation with nonmock, nothing to do"; // const-string v4, "begin navigation with nonmock, nothing to do"
android.util.Log .d ( v1,v4 );
/* .line 206 */
} // :goto_5
/* monitor-exit v3 */
/* .line 207 */
return;
/* .line 206 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v3 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void handleNaviBatUnregisteration ( android.location.util.identity.CallerIdentity p0, java.lang.String p1, com.android.server.location.provider.MockableLocationProvider p2 ) {
/* .locals 5 */
/* .param p1, "callerIdentity" # Landroid/location/util/identity/CallerIdentity; */
/* .param p2, "name" # Ljava/lang/String; */
/* .param p3, "provider" # Lcom/android/server/location/provider/MockableLocationProvider; */
/* .line 213 */
/* iget-boolean v0, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mEnableGnssMockLocationOpt:Z */
/* if-nez v0, :cond_0 */
return;
/* .line 214 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "["; // const-string v1, "["
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "-"; // const-string v1, "-"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = "]"; // const-string v1, "]"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 215 */
/* .local v0, "identity":Ljava/lang/String; */
(( android.location.util.identity.CallerIdentity ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Landroid/location/util/identity/CallerIdentity;->getPackageName()Ljava/lang/String;
v1 = /* invoke-direct {p0, v1}, Lcom/android/server/location/GnssMockLocationOptImpl;->isNaviBat(Ljava/lang/String;)Z */
/* if-nez v1, :cond_1 */
return;
/* .line 217 */
} // :cond_1
/* iget-boolean v1, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 218 */
final String v1 = "GnssMockLocationOpt"; // const-string v1, "GnssMockLocationOpt"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "handleNaviBatUnregisteration, remove "; // const-string v3, "handleNaviBatUnregisteration, remove "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " done"; // const-string v3, " done"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v2 );
/* .line 220 */
} // :cond_2
v1 = this.mBatUsing;
/* .line 222 */
v1 = v1 = this.mBatUsing;
/* if-nez v1, :cond_3 */
return;
/* .line 224 */
} // :cond_3
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v1 */
/* iput-wide v1, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeStop:J */
/* .line 226 */
/* sget-boolean v1, Lcom/android/server/location/GnssMockLocationOptImpl;->sMockFlagSetByUser:Z */
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 227 */
/* iget-boolean v1, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z */
if ( v1 != null) { // if-eqz v1, :cond_4
final String v1 = "GnssMockLocationOpt"; // const-string v1, "GnssMockLocationOpt"
final String v2 = "axis set end with mock"; // const-string v2, "axis set end with mock"
android.util.Log .d ( v1,v2 );
/* .line 228 */
} // :cond_4
v1 = this.mRecordMockSchedule;
/* iget-wide v2, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeStop:J */
java.lang.Long .valueOf ( v2,v3 );
final String v3 = "1"; // const-string v3, "1"
/* .line 230 */
} // :cond_5
/* iget-boolean v1, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z */
if ( v1 != null) { // if-eqz v1, :cond_6
final String v1 = "GnssMockLocationOpt"; // const-string v1, "GnssMockLocationOpt"
final String v2 = "axis set end with nonmock"; // const-string v2, "axis set end with nonmock"
android.util.Log .d ( v1,v2 );
/* .line 231 */
} // :cond_6
v1 = this.mRecordMockSchedule;
/* iget-wide v2, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mTimeStop:J */
java.lang.Long .valueOf ( v2,v3 );
final String v3 = "0"; // const-string v3, "0"
/* .line 234 */
} // :goto_0
/* const-class v1, Lcom/android/server/location/GnssMockLocationOptImpl; */
/* monitor-enter v1 */
/* .line 235 */
try { // :try_start_0
/* iget-boolean v2, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mEnableGmoControlStatus:Z */
/* if-nez v2, :cond_7 */
/* .line 237 */
/* invoke-direct {p0}, Lcom/android/server/location/GnssMockLocationOptImpl;->dealingProcess()V */
/* .line 238 */
/* monitor-exit v1 */
return;
/* .line 240 */
} // :cond_7
/* iget-boolean v2, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mChangeModeInNaviCondition:Z */
int v3 = 1; // const/4 v3, 0x1
/* if-nez v2, :cond_9 */
/* .line 241 */
/* iget-boolean v2, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mRemovedBySystem:Z */
int v4 = 0; // const/4 v4, 0x0
if ( v2 != null) { // if-eqz v2, :cond_8
/* .line 242 */
/* invoke-direct {p0}, Lcom/android/server/location/GnssMockLocationOptImpl;->restoreMock()V */
/* .line 243 */
com.android.server.location.GnssMockLocationOptImpl.sLastFlag = (v3!= 0);
/* .line 244 */
/* iput-boolean v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mRemovedBySystem:Z */
/* .line 245 */
final String v2 = "GnssMockLocationOpt"; // const-string v2, "GnssMockLocationOpt"
final String v4 = "restore begin mock, and no interrupt by third app"; // const-string v4, "restore begin mock, and no interrupt by third app"
android.util.Log .d ( v2,v4 );
/* .line 247 */
} // :cond_8
com.android.server.location.GnssMockLocationOptImpl.sLastFlag = (v4!= 0);
/* .line 250 */
} // :cond_9
/* invoke-direct {p0}, Lcom/android/server/location/GnssMockLocationOptImpl;->restoreMock()V */
/* .line 251 */
com.android.server.location.GnssMockLocationOptImpl.sLastFlag = (v3!= 0);
/* .line 252 */
final String v2 = "GnssMockLocationOpt"; // const-string v2, "GnssMockLocationOpt"
final String v4 = "force restore command"; // const-string v4, "force restore command"
android.util.Log .d ( v2,v4 );
/* .line 254 */
} // :goto_1
/* iput-boolean v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mPermittedRunning:Z */
/* .line 256 */
/* invoke-direct {p0}, Lcom/android/server/location/GnssMockLocationOptImpl;->dealingProcess()V */
/* .line 257 */
/* monitor-exit v1 */
/* .line 258 */
return;
/* .line 257 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
public Boolean recordOrderSchedule ( Boolean p0, java.lang.String p1, com.android.server.location.provider.MockableLocationProvider p2, com.android.server.location.provider.MockLocationProvider p3 ) {
/* .locals 8 */
/* .param p1, "flag" # Z */
/* .param p2, "name" # Ljava/lang/String; */
/* .param p3, "provider" # Lcom/android/server/location/provider/MockableLocationProvider; */
/* .param p4, "testProvider" # Lcom/android/server/location/provider/MockLocationProvider; */
/* .line 313 */
/* sget-boolean v0, Lcom/android/server/location/GnssMockLocationOptImpl;->sLastFlag:Z */
int v1 = 1; // const/4 v1, 0x1
int v2 = 0; // const/4 v2, 0x0
/* if-ne v0, p1, :cond_0 */
/* move v0, v1 */
} // :cond_0
/* move v0, v2 */
/* .line 316 */
/* .local v0, "done":Z */
} // :goto_0
/* iget-boolean v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mEnableGmoControlStatus:Z */
if ( v3 != null) { // if-eqz v3, :cond_12
/* .line 318 */
/* iget-boolean v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 319 */
v3 = android.os.Binder .getCallingPid ( );
/* .line 320 */
/* .local v3, "pid":I */
final String v4 = "GnssMockLocationOpt"; // const-string v4, "GnssMockLocationOpt"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "pid: "; // const-string v6, "pid: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.String .valueOf ( v3 );
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = ", android os identity: "; // const-string v6, ", android os identity: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = android.os.Process .myPid ( );
java.lang.String .valueOf ( v6 );
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v4,v5 );
/* .line 322 */
} // .end local v3 # "pid":I
} // :cond_1
v3 = com.android.server.location.GnssMockLocationOptImpl.mContext;
/* invoke-direct {p0, v3}, Lcom/android/server/location/GnssMockLocationOptImpl;->getLatestMockAppName(Landroid/content/Context;)V */
/* .line 325 */
/* if-nez p1, :cond_3 */
if ( p4 != null) { // if-eqz p4, :cond_3
v3 = v3 = this.mBatUsing;
/* if-nez v3, :cond_3 */
/* .line 326 */
/* iget-boolean v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z */
if ( v3 != null) { // if-eqz v3, :cond_2
final String v3 = "GnssMockLocationOpt"; // const-string v3, "GnssMockLocationOpt"
/* const-string/jumbo v4, "third app add test provider" */
android.util.Log .d ( v3,v4 );
/* .line 328 */
} // :cond_2
/* invoke-direct {p0, p2, p3}, Lcom/android/server/location/GnssMockLocationOptImpl;->isMockInfluence(Ljava/lang/String;Lcom/android/server/location/provider/MockableLocationProvider;)Z */
/* .line 330 */
/* iput-boolean v1, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mChangeModeInNaviCondition:Z */
/* .line 331 */
/* .line 334 */
} // :cond_3
/* if-nez p1, :cond_4 */
/* if-nez p4, :cond_4 */
v3 = v3 = this.mBatUsing;
/* if-nez v3, :cond_4 */
/* .line 337 */
/* iput-boolean v2, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mChangeModeInNaviCondition:Z */
/* .line 339 */
} // :cond_4
/* if-nez p1, :cond_6 */
if ( p4 != null) { // if-eqz p4, :cond_6
v3 = v3 = this.mBatUsing;
if ( v3 != null) { // if-eqz v3, :cond_6
/* .line 340 */
/* iget-boolean v2, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z */
if ( v2 != null) { // if-eqz v2, :cond_5
final String v2 = "GnssMockLocationOpt"; // const-string v2, "GnssMockLocationOpt"
final String v3 = "recover add test provider"; // const-string v3, "recover add test provider"
android.util.Log .d ( v2,v3 );
/* .line 341 */
} // :cond_5
/* .line 345 */
} // :cond_6
/* iget-boolean v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z */
if ( v3 != null) { // if-eqz v3, :cond_8
/* .line 346 */
final String v3 = "GnssMockLocationOpt"; // const-string v3, "GnssMockLocationOpt"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "flag: "; // const-string v5, "flag: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.String .valueOf ( p1 );
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = ", provider: "; // const-string v5, ", provider: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* if-nez p4, :cond_7 */
/* move v5, v1 */
} // :cond_7
/* move v5, v2 */
} // :goto_1
java.lang.String .valueOf ( v5 );
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v3,v4 );
/* .line 348 */
} // :cond_8
/* if-nez p1, :cond_e */
/* if-nez p4, :cond_e */
/* .line 349 */
/* iget-boolean v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z */
if ( v3 != null) { // if-eqz v3, :cond_9
/* .line 350 */
final String v3 = "GnssMockLocationOpt"; // const-string v3, "GnssMockLocationOpt"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "system process: " */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.mLatestMockApp;
/* const-string/jumbo v6, "system" */
v5 = (( java.lang.String ) v5 ).equalsIgnoreCase ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
java.lang.String .valueOf ( v5 );
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = ", fact process: "; // const-string v5, ", fact process: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.mLatestMockApp;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = ", judge flag: "; // const-string v5, ", judge flag: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v5, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mRemovedBySystem:Z */
/* .line 351 */
java.lang.String .valueOf ( v5 );
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 350 */
android.util.Log .d ( v3,v4 );
/* .line 354 */
} // :cond_9
/* const-class v3, Lcom/android/server/location/GnssMockLocationOptImpl; */
/* monitor-enter v3 */
/* .line 355 */
try { // :try_start_0
/* iget-boolean v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mRemovedBySystem:Z */
if ( v4 != null) { // if-eqz v4, :cond_b
/* .line 356 */
/* iget-wide v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mCountRemoveTimes:J */
/* const-wide/16 v6, 0x1 */
/* add-long/2addr v4, v6 */
/* iput-wide v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mCountRemoveTimes:J */
/* cmp-long v4, v4, v6 */
if ( v4 != null) { // if-eqz v4, :cond_b
/* .line 357 */
/* iget-boolean v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z */
if ( v4 != null) { // if-eqz v4, :cond_a
final String v4 = "GnssMockLocationOpt"; // const-string v4, "GnssMockLocationOpt"
final String v5 = "interrupt by third app"; // const-string v5, "interrupt by third app"
android.util.Log .d ( v4,v5 );
/* .line 358 */
} // :cond_a
/* iput-boolean v2, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mRemovedBySystem:Z */
/* .line 359 */
/* const-wide/16 v4, 0x0 */
/* iput-wide v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mCountRemoveTimes:J */
/* .line 362 */
} // :cond_b
/* iget-boolean v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mRemovedBySystem:Z */
/* if-nez v4, :cond_d */
/* .line 363 */
/* iget-boolean v4, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z */
if ( v4 != null) { // if-eqz v4, :cond_c
final String v4 = "GnssMockLocationOpt"; // const-string v4, "GnssMockLocationOpt"
final String v5 = "reset all provider"; // const-string v5, "reset all provider"
android.util.Log .d ( v4,v5 );
/* .line 364 */
} // :cond_c
com.android.server.location.GnssMockLocationOptImpl.sNetworkProviderStatus = (v2!= 0);
/* .line 365 */
com.android.server.location.GnssMockLocationOptImpl.sGpsProviderStatus = (v2!= 0);
/* .line 366 */
com.android.server.location.GnssMockLocationOptImpl.sFusedProviderStatus = (v2!= 0);
/* .line 368 */
} // :cond_d
/* monitor-exit v3 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v3 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 371 */
} // :cond_e
} // :goto_2
/* iget-boolean v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z */
if ( v3 != null) { // if-eqz v3, :cond_f
/* .line 372 */
final String v3 = "GnssMockLocationOpt"; // const-string v3, "GnssMockLocationOpt"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "bat null: "; // const-string v5, "bat null: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = v5 = this.mBatUsing;
java.lang.String .valueOf ( v5 );
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = ", permitted: "; // const-string v5, ", permitted: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v5, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mPermittedRunning:Z */
java.lang.String .valueOf ( v5 );
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v3,v4 );
/* .line 373 */
} // :cond_f
v3 = v3 = this.mBatUsing;
if ( v3 != null) { // if-eqz v3, :cond_10
/* iget-boolean v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mPermittedRunning:Z */
if ( v3 != null) { // if-eqz v3, :cond_10
/* .line 374 */
/* iget-boolean v3, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z */
if ( v3 != null) { // if-eqz v3, :cond_12
final String v3 = "GnssMockLocationOpt"; // const-string v3, "GnssMockLocationOpt"
final String v4 = "mock permitted"; // const-string v4, "mock permitted"
android.util.Log .d ( v3,v4 );
/* .line 376 */
} // :cond_10
/* iget-boolean v1, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->D:Z */
if ( v1 != null) { // if-eqz v1, :cond_11
final String v1 = "GnssMockLocationOpt"; // const-string v1, "GnssMockLocationOpt"
final String v3 = "mock denied"; // const-string v3, "mock denied"
android.util.Log .d ( v1,v3 );
/* .line 377 */
} // :cond_11
/* .line 381 */
} // :cond_12
} // :goto_3
/* if-nez v0, :cond_15 */
/* .line 382 */
if ( p1 != null) { // if-eqz p1, :cond_13
final String v3 = "1"; // const-string v3, "1"
} // :cond_13
final String v3 = "0"; // const-string v3, "0"
/* .line 383 */
/* .local v3, "flage":Ljava/lang/String; */
} // :goto_4
com.android.server.location.Order .setFlag ( v3 );
/* .line 385 */
final String v4 = "GnssMockLocationOpt"; // const-string v4, "GnssMockLocationOpt"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "mode switch to "; // const-string v6, "mode switch to "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v4,v5 );
/* .line 386 */
final String v4 = "1"; // const-string v4, "1"
v4 = (( java.lang.String ) v3 ).equals ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_14
/* .line 387 */
com.android.server.location.GnssMockLocationOptImpl.sMockFlagSetByUser = (v1!= 0);
/* .line 388 */
/* invoke-direct {p0, p2, p3}, Lcom/android/server/location/GnssMockLocationOptImpl;->isMockInfluence(Ljava/lang/String;Lcom/android/server/location/provider/MockableLocationProvider;)Z */
/* .line 390 */
} // :cond_14
final String v4 = "0"; // const-string v4, "0"
v4 = (( java.lang.String ) v3 ).equals ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_15
/* .line 391 */
com.android.server.location.GnssMockLocationOptImpl.sMockFlagSetByUser = (v2!= 0);
/* .line 394 */
} // .end local v3 # "flage":Ljava/lang/String;
} // :cond_15
com.android.server.location.GnssMockLocationOptImpl.sLastFlag = (p1!= 0);
/* .line 395 */
} // .end method
public void setMockLocationOptStatus ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "status" # Z */
/* .line 676 */
final String v0 = "GnssMockLocationOpt"; // const-string v0, "GnssMockLocationOpt"
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 677 */
v1 = com.android.server.location.GnssMockLocationOptImpl.mContext;
/* invoke-direct {p0, v1, p1}, Lcom/android/server/location/GnssMockLocationOptImpl;->saveCloudDataToSP(Landroid/content/Context;Z)V */
/* .line 678 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mEnableGmoControlStatus:Z */
/* .line 679 */
final String v1 = "enable GMO by cloud"; // const-string v1, "enable GMO by cloud"
android.util.Log .d ( v0,v1 );
/* .line 681 */
} // :cond_0
v1 = com.android.server.location.GnssMockLocationOptImpl.mContext;
/* invoke-direct {p0, v1, p1}, Lcom/android/server/location/GnssMockLocationOptImpl;->saveCloudDataToSP(Landroid/content/Context;Z)V */
/* .line 682 */
int v1 = 0; // const/4 v1, 0x0
/* iput-boolean v1, p0, Lcom/android/server/location/GnssMockLocationOptImpl;->mEnableGmoControlStatus:Z */
/* .line 683 */
final String v1 = "disable GMO by cloud"; // const-string v1, "disable GMO by cloud"
android.util.Log .d ( v0,v1 );
/* .line 685 */
} // :goto_0
return;
} // .end method
