.class public Lcom/android/server/location/GnssSessionInfo;
.super Ljava/lang/Object;
.source "GnssSessionInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/location/GnssSessionInfo$PQWP6;
    }
.end annotation


# static fields
.field public static final STATE_FIX:I = 0x2

.field public static final STATE_INIT:I = 0x0

.field public static final STATE_LOSE:I = 0x4

.field public static final STATE_SAVE:I = 0x5

.field public static final STATE_START:I = 0x1

.field public static final STATE_STOP:I = 0x3

.field public static final STATE_UNKNOWN:I = 0x64

.field private static final TAG:Ljava/lang/String; = "GnssSessionInfo"


# instance fields
.field private mAllFixed:I

.field private mCurrentState:I

.field private mEndTime:J

.field private mLostTimes:I

.field private mPDREngaged:I

.field private mPackName:Ljava/lang/String;

.field private mSAPEngaged:I

.field private mStartTime:J

.field private mTTFF:J

.field private mTop4B1Cn0Sum:D

.field private mTop4B1Cn0Times:I

.field private mTop4E1Cn0Sum:D

.field private mTop4E1Cn0Times:I

.field private mTop4G1Cn0Sum:D

.field private mTop4G1Cn0Times:I

.field private mTop4L1Cn0Sum:D

.field private mTop4L1Cn0Times:I

.field private mTop4L5Cn0Sum:D

.field private mTop4L5Cn0Times:I


# direct methods
.method public constructor <init>()V
    .locals 9

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/location/GnssSessionInfo;->mPackName:Ljava/lang/String;

    .line 8
    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/android/server/location/GnssSessionInfo;->mStartTime:J

    .line 9
    iput-wide v1, p0, Lcom/android/server/location/GnssSessionInfo;->mEndTime:J

    .line 10
    const-wide/16 v3, -0x1

    iput-wide v3, p0, Lcom/android/server/location/GnssSessionInfo;->mTTFF:J

    .line 11
    const/4 v5, 0x0

    iput v5, p0, Lcom/android/server/location/GnssSessionInfo;->mLostTimes:I

    .line 12
    iput v5, p0, Lcom/android/server/location/GnssSessionInfo;->mSAPEngaged:I

    .line 13
    iput v5, p0, Lcom/android/server/location/GnssSessionInfo;->mPDREngaged:I

    .line 14
    iput v5, p0, Lcom/android/server/location/GnssSessionInfo;->mAllFixed:I

    .line 15
    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4L1Cn0Sum:D

    .line 16
    iput v5, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4L1Cn0Times:I

    .line 17
    iput-wide v6, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4L5Cn0Sum:D

    .line 18
    iput v5, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4L5Cn0Times:I

    .line 19
    iput-wide v6, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4B1Cn0Sum:D

    .line 20
    iput v5, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4B1Cn0Times:I

    .line 21
    iput-wide v6, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4G1Cn0Sum:D

    .line 22
    iput v5, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4G1Cn0Times:I

    .line 23
    iput-wide v6, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4E1Cn0Sum:D

    .line 24
    iput v5, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4E1Cn0Times:I

    .line 32
    const/16 v8, 0x64

    iput v8, p0, Lcom/android/server/location/GnssSessionInfo;->mCurrentState:I

    .line 36
    iput v8, p0, Lcom/android/server/location/GnssSessionInfo;->mCurrentState:I

    .line 37
    iput-object v0, p0, Lcom/android/server/location/GnssSessionInfo;->mPackName:Ljava/lang/String;

    .line 38
    iput-wide v1, p0, Lcom/android/server/location/GnssSessionInfo;->mStartTime:J

    .line 39
    iput-wide v1, p0, Lcom/android/server/location/GnssSessionInfo;->mEndTime:J

    .line 40
    iput-wide v3, p0, Lcom/android/server/location/GnssSessionInfo;->mTTFF:J

    .line 41
    iput v5, p0, Lcom/android/server/location/GnssSessionInfo;->mLostTimes:I

    .line 42
    iput v5, p0, Lcom/android/server/location/GnssSessionInfo;->mSAPEngaged:I

    .line 43
    iput v5, p0, Lcom/android/server/location/GnssSessionInfo;->mPDREngaged:I

    .line 44
    iput v5, p0, Lcom/android/server/location/GnssSessionInfo;->mAllFixed:I

    .line 45
    iput-wide v6, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4L1Cn0Sum:D

    .line 46
    iput v5, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4L1Cn0Times:I

    .line 47
    iput-wide v6, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4L5Cn0Sum:D

    .line 48
    iput v5, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4L5Cn0Times:I

    .line 49
    iput-wide v6, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4B1Cn0Sum:D

    .line 50
    iput v5, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4B1Cn0Times:I

    .line 51
    iput-wide v6, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4G1Cn0Sum:D

    .line 52
    iput v5, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4G1Cn0Times:I

    .line 53
    iput-wide v6, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4E1Cn0Sum:D

    .line 54
    iput v5, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4E1Cn0Times:I

    .line 55
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JJJII)V
    .locals 3
    .param p1, "pack"    # Ljava/lang/String;
    .param p2, "starttime"    # J
    .param p4, "endtime"    # J
    .param p6, "ttff"    # J
    .param p8, "lost"    # I
    .param p9, "state"    # I

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/location/GnssSessionInfo;->mPackName:Ljava/lang/String;

    .line 8
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/location/GnssSessionInfo;->mStartTime:J

    .line 9
    iput-wide v0, p0, Lcom/android/server/location/GnssSessionInfo;->mEndTime:J

    .line 10
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/server/location/GnssSessionInfo;->mTTFF:J

    .line 11
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/location/GnssSessionInfo;->mLostTimes:I

    .line 12
    iput v0, p0, Lcom/android/server/location/GnssSessionInfo;->mSAPEngaged:I

    .line 13
    iput v0, p0, Lcom/android/server/location/GnssSessionInfo;->mPDREngaged:I

    .line 14
    iput v0, p0, Lcom/android/server/location/GnssSessionInfo;->mAllFixed:I

    .line 15
    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4L1Cn0Sum:D

    .line 16
    iput v0, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4L1Cn0Times:I

    .line 17
    iput-wide v1, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4L5Cn0Sum:D

    .line 18
    iput v0, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4L5Cn0Times:I

    .line 19
    iput-wide v1, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4B1Cn0Sum:D

    .line 20
    iput v0, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4B1Cn0Times:I

    .line 21
    iput-wide v1, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4G1Cn0Sum:D

    .line 22
    iput v0, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4G1Cn0Times:I

    .line 23
    iput-wide v1, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4E1Cn0Sum:D

    .line 24
    iput v0, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4E1Cn0Times:I

    .line 32
    const/16 v0, 0x64

    iput v0, p0, Lcom/android/server/location/GnssSessionInfo;->mCurrentState:I

    .line 58
    iput-object p1, p0, Lcom/android/server/location/GnssSessionInfo;->mPackName:Ljava/lang/String;

    .line 59
    iput-wide p2, p0, Lcom/android/server/location/GnssSessionInfo;->mStartTime:J

    .line 60
    iput-wide p4, p0, Lcom/android/server/location/GnssSessionInfo;->mEndTime:J

    .line 61
    iput-wide p6, p0, Lcom/android/server/location/GnssSessionInfo;->mTTFF:J

    .line 62
    iput p8, p0, Lcom/android/server/location/GnssSessionInfo;->mLostTimes:I

    .line 63
    iput p9, p0, Lcom/android/server/location/GnssSessionInfo;->mCurrentState:I

    .line 64
    return-void
.end method

.method private getCurrentTime()J
    .locals 2

    .line 136
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public checkValidity()Z
    .locals 7

    .line 127
    iget v0, p0, Lcom/android/server/location/GnssSessionInfo;->mCurrentState:I

    const-wide/16 v1, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-ne v0, v4, :cond_0

    iget-wide v5, p0, Lcom/android/server/location/GnssSessionInfo;->mStartTime:J

    cmp-long v5, v5, v1

    if-nez v5, :cond_0

    return v3

    .line 128
    :cond_0
    const/4 v5, 0x3

    if-ne v0, v5, :cond_1

    iget-wide v5, p0, Lcom/android/server/location/GnssSessionInfo;->mEndTime:J

    cmp-long v1, v5, v1

    if-nez v1, :cond_1

    return v3

    .line 129
    :cond_1
    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    iget v1, p0, Lcom/android/server/location/GnssSessionInfo;->mLostTimes:I

    if-nez v1, :cond_2

    return v3

    .line 130
    :cond_2
    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    iget-wide v1, p0, Lcom/android/server/location/GnssSessionInfo;->mTTFF:J

    const-wide/16 v5, -0x1

    cmp-long v1, v1, v5

    if-nez v1, :cond_3

    return v3

    .line 131
    :cond_3
    const/16 v1, 0x64

    if-eq v0, v1, :cond_5

    if-nez v0, :cond_4

    goto :goto_0

    .line 132
    :cond_4
    return v4

    .line 131
    :cond_5
    :goto_0
    return v3
.end method

.method public getAllFixTimes()I
    .locals 1

    .line 164
    iget v0, p0, Lcom/android/server/location/GnssSessionInfo;->mAllFixed:I

    return v0
.end method

.method public getB1Top4Cn0Mean()D
    .locals 5

    .line 190
    iget v0, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4B1Cn0Times:I

    if-eqz v0, :cond_0

    .line 191
    iget-wide v1, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4B1Cn0Sum:D

    int-to-double v3, v0

    div-double/2addr v1, v3

    return-wide v1

    .line 193
    :cond_0
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getE1Top4Cn0Mean()D
    .locals 5

    .line 204
    iget v0, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4E1Cn0Times:I

    if-eqz v0, :cond_0

    .line 205
    iget-wide v1, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4E1Cn0Sum:D

    int-to-double v3, v0

    div-double/2addr v1, v3

    return-wide v1

    .line 207
    :cond_0
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getG1Top4Cn0Mean()D
    .locals 5

    .line 197
    iget v0, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4G1Cn0Times:I

    if-eqz v0, :cond_0

    .line 198
    iget-wide v1, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4G1Cn0Sum:D

    int-to-double v3, v0

    div-double/2addr v1, v3

    return-wide v1

    .line 200
    :cond_0
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getL1Top4Cn0Mean()D
    .locals 5

    .line 176
    iget v0, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4L1Cn0Times:I

    if-eqz v0, :cond_0

    .line 177
    iget-wide v1, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4L1Cn0Sum:D

    int-to-double v3, v0

    div-double/2addr v1, v3

    return-wide v1

    .line 179
    :cond_0
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getL5Top4Cn0Mean()D
    .locals 5

    .line 183
    iget v0, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4L5Cn0Times:I

    if-eqz v0, :cond_0

    .line 184
    iget-wide v1, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4L5Cn0Sum:D

    int-to-double v3, v0

    div-double/2addr v1, v3

    return-wide v1

    .line 186
    :cond_0
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getLoseTimes()I
    .locals 1

    .line 160
    iget v0, p0, Lcom/android/server/location/GnssSessionInfo;->mLostTimes:I

    return v0
.end method

.method public getPackName()Ljava/lang/String;
    .locals 1

    .line 144
    iget-object v0, p0, Lcom/android/server/location/GnssSessionInfo;->mPackName:Ljava/lang/String;

    return-object v0
.end method

.method public getPdrTimes()I
    .locals 1

    .line 172
    iget v0, p0, Lcom/android/server/location/GnssSessionInfo;->mPDREngaged:I

    return v0
.end method

.method public getRunTime()J
    .locals 5

    .line 154
    iget-wide v0, p0, Lcom/android/server/location/GnssSessionInfo;->mEndTime:J

    iget-wide v2, p0, Lcom/android/server/location/GnssSessionInfo;->mStartTime:J

    cmp-long v4, v0, v2

    if-ltz v4, :cond_0

    .line 155
    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    return-wide v0

    .line 156
    :cond_0
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public getSapTimes()I
    .locals 1

    .line 168
    iget v0, p0, Lcom/android/server/location/GnssSessionInfo;->mSAPEngaged:I

    return v0
.end method

.method public getStartTimeInHour()I
    .locals 3

    .line 148
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 149
    .local v0, "c":Ljava/util/Calendar;
    iget-wide v1, p0, Lcom/android/server/location/GnssSessionInfo;->mStartTime:J

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 150
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    return v1
.end method

.method public getTtff()J
    .locals 2

    .line 140
    iget-wide v0, p0, Lcom/android/server/location/GnssSessionInfo;->mTTFF:J

    return-wide v0
.end method

.method public newSessionReset()V
    .locals 3

    .line 105
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/location/GnssSessionInfo;->mPackName:Ljava/lang/String;

    .line 106
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/location/GnssSessionInfo;->mStartTime:J

    .line 107
    iput-wide v0, p0, Lcom/android/server/location/GnssSessionInfo;->mEndTime:J

    .line 108
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/server/location/GnssSessionInfo;->mTTFF:J

    .line 109
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/location/GnssSessionInfo;->mLostTimes:I

    .line 110
    iput v0, p0, Lcom/android/server/location/GnssSessionInfo;->mCurrentState:I

    .line 111
    iput v0, p0, Lcom/android/server/location/GnssSessionInfo;->mSAPEngaged:I

    .line 112
    iput v0, p0, Lcom/android/server/location/GnssSessionInfo;->mPDREngaged:I

    .line 113
    iput v0, p0, Lcom/android/server/location/GnssSessionInfo;->mAllFixed:I

    .line 114
    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4L1Cn0Sum:D

    .line 115
    iput v0, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4L1Cn0Times:I

    .line 116
    iput-wide v1, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4L5Cn0Sum:D

    .line 117
    iput v0, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4L5Cn0Times:I

    .line 118
    iput-wide v1, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4B1Cn0Sum:D

    .line 119
    iput v0, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4B1Cn0Times:I

    .line 120
    iput-wide v1, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4G1Cn0Sum:D

    .line 121
    iput v0, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4G1Cn0Times:I

    .line 122
    iput-wide v1, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4E1Cn0Sum:D

    .line 123
    iput v0, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4E1Cn0Times:I

    .line 124
    return-void
.end method

.method public parseNmea(Ljava/lang/String;)V
    .locals 2
    .param p1, "nmea"    # Ljava/lang/String;

    .line 221
    iget v0, p0, Lcom/android/server/location/GnssSessionInfo;->mCurrentState:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 222
    return-void

    .line 223
    :cond_0
    iget v0, p0, Lcom/android/server/location/GnssSessionInfo;->mAllFixed:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/location/GnssSessionInfo;->mAllFixed:I

    .line 224
    const-string v0, "$PQWP6"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 225
    new-instance v0, Lcom/android/server/location/GnssSessionInfo$PQWP6;

    invoke-direct {v0, p0, p1}, Lcom/android/server/location/GnssSessionInfo$PQWP6;-><init>(Lcom/android/server/location/GnssSessionInfo;Ljava/lang/String;)V

    .line 226
    .local v0, "p6":Lcom/android/server/location/GnssSessionInfo$PQWP6;
    invoke-virtual {v0}, Lcom/android/server/location/GnssSessionInfo$PQWP6;->parse()V

    .line 227
    invoke-virtual {v0}, Lcom/android/server/location/GnssSessionInfo$PQWP6;->isSapEnaged()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 228
    iget v1, p0, Lcom/android/server/location/GnssSessionInfo;->mSAPEngaged:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/server/location/GnssSessionInfo;->mSAPEngaged:I

    .line 229
    :cond_1
    invoke-virtual {v0}, Lcom/android/server/location/GnssSessionInfo$PQWP6;->isPdrEngaged()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 230
    iget v1, p0, Lcom/android/server/location/GnssSessionInfo;->mPDREngaged:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/server/location/GnssSessionInfo;->mPDREngaged:I

    .line 232
    .end local v0    # "p6":Lcom/android/server/location/GnssSessionInfo$PQWP6;
    :cond_2
    return-void
.end method

.method public setB1Cn0(D)V
    .locals 2
    .param p1, "currentTop4AvgCn0"    # D

    .line 279
    iget v0, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4B1Cn0Times:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4B1Cn0Times:I

    .line 280
    iget-wide v0, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4B1Cn0Sum:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4B1Cn0Sum:D

    .line 281
    return-void
.end method

.method public setE1Cn0(D)V
    .locals 2
    .param p1, "currentTop4AvgCn0"    # D

    .line 289
    iget v0, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4E1Cn0Times:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4E1Cn0Times:I

    .line 290
    iget-wide v0, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4E1Cn0Sum:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4E1Cn0Sum:D

    .line 291
    return-void
.end method

.method public setEnd()V
    .locals 2

    .line 77
    invoke-direct {p0}, Lcom/android/server/location/GnssSessionInfo;->getCurrentTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/location/GnssSessionInfo;->mEndTime:J

    .line 78
    const/4 v0, 0x3

    iput v0, p0, Lcom/android/server/location/GnssSessionInfo;->mCurrentState:I

    .line 79
    return-void
.end method

.method public setG1Cn0(D)V
    .locals 2
    .param p1, "currentTop4AvgCn0"    # D

    .line 284
    iget v0, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4G1Cn0Times:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4G1Cn0Times:I

    .line 285
    iget-wide v0, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4G1Cn0Sum:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4G1Cn0Sum:D

    .line 286
    return-void
.end method

.method public setL1Cn0(D)V
    .locals 2
    .param p1, "currentTop4AvgCn0"    # D

    .line 269
    iget v0, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4L1Cn0Times:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4L1Cn0Times:I

    .line 270
    iget-wide v0, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4L1Cn0Sum:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4L1Cn0Sum:D

    .line 271
    return-void
.end method

.method public setL5Cn0(D)V
    .locals 2
    .param p1, "currentTop4AvgCn0"    # D

    .line 274
    iget v0, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4L5Cn0Times:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4L5Cn0Times:I

    .line 275
    iget-wide v0, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4L5Cn0Sum:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Lcom/android/server/location/GnssSessionInfo;->mTop4L5Cn0Sum:D

    .line 276
    return-void
.end method

.method public setLostTimes()V
    .locals 1

    .line 87
    iget v0, p0, Lcom/android/server/location/GnssSessionInfo;->mLostTimes:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/location/GnssSessionInfo;->mLostTimes:I

    .line 88
    const/4 v0, 0x4

    iput v0, p0, Lcom/android/server/location/GnssSessionInfo;->mCurrentState:I

    .line 89
    return-void
.end method

.method public setLostTimes(I)V
    .locals 0
    .param p1, "lostTimes"    # I

    .line 92
    iput p1, p0, Lcom/android/server/location/GnssSessionInfo;->mLostTimes:I

    .line 93
    return-void
.end method

.method public setStart()V
    .locals 2

    .line 72
    invoke-direct {p0}, Lcom/android/server/location/GnssSessionInfo;->getCurrentTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/location/GnssSessionInfo;->mStartTime:J

    .line 73
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/server/location/GnssSessionInfo;->mCurrentState:I

    .line 74
    return-void
.end method

.method public setStart(Ljava/lang/String;)V
    .locals 2
    .param p1, "pack"    # Ljava/lang/String;

    .line 66
    invoke-direct {p0}, Lcom/android/server/location/GnssSessionInfo;->getCurrentTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/location/GnssSessionInfo;->mStartTime:J

    .line 67
    iput-object p1, p0, Lcom/android/server/location/GnssSessionInfo;->mPackName:Ljava/lang/String;

    .line 68
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/server/location/GnssSessionInfo;->mCurrentState:I

    .line 69
    return-void
.end method

.method public setTtffAuto()V
    .locals 6

    .line 96
    invoke-direct {p0}, Lcom/android/server/location/GnssSessionInfo;->getCurrentTime()J

    move-result-wide v0

    .line 97
    .local v0, "cur":J
    iget-wide v2, p0, Lcom/android/server/location/GnssSessionInfo;->mStartTime:J

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    .line 98
    sub-long v2, v0, v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    iput-wide v2, p0, Lcom/android/server/location/GnssSessionInfo;->mTTFF:J

    goto :goto_0

    .line 100
    :cond_0
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/android/server/location/GnssSessionInfo;->mTTFF:J

    .line 101
    :goto_0
    const/4 v2, 0x2

    iput v2, p0, Lcom/android/server/location/GnssSessionInfo;->mCurrentState:I

    .line 102
    return-void
.end method

.method public setTtffManually(J)V
    .locals 1
    .param p1, "ttff"    # J

    .line 82
    iput-wide p1, p0, Lcom/android/server/location/GnssSessionInfo;->mTTFF:J

    .line 83
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/server/location/GnssSessionInfo;->mCurrentState:I

    .line 84
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 211
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "state = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/location/GnssSessionInfo;->mCurrentState:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " start time = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/server/location/GnssSessionInfo;->mStartTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ttff = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/server/location/GnssSessionInfo;->mTTFF:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " end time = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/server/location/GnssSessionInfo;->mEndTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " lose times = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/location/GnssSessionInfo;->mLostTimes:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "fixed number "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/location/GnssSessionInfo;->mAllFixed:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " sap number "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/location/GnssSessionInfo;->mSAPEngaged:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " pdr number "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/location/GnssSessionInfo;->mPDREngaged:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
