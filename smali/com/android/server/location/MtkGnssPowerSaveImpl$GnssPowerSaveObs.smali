.class Lcom/android/server/location/MtkGnssPowerSaveImpl$GnssPowerSaveObs;
.super Landroid/database/ContentObserver;
.source "MtkGnssPowerSaveImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/location/MtkGnssPowerSaveImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GnssPowerSaveObs"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/android/server/location/MtkGnssPowerSaveImpl;


# direct methods
.method public constructor <init>(Lcom/android/server/location/MtkGnssPowerSaveImpl;Landroid/content/Context;Landroid/os/Handler;)V
    .locals 0
    .param p2, "mContext"    # Landroid/content/Context;
    .param p3, "handler"    # Landroid/os/Handler;

    .line 555
    iput-object p1, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl$GnssPowerSaveObs;->this$0:Lcom/android/server/location/MtkGnssPowerSaveImpl;

    .line 556
    invoke-direct {p0, p3}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 557
    iput-object p2, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl$GnssPowerSaveObs;->mContext:Landroid/content/Context;

    .line 558
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 4
    .param p1, "selfChange"    # Z

    .line 562
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 563
    const-string v0, "persist.sys.gps.support_disable_l5_config"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 564
    return-void

    .line 566
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl$GnssPowerSaveObs;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "xiaomi_gnss_config_disable_l5"

    const/4 v2, 0x2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 569
    .local v0, "state":I
    iget-object v1, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl$GnssPowerSaveObs;->this$0:Lcom/android/server/location/MtkGnssPowerSaveImpl;

    invoke-static {v1}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->-$$Nest$fgetTAG(Lcom/android/server/location/MtkGnssPowerSaveImpl;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "xiaomi_mtk_gnss_power_save_feature had set to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 570
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 571
    iget-object v1, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl$GnssPowerSaveObs;->this$0:Lcom/android/server/location/MtkGnssPowerSaveImpl;

    invoke-virtual {v1}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->disableL5AndDisableGlp()V

    goto :goto_0

    .line 573
    :cond_1
    iget-object v1, p0, Lcom/android/server/location/MtkGnssPowerSaveImpl$GnssPowerSaveObs;->this$0:Lcom/android/server/location/MtkGnssPowerSaveImpl;

    invoke-virtual {v1}, Lcom/android/server/location/MtkGnssPowerSaveImpl;->enableL5AndEnableGlp()V

    .line 575
    :goto_0
    return-void
.end method
