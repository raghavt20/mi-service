public class com.android.server.location.ThreadPoolUtil {
	 /* .source "ThreadPoolUtil.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/location/ThreadPoolUtil$DefaultThreadFactory; */
	 /* } */
} // .end annotation
/* # static fields */
private static volatile com.android.server.location.ThreadPoolUtil mThreadPoolUtil;
/* # instance fields */
private Integer corePoolSize;
private java.util.concurrent.ThreadPoolExecutor executor;
private Long keepAliveTime;
private Integer maximumPoolSize;
private java.util.concurrent.TimeUnit unit;
/* # direct methods */
static com.android.server.location.ThreadPoolUtil ( ) {
	 /* .locals 1 */
	 /* .line 19 */
	 int v0 = 0; // const/4 v0, 0x0
	 return;
} // .end method
private com.android.server.location.ThreadPoolUtil ( ) {
	 /* .locals 2 */
	 /* .line 53 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 44 */
	 /* const-wide/16 v0, 0x1 */
	 /* iput-wide v0, p0, Lcom/android/server/location/ThreadPoolUtil;->keepAliveTime:J */
	 /* .line 46 */
	 v0 = java.util.concurrent.TimeUnit.HOURS;
	 this.unit = v0;
	 /* .line 54 */
	 java.lang.Runtime .getRuntime ( );
	 v0 = 	 (( java.lang.Runtime ) v0 ).availableProcessors ( ); // invoke-virtual {v0}, Ljava/lang/Runtime;->availableProcessors()I
	 /* mul-int/lit8 v0, v0, 0x2 */
	 /* add-int/lit8 v0, v0, 0x1 */
	 /* iput v0, p0, Lcom/android/server/location/ThreadPoolUtil;->corePoolSize:I */
	 /* .line 56 */
	 /* iput v0, p0, Lcom/android/server/location/ThreadPoolUtil;->maximumPoolSize:I */
	 /* .line 57 */
	 /* invoke-direct {p0}, Lcom/android/server/location/ThreadPoolUtil;->initThreadPool()V */
	 /* .line 58 */
	 return;
} // .end method
public static com.android.server.location.ThreadPoolUtil getInstance ( ) {
	 /* .locals 2 */
	 /* .line 23 */
	 v0 = com.android.server.location.ThreadPoolUtil.mThreadPoolUtil;
	 /* if-nez v0, :cond_1 */
	 /* .line 24 */
	 /* const-class v0, Lcom/android/server/location/ThreadPoolUtil; */
	 /* monitor-enter v0 */
	 /* .line 25 */
	 try { // :try_start_0
		 v1 = com.android.server.location.ThreadPoolUtil.mThreadPoolUtil;
		 /* if-nez v1, :cond_0 */
		 /* .line 26 */
		 /* new-instance v1, Lcom/android/server/location/ThreadPoolUtil; */
		 /* invoke-direct {v1}, Lcom/android/server/location/ThreadPoolUtil;-><init>()V */
		 /* .line 28 */
	 } // :cond_0
	 /* monitor-exit v0 */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 /* monitor-exit v0 */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* throw v1 */
	 /* .line 30 */
} // :cond_1
} // :goto_0
v0 = com.android.server.location.ThreadPoolUtil.mThreadPoolUtil;
} // .end method
private void initThreadPool ( ) {
/* .locals 11 */
/* .line 64 */
/* new-instance v9, Ljava/util/concurrent/ThreadPoolExecutor; */
/* iget v1, p0, Lcom/android/server/location/ThreadPoolUtil;->corePoolSize:I */
/* iget v2, p0, Lcom/android/server/location/ThreadPoolUtil;->maximumPoolSize:I */
/* iget-wide v3, p0, Lcom/android/server/location/ThreadPoolUtil;->keepAliveTime:J */
v5 = this.unit;
/* new-instance v6, Ljava/util/concurrent/LinkedBlockingQueue; */
/* invoke-direct {v6}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V */
/* new-instance v7, Lcom/android/server/location/ThreadPoolUtil$DefaultThreadFactory; */
final String v0 = "mi-gnss-pool-"; // const-string v0, "mi-gnss-pool-"
int v8 = 0; // const/4 v8, 0x0
int v10 = 5; // const/4 v10, 0x5
/* invoke-direct {v7, v10, v0, v8}, Lcom/android/server/location/ThreadPoolUtil$DefaultThreadFactory;-><init>(ILjava/lang/String;Lcom/android/server/location/ThreadPoolUtil$DefaultThreadFactory-IA;)V */
/* new-instance v8, Ljava/util/concurrent/ThreadPoolExecutor$AbortPolicy; */
/* invoke-direct {v8}, Ljava/util/concurrent/ThreadPoolExecutor$AbortPolicy;-><init>()V */
/* move-object v0, v9 */
/* invoke-direct/range {v0 ..v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;Ljava/util/concurrent/RejectedExecutionHandler;)V */
this.executor = v9;
/* .line 80 */
return;
} // .end method
/* # virtual methods */
public void execute ( java.lang.Runnable p0 ) {
/* .locals 1 */
/* .param p1, "runnable" # Ljava/lang/Runnable; */
/* .line 86 */
/* if-nez p1, :cond_0 */
/* .line 87 */
return;
/* .line 89 */
} // :cond_0
v0 = this.executor;
/* if-nez v0, :cond_1 */
/* .line 90 */
/* invoke-direct {p0}, Lcom/android/server/location/ThreadPoolUtil;->initThreadPool()V */
/* .line 92 */
} // :cond_1
v0 = this.executor;
(( java.util.concurrent.ThreadPoolExecutor ) v0 ).execute ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V
/* .line 93 */
return;
} // .end method
public void remove ( java.lang.Runnable p0 ) {
/* .locals 1 */
/* .param p1, "runnable" # Ljava/lang/Runnable; */
/* .line 99 */
/* if-nez p1, :cond_0 */
/* .line 100 */
return;
/* .line 102 */
} // :cond_0
v0 = this.executor;
/* if-nez v0, :cond_1 */
/* .line 103 */
/* invoke-direct {p0}, Lcom/android/server/location/ThreadPoolUtil;->initThreadPool()V */
/* .line 105 */
} // :cond_1
v0 = this.executor;
(( java.util.concurrent.ThreadPoolExecutor ) v0 ).remove ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/concurrent/ThreadPoolExecutor;->remove(Ljava/lang/Runnable;)Z
/* .line 106 */
return;
} // .end method
public void setCorePoolSize ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "corePoolSize" # I */
/* .line 109 */
/* iput p1, p0, Lcom/android/server/location/ThreadPoolUtil;->corePoolSize:I */
/* .line 110 */
return;
} // .end method
public void setKeepAliveTime ( Long p0 ) {
/* .locals 0 */
/* .param p1, "keepAliveTime" # J */
/* .line 117 */
/* iput-wide p1, p0, Lcom/android/server/location/ThreadPoolUtil;->keepAliveTime:J */
/* .line 118 */
return;
} // .end method
public void setMaximumPoolSize ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "maximumPoolSize" # I */
/* .line 113 */
/* iput p1, p0, Lcom/android/server/location/ThreadPoolUtil;->maximumPoolSize:I */
/* .line 114 */
return;
} // .end method
public void setUnit ( java.util.concurrent.TimeUnit p0 ) {
/* .locals 0 */
/* .param p1, "unit" # Ljava/util/concurrent/TimeUnit; */
/* .line 121 */
this.unit = p1;
/* .line 122 */
return;
} // .end method
