.class final Lcom/android/server/location/LocationExtCooperateImpl$SettingsObserver;
.super Landroid/database/ContentObserver;
.source "LocationExtCooperateImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/location/LocationExtCooperateImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SettingsObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/location/LocationExtCooperateImpl;


# direct methods
.method public constructor <init>(Lcom/android/server/location/LocationExtCooperateImpl;Landroid/os/Handler;)V
    .locals 0
    .param p2, "handler"    # Landroid/os/Handler;

    .line 294
    iput-object p1, p0, Lcom/android/server/location/LocationExtCooperateImpl$SettingsObserver;->this$0:Lcom/android/server/location/LocationExtCooperateImpl;

    .line 295
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 296
    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 7
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 300
    if-nez p2, :cond_0

    return-void

    .line 301
    :cond_0
    const-string v0, "satellite_state"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, -0x1

    const-string v3, "LocationExtCooperate"

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-eqz v1, :cond_3

    .line 302
    iget-object v1, p0, Lcom/android/server/location/LocationExtCooperateImpl$SettingsObserver;->this$0:Lcom/android/server/location/LocationExtCooperateImpl;

    invoke-static {v1}, Lcom/android/server/location/LocationExtCooperateImpl;->-$$Nest$fgetmContext(Lcom/android/server/location/LocationExtCooperateImpl;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, v0, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v5, :cond_1

    move v4, v5

    :cond_1
    move v0, v4

    .line 303
    .local v0, "state":Z
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Database SATELLITE_STATE changed:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    if-eqz v0, :cond_2

    .line 308
    invoke-static {}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub;->getInstance()Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/location/LocationExtCooperateImpl$SettingsObserver;->this$0:Lcom/android/server/location/LocationExtCooperateImpl;

    invoke-static {v2}, Lcom/android/server/location/LocationExtCooperateImpl;->-$$Nest$fgetsPkg(Lcom/android/server/location/LocationExtCooperateImpl;)Ljava/util/HashSet;

    move-result-object v2

    const-string v3, "MI_GNSS"

    invoke-virtual {v1, v2, v3}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub;->registerSatelliteCallMode(Ljava/util/HashSet;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 312
    :cond_2
    invoke-static {}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub;->getInstance()Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/location/gnss/exp/GnssBackgroundUsageOptStub;->unRegisterSatelliteCallMode()V

    goto/16 :goto_1

    .line 314
    .end local v0    # "state":Z
    :cond_3
    const-string v0, "gscoStatus"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 315
    iget-object v1, p0, Lcom/android/server/location/LocationExtCooperateImpl$SettingsObserver;->this$0:Lcom/android/server/location/LocationExtCooperateImpl;

    invoke-static {v1}, Lcom/android/server/location/LocationExtCooperateImpl;->-$$Nest$fgetmContext(Lcom/android/server/location/LocationExtCooperateImpl;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, v0, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v5, :cond_4

    move v1, v5

    goto :goto_0

    :cond_4
    move v1, v4

    .line 316
    .local v1, "state":Z
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "receive gscoStatus database change:"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    iget-object v2, p0, Lcom/android/server/location/LocationExtCooperateImpl$SettingsObserver;->this$0:Lcom/android/server/location/LocationExtCooperateImpl;

    invoke-static {v2}, Lcom/android/server/location/LocationExtCooperateImpl;->-$$Nest$fgetmIsSpecifiedDevice(Lcom/android/server/location/LocationExtCooperateImpl;)Z

    move-result v2

    if-nez v2, :cond_5

    if-eqz v1, :cond_5

    .line 319
    const-string v2, "Has Register Process Observer by database..."

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    iget-object v2, p0, Lcom/android/server/location/LocationExtCooperateImpl$SettingsObserver;->this$0:Lcom/android/server/location/LocationExtCooperateImpl;

    invoke-static {v2, v5}, Lcom/android/server/location/LocationExtCooperateImpl;->-$$Nest$fputmIsSpecifiedDevice(Lcom/android/server/location/LocationExtCooperateImpl;Z)V

    .line 321
    iget-object v2, p0, Lcom/android/server/location/LocationExtCooperateImpl$SettingsObserver;->this$0:Lcom/android/server/location/LocationExtCooperateImpl;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v2, v0, v3}, Lcom/android/server/location/LocationExtCooperateImpl;->-$$Nest$msaveCloudDataToSP(Lcom/android/server/location/LocationExtCooperateImpl;Ljava/lang/String;Ljava/lang/Object;)V

    .line 323
    iget-object v0, p0, Lcom/android/server/location/LocationExtCooperateImpl$SettingsObserver;->this$0:Lcom/android/server/location/LocationExtCooperateImpl;

    invoke-static {v0}, Lcom/android/server/location/LocationExtCooperateImpl;->-$$Nest$mregisterReceiver(Lcom/android/server/location/LocationExtCooperateImpl;)V

    goto :goto_2

    .line 324
    :cond_5
    iget-object v2, p0, Lcom/android/server/location/LocationExtCooperateImpl$SettingsObserver;->this$0:Lcom/android/server/location/LocationExtCooperateImpl;

    invoke-static {v2}, Lcom/android/server/location/LocationExtCooperateImpl;->-$$Nest$fgetmIsSpecifiedDevice(Lcom/android/server/location/LocationExtCooperateImpl;)Z

    move-result v2

    if-eqz v2, :cond_7

    if-nez v1, :cond_7

    .line 326
    const-string v2, "Has unRegister Process Observer by database..."

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 327
    iget-object v2, p0, Lcom/android/server/location/LocationExtCooperateImpl$SettingsObserver;->this$0:Lcom/android/server/location/LocationExtCooperateImpl;

    invoke-static {v2, v4}, Lcom/android/server/location/LocationExtCooperateImpl;->-$$Nest$fputmIsSpecifiedDevice(Lcom/android/server/location/LocationExtCooperateImpl;Z)V

    .line 328
    iget-object v2, p0, Lcom/android/server/location/LocationExtCooperateImpl$SettingsObserver;->this$0:Lcom/android/server/location/LocationExtCooperateImpl;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v2, v0, v3}, Lcom/android/server/location/LocationExtCooperateImpl;->-$$Nest$msaveCloudDataToSP(Lcom/android/server/location/LocationExtCooperateImpl;Ljava/lang/String;Ljava/lang/Object;)V

    .line 330
    iget-object v0, p0, Lcom/android/server/location/LocationExtCooperateImpl$SettingsObserver;->this$0:Lcom/android/server/location/LocationExtCooperateImpl;

    invoke-static {v0}, Lcom/android/server/location/LocationExtCooperateImpl;->-$$Nest$munRegisterReceiver(Lcom/android/server/location/LocationExtCooperateImpl;)V

    goto :goto_2

    .line 314
    .end local v1    # "state":Z
    :cond_6
    :goto_1
    nop

    .line 333
    :cond_7
    :goto_2
    return-void
.end method
