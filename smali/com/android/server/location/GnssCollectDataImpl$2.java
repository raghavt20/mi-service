class com.android.server.location.GnssCollectDataImpl$2 extends android.os.Handler {
	 /* .source "GnssCollectDataImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/location/GnssCollectDataImpl;->startHandlerThread()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.location.GnssCollectDataImpl this$0; //synthetic
/* # direct methods */
 com.android.server.location.GnssCollectDataImpl$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/location/GnssCollectDataImpl; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 790 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 4 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 792 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* .line 793 */
/* .local v0, "message":I */
/* packed-switch v0, :pswitch_data_0 */
/* goto/16 :goto_0 */
/* .line 825 */
/* :pswitch_0 */
v1 = this.this$0;
v2 = this.obj;
/* check-cast v2, Ljava/lang/Double; */
(( java.lang.Double ) v2 ).doubleValue ( ); // invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D
/* move-result-wide v2 */
com.android.server.location.GnssCollectDataImpl .-$$Nest$msetB1Cn0 ( v1,v2,v3 );
/* .line 826 */
/* .line 822 */
/* :pswitch_1 */
v1 = this.this$0;
v2 = this.obj;
/* check-cast v2, Ljava/lang/Double; */
(( java.lang.Double ) v2 ).doubleValue ( ); // invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D
/* move-result-wide v2 */
com.android.server.location.GnssCollectDataImpl .-$$Nest$msetL5Cn0 ( v1,v2,v3 );
/* .line 823 */
/* .line 819 */
/* :pswitch_2 */
v1 = this.this$0;
v2 = this.obj;
/* check-cast v2, Ljava/lang/Double; */
(( java.lang.Double ) v2 ).doubleValue ( ); // invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D
/* move-result-wide v2 */
com.android.server.location.GnssCollectDataImpl .-$$Nest$msetL1Cn0 ( v1,v2,v3 );
/* .line 820 */
/* .line 816 */
/* :pswitch_3 */
v1 = this.this$0;
v2 = this.obj;
/* check-cast v2, Ljava/lang/String; */
com.android.server.location.GnssCollectDataImpl .-$$Nest$mparseNmea ( v1,v2 );
/* .line 817 */
/* .line 812 */
/* :pswitch_4 */
v1 = this.this$0;
com.android.server.location.GnssCollectDataImpl .-$$Nest$msaveLog ( v1 );
/* .line 813 */
v1 = this.this$0;
int v2 = 5; // const/4 v2, 0x5
com.android.server.location.GnssCollectDataImpl .-$$Nest$msetCurrentState ( v1,v2 );
/* .line 814 */
/* .line 808 */
/* :pswitch_5 */
v1 = this.this$0;
com.android.server.location.GnssCollectDataImpl .-$$Nest$msaveLoseStatus ( v1 );
/* .line 809 */
v1 = this.this$0;
int v2 = 4; // const/4 v2, 0x4
com.android.server.location.GnssCollectDataImpl .-$$Nest$msetCurrentState ( v1,v2 );
/* .line 810 */
/* .line 803 */
/* :pswitch_6 */
v1 = this.this$0;
com.android.server.location.GnssCollectDataImpl .-$$Nest$msaveStopStatus ( v1 );
/* .line 804 */
v1 = this.this$0;
int v2 = 3; // const/4 v2, 0x3
com.android.server.location.GnssCollectDataImpl .-$$Nest$msetCurrentState ( v1,v2 );
/* .line 805 */
v1 = this.this$0;
com.android.server.location.GnssCollectDataImpl .-$$Nest$msaveState ( v1 );
/* .line 806 */
/* .line 799 */
/* :pswitch_7 */
v1 = this.this$0;
com.android.server.location.GnssCollectDataImpl .-$$Nest$msaveFixStatus ( v1 );
/* .line 800 */
v1 = this.this$0;
int v2 = 2; // const/4 v2, 0x2
com.android.server.location.GnssCollectDataImpl .-$$Nest$msetCurrentState ( v1,v2 );
/* .line 801 */
/* .line 795 */
/* :pswitch_8 */
v1 = this.this$0;
com.android.server.location.GnssCollectDataImpl .-$$Nest$msaveStartStatus ( v1 );
/* .line 796 */
v1 = this.this$0;
int v2 = 1; // const/4 v2, 0x1
com.android.server.location.GnssCollectDataImpl .-$$Nest$msetCurrentState ( v1,v2 );
/* .line 797 */
/* nop */
/* .line 831 */
} // :goto_0
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
