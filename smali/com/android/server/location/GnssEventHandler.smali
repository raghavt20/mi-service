.class public Lcom/android/server/location/GnssEventHandler;
.super Ljava/lang/Object;
.source "GnssEventHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/location/GnssEventHandler$NotifyManager;
    }
.end annotation


# static fields
.field private static CLOSE_BLUR_LOCATION:Ljava/lang/String; = null

.field private static final TAG:Ljava/lang/String; = "GnssNps"

.field private static mInstance:Lcom/android/server/location/GnssEventHandler;


# instance fields
.field private mAction:Landroid/app/Notification$Action;

.field private mAppContext:Landroid/content/Context;

.field private mBlur:Z

.field private mNotifyManager:Lcom/android/server/location/GnssEventHandler$NotifyManager;

.field private pkgName:Ljava/lang/String;


# direct methods
.method static bridge synthetic -$$Nest$fgetmAction(Lcom/android/server/location/GnssEventHandler;)Landroid/app/Notification$Action;
    .locals 0

    iget-object p0, p0, Lcom/android/server/location/GnssEventHandler;->mAction:Landroid/app/Notification$Action;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmAppContext(Lcom/android/server/location/GnssEventHandler;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/location/GnssEventHandler;->mAppContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmBlur(Lcom/android/server/location/GnssEventHandler;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/location/GnssEventHandler;->mBlur:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmNotifyManager(Lcom/android/server/location/GnssEventHandler;)Lcom/android/server/location/GnssEventHandler$NotifyManager;
    .locals 0

    iget-object p0, p0, Lcom/android/server/location/GnssEventHandler;->mNotifyManager:Lcom/android/server/location/GnssEventHandler$NotifyManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetpkgName(Lcom/android/server/location/GnssEventHandler;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/server/location/GnssEventHandler;->pkgName:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmAction(Lcom/android/server/location/GnssEventHandler;Landroid/app/Notification$Action;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/location/GnssEventHandler;->mAction:Landroid/app/Notification$Action;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmBlur(Lcom/android/server/location/GnssEventHandler;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/location/GnssEventHandler;->mBlur:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$misChineseLanguage(Lcom/android/server/location/GnssEventHandler;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/server/location/GnssEventHandler;->isChineseLanguage()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$sfgetCLOSE_BLUR_LOCATION()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/server/location/GnssEventHandler;->CLOSE_BLUR_LOCATION:Ljava/lang/String;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 39
    const-string v0, "com.miui.gnss.CLOSE_BLUR_LOCATION"

    sput-object v0, Lcom/android/server/location/GnssEventHandler;->CLOSE_BLUR_LOCATION:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Lcom/android/server/location/GnssEventHandler$NotifyManager;

    invoke-direct {v0, p0}, Lcom/android/server/location/GnssEventHandler$NotifyManager;-><init>(Lcom/android/server/location/GnssEventHandler;)V

    iput-object v0, p0, Lcom/android/server/location/GnssEventHandler;->mNotifyManager:Lcom/android/server/location/GnssEventHandler$NotifyManager;

    .line 41
    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/location/GnssEventHandler;->pkgName:Ljava/lang/String;

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/location/GnssEventHandler;->mBlur:Z

    .line 51
    iput-object p1, p0, Lcom/android/server/location/GnssEventHandler;->mAppContext:Landroid/content/Context;

    .line 52
    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/android/server/location/GnssEventHandler;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    const-class v0, Lcom/android/server/location/GnssEventHandler;

    monitor-enter v0

    .line 45
    :try_start_0
    sget-object v1, Lcom/android/server/location/GnssEventHandler;->mInstance:Lcom/android/server/location/GnssEventHandler;

    if-nez v1, :cond_0

    .line 46
    new-instance v1, Lcom/android/server/location/GnssEventHandler;

    invoke-direct {v1, p0}, Lcom/android/server/location/GnssEventHandler;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/android/server/location/GnssEventHandler;->mInstance:Lcom/android/server/location/GnssEventHandler;

    .line 47
    :cond_0
    sget-object v1, Lcom/android/server/location/GnssEventHandler;->mInstance:Lcom/android/server/location/GnssEventHandler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    .line 44
    .end local p0    # "context":Landroid/content/Context;
    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method private isChineseLanguage()Z
    .locals 2

    .line 94
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    .line 95
    .local v0, "language":Ljava/lang/String;
    const-string/jumbo v1, "zh_CN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    return v1
.end method


# virtual methods
.method public handleCallerName(Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .line 82
    const-string v0, "GnssNps"

    const-string v1, "APP caller name"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    iget-object v0, p0, Lcom/android/server/location/GnssEventHandler;->mNotifyManager:Lcom/android/server/location/GnssEventHandler$NotifyManager;

    invoke-virtual {v0, p1}, Lcom/android/server/location/GnssEventHandler$NotifyManager;->updateCallerName(Ljava/lang/String;)V

    .line 84
    return-void
.end method

.method public handleCallerName(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "event"    # Ljava/lang/String;

    .line 88
    const-string v0, "GnssNps"

    const-string v1, "APP caller name"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    iput-object p1, p0, Lcom/android/server/location/GnssEventHandler;->pkgName:Ljava/lang/String;

    .line 90
    iget-object v0, p0, Lcom/android/server/location/GnssEventHandler;->mNotifyManager:Lcom/android/server/location/GnssEventHandler$NotifyManager;

    invoke-virtual {v0, p2}, Lcom/android/server/location/GnssEventHandler$NotifyManager;->updateCallerName(Ljava/lang/String;)V

    .line 91
    return-void
.end method

.method public handleFix()V
    .locals 2

    .line 67
    const-string v0, "GnssNps"

    const-string v1, "fixed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    iget-object v0, p0, Lcom/android/server/location/GnssEventHandler;->mNotifyManager:Lcom/android/server/location/GnssEventHandler$NotifyManager;

    invoke-virtual {v0}, Lcom/android/server/location/GnssEventHandler$NotifyManager;->removeNotification()V

    .line 69
    return-void
.end method

.method public handleLose()V
    .locals 2

    .line 72
    const-string v0, "GnssNps"

    const-string v1, "lose location"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    iget-object v0, p0, Lcom/android/server/location/GnssEventHandler;->mNotifyManager:Lcom/android/server/location/GnssEventHandler$NotifyManager;

    invoke-virtual {v0}, Lcom/android/server/location/GnssEventHandler$NotifyManager;->showNotification()V

    .line 74
    return-void
.end method

.method public handleRecover()V
    .locals 2

    .line 77
    const-string v0, "GnssNps"

    const-string v1, "fix again"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    iget-object v0, p0, Lcom/android/server/location/GnssEventHandler;->mNotifyManager:Lcom/android/server/location/GnssEventHandler$NotifyManager;

    invoke-virtual {v0}, Lcom/android/server/location/GnssEventHandler$NotifyManager;->removeNotification()V

    .line 79
    return-void
.end method

.method public handleStart()V
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/android/server/location/GnssEventHandler;->mNotifyManager:Lcom/android/server/location/GnssEventHandler$NotifyManager;

    invoke-virtual {v0}, Lcom/android/server/location/GnssEventHandler$NotifyManager;->initNotification()V

    .line 56
    iget-object v0, p0, Lcom/android/server/location/GnssEventHandler;->mNotifyManager:Lcom/android/server/location/GnssEventHandler$NotifyManager;

    invoke-virtual {v0}, Lcom/android/server/location/GnssEventHandler$NotifyManager;->showNotification()V

    .line 57
    return-void
.end method

.method public handleStop()V
    .locals 2

    .line 60
    iget-boolean v0, p0, Lcom/android/server/location/GnssEventHandler;->mBlur:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 61
    return-void

    .line 63
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/GnssEventHandler;->mNotifyManager:Lcom/android/server/location/GnssEventHandler$NotifyManager;

    invoke-virtual {v0}, Lcom/android/server/location/GnssEventHandler$NotifyManager;->removeNotification()V

    .line 64
    return-void
.end method
