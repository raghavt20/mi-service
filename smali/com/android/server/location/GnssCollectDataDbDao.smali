.class public Lcom/android/server/location/GnssCollectDataDbDao;
.super Ljava/lang/Object;
.source "GnssCollectDataDbDao.java"


# static fields
.field private static final COLLECT_COLUMNS:[Ljava/lang/String;

.field private static final DEBUG:Z

.field public static final EMPTY_COLLECT:Lcom/android/server/location/GnssCollectData;

.field public static final EMPTY_COLLECT_LIST:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/server/location/GnssCollectData;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "GnssCollectDataDbDao"

.field private static final VERBOSE:Z

.field private static mGnssCollectDataDbDao:Lcom/android/server/location/GnssCollectDataDbDao;


# instance fields
.field private mGnssCollectDataDbHelper:Lcom/android/server/location/GnssCollectDataDbHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .line 42
    const/4 v0, 0x3

    const-string v1, "GnssCollectDataDbDao"

    invoke-static {v1, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/location/GnssCollectDataDbDao;->DEBUG:Z

    .line 43
    const/4 v0, 0x2

    invoke-static {v1, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/location/GnssCollectDataDbDao;->VERBOSE:Z

    .line 46
    const-string v1, "_id"

    const-string/jumbo v2, "startTime"

    const-string v3, "TTFF"

    const-string v4, "runTime"

    const-string v5, "loseTimes"

    const-string v6, "SAPNumber"

    const-string v7, "PDRNumber"

    const-string/jumbo v8, "totalNumber"

    const-string v9, "L1Top4MeanCn0"

    const-string v10, "L5Top4MeanCn0"

    const-string v11, "B1Top4MeanCn0"

    filled-new-array/range {v1 .. v11}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/location/GnssCollectDataDbDao;->COLLECT_COLUMNS:[Ljava/lang/String;

    .line 61
    new-instance v0, Lcom/android/server/location/GnssCollectData;

    invoke-direct {v0}, Lcom/android/server/location/GnssCollectData;-><init>()V

    sput-object v0, Lcom/android/server/location/GnssCollectDataDbDao;->EMPTY_COLLECT:Lcom/android/server/location/GnssCollectData;

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/location/GnssCollectDataDbDao;->EMPTY_COLLECT_LIST:Ljava/util/List;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    invoke-direct {p0, p1}, Lcom/android/server/location/GnssCollectDataDbDao;->getGnssCollectDataDbHelper(Landroid/content/Context;)Lcom/android/server/location/GnssCollectDataDbHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/location/GnssCollectDataDbDao;->mGnssCollectDataDbHelper:Lcom/android/server/location/GnssCollectDataDbHelper;

    .line 73
    return-void
.end method

.method private getGnssCollectDataDbHelper(Landroid/content/Context;)Lcom/android/server/location/GnssCollectDataDbHelper;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 76
    const-class v0, Lcom/android/server/location/GnssCollectDataDbDao;

    monitor-enter v0

    .line 77
    :try_start_0
    iget-object v1, p0, Lcom/android/server/location/GnssCollectDataDbDao;->mGnssCollectDataDbHelper:Lcom/android/server/location/GnssCollectDataDbHelper;

    if-nez v1, :cond_0

    .line 78
    invoke-static {p1}, Lcom/android/server/location/GnssCollectDataDbHelper;->getInstance(Landroid/content/Context;)Lcom/android/server/location/GnssCollectDataDbHelper;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/location/GnssCollectDataDbDao;->mGnssCollectDataDbHelper:Lcom/android/server/location/GnssCollectDataDbHelper;

    .line 80
    :cond_0
    iget-object v1, p0, Lcom/android/server/location/GnssCollectDataDbDao;->mGnssCollectDataDbHelper:Lcom/android/server/location/GnssCollectDataDbHelper;

    monitor-exit v0

    return-object v1

    .line 81
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/android/server/location/GnssCollectDataDbDao;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .line 65
    sget-object v0, Lcom/android/server/location/GnssCollectDataDbDao;->mGnssCollectDataDbDao:Lcom/android/server/location/GnssCollectDataDbDao;

    if-nez v0, :cond_0

    .line 66
    new-instance v0, Lcom/android/server/location/GnssCollectDataDbDao;

    invoke-direct {v0, p0}, Lcom/android/server/location/GnssCollectDataDbDao;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/server/location/GnssCollectDataDbDao;->mGnssCollectDataDbDao:Lcom/android/server/location/GnssCollectDataDbDao;

    .line 68
    :cond_0
    sget-object v0, Lcom/android/server/location/GnssCollectDataDbDao;->mGnssCollectDataDbDao:Lcom/android/server/location/GnssCollectDataDbDao;

    return-object v0
.end method


# virtual methods
.method public deleteAllGnssCollectData()V
    .locals 6

    .line 130
    const-class v0, Lcom/android/server/location/GnssCollectDataDbDao;

    monitor-enter v0

    .line 131
    :try_start_0
    iget-object v1, p0, Lcom/android/server/location/GnssCollectDataDbDao;->mGnssCollectDataDbHelper:Lcom/android/server/location/GnssCollectDataDbHelper;

    invoke-virtual {v1}, Lcom/android/server/location/GnssCollectDataDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 132
    .local v1, "sqLiteDatabaseWritable":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_1
    const-string v2, "GnssCollectData"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 133
    .local v2, "rowId":I
    const-string v3, "GnssCollectDataDbDao"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "deleteAllGnssCollectData "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz v2, :cond_0

    const-string/jumbo v5, "successfully"

    goto :goto_0

    :cond_0
    const-string v5, "failed"

    :goto_0
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 134
    .end local v2    # "rowId":I
    if-eqz v1, :cond_1

    :try_start_2
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 136
    .end local v1    # "sqLiteDatabaseWritable":Landroid/database/sqlite/SQLiteDatabase;
    :cond_1
    goto :goto_2

    .line 131
    .restart local v1    # "sqLiteDatabaseWritable":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_0
    move-exception v2

    if-eqz v1, :cond_2

    :try_start_3
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v3

    :try_start_4
    invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :cond_2
    :goto_1
    throw v2
    :try_end_4
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 137
    .end local v1    # "sqLiteDatabaseWritable":Landroid/database/sqlite/SQLiteDatabase;
    .restart local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :catchall_2
    move-exception v1

    goto :goto_3

    .line 134
    :catch_0
    move-exception v1

    .line 135
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_5
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 137
    .end local v1    # "e":Landroid/database/sqlite/SQLiteException;
    :goto_2
    monitor-exit v0

    .line 138
    return-void

    .line 137
    :goto_3
    monitor-exit v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v1
.end method

.method public filterB1Top4MeanCn0()Ljava/util/ArrayList;
    .locals 27
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 381
    const-class v1, Lcom/android/server/location/GnssCollectDataDbDao;

    monitor-enter v1

    .line 382
    move-object/from16 v2, p0

    :try_start_0
    iget-object v0, v2, Lcom/android/server/location/GnssCollectDataDbDao;->mGnssCollectDataDbHelper:Lcom/android/server/location/GnssCollectDataDbHelper;

    invoke-virtual {v0}, Lcom/android/server/location/GnssCollectDataDbHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 383
    .local v3, "sqLiteDatabaseReadable":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move-object v11, v0

    .line 384
    .local v11, "item":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v6, "B1Top4MeanCn0 > ? AND B1Top4MeanCn0 <= ?"

    .line 386
    .local v6, "selection":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v7, v0, [Ljava/lang/String;

    const-string v4, "0"

    const/4 v12, 0x0

    aput-object v4, v7, v12

    const-string v4, "20"

    const/4 v13, 0x1

    aput-object v4, v7, v13

    .line 387
    .local v7, "selectionArgs":[Ljava/lang/String;
    const-string v4, "GnssCollectData"

    sget-object v23, Lcom/android/server/location/GnssCollectDataDbDao;->COLLECT_COLUMNS:[Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v5, v23

    invoke-virtual/range {v3 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_8

    .line 389
    .local v4, "cursor":Landroid/database/Cursor;
    :try_start_1
    const-string v5, "B1T0-20"

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 390
    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/database/Cursor;

    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_6

    .line 391
    if-eqz v4, :cond_0

    :try_start_2
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 392
    .end local v4    # "cursor":Landroid/database/Cursor;
    :cond_0
    new-array v4, v0, [Ljava/lang/String;

    const-string v5, "20"

    aput-object v5, v4, v12

    const-string v5, "30"

    aput-object v5, v4, v13

    move-object/from16 v18, v4

    .line 393
    .end local v7    # "selectionArgs":[Ljava/lang/String;
    .local v18, "selectionArgs":[Ljava/lang/String;
    const-string v15, "GnssCollectData"

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object v14, v3

    move-object/from16 v16, v23

    move-object/from16 v17, v6

    invoke-virtual/range {v14 .. v21}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_8

    .line 395
    .restart local v4    # "cursor":Landroid/database/Cursor;
    :try_start_3
    const-string v5, "B1T20-30"

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 396
    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/database/Cursor;

    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    .line 397
    if-eqz v4, :cond_1

    :try_start_4
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 398
    .end local v4    # "cursor":Landroid/database/Cursor;
    :cond_1
    new-array v0, v0, [Ljava/lang/String;

    const-string v4, "30"

    aput-object v4, v0, v12

    const-string v4, "40"

    aput-object v4, v0, v13

    move-object/from16 v18, v0

    .line 399
    const-string v15, "GnssCollectData"

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object v14, v3

    move-object/from16 v16, v23

    move-object/from16 v17, v6

    invoke-virtual/range {v14 .. v21}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_8

    move-object v4, v0

    .line 401
    .restart local v4    # "cursor":Landroid/database/Cursor;
    :try_start_5
    const-string v0, "B1T30-40"

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 402
    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 403
    if-eqz v4, :cond_2

    :try_start_6
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 404
    .end local v4    # "cursor":Landroid/database/Cursor;
    :cond_2
    const-string v22, "B1Top4MeanCn0 > 40 "

    .line 405
    .end local v6    # "selection":Ljava/lang/String;
    .local v22, "selection":Ljava/lang/String;
    const-string v20, "GnssCollectData"

    const/4 v0, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    move-object/from16 v19, v3

    move-object/from16 v21, v23

    move-object/from16 v23, v0

    invoke-virtual/range {v19 .. v26}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_8

    move-object v4, v0

    .line 407
    .restart local v4    # "cursor":Landroid/database/Cursor;
    :try_start_7
    const-string v0, "B1T40+"

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 408
    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 409
    if-eqz v4, :cond_3

    :try_start_8
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 410
    .end local v4    # "cursor":Landroid/database/Cursor;
    :cond_3
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 411
    monitor-exit v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_8

    return-object v11

    .line 405
    .restart local v4    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    move-object v5, v0

    if-eqz v4, :cond_4

    :try_start_9
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v0

    move-object v6, v0

    :try_start_a
    invoke-virtual {v5, v6}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :cond_4
    :goto_0
    throw v5
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_8

    .line 399
    .end local v22    # "selection":Ljava/lang/String;
    .restart local v6    # "selection":Ljava/lang/String;
    .restart local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :catchall_2
    move-exception v0

    move-object v5, v0

    if-eqz v4, :cond_5

    :try_start_b
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    goto :goto_1

    :catchall_3
    move-exception v0

    move-object v7, v0

    :try_start_c
    invoke-virtual {v5, v7}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :cond_5
    :goto_1
    throw v5
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_8

    .line 393
    .restart local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :catchall_4
    move-exception v0

    move-object v5, v0

    if-eqz v4, :cond_6

    :try_start_d
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_5

    goto :goto_2

    :catchall_5
    move-exception v0

    move-object v7, v0

    :try_start_e
    invoke-virtual {v5, v7}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :cond_6
    :goto_2
    throw v5
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_8

    .line 387
    .end local v18    # "selectionArgs":[Ljava/lang/String;
    .restart local v7    # "selectionArgs":[Ljava/lang/String;
    .restart local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :catchall_6
    move-exception v0

    move-object v5, v0

    if-eqz v4, :cond_7

    :try_start_f
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_7

    goto :goto_3

    :catchall_7
    move-exception v0

    move-object v8, v0

    :try_start_10
    invoke-virtual {v5, v8}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :cond_7
    :goto_3
    throw v5

    .line 412
    .end local v3    # "sqLiteDatabaseReadable":Landroid/database/sqlite/SQLiteDatabase;
    .end local v4    # "cursor":Landroid/database/Cursor;
    .end local v6    # "selection":Ljava/lang/String;
    .end local v7    # "selectionArgs":[Ljava/lang/String;
    .end local v11    # "item":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :catchall_8
    move-exception v0

    monitor-exit v1
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_8

    throw v0
.end method

.method public filterL1Top4MeanCn0()Ljava/util/ArrayList;
    .locals 27
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 311
    const-class v1, Lcom/android/server/location/GnssCollectDataDbDao;

    monitor-enter v1

    .line 312
    move-object/from16 v2, p0

    :try_start_0
    iget-object v0, v2, Lcom/android/server/location/GnssCollectDataDbDao;->mGnssCollectDataDbHelper:Lcom/android/server/location/GnssCollectDataDbHelper;

    invoke-virtual {v0}, Lcom/android/server/location/GnssCollectDataDbHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 313
    .local v3, "sqLiteDatabaseReadable":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move-object v11, v0

    .line 314
    .local v11, "item":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v6, "L1Top4MeanCn0 > ? AND L1Top4MeanCn0 <= ?"

    .line 316
    .local v6, "selection":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v7, v0, [Ljava/lang/String;

    const-string v4, "0"

    const/4 v12, 0x0

    aput-object v4, v7, v12

    const-string v4, "20"

    const/4 v13, 0x1

    aput-object v4, v7, v13

    .line 317
    .local v7, "selectionArgs":[Ljava/lang/String;
    const-string v4, "GnssCollectData"

    sget-object v23, Lcom/android/server/location/GnssCollectDataDbDao;->COLLECT_COLUMNS:[Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v5, v23

    invoke-virtual/range {v3 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_8

    .line 319
    .local v4, "cursor":Landroid/database/Cursor;
    :try_start_1
    const-string v5, "L1T0-20"

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 320
    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/database/Cursor;

    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_6

    .line 321
    if-eqz v4, :cond_0

    :try_start_2
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 322
    .end local v4    # "cursor":Landroid/database/Cursor;
    :cond_0
    new-array v4, v0, [Ljava/lang/String;

    const-string v5, "20"

    aput-object v5, v4, v12

    const-string v5, "30"

    aput-object v5, v4, v13

    move-object/from16 v18, v4

    .line 323
    .end local v7    # "selectionArgs":[Ljava/lang/String;
    .local v18, "selectionArgs":[Ljava/lang/String;
    const-string v15, "GnssCollectData"

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object v14, v3

    move-object/from16 v16, v23

    move-object/from16 v17, v6

    invoke-virtual/range {v14 .. v21}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_8

    .line 325
    .restart local v4    # "cursor":Landroid/database/Cursor;
    :try_start_3
    const-string v5, "L1T20-30"

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 326
    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/database/Cursor;

    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    .line 327
    if-eqz v4, :cond_1

    :try_start_4
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 328
    .end local v4    # "cursor":Landroid/database/Cursor;
    :cond_1
    new-array v0, v0, [Ljava/lang/String;

    const-string v4, "30"

    aput-object v4, v0, v12

    const-string v4, "40"

    aput-object v4, v0, v13

    move-object/from16 v18, v0

    .line 329
    const-string v15, "GnssCollectData"

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object v14, v3

    move-object/from16 v16, v23

    move-object/from16 v17, v6

    invoke-virtual/range {v14 .. v21}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_8

    move-object v4, v0

    .line 331
    .restart local v4    # "cursor":Landroid/database/Cursor;
    :try_start_5
    const-string v0, "L1T30-40"

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 332
    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 333
    if-eqz v4, :cond_2

    :try_start_6
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 334
    .end local v4    # "cursor":Landroid/database/Cursor;
    :cond_2
    const-string v22, "L1Top4MeanCn0 > 40 "

    .line 335
    .end local v6    # "selection":Ljava/lang/String;
    .local v22, "selection":Ljava/lang/String;
    const-string v20, "GnssCollectData"

    const/4 v0, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    move-object/from16 v19, v3

    move-object/from16 v21, v23

    move-object/from16 v23, v0

    invoke-virtual/range {v19 .. v26}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_8

    move-object v4, v0

    .line 337
    .restart local v4    # "cursor":Landroid/database/Cursor;
    :try_start_7
    const-string v0, "L1T40+"

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 338
    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 339
    if-eqz v4, :cond_3

    :try_start_8
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 340
    .end local v4    # "cursor":Landroid/database/Cursor;
    :cond_3
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 341
    monitor-exit v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_8

    return-object v11

    .line 335
    .restart local v4    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    move-object v5, v0

    if-eqz v4, :cond_4

    :try_start_9
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v0

    move-object v6, v0

    :try_start_a
    invoke-virtual {v5, v6}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :cond_4
    :goto_0
    throw v5
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_8

    .line 329
    .end local v22    # "selection":Ljava/lang/String;
    .restart local v6    # "selection":Ljava/lang/String;
    .restart local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :catchall_2
    move-exception v0

    move-object v5, v0

    if-eqz v4, :cond_5

    :try_start_b
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    goto :goto_1

    :catchall_3
    move-exception v0

    move-object v7, v0

    :try_start_c
    invoke-virtual {v5, v7}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :cond_5
    :goto_1
    throw v5
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_8

    .line 323
    .restart local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :catchall_4
    move-exception v0

    move-object v5, v0

    if-eqz v4, :cond_6

    :try_start_d
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_5

    goto :goto_2

    :catchall_5
    move-exception v0

    move-object v7, v0

    :try_start_e
    invoke-virtual {v5, v7}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :cond_6
    :goto_2
    throw v5
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_8

    .line 317
    .end local v18    # "selectionArgs":[Ljava/lang/String;
    .restart local v7    # "selectionArgs":[Ljava/lang/String;
    .restart local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :catchall_6
    move-exception v0

    move-object v5, v0

    if-eqz v4, :cond_7

    :try_start_f
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_7

    goto :goto_3

    :catchall_7
    move-exception v0

    move-object v8, v0

    :try_start_10
    invoke-virtual {v5, v8}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :cond_7
    :goto_3
    throw v5

    .line 342
    .end local v3    # "sqLiteDatabaseReadable":Landroid/database/sqlite/SQLiteDatabase;
    .end local v4    # "cursor":Landroid/database/Cursor;
    .end local v6    # "selection":Ljava/lang/String;
    .end local v7    # "selectionArgs":[Ljava/lang/String;
    .end local v11    # "item":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :catchall_8
    move-exception v0

    monitor-exit v1
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_8

    throw v0
.end method

.method public filterL5Top4MeanCn0()Ljava/util/ArrayList;
    .locals 27
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 346
    const-class v1, Lcom/android/server/location/GnssCollectDataDbDao;

    monitor-enter v1

    .line 347
    move-object/from16 v2, p0

    :try_start_0
    iget-object v0, v2, Lcom/android/server/location/GnssCollectDataDbDao;->mGnssCollectDataDbHelper:Lcom/android/server/location/GnssCollectDataDbHelper;

    invoke-virtual {v0}, Lcom/android/server/location/GnssCollectDataDbHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 348
    .local v3, "sqLiteDatabaseReadable":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move-object v11, v0

    .line 349
    .local v11, "item":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v6, "L5Top4MeanCn0 > ? AND L5Top4MeanCn0 <= ?"

    .line 351
    .local v6, "selection":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v7, v0, [Ljava/lang/String;

    const-string v4, "0"

    const/4 v12, 0x0

    aput-object v4, v7, v12

    const-string v4, "20"

    const/4 v13, 0x1

    aput-object v4, v7, v13

    .line 352
    .local v7, "selectionArgs":[Ljava/lang/String;
    const-string v4, "GnssCollectData"

    sget-object v23, Lcom/android/server/location/GnssCollectDataDbDao;->COLLECT_COLUMNS:[Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v5, v23

    invoke-virtual/range {v3 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_8

    .line 354
    .local v4, "cursor":Landroid/database/Cursor;
    :try_start_1
    const-string v5, "L5T0-20"

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 355
    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/database/Cursor;

    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_6

    .line 356
    if-eqz v4, :cond_0

    :try_start_2
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 357
    .end local v4    # "cursor":Landroid/database/Cursor;
    :cond_0
    new-array v4, v0, [Ljava/lang/String;

    const-string v5, "20"

    aput-object v5, v4, v12

    const-string v5, "30"

    aput-object v5, v4, v13

    move-object/from16 v18, v4

    .line 358
    .end local v7    # "selectionArgs":[Ljava/lang/String;
    .local v18, "selectionArgs":[Ljava/lang/String;
    const-string v15, "GnssCollectData"

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object v14, v3

    move-object/from16 v16, v23

    move-object/from16 v17, v6

    invoke-virtual/range {v14 .. v21}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_8

    .line 360
    .restart local v4    # "cursor":Landroid/database/Cursor;
    :try_start_3
    const-string v5, "L5T20-30"

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 361
    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/database/Cursor;

    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    .line 362
    if-eqz v4, :cond_1

    :try_start_4
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 363
    .end local v4    # "cursor":Landroid/database/Cursor;
    :cond_1
    new-array v0, v0, [Ljava/lang/String;

    const-string v4, "30"

    aput-object v4, v0, v12

    const-string v4, "40"

    aput-object v4, v0, v13

    move-object/from16 v18, v0

    .line 364
    const-string v15, "GnssCollectData"

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object v14, v3

    move-object/from16 v16, v23

    move-object/from16 v17, v6

    invoke-virtual/range {v14 .. v21}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_8

    move-object v4, v0

    .line 366
    .restart local v4    # "cursor":Landroid/database/Cursor;
    :try_start_5
    const-string v0, "L5T30-40"

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 367
    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 368
    if-eqz v4, :cond_2

    :try_start_6
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 369
    .end local v4    # "cursor":Landroid/database/Cursor;
    :cond_2
    const-string v22, "L5Top4MeanCn0 > 40 "

    .line 370
    .end local v6    # "selection":Ljava/lang/String;
    .local v22, "selection":Ljava/lang/String;
    const-string v20, "GnssCollectData"

    const/4 v0, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    move-object/from16 v19, v3

    move-object/from16 v21, v23

    move-object/from16 v23, v0

    invoke-virtual/range {v19 .. v26}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_8

    move-object v4, v0

    .line 372
    .restart local v4    # "cursor":Landroid/database/Cursor;
    :try_start_7
    const-string v0, "L5T40+"

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 373
    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 374
    if-eqz v4, :cond_3

    :try_start_8
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 375
    .end local v4    # "cursor":Landroid/database/Cursor;
    :cond_3
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 376
    monitor-exit v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_8

    return-object v11

    .line 370
    .restart local v4    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    move-object v5, v0

    if-eqz v4, :cond_4

    :try_start_9
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v0

    move-object v6, v0

    :try_start_a
    invoke-virtual {v5, v6}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :cond_4
    :goto_0
    throw v5
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_8

    .line 364
    .end local v22    # "selection":Ljava/lang/String;
    .restart local v6    # "selection":Ljava/lang/String;
    .restart local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :catchall_2
    move-exception v0

    move-object v5, v0

    if-eqz v4, :cond_5

    :try_start_b
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    goto :goto_1

    :catchall_3
    move-exception v0

    move-object v7, v0

    :try_start_c
    invoke-virtual {v5, v7}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :cond_5
    :goto_1
    throw v5
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_8

    .line 358
    .restart local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :catchall_4
    move-exception v0

    move-object v5, v0

    if-eqz v4, :cond_6

    :try_start_d
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_5

    goto :goto_2

    :catchall_5
    move-exception v0

    move-object v7, v0

    :try_start_e
    invoke-virtual {v5, v7}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :cond_6
    :goto_2
    throw v5
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_8

    .line 352
    .end local v18    # "selectionArgs":[Ljava/lang/String;
    .restart local v7    # "selectionArgs":[Ljava/lang/String;
    .restart local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :catchall_6
    move-exception v0

    move-object v5, v0

    if-eqz v4, :cond_7

    :try_start_f
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_7

    goto :goto_3

    :catchall_7
    move-exception v0

    move-object v8, v0

    :try_start_10
    invoke-virtual {v5, v8}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :cond_7
    :goto_3
    throw v5

    .line 377
    .end local v3    # "sqLiteDatabaseReadable":Landroid/database/sqlite/SQLiteDatabase;
    .end local v4    # "cursor":Landroid/database/Cursor;
    .end local v6    # "selection":Ljava/lang/String;
    .end local v7    # "selectionArgs":[Ljava/lang/String;
    .end local v11    # "item":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :catchall_8
    move-exception v0

    monitor-exit v1
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_8

    throw v0
.end method

.method public filterLoseTimes()Ljava/util/ArrayList;
    .locals 26
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 244
    const-class v1, Lcom/android/server/location/GnssCollectDataDbDao;

    monitor-enter v1

    .line 245
    move-object/from16 v2, p0

    :try_start_0
    iget-object v0, v2, Lcom/android/server/location/GnssCollectDataDbDao;->mGnssCollectDataDbHelper:Lcom/android/server/location/GnssCollectDataDbHelper;

    invoke-virtual {v0}, Lcom/android/server/location/GnssCollectDataDbHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 246
    .local v3, "sqLiteDatabaseReadable":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move-object v11, v0

    .line 247
    .local v11, "item":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v6, "loseTimes == ? "

    .line 248
    .local v6, "selection":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v7, v0, [Ljava/lang/String;

    const-string v4, "0"

    const/4 v12, 0x0

    aput-object v4, v7, v12

    .line 249
    .local v7, "selectionArgs":[Ljava/lang/String;
    const-string v4, "GnssCollectData"

    sget-object v22, Lcom/android/server/location/GnssCollectDataDbDao;->COLLECT_COLUMNS:[Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v5, v22

    invoke-virtual/range {v3 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_a

    .line 251
    .local v4, "cursor":Landroid/database/Cursor;
    :try_start_1
    const-string v5, "LT0"

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 252
    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/database/Cursor;

    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_8

    .line 253
    if-eqz v4, :cond_0

    :try_start_2
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 254
    .end local v4    # "cursor":Landroid/database/Cursor;
    :cond_0
    new-array v4, v0, [Ljava/lang/String;

    const-string v5, "1"

    aput-object v5, v4, v12

    move-object/from16 v17, v4

    .line 255
    .end local v7    # "selectionArgs":[Ljava/lang/String;
    .local v17, "selectionArgs":[Ljava/lang/String;
    const-string v14, "GnssCollectData"

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object v13, v3

    move-object/from16 v15, v22

    move-object/from16 v16, v6

    invoke-virtual/range {v13 .. v20}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_a

    .line 257
    .restart local v4    # "cursor":Landroid/database/Cursor;
    :try_start_3
    const-string v5, "LT1"

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 258
    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/database/Cursor;

    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_6

    .line 259
    if-eqz v4, :cond_1

    :try_start_4
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 260
    .end local v4    # "cursor":Landroid/database/Cursor;
    :cond_1
    new-array v4, v0, [Ljava/lang/String;

    const-string v5, "2"

    aput-object v5, v4, v12

    move-object/from16 v17, v4

    .line 261
    const-string v14, "GnssCollectData"

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object v13, v3

    move-object/from16 v15, v22

    move-object/from16 v16, v6

    invoke-virtual/range {v13 .. v20}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_a

    .line 263
    .restart local v4    # "cursor":Landroid/database/Cursor;
    :try_start_5
    const-string v5, "LT2"

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 264
    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/database/Cursor;

    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    .line 265
    if-eqz v4, :cond_2

    :try_start_6
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 266
    .end local v4    # "cursor":Landroid/database/Cursor;
    :cond_2
    new-array v0, v0, [Ljava/lang/String;

    const-string v4, "3"

    aput-object v4, v0, v12

    move-object/from16 v17, v0

    .line 267
    const-string v14, "GnssCollectData"

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object v13, v3

    move-object/from16 v15, v22

    move-object/from16 v16, v6

    invoke-virtual/range {v13 .. v20}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_a

    move-object v4, v0

    .line 269
    .restart local v4    # "cursor":Landroid/database/Cursor;
    :try_start_7
    const-string v0, "LT3"

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 270
    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 271
    if-eqz v4, :cond_3

    :try_start_8
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 272
    .end local v4    # "cursor":Landroid/database/Cursor;
    :cond_3
    const-string v21, "loseTimes > 3 "

    .line 273
    .end local v6    # "selection":Ljava/lang/String;
    .local v21, "selection":Ljava/lang/String;
    const-string v19, "GnssCollectData"

    const/4 v0, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    move-object/from16 v18, v3

    move-object/from16 v20, v22

    move-object/from16 v22, v0

    invoke-virtual/range {v18 .. v25}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_a

    move-object v4, v0

    .line 275
    .restart local v4    # "cursor":Landroid/database/Cursor;
    :try_start_9
    const-string v0, "LT3+"

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 276
    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 277
    if-eqz v4, :cond_4

    :try_start_a
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 278
    .end local v4    # "cursor":Landroid/database/Cursor;
    :cond_4
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 279
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_a

    return-object v11

    .line 273
    .restart local v4    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    move-object v5, v0

    if-eqz v4, :cond_5

    :try_start_b
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v0

    move-object v6, v0

    :try_start_c
    invoke-virtual {v5, v6}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :cond_5
    :goto_0
    throw v5
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_a

    .line 267
    .end local v21    # "selection":Ljava/lang/String;
    .restart local v6    # "selection":Ljava/lang/String;
    .restart local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :catchall_2
    move-exception v0

    move-object v5, v0

    if-eqz v4, :cond_6

    :try_start_d
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    goto :goto_1

    :catchall_3
    move-exception v0

    move-object v7, v0

    :try_start_e
    invoke-virtual {v5, v7}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :cond_6
    :goto_1
    throw v5
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_a

    .line 261
    .restart local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :catchall_4
    move-exception v0

    move-object v5, v0

    if-eqz v4, :cond_7

    :try_start_f
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_5

    goto :goto_2

    :catchall_5
    move-exception v0

    move-object v7, v0

    :try_start_10
    invoke-virtual {v5, v7}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :cond_7
    :goto_2
    throw v5
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_a

    .line 255
    .restart local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :catchall_6
    move-exception v0

    move-object v5, v0

    if-eqz v4, :cond_8

    :try_start_11
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_7

    goto :goto_3

    :catchall_7
    move-exception v0

    move-object v7, v0

    :try_start_12
    invoke-virtual {v5, v7}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :cond_8
    :goto_3
    throw v5
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_a

    .line 249
    .end local v17    # "selectionArgs":[Ljava/lang/String;
    .restart local v7    # "selectionArgs":[Ljava/lang/String;
    .restart local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :catchall_8
    move-exception v0

    move-object v5, v0

    if-eqz v4, :cond_9

    :try_start_13
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_9

    goto :goto_4

    :catchall_9
    move-exception v0

    move-object v8, v0

    :try_start_14
    invoke-virtual {v5, v8}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :cond_9
    :goto_4
    throw v5

    .line 280
    .end local v3    # "sqLiteDatabaseReadable":Landroid/database/sqlite/SQLiteDatabase;
    .end local v4    # "cursor":Landroid/database/Cursor;
    .end local v6    # "selection":Ljava/lang/String;
    .end local v7    # "selectionArgs":[Ljava/lang/String;
    .end local v11    # "item":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :catchall_a
    move-exception v0

    monitor-exit v1
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_a

    throw v0
.end method

.method public filterRunTime()Ljava/util/ArrayList;
    .locals 27
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 203
    const-class v1, Lcom/android/server/location/GnssCollectDataDbDao;

    monitor-enter v1

    .line 204
    move-object/from16 v2, p0

    :try_start_0
    iget-object v0, v2, Lcom/android/server/location/GnssCollectDataDbDao;->mGnssCollectDataDbHelper:Lcom/android/server/location/GnssCollectDataDbHelper;

    invoke-virtual {v0}, Lcom/android/server/location/GnssCollectDataDbHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 205
    .local v3, "sqLiteDatabaseReadable":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move-object v11, v0

    .line 206
    .local v11, "item":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v6, "runTime > ? AND runTime <= ?"

    .line 208
    .local v6, "selection":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v7, v0, [Ljava/lang/String;

    const-string v4, "0"

    const/4 v12, 0x0

    aput-object v4, v7, v12

    const-string v4, "10"

    const/4 v13, 0x1

    aput-object v4, v7, v13

    .line 209
    .local v7, "selectionArgs":[Ljava/lang/String;
    const-string v4, "GnssCollectData"

    sget-object v23, Lcom/android/server/location/GnssCollectDataDbDao;->COLLECT_COLUMNS:[Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v5, v23

    invoke-virtual/range {v3 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_a

    .line 211
    .local v4, "cursor":Landroid/database/Cursor;
    :try_start_1
    const-string v5, "RT0-10"

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 212
    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/database/Cursor;

    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_8

    .line 213
    if-eqz v4, :cond_0

    :try_start_2
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 214
    .end local v4    # "cursor":Landroid/database/Cursor;
    :cond_0
    new-array v4, v0, [Ljava/lang/String;

    const-string v5, "10"

    aput-object v5, v4, v12

    const-string v5, "60"

    aput-object v5, v4, v13

    move-object/from16 v18, v4

    .line 215
    .end local v7    # "selectionArgs":[Ljava/lang/String;
    .local v18, "selectionArgs":[Ljava/lang/String;
    const-string v15, "GnssCollectData"

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object v14, v3

    move-object/from16 v16, v23

    move-object/from16 v17, v6

    invoke-virtual/range {v14 .. v21}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_a

    .line 217
    .restart local v4    # "cursor":Landroid/database/Cursor;
    :try_start_3
    const-string v5, "RT10-60"

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 218
    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/database/Cursor;

    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_6

    .line 219
    if-eqz v4, :cond_1

    :try_start_4
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 220
    .end local v4    # "cursor":Landroid/database/Cursor;
    :cond_1
    new-array v4, v0, [Ljava/lang/String;

    const-string v5, "60"

    aput-object v5, v4, v12

    const-string v5, "600"

    aput-object v5, v4, v13

    move-object/from16 v18, v4

    .line 221
    const-string v15, "GnssCollectData"

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object v14, v3

    move-object/from16 v16, v23

    move-object/from16 v17, v6

    invoke-virtual/range {v14 .. v21}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_a

    .line 223
    .restart local v4    # "cursor":Landroid/database/Cursor;
    :try_start_5
    const-string v5, "RT60-600"

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 224
    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/database/Cursor;

    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    .line 225
    if-eqz v4, :cond_2

    :try_start_6
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 226
    .end local v4    # "cursor":Landroid/database/Cursor;
    :cond_2
    new-array v0, v0, [Ljava/lang/String;

    const-string v4, "600"

    aput-object v4, v0, v12

    const-string v4, "3600"

    aput-object v4, v0, v13

    move-object/from16 v18, v0

    .line 227
    const-string v15, "GnssCollectData"

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object v14, v3

    move-object/from16 v16, v23

    move-object/from16 v17, v6

    invoke-virtual/range {v14 .. v21}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_a

    move-object v4, v0

    .line 229
    .restart local v4    # "cursor":Landroid/database/Cursor;
    :try_start_7
    const-string v0, "RT600-3600"

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 230
    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 231
    if-eqz v4, :cond_3

    :try_start_8
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 232
    .end local v4    # "cursor":Landroid/database/Cursor;
    :cond_3
    const-string v22, "runTime > 3600 "

    .line 233
    .end local v6    # "selection":Ljava/lang/String;
    .local v22, "selection":Ljava/lang/String;
    const-string v20, "GnssCollectData"

    const/4 v0, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    move-object/from16 v19, v3

    move-object/from16 v21, v23

    move-object/from16 v23, v0

    invoke-virtual/range {v19 .. v26}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_a

    move-object v4, v0

    .line 235
    .restart local v4    # "cursor":Landroid/database/Cursor;
    :try_start_9
    const-string v0, "RT3600+"

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 236
    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 237
    if-eqz v4, :cond_4

    :try_start_a
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 238
    .end local v4    # "cursor":Landroid/database/Cursor;
    :cond_4
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 239
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_a

    return-object v11

    .line 233
    .restart local v4    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    move-object v5, v0

    if-eqz v4, :cond_5

    :try_start_b
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v0

    move-object v6, v0

    :try_start_c
    invoke-virtual {v5, v6}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :cond_5
    :goto_0
    throw v5
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_a

    .line 227
    .end local v22    # "selection":Ljava/lang/String;
    .restart local v6    # "selection":Ljava/lang/String;
    .restart local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :catchall_2
    move-exception v0

    move-object v5, v0

    if-eqz v4, :cond_6

    :try_start_d
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    goto :goto_1

    :catchall_3
    move-exception v0

    move-object v7, v0

    :try_start_e
    invoke-virtual {v5, v7}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :cond_6
    :goto_1
    throw v5
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_a

    .line 221
    .restart local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :catchall_4
    move-exception v0

    move-object v5, v0

    if-eqz v4, :cond_7

    :try_start_f
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_5

    goto :goto_2

    :catchall_5
    move-exception v0

    move-object v7, v0

    :try_start_10
    invoke-virtual {v5, v7}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :cond_7
    :goto_2
    throw v5
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_a

    .line 215
    .restart local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :catchall_6
    move-exception v0

    move-object v5, v0

    if-eqz v4, :cond_8

    :try_start_11
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_7

    goto :goto_3

    :catchall_7
    move-exception v0

    move-object v7, v0

    :try_start_12
    invoke-virtual {v5, v7}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :cond_8
    :goto_3
    throw v5
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_a

    .line 209
    .end local v18    # "selectionArgs":[Ljava/lang/String;
    .restart local v7    # "selectionArgs":[Ljava/lang/String;
    .restart local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :catchall_8
    move-exception v0

    move-object v5, v0

    if-eqz v4, :cond_9

    :try_start_13
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_9

    goto :goto_4

    :catchall_9
    move-exception v0

    move-object v8, v0

    :try_start_14
    invoke-virtual {v5, v8}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :cond_9
    :goto_4
    throw v5

    .line 240
    .end local v3    # "sqLiteDatabaseReadable":Landroid/database/sqlite/SQLiteDatabase;
    .end local v4    # "cursor":Landroid/database/Cursor;
    .end local v6    # "selection":Ljava/lang/String;
    .end local v7    # "selectionArgs":[Ljava/lang/String;
    .end local v11    # "item":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :catchall_a
    move-exception v0

    monitor-exit v1
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_a

    throw v0
.end method

.method public filterStartTime()Ljava/util/ArrayList;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 144
    const-class v0, Lcom/android/server/location/GnssCollectDataDbDao;

    monitor-enter v0

    .line 145
    :try_start_0
    iget-object v1, p0, Lcom/android/server/location/GnssCollectDataDbDao;->mGnssCollectDataDbHelper:Lcom/android/server/location/GnssCollectDataDbHelper;

    invoke-virtual {v1}, Lcom/android/server/location/GnssCollectDataDbHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 146
    .local v2, "sqLiteDatabaseReadable":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 147
    .local v1, "item":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string/jumbo v5, "startTime == ? "

    .line 148
    .local v5, "selection":Ljava/lang/String;
    const/4 v3, 0x0

    move v10, v3

    .local v10, "i":I
    :goto_0
    const/16 v3, 0x18

    if-ge v10, v3, :cond_2

    .line 149
    const/4 v3, 0x1

    new-array v6, v3, [Ljava/lang/String;

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v6, v4

    .line 150
    .local v6, "selectionArgs":[Ljava/lang/String;
    const-string v3, "GnssCollectData"

    sget-object v4, Lcom/android/server/location/GnssCollectDataDbDao;->COLLECT_COLUMNS:[Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 152
    .local v3, "cursor":Landroid/database/Cursor;
    :try_start_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ST"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 153
    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/database/Cursor;

    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 154
    if-eqz v3, :cond_0

    :try_start_2
    invoke-interface {v3}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 148
    .end local v3    # "cursor":Landroid/database/Cursor;
    .end local v6    # "selectionArgs":[Ljava/lang/String;
    :cond_0
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 150
    .restart local v3    # "cursor":Landroid/database/Cursor;
    .restart local v6    # "selectionArgs":[Ljava/lang/String;
    :catchall_0
    move-exception v4

    if-eqz v3, :cond_1

    :try_start_3
    invoke-interface {v3}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v7

    :try_start_4
    invoke-virtual {v4, v7}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :cond_1
    :goto_1
    throw v4

    .line 156
    .end local v3    # "cursor":Landroid/database/Cursor;
    .end local v6    # "selectionArgs":[Ljava/lang/String;
    .end local v10    # "i":I
    .restart local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :cond_2
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 157
    monitor-exit v0

    return-object v1

    .line 158
    .end local v1    # "item":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v2    # "sqLiteDatabaseReadable":Landroid/database/sqlite/SQLiteDatabase;
    .end local v5    # "selection":Ljava/lang/String;
    :catchall_2
    move-exception v1

    monitor-exit v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v1
.end method

.method public filterTTFF()Ljava/util/ArrayList;
    .locals 27
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 162
    const-class v1, Lcom/android/server/location/GnssCollectDataDbDao;

    monitor-enter v1

    .line 163
    move-object/from16 v2, p0

    :try_start_0
    iget-object v0, v2, Lcom/android/server/location/GnssCollectDataDbDao;->mGnssCollectDataDbHelper:Lcom/android/server/location/GnssCollectDataDbHelper;

    invoke-virtual {v0}, Lcom/android/server/location/GnssCollectDataDbHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 164
    .local v3, "sqLiteDatabaseReadable":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move-object v11, v0

    .line 165
    .local v11, "item":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v6, "TTFF > ? AND TTFF <= ?"

    .line 167
    .local v6, "selection":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v7, v0, [Ljava/lang/String;

    const-string v4, "0"

    const/4 v12, 0x0

    aput-object v4, v7, v12

    const-string v4, "5"

    const/4 v13, 0x1

    aput-object v4, v7, v13

    .line 168
    .local v7, "selectionArgs":[Ljava/lang/String;
    const-string v4, "GnssCollectData"

    sget-object v23, Lcom/android/server/location/GnssCollectDataDbDao;->COLLECT_COLUMNS:[Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v5, v23

    invoke-virtual/range {v3 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_a

    .line 170
    .local v4, "cursor":Landroid/database/Cursor;
    :try_start_1
    const-string v5, "TTFF0-5"

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 171
    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/database/Cursor;

    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_8

    .line 172
    if-eqz v4, :cond_0

    :try_start_2
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 173
    .end local v4    # "cursor":Landroid/database/Cursor;
    :cond_0
    new-array v4, v0, [Ljava/lang/String;

    const-string v5, "5"

    aput-object v5, v4, v12

    const-string v5, "10"

    aput-object v5, v4, v13

    move-object/from16 v18, v4

    .line 174
    .end local v7    # "selectionArgs":[Ljava/lang/String;
    .local v18, "selectionArgs":[Ljava/lang/String;
    const-string v15, "GnssCollectData"

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object v14, v3

    move-object/from16 v16, v23

    move-object/from16 v17, v6

    invoke-virtual/range {v14 .. v21}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_a

    .line 176
    .restart local v4    # "cursor":Landroid/database/Cursor;
    :try_start_3
    const-string v5, "TTFF5-10"

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 177
    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/database/Cursor;

    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_6

    .line 178
    if-eqz v4, :cond_1

    :try_start_4
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 179
    .end local v4    # "cursor":Landroid/database/Cursor;
    :cond_1
    new-array v4, v0, [Ljava/lang/String;

    const-string v5, "10"

    aput-object v5, v4, v12

    const-string v5, "20"

    aput-object v5, v4, v13

    move-object/from16 v18, v4

    .line 180
    const-string v15, "GnssCollectData"

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object v14, v3

    move-object/from16 v16, v23

    move-object/from16 v17, v6

    invoke-virtual/range {v14 .. v21}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_a

    .line 182
    .restart local v4    # "cursor":Landroid/database/Cursor;
    :try_start_5
    const-string v5, "TTFF10-20"

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 183
    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/database/Cursor;

    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    .line 184
    if-eqz v4, :cond_2

    :try_start_6
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 185
    .end local v4    # "cursor":Landroid/database/Cursor;
    :cond_2
    new-array v0, v0, [Ljava/lang/String;

    const-string v4, "20"

    aput-object v4, v0, v12

    const-string v4, "30"

    aput-object v4, v0, v13

    move-object/from16 v18, v0

    .line 186
    const-string v15, "GnssCollectData"

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object v14, v3

    move-object/from16 v16, v23

    move-object/from16 v17, v6

    invoke-virtual/range {v14 .. v21}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_a

    move-object v4, v0

    .line 188
    .restart local v4    # "cursor":Landroid/database/Cursor;
    :try_start_7
    const-string v0, "TTFF20-30"

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 189
    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 190
    if-eqz v4, :cond_3

    :try_start_8
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 191
    .end local v4    # "cursor":Landroid/database/Cursor;
    :cond_3
    const-string v22, "TTFF > 30 "

    .line 192
    .end local v6    # "selection":Ljava/lang/String;
    .local v22, "selection":Ljava/lang/String;
    const-string v20, "GnssCollectData"

    const/4 v0, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    move-object/from16 v19, v3

    move-object/from16 v21, v23

    move-object/from16 v23, v0

    invoke-virtual/range {v19 .. v26}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_a

    move-object v4, v0

    .line 194
    .restart local v4    # "cursor":Landroid/database/Cursor;
    :try_start_9
    const-string v0, "TTFF30+"

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 195
    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 196
    if-eqz v4, :cond_4

    :try_start_a
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 197
    .end local v4    # "cursor":Landroid/database/Cursor;
    :cond_4
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 198
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_a

    return-object v11

    .line 192
    .restart local v4    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    move-object v5, v0

    if-eqz v4, :cond_5

    :try_start_b
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v0

    move-object v6, v0

    :try_start_c
    invoke-virtual {v5, v6}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :cond_5
    :goto_0
    throw v5
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_a

    .line 186
    .end local v22    # "selection":Ljava/lang/String;
    .restart local v6    # "selection":Ljava/lang/String;
    .restart local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :catchall_2
    move-exception v0

    move-object v5, v0

    if-eqz v4, :cond_6

    :try_start_d
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    goto :goto_1

    :catchall_3
    move-exception v0

    move-object v7, v0

    :try_start_e
    invoke-virtual {v5, v7}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :cond_6
    :goto_1
    throw v5
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_a

    .line 180
    .restart local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :catchall_4
    move-exception v0

    move-object v5, v0

    if-eqz v4, :cond_7

    :try_start_f
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_5

    goto :goto_2

    :catchall_5
    move-exception v0

    move-object v7, v0

    :try_start_10
    invoke-virtual {v5, v7}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :cond_7
    :goto_2
    throw v5
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_a

    .line 174
    .restart local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :catchall_6
    move-exception v0

    move-object v5, v0

    if-eqz v4, :cond_8

    :try_start_11
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_7

    goto :goto_3

    :catchall_7
    move-exception v0

    move-object v7, v0

    :try_start_12
    invoke-virtual {v5, v7}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :cond_8
    :goto_3
    throw v5
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_a

    .line 168
    .end local v18    # "selectionArgs":[Ljava/lang/String;
    .restart local v7    # "selectionArgs":[Ljava/lang/String;
    .restart local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :catchall_8
    move-exception v0

    move-object v5, v0

    if-eqz v4, :cond_9

    :try_start_13
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_9

    goto :goto_4

    :catchall_9
    move-exception v0

    move-object v8, v0

    :try_start_14
    invoke-virtual {v5, v8}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :cond_9
    :goto_4
    throw v5

    .line 199
    .end local v3    # "sqLiteDatabaseReadable":Landroid/database/sqlite/SQLiteDatabase;
    .end local v4    # "cursor":Landroid/database/Cursor;
    .end local v6    # "selection":Ljava/lang/String;
    .end local v7    # "selectionArgs":[Ljava/lang/String;
    .end local v11    # "item":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :catchall_a
    move-exception v0

    monitor-exit v1
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_a

    throw v0
.end method

.method public insertGnssCollectData(JJJIIIIDDD)V
    .locals 10
    .param p1, "startTime"    # J
    .param p3, "TTFF"    # J
    .param p5, "runTime"    # J
    .param p7, "loseTimes"    # I
    .param p8, "SAPNumber"    # I
    .param p9, "PDRNumber"    # I
    .param p10, "totalNumber"    # I
    .param p11, "L1Top4MeanCn0"    # D
    .param p13, "L5Top4MeanCn0"    # D
    .param p15, "B1Top4MeanCn0"    # D

    .line 100
    const-class v1, Lcom/android/server/location/GnssCollectDataDbDao;

    monitor-enter v1

    .line 103
    move-object v2, p0

    :try_start_0
    iget-object v0, v2, Lcom/android/server/location/GnssCollectDataDbDao;->mGnssCollectDataDbHelper:Lcom/android/server/location/GnssCollectDataDbHelper;

    invoke-virtual {v0}, Lcom/android/server/location/GnssCollectDataDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-object v3, v0

    .line 104
    .local v3, "sqLiteDatabaseWritable":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_1
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 105
    .local v0, "values":Landroid/content/ContentValues;
    const-string/jumbo v4, "startTime"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 106
    const-string v4, "TTFF"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 107
    const-string v4, "runTime"

    invoke-static/range {p5 .. p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 108
    const-string v4, "loseTimes"

    invoke-static/range {p7 .. p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 109
    const-string v4, "SAPNumber"

    invoke-static/range {p8 .. p8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 110
    const-string v4, "PDRNumber"

    invoke-static/range {p9 .. p9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 111
    const-string/jumbo v4, "totalNumber"

    invoke-static/range {p10 .. p10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 112
    const-string v4, "L1Top4MeanCn0"

    invoke-static/range {p11 .. p12}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 113
    const-string v4, "L5Top4MeanCn0"

    invoke-static/range {p13 .. p14}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 114
    const-string v4, "B1Top4MeanCn0"

    invoke-static/range {p15 .. p16}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 116
    const-string v4, "GnssCollectData"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 119
    .local v4, "newRowId":J
    const-string v6, "GnssCollectDataDbDao"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "insertGnssCollectData "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-wide/16 v8, -0x1

    cmp-long v8, v4, v8

    if-eqz v8, :cond_0

    const-string/jumbo v8, "successfully"

    goto :goto_0

    :cond_0
    const-string v8, "failed"

    :goto_0
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 120
    .end local v0    # "values":Landroid/content/ContentValues;
    .end local v4    # "newRowId":J
    if-eqz v3, :cond_1

    :try_start_2
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 122
    .end local v3    # "sqLiteDatabaseWritable":Landroid/database/sqlite/SQLiteDatabase;
    :cond_1
    goto :goto_2

    .line 103
    .restart local v3    # "sqLiteDatabaseWritable":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_0
    move-exception v0

    move-object v4, v0

    if-eqz v3, :cond_2

    :try_start_3
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v0

    move-object v5, v0

    :try_start_4
    invoke-virtual {v4, v5}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    .end local p1    # "startTime":J
    .end local p3    # "TTFF":J
    .end local p5    # "runTime":J
    .end local p7    # "loseTimes":I
    .end local p8    # "SAPNumber":I
    .end local p9    # "PDRNumber":I
    .end local p10    # "totalNumber":I
    .end local p11    # "L1Top4MeanCn0":D
    .end local p13    # "L5Top4MeanCn0":D
    .end local p15    # "B1Top4MeanCn0":D
    :cond_2
    :goto_1
    throw v4
    :try_end_4
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 123
    .end local v3    # "sqLiteDatabaseWritable":Landroid/database/sqlite/SQLiteDatabase;
    .restart local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    .restart local p1    # "startTime":J
    .restart local p3    # "TTFF":J
    .restart local p5    # "runTime":J
    .restart local p7    # "loseTimes":I
    .restart local p8    # "SAPNumber":I
    .restart local p9    # "PDRNumber":I
    .restart local p10    # "totalNumber":I
    .restart local p11    # "L1Top4MeanCn0":D
    .restart local p13    # "L5Top4MeanCn0":D
    .restart local p15    # "B1Top4MeanCn0":D
    :catchall_2
    move-exception v0

    goto :goto_3

    .line 120
    :catch_0
    move-exception v0

    .line 121
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_5
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 123
    .end local v0    # "e":Landroid/database/sqlite/SQLiteException;
    :goto_2
    monitor-exit v1

    .line 124
    return-void

    .line 123
    :goto_3
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0
.end method

.method public insertGnssCollectData(Lcom/android/server/location/GnssCollectData;)V
    .locals 19
    .param p1, "gnssCollectData"    # Lcom/android/server/location/GnssCollectData;

    .line 87
    const-class v1, Lcom/android/server/location/GnssCollectDataDbDao;

    monitor-enter v1

    .line 88
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Lcom/android/server/location/GnssCollectData;->getStartTime()J

    move-result-wide v3

    invoke-virtual/range {p1 .. p1}, Lcom/android/server/location/GnssCollectData;->getTtff()J

    move-result-wide v5

    invoke-virtual/range {p1 .. p1}, Lcom/android/server/location/GnssCollectData;->getRunTime()J

    move-result-wide v7

    .line 89
    invoke-virtual/range {p1 .. p1}, Lcom/android/server/location/GnssCollectData;->getLoseTimes()I

    move-result v9

    invoke-virtual/range {p1 .. p1}, Lcom/android/server/location/GnssCollectData;->getSapNumber()I

    move-result v10

    invoke-virtual/range {p1 .. p1}, Lcom/android/server/location/GnssCollectData;->getPdrNumber()I

    move-result v11

    invoke-virtual/range {p1 .. p1}, Lcom/android/server/location/GnssCollectData;->getTotalNumber()I

    move-result v12

    .line 90
    invoke-virtual/range {p1 .. p1}, Lcom/android/server/location/GnssCollectData;->getL1Top4MeanCn0()D

    move-result-wide v13

    invoke-virtual/range {p1 .. p1}, Lcom/android/server/location/GnssCollectData;->getL5Top4MeanCn0()D

    move-result-wide v15

    invoke-virtual/range {p1 .. p1}, Lcom/android/server/location/GnssCollectData;->getB1Top4MeanCn0()D

    move-result-wide v17

    .line 88
    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v18}, Lcom/android/server/location/GnssCollectDataDbDao;->insertGnssCollectData(JJJIIIIDDD)V

    .line 91
    monitor-exit v1

    .line 92
    return-void

    .line 91
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public sumSPT()Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 284
    const-class v0, Lcom/android/server/location/GnssCollectDataDbDao;

    monitor-enter v0

    .line 285
    :try_start_0
    iget-object v1, p0, Lcom/android/server/location/GnssCollectDataDbDao;->mGnssCollectDataDbHelper:Lcom/android/server/location/GnssCollectDataDbHelper;

    invoke-virtual {v1}, Lcom/android/server/location/GnssCollectDataDbHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 286
    .local v1, "sqLiteDatabaseReadable":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 287
    .local v2, "item":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string/jumbo v3, "select sum(SAPNumber) from GnssCollectData"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_6

    .line 289
    .local v3, "cursor":Landroid/database/Cursor;
    :try_start_1
    const-string v5, "SAP"

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 290
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    const/4 v6, 0x0

    if-eqz v5, :cond_0

    invoke-interface {v3, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 291
    :cond_0
    const-string v5, "0"

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_4

    .line 292
    :goto_0
    if-eqz v3, :cond_1

    :try_start_2
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 293
    .end local v3    # "cursor":Landroid/database/Cursor;
    :cond_1
    const-string/jumbo v3, "select sum(PDRNumber) from GnssCollectData"

    invoke-virtual {v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_6

    .line 295
    .restart local v3    # "cursor":Landroid/database/Cursor;
    :try_start_3
    const-string v5, "PDR"

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 296
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v3, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 297
    :cond_2
    const-string v5, "0"

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 298
    :goto_1
    if-eqz v3, :cond_3

    :try_start_4
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 299
    .end local v3    # "cursor":Landroid/database/Cursor;
    :cond_3
    const-string/jumbo v3, "select sum(totalNumber) from GnssCollectData"

    invoke-virtual {v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_6

    .line 301
    .restart local v3    # "cursor":Landroid/database/Cursor;
    :try_start_5
    const-string v4, "Total"

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 302
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v3, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 303
    :cond_4
    const-string v4, "0"

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 304
    :goto_2
    if-eqz v3, :cond_5

    :try_start_6
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 305
    .end local v3    # "cursor":Landroid/database/Cursor;
    :cond_5
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 306
    monitor-exit v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_6

    return-object v2

    .line 299
    .restart local v3    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v4

    if-eqz v3, :cond_6

    :try_start_7
    invoke-interface {v3}, Landroid/database/Cursor;->close()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_3

    :catchall_1
    move-exception v5

    :try_start_8
    invoke-virtual {v4, v5}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :cond_6
    :goto_3
    throw v4
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_6

    .line 293
    .restart local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :catchall_2
    move-exception v4

    if-eqz v3, :cond_7

    :try_start_9
    invoke-interface {v3}, Landroid/database/Cursor;->close()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    goto :goto_4

    :catchall_3
    move-exception v5

    :try_start_a
    invoke-virtual {v4, v5}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :cond_7
    :goto_4
    throw v4
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_6

    .line 287
    .restart local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :catchall_4
    move-exception v4

    if-eqz v3, :cond_8

    :try_start_b
    invoke-interface {v3}, Landroid/database/Cursor;->close()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_5

    goto :goto_5

    :catchall_5
    move-exception v5

    :try_start_c
    invoke-virtual {v4, v5}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :cond_8
    :goto_5
    throw v4

    .line 307
    .end local v1    # "sqLiteDatabaseReadable":Landroid/database/sqlite/SQLiteDatabase;
    .end local v2    # "item":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v3    # "cursor":Landroid/database/Cursor;
    .restart local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    :catchall_6
    move-exception v1

    monitor-exit v0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_6

    throw v1
.end method

.method public updateGnssCollectData(IJ)V
    .locals 7
    .param p1, "id"    # I
    .param p2, "startTime"    # J

    .line 419
    const-class v0, Lcom/android/server/location/GnssCollectDataDbDao;

    monitor-enter v0

    .line 420
    :try_start_0
    iget-object v1, p0, Lcom/android/server/location/GnssCollectDataDbDao;->mGnssCollectDataDbHelper:Lcom/android/server/location/GnssCollectDataDbHelper;

    invoke-virtual {v1}, Lcom/android/server/location/GnssCollectDataDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 421
    .local v1, "sqLiteDatabaseWritable":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_1
    const-string v2, "_id = ?"

    .line 422
    .local v2, "selection":Ljava/lang/String;
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    aput-object v4, v3, v5

    .line 423
    .local v3, "selectionArgs":[Ljava/lang/String;
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 424
    .local v4, "values":Landroid/content/ContentValues;
    const-string/jumbo v5, "startTime"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 425
    const-string v5, "GnssCollectData"

    invoke-virtual {v1, v5, v4, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 426
    .end local v2    # "selection":Ljava/lang/String;
    .end local v3    # "selectionArgs":[Ljava/lang/String;
    .end local v4    # "values":Landroid/content/ContentValues;
    if-eqz v1, :cond_0

    :try_start_2
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 428
    .end local v1    # "sqLiteDatabaseWritable":Landroid/database/sqlite/SQLiteDatabase;
    :cond_0
    goto :goto_1

    .line 420
    .restart local v1    # "sqLiteDatabaseWritable":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_0
    move-exception v2

    if-eqz v1, :cond_1

    :try_start_3
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v3

    :try_start_4
    invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    .end local p1    # "id":I
    .end local p2    # "startTime":J
    :cond_1
    :goto_0
    throw v2
    :try_end_4
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 429
    .end local v1    # "sqLiteDatabaseWritable":Landroid/database/sqlite/SQLiteDatabase;
    .restart local p0    # "this":Lcom/android/server/location/GnssCollectDataDbDao;
    .restart local p1    # "id":I
    .restart local p2    # "startTime":J
    :catchall_2
    move-exception v1

    goto :goto_2

    .line 426
    :catch_0
    move-exception v1

    .line 427
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_5
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 429
    .end local v1    # "e":Landroid/database/sqlite/SQLiteException;
    :goto_1
    monitor-exit v0

    .line 430
    return-void

    .line 429
    :goto_2
    monitor-exit v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v1
.end method
