class com.android.server.location.GnssSmartSatelliteSwitchImpl$BlockListHandler extends android.os.Handler {
	 /* .source "GnssSmartSatelliteSwitchImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/location/GnssSmartSatelliteSwitchImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "BlockListHandler" */
} // .end annotation
/* # instance fields */
final com.android.server.location.GnssSmartSatelliteSwitchImpl this$0; //synthetic
/* # direct methods */
public com.android.server.location.GnssSmartSatelliteSwitchImpl$BlockListHandler ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 341 */
this.this$0 = p1;
/* .line 342 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 343 */
return;
} // .end method
public com.android.server.location.GnssSmartSatelliteSwitchImpl$BlockListHandler ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .param p3, "callback" # Landroid/os/Handler$Callback; */
/* .line 345 */
this.this$0 = p1;
/* .line 346 */
/* invoke-direct {p0, p2, p3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V */
/* .line 347 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 2 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 351 */
/* invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V */
/* .line 352 */
/* if-nez p1, :cond_0 */
/* .line 353 */
return;
/* .line 354 */
} // :cond_0
/* iget v0, p1, Landroid/os/Message;->what:I */
/* packed-switch v0, :pswitch_data_0 */
/* .line 359 */
/* :pswitch_0 */
v0 = this.this$0;
final String v1 = "3,0,4,0,6,0,7,0"; // const-string v1, "3,0,4,0,6,0,7,0"
com.android.server.location.GnssSmartSatelliteSwitchImpl .-$$Nest$mupdateBlockList ( v0,v1 );
/* .line 360 */
/* .line 356 */
/* :pswitch_1 */
v0 = this.this$0;
final String v1 = ""; // const-string v1, ""
com.android.server.location.GnssSmartSatelliteSwitchImpl .-$$Nest$mupdateBlockList ( v0,v1 );
/* .line 357 */
/* nop */
/* .line 364 */
} // :goto_0
v0 = this.this$0;
com.android.server.location.GnssSmartSatelliteSwitchImpl .-$$Nest$fgetBDS_COLLECTOR ( v0 );
(( java.util.concurrent.ConcurrentHashMap ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V
/* .line 365 */
v0 = this.this$0;
com.android.server.location.GnssSmartSatelliteSwitchImpl .-$$Nest$fgetGPS_COLLECTOR ( v0 );
(( java.util.concurrent.ConcurrentHashMap ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V
/* .line 366 */
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
