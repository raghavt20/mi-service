public class com.android.server.location.GnssCollectData$CollectDbEntry implements android.provider.BaseColumns {
	 /* .source "GnssCollectData.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/location/GnssCollectData; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "CollectDbEntry" */
} // .end annotation
/* # static fields */
public static final java.lang.String COLUMN_NAME_B1TOP4MEANCN0;
public static final java.lang.String COLUMN_NAME_L1TOP4MEANCN0;
public static final java.lang.String COLUMN_NAME_L5TOP4MEANCN0;
public static final java.lang.String COLUMN_NAME_LOSETIMES;
public static final java.lang.String COLUMN_NAME_PDRNUMBER;
public static final java.lang.String COLUMN_NAME_RUNTIME;
public static final java.lang.String COLUMN_NAME_SAPNUMBER;
public static final java.lang.String COLUMN_NAME_STARTTIME;
public static final java.lang.String COLUMN_NAME_TOTALNUMBER;
public static final java.lang.String COLUMN_NAME_TTFF;
public static final java.lang.String TABLE_NAME;
/* # direct methods */
private com.android.server.location.GnssCollectData$CollectDbEntry ( ) {
/* .locals 0 */
/* .line 150 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
