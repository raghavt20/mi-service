class com.android.server.location.MtkGnssPowerSaveImpl$SmartSatelliteHandler extends android.os.Handler {
	 /* .source "MtkGnssPowerSaveImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/location/MtkGnssPowerSaveImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "SmartSatelliteHandler" */
} // .end annotation
/* # instance fields */
final com.android.server.location.MtkGnssPowerSaveImpl this$0; //synthetic
/* # direct methods */
public com.android.server.location.MtkGnssPowerSaveImpl$SmartSatelliteHandler ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 614 */
this.this$0 = p1;
/* .line 615 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 616 */
return;
} // .end method
public com.android.server.location.MtkGnssPowerSaveImpl$SmartSatelliteHandler ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .param p3, "callback" # Landroid/os/Handler$Callback; */
/* .line 618 */
this.this$0 = p1;
/* .line 619 */
/* invoke-direct {p0, p2, p3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V */
/* .line 620 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 5 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 624 */
/* invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V */
/* .line 625 */
v0 = this.obj;
/* if-nez v0, :cond_0 */
/* .line 626 */
return;
/* .line 628 */
} // :cond_0
/* iget v0, p1, Landroid/os/Message;->what:I */
int v1 = 1; // const/4 v1, 0x1
int v2 = 2; // const/4 v2, 0x2
/* const-string/jumbo v3, "xiaomi_gnss_smart_switch" */
/* packed-switch v0, :pswitch_data_0 */
/* .line 630 */
/* :pswitch_0 */
v0 = this.obj;
(( java.lang.Object ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;
java.lang.Boolean .valueOf ( v0 );
v0 = (( java.lang.Boolean ) v0 ).booleanValue ( ); // invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
/* .line 631 */
/* .local v0, "powerSaveMode":Z */
/* const-string/jumbo v4, "xiaomi_gnss_config_disable_l5" */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 632 */
v2 = this.this$0;
com.android.server.location.MtkGnssPowerSaveImpl .-$$Nest$fgetmContext ( v2 );
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$Secure .putInt ( v2,v4,v1 );
/* .line 634 */
v2 = this.this$0;
com.android.server.location.MtkGnssPowerSaveImpl .-$$Nest$fgetmContext ( v2 );
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$Secure .putInt ( v2,v3,v1 );
/* .line 637 */
} // :cond_1
v1 = this.this$0;
com.android.server.location.MtkGnssPowerSaveImpl .-$$Nest$fgetmContext ( v1 );
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$Secure .putInt ( v1,v4,v2 );
/* .line 639 */
v1 = this.this$0;
com.android.server.location.MtkGnssPowerSaveImpl .-$$Nest$fgetmContext ( v1 );
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$Secure .putInt ( v1,v3,v2 );
/* .line 644 */
/* .line 646 */
} // .end local v0 # "powerSaveMode":Z
/* :pswitch_1 */
v0 = this.this$0;
com.android.server.location.MtkGnssPowerSaveImpl .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$Secure .putInt ( v0,v3,v1 );
/* .line 648 */
/* .line 650 */
/* :pswitch_2 */
v0 = this.this$0;
com.android.server.location.MtkGnssPowerSaveImpl .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$Secure .putInt ( v0,v3,v2 );
/* .line 652 */
/* nop */
/* .line 656 */
} // :goto_0
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
