.class public Lcom/android/server/location/LocationDumpLogImpl;
.super Ljava/lang/Object;
.source "LocationDumpLogImpl.java"

# interfaces
.implements Lcom/android/server/location/LocationDumpLogStub;


# static fields
.field private static final DUMP_TAG_GLP:Ljava/lang/String; = "=MI GLP= "

.field private static final DUMP_TAG_GLP_EN:Ljava/lang/String; = "=MI GLP EN="

.field private static final DUMP_TAG_LMS:Ljava/lang/String; = "=MI LMS= "

.field private static final DUMP_TAG_NMEA:Ljava/lang/String; = "=MI NMEA="

.field private static final mdumpGlp:Lcom/android/server/location/GnssLocalLog;

.field private static final mdumpLms:Lcom/android/server/location/GnssLocalLog;

.field private static final mdumpNmea:Lcom/android/server/location/GnssLocalLog;


# instance fields
.field private defaultFusedProviderName:I

.field private defaultGeocoderProviderName:I

.field private defaultGeofenceProviderName:I

.field private defaultNetworkProviderName:I

.field private mRecordLoseCount:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 14
    new-instance v0, Lcom/android/server/location/GnssLocalLog;

    const/16 v1, 0x3e8

    invoke-direct {v0, v1}, Lcom/android/server/location/GnssLocalLog;-><init>(I)V

    sput-object v0, Lcom/android/server/location/LocationDumpLogImpl;->mdumpLms:Lcom/android/server/location/GnssLocalLog;

    .line 15
    new-instance v0, Lcom/android/server/location/GnssLocalLog;

    invoke-direct {v0, v1}, Lcom/android/server/location/GnssLocalLog;-><init>(I)V

    sput-object v0, Lcom/android/server/location/LocationDumpLogImpl;->mdumpGlp:Lcom/android/server/location/GnssLocalLog;

    .line 16
    new-instance v0, Lcom/android/server/location/GnssLocalLog;

    const/16 v1, 0x4e20

    invoke-direct {v0, v1}, Lcom/android/server/location/GnssLocalLog;-><init>(I)V

    sput-object v0, Lcom/android/server/location/LocationDumpLogImpl;->mdumpNmea:Lcom/android/server/location/GnssLocalLog;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const v0, 0x10402b7

    iput v0, p0, Lcom/android/server/location/LocationDumpLogImpl;->defaultNetworkProviderName:I

    .line 24
    const v0, 0x104029b

    iput v0, p0, Lcom/android/server/location/LocationDumpLogImpl;->defaultFusedProviderName:I

    .line 25
    const v0, 0x104029c

    iput v0, p0, Lcom/android/server/location/LocationDumpLogImpl;->defaultGeocoderProviderName:I

    .line 26
    const v0, 0x104029d

    iput v0, p0, Lcom/android/server/location/LocationDumpLogImpl;->defaultGeofenceProviderName:I

    .line 28
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/location/LocationDumpLogImpl;->mRecordLoseCount:Z

    return-void
.end method

.method private isCnVersion()Z
    .locals 2

    .line 84
    const-string v0, "ro.miui.build.region"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "CN"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private isXOptMode()Z
    .locals 2

    .line 80
    const-string v0, "persist.sys.miui_optimization"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    xor-int/2addr v0, v1

    return v0
.end method

.method private switchTypeToDump(I)Lcom/android/server/location/GnssLocalLog;
    .locals 1
    .param p1, "type"    # I

    .line 88
    packed-switch p1, :pswitch_data_0

    .line 95
    sget-object v0, Lcom/android/server/location/LocationDumpLogImpl;->mdumpNmea:Lcom/android/server/location/GnssLocalLog;

    return-object v0

    .line 93
    :pswitch_0
    sget-object v0, Lcom/android/server/location/LocationDumpLogImpl;->mdumpGlp:Lcom/android/server/location/GnssLocalLog;

    return-object v0

    .line 90
    :pswitch_1
    sget-object v0, Lcom/android/server/location/LocationDumpLogImpl;->mdumpLms:Lcom/android/server/location/GnssLocalLog;

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private switchTypeToLogTag(I)Ljava/lang/String;
    .locals 1
    .param p1, "type"    # I

    .line 100
    packed-switch p1, :pswitch_data_0

    .line 108
    const-string v0, "=MI NMEA="

    return-object v0

    .line 106
    :pswitch_0
    const-string v0, "=MI GLP EN="

    return-object v0

    .line 104
    :pswitch_1
    const-string v0, "=MI GLP= "

    return-object v0

    .line 102
    :pswitch_2
    const-string v0, "=MI LMS= "

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public addToBugreport(ILjava/lang/String;)V
    .locals 3
    .param p1, "type"    # I
    .param p2, "log"    # Ljava/lang/String;

    .line 32
    invoke-direct {p0, p1}, Lcom/android/server/location/LocationDumpLogImpl;->switchTypeToDump(I)Lcom/android/server/location/GnssLocalLog;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0, p1}, Lcom/android/server/location/LocationDumpLogImpl;->switchTypeToLogTag(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/location/GnssLocalLog;->log(Ljava/lang/String;)V

    .line 33
    return-void
.end method

.method public clearData()V
    .locals 1

    .line 114
    sget-object v0, Lcom/android/server/location/LocationDumpLogImpl;->mdumpGlp:Lcom/android/server/location/GnssLocalLog;

    invoke-virtual {v0}, Lcom/android/server/location/GnssLocalLog;->clearData()V

    .line 115
    sget-object v0, Lcom/android/server/location/LocationDumpLogImpl;->mdumpNmea:Lcom/android/server/location/GnssLocalLog;

    invoke-virtual {v0}, Lcom/android/server/location/GnssLocalLog;->clearData()V

    .line 116
    return-void
.end method

.method public dump(ILjava/io/PrintWriter;)V
    .locals 1
    .param p1, "type"    # I
    .param p2, "pw"    # Ljava/io/PrintWriter;

    .line 76
    invoke-direct {p0, p1}, Lcom/android/server/location/LocationDumpLogImpl;->switchTypeToDump(I)Lcom/android/server/location/GnssLocalLog;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/android/server/location/GnssLocalLog;->dump(Ljava/io/PrintWriter;)V

    .line 77
    return-void
.end method

.method public getConfig(Ljava/util/Properties;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "properties"    # Ljava/util/Properties;
    .param p2, "config"    # Ljava/lang/String;
    .param p3, "defaultConfig"    # Ljava/lang/String;

    .line 42
    invoke-virtual {p1, p2, p3}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultProviderName(Ljava/lang/String;)I
    .locals 1
    .param p1, "type"    # Ljava/lang/String;

    .line 57
    invoke-direct {p0}, Lcom/android/server/location/LocationDumpLogImpl;->isXOptMode()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/server/location/LocationDumpLogImpl;->isCnVersion()Z

    move-result v0

    if-nez v0, :cond_0

    .line 58
    const v0, 0x10402b8

    iput v0, p0, Lcom/android/server/location/LocationDumpLogImpl;->defaultNetworkProviderName:I

    .line 59
    iput v0, p0, Lcom/android/server/location/LocationDumpLogImpl;->defaultFusedProviderName:I

    .line 61
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :cond_1
    goto :goto_0

    :sswitch_0
    const-string v0, "network"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_1
    const-string v0, "geofence"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    goto :goto_1

    :sswitch_2
    const-string v0, "geocoder"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    goto :goto_1

    :sswitch_3
    const-string v0, "fused"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 70
    iget v0, p0, Lcom/android/server/location/LocationDumpLogImpl;->defaultGeofenceProviderName:I

    return v0

    .line 67
    :pswitch_0
    iget v0, p0, Lcom/android/server/location/LocationDumpLogImpl;->defaultGeocoderProviderName:I

    return v0

    .line 65
    :pswitch_1
    iget v0, p0, Lcom/android/server/location/LocationDumpLogImpl;->defaultFusedProviderName:I

    return v0

    .line 63
    :pswitch_2
    iget v0, p0, Lcom/android/server/location/LocationDumpLogImpl;->defaultNetworkProviderName:I

    return v0

    nop

    :sswitch_data_0
    .sparse-switch
        0x5d44923 -> :sswitch_3
        0x6d7f6b74 -> :sswitch_2
        0x6da54b80 -> :sswitch_1
        0x6de15a2e -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getRecordLoseLocation()Z
    .locals 1

    .line 47
    iget-boolean v0, p0, Lcom/android/server/location/LocationDumpLogImpl;->mRecordLoseCount:Z

    return v0
.end method

.method public setLength(II)V
    .locals 1
    .param p1, "type"    # I
    .param p2, "length"    # I

    .line 37
    invoke-direct {p0, p1}, Lcom/android/server/location/LocationDumpLogImpl;->switchTypeToDump(I)Lcom/android/server/location/GnssLocalLog;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/android/server/location/GnssLocalLog;->setLength(I)I

    .line 38
    return-void
.end method

.method public setRecordLoseLocation(Z)V
    .locals 0
    .param p1, "newValue"    # Z

    .line 52
    iput-boolean p1, p0, Lcom/android/server/location/LocationDumpLogImpl;->mRecordLoseCount:Z

    .line 53
    return-void
.end method
