.class Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;
.super Ljava/lang/Object;
.source "MiuiBlurLocationManagerImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/location/MiuiBlurLocationManagerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BaseCellLocation"
.end annotation


# instance fields
.field private cid:I

.field private lac:I

.field private latitude:D

.field private longitude:D


# direct methods
.method static bridge synthetic -$$Nest$fgetcid(Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;)I
    .locals 0

    iget p0, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->cid:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetlac(Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;)I
    .locals 0

    iget p0, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->lac:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetlatitude(Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;)D
    .locals 2

    iget-wide v0, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->latitude:D

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetlongitude(Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;)D
    .locals 2

    iget-wide v0, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->longitude:D

    return-wide v0
.end method

.method private constructor <init>()V
    .locals 0

    .line 324
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation-IA;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;-><init>()V

    return-void
.end method


# virtual methods
.method public isValidInfo()Z
    .locals 4

    .line 336
    iget v0, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->lac:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->cid:I

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->longitude:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->latitude:D

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public set(IIDD)V
    .locals 0
    .param p1, "lac"    # I
    .param p2, "cid"    # I
    .param p3, "longitude"    # D
    .param p5, "latitude"    # D

    .line 329
    iput p1, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->lac:I

    .line 330
    iput p2, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->cid:I

    .line 331
    iput-wide p3, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->longitude:D

    .line 332
    iput-wide p5, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->latitude:D

    .line 333
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 341
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BaseCellLocation{lac="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->lac:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", cid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->cid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", longitude="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->longitude:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", latitude="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/server/location/MiuiBlurLocationManagerImpl$BaseCellLocation;->latitude:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
