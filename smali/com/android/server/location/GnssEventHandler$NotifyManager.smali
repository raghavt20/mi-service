.class public Lcom/android/server/location/GnssEventHandler$NotifyManager;
.super Ljava/lang/Object;
.source "GnssEventHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/location/GnssEventHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "NotifyManager"
.end annotation


# instance fields
.field private final CHANNEL_ID:Ljava/lang/String;

.field private appName:Ljava/lang/CharSequence;

.field private mBuilder:Landroid/app/Notification$Builder;

.field private final mCloseBlurLocationListener:Landroid/content/BroadcastReceiver;

.field private mNotificationManager:Landroid/app/NotificationManager;

.field private mnoise:Z

.field private packageName:Ljava/lang/String;

.field final synthetic this$0:Lcom/android/server/location/GnssEventHandler;


# direct methods
.method static bridge synthetic -$$Nest$fgetmCloseBlurLocationListener(Lcom/android/server/location/GnssEventHandler$NotifyManager;)Landroid/content/BroadcastReceiver;
    .locals 0

    iget-object p0, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->mCloseBlurLocationListener:Landroid/content/BroadcastReceiver;

    return-object p0
.end method

.method public constructor <init>(Lcom/android/server/location/GnssEventHandler;)V
    .locals 1
    .param p1, "this$0"    # Lcom/android/server/location/GnssEventHandler;

    .line 98
    iput-object p1, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->this$0:Lcom/android/server/location/GnssEventHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    const-string v0, "GPS_STATUS_MONITOR_ID"

    iput-object v0, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->CHANNEL_ID:Ljava/lang/String;

    .line 102
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->packageName:Ljava/lang/String;

    .line 103
    iput-object v0, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->appName:Ljava/lang/CharSequence;

    .line 104
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->mnoise:Z

    .line 186
    new-instance v0, Lcom/android/server/location/GnssEventHandler$NotifyManager$1;

    invoke-direct {v0, p0}, Lcom/android/server/location/GnssEventHandler$NotifyManager$1;-><init>(Lcom/android/server/location/GnssEventHandler$NotifyManager;)V

    iput-object v0, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->mCloseBlurLocationListener:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private constructNotification()V
    .locals 14

    .line 113
    iget-object v0, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->this$0:Lcom/android/server/location/GnssEventHandler;

    invoke-static {v0}, Lcom/android/server/location/GnssEventHandler;->-$$Nest$fgetmAppContext(Lcom/android/server/location/GnssEventHandler;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x110f02bd

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 114
    .local v0, "description":Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 115
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "com.android.settings"

    const-string v3, "com.android.settings.Settings$LocationSettingsActivity"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 116
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 117
    iget-object v2, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->this$0:Lcom/android/server/location/GnssEventHandler;

    invoke-static {v2}, Lcom/android/server/location/GnssEventHandler;->-$$Nest$fgetmAppContext(Lcom/android/server/location/GnssEventHandler;)Landroid/content/Context;

    move-result-object v2

    const/high16 v3, 0xc000000

    const/4 v4, 0x0

    invoke-static {v2, v4, v1, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 119
    .local v2, "pendingIntent":Landroid/app/PendingIntent;
    new-instance v3, Landroid/app/NotificationChannel;

    const-string v5, "GPS_STATUS_MONITOR_ID"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v0, v6}, Landroid/app/NotificationChannel;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V

    .line 121
    .local v3, "channel":Landroid/app/NotificationChannel;
    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Landroid/app/NotificationChannel;->setLockscreenVisibility(I)V

    .line 122
    const/4 v7, 0x0

    invoke-virtual {v3, v7, v7}, Landroid/app/NotificationChannel;->setSound(Landroid/net/Uri;Landroid/media/AudioAttributes;)V

    .line 123
    invoke-virtual {v3, v4}, Landroid/app/NotificationChannel;->enableVibration(Z)V

    .line 124
    new-array v7, v5, [J

    const-wide/16 v8, 0x0

    aput-wide v8, v7, v4

    invoke-virtual {v3, v7}, Landroid/app/NotificationChannel;->setVibrationPattern([J)V

    .line 125
    iget-object v7, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->mBuilder:Landroid/app/Notification$Builder;

    const v8, 0x110801b9

    invoke-virtual {v7, v8}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    .line 127
    :try_start_0
    iget-object v7, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->mNotificationManager:Landroid/app/NotificationManager;

    invoke-virtual {v7, v3}, Landroid/app/NotificationManager;->createNotificationChannel(Landroid/app/NotificationChannel;)V

    .line 128
    iget-boolean v7, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->mnoise:Z

    if-ne v7, v5, :cond_0

    .line 129
    iget-object v4, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->mBuilder:Landroid/app/Notification$Builder;

    iget-object v6, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->this$0:Lcom/android/server/location/GnssEventHandler;

    invoke-static {v6}, Lcom/android/server/location/GnssEventHandler;->-$$Nest$fgetmAppContext(Lcom/android/server/location/GnssEventHandler;)Landroid/content/Context;

    move-result-object v6

    const v7, 0x110f02c1

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v4

    iget-object v6, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->this$0:Lcom/android/server/location/GnssEventHandler;

    invoke-static {v6}, Lcom/android/server/location/GnssEventHandler;->-$$Nest$fgetmAppContext(Lcom/android/server/location/GnssEventHandler;)Landroid/content/Context;

    move-result-object v6

    .line 131
    const v7, 0x110f02b7

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v4

    .line 132
    invoke-virtual {v4, v2}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v4

    .line 133
    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v4

    .line 134
    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    goto/16 :goto_1

    .line 135
    :cond_0
    iget-object v7, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->this$0:Lcom/android/server/location/GnssEventHandler;

    invoke-static {v7}, Lcom/android/server/location/GnssEventHandler;->-$$Nest$fgetmBlur(Lcom/android/server/location/GnssEventHandler;)Z

    move-result v7

    if-ne v7, v5, :cond_2

    .line 136
    iget-object v7, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->this$0:Lcom/android/server/location/GnssEventHandler;

    invoke-static {v7}, Lcom/android/server/location/GnssEventHandler;->-$$Nest$fgetmAppContext(Lcom/android/server/location/GnssEventHandler;)Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 138
    .local v7, "pm":Landroid/content/pm/PackageManager;
    :try_start_1
    iget-object v8, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->packageName:Ljava/lang/String;

    invoke-virtual {v7, v8, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v8

    .line 139
    .local v8, "ai":Landroid/content/pm/ApplicationInfo;
    invoke-virtual {v8, v7}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v9

    iput-object v9, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->appName:Ljava/lang/CharSequence;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 142
    .end local v8    # "ai":Landroid/content/pm/ApplicationInfo;
    goto :goto_0

    .line 140
    :catch_0
    move-exception v8

    .line 141
    .local v8, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_2
    const-string v9, "GnssNps"

    const-string v10, "No such package for this name!"

    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    .end local v8    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :goto_0
    iget-object v8, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->appName:Ljava/lang/CharSequence;

    if-nez v8, :cond_1

    .line 144
    const-string v8, "gps server"

    iput-object v8, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->appName:Ljava/lang/CharSequence;

    .line 146
    :cond_1
    iget-object v8, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->this$0:Lcom/android/server/location/GnssEventHandler;

    invoke-static {v8}, Lcom/android/server/location/GnssEventHandler;->-$$Nest$fgetmAppContext(Lcom/android/server/location/GnssEventHandler;)Landroid/content/Context;

    move-result-object v8

    new-instance v9, Landroid/content/Intent;

    invoke-static {}, Lcom/android/server/location/GnssEventHandler;->-$$Nest$sfgetCLOSE_BLUR_LOCATION()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v10, 0x4000000

    invoke-static {v8, v4, v9, v10}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v8

    .line 148
    .local v8, "blurLocationPendingIntent":Landroid/app/PendingIntent;
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    .line 149
    .local v9, "bundle":Landroid/os/Bundle;
    const-string v10, "miui.showAction"

    invoke-virtual {v9, v10, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 150
    iget-object v10, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->this$0:Lcom/android/server/location/GnssEventHandler;

    new-instance v11, Landroid/app/Notification$Action;

    iget-object v12, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->this$0:Lcom/android/server/location/GnssEventHandler;

    invoke-static {v12}, Lcom/android/server/location/GnssEventHandler;->-$$Nest$fgetmAppContext(Lcom/android/server/location/GnssEventHandler;)Landroid/content/Context;

    move-result-object v12

    .line 151
    const v13, 0x110f02bb

    invoke-virtual {v12, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    const v13, 0x11080198    # 1.0729E-28f

    invoke-direct {v11, v13, v12, v8}, Landroid/app/Notification$Action;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    invoke-static {v10, v11}, Lcom/android/server/location/GnssEventHandler;->-$$Nest$fputmAction(Lcom/android/server/location/GnssEventHandler;Landroid/app/Notification$Action;)V

    .line 153
    iget-object v10, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->mBuilder:Landroid/app/Notification$Builder;

    iget-object v11, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->appName:Ljava/lang/CharSequence;

    invoke-virtual {v10, v11}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v10

    .line 155
    invoke-virtual {v10, v6}, Landroid/app/Notification$Builder;->setPriority(I)Landroid/app/Notification$Builder;

    move-result-object v6

    iget-object v10, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->this$0:Lcom/android/server/location/GnssEventHandler;

    invoke-static {v10}, Lcom/android/server/location/GnssEventHandler;->-$$Nest$fgetmAppContext(Lcom/android/server/location/GnssEventHandler;)Landroid/content/Context;

    move-result-object v10

    .line 156
    const v11, 0x110f02ba

    invoke-virtual {v10, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v6

    new-array v10, v5, [Landroid/app/Notification$Action;

    iget-object v11, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->this$0:Lcom/android/server/location/GnssEventHandler;

    invoke-static {v11}, Lcom/android/server/location/GnssEventHandler;->-$$Nest$fgetmAction(Lcom/android/server/location/GnssEventHandler;)Landroid/app/Notification$Action;

    move-result-object v11

    aput-object v11, v10, v4

    .line 157
    invoke-virtual {v6, v10}, Landroid/app/Notification$Builder;->setActions([Landroid/app/Notification$Action;)Landroid/app/Notification$Builder;

    move-result-object v4

    .line 158
    invoke-virtual {v4, v9}, Landroid/app/Notification$Builder;->setExtras(Landroid/os/Bundle;)Landroid/app/Notification$Builder;

    move-result-object v4

    .line 159
    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    .line 160
    iget-object v4, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->this$0:Lcom/android/server/location/GnssEventHandler;

    invoke-static {v4}, Lcom/android/server/location/GnssEventHandler;->-$$Nest$fgetmAppContext(Lcom/android/server/location/GnssEventHandler;)Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->mCloseBlurLocationListener:Landroid/content/BroadcastReceiver;

    new-instance v6, Landroid/content/IntentFilter;

    invoke-static {}, Lcom/android/server/location/GnssEventHandler;->-$$Nest$sfgetCLOSE_BLUR_LOCATION()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v6, v10}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 165
    .end local v7    # "pm":Landroid/content/pm/PackageManager;
    .end local v8    # "blurLocationPendingIntent":Landroid/app/PendingIntent;
    .end local v9    # "bundle":Landroid/os/Bundle;
    :cond_2
    :goto_1
    goto :goto_2

    .line 163
    :catch_1
    move-exception v4

    .line 164
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    .line 166
    .end local v4    # "e":Ljava/lang/Exception;
    :goto_2
    return-void
.end method

.method private unregisterBlurLocationListener()V
    .locals 5

    .line 229
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 230
    .local v0, "queryIntent":Landroid/content/Intent;
    invoke-static {}, Lcom/android/server/location/GnssEventHandler;->-$$Nest$sfgetCLOSE_BLUR_LOCATION()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 231
    iget-object v1, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->this$0:Lcom/android/server/location/GnssEventHandler;

    invoke-static {v1}, Lcom/android/server/location/GnssEventHandler;->-$$Nest$fgetmAppContext(Lcom/android/server/location/GnssEventHandler;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 232
    .local v1, "pm":Landroid/content/pm/PackageManager;
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->queryBroadcastReceivers(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    .line 233
    .local v2, "resolveInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 234
    iget-object v3, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->this$0:Lcom/android/server/location/GnssEventHandler;

    invoke-static {v3}, Lcom/android/server/location/GnssEventHandler;->-$$Nest$fgetmAppContext(Lcom/android/server/location/GnssEventHandler;)Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->mCloseBlurLocationListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {v3, v4}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 236
    :cond_0
    return-void
.end method


# virtual methods
.method public initNotification()V
    .locals 3

    .line 107
    iget-object v0, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->this$0:Lcom/android/server/location/GnssEventHandler;

    invoke-static {v0}, Lcom/android/server/location/GnssEventHandler;->-$$Nest$fgetmAppContext(Lcom/android/server/location/GnssEventHandler;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->mNotificationManager:Landroid/app/NotificationManager;

    .line 108
    new-instance v0, Landroid/app/Notification$Builder;

    iget-object v1, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->this$0:Lcom/android/server/location/GnssEventHandler;

    invoke-static {v1}, Lcom/android/server/location/GnssEventHandler;->-$$Nest$fgetmAppContext(Lcom/android/server/location/GnssEventHandler;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "GPS_STATUS_MONITOR_ID"

    invoke-direct {v0, v1, v2}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->mBuilder:Landroid/app/Notification$Builder;

    .line 109
    invoke-direct {p0}, Lcom/android/server/location/GnssEventHandler$NotifyManager;->constructNotification()V

    .line 110
    return-void
.end method

.method public removeNotification()V
    .locals 5

    .line 239
    const-string v0, "removeNotification"

    const-string v1, "GnssNps"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    iget-object v0, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->mNotificationManager:Landroid/app/NotificationManager;

    if-nez v0, :cond_0

    .line 241
    const-string v0, "mNotificationManager is null"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    invoke-virtual {p0}, Lcom/android/server/location/GnssEventHandler$NotifyManager;->initNotification()V

    .line 245
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->mNotificationManager:Landroid/app/NotificationManager;

    if-eqz v0, :cond_1

    .line 247
    const v2, 0x110801b9

    .line 248
    .local v2, "id":I
    sget-object v3, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const/4 v4, 0x0

    invoke-virtual {v0, v4, v2, v3}, Landroid/app/NotificationManager;->cancelAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)V

    .line 249
    invoke-direct {p0}, Lcom/android/server/location/GnssEventHandler$NotifyManager;->unregisterBlurLocationListener()V

    .line 250
    iput-object v4, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->mBuilder:Landroid/app/Notification$Builder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 254
    .end local v2    # "id":I
    :cond_1
    goto :goto_0

    .line 252
    :catch_0
    move-exception v0

    .line 253
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public showNotification()V
    .locals 5

    .line 169
    iget-object v0, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->this$0:Lcom/android/server/location/GnssEventHandler;

    invoke-static {v0}, Lcom/android/server/location/GnssEventHandler;->-$$Nest$misChineseLanguage(Lcom/android/server/location/GnssEventHandler;)Z

    move-result v0

    const-string v1, "GnssNps"

    if-nez v0, :cond_0

    .line 170
    const-string/jumbo v0, "show notification only in CHINESE language"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    return-void

    .line 173
    :cond_0
    iget-object v0, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->packageName:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 174
    const-string v0, "no package name presence"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    return-void

    .line 179
    :cond_1
    const v0, 0x110801b9

    .line 180
    .local v0, "id":I
    :try_start_0
    iget-object v1, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->mNotificationManager:Landroid/app/NotificationManager;

    iget-object v2, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->mBuilder:Landroid/app/Notification$Builder;

    invoke-virtual {v2}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v2

    sget-object v3, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const/4 v4, 0x0

    invoke-virtual {v1, v4, v0, v2, v3}, Landroid/app/NotificationManager;->notifyAsUser(Ljava/lang/String;ILandroid/app/Notification;Landroid/os/UserHandle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 183
    .end local v0    # "id":I
    goto :goto_0

    .line 181
    :catch_0
    move-exception v0

    .line 182
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 184
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public updateCallerName(Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .line 208
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "updateCallerName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GnssNps"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    iput-object p1, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->packageName:Ljava/lang/String;

    .line 210
    const-string v0, "noise_environment"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->mnoise:Z

    if-nez v0, :cond_0

    .line 211
    iput-boolean v1, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->mnoise:Z

    .line 212
    iget-object v0, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->this$0:Lcom/android/server/location/GnssEventHandler;

    invoke-static {v0}, Lcom/android/server/location/GnssEventHandler;->-$$Nest$fgetmNotifyManager(Lcom/android/server/location/GnssEventHandler;)Lcom/android/server/location/GnssEventHandler$NotifyManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/location/GnssEventHandler$NotifyManager;->initNotification()V

    .line 213
    iget-object v0, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->this$0:Lcom/android/server/location/GnssEventHandler;

    invoke-static {v0}, Lcom/android/server/location/GnssEventHandler;->-$$Nest$fgetmNotifyManager(Lcom/android/server/location/GnssEventHandler;)Lcom/android/server/location/GnssEventHandler$NotifyManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/location/GnssEventHandler$NotifyManager;->showNotification()V

    goto :goto_0

    .line 214
    :cond_0
    const-string v0, "normal_environment"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->mnoise:Z

    if-ne v0, v1, :cond_1

    .line 215
    iput-boolean v2, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->mnoise:Z

    .line 216
    iget-object v0, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->this$0:Lcom/android/server/location/GnssEventHandler;

    invoke-static {v0}, Lcom/android/server/location/GnssEventHandler;->-$$Nest$fgetmNotifyManager(Lcom/android/server/location/GnssEventHandler;)Lcom/android/server/location/GnssEventHandler$NotifyManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/location/GnssEventHandler$NotifyManager;->removeNotification()V

    goto :goto_0

    .line 217
    :cond_1
    const-string v0, "blurLocation_notify is on"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 218
    iget-object v0, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->this$0:Lcom/android/server/location/GnssEventHandler;

    invoke-static {v0, v1}, Lcom/android/server/location/GnssEventHandler;->-$$Nest$fputmBlur(Lcom/android/server/location/GnssEventHandler;Z)V

    .line 219
    iget-object v0, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->this$0:Lcom/android/server/location/GnssEventHandler;

    invoke-static {v0}, Lcom/android/server/location/GnssEventHandler;->-$$Nest$fgetpkgName(Lcom/android/server/location/GnssEventHandler;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->packageName:Ljava/lang/String;

    .line 220
    iget-object v0, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->this$0:Lcom/android/server/location/GnssEventHandler;

    invoke-static {v0}, Lcom/android/server/location/GnssEventHandler;->-$$Nest$fgetmNotifyManager(Lcom/android/server/location/GnssEventHandler;)Lcom/android/server/location/GnssEventHandler$NotifyManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/location/GnssEventHandler$NotifyManager;->initNotification()V

    .line 221
    iget-object v0, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->this$0:Lcom/android/server/location/GnssEventHandler;

    invoke-static {v0}, Lcom/android/server/location/GnssEventHandler;->-$$Nest$fgetmNotifyManager(Lcom/android/server/location/GnssEventHandler;)Lcom/android/server/location/GnssEventHandler$NotifyManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/location/GnssEventHandler$NotifyManager;->showNotification()V

    goto :goto_0

    .line 222
    :cond_2
    const-string v0, "blurLocation_notify is off"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 223
    iget-object v0, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->this$0:Lcom/android/server/location/GnssEventHandler;

    invoke-static {v0, v2}, Lcom/android/server/location/GnssEventHandler;->-$$Nest$fputmBlur(Lcom/android/server/location/GnssEventHandler;Z)V

    .line 224
    iget-object v0, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager;->this$0:Lcom/android/server/location/GnssEventHandler;

    invoke-static {v0}, Lcom/android/server/location/GnssEventHandler;->-$$Nest$fgetmNotifyManager(Lcom/android/server/location/GnssEventHandler;)Lcom/android/server/location/GnssEventHandler$NotifyManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/location/GnssEventHandler$NotifyManager;->removeNotification()V

    .line 226
    :cond_3
    :goto_0
    return-void
.end method
