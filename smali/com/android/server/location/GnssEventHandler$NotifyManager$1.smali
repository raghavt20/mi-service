.class Lcom/android/server/location/GnssEventHandler$NotifyManager$1;
.super Landroid/content/BroadcastReceiver;
.source "GnssEventHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/location/GnssEventHandler$NotifyManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/server/location/GnssEventHandler$NotifyManager;


# direct methods
.method constructor <init>(Lcom/android/server/location/GnssEventHandler$NotifyManager;)V
    .locals 0
    .param p1, "this$1"    # Lcom/android/server/location/GnssEventHandler$NotifyManager;

    .line 186
    iput-object p1, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager$1;->this$1:Lcom/android/server/location/GnssEventHandler$NotifyManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 189
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 190
    .local v0, "mContext":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 191
    .local v1, "mPackageManager":Landroid/content/pm/PackageManager;
    const-string v2, "appops"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/AppOpsManager;

    .line 193
    .local v2, "appOpsManager":Landroid/app/AppOpsManager;
    const/4 v3, 0x0

    :try_start_0
    iget-object v4, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager$1;->this$1:Lcom/android/server/location/GnssEventHandler$NotifyManager;

    iget-object v4, v4, Lcom/android/server/location/GnssEventHandler$NotifyManager;->this$0:Lcom/android/server/location/GnssEventHandler;

    invoke-static {v4}, Lcom/android/server/location/GnssEventHandler;->-$$Nest$fgetpkgName(Lcom/android/server/location/GnssEventHandler;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 195
    .local v4, "mUid":I
    iget-object v5, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager$1;->this$1:Lcom/android/server/location/GnssEventHandler$NotifyManager;

    iget-object v5, v5, Lcom/android/server/location/GnssEventHandler$NotifyManager;->this$0:Lcom/android/server/location/GnssEventHandler;

    invoke-static {v5}, Lcom/android/server/location/GnssEventHandler;->-$$Nest$fgetpkgName(Lcom/android/server/location/GnssEventHandler;)Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x2734

    invoke-virtual {v2, v6, v4, v5, v3}, Landroid/app/AppOpsManager;->setMode(IILjava/lang/String;I)V

    .line 196
    iget-object v5, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager$1;->this$1:Lcom/android/server/location/GnssEventHandler$NotifyManager;

    invoke-static {v5}, Lcom/android/server/location/GnssEventHandler$NotifyManager;->-$$Nest$fgetmCloseBlurLocationListener(Lcom/android/server/location/GnssEventHandler$NotifyManager;)Landroid/content/BroadcastReceiver;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 197
    iget-object v5, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager$1;->this$1:Lcom/android/server/location/GnssEventHandler$NotifyManager;

    iget-object v5, v5, Lcom/android/server/location/GnssEventHandler$NotifyManager;->this$0:Lcom/android/server/location/GnssEventHandler;

    invoke-static {v5}, Lcom/android/server/location/GnssEventHandler;->-$$Nest$fgetmAppContext(Lcom/android/server/location/GnssEventHandler;)Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager$1;->this$1:Lcom/android/server/location/GnssEventHandler$NotifyManager;

    invoke-static {v6}, Lcom/android/server/location/GnssEventHandler$NotifyManager;->-$$Nest$fgetmCloseBlurLocationListener(Lcom/android/server/location/GnssEventHandler$NotifyManager;)Landroid/content/BroadcastReceiver;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 201
    .end local v4    # "mUid":I
    :cond_0
    goto :goto_0

    .line 199
    :catch_0
    move-exception v4

    .line 200
    .local v4, "e":Ljava/lang/Exception;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "exception in close blur location : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v4}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "GnssNps"

    invoke-static {v6, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    .end local v4    # "e":Ljava/lang/Exception;
    :goto_0
    iget-object v4, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager$1;->this$1:Lcom/android/server/location/GnssEventHandler$NotifyManager;

    invoke-virtual {v4}, Lcom/android/server/location/GnssEventHandler$NotifyManager;->removeNotification()V

    .line 203
    iget-object v4, p0, Lcom/android/server/location/GnssEventHandler$NotifyManager$1;->this$1:Lcom/android/server/location/GnssEventHandler$NotifyManager;

    iget-object v4, v4, Lcom/android/server/location/GnssEventHandler$NotifyManager;->this$0:Lcom/android/server/location/GnssEventHandler;

    invoke-static {v4, v3}, Lcom/android/server/location/GnssEventHandler;->-$$Nest$fputmBlur(Lcom/android/server/location/GnssEventHandler;Z)V

    .line 204
    return-void
.end method
