.class public Lcom/android/server/location/Order;
.super Ljava/lang/Object;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/location/Order$OnChangeListener;
    }
.end annotation


# static fields
.field private static flag:Ljava/lang/String;

.field private static onChangeListener:Lcom/android/server/location/Order$OnChangeListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getFlag()Ljava/lang/String;
    .locals 1

    .line 17
    sget-object v0, Lcom/android/server/location/Order;->flag:Ljava/lang/String;

    return-object v0
.end method

.method public static setFlag(Ljava/lang/String;)V
    .locals 1
    .param p0, "flag"    # Ljava/lang/String;

    .line 21
    sput-object p0, Lcom/android/server/location/Order;->flag:Ljava/lang/String;

    .line 22
    sget-object v0, Lcom/android/server/location/Order;->onChangeListener:Lcom/android/server/location/Order$OnChangeListener;

    invoke-interface {v0}, Lcom/android/server/location/Order$OnChangeListener;->onChange()V

    .line 23
    return-void
.end method

.method public static setOnChangeListener(Lcom/android/server/location/Order$OnChangeListener;)V
    .locals 0
    .param p0, "onChange"    # Lcom/android/server/location/Order$OnChangeListener;

    .line 11
    sput-object p0, Lcom/android/server/location/Order;->onChangeListener:Lcom/android/server/location/Order$OnChangeListener;

    .line 12
    return-void
.end method
