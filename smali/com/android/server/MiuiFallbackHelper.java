public class com.android.server.MiuiFallbackHelper extends com.android.server.MiuiFallbackHelperStub {
	 /* .source "MiuiFallbackHelper.java" */
	 /* # static fields */
	 private static final java.lang.String FALLBACK_FILE_SUFFIX;
	 private static final java.lang.String FALLBACK_TEMP_FILE_SUFFIX;
	 private static final java.lang.String TAG;
	 private static final java.lang.String XATTR_MD5;
	 /* # direct methods */
	 static com.android.server.MiuiFallbackHelper ( ) {
		 /* .locals 1 */
		 /* .line 27 */
		 /* const-class v0, Lcom/android/server/MiuiFallbackHelper; */
		 (( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
		 return;
	 } // .end method
	 public com.android.server.MiuiFallbackHelper ( ) {
		 /* .locals 0 */
		 /* .line 26 */
		 /* invoke-direct {p0}, Lcom/android/server/MiuiFallbackHelperStub;-><init>()V */
		 return;
	 } // .end method
	 private calculateMD5 ( java.io.File p0, Boolean p1 ) {
		 /* .locals 8 */
		 /* .param p1, "file" # Ljava/io/File; */
		 /* .param p2, "setXattr" # Z */
		 /* .line 129 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* .line 130 */
		 /* .local v0, "md5sum":[B */
		 try { // :try_start_0
			 /* new-instance v1, Ljava/io/FileInputStream; */
			 /* invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V */
			 /* :try_end_0 */
			 /* .catch Landroid/system/ErrnoException; {:try_start_0 ..:try_end_0} :catch_2 */
			 /* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_1 */
			 /* .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* .line 131 */
			 /* .local v1, "fis":Ljava/io/FileInputStream; */
			 try { // :try_start_1
				 final String v2 = "MD5"; // const-string v2, "MD5"
				 java.security.MessageDigest .getInstance ( v2 );
				 /* .line 133 */
				 /* .local v2, "digest":Ljava/security/MessageDigest; */
				 /* const/16 v3, 0x2000 */
				 /* new-array v3, v3, [B */
				 /* .line 134 */
				 /* .local v3, "buffer":[B */
				 int v4 = 0; // const/4 v4, 0x0
				 /* .line 135 */
				 /* .local v4, "read":I */
			 } // :goto_0
			 v5 = 			 (( java.io.FileInputStream ) v1 ).read ( v3 ); // invoke-virtual {v1, v3}, Ljava/io/FileInputStream;->read([B)I
			 /* move v4, v5 */
			 int v6 = 0; // const/4 v6, 0x0
			 /* if-lez v5, :cond_0 */
			 /* .line 136 */
			 (( java.security.MessageDigest ) v2 ).update ( v3, v6, v4 ); // invoke-virtual {v2, v3, v6, v4}, Ljava/security/MessageDigest;->update([BII)V
			 /* .line 138 */
		 } // :cond_0
		 (( java.security.MessageDigest ) v2 ).digest ( ); // invoke-virtual {v2}, Ljava/security/MessageDigest;->digest()[B
		 /* move-object v0, v5 */
		 /* .line 139 */
		 if ( p2 != null) { // if-eqz p2, :cond_1
			 /* .line 140 */
			 (( java.io.File ) p1 ).getAbsolutePath ( ); // invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
			 /* const-string/jumbo v7, "user.md5" */
			 android.system.Os .setxattr ( v5,v7,v0,v6 );
			 /* :try_end_1 */
			 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
			 /* .line 142 */
		 } // .end local v2 # "digest":Ljava/security/MessageDigest;
	 } // .end local v3 # "buffer":[B
} // .end local v4 # "read":I
} // :cond_1
try { // :try_start_2
(( java.io.FileInputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
/* :try_end_2 */
/* .catch Landroid/system/ErrnoException; {:try_start_2 ..:try_end_2} :catch_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_1 */
/* .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 130 */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_3
	 (( java.io.FileInputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
	 /* :try_end_3 */
	 /* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
	 /* :catchall_1 */
	 /* move-exception v3 */
	 try { // :try_start_4
		 (( java.lang.Throwable ) v2 ).addSuppressed ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
	 } // .end local v0 # "md5sum":[B
} // .end local p0 # "this":Lcom/android/server/MiuiFallbackHelper;
} // .end local p1 # "file":Ljava/io/File;
} // .end local p2 # "setXattr":Z
} // :goto_1
/* throw v2 */
/* :try_end_4 */
/* .catch Landroid/system/ErrnoException; {:try_start_4 ..:try_end_4} :catch_2 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_1 */
/* .catch Ljava/security/NoSuchAlgorithmException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 146 */
} // .end local v1 # "fis":Ljava/io/FileInputStream;
/* .restart local v0 # "md5sum":[B */
/* .restart local p0 # "this":Lcom/android/server/MiuiFallbackHelper; */
/* .restart local p1 # "file":Ljava/io/File; */
/* .restart local p2 # "setXattr":Z */
/* :catch_0 */
/* move-exception v1 */
/* .line 147 */
/* .local v1, "e":Ljava/security/NoSuchAlgorithmException; */
v2 = com.android.server.MiuiFallbackHelper.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "calculateMD5 NoSuchAlgorithmException: "; // const-string v4, "calculateMD5 NoSuchAlgorithmException: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 144 */
} // .end local v1 # "e":Ljava/security/NoSuchAlgorithmException;
/* :catch_1 */
/* move-exception v1 */
/* .line 145 */
/* .local v1, "e":Ljava/io/IOException; */
v2 = com.android.server.MiuiFallbackHelper.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "calculateMD5 IOException: "; // const-string v4, "calculateMD5 IOException: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
} // .end local v1 # "e":Ljava/io/IOException;
/* .line 142 */
/* :catch_2 */
/* move-exception v1 */
/* .line 143 */
/* .local v1, "e":Landroid/system/ErrnoException; */
v2 = com.android.server.MiuiFallbackHelper.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "calculateMD5 ErrnoException: "; // const-string v4, "calculateMD5 ErrnoException: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 148 */
} // .end local v1 # "e":Landroid/system/ErrnoException;
} // :goto_2
/* nop */
/* .line 150 */
} // :goto_3
} // .end method
private Boolean checkNeedCopy ( java.lang.String p0, Long p1 ) {
/* .locals 7 */
/* .param p1, "filePath" # Ljava/lang/String; */
/* .param p2, "periodTime" # J */
/* .line 119 */
/* invoke-direct {p0, p1}, Lcom/android/server/MiuiFallbackHelper;->getFallbackFile(Ljava/lang/String;)Ljava/io/File; */
/* .line 120 */
/* .local v0, "fallbackFile":Ljava/io/File; */
v1 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
int v2 = 1; // const/4 v2, 0x1
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 121 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v3 */
(( java.io.File ) v0 ).lastModified ( ); // invoke-virtual {v0}, Ljava/io/File;->lastModified()J
/* move-result-wide v5 */
/* sub-long/2addr v3, v5 */
/* cmp-long v1, v3, p2 */
/* if-lez v1, :cond_0 */
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
/* .line 124 */
} // :cond_1
} // .end method
private java.io.File getFallbackFile ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "filePath" # Ljava/lang/String; */
/* .line 154 */
/* new-instance v0, Ljava/io/File; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ".fallback"; // const-string v2, ".fallback"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
} // .end method
private java.io.File getFallbackTempFile ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "filePath" # Ljava/lang/String; */
/* .line 158 */
/* new-instance v0, Ljava/io/File; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ".fallbacktemp"; // const-string v2, ".fallbacktemp"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
} // .end method
private void reportFileCorruptionForMqs ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "filePath" # Ljava/lang/String; */
/* .param p2, "timeStamp" # Ljava/lang/String; */
/* .line 162 */
/* new-instance v0, Lmiui/mqsas/sdk/event/GeneralExceptionEvent; */
/* invoke-direct {v0}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;-><init>()V */
/* .line 163 */
/* .local v0, "event":Lmiui/mqsas/sdk/event/GeneralExceptionEvent; */
/* const/16 v1, 0x1b7 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v0 ).setType ( v1 ); // invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setType(I)V
/* .line 164 */
int v1 = 1; // const/4 v1, 0x1
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v0 ).setSystem ( v1 ); // invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setSystem(Z)V
/* .line 165 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v2 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v0 ).setTimeStamp ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setTimeStamp(J)V
/* .line 166 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v0 ).setSummary ( p1 ); // invoke-virtual {v0, p1}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setSummary(Ljava/lang/String;)V
/* .line 167 */
final String v2 = "android"; // const-string v2, "android"
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v0 ).setPackageName ( v2 ); // invoke-virtual {v0, v2}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setPackageName(Ljava/lang/String;)V
/* .line 168 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v0 ).setDetails ( p2 ); // invoke-virtual {v0, p2}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setDetails(Ljava/lang/String;)V
/* .line 169 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v0 ).setUpload ( v1 ); // invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setUpload(Z)V
/* .line 170 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v0 ).setEnsureReport ( v1 ); // invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setEnsureReport(Z)V
/* .line 171 */
miui.mqsas.sdk.MQSEventManagerDelegate .getInstance ( );
(( miui.mqsas.sdk.MQSEventManagerDelegate ) v1 ).reportGeneralException ( v0 ); // invoke-virtual {v1, v0}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->reportGeneralException(Lmiui/mqsas/sdk/event/GeneralExceptionEvent;)Z
/* .line 172 */
return;
} // .end method
/* # virtual methods */
public Boolean restoreFile ( java.lang.String p0 ) {
/* .locals 10 */
/* .param p1, "filePath" # Ljava/lang/String; */
/* .line 78 */
final String v0 = "restoreFile: "; // const-string v0, "restoreFile: "
int v1 = 0; // const/4 v1, 0x0
/* .line 79 */
/* .local v1, "result":Z */
/* invoke-direct {p0, p1}, Lcom/android/server/MiuiFallbackHelper;->getFallbackFile(Ljava/lang/String;)Ljava/io/File; */
/* .line 80 */
/* .local v2, "fallbackFile":Ljava/io/File; */
/* invoke-direct {p0, p1}, Lcom/android/server/MiuiFallbackHelper;->getFallbackTempFile(Ljava/lang/String;)Ljava/io/File; */
/* .line 81 */
/* .local v3, "fallbackTempFile":Ljava/io/File; */
/* new-instance v4, Ljava/io/File; */
/* invoke-direct {v4, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 82 */
/* .local v4, "originalFile":Ljava/io/File; */
v5 = (( java.io.File ) v2 ).exists ( ); // invoke-virtual {v2}, Ljava/io/File;->exists()Z
int v6 = 0; // const/4 v6, 0x0
if ( v5 != null) { // if-eqz v5, :cond_1
v5 = (( java.io.File ) v3 ).exists ( ); // invoke-virtual {v3}, Ljava/io/File;->exists()Z
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 83 */
v5 = (( java.io.File ) v3 ).renameTo ( v2 ); // invoke-virtual {v3, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 84 */
v5 = com.android.server.MiuiFallbackHelper.TAG;
final String v7 = "fallback file maybe saved abnormally last time.trust temp file"; // const-string v7, "fallback file maybe saved abnormally last time.trust temp file"
android.util.Slog .w ( v5,v7 );
/* .line 86 */
} // :cond_0
v0 = com.android.server.MiuiFallbackHelper.TAG;
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "fallback temp file rename failed: "; // const-string v7, "fallback temp file rename failed: "
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 87 */
(( java.io.File ) v3 ).getAbsolutePath ( ); // invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 86 */
android.util.Slog .w ( v0,v5 );
/* .line 88 */
/* .line 91 */
} // :cond_1
} // :goto_0
v5 = (( java.io.File ) v2 ).exists ( ); // invoke-virtual {v2}, Ljava/io/File;->exists()Z
if ( v5 != null) { // if-eqz v5, :cond_4
/* .line 92 */
/* invoke-direct {p0, v2, v6}, Lcom/android/server/MiuiFallbackHelper;->calculateMD5(Ljava/io/File;Z)[B */
/* .line 93 */
/* .local v5, "md5sum":[B */
int v6 = 0; // const/4 v6, 0x0
/* .line 95 */
/* .local v6, "xattr":[B */
try { // :try_start_0
(( java.io.File ) v2 ).getAbsolutePath ( ); // invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
/* const-string/jumbo v8, "user.md5" */
android.system.Os .getxattr ( v7,v8 );
/* :try_end_0 */
/* .catch Landroid/system/ErrnoException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object v6, v7 */
/* .line 98 */
/* .line 96 */
/* :catch_0 */
/* move-exception v7 */
/* .line 97 */
/* .local v7, "e":Landroid/system/ErrnoException; */
v8 = com.android.server.MiuiFallbackHelper.TAG;
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v9 ).append ( v0 ); // invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v7 ); // invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v8,v9 );
/* .line 99 */
} // .end local v7 # "e":Landroid/system/ErrnoException;
} // :goto_1
if ( v5 != null) { // if-eqz v5, :cond_3
v7 = java.util.Arrays .equals ( v5,v6 );
if ( v7 != null) { // if-eqz v7, :cond_3
/* .line 100 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = " exists but is corrupted.Fallback file will be used.lastModified: "; // const-string v7, " exists but is corrupted.Fallback file will be used.lastModified: "
(( java.lang.StringBuilder ) v0 ).append ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 102 */
(( java.io.File ) v2 ).lastModified ( ); // invoke-virtual {v2}, Ljava/io/File;->lastModified()J
/* move-result-wide v7 */
(( java.lang.StringBuilder ) v0 ).append ( v7, v8 ); // invoke-virtual {v0, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 103 */
/* .local v0, "msg":Ljava/lang/String; */
(( java.io.File ) v2 ).lastModified ( ); // invoke-virtual {v2}, Ljava/io/File;->lastModified()J
/* move-result-wide v7 */
java.lang.String .valueOf ( v7,v8 );
/* .line 104 */
/* .local v7, "timeStamp":Ljava/lang/String; */
int v8 = 3; // const/4 v8, 0x3
com.android.server.pm.PackageManagerService .reportSettingsProblem ( v8,v0 );
/* .line 105 */
v8 = (( java.io.File ) v2 ).renameTo ( v4 ); // invoke-virtual {v2, v4}, Ljava/io/File;->renameTo(Ljava/io/File;)Z
if ( v8 != null) { // if-eqz v8, :cond_2
/* .line 106 */
int v1 = 1; // const/4 v1, 0x1
/* .line 107 */
(( java.io.File ) v4 ).getPath ( ); // invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;
/* .line 108 */
/* .local v8, "restoreFilePath":Ljava/lang/String; */
/* invoke-direct {p0, v8, v7}, Lcom/android/server/MiuiFallbackHelper;->reportFileCorruptionForMqs(Ljava/lang/String;Ljava/lang/String;)V */
/* .line 110 */
} // .end local v0 # "msg":Ljava/lang/String;
} // .end local v7 # "timeStamp":Ljava/lang/String;
} // .end local v8 # "restoreFilePath":Ljava/lang/String;
} // :cond_2
/* .line 111 */
} // :cond_3
v7 = com.android.server.MiuiFallbackHelper.TAG;
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v8 ).append ( v0 ); // invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = ".but MD5 is not equal!"; // const-string v8, ".but MD5 is not equal!"
(( java.lang.StringBuilder ) v0 ).append ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v7,v0 );
/* .line 115 */
} // .end local v5 # "md5sum":[B
} // .end local v6 # "xattr":[B
} // :cond_4
} // :goto_2
} // .end method
public void snapshotFile ( java.lang.String p0, Long p1 ) {
/* .locals 10 */
/* .param p1, "filePath" # Ljava/lang/String; */
/* .param p2, "periodTime" # J */
/* .line 34 */
/* const-wide/16 v0, 0x0 */
/* cmp-long v2, p2, v0 */
/* if-ltz v2, :cond_5 */
/* .line 37 */
/* new-instance v2, Ljava/io/File; */
/* invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 38 */
/* .local v2, "originalFile":Ljava/io/File; */
v3 = (( java.io.File ) v2 ).exists ( ); // invoke-virtual {v2}, Ljava/io/File;->exists()Z
if ( v3 != null) { // if-eqz v3, :cond_4
/* cmp-long v0, p2, v0 */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = /* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/MiuiFallbackHelper;->checkNeedCopy(Ljava/lang/String;J)Z */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 39 */
} // :cond_0
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 40 */
/* .local v0, "startTime":J */
/* invoke-direct {p0, p1}, Lcom/android/server/MiuiFallbackHelper;->getFallbackFile(Ljava/lang/String;)Ljava/io/File; */
/* .line 41 */
/* .local v3, "fallbackFile":Ljava/io/File; */
/* invoke-direct {p0, p1}, Lcom/android/server/MiuiFallbackHelper;->getFallbackTempFile(Ljava/lang/String;)Ljava/io/File; */
/* .line 43 */
/* .local v4, "fallbackTempFile":Ljava/io/File; */
v5 = (( java.io.File ) v3 ).exists ( ); // invoke-virtual {v3}, Ljava/io/File;->exists()Z
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 44 */
v5 = (( java.io.File ) v4 ).exists ( ); // invoke-virtual {v4}, Ljava/io/File;->exists()Z
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 45 */
(( java.io.File ) v3 ).delete ( ); // invoke-virtual {v3}, Ljava/io/File;->delete()Z
/* .line 46 */
v5 = com.android.server.MiuiFallbackHelper.TAG;
final String v6 = "Preserving older fallback file backup"; // const-string v6, "Preserving older fallback file backup"
android.util.Slog .w ( v5,v6 );
/* .line 47 */
} // :cond_1
v5 = (( java.io.File ) v3 ).renameTo ( v4 ); // invoke-virtual {v3, v4}, Ljava/io/File;->renameTo(Ljava/io/File;)Z
/* if-nez v5, :cond_2 */
/* .line 48 */
v5 = com.android.server.MiuiFallbackHelper.TAG;
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Unable to backup "; // const-string v7, "Unable to backup "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( p1 ); // invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v5,v6 );
/* .line 49 */
return;
/* .line 53 */
} // :cond_2
} // :goto_0
try { // :try_start_0
/* new-instance v5, Ljava/io/FileInputStream; */
/* invoke-direct {v5, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 54 */
/* .local v5, "fis":Ljava/io/FileInputStream; */
try { // :try_start_1
/* new-instance v6, Ljava/io/FileOutputStream; */
/* invoke-direct {v6, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_2 */
/* .line 55 */
/* .local v6, "fos":Ljava/io/FileOutputStream; */
try { // :try_start_2
android.os.FileUtils .copy ( v5,v6 );
/* .line 56 */
(( java.io.FileOutputStream ) v6 ).flush ( ); // invoke-virtual {v6}, Ljava/io/FileOutputStream;->flush()V
/* .line 57 */
int v7 = 1; // const/4 v7, 0x1
/* invoke-direct {p0, v3, v7}, Lcom/android/server/MiuiFallbackHelper;->calculateMD5(Ljava/io/File;Z)[B */
/* .line 58 */
android.os.FileUtils .sync ( v6 );
/* .line 60 */
v7 = (( java.io.File ) v4 ).exists ( ); // invoke-virtual {v4}, Ljava/io/File;->exists()Z
if ( v7 != null) { // if-eqz v7, :cond_3
/* .line 61 */
(( java.io.File ) v4 ).delete ( ); // invoke-virtual {v4}, Ljava/io/File;->delete()Z
/* .line 65 */
} // :cond_3
(( java.io.File ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/io/File;->toString()Ljava/lang/String;
/* const/16 v8, 0x120 */
int v9 = -1; // const/4 v9, -0x1
android.os.FileUtils .setPermissions ( v7,v8,v9,v9 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 67 */
try { // :try_start_3
(( java.io.FileOutputStream ) v6 ).close ( ); // invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_2 */
} // .end local v6 # "fos":Ljava/io/FileOutputStream;
try { // :try_start_4
(( java.io.FileInputStream ) v5 ).close ( ); // invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 69 */
} // .end local v5 # "fis":Ljava/io/FileInputStream;
/* .line 53 */
/* .restart local v5 # "fis":Ljava/io/FileInputStream; */
/* .restart local v6 # "fos":Ljava/io/FileOutputStream; */
/* :catchall_0 */
/* move-exception v7 */
try { // :try_start_5
(( java.io.FileOutputStream ) v6 ).close ( ); // invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_1 */
/* :catchall_1 */
/* move-exception v8 */
try { // :try_start_6
(( java.lang.Throwable ) v7 ).addSuppressed ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "startTime":J
} // .end local v2 # "originalFile":Ljava/io/File;
} // .end local v3 # "fallbackFile":Ljava/io/File;
} // .end local v4 # "fallbackTempFile":Ljava/io/File;
} // .end local v5 # "fis":Ljava/io/FileInputStream;
} // .end local p0 # "this":Lcom/android/server/MiuiFallbackHelper;
} // .end local p1 # "filePath":Ljava/lang/String;
} // .end local p2 # "periodTime":J
} // :goto_1
/* throw v7 */
/* :try_end_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_2 */
} // .end local v6 # "fos":Ljava/io/FileOutputStream;
/* .restart local v0 # "startTime":J */
/* .restart local v2 # "originalFile":Ljava/io/File; */
/* .restart local v3 # "fallbackFile":Ljava/io/File; */
/* .restart local v4 # "fallbackTempFile":Ljava/io/File; */
/* .restart local v5 # "fis":Ljava/io/FileInputStream; */
/* .restart local p0 # "this":Lcom/android/server/MiuiFallbackHelper; */
/* .restart local p1 # "filePath":Ljava/lang/String; */
/* .restart local p2 # "periodTime":J */
/* :catchall_2 */
/* move-exception v6 */
try { // :try_start_7
(( java.io.FileInputStream ) v5 ).close ( ); // invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
/* :try_end_7 */
/* .catchall {:try_start_7 ..:try_end_7} :catchall_3 */
/* :catchall_3 */
/* move-exception v7 */
try { // :try_start_8
(( java.lang.Throwable ) v6 ).addSuppressed ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "startTime":J
} // .end local v2 # "originalFile":Ljava/io/File;
} // .end local v3 # "fallbackFile":Ljava/io/File;
} // .end local v4 # "fallbackTempFile":Ljava/io/File;
} // .end local p0 # "this":Lcom/android/server/MiuiFallbackHelper;
} // .end local p1 # "filePath":Ljava/lang/String;
} // .end local p2 # "periodTime":J
} // :goto_2
/* throw v6 */
/* :try_end_8 */
/* .catch Ljava/io/IOException; {:try_start_8 ..:try_end_8} :catch_0 */
/* .line 67 */
} // .end local v5 # "fis":Ljava/io/FileInputStream;
/* .restart local v0 # "startTime":J */
/* .restart local v2 # "originalFile":Ljava/io/File; */
/* .restart local v3 # "fallbackFile":Ljava/io/File; */
/* .restart local v4 # "fallbackTempFile":Ljava/io/File; */
/* .restart local p0 # "this":Lcom/android/server/MiuiFallbackHelper; */
/* .restart local p1 # "filePath":Ljava/lang/String; */
/* .restart local p2 # "periodTime":J */
/* :catch_0 */
/* move-exception v5 */
/* .line 68 */
/* .local v5, "e":Ljava/io/IOException; */
v6 = com.android.server.MiuiFallbackHelper.TAG;
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "Failed to write fallback file for: "; // const-string v8, "Failed to write fallback file for: "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( p1 ); // invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v6,v7 );
/* .line 71 */
} // .end local v5 # "e":Ljava/io/IOException;
} // :goto_3
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v6, "snapshotFile: " */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( p1 ); // invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 72 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v6 */
/* sub-long/2addr v6, v0 */
/* .line 71 */
com.android.internal.logging.EventLogTags .writeCommitSysConfigFile ( v5,v6,v7 );
/* .line 74 */
} // .end local v0 # "startTime":J
} // .end local v3 # "fallbackFile":Ljava/io/File;
} // .end local v4 # "fallbackTempFile":Ljava/io/File;
} // :cond_4
return;
/* .line 35 */
} // .end local v2 # "originalFile":Ljava/io/File;
} // :cond_5
/* new-instance v0, Ljava/lang/IllegalArgumentException; */
/* const-string/jumbo v1, "snapshotFile periodTime must >= 0" */
/* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
