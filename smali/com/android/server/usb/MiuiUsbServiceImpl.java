public class com.android.server.usb.MiuiUsbServiceImpl extends com.android.server.usb.MiuiUsbServiceStub {
	 /* .source "MiuiUsbServiceImpl.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.usb.MiuiUsbServiceStub$$" */
} // .end annotation
/* # static fields */
private static final java.lang.String CARLIFE_PACKAGE_NAME;
private static final java.lang.String CARWITH_PACKAGE_NAME;
private static final Integer FLAG_NON_ANONYMOUS;
public static final Integer SPECIAL_PRODUCT_ID;
public static final Integer SPECIAL_VENDOR_ID;
/* # instance fields */
private final java.lang.String TAG;
/* # direct methods */
public com.android.server.usb.MiuiUsbServiceImpl ( ) {
	 /* .locals 1 */
	 /* .line 25 */
	 /* invoke-direct {p0}, Lcom/android/server/usb/MiuiUsbServiceStub;-><init>()V */
	 /* .line 26 */
	 final String v0 = "MiuiUsbServiceImpl"; // const-string v0, "MiuiUsbServiceImpl"
	 this.TAG = v0;
	 return;
} // .end method
/* # virtual methods */
public void collectUsbHostConnectedInfo ( android.content.Context p0, com.android.server.usb.descriptors.UsbDescriptorParser p1 ) {
	 /* .locals 9 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .param p2, "parser" # Lcom/android/server/usb/descriptors/UsbDescriptorParser; */
	 /* .line 37 */
	 final String v0 = "MiuiUsbServiceImpl"; // const-string v0, "MiuiUsbServiceImpl"
	 miui.util.IMiCharge .getInstance ( );
	 /* .line 39 */
	 /* .local v1, "miCharge":Lmiui/util/IMiCharge; */
	 /* sget-boolean v2, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
	 /* if-nez v2, :cond_7 */
	 /* .line 40 */
	 v2 = 	 (( com.android.server.usb.descriptors.UsbDescriptorParser ) p2 ).hasAudioInterface ( ); // invoke-virtual {p2}, Lcom/android/server/usb/descriptors/UsbDescriptorParser;->hasAudioInterface()Z
	 /* .line 41 */
	 /* .local v2, "hasAudio":Z */
	 v3 = 	 (( com.android.server.usb.descriptors.UsbDescriptorParser ) p2 ).hasHIDInterface ( ); // invoke-virtual {p2}, Lcom/android/server/usb/descriptors/UsbDescriptorParser;->hasHIDInterface()Z
	 /* .line 42 */
	 /* .local v3, "hasHid":Z */
	 v4 = 	 (( com.android.server.usb.descriptors.UsbDescriptorParser ) p2 ).hasStorageInterface ( ); // invoke-virtual {p2}, Lcom/android/server/usb/descriptors/UsbDescriptorParser;->hasStorageInterface()Z
	 /* .line 43 */
	 /* .local v4, "hasStorage":Z */
	 final String v5 = ""; // const-string v5, ""
	 /* .line 45 */
	 /* .local v5, "value":Ljava/lang/String; */
	 v6 = 	 (( miui.util.IMiCharge ) v1 ).isDPConnected ( ); // invoke-virtual {v1}, Lmiui/util/IMiCharge;->isDPConnected()Z
	 if ( v6 != null) { // if-eqz v6, :cond_0
		 /* .line 46 */
		 /* new-instance v6, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
		 (( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 final String v7 = "DP"; // const-string v7, "DP"
		 (( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 /* .line 48 */
	 } // :cond_0
	 if ( v2 != null) { // if-eqz v2, :cond_1
		 /* new-instance v6, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
		 (( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 final String v7 = "Audio"; // const-string v7, "Audio"
		 (( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 /* .line 49 */
	 } // :cond_1
	 if ( v3 != null) { // if-eqz v3, :cond_2
		 /* new-instance v6, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
		 (( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 final String v7 = "HID"; // const-string v7, "HID"
		 (( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 /* .line 50 */
	 } // :cond_2
	 if ( v4 != null) { // if-eqz v4, :cond_3
		 /* new-instance v6, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
		 (( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 final String v7 = "Storage"; // const-string v7, "Storage"
		 (( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 /* .line 51 */
	 } // :cond_3
	 if ( v5 != null) { // if-eqz v5, :cond_4
		 v6 = 		 (( java.lang.String ) v5 ).length ( ); // invoke-virtual {v5}, Ljava/lang/String;->length()I
		 /* if-nez v6, :cond_5 */
		 /* .line 52 */
	 } // :cond_4
	 /* new-instance v6, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
	 (( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 final String v7 = "Other"; // const-string v7, "Other"
	 (( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 /* .line 56 */
} // :cond_5
} // :goto_0
/* new-instance v6, Landroid/content/Intent; */
final String v7 = "onetrack.action.TRACK_EVENT"; // const-string v7, "onetrack.action.TRACK_EVENT"
/* invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 57 */
/* .local v6, "intent":Landroid/content/Intent; */
final String v7 = "com.miui.analytics"; // const-string v7, "com.miui.analytics"
(( android.content.Intent ) v6 ).setPackage ( v7 ); // invoke-virtual {v6, v7}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 58 */
final String v7 = "APP_ID"; // const-string v7, "APP_ID"
final String v8 = "31000000092"; // const-string v8, "31000000092"
(( android.content.Intent ) v6 ).putExtra ( v7, v8 ); // invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 59 */
final String v7 = "EVENT_NAME"; // const-string v7, "EVENT_NAME"
/* const-string/jumbo v8, "usb_host" */
(( android.content.Intent ) v6 ).putExtra ( v7, v8 ); // invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 60 */
final String v7 = "PACKAGE"; // const-string v7, "PACKAGE"
final String v8 = "Android"; // const-string v8, "Android"
(( android.content.Intent ) v6 ).putExtra ( v7, v8 ); // invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 61 */
final String v7 = "device_connected"; // const-string v7, "device_connected"
(( android.content.Intent ) v6 ).putExtra ( v7, v5 ); // invoke-virtual {v6, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 62 */
v7 = (( miui.util.IMiCharge ) v1 ).isUSB32 ( ); // invoke-virtual {v1}, Lmiui/util/IMiCharge;->isUSB32()Z
final String v8 = "USB32"; // const-string v8, "USB32"
(( android.content.Intent ) v6 ).putExtra ( v8, v7 ); // invoke-virtual {v6, v8, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 63 */
/* sget-boolean v7, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
/* if-nez v7, :cond_6 */
/* .line 64 */
int v7 = 2; // const/4 v7, 0x2
(( android.content.Intent ) v6 ).setFlags ( v7 ); // invoke-virtual {v6, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 67 */
} // :cond_6
try { // :try_start_0
v7 = android.os.UserHandle.CURRENT;
(( android.content.Context ) p1 ).startServiceAsUser ( v6, v7 ); // invoke-virtual {p1, v6, v7}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
/* :try_end_0 */
/* .catch Ljava/lang/IllegalStateException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 70 */
/* .line 68 */
/* :catch_0 */
/* move-exception v7 */
/* .line 69 */
/* .local v7, "e":Ljava/lang/IllegalStateException; */
final String v8 = "Start one-Track service failed"; // const-string v8, "Start one-Track service failed"
android.util.Slog .e ( v0,v8,v7 );
/* .line 71 */
} // .end local v7 # "e":Ljava/lang/IllegalStateException;
} // :goto_1
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "connected device : "; // const-string v8, "connected device : "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v5 ); // invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v7 );
/* .line 73 */
} // .end local v2 # "hasAudio":Z
} // .end local v3 # "hasHid":Z
} // .end local v4 # "hasStorage":Z
} // .end local v5 # "value":Ljava/lang/String;
} // .end local v6 # "intent":Landroid/content/Intent;
} // :cond_7
return;
} // .end method
public Boolean isSpecialKeyBoard ( android.hardware.usb.UsbDevice p0 ) {
/* .locals 2 */
/* .param p1, "device" # Landroid/hardware/usb/UsbDevice; */
/* .line 77 */
/* const/16 v0, 0x3206 */
v1 = (( android.hardware.usb.UsbDevice ) p1 ).getVendorId ( ); // invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getVendorId()I
/* if-ne v0, v1, :cond_0 */
/* .line 78 */
v0 = (( android.hardware.usb.UsbDevice ) p1 ).getProductId ( ); // invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getProductId()I
/* const/16 v1, 0x3ffc */
/* if-ne v1, v0, :cond_0 */
/* .line 79 */
int v0 = 1; // const/4 v0, 0x1
/* .line 81 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean preSelectUsbHandlerForCarWith ( android.content.Context p0, android.content.Intent p1, java.util.ArrayList p2, android.hardware.usb.UsbDevice p3, android.hardware.usb.UsbAccessory p4, com.android.server.usb.UsbSettingsManager p5 ) {
/* .locals 14 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .param p4, "device" # Landroid/hardware/usb/UsbDevice; */
/* .param p5, "accessory" # Landroid/hardware/usb/UsbAccessory; */
/* .param p6, "settingsManager" # Lcom/android/server/usb/UsbSettingsManager; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/content/Context;", */
/* "Landroid/content/Intent;", */
/* "Ljava/util/ArrayList<", */
/* "Landroid/content/pm/ResolveInfo;", */
/* ">;", */
/* "Landroid/hardware/usb/UsbDevice;", */
/* "Landroid/hardware/usb/UsbAccessory;", */
/* "Lcom/android/server/usb/UsbSettingsManager;", */
/* ")Z" */
/* } */
} // .end annotation
/* .line 88 */
/* .local p3, "matches":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/pm/ResolveInfo;>;" */
/* move-object v0, p1 */
/* move-object/from16 v1, p2 */
/* move-object/from16 v2, p5 */
int v3 = 0; // const/4 v3, 0x0
/* .line 89 */
/* .local v3, "isConfirmed":Z */
v4 = /* invoke-virtual/range {p3 ..p3}, Ljava/util/ArrayList;->size()I */
int v5 = 2; // const/4 v5, 0x2
/* if-ne v4, v5, :cond_7 */
/* if-nez p4, :cond_7 */
if ( v2 != null) { // if-eqz v2, :cond_7
/* .line 91 */
int v4 = 0; // const/4 v4, 0x0
/* .line 92 */
/* .local v4, "carWithResolveInfo":Landroid/content/pm/ResolveInfo; */
int v5 = 0; // const/4 v5, 0x0
/* .line 93 */
/* .local v5, "carLifeResolveInfo":Landroid/content/pm/ResolveInfo; */
int v6 = 0; // const/4 v6, 0x0
/* .line 95 */
/* .local v6, "confirmedResolveInfo":Landroid/content/pm/ResolveInfo; */
final String v7 = "activity"; // const-string v7, "activity"
(( android.content.Context ) p1 ).getSystemService ( v7 ); // invoke-virtual {p1, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v7, Landroid/app/ActivityManager; */
/* .line 97 */
/* .local v7, "am":Landroid/app/ActivityManager; */
/* invoke-virtual/range {p3 ..p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator; */
v9 = } // :goto_0
final String v10 = "com.baidu.carlife"; // const-string v10, "com.baidu.carlife"
if ( v9 != null) { // if-eqz v9, :cond_3
/* check-cast v9, Landroid/content/pm/ResolveInfo; */
/* .line 98 */
/* .local v9, "resolveInfo":Landroid/content/pm/ResolveInfo; */
v11 = this.activityInfo;
if ( v11 != null) { // if-eqz v11, :cond_1
/* .line 99 */
v11 = this.activityInfo;
v11 = this.packageName;
final String v12 = "com.miui.carlink"; // const-string v12, "com.miui.carlink"
v11 = (( java.lang.String ) v11 ).equals ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v11 != null) { // if-eqz v11, :cond_0
/* .line 100 */
/* move-object v4, v9 */
/* .line 101 */
} // :cond_0
v11 = this.activityInfo;
v11 = this.packageName;
v10 = (( java.lang.String ) v11 ).equals ( v10 ); // invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v10 != null) { // if-eqz v10, :cond_2
/* .line 102 */
/* move-object v5, v9 */
/* .line 105 */
} // :cond_1
final String v10 = "MiuiUsbServiceImpl"; // const-string v10, "MiuiUsbServiceImpl"
final String v11 = "resolveInfo has no activityInfo"; // const-string v11, "resolveInfo has no activityInfo"
android.util.Slog .w ( v10,v11 );
/* .line 107 */
} // .end local v9 # "resolveInfo":Landroid/content/pm/ResolveInfo;
} // :cond_2
} // :goto_1
/* .line 109 */
} // :cond_3
if ( v4 != null) { // if-eqz v4, :cond_6
if ( v5 != null) { // if-eqz v5, :cond_6
/* .line 111 */
/* move-object v6, v4 */
/* .line 112 */
(( android.app.ActivityManager ) v7 ).getRunningAppProcesses ( ); // invoke-virtual {v7}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;
/* .line 113 */
/* .local v8, "processes":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;" */
v11 = } // :goto_2
if ( v11 != null) { // if-eqz v11, :cond_5
/* check-cast v11, Landroid/app/ActivityManager$RunningAppProcessInfo; */
/* .line 114 */
/* .local v11, "process":Landroid/app/ActivityManager$RunningAppProcessInfo; */
v12 = this.processName;
v12 = (( java.lang.String ) v12 ).equals ( v10 ); // invoke-virtual {v12, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v12 != null) { // if-eqz v12, :cond_4
/* .line 115 */
/* iget v9, v11, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I */
/* const/16 v10, 0x64 */
/* if-ne v9, v10, :cond_5 */
/* .line 116 */
/* move-object v6, v5 */
/* .line 120 */
} // .end local v11 # "process":Landroid/app/ActivityManager$RunningAppProcessInfo;
} // :cond_4
/* .line 122 */
} // :cond_5
} // :goto_3
/* move-object/from16 v9, p6 */
v10 = this.mUsbService;
v11 = this.activityInfo;
v11 = this.applicationInfo;
/* iget v11, v11, Landroid/content/pm/ApplicationInfo;->uid:I */
/* .line 124 */
v11 = android.os.UserHandle .getUserId ( v11 );
/* .line 123 */
(( com.android.server.usb.UsbService ) v10 ).getPermissionsForUser ( v11 ); // invoke-virtual {v10, v11}, Lcom/android/server/usb/UsbService;->getPermissionsForUser(I)Lcom/android/server/usb/UsbUserPermissionManager;
/* .line 125 */
/* .local v10, "userPermissionsManager":Lcom/android/server/usb/UsbUserPermissionManager; */
v11 = this.activityInfo;
v11 = this.applicationInfo;
/* iget v11, v11, Landroid/content/pm/ApplicationInfo;->uid:I */
(( com.android.server.usb.UsbUserPermissionManager ) v10 ).grantAccessoryPermission ( v2, v11 ); // invoke-virtual {v10, v2, v11}, Lcom/android/server/usb/UsbUserPermissionManager;->grantAccessoryPermission(Landroid/hardware/usb/UsbAccessory;I)V
/* .line 127 */
/* new-instance v11, Landroid/content/ComponentName; */
v12 = this.activityInfo;
v12 = this.packageName;
v13 = this.activityInfo;
v13 = this.name;
/* invoke-direct {v11, v12, v13}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( android.content.Intent ) v1 ).setComponent ( v11 ); // invoke-virtual {v1, v11}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 129 */
v11 = this.activityInfo;
v11 = this.applicationInfo;
/* iget v11, v11, Landroid/content/pm/ApplicationInfo;->uid:I */
android.os.UserHandle .getUserHandleForUid ( v11 );
/* .line 131 */
/* .local v11, "user":Landroid/os/UserHandle; */
(( android.content.Context ) p1 ).startActivityAsUser ( v1, v11 ); // invoke-virtual {p1, v1, v11}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
/* .line 133 */
int v3 = 1; // const/4 v3, 0x1
/* .line 109 */
} // .end local v8 # "processes":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
} // .end local v10 # "userPermissionsManager":Lcom/android/server/usb/UsbUserPermissionManager;
} // .end local v11 # "user":Landroid/os/UserHandle;
} // :cond_6
/* move-object/from16 v9, p6 */
/* .line 89 */
} // .end local v4 # "carWithResolveInfo":Landroid/content/pm/ResolveInfo;
} // .end local v5 # "carLifeResolveInfo":Landroid/content/pm/ResolveInfo;
} // .end local v6 # "confirmedResolveInfo":Landroid/content/pm/ResolveInfo;
} // .end local v7 # "am":Landroid/app/ActivityManager;
} // :cond_7
/* move-object/from16 v9, p6 */
/* .line 137 */
} // :goto_4
} // .end method
