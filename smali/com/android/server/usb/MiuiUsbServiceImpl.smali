.class public Lcom/android/server/usb/MiuiUsbServiceImpl;
.super Lcom/android/server/usb/MiuiUsbServiceStub;
.source "MiuiUsbServiceImpl.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.usb.MiuiUsbServiceStub$$"
.end annotation


# static fields
.field private static final CARLIFE_PACKAGE_NAME:Ljava/lang/String; = "com.baidu.carlife"

.field private static final CARWITH_PACKAGE_NAME:Ljava/lang/String; = "com.miui.carlink"

.field private static final FLAG_NON_ANONYMOUS:I = 0x2

.field public static final SPECIAL_PRODUCT_ID:I = 0x3ffc

.field public static final SPECIAL_VENDOR_ID:I = 0x3206


# instance fields
.field private final TAG:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 25
    invoke-direct {p0}, Lcom/android/server/usb/MiuiUsbServiceStub;-><init>()V

    .line 26
    const-string v0, "MiuiUsbServiceImpl"

    iput-object v0, p0, Lcom/android/server/usb/MiuiUsbServiceImpl;->TAG:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public collectUsbHostConnectedInfo(Landroid/content/Context;Lcom/android/server/usb/descriptors/UsbDescriptorParser;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "parser"    # Lcom/android/server/usb/descriptors/UsbDescriptorParser;

    .line 37
    const-string v0, "MiuiUsbServiceImpl"

    invoke-static {}, Lmiui/util/IMiCharge;->getInstance()Lmiui/util/IMiCharge;

    move-result-object v1

    .line 39
    .local v1, "miCharge":Lmiui/util/IMiCharge;
    sget-boolean v2, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v2, :cond_7

    .line 40
    invoke-virtual {p2}, Lcom/android/server/usb/descriptors/UsbDescriptorParser;->hasAudioInterface()Z

    move-result v2

    .line 41
    .local v2, "hasAudio":Z
    invoke-virtual {p2}, Lcom/android/server/usb/descriptors/UsbDescriptorParser;->hasHIDInterface()Z

    move-result v3

    .line 42
    .local v3, "hasHid":Z
    invoke-virtual {p2}, Lcom/android/server/usb/descriptors/UsbDescriptorParser;->hasStorageInterface()Z

    move-result v4

    .line 43
    .local v4, "hasStorage":Z
    const-string v5, ""

    .line 45
    .local v5, "value":Ljava/lang/String;
    invoke-virtual {v1}, Lmiui/util/IMiCharge;->isDPConnected()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 46
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "DP"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 48
    :cond_0
    if-eqz v2, :cond_1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "Audio"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 49
    :cond_1
    if-eqz v3, :cond_2

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "HID"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 50
    :cond_2
    if-eqz v4, :cond_3

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "Storage"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 51
    :cond_3
    if-eqz v5, :cond_4

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    if-nez v6, :cond_5

    .line 52
    :cond_4
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "Other"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 56
    :cond_5
    :goto_0
    new-instance v6, Landroid/content/Intent;

    const-string v7, "onetrack.action.TRACK_EVENT"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 57
    .local v6, "intent":Landroid/content/Intent;
    const-string v7, "com.miui.analytics"

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 58
    const-string v7, "APP_ID"

    const-string v8, "31000000092"

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 59
    const-string v7, "EVENT_NAME"

    const-string/jumbo v8, "usb_host"

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 60
    const-string v7, "PACKAGE"

    const-string v8, "Android"

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 61
    const-string v7, "device_connected"

    invoke-virtual {v6, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 62
    invoke-virtual {v1}, Lmiui/util/IMiCharge;->isUSB32()Z

    move-result v7

    const-string v8, "USB32"

    invoke-virtual {v6, v8, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 63
    sget-boolean v7, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v7, :cond_6

    .line 64
    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 67
    :cond_6
    :try_start_0
    sget-object v7, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {p1, v6, v7}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 70
    goto :goto_1

    .line 68
    :catch_0
    move-exception v7

    .line 69
    .local v7, "e":Ljava/lang/IllegalStateException;
    const-string v8, "Start one-Track service failed"

    invoke-static {v0, v8, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 71
    .end local v7    # "e":Ljava/lang/IllegalStateException;
    :goto_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "connected device : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    .end local v2    # "hasAudio":Z
    .end local v3    # "hasHid":Z
    .end local v4    # "hasStorage":Z
    .end local v5    # "value":Ljava/lang/String;
    .end local v6    # "intent":Landroid/content/Intent;
    :cond_7
    return-void
.end method

.method public isSpecialKeyBoard(Landroid/hardware/usb/UsbDevice;)Z
    .locals 2
    .param p1, "device"    # Landroid/hardware/usb/UsbDevice;

    .line 77
    const/16 v0, 0x3206

    invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getVendorId()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 78
    invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getProductId()I

    move-result v0

    const/16 v1, 0x3ffc

    if-ne v1, v0, :cond_0

    .line 79
    const/4 v0, 0x1

    return v0

    .line 81
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public preSelectUsbHandlerForCarWith(Landroid/content/Context;Landroid/content/Intent;Ljava/util/ArrayList;Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbAccessory;Lcom/android/server/usb/UsbSettingsManager;)Z
    .locals 14
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p4, "device"    # Landroid/hardware/usb/UsbDevice;
    .param p5, "accessory"    # Landroid/hardware/usb/UsbAccessory;
    .param p6, "settingsManager"    # Lcom/android/server/usb/UsbSettingsManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/content/Intent;",
            "Ljava/util/ArrayList<",
            "Landroid/content/pm/ResolveInfo;",
            ">;",
            "Landroid/hardware/usb/UsbDevice;",
            "Landroid/hardware/usb/UsbAccessory;",
            "Lcom/android/server/usb/UsbSettingsManager;",
            ")Z"
        }
    .end annotation

    .line 88
    .local p3, "matches":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/pm/ResolveInfo;>;"
    move-object v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p5

    const/4 v3, 0x0

    .line 89
    .local v3, "isConfirmed":Z
    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_7

    if-nez p4, :cond_7

    if-eqz v2, :cond_7

    .line 91
    const/4 v4, 0x0

    .line 92
    .local v4, "carWithResolveInfo":Landroid/content/pm/ResolveInfo;
    const/4 v5, 0x0

    .line 93
    .local v5, "carLifeResolveInfo":Landroid/content/pm/ResolveInfo;
    const/4 v6, 0x0

    .line 95
    .local v6, "confirmedResolveInfo":Landroid/content/pm/ResolveInfo;
    const-string v7, "activity"

    invoke-virtual {p1, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/app/ActivityManager;

    .line 97
    .local v7, "am":Landroid/app/ActivityManager;
    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    const-string v10, "com.baidu.carlife"

    if-eqz v9, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/pm/ResolveInfo;

    .line 98
    .local v9, "resolveInfo":Landroid/content/pm/ResolveInfo;
    iget-object v11, v9, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v11, :cond_1

    .line 99
    iget-object v11, v9, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v11, v11, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    const-string v12, "com.miui.carlink"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 100
    move-object v4, v9

    goto :goto_1

    .line 101
    :cond_0
    iget-object v11, v9, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v11, v11, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 102
    move-object v5, v9

    goto :goto_1

    .line 105
    :cond_1
    const-string v10, "MiuiUsbServiceImpl"

    const-string v11, "resolveInfo has no activityInfo"

    invoke-static {v10, v11}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    .end local v9    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    :cond_2
    :goto_1
    goto :goto_0

    .line 109
    :cond_3
    if-eqz v4, :cond_6

    if-eqz v5, :cond_6

    .line 111
    move-object v6, v4

    .line 112
    invoke-virtual {v7}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v8

    .line 113
    .local v8, "processes":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_5

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 114
    .local v11, "process":Landroid/app/ActivityManager$RunningAppProcessInfo;
    iget-object v12, v11, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v12, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 115
    iget v9, v11, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v10, 0x64

    if-ne v9, v10, :cond_5

    .line 116
    move-object v6, v5

    goto :goto_3

    .line 120
    .end local v11    # "process":Landroid/app/ActivityManager$RunningAppProcessInfo;
    :cond_4
    goto :goto_2

    .line 122
    :cond_5
    :goto_3
    move-object/from16 v9, p6

    iget-object v10, v9, Lcom/android/server/usb/UsbSettingsManager;->mUsbService:Lcom/android/server/usb/UsbService;

    iget-object v11, v6, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v11, v11, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v11, v11, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 124
    invoke-static {v11}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v11

    .line 123
    invoke-virtual {v10, v11}, Lcom/android/server/usb/UsbService;->getPermissionsForUser(I)Lcom/android/server/usb/UsbUserPermissionManager;

    move-result-object v10

    .line 125
    .local v10, "userPermissionsManager":Lcom/android/server/usb/UsbUserPermissionManager;
    iget-object v11, v6, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v11, v11, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v11, v11, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {v10, v2, v11}, Lcom/android/server/usb/UsbUserPermissionManager;->grantAccessoryPermission(Landroid/hardware/usb/UsbAccessory;I)V

    .line 127
    new-instance v11, Landroid/content/ComponentName;

    iget-object v12, v6, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v12, v12, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v13, v6, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v13, v13, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v11, v12, v13}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v11}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 129
    iget-object v11, v6, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v11, v11, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v11, v11, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v11}, Landroid/os/UserHandle;->getUserHandleForUid(I)Landroid/os/UserHandle;

    move-result-object v11

    .line 131
    .local v11, "user":Landroid/os/UserHandle;
    invoke-virtual {p1, v1, v11}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 133
    const/4 v3, 0x1

    goto :goto_4

    .line 109
    .end local v8    # "processes":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    .end local v10    # "userPermissionsManager":Lcom/android/server/usb/UsbUserPermissionManager;
    .end local v11    # "user":Landroid/os/UserHandle;
    :cond_6
    move-object/from16 v9, p6

    goto :goto_4

    .line 89
    .end local v4    # "carWithResolveInfo":Landroid/content/pm/ResolveInfo;
    .end local v5    # "carLifeResolveInfo":Landroid/content/pm/ResolveInfo;
    .end local v6    # "confirmedResolveInfo":Landroid/content/pm/ResolveInfo;
    .end local v7    # "am":Landroid/app/ActivityManager;
    :cond_7
    move-object/from16 v9, p6

    .line 137
    :goto_4
    return v3
.end method
