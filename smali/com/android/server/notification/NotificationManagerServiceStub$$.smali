.class public final Lcom/android/server/notification/NotificationManagerServiceStub$$;
.super Ljava/lang/Object;
.source "NotificationManagerServiceStub$$.java"

# interfaces
.implements Lcom/miui/base/MiuiStubRegistry$ImplProviderManifest;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final collectImplProviders(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/miui/base/MiuiStubRegistry$ImplProvider<",
            "*>;>;)V"
        }
    .end annotation

    .line 18
    .local p1, "outProviders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/miui/base/MiuiStubRegistry$ImplProvider<*>;>;"
    new-instance v0, Lcom/android/server/notification/NotificationManagerServiceImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/notification/NotificationManagerServiceImpl$Provider;-><init>()V

    const-string v1, "com.android.server.notification.NotificationManagerServiceStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 19
    new-instance v0, Lcom/android/server/notification/NotificationManagerServiceImpl$NMSVersionImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/notification/NotificationManagerServiceImpl$NMSVersionImpl$Provider;-><init>()V

    const-string v1, "com.android.server.notification.NotificationManagerServiceStub$NMSVersionStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    new-instance v0, Lcom/android/server/notification/ZenModeStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/notification/ZenModeStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.notification.ZenModeStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    new-instance v0, Lcom/android/server/notification/NotificationManagerServiceImpl$NRStatusImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/notification/NotificationManagerServiceImpl$NRStatusImpl$Provider;-><init>()V

    const-string v1, "com.android.server.notification.NotificationManagerServiceStub$NRStatusStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    new-instance v0, Lcom/android/server/notification/BarrageListenerServiceImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/notification/BarrageListenerServiceImpl$Provider;-><init>()V

    const-string v1, "com.android.server.notification.BarrageListenerServiceStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    return-void
.end method
