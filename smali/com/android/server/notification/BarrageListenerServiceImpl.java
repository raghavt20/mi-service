public class com.android.server.notification.BarrageListenerServiceImpl implements com.android.server.notification.BarrageListenerServiceStub {
	 /* .source "BarrageListenerServiceImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/notification/BarrageListenerServiceImpl$BHandler;, */
	 /* Lcom/android/server/notification/BarrageListenerServiceImpl$SettingsObserver;, */
	 /* Lcom/android/server/notification/BarrageListenerServiceImpl$UserSwitchReceiver; */
	 /* } */
} // .end annotation
/* # instance fields */
private final java.lang.String COMPONENT_BARRAGE;
private final Integer DELAY_FORCE_STOP;
private final java.lang.String GB_BOOSTING;
private final java.lang.String GB_BULLET_CHAT;
private final java.lang.String MI_BARRAGE;
private final java.lang.String TAG;
private android.app.ActivityManager mAm;
private android.content.Context mContext;
private Integer mCurrentUserId;
private android.os.Handler mHandler;
private Integer mInGame;
private com.android.server.notification.ManagedServices mListener;
private android.content.pm.PackageManager mPm;
private com.android.server.notification.BarrageListenerServiceImpl$SettingsObserver mSettingsObserver;
private Integer mTurnOn;
private com.android.server.notification.BarrageListenerServiceImpl$UserSwitchReceiver mUserSwitchReceiver;
/* # direct methods */
static android.app.ActivityManager -$$Nest$fgetmAm ( com.android.server.notification.BarrageListenerServiceImpl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mAm;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.android.server.notification.BarrageListenerServiceImpl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mContext;
} // .end method
static Integer -$$Nest$fgetmCurrentUserId ( com.android.server.notification.BarrageListenerServiceImpl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget p0, p0, Lcom/android/server/notification/BarrageListenerServiceImpl;->mCurrentUserId:I */
} // .end method
static android.os.Handler -$$Nest$fgetmHandler ( com.android.server.notification.BarrageListenerServiceImpl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mHandler;
} // .end method
static Integer -$$Nest$fgetmInGame ( com.android.server.notification.BarrageListenerServiceImpl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget p0, p0, Lcom/android/server/notification/BarrageListenerServiceImpl;->mInGame:I */
} // .end method
static com.android.server.notification.ManagedServices -$$Nest$fgetmListener ( com.android.server.notification.BarrageListenerServiceImpl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mListener;
} // .end method
static com.android.server.notification.BarrageListenerServiceImpl$SettingsObserver -$$Nest$fgetmSettingsObserver ( com.android.server.notification.BarrageListenerServiceImpl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mSettingsObserver;
} // .end method
static Integer -$$Nest$fgetmTurnOn ( com.android.server.notification.BarrageListenerServiceImpl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget p0, p0, Lcom/android/server/notification/BarrageListenerServiceImpl;->mTurnOn:I */
} // .end method
static void -$$Nest$fputmCurrentUserId ( com.android.server.notification.BarrageListenerServiceImpl p0, Integer p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput p1, p0, Lcom/android/server/notification/BarrageListenerServiceImpl;->mCurrentUserId:I */
	 return;
} // .end method
static void -$$Nest$fputmInGame ( com.android.server.notification.BarrageListenerServiceImpl p0, Integer p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput p1, p0, Lcom/android/server/notification/BarrageListenerServiceImpl;->mInGame:I */
	 return;
} // .end method
static void -$$Nest$fputmTurnOn ( com.android.server.notification.BarrageListenerServiceImpl p0, Integer p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput p1, p0, Lcom/android/server/notification/BarrageListenerServiceImpl;->mTurnOn:I */
	 return;
} // .end method
public com.android.server.notification.BarrageListenerServiceImpl ( ) {
	 /* .locals 1 */
	 /* .line 32 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 34 */
	 final String v0 = "BarrageListenerService"; // const-string v0, "BarrageListenerService"
	 this.TAG = v0;
	 /* .line 35 */
	 final String v0 = "gb_bullet_chat"; // const-string v0, "gb_bullet_chat"
	 this.GB_BULLET_CHAT = v0;
	 /* .line 36 */
	 final String v0 = "gb_boosting"; // const-string v0, "gb_boosting"
	 this.GB_BOOSTING = v0;
	 /* .line 37 */
	 final String v0 = "com.xiaomi.barrage"; // const-string v0, "com.xiaomi.barrage"
	 this.MI_BARRAGE = v0;
	 /* .line 38 */
	 final String v0 = "com.xiaomi.barrage/com.xiaomi.barrage.service.NotificationMonitorService"; // const-string v0, "com.xiaomi.barrage/com.xiaomi.barrage.service.NotificationMonitorService"
	 this.COMPONENT_BARRAGE = v0;
	 /* .line 40 */
	 int v0 = 1; // const/4 v0, 0x1
	 /* iput v0, p0, Lcom/android/server/notification/BarrageListenerServiceImpl;->DELAY_FORCE_STOP:I */
	 return;
} // .end method
/* # virtual methods */
public void init ( android.content.Context p0, com.android.server.notification.ManagedServices p1 ) {
	 /* .locals 9 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .param p2, "listeners" # Lcom/android/server/notification/ManagedServices; */
	 /* .line 138 */
	 this.mContext = p1;
	 /* .line 139 */
	 this.mListener = p2;
	 /* .line 140 */
	 /* new-instance v0, Lcom/android/server/notification/BarrageListenerServiceImpl$BHandler; */
	 int v1 = 0; // const/4 v1, 0x0
	 /* invoke-direct {v0, p0, v1}, Lcom/android/server/notification/BarrageListenerServiceImpl$BHandler;-><init>(Lcom/android/server/notification/BarrageListenerServiceImpl;Lcom/android/server/notification/BarrageListenerServiceImpl$BHandler-IA;)V */
	 this.mHandler = v0;
	 /* .line 141 */
	 v0 = this.mContext;
	 (( android.content.Context ) v0 ).getPackageManager ( ); // invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
	 this.mPm = v0;
	 /* .line 142 */
	 v0 = this.mContext;
	 final String v2 = "activity"; // const-string v2, "activity"
	 (( android.content.Context ) v0 ).getSystemService ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
	 /* check-cast v0, Landroid/app/ActivityManager; */
	 this.mAm = v0;
	 /* .line 143 */
	 /* new-instance v0, Lcom/android/server/notification/BarrageListenerServiceImpl$SettingsObserver; */
	 v2 = this.mHandler;
	 /* invoke-direct {v0, p0, v2}, Lcom/android/server/notification/BarrageListenerServiceImpl$SettingsObserver;-><init>(Lcom/android/server/notification/BarrageListenerServiceImpl;Landroid/os/Handler;)V */
	 this.mSettingsObserver = v0;
	 /* .line 144 */
	 v0 = this.mContext;
	 (( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
	 /* .line 145 */
	 /* .local v0, "resolver":Landroid/content/ContentResolver; */
	 final String v2 = "gb_bullet_chat"; // const-string v2, "gb_bullet_chat"
	 android.provider.Settings$Secure .getUriFor ( v2 );
	 v3 = this.mSettingsObserver;
	 int v4 = 0; // const/4 v4, 0x0
	 int v5 = -1; // const/4 v5, -0x1
	 (( android.content.ContentResolver ) v0 ).registerContentObserver ( v2, v4, v3, v5 ); // invoke-virtual {v0, v2, v4, v3, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
	 /* .line 147 */
	 final String v2 = "gb_boosting"; // const-string v2, "gb_boosting"
	 android.provider.Settings$Secure .getUriFor ( v2 );
	 v3 = this.mSettingsObserver;
	 (( android.content.ContentResolver ) v0 ).registerContentObserver ( v2, v4, v3, v5 ); // invoke-virtual {v0, v2, v4, v3, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
	 /* .line 149 */
	 /* iput v4, p0, Lcom/android/server/notification/BarrageListenerServiceImpl;->mCurrentUserId:I */
	 /* .line 150 */
	 /* new-instance v2, Landroid/content/IntentFilter; */
	 /* invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V */
	 /* .line 151 */
	 /* .local v2, "filter":Landroid/content/IntentFilter; */
	 final String v3 = "android.intent.action.USER_SWITCHED"; // const-string v3, "android.intent.action.USER_SWITCHED"
	 (( android.content.IntentFilter ) v2 ).addAction ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
	 /* .line 152 */
	 /* new-instance v4, Lcom/android/server/notification/BarrageListenerServiceImpl$UserSwitchReceiver; */
	 /* invoke-direct {v4, p0, v1}, Lcom/android/server/notification/BarrageListenerServiceImpl$UserSwitchReceiver;-><init>(Lcom/android/server/notification/BarrageListenerServiceImpl;Lcom/android/server/notification/BarrageListenerServiceImpl$UserSwitchReceiver-IA;)V */
	 this.mUserSwitchReceiver = v4;
	 /* .line 153 */
	 v3 = this.mContext;
	 v5 = android.os.UserHandle.ALL;
	 int v7 = 0; // const/4 v7, 0x0
	 int v8 = 0; // const/4 v8, 0x0
	 /* move-object v6, v2 */
	 /* invoke-virtual/range {v3 ..v8}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent; */
	 /* .line 155 */
	 return;
} // .end method
public Boolean isAllowStartBarrage ( java.lang.String p0 ) {
	 /* .locals 2 */
	 /* .param p1, "packageName" # Ljava/lang/String; */
	 /* .line 159 */
	 final String v0 = "com.xiaomi.barrage"; // const-string v0, "com.xiaomi.barrage"
	 v0 = 	 (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 int v1 = 1; // const/4 v1, 0x1
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 /* .line 160 */
		 /* iget v0, p0, Lcom/android/server/notification/BarrageListenerServiceImpl;->mTurnOn:I */
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* iget v0, p0, Lcom/android/server/notification/BarrageListenerServiceImpl;->mInGame:I */
			 if ( v0 != null) { // if-eqz v0, :cond_0
				 /* .line 161 */
				 /* .line 163 */
			 } // :cond_0
			 int v0 = 0; // const/4 v0, 0x0
			 /* .line 165 */
		 } // :cond_1
	 } // .end method
