.class Lcom/android/server/notification/BarrageListenerServiceImpl$SettingsObserver;
.super Landroid/database/ContentObserver;
.source "BarrageListenerServiceImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/notification/BarrageListenerServiceImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SettingsObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/notification/BarrageListenerServiceImpl;


# direct methods
.method public constructor <init>(Lcom/android/server/notification/BarrageListenerServiceImpl;Landroid/os/Handler;)V
    .locals 0
    .param p2, "handler"    # Landroid/os/Handler;

    .line 72
    iput-object p1, p0, Lcom/android/server/notification/BarrageListenerServiceImpl$SettingsObserver;->this$0:Lcom/android/server/notification/BarrageListenerServiceImpl;

    .line 73
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 74
    return-void
.end method

.method private update()V
    .locals 4

    .line 93
    iget-object v0, p0, Lcom/android/server/notification/BarrageListenerServiceImpl$SettingsObserver;->this$0:Lcom/android/server/notification/BarrageListenerServiceImpl;

    invoke-static {v0}, Lcom/android/server/notification/BarrageListenerServiceImpl;->-$$Nest$fgetmTurnOn(Lcom/android/server/notification/BarrageListenerServiceImpl;)I

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/notification/BarrageListenerServiceImpl$SettingsObserver;->this$0:Lcom/android/server/notification/BarrageListenerServiceImpl;

    invoke-static {v0}, Lcom/android/server/notification/BarrageListenerServiceImpl;->-$$Nest$fgetmInGame(Lcom/android/server/notification/BarrageListenerServiceImpl;)I

    move-result v0

    if-eqz v0, :cond_1

    .line 94
    iget-object v0, p0, Lcom/android/server/notification/BarrageListenerServiceImpl$SettingsObserver;->this$0:Lcom/android/server/notification/BarrageListenerServiceImpl;

    invoke-static {v0}, Lcom/android/server/notification/BarrageListenerServiceImpl;->-$$Nest$fgetmHandler(Lcom/android/server/notification/BarrageListenerServiceImpl;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/android/server/notification/BarrageListenerServiceImpl$SettingsObserver;->this$0:Lcom/android/server/notification/BarrageListenerServiceImpl;

    invoke-static {v0}, Lcom/android/server/notification/BarrageListenerServiceImpl;->-$$Nest$fgetmHandler(Lcom/android/server/notification/BarrageListenerServiceImpl;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 97
    :cond_0
    const-string v0, "com.xiaomi.barrage/com.xiaomi.barrage.service.NotificationMonitorService"

    invoke-static {v0}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    .line 98
    .local v0, "cn":Landroid/content/ComponentName;
    iget-object v1, p0, Lcom/android/server/notification/BarrageListenerServiceImpl$SettingsObserver;->this$0:Lcom/android/server/notification/BarrageListenerServiceImpl;

    invoke-static {v1}, Lcom/android/server/notification/BarrageListenerServiceImpl;->-$$Nest$fgetmListener(Lcom/android/server/notification/BarrageListenerServiceImpl;)Lcom/android/server/notification/ManagedServices;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/notification/BarrageListenerServiceImpl$SettingsObserver;->this$0:Lcom/android/server/notification/BarrageListenerServiceImpl;

    invoke-static {v2}, Lcom/android/server/notification/BarrageListenerServiceImpl;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/notification/BarrageListenerServiceImpl;)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/android/server/notification/ManagedServices;->registerService(Landroid/content/ComponentName;I)V

    .line 99
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "start the barrage, mTurnOn:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/notification/BarrageListenerServiceImpl$SettingsObserver;->this$0:Lcom/android/server/notification/BarrageListenerServiceImpl;

    invoke-static {v2}, Lcom/android/server/notification/BarrageListenerServiceImpl;->-$$Nest$fgetmTurnOn(Lcom/android/server/notification/BarrageListenerServiceImpl;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mInGame:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/notification/BarrageListenerServiceImpl$SettingsObserver;->this$0:Lcom/android/server/notification/BarrageListenerServiceImpl;

    invoke-static {v2}, Lcom/android/server/notification/BarrageListenerServiceImpl;->-$$Nest$fgetmInGame(Lcom/android/server/notification/BarrageListenerServiceImpl;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "BarrageListenerService"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    .end local v0    # "cn":Landroid/content/ComponentName;
    goto :goto_0

    .line 101
    :cond_1
    iget-object v0, p0, Lcom/android/server/notification/BarrageListenerServiceImpl$SettingsObserver;->this$0:Lcom/android/server/notification/BarrageListenerServiceImpl;

    invoke-static {v0}, Lcom/android/server/notification/BarrageListenerServiceImpl;->-$$Nest$fgetmHandler(Lcom/android/server/notification/BarrageListenerServiceImpl;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 102
    return-void

    .line 104
    :cond_2
    iget-object v0, p0, Lcom/android/server/notification/BarrageListenerServiceImpl$SettingsObserver;->this$0:Lcom/android/server/notification/BarrageListenerServiceImpl;

    invoke-static {v0}, Lcom/android/server/notification/BarrageListenerServiceImpl;->-$$Nest$fgetmHandler(Lcom/android/server/notification/BarrageListenerServiceImpl;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 105
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/notification/BarrageListenerServiceImpl$SettingsObserver;->this$0:Lcom/android/server/notification/BarrageListenerServiceImpl;

    invoke-static {v1}, Lcom/android/server/notification/BarrageListenerServiceImpl;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/notification/BarrageListenerServiceImpl;)I

    move-result v1

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 106
    iget-object v1, p0, Lcom/android/server/notification/BarrageListenerServiceImpl$SettingsObserver;->this$0:Lcom/android/server/notification/BarrageListenerServiceImpl;

    invoke-static {v1}, Lcom/android/server/notification/BarrageListenerServiceImpl;->-$$Nest$fgetmHandler(Lcom/android/server/notification/BarrageListenerServiceImpl;)Landroid/os/Handler;

    move-result-object v1

    const-wide/16 v2, 0x2710

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 108
    .end local v0    # "msg":Landroid/os/Message;
    :goto_0
    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 5
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 78
    invoke-super {p0, p1, p2}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;)V

    .line 79
    const-string v0, "gb_bullet_chat"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 80
    iget-object v1, p0, Lcom/android/server/notification/BarrageListenerServiceImpl$SettingsObserver;->this$0:Lcom/android/server/notification/BarrageListenerServiceImpl;

    invoke-static {v1}, Lcom/android/server/notification/BarrageListenerServiceImpl;->-$$Nest$fgetmContext(Lcom/android/server/notification/BarrageListenerServiceImpl;)Landroid/content/Context;

    move-result-object v3

    .line 81
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/notification/BarrageListenerServiceImpl$SettingsObserver;->this$0:Lcom/android/server/notification/BarrageListenerServiceImpl;

    invoke-static {v4}, Lcom/android/server/notification/BarrageListenerServiceImpl;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/notification/BarrageListenerServiceImpl;)I

    move-result v4

    invoke-static {v3, v0, v2, v4}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    invoke-static {v1, v0}, Lcom/android/server/notification/BarrageListenerServiceImpl;->-$$Nest$fputmTurnOn(Lcom/android/server/notification/BarrageListenerServiceImpl;I)V

    .line 84
    :cond_0
    const-string v0, "gb_boosting"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 85
    iget-object v1, p0, Lcom/android/server/notification/BarrageListenerServiceImpl$SettingsObserver;->this$0:Lcom/android/server/notification/BarrageListenerServiceImpl;

    invoke-static {v1}, Lcom/android/server/notification/BarrageListenerServiceImpl;->-$$Nest$fgetmContext(Lcom/android/server/notification/BarrageListenerServiceImpl;)Landroid/content/Context;

    move-result-object v3

    .line 86
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/notification/BarrageListenerServiceImpl$SettingsObserver;->this$0:Lcom/android/server/notification/BarrageListenerServiceImpl;

    invoke-static {v4}, Lcom/android/server/notification/BarrageListenerServiceImpl;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/notification/BarrageListenerServiceImpl;)I

    move-result v4

    invoke-static {v3, v0, v2, v4}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    invoke-static {v1, v0}, Lcom/android/server/notification/BarrageListenerServiceImpl;->-$$Nest$fputmInGame(Lcom/android/server/notification/BarrageListenerServiceImpl;I)V

    .line 89
    :cond_1
    invoke-direct {p0}, Lcom/android/server/notification/BarrageListenerServiceImpl$SettingsObserver;->update()V

    .line 90
    return-void
.end method

.method public onChangeAll()V
    .locals 2

    .line 111
    const-string v0, "gb_bullet_chat"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/android/server/notification/BarrageListenerServiceImpl$SettingsObserver;->onChange(ZLandroid/net/Uri;)V

    .line 112
    const-string v0, "gb_boosting"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/android/server/notification/BarrageListenerServiceImpl$SettingsObserver;->onChange(ZLandroid/net/Uri;)V

    .line 113
    return-void
.end method
