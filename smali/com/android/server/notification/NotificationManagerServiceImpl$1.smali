.class Lcom/android/server/notification/NotificationManagerServiceImpl$1;
.super Landroid/database/ContentObserver;
.source "NotificationManagerServiceImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/notification/NotificationManagerServiceImpl;->registerPrivacyInputMode(Landroid/os/Handler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/notification/NotificationManagerServiceImpl;


# direct methods
.method constructor <init>(Lcom/android/server/notification/NotificationManagerServiceImpl;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/notification/NotificationManagerServiceImpl;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 804
    iput-object p1, p0, Lcom/android/server/notification/NotificationManagerServiceImpl$1;->this$0:Lcom/android/server/notification/NotificationManagerServiceImpl;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 3
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 807
    invoke-super {p0, p1, p2}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;)V

    .line 808
    iget-object v0, p0, Lcom/android/server/notification/NotificationManagerServiceImpl$1;->this$0:Lcom/android/server/notification/NotificationManagerServiceImpl;

    invoke-static {v0}, Lcom/android/server/notification/NotificationManagerServiceImpl;->-$$Nest$fgetmContext(Lcom/android/server/notification/NotificationManagerServiceImpl;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "miui_privacy_input_pkg_name"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 809
    .local v0, "pkg":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/notification/NotificationManagerServiceImpl$1;->this$0:Lcom/android/server/notification/NotificationManagerServiceImpl;

    if-nez v0, :cond_0

    const-string v2, ""

    goto :goto_0

    :cond_0
    move-object v2, v0

    :goto_0
    invoke-static {v1, v2}, Lcom/android/server/notification/NotificationManagerServiceImpl;->-$$Nest$fputmPrivacyInputModePkgName(Lcom/android/server/notification/NotificationManagerServiceImpl;Ljava/lang/String;)V

    .line 810
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onChange:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "NotificationManagerServiceImpl"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 811
    return-void
.end method
