.class Lcom/android/server/notification/BarrageListenerServiceImpl$UserSwitchReceiver;
.super Landroid/content/BroadcastReceiver;
.source "BarrageListenerServiceImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/notification/BarrageListenerServiceImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UserSwitchReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/notification/BarrageListenerServiceImpl;


# direct methods
.method private constructor <init>(Lcom/android/server/notification/BarrageListenerServiceImpl;)V
    .locals 0

    .line 116
    iput-object p1, p0, Lcom/android/server/notification/BarrageListenerServiceImpl$UserSwitchReceiver;->this$0:Lcom/android/server/notification/BarrageListenerServiceImpl;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/notification/BarrageListenerServiceImpl;Lcom/android/server/notification/BarrageListenerServiceImpl$UserSwitchReceiver-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/notification/BarrageListenerServiceImpl$UserSwitchReceiver;-><init>(Lcom/android/server/notification/BarrageListenerServiceImpl;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 120
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 121
    .local v0, "action":Ljava/lang/String;
    const-string v1, "android.intent.action.USER_SWITCHED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 122
    iget-object v1, p0, Lcom/android/server/notification/BarrageListenerServiceImpl$UserSwitchReceiver;->this$0:Lcom/android/server/notification/BarrageListenerServiceImpl;

    invoke-static {v1}, Lcom/android/server/notification/BarrageListenerServiceImpl;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/notification/BarrageListenerServiceImpl;)I

    move-result v1

    .line 123
    .local v1, "oldUserId":I
    iget-object v2, p0, Lcom/android/server/notification/BarrageListenerServiceImpl$UserSwitchReceiver;->this$0:Lcom/android/server/notification/BarrageListenerServiceImpl;

    const-string v3, "android.intent.extra.user_handle"

    const/4 v4, -0x1

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    invoke-static {v2, v3}, Lcom/android/server/notification/BarrageListenerServiceImpl;->-$$Nest$fputmCurrentUserId(Lcom/android/server/notification/BarrageListenerServiceImpl;I)V

    .line 124
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "the user switches to u"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/notification/BarrageListenerServiceImpl$UserSwitchReceiver;->this$0:Lcom/android/server/notification/BarrageListenerServiceImpl;

    invoke-static {v3}, Lcom/android/server/notification/BarrageListenerServiceImpl;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/notification/BarrageListenerServiceImpl;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "BarrageListenerService"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    iget-object v2, p0, Lcom/android/server/notification/BarrageListenerServiceImpl$UserSwitchReceiver;->this$0:Lcom/android/server/notification/BarrageListenerServiceImpl;

    invoke-static {v2}, Lcom/android/server/notification/BarrageListenerServiceImpl;->-$$Nest$fgetmHandler(Lcom/android/server/notification/BarrageListenerServiceImpl;)Landroid/os/Handler;

    move-result-object v2

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 126
    iget-object v2, p0, Lcom/android/server/notification/BarrageListenerServiceImpl$UserSwitchReceiver;->this$0:Lcom/android/server/notification/BarrageListenerServiceImpl;

    invoke-static {v2}, Lcom/android/server/notification/BarrageListenerServiceImpl;->-$$Nest$fgetmHandler(Lcom/android/server/notification/BarrageListenerServiceImpl;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 127
    iget-object v2, p0, Lcom/android/server/notification/BarrageListenerServiceImpl$UserSwitchReceiver;->this$0:Lcom/android/server/notification/BarrageListenerServiceImpl;

    invoke-static {v2}, Lcom/android/server/notification/BarrageListenerServiceImpl;->-$$Nest$fgetmAm(Lcom/android/server/notification/BarrageListenerServiceImpl;)Landroid/app/ActivityManager;

    move-result-object v2

    const-string v4, "com.xiaomi.barrage"

    invoke-virtual {v2, v4, v1}, Landroid/app/ActivityManager;->forceStopPackageAsUser(Ljava/lang/String;I)V

    .line 128
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "stop the barrage when the user has been changed, mTurnOn:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/android/server/notification/BarrageListenerServiceImpl$UserSwitchReceiver;->this$0:Lcom/android/server/notification/BarrageListenerServiceImpl;

    invoke-static {v4}, Lcom/android/server/notification/BarrageListenerServiceImpl;->-$$Nest$fgetmTurnOn(Lcom/android/server/notification/BarrageListenerServiceImpl;)I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " mInGame:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/android/server/notification/BarrageListenerServiceImpl$UserSwitchReceiver;->this$0:Lcom/android/server/notification/BarrageListenerServiceImpl;

    invoke-static {v4}, Lcom/android/server/notification/BarrageListenerServiceImpl;->-$$Nest$fgetmInGame(Lcom/android/server/notification/BarrageListenerServiceImpl;)I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " oldUserId:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    :cond_0
    iget-object v2, p0, Lcom/android/server/notification/BarrageListenerServiceImpl$UserSwitchReceiver;->this$0:Lcom/android/server/notification/BarrageListenerServiceImpl;

    invoke-static {v2}, Lcom/android/server/notification/BarrageListenerServiceImpl;->-$$Nest$fgetmSettingsObserver(Lcom/android/server/notification/BarrageListenerServiceImpl;)Lcom/android/server/notification/BarrageListenerServiceImpl$SettingsObserver;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/server/notification/BarrageListenerServiceImpl$SettingsObserver;->onChangeAll()V

    .line 133
    .end local v1    # "oldUserId":I
    :cond_1
    return-void
.end method
