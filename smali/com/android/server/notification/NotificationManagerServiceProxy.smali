.class public Lcom/android/server/notification/NotificationManagerServiceProxy;
.super Ljava/lang/Object;
.source "NotificationManagerServiceProxy.java"


# static fields
.field public static allowNotificationListener:Lcom/xiaomi/reflect/RefMethod;
    .annotation runtime Lcom/xiaomi/reflect/annotation/MethodArguments;
        cls = {
            I,
            Landroid/content/ComponentName;
        }
    .end annotation
.end field

.field public static checkCallerIsSystem:Lcom/xiaomi/reflect/RefMethod;
    .annotation runtime Lcom/xiaomi/reflect/annotation/MethodArguments;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 12
    const-class v0, Lcom/android/server/notification/NotificationManagerServiceProxy;

    const-string v1, "com.android.server.notification.NotificationManagerService"

    invoke-static {v0, v1}, Lcom/xiaomi/reflect/RefClass;->attach(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Class;

    .line 13
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
