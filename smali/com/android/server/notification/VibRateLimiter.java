public class com.android.server.notification.VibRateLimiter {
	 /* .source "VibRateLimiter.java" */
	 /* # static fields */
	 private static final Long NOTIFICATION_VIBRATION_TIME_RATE;
	 /* # instance fields */
	 private Long mLastVibNotificationMillis;
	 /* # direct methods */
	 public com.android.server.notification.VibRateLimiter ( ) {
		 /* .locals 2 */
		 /* .line 7 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 8 */
		 /* const-wide/16 v0, 0x0 */
		 /* iput-wide v0, p0, Lcom/android/server/notification/VibRateLimiter;->mLastVibNotificationMillis:J */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Boolean shouldRateLimitVib ( Long p0 ) {
		 /* .locals 4 */
		 /* .param p1, "now" # J */
		 /* .line 12 */
		 /* iget-wide v0, p0, Lcom/android/server/notification/VibRateLimiter;->mLastVibNotificationMillis:J */
		 /* sub-long v0, p1, v0 */
		 /* .line 13 */
		 /* .local v0, "millisSinceLast":J */
		 /* const-wide/16 v2, 0x3a98 */
		 /* cmp-long v2, v0, v2 */
		 /* if-gez v2, :cond_0 */
		 /* .line 14 */
		 int v2 = 1; // const/4 v2, 0x1
		 /* .line 16 */
	 } // :cond_0
	 /* iput-wide p1, p0, Lcom/android/server/notification/VibRateLimiter;->mLastVibNotificationMillis:J */
	 /* .line 17 */
	 int v2 = 0; // const/4 v2, 0x0
} // .end method
