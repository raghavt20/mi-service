.class public Lcom/android/server/notification/VibRateLimiter;
.super Ljava/lang/Object;
.source "VibRateLimiter.java"


# static fields
.field private static final NOTIFICATION_VIBRATION_TIME_RATE:J = 0x3a98L


# instance fields
.field private mLastVibNotificationMillis:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/notification/VibRateLimiter;->mLastVibNotificationMillis:J

    return-void
.end method


# virtual methods
.method public shouldRateLimitVib(J)Z
    .locals 4
    .param p1, "now"    # J

    .line 12
    iget-wide v0, p0, Lcom/android/server/notification/VibRateLimiter;->mLastVibNotificationMillis:J

    sub-long v0, p1, v0

    .line 13
    .local v0, "millisSinceLast":J
    const-wide/16 v2, 0x3a98

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 14
    const/4 v2, 0x1

    return v2

    .line 16
    :cond_0
    iput-wide p1, p0, Lcom/android/server/notification/VibRateLimiter;->mLastVibNotificationMillis:J

    .line 17
    const/4 v2, 0x0

    return v2
.end method
