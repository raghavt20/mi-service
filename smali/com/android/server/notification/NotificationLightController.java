public class com.android.server.notification.NotificationLightController {
	 /* .source "NotificationLightController.java" */
	 /* # direct methods */
	 public com.android.server.notification.NotificationLightController ( ) {
		 /* .locals 0 */
		 /* .line 13 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static com.android.server.notification.NotificationRecord$Light customizeNotificationLight ( com.android.server.notification.NotificationManagerService p0, com.android.server.notification.NotificationRecord p1 ) {
		 /* .locals 5 */
		 /* .param p0, "service" # Lcom/android/server/notification/NotificationManagerService; */
		 /* .param p1, "ledNotification" # Lcom/android/server/notification/NotificationRecord; */
		 /* .line 16 */
		 (( com.android.server.notification.NotificationManagerService ) p0 ).getContext ( ); // invoke-virtual {p0}, Lcom/android/server/notification/NotificationManagerService;->getContext()Landroid/content/Context;
		 (( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
		 /* .line 17 */
		 /* const v1, 0x10601e5 */
		 v0 = 		 (( android.content.res.Resources ) v0 ).getColor ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I
		 /* .line 18 */
		 /* .local v0, "defaultColor":I */
		 (( com.android.server.notification.NotificationRecord ) p1 ).getNotification ( ); // invoke-virtual {p1}, Lcom/android/server/notification/NotificationRecord;->getNotification()Landroid/app/Notification;
		 com.android.server.notification.NotificationLightController .customizeNotificationLight ( p0,v1,v0 );
		 /* .line 19 */
		 (( com.android.server.notification.NotificationRecord ) p1 ).getNotification ( ); // invoke-virtual {p1}, Lcom/android/server/notification/NotificationRecord;->getNotification()Landroid/app/Notification;
		 /* iget v1, v1, Landroid/app/Notification;->ledARGB:I */
		 /* .line 20 */
		 /* .local v1, "ledARGB":I */
		 /* if-nez v1, :cond_0 */
		 /* .line 21 */
		 (( com.android.server.notification.NotificationRecord ) p1 ).getLight ( ); // invoke-virtual {p1}, Lcom/android/server/notification/NotificationRecord;->getLight()Lcom/android/server/notification/NotificationRecord$Light;
		 /* .line 23 */
	 } // :cond_0
	 (( com.android.server.notification.NotificationRecord ) p1 ).getNotification ( ); // invoke-virtual {p1}, Lcom/android/server/notification/NotificationRecord;->getNotification()Landroid/app/Notification;
	 /* iget v2, v2, Landroid/app/Notification;->ledOnMS:I */
	 /* .line 24 */
	 /* .local v2, "onMs":I */
	 (( com.android.server.notification.NotificationRecord ) p1 ).getNotification ( ); // invoke-virtual {p1}, Lcom/android/server/notification/NotificationRecord;->getNotification()Landroid/app/Notification;
	 /* iget v3, v3, Landroid/app/Notification;->ledOffMS:I */
	 /* .line 25 */
	 /* .local v3, "offMs":I */
	 /* new-instance v4, Lcom/android/server/notification/NotificationRecord$Light; */
	 /* invoke-direct {v4, v1, v2, v3}, Lcom/android/server/notification/NotificationRecord$Light;-><init>(III)V */
} // .end method
private static void customizeNotificationLight ( android.content.Context p0, android.app.Notification p1, java.lang.String p2, java.lang.String p3, Integer p4 ) {
	 /* .locals 4 */
	 /* .param p0, "context" # Landroid/content/Context; */
	 /* .param p1, "notification" # Landroid/app/Notification; */
	 /* .param p2, "colorKey" # Ljava/lang/String; */
	 /* .param p3, "freqKey" # Ljava/lang/String; */
	 /* .param p4, "defaultNotificationColor" # I */
	 /* .line 66 */
	 (( android.content.Context ) p0 ).getResources ( ); // invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
	 /* const v1, 0x110b0015 */
	 v0 = 	 (( android.content.res.Resources ) v0 ).getInteger ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I
	 /* .line 68 */
	 /* .local v0, "defaultFreq":I */
	 (( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
	 int v2 = -2; // const/4 v2, -0x2
	 v1 = 	 android.provider.Settings$System .getIntForUser ( v1,p2,p4,v2 );
	 /* iput v1, p1, Landroid/app/Notification;->ledARGB:I */
	 /* .line 70 */
	 (( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
	 v1 = 	 android.provider.Settings$System .getIntForUser ( v1,p3,v0,v2 );
	 /* .line 72 */
	 /* .local v1, "freq":I */
	 /* if-gez v1, :cond_0 */
	 /* move v2, v0 */
} // :cond_0
/* move v2, v1 */
} // :goto_0
com.android.server.notification.NotificationLightController .getLedPwmOffOn ( v2 );
/* .line 73 */
/* .local v2, "offOn":[I */
int v3 = 1; // const/4 v3, 0x1
/* aget v3, v2, v3 */
/* iput v3, p1, Landroid/app/Notification;->ledOnMS:I */
/* .line 74 */
int v3 = 0; // const/4 v3, 0x0
/* aget v3, v2, v3 */
/* iput v3, p1, Landroid/app/Notification;->ledOffMS:I */
/* .line 75 */
return;
} // .end method
public static void customizeNotificationLight ( com.android.server.notification.NotificationManagerService p0, android.app.Notification p1, Integer p2 ) {
/* .locals 8 */
/* .param p0, "service" # Lcom/android/server/notification/NotificationManagerService; */
/* .param p1, "notification" # Landroid/app/Notification; */
/* .param p2, "defaultNotificationColor" # I */
/* .line 31 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 32 */
/* .local v0, "identify":J */
int v2 = 0; // const/4 v2, 0x0
/* .line 35 */
/* .local v2, "customized":Z */
v3 = this.mLights;
(( java.util.ArrayList ) v3 ).iterator ( ); // invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_3
/* check-cast v4, Ljava/lang/String; */
/* .line 36 */
/* .local v4, "light":Ljava/lang/String; */
final String v5 = "com.android.phone"; // const-string v5, "com.android.phone"
v5 = (( java.lang.String ) v4 ).contains ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v5, :cond_2 */
final String v5 = "com.android.server.telecom"; // const-string v5, "com.android.server.telecom"
v5 = (( java.lang.String ) v4 ).contains ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 41 */
} // :cond_0
final String v5 = "com.android.mms"; // const-string v5, "com.android.mms"
v5 = (( java.lang.String ) v4 ).contains ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 42 */
(( com.android.server.notification.NotificationManagerService ) p0 ).getContext ( ); // invoke-virtual {p0}, Lcom/android/server/notification/NotificationManagerService;->getContext()Landroid/content/Context;
final String v6 = "mms_breathing_light_color"; // const-string v6, "mms_breathing_light_color"
final String v7 = "mms_breathing_light_freq"; // const-string v7, "mms_breathing_light_freq"
com.android.server.notification.NotificationLightController .customizeNotificationLight ( v5,p1,v6,v7,p2 );
/* .line 45 */
int v2 = 1; // const/4 v2, 0x1
/* .line 47 */
} // .end local v4 # "light":Ljava/lang/String;
} // :cond_1
/* .line 37 */
/* .restart local v4 # "light":Ljava/lang/String; */
} // :cond_2
} // :goto_1
(( com.android.server.notification.NotificationManagerService ) p0 ).getContext ( ); // invoke-virtual {p0}, Lcom/android/server/notification/NotificationManagerService;->getContext()Landroid/content/Context;
final String v5 = "call_breathing_light_color"; // const-string v5, "call_breathing_light_color"
final String v6 = "call_breathing_light_freq"; // const-string v6, "call_breathing_light_freq"
com.android.server.notification.NotificationLightController .customizeNotificationLight ( v3,p1,v5,v6,p2 );
/* .line 40 */
return;
/* .line 49 */
} // .end local v4 # "light":Ljava/lang/String;
} // :cond_3
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 50 */
return;
/* .line 54 */
} // :cond_4
/* iget v3, p1, Landroid/app/Notification;->defaults:I */
/* and-int/lit8 v3, v3, 0x4 */
if ( v3 != null) { // if-eqz v3, :cond_5
/* .line 55 */
(( com.android.server.notification.NotificationManagerService ) p0 ).getContext ( ); // invoke-virtual {p0}, Lcom/android/server/notification/NotificationManagerService;->getContext()Landroid/content/Context;
final String v4 = "breathing_light_color"; // const-string v4, "breathing_light_color"
final String v5 = "breathing_light_freq"; // const-string v5, "breathing_light_freq"
com.android.server.notification.NotificationLightController .customizeNotificationLight ( v3,p1,v4,v5,p2 );
/* .line 60 */
} // :cond_5
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 61 */
return;
} // .end method
public static getLedPwmOffOn ( Integer p0 ) {
/* .locals 3 */
/* .param p0, "totalLength" # I */
/* .line 82 */
int v0 = 2; // const/4 v0, 0x2
/* new-array v0, v0, [I */
/* .line 83 */
/* .local v0, "values":[I */
/* div-int/lit8 v1, p0, 0x4 */
/* mul-int/lit8 v1, v1, 0x3 */
int v2 = 0; // const/4 v2, 0x0
/* aput v1, v0, v2 */
/* .line 84 */
/* aget v1, v0, v2 */
/* sub-int v1, p0, v1 */
int v2 = 1; // const/4 v2, 0x1
/* aput v1, v0, v2 */
/* .line 85 */
} // .end method
