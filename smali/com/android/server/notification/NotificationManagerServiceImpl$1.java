class com.android.server.notification.NotificationManagerServiceImpl$1 extends android.database.ContentObserver {
	 /* .source "NotificationManagerServiceImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/notification/NotificationManagerServiceImpl;->registerPrivacyInputMode(Landroid/os/Handler;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.notification.NotificationManagerServiceImpl this$0; //synthetic
/* # direct methods */
 com.android.server.notification.NotificationManagerServiceImpl$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/notification/NotificationManagerServiceImpl; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 804 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 3 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 807 */
/* invoke-super {p0, p1, p2}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;)V */
/* .line 808 */
v0 = this.this$0;
com.android.server.notification.NotificationManagerServiceImpl .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "miui_privacy_input_pkg_name"; // const-string v1, "miui_privacy_input_pkg_name"
android.provider.Settings$Secure .getString ( v0,v1 );
/* .line 809 */
/* .local v0, "pkg":Ljava/lang/String; */
v1 = this.this$0;
/* if-nez v0, :cond_0 */
final String v2 = ""; // const-string v2, ""
} // :cond_0
/* move-object v2, v0 */
} // :goto_0
com.android.server.notification.NotificationManagerServiceImpl .-$$Nest$fputmPrivacyInputModePkgName ( v1,v2 );
/* .line 810 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "onChange:"; // const-string v2, "onChange:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "NotificationManagerServiceImpl"; // const-string v2, "NotificationManagerServiceImpl"
android.util.Slog .d ( v2,v1 );
/* .line 811 */
return;
} // .end method
