class com.android.server.notification.BarrageListenerServiceImpl$BHandler extends android.os.Handler {
	 /* .source "BarrageListenerServiceImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/notification/BarrageListenerServiceImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "BHandler" */
} // .end annotation
/* # instance fields */
final com.android.server.notification.BarrageListenerServiceImpl this$0; //synthetic
/* # direct methods */
private com.android.server.notification.BarrageListenerServiceImpl$BHandler ( ) {
/* .locals 0 */
/* .line 56 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/os/Handler;-><init>()V */
return;
} // .end method
 com.android.server.notification.BarrageListenerServiceImpl$BHandler ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/notification/BarrageListenerServiceImpl$BHandler;-><init>(Lcom/android/server/notification/BarrageListenerServiceImpl;)V */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 3 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 60 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* packed-switch v0, :pswitch_data_0 */
/* .line 62 */
/* :pswitch_0 */
/* iget v0, p1, Landroid/os/Message;->arg1:I */
/* .line 63 */
/* .local v0, "userId":I */
v1 = this.this$0;
com.android.server.notification.BarrageListenerServiceImpl .-$$Nest$fgetmAm ( v1 );
final String v2 = "com.xiaomi.barrage"; // const-string v2, "com.xiaomi.barrage"
(( android.app.ActivityManager ) v1 ).forceStopPackageAsUser ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Landroid/app/ActivityManager;->forceStopPackageAsUser(Ljava/lang/String;I)V
/* .line 64 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "stop the barrage, mTurnOn:" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.this$0;
v2 = com.android.server.notification.BarrageListenerServiceImpl .-$$Nest$fgetmTurnOn ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " mInGame:"; // const-string v2, " mInGame:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.this$0;
v2 = com.android.server.notification.BarrageListenerServiceImpl .-$$Nest$fgetmInGame ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "BarrageListenerService"; // const-string v2, "BarrageListenerService"
android.util.Log .d ( v2,v1 );
/* .line 67 */
} // .end local v0 # "userId":I
} // :goto_0
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
