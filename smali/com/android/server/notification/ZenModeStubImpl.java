public class com.android.server.notification.ZenModeStubImpl implements com.android.server.notification.ZenModeStub {
	 /* .source "ZenModeStubImpl.java" */
	 /* # interfaces */
	 /* # direct methods */
	 public com.android.server.notification.ZenModeStubImpl ( ) {
		 /* .locals 0 */
		 /* .line 16 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Integer getRingerModeAffectedStreams ( android.content.Context p0, Integer p1, Integer p2 ) {
		 /* .locals 4 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .param p2, "streams" # I */
		 /* .param p3, "zenmode" # I */
		 /* .line 61 */
		 /* or-int/lit8 p2, p2, 0x26 */
		 /* .line 65 */
		 int v0 = 2; // const/4 v0, 0x2
		 /* if-ne p3, v0, :cond_0 */
		 /* .line 68 */
		 /* or-int/lit16 p2, p2, 0x818 */
		 /* .line 72 */
	 } // :cond_0
	 /* and-int/lit16 p2, p2, -0x819 */
	 /* .line 76 */
	 (( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
	 int v1 = 0; // const/4 v1, 0x0
	 int v2 = -3; // const/4 v2, -0x3
	 final String v3 = "mute_music_at_silent"; // const-string v3, "mute_music_at_silent"
	 v0 = 	 android.provider.Settings$System .getIntForUser ( v0,v3,v1,v2 );
	 /* .line 78 */
	 /* .local v0, "muteMusic":I */
	 int v1 = 1; // const/4 v1, 0x1
	 /* if-ne v0, v1, :cond_1 */
	 /* .line 79 */
	 /* or-int/lit16 p2, p2, 0x808 */
	 /* .line 82 */
} // :cond_1
/* and-int/lit16 p2, p2, -0x809 */
/* .line 88 */
} // .end local v0 # "muteMusic":I
} // :goto_0
} // .end method
public java.lang.String hideTelNumbers ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "str" # Ljava/lang/String; */
/* .line 19 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 20 */
/* .local v0, "sb":Ljava/lang/StringBuilder; */
/* const-string/jumbo v1, "tel:" */
v1 = (( java.lang.String ) p1 ).indexOf ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
/* .line 21 */
/* .local v1, "index":I */
/* if-ltz v1, :cond_1 */
/* .line 26 */
/* add-int/lit8 v2, v1, 0x7 */
(( java.lang.String ) p1 ).substring ( v1, v2 ); // invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 27 */
/* add-int/lit8 v2, v1, 0x7 */
/* .local v2, "i":I */
} // :goto_0
v3 = (( java.lang.String ) p1 ).length ( ); // invoke-virtual {p1}, Ljava/lang/String;->length()I
/* if-ge v2, v3, :cond_0 */
/* .line 28 */
/* const/16 v3, 0x2a */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* .line 27 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 30 */
} // .end local v2 # "i":I
} // :cond_0
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 32 */
} // :cond_1
} // .end method
public Boolean isXSpaceUserId ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "userId" # I */
/* .line 93 */
v0 = miui.securityspace.XSpaceUserHandle .isXSpaceUserId ( p1 );
} // .end method
public Boolean shouldInterceptWhenUnLocked ( android.content.Context p0, Boolean p1 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "isForce" # Z */
/* .line 40 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_0 */
/* .line 41 */
/* .line 43 */
} // :cond_0
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v2, "zen_mode_intercepted_when_unlocked" */
int v3 = 1; // const/4 v3, 0x1
v1 = android.provider.Settings$System .getInt ( v1,v2,v3 );
/* if-nez v1, :cond_2 */
/* .line 45 */
if ( p2 != null) { // if-eqz p2, :cond_1
/* .line 46 */
/* .line 48 */
} // :cond_1
/* nop */
/* .line 49 */
final String v1 = "keyguard"; // const-string v1, "keyguard"
(( android.content.Context ) p1 ).getSystemService ( v1 ); // invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v1, Landroid/app/KeyguardManager; */
/* .line 50 */
/* .local v1, "mKgm":Landroid/app/KeyguardManager; */
if ( v1 != null) { // if-eqz v1, :cond_2
v2 = (( android.app.KeyguardManager ) v1 ).inKeyguardRestrictedInputMode ( ); // invoke-virtual {v1}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z
/* if-nez v2, :cond_2 */
/* .line 51 */
/* .line 54 */
} // .end local v1 # "mKgm":Landroid/app/KeyguardManager;
} // :cond_2
} // .end method
public void zenContentObserver ( android.content.Context p0, java.lang.String p1 ) {
/* .locals 7 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "reason" # Ljava/lang/String; */
/* .line 98 */
final String v0 = "ZEN_NUMBER"; // const-string v0, "ZEN_NUMBER"
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v1 */
/* .line 100 */
/* .local v1, "identity":J */
try { // :try_start_0
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v4 = 0; // const/4 v4, 0x0
v3 = android.provider.Settings$Secure .getInt ( v3,v0,v4 );
int v5 = 1; // const/4 v5, 0x1
/* if-gt v3, v5, :cond_0 */
/* .line 101 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 102 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v4 = android.provider.Settings$Secure .getInt ( v6,v0,v4 );
/* add-int/2addr v4, v5 */
/* .line 101 */
android.provider.Settings$Secure .putInt ( v3,v0,v4 );
/* .line 104 */
} // :cond_0
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v3 = "REASON"; // const-string v3, "REASON"
android.provider.Settings$Secure .putString ( v0,v3,p2 );
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 106 */
android.os.Binder .restoreCallingIdentity ( v1,v2 );
/* .line 107 */
/* nop */
/* .line 108 */
return;
/* .line 106 */
/* :catchall_0 */
/* move-exception v0 */
android.os.Binder .restoreCallingIdentity ( v1,v2 );
/* .line 107 */
/* throw v0 */
} // .end method
