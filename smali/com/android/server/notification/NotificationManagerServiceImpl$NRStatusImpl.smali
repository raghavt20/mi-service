.class public final Lcom/android/server/notification/NotificationManagerServiceImpl$NRStatusImpl;
.super Ljava/lang/Object;
.source "NotificationManagerServiceImpl.java"

# interfaces
.implements Lcom/android/server/notification/NotificationManagerServiceStub$NRStatusStub;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/notification/NotificationManagerServiceImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NRStatusImpl"
.end annotation


# instance fields
.field private mStatusBarAllowAlert:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 216
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 218
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/notification/NotificationManagerServiceImpl$NRStatusImpl;->mStatusBarAllowAlert:I

    return-void
.end method


# virtual methods
.method public getStatusBarAllowAlert()I
    .locals 1

    .line 227
    iget v0, p0, Lcom/android/server/notification/NotificationManagerServiceImpl$NRStatusImpl;->mStatusBarAllowAlert:I

    return v0
.end method

.method public isStatusBarAllowLed()Z
    .locals 1

    .line 242
    iget v0, p0, Lcom/android/server/notification/NotificationManagerServiceImpl$NRStatusImpl;->mStatusBarAllowAlert:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isStatusBarAllowSound()Z
    .locals 1

    .line 237
    iget v0, p0, Lcom/android/server/notification/NotificationManagerServiceImpl$NRStatusImpl;->mStatusBarAllowAlert:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isStatusBarAllowVibration()Z
    .locals 2

    .line 232
    iget v0, p0, Lcom/android/server/notification/NotificationManagerServiceImpl$NRStatusImpl;->mStatusBarAllowAlert:I

    const/4 v1, 0x1

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public setStatusBarAllowAlert(I)V
    .locals 0
    .param p1, "statusBarAllowAlert"    # I

    .line 222
    iput p1, p0, Lcom/android/server/notification/NotificationManagerServiceImpl$NRStatusImpl;->mStatusBarAllowAlert:I

    .line 223
    return-void
.end method
