.class public Lcom/android/server/notification/ZenModeStubImpl;
.super Ljava/lang/Object;
.source "ZenModeStubImpl.java"

# interfaces
.implements Lcom/android/server/notification/ZenModeStub;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getRingerModeAffectedStreams(Landroid/content/Context;II)I
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "streams"    # I
    .param p3, "zenmode"    # I

    .line 61
    or-int/lit8 p2, p2, 0x26

    .line 65
    const/4 v0, 0x2

    if-ne p3, v0, :cond_0

    .line 68
    or-int/lit16 p2, p2, 0x818

    goto :goto_0

    .line 72
    :cond_0
    and-int/lit16 p2, p2, -0x819

    .line 76
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, -0x3

    const-string v3, "mute_music_at_silent"

    invoke-static {v0, v3, v1, v2}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    .line 78
    .local v0, "muteMusic":I
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 79
    or-int/lit16 p2, p2, 0x808

    goto :goto_0

    .line 82
    :cond_1
    and-int/lit16 p2, p2, -0x809

    .line 88
    .end local v0    # "muteMusic":I
    :goto_0
    return p2
.end method

.method public hideTelNumbers(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "str"    # Ljava/lang/String;

    .line 19
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 20
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "tel:"

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 21
    .local v1, "index":I
    if-ltz v1, :cond_1

    .line 26
    add-int/lit8 v2, v1, 0x7

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    add-int/lit8 v2, v1, 0x7

    .local v2, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 28
    const/16 v3, 0x2a

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 27
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 30
    .end local v2    # "i":I
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 32
    :cond_1
    return-object p1
.end method

.method public isXSpaceUserId(I)Z
    .locals 1
    .param p1, "userId"    # I

    .line 93
    invoke-static {p1}, Lmiui/securityspace/XSpaceUserHandle;->isXSpaceUserId(I)Z

    move-result v0

    return v0
.end method

.method public shouldInterceptWhenUnLocked(Landroid/content/Context;Z)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "isForce"    # Z

    .line 40
    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 41
    return v0

    .line 43
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "zen_mode_intercepted_when_unlocked"

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_2

    .line 45
    if-eqz p2, :cond_1

    .line 46
    return v0

    .line 48
    :cond_1
    nop

    .line 49
    const-string v1, "keyguard"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/KeyguardManager;

    .line 50
    .local v1, "mKgm":Landroid/app/KeyguardManager;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v2

    if-nez v2, :cond_2

    .line 51
    return v0

    .line 54
    .end local v1    # "mKgm":Landroid/app/KeyguardManager;
    :cond_2
    return v3
.end method

.method public zenContentObserver(Landroid/content/Context;Ljava/lang/String;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "reason"    # Ljava/lang/String;

    .line 98
    const-string v0, "ZEN_NUMBER"

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v1

    .line 100
    .local v1, "identity":J
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v3, v0, v4}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    const/4 v5, 0x1

    if-gt v3, v5, :cond_0

    .line 101
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 102
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-static {v6, v0, v4}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    add-int/2addr v4, v5

    .line 101
    invoke-static {v3, v0, v4}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 104
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "REASON"

    invoke-static {v0, v3, p2}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 107
    nop

    .line 108
    return-void

    .line 106
    :catchall_0
    move-exception v0

    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 107
    throw v0
.end method
