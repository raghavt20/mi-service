.class public final Lcom/android/server/notification/NotificationManagerServiceImpl$NMSVersionImpl;
.super Ljava/lang/Object;
.source "NotificationManagerServiceImpl.java"

# interfaces
.implements Lcom/android/server/notification/NotificationManagerServiceStub$NMSVersionStub;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/notification/NotificationManagerServiceImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NMSVersionImpl"
.end annotation


# static fields
.field static final ATT_MIUI_VERSION:Ljava/lang/String; = "miui_version"


# instance fields
.field private miuiVersion:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 176
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/notification/NotificationManagerServiceImpl$NMSVersionImpl;->miuiVersion:I

    return-void
.end method


# virtual methods
.method public getMiuiVersion()I
    .locals 1

    .line 182
    iget v0, p0, Lcom/android/server/notification/NotificationManagerServiceImpl$NMSVersionImpl;->miuiVersion:I

    return v0
.end method

.method public readXml(Lcom/android/modules/utils/TypedXmlPullParser;)V
    .locals 2
    .param p1, "parser"    # Lcom/android/modules/utils/TypedXmlPullParser;

    .line 192
    const-string v0, "miui_version"

    const/4 v1, -0x1

    invoke-static {p1, v0, v1}, Lcom/android/internal/util/XmlUtils;->readIntAttribute(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/server/notification/NotificationManagerServiceImpl$NMSVersionImpl;->miuiVersion:I

    .line 193
    return-void
.end method

.method public setMiuiVersion(I)V
    .locals 0
    .param p1, "version"    # I

    .line 187
    iput p1, p0, Lcom/android/server/notification/NotificationManagerServiceImpl$NMSVersionImpl;->miuiVersion:I

    .line 188
    return-void
.end method

.method public writeXml(Lcom/android/modules/utils/TypedXmlSerializer;)V
    .locals 3
    .param p1, "out"    # Lcom/android/modules/utils/TypedXmlSerializer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 197
    const-string v0, "miui_version"

    iget v1, p0, Lcom/android/server/notification/NotificationManagerServiceImpl$NMSVersionImpl;->miuiVersion:I

    const/4 v2, 0x0

    invoke-interface {p1, v2, v0, v1}, Lcom/android/modules/utils/TypedXmlSerializer;->attributeInt(Ljava/lang/String;Ljava/lang/String;I)Lorg/xmlpull/v1/XmlSerializer;

    .line 198
    return-void
.end method
