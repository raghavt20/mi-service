.class public Lcom/android/server/notification/ZenModeHelperInjector;
.super Ljava/lang/Object;
.source "ZenModeHelperInjector.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    return-void
.end method

.method static applyMiuiRestrictions(Lcom/android/server/notification/ZenModeHelper;Landroid/app/AppOpsManager;)V
    .locals 9
    .param p0, "helper"    # Lcom/android/server/notification/ZenModeHelper;
    .param p1, "mAppOps"    # Landroid/app/AppOpsManager;

    .line 85
    sget-boolean v0, Landroid/provider/MiuiSettings$SilenceMode;->isSupported:Z

    if-nez v0, :cond_0

    return-void

    .line 87
    :cond_0
    const-string v0, "com.android.cellbroadcastreceiver"

    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v1

    .line 88
    .local v1, "defaultException":[Ljava/lang/String;
    const-string v2, "android"

    const-string v3, "com.android.server.telecom"

    const-string v4, "com.android.systemui"

    filled-new-array {v4, v2, v0, v3}, [Ljava/lang/String;

    move-result-object v0

    .line 93
    .local v0, "exceptionPackages":[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/android/server/notification/ZenModeHelper;->getZenMode()I

    move-result v2

    .line 94
    .local v2, "mode":I
    invoke-virtual {p0}, Lcom/android/server/notification/ZenModeHelper;->getConfig()Landroid/service/notification/ZenModeConfig;

    move-result-object v3

    .line 95
    .local v3, "config":Landroid/service/notification/ZenModeConfig;
    const/4 v4, 0x1

    .line 96
    .local v4, "allowRingtone":Z
    const/4 v5, 0x1

    .line 97
    .local v5, "allowNotification":Z
    const/4 v6, 0x0

    .line 98
    .local v6, "hasException":Z
    packed-switch v2, :pswitch_data_0

    .line 108
    const/4 v5, 0x1

    .line 109
    const/4 v4, 0x1

    goto :goto_2

    .line 100
    :pswitch_0
    const/4 v4, 0x0

    .line 101
    const/4 v5, 0x0

    .line 102
    iget-boolean v7, v3, Landroid/service/notification/ZenModeConfig;->allowCalls:Z

    if-nez v7, :cond_2

    iget-boolean v7, v3, Landroid/service/notification/ZenModeConfig;->allowRepeatCallers:Z

    if-eqz v7, :cond_1

    goto :goto_0

    :cond_1
    const/4 v7, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v7, 0x1

    :goto_1
    move v6, v7

    .line 104
    nop

    .line 112
    :goto_2
    nop

    .line 116
    if-eqz v6, :cond_3

    move-object v7, v0

    goto :goto_3

    :cond_3
    move-object v7, v1

    .line 112
    :goto_3
    const/4 v8, 0x6

    invoke-static {v4, v8, p1, v7}, Lcom/android/server/notification/ZenModeHelperInjector;->applyRestriction(ZILandroid/app/AppOpsManager;[Ljava/lang/String;)V

    .line 117
    nop

    .line 121
    if-eqz v6, :cond_4

    move-object v7, v0

    goto :goto_4

    :cond_4
    move-object v7, v1

    .line 117
    :goto_4
    const/4 v8, 0x5

    invoke-static {v5, v8, p1, v7}, Lcom/android/server/notification/ZenModeHelperInjector;->applyRestriction(ZILandroid/app/AppOpsManager;[Ljava/lang/String;)V

    .line 122
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private static applyRestriction(ZILandroid/app/AppOpsManager;[Ljava/lang/String;)V
    .locals 2
    .param p0, "allow"    # Z
    .param p1, "usage"    # I
    .param p2, "appOps"    # Landroid/app/AppOpsManager;
    .param p3, "exception"    # [Ljava/lang/String;

    .line 125
    nop

    .line 126
    nop

    .line 125
    xor-int/lit8 v0, p0, 0x1

    const/16 v1, 0x1c

    invoke-virtual {p2, v1, p1, v0, p3}, Landroid/app/AppOpsManager;->setRestriction(III[Ljava/lang/String;)V

    .line 128
    nop

    .line 129
    nop

    .line 128
    xor-int/lit8 v0, p0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p2, v1, p1, v0, p3}, Landroid/app/AppOpsManager;->setRestriction(III[Ljava/lang/String;)V

    .line 131
    return-void
.end method

.method static applyRingerModeToZen(Lcom/android/server/notification/ZenModeHelper;Landroid/content/Context;I)I
    .locals 4
    .param p0, "helper"    # Lcom/android/server/notification/ZenModeHelper;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "ringerMode"    # I

    .line 26
    invoke-virtual {p0}, Lcom/android/server/notification/ZenModeHelper;->getZenMode()I

    move-result v0

    .line 27
    .local v0, "zenMode":I
    const/4 v1, -0x1

    .line 28
    .local v1, "newZen":I
    packed-switch p2, :pswitch_data_0

    .line 42
    return v1

    .line 36
    :pswitch_0
    if-eqz v0, :cond_1

    const/4 v2, 0x1

    if-eq v0, v2, :cond_1

    .line 37
    invoke-static {p1}, Landroid/provider/MiuiSettings$AntiSpam;->isQuietModeEnable(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    .line 38
    :cond_0
    const/4 v2, 0x0

    :goto_0
    move v1, v2

    goto :goto_1

    .line 30
    :pswitch_1
    const/4 v2, 0x3

    if-eq v0, v2, :cond_1

    const/4 v2, 0x2

    if-eq v0, v2, :cond_1

    .line 31
    const/4 v1, 0x3

    .line 44
    :cond_1
    :goto_1
    return v1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method static applyRingerModeToZen(Lcom/android/server/notification/ZenModeHelper;Landroid/content/Context;III)I
    .locals 4
    .param p0, "helper"    # Lcom/android/server/notification/ZenModeHelper;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "ringerModeOld"    # I
    .param p3, "ringerModeNew"    # I
    .param p4, "newZen"    # I

    .line 49
    sget-boolean v0, Landroid/provider/MiuiSettings$SilenceMode;->isSupported:Z

    if-nez v0, :cond_0

    .line 50
    invoke-static {p0, p1, p3}, Lcom/android/server/notification/ZenModeHelperInjector;->applyRingerModeToZen(Lcom/android/server/notification/ZenModeHelper;Landroid/content/Context;I)I

    move-result v0

    return v0

    .line 53
    :cond_0
    if-eq p3, p2, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 54
    .local v0, "isChange":Z
    :goto_0
    invoke-virtual {p0}, Lcom/android/server/notification/ZenModeHelper;->getZenMode()I

    move-result v1

    .line 56
    .local v1, "zenMode":I
    const/4 v2, 0x4

    packed-switch p3, :pswitch_data_0

    .line 78
    return p4

    .line 73
    :pswitch_0
    if-eqz v0, :cond_4

    if-ne v1, v2, :cond_4

    .line 74
    const/4 p4, 0x0

    goto :goto_1

    .line 59
    :pswitch_1
    if-eqz v0, :cond_3

    .line 60
    if-nez v1, :cond_2

    .line 61
    const/4 p4, 0x4

    goto :goto_1

    .line 62
    :cond_2
    if-ne v2, v1, :cond_4

    .line 63
    const/4 p4, -0x1

    goto :goto_1

    .line 66
    :cond_3
    if-nez p4, :cond_4

    .line 67
    move p4, v1

    .line 68
    const-string v2, "ZenModeHelperInjector"

    const-string v3, "RINGER MODE is not Change"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    :cond_4
    :goto_1
    return p4

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static getOutRingerMode(IIII)I
    .locals 1
    .param p0, "newZen"    # I
    .param p1, "curZen"    # I
    .param p2, "ringerModeNew"    # I
    .param p3, "out"    # I

    .line 134
    sget-boolean v0, Landroid/provider/MiuiSettings$SilenceMode;->isSupported:Z

    if-nez v0, :cond_0

    return p3

    .line 135
    :cond_0
    const/4 v0, -0x1

    if-ne p0, v0, :cond_1

    move v0, p1

    goto :goto_0

    :cond_1
    move v0, p0

    :goto_0
    move p0, v0

    .line 136
    const/4 v0, 0x1

    if-ne p0, v0, :cond_2

    move v0, p3

    goto :goto_1

    :cond_2
    move v0, p2

    :goto_1
    return v0
.end method

.method static miuiComputeZenMode(Ljava/lang/String;Landroid/service/notification/ZenModeConfig;)I
    .locals 5
    .param p0, "reason"    # Ljava/lang/String;
    .param p1, "config"    # Landroid/service/notification/ZenModeConfig;

    .line 153
    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 154
    :cond_0
    iget-object v1, p1, Landroid/service/notification/ZenModeConfig;->manualRule:Landroid/service/notification/ZenModeConfig$ZenRule;

    if-eqz v1, :cond_1

    .line 155
    const-string v1, "conditionChanged"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 156
    const-string/jumbo v1, "setNotificationPolicy"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 157
    const-string/jumbo v1, "updateAutomaticZenRule"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 158
    const-string v1, "onSystemReady"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 159
    const-string v1, "readXml"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 160
    const-string v1, "init"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 161
    const-string/jumbo v1, "zmc.onServiceAdded"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 162
    const-string v1, "cleanUpZenRules"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 163
    iget-object v0, p1, Landroid/service/notification/ZenModeConfig;->manualRule:Landroid/service/notification/ZenModeConfig$ZenRule;

    iget v0, v0, Landroid/service/notification/ZenModeConfig$ZenRule;->zenMode:I

    return v0

    .line 165
    :cond_1
    iget-object v1, p1, Landroid/service/notification/ZenModeConfig;->manualRule:Landroid/service/notification/ZenModeConfig$ZenRule;

    if-nez v1, :cond_2

    goto :goto_0

    :cond_2
    iget-object v0, p1, Landroid/service/notification/ZenModeConfig;->manualRule:Landroid/service/notification/ZenModeConfig$ZenRule;

    iget v0, v0, Landroid/service/notification/ZenModeConfig$ZenRule;->zenMode:I

    .line 166
    .local v0, "zen":I
    :goto_0
    iget-object v1, p1, Landroid/service/notification/ZenModeConfig;->automaticRules:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/service/notification/ZenModeConfig$ZenRule;

    .line 167
    .local v2, "automaticRule":Landroid/service/notification/ZenModeConfig$ZenRule;
    invoke-virtual {v2}, Landroid/service/notification/ZenModeConfig$ZenRule;->isAutomaticActive()Z

    move-result v3

    if-eqz v3, :cond_4

    iget v3, v2, Landroid/service/notification/ZenModeConfig$ZenRule;->zenMode:I

    invoke-static {v3}, Lcom/android/server/notification/ZenModeHelperInjector;->zenSeverity(I)I

    move-result v3

    invoke-static {v0}, Lcom/android/server/notification/ZenModeHelperInjector;->zenSeverity(I)I

    move-result v4

    if-gt v3, v4, :cond_3

    iget v3, v2, Landroid/service/notification/ZenModeConfig$ZenRule;->zenMode:I

    const/4 v4, 0x4

    if-ne v3, v4, :cond_4

    if-nez v0, :cond_4

    .line 169
    :cond_3
    iget v0, v2, Landroid/service/notification/ZenModeConfig$ZenRule;->zenMode:I

    .line 171
    .end local v2    # "automaticRule":Landroid/service/notification/ZenModeConfig$ZenRule;
    :cond_4
    goto :goto_1

    .line 172
    :cond_5
    return v0
.end method

.method private static zenSeverity(I)I
    .locals 1
    .param p0, "zen"    # I

    .line 140
    packed-switch p0, :pswitch_data_0

    .line 148
    const/4 v0, 0x0

    return v0

    .line 144
    :pswitch_0
    const/4 v0, 0x2

    return v0

    .line 146
    :pswitch_1
    const/4 v0, 0x3

    return v0

    .line 142
    :pswitch_2
    const/4 v0, 0x1

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
