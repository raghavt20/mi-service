class com.android.server.notification.BarrageListenerServiceImpl$SettingsObserver extends android.database.ContentObserver {
	 /* .source "BarrageListenerServiceImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/notification/BarrageListenerServiceImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "SettingsObserver" */
} // .end annotation
/* # instance fields */
final com.android.server.notification.BarrageListenerServiceImpl this$0; //synthetic
/* # direct methods */
public com.android.server.notification.BarrageListenerServiceImpl$SettingsObserver ( ) {
/* .locals 0 */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 72 */
this.this$0 = p1;
/* .line 73 */
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
/* .line 74 */
return;
} // .end method
private void update ( ) {
/* .locals 4 */
/* .line 93 */
v0 = this.this$0;
v0 = com.android.server.notification.BarrageListenerServiceImpl .-$$Nest$fgetmTurnOn ( v0 );
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_1
	 v0 = this.this$0;
	 v0 = 	 com.android.server.notification.BarrageListenerServiceImpl .-$$Nest$fgetmInGame ( v0 );
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 /* .line 94 */
		 v0 = this.this$0;
		 com.android.server.notification.BarrageListenerServiceImpl .-$$Nest$fgetmHandler ( v0 );
		 v0 = 		 (( android.os.Handler ) v0 ).hasMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 95 */
			 v0 = this.this$0;
			 com.android.server.notification.BarrageListenerServiceImpl .-$$Nest$fgetmHandler ( v0 );
			 (( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
			 /* .line 97 */
		 } // :cond_0
		 final String v0 = "com.xiaomi.barrage/com.xiaomi.barrage.service.NotificationMonitorService"; // const-string v0, "com.xiaomi.barrage/com.xiaomi.barrage.service.NotificationMonitorService"
		 android.content.ComponentName .unflattenFromString ( v0 );
		 /* .line 98 */
		 /* .local v0, "cn":Landroid/content/ComponentName; */
		 v1 = this.this$0;
		 com.android.server.notification.BarrageListenerServiceImpl .-$$Nest$fgetmListener ( v1 );
		 v2 = this.this$0;
		 v2 = 		 com.android.server.notification.BarrageListenerServiceImpl .-$$Nest$fgetmCurrentUserId ( v2 );
		 (( com.android.server.notification.ManagedServices ) v1 ).registerService ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Lcom/android/server/notification/ManagedServices;->registerService(Landroid/content/ComponentName;I)V
		 /* .line 99 */
		 /* new-instance v1, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
		 /* const-string/jumbo v2, "start the barrage, mTurnOn:" */
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 v2 = this.this$0;
		 v2 = 		 com.android.server.notification.BarrageListenerServiceImpl .-$$Nest$fgetmTurnOn ( v2 );
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 final String v2 = " mInGame:"; // const-string v2, " mInGame:"
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 v2 = this.this$0;
		 v2 = 		 com.android.server.notification.BarrageListenerServiceImpl .-$$Nest$fgetmInGame ( v2 );
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 final String v2 = "BarrageListenerService"; // const-string v2, "BarrageListenerService"
		 android.util.Log .d ( v2,v1 );
		 /* .line 100 */
	 } // .end local v0 # "cn":Landroid/content/ComponentName;
	 /* .line 101 */
} // :cond_1
v0 = this.this$0;
com.android.server.notification.BarrageListenerServiceImpl .-$$Nest$fgetmHandler ( v0 );
v0 = (( android.os.Handler ) v0 ).hasMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z
if ( v0 != null) { // if-eqz v0, :cond_2
	 /* .line 102 */
	 return;
	 /* .line 104 */
} // :cond_2
v0 = this.this$0;
com.android.server.notification.BarrageListenerServiceImpl .-$$Nest$fgetmHandler ( v0 );
(( android.os.Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 105 */
/* .local v0, "msg":Landroid/os/Message; */
v1 = this.this$0;
v1 = com.android.server.notification.BarrageListenerServiceImpl .-$$Nest$fgetmCurrentUserId ( v1 );
/* iput v1, v0, Landroid/os/Message;->arg1:I */
/* .line 106 */
v1 = this.this$0;
com.android.server.notification.BarrageListenerServiceImpl .-$$Nest$fgetmHandler ( v1 );
/* const-wide/16 v2, 0x2710 */
(( android.os.Handler ) v1 ).sendMessageDelayed ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 108 */
} // .end local v0 # "msg":Landroid/os/Message;
} // :goto_0
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 5 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 78 */
/* invoke-super {p0, p1, p2}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;)V */
/* .line 79 */
final String v0 = "gb_bullet_chat"; // const-string v0, "gb_bullet_chat"
android.provider.Settings$Secure .getUriFor ( v0 );
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
int v2 = 0; // const/4 v2, 0x0
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 80 */
v1 = this.this$0;
com.android.server.notification.BarrageListenerServiceImpl .-$$Nest$fgetmContext ( v1 );
/* .line 81 */
(( android.content.Context ) v3 ).getContentResolver ( ); // invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v4 = this.this$0;
v4 = com.android.server.notification.BarrageListenerServiceImpl .-$$Nest$fgetmCurrentUserId ( v4 );
v0 = android.provider.Settings$Secure .getIntForUser ( v3,v0,v2,v4 );
com.android.server.notification.BarrageListenerServiceImpl .-$$Nest$fputmTurnOn ( v1,v0 );
/* .line 84 */
} // :cond_0
final String v0 = "gb_boosting"; // const-string v0, "gb_boosting"
android.provider.Settings$Secure .getUriFor ( v0 );
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 85 */
v1 = this.this$0;
com.android.server.notification.BarrageListenerServiceImpl .-$$Nest$fgetmContext ( v1 );
/* .line 86 */
(( android.content.Context ) v3 ).getContentResolver ( ); // invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v4 = this.this$0;
v4 = com.android.server.notification.BarrageListenerServiceImpl .-$$Nest$fgetmCurrentUserId ( v4 );
v0 = android.provider.Settings$Secure .getIntForUser ( v3,v0,v2,v4 );
com.android.server.notification.BarrageListenerServiceImpl .-$$Nest$fputmInGame ( v1,v0 );
/* .line 89 */
} // :cond_1
/* invoke-direct {p0}, Lcom/android/server/notification/BarrageListenerServiceImpl$SettingsObserver;->update()V */
/* .line 90 */
return;
} // .end method
public void onChangeAll ( ) {
/* .locals 2 */
/* .line 111 */
final String v0 = "gb_bullet_chat"; // const-string v0, "gb_bullet_chat"
android.provider.Settings$Secure .getUriFor ( v0 );
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.notification.BarrageListenerServiceImpl$SettingsObserver ) p0 ).onChange ( v1, v0 ); // invoke-virtual {p0, v1, v0}, Lcom/android/server/notification/BarrageListenerServiceImpl$SettingsObserver;->onChange(ZLandroid/net/Uri;)V
/* .line 112 */
final String v0 = "gb_boosting"; // const-string v0, "gb_boosting"
android.provider.Settings$Secure .getUriFor ( v0 );
(( com.android.server.notification.BarrageListenerServiceImpl$SettingsObserver ) p0 ).onChange ( v1, v0 ); // invoke-virtual {p0, v1, v0}, Lcom/android/server/notification/BarrageListenerServiceImpl$SettingsObserver;->onChange(ZLandroid/net/Uri;)V
/* .line 113 */
return;
} // .end method
