class com.android.server.notification.BarrageListenerServiceImpl$UserSwitchReceiver extends android.content.BroadcastReceiver {
	 /* .source "BarrageListenerServiceImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/notification/BarrageListenerServiceImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "UserSwitchReceiver" */
} // .end annotation
/* # instance fields */
final com.android.server.notification.BarrageListenerServiceImpl this$0; //synthetic
/* # direct methods */
private com.android.server.notification.BarrageListenerServiceImpl$UserSwitchReceiver ( ) {
/* .locals 0 */
/* .line 116 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
 com.android.server.notification.BarrageListenerServiceImpl$UserSwitchReceiver ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/notification/BarrageListenerServiceImpl$UserSwitchReceiver;-><init>(Lcom/android/server/notification/BarrageListenerServiceImpl;)V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 120 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 121 */
/* .local v0, "action":Ljava/lang/String; */
final String v1 = "android.intent.action.USER_SWITCHED"; // const-string v1, "android.intent.action.USER_SWITCHED"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
	 /* .line 122 */
	 v1 = this.this$0;
	 v1 = 	 com.android.server.notification.BarrageListenerServiceImpl .-$$Nest$fgetmCurrentUserId ( v1 );
	 /* .line 123 */
	 /* .local v1, "oldUserId":I */
	 v2 = this.this$0;
	 final String v3 = "android.intent.extra.user_handle"; // const-string v3, "android.intent.extra.user_handle"
	 int v4 = -1; // const/4 v4, -0x1
	 v3 = 	 (( android.content.Intent ) p2 ).getIntExtra ( v3, v4 ); // invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
	 com.android.server.notification.BarrageListenerServiceImpl .-$$Nest$fputmCurrentUserId ( v2,v3 );
	 /* .line 124 */
	 /* new-instance v2, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
	 /* const-string/jumbo v3, "the user switches to u" */
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v3 = this.this$0;
	 v3 = 	 com.android.server.notification.BarrageListenerServiceImpl .-$$Nest$fgetmCurrentUserId ( v3 );
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v3 = "BarrageListenerService"; // const-string v3, "BarrageListenerService"
	 android.util.Log .d ( v3,v2 );
	 /* .line 125 */
	 v2 = this.this$0;
	 com.android.server.notification.BarrageListenerServiceImpl .-$$Nest$fgetmHandler ( v2 );
	 int v4 = 1; // const/4 v4, 0x1
	 v2 = 	 (( android.os.Handler ) v2 ).hasMessages ( v4 ); // invoke-virtual {v2, v4}, Landroid/os/Handler;->hasMessages(I)Z
	 if ( v2 != null) { // if-eqz v2, :cond_0
		 /* .line 126 */
		 v2 = this.this$0;
		 com.android.server.notification.BarrageListenerServiceImpl .-$$Nest$fgetmHandler ( v2 );
		 (( android.os.Handler ) v2 ).removeMessages ( v4 ); // invoke-virtual {v2, v4}, Landroid/os/Handler;->removeMessages(I)V
		 /* .line 127 */
		 v2 = this.this$0;
		 com.android.server.notification.BarrageListenerServiceImpl .-$$Nest$fgetmAm ( v2 );
		 final String v4 = "com.xiaomi.barrage"; // const-string v4, "com.xiaomi.barrage"
		 (( android.app.ActivityManager ) v2 ).forceStopPackageAsUser ( v4, v1 ); // invoke-virtual {v2, v4, v1}, Landroid/app/ActivityManager;->forceStopPackageAsUser(Ljava/lang/String;I)V
		 /* .line 128 */
		 /* new-instance v2, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
		 /* const-string/jumbo v4, "stop the barrage when the user has been changed, mTurnOn:" */
		 (( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 v4 = this.this$0;
		 v4 = 		 com.android.server.notification.BarrageListenerServiceImpl .-$$Nest$fgetmTurnOn ( v4 );
		 (( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 final String v4 = " mInGame:"; // const-string v4, " mInGame:"
		 (( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 v4 = this.this$0;
		 v4 = 		 com.android.server.notification.BarrageListenerServiceImpl .-$$Nest$fgetmInGame ( v4 );
		 (( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 final String v4 = " oldUserId:"; // const-string v4, " oldUserId:"
		 (( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Log .d ( v3,v2 );
		 /* .line 131 */
	 } // :cond_0
	 v2 = this.this$0;
	 com.android.server.notification.BarrageListenerServiceImpl .-$$Nest$fgetmSettingsObserver ( v2 );
	 (( com.android.server.notification.BarrageListenerServiceImpl$SettingsObserver ) v2 ).onChangeAll ( ); // invoke-virtual {v2}, Lcom/android/server/notification/BarrageListenerServiceImpl$SettingsObserver;->onChangeAll()V
	 /* .line 133 */
} // .end local v1 # "oldUserId":I
} // :cond_1
return;
} // .end method
