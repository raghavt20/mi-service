.class Lcom/android/server/notification/BarrageListenerServiceImpl$BHandler;
.super Landroid/os/Handler;
.source "BarrageListenerServiceImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/notification/BarrageListenerServiceImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/notification/BarrageListenerServiceImpl;


# direct methods
.method private constructor <init>(Lcom/android/server/notification/BarrageListenerServiceImpl;)V
    .locals 0

    .line 56
    iput-object p1, p0, Lcom/android/server/notification/BarrageListenerServiceImpl$BHandler;->this$0:Lcom/android/server/notification/BarrageListenerServiceImpl;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/notification/BarrageListenerServiceImpl;Lcom/android/server/notification/BarrageListenerServiceImpl$BHandler-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/notification/BarrageListenerServiceImpl$BHandler;-><init>(Lcom/android/server/notification/BarrageListenerServiceImpl;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .line 60
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 62
    :pswitch_0
    iget v0, p1, Landroid/os/Message;->arg1:I

    .line 63
    .local v0, "userId":I
    iget-object v1, p0, Lcom/android/server/notification/BarrageListenerServiceImpl$BHandler;->this$0:Lcom/android/server/notification/BarrageListenerServiceImpl;

    invoke-static {v1}, Lcom/android/server/notification/BarrageListenerServiceImpl;->-$$Nest$fgetmAm(Lcom/android/server/notification/BarrageListenerServiceImpl;)Landroid/app/ActivityManager;

    move-result-object v1

    const-string v2, "com.xiaomi.barrage"

    invoke-virtual {v1, v2, v0}, Landroid/app/ActivityManager;->forceStopPackageAsUser(Ljava/lang/String;I)V

    .line 64
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "stop the barrage, mTurnOn:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/notification/BarrageListenerServiceImpl$BHandler;->this$0:Lcom/android/server/notification/BarrageListenerServiceImpl;

    invoke-static {v2}, Lcom/android/server/notification/BarrageListenerServiceImpl;->-$$Nest$fgetmTurnOn(Lcom/android/server/notification/BarrageListenerServiceImpl;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mInGame:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/notification/BarrageListenerServiceImpl$BHandler;->this$0:Lcom/android/server/notification/BarrageListenerServiceImpl;

    invoke-static {v2}, Lcom/android/server/notification/BarrageListenerServiceImpl;->-$$Nest$fgetmInGame(Lcom/android/server/notification/BarrageListenerServiceImpl;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "BarrageListenerService"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    .end local v0    # "userId":I
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
