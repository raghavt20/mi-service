public class com.android.server.notification.ZenModeHelperInjector {
	 /* .source "ZenModeHelperInjector.java" */
	 /* # direct methods */
	 private com.android.server.notification.ZenModeHelperInjector ( ) {
		 /* .locals 0 */
		 /* .line 22 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 23 */
		 return;
	 } // .end method
	 static void applyMiuiRestrictions ( com.android.server.notification.ZenModeHelper p0, android.app.AppOpsManager p1 ) {
		 /* .locals 9 */
		 /* .param p0, "helper" # Lcom/android/server/notification/ZenModeHelper; */
		 /* .param p1, "mAppOps" # Landroid/app/AppOpsManager; */
		 /* .line 85 */
		 /* sget-boolean v0, Landroid/provider/MiuiSettings$SilenceMode;->isSupported:Z */
		 /* if-nez v0, :cond_0 */
		 return;
		 /* .line 87 */
	 } // :cond_0
	 final String v0 = "com.android.cellbroadcastreceiver"; // const-string v0, "com.android.cellbroadcastreceiver"
	 /* filled-new-array {v0}, [Ljava/lang/String; */
	 /* .line 88 */
	 /* .local v1, "defaultException":[Ljava/lang/String; */
	 final String v2 = "android"; // const-string v2, "android"
	 final String v3 = "com.android.server.telecom"; // const-string v3, "com.android.server.telecom"
	 final String v4 = "com.android.systemui"; // const-string v4, "com.android.systemui"
	 /* filled-new-array {v4, v2, v0, v3}, [Ljava/lang/String; */
	 /* .line 93 */
	 /* .local v0, "exceptionPackages":[Ljava/lang/String; */
	 v2 = 	 (( com.android.server.notification.ZenModeHelper ) p0 ).getZenMode ( ); // invoke-virtual {p0}, Lcom/android/server/notification/ZenModeHelper;->getZenMode()I
	 /* .line 94 */
	 /* .local v2, "mode":I */
	 (( com.android.server.notification.ZenModeHelper ) p0 ).getConfig ( ); // invoke-virtual {p0}, Lcom/android/server/notification/ZenModeHelper;->getConfig()Landroid/service/notification/ZenModeConfig;
	 /* .line 95 */
	 /* .local v3, "config":Landroid/service/notification/ZenModeConfig; */
	 int v4 = 1; // const/4 v4, 0x1
	 /* .line 96 */
	 /* .local v4, "allowRingtone":Z */
	 int v5 = 1; // const/4 v5, 0x1
	 /* .line 97 */
	 /* .local v5, "allowNotification":Z */
	 int v6 = 0; // const/4 v6, 0x0
	 /* .line 98 */
	 /* .local v6, "hasException":Z */
	 /* packed-switch v2, :pswitch_data_0 */
	 /* .line 108 */
	 int v5 = 1; // const/4 v5, 0x1
	 /* .line 109 */
	 int v4 = 1; // const/4 v4, 0x1
	 /* .line 100 */
	 /* :pswitch_0 */
	 int v4 = 0; // const/4 v4, 0x0
	 /* .line 101 */
	 int v5 = 0; // const/4 v5, 0x0
	 /* .line 102 */
	 /* iget-boolean v7, v3, Landroid/service/notification/ZenModeConfig;->allowCalls:Z */
	 /* if-nez v7, :cond_2 */
	 /* iget-boolean v7, v3, Landroid/service/notification/ZenModeConfig;->allowRepeatCallers:Z */
	 if ( v7 != null) { // if-eqz v7, :cond_1
	 } // :cond_1
	 int v7 = 0; // const/4 v7, 0x0
} // :cond_2
} // :goto_0
int v7 = 1; // const/4 v7, 0x1
} // :goto_1
/* move v6, v7 */
/* .line 104 */
/* nop */
/* .line 112 */
} // :goto_2
/* nop */
/* .line 116 */
if ( v6 != null) { // if-eqz v6, :cond_3
/* move-object v7, v0 */
} // :cond_3
/* move-object v7, v1 */
/* .line 112 */
} // :goto_3
int v8 = 6; // const/4 v8, 0x6
com.android.server.notification.ZenModeHelperInjector .applyRestriction ( v4,v8,p1,v7 );
/* .line 117 */
/* nop */
/* .line 121 */
if ( v6 != null) { // if-eqz v6, :cond_4
/* move-object v7, v0 */
} // :cond_4
/* move-object v7, v1 */
/* .line 117 */
} // :goto_4
int v8 = 5; // const/4 v8, 0x5
com.android.server.notification.ZenModeHelperInjector .applyRestriction ( v5,v8,p1,v7 );
/* .line 122 */
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private static void applyRestriction ( Boolean p0, Integer p1, android.app.AppOpsManager p2, java.lang.String[] p3 ) {
/* .locals 2 */
/* .param p0, "allow" # Z */
/* .param p1, "usage" # I */
/* .param p2, "appOps" # Landroid/app/AppOpsManager; */
/* .param p3, "exception" # [Ljava/lang/String; */
/* .line 125 */
/* nop */
/* .line 126 */
/* nop */
/* .line 125 */
/* xor-int/lit8 v0, p0, 0x1 */
/* const/16 v1, 0x1c */
(( android.app.AppOpsManager ) p2 ).setRestriction ( v1, p1, v0, p3 ); // invoke-virtual {p2, v1, p1, v0, p3}, Landroid/app/AppOpsManager;->setRestriction(III[Ljava/lang/String;)V
/* .line 128 */
/* nop */
/* .line 129 */
/* nop */
/* .line 128 */
/* xor-int/lit8 v0, p0, 0x1 */
int v1 = 3; // const/4 v1, 0x3
(( android.app.AppOpsManager ) p2 ).setRestriction ( v1, p1, v0, p3 ); // invoke-virtual {p2, v1, p1, v0, p3}, Landroid/app/AppOpsManager;->setRestriction(III[Ljava/lang/String;)V
/* .line 131 */
return;
} // .end method
static Integer applyRingerModeToZen ( com.android.server.notification.ZenModeHelper p0, android.content.Context p1, Integer p2 ) {
/* .locals 4 */
/* .param p0, "helper" # Lcom/android/server/notification/ZenModeHelper; */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "ringerMode" # I */
/* .line 26 */
v0 = (( com.android.server.notification.ZenModeHelper ) p0 ).getZenMode ( ); // invoke-virtual {p0}, Lcom/android/server/notification/ZenModeHelper;->getZenMode()I
/* .line 27 */
/* .local v0, "zenMode":I */
int v1 = -1; // const/4 v1, -0x1
/* .line 28 */
/* .local v1, "newZen":I */
/* packed-switch p2, :pswitch_data_0 */
/* .line 42 */
/* .line 36 */
/* :pswitch_0 */
if ( v0 != null) { // if-eqz v0, :cond_1
int v2 = 1; // const/4 v2, 0x1
/* if-eq v0, v2, :cond_1 */
/* .line 37 */
v3 = android.provider.MiuiSettings$AntiSpam .isQuietModeEnable ( p1 );
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 38 */
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
/* move v1, v2 */
/* .line 30 */
/* :pswitch_1 */
int v2 = 3; // const/4 v2, 0x3
/* if-eq v0, v2, :cond_1 */
int v2 = 2; // const/4 v2, 0x2
/* if-eq v0, v2, :cond_1 */
/* .line 31 */
int v1 = 3; // const/4 v1, 0x3
/* .line 44 */
} // :cond_1
} // :goto_1
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
/* :pswitch_0 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
static Integer applyRingerModeToZen ( com.android.server.notification.ZenModeHelper p0, android.content.Context p1, Integer p2, Integer p3, Integer p4 ) {
/* .locals 4 */
/* .param p0, "helper" # Lcom/android/server/notification/ZenModeHelper; */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "ringerModeOld" # I */
/* .param p3, "ringerModeNew" # I */
/* .param p4, "newZen" # I */
/* .line 49 */
/* sget-boolean v0, Landroid/provider/MiuiSettings$SilenceMode;->isSupported:Z */
/* if-nez v0, :cond_0 */
/* .line 50 */
v0 = com.android.server.notification.ZenModeHelperInjector .applyRingerModeToZen ( p0,p1,p3 );
/* .line 53 */
} // :cond_0
/* if-eq p3, p2, :cond_1 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
/* .line 54 */
/* .local v0, "isChange":Z */
} // :goto_0
v1 = (( com.android.server.notification.ZenModeHelper ) p0 ).getZenMode ( ); // invoke-virtual {p0}, Lcom/android/server/notification/ZenModeHelper;->getZenMode()I
/* .line 56 */
/* .local v1, "zenMode":I */
int v2 = 4; // const/4 v2, 0x4
/* packed-switch p3, :pswitch_data_0 */
/* .line 78 */
/* .line 73 */
/* :pswitch_0 */
if ( v0 != null) { // if-eqz v0, :cond_4
/* if-ne v1, v2, :cond_4 */
/* .line 74 */
int p4 = 0; // const/4 p4, 0x0
/* .line 59 */
/* :pswitch_1 */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 60 */
/* if-nez v1, :cond_2 */
/* .line 61 */
int p4 = 4; // const/4 p4, 0x4
/* .line 62 */
} // :cond_2
/* if-ne v2, v1, :cond_4 */
/* .line 63 */
int p4 = -1; // const/4 p4, -0x1
/* .line 66 */
} // :cond_3
/* if-nez p4, :cond_4 */
/* .line 67 */
/* move p4, v1 */
/* .line 68 */
final String v2 = "ZenModeHelperInjector"; // const-string v2, "ZenModeHelperInjector"
final String v3 = "RINGER MODE is not Change"; // const-string v3, "RINGER MODE is not Change"
android.util.Log .d ( v2,v3 );
/* .line 81 */
} // :cond_4
} // :goto_1
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
static Integer getOutRingerMode ( Integer p0, Integer p1, Integer p2, Integer p3 ) {
/* .locals 1 */
/* .param p0, "newZen" # I */
/* .param p1, "curZen" # I */
/* .param p2, "ringerModeNew" # I */
/* .param p3, "out" # I */
/* .line 134 */
/* sget-boolean v0, Landroid/provider/MiuiSettings$SilenceMode;->isSupported:Z */
/* if-nez v0, :cond_0 */
/* .line 135 */
} // :cond_0
int v0 = -1; // const/4 v0, -0x1
/* if-ne p0, v0, :cond_1 */
/* move v0, p1 */
} // :cond_1
/* move v0, p0 */
} // :goto_0
/* move p0, v0 */
/* .line 136 */
int v0 = 1; // const/4 v0, 0x1
/* if-ne p0, v0, :cond_2 */
/* move v0, p3 */
} // :cond_2
/* move v0, p2 */
} // :goto_1
} // .end method
static Integer miuiComputeZenMode ( java.lang.String p0, android.service.notification.ZenModeConfig p1 ) {
/* .locals 5 */
/* .param p0, "reason" # Ljava/lang/String; */
/* .param p1, "config" # Landroid/service/notification/ZenModeConfig; */
/* .line 153 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_0 */
/* .line 154 */
} // :cond_0
v1 = this.manualRule;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 155 */
final String v1 = "conditionChanged"; // const-string v1, "conditionChanged"
v1 = (( java.lang.String ) v1 ).equals ( p0 ); // invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_1 */
/* .line 156 */
/* const-string/jumbo v1, "setNotificationPolicy" */
v1 = (( java.lang.String ) v1 ).equals ( p0 ); // invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_1 */
/* .line 157 */
/* const-string/jumbo v1, "updateAutomaticZenRule" */
v1 = (( java.lang.String ) v1 ).equals ( p0 ); // invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_1 */
/* .line 158 */
final String v1 = "onSystemReady"; // const-string v1, "onSystemReady"
v1 = (( java.lang.String ) v1 ).equals ( p0 ); // invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_1 */
/* .line 159 */
final String v1 = "readXml"; // const-string v1, "readXml"
v1 = (( java.lang.String ) v1 ).equals ( p0 ); // invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_1 */
/* .line 160 */
final String v1 = "init"; // const-string v1, "init"
v1 = (( java.lang.String ) v1 ).equals ( p0 ); // invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_1 */
/* .line 161 */
/* const-string/jumbo v1, "zmc.onServiceAdded" */
v1 = (( java.lang.String ) v1 ).equals ( p0 ); // invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_1 */
/* .line 162 */
final String v1 = "cleanUpZenRules"; // const-string v1, "cleanUpZenRules"
v1 = (( java.lang.String ) v1 ).equals ( p0 ); // invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_1 */
/* .line 163 */
v0 = this.manualRule;
/* iget v0, v0, Landroid/service/notification/ZenModeConfig$ZenRule;->zenMode:I */
/* .line 165 */
} // :cond_1
v1 = this.manualRule;
/* if-nez v1, :cond_2 */
} // :cond_2
v0 = this.manualRule;
/* iget v0, v0, Landroid/service/notification/ZenModeConfig$ZenRule;->zenMode:I */
/* .line 166 */
/* .local v0, "zen":I */
} // :goto_0
v1 = this.automaticRules;
(( android.util.ArrayMap ) v1 ).values ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;
v2 = } // :goto_1
if ( v2 != null) { // if-eqz v2, :cond_5
/* check-cast v2, Landroid/service/notification/ZenModeConfig$ZenRule; */
/* .line 167 */
/* .local v2, "automaticRule":Landroid/service/notification/ZenModeConfig$ZenRule; */
v3 = (( android.service.notification.ZenModeConfig$ZenRule ) v2 ).isAutomaticActive ( ); // invoke-virtual {v2}, Landroid/service/notification/ZenModeConfig$ZenRule;->isAutomaticActive()Z
if ( v3 != null) { // if-eqz v3, :cond_4
/* iget v3, v2, Landroid/service/notification/ZenModeConfig$ZenRule;->zenMode:I */
v3 = com.android.server.notification.ZenModeHelperInjector .zenSeverity ( v3 );
v4 = com.android.server.notification.ZenModeHelperInjector .zenSeverity ( v0 );
/* if-gt v3, v4, :cond_3 */
/* iget v3, v2, Landroid/service/notification/ZenModeConfig$ZenRule;->zenMode:I */
int v4 = 4; // const/4 v4, 0x4
/* if-ne v3, v4, :cond_4 */
/* if-nez v0, :cond_4 */
/* .line 169 */
} // :cond_3
/* iget v0, v2, Landroid/service/notification/ZenModeConfig$ZenRule;->zenMode:I */
/* .line 171 */
} // .end local v2 # "automaticRule":Landroid/service/notification/ZenModeConfig$ZenRule;
} // :cond_4
/* .line 172 */
} // :cond_5
} // .end method
private static Integer zenSeverity ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "zen" # I */
/* .line 140 */
/* packed-switch p0, :pswitch_data_0 */
/* .line 148 */
int v0 = 0; // const/4 v0, 0x0
/* .line 144 */
/* :pswitch_0 */
int v0 = 2; // const/4 v0, 0x2
/* .line 146 */
/* :pswitch_1 */
int v0 = 3; // const/4 v0, 0x3
/* .line 142 */
/* :pswitch_2 */
int v0 = 1; // const/4 v0, 0x1
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
