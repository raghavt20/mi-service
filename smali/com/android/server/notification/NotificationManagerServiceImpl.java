public class com.android.server.notification.NotificationManagerServiceImpl extends com.android.server.notification.NotificationManagerServiceStub {
	 /* .source "NotificationManagerServiceImpl.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.notification.NotificationManagerServiceStub$$" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lcom/android/server/notification/NotificationManagerServiceImpl$NRStatusImpl;, */
/* Lcom/android/server/notification/NotificationManagerServiceImpl$NMSVersionImpl; */
/* } */
} // .end annotation
/* # static fields */
private static final java.lang.String BREATHING_LIGHT;
protected static final java.lang.String ENABLED_SERVICES_SEPARATOR;
private static final Integer INVALID_UID;
private static final java.lang.String KEY_MEDIA_NOTIFICATION_CLOUD;
private static final java.util.List MIUI_SYSTEM_APPS_LIST;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.lang.String MODULE_MEDIA_NOTIFICATION_CLOUD_LIST;
private static final java.util.List OTHER_APPS_LIST;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.lang.String SPLIT_CHAR;
private static final Integer SYSTEM_APP_MASK;
public static final java.lang.String TAG;
private static final java.lang.String XMSF_CHANNEL_ID_PREFIX;
private static final java.lang.String XMSF_FAKE_CONDITION_PROVIDER_PATH;
protected static final java.lang.String XMSF_PACKAGE_NAME;
private static java.util.List allowMediaNotificationCloudDataList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.util.Set sAllowToastSet;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private final java.lang.String PRIVACY_INPUT_MODE_PKG_NAME;
private java.lang.String allowMediaNotificationCloudData;
private java.lang.String allowNotificationAccessList;
private java.lang.String interceptChannelId;
private java.lang.String interceptListener;
private android.content.Context mContext;
private com.android.server.notification.NotificationManagerService mNMS;
private java.lang.String mPrivacyInputModePkgName;
/* # direct methods */
static android.content.Context -$$Nest$fgetmContext ( com.android.server.notification.NotificationManagerServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static void -$$Nest$fputmPrivacyInputModePkgName ( com.android.server.notification.NotificationManagerServiceImpl p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.mPrivacyInputModePkgName = p1;
return;
} // .end method
static void -$$Nest$mupdateCloudData ( com.android.server.notification.NotificationManagerServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/notification/NotificationManagerServiceImpl;->updateCloudData()V */
return;
} // .end method
static com.android.server.notification.NotificationManagerServiceImpl ( ) {
/* .locals 2 */
/* .line 105 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 380 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 381 */
final String v1 = "com.lbe.security.miui"; // const-string v1, "com.lbe.security.miui"
/* .line 665 */
v0 = miui.content.pm.ExtraPackageManager.MIUI_SYSTEM_APPS;
java.util.Arrays .asList ( v0 );
/* .line 667 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 670 */
final String v1 = "com.xiaomi.wearable"; // const-string v1, "com.xiaomi.wearable"
/* .line 671 */
final String v1 = "com.xiaomi.hm.health"; // const-string v1, "com.xiaomi.hm.health"
/* .line 672 */
final String v1 = "com.mi.health"; // const-string v1, "com.mi.health"
/* .line 673 */
return;
} // .end method
public com.android.server.notification.NotificationManagerServiceImpl ( ) {
/* .locals 1 */
/* .line 89 */
/* invoke-direct {p0}, Lcom/android/server/notification/NotificationManagerServiceStub;-><init>()V */
/* .line 96 */
final String v0 = "miui_privacy_input_pkg_name"; // const-string v0, "miui_privacy_input_pkg_name"
this.PRIVACY_INPUT_MODE_PKG_NAME = v0;
/* .line 97 */
int v0 = 0; // const/4 v0, 0x0
this.mPrivacyInputModePkgName = v0;
return;
} // .end method
private void checkAllowToDeleteChannel ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "channelId" # Ljava/lang/String; */
/* .line 579 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/notification/NotificationManagerServiceImpl;->isCallerXmsfOrSystem(Ljava/lang/String;)Z */
/* if-nez v0, :cond_1 */
v0 = /* invoke-direct {p0, p2}, Lcom/android/server/notification/NotificationManagerServiceImpl;->isXmsfChannelId(Ljava/lang/String;)Z */
/* if-nez v0, :cond_0 */
/* .line 580 */
} // :cond_0
/* new-instance v0, Ljava/lang/SecurityException; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Pkg "; // const-string v2, "Pkg "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " cannot delete channel "; // const-string v2, " cannot delete channel "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " that starts with \'mipush|\'"; // const-string v2, " that starts with \'mipush|\'"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
/* .line 583 */
} // :cond_1
} // :goto_0
return;
} // .end method
private Boolean checkAppSystemOrNot ( java.lang.String p0 ) {
/* .locals 8 */
/* .param p1, "monitorPkg" # Ljava/lang/String; */
/* .line 752 */
int v0 = 0; // const/4 v0, 0x0
/* .line 754 */
/* .local v0, "isSystem":Z */
try { // :try_start_0
/* const-class v1, Landroid/content/pm/PackageManagerInternal; */
com.android.server.LocalServices .getService ( v1 );
/* move-object v2, v1 */
/* check-cast v2, Landroid/content/pm/PackageManagerInternal; */
/* const-wide/16 v4, 0x0 */
/* const/16 v6, 0x3e8 */
int v7 = 0; // const/4 v7, 0x0
/* .line 755 */
/* move-object v3, p1 */
/* invoke-virtual/range {v2 ..v7}, Landroid/content/pm/PackageManagerInternal;->getApplicationInfo(Ljava/lang/String;JII)Landroid/content/pm/ApplicationInfo; */
/* .line 756 */
/* .local v1, "ai":Landroid/content/pm/ApplicationInfo; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 757 */
v2 = (( android.content.pm.ApplicationInfo ) v1 ).isSystemApp ( ); // invoke-virtual {v1}, Landroid/content/pm/ApplicationInfo;->isSystemApp()Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* move v0, v2 */
/* .line 761 */
} // .end local v1 # "ai":Landroid/content/pm/ApplicationInfo;
} // :cond_0
/* .line 759 */
/* :catch_0 */
/* move-exception v1 */
/* .line 760 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v2 = "NotificationManagerServiceImpl"; // const-string v2, "NotificationManagerServiceImpl"
final String v3 = "checkAppSystemOrNot : "; // const-string v3, "checkAppSystemOrNot : "
android.util.Slog .e ( v2,v3,v1 );
/* .line 762 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
} // .end method
private Boolean checkCallerIsXmsf ( java.lang.String p0, java.lang.String p1, Integer p2, Integer p3 ) {
/* .locals 2 */
/* .param p1, "callingPkg" # Ljava/lang/String; */
/* .param p2, "targetPkg" # Ljava/lang/String; */
/* .param p3, "callingUid" # I */
/* .param p4, "userId" # I */
/* .line 464 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/notification/NotificationManagerServiceImpl;->isXmsf(Ljava/lang/String;)Z */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = /* invoke-direct {p0, p2}, Lcom/android/server/notification/NotificationManagerServiceImpl;->isXmsf(Ljava/lang/String;)Z */
/* if-nez v0, :cond_2 */
/* .line 468 */
v0 = /* invoke-direct {p0}, Lcom/android/server/notification/NotificationManagerServiceImpl;->isCallerSystem()Z */
/* if-nez v0, :cond_0 */
v0 = /* invoke-direct {p0, p3, p4}, Lcom/android/server/notification/NotificationManagerServiceImpl;->checkCallerIsXmsfInternal(II)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
} // :cond_0
int v1 = 1; // const/4 v1, 0x1
} // :cond_1
/* .line 470 */
} // :cond_2
} // .end method
private Boolean checkCallerIsXmsfInternal ( Integer p0, Integer p1 ) {
/* .locals 7 */
/* .param p1, "callingUid" # I */
/* .param p2, "userId" # I */
/* .line 475 */
try { // :try_start_0
/* const-class v0, Landroid/content/pm/PackageManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* move-object v1, v0 */
/* check-cast v1, Landroid/content/pm/PackageManagerInternal; */
final String v2 = "com.xiaomi.xmsf"; // const-string v2, "com.xiaomi.xmsf"
/* const-wide/16 v3, 0x0 */
/* .line 476 */
/* move v5, p1 */
/* move v6, p2 */
/* invoke-virtual/range {v1 ..v6}, Landroid/content/pm/PackageManagerInternal;->getApplicationInfo(Ljava/lang/String;JII)Landroid/content/pm/ApplicationInfo; */
/* .line 477 */
/* .local v0, "ai":Landroid/content/pm/ApplicationInfo; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget v1, v0, Landroid/content/pm/ApplicationInfo;->uid:I */
v1 = android.os.UserHandle .isSameApp ( v1,p1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* iget v1, v0, Landroid/content/pm/ApplicationInfo;->flags:I */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* and-int/lit16 v1, v1, 0x81 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 479 */
int v1 = 1; // const/4 v1, 0x1
/* .line 483 */
} // .end local v0 # "ai":Landroid/content/pm/ApplicationInfo;
} // :cond_0
/* .line 481 */
/* :catch_0 */
/* move-exception v0 */
/* .line 484 */
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean checkInterceptListenerForMIUIInternal ( android.service.notification.StatusBarNotification p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "sbn" # Landroid/service/notification/StatusBarNotification; */
/* .param p2, "listener" # Ljava/lang/String; */
/* .line 716 */
v0 = /* invoke-direct {p0, p2}, Lcom/android/server/notification/NotificationManagerServiceImpl;->checkAppSystemOrNot(Ljava/lang/String;)Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
v0 = com.android.server.notification.NotificationManagerServiceImpl.MIUI_SYSTEM_APPS_LIST;
v0 = /* .line 717 */
/* if-nez v0, :cond_0 */
v0 = com.android.server.notification.NotificationManagerServiceImpl.OTHER_APPS_LIST;
v0 = /* .line 718 */
/* if-nez v0, :cond_0 */
/* .line 719 */
int v0 = 0; // const/4 v0, 0x0
v0 = miui.content.pm.PreloadedAppPolicy .isProtectedDataApp ( v0,p2,v1 );
/* if-nez v0, :cond_0 */
/* .line 720 */
(( android.service.notification.StatusBarNotification ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;
v2 = (( android.service.notification.StatusBarNotification ) p1 ).getUid ( ); // invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getUid()I
v0 = /* invoke-direct {p0, v0, v2}, Lcom/android/server/notification/NotificationManagerServiceImpl;->checkNotificationMasked(Ljava/lang/String;I)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
/* nop */
} // :goto_0
/* move v0, v1 */
/* .line 721 */
/* .local v0, "intercept":Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 722 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "checkInterceptListenerForMIUIInternal pkg:"; // const-string v2, "checkInterceptListenerForMIUIInternal pkg:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.service.notification.StatusBarNotification ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "NotificationManagerServiceImpl"; // const-string v2, "NotificationManagerServiceImpl"
android.util.Slog .w ( v2,v1 );
/* .line 724 */
} // :cond_1
} // .end method
private Boolean checkInterceptListenerForXiaomiInternal ( android.service.notification.StatusBarNotification p0, android.content.ComponentName p1 ) {
/* .locals 4 */
/* .param p1, "sbn" # Landroid/service/notification/StatusBarNotification; */
/* .param p2, "component" # Landroid/content/ComponentName; */
/* .line 697 */
int v0 = 0; // const/4 v0, 0x0
/* .line 698 */
/* .local v0, "intercept":Z */
v1 = this.interceptListener;
(( android.content.ComponentName ) p2 ).flattenToString ( ); // invoke-virtual {p2}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;
v1 = com.android.internal.util.ArrayUtils .contains ( v1,v2 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 699 */
v1 = this.interceptChannelId;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 700 */
(( android.service.notification.StatusBarNotification ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = ":"; // const-string v3, ":"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.service.notification.StatusBarNotification ) p1 ).getNotification ( ); // invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;
(( android.app.Notification ) v3 ).getChannelId ( ); // invoke-virtual {v3}, Landroid/app/Notification;->getChannelId()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 699 */
v1 = com.android.internal.util.ArrayUtils .contains ( v1,v2 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 701 */
int v0 = 1; // const/4 v0, 0x1
/* .line 702 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "checkInterceptListenerForXiaomiInternal pkg:"; // const-string v2, "checkInterceptListenerForXiaomiInternal pkg:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.service.notification.StatusBarNotification ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "listener:"; // const-string v2, "listener:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 703 */
(( android.content.ComponentName ) p2 ).flattenToString ( ); // invoke-virtual {p2}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 702 */
final String v2 = "NotificationManagerServiceImpl"; // const-string v2, "NotificationManagerServiceImpl"
android.util.Slog .w ( v2,v1 );
/* .line 706 */
} // :cond_0
} // .end method
private Boolean checkNotificationMasked ( java.lang.String p0, Integer p1 ) {
/* .locals 5 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .line 735 */
/* const-string/jumbo v0, "security" */
android.os.ServiceManager .getService ( v0 );
/* .line 736 */
/* .local v0, "b":Landroid/os/IBinder; */
int v1 = 0; // const/4 v1, 0x0
/* .line 737 */
/* .local v1, "result":Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 739 */
try { // :try_start_0
v2 = android.os.UserHandle .getUserId ( p2 );
/* .line 740 */
/* .local v2, "userId":I */
miui.security.ISecurityManager$Stub .asInterface ( v0 );
/* .line 741 */
v4 = /* .local v3, "service":Lmiui/security/ISecurityManager; */
if ( v4 != null) { // if-eqz v4, :cond_0
v4 = /* .line 742 */
if ( v4 != null) { // if-eqz v4, :cond_0
v4 = /* .line 743 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
if ( v4 != null) { // if-eqz v4, :cond_0
int v4 = 1; // const/4 v4, 0x1
} // :cond_0
int v4 = 0; // const/4 v4, 0x0
} // :goto_0
/* move v1, v4 */
/* .line 746 */
} // .end local v2 # "userId":I
} // .end local v3 # "service":Lmiui/security/ISecurityManager;
/* .line 744 */
/* :catch_0 */
/* move-exception v2 */
/* .line 745 */
/* .local v2, "e":Ljava/lang/Exception; */
final String v3 = "NotificationManagerServiceImpl"; // const-string v3, "NotificationManagerServiceImpl"
final String v4 = "check notification masked error: "; // const-string v4, "check notification masked error: "
android.util.Slog .e ( v3,v4,v2 );
/* .line 748 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :cond_1
} // :goto_1
} // .end method
public static java.util.List getMediaNotificationDataCloudConversion ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p0, "allowMediaNotificationCloudData" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* ")", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1000 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 1003 */
/* .local v0, "mediationNotificationDataFromCloud":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
try { // :try_start_0
v1 = android.text.TextUtils .isEmpty ( p0 );
/* if-nez v1, :cond_1 */
/* .line 1004 */
/* new-instance v1, Lorg/json/JSONArray; */
/* invoke-direct {v1, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V */
/* .line 1005 */
/* .local v1, "dataConversion":Lorg/json/JSONArray; */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
v3 = (( org.json.JSONArray ) v1 ).length ( ); // invoke-virtual {v1}, Lorg/json/JSONArray;->length()I
/* if-ge v2, v3, :cond_0 */
/* .line 1006 */
(( org.json.JSONArray ) v1 ).getString ( v2 ); // invoke-virtual {v1, v2}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;
/* .line 1005 */
/* add-int/lit8 v2, v2, 0x1 */
} // .end local v2 # "i":I
} // :cond_0
/* .line 1009 */
} // .end local v1 # "dataConversion":Lorg/json/JSONArray;
} // :cond_1
v1 = com.android.server.notification.NotificationManagerServiceImpl.allowMediaNotificationCloudDataList;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1013 */
} // :goto_1
/* .line 1011 */
/* :catch_0 */
/* move-exception v1 */
/* .line 1012 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v2 = "NotificationManagerServiceImpl"; // const-string v2, "NotificationManagerServiceImpl"
final String v3 = "Exception when get getMediaNotificationDataCloudConversion :"; // const-string v3, "Exception when get getMediaNotificationDataCloudConversion :"
android.util.Log .e ( v2,v3,v1 );
/* .line 1014 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_2
} // .end method
private Integer getUidByPkg ( java.lang.String p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 586 */
int v0 = -1; // const/4 v0, -0x1
/* .line 588 */
/* .local v0, "uid":I */
try { // :try_start_0
v1 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v1, :cond_0 */
/* .line 589 */
/* const-class v1, Landroid/content/pm/PackageManagerInternal; */
com.android.server.LocalServices .getService ( v1 );
/* check-cast v1, Landroid/content/pm/PackageManagerInternal; */
/* .line 590 */
/* const-wide/16 v2, 0x0 */
v1 = (( android.content.pm.PackageManagerInternal ) v1 ).getPackageUid ( p1, v2, v3, p2 ); // invoke-virtual {v1, p1, v2, v3, p2}, Landroid/content/pm/PackageManagerInternal;->getPackageUid(Ljava/lang/String;JI)I
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* move v0, v1 */
/* .line 594 */
} // :cond_0
/* .line 592 */
/* :catch_0 */
/* move-exception v1 */
/* .line 595 */
} // :goto_0
} // .end method
private Boolean hasProgress ( android.app.Notification p0 ) {
/* .locals 4 */
/* .param p1, "n" # Landroid/app/Notification; */
/* .line 617 */
v0 = this.extras;
final String v1 = "android.progress"; // const-string v1, "android.progress"
v0 = (( android.os.Bundle ) v0 ).containsKey ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z
int v2 = 0; // const/4 v2, 0x0
if ( v0 != null) { // if-eqz v0, :cond_3
v0 = this.extras;
/* .line 618 */
final String v3 = "android.progressMax"; // const-string v3, "android.progressMax"
v0 = (( android.os.Bundle ) v0 ).containsKey ( v3 ); // invoke-virtual {v0, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z
/* if-nez v0, :cond_0 */
/* .line 622 */
} // :cond_0
v0 = this.extras;
v0 = (( android.os.Bundle ) v0 ).getInt ( v3 ); // invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I
/* if-nez v0, :cond_1 */
/* .line 623 */
/* .line 625 */
} // :cond_1
v0 = this.extras;
v0 = (( android.os.Bundle ) v0 ).getInt ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I
v1 = this.extras;
v1 = (( android.os.Bundle ) v1 ).getInt ( v3 ); // invoke-virtual {v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I
/* if-eq v0, v1, :cond_2 */
int v2 = 1; // const/4 v2, 0x1
} // :cond_2
/* .line 619 */
} // :cond_3
} // :goto_0
} // .end method
private Boolean isAudioCanPlay ( android.media.AudioManager p0 ) {
/* .locals 2 */
/* .param p1, "audioManager" # Landroid/media/AudioManager; */
/* .line 259 */
v0 = (( android.media.AudioManager ) p1 ).getRingerModeInternal ( ); // invoke-virtual {p1}, Landroid/media/AudioManager;->getRingerModeInternal()I
int v1 = 1; // const/4 v1, 0x1
/* if-eq v0, v1, :cond_0 */
/* .line 260 */
v0 = (( android.media.AudioManager ) p1 ).getRingerModeInternal ( ); // invoke-virtual {p1}, Landroid/media/AudioManager;->getRingerModeInternal()I
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 259 */
} // :goto_0
} // .end method
private Boolean isCallerAndroid ( java.lang.String p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "callingPkg" # Ljava/lang/String; */
/* .param p2, "callingUid" # I */
/* .line 494 */
v0 = /* invoke-direct {p0, p2}, Lcom/android/server/notification/NotificationManagerServiceImpl;->isUidSystemOrPhone(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 495 */
final String v0 = "android"; // const-string v0, "android"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 494 */
} // :goto_0
} // .end method
private Boolean isCallerSecurityCenter ( java.lang.String p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "callingPkg" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .line 502 */
v0 = android.os.UserHandle .getAppId ( p2 );
/* .line 503 */
/* .local v0, "appid":I */
/* const/16 v1, 0x3e8 */
/* if-ne v0, v1, :cond_0 */
/* .line 504 */
final String v1 = "com.miui.securitycenter"; // const-string v1, "com.miui.securitycenter"
v1 = (( java.lang.String ) v1 ).equals ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 503 */
} // :goto_0
} // .end method
private Boolean isCallerSystem ( ) {
/* .locals 2 */
/* .line 443 */
v0 = android.os.Binder .getCallingUid ( );
v0 = android.os.UserHandle .getAppId ( v0 );
/* .line 444 */
/* .local v0, "appId":I */
/* const/16 v1, 0x3e8 */
/* if-ne v0, v1, :cond_0 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
} // .end method
private Boolean isCallerXmsfOrSystem ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "callingPkg" # Ljava/lang/String; */
/* .line 435 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/notification/NotificationManagerServiceImpl;->isXmsf(Ljava/lang/String;)Z */
/* if-nez v0, :cond_1 */
v0 = /* invoke-direct {p0}, Lcom/android/server/notification/NotificationManagerServiceImpl;->isCallerSystem()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
private Boolean isCallingSystem ( Integer p0, Integer p1, java.lang.String p2 ) {
/* .locals 6 */
/* .param p1, "userId" # I */
/* .param p2, "uid" # I */
/* .param p3, "callingPkg" # Ljava/lang/String; */
/* .line 630 */
final String v0 = "NotificationManagerServiceImpl"; // const-string v0, "NotificationManagerServiceImpl"
v1 = android.os.UserHandle .getAppId ( p2 );
/* .line 631 */
/* .local v1, "appid":I */
/* const/16 v2, 0x3e8 */
/* if-eq v1, v2, :cond_2 */
/* const/16 v2, 0x3e9 */
/* if-eq v1, v2, :cond_2 */
if ( p2 != null) { // if-eqz p2, :cond_2
/* const/16 v2, 0x7d0 */
/* if-ne p2, v2, :cond_0 */
/* .line 637 */
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
try { // :try_start_0
android.app.AppGlobals .getPackageManager ( );
/* const-wide/16 v4, 0x0 */
/* .line 638 */
/* .local v3, "ai":Landroid/content/pm/ApplicationInfo; */
/* if-nez v3, :cond_1 */
/* .line 639 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Unknown package "; // const-string v5, "Unknown package "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p3 ); // invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v4 );
/* .line 640 */
/* .line 642 */
} // :cond_1
v0 = (( android.content.pm.ApplicationInfo ) v3 ).isSystemApp ( ); // invoke-virtual {v3}, Landroid/content/pm/ApplicationInfo;->isSystemApp()Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 643 */
} // .end local v3 # "ai":Landroid/content/pm/ApplicationInfo;
/* :catch_0 */
/* move-exception v3 */
/* .line 644 */
/* .local v3, "e":Ljava/lang/Exception; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "isCallingSystem error: "; // const-string v5, "isCallingSystem error: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v4 );
/* .line 646 */
} // .end local v3 # "e":Ljava/lang/Exception;
/* .line 633 */
} // :cond_2
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
private Boolean isPackageDistractionRestrictionForUser ( java.lang.String p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .line 407 */
v0 = android.os.UserHandle .getUserId ( p2 );
/* .line 409 */
/* .local v0, "userId":I */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
/* const-class v2, Landroid/content/pm/PackageManagerInternal; */
com.android.server.LocalServices .getService ( v2 );
/* check-cast v2, Landroid/content/pm/PackageManagerInternal; */
/* .line 411 */
/* .local v2, "pmi":Landroid/content/pm/PackageManagerInternal; */
v3 = (( android.content.pm.PackageManagerInternal ) v2 ).getDistractingPackageRestrictions ( p1, v0 ); // invoke-virtual {v2, p1, v0}, Landroid/content/pm/PackageManagerInternal;->getDistractingPackageRestrictions(Ljava/lang/String;I)I
/* :try_end_0 */
/* .catch Ljava/lang/IllegalArgumentException; {:try_start_0 ..:try_end_0} :catch_0 */
/* and-int/lit8 v3, v3, 0x2 */
if ( v3 != null) { // if-eqz v3, :cond_0
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
/* .line 413 */
} // .end local v2 # "pmi":Landroid/content/pm/PackageManagerInternal;
/* :catch_0 */
/* move-exception v2 */
/* .line 415 */
/* .local v2, "ex":Ljava/lang/IllegalArgumentException; */
} // .end method
private Boolean isSilenceMode ( com.android.server.notification.NotificationRecord p0 ) {
/* .locals 5 */
/* .param p1, "record" # Lcom/android/server/notification/NotificationRecord; */
/* .line 264 */
(( com.android.server.notification.NotificationRecord ) p1 ).getSbn ( ); // invoke-virtual {p1}, Lcom/android/server/notification/NotificationRecord;->getSbn()Landroid/service/notification/StatusBarNotification;
(( android.service.notification.StatusBarNotification ) v0 ).getNotification ( ); // invoke-virtual {v0}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;
v0 = this.extraNotification;
/* .line 265 */
/* .local v0, "extraNotification":Landroid/app/MiuiNotification; */
/* nop */
/* .line 267 */
(( com.android.server.notification.NotificationRecord ) p1 ).getSbn ( ); // invoke-virtual {p1}, Lcom/android/server/notification/NotificationRecord;->getSbn()Landroid/service/notification/StatusBarNotification;
(( android.service.notification.StatusBarNotification ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;
/* .line 268 */
/* if-nez v0, :cond_0 */
int v2 = 0; // const/4 v2, 0x0
} // :cond_0
(( android.app.MiuiNotification ) v0 ).getTargetPkg ( ); // invoke-virtual {v0}, Landroid/app/MiuiNotification;->getTargetPkg()Ljava/lang/CharSequence;
/* .line 265 */
} // :goto_0
int v3 = 5; // const/4 v3, 0x5
int v4 = 0; // const/4 v4, 0x0
v1 = miui.util.QuietUtils .checkQuiet ( v3,v4,v1,v2 );
} // .end method
private Boolean isUidSystemOrPhone ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .line 447 */
v0 = android.os.UserHandle .getAppId ( p1 );
/* .line 448 */
/* .local v0, "appid":I */
/* const/16 v1, 0x3e8 */
/* if-eq v0, v1, :cond_1 */
/* const/16 v1, 0x3e9 */
/* if-eq v0, v1, :cond_1 */
/* if-nez p1, :cond_0 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :cond_1
} // :goto_0
int v1 = 1; // const/4 v1, 0x1
} // :goto_1
} // .end method
private Boolean isUpdatableFocusNotification ( android.content.Context p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "key" # Ljava/lang/String; */
/* .line 347 */
/* nop */
/* .line 348 */
try { // :try_start_0
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v1, "updatable_focus_notifs" */
android.provider.Settings$Secure .getString ( v0,v1 );
/* .line 349 */
/* .local v0, "settingsValue":Ljava/lang/String; */
/* new-instance v1, Lorg/json/JSONArray; */
/* invoke-direct {v1, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V */
/* .line 350 */
/* .local v1, "jsonArray":Lorg/json/JSONArray; */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
v3 = (( org.json.JSONArray ) v1 ).length ( ); // invoke-virtual {v1}, Lorg/json/JSONArray;->length()I
/* if-ge v2, v3, :cond_1 */
/* .line 351 */
final String v3 = ""; // const-string v3, ""
(( org.json.JSONArray ) v1 ).optString ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lorg/json/JSONArray;->optString(ILjava/lang/String;)Ljava/lang/String;
v3 = android.text.TextUtils .equals ( p2,v3 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 352 */
int v3 = 1; // const/4 v3, 0x1
/* .line 350 */
} // :cond_0
/* add-int/lit8 v2, v2, 0x1 */
/* .line 357 */
} // .end local v0 # "settingsValue":Ljava/lang/String;
} // .end local v1 # "jsonArray":Lorg/json/JSONArray;
} // .end local v2 # "i":I
} // :cond_1
/* .line 355 */
/* :catch_0 */
/* move-exception v0 */
/* .line 356 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "NotificationManagerServiceImpl"; // const-string v1, "NotificationManagerServiceImpl"
/* const-string/jumbo v2, "updatableFocus jsonArray error" */
android.util.Slog .i ( v1,v2,v0 );
/* .line 358 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean isXmsf ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .line 439 */
final String v0 = "com.xiaomi.xmsf"; // const-string v0, "com.xiaomi.xmsf"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
} // .end method
private Boolean isXmsfChannelId ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "channelId" # Ljava/lang/String; */
/* .line 453 */
v0 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v0, :cond_0 */
final String v0 = "mipush|"; // const-string v0, "mipush|"
v0 = (( java.lang.String ) p1 ).startsWith ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private void registerCloudDataResolver ( android.os.Handler p0 ) {
/* .locals 4 */
/* .param p1, "handler" # Landroid/os/Handler; */
/* .line 816 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 817 */
android.provider.MiuiSettings$SettingsCloudData .getCloudDataNotifyUri ( );
/* new-instance v2, Lcom/android/server/notification/NotificationManagerServiceImpl$2; */
/* invoke-direct {v2, p0, p1}, Lcom/android/server/notification/NotificationManagerServiceImpl$2;-><init>(Lcom/android/server/notification/NotificationManagerServiceImpl;Landroid/os/Handler;)V */
/* .line 816 */
int v3 = 0; // const/4 v3, 0x0
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2 ); // invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 825 */
return;
} // .end method
private void registerPrivacyInputMode ( android.os.Handler p0 ) {
/* .locals 4 */
/* .param p1, "handler" # Landroid/os/Handler; */
/* .line 802 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 803 */
final String v1 = "miui_privacy_input_pkg_name"; // const-string v1, "miui_privacy_input_pkg_name"
android.provider.Settings$Secure .getUriFor ( v1 );
/* new-instance v2, Lcom/android/server/notification/NotificationManagerServiceImpl$1; */
/* invoke-direct {v2, p0, p1}, Lcom/android/server/notification/NotificationManagerServiceImpl$1;-><init>(Lcom/android/server/notification/NotificationManagerServiceImpl;Landroid/os/Handler;)V */
/* .line 802 */
int v3 = 0; // const/4 v3, 0x0
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2 ); // invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 813 */
return;
} // .end method
private Boolean shouldSkipFrequentlyVib ( com.android.server.notification.NotificationRecord p0, com.android.server.notification.NotificationUsageStats$AggregatedStats p1 ) {
/* .locals 3 */
/* .param p1, "record" # Lcom/android/server/notification/NotificationRecord; */
/* .param p2, "mStats" # Lcom/android/server/notification/NotificationUsageStats$AggregatedStats; */
/* .line 657 */
v0 = this.vibRate;
/* check-cast v0, Lcom/android/server/notification/VibRateLimiter; */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v1 */
v0 = (( com.android.server.notification.VibRateLimiter ) v0 ).shouldRateLimitVib ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/notification/VibRateLimiter;->shouldRateLimitVib(J)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 658 */
/* iget v0, p2, Lcom/android/server/notification/NotificationUsageStats$AggregatedStats;->numVibViolations:I */
int v1 = 1; // const/4 v1, 0x1
/* add-int/2addr v0, v1 */
/* iput v0, p2, Lcom/android/server/notification/NotificationUsageStats$AggregatedStats;->numVibViolations:I */
/* .line 659 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Cancel the recent frequently vibration in 15s "; // const-string v2, "Cancel the recent frequently vibration in 15s "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.notification.NotificationRecord ) p1 ).getKey ( ); // invoke-virtual {p1}, Lcom/android/server/notification/NotificationRecord;->getKey()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "NotificationManagerServiceImpl"; // const-string v2, "NotificationManagerServiceImpl"
android.util.Slog .e ( v2,v0 );
/* .line 660 */
/* .line 662 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private void updateCloudData ( ) {
/* .locals 4 */
/* .line 991 */
v0 = this.mContext;
/* .line 992 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 991 */
final String v1 = "config_notification_medianotificationcontrol"; // const-string v1, "config_notification_medianotificationcontrol"
final String v2 = "blacklist"; // const-string v2, "blacklist"
final String v3 = ""; // const-string v3, ""
android.provider.MiuiSettings$SettingsCloudData .getCloudDataString ( v0,v1,v2,v3 );
/* .line 994 */
/* .local v0, "updateNotificationCloudData":Ljava/lang/String; */
/* nop */
/* .line 995 */
com.android.server.notification.NotificationManagerServiceImpl .getMediaNotificationDataCloudConversion ( v0 );
/* .line 996 */
return;
} // .end method
/* # virtual methods */
public android.os.VibrationEffect adjustVibration ( android.os.VibrationEffect p0 ) {
/* .locals 5 */
/* .param p1, "vibration" # Landroid/os/VibrationEffect; */
/* .line 284 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* instance-of v0, p1, Landroid/os/VibrationEffect$Composed; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 285 */
/* move-object v0, p1 */
/* check-cast v0, Landroid/os/VibrationEffect$Composed; */
/* .line 286 */
/* .local v0, "currentVibration":Landroid/os/VibrationEffect$Composed; */
(( android.os.VibrationEffect$Composed ) v0 ).getSegments ( ); // invoke-virtual {v0}, Landroid/os/VibrationEffect$Composed;->getSegments()Ljava/util/List;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 287 */
v1 = (( android.os.VibrationEffect$Composed ) v0 ).getSegments ( ); // invoke-virtual {v0}, Landroid/os/VibrationEffect$Composed;->getSegments()Ljava/util/List;
int v2 = 4; // const/4 v2, 0x4
/* if-le v1, v2, :cond_0 */
/* .line 288 */
/* new-instance v1, Landroid/os/VibrationEffect$Composed; */
/* .line 289 */
(( android.os.VibrationEffect$Composed ) v0 ).getSegments ( ); // invoke-virtual {v0}, Landroid/os/VibrationEffect$Composed;->getSegments()Ljava/util/List;
int v4 = 0; // const/4 v4, 0x0
v3 = (( android.os.VibrationEffect$Composed ) v0 ).getRepeatIndex ( ); // invoke-virtual {v0}, Landroid/os/VibrationEffect$Composed;->getRepeatIndex()I
/* invoke-direct {v1, v2, v3}, Landroid/os/VibrationEffect$Composed;-><init>(Ljava/util/List;I)V */
/* move-object p1, v1 */
/* .line 292 */
} // .end local v0 # "currentVibration":Landroid/os/VibrationEffect$Composed;
} // :cond_0
} // .end method
public void allowMiuiDefaultApprovedServices ( com.android.server.notification.ManagedServices p0, Boolean p1 ) {
/* .locals 11 */
/* .param p1, "mListeners" # Lcom/android/server/notification/ManagedServices; */
/* .param p2, "DBG" # Z */
/* .line 137 */
(( com.android.server.notification.NotificationManagerServiceImpl ) p0 ).getMiuiDefaultListerWithVersion ( ); // invoke-virtual {p0}, Lcom/android/server/notification/NotificationManagerServiceImpl;->getMiuiDefaultListerWithVersion()Landroid/util/Pair;
/* .line 138 */
/* .local v0, "listenerWithVersion":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/String;>;" */
final String v1 = "NotificationManagerServiceImpl"; // const-string v1, "NotificationManagerServiceImpl"
if ( p2 != null) { // if-eqz p2, :cond_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "allowDefaultApprovedServices: "; // const-string v3, "allowDefaultApprovedServices: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.first;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v3 = " second "; // const-string v3, " second "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.second;
/* check-cast v3, Ljava/lang/String; */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v2 );
/* .line 140 */
} // :cond_0
v2 = this.first;
/* check-cast v2, Ljava/lang/Integer; */
v2 = (( java.lang.Integer ) v2 ).intValue ( ); // invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
/* if-gez v2, :cond_1 */
return;
/* .line 141 */
} // :cond_1
v2 = v2 = this.mVersionStub;
/* .line 142 */
/* .local v2, "miuiVersion":I */
if ( p2 != null) { // if-eqz p2, :cond_2
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "allowDefaultApprovedServices: miuiVersion "; // const-string v4, "allowDefaultApprovedServices: miuiVersion "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v3 );
/* .line 143 */
} // :cond_2
v1 = this.first;
/* check-cast v1, Ljava/lang/Integer; */
v1 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
/* if-lt v2, v1, :cond_3 */
return;
/* .line 145 */
} // :cond_3
v1 = this.second;
/* check-cast v1, Ljava/lang/String; */
/* .line 146 */
/* .local v1, "defaultListenerAccess":Ljava/lang/String; */
/* new-instance v3, Landroid/util/ArraySet; */
/* invoke-direct {v3}, Landroid/util/ArraySet;-><init>()V */
/* .line 147 */
/* .local v3, "defaultListeners":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Landroid/content/ComponentName;>;" */
int v4 = 0; // const/4 v4, 0x0
if ( v1 != null) { // if-eqz v1, :cond_7
/* .line 148 */
/* nop */
/* .line 149 */
final String v5 = ":"; // const-string v5, ":"
(( java.lang.String ) v1 ).split ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 150 */
/* .local v5, "listeners":[Ljava/lang/String; */
int v6 = 0; // const/4 v6, 0x0
/* .local v6, "i":I */
} // :goto_0
/* array-length v7, v5 */
/* if-ge v6, v7, :cond_6 */
/* .line 151 */
/* aget-object v7, v5, v6 */
v7 = android.text.TextUtils .isEmpty ( v7 );
if ( v7 != null) { // if-eqz v7, :cond_4
/* .line 152 */
/* .line 154 */
} // :cond_4
/* aget-object v7, v5, v6 */
/* .line 155 */
/* const/high16 v8, 0xc0000 */
(( com.android.server.notification.ManagedServices ) p1 ).queryPackageForServices ( v7, v8, v4 ); // invoke-virtual {p1, v7, v8, v4}, Lcom/android/server/notification/ManagedServices;->queryPackageForServices(Ljava/lang/String;II)Landroid/util/ArraySet;
/* .line 158 */
/* .local v7, "approvedListeners":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Landroid/content/ComponentName;>;" */
int v8 = 0; // const/4 v8, 0x0
/* .local v8, "k":I */
} // :goto_1
v9 = (( android.util.ArraySet ) v7 ).size ( ); // invoke-virtual {v7}, Landroid/util/ArraySet;->size()I
/* if-ge v8, v9, :cond_5 */
/* .line 159 */
(( android.util.ArraySet ) v7 ).valueAt ( v8 ); // invoke-virtual {v7, v8}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;
/* check-cast v9, Landroid/content/ComponentName; */
/* .line 160 */
/* .local v9, "cn":Landroid/content/ComponentName; */
(( android.content.ComponentName ) v9 ).flattenToString ( ); // invoke-virtual {v9}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;
(( com.android.server.notification.ManagedServices ) p1 ).addDefaultComponentOrPackage ( v10 ); // invoke-virtual {p1, v10}, Lcom/android/server/notification/ManagedServices;->addDefaultComponentOrPackage(Ljava/lang/String;)V
/* .line 161 */
(( android.util.ArraySet ) v3 ).add ( v9 ); // invoke-virtual {v3, v9}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 158 */
} // .end local v9 # "cn":Landroid/content/ComponentName;
/* add-int/lit8 v8, v8, 0x1 */
/* .line 150 */
} // .end local v7 # "approvedListeners":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Landroid/content/ComponentName;>;"
} // .end local v8 # "k":I
} // :cond_5
} // :goto_2
/* add-int/lit8 v6, v6, 0x1 */
/* .line 164 */
} // .end local v6 # "i":I
} // :cond_6
v6 = this.mVersionStub;
v7 = this.first;
/* check-cast v7, Ljava/lang/Integer; */
v7 = (( java.lang.Integer ) v7 ).intValue ( ); // invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I
/* .line 167 */
} // .end local v5 # "listeners":[Ljava/lang/String;
} // :cond_7
int v5 = 0; // const/4 v5, 0x0
/* .local v5, "i":I */
} // :goto_3
v6 = (( android.util.ArraySet ) v3 ).size ( ); // invoke-virtual {v3}, Landroid/util/ArraySet;->size()I
/* if-ge v5, v6, :cond_8 */
/* .line 168 */
(( android.util.ArraySet ) v3 ).valueAt ( v5 ); // invoke-virtual {v3, v5}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;
/* check-cast v6, Landroid/content/ComponentName; */
/* .line 169 */
/* .local v6, "cn":Landroid/content/ComponentName; */
v7 = com.android.server.notification.NotificationManagerServiceProxy.allowNotificationListener;
v8 = this.mNMS;
java.lang.Integer .valueOf ( v4 );
/* filled-new-array {v9, v6}, [Ljava/lang/Object; */
(( com.xiaomi.reflect.RefMethod ) v7 ).invoke ( v8, v9 ); // invoke-virtual {v7, v8, v9}, Lcom/xiaomi/reflect/RefMethod;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* .line 167 */
} // .end local v6 # "cn":Landroid/content/ComponentName;
/* add-int/lit8 v5, v5, 0x1 */
/* .line 171 */
} // .end local v5 # "i":I
} // :cond_8
return;
} // .end method
public void buzzBeepBlinkForNotification ( java.lang.String p0, Integer p1, java.lang.Object p2 ) {
/* .locals 3 */
/* .param p1, "key" # Ljava/lang/String; */
/* .param p2, "buzzBeepBlink" # I */
/* .param p3, "mNotificationLock" # Ljava/lang/Object; */
/* .line 205 */
v0 = com.android.server.notification.NotificationManagerServiceProxy.checkCallerIsSystem;
v1 = this.mNMS;
int v2 = 0; // const/4 v2, 0x0
/* new-array v2, v2, [Ljava/lang/Object; */
(( com.xiaomi.reflect.RefMethod ) v0 ).invoke ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/xiaomi/reflect/RefMethod;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* .line 206 */
/* monitor-enter p3 */
/* .line 207 */
try { // :try_start_0
v0 = this.mNMS;
v0 = this.mNotificationsByKey;
(( android.util.ArrayMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/notification/NotificationRecord; */
/* .line 208 */
/* .local v0, "record":Lcom/android/server/notification/NotificationRecord; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 209 */
v1 = this.mNRStatusStub;
/* .line 210 */
v1 = this.mNMS;
(( com.android.server.notification.NotificationManagerService ) v1 ).buzzBeepBlinkLocked ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/notification/NotificationManagerService;->buzzBeepBlinkLocked(Lcom/android/server/notification/NotificationRecord;)I
/* .line 212 */
} // .end local v0 # "record":Lcom/android/server/notification/NotificationRecord;
} // :cond_0
/* monitor-exit p3 */
/* .line 213 */
return;
/* .line 212 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p3 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v0 */
} // .end method
public void calculateAudiblyAlerted ( android.media.AudioManager p0, com.android.server.notification.NotificationRecord p1, com.android.server.notification.NotificationManagerService p2 ) {
/* .locals 10 */
/* .param p1, "audioManager" # Landroid/media/AudioManager; */
/* .param p2, "record" # Lcom/android/server/notification/NotificationRecord; */
/* .param p3, "mNm" # Lcom/android/server/notification/NotificationManagerService; */
/* .line 305 */
int v0 = 0; // const/4 v0, 0x0
/* .line 306 */
/* .local v0, "buzz":Z */
int v1 = 0; // const/4 v1, 0x0
/* .line 307 */
/* .local v1, "beep":Z */
v2 = (( com.android.server.notification.NotificationRecord ) p2 ).getImportance ( ); // invoke-virtual {p2}, Lcom/android/server/notification/NotificationRecord;->getImportance()I
int v3 = 3; // const/4 v3, 0x3
int v4 = 1; // const/4 v4, 0x1
int v5 = 0; // const/4 v5, 0x0
/* if-lt v2, v3, :cond_0 */
/* move v2, v4 */
} // :cond_0
/* move v2, v5 */
/* .line 308 */
/* .local v2, "aboveThreshold":Z */
} // :goto_0
(( com.android.server.notification.NotificationRecord ) p2 ).getSound ( ); // invoke-virtual {p2}, Lcom/android/server/notification/NotificationRecord;->getSound()Landroid/net/Uri;
/* .line 309 */
/* .local v3, "soundUri":Landroid/net/Uri; */
(( com.android.server.notification.NotificationRecord ) p2 ).getVibration ( ); // invoke-virtual {p2}, Lcom/android/server/notification/NotificationRecord;->getVibration()Landroid/os/VibrationEffect;
/* .line 310 */
/* .local v6, "vibration":Landroid/os/VibrationEffect; */
if ( v6 != null) { // if-eqz v6, :cond_1
/* move v7, v4 */
} // :cond_1
/* move v7, v5 */
/* .line 311 */
/* .local v7, "hasValidVibrate":Z */
} // :goto_1
if ( v2 != null) { // if-eqz v2, :cond_5
if ( p1 != null) { // if-eqz p1, :cond_5
v8 = (( com.android.server.notification.NotificationManagerServiceImpl ) p0 ).shouldOnlyAlertNotification ( p2 ); // invoke-virtual {p0, p2}, Lcom/android/server/notification/NotificationManagerServiceImpl;->shouldOnlyAlertNotification(Lcom/android/server/notification/NotificationRecord;)Z
if ( v8 != null) { // if-eqz v8, :cond_5
/* .line 312 */
if ( v3 != null) { // if-eqz v3, :cond_2
v8 = android.net.Uri.EMPTY;
v8 = (( android.net.Uri ) v8 ).equals ( v3 ); // invoke-virtual {v8, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
/* if-nez v8, :cond_2 */
/* move v8, v4 */
} // :cond_2
/* move v8, v5 */
/* .line 313 */
/* .local v8, "hasValidSound":Z */
} // :goto_2
if ( v8 != null) { // if-eqz v8, :cond_3
/* .line 314 */
int v1 = 1; // const/4 v1, 0x1
/* .line 316 */
} // :cond_3
/* nop */
/* .line 317 */
v9 = (( android.media.AudioManager ) p1 ).getRingerModeInternal ( ); // invoke-virtual {p1}, Landroid/media/AudioManager;->getRingerModeInternal()I
/* if-nez v9, :cond_4 */
/* move v9, v4 */
} // :cond_4
/* move v9, v5 */
/* .line 319 */
/* .local v9, "ringerModeSilent":Z */
} // :goto_3
if ( v7 != null) { // if-eqz v7, :cond_5
/* if-nez v9, :cond_5 */
/* .line 320 */
int v0 = 1; // const/4 v0, 0x1
/* .line 324 */
} // .end local v8 # "hasValidSound":Z
} // .end local v9 # "ringerModeSilent":Z
} // :cond_5
if ( v1 != null) { // if-eqz v1, :cond_6
int v8 = 2; // const/4 v8, 0x2
} // :cond_6
/* move v8, v5 */
} // :goto_4
/* or-int/2addr v8, v0 */
/* .line 325 */
/* .local v8, "buzzBeepBlink":I */
v9 = (( com.android.server.notification.NotificationRecord ) p2 ).getBuzzBeepBlinkCode ( ); // invoke-virtual {p2}, Lcom/android/server/notification/NotificationRecord;->getBuzzBeepBlinkCode()I
/* or-int/2addr v9, v8 */
(( com.android.server.notification.NotificationRecord ) p2 ).setBuzzBeepBlinkCode ( v9 ); // invoke-virtual {p2, v9}, Lcom/android/server/notification/NotificationRecord;->setBuzzBeepBlinkCode(I)V
/* .line 326 */
/* if-nez v0, :cond_8 */
if ( v1 != null) { // if-eqz v1, :cond_7
} // :cond_7
/* move v4, v5 */
} // :cond_8
} // :goto_5
(( com.android.server.notification.NotificationRecord ) p2 ).setAudiblyAlerted ( v4 ); // invoke-virtual {p2, v4}, Lcom/android/server/notification/NotificationRecord;->setAudiblyAlerted(Z)V
/* .line 327 */
return;
} // .end method
public void checkAllowToCreateChannels ( java.lang.String p0, android.content.pm.ParceledListSlice p1 ) {
/* .locals 6 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "channelsList" # Landroid/content/pm/ParceledListSlice; */
/* .line 422 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/notification/NotificationManagerServiceImpl;->isCallerXmsfOrSystem(Ljava/lang/String;)Z */
/* if-nez v0, :cond_2 */
/* .line 423 */
(( android.content.pm.ParceledListSlice ) p2 ).getList ( ); // invoke-virtual {p2}, Landroid/content/pm/ParceledListSlice;->getList()Ljava/util/List;
/* .line 424 */
/* .local v0, "channels":Ljava/util/List;, "Ljava/util/List<Landroid/app/NotificationChannel;>;" */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
v2 = } // :goto_0
/* if-ge v1, v2, :cond_2 */
/* .line 425 */
/* check-cast v2, Landroid/app/NotificationChannel; */
/* .line 426 */
/* .local v2, "channel":Landroid/app/NotificationChannel; */
if ( v2 != null) { // if-eqz v2, :cond_1
(( android.app.NotificationChannel ) v2 ).getId ( ); // invoke-virtual {v2}, Landroid/app/NotificationChannel;->getId()Ljava/lang/String;
v3 = /* invoke-direct {p0, v3}, Lcom/android/server/notification/NotificationManagerServiceImpl;->isXmsfChannelId(Ljava/lang/String;)Z */
/* if-nez v3, :cond_0 */
/* .line 427 */
} // :cond_0
/* new-instance v3, Ljava/lang/SecurityException; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Pkg "; // const-string v5, "Pkg "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " cannot create channels, because "; // const-string v5, " cannot create channels, because "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 428 */
(( android.app.NotificationChannel ) v2 ).getId ( ); // invoke-virtual {v2}, Landroid/app/NotificationChannel;->getId()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " starts with \'mipush|\'"; // const-string v5, " starts with \'mipush|\'"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v3, v4}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
/* throw v3 */
/* .line 424 */
} // .end local v2 # "channel":Landroid/app/NotificationChannel;
} // :cond_1
} // :goto_1
/* add-int/lit8 v1, v1, 0x1 */
/* .line 432 */
} // .end local v0 # "channels":Ljava/util/List;, "Ljava/util/List<Landroid/app/NotificationChannel;>;"
} // .end local v1 # "i":I
} // :cond_2
return;
} // .end method
public Boolean checkCallerIsXmsf ( ) {
/* .locals 2 */
/* .line 532 */
v0 = android.os.Binder .getCallingUid ( );
v1 = android.os.UserHandle .getCallingUserId ( );
v0 = /* invoke-direct {p0, v0, v1}, Lcom/android/server/notification/NotificationManagerServiceImpl;->checkCallerIsXmsfInternal(II)Z */
} // .end method
public void checkFullScreenIntent ( android.app.Notification p0, android.app.AppOpsManager p1, Integer p2, java.lang.String p3 ) {
/* .locals 7 */
/* .param p1, "notification" # Landroid/app/Notification; */
/* .param p2, "appOpsManager" # Landroid/app/AppOpsManager; */
/* .param p3, "uid" # I */
/* .param p4, "packageName" # Ljava/lang/String; */
/* .line 364 */
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
/* if-nez v0, :cond_0 */
v0 = this.fullScreenIntent;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 367 */
/* const/16 v2, 0x2725 */
int v5 = 0; // const/4 v5, 0x0
final String v6 = "NotificationManagementImpl#checkFullScreenIntent"; // const-string v6, "NotificationManagementImpl#checkFullScreenIntent"
/* move-object v1, p2 */
/* move v3, p3 */
/* move-object v4, p4 */
v0 = /* invoke-virtual/range {v1 ..v6}, Landroid/app/AppOpsManager;->noteOpNoThrow(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I */
/* .line 369 */
/* .local v0, "mode":I */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 370 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "MIUILOG- Permission Denied Activity : "; // const-string v2, "MIUILOG- Permission Denied Activity : "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.fullScreenIntent;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v2 = " pkg : "; // const-string v2, " pkg : "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p4 ); // invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " uid : "; // const-string v2, " uid : "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "NotificationManagerServiceImpl"; // const-string v2, "NotificationManagerServiceImpl"
android.util.Slog .i ( v2,v1 );
/* .line 372 */
int v1 = 0; // const/4 v1, 0x0
this.fullScreenIntent = v1;
/* .line 375 */
} // .end local v0 # "mode":I
} // :cond_0
return;
} // .end method
public Boolean checkInterceptListener ( android.service.notification.StatusBarNotification p0, android.content.ComponentName p1 ) {
/* .locals 1 */
/* .param p1, "sbn" # Landroid/service/notification/StatusBarNotification; */
/* .param p2, "component" # Landroid/content/ComponentName; */
/* .line 684 */
v0 = /* invoke-direct {p0, p1, p2}, Lcom/android/server/notification/NotificationManagerServiceImpl;->checkInterceptListenerForXiaomiInternal(Landroid/service/notification/StatusBarNotification;Landroid/content/ComponentName;)Z */
/* if-nez v0, :cond_1 */
/* .line 685 */
(( android.content.ComponentName ) p2 ).getPackageName ( ); // invoke-virtual {p2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
v0 = /* invoke-direct {p0, p1, v0}, Lcom/android/server/notification/NotificationManagerServiceImpl;->checkInterceptListenerForMIUIInternal(Landroid/service/notification/StatusBarNotification;Ljava/lang/String;)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
/* .line 684 */
} // :goto_1
} // .end method
public Boolean checkIsXmsfFakeConditionProviderEnabled ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "path" # Ljava/lang/String; */
/* .line 459 */
/* const-string/jumbo v0, "xmsf_fake_condition_provider_path" */
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = (( com.android.server.notification.NotificationManagerServiceImpl ) p0 ).checkCallerIsXmsf ( ); // invoke-virtual {p0}, Lcom/android/server/notification/NotificationManagerServiceImpl;->checkCallerIsXmsf()Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean checkMediaNotificationControl ( Integer p0, java.lang.String p1, com.android.server.notification.PreferencesHelper p2 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .param p2, "pkg" # Ljava/lang/String; */
/* .param p3, "mPreferencesHelper" # Lcom/android/server/notification/PreferencesHelper; */
/* .line 960 */
int v0 = 1; // const/4 v0, 0x1
/* .line 961 */
/* .local v0, "isAllowMediaNotification":Z */
v1 = (( com.android.server.notification.PreferencesHelper ) p3 ).getMediaNotificationsEnabled ( p2, p1 ); // invoke-virtual {p3, p2, p1}, Lcom/android/server/notification/PreferencesHelper;->getMediaNotificationsEnabled(Ljava/lang/String;I)Z
final String v2 = "checkMediaNotificationControl pkg:"; // const-string v2, "checkMediaNotificationControl pkg:"
final String v3 = "NotificationManagerServiceImpl"; // const-string v3, "NotificationManagerServiceImpl"
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 962 */
v1 = com.android.server.notification.NotificationManagerServiceImpl.allowMediaNotificationCloudDataList;
v1 = com.android.internal.util.ArrayUtils .contains ( v1,p2 );
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 963 */
int v0 = 0; // const/4 v0, 0x0
/* .line 964 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " This media notification is in the blacklist, and media notifications are not allowed to be sent! backlist is "; // const-string v2, " This media notification is in the blacklist, and media notifications are not allowed to be sent! backlist is "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = com.android.server.notification.NotificationManagerServiceImpl.allowMediaNotificationCloudDataList;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v3,v1 );
/* .line 969 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 970 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " This media notification media notification switch closed, and notifications are not allowed to be sent!"; // const-string v2, " This media notification media notification switch closed, and notifications are not allowed to be sent!"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v3,v1 );
/* .line 974 */
} // :cond_1
} // :goto_0
} // .end method
public com.android.server.notification.NotificationRecord$Light customizeNotificationLight ( android.content.Context p0, com.android.server.notification.NotificationRecord$Light p1 ) {
/* .locals 8 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "originLight" # Lcom/android/server/notification/NotificationRecord$Light; */
/* .line 888 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "breathing_light"; // const-string v1, "breathing_light"
int v2 = -2; // const/4 v2, -0x2
android.provider.Settings$Secure .getStringForUser ( v0,v1,v2 );
/* .line 890 */
/* .local v0, "jsonText":Ljava/lang/String; */
v1 = android.text.TextUtils .isEmpty ( v0 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 891 */
/* .line 893 */
} // :cond_0
/* iget v1, p2, Lcom/android/server/notification/NotificationRecord$Light;->color:I */
/* .line 894 */
/* .local v1, "customizeColor":I */
/* iget v2, p2, Lcom/android/server/notification/NotificationRecord$Light;->onMs:I */
/* .line 896 */
/* .local v2, "customizeOnMs":I */
try { // :try_start_0
/* new-instance v3, Lorg/json/JSONArray; */
/* invoke-direct {v3, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V */
/* .line 897 */
/* .local v3, "jsonArray":Lorg/json/JSONArray; */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
} // :goto_0
v5 = (( org.json.JSONArray ) v3 ).length ( ); // invoke-virtual {v3}, Lorg/json/JSONArray;->length()I
/* if-ge v4, v5, :cond_2 */
/* .line 898 */
(( org.json.JSONArray ) v3 ).getJSONObject ( v4 ); // invoke-virtual {v3, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
/* .line 899 */
/* .local v5, "logObject":Lorg/json/JSONObject; */
final String v6 = "light"; // const-string v6, "light"
v6 = (( org.json.JSONObject ) v5 ).getInt ( v6 ); // invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
int v7 = 4; // const/4 v7, 0x4
/* if-ne v6, v7, :cond_1 */
/* .line 900 */
final String v6 = "color"; // const-string v6, "color"
v6 = (( org.json.JSONObject ) v5 ).getInt ( v6 ); // invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* move v1, v6 */
/* .line 901 */
final String v6 = "onMS"; // const-string v6, "onMS"
v6 = (( org.json.JSONObject ) v5 ).getInt ( v6 ); // invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move v2, v6 */
/* .line 897 */
} // .end local v5 # "logObject":Lorg/json/JSONObject;
} // :cond_1
/* add-int/lit8 v4, v4, 0x1 */
/* .line 906 */
} // .end local v3 # "jsonArray":Lorg/json/JSONArray;
} // .end local v4 # "i":I
} // :cond_2
/* .line 904 */
/* :catch_0 */
/* move-exception v3 */
/* .line 905 */
/* .local v3, "e":Lorg/json/JSONException; */
final String v4 = "NotificationManagerServiceImpl"; // const-string v4, "NotificationManagerServiceImpl"
final String v5 = "Light jsonArray error"; // const-string v5, "Light jsonArray error"
android.util.Slog .i ( v4,v5,v3 );
/* .line 907 */
} // .end local v3 # "e":Lorg/json/JSONException;
} // :goto_1
/* new-instance v3, Lcom/android/server/notification/NotificationRecord$Light; */
/* iget v4, p2, Lcom/android/server/notification/NotificationRecord$Light;->offMs:I */
/* invoke-direct {v3, v1, v2, v4}, Lcom/android/server/notification/NotificationRecord$Light;-><init>(III)V */
} // .end method
public void dumpLight ( java.io.PrintWriter p0, com.android.server.notification.NotificationManagerService$DumpFilter p1 ) {
/* .locals 8 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "filter" # Lcom/android/server/notification/NotificationManagerService$DumpFilter; */
/* .line 773 */
int v0 = 0; // const/4 v0, 0x0
/* .line 774 */
/* .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
int v1 = 0; // const/4 v1, 0x0
/* .line 776 */
/* .local v1, "instance":Ljava/lang/Object; */
try { // :try_start_0
final String v2 = "com.android.server.lights.MiuiLightsService"; // const-string v2, "com.android.server.lights.MiuiLightsService"
java.lang.Class .forName ( v2 );
/* move-object v0, v2 */
/* .line 777 */
final String v2 = "getInstance"; // const-string v2, "getInstance"
int v3 = 0; // const/4 v3, 0x0
/* new-array v4, v3, [Ljava/lang/Class; */
(( java.lang.Class ) v0 ).getMethod ( v2, v4 ); // invoke-virtual {v0, v2, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 778 */
/* .local v2, "getInstanceMethod":Ljava/lang/reflect/Method; */
final String v4 = "dumpLight"; // const-string v4, "dumpLight"
int v5 = 2; // const/4 v5, 0x2
/* new-array v5, v5, [Ljava/lang/Class; */
/* const-class v6, Ljava/io/PrintWriter; */
/* aput-object v6, v5, v3 */
/* const-class v6, Lcom/android/server/notification/NotificationManagerService$DumpFilter; */
int v7 = 1; // const/4 v7, 0x1
/* aput-object v6, v5, v7 */
(( java.lang.Class ) v0 ).getMethod ( v4, v5 ); // invoke-virtual {v0, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 779 */
/* .local v4, "dumpLightMethod":Ljava/lang/reflect/Method; */
/* new-array v3, v3, [Ljava/lang/Object; */
int v5 = 0; // const/4 v5, 0x0
(( java.lang.reflect.Method ) v2 ).invoke ( v5, v3 ); // invoke-virtual {v2, v5, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* move-object v1, v3 */
/* .line 780 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 781 */
/* filled-new-array {p1, p2}, [Ljava/lang/Object; */
(( java.lang.reflect.Method ) v4 ).invoke ( v1, v3 ); // invoke-virtual {v4, v1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/IllegalAccessException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/NoSuchMethodException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/ClassNotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 789 */
} // .end local v2 # "getInstanceMethod":Ljava/lang/reflect/Method;
} // .end local v4 # "dumpLightMethod":Ljava/lang/reflect/Method;
} // :cond_0
/* .line 784 */
/* :catch_0 */
/* move-exception v2 */
/* .line 788 */
/* .local v2, "e":Ljava/lang/ReflectiveOperationException; */
final String v3 = "NotificationManagerServiceImpl"; // const-string v3, "NotificationManagerServiceImpl"
final String v4 = "Failed to invoke MiuiLightsService dumpLight method"; // const-string v4, "Failed to invoke MiuiLightsService dumpLight method"
android.util.Slog .e ( v3,v4,v2 );
/* .line 790 */
} // .end local v2 # "e":Ljava/lang/ReflectiveOperationException;
} // :goto_0
return;
} // .end method
public Boolean enableBlockedToasts ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .line 794 */
v0 = this.mPrivacyInputModePkgName;
/* if-nez v0, :cond_1 */
/* .line 795 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "miui_privacy_input_pkg_name"; // const-string v1, "miui_privacy_input_pkg_name"
android.provider.Settings$Secure .getString ( v0,v1 );
/* .line 796 */
/* .local v0, "packageName":Ljava/lang/String; */
/* if-nez v0, :cond_0 */
final String v1 = ""; // const-string v1, ""
} // :cond_0
/* move-object v1, v0 */
} // :goto_0
this.mPrivacyInputModePkgName = v1;
/* .line 798 */
} // .end local v0 # "packageName":Ljava/lang/String;
} // :cond_1
v0 = this.mPrivacyInputModePkgName;
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* xor-int/lit8 v0, v0, 0x1 */
} // .end method
public void fixCheckInterceptListenerAutoGroup ( android.app.Notification p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "notification" # Landroid/app/Notification; */
/* .param p2, "pkg" # Ljava/lang/String; */
/* .line 920 */
try { // :try_start_0
(( android.app.Notification ) p1 ).getChannelId ( ); // invoke-virtual {p1}, Landroid/app/Notification;->getChannelId()Ljava/lang/String;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.interceptChannelId;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ":"; // const-string v2, ":"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 921 */
(( android.app.Notification ) p1 ).getChannelId ( ); // invoke-virtual {p1}, Landroid/app/Notification;->getChannelId()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 920 */
v0 = com.android.internal.util.ArrayUtils .contains ( v0,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 922 */
(( java.lang.Object ) p1 ).getClass ( ); // invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
/* .line 923 */
/* .local v0, "notificationClazz":Ljava/lang/Class; */
final String v1 = "mGroupKey"; // const-string v1, "mGroupKey"
(( java.lang.Class ) v0 ).getDeclaredField ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
/* .line 924 */
/* .local v1, "mGroupKeyField":Ljava/lang/reflect/Field; */
int v2 = 1; // const/4 v2, 0x1
(( java.lang.reflect.Field ) v1 ).setAccessible ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V
/* .line 925 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( android.app.Notification ) p1 ).getChannelId ( ); // invoke-virtual {p1}, Landroid/app/Notification;->getChannelId()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "_autogroup"; // const-string v3, "_autogroup"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.reflect.Field ) v1 ).set ( p1, v2 ); // invoke-virtual {v1, p1, v2}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 929 */
} // .end local v0 # "notificationClazz":Ljava/lang/Class;
} // .end local v1 # "mGroupKeyField":Ljava/lang/reflect/Field;
} // :cond_0
/* .line 927 */
/* :catch_0 */
/* move-exception v0 */
/* .line 928 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "NotificationManagerServiceImpl"; // const-string v1, "NotificationManagerServiceImpl"
final String v2 = " fixCheckInterceptListenerGroup exception:"; // const-string v2, " fixCheckInterceptListenerGroup exception:"
android.util.Slog .i ( v1,v2,v0 );
/* .line 930 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public java.lang.String getAppPkgByChannel ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "channelIdOrGroupId" # Ljava/lang/String; */
/* .param p2, "defaultPkg" # Ljava/lang/String; */
/* .line 557 */
v0 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v0, :cond_0 */
/* .line 558 */
final String v0 = "\\|"; // const-string v0, "\\|"
(( java.lang.String ) p1 ).split ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 559 */
/* .local v0, "array":[Ljava/lang/String; */
/* array-length v1, v0 */
int v2 = 2; // const/4 v2, 0x2
/* if-lt v1, v2, :cond_0 */
/* .line 560 */
int v1 = 1; // const/4 v1, 0x1
/* aget-object v1, v0, v1 */
/* .line 563 */
} // .end local v0 # "array":[Ljava/lang/String;
} // :cond_0
} // .end method
public Integer getAppUidByPkg ( java.lang.String p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "defaultUid" # I */
/* .line 568 */
v0 = android.os.UserHandle .getCallingUserId ( );
v0 = /* invoke-direct {p0, p1, v0}, Lcom/android/server/notification/NotificationManagerServiceImpl;->getUidByPkg(Ljava/lang/String;I)I */
/* .line 569 */
/* .local v0, "appUid":I */
int v1 = -1; // const/4 v1, -0x1
/* if-eq v0, v1, :cond_0 */
/* .line 570 */
/* .line 572 */
} // :cond_0
} // .end method
public android.os.IBinder getColorLightManager ( ) {
/* .locals 1 */
/* .line 767 */
/* const-class v0, Lmiui/app/MiuiLightsManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lmiui/app/MiuiLightsManagerInternal; */
/* .line 768 */
(( miui.app.MiuiLightsManagerInternal ) v0 ).getBinderService ( ); // invoke-virtual {v0}, Lmiui/app/MiuiLightsManagerInternal;->getBinderService()Landroid/os/IBinder;
/* .line 767 */
} // .end method
public Boolean getMediaNotificationsEnabled ( java.lang.String p0, Integer p1, com.android.server.notification.PreferencesHelper p2 ) {
/* .locals 1 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .param p3, "mPreferencesHelper" # Lcom/android/server/notification/PreferencesHelper; */
/* .line 987 */
v0 = (( com.android.server.notification.NotificationManagerServiceImpl ) p0 ).checkMediaNotificationControl ( p2, p1, p3 ); // invoke-virtual {p0, p2, p1, p3}, Lcom/android/server/notification/NotificationManagerServiceImpl;->checkMediaNotificationControl(ILjava/lang/String;Lcom/android/server/notification/PreferencesHelper;)Z
} // .end method
public android.util.Pair getMiuiDefaultListerWithVersion ( ) {
/* .locals 8 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Landroid/util/Pair<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 832 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x110f00a4 */
(( android.content.res.Resources ) v0 ).getString ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* .line 834 */
/* .local v0, "defaultListenerAccess":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 835 */
final String v1 = ","; // const-string v1, ","
(( java.lang.String ) v0 ).split ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 836 */
/* .local v1, "listenersWithVersion":[Ljava/lang/String; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "listenersWithVersion "; // const-string v3, "listenersWithVersion "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.util.Arrays .toString ( v1 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "NotificationManagerServiceImpl"; // const-string v3, "NotificationManagerServiceImpl"
android.util.Slog .d ( v3,v2 );
/* .line 837 */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 838 */
int v2 = -1; // const/4 v2, -0x1
/* .line 839 */
/* .local v2, "version":I */
int v3 = 0; // const/4 v3, 0x0
/* .line 840 */
/* .local v3, "listeners":Ljava/lang/String; */
/* array-length v4, v1 */
int v5 = 2; // const/4 v5, 0x2
int v6 = 0; // const/4 v6, 0x0
int v7 = 1; // const/4 v7, 0x1
/* if-ne v4, v5, :cond_0 */
/* .line 841 */
/* aget-object v3, v1, v6 */
/* .line 842 */
/* aget-object v4, v1, v7 */
java.lang.Integer .valueOf ( v4 );
v2 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
/* .line 843 */
} // :cond_0
/* array-length v4, v1 */
/* if-ne v4, v7, :cond_1 */
/* .line 844 */
/* aget-object v3, v1, v6 */
/* .line 845 */
int v2 = 0; // const/4 v2, 0x0
/* .line 847 */
} // :cond_1
} // :goto_0
/* new-instance v4, Landroid/util/Pair; */
java.lang.Integer .valueOf ( v2 );
/* invoke-direct {v4, v5, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 850 */
} // .end local v1 # "listenersWithVersion":[Ljava/lang/String;
} // .end local v2 # "version":I
} // .end local v3 # "listeners":Ljava/lang/String;
} // :cond_2
/* new-instance v1, Landroid/util/Pair; */
int v2 = -1; // const/4 v2, -0x1
java.lang.Integer .valueOf ( v2 );
int v3 = 0; // const/4 v3, 0x0
/* invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
} // .end method
public java.lang.String getPlayVibrationPkg ( java.lang.String p0, java.lang.String p1, java.lang.String p2 ) {
/* .locals 1 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "opPkg" # Ljava/lang/String; */
/* .param p3, "defaultPkg" # Ljava/lang/String; */
/* .line 524 */
v0 = /* invoke-direct {p0, p2}, Lcom/android/server/notification/NotificationManagerServiceImpl;->isXmsf(Ljava/lang/String;)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/notification/NotificationManagerServiceImpl;->isXmsf(Ljava/lang/String;)Z */
/* if-nez v0, :cond_0 */
/* .line 525 */
/* .line 527 */
} // :cond_0
} // .end method
public java.lang.Object getVibRateLimiter ( ) {
/* .locals 1 */
/* .line 651 */
/* new-instance v0, Lcom/android/server/notification/VibRateLimiter; */
/* invoke-direct {v0}, Lcom/android/server/notification/VibRateLimiter;-><init>()V */
} // .end method
public void init ( android.content.Context p0, com.android.server.notification.NotificationManagerService p1 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "nms" # Lcom/android/server/notification/NotificationManagerService; */
/* .line 117 */
this.mContext = p1;
/* .line 118 */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x11030045 */
(( android.content.res.Resources ) v0 ).getStringArray ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
this.interceptListener = v0;
/* .line 120 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x11030044 */
(( android.content.res.Resources ) v0 ).getStringArray ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
this.interceptChannelId = v0;
/* .line 122 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x11030049 */
(( android.content.res.Resources ) v0 ).getStringArray ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
this.allowNotificationAccessList = v0;
/* .line 124 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " allowNotificationListener :"; // const-string v1, " allowNotificationListener :"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.allowNotificationAccessList;
java.util.Arrays .toString ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "NotificationManagerServiceImpl"; // const-string v1, "NotificationManagerServiceImpl"
android.util.Slog .i ( v1,v0 );
/* .line 125 */
v0 = this.mContext;
/* .line 126 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 125 */
final String v1 = "config_notification_medianotificationcontrol"; // const-string v1, "config_notification_medianotificationcontrol"
final String v2 = "blacklist"; // const-string v2, "blacklist"
final String v3 = ""; // const-string v3, ""
android.provider.MiuiSettings$SettingsCloudData .getCloudDataString ( v0,v1,v2,v3 );
this.allowMediaNotificationCloudData = v0;
/* .line 128 */
/* nop */
/* .line 129 */
com.android.server.notification.NotificationManagerServiceImpl .getMediaNotificationDataCloudConversion ( v0 );
/* .line 130 */
com.android.internal.os.BackgroundThread .getHandler ( );
/* invoke-direct {p0, v0}, Lcom/android/server/notification/NotificationManagerServiceImpl;->registerPrivacyInputMode(Landroid/os/Handler;)V */
/* .line 131 */
com.android.internal.os.BackgroundThread .getHandler ( );
/* invoke-direct {p0, v0}, Lcom/android/server/notification/NotificationManagerServiceImpl;->registerCloudDataResolver(Landroid/os/Handler;)V */
/* .line 132 */
this.mNMS = p2;
/* .line 133 */
return;
} // .end method
public void isAllowAppNotificationListener ( Boolean p0, com.android.server.notification.NotificationManagerService$NotificationListeners p1, Integer p2, java.lang.String p3 ) {
/* .locals 7 */
/* .param p1, "isPackageAdded" # Z */
/* .param p2, "mListeners" # Lcom/android/server/notification/NotificationManagerService$NotificationListeners; */
/* .param p3, "userId" # I */
/* .param p4, "pkg" # Ljava/lang/String; */
/* .line 940 */
if ( p1 != null) { // if-eqz p1, :cond_0
v0 = this.allowNotificationAccessList;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 941 */
v0 = com.android.internal.util.ArrayUtils .contains ( v0,p4 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 942 */
v0 = this.mContext;
/* const-class v1, Landroid/app/NotificationManager; */
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/NotificationManager; */
/* .line 943 */
/* .local v0, "nm":Landroid/app/NotificationManager; */
/* const/high16 v1, 0xc0000 */
int v2 = 0; // const/4 v2, 0x0
(( com.android.server.notification.NotificationManagerService$NotificationListeners ) p2 ).queryPackageForServices ( p4, v1, v2 ); // invoke-virtual {p2, p4, v1, v2}, Lcom/android/server/notification/NotificationManagerService$NotificationListeners;->queryPackageForServices(Ljava/lang/String;II)Landroid/util/ArraySet;
/* .line 946 */
/* .local v1, "listeners":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Landroid/content/ComponentName;>;" */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "k":I */
} // :goto_0
v3 = (( android.util.ArraySet ) v1 ).size ( ); // invoke-virtual {v1}, Landroid/util/ArraySet;->size()I
/* if-ge v2, v3, :cond_0 */
/* .line 947 */
(( android.util.ArraySet ) v1 ).valueAt ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;
/* check-cast v3, Landroid/content/ComponentName; */
/* .line 949 */
/* .local v3, "cn":Landroid/content/ComponentName; */
int v4 = 1; // const/4 v4, 0x1
try { // :try_start_0
(( android.app.NotificationManager ) v0 ).setNotificationListenerAccessGrantedForUser ( v3, p3, v4 ); // invoke-virtual {v0, v3, p3, v4}, Landroid/app/NotificationManager;->setNotificationListenerAccessGrantedForUser(Landroid/content/ComponentName;IZ)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 952 */
/* .line 950 */
/* :catch_0 */
/* move-exception v4 */
/* .line 951 */
/* .local v4, "e":Ljava/lang/Exception; */
final String v5 = "NotificationManagerServiceImpl"; // const-string v5, "NotificationManagerServiceImpl"
final String v6 = " isAllowAppNotificationListener exception:"; // const-string v6, " isAllowAppNotificationListener exception:"
android.util.Slog .i ( v5,v6,v4 );
/* .line 946 */
} // .end local v3 # "cn":Landroid/content/ComponentName;
} // .end local v4 # "e":Ljava/lang/Exception;
} // :goto_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 955 */
} // .end local v0 # "nm":Landroid/app/NotificationManager;
} // .end local v1 # "listeners":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Landroid/content/ComponentName;>;"
} // .end local v2 # "k":I
} // :cond_0
return;
} // .end method
public Boolean isAllowAppRenderedToast ( java.lang.String p0, Integer p1, Integer p2 ) {
/* .locals 8 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "callingUid" # I */
/* .param p3, "userId" # I */
/* .line 386 */
v0 = v0 = com.android.server.notification.NotificationManagerServiceImpl.sAllowToastSet;
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 387 */
/* const-class v0, Landroid/content/pm/PackageManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Landroid/content/pm/PackageManagerInternal; */
/* .line 389 */
/* .local v0, "pm":Landroid/content/pm/PackageManagerInternal; */
/* const-wide/16 v4, 0x0 */
try { // :try_start_0
v6 = android.os.Process .myUid ( );
/* move-object v2, v0 */
/* move-object v3, p1 */
/* move v7, p3 */
/* invoke-virtual/range {v2 ..v7}, Landroid/content/pm/PackageManagerInternal;->getPackageInfo(Ljava/lang/String;JII)Landroid/content/pm/PackageInfo; */
/* .line 390 */
/* .local v2, "info":Landroid/content/pm/PackageInfo; */
v3 = this.applicationInfo;
v3 = (( android.content.pm.ApplicationInfo ) v3 ).isSystemApp ( ); // invoke-virtual {v3}, Landroid/content/pm/ApplicationInfo;->isSystemApp()Z
if ( v3 != null) { // if-eqz v3, :cond_0
v3 = this.applicationInfo;
/* iget v3, v3, Landroid/content/pm/ApplicationInfo;->uid:I */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* if-ne p2, v3, :cond_0 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
/* .line 391 */
} // .end local v2 # "info":Landroid/content/pm/PackageInfo;
/* :catch_0 */
/* move-exception v2 */
/* .line 392 */
/* .local v2, "e":Ljava/lang/Exception; */
final String v3 = "NotificationManagerServiceImpl"; // const-string v3, "NotificationManagerServiceImpl"
final String v4 = "can\'t find package!"; // const-string v4, "can\'t find package!"
android.util.Slog .e ( v3,v4,v2 );
/* .line 395 */
} // .end local v0 # "pm":Landroid/content/pm/PackageManagerInternal;
} // .end local v2 # "e":Ljava/lang/Exception;
} // :cond_1
} // .end method
public Boolean isDelegateAllowed ( java.lang.String p0, Integer p1, java.lang.String p2, Integer p3 ) {
/* .locals 1 */
/* .param p1, "callingPkg" # Ljava/lang/String; */
/* .param p2, "callingUid" # I */
/* .param p3, "targetPkg" # Ljava/lang/String; */
/* .param p4, "userId" # I */
/* .line 489 */
v0 = /* invoke-direct {p0, p1, p2}, Lcom/android/server/notification/NotificationManagerServiceImpl;->isCallerAndroid(Ljava/lang/String;I)Z */
/* if-nez v0, :cond_1 */
v0 = /* invoke-direct {p0, p1, p2}, Lcom/android/server/notification/NotificationManagerServiceImpl;->isCallerSecurityCenter(Ljava/lang/String;I)Z */
/* if-nez v0, :cond_1 */
/* .line 490 */
v0 = /* invoke-direct {p0, p1, p3, p2, p4}, Lcom/android/server/notification/NotificationManagerServiceImpl;->checkCallerIsXmsf(Ljava/lang/String;Ljava/lang/String;II)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
/* .line 489 */
} // :goto_1
} // .end method
public Boolean isDeniedDemoPostNotification ( Integer p0, Integer p1, android.app.Notification p2, java.lang.String p3 ) {
/* .locals 2 */
/* .param p1, "userId" # I */
/* .param p2, "uid" # I */
/* .param p3, "notification" # Landroid/app/Notification; */
/* .param p4, "pkg" # Ljava/lang/String; */
/* .line 601 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p3, :cond_0 */
/* .line 602 */
/* .line 604 */
} // :cond_0
/* sget-boolean v1, Lmiui/os/Build;->IS_DEMO_BUILD:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
v1 = (( android.app.Notification ) p3 ).isForegroundService ( ); // invoke-virtual {p3}, Landroid/app/Notification;->isForegroundService()Z
/* if-nez v1, :cond_1 */
/* .line 605 */
v1 = /* invoke-direct {p0, p3}, Lcom/android/server/notification/NotificationManagerServiceImpl;->hasProgress(Landroid/app/Notification;)Z */
/* if-nez v1, :cond_1 */
/* .line 606 */
v1 = (( android.app.Notification ) p3 ).isMediaNotification ( ); // invoke-virtual {p3}, Landroid/app/Notification;->isMediaNotification()Z
/* if-nez v1, :cond_1 */
/* .line 607 */
v1 = /* invoke-direct {p0, p1, p2, p4}, Lcom/android/server/notification/NotificationManagerServiceImpl;->isCallingSystem(IILjava/lang/String;)Z */
/* if-nez v1, :cond_1 */
/* .line 608 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Denied demo product third-party app :"; // const-string v1, "Denied demo product third-party app :"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p4 ); // invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "to post notification :" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 609 */
(( android.app.Notification ) p3 ).toString ( ); // invoke-virtual {p3}, Landroid/app/Notification;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 608 */
final String v1 = "NotificationManagerServiceImpl"; // const-string v1, "NotificationManagerServiceImpl"
android.util.Slog .i ( v1,v0 );
/* .line 610 */
int v0 = 1; // const/4 v0, 0x1
/* .line 612 */
} // :cond_1
} // .end method
public Boolean isDeniedLed ( com.android.server.notification.NotificationRecord p0 ) {
/* .locals 1 */
/* .param p1, "record" # Lcom/android/server/notification/NotificationRecord; */
/* .line 298 */
v0 = v0 = this.mNRStatusStub;
/* xor-int/lit8 v0, v0, 0x1 */
} // .end method
public Boolean isDeniedLocalNotification ( android.app.AppOpsManager p0, android.app.Notification p1, Integer p2, java.lang.String p3 ) {
/* .locals 3 */
/* .param p1, "appops" # Landroid/app/AppOpsManager; */
/* .param p2, "notification" # Landroid/app/Notification; */
/* .param p3, "callingUid" # I */
/* .param p4, "callingPkg" # Ljava/lang/String; */
/* .line 510 */
final String v0 = "com.xiaomi.xmsf"; // const-string v0, "com.xiaomi.xmsf"
v0 = (( java.lang.String ) v0 ).equals ( p4 ); // invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_2 */
/* iget v0, p2, Landroid/app/Notification;->flags:I */
/* and-int/lit8 v0, v0, 0x40 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 513 */
} // :cond_0
/* const/16 v0, 0x2731 */
v0 = (( android.app.AppOpsManager ) p1 ).noteOpNoThrow ( v0, p3, p4 ); // invoke-virtual {p1, v0, p3, p4}, Landroid/app/AppOpsManager;->noteOpNoThrow(IILjava/lang/String;)I
/* .line 514 */
/* .local v0, "mode":I */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 515 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "MIUILOG- Permission Denied to post local notification for "; // const-string v2, "MIUILOG- Permission Denied to post local notification for "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p4 ); // invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "NotificationManagerServiceImpl"; // const-string v2, "NotificationManagerServiceImpl"
android.util.Slog .i ( v2,v1 );
/* .line 516 */
int v1 = 1; // const/4 v1, 0x1
/* .line 518 */
} // :cond_1
/* .line 511 */
} // .end local v0 # "mode":I
} // :cond_2
} // :goto_0
} // .end method
public Boolean isDeniedPlaySound ( android.media.AudioManager p0, com.android.server.notification.NotificationRecord p1 ) {
/* .locals 1 */
/* .param p1, "audioManager" # Landroid/media/AudioManager; */
/* .param p2, "record" # Lcom/android/server/notification/NotificationRecord; */
/* .line 250 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/notification/NotificationManagerServiceImpl;->isAudioCanPlay(Landroid/media/AudioManager;)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = /* invoke-direct {p0, p2}, Lcom/android/server/notification/NotificationManagerServiceImpl;->isSilenceMode(Lcom/android/server/notification/NotificationRecord;)Z */
/* if-nez v0, :cond_1 */
v0 = this.mNRStatusStub;
v0 = /* .line 251 */
/* if-nez v0, :cond_0 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
/* .line 250 */
} // :goto_1
} // .end method
public Boolean isDeniedPlayVibration ( android.media.AudioManager p0, com.android.server.notification.NotificationRecord p1, com.android.server.notification.NotificationUsageStats$AggregatedStats p2 ) {
/* .locals 4 */
/* .param p1, "audioManager" # Landroid/media/AudioManager; */
/* .param p2, "record" # Lcom/android/server/notification/NotificationRecord; */
/* .param p3, "mStats" # Lcom/android/server/notification/NotificationUsageStats$AggregatedStats; */
/* .line 275 */
/* nop */
/* .line 276 */
v0 = (( android.media.AudioManager ) p1 ).getRingerMode ( ); // invoke-virtual {p1}, Landroid/media/AudioManager;->getRingerMode()I
int v1 = 1; // const/4 v1, 0x1
int v2 = 0; // const/4 v2, 0x0
/* if-nez v0, :cond_0 */
/* move v0, v1 */
} // :cond_0
/* move v0, v2 */
/* .line 278 */
/* .local v0, "exRingerModeSilent":Z */
} // :goto_0
/* if-nez v0, :cond_2 */
v3 = v3 = this.mNRStatusStub;
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 279 */
v3 = /* invoke-direct {p0, p2, p3}, Lcom/android/server/notification/NotificationManagerServiceImpl;->shouldSkipFrequentlyVib(Lcom/android/server/notification/NotificationRecord;Lcom/android/server/notification/NotificationUsageStats$AggregatedStats;)Z */
if ( v3 != null) { // if-eqz v3, :cond_1
} // :cond_1
/* move v1, v2 */
} // :cond_2
} // :goto_1
/* nop */
/* .line 278 */
} // :goto_2
} // .end method
public Boolean isPackageDistractionRestrictionLocked ( com.android.server.notification.NotificationRecord p0 ) {
/* .locals 3 */
/* .param p1, "r" # Lcom/android/server/notification/NotificationRecord; */
/* .line 400 */
(( com.android.server.notification.NotificationRecord ) p1 ).getSbn ( ); // invoke-virtual {p1}, Lcom/android/server/notification/NotificationRecord;->getSbn()Landroid/service/notification/StatusBarNotification;
(( android.service.notification.StatusBarNotification ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;
/* .line 401 */
/* .local v0, "pkg":Ljava/lang/String; */
(( com.android.server.notification.NotificationRecord ) p1 ).getSbn ( ); // invoke-virtual {p1}, Lcom/android/server/notification/NotificationRecord;->getSbn()Landroid/service/notification/StatusBarNotification;
v1 = (( android.service.notification.StatusBarNotification ) v1 ).getUid ( ); // invoke-virtual {v1}, Landroid/service/notification/StatusBarNotification;->getUid()I
/* .line 403 */
/* .local v1, "callingUid":I */
v2 = /* invoke-direct {p0, v0, v1}, Lcom/android/server/notification/NotificationManagerServiceImpl;->isPackageDistractionRestrictionForUser(Ljava/lang/String;I)Z */
} // .end method
public Boolean isSupportSilenceMode ( ) {
/* .locals 1 */
/* .line 255 */
/* sget-boolean v0, Landroid/provider/MiuiSettings$SilenceMode;->isSupported:Z */
} // .end method
public void readDefaultsAndFixMiuiVersion ( com.android.server.notification.ManagedServices p0, java.lang.String p1, com.android.modules.utils.TypedXmlPullParser p2 ) {
/* .locals 8 */
/* .param p1, "services" # Lcom/android/server/notification/ManagedServices; */
/* .param p2, "defaultComponents" # Ljava/lang/String; */
/* .param p3, "parser" # Lcom/android/modules/utils/TypedXmlPullParser; */
/* .line 861 */
final String v1 = "enabled_listeners"; // const-string v1, "enabled_listeners"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* .line 863 */
/* .local v0, "isEnableListener":Z */
v1 = android.text.TextUtils .isEmpty ( p2 );
int v2 = -1; // const/4 v2, -0x1
/* if-nez v1, :cond_3 */
/* .line 864 */
int v1 = 0; // const/4 v1, 0x0
/* .line 865 */
/* .local v1, "hasBarrage":Z */
final String v3 = ":"; // const-string v3, ":"
(( java.lang.String ) p2 ).split ( v3 ); // invoke-virtual {p2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 866 */
/* .local v3, "components":[Ljava/lang/String; */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
} // :goto_0
/* array-length v5, v3 */
/* if-ge v4, v5, :cond_1 */
/* .line 867 */
/* aget-object v5, v3, v4 */
v5 = android.text.TextUtils .isEmpty ( v5 );
/* if-nez v5, :cond_0 */
/* .line 868 */
/* aget-object v5, v3, v4 */
android.content.ComponentName .unflattenFromString ( v5 );
/* .line 869 */
/* .local v5, "cn":Landroid/content/ComponentName; */
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 870 */
(( android.content.ComponentName ) v5 ).getPackageName ( ); // invoke-virtual {v5}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
final String v7 = "com.xiaomi.barrage"; // const-string v7, "com.xiaomi.barrage"
v6 = (( java.lang.String ) v6 ).equals ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 872 */
int v1 = 1; // const/4 v1, 0x1
/* .line 866 */
} // .end local v5 # "cn":Landroid/content/ComponentName;
} // :cond_0
/* add-int/lit8 v4, v4, 0x1 */
/* .line 877 */
} // .end local v4 # "i":I
} // :cond_1
/* if-nez v1, :cond_2 */
v4 = v4 = this.mVersionStub;
/* if-eq v4, v2, :cond_2 */
/* .line 878 */
v4 = this.mVersionStub;
/* .line 880 */
} // .end local v1 # "hasBarrage":Z
} // .end local v3 # "components":[Ljava/lang/String;
} // :cond_2
/* .line 881 */
} // :cond_3
if ( v0 != null) { // if-eqz v0, :cond_4
v1 = v1 = this.mVersionStub;
/* if-eq v1, v2, :cond_4 */
/* .line 882 */
v1 = this.mVersionStub;
/* .line 885 */
} // :cond_4
} // :goto_1
return;
} // .end method
public void setInterceptChannelId ( java.lang.String[] p0 ) {
/* .locals 0 */
/* .param p1, "interceptChannelId" # [Ljava/lang/String; */
/* .line 934 */
this.interceptChannelId = p1;
/* .line 935 */
return;
} // .end method
public void setMediaNotificationEnabled ( java.lang.String p0, Integer p1, Boolean p2, com.android.server.notification.PreferencesHelper p3 ) {
/* .locals 1 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .param p3, "enabled" # Z */
/* .param p4, "mPreferencesHelper" # Lcom/android/server/notification/PreferencesHelper; */
/* .line 980 */
(( com.android.server.notification.PreferencesHelper ) p4 ).setMediaNotificationEnabled ( p1, p2, p3 ); // invoke-virtual {p4, p1, p2, p3}, Lcom/android/server/notification/PreferencesHelper;->setMediaNotificationEnabled(Ljava/lang/String;IZ)V
/* .line 981 */
v0 = this.mNMS;
(( com.android.server.notification.NotificationManagerService ) v0 ).handleSavePolicyFile ( ); // invoke-virtual {v0}, Lcom/android/server/notification/NotificationManagerService;->handleSavePolicyFile()V
/* .line 982 */
return;
} // .end method
public Boolean shouldInjectDeleteChannel ( java.lang.String p0, java.lang.String p1, java.lang.String p2 ) {
/* .locals 1 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "channelId" # Ljava/lang/String; */
/* .param p3, "groupId" # Ljava/lang/String; */
/* .line 539 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/notification/NotificationManagerServiceImpl;->isXmsf(Ljava/lang/String;)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = /* invoke-direct {p0, p2}, Lcom/android/server/notification/NotificationManagerServiceImpl;->isXmsfChannelId(Ljava/lang/String;)Z */
/* if-nez v0, :cond_0 */
v0 = /* invoke-direct {p0, p3}, Lcom/android/server/notification/NotificationManagerServiceImpl;->isXmsfChannelId(Ljava/lang/String;)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 541 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* .line 545 */
} // :cond_1
v0 = android.text.TextUtils .isEmpty ( p2 );
/* if-nez v0, :cond_2 */
/* .line 547 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/notification/NotificationManagerServiceImpl;->checkAllowToDeleteChannel(Ljava/lang/String;Ljava/lang/String;)V */
/* .line 550 */
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
} // .end method
Boolean shouldOnlyAlertNotification ( com.android.server.notification.NotificationRecord p0 ) {
/* .locals 1 */
/* .param p1, "record" # Lcom/android/server/notification/NotificationRecord; */
/* .line 330 */
/* iget-boolean v0, p1, Lcom/android/server/notification/NotificationRecord;->isUpdate:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
(( com.android.server.notification.NotificationRecord ) p1 ).getNotification ( ); // invoke-virtual {p1}, Lcom/android/server/notification/NotificationRecord;->getNotification()Landroid/app/Notification;
/* iget v0, v0, Landroid/app/Notification;->flags:I */
/* and-int/lit8 v0, v0, 0x8 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 331 */
int v0 = 0; // const/4 v0, 0x0
/* .line 333 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
public Boolean skipClearAll ( com.android.server.notification.NotificationRecord p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "record" # Lcom/android/server/notification/NotificationRecord; */
/* .param p2, "reason" # I */
/* .line 338 */
int v0 = 3; // const/4 v0, 0x3
/* if-eq p2, v0, :cond_0 */
/* const/16 v0, 0xb */
/* if-eq p2, v0, :cond_0 */
/* .line 340 */
int v0 = 0; // const/4 v0, 0x0
/* .line 342 */
} // :cond_0
v0 = this.mContext;
(( com.android.server.notification.NotificationRecord ) p1 ).getKey ( ); // invoke-virtual {p1}, Lcom/android/server/notification/NotificationRecord;->getKey()Ljava/lang/String;
v0 = /* invoke-direct {p0, v0, v1}, Lcom/android/server/notification/NotificationManagerServiceImpl;->isUpdatableFocusNotification(Landroid/content/Context;Ljava/lang/String;)Z */
} // .end method
