.class public Lcom/android/server/notification/BarrageListenerServiceImpl;
.super Ljava/lang/Object;
.source "BarrageListenerServiceImpl.java"

# interfaces
.implements Lcom/android/server/notification/BarrageListenerServiceStub;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/notification/BarrageListenerServiceImpl$BHandler;,
        Lcom/android/server/notification/BarrageListenerServiceImpl$SettingsObserver;,
        Lcom/android/server/notification/BarrageListenerServiceImpl$UserSwitchReceiver;
    }
.end annotation


# instance fields
.field private final COMPONENT_BARRAGE:Ljava/lang/String;

.field private final DELAY_FORCE_STOP:I

.field private final GB_BOOSTING:Ljava/lang/String;

.field private final GB_BULLET_CHAT:Ljava/lang/String;

.field private final MI_BARRAGE:Ljava/lang/String;

.field private final TAG:Ljava/lang/String;

.field private mAm:Landroid/app/ActivityManager;

.field private mContext:Landroid/content/Context;

.field private mCurrentUserId:I

.field private mHandler:Landroid/os/Handler;

.field private mInGame:I

.field private mListener:Lcom/android/server/notification/ManagedServices;

.field private mPm:Landroid/content/pm/PackageManager;

.field private mSettingsObserver:Lcom/android/server/notification/BarrageListenerServiceImpl$SettingsObserver;

.field private mTurnOn:I

.field private mUserSwitchReceiver:Lcom/android/server/notification/BarrageListenerServiceImpl$UserSwitchReceiver;


# direct methods
.method static bridge synthetic -$$Nest$fgetmAm(Lcom/android/server/notification/BarrageListenerServiceImpl;)Landroid/app/ActivityManager;
    .locals 0

    iget-object p0, p0, Lcom/android/server/notification/BarrageListenerServiceImpl;->mAm:Landroid/app/ActivityManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/notification/BarrageListenerServiceImpl;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/notification/BarrageListenerServiceImpl;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmCurrentUserId(Lcom/android/server/notification/BarrageListenerServiceImpl;)I
    .locals 0

    iget p0, p0, Lcom/android/server/notification/BarrageListenerServiceImpl;->mCurrentUserId:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/server/notification/BarrageListenerServiceImpl;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/notification/BarrageListenerServiceImpl;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmInGame(Lcom/android/server/notification/BarrageListenerServiceImpl;)I
    .locals 0

    iget p0, p0, Lcom/android/server/notification/BarrageListenerServiceImpl;->mInGame:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmListener(Lcom/android/server/notification/BarrageListenerServiceImpl;)Lcom/android/server/notification/ManagedServices;
    .locals 0

    iget-object p0, p0, Lcom/android/server/notification/BarrageListenerServiceImpl;->mListener:Lcom/android/server/notification/ManagedServices;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSettingsObserver(Lcom/android/server/notification/BarrageListenerServiceImpl;)Lcom/android/server/notification/BarrageListenerServiceImpl$SettingsObserver;
    .locals 0

    iget-object p0, p0, Lcom/android/server/notification/BarrageListenerServiceImpl;->mSettingsObserver:Lcom/android/server/notification/BarrageListenerServiceImpl$SettingsObserver;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmTurnOn(Lcom/android/server/notification/BarrageListenerServiceImpl;)I
    .locals 0

    iget p0, p0, Lcom/android/server/notification/BarrageListenerServiceImpl;->mTurnOn:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmCurrentUserId(Lcom/android/server/notification/BarrageListenerServiceImpl;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/notification/BarrageListenerServiceImpl;->mCurrentUserId:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmInGame(Lcom/android/server/notification/BarrageListenerServiceImpl;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/notification/BarrageListenerServiceImpl;->mInGame:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmTurnOn(Lcom/android/server/notification/BarrageListenerServiceImpl;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/notification/BarrageListenerServiceImpl;->mTurnOn:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const-string v0, "BarrageListenerService"

    iput-object v0, p0, Lcom/android/server/notification/BarrageListenerServiceImpl;->TAG:Ljava/lang/String;

    .line 35
    const-string v0, "gb_bullet_chat"

    iput-object v0, p0, Lcom/android/server/notification/BarrageListenerServiceImpl;->GB_BULLET_CHAT:Ljava/lang/String;

    .line 36
    const-string v0, "gb_boosting"

    iput-object v0, p0, Lcom/android/server/notification/BarrageListenerServiceImpl;->GB_BOOSTING:Ljava/lang/String;

    .line 37
    const-string v0, "com.xiaomi.barrage"

    iput-object v0, p0, Lcom/android/server/notification/BarrageListenerServiceImpl;->MI_BARRAGE:Ljava/lang/String;

    .line 38
    const-string v0, "com.xiaomi.barrage/com.xiaomi.barrage.service.NotificationMonitorService"

    iput-object v0, p0, Lcom/android/server/notification/BarrageListenerServiceImpl;->COMPONENT_BARRAGE:Ljava/lang/String;

    .line 40
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/server/notification/BarrageListenerServiceImpl;->DELAY_FORCE_STOP:I

    return-void
.end method


# virtual methods
.method public init(Landroid/content/Context;Lcom/android/server/notification/ManagedServices;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listeners"    # Lcom/android/server/notification/ManagedServices;

    .line 138
    iput-object p1, p0, Lcom/android/server/notification/BarrageListenerServiceImpl;->mContext:Landroid/content/Context;

    .line 139
    iput-object p2, p0, Lcom/android/server/notification/BarrageListenerServiceImpl;->mListener:Lcom/android/server/notification/ManagedServices;

    .line 140
    new-instance v0, Lcom/android/server/notification/BarrageListenerServiceImpl$BHandler;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/server/notification/BarrageListenerServiceImpl$BHandler;-><init>(Lcom/android/server/notification/BarrageListenerServiceImpl;Lcom/android/server/notification/BarrageListenerServiceImpl$BHandler-IA;)V

    iput-object v0, p0, Lcom/android/server/notification/BarrageListenerServiceImpl;->mHandler:Landroid/os/Handler;

    .line 141
    iget-object v0, p0, Lcom/android/server/notification/BarrageListenerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/notification/BarrageListenerServiceImpl;->mPm:Landroid/content/pm/PackageManager;

    .line 142
    iget-object v0, p0, Lcom/android/server/notification/BarrageListenerServiceImpl;->mContext:Landroid/content/Context;

    const-string v2, "activity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    iput-object v0, p0, Lcom/android/server/notification/BarrageListenerServiceImpl;->mAm:Landroid/app/ActivityManager;

    .line 143
    new-instance v0, Lcom/android/server/notification/BarrageListenerServiceImpl$SettingsObserver;

    iget-object v2, p0, Lcom/android/server/notification/BarrageListenerServiceImpl;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v2}, Lcom/android/server/notification/BarrageListenerServiceImpl$SettingsObserver;-><init>(Lcom/android/server/notification/BarrageListenerServiceImpl;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/server/notification/BarrageListenerServiceImpl;->mSettingsObserver:Lcom/android/server/notification/BarrageListenerServiceImpl$SettingsObserver;

    .line 144
    iget-object v0, p0, Lcom/android/server/notification/BarrageListenerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 145
    .local v0, "resolver":Landroid/content/ContentResolver;
    const-string v2, "gb_bullet_chat"

    invoke-static {v2}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/notification/BarrageListenerServiceImpl;->mSettingsObserver:Lcom/android/server/notification/BarrageListenerServiceImpl$SettingsObserver;

    const/4 v4, 0x0

    const/4 v5, -0x1

    invoke-virtual {v0, v2, v4, v3, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 147
    const-string v2, "gb_boosting"

    invoke-static {v2}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/notification/BarrageListenerServiceImpl;->mSettingsObserver:Lcom/android/server/notification/BarrageListenerServiceImpl$SettingsObserver;

    invoke-virtual {v0, v2, v4, v3, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 149
    iput v4, p0, Lcom/android/server/notification/BarrageListenerServiceImpl;->mCurrentUserId:I

    .line 150
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 151
    .local v2, "filter":Landroid/content/IntentFilter;
    const-string v3, "android.intent.action.USER_SWITCHED"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 152
    new-instance v4, Lcom/android/server/notification/BarrageListenerServiceImpl$UserSwitchReceiver;

    invoke-direct {v4, p0, v1}, Lcom/android/server/notification/BarrageListenerServiceImpl$UserSwitchReceiver;-><init>(Lcom/android/server/notification/BarrageListenerServiceImpl;Lcom/android/server/notification/BarrageListenerServiceImpl$UserSwitchReceiver-IA;)V

    iput-object v4, p0, Lcom/android/server/notification/BarrageListenerServiceImpl;->mUserSwitchReceiver:Lcom/android/server/notification/BarrageListenerServiceImpl$UserSwitchReceiver;

    .line 153
    iget-object v3, p0, Lcom/android/server/notification/BarrageListenerServiceImpl;->mContext:Landroid/content/Context;

    sget-object v5, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v6, v2

    invoke-virtual/range {v3 .. v8}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 155
    return-void
.end method

.method public isAllowStartBarrage(Ljava/lang/String;)Z
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .line 159
    const-string v0, "com.xiaomi.barrage"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    .line 160
    iget v0, p0, Lcom/android/server/notification/BarrageListenerServiceImpl;->mTurnOn:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/server/notification/BarrageListenerServiceImpl;->mInGame:I

    if-eqz v0, :cond_0

    .line 161
    return v1

    .line 163
    :cond_0
    const/4 v0, 0x0

    return v0

    .line 165
    :cond_1
    return v1
.end method
