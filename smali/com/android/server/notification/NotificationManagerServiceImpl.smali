.class public Lcom/android/server/notification/NotificationManagerServiceImpl;
.super Lcom/android/server/notification/NotificationManagerServiceStub;
.source "NotificationManagerServiceImpl.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.notification.NotificationManagerServiceStub$$"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/notification/NotificationManagerServiceImpl$NRStatusImpl;,
        Lcom/android/server/notification/NotificationManagerServiceImpl$NMSVersionImpl;
    }
.end annotation


# static fields
.field private static final BREATHING_LIGHT:Ljava/lang/String; = "breathing_light"

.field protected static final ENABLED_SERVICES_SEPARATOR:Ljava/lang/String; = ":"

.field private static final INVALID_UID:I = -0x1

.field private static final KEY_MEDIA_NOTIFICATION_CLOUD:Ljava/lang/String; = "blacklist"

.field private static final MIUI_SYSTEM_APPS_LIST:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final MODULE_MEDIA_NOTIFICATION_CLOUD_LIST:Ljava/lang/String; = "config_notification_medianotificationcontrol"

.field private static final OTHER_APPS_LIST:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final SPLIT_CHAR:Ljava/lang/String; = "\\|"

.field private static final SYSTEM_APP_MASK:I = 0x81

.field public static final TAG:Ljava/lang/String; = "NotificationManagerServiceImpl"

.field private static final XMSF_CHANNEL_ID_PREFIX:Ljava/lang/String; = "mipush|"

.field private static final XMSF_FAKE_CONDITION_PROVIDER_PATH:Ljava/lang/String; = "xmsf_fake_condition_provider_path"

.field protected static final XMSF_PACKAGE_NAME:Ljava/lang/String; = "com.xiaomi.xmsf"

.field private static allowMediaNotificationCloudDataList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final sAllowToastSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final PRIVACY_INPUT_MODE_PKG_NAME:Ljava/lang/String;

.field private allowMediaNotificationCloudData:Ljava/lang/String;

.field private allowNotificationAccessList:[Ljava/lang/String;

.field private interceptChannelId:[Ljava/lang/String;

.field private interceptListener:[Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mNMS:Lcom/android/server/notification/NotificationManagerService;

.field private mPrivacyInputModePkgName:Ljava/lang/String;


# direct methods
.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/notification/NotificationManagerServiceImpl;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/notification/NotificationManagerServiceImpl;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmPrivacyInputModePkgName(Lcom/android/server/notification/NotificationManagerServiceImpl;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/notification/NotificationManagerServiceImpl;->mPrivacyInputModePkgName:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateCloudData(Lcom/android/server/notification/NotificationManagerServiceImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/notification/NotificationManagerServiceImpl;->updateCloudData()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 105
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/notification/NotificationManagerServiceImpl;->allowMediaNotificationCloudDataList:Ljava/util/List;

    .line 380
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/android/server/notification/NotificationManagerServiceImpl;->sAllowToastSet:Ljava/util/Set;

    .line 381
    const-string v1, "com.lbe.security.miui"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 665
    sget-object v0, Lmiui/content/pm/ExtraPackageManager;->MIUI_SYSTEM_APPS:[Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/android/server/notification/NotificationManagerServiceImpl;->MIUI_SYSTEM_APPS_LIST:Ljava/util/List;

    .line 667
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/notification/NotificationManagerServiceImpl;->OTHER_APPS_LIST:Ljava/util/List;

    .line 670
    const-string v1, "com.xiaomi.wearable"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 671
    const-string v1, "com.xiaomi.hm.health"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 672
    const-string v1, "com.mi.health"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 673
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 89
    invoke-direct {p0}, Lcom/android/server/notification/NotificationManagerServiceStub;-><init>()V

    .line 96
    const-string v0, "miui_privacy_input_pkg_name"

    iput-object v0, p0, Lcom/android/server/notification/NotificationManagerServiceImpl;->PRIVACY_INPUT_MODE_PKG_NAME:Ljava/lang/String;

    .line 97
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/notification/NotificationManagerServiceImpl;->mPrivacyInputModePkgName:Ljava/lang/String;

    return-void
.end method

.method private checkAllowToDeleteChannel(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "channelId"    # Ljava/lang/String;

    .line 579
    invoke-direct {p0, p1}, Lcom/android/server/notification/NotificationManagerServiceImpl;->isCallerXmsfOrSystem(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, p2}, Lcom/android/server/notification/NotificationManagerServiceImpl;->isXmsfChannelId(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 580
    :cond_0
    new-instance v0, Ljava/lang/SecurityException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Pkg "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " cannot delete channel "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " that starts with \'mipush|\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 583
    :cond_1
    :goto_0
    return-void
.end method

.method private checkAppSystemOrNot(Ljava/lang/String;)Z
    .locals 8
    .param p1, "monitorPkg"    # Ljava/lang/String;

    .line 752
    const/4 v0, 0x0

    .line 754
    .local v0, "isSystem":Z
    :try_start_0
    const-class v1, Landroid/content/pm/PackageManagerInternal;

    invoke-static {v1}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Landroid/content/pm/PackageManagerInternal;

    const-wide/16 v4, 0x0

    const/16 v6, 0x3e8

    const/4 v7, 0x0

    .line 755
    move-object v3, p1

    invoke-virtual/range {v2 .. v7}, Landroid/content/pm/PackageManagerInternal;->getApplicationInfo(Ljava/lang/String;JII)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 756
    .local v1, "ai":Landroid/content/pm/ApplicationInfo;
    if-eqz v1, :cond_0

    .line 757
    invoke-virtual {v1}, Landroid/content/pm/ApplicationInfo;->isSystemApp()Z

    move-result v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v2

    .line 761
    .end local v1    # "ai":Landroid/content/pm/ApplicationInfo;
    :cond_0
    goto :goto_0

    .line 759
    :catch_0
    move-exception v1

    .line 760
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "NotificationManagerServiceImpl"

    const-string v3, "checkAppSystemOrNot : "

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 762
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return v0
.end method

.method private checkCallerIsXmsf(Ljava/lang/String;Ljava/lang/String;II)Z
    .locals 2
    .param p1, "callingPkg"    # Ljava/lang/String;
    .param p2, "targetPkg"    # Ljava/lang/String;
    .param p3, "callingUid"    # I
    .param p4, "userId"    # I

    .line 464
    invoke-direct {p0, p1}, Lcom/android/server/notification/NotificationManagerServiceImpl;->isXmsf(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    invoke-direct {p0, p2}, Lcom/android/server/notification/NotificationManagerServiceImpl;->isXmsf(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 468
    invoke-direct {p0}, Lcom/android/server/notification/NotificationManagerServiceImpl;->isCallerSystem()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p3, p4}, Lcom/android/server/notification/NotificationManagerServiceImpl;->checkCallerIsXmsfInternal(II)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v1, 0x1

    :cond_1
    return v1

    .line 470
    :cond_2
    return v1
.end method

.method private checkCallerIsXmsfInternal(II)Z
    .locals 7
    .param p1, "callingUid"    # I
    .param p2, "userId"    # I

    .line 475
    :try_start_0
    const-class v0, Landroid/content/pm/PackageManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/content/pm/PackageManagerInternal;

    const-string v2, "com.xiaomi.xmsf"

    const-wide/16 v3, 0x0

    .line 476
    move v5, p1

    move v6, p2

    invoke-virtual/range {v1 .. v6}, Landroid/content/pm/PackageManagerInternal;->getApplicationInfo(Ljava/lang/String;JII)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 477
    .local v0, "ai":Landroid/content/pm/ApplicationInfo;
    if-eqz v0, :cond_0

    iget v1, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v1, p1}, Landroid/os/UserHandle;->isSameApp(II)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, v0, Landroid/content/pm/ApplicationInfo;->flags:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    and-int/lit16 v1, v1, 0x81

    if-eqz v1, :cond_0

    .line 479
    const/4 v1, 0x1

    return v1

    .line 483
    .end local v0    # "ai":Landroid/content/pm/ApplicationInfo;
    :cond_0
    goto :goto_0

    .line 481
    :catch_0
    move-exception v0

    .line 484
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method private checkInterceptListenerForMIUIInternal(Landroid/service/notification/StatusBarNotification;Ljava/lang/String;)Z
    .locals 3
    .param p1, "sbn"    # Landroid/service/notification/StatusBarNotification;
    .param p2, "listener"    # Ljava/lang/String;

    .line 716
    invoke-direct {p0, p2}, Lcom/android/server/notification/NotificationManagerServiceImpl;->checkAppSystemOrNot(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    sget-object v0, Lcom/android/server/notification/NotificationManagerServiceImpl;->MIUI_SYSTEM_APPS_LIST:Ljava/util/List;

    .line 717
    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/android/server/notification/NotificationManagerServiceImpl;->OTHER_APPS_LIST:Ljava/util/List;

    .line 718
    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 719
    const/4 v0, 0x0

    invoke-static {v0, p2, v1}, Lmiui/content/pm/PreloadedAppPolicy;->isProtectedDataApp(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 720
    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getUid()I

    move-result v2

    invoke-direct {p0, v0, v2}, Lcom/android/server/notification/NotificationManagerServiceImpl;->checkNotificationMasked(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    nop

    :goto_0
    move v0, v1

    .line 721
    .local v0, "intercept":Z
    if-eqz v0, :cond_1

    .line 722
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "checkInterceptListenerForMIUIInternal pkg:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "NotificationManagerServiceImpl"

    invoke-static {v2, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 724
    :cond_1
    return v0
.end method

.method private checkInterceptListenerForXiaomiInternal(Landroid/service/notification/StatusBarNotification;Landroid/content/ComponentName;)Z
    .locals 4
    .param p1, "sbn"    # Landroid/service/notification/StatusBarNotification;
    .param p2, "component"    # Landroid/content/ComponentName;

    .line 697
    const/4 v0, 0x0

    .line 698
    .local v0, "intercept":Z
    iget-object v1, p0, Lcom/android/server/notification/NotificationManagerServiceImpl;->interceptListener:[Ljava/lang/String;

    invoke-virtual {p2}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/internal/util/ArrayUtils;->contains([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 699
    iget-object v1, p0, Lcom/android/server/notification/NotificationManagerServiceImpl;->interceptChannelId:[Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 700
    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Notification;->getChannelId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 699
    invoke-static {v1, v2}, Lcom/android/internal/util/ArrayUtils;->contains([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 701
    const/4 v0, 0x1

    .line 702
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "checkInterceptListenerForXiaomiInternal pkg:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "listener:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 703
    invoke-virtual {p2}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 702
    const-string v2, "NotificationManagerServiceImpl"

    invoke-static {v2, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 706
    :cond_0
    return v0
.end method

.method private checkNotificationMasked(Ljava/lang/String;I)Z
    .locals 5
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "uid"    # I

    .line 735
    const-string/jumbo v0, "security"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 736
    .local v0, "b":Landroid/os/IBinder;
    const/4 v1, 0x0

    .line 737
    .local v1, "result":Z
    if-eqz v0, :cond_1

    .line 739
    :try_start_0
    invoke-static {p2}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v2

    .line 740
    .local v2, "userId":I
    invoke-static {v0}, Lmiui/security/ISecurityManager$Stub;->asInterface(Landroid/os/IBinder;)Lmiui/security/ISecurityManager;

    move-result-object v3

    .line 741
    .local v3, "service":Lmiui/security/ISecurityManager;
    invoke-interface {v3, v2}, Lmiui/security/ISecurityManager;->haveAccessControlPassword(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 742
    invoke-interface {v3, p1, v2}, Lmiui/security/ISecurityManager;->getApplicationAccessControlEnabledAsUser(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 743
    invoke-interface {v3, p1, v2}, Lmiui/security/ISecurityManager;->getApplicationMaskNotificationEnabledAsUser(Ljava/lang/String;I)Z

    move-result v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :goto_0
    move v1, v4

    .line 746
    .end local v2    # "userId":I
    .end local v3    # "service":Lmiui/security/ISecurityManager;
    goto :goto_1

    .line 744
    :catch_0
    move-exception v2

    .line 745
    .local v2, "e":Ljava/lang/Exception;
    const-string v3, "NotificationManagerServiceImpl"

    const-string v4, "check notification masked error: "

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 748
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_1
    return v1
.end method

.method public static getMediaNotificationDataCloudConversion(Ljava/lang/String;)Ljava/util/List;
    .locals 4
    .param p0, "allowMediaNotificationCloudData"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1000
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1003
    .local v0, "mediationNotificationDataFromCloud":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :try_start_0
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1004
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 1005
    .local v1, "dataConversion":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 1006
    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1005
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .end local v2    # "i":I
    :cond_0
    goto :goto_1

    .line 1009
    .end local v1    # "dataConversion":Lorg/json/JSONArray;
    :cond_1
    sget-object v1, Lcom/android/server/notification/NotificationManagerServiceImpl;->allowMediaNotificationCloudDataList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1013
    :goto_1
    goto :goto_2

    .line 1011
    :catch_0
    move-exception v1

    .line 1012
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "NotificationManagerServiceImpl"

    const-string v3, "Exception when get getMediaNotificationDataCloudConversion :"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1014
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_2
    return-object v0
.end method

.method private getUidByPkg(Ljava/lang/String;I)I
    .locals 4
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 586
    const/4 v0, -0x1

    .line 588
    .local v0, "uid":I
    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 589
    const-class v1, Landroid/content/pm/PackageManagerInternal;

    invoke-static {v1}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/PackageManagerInternal;

    .line 590
    const-wide/16 v2, 0x0

    invoke-virtual {v1, p1, v2, v3, p2}, Landroid/content/pm/PackageManagerInternal;->getPackageUid(Ljava/lang/String;JI)I

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 594
    :cond_0
    goto :goto_0

    .line 592
    :catch_0
    move-exception v1

    .line 595
    :goto_0
    return v0
.end method

.method private hasProgress(Landroid/app/Notification;)Z
    .locals 4
    .param p1, "n"    # Landroid/app/Notification;

    .line 617
    iget-object v0, p1, Landroid/app/Notification;->extras:Landroid/os/Bundle;

    const-string v1, "android.progress"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    const/4 v2, 0x0

    if-eqz v0, :cond_3

    iget-object v0, p1, Landroid/app/Notification;->extras:Landroid/os/Bundle;

    .line 618
    const-string v3, "android.progressMax"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 622
    :cond_0
    iget-object v0, p1, Landroid/app/Notification;->extras:Landroid/os/Bundle;

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    .line 623
    return v2

    .line 625
    :cond_1
    iget-object v0, p1, Landroid/app/Notification;->extras:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iget-object v1, p1, Landroid/app/Notification;->extras:Landroid/os/Bundle;

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    if-eq v0, v1, :cond_2

    const/4 v2, 0x1

    :cond_2
    return v2

    .line 619
    :cond_3
    :goto_0
    return v2
.end method

.method private isAudioCanPlay(Landroid/media/AudioManager;)Z
    .locals 2
    .param p1, "audioManager"    # Landroid/media/AudioManager;

    .line 259
    invoke-virtual {p1}, Landroid/media/AudioManager;->getRingerModeInternal()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 260
    invoke-virtual {p1}, Landroid/media/AudioManager;->getRingerModeInternal()I

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 259
    :goto_0
    return v1
.end method

.method private isCallerAndroid(Ljava/lang/String;I)Z
    .locals 1
    .param p1, "callingPkg"    # Ljava/lang/String;
    .param p2, "callingUid"    # I

    .line 494
    invoke-direct {p0, p2}, Lcom/android/server/notification/NotificationManagerServiceImpl;->isUidSystemOrPhone(I)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 495
    const-string v0, "android"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 494
    :goto_0
    return v0
.end method

.method private isCallerSecurityCenter(Ljava/lang/String;I)Z
    .locals 2
    .param p1, "callingPkg"    # Ljava/lang/String;
    .param p2, "uid"    # I

    .line 502
    invoke-static {p2}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v0

    .line 503
    .local v0, "appid":I
    const/16 v1, 0x3e8

    if-ne v0, v1, :cond_0

    .line 504
    const-string v1, "com.miui.securitycenter"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 503
    :goto_0
    return v1
.end method

.method private isCallerSystem()Z
    .locals 2

    .line 443
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-static {v0}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v0

    .line 444
    .local v0, "appId":I
    const/16 v1, 0x3e8

    if-ne v0, v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private isCallerXmsfOrSystem(Ljava/lang/String;)Z
    .locals 1
    .param p1, "callingPkg"    # Ljava/lang/String;

    .line 435
    invoke-direct {p0, p1}, Lcom/android/server/notification/NotificationManagerServiceImpl;->isXmsf(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/android/server/notification/NotificationManagerServiceImpl;->isCallerSystem()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private isCallingSystem(IILjava/lang/String;)Z
    .locals 6
    .param p1, "userId"    # I
    .param p2, "uid"    # I
    .param p3, "callingPkg"    # Ljava/lang/String;

    .line 630
    const-string v0, "NotificationManagerServiceImpl"

    invoke-static {p2}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v1

    .line 631
    .local v1, "appid":I
    const/16 v2, 0x3e8

    if-eq v1, v2, :cond_2

    const/16 v2, 0x3e9

    if-eq v1, v2, :cond_2

    if-eqz p2, :cond_2

    const/16 v2, 0x7d0

    if-ne p2, v2, :cond_0

    goto :goto_0

    .line 637
    :cond_0
    const/4 v2, 0x0

    :try_start_0
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v3

    const-wide/16 v4, 0x0

    invoke-interface {v3, p3, v4, v5, p1}, Landroid/content/pm/IPackageManager;->getApplicationInfo(Ljava/lang/String;JI)Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    .line 638
    .local v3, "ai":Landroid/content/pm/ApplicationInfo;
    if-nez v3, :cond_1

    .line 639
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unknown package "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 640
    return v2

    .line 642
    :cond_1
    invoke-virtual {v3}, Landroid/content/pm/ApplicationInfo;->isSystemApp()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 643
    .end local v3    # "ai":Landroid/content/pm/ApplicationInfo;
    :catch_0
    move-exception v3

    .line 644
    .local v3, "e":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isCallingSystem error: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 646
    .end local v3    # "e":Ljava/lang/Exception;
    return v2

    .line 633
    :cond_2
    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method private isPackageDistractionRestrictionForUser(Ljava/lang/String;I)Z
    .locals 4
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "uid"    # I

    .line 407
    invoke-static {p2}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v0

    .line 409
    .local v0, "userId":I
    const/4 v1, 0x0

    :try_start_0
    const-class v2, Landroid/content/pm/PackageManagerInternal;

    invoke-static {v2}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/PackageManagerInternal;

    .line 411
    .local v2, "pmi":Landroid/content/pm/PackageManagerInternal;
    invoke-virtual {v2, p1, v0}, Landroid/content/pm/PackageManagerInternal;->getDistractingPackageRestrictions(Ljava/lang/String;I)I

    move-result v3
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    and-int/lit8 v3, v3, 0x2

    if-eqz v3, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1

    .line 413
    .end local v2    # "pmi":Landroid/content/pm/PackageManagerInternal;
    :catch_0
    move-exception v2

    .line 415
    .local v2, "ex":Ljava/lang/IllegalArgumentException;
    return v1
.end method

.method private isSilenceMode(Lcom/android/server/notification/NotificationRecord;)Z
    .locals 5
    .param p1, "record"    # Lcom/android/server/notification/NotificationRecord;

    .line 264
    invoke-virtual {p1}, Lcom/android/server/notification/NotificationRecord;->getSbn()Landroid/service/notification/StatusBarNotification;

    move-result-object v0

    invoke-virtual {v0}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v0

    iget-object v0, v0, Landroid/app/Notification;->extraNotification:Landroid/app/MiuiNotification;

    .line 265
    .local v0, "extraNotification":Landroid/app/MiuiNotification;
    nop

    .line 267
    invoke-virtual {p1}, Lcom/android/server/notification/NotificationRecord;->getSbn()Landroid/service/notification/StatusBarNotification;

    move-result-object v1

    invoke-virtual {v1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 268
    if-nez v0, :cond_0

    const/4 v2, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Landroid/app/MiuiNotification;->getTargetPkg()Ljava/lang/CharSequence;

    move-result-object v2

    .line 265
    :goto_0
    const/4 v3, 0x5

    const/4 v4, 0x0

    invoke-static {v3, v4, v1, v2}, Lmiui/util/QuietUtils;->checkQuiet(IILjava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v1

    return v1
.end method

.method private isUidSystemOrPhone(I)Z
    .locals 2
    .param p1, "uid"    # I

    .line 447
    invoke-static {p1}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v0

    .line 448
    .local v0, "appid":I
    const/16 v1, 0x3e8

    if-eq v0, v1, :cond_1

    const/16 v1, 0x3e9

    if-eq v0, v1, :cond_1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    return v1
.end method

.method private isUpdatableFocusNotification(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "key"    # Ljava/lang/String;

    .line 347
    nop

    .line 348
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "updatable_focus_notifs"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 349
    .local v0, "settingsValue":Ljava/lang/String;
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 350
    .local v1, "jsonArray":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 351
    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONArray;->optString(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v3, :cond_0

    .line 352
    const/4 v3, 0x1

    return v3

    .line 350
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 357
    .end local v0    # "settingsValue":Ljava/lang/String;
    .end local v1    # "jsonArray":Lorg/json/JSONArray;
    .end local v2    # "i":I
    :cond_1
    goto :goto_1

    .line 355
    :catch_0
    move-exception v0

    .line 356
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "NotificationManagerServiceImpl"

    const-string/jumbo v2, "updatableFocus jsonArray error"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 358
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    const/4 v0, 0x0

    return v0
.end method

.method private isXmsf(Ljava/lang/String;)Z
    .locals 1
    .param p1, "pkg"    # Ljava/lang/String;

    .line 439
    const-string v0, "com.xiaomi.xmsf"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private isXmsfChannelId(Ljava/lang/String;)Z
    .locals 1
    .param p1, "channelId"    # Ljava/lang/String;

    .line 453
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "mipush|"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private registerCloudDataResolver(Landroid/os/Handler;)V
    .locals 4
    .param p1, "handler"    # Landroid/os/Handler;

    .line 816
    iget-object v0, p0, Lcom/android/server/notification/NotificationManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 817
    invoke-static {}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataNotifyUri()Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lcom/android/server/notification/NotificationManagerServiceImpl$2;

    invoke-direct {v2, p0, p1}, Lcom/android/server/notification/NotificationManagerServiceImpl$2;-><init>(Lcom/android/server/notification/NotificationManagerServiceImpl;Landroid/os/Handler;)V

    .line 816
    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 825
    return-void
.end method

.method private registerPrivacyInputMode(Landroid/os/Handler;)V
    .locals 4
    .param p1, "handler"    # Landroid/os/Handler;

    .line 802
    iget-object v0, p0, Lcom/android/server/notification/NotificationManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 803
    const-string v1, "miui_privacy_input_pkg_name"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lcom/android/server/notification/NotificationManagerServiceImpl$1;

    invoke-direct {v2, p0, p1}, Lcom/android/server/notification/NotificationManagerServiceImpl$1;-><init>(Lcom/android/server/notification/NotificationManagerServiceImpl;Landroid/os/Handler;)V

    .line 802
    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 813
    return-void
.end method

.method private shouldSkipFrequentlyVib(Lcom/android/server/notification/NotificationRecord;Lcom/android/server/notification/NotificationUsageStats$AggregatedStats;)Z
    .locals 3
    .param p1, "record"    # Lcom/android/server/notification/NotificationRecord;
    .param p2, "mStats"    # Lcom/android/server/notification/NotificationUsageStats$AggregatedStats;

    .line 657
    iget-object v0, p2, Lcom/android/server/notification/NotificationUsageStats$AggregatedStats;->vibRate:Ljava/lang/Object;

    check-cast v0, Lcom/android/server/notification/VibRateLimiter;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/android/server/notification/VibRateLimiter;->shouldRateLimitVib(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 658
    iget v0, p2, Lcom/android/server/notification/NotificationUsageStats$AggregatedStats;->numVibViolations:I

    const/4 v1, 0x1

    add-int/2addr v0, v1

    iput v0, p2, Lcom/android/server/notification/NotificationUsageStats$AggregatedStats;->numVibViolations:I

    .line 659
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cancel the recent frequently vibration in 15s "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/android/server/notification/NotificationRecord;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "NotificationManagerServiceImpl"

    invoke-static {v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 660
    return v1

    .line 662
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private updateCloudData()V
    .locals 4

    .line 991
    iget-object v0, p0, Lcom/android/server/notification/NotificationManagerServiceImpl;->mContext:Landroid/content/Context;

    .line 992
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 991
    const-string v1, "config_notification_medianotificationcontrol"

    const-string v2, "blacklist"

    const-string v3, ""

    invoke-static {v0, v1, v2, v3}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 994
    .local v0, "updateNotificationCloudData":Ljava/lang/String;
    nop

    .line 995
    invoke-static {v0}, Lcom/android/server/notification/NotificationManagerServiceImpl;->getMediaNotificationDataCloudConversion(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    sput-object v1, Lcom/android/server/notification/NotificationManagerServiceImpl;->allowMediaNotificationCloudDataList:Ljava/util/List;

    .line 996
    return-void
.end method


# virtual methods
.method public adjustVibration(Landroid/os/VibrationEffect;)Landroid/os/VibrationEffect;
    .locals 5
    .param p1, "vibration"    # Landroid/os/VibrationEffect;

    .line 284
    if-eqz p1, :cond_0

    instance-of v0, p1, Landroid/os/VibrationEffect$Composed;

    if-eqz v0, :cond_0

    .line 285
    move-object v0, p1

    check-cast v0, Landroid/os/VibrationEffect$Composed;

    .line 286
    .local v0, "currentVibration":Landroid/os/VibrationEffect$Composed;
    invoke-virtual {v0}, Landroid/os/VibrationEffect$Composed;->getSegments()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 287
    invoke-virtual {v0}, Landroid/os/VibrationEffect$Composed;->getSegments()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x4

    if-le v1, v2, :cond_0

    .line 288
    new-instance v1, Landroid/os/VibrationEffect$Composed;

    .line 289
    invoke-virtual {v0}, Landroid/os/VibrationEffect$Composed;->getSegments()Ljava/util/List;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v4, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/VibrationEffect$Composed;->getRepeatIndex()I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/os/VibrationEffect$Composed;-><init>(Ljava/util/List;I)V

    move-object p1, v1

    .line 292
    .end local v0    # "currentVibration":Landroid/os/VibrationEffect$Composed;
    :cond_0
    return-object p1
.end method

.method public allowMiuiDefaultApprovedServices(Lcom/android/server/notification/ManagedServices;Z)V
    .locals 11
    .param p1, "mListeners"    # Lcom/android/server/notification/ManagedServices;
    .param p2, "DBG"    # Z

    .line 137
    invoke-virtual {p0}, Lcom/android/server/notification/NotificationManagerServiceImpl;->getMiuiDefaultListerWithVersion()Landroid/util/Pair;

    move-result-object v0

    .line 138
    .local v0, "listenerWithVersion":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/String;>;"
    const-string v1, "NotificationManagerServiceImpl"

    if-eqz p2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "allowDefaultApprovedServices: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " second "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    :cond_0
    iget-object v2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-gez v2, :cond_1

    return-void

    .line 141
    :cond_1
    iget-object v2, p1, Lcom/android/server/notification/ManagedServices;->mVersionStub:Lcom/android/server/notification/NotificationManagerServiceStub$NMSVersionStub;

    invoke-interface {v2}, Lcom/android/server/notification/NotificationManagerServiceStub$NMSVersionStub;->getMiuiVersion()I

    move-result v2

    .line 142
    .local v2, "miuiVersion":I
    if-eqz p2, :cond_2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "allowDefaultApprovedServices: miuiVersion "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    :cond_2
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-lt v2, v1, :cond_3

    return-void

    .line 145
    :cond_3
    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    .line 146
    .local v1, "defaultListenerAccess":Ljava/lang/String;
    new-instance v3, Landroid/util/ArraySet;

    invoke-direct {v3}, Landroid/util/ArraySet;-><init>()V

    .line 147
    .local v3, "defaultListeners":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Landroid/content/ComponentName;>;"
    const/4 v4, 0x0

    if-eqz v1, :cond_7

    .line 148
    nop

    .line 149
    const-string v5, ":"

    invoke-virtual {v1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 150
    .local v5, "listeners":[Ljava/lang/String;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    array-length v7, v5

    if-ge v6, v7, :cond_6

    .line 151
    aget-object v7, v5, v6

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 152
    goto :goto_2

    .line 154
    :cond_4
    aget-object v7, v5, v6

    .line 155
    const/high16 v8, 0xc0000

    invoke-virtual {p1, v7, v8, v4}, Lcom/android/server/notification/ManagedServices;->queryPackageForServices(Ljava/lang/String;II)Landroid/util/ArraySet;

    move-result-object v7

    .line 158
    .local v7, "approvedListeners":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Landroid/content/ComponentName;>;"
    const/4 v8, 0x0

    .local v8, "k":I
    :goto_1
    invoke-virtual {v7}, Landroid/util/ArraySet;->size()I

    move-result v9

    if-ge v8, v9, :cond_5

    .line 159
    invoke-virtual {v7, v8}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/ComponentName;

    .line 160
    .local v9, "cn":Landroid/content/ComponentName;
    invoke-virtual {v9}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, Lcom/android/server/notification/ManagedServices;->addDefaultComponentOrPackage(Ljava/lang/String;)V

    .line 161
    invoke-virtual {v3, v9}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 158
    .end local v9    # "cn":Landroid/content/ComponentName;
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 150
    .end local v7    # "approvedListeners":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Landroid/content/ComponentName;>;"
    .end local v8    # "k":I
    :cond_5
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 164
    .end local v6    # "i":I
    :cond_6
    iget-object v6, p1, Lcom/android/server/notification/ManagedServices;->mVersionStub:Lcom/android/server/notification/NotificationManagerServiceStub$NMSVersionStub;

    iget-object v7, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-interface {v6, v7}, Lcom/android/server/notification/NotificationManagerServiceStub$NMSVersionStub;->setMiuiVersion(I)V

    .line 167
    .end local v5    # "listeners":[Ljava/lang/String;
    :cond_7
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_3
    invoke-virtual {v3}, Landroid/util/ArraySet;->size()I

    move-result v6

    if-ge v5, v6, :cond_8

    .line 168
    invoke-virtual {v3, v5}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/ComponentName;

    .line 169
    .local v6, "cn":Landroid/content/ComponentName;
    sget-object v7, Lcom/android/server/notification/NotificationManagerServiceProxy;->allowNotificationListener:Lcom/xiaomi/reflect/RefMethod;

    iget-object v8, p0, Lcom/android/server/notification/NotificationManagerServiceImpl;->mNMS:Lcom/android/server/notification/NotificationManagerService;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    filled-new-array {v9, v6}, [Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Lcom/xiaomi/reflect/RefMethod;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    .end local v6    # "cn":Landroid/content/ComponentName;
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 171
    .end local v5    # "i":I
    :cond_8
    return-void
.end method

.method public buzzBeepBlinkForNotification(Ljava/lang/String;ILjava/lang/Object;)V
    .locals 3
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "buzzBeepBlink"    # I
    .param p3, "mNotificationLock"    # Ljava/lang/Object;

    .line 205
    sget-object v0, Lcom/android/server/notification/NotificationManagerServiceProxy;->checkCallerIsSystem:Lcom/xiaomi/reflect/RefMethod;

    iget-object v1, p0, Lcom/android/server/notification/NotificationManagerServiceImpl;->mNMS:Lcom/android/server/notification/NotificationManagerService;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/xiaomi/reflect/RefMethod;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 206
    monitor-enter p3

    .line 207
    :try_start_0
    iget-object v0, p0, Lcom/android/server/notification/NotificationManagerServiceImpl;->mNMS:Lcom/android/server/notification/NotificationManagerService;

    iget-object v0, v0, Lcom/android/server/notification/NotificationManagerService;->mNotificationsByKey:Landroid/util/ArrayMap;

    invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/notification/NotificationRecord;

    .line 208
    .local v0, "record":Lcom/android/server/notification/NotificationRecord;
    if-eqz v0, :cond_0

    .line 209
    iget-object v1, v0, Lcom/android/server/notification/NotificationRecord;->mNRStatusStub:Lcom/android/server/notification/NotificationManagerServiceStub$NRStatusStub;

    invoke-interface {v1, p2}, Lcom/android/server/notification/NotificationManagerServiceStub$NRStatusStub;->setStatusBarAllowAlert(I)V

    .line 210
    iget-object v1, p0, Lcom/android/server/notification/NotificationManagerServiceImpl;->mNMS:Lcom/android/server/notification/NotificationManagerService;

    invoke-virtual {v1, v0}, Lcom/android/server/notification/NotificationManagerService;->buzzBeepBlinkLocked(Lcom/android/server/notification/NotificationRecord;)I

    .line 212
    .end local v0    # "record":Lcom/android/server/notification/NotificationRecord;
    :cond_0
    monitor-exit p3

    .line 213
    return-void

    .line 212
    :catchall_0
    move-exception v0

    monitor-exit p3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public calculateAudiblyAlerted(Landroid/media/AudioManager;Lcom/android/server/notification/NotificationRecord;Lcom/android/server/notification/NotificationManagerService;)V
    .locals 10
    .param p1, "audioManager"    # Landroid/media/AudioManager;
    .param p2, "record"    # Lcom/android/server/notification/NotificationRecord;
    .param p3, "mNm"    # Lcom/android/server/notification/NotificationManagerService;

    .line 305
    const/4 v0, 0x0

    .line 306
    .local v0, "buzz":Z
    const/4 v1, 0x0

    .line 307
    .local v1, "beep":Z
    invoke-virtual {p2}, Lcom/android/server/notification/NotificationRecord;->getImportance()I

    move-result v2

    const/4 v3, 0x3

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-lt v2, v3, :cond_0

    move v2, v4

    goto :goto_0

    :cond_0
    move v2, v5

    .line 308
    .local v2, "aboveThreshold":Z
    :goto_0
    invoke-virtual {p2}, Lcom/android/server/notification/NotificationRecord;->getSound()Landroid/net/Uri;

    move-result-object v3

    .line 309
    .local v3, "soundUri":Landroid/net/Uri;
    invoke-virtual {p2}, Lcom/android/server/notification/NotificationRecord;->getVibration()Landroid/os/VibrationEffect;

    move-result-object v6

    .line 310
    .local v6, "vibration":Landroid/os/VibrationEffect;
    if-eqz v6, :cond_1

    move v7, v4

    goto :goto_1

    :cond_1
    move v7, v5

    .line 311
    .local v7, "hasValidVibrate":Z
    :goto_1
    if-eqz v2, :cond_5

    if-eqz p1, :cond_5

    invoke-virtual {p0, p2}, Lcom/android/server/notification/NotificationManagerServiceImpl;->shouldOnlyAlertNotification(Lcom/android/server/notification/NotificationRecord;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 312
    if-eqz v3, :cond_2

    sget-object v8, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    invoke-virtual {v8, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    move v8, v4

    goto :goto_2

    :cond_2
    move v8, v5

    .line 313
    .local v8, "hasValidSound":Z
    :goto_2
    if-eqz v8, :cond_3

    .line 314
    const/4 v1, 0x1

    .line 316
    :cond_3
    nop

    .line 317
    invoke-virtual {p1}, Landroid/media/AudioManager;->getRingerModeInternal()I

    move-result v9

    if-nez v9, :cond_4

    move v9, v4

    goto :goto_3

    :cond_4
    move v9, v5

    .line 319
    .local v9, "ringerModeSilent":Z
    :goto_3
    if-eqz v7, :cond_5

    if-nez v9, :cond_5

    .line 320
    const/4 v0, 0x1

    .line 324
    .end local v8    # "hasValidSound":Z
    .end local v9    # "ringerModeSilent":Z
    :cond_5
    if-eqz v1, :cond_6

    const/4 v8, 0x2

    goto :goto_4

    :cond_6
    move v8, v5

    :goto_4
    or-int/2addr v8, v0

    .line 325
    .local v8, "buzzBeepBlink":I
    invoke-virtual {p2}, Lcom/android/server/notification/NotificationRecord;->getBuzzBeepBlinkCode()I

    move-result v9

    or-int/2addr v9, v8

    invoke-virtual {p2, v9}, Lcom/android/server/notification/NotificationRecord;->setBuzzBeepBlinkCode(I)V

    .line 326
    if-nez v0, :cond_8

    if-eqz v1, :cond_7

    goto :goto_5

    :cond_7
    move v4, v5

    :cond_8
    :goto_5
    invoke-virtual {p2, v4}, Lcom/android/server/notification/NotificationRecord;->setAudiblyAlerted(Z)V

    .line 327
    return-void
.end method

.method public checkAllowToCreateChannels(Ljava/lang/String;Landroid/content/pm/ParceledListSlice;)V
    .locals 6
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "channelsList"    # Landroid/content/pm/ParceledListSlice;

    .line 422
    invoke-direct {p0, p1}, Lcom/android/server/notification/NotificationManagerServiceImpl;->isCallerXmsfOrSystem(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 423
    invoke-virtual {p2}, Landroid/content/pm/ParceledListSlice;->getList()Ljava/util/List;

    move-result-object v0

    .line 424
    .local v0, "channels":Ljava/util/List;, "Ljava/util/List<Landroid/app/NotificationChannel;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 425
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/NotificationChannel;

    .line 426
    .local v2, "channel":Landroid/app/NotificationChannel;
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/app/NotificationChannel;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/android/server/notification/NotificationManagerServiceImpl;->isXmsfChannelId(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    goto :goto_1

    .line 427
    :cond_0
    new-instance v3, Ljava/lang/SecurityException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Pkg "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " cannot create channels, because "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 428
    invoke-virtual {v2}, Landroid/app/NotificationChannel;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " starts with \'mipush|\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 424
    .end local v2    # "channel":Landroid/app/NotificationChannel;
    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 432
    .end local v0    # "channels":Ljava/util/List;, "Ljava/util/List<Landroid/app/NotificationChannel;>;"
    .end local v1    # "i":I
    :cond_2
    return-void
.end method

.method public checkCallerIsXmsf()Z
    .locals 2

    .line 532
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/android/server/notification/NotificationManagerServiceImpl;->checkCallerIsXmsfInternal(II)Z

    move-result v0

    return v0
.end method

.method public checkFullScreenIntent(Landroid/app/Notification;Landroid/app/AppOpsManager;ILjava/lang/String;)V
    .locals 7
    .param p1, "notification"    # Landroid/app/Notification;
    .param p2, "appOpsManager"    # Landroid/app/AppOpsManager;
    .param p3, "uid"    # I
    .param p4, "packageName"    # Ljava/lang/String;

    .line 364
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v0, :cond_0

    iget-object v0, p1, Landroid/app/Notification;->fullScreenIntent:Landroid/app/PendingIntent;

    if-eqz v0, :cond_0

    .line 367
    const/16 v2, 0x2725

    const/4 v5, 0x0

    const-string v6, "NotificationManagementImpl#checkFullScreenIntent"

    move-object v1, p2

    move v3, p3

    move-object v4, p4

    invoke-virtual/range {v1 .. v6}, Landroid/app/AppOpsManager;->noteOpNoThrow(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 369
    .local v0, "mode":I
    if-eqz v0, :cond_0

    .line 370
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MIUILOG- Permission Denied Activity : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Landroid/app/Notification;->fullScreenIntent:Landroid/app/PendingIntent;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " pkg : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " uid : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "NotificationManagerServiceImpl"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 372
    const/4 v1, 0x0

    iput-object v1, p1, Landroid/app/Notification;->fullScreenIntent:Landroid/app/PendingIntent;

    .line 375
    .end local v0    # "mode":I
    :cond_0
    return-void
.end method

.method public checkInterceptListener(Landroid/service/notification/StatusBarNotification;Landroid/content/ComponentName;)Z
    .locals 1
    .param p1, "sbn"    # Landroid/service/notification/StatusBarNotification;
    .param p2, "component"    # Landroid/content/ComponentName;

    .line 684
    invoke-direct {p0, p1, p2}, Lcom/android/server/notification/NotificationManagerServiceImpl;->checkInterceptListenerForXiaomiInternal(Landroid/service/notification/StatusBarNotification;Landroid/content/ComponentName;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 685
    invoke-virtual {p2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/android/server/notification/NotificationManagerServiceImpl;->checkInterceptListenerForMIUIInternal(Landroid/service/notification/StatusBarNotification;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 684
    :goto_1
    return v0
.end method

.method public checkIsXmsfFakeConditionProviderEnabled(Ljava/lang/String;)Z
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .line 459
    const-string/jumbo v0, "xmsf_fake_condition_provider_path"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/server/notification/NotificationManagerServiceImpl;->checkCallerIsXmsf()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public checkMediaNotificationControl(ILjava/lang/String;Lcom/android/server/notification/PreferencesHelper;)Z
    .locals 4
    .param p1, "uid"    # I
    .param p2, "pkg"    # Ljava/lang/String;
    .param p3, "mPreferencesHelper"    # Lcom/android/server/notification/PreferencesHelper;

    .line 960
    const/4 v0, 0x1

    .line 961
    .local v0, "isAllowMediaNotification":Z
    invoke-virtual {p3, p2, p1}, Lcom/android/server/notification/PreferencesHelper;->getMediaNotificationsEnabled(Ljava/lang/String;I)Z

    move-result v1

    const-string v2, "checkMediaNotificationControl pkg:"

    const-string v3, "NotificationManagerServiceImpl"

    if-eqz v1, :cond_0

    .line 962
    sget-object v1, Lcom/android/server/notification/NotificationManagerServiceImpl;->allowMediaNotificationCloudDataList:Ljava/util/List;

    invoke-static {v1, p2}, Lcom/android/internal/util/ArrayUtils;->contains(Ljava/util/Collection;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 963
    const/4 v0, 0x0

    .line 964
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " This media notification is in the blacklist, and media notifications are not allowed to be sent! backlist is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/android/server/notification/NotificationManagerServiceImpl;->allowMediaNotificationCloudDataList:Ljava/util/List;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 969
    :cond_0
    const/4 v0, 0x0

    .line 970
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " This media notification media notification switch closed, and notifications are not allowed to be sent!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 974
    :cond_1
    :goto_0
    return v0
.end method

.method public customizeNotificationLight(Landroid/content/Context;Lcom/android/server/notification/NotificationRecord$Light;)Lcom/android/server/notification/NotificationRecord$Light;
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "originLight"    # Lcom/android/server/notification/NotificationRecord$Light;

    .line 888
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "breathing_light"

    const/4 v2, -0x2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 890
    .local v0, "jsonText":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 891
    return-object p2

    .line 893
    :cond_0
    iget v1, p2, Lcom/android/server/notification/NotificationRecord$Light;->color:I

    .line 894
    .local v1, "customizeColor":I
    iget v2, p2, Lcom/android/server/notification/NotificationRecord$Light;->onMs:I

    .line 896
    .local v2, "customizeOnMs":I
    :try_start_0
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 897
    .local v3, "jsonArray":Lorg/json/JSONArray;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v4, v5, :cond_2

    .line 898
    invoke-virtual {v3, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 899
    .local v5, "logObject":Lorg/json/JSONObject;
    const-string v6, "light"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v6

    const/4 v7, 0x4

    if-ne v6, v7, :cond_1

    .line 900
    const-string v6, "color"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v6

    move v1, v6

    .line 901
    const-string v6, "onMS"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v6
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move v2, v6

    .line 897
    .end local v5    # "logObject":Lorg/json/JSONObject;
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 906
    .end local v3    # "jsonArray":Lorg/json/JSONArray;
    .end local v4    # "i":I
    :cond_2
    goto :goto_1

    .line 904
    :catch_0
    move-exception v3

    .line 905
    .local v3, "e":Lorg/json/JSONException;
    const-string v4, "NotificationManagerServiceImpl"

    const-string v5, "Light jsonArray error"

    invoke-static {v4, v5, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 907
    .end local v3    # "e":Lorg/json/JSONException;
    :goto_1
    new-instance v3, Lcom/android/server/notification/NotificationRecord$Light;

    iget v4, p2, Lcom/android/server/notification/NotificationRecord$Light;->offMs:I

    invoke-direct {v3, v1, v2, v4}, Lcom/android/server/notification/NotificationRecord$Light;-><init>(III)V

    return-object v3
.end method

.method public dumpLight(Ljava/io/PrintWriter;Lcom/android/server/notification/NotificationManagerService$DumpFilter;)V
    .locals 8
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "filter"    # Lcom/android/server/notification/NotificationManagerService$DumpFilter;

    .line 773
    const/4 v0, 0x0

    .line 774
    .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v1, 0x0

    .line 776
    .local v1, "instance":Ljava/lang/Object;
    :try_start_0
    const-string v2, "com.android.server.lights.MiuiLightsService"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    move-object v0, v2

    .line 777
    const-string v2, "getInstance"

    const/4 v3, 0x0

    new-array v4, v3, [Ljava/lang/Class;

    invoke-virtual {v0, v2, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 778
    .local v2, "getInstanceMethod":Ljava/lang/reflect/Method;
    const-string v4, "dumpLight"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Class;

    const-class v6, Ljava/io/PrintWriter;

    aput-object v6, v5, v3

    const-class v6, Lcom/android/server/notification/NotificationManagerService$DumpFilter;

    const/4 v7, 0x1

    aput-object v6, v5, v7

    invoke-virtual {v0, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 779
    .local v4, "dumpLightMethod":Ljava/lang/reflect/Method;
    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v2, v5, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v1, v3

    .line 780
    if-eqz v1, :cond_0

    .line 781
    filled-new-array {p1, p2}, [Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v4, v1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 789
    .end local v2    # "getInstanceMethod":Ljava/lang/reflect/Method;
    .end local v4    # "dumpLightMethod":Ljava/lang/reflect/Method;
    :cond_0
    goto :goto_0

    .line 784
    :catch_0
    move-exception v2

    .line 788
    .local v2, "e":Ljava/lang/ReflectiveOperationException;
    const-string v3, "NotificationManagerServiceImpl"

    const-string v4, "Failed to invoke MiuiLightsService dumpLight method"

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 790
    .end local v2    # "e":Ljava/lang/ReflectiveOperationException;
    :goto_0
    return-void
.end method

.method public enableBlockedToasts(Ljava/lang/String;)Z
    .locals 2
    .param p1, "pkg"    # Ljava/lang/String;

    .line 794
    iget-object v0, p0, Lcom/android/server/notification/NotificationManagerServiceImpl;->mPrivacyInputModePkgName:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 795
    iget-object v0, p0, Lcom/android/server/notification/NotificationManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "miui_privacy_input_pkg_name"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 796
    .local v0, "packageName":Ljava/lang/String;
    if-nez v0, :cond_0

    const-string v1, ""

    goto :goto_0

    :cond_0
    move-object v1, v0

    :goto_0
    iput-object v1, p0, Lcom/android/server/notification/NotificationManagerServiceImpl;->mPrivacyInputModePkgName:Ljava/lang/String;

    .line 798
    .end local v0    # "packageName":Ljava/lang/String;
    :cond_1
    iget-object v0, p0, Lcom/android/server/notification/NotificationManagerServiceImpl;->mPrivacyInputModePkgName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public fixCheckInterceptListenerAutoGroup(Landroid/app/Notification;Ljava/lang/String;)V
    .locals 4
    .param p1, "notification"    # Landroid/app/Notification;
    .param p2, "pkg"    # Ljava/lang/String;

    .line 920
    :try_start_0
    invoke-virtual {p1}, Landroid/app/Notification;->getChannelId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/notification/NotificationManagerServiceImpl;->interceptChannelId:[Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 921
    invoke-virtual {p1}, Landroid/app/Notification;->getChannelId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 920
    invoke-static {v0, v1}, Lcom/android/internal/util/ArrayUtils;->contains([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 922
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 923
    .local v0, "notificationClazz":Ljava/lang/Class;
    const-string v1, "mGroupKey"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 924
    .local v1, "mGroupKeyField":Ljava/lang/reflect/Field;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 925
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/app/Notification;->getChannelId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_autogroup"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 929
    .end local v0    # "notificationClazz":Ljava/lang/Class;
    .end local v1    # "mGroupKeyField":Ljava/lang/reflect/Field;
    :cond_0
    goto :goto_0

    .line 927
    :catch_0
    move-exception v0

    .line 928
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "NotificationManagerServiceImpl"

    const-string v2, " fixCheckInterceptListenerGroup exception:"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 930
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public getAppPkgByChannel(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "channelIdOrGroupId"    # Ljava/lang/String;
    .param p2, "defaultPkg"    # Ljava/lang/String;

    .line 557
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 558
    const-string v0, "\\|"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 559
    .local v0, "array":[Ljava/lang/String;
    array-length v1, v0

    const/4 v2, 0x2

    if-lt v1, v2, :cond_0

    .line 560
    const/4 v1, 0x1

    aget-object v1, v0, v1

    return-object v1

    .line 563
    .end local v0    # "array":[Ljava/lang/String;
    :cond_0
    return-object p2
.end method

.method public getAppUidByPkg(Ljava/lang/String;I)I
    .locals 2
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "defaultUid"    # I

    .line 568
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/android/server/notification/NotificationManagerServiceImpl;->getUidByPkg(Ljava/lang/String;I)I

    move-result v0

    .line 569
    .local v0, "appUid":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 570
    return v0

    .line 572
    :cond_0
    return p2
.end method

.method public getColorLightManager()Landroid/os/IBinder;
    .locals 1

    .line 767
    const-class v0, Lmiui/app/MiuiLightsManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/app/MiuiLightsManagerInternal;

    .line 768
    invoke-virtual {v0}, Lmiui/app/MiuiLightsManagerInternal;->getBinderService()Landroid/os/IBinder;

    move-result-object v0

    .line 767
    return-object v0
.end method

.method public getMediaNotificationsEnabled(Ljava/lang/String;ILcom/android/server/notification/PreferencesHelper;)Z
    .locals 1
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "uid"    # I
    .param p3, "mPreferencesHelper"    # Lcom/android/server/notification/PreferencesHelper;

    .line 987
    invoke-virtual {p0, p2, p1, p3}, Lcom/android/server/notification/NotificationManagerServiceImpl;->checkMediaNotificationControl(ILjava/lang/String;Lcom/android/server/notification/PreferencesHelper;)Z

    move-result v0

    return v0
.end method

.method public getMiuiDefaultListerWithVersion()Landroid/util/Pair;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 832
    iget-object v0, p0, Lcom/android/server/notification/NotificationManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x110f00a4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 834
    .local v0, "defaultListenerAccess":Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 835
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 836
    .local v1, "listenersWithVersion":[Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "listenersWithVersion "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "NotificationManagerServiceImpl"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 837
    if-eqz v1, :cond_2

    .line 838
    const/4 v2, -0x1

    .line 839
    .local v2, "version":I
    const/4 v3, 0x0

    .line 840
    .local v3, "listeners":Ljava/lang/String;
    array-length v4, v1

    const/4 v5, 0x2

    const/4 v6, 0x0

    const/4 v7, 0x1

    if-ne v4, v5, :cond_0

    .line 841
    aget-object v3, v1, v6

    .line 842
    aget-object v4, v1, v7

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto :goto_0

    .line 843
    :cond_0
    array-length v4, v1

    if-ne v4, v7, :cond_1

    .line 844
    aget-object v3, v1, v6

    .line 845
    const/4 v2, 0x0

    .line 847
    :cond_1
    :goto_0
    new-instance v4, Landroid/util/Pair;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v4, v5, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v4

    .line 850
    .end local v1    # "listenersWithVersion":[Ljava/lang/String;
    .end local v2    # "version":I
    .end local v3    # "listeners":Ljava/lang/String;
    :cond_2
    new-instance v1, Landroid/util/Pair;

    const/4 v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v1
.end method

.method public getPlayVibrationPkg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "opPkg"    # Ljava/lang/String;
    .param p3, "defaultPkg"    # Ljava/lang/String;

    .line 524
    invoke-direct {p0, p2}, Lcom/android/server/notification/NotificationManagerServiceImpl;->isXmsf(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/android/server/notification/NotificationManagerServiceImpl;->isXmsf(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 525
    return-object p1

    .line 527
    :cond_0
    return-object p3
.end method

.method public getVibRateLimiter()Ljava/lang/Object;
    .locals 1

    .line 651
    new-instance v0, Lcom/android/server/notification/VibRateLimiter;

    invoke-direct {v0}, Lcom/android/server/notification/VibRateLimiter;-><init>()V

    return-object v0
.end method

.method public init(Landroid/content/Context;Lcom/android/server/notification/NotificationManagerService;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "nms"    # Lcom/android/server/notification/NotificationManagerService;

    .line 117
    iput-object p1, p0, Lcom/android/server/notification/NotificationManagerServiceImpl;->mContext:Landroid/content/Context;

    .line 118
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x11030045

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/notification/NotificationManagerServiceImpl;->interceptListener:[Ljava/lang/String;

    .line 120
    iget-object v0, p0, Lcom/android/server/notification/NotificationManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x11030044

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/notification/NotificationManagerServiceImpl;->interceptChannelId:[Ljava/lang/String;

    .line 122
    iget-object v0, p0, Lcom/android/server/notification/NotificationManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x11030049

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/notification/NotificationManagerServiceImpl;->allowNotificationAccessList:[Ljava/lang/String;

    .line 124
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " allowNotificationListener :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/notification/NotificationManagerServiceImpl;->allowNotificationAccessList:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "NotificationManagerServiceImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    iget-object v0, p0, Lcom/android/server/notification/NotificationManagerServiceImpl;->mContext:Landroid/content/Context;

    .line 126
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 125
    const-string v1, "config_notification_medianotificationcontrol"

    const-string v2, "blacklist"

    const-string v3, ""

    invoke-static {v0, v1, v2, v3}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/notification/NotificationManagerServiceImpl;->allowMediaNotificationCloudData:Ljava/lang/String;

    .line 128
    nop

    .line 129
    invoke-static {v0}, Lcom/android/server/notification/NotificationManagerServiceImpl;->getMediaNotificationDataCloudConversion(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/android/server/notification/NotificationManagerServiceImpl;->allowMediaNotificationCloudDataList:Ljava/util/List;

    .line 130
    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/notification/NotificationManagerServiceImpl;->registerPrivacyInputMode(Landroid/os/Handler;)V

    .line 131
    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/notification/NotificationManagerServiceImpl;->registerCloudDataResolver(Landroid/os/Handler;)V

    .line 132
    iput-object p2, p0, Lcom/android/server/notification/NotificationManagerServiceImpl;->mNMS:Lcom/android/server/notification/NotificationManagerService;

    .line 133
    return-void
.end method

.method public isAllowAppNotificationListener(ZLcom/android/server/notification/NotificationManagerService$NotificationListeners;ILjava/lang/String;)V
    .locals 7
    .param p1, "isPackageAdded"    # Z
    .param p2, "mListeners"    # Lcom/android/server/notification/NotificationManagerService$NotificationListeners;
    .param p3, "userId"    # I
    .param p4, "pkg"    # Ljava/lang/String;

    .line 940
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/server/notification/NotificationManagerServiceImpl;->allowNotificationAccessList:[Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 941
    invoke-static {v0, p4}, Lcom/android/internal/util/ArrayUtils;->contains([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 942
    iget-object v0, p0, Lcom/android/server/notification/NotificationManagerServiceImpl;->mContext:Landroid/content/Context;

    const-class v1, Landroid/app/NotificationManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 943
    .local v0, "nm":Landroid/app/NotificationManager;
    const/high16 v1, 0xc0000

    const/4 v2, 0x0

    invoke-virtual {p2, p4, v1, v2}, Lcom/android/server/notification/NotificationManagerService$NotificationListeners;->queryPackageForServices(Ljava/lang/String;II)Landroid/util/ArraySet;

    move-result-object v1

    .line 946
    .local v1, "listeners":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Landroid/content/ComponentName;>;"
    const/4 v2, 0x0

    .local v2, "k":I
    :goto_0
    invoke-virtual {v1}, Landroid/util/ArraySet;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 947
    invoke-virtual {v1, v2}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/ComponentName;

    .line 949
    .local v3, "cn":Landroid/content/ComponentName;
    const/4 v4, 0x1

    :try_start_0
    invoke-virtual {v0, v3, p3, v4}, Landroid/app/NotificationManager;->setNotificationListenerAccessGrantedForUser(Landroid/content/ComponentName;IZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 952
    goto :goto_1

    .line 950
    :catch_0
    move-exception v4

    .line 951
    .local v4, "e":Ljava/lang/Exception;
    const-string v5, "NotificationManagerServiceImpl"

    const-string v6, " isAllowAppNotificationListener exception:"

    invoke-static {v5, v6, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 946
    .end local v3    # "cn":Landroid/content/ComponentName;
    .end local v4    # "e":Ljava/lang/Exception;
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 955
    .end local v0    # "nm":Landroid/app/NotificationManager;
    .end local v1    # "listeners":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Landroid/content/ComponentName;>;"
    .end local v2    # "k":I
    :cond_0
    return-void
.end method

.method public isAllowAppRenderedToast(Ljava/lang/String;II)Z
    .locals 8
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "callingUid"    # I
    .param p3, "userId"    # I

    .line 386
    sget-object v0, Lcom/android/server/notification/NotificationManagerServiceImpl;->sAllowToastSet:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 387
    const-class v0, Landroid/content/pm/PackageManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageManagerInternal;

    .line 389
    .local v0, "pm":Landroid/content/pm/PackageManagerInternal;
    const-wide/16 v4, 0x0

    :try_start_0
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v6

    move-object v2, v0

    move-object v3, p1

    move v7, p3

    invoke-virtual/range {v2 .. v7}, Landroid/content/pm/PackageManagerInternal;->getPackageInfo(Ljava/lang/String;JII)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 390
    .local v2, "info":Landroid/content/pm/PackageInfo;
    iget-object v3, v2, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v3}, Landroid/content/pm/ApplicationInfo;->isSystemApp()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v2, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v3, v3, Landroid/content/pm/ApplicationInfo;->uid:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-ne p2, v3, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1

    .line 391
    .end local v2    # "info":Landroid/content/pm/PackageInfo;
    :catch_0
    move-exception v2

    .line 392
    .local v2, "e":Ljava/lang/Exception;
    const-string v3, "NotificationManagerServiceImpl"

    const-string v4, "can\'t find package!"

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 395
    .end local v0    # "pm":Landroid/content/pm/PackageManagerInternal;
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_1
    return v1
.end method

.method public isDelegateAllowed(Ljava/lang/String;ILjava/lang/String;I)Z
    .locals 1
    .param p1, "callingPkg"    # Ljava/lang/String;
    .param p2, "callingUid"    # I
    .param p3, "targetPkg"    # Ljava/lang/String;
    .param p4, "userId"    # I

    .line 489
    invoke-direct {p0, p1, p2}, Lcom/android/server/notification/NotificationManagerServiceImpl;->isCallerAndroid(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, p1, p2}, Lcom/android/server/notification/NotificationManagerServiceImpl;->isCallerSecurityCenter(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 490
    invoke-direct {p0, p1, p3, p2, p4}, Lcom/android/server/notification/NotificationManagerServiceImpl;->checkCallerIsXmsf(Ljava/lang/String;Ljava/lang/String;II)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 489
    :goto_1
    return v0
.end method

.method public isDeniedDemoPostNotification(IILandroid/app/Notification;Ljava/lang/String;)Z
    .locals 2
    .param p1, "userId"    # I
    .param p2, "uid"    # I
    .param p3, "notification"    # Landroid/app/Notification;
    .param p4, "pkg"    # Ljava/lang/String;

    .line 601
    const/4 v0, 0x0

    if-nez p3, :cond_0

    .line 602
    return v0

    .line 604
    :cond_0
    sget-boolean v1, Lmiui/os/Build;->IS_DEMO_BUILD:Z

    if-eqz v1, :cond_1

    invoke-virtual {p3}, Landroid/app/Notification;->isForegroundService()Z

    move-result v1

    if-nez v1, :cond_1

    .line 605
    invoke-direct {p0, p3}, Lcom/android/server/notification/NotificationManagerServiceImpl;->hasProgress(Landroid/app/Notification;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 606
    invoke-virtual {p3}, Landroid/app/Notification;->isMediaNotification()Z

    move-result v1

    if-nez v1, :cond_1

    .line 607
    invoke-direct {p0, p1, p2, p4}, Lcom/android/server/notification/NotificationManagerServiceImpl;->isCallingSystem(IILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 608
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Denied demo product third-party app :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "to post notification :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 609
    invoke-virtual {p3}, Landroid/app/Notification;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 608
    const-string v1, "NotificationManagerServiceImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 610
    const/4 v0, 0x1

    return v0

    .line 612
    :cond_1
    return v0
.end method

.method public isDeniedLed(Lcom/android/server/notification/NotificationRecord;)Z
    .locals 1
    .param p1, "record"    # Lcom/android/server/notification/NotificationRecord;

    .line 298
    iget-object v0, p1, Lcom/android/server/notification/NotificationRecord;->mNRStatusStub:Lcom/android/server/notification/NotificationManagerServiceStub$NRStatusStub;

    invoke-interface {v0}, Lcom/android/server/notification/NotificationManagerServiceStub$NRStatusStub;->isStatusBarAllowLed()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public isDeniedLocalNotification(Landroid/app/AppOpsManager;Landroid/app/Notification;ILjava/lang/String;)Z
    .locals 3
    .param p1, "appops"    # Landroid/app/AppOpsManager;
    .param p2, "notification"    # Landroid/app/Notification;
    .param p3, "callingUid"    # I
    .param p4, "callingPkg"    # Ljava/lang/String;

    .line 510
    const-string v0, "com.xiaomi.xmsf"

    invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_2

    iget v0, p2, Landroid/app/Notification;->flags:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_0

    goto :goto_0

    .line 513
    :cond_0
    const/16 v0, 0x2731

    invoke-virtual {p1, v0, p3, p4}, Landroid/app/AppOpsManager;->noteOpNoThrow(IILjava/lang/String;)I

    move-result v0

    .line 514
    .local v0, "mode":I
    if-eqz v0, :cond_1

    .line 515
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MIUILOG- Permission Denied to post local notification for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "NotificationManagerServiceImpl"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 516
    const/4 v1, 0x1

    return v1

    .line 518
    :cond_1
    return v1

    .line 511
    .end local v0    # "mode":I
    :cond_2
    :goto_0
    return v1
.end method

.method public isDeniedPlaySound(Landroid/media/AudioManager;Lcom/android/server/notification/NotificationRecord;)Z
    .locals 1
    .param p1, "audioManager"    # Landroid/media/AudioManager;
    .param p2, "record"    # Lcom/android/server/notification/NotificationRecord;

    .line 250
    invoke-direct {p0, p1}, Lcom/android/server/notification/NotificationManagerServiceImpl;->isAudioCanPlay(Landroid/media/AudioManager;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p2}, Lcom/android/server/notification/NotificationManagerServiceImpl;->isSilenceMode(Lcom/android/server/notification/NotificationRecord;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p2, Lcom/android/server/notification/NotificationRecord;->mNRStatusStub:Lcom/android/server/notification/NotificationManagerServiceStub$NRStatusStub;

    .line 251
    invoke-interface {v0}, Lcom/android/server/notification/NotificationManagerServiceStub$NRStatusStub;->isStatusBarAllowSound()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 250
    :goto_1
    return v0
.end method

.method public isDeniedPlayVibration(Landroid/media/AudioManager;Lcom/android/server/notification/NotificationRecord;Lcom/android/server/notification/NotificationUsageStats$AggregatedStats;)Z
    .locals 4
    .param p1, "audioManager"    # Landroid/media/AudioManager;
    .param p2, "record"    # Lcom/android/server/notification/NotificationRecord;
    .param p3, "mStats"    # Lcom/android/server/notification/NotificationUsageStats$AggregatedStats;

    .line 275
    nop

    .line 276
    invoke-virtual {p1}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v2

    .line 278
    .local v0, "exRingerModeSilent":Z
    :goto_0
    if-nez v0, :cond_2

    iget-object v3, p2, Lcom/android/server/notification/NotificationRecord;->mNRStatusStub:Lcom/android/server/notification/NotificationManagerServiceStub$NRStatusStub;

    invoke-interface {v3}, Lcom/android/server/notification/NotificationManagerServiceStub$NRStatusStub;->isStatusBarAllowVibration()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 279
    invoke-direct {p0, p2, p3}, Lcom/android/server/notification/NotificationManagerServiceImpl;->shouldSkipFrequentlyVib(Lcom/android/server/notification/NotificationRecord;Lcom/android/server/notification/NotificationUsageStats$AggregatedStats;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_1

    :cond_1
    move v1, v2

    goto :goto_2

    :cond_2
    :goto_1
    nop

    .line 278
    :goto_2
    return v1
.end method

.method public isPackageDistractionRestrictionLocked(Lcom/android/server/notification/NotificationRecord;)Z
    .locals 3
    .param p1, "r"    # Lcom/android/server/notification/NotificationRecord;

    .line 400
    invoke-virtual {p1}, Lcom/android/server/notification/NotificationRecord;->getSbn()Landroid/service/notification/StatusBarNotification;

    move-result-object v0

    invoke-virtual {v0}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 401
    .local v0, "pkg":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/android/server/notification/NotificationRecord;->getSbn()Landroid/service/notification/StatusBarNotification;

    move-result-object v1

    invoke-virtual {v1}, Landroid/service/notification/StatusBarNotification;->getUid()I

    move-result v1

    .line 403
    .local v1, "callingUid":I
    invoke-direct {p0, v0, v1}, Lcom/android/server/notification/NotificationManagerServiceImpl;->isPackageDistractionRestrictionForUser(Ljava/lang/String;I)Z

    move-result v2

    return v2
.end method

.method public isSupportSilenceMode()Z
    .locals 1

    .line 255
    sget-boolean v0, Landroid/provider/MiuiSettings$SilenceMode;->isSupported:Z

    return v0
.end method

.method public readDefaultsAndFixMiuiVersion(Lcom/android/server/notification/ManagedServices;Ljava/lang/String;Lcom/android/modules/utils/TypedXmlPullParser;)V
    .locals 8
    .param p1, "services"    # Lcom/android/server/notification/ManagedServices;
    .param p2, "defaultComponents"    # Ljava/lang/String;
    .param p3, "parser"    # Lcom/android/modules/utils/TypedXmlPullParser;

    .line 861
    invoke-interface {p3}, Lcom/android/modules/utils/TypedXmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "enabled_listeners"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 863
    .local v0, "isEnableListener":Z
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, -0x1

    if-nez v1, :cond_3

    .line 864
    const/4 v1, 0x0

    .line 865
    .local v1, "hasBarrage":Z
    const-string v3, ":"

    invoke-virtual {p2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 866
    .local v3, "components":[Ljava/lang/String;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    array-length v5, v3

    if-ge v4, v5, :cond_1

    .line 867
    aget-object v5, v3, v4

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 868
    aget-object v5, v3, v4

    invoke-static {v5}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v5

    .line 869
    .local v5, "cn":Landroid/content/ComponentName;
    if-eqz v5, :cond_0

    .line 870
    invoke-virtual {v5}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "com.xiaomi.barrage"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    if-eqz v0, :cond_0

    .line 872
    const/4 v1, 0x1

    .line 866
    .end local v5    # "cn":Landroid/content/ComponentName;
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 877
    .end local v4    # "i":I
    :cond_1
    if-nez v1, :cond_2

    iget-object v4, p1, Lcom/android/server/notification/ManagedServices;->mVersionStub:Lcom/android/server/notification/NotificationManagerServiceStub$NMSVersionStub;

    invoke-interface {v4}, Lcom/android/server/notification/NotificationManagerServiceStub$NMSVersionStub;->getMiuiVersion()I

    move-result v4

    if-eq v4, v2, :cond_2

    .line 878
    iget-object v4, p1, Lcom/android/server/notification/ManagedServices;->mVersionStub:Lcom/android/server/notification/NotificationManagerServiceStub$NMSVersionStub;

    invoke-interface {v4, v2}, Lcom/android/server/notification/NotificationManagerServiceStub$NMSVersionStub;->setMiuiVersion(I)V

    .line 880
    .end local v1    # "hasBarrage":Z
    .end local v3    # "components":[Ljava/lang/String;
    :cond_2
    goto :goto_1

    .line 881
    :cond_3
    if-eqz v0, :cond_4

    iget-object v1, p1, Lcom/android/server/notification/ManagedServices;->mVersionStub:Lcom/android/server/notification/NotificationManagerServiceStub$NMSVersionStub;

    invoke-interface {v1}, Lcom/android/server/notification/NotificationManagerServiceStub$NMSVersionStub;->getMiuiVersion()I

    move-result v1

    if-eq v1, v2, :cond_4

    .line 882
    iget-object v1, p1, Lcom/android/server/notification/ManagedServices;->mVersionStub:Lcom/android/server/notification/NotificationManagerServiceStub$NMSVersionStub;

    invoke-interface {v1, v2}, Lcom/android/server/notification/NotificationManagerServiceStub$NMSVersionStub;->setMiuiVersion(I)V

    .line 885
    :cond_4
    :goto_1
    return-void
.end method

.method public setInterceptChannelId([Ljava/lang/String;)V
    .locals 0
    .param p1, "interceptChannelId"    # [Ljava/lang/String;

    .line 934
    iput-object p1, p0, Lcom/android/server/notification/NotificationManagerServiceImpl;->interceptChannelId:[Ljava/lang/String;

    .line 935
    return-void
.end method

.method public setMediaNotificationEnabled(Ljava/lang/String;IZLcom/android/server/notification/PreferencesHelper;)V
    .locals 1
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "uid"    # I
    .param p3, "enabled"    # Z
    .param p4, "mPreferencesHelper"    # Lcom/android/server/notification/PreferencesHelper;

    .line 980
    invoke-virtual {p4, p1, p2, p3}, Lcom/android/server/notification/PreferencesHelper;->setMediaNotificationEnabled(Ljava/lang/String;IZ)V

    .line 981
    iget-object v0, p0, Lcom/android/server/notification/NotificationManagerServiceImpl;->mNMS:Lcom/android/server/notification/NotificationManagerService;

    invoke-virtual {v0}, Lcom/android/server/notification/NotificationManagerService;->handleSavePolicyFile()V

    .line 982
    return-void
.end method

.method public shouldInjectDeleteChannel(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "channelId"    # Ljava/lang/String;
    .param p3, "groupId"    # Ljava/lang/String;

    .line 539
    invoke-direct {p0, p1}, Lcom/android/server/notification/NotificationManagerServiceImpl;->isXmsf(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p2}, Lcom/android/server/notification/NotificationManagerServiceImpl;->isXmsfChannelId(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p3}, Lcom/android/server/notification/NotificationManagerServiceImpl;->isXmsfChannelId(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 541
    :cond_0
    const/4 v0, 0x1

    return v0

    .line 545
    :cond_1
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 547
    invoke-direct {p0, p1, p2}, Lcom/android/server/notification/NotificationManagerServiceImpl;->checkAllowToDeleteChannel(Ljava/lang/String;Ljava/lang/String;)V

    .line 550
    :cond_2
    const/4 v0, 0x0

    return v0
.end method

.method shouldOnlyAlertNotification(Lcom/android/server/notification/NotificationRecord;)Z
    .locals 1
    .param p1, "record"    # Lcom/android/server/notification/NotificationRecord;

    .line 330
    iget-boolean v0, p1, Lcom/android/server/notification/NotificationRecord;->isUpdate:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/android/server/notification/NotificationRecord;->getNotification()Landroid/app/Notification;

    move-result-object v0

    iget v0, v0, Landroid/app/Notification;->flags:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    .line 331
    const/4 v0, 0x0

    return v0

    .line 333
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public skipClearAll(Lcom/android/server/notification/NotificationRecord;I)Z
    .locals 2
    .param p1, "record"    # Lcom/android/server/notification/NotificationRecord;
    .param p2, "reason"    # I

    .line 338
    const/4 v0, 0x3

    if-eq p2, v0, :cond_0

    const/16 v0, 0xb

    if-eq p2, v0, :cond_0

    .line 340
    const/4 v0, 0x0

    return v0

    .line 342
    :cond_0
    iget-object v0, p0, Lcom/android/server/notification/NotificationManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/android/server/notification/NotificationRecord;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/android/server/notification/NotificationManagerServiceImpl;->isUpdatableFocusNotification(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
