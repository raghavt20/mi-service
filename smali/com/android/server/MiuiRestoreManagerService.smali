.class public Lcom/android/server/MiuiRestoreManagerService;
.super Lmiui/app/backup/IMiuiRestoreManager$Stub;
.source "MiuiRestoreManagerService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;,
        Lcom/android/server/MiuiRestoreManagerService$BackupDataTask;,
        Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;,
        Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;,
        Lcom/android/server/MiuiRestoreManagerService$GetDataFileInfoTask;,
        Lcom/android/server/MiuiRestoreManagerService$ListDataDirTask;,
        Lcom/android/server/MiuiRestoreManagerService$FileChangedException;,
        Lcom/android/server/MiuiRestoreManagerService$BadSeInfoException;,
        Lcom/android/server/MiuiRestoreManagerService$QueryAppInfoErrorException;,
        Lcom/android/server/MiuiRestoreManagerService$BadFileDescriptorException;,
        Lcom/android/server/MiuiRestoreManagerService$Lifecycle;
    }
.end annotation


# static fields
.field private static final DEFAULT_THREAD_COUNT:I = 0x3

.field private static final ERROR:I = 0x1

.field private static final ERROR_CODE_BAD_SEINFO:I = -0x6

.field private static final ERROR_CODE_INVOKE_FAIL:I = -0x1

.field private static final ERROR_CODE_NO_APP_INFO:I = -0x2

.field private static final ERROR_FILE_CHANGED:I = -0x7

.field private static final ERROR_NONE:I = 0x0

.field private static final FLAG_MOVE_COPY_MODE:I = 0x2

.field private static final FLAG_MOVE_RENAME_MODE:I = 0x0

.field private static final FLAG_MOVE_WITH_LOCK:I = 0x4

.field private static final PACKAGE_BACKUP:Ljava/lang/String; = "com.miui.backup"

.field private static final PACKAGE_CLOUD_BACKUP:Ljava/lang/String; = "com.miui.cloudbackup"

.field private static final PACKAGE_HUANJI:Ljava/lang/String; = "com.miui.huanji"

.field public static final SERVICE_NAME:Ljava/lang/String; = "miui.restore.service"

.field private static final TAG:Ljava/lang/String; = "MiuiRestoreManagerService"


# instance fields
.field private final mAppDataRootPathMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mInstaller:Lcom/android/server/pm/Installer;

.field private mInstallerExecutor:Ljava/util/concurrent/Executor;

.field private mListDataExecutor:Ljava/util/concurrent/Executor;

.field private final mRestoreObservers:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList<",
            "Lmiui/app/backup/IRestoreListener;",
            ">;"
        }
    .end annotation
.end field

.field private mTransferExecutor:Ljava/util/concurrent/Executor;


# direct methods
.method static bridge synthetic -$$Nest$fgetmInstaller(Lcom/android/server/MiuiRestoreManagerService;)Lcom/android/server/pm/Installer;
    .locals 0

    iget-object p0, p0, Lcom/android/server/MiuiRestoreManagerService;->mInstaller:Lcom/android/server/pm/Installer;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mnotifyMoveTaskEnd(Lcom/android/server/MiuiRestoreManagerService;Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/MiuiRestoreManagerService;->notifyMoveTaskEnd(Ljava/lang/String;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/server/pm/Installer;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "installer"    # Lcom/android/server/pm/Installer;

    .line 99
    invoke-direct {p0}, Lmiui/app/backup/IMiuiRestoreManager$Stub;-><init>()V

    .line 109
    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    iput-object v0, p0, Lcom/android/server/MiuiRestoreManagerService;->mRestoreObservers:Landroid/os/RemoteCallbackList;

    .line 100
    const-string v0, "MiuiRestoreManagerService"

    const-string v1, "init MiuiRestoreManagerService"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    iput-object p1, p0, Lcom/android/server/MiuiRestoreManagerService;->mContext:Landroid/content/Context;

    .line 102
    iput-object p2, p0, Lcom/android/server/MiuiRestoreManagerService;->mInstaller:Lcom/android/server/pm/Installer;

    .line 103
    const/4 v0, 0x3

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/MiuiRestoreManagerService;->mInstallerExecutor:Ljava/util/concurrent/Executor;

    .line 104
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/MiuiRestoreManagerService;->mListDataExecutor:Ljava/util/concurrent/Executor;

    .line 105
    const/4 v0, 0x5

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/MiuiRestoreManagerService;->mTransferExecutor:Ljava/util/concurrent/Executor;

    .line 106
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/MiuiRestoreManagerService;->mAppDataRootPathMap:Ljava/util/Map;

    .line 107
    return-void
.end method

.method private checkIsLegalPackageAndCallingUid(ILjava/lang/String;)Z
    .locals 7
    .param p1, "callingUid"    # I
    .param p2, "packageName"    # Ljava/lang/String;

    .line 372
    const/4 v0, 0x0

    :try_start_0
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v1

    invoke-interface {v1, p1}, Landroid/content/pm/IPackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v1

    .line 373
    .local v1, "packages":[Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 374
    array-length v2, v1

    move v3, v0

    :goto_0
    if-ge v3, v2, :cond_1

    aget-object v4, v1, v3

    .line 375
    .local v4, "pkgName":Ljava/lang/String;
    invoke-static {p2, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 376
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v5

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v6

    invoke-interface {v5, p1, v6}, Landroid/content/pm/IPackageManager;->checkUidSignatures(II)I

    move-result v5
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v5, :cond_0

    .line 377
    const/4 v0, 0x1

    return v0

    .line 374
    .end local v4    # "pkgName":Ljava/lang/String;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 383
    .end local v1    # "packages":[Ljava/lang/String;
    :cond_1
    goto :goto_1

    .line 381
    :catch_0
    move-exception v1

    .line 382
    .local v1, "e":Landroid/os/RemoteException;
    const-string v2, "MiuiRestoreManagerService"

    const-string v3, "get package info fail "

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 385
    .end local v1    # "e":Landroid/os/RemoteException;
    :goto_1
    return v0
.end method

.method private getAppDataRootPath(I)Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;
    .locals 5
    .param p1, "deviceUid"    # I

    .line 181
    iget-object v0, p0, Lcom/android/server/MiuiRestoreManagerService;->mAppDataRootPathMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;

    .line 182
    .local v0, "rootPath":Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;
    if-nez v0, :cond_0

    .line 183
    invoke-static {p1}, Lcom/android/server/MiuiRestoreManagerService;->getExternalAppDataRootPathOrNull(I)Ljava/lang/String;

    move-result-object v1

    .line 184
    .local v1, "external":Ljava/lang/String;
    iget-object v2, p0, Lcom/android/server/MiuiRestoreManagerService;->mContext:Landroid/content/Context;

    invoke-static {v2, p1}, Lcom/android/server/MiuiRestoreManagerService;->getInternalAppDataRootPathOrNull(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 185
    .local v2, "internal":Ljava/lang/String;
    new-instance v3, Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;

    invoke-direct {v3, v1, v2}, Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v3

    .line 186
    iget-object v3, p0, Lcom/android/server/MiuiRestoreManagerService;->mAppDataRootPathMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "app data root\uff1a"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "MiuiRestoreManagerService"

    invoke-static {v4, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    .end local v1    # "external":Ljava/lang/String;
    .end local v2    # "internal":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method private static getExternalAppDataRootPathOrNull(I)Ljava/lang/String;
    .locals 4
    .param p0, "deviceUid"    # I

    .line 193
    new-instance v0, Landroid/os/Environment$UserEnvironment;

    invoke-direct {v0, p0}, Landroid/os/Environment$UserEnvironment;-><init>(I)V

    .line 194
    .local v0, "environment":Landroid/os/Environment$UserEnvironment;
    const-string v1, "com.miui.cloudbackup"

    invoke-virtual {v0, v1}, Landroid/os/Environment$UserEnvironment;->buildExternalStorageAppDataDirs(Ljava/lang/String;)[Ljava/io/File;

    move-result-object v1

    .line 195
    .local v1, "files":[Ljava/io/File;
    if-eqz v1, :cond_0

    array-length v2, v1

    if-lez v2, :cond_0

    .line 196
    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    .line 197
    .local v2, "appDataParentFile":Ljava/io/File;
    if-eqz v2, :cond_0

    .line 198
    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 201
    .end local v2    # "appDataParentFile":Ljava/io/File;
    :cond_0
    const/4 v2, 0x0

    return-object v2
.end method

.method private static getInternalAppDataRootPathOrNull(Landroid/content/Context;I)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "deviceUid"    # I

    .line 206
    :try_start_0
    const-string v0, "com.miui.cloudbackup"

    .line 209
    invoke-static {p1}, Landroid/os/UserHandle;->of(I)Landroid/os/UserHandle;

    move-result-object v1

    .line 206
    const/4 v2, 0x2

    invoke-virtual {p0, v0, v2, v1}, Landroid/content/Context;->createPackageContextAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)Landroid/content/Context;

    move-result-object v0

    .line 210
    invoke-virtual {v0}, Landroid/content/Context;->getDataDir()Ljava/io/File;

    move-result-object v0

    .line 211
    .local v0, "appDataFile":Ljava/io/File;
    if-eqz v0, :cond_0

    .line 212
    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    .line 213
    .local v1, "appDataParentFile":Ljava/io/File;
    if-eqz v1, :cond_0

    .line 214
    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v2

    .line 219
    .end local v0    # "appDataFile":Ljava/io/File;
    .end local v1    # "appDataParentFile":Ljava/io/File;
    :cond_0
    goto :goto_0

    .line 217
    :catch_0
    move-exception v0

    .line 218
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v1, "MiuiRestoreManagerService"

    const-string v2, "package name not find"

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :goto_0
    const/4 v0, 0x0

    return-object v0
.end method

.method private moveSdcardData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Z
    .locals 14
    .param p1, "sourcePath"    # Ljava/lang/String;
    .param p2, "destPath"    # Ljava/lang/String;
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "userId"    # I
    .param p5, "flag"    # I

    .line 156
    move-object v11, p0

    iget-object v12, v11, Lcom/android/server/MiuiRestoreManagerService;->mInstallerExecutor:Ljava/util/concurrent/Executor;

    new-instance v13, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;

    iget-object v2, v11, Lcom/android/server/MiuiRestoreManagerService;->mInstaller:Lcom/android/server/pm/Installer;

    const/4 v6, 0x0

    const-string v8, ""

    const/4 v10, 0x0

    move-object v0, v13

    move-object v1, p0

    move-object v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move/from16 v7, p4

    move/from16 v9, p5

    invoke-direct/range {v0 .. v10}, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;-><init>(Lcom/android/server/MiuiRestoreManagerService;Lcom/android/server/pm/Installer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;ILcom/android/server/MiuiRestoreManagerService$MoveDataTask-IA;)V

    invoke-interface {v12, v13}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 158
    const/4 v0, 0x1

    return v0
.end method

.method private notifyMoveTaskEnd(Ljava/lang/String;II)V
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I
    .param p3, "errorCode"    # I

    .line 399
    iget-object v0, p0, Lcom/android/server/MiuiRestoreManagerService;->mRestoreObservers:Landroid/os/RemoteCallbackList;

    monitor-enter v0

    .line 400
    :try_start_0
    iget-object v1, p0, Lcom/android/server/MiuiRestoreManagerService;->mRestoreObservers:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 401
    .local v1, "cnt":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 403
    :try_start_1
    iget-object v3, p0, Lcom/android/server/MiuiRestoreManagerService;->mRestoreObservers:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lmiui/app/backup/IRestoreListener;

    invoke-interface {v3, p1, p2, p3}, Lmiui/app/backup/IRestoreListener;->onRestoreEnd(Ljava/lang/String;II)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 406
    goto :goto_1

    .line 404
    :catch_0
    move-exception v3

    .line 405
    .local v3, "e":Landroid/os/RemoteException;
    :try_start_2
    const-string v4, "MiuiRestoreManagerService"

    const-string v5, "RemoteException error in notifyMoveTaskEnd "

    invoke-static {v4, v5, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 401
    .end local v3    # "e":Landroid/os/RemoteException;
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 408
    .end local v2    # "i":I
    :cond_0
    iget-object v2, p0, Lcom/android/server/MiuiRestoreManagerService;->mRestoreObservers:Landroid/os/RemoteCallbackList;

    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 409
    .end local v1    # "cnt":I
    monitor-exit v0

    .line 410
    return-void

    .line 409
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method


# virtual methods
.method public backupAppData(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Landroid/os/ParcelFileDescriptor;ILmiui/app/backup/ITaskCommonCallback;)I
    .locals 14
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "paths"    # [Ljava/lang/String;
    .param p3, "domains"    # [Ljava/lang/String;
    .param p4, "parentPaths"    # [Ljava/lang/String;
    .param p5, "excludePaths"    # [Ljava/lang/String;
    .param p6, "outFd"    # Landroid/os/ParcelFileDescriptor;
    .param p7, "type"    # I
    .param p8, "callback"    # Lmiui/app/backup/ITaskCommonCallback;

    .line 164
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    const/16 v1, 0x17d4

    const/4 v2, 0x1

    const-string v3, "MiuiRestoreManagerService"

    if-eq v0, v1, :cond_0

    .line 165
    const-string v0, "caller is not backup uid"

    invoke-static {v3, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    return v2

    .line 168
    :cond_0
    if-eqz p2, :cond_2

    if-nez p3, :cond_1

    move-object v0, p0

    goto :goto_0

    .line 172
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "call backupData:paths="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static/range {p2 .. p2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    move-object v0, p0

    iget-object v1, v0, Lcom/android/server/MiuiRestoreManagerService;->mInstallerExecutor:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/android/server/MiuiRestoreManagerService$BackupDataTask;

    move-object v4, v2

    move-object v5, p0

    move-object v6, p1

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    move-object/from16 v9, p4

    move-object/from16 v10, p5

    move-object/from16 v11, p6

    move/from16 v12, p7

    move-object/from16 v13, p8

    invoke-direct/range {v4 .. v13}, Lcom/android/server/MiuiRestoreManagerService$BackupDataTask;-><init>(Lcom/android/server/MiuiRestoreManagerService;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Landroid/os/ParcelFileDescriptor;ILmiui/app/backup/ITaskCommonCallback;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 176
    const/4 v1, 0x0

    return v1

    .line 168
    :cond_2
    move-object v0, p0

    .line 169
    :goto_0
    const-string v1, "path or domains is null"

    invoke-static {v3, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    return v2
.end method

.method public getDataFileInfo(Ljava/lang/String;Lmiui/app/backup/IGetFileInfoCallback;)Z
    .locals 10
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "callback"    # Lmiui/app/backup/IGetFileInfoCallback;

    .line 280
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 281
    .local v0, "callingUid":I
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    .line 282
    .local v1, "callingPid":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "call get data file info, path= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", uid= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", pid= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MiuiRestoreManagerService"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v4

    .line 285
    .local v4, "token":J
    :try_start_0
    const-string v2, "com.miui.cloudbackup"

    invoke-direct {p0, v0, v2}, Lcom/android/server/MiuiRestoreManagerService;->checkIsLegalPackageAndCallingUid(ILjava/lang/String;)Z

    move-result v2

    const/4 v6, 0x0

    if-nez v2, :cond_0

    const-string v2, "com.miui.backup"

    .line 286
    invoke-direct {p0, v0, v2}, Lcom/android/server/MiuiRestoreManagerService;->checkIsLegalPackageAndCallingUid(ILjava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "com.miui.huanji"

    .line 287
    invoke-direct {p0, v0, v2}, Lcom/android/server/MiuiRestoreManagerService;->checkIsLegalPackageAndCallingUid(ILjava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 288
    const-string v2, "not cloud backup or huanji"

    invoke-static {v3, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 289
    nop

    .line 318
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 289
    return v6

    .line 292
    :cond_0
    :try_start_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 293
    const-string v2, "invalid params"

    invoke-static {v3, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 294
    nop

    .line 318
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 294
    return v6

    .line 297
    :cond_1
    :try_start_2
    invoke-static {v0}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v2

    invoke-direct {p0, v2}, Lcom/android/server/MiuiRestoreManagerService;->getAppDataRootPath(I)Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;

    move-result-object v2

    .line 298
    .local v2, "appDataRootPath":Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;
    iget-object v7, v2, Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;->external:Ljava/lang/String;

    if-nez v7, :cond_2

    iget-object v7, v2, Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;->internal:Ljava/lang/String;

    if-nez v7, :cond_2

    .line 299
    const-string v7, "null root path"

    invoke-static {v3, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 300
    nop

    .line 318
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 300
    return v6

    .line 303
    :cond_2
    const/4 v7, 0x0

    .line 304
    .local v7, "verify":Z
    const/4 v8, 0x1

    if-nez v7, :cond_4

    :try_start_3
    iget-object v9, v2, Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;->external:Ljava/lang/String;

    if-eqz v9, :cond_3

    iget-object v9, v2, Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;->external:Ljava/lang/String;

    invoke-virtual {p1, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    goto :goto_0

    :cond_3
    move v9, v6

    goto :goto_1

    :cond_4
    :goto_0
    move v9, v8

    :goto_1
    move v7, v9

    .line 305
    if-nez v7, :cond_6

    iget-object v9, v2, Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;->internal:Ljava/lang/String;

    if-eqz v9, :cond_5

    iget-object v9, v2, Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;->internal:Ljava/lang/String;

    invoke-virtual {p1, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    goto :goto_2

    :cond_5
    move v9, v6

    goto :goto_3

    :cond_6
    :goto_2
    move v9, v8

    :goto_3
    move v7, v9

    .line 306
    if-nez v7, :cond_7

    .line 307
    const-string v8, "illegal path"

    invoke-static {v3, v8}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 308
    nop

    .line 318
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 308
    return v6

    .line 311
    :cond_7
    if-nez p2, :cond_8

    .line 312
    :try_start_4
    const-string v8, "no callback"

    invoke-static {v3, v8}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 313
    nop

    .line 318
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 313
    return v6

    .line 316
    :cond_8
    :try_start_5
    iget-object v3, p0, Lcom/android/server/MiuiRestoreManagerService;->mListDataExecutor:Ljava/util/concurrent/Executor;

    new-instance v6, Lcom/android/server/MiuiRestoreManagerService$GetDataFileInfoTask;

    invoke-direct {v6, p0, p1, p2}, Lcom/android/server/MiuiRestoreManagerService$GetDataFileInfoTask;-><init>(Lcom/android/server/MiuiRestoreManagerService;Ljava/lang/String;Lmiui/app/backup/IGetFileInfoCallback;)V

    invoke-interface {v3, v6}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 318
    .end local v2    # "appDataRootPath":Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;
    .end local v7    # "verify":Z
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 319
    nop

    .line 321
    return v8

    .line 318
    :catchall_0
    move-exception v2

    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 319
    throw v2
.end method

.method public listDataDir(Ljava/lang/String;JILmiui/app/backup/IListDirCallback;)Z
    .locals 20
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "start"    # J
    .param p4, "maxCount"    # I
    .param p5, "callback"    # Lmiui/app/backup/IListDirCallback;

    .line 326
    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move-wide/from16 v10, p2

    move/from16 v12, p4

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v13

    .line 327
    .local v13, "callingUid":I
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v14

    .line 328
    .local v14, "callingPid":I
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "call list data dir, path= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", start= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", maxCount= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", uid= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", pid= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiRestoreManagerService"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v15

    .line 332
    .local v15, "token":J
    :try_start_0
    const-string v0, "com.miui.cloudbackup"

    invoke-direct {v8, v13, v0}, Lcom/android/server/MiuiRestoreManagerService;->checkIsLegalPackageAndCallingUid(ILjava/lang/String;)Z

    move-result v0

    const/4 v2, 0x0

    if-nez v0, :cond_0

    const-string v0, "com.miui.backup"

    .line 333
    invoke-direct {v8, v13, v0}, Lcom/android/server/MiuiRestoreManagerService;->checkIsLegalPackageAndCallingUid(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "com.miui.huanji"

    .line 334
    invoke-direct {v8, v13, v0}, Lcom/android/server/MiuiRestoreManagerService;->checkIsLegalPackageAndCallingUid(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 335
    const-string v0, "not cloud backup or huanji"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 336
    nop

    .line 364
    invoke-static/range {v15 .. v16}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 336
    return v2

    .line 338
    :cond_0
    :try_start_1
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    const-wide/16 v3, 0x0

    cmp-long v0, v10, v3

    if-ltz v0, :cond_9

    if-gez v12, :cond_1

    goto/16 :goto_4

    .line 343
    :cond_1
    invoke-static {v13}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v0

    invoke-direct {v8, v0}, Lcom/android/server/MiuiRestoreManagerService;->getAppDataRootPath(I)Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;

    move-result-object v0

    .line 344
    .local v0, "appDataRootPath":Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;
    iget-object v3, v0, Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;->external:Ljava/lang/String;

    if-nez v3, :cond_2

    iget-object v3, v0, Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;->internal:Ljava/lang/String;

    if-nez v3, :cond_2

    .line 345
    const-string v3, "null root path"

    invoke-static {v1, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 346
    nop

    .line 364
    invoke-static/range {v15 .. v16}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 346
    return v2

    .line 349
    :cond_2
    const/4 v3, 0x0

    .line 350
    .local v3, "verify":Z
    const/16 v17, 0x1

    if-nez v3, :cond_4

    :try_start_2
    iget-object v4, v0, Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;->external:Ljava/lang/String;

    if-eqz v4, :cond_3

    iget-object v4, v0, Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;->external:Ljava/lang/String;

    invoke-virtual {v9, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    goto :goto_0

    :cond_3
    move v4, v2

    goto :goto_1

    :cond_4
    :goto_0
    move/from16 v4, v17

    :goto_1
    move v3, v4

    .line 351
    if-nez v3, :cond_6

    iget-object v4, v0, Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;->internal:Ljava/lang/String;

    if-eqz v4, :cond_5

    iget-object v4, v0, Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;->internal:Ljava/lang/String;

    invoke-virtual {v9, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    goto :goto_2

    :cond_5
    move v4, v2

    goto :goto_3

    :cond_6
    :goto_2
    move/from16 v4, v17

    :goto_3
    move/from16 v18, v4

    .line 352
    .end local v3    # "verify":Z
    .local v18, "verify":Z
    if-nez v18, :cond_7

    .line 353
    const-string v3, "illegal path"

    invoke-static {v1, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 354
    nop

    .line 364
    invoke-static/range {v15 .. v16}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 354
    return v2

    .line 357
    :cond_7
    if-nez p5, :cond_8

    .line 358
    :try_start_3
    const-string v3, "no callback"

    invoke-static {v1, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 359
    nop

    .line 364
    invoke-static/range {v15 .. v16}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 359
    return v2

    .line 362
    :cond_8
    :try_start_4
    iget-object v7, v8, Lcom/android/server/MiuiRestoreManagerService;->mListDataExecutor:Ljava/util/concurrent/Executor;

    new-instance v6, Lcom/android/server/MiuiRestoreManagerService$ListDataDirTask;

    move-object v1, v6

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-wide/from16 v4, p2

    move-object/from16 v19, v0

    move-object v0, v6

    .end local v0    # "appDataRootPath":Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;
    .local v19, "appDataRootPath":Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;
    move/from16 v6, p4

    move-object v8, v7

    move-object/from16 v7, p5

    invoke-direct/range {v1 .. v7}, Lcom/android/server/MiuiRestoreManagerService$ListDataDirTask;-><init>(Lcom/android/server/MiuiRestoreManagerService;Ljava/lang/String;JILmiui/app/backup/IListDirCallback;)V

    invoke-interface {v8, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 364
    .end local v18    # "verify":Z
    .end local v19    # "appDataRootPath":Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;
    invoke-static/range {v15 .. v16}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 365
    nop

    .line 367
    return v17

    .line 339
    :cond_9
    :goto_4
    :try_start_5
    const-string v0, "invalid params"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 340
    nop

    .line 364
    invoke-static/range {v15 .. v16}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 340
    return v2

    .line 364
    :catchall_0
    move-exception v0

    invoke-static/range {v15 .. v16}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 365
    throw v0
.end method

.method public moveData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZI)Z
    .locals 21
    .param p1, "sourcePath"    # Ljava/lang/String;
    .param p2, "destPath"    # Ljava/lang/String;
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "userId"    # I
    .param p5, "isSdcardData"    # Z
    .param p6, "flag"    # I

    .line 114
    move-object/from16 v12, p0

    move-object/from16 v13, p3

    move/from16 v14, p4

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    const/16 v1, 0x17d4

    const/4 v2, 0x0

    const-string v3, "MiuiRestoreManagerService"

    if-eq v0, v1, :cond_0

    .line 115
    const-string v0, "caller is not backup uid"

    invoke-static {v3, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    return v2

    .line 118
    :cond_0
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object/from16 v15, p2

    goto/16 :goto_2

    .line 122
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "call move:dp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    move-object/from16 v15, p2

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",pkg="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",userId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    if-eqz p5, :cond_2

    .line 125
    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move/from16 v5, p4

    move/from16 v6, p6

    invoke-direct/range {v1 .. v6}, Lcom/android/server/MiuiRestoreManagerService;->moveSdcardData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Z

    move-result v0

    return v0

    .line 127
    :cond_2
    move-object/from16 v11, p1

    invoke-virtual {v11, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual/range {p2 .. p3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    goto :goto_1

    .line 131
    :cond_3
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v16

    .line 134
    .local v16, "token":J
    :try_start_0
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v0

    .line 135
    const-wide/16 v4, 0x400

    invoke-interface {v0, v13, v4, v5, v14}, Landroid/content/pm/IPackageManager;->getApplicationInfo(Ljava/lang/String;JI)Landroid/content/pm/ApplicationInfo;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 143
    .local v0, "info":Landroid/content/pm/ApplicationInfo;
    invoke-static/range {v16 .. v17}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 144
    nop

    .line 145
    if-nez v0, :cond_4

    .line 146
    const-string v1, "package application info is null"

    invoke-static {v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    return v2

    .line 149
    :cond_4
    iget-object v10, v12, Lcom/android/server/MiuiRestoreManagerService;->mInstallerExecutor:Ljava/util/concurrent/Executor;

    new-instance v9, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;

    iget-object v3, v12, Lcom/android/server/MiuiRestoreManagerService;->mInstaller:Lcom/android/server/pm/Installer;

    iget v7, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    iget-object v8, v0, Landroid/content/pm/ApplicationInfo;->seInfo:Ljava/lang/String;

    const/16 v18, 0x0

    move-object v1, v9

    move-object/from16 v2, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move-object/from16 v19, v8

    move/from16 v8, p4

    move-object/from16 v20, v9

    move-object/from16 v9, v19

    move-object/from16 v19, v0

    move-object v0, v10

    .end local v0    # "info":Landroid/content/pm/ApplicationInfo;
    .local v19, "info":Landroid/content/pm/ApplicationInfo;
    move/from16 v10, p6

    move-object/from16 v11, v18

    invoke-direct/range {v1 .. v11}, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;-><init>(Lcom/android/server/MiuiRestoreManagerService;Lcom/android/server/pm/Installer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;ILcom/android/server/MiuiRestoreManagerService$MoveDataTask-IA;)V

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 151
    const/4 v0, 0x1

    return v0

    .line 143
    .end local v19    # "info":Landroid/content/pm/ApplicationInfo;
    :catchall_0
    move-exception v0

    goto :goto_0

    .line 139
    :catch_0
    move-exception v0

    .line 140
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v1, "get package application info fail "

    invoke-static {v3, v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 141
    nop

    .line 143
    invoke-static/range {v16 .. v17}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 141
    return v2

    .line 143
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    invoke-static/range {v16 .. v17}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 144
    throw v0

    .line 128
    .end local v16    # "token":J
    :cond_5
    :goto_1
    const-string/jumbo v0, "the path does not match the package name"

    invoke-static {v3, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    return v2

    .line 118
    :cond_6
    move-object/from16 v15, p2

    .line 119
    :goto_2
    const-string v0, "path or packageName is null"

    invoke-static {v3, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    return v2
.end method

.method public registerRestoreListener(Lmiui/app/backup/IRestoreListener;)V
    .locals 1
    .param p1, "listener"    # Lmiui/app/backup/IRestoreListener;

    .line 390
    iget-object v0, p0, Lcom/android/server/MiuiRestoreManagerService;->mRestoreObservers:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    .line 391
    return-void
.end method

.method public transferData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZIZLmiui/app/backup/ITransferDataCallback;)Z
    .locals 22
    .param p1, "src"    # Ljava/lang/String;
    .param p2, "targetBase"    # Ljava/lang/String;
    .param p3, "targetRelative"    # Ljava/lang/String;
    .param p4, "targetPkgName"    # Ljava/lang/String;
    .param p5, "targetMode"    # I
    .param p6, "copy"    # Z
    .param p7, "userId"    # I
    .param p8, "isExternal"    # Z
    .param p9, "callback"    # Lmiui/app/backup/ITransferDataCallback;

    .line 234
    move-object/from16 v12, p0

    move-object/from16 v13, p1

    move-object/from16 v14, p2

    move/from16 v15, p8

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v11

    .line 235
    .local v11, "callingUid":I
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v10

    .line 236
    .local v10, "callingPid":I
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "call transfer data, src= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", targetBase= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", targetRelative= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    move-object/from16 v9, p3

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", targetPkgName= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    move-object/from16 v8, p4

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", targetMode= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    move/from16 v7, p5

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", copy= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    move/from16 v6, p6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", userId= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    move/from16 v5, p7

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isExternal= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", uid= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", pid= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiRestoreManagerService"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v16

    .line 241
    .local v16, "token":J
    :try_start_0
    const-string v0, "com.miui.cloudbackup"

    invoke-direct {v12, v11, v0}, Lcom/android/server/MiuiRestoreManagerService;->checkIsLegalPackageAndCallingUid(ILjava/lang/String;)Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    const/4 v2, 0x0

    if-nez v0, :cond_0

    .line 242
    :try_start_1
    const-string v0, "not cloud backup"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 243
    nop

    .line 272
    invoke-static/range {v16 .. v17}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 243
    return v2

    .line 272
    :catchall_0
    move-exception v0

    move/from16 v20, v10

    move/from16 v21, v11

    goto/16 :goto_3

    .line 246
    :cond_0
    :try_start_2
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 247
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    move/from16 v20, v10

    move/from16 v21, v11

    goto/16 :goto_2

    .line 252
    :cond_1
    invoke-static {v11}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v0

    invoke-direct {v12, v0}, Lcom/android/server/MiuiRestoreManagerService;->getAppDataRootPath(I)Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;

    move-result-object v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 253
    .local v0, "appDataRootPath":Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;
    if-eqz v15, :cond_2

    :try_start_3
    iget-object v3, v0, Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;->external:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :cond_2
    :try_start_4
    iget-object v3, v0, Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;->internal:Ljava/lang/String;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :goto_0
    move-object v4, v3

    .line 254
    .local v4, "rootPath":Ljava/lang/String;
    if-nez v4, :cond_3

    .line 255
    :try_start_5
    const-string v3, "null root path"

    invoke-static {v1, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 256
    nop

    .line 272
    invoke-static/range {v16 .. v17}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 256
    return v2

    .line 259
    :cond_3
    :try_start_6
    invoke-virtual {v13, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual {v14, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    if-nez v3, :cond_4

    move-object/from16 v18, v0

    move-object/from16 v19, v4

    move/from16 v20, v10

    move/from16 v21, v11

    goto :goto_1

    .line 264
    :cond_4
    if-nez p9, :cond_5

    .line 265
    :try_start_7
    const-string v3, "no callback"

    invoke-static {v1, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 266
    nop

    .line 272
    invoke-static/range {v16 .. v17}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 266
    return v2

    .line 269
    :cond_5
    :try_start_8
    iget-object v3, v12, Lcom/android/server/MiuiRestoreManagerService;->mTransferExecutor:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    move-object v1, v2

    move-object/from16 v18, v0

    move-object v0, v2

    .end local v0    # "appDataRootPath":Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;
    .local v18, "appDataRootPath":Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;
    move-object/from16 v2, p0

    move-object v12, v3

    move-object/from16 v3, p1

    move-object/from16 v19, v4

    .end local v4    # "rootPath":Ljava/lang/String;
    .local v19, "rootPath":Ljava/lang/String;
    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move/from16 v7, p5

    move/from16 v8, p6

    move/from16 v9, p7

    move/from16 v20, v10

    .end local v10    # "callingPid":I
    .local v20, "callingPid":I
    move/from16 v10, p8

    move/from16 v21, v11

    .end local v11    # "callingUid":I
    .local v21, "callingUid":I
    move-object/from16 v11, p9

    :try_start_9
    invoke-direct/range {v1 .. v11}, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;-><init>(Lcom/android/server/MiuiRestoreManagerService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZIZLmiui/app/backup/ITransferDataCallback;)V

    invoke-interface {v12, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 272
    .end local v18    # "appDataRootPath":Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;
    .end local v19    # "rootPath":Ljava/lang/String;
    invoke-static/range {v16 .. v17}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 273
    nop

    .line 275
    const/4 v0, 0x1

    return v0

    .line 259
    .end local v20    # "callingPid":I
    .end local v21    # "callingUid":I
    .restart local v0    # "appDataRootPath":Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;
    .restart local v4    # "rootPath":Ljava/lang/String;
    .restart local v10    # "callingPid":I
    .restart local v11    # "callingUid":I
    :cond_6
    move-object/from16 v18, v0

    move-object/from16 v19, v4

    move/from16 v20, v10

    move/from16 v21, v11

    .line 260
    .end local v0    # "appDataRootPath":Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;
    .end local v4    # "rootPath":Ljava/lang/String;
    .end local v10    # "callingPid":I
    .end local v11    # "callingUid":I
    .restart local v18    # "appDataRootPath":Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;
    .restart local v19    # "rootPath":Ljava/lang/String;
    .restart local v20    # "callingPid":I
    .restart local v21    # "callingUid":I
    :goto_1
    :try_start_a
    const-string v0, "illegal path"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 261
    nop

    .line 272
    invoke-static/range {v16 .. v17}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 261
    return v2

    .line 247
    .end local v18    # "appDataRootPath":Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;
    .end local v19    # "rootPath":Ljava/lang/String;
    .end local v20    # "callingPid":I
    .end local v21    # "callingUid":I
    .restart local v10    # "callingPid":I
    .restart local v11    # "callingUid":I
    :cond_7
    move/from16 v20, v10

    move/from16 v21, v11

    .end local v10    # "callingPid":I
    .end local v11    # "callingUid":I
    .restart local v20    # "callingPid":I
    .restart local v21    # "callingUid":I
    goto :goto_2

    .line 246
    .end local v20    # "callingPid":I
    .end local v21    # "callingUid":I
    .restart local v10    # "callingPid":I
    .restart local v11    # "callingUid":I
    :cond_8
    move/from16 v20, v10

    move/from16 v21, v11

    .line 248
    .end local v10    # "callingPid":I
    .end local v11    # "callingUid":I
    .restart local v20    # "callingPid":I
    .restart local v21    # "callingUid":I
    :goto_2
    :try_start_b
    const-string v0, "invalid params"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 249
    nop

    .line 272
    invoke-static/range {v16 .. v17}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 249
    return v2

    .line 272
    :catchall_1
    move-exception v0

    goto :goto_3

    .end local v20    # "callingPid":I
    .end local v21    # "callingUid":I
    .restart local v10    # "callingPid":I
    .restart local v11    # "callingUid":I
    :catchall_2
    move-exception v0

    move/from16 v20, v10

    move/from16 v21, v11

    .end local v10    # "callingPid":I
    .end local v11    # "callingUid":I
    .restart local v20    # "callingPid":I
    .restart local v21    # "callingUid":I
    :goto_3
    invoke-static/range {v16 .. v17}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 273
    throw v0
.end method

.method public unregisterRestoreListener(Lmiui/app/backup/IRestoreListener;)V
    .locals 1
    .param p1, "listener"    # Lmiui/app/backup/IRestoreListener;

    .line 395
    iget-object v0, p0, Lcom/android/server/MiuiRestoreManagerService;->mRestoreObservers:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    .line 396
    return-void
.end method
