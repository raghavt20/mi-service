.class public Lcom/android/server/storage/DeviceStorageMonitorServiceImpl;
.super Lcom/android/server/storage/DeviceStorageMonitorServiceStub;
.source "DeviceStorageMonitorServiceImpl.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.storage.DeviceStorageMonitorServiceStub$$"
.end annotation


# static fields
.field private static final DATA_SUB_DIRS:[Ljava/lang/String;

.field private static final EXTERNAL_SUB_DIRS:[Ljava/lang/String;

.field private static final LEVEL_DEEP_CLEAN:I = 0x3

.field private static final LEVEL_NORMAL_CLEAN:I = 0x2

.field private static final TAG:Ljava/lang/String; = "DeviceStorageMonitorService"


# instance fields
.field private final FULL_THRESHOLD:J

.field private mContext:Landroid/content/Context;

.field private mLowStorage:Z


# direct methods
.method static bridge synthetic -$$Nest$fgetmLowStorage(Lcom/android/server/storage/DeviceStorageMonitorServiceImpl;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/storage/DeviceStorageMonitorServiceImpl;->mLowStorage:Z

    return p0
.end method

.method static constructor <clinit>()V
    .locals 14

    .line 32
    const-string v0, "downloaded_rom"

    const-string v1, "ramdump"

    const-string v2, "MIUI/debug_log"

    const-string/jumbo v3, "step_log"

    const-string v4, "MIUI//music/album"

    const-string v5, "MIUI/music/avatar"

    const-string v6, "MIUI/music/lyric"

    const-string v7, "MIUI/.cache/resource"

    const-string v8, "MIUI/Gallery/cloud/.cache"

    const-string v9, "MIUI/Gallery/cloud/.microthumbnailFile"

    const-string v10, "MIUI/assistant"

    const-string v11, "DuoKan/Cache"

    const-string v12, "DuoKan/Downloads/Covers"

    const-string v13, "browser/MediaCache"

    filled-new-array/range {v0 .. v13}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/storage/DeviceStorageMonitorServiceImpl;->EXTERNAL_SUB_DIRS:[Ljava/lang/String;

    .line 49
    const-string v1, "anr"

    const-string/jumbo v2, "tombstones"

    const-string/jumbo v3, "system/dropbox"

    const-string/jumbo v4, "system/app_screenshot"

    const-string/jumbo v5, "system/nativedebug"

    const-string v6, "mqsas"

    filled-new-array/range {v1 .. v6}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/storage/DeviceStorageMonitorServiceImpl;->DATA_SUB_DIRS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 23
    invoke-direct {p0}, Lcom/android/server/storage/DeviceStorageMonitorServiceStub;-><init>()V

    .line 26
    sget-object v0, Landroid/util/DataUnit;->MEBIBYTES:Landroid/util/DataUnit;

    const-wide/16 v1, 0x32

    invoke-virtual {v0, v1, v2}, Landroid/util/DataUnit;->toBytes(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/storage/DeviceStorageMonitorServiceImpl;->FULL_THRESHOLD:J

    return-void
.end method

.method private deleteUnimportantFiles()V
    .locals 11

    .line 154
    const-string v0, "DeviceStorageMonitorService"

    :try_start_0
    const-string v1, "Storage space is extremely low, deleting unimportant files"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v1

    .line 156
    .local v1, "dataDir":Ljava/io/File;
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    .line 157
    .local v2, "externalDir":Ljava/io/File;
    sget-object v3, Lcom/android/server/storage/DeviceStorageMonitorServiceImpl;->EXTERNAL_SUB_DIRS:[Ljava/lang/String;

    array-length v4, v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v5, 0x0

    move v6, v5

    :goto_0
    const-string v7, "Deleting "

    if-ge v6, v4, :cond_0

    :try_start_1
    aget-object v8, v3, v6

    .line 158
    .local v8, "subDir":Ljava/lang/String;
    new-instance v9, Ljava/io/File;

    invoke-direct {v9, v2, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 161
    .local v9, "dir":Ljava/io/File;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    invoke-virtual {v9}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/miui/Shell;->remove(Ljava/lang/String;)Z

    .line 157
    nop

    .end local v8    # "subDir":Ljava/lang/String;
    .end local v9    # "dir":Ljava/io/File;
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 165
    :cond_0
    sget-object v3, Lcom/android/server/storage/DeviceStorageMonitorServiceImpl;->DATA_SUB_DIRS:[Ljava/lang/String;

    array-length v4, v3

    :goto_1
    if-ge v5, v4, :cond_2

    aget-object v6, v3, v5

    .line 166
    .local v6, "subDir":Ljava/lang/String;
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v1, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 167
    .local v8, "dir":Ljava/io/File;
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 168
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v0, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    invoke-static {v8}, Landroid/os/FileUtils;->deleteContents(Ljava/io/File;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 165
    .end local v6    # "subDir":Ljava/lang/String;
    .end local v8    # "dir":Ljava/io/File;
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 174
    .end local v1    # "dataDir":Ljava/io/File;
    .end local v2    # "externalDir":Ljava/io/File;
    :cond_2
    goto :goto_2

    .line 172
    :catchall_0
    move-exception v1

    .line 173
    .local v1, "e":Ljava/lang/Throwable;
    const-string v2, "deleteUnimportantFiles failed"

    invoke-static {v0, v2, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 175
    .end local v1    # "e":Ljava/lang/Throwable;
    :goto_2
    return-void
.end method

.method private shouldCheckVolume(Landroid/os/storage/VolumeInfo;)Z
    .locals 2
    .param p1, "vol"    # Landroid/os/storage/VolumeInfo;

    .line 91
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/storage/VolumeInfo;->getPath()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public enteringLowStorage(Landroid/os/storage/VolumeInfo;ZZJ)V
    .locals 2
    .param p1, "vol"    # Landroid/os/storage/VolumeInfo;
    .param p2, "isLow"    # Z
    .param p3, "isFull"    # Z
    .param p4, "usableBytes"    # J

    .line 118
    invoke-direct {p0, p1}, Lcom/android/server/storage/DeviceStorageMonitorServiceImpl;->shouldCheckVolume(Landroid/os/storage/VolumeInfo;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 119
    return-void

    .line 121
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Enter low storage state, isFull="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", usableBytes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "DeviceStorageMonitorService"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    if-nez p2, :cond_2

    if-eqz p3, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/android/server/storage/DeviceStorageMonitorServiceImpl;->mLowStorage:Z

    .line 123
    invoke-virtual {p0, p3}, Lcom/android/server/storage/DeviceStorageMonitorServiceImpl;->startCleanerActivity(Z)V

    .line 124
    if-eqz p3, :cond_3

    .line 127
    invoke-direct {p0}, Lcom/android/server/storage/DeviceStorageMonitorServiceImpl;->deleteUnimportantFiles()V

    .line 129
    :cond_3
    return-void
.end method

.method public getManageStorageIntent(Z)Landroid/content/Intent;
    .locals 3
    .param p1, "isFull"    # Z

    .line 110
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.miui.securitycenter.LunchCleanMaster"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 111
    .local v0, "lowStorageIntent":Landroid/content/Intent;
    if-eqz p1, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    :cond_0
    const/4 v1, 0x2

    :goto_0
    const-string v2, "level"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 112
    return-object v0
.end method

.method public getStorageFullBytes(Landroid/os/storage/StorageManager;Landroid/os/storage/VolumeInfo;)J
    .locals 6
    .param p1, "storageManager"    # Landroid/os/storage/StorageManager;
    .param p2, "vol"    # Landroid/os/storage/VolumeInfo;

    .line 96
    invoke-super {p0, p1, p2}, Lcom/android/server/storage/DeviceStorageMonitorServiceStub;->getStorageFullBytes(Landroid/os/storage/StorageManager;Landroid/os/storage/VolumeInfo;)J

    move-result-wide v0

    .line 97
    .local v0, "fullBytes":J
    invoke-direct {p0, p2}, Lcom/android/server/storage/DeviceStorageMonitorServiceImpl;->shouldCheckVolume(Landroid/os/storage/VolumeInfo;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 98
    return-wide v0

    .line 100
    :cond_0
    invoke-virtual {p2}, Landroid/os/storage/VolumeInfo;->getPath()Ljava/io/File;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/storage/StorageManager;->getStorageLowBytes(Ljava/io/File;)J

    move-result-wide v2

    .line 104
    .local v2, "lowBytes":J
    iget-wide v4, p0, Lcom/android/server/storage/DeviceStorageMonitorServiceImpl;->FULL_THRESHOLD:J

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    return-wide v4
.end method

.method public isMIUI()Z
    .locals 1

    .line 63
    const/4 v0, 0x1

    return v0
.end method

.method public leavingLowStorage(Landroid/os/storage/VolumeInfo;J)V
    .locals 2
    .param p1, "vol"    # Landroid/os/storage/VolumeInfo;
    .param p2, "usableBytes"    # J

    .line 133
    invoke-direct {p0, p1}, Lcom/android/server/storage/DeviceStorageMonitorServiceImpl;->shouldCheckVolume(Landroid/os/storage/VolumeInfo;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 134
    return-void

    .line 136
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/storage/DeviceStorageMonitorServiceImpl;->mLowStorage:Z

    .line 137
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Leave low storage state, usableBytes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "DeviceStorageMonitorService"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    return-void
.end method

.method public onBootPhase(Landroid/content/Context;I)V
    .locals 4
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "phase"    # I

    .line 67
    iput-object p1, p0, Lcom/android/server/storage/DeviceStorageMonitorServiceImpl;->mContext:Landroid/content/Context;

    .line 68
    const/16 v0, 0x3e8

    if-ne p2, v0, :cond_0

    .line 69
    const-class v0, Lcom/android/server/storage/DeviceStorageMonitorInternal;

    .line 70
    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/storage/DeviceStorageMonitorInternal;

    .line 71
    .local v0, "localService":Lcom/android/server/storage/DeviceStorageMonitorInternal;
    new-instance v1, Lcom/android/server/storage/DeviceStorageMonitorServiceImpl$1;

    invoke-direct {v1, p0, v0}, Lcom/android/server/storage/DeviceStorageMonitorServiceImpl$1;-><init>(Lcom/android/server/storage/DeviceStorageMonitorServiceImpl;Lcom/android/server/storage/DeviceStorageMonitorInternal;)V

    .line 82
    .local v1, "mBroadcastReceiver":Landroid/content/BroadcastReceiver;
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 83
    .local v2, "filter":Landroid/content/IntentFilter;
    const-string v3, "android.intent.action.USER_PRESENT"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 84
    const-string v3, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 85
    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 87
    .end local v0    # "localService":Lcom/android/server/storage/DeviceStorageMonitorInternal;
    .end local v1    # "mBroadcastReceiver":Landroid/content/BroadcastReceiver;
    .end local v2    # "filter":Landroid/content/IntentFilter;
    :cond_0
    return-void
.end method

.method public startCleanerActivity(Z)V
    .locals 4
    .param p1, "isFull"    # Z

    .line 142
    const-string v0, "DeviceStorageMonitorService"

    :try_start_0
    const-string v1, "Running out of disk space, launching CleanMaster"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.miui.securitycenter.action.START_LOW_MEMORY_CLEAN"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 144
    .local v1, "lowStorageIntent":Landroid/content/Intent;
    const-string v2, "level"

    if-eqz p1, :cond_0

    const/4 v3, 0x3

    goto :goto_0

    :cond_0
    const/4 v3, 0x2

    :goto_0
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 145
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 146
    iget-object v2, p0, Lcom/android/server/storage/DeviceStorageMonitorServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 149
    .end local v1    # "lowStorageIntent":Landroid/content/Intent;
    goto :goto_1

    .line 147
    :catch_0
    move-exception v1

    .line 148
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to start CleanMaster "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_1
    return-void
.end method
