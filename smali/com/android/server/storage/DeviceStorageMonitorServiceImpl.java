public class com.android.server.storage.DeviceStorageMonitorServiceImpl extends com.android.server.storage.DeviceStorageMonitorServiceStub {
	 /* .source "DeviceStorageMonitorServiceImpl.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.storage.DeviceStorageMonitorServiceStub$$" */
} // .end annotation
/* # static fields */
private static final java.lang.String DATA_SUB_DIRS;
private static final java.lang.String EXTERNAL_SUB_DIRS;
private static final Integer LEVEL_DEEP_CLEAN;
private static final Integer LEVEL_NORMAL_CLEAN;
private static final java.lang.String TAG;
/* # instance fields */
private final Long FULL_THRESHOLD;
private android.content.Context mContext;
private Boolean mLowStorage;
/* # direct methods */
static Boolean -$$Nest$fgetmLowStorage ( com.android.server.storage.DeviceStorageMonitorServiceImpl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/android/server/storage/DeviceStorageMonitorServiceImpl;->mLowStorage:Z */
} // .end method
static com.android.server.storage.DeviceStorageMonitorServiceImpl ( ) {
	 /* .locals 14 */
	 /* .line 32 */
	 final String v0 = "downloaded_rom"; // const-string v0, "downloaded_rom"
	 final String v1 = "ramdump"; // const-string v1, "ramdump"
	 final String v2 = "MIUI/debug_log"; // const-string v2, "MIUI/debug_log"
	 /* const-string/jumbo v3, "step_log" */
	 final String v4 = "MIUI//music/album"; // const-string v4, "MIUI//music/album"
	 final String v5 = "MIUI/music/avatar"; // const-string v5, "MIUI/music/avatar"
	 final String v6 = "MIUI/music/lyric"; // const-string v6, "MIUI/music/lyric"
	 final String v7 = "MIUI/.cache/resource"; // const-string v7, "MIUI/.cache/resource"
	 final String v8 = "MIUI/Gallery/cloud/.cache"; // const-string v8, "MIUI/Gallery/cloud/.cache"
	 final String v9 = "MIUI/Gallery/cloud/.microthumbnailFile"; // const-string v9, "MIUI/Gallery/cloud/.microthumbnailFile"
	 final String v10 = "MIUI/assistant"; // const-string v10, "MIUI/assistant"
	 final String v11 = "DuoKan/Cache"; // const-string v11, "DuoKan/Cache"
	 final String v12 = "DuoKan/Downloads/Covers"; // const-string v12, "DuoKan/Downloads/Covers"
	 final String v13 = "browser/MediaCache"; // const-string v13, "browser/MediaCache"
	 /* filled-new-array/range {v0 ..v13}, [Ljava/lang/String; */
	 /* .line 49 */
	 final String v1 = "anr"; // const-string v1, "anr"
	 /* const-string/jumbo v2, "tombstones" */
	 /* const-string/jumbo v3, "system/dropbox" */
	 /* const-string/jumbo v4, "system/app_screenshot" */
	 /* const-string/jumbo v5, "system/nativedebug" */
	 final String v6 = "mqsas"; // const-string v6, "mqsas"
	 /* filled-new-array/range {v1 ..v6}, [Ljava/lang/String; */
	 return;
} // .end method
public com.android.server.storage.DeviceStorageMonitorServiceImpl ( ) {
	 /* .locals 3 */
	 /* .line 23 */
	 /* invoke-direct {p0}, Lcom/android/server/storage/DeviceStorageMonitorServiceStub;-><init>()V */
	 /* .line 26 */
	 v0 = android.util.DataUnit.MEBIBYTES;
	 /* const-wide/16 v1, 0x32 */
	 (( android.util.DataUnit ) v0 ).toBytes ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/util/DataUnit;->toBytes(J)J
	 /* move-result-wide v0 */
	 /* iput-wide v0, p0, Lcom/android/server/storage/DeviceStorageMonitorServiceImpl;->FULL_THRESHOLD:J */
	 return;
} // .end method
private void deleteUnimportantFiles ( ) {
	 /* .locals 11 */
	 /* .line 154 */
	 final String v0 = "DeviceStorageMonitorService"; // const-string v0, "DeviceStorageMonitorService"
	 try { // :try_start_0
		 final String v1 = "Storage space is extremely low, deleting unimportant files"; // const-string v1, "Storage space is extremely low, deleting unimportant files"
		 android.util.Slog .i ( v0,v1 );
		 /* .line 155 */
		 android.os.Environment .getDataDirectory ( );
		 /* .line 156 */
		 /* .local v1, "dataDir":Ljava/io/File; */
		 android.os.Environment .getExternalStorageDirectory ( );
		 /* .line 157 */
		 /* .local v2, "externalDir":Ljava/io/File; */
		 v3 = com.android.server.storage.DeviceStorageMonitorServiceImpl.EXTERNAL_SUB_DIRS;
		 /* array-length v4, v3 */
		 /* :try_end_0 */
		 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
		 int v5 = 0; // const/4 v5, 0x0
		 /* move v6, v5 */
	 } // :goto_0
	 final String v7 = "Deleting "; // const-string v7, "Deleting "
	 /* if-ge v6, v4, :cond_0 */
	 try { // :try_start_1
		 /* aget-object v8, v3, v6 */
		 /* .line 158 */
		 /* .local v8, "subDir":Ljava/lang/String; */
		 /* new-instance v9, Ljava/io/File; */
		 /* invoke-direct {v9, v2, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
		 /* .line 161 */
		 /* .local v9, "dir":Ljava/io/File; */
		 /* new-instance v10, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
		 (( java.lang.StringBuilder ) v10 ).append ( v7 ); // invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v7 ).append ( v9 ); // invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Slog .i ( v0,v7 );
		 /* .line 163 */
		 (( java.io.File ) v9 ).getAbsolutePath ( ); // invoke-virtual {v9}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
		 android.miui.Shell .remove ( v7 );
		 /* .line 157 */
		 /* nop */
	 } // .end local v8 # "subDir":Ljava/lang/String;
} // .end local v9 # "dir":Ljava/io/File;
/* add-int/lit8 v6, v6, 0x1 */
/* .line 165 */
} // :cond_0
v3 = com.android.server.storage.DeviceStorageMonitorServiceImpl.DATA_SUB_DIRS;
/* array-length v4, v3 */
} // :goto_1
/* if-ge v5, v4, :cond_2 */
/* aget-object v6, v3, v5 */
/* .line 166 */
/* .local v6, "subDir":Ljava/lang/String; */
/* new-instance v8, Ljava/io/File; */
/* invoke-direct {v8, v1, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* .line 167 */
/* .local v8, "dir":Ljava/io/File; */
v9 = (( java.io.File ) v8 ).exists ( ); // invoke-virtual {v8}, Ljava/io/File;->exists()Z
if ( v9 != null) { // if-eqz v9, :cond_1
/* .line 168 */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v9 ).append ( v7 ); // invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v8 ); // invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v9 );
/* .line 169 */
android.os.FileUtils .deleteContents ( v8 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 165 */
} // .end local v6 # "subDir":Ljava/lang/String;
} // .end local v8 # "dir":Ljava/io/File;
} // :cond_1
/* add-int/lit8 v5, v5, 0x1 */
/* .line 174 */
} // .end local v1 # "dataDir":Ljava/io/File;
} // .end local v2 # "externalDir":Ljava/io/File;
} // :cond_2
/* .line 172 */
/* :catchall_0 */
/* move-exception v1 */
/* .line 173 */
/* .local v1, "e":Ljava/lang/Throwable; */
final String v2 = "deleteUnimportantFiles failed"; // const-string v2, "deleteUnimportantFiles failed"
android.util.Slog .w ( v0,v2,v1 );
/* .line 175 */
} // .end local v1 # "e":Ljava/lang/Throwable;
} // :goto_2
return;
} // .end method
private Boolean shouldCheckVolume ( android.os.storage.VolumeInfo p0 ) {
/* .locals 2 */
/* .param p1, "vol" # Landroid/os/storage/VolumeInfo; */
/* .line 91 */
android.os.Environment .getDataDirectory ( );
(( android.os.storage.VolumeInfo ) p1 ).getPath ( ); // invoke-virtual {p1}, Landroid/os/storage/VolumeInfo;->getPath()Ljava/io/File;
v0 = (( java.io.File ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/File;->equals(Ljava/lang/Object;)Z
} // .end method
/* # virtual methods */
public void enteringLowStorage ( android.os.storage.VolumeInfo p0, Boolean p1, Boolean p2, Long p3 ) {
/* .locals 2 */
/* .param p1, "vol" # Landroid/os/storage/VolumeInfo; */
/* .param p2, "isLow" # Z */
/* .param p3, "isFull" # Z */
/* .param p4, "usableBytes" # J */
/* .line 118 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/storage/DeviceStorageMonitorServiceImpl;->shouldCheckVolume(Landroid/os/storage/VolumeInfo;)Z */
/* if-nez v0, :cond_0 */
/* .line 119 */
return;
/* .line 121 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Enter low storage state, isFull="; // const-string v1, "Enter low storage state, isFull="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = ", usableBytes="; // const-string v1, ", usableBytes="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p4, p5 ); // invoke-virtual {v0, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "DeviceStorageMonitorService"; // const-string v1, "DeviceStorageMonitorService"
android.util.Slog .i ( v1,v0 );
/* .line 122 */
/* if-nez p2, :cond_2 */
if ( p3 != null) { // if-eqz p3, :cond_1
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // :cond_2
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
/* iput-boolean v0, p0, Lcom/android/server/storage/DeviceStorageMonitorServiceImpl;->mLowStorage:Z */
/* .line 123 */
(( com.android.server.storage.DeviceStorageMonitorServiceImpl ) p0 ).startCleanerActivity ( p3 ); // invoke-virtual {p0, p3}, Lcom/android/server/storage/DeviceStorageMonitorServiceImpl;->startCleanerActivity(Z)V
/* .line 124 */
if ( p3 != null) { // if-eqz p3, :cond_3
/* .line 127 */
/* invoke-direct {p0}, Lcom/android/server/storage/DeviceStorageMonitorServiceImpl;->deleteUnimportantFiles()V */
/* .line 129 */
} // :cond_3
return;
} // .end method
public android.content.Intent getManageStorageIntent ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "isFull" # Z */
/* .line 110 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "com.miui.securitycenter.LunchCleanMaster"; // const-string v1, "com.miui.securitycenter.LunchCleanMaster"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 111 */
/* .local v0, "lowStorageIntent":Landroid/content/Intent; */
if ( p1 != null) { // if-eqz p1, :cond_0
int v1 = 3; // const/4 v1, 0x3
} // :cond_0
int v1 = 2; // const/4 v1, 0x2
} // :goto_0
final String v2 = "level"; // const-string v2, "level"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 112 */
} // .end method
public Long getStorageFullBytes ( android.os.storage.StorageManager p0, android.os.storage.VolumeInfo p1 ) {
/* .locals 6 */
/* .param p1, "storageManager" # Landroid/os/storage/StorageManager; */
/* .param p2, "vol" # Landroid/os/storage/VolumeInfo; */
/* .line 96 */
/* invoke-super {p0, p1, p2}, Lcom/android/server/storage/DeviceStorageMonitorServiceStub;->getStorageFullBytes(Landroid/os/storage/StorageManager;Landroid/os/storage/VolumeInfo;)J */
/* move-result-wide v0 */
/* .line 97 */
/* .local v0, "fullBytes":J */
v2 = /* invoke-direct {p0, p2}, Lcom/android/server/storage/DeviceStorageMonitorServiceImpl;->shouldCheckVolume(Landroid/os/storage/VolumeInfo;)Z */
/* if-nez v2, :cond_0 */
/* .line 98 */
/* return-wide v0 */
/* .line 100 */
} // :cond_0
(( android.os.storage.VolumeInfo ) p2 ).getPath ( ); // invoke-virtual {p2}, Landroid/os/storage/VolumeInfo;->getPath()Ljava/io/File;
(( android.os.storage.StorageManager ) p1 ).getStorageLowBytes ( v2 ); // invoke-virtual {p1, v2}, Landroid/os/storage/StorageManager;->getStorageLowBytes(Ljava/io/File;)J
/* move-result-wide v2 */
/* .line 104 */
/* .local v2, "lowBytes":J */
/* iget-wide v4, p0, Lcom/android/server/storage/DeviceStorageMonitorServiceImpl;->FULL_THRESHOLD:J */
java.lang.Math .max ( v4,v5,v0,v1 );
/* move-result-wide v4 */
java.lang.Math .min ( v2,v3,v4,v5 );
/* move-result-wide v4 */
/* return-wide v4 */
} // .end method
public Boolean isMIUI ( ) {
/* .locals 1 */
/* .line 63 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
public void leavingLowStorage ( android.os.storage.VolumeInfo p0, Long p1 ) {
/* .locals 2 */
/* .param p1, "vol" # Landroid/os/storage/VolumeInfo; */
/* .param p2, "usableBytes" # J */
/* .line 133 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/storage/DeviceStorageMonitorServiceImpl;->shouldCheckVolume(Landroid/os/storage/VolumeInfo;)Z */
/* if-nez v0, :cond_0 */
/* .line 134 */
return;
/* .line 136 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/storage/DeviceStorageMonitorServiceImpl;->mLowStorage:Z */
/* .line 137 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Leave low storage state, usableBytes="; // const-string v1, "Leave low storage state, usableBytes="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2, p3 ); // invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "DeviceStorageMonitorService"; // const-string v1, "DeviceStorageMonitorService"
android.util.Slog .i ( v1,v0 );
/* .line 138 */
return;
} // .end method
public void onBootPhase ( android.content.Context p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "ctx" # Landroid/content/Context; */
/* .param p2, "phase" # I */
/* .line 67 */
this.mContext = p1;
/* .line 68 */
/* const/16 v0, 0x3e8 */
/* if-ne p2, v0, :cond_0 */
/* .line 69 */
/* const-class v0, Lcom/android/server/storage/DeviceStorageMonitorInternal; */
/* .line 70 */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/storage/DeviceStorageMonitorInternal; */
/* .line 71 */
/* .local v0, "localService":Lcom/android/server/storage/DeviceStorageMonitorInternal; */
/* new-instance v1, Lcom/android/server/storage/DeviceStorageMonitorServiceImpl$1; */
/* invoke-direct {v1, p0, v0}, Lcom/android/server/storage/DeviceStorageMonitorServiceImpl$1;-><init>(Lcom/android/server/storage/DeviceStorageMonitorServiceImpl;Lcom/android/server/storage/DeviceStorageMonitorInternal;)V */
/* .line 82 */
/* .local v1, "mBroadcastReceiver":Landroid/content/BroadcastReceiver; */
/* new-instance v2, Landroid/content/IntentFilter; */
/* invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V */
/* .line 83 */
/* .local v2, "filter":Landroid/content/IntentFilter; */
final String v3 = "android.intent.action.USER_PRESENT"; // const-string v3, "android.intent.action.USER_PRESENT"
(( android.content.IntentFilter ) v2 ).addAction ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 84 */
final String v3 = "android.intent.action.BOOT_COMPLETED"; // const-string v3, "android.intent.action.BOOT_COMPLETED"
(( android.content.IntentFilter ) v2 ).addAction ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 85 */
(( android.content.Context ) p1 ).registerReceiver ( v1, v2 ); // invoke-virtual {p1, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 87 */
} // .end local v0 # "localService":Lcom/android/server/storage/DeviceStorageMonitorInternal;
} // .end local v1 # "mBroadcastReceiver":Landroid/content/BroadcastReceiver;
} // .end local v2 # "filter":Landroid/content/IntentFilter;
} // :cond_0
return;
} // .end method
public void startCleanerActivity ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "isFull" # Z */
/* .line 142 */
final String v0 = "DeviceStorageMonitorService"; // const-string v0, "DeviceStorageMonitorService"
try { // :try_start_0
final String v1 = "Running out of disk space, launching CleanMaster"; // const-string v1, "Running out of disk space, launching CleanMaster"
android.util.Slog .i ( v0,v1 );
/* .line 143 */
/* new-instance v1, Landroid/content/Intent; */
final String v2 = "com.miui.securitycenter.action.START_LOW_MEMORY_CLEAN"; // const-string v2, "com.miui.securitycenter.action.START_LOW_MEMORY_CLEAN"
/* invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 144 */
/* .local v1, "lowStorageIntent":Landroid/content/Intent; */
final String v2 = "level"; // const-string v2, "level"
if ( p1 != null) { // if-eqz p1, :cond_0
int v3 = 3; // const/4 v3, 0x3
} // :cond_0
int v3 = 2; // const/4 v3, 0x2
} // :goto_0
(( android.content.Intent ) v1 ).putExtra ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 145 */
/* const/high16 v2, 0x10000000 */
(( android.content.Intent ) v1 ).addFlags ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 146 */
v2 = this.mContext;
(( android.content.Context ) v2 ).startActivity ( v1 ); // invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 149 */
} // .end local v1 # "lowStorageIntent":Landroid/content/Intent;
/* .line 147 */
/* :catch_0 */
/* move-exception v1 */
/* .line 148 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Failed to start CleanMaster "; // const-string v3, "Failed to start CleanMaster "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v2 );
/* .line 150 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_1
return;
} // .end method
