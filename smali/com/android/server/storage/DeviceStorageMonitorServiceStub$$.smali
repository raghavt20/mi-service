.class public final Lcom/android/server/storage/DeviceStorageMonitorServiceStub$$;
.super Ljava/lang/Object;
.source "DeviceStorageMonitorServiceStub$$.java"

# interfaces
.implements Lcom/miui/base/MiuiStubRegistry$ImplProviderManifest;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final collectImplProviders(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/miui/base/MiuiStubRegistry$ImplProvider<",
            "*>;>;)V"
        }
    .end annotation

    .line 20
    .local p1, "outProviders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/miui/base/MiuiStubRegistry$ImplProvider<*>;>;"
    new-instance v0, Lcom/android/server/storage/DeviceStorageMonitorServiceImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/storage/DeviceStorageMonitorServiceImpl$Provider;-><init>()V

    const-string v1, "com.android.server.storage.DeviceStorageMonitorServiceStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    new-instance v0, Lcom/android/server/StorageManagerServiceImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/StorageManagerServiceImpl$Provider;-><init>()V

    const-string v1, "com.android.server.StorageManagerServiceStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    new-instance v0, Lcom/android/server/MountServiceIdlerImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/MountServiceIdlerImpl$Provider;-><init>()V

    const-string v1, "com.android.server.MountServiceIdlerStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    return-void
.end method
