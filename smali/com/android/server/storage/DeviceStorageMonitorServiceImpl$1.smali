.class Lcom/android/server/storage/DeviceStorageMonitorServiceImpl$1;
.super Landroid/content/BroadcastReceiver;
.source "DeviceStorageMonitorServiceImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/storage/DeviceStorageMonitorServiceImpl;->onBootPhase(Landroid/content/Context;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/storage/DeviceStorageMonitorServiceImpl;

.field final synthetic val$localService:Lcom/android/server/storage/DeviceStorageMonitorInternal;


# direct methods
.method constructor <init>(Lcom/android/server/storage/DeviceStorageMonitorServiceImpl;Lcom/android/server/storage/DeviceStorageMonitorInternal;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/storage/DeviceStorageMonitorServiceImpl;

    .line 71
    iput-object p1, p0, Lcom/android/server/storage/DeviceStorageMonitorServiceImpl$1;->this$0:Lcom/android/server/storage/DeviceStorageMonitorServiceImpl;

    iput-object p2, p0, Lcom/android/server/storage/DeviceStorageMonitorServiceImpl$1;->val$localService:Lcom/android/server/storage/DeviceStorageMonitorInternal;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 74
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 75
    .local v0, "action":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/storage/DeviceStorageMonitorServiceImpl$1;->this$0:Lcom/android/server/storage/DeviceStorageMonitorServiceImpl;

    invoke-static {v1}, Lcom/android/server/storage/DeviceStorageMonitorServiceImpl;->-$$Nest$fgetmLowStorage(Lcom/android/server/storage/DeviceStorageMonitorServiceImpl;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 76
    iget-object v1, p0, Lcom/android/server/storage/DeviceStorageMonitorServiceImpl$1;->val$localService:Lcom/android/server/storage/DeviceStorageMonitorInternal;

    invoke-interface {v1}, Lcom/android/server/storage/DeviceStorageMonitorInternal;->checkMemory()V

    goto :goto_0

    .line 77
    :cond_0
    const-string v1, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 78
    iget-object v1, p0, Lcom/android/server/storage/DeviceStorageMonitorServiceImpl$1;->val$localService:Lcom/android/server/storage/DeviceStorageMonitorInternal;

    invoke-interface {v1}, Lcom/android/server/storage/DeviceStorageMonitorInternal;->checkMemory()V

    .line 80
    :cond_1
    :goto_0
    return-void
.end method
