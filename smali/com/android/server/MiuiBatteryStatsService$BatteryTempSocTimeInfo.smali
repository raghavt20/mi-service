.class Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;
.super Ljava/lang/Object;
.source "MiuiBatteryStatsService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/MiuiBatteryStatsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "BatteryTempSocTimeInfo"
.end annotation


# instance fields
.field private final BATTERT_INFO_FILE:Ljava/lang/String;

.field private final COLLECT_DAYS:I

.field private final IS_SATISFY_TEMP_SOC_CONDITION:Ljava/lang/String;

.field private RValue:Ljava/lang/String;

.field private final STOP_COLLECT_DATA_TIME:Ljava/lang/String;

.field private final TOTAL_TIME_IN38:Ljava/lang/String;

.field private final TOTAL_TIME_IN43:Ljava/lang/String;

.field private final TOTAL_TIME_IN_HIGH_SOC:Ljava/lang/String;

.field private allDaysData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private startTimeIn38C:J

.field private startTimeIn43C:J

.field private startTimeInHighSoc:J

.field private stopCollectTempSocTime:J

.field final synthetic this$0:Lcom/android/server/MiuiBatteryStatsService;

.field private totalTimeIn38C:J

.field private totalTimeIn43C:J

.field private totalTimeInHighSoc:J


# direct methods
.method static bridge synthetic -$$Nest$fgetstopCollectTempSocTime(Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;)J
    .locals 2

    iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->stopCollectTempSocTime:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$mreadDataFromFile(Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->readDataFromFile()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mwriteDataToFile(Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->writeDataToFile()V

    return-void
.end method

.method constructor <init>(Lcom/android/server/MiuiBatteryStatsService;)V
    .locals 2
    .param p1, "this$0"    # Lcom/android/server/MiuiBatteryStatsService;

    .line 1457
    iput-object p1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1458
    const-string v0, "/data/system/battery-info.txt"

    iput-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->BATTERT_INFO_FILE:Ljava/lang/String;

    .line 1459
    const-string v0, "mIsSatisfyTempSocCondition="

    iput-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->IS_SATISFY_TEMP_SOC_CONDITION:Ljava/lang/String;

    .line 1460
    const-string/jumbo v0, "stopCollectTempSocTime="

    iput-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->STOP_COLLECT_DATA_TIME:Ljava/lang/String;

    .line 1461
    const-string/jumbo v0, "totalTimeIn38C="

    iput-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->TOTAL_TIME_IN38:Ljava/lang/String;

    .line 1462
    const-string/jumbo v0, "totalTimeIn43C="

    iput-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->TOTAL_TIME_IN43:Ljava/lang/String;

    .line 1463
    const-string/jumbo v0, "totalTimeInHighSoc="

    iput-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->TOTAL_TIME_IN_HIGH_SOC:Ljava/lang/String;

    .line 1464
    const/4 v0, 0x7

    iput v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->COLLECT_DAYS:I

    .line 1470
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeIn38C:J

    .line 1471
    iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeIn43C:J

    .line 1472
    iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeInHighSoc:J

    .line 1473
    iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->stopCollectTempSocTime:J

    .line 1475
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->allDaysData:Ljava/util/List;

    return-void
.end method

.method private getRValue(I)I
    .locals 5
    .param p1, "index"    # I

    .line 1576
    const/4 v0, 0x0

    .line 1577
    .local v0, "sum":I
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->allDaysData:Ljava/util/List;

    const/4 v3, 0x6

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I

    move-result v1

    iget-object v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->allDaysData:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I

    move-result v2

    sub-int/2addr v1, v2

    .line 1578
    .end local v0    # "sum":I
    .local v1, "sum":I
    return v1
.end method

.method private getTotalTime(I)J
    .locals 5
    .param p1, "index"    # I

    .line 1569
    const-wide/16 v0, 0x0

    .line 1570
    .local v0, "sum":J
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->allDaysData:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 1571
    iget-object v3, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->allDaysData:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    add-long/2addr v0, v3

    .line 1570
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1573
    .end local v2    # "i":I
    :cond_0
    return-wide v0
.end method

.method private parseLong(Ljava/lang/String;)J
    .locals 3
    .param p1, "time"    # Ljava/lang/String;

    .line 1657
    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    return-wide v0

    .line 1658
    :catch_0
    move-exception v0

    .line 1659
    .local v0, "e":Ljava/lang/NumberFormatException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid string time "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiBatteryStatsService"

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1660
    const-wide/16 v1, 0x0

    return-wide v1
.end method

.method private parseString(J)Ljava/lang/String;
    .locals 1
    .param p1, "time"    # J

    .line 1653
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private readDataFromFile()V
    .locals 13

    .line 1606
    const-string v0, ","

    const-string v1, ", "

    new-instance v2, Ljava/io/File;

    const-string v3, "/data/system/battery-info.txt"

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1607
    .local v2, "battInfoFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    const-string v4, "MiuiBatteryStatsService"

    if-nez v3, :cond_0

    .line 1608
    const-string v0, "Can\'t find battery info file"

    invoke-static {v4, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1609
    return-void

    .line 1612
    :cond_0
    const/4 v3, 0x0

    const/4 v5, 0x0

    :try_start_0
    invoke-static {v2, v3, v5}, Landroid/os/FileUtils;->readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1613
    .local v3, "text":Ljava/lang/String;
    const-string v5, "\n"

    invoke-virtual {v3, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 1614
    .local v5, "lines":[Ljava/lang/String;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    invoke-static {v5}, Lcom/android/internal/util/ArrayUtils;->size([Ljava/lang/Object;)I

    move-result v7
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const-string/jumbo v8, "totalTimeInHighSoc="

    const-string/jumbo v9, "totalTimeIn43C="

    const-string/jumbo v10, "totalTimeIn38C="

    const-string/jumbo v11, "stopCollectTempSocTime="

    const-string v12, "mIsSatisfyTempSocCondition="

    if-ge v6, v7, :cond_8

    .line 1615
    :try_start_1
    aget-object v7, v5, v6

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1616
    goto/16 :goto_1

    .line 1618
    :cond_1
    aget-object v7, v5, v6

    invoke-virtual {v7, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1619
    aget-object v7, v5, v6

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    invoke-virtual {v7, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v7

    invoke-static {v7}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$sfputmIsSatisfyTempSocCondition(Z)V

    .line 1621
    :cond_2
    aget-object v7, v5, v6

    invoke-virtual {v7, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1622
    aget-object v7, v5, v6

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    iput-wide v7, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->stopCollectTempSocTime:J

    .line 1623
    goto :goto_1

    .line 1625
    :cond_3
    aget-object v7, v5, v6

    invoke-virtual {v7, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1626
    aget-object v7, v5, v6

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    iput-wide v7, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeIn38C:J

    .line 1627
    goto :goto_1

    .line 1629
    :cond_4
    aget-object v7, v5, v6

    invoke-virtual {v7, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1630
    aget-object v7, v5, v6

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    iput-wide v7, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeIn43C:J

    .line 1631
    goto :goto_1

    .line 1633
    :cond_5
    aget-object v7, v5, v6

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 1634
    aget-object v7, v5, v6

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    iput-wide v7, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeInHighSoc:J

    .line 1635
    goto :goto_1

    .line 1637
    :cond_6
    aget-object v7, v5, v6

    invoke-virtual {v7, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/android/internal/util/ArrayUtils;->size([Ljava/lang/Object;)I

    move-result v7

    const/4 v8, 0x4

    if-ne v7, v8, :cond_7

    .line 1638
    aget-object v7, v5, v6

    invoke-virtual {v7, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 1639
    .local v7, "listOfCurrentline":[Ljava/lang/String;
    iget-object v8, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->allDaysData:Ljava/util/List;

    invoke-static {v7}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1614
    .end local v7    # "listOfCurrentline":[Ljava/lang/String;
    :cond_7
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_0

    .line 1642
    .end local v6    # "i":I
    :cond_8
    iget-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetDEBUG(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1643
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$sfgetmIsSatisfyTempSocCondition()Z

    move-result v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v6, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v6}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    move-result-object v6

    iget-wide v11, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->stopCollectTempSocTime:J

    .line 1644
    invoke-static {v6, v11, v12}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->-$$Nest$mformatTime(Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v6, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeIn38C:J

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v6, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeIn43C:J

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v6, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeInHighSoc:J

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", allDaysData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->allDaysData:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1643
    invoke-static {v4, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1650
    .end local v3    # "text":Ljava/lang/String;
    .end local v5    # "lines":[Ljava/lang/String;
    :cond_9
    goto :goto_2

    .line 1648
    :catch_0
    move-exception v0

    .line 1649
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "can\'t read file : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1651
    .end local v0    # "e":Ljava/io/IOException;
    :goto_2
    return-void
.end method

.method private writeDataToFile()V
    .locals 8

    .line 1581
    invoke-virtual {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->stopCollectTempSocData()V

    .line 1582
    new-instance v0, Landroid/util/AtomicFile;

    new-instance v1, Ljava/io/File;

    const-string v2, "/data/system/battery-info.txt"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V

    .line 1583
    .local v0, "file":Landroid/util/AtomicFile;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1584
    .local v1, "lines":Ljava/lang/StringBuilder;
    const-string v2, "mIsSatisfyTempSocCondition="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$sfgetmIsSatisfyTempSocCondition()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1585
    const-string/jumbo v2, "stopCollectTempSocTime="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->stopCollectTempSocTime:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1586
    const-string/jumbo v2, "totalTimeIn38C="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeIn38C:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1587
    const-string/jumbo v2, "totalTimeIn43C="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeIn43C:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1588
    const-string/jumbo v2, "totalTimeInHighSoc="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeInHighSoc:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1589
    iget-object v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->allDaysData:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    .line 1590
    .local v4, "list":Ljava/util/List;
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    if-ge v5, v6, :cond_0

    .line 1591
    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1590
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 1593
    .end local v5    # "i":I
    :cond_0
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1594
    .end local v4    # "list":Ljava/util/List;
    goto :goto_0

    .line 1595
    :cond_1
    const/4 v2, 0x0

    .line 1597
    .local v2, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    invoke-virtual {v0}, Landroid/util/AtomicFile;->startWrite()Ljava/io/FileOutputStream;

    move-result-object v3

    move-object v2, v3

    .line 1598
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/FileOutputStream;->write([B)V

    .line 1599
    invoke-virtual {v0, v2}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1603
    goto :goto_2

    .line 1600
    :catch_0
    move-exception v3

    .line 1601
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v0, v2}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V

    .line 1602
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "failed to write battery_info.txt : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "MiuiBatteryStatsService"

    invoke-static {v5, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1604
    .end local v3    # "e":Ljava/io/IOException;
    :goto_2
    return-void
.end method


# virtual methods
.method public clearData()V
    .locals 2

    .line 1563
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeIn38C:J

    .line 1564
    iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeIn43C:J

    .line 1565
    iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeInHighSoc:J

    .line 1566
    iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->stopCollectTempSocTime:J

    .line 1567
    return-void
.end method

.method public collectTempSocData(II)V
    .locals 11
    .param p1, "temp"    # I
    .param p2, "soc"    # I

    .line 1478
    iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->stopCollectTempSocTime:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1479
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/32 v4, 0x5265c00

    add-long/2addr v0, v4

    iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->stopCollectTempSocTime:J

    .line 1480
    iget-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetDEBUG(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "stopCollectTempSocTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    move-result-object v1

    iget-wide v4, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->stopCollectTempSocTime:J

    invoke-static {v1, v4, v5}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->-$$Nest$mformatTime(Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiBatteryStatsService"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1482
    :cond_0
    const/16 v0, 0x17c

    if-lt p1, v0, :cond_1

    .line 1483
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeIn38C:J

    .line 1485
    :cond_1
    const/16 v1, 0x1ae

    if-lt p1, v1, :cond_2

    .line 1486
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeIn43C:J

    .line 1488
    :cond_2
    const/16 v4, 0x63

    if-lt p2, v4, :cond_3

    .line 1489
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    iput-wide v5, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeInHighSoc:J

    .line 1491
    :cond_3
    if-ge p1, v0, :cond_4

    iget-wide v5, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeIn38C:J

    cmp-long v0, v5, v2

    if-eqz v0, :cond_4

    .line 1492
    iget-wide v5, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeIn38C:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    iget-wide v9, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeIn38C:J

    sub-long/2addr v7, v9

    add-long/2addr v5, v7

    iput-wide v5, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeIn38C:J

    .line 1493
    iput-wide v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeIn38C:J

    .line 1495
    :cond_4
    if-ge p1, v1, :cond_5

    iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeIn43C:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_5

    .line 1496
    iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeIn43C:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    iget-wide v7, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeIn43C:J

    sub-long/2addr v5, v7

    add-long/2addr v0, v5

    iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeIn43C:J

    .line 1497
    iput-wide v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeIn43C:J

    .line 1499
    :cond_5
    if-ge p2, v4, :cond_6

    iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeInHighSoc:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_6

    .line 1500
    iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeInHighSoc:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeInHighSoc:J

    sub-long/2addr v4, v6

    add-long/2addr v0, v4

    iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeInHighSoc:J

    .line 1501
    iput-wide v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeInHighSoc:J

    .line 1503
    :cond_6
    return-void
.end method

.method public processTempSocData()V
    .locals 12

    .line 1505
    invoke-virtual {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->stopCollectTempSocData()V

    .line 1506
    const-string v0, "persist.vendor.smart.battMntor"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1507
    iget-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v0

    const-string v2, "calc_rvalue"

    invoke-virtual {v0, v2}, Lmiui/util/IMiCharge;->getMiChargePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->RValue:Ljava/lang/String;

    goto :goto_0

    .line 1509
    :cond_0
    const-string v0, "0"

    iput-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->RValue:Ljava/lang/String;

    .line 1511
    :goto_0
    iget-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->allDaysData:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const-string v2, "MiuiBatteryStatsService"

    const/4 v3, 0x7

    if-ge v0, v3, :cond_1

    .line 1512
    invoke-virtual {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->setDataToList()V

    .line 1513
    iget-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetDEBUG(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "allDaysData.size()="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->allDaysData:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1515
    :cond_1
    iget-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->allDaysData:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v3, :cond_9

    .line 1517
    invoke-direct {p0, v1}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->getTotalTime(I)J

    move-result-wide v3

    const-wide/32 v5, 0x134fd900

    cmp-long v0, v3, v5

    const/4 v3, 0x1

    if-ltz v0, :cond_2

    move v0, v3

    goto :goto_1

    :cond_2
    move v0, v1

    .line 1519
    .local v0, "isBattTempContinueAbove38C":Z
    :goto_1
    invoke-direct {p0, v3}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->getTotalTime(I)J

    move-result-wide v4

    const-wide/32 v6, 0xf053700

    cmp-long v4, v4, v6

    if-ltz v4, :cond_3

    move v4, v3

    goto :goto_2

    :cond_3
    move v4, v1

    .line 1521
    .local v4, "isBattTempContinueAbove43C":Z
    :goto_2
    const/4 v5, 0x2

    invoke-direct {p0, v5}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->getTotalTime(I)J

    move-result-wide v6

    const-wide/32 v8, 0x15752a00

    cmp-long v6, v6, v8

    if-ltz v6, :cond_4

    move v6, v3

    goto :goto_3

    :cond_4
    move v6, v1

    .line 1523
    .local v6, "isSocContinueAbove99":Z
    :goto_3
    const/4 v7, 0x3

    invoke-direct {p0, v7}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->getRValue(I)I

    move-result v8

    const/16 v9, 0xfc

    if-lt v8, v9, :cond_5

    move v8, v3

    goto :goto_4

    :cond_5
    move v8, v1

    .line 1524
    .local v8, "isSatisfyRCondition":Z
    :goto_4
    iget-object v9, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v9}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetDEBUG(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 1525
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Time of battery temp above 38c in 7 Days is "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-direct {p0, v1}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->getTotalTime(I)J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "(ms), battery temp above 43c in 7 Days is "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 1526
    invoke-direct {p0, v3}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->getTotalTime(I)J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "(ms), soc above 99 in 7 Days is "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 1527
    invoke-direct {p0, v5}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->getTotalTime(I)J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, "(ms) , R value in 7 days is "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-direct {p0, v7}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->getRValue(I)I

    move-result v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1525
    invoke-static {v2, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1528
    :cond_6
    if-nez v0, :cond_8

    if-nez v4, :cond_8

    if-nez v6, :cond_8

    if-eqz v8, :cond_7

    goto :goto_5

    .line 1532
    :cond_7
    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$sfputmIsSatisfyTempSocCondition(Z)V

    .line 1533
    iget-object v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->allDaysData:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_6

    .line 1529
    :cond_8
    :goto_5
    invoke-static {v3}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$sfputmIsSatisfyTempSocCondition(Z)V

    .line 1530
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->allDaysData:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 1535
    :goto_6
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    move-result-object v1

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->-$$Nest$msendAdjustVolBroadcast(Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;)V

    .line 1537
    .end local v0    # "isBattTempContinueAbove38C":Z
    .end local v4    # "isBattTempContinueAbove43C":Z
    .end local v6    # "isSocContinueAbove99":Z
    .end local v8    # "isSatisfyRCondition":Z
    :cond_9
    invoke-virtual {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->clearData()V

    .line 1538
    return-void
.end method

.method public setDataToList()V
    .locals 3

    .line 1554
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1555
    .local v0, "dataList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-wide v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeIn38C:J

    invoke-direct {p0, v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->parseString(J)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1556
    iget-wide v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeIn43C:J

    invoke-direct {p0, v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->parseString(J)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1557
    iget-wide v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeInHighSoc:J

    invoke-direct {p0, v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->parseString(J)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1558
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->RValue:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1559
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->allDaysData:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1560
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetDEBUG(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "allDaysData="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->allDaysData:Ljava/util/List;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiBatteryStatsService"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1561
    :cond_0
    return-void
.end method

.method public stopCollectTempSocData()V
    .locals 8

    .line 1540
    iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeIn38C:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 1541
    iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeIn38C:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeIn38C:J

    sub-long/2addr v4, v6

    add-long/2addr v0, v4

    iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeIn38C:J

    .line 1542
    iput-wide v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeIn38C:J

    .line 1544
    :cond_0
    iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeIn43C:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 1545
    iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeIn43C:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeIn43C:J

    sub-long/2addr v4, v6

    add-long/2addr v0, v4

    iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeIn43C:J

    .line 1546
    iput-wide v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeIn43C:J

    .line 1548
    :cond_1
    iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeInHighSoc:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    .line 1549
    iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeInHighSoc:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeInHighSoc:J

    sub-long/2addr v4, v6

    add-long/2addr v0, v4

    iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeInHighSoc:J

    .line 1550
    iput-wide v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeInHighSoc:J

    .line 1552
    :cond_2
    return-void
.end method
