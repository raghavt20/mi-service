public class com.android.server.SystemServerImpl extends com.android.server.SystemServerStub {
	 /* .source "SystemServerImpl.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.MiuiStubImplManifest$$" */
} // .end annotation
/* # static fields */
private static final java.lang.String AML_MIUI_WIFI_SERVICE;
private static final java.lang.String AML_MIUI_WIFI_SERVICE_CLASS;
private static final java.lang.String AML_SLAVE_WIFI_SERVICE;
private static final java.lang.String AML_SLAVE_WIFI_SERVICE_CLASS;
private static final Boolean DEBUG;
private static final java.io.File DEFAULT_HEAP_DUMP_FILE;
private static final Integer MAX_HEAP_DUMPS;
private static final java.lang.String MIUI_HEAP_DUMP_PATH;
private static final java.lang.String MIUI_SLAVE_WIFI_SERVICE;
private static final java.lang.String MIUI_WIFI_SERVICE;
private static final java.lang.String TAG;
private static final java.lang.String WIFI_APEX_SERVICE_JAR_PATH;
/* # direct methods */
static com.android.server.SystemServerImpl ( ) {
	 /* .locals 4 */
	 /* .line 82 */
	 final String v0 = "SystemServerI"; // const-string v0, "SystemServerI"
	 /* new-instance v1, Ljava/io/File; */
	 final String v2 = "/data/system/heapdump/"; // const-string v2, "/data/system/heapdump/"
	 /* invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
	 /* .line 118 */
	 /* const-class v1, Lcom/android/server/SystemServerImpl; */
	 (( java.lang.Class ) v1 ).getClassLoader ( ); // invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;
	 /* check-cast v1, Ldalvik/system/PathClassLoader; */
	 /* .line 119 */
	 /* .local v1, "pathClassLoader":Ldalvik/system/PathClassLoader; */
	 final String v2 = "/system_ext/framework/miuix.jar"; // const-string v2, "/system_ext/framework/miuix.jar"
	 (( dalvik.system.PathClassLoader ) v1 ).addDexPath ( v2 ); // invoke-virtual {v1, v2}, Ldalvik/system/PathClassLoader;->addDexPath(Ljava/lang/String;)V
	 /* .line 121 */
	 final String v2 = "/system_ext/framework/miui-cameraopt.jar"; // const-string v2, "/system_ext/framework/miui-cameraopt.jar"
	 (( dalvik.system.PathClassLoader ) v1 ).addDexPath ( v2 ); // invoke-virtual {v1, v2}, Ldalvik/system/PathClassLoader;->addDexPath(Ljava/lang/String;)V
	 /* .line 125 */
	 try { // :try_start_0
		 final String v2 = "Load libmiui_service"; // const-string v2, "Load libmiui_service"
		 android.util.Log .i ( v0,v2 );
		 /* .line 126 */
		 final String v2 = "miui_service"; // const-string v2, "miui_service"
		 java.lang.System .loadLibrary ( v2 );
		 /* :try_end_0 */
		 /* .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 129 */
		 /* .line 127 */
		 /* :catch_0 */
		 /* move-exception v2 */
		 /* .line 128 */
		 /* .local v2, "e":Ljava/lang/UnsatisfiedLinkError; */
		 final String v3 = "can\'t loadLibrary libmiui_service"; // const-string v3, "can\'t loadLibrary libmiui_service"
		 android.util.Log .w ( v0,v3,v2 );
		 /* .line 130 */
	 } // .end local v1 # "pathClassLoader":Ldalvik/system/PathClassLoader;
} // .end local v2 # "e":Ljava/lang/UnsatisfiedLinkError;
} // :goto_0
return;
} // .end method
public com.android.server.SystemServerImpl ( ) {
/* .locals 2 */
/* .line 103 */
/* invoke-direct {p0}, Lcom/android/server/SystemServerStub;-><init>()V */
/* .line 104 */
com.android.server.pm.PackageManagerServiceStub .get ( );
/* .line 106 */
final String v0 = "ro.crypto.type"; // const-string v0, "ro.crypto.type"
android.os.SystemProperties .get ( v0 );
final String v1 = "file"; // const-string v1, "file"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_0 */
/* const-string/jumbo v0, "vold.decrypt" */
android.os.SystemProperties .get ( v0 );
/* const-string/jumbo v1, "trigger_restart_framework" */
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 111 */
} // :cond_0
com.android.server.SystemServerImpl .enforceVersionPolicy ( );
/* .line 114 */
} // :cond_1
return;
} // .end method
private static void enforceVersionPolicy ( ) {
/* .locals 9 */
/* .line 353 */
final String v0 = "ro.product.name"; // const-string v0, "ro.product.name"
android.os.SystemProperties .get ( v0 );
/* .line 354 */
/* .local v0, "product":Ljava/lang/String; */
final String v1 = "ro.product.mod_device"; // const-string v1, "ro.product.mod_device"
final String v2 = ""; // const-string v2, ""
android.os.SystemProperties .get ( v1,v2 );
final String v2 = "_in"; // const-string v2, "_in"
v1 = (( java.lang.String ) v1 ).contains ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* .line 355 */
/* .local v1, "isIndiaBuild":Z */
final String v2 = "chenfeng"; // const-string v2, "chenfeng"
final String v3 = "chenfeng_"; // const-string v3, "chenfeng_"
/* filled-new-array {v2, v3}, [Ljava/lang/String; */
/* .line 359 */
/* .local v2, "DPC_DLC_WHITE_LIST":[Ljava/lang/String; */
final String v3 = "ro.secureboot.lockstate"; // const-string v3, "ro.secureboot.lockstate"
android.os.SystemProperties .get ( v3 );
final String v4 = "locked"; // const-string v4, "locked"
v3 = (( java.lang.String ) v4 ).equals ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
final String v4 = "SystemServerI"; // const-string v4, "SystemServerI"
/* if-nez v3, :cond_0 */
/* .line 360 */
final String v3 = "enforceVersionPolicy: device unlocked"; // const-string v3, "enforceVersionPolicy: device unlocked"
android.util.Slog .d ( v4,v3 );
/* .line 361 */
return;
/* .line 364 */
} // :cond_0
/* array-length v3, v2 */
int v5 = 0; // const/4 v5, 0x0
} // :goto_0
/* if-ge v5, v3, :cond_5 */
/* aget-object v6, v2, v5 */
/* .line 365 */
/* .local v6, "p":Ljava/lang/String; */
v7 = (( java.lang.String ) v6 ).equals ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v7, :cond_1 */
if ( v0 != null) { // if-eqz v0, :cond_4
final String v7 = "_"; // const-string v7, "_"
v7 = (( java.lang.String ) v6 ).endsWith ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z
if ( v7 != null) { // if-eqz v7, :cond_4
v7 = (( java.lang.String ) v0 ).startsWith ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v7 != null) { // if-eqz v7, :cond_4
/* .line 366 */
} // :cond_1
final String v7 = "ro.boot.hwc"; // const-string v7, "ro.boot.hwc"
android.os.SystemProperties .get ( v7 );
/* .line 367 */
/* .local v7, "country":Ljava/lang/String; */
if ( v7 != null) { // if-eqz v7, :cond_4
final String v8 = "India"; // const-string v8, "India"
v8 = (( java.lang.String ) v7 ).startsWith ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
/* if-nez v8, :cond_2 */
final String v8 = "IN"; // const-string v8, "IN"
v8 = (( java.lang.String ) v7 ).startsWith ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v8 != null) { // if-eqz v8, :cond_4
/* .line 368 */
} // :cond_2
/* if-nez v1, :cond_3 */
/* .line 369 */
final String v8 = "DPC/DLC devices IN hardware can\'t run Global build; reboot into recovery!!!"; // const-string v8, "DPC/DLC devices IN hardware can\'t run Global build; reboot into recovery!!!"
android.util.Slog .e ( v4,v8 );
/* .line 370 */
com.android.server.SystemServerImpl .rebootIntoRecovery ( );
/* .line 372 */
} // :cond_3
final String v3 = "enforceVersionPolicy:no in device"; // const-string v3, "enforceVersionPolicy:no in device"
android.util.Slog .d ( v4,v3 );
/* .line 373 */
return;
/* .line 364 */
} // .end local v6 # "p":Ljava/lang/String;
} // .end local v7 # "country":Ljava/lang/String;
} // :cond_4
} // :goto_1
/* add-int/lit8 v5, v5, 0x1 */
/* .line 379 */
} // :cond_5
v3 = com.android.server.SystemServerImpl .isGlobalHaredware ( );
if ( v3 != null) { // if-eqz v3, :cond_6
/* .line 380 */
final String v3 = "enforceVersionPolicy: global device"; // const-string v3, "enforceVersionPolicy: global device"
android.util.Slog .d ( v4,v3 );
/* .line 381 */
return;
/* .line 384 */
} // :cond_6
/* sget-boolean v3, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
if ( v3 != null) { // if-eqz v3, :cond_7
/* .line 385 */
final String v3 = "CN hardware can\'t run Global build; reboot into recovery!!!"; // const-string v3, "CN hardware can\'t run Global build; reboot into recovery!!!"
android.util.Slog .e ( v4,v3 );
/* .line 386 */
com.android.server.SystemServerImpl .rebootIntoRecovery ( );
/* .line 388 */
} // :cond_7
return;
} // .end method
private static Boolean isGlobalHaredware ( ) {
/* .locals 2 */
/* .line 345 */
final String v0 = "ro.boot.hwc"; // const-string v0, "ro.boot.hwc"
android.os.SystemProperties .get ( v0 );
/* .line 346 */
/* .local v0, "country":Ljava/lang/String; */
final String v1 = "CN"; // const-string v1, "CN"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_1 */
if ( v0 != null) { // if-eqz v0, :cond_0
final String v1 = "CN_"; // const-string v1, "CN_"
v1 = (( java.lang.String ) v0 ).startsWith ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 349 */
} // :cond_0
int v1 = 1; // const/4 v1, 0x1
/* .line 347 */
} // :cond_1
} // :goto_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
private static void rebootIntoRecovery ( ) {
/* .locals 2 */
/* .line 338 */
final String v0 = "--show_version_mismatch\n"; // const-string v0, "--show_version_mismatch\n"
com.android.server.BcbUtil .setupBcb ( v0 );
/* .line 339 */
/* const-string/jumbo v0, "sys.powerctl" */
final String v1 = "reboot,recovery"; // const-string v1, "reboot,recovery"
android.os.SystemProperties .set ( v0,v1 );
/* .line 340 */
return;
} // .end method
private static void startService ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p0, "className" # Ljava/lang/String; */
/* .line 156 */
/* const-class v0, Lcom/android/server/SystemServiceManager; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/SystemServiceManager; */
/* .line 158 */
/* .local v0, "manager":Lcom/android/server/SystemServiceManager; */
try { // :try_start_0
(( com.android.server.SystemServiceManager ) v0 ).startService ( p0 ); // invoke-virtual {v0, p0}, Lcom/android/server/SystemServiceManager;->startService(Ljava/lang/String;)Lcom/android/server/SystemService;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 161 */
/* .line 159 */
/* :catch_0 */
/* move-exception v1 */
/* .line 160 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Failed to start "; // const-string v3, "Failed to start "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p0 ); // invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "SystemServerI"; // const-string v3, "SystemServerI"
android.util.Slog .w ( v3,v2,v1 );
/* .line 162 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
/* # virtual methods */
void addCameraCoveredManagerService ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 468 */
final String v0 = "SystemServerI"; // const-string v0, "SystemServerI"
try { // :try_start_0
final String v1 = "add CameraCoveredManagerService"; // const-string v1, "add CameraCoveredManagerService"
android.util.Slog .d ( v0,v1 );
/* .line 469 */
final String v1 = "com.android.server.cameracovered.MiuiCameraCoveredManagerService$Lifecycle"; // const-string v1, "com.android.server.cameracovered.MiuiCameraCoveredManagerService$Lifecycle"
com.android.server.SystemServerImpl .startService ( v1 );
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 472 */
/* .line 470 */
/* :catchall_0 */
/* move-exception v1 */
/* .line 471 */
/* .local v1, "e":Ljava/lang/Throwable; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "add CameraCoveredManagerService fail "; // const-string v3, "add CameraCoveredManagerService fail "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v2 );
/* .line 474 */
} // .end local v1 # "e":Ljava/lang/Throwable;
} // :goto_0
return;
} // .end method
final void addExtraServices ( android.content.Context p0, Boolean p1 ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "onlyCore" # Z */
/* .line 171 */
/* sget-boolean v0, Lmiui/hardware/shoulderkey/ShoulderKeyManager;->SUPPORT_SHOULDERKEY:Z */
/* if-nez v0, :cond_0 */
/* sget-boolean v0, Lmiui/hardware/shoulderkey/ShoulderKeyManager;->SUPPORT_MIGAMEMACRO:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 172 */
} // :cond_0
final String v0 = "com.android.server.input.shoulderkey.ShoulderKeyManagerService$Lifecycle"; // const-string v0, "com.android.server.input.shoulderkey.ShoulderKeyManagerService$Lifecycle"
com.android.server.SystemServerImpl .startService ( v0 );
/* .line 176 */
} // :cond_1
com.android.server.wm.AppContinuityRouterStub .get ( );
(( com.android.server.wm.AppContinuityRouterStub ) v0 ).initContinuityManagerService ( ); // invoke-virtual {v0}, Lcom/android/server/wm/AppContinuityRouterStub;->initContinuityManagerService()V
/* .line 179 */
final String v0 = "com.miui.server.SecurityManagerService$Lifecycle"; // const-string v0, "com.miui.server.SecurityManagerService$Lifecycle"
com.android.server.SystemServerImpl .startService ( v0 );
/* .line 181 */
final String v0 = "com.miui.server.MiuiWebViewManagerService$Lifecycle"; // const-string v0, "com.miui.server.MiuiWebViewManagerService$Lifecycle"
com.android.server.SystemServerImpl .startService ( v0 );
/* .line 183 */
final String v0 = "com.miui.server.MiuiInitServer$Lifecycle"; // const-string v0, "com.miui.server.MiuiInitServer$Lifecycle"
com.android.server.SystemServerImpl .startService ( v0 );
/* .line 185 */
final String v0 = "com.miui.server.BackupManagerService$Lifecycle"; // const-string v0, "com.miui.server.BackupManagerService$Lifecycle"
com.android.server.SystemServerImpl .startService ( v0 );
/* .line 187 */
final String v0 = "com.miui.server.stepcounter.StepCounterManagerService"; // const-string v0, "com.miui.server.stepcounter.StepCounterManagerService"
com.android.server.SystemServerImpl .startService ( v0 );
/* .line 189 */
final String v0 = "com.android.server.location.LocationPolicyManagerService$Lifecycle"; // const-string v0, "com.android.server.location.LocationPolicyManagerService$Lifecycle"
com.android.server.SystemServerImpl .startService ( v0 );
/* .line 191 */
final String v0 = "com.miui.server.PerfShielderService$Lifecycle"; // const-string v0, "com.miui.server.PerfShielderService$Lifecycle"
com.android.server.SystemServerImpl .startService ( v0 );
/* .line 193 */
final String v0 = "com.miui.server.greeze.GreezeManagerService$Lifecycle"; // const-string v0, "com.miui.server.greeze.GreezeManagerService$Lifecycle"
com.android.server.SystemServerImpl .startService ( v0 );
/* .line 195 */
final String v0 = "config_tof_proximity_available"; // const-string v0, "config_tof_proximity_available"
int v1 = 0; // const/4 v1, 0x0
v0 = miui.util.FeatureParser .getBoolean ( v0,v1 );
/* if-nez v0, :cond_2 */
/* .line 196 */
final String v0 = "config_tof_gesture_available"; // const-string v0, "config_tof_gesture_available"
v0 = miui.util.FeatureParser .getBoolean ( v0,v1 );
/* if-nez v0, :cond_2 */
/* .line 197 */
final String v0 = "config_aon_gesture_available"; // const-string v0, "config_aon_gesture_available"
v0 = miui.util.FeatureParser .getBoolean ( v0,v1 );
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 198 */
} // :cond_2
final String v0 = "com.android.server.tof.ContactlessGestureService"; // const-string v0, "com.android.server.tof.ContactlessGestureService"
com.android.server.SystemServerImpl .startService ( v0 );
/* .line 202 */
} // :cond_3
com.miui.server.car.MiuiCarServiceStub .get ( );
(( com.miui.server.car.MiuiCarServiceStub ) v0 ).publishCarService ( ); // invoke-virtual {v0}, Lcom/miui/server/car/MiuiCarServiceStub;->publishCarService()V
/* .line 204 */
final String v0 = "com.miui.server.turbosched.TurboSchedManagerService$Lifecycle"; // const-string v0, "com.miui.server.turbosched.TurboSchedManagerService$Lifecycle"
com.android.server.SystemServerImpl .startService ( v0 );
/* .line 206 */
final String v0 = "com.android.server.am.ProcessManagerService$Lifecycle"; // const-string v0, "com.android.server.am.ProcessManagerService$Lifecycle"
com.android.server.SystemServerImpl .startService ( v0 );
/* .line 208 */
final String v0 = "com.miui.server.rtboost.SchedBoostService$Lifecycle"; // const-string v0, "com.miui.server.rtboost.SchedBoostService$Lifecycle"
com.android.server.SystemServerImpl .startService ( v0 );
/* .line 210 */
final String v0 = "com.miui.server.MiuiCldService$Lifecycle"; // const-string v0, "com.miui.server.MiuiCldService$Lifecycle"
com.android.server.SystemServerImpl .startService ( v0 );
/* .line 212 */
final String v0 = "com.miui.server.MiuiFboService$Lifecycle"; // const-string v0, "com.miui.server.MiuiFboService$Lifecycle"
com.android.server.SystemServerImpl .startService ( v0 );
/* .line 214 */
final String v0 = "com.miui.server.stability.StabilityLocalService$Lifecycle"; // const-string v0, "com.miui.server.stability.StabilityLocalService$Lifecycle"
com.android.server.SystemServerImpl .startService ( v0 );
/* .line 216 */
final String v0 = "persist.sys.stability.swapEnable"; // const-string v0, "persist.sys.stability.swapEnable"
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 217 */
final String v0 = "com.miui.server.MiuiSwapService$Lifecycle"; // const-string v0, "com.miui.server.MiuiSwapService$Lifecycle"
com.android.server.SystemServerImpl .startService ( v0 );
/* .line 220 */
} // :cond_4
final String v0 = "com.android.server.am.MiuiMemoryService$Lifecycle"; // const-string v0, "com.android.server.am.MiuiMemoryService$Lifecycle"
com.android.server.SystemServerImpl .startService ( v0 );
/* .line 222 */
final String v0 = "com.miui.server.MiuiDfcService$Lifecycle"; // const-string v0, "com.miui.server.MiuiDfcService$Lifecycle"
com.android.server.SystemServerImpl .startService ( v0 );
/* .line 224 */
final String v0 = "com.miui.server.sentinel.MiuiSentinelService$Lifecycle"; // const-string v0, "com.miui.server.sentinel.MiuiSentinelService$Lifecycle"
com.android.server.SystemServerImpl .startService ( v0 );
/* .line 227 */
/* new-instance v0, Lcom/miui/whetstone/server/WhetstoneActivityManagerService; */
/* invoke-direct {v0, p1}, Lcom/miui/whetstone/server/WhetstoneActivityManagerService;-><init>(Landroid/content/Context;)V */
/* const-string/jumbo v2, "whetstone.activity" */
android.os.ServiceManager .addService ( v2,v0 );
/* .line 230 */
android.util.MiuiAppSizeCompatModeImpl .getInstance ( );
v0 = (( android.util.MiuiAppSizeCompatModeImpl ) v0 ).isEnabled ( ); // invoke-virtual {v0}, Landroid/util/MiuiAppSizeCompatModeImpl;->isEnabled()Z
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 231 */
final String v0 = "com.android.server.wm.MiuiSizeCompatService$Lifecycle"; // const-string v0, "com.android.server.wm.MiuiSizeCompatService$Lifecycle"
com.android.server.SystemServerImpl .startService ( v0 );
/* .line 235 */
} // :cond_5
/* sget-boolean v0, Lcom/miui/autoui/MiuiAutoUIManager;->IS_AUTO_UI_ENABLED:Z */
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 236 */
final String v0 = "com.android.server.autoui.MiuiAutoUIManagerService"; // const-string v0, "com.android.server.autoui.MiuiAutoUIManagerService"
com.android.server.SystemServerImpl .startService ( v0 );
/* .line 240 */
} // :cond_6
final String v0 = "com.android.server.input.MiuiInputManagerService$Lifecycle"; // const-string v0, "com.android.server.input.MiuiInputManagerService$Lifecycle"
com.android.server.SystemServerImpl .startService ( v0 );
/* .line 243 */
/* sget-boolean v0, Landroid/hovermode/MiuiHoverModeManager;->IS_HOVERMODE_ENABLED:Z */
if ( v0 != null) { // if-eqz v0, :cond_7
/* .line 244 */
final String v0 = "SystemServerI"; // const-string v0, "SystemServerI"
/* const-string/jumbo v2, "start MiuiHoverModeService" */
android.util.Slog .i ( v0,v2 );
/* .line 245 */
final String v0 = "com.android.server.wm.MiuiHoverModeService$Lifecycle"; // const-string v0, "com.android.server.wm.MiuiHoverModeService$Lifecycle"
com.android.server.SystemServerImpl .startService ( v0 );
/* .line 249 */
} // :cond_7
if ( v0 != null) { // if-eqz v0, :cond_8
/* .line 250 */
final String v0 = "com.android.server.display.DisplayFeatureManagerService"; // const-string v0, "com.android.server.display.DisplayFeatureManagerService"
com.android.server.SystemServerImpl .startService ( v0 );
/* .line 254 */
} // :cond_8
final String v0 = "ro.vendor.display.uiservice.enable"; // const-string v0, "ro.vendor.display.uiservice.enable"
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
if ( v0 != null) { // if-eqz v0, :cond_9
/* .line 255 */
final String v0 = "com.android.server.ui.UIService$Lifecycle"; // const-string v0, "com.android.server.ui.UIService$Lifecycle"
com.android.server.SystemServerImpl .startService ( v0 );
/* .line 259 */
} // :cond_9
com.android.server.MiuiFgThread .initialMiuiFgThread ( );
/* .line 262 */
/* sget-boolean v0, Lcom/miui/enterprise/settings/EnterpriseSettings;->ENTERPRISE_ACTIVATED:Z */
if ( v0 != null) { // if-eqz v0, :cond_a
/* .line 263 */
final String v0 = "com.miui.server.enterprise.EnterpriseManagerService$Lifecycle"; // const-string v0, "com.miui.server.enterprise.EnterpriseManagerService$Lifecycle"
com.android.server.SystemServerImpl .startService ( v0 );
/* .line 265 */
} // :cond_a
/* sget-boolean v0, Lmiui/enterprise/EnterpriseManagerStub;->ENTERPRISE_ACTIVATED:Z */
if ( v0 != null) { // if-eqz v0, :cond_b
/* .line 266 */
miui.enterprise.EnterpriseManagerStub .getInstance ( );
/* .line 267 */
(( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
(( java.lang.Class ) v1 ).getClassLoader ( ); // invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;
/* .line 266 */
/* .line 271 */
} // :cond_b
final String v0 = "com.android.server.am.SmartPowerService$Lifecycle"; // const-string v0, "com.android.server.am.SmartPowerService$Lifecycle"
com.android.server.SystemServerImpl .startService ( v0 );
/* .line 274 */
final String v0 = "com.miui.server.migard.MiGardService$Lifecycle"; // const-string v0, "com.miui.server.migard.MiGardService$Lifecycle"
com.android.server.SystemServerImpl .startService ( v0 );
/* .line 277 */
final String v0 = "com.miui.server.blackmask.MiBlackMaskService$Lifecycle"; // const-string v0, "com.miui.server.blackmask.MiBlackMaskService$Lifecycle"
com.android.server.SystemServerImpl .startService ( v0 );
/* .line 279 */
final String v0 = "com.xiaomi.mirror.service.MirrorService$Lifecycle"; // const-string v0, "com.xiaomi.mirror.service.MirrorService$Lifecycle"
com.android.server.SystemServerImpl .startService ( v0 );
/* .line 282 */
v0 = android.magicpointer.MiuiMagicPointerStub .isSupportMagicPointer ( );
if ( v0 != null) { // if-eqz v0, :cond_c
/* .line 284 */
final String v0 = "com.android.server.input.MiuiMagicPointerService$Lifecycle"; // const-string v0, "com.android.server.input.MiuiMagicPointerService$Lifecycle"
com.android.server.SystemServerImpl .startService ( v0 );
/* .line 288 */
} // :cond_c
com.android.server.MiuiCommonCloudServiceStub .getInstance ( );
(( com.android.server.MiuiCommonCloudServiceStub ) v0 ).init ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/MiuiCommonCloudServiceStub;->init(Landroid/content/Context;)V
/* .line 290 */
final String v0 = "com.miui.server.multisence.MultiSenceService"; // const-string v0, "com.miui.server.multisence.MultiSenceService"
com.android.server.SystemServerImpl .startService ( v0 );
/* .line 292 */
final String v0 = "ro.miui.build.region"; // const-string v0, "ro.miui.build.region"
final String v1 = ""; // const-string v1, ""
android.os.SystemProperties .get ( v0,v1 );
final String v1 = "cn"; // const-string v1, "cn"
v0 = android.text.TextUtils .equals ( v0,v1 );
if ( v0 != null) { // if-eqz v0, :cond_d
/* .line 293 */
final String v0 = "com.miui.server.rescue.BrokenScreenRescueService"; // const-string v0, "com.miui.server.rescue.BrokenScreenRescueService"
com.android.server.SystemServerImpl .startService ( v0 );
/* .line 296 */
} // :cond_d
android.appcompat.ApplicationCompatUtilsStub .get ( );
v0 = (( android.appcompat.ApplicationCompatUtilsStub ) v0 ).isAppCompatEnabled ( ); // invoke-virtual {v0}, Landroid/appcompat/ApplicationCompatUtilsStub;->isAppCompatEnabled()Z
if ( v0 != null) { // if-eqz v0, :cond_e
/* .line 298 */
com.android.server.wm.ApplicationCompatRouterStub .get ( );
/* .line 300 */
android.appcompat.ApplicationCompatUtilsStub .get ( );
v0 = (( android.appcompat.ApplicationCompatUtilsStub ) v0 ).isContinuityEnabled ( ); // invoke-virtual {v0}, Landroid/appcompat/ApplicationCompatUtilsStub;->isContinuityEnabled()Z
if ( v0 != null) { // if-eqz v0, :cond_e
/* .line 301 */
com.android.server.wm.AppContinuityRouterStub .get ( );
/* .line 305 */
} // :cond_e
final String v0 = "com.miui.server.MiuiFreeDragService$Lifecycle"; // const-string v0, "com.miui.server.MiuiFreeDragService$Lifecycle"
com.android.server.SystemServerImpl .startService ( v0 );
/* .line 307 */
final String v0 = "com.android.server.powerconsumpiton.PowerConsumptionService"; // const-string v0, "com.android.server.powerconsumpiton.PowerConsumptionService"
com.android.server.SystemServerImpl .startService ( v0 );
/* .line 309 */
final String v0 = "com.android.server.aiinput.AIInputTextManagerService$Lifecycle"; // const-string v0, "com.android.server.aiinput.AIInputTextManagerService$Lifecycle"
com.android.server.SystemServerImpl .startService ( v0 );
/* .line 312 */
final String v0 = "com.miui.cameraopt.CameraOptManagerService$Lifecycle"; // const-string v0, "com.miui.cameraopt.CameraOptManagerService$Lifecycle"
com.android.server.SystemServerImpl .startService ( v0 );
/* .line 315 */
/* new-instance v0, Lcom/xiaomi/interconnection/InterconnectionService; */
/* invoke-direct {v0, p1}, Lcom/xiaomi/interconnection/InterconnectionService;-><init>(Landroid/content/Context;)V */
/* const-string/jumbo v1, "xiaomi.InterconnectionService" */
android.os.ServiceManager .addService ( v1,v0 );
/* .line 319 */
final String v0 = "com.xiaomi.vkmode.service.MiuiForceVkService$Lifecycle"; // const-string v0, "com.xiaomi.vkmode.service.MiuiForceVkService$Lifecycle"
com.android.server.SystemServerImpl .startService ( v0 );
/* .line 322 */
com.android.server.am.MimdManagerServiceStub .get ( );
/* .line 324 */
return;
} // .end method
void addMiuiPeriodicCleanerService ( com.android.server.wm.ActivityTaskManagerService p0 ) {
/* .locals 4 */
/* .param p1, "atm" # Lcom/android/server/wm/ActivityTaskManagerService; */
/* .line 479 */
final String v0 = "SystemServerI"; // const-string v0, "SystemServerI"
final String v1 = "persist.sys.periodic.u.enable"; // const-string v1, "persist.sys.periodic.u.enable"
int v2 = 0; // const/4 v2, 0x0
v1 = android.os.SystemProperties .getBoolean ( v1,v2 );
/* if-nez v1, :cond_0 */
/* .line 480 */
return;
/* .line 483 */
} // :cond_0
try { // :try_start_0
/* const-string/jumbo v1, "start PeriodicCleanerService" */
android.util.Slog .d ( v0,v1 );
/* .line 484 */
final String v1 = "com.android.server.am.PeriodicCleanerService"; // const-string v1, "com.android.server.am.PeriodicCleanerService"
com.android.server.SystemServerImpl .startService ( v1 );
/* .line 485 */
/* const-class v1, Lcom/android/server/am/PeriodicCleanerInternalStub; */
com.android.server.LocalServices .getService ( v1 );
/* check-cast v1, Lcom/android/server/am/PeriodicCleanerInternalStub; */
(( com.android.server.wm.ActivityTaskManagerService ) p1 ).setPeriodicCleaner ( v1 ); // invoke-virtual {p1, v1}, Lcom/android/server/wm/ActivityTaskManagerService;->setPeriodicCleaner(Lcom/android/server/am/PeriodicCleanerInternalStub;)V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 488 */
/* .line 486 */
/* :catchall_0 */
/* move-exception v1 */
/* .line 487 */
/* .local v1, "e":Ljava/lang/Throwable; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "add MiuiPeriodicCleanerService fail "; // const-string v3, "add MiuiPeriodicCleanerService fail "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v2 );
/* .line 489 */
} // .end local v1 # "e":Ljava/lang/Throwable;
} // :goto_0
return;
} // .end method
void addMiuiRestoreManagerService ( android.content.Context p0, com.android.server.pm.Installer p1 ) {
/* .locals 11 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "installer" # Lcom/android/server/pm/Installer; */
/* .line 407 */
final String v0 = ": service must have a public constructor with a Context argument"; // const-string v0, ": service must have a public constructor with a Context argument"
final String v1 = "Failed to create service "; // const-string v1, "Failed to create service "
final String v2 = "com.android.server.MiuiRestoreManagerService$Lifecycle"; // const-string v2, "com.android.server.MiuiRestoreManagerService$Lifecycle"
/* .line 410 */
/* .local v2, "className":Ljava/lang/String; */
(( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
(( java.lang.Class ) v3 ).getClassLoader ( ); // invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;
/* .line 413 */
/* .local v3, "classLoader":Ljava/lang/ClassLoader; */
int v4 = 1; // const/4 v4, 0x1
try { // :try_start_0
java.lang.Class .forName ( v2,v4,v3 );
/* :try_end_0 */
/* .catch Ljava/lang/ClassNotFoundException; {:try_start_0 ..:try_end_0} :catch_5 */
/* .line 423 */
/* .local v5, "serviceClass":Ljava/lang/Class;, "Ljava/lang/Class<Lcom/android/server/SystemService;>;" */
/* nop */
/* .line 424 */
(( java.lang.Class ) v5 ).getName ( ); // invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;
/* .line 425 */
/* .local v6, "name":Ljava/lang/String; */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "Starting "; // const-string v8, "Starting "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v8 = "SystemServerI"; // const-string v8, "SystemServerI"
android.util.Slog .i ( v8,v7 );
/* .line 426 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "StartService "; // const-string v9, "StartService "
(( java.lang.StringBuilder ) v7 ).append ( v9 ); // invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* const-wide/32 v9, 0x80000 */
android.os.Trace .traceBegin ( v9,v10,v7 );
/* .line 429 */
/* const-class v7, Lcom/android/server/SystemService; */
v7 = (( java.lang.Class ) v7 ).isAssignableFrom ( v5 ); // invoke-virtual {v7, v5}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z
if ( v7 != null) { // if-eqz v7, :cond_0
/* .line 435 */
int v7 = 2; // const/4 v7, 0x2
try { // :try_start_1
/* new-array v7, v7, [Ljava/lang/Class; */
/* const-class v9, Landroid/content/Context; */
int v10 = 0; // const/4 v10, 0x0
/* aput-object v9, v7, v10 */
/* const-class v9, Lcom/android/server/pm/Installer; */
/* aput-object v9, v7, v4 */
(( java.lang.Class ) v5 ).getConstructor ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
/* .line 436 */
/* .local v4, "constructor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<Lcom/android/server/SystemService;>;" */
/* filled-new-array {p1, p2}, [Ljava/lang/Object; */
(( java.lang.reflect.Constructor ) v4 ).newInstance ( v7 ); // invoke-virtual {v4, v7}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v7, Lcom/android/server/SystemService; */
/* :try_end_1 */
/* .catch Ljava/lang/InstantiationException; {:try_start_1 ..:try_end_1} :catch_4 */
/* .catch Ljava/lang/IllegalAccessException; {:try_start_1 ..:try_end_1} :catch_3 */
/* .catch Ljava/lang/NoSuchMethodException; {:try_start_1 ..:try_end_1} :catch_2 */
/* .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 ..:try_end_1} :catch_1 */
/* move-object v0, v7 */
/* .line 449 */
} // .end local v4 # "constructor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<Lcom/android/server/SystemService;>;"
/* .local v0, "service":Lcom/android/server/SystemService; */
/* nop */
/* .line 450 */
/* const-class v1, Lcom/android/server/SystemServiceManager; */
com.android.server.LocalServices .getService ( v1 );
/* check-cast v1, Lcom/android/server/SystemServiceManager; */
/* .line 452 */
/* .local v1, "manager":Lcom/android/server/SystemServiceManager; */
try { // :try_start_2
(( com.android.server.SystemServiceManager ) v1 ).startService ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/SystemServiceManager;->startService(Lcom/android/server/SystemService;)V
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 455 */
/* .line 453 */
/* :catch_0 */
/* move-exception v4 */
/* .line 454 */
/* .local v4, "e":Ljava/lang/Exception; */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "Failed to start "; // const-string v9, "Failed to start "
(( java.lang.StringBuilder ) v7 ).append ( v9 ); // invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v0 ); // invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v8,v7,v4 );
/* .line 456 */
} // .end local v4 # "e":Ljava/lang/Exception;
} // :goto_0
return;
/* .line 446 */
} // .end local v0 # "service":Lcom/android/server/SystemService;
} // .end local v1 # "manager":Lcom/android/server/SystemServiceManager;
/* :catch_1 */
/* move-exception v0 */
/* .line 447 */
/* .local v0, "ex":Ljava/lang/reflect/InvocationTargetException; */
/* new-instance v4, Ljava/lang/RuntimeException; */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v1 ); // invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = ": service constructor threw an exception"; // const-string v7, ": service constructor threw an exception"
(( java.lang.StringBuilder ) v1 ).append ( v7 ); // invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v4, v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V */
/* throw v4 */
/* .line 443 */
} // .end local v0 # "ex":Ljava/lang/reflect/InvocationTargetException;
/* :catch_2 */
/* move-exception v4 */
/* .line 444 */
/* .local v4, "ex":Ljava/lang/NoSuchMethodException; */
/* new-instance v7, Ljava/lang/RuntimeException; */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v8 ).append ( v1 ); // invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v7, v0, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V */
/* throw v7 */
/* .line 440 */
} // .end local v4 # "ex":Ljava/lang/NoSuchMethodException;
/* :catch_3 */
/* move-exception v4 */
/* .line 441 */
/* .local v4, "ex":Ljava/lang/IllegalAccessException; */
/* new-instance v7, Ljava/lang/RuntimeException; */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v8 ).append ( v1 ); // invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v7, v0, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V */
/* throw v7 */
/* .line 437 */
} // .end local v4 # "ex":Ljava/lang/IllegalAccessException;
/* :catch_4 */
/* move-exception v0 */
/* .line 438 */
/* .local v0, "ex":Ljava/lang/InstantiationException; */
/* new-instance v4, Ljava/lang/RuntimeException; */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v1 ); // invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = ": service could not be instantiated"; // const-string v7, ": service could not be instantiated"
(( java.lang.StringBuilder ) v1 ).append ( v7 ); // invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v4, v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V */
/* throw v4 */
/* .line 430 */
} // .end local v0 # "ex":Ljava/lang/InstantiationException;
} // :cond_0
/* new-instance v0, Ljava/lang/RuntimeException; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Failed to create "; // const-string v4, "Failed to create "
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ": service must extend "; // const-string v4, ": service must extend "
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-class v4, Lcom/android/server/SystemService; */
/* .line 431 */
(( java.lang.Class ) v4 ).getName ( ); // invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
/* .line 415 */
} // .end local v5 # "serviceClass":Ljava/lang/Class;, "Ljava/lang/Class<Lcom/android/server/SystemService;>;"
} // .end local v6 # "name":Ljava/lang/String;
/* :catch_5 */
/* move-exception v0 */
/* .line 416 */
/* .local v0, "ex":Ljava/lang/ClassNotFoundException; */
/* new-instance v4, Ljava/lang/RuntimeException; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v1 ); // invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " from class loader "; // const-string v5, " from class loader "
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 417 */
(( java.lang.Object ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = ": service class not found, usually indicates that the caller should have called PackageManager.hasSystemFeature() to check whether the feature is available on this device before trying to start the services that implement it.Also ensure that the correct path for the classloader is supplied, if applicable."; // const-string v5, ": service class not found, usually indicates that the caller should have called PackageManager.hasSystemFeature() to check whether the feature is available on this device before trying to start the services that implement it.Also ensure that the correct path for the classloader is supplied, if applicable."
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v4, v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V */
/* throw v4 */
} // .end method
java.lang.Class createLightsServices ( ) {
/* .locals 4 */
/* .line 146 */
/* const-class v0, Lcom/android/server/lights/LightsService; */
/* .line 148 */
/* .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
try { // :try_start_0
final String v1 = "com.android.server.lights.MiuiLightsService"; // const-string v1, "com.android.server.lights.MiuiLightsService"
java.lang.Class .forName ( v1 );
/* :try_end_0 */
/* .catch Ljava/lang/ClassNotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object v0, v1 */
/* .line 151 */
/* .line 149 */
/* :catch_0 */
/* move-exception v1 */
/* .line 150 */
/* .local v1, "e":Ljava/lang/ClassNotFoundException; */
final String v2 = "SystemServerI"; // const-string v2, "SystemServerI"
final String v3 = "Failed to find MiuiLightsService"; // const-string v3, "Failed to find MiuiLightsService"
android.util.Log .e ( v2,v3,v1 );
/* .line 152 */
} // .end local v1 # "e":Ljava/lang/ClassNotFoundException;
} // :goto_0
} // .end method
com.android.server.policy.PhoneWindowManager createPhoneWindowManager ( ) {
/* .locals 2 */
/* .line 134 */
/* new-instance v0, Lcom/android/server/policy/PhoneWindowManager; */
/* invoke-direct {v0}, Lcom/android/server/policy/PhoneWindowManager;-><init>()V */
/* .line 136 */
/* .local v0, "phoneWindowManager":Lcom/android/server/policy/PhoneWindowManager; */
try { // :try_start_0
final String v1 = "com.android.server.policy.MiuiPhoneWindowManager"; // const-string v1, "com.android.server.policy.MiuiPhoneWindowManager"
/* .line 137 */
java.lang.Class .forName ( v1 );
(( java.lang.Class ) v1 ).newInstance ( ); // invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;
/* check-cast v1, Lcom/android/server/policy/PhoneWindowManager; */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object v0, v1 */
/* .line 140 */
/* .line 138 */
/* :catch_0 */
/* move-exception v1 */
/* .line 139 */
/* .local v1, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 141 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
} // .end method
public java.lang.String getConnectivitylibpath ( ) {
/* .locals 1 */
/* .line 460 */
final String v0 = ":/system_ext/framework/miui-connectivity-service.jar"; // const-string v0, ":/system_ext/framework/miui-connectivity-service.jar"
} // .end method
public java.io.File getHeapDumpDir ( ) {
/* .locals 4 */
/* .line 494 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/data/miuilog/stability/resleak/fdtrack"; // const-string v1, "/data/miuilog/stability/resleak/fdtrack"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 495 */
/* .local v0, "fdtrackDir":Ljava/io/File; */
v1 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
/* if-nez v1, :cond_0 */
v1 = (( java.io.File ) v0 ).mkdirs ( ); // invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z
/* if-nez v1, :cond_0 */
/* .line 496 */
v1 = com.android.server.SystemServerImpl.DEFAULT_HEAP_DUMP_FILE;
/* .line 498 */
} // :cond_0
v1 = android.os.Process .myUid ( );
v2 = android.os.Process .myUid ( );
/* const/16 v3, 0x1ed */
v1 = android.os.FileUtils .setPermissions ( v0,v3,v1,v2 );
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 499 */
v1 = com.android.server.SystemServerImpl.DEFAULT_HEAP_DUMP_FILE;
/* .line 501 */
} // :cond_1
} // .end method
public java.lang.String getMiuilibpath ( ) {
/* .locals 1 */
/* .line 402 */
final String v0 = ":/system_ext/framework/miui-wifi-service.jar"; // const-string v0, ":/system_ext/framework/miui-wifi-service.jar"
} // .end method
public void keepDumpSize ( java.util.TreeSet p0 ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/TreeSet<", */
/* "Ljava/io/File;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 505 */
/* .local p1, "fileTreeSet":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Ljava/io/File;>;" */
v0 = (( java.util.TreeSet ) p1 ).size ( ); // invoke-virtual {p1}, Ljava/util/TreeSet;->size()I
int v1 = 2; // const/4 v1, 0x2
/* if-lt v0, v1, :cond_2 */
/* .line 506 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
int v1 = 1; // const/4 v1, 0x1
/* if-ge v0, v1, :cond_0 */
/* .line 507 */
(( java.util.TreeSet ) p1 ).pollLast ( ); // invoke-virtual {p1}, Ljava/util/TreeSet;->pollLast()Ljava/lang/Object;
/* .line 506 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 509 */
} // .end local v0 # "i":I
} // :cond_0
(( java.util.TreeSet ) p1 ).iterator ( ); // invoke-virtual {p1}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;
v1 = } // :goto_1
if ( v1 != null) { // if-eqz v1, :cond_2
/* check-cast v1, Ljava/io/File; */
/* .line 510 */
/* .local v1, "file":Ljava/io/File; */
v2 = (( java.io.File ) v1 ).delete ( ); // invoke-virtual {v1}, Ljava/io/File;->delete()Z
/* if-nez v2, :cond_1 */
/* .line 511 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Failed to clean up fdtrack "; // const-string v3, "Failed to clean up fdtrack "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "System"; // const-string v3, "System"
android.util.Slog .w ( v3,v2 );
/* .line 513 */
} // .end local v1 # "file":Ljava/io/File;
} // :cond_1
/* .line 515 */
} // :cond_2
return;
} // .end method
void markBootDexopt ( Long p0, Long p1 ) {
/* .locals 3 */
/* .param p1, "startTime" # J */
/* .param p3, "endTime" # J */
/* .line 397 */
miui.mqsas.sdk.BootEventManager .getInstance ( );
/* sub-long v1, p3, p1 */
(( miui.mqsas.sdk.BootEventManager ) v0 ).setBootDexopt ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lmiui/mqsas/sdk/BootEventManager;->setBootDexopt(J)V
/* .line 398 */
return;
} // .end method
void markPmsScan ( Long p0, Long p1 ) {
/* .locals 1 */
/* .param p1, "startTime" # J */
/* .param p3, "endTime" # J */
/* .line 392 */
miui.mqsas.sdk.BootEventManager .getInstance ( );
(( miui.mqsas.sdk.BootEventManager ) v0 ).setPmsScanStart ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lmiui/mqsas/sdk/BootEventManager;->setPmsScanStart(J)V
/* .line 393 */
miui.mqsas.sdk.BootEventManager .getInstance ( );
(( miui.mqsas.sdk.BootEventManager ) v0 ).setPmsScanEnd ( p3, p4 ); // invoke-virtual {v0, p3, p4}, Lmiui/mqsas/sdk/BootEventManager;->setPmsScanEnd(J)V
/* .line 394 */
return;
} // .end method
void markSystemRun ( Long p0 ) {
/* .locals 5 */
/* .param p1, "time" # J */
/* .line 328 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 329 */
/* .local v0, "now":J */
miui.mqsas.sdk.BootEventManager .getInstance ( );
/* sub-long v3, v0, p1 */
(( miui.mqsas.sdk.BootEventManager ) v2 ).setZygotePreload ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Lmiui/mqsas/sdk/BootEventManager;->setZygotePreload(J)V
/* .line 330 */
miui.mqsas.sdk.BootEventManager .getInstance ( );
(( miui.mqsas.sdk.BootEventManager ) v2 ).setSystemRun ( p1, p2 ); // invoke-virtual {v2, p1, p2}, Lmiui/mqsas/sdk/BootEventManager;->setSystemRun(J)V
/* .line 331 */
v2 = com.android.server.wm.MiuiEmbeddingWindowServiceStubHead .isActivityEmbeddingEnable ( );
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 332 */
com.android.server.wm.MiuiEmbeddingWindowServiceStubHead .get ( );
/* .line 334 */
} // :cond_0
return;
} // .end method
public void startAmlMiuiWifiService ( com.android.server.utils.TimingsTraceAndSlog p0, android.content.Context p1 ) {
/* .locals 3 */
/* .param p1, "t" # Lcom/android/server/utils/TimingsTraceAndSlog; */
/* .param p2, "context" # Landroid/content/Context; */
/* .line 520 */
final String v0 = "MiuiWifiService"; // const-string v0, "MiuiWifiService"
(( android.content.Context ) p2 ).getSystemService ( v0 ); // invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* if-nez v0, :cond_0 */
/* .line 521 */
/* const-class v0, Lcom/android/server/SystemServiceManager; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/SystemServiceManager; */
/* .line 522 */
/* .local v0, "manager":Lcom/android/server/SystemServiceManager; */
final String v1 = "AmlMiuiWifiService"; // const-string v1, "AmlMiuiWifiService"
(( com.android.server.utils.TimingsTraceAndSlog ) p1 ).traceBegin ( v1 ); // invoke-virtual {p1, v1}, Lcom/android/server/utils/TimingsTraceAndSlog;->traceBegin(Ljava/lang/String;)V
/* .line 523 */
final String v1 = "com.android.server.wifi.AmlMiuiWifiService"; // const-string v1, "com.android.server.wifi.AmlMiuiWifiService"
final String v2 = "/apex/com.android.wifi/javalib/service-wifi.jar"; // const-string v2, "/apex/com.android.wifi/javalib/service-wifi.jar"
(( com.android.server.SystemServiceManager ) v0 ).startServiceFromJar ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/SystemServiceManager;->startServiceFromJar(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/SystemService;
/* .line 525 */
(( com.android.server.utils.TimingsTraceAndSlog ) p1 ).traceEnd ( ); // invoke-virtual {p1}, Lcom/android/server/utils/TimingsTraceAndSlog;->traceEnd()V
/* .line 527 */
} // .end local v0 # "manager":Lcom/android/server/SystemServiceManager;
} // :cond_0
return;
} // .end method
public void startAmlSlaveWifiService ( com.android.server.utils.TimingsTraceAndSlog p0, android.content.Context p1 ) {
/* .locals 3 */
/* .param p1, "t" # Lcom/android/server/utils/TimingsTraceAndSlog; */
/* .param p2, "context" # Landroid/content/Context; */
/* .line 530 */
final String v0 = "SlaveWifiService"; // const-string v0, "SlaveWifiService"
(( android.content.Context ) p2 ).getSystemService ( v0 ); // invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* if-nez v0, :cond_0 */
/* .line 531 */
/* const-class v0, Lcom/android/server/SystemServiceManager; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/SystemServiceManager; */
/* .line 532 */
/* .local v0, "manager":Lcom/android/server/SystemServiceManager; */
final String v1 = "AmlSlaveWifiService"; // const-string v1, "AmlSlaveWifiService"
(( com.android.server.utils.TimingsTraceAndSlog ) p1 ).traceBegin ( v1 ); // invoke-virtual {p1, v1}, Lcom/android/server/utils/TimingsTraceAndSlog;->traceBegin(Ljava/lang/String;)V
/* .line 533 */
final String v1 = "com.android.server.wifi.AmlSlaveWifiService"; // const-string v1, "com.android.server.wifi.AmlSlaveWifiService"
final String v2 = "/apex/com.android.wifi/javalib/service-wifi.jar"; // const-string v2, "/apex/com.android.wifi/javalib/service-wifi.jar"
(( com.android.server.SystemServiceManager ) v0 ).startServiceFromJar ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/SystemServiceManager;->startServiceFromJar(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/SystemService;
/* .line 535 */
(( com.android.server.utils.TimingsTraceAndSlog ) p1 ).traceEnd ( ); // invoke-virtual {p1}, Lcom/android/server/utils/TimingsTraceAndSlog;->traceEnd()V
/* .line 537 */
} // .end local v0 # "manager":Lcom/android/server/SystemServiceManager;
} // :cond_0
return;
} // .end method
