.class Lcom/android/server/DarkModeStatusTracker$1;
.super Landroid/database/ContentObserver;
.source "DarkModeStatusTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/DarkModeStatusTracker;->registerSettingsObserver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/DarkModeStatusTracker;


# direct methods
.method constructor <init>(Lcom/android/server/DarkModeStatusTracker;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/DarkModeStatusTracker;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 149
    iput-object p1, p0, Lcom/android/server/DarkModeStatusTracker$1;->this$0:Lcom/android/server/DarkModeStatusTracker;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 2
    .param p1, "selfChange"    # Z

    .line 152
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 154
    invoke-static {}, Lcom/android/server/DarkModeSuggestProvider;->getInstance()Lcom/android/server/DarkModeSuggestProvider;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/DarkModeStatusTracker$1;->this$0:Lcom/android/server/DarkModeStatusTracker;

    invoke-static {v1}, Lcom/android/server/DarkModeStatusTracker;->-$$Nest$fgetmContext(Lcom/android/server/DarkModeStatusTracker;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/DarkModeSuggestProvider;->updateCloudDataForDisableRegion(Landroid/content/Context;)Ljava/util/Set;

    move-result-object v0

    .line 153
    invoke-static {v0}, Lcom/android/server/DarkModeOneTrackHelper;->setDataDisableRegion(Ljava/util/Set;)V

    .line 155
    return-void
.end method
