.class Lcom/android/server/ScoutSystemMonitor$ScreenStateReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ScoutSystemMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/ScoutSystemMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ScreenStateReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/ScoutSystemMonitor;


# direct methods
.method constructor <init>(Lcom/android/server/ScoutSystemMonitor;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/ScoutSystemMonitor;

    .line 280
    iput-object p1, p0, Lcom/android/server/ScoutSystemMonitor$ScreenStateReceiver;->this$0:Lcom/android/server/ScoutSystemMonitor;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 283
    const/4 v0, 0x0

    .line 284
    .local v0, "screenOn":Z
    iget-object v1, p0, Lcom/android/server/ScoutSystemMonitor$ScreenStateReceiver;->this$0:Lcom/android/server/ScoutSystemMonitor;

    invoke-static {v1}, Lcom/android/server/ScoutSystemMonitor;->-$$Nest$fgetmiuiFboService(Lcom/android/server/ScoutSystemMonitor;)Lcom/miui/app/MiuiFboServiceInternal;

    move-result-object v1

    if-nez v1, :cond_0

    .line 285
    iget-object v1, p0, Lcom/android/server/ScoutSystemMonitor$ScreenStateReceiver;->this$0:Lcom/android/server/ScoutSystemMonitor;

    const-class v2, Lcom/miui/app/MiuiFboServiceInternal;

    invoke-static {v2}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/app/MiuiFboServiceInternal;

    invoke-static {v1, v2}, Lcom/android/server/ScoutSystemMonitor;->-$$Nest$fputmiuiFboService(Lcom/android/server/ScoutSystemMonitor;Lcom/miui/app/MiuiFboServiceInternal;)V

    .line 288
    :cond_0
    :try_start_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_1
    goto :goto_0

    :sswitch_0
    const-string v2, "android.intent.action.SCREEN_ON"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    goto :goto_1

    :sswitch_1
    const-string v2, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :goto_0
    const/4 v1, -0x1

    :goto_1
    packed-switch v1, :pswitch_data_0

    .line 298
    const/4 v0, 0x0

    goto :goto_2

    .line 290
    :pswitch_0
    const/4 v0, 0x1

    .line 291
    iget-object v1, p0, Lcom/android/server/ScoutSystemMonitor$ScreenStateReceiver;->this$0:Lcom/android/server/ScoutSystemMonitor;

    invoke-static {v1}, Lcom/android/server/ScoutSystemMonitor;->-$$Nest$fgetmiuiFboService(Lcom/android/server/ScoutSystemMonitor;)Lcom/miui/app/MiuiFboServiceInternal;

    move-result-object v1

    invoke-interface {v1}, Lcom/miui/app/MiuiFboServiceInternal;->getGlobalSwitch()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/server/ScoutSystemMonitor$ScreenStateReceiver;->this$0:Lcom/android/server/ScoutSystemMonitor;

    invoke-static {v1}, Lcom/android/server/ScoutSystemMonitor;->-$$Nest$fgetmiuiFboService(Lcom/android/server/ScoutSystemMonitor;)Lcom/miui/app/MiuiFboServiceInternal;

    move-result-object v1

    invoke-interface {v1}, Lcom/miui/app/MiuiFboServiceInternal;->getNativeIsRunning()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 292
    iget-object v1, p0, Lcom/android/server/ScoutSystemMonitor$ScreenStateReceiver;->this$0:Lcom/android/server/ScoutSystemMonitor;

    invoke-static {v1}, Lcom/android/server/ScoutSystemMonitor;->-$$Nest$fgetmiuiFboService(Lcom/android/server/ScoutSystemMonitor;)Lcom/miui/app/MiuiFboServiceInternal;

    move-result-object v1

    const-string/jumbo v2, "stopDueToScreen"

    const/4 v3, 0x5

    const-wide/16 v4, 0x0

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/miui/app/MiuiFboServiceInternal;->deliverMessage(Ljava/lang/String;IJ)V

    .line 301
    :cond_2
    :goto_2
    iget-object v1, p0, Lcom/android/server/ScoutSystemMonitor$ScreenStateReceiver;->this$0:Lcom/android/server/ScoutSystemMonitor;

    invoke-static {v1}, Lcom/android/server/ScoutSystemMonitor;->-$$Nest$fgetmiuiFboService(Lcom/android/server/ScoutSystemMonitor;)Lcom/miui/app/MiuiFboServiceInternal;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/miui/app/MiuiFboServiceInternal;->setScreenStatus(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 304
    goto :goto_3

    .line 302
    :catch_0
    move-exception v1

    .line 303
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "ScoutSystemMonitor"

    const-string v3, "brodacastReceiver exp "

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 306
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_3
    iget-object v1, p0, Lcom/android/server/ScoutSystemMonitor$ScreenStateReceiver;->this$0:Lcom/android/server/ScoutSystemMonitor;

    invoke-virtual {v1, v0}, Lcom/android/server/ScoutSystemMonitor;->updateScreenState(Z)V

    .line 307
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x7ed8ea7f -> :sswitch_1
        -0x56ac2893 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
