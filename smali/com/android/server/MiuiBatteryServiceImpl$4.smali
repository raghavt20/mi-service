.class Lcom/android/server/MiuiBatteryServiceImpl$4;
.super Landroid/content/BroadcastReceiver;
.source "MiuiBatteryServiceImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/MiuiBatteryServiceImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/MiuiBatteryServiceImpl;


# direct methods
.method constructor <init>(Lcom/android/server/MiuiBatteryServiceImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/MiuiBatteryServiceImpl;

    .line 203
    iput-object p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$4;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 206
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 207
    .local v0, "action":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/4 v2, -0x1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    goto :goto_0

    :sswitch_0
    const-string v1, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    goto :goto_1

    :sswitch_1
    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x3

    goto :goto_1

    :sswitch_2
    const-string v1, "android.hardware.usb.action.USB_DEVICE_DETACHED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_1

    :sswitch_3
    const-string v1, "android.hardware.usb.action.USB_DEVICE_ATTACHED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_1

    :goto_0
    move v1, v2

    :goto_1
    const/16 v3, 0x5083

    const/16 v4, 0x2717

    const-string v5, "device"

    const-wide/16 v6, 0x0

    packed-switch v1, :pswitch_data_0

    goto/16 :goto_2

    .line 228
    :pswitch_0
    const-string v1, "level"

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 229
    .local v1, "phoneBatteryLevel":I
    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$4;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryServiceImpl;)Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->isHandleConnect()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$4;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmLastPhoneBatteryLevel(Lcom/android/server/MiuiBatteryServiceImpl;)I

    move-result v2

    if-eq v2, v1, :cond_1

    .line 230
    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$4;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v2, v1}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fputmLastPhoneBatteryLevel(Lcom/android/server/MiuiBatteryServiceImpl;I)V

    .line 231
    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$4;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryServiceImpl;)Lmiui/util/IMiCharge;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/MiuiBatteryServiceImpl$4;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v3}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmLastPhoneBatteryLevel(Lcom/android/server/MiuiBatteryServiceImpl;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "PhoneBatteryChanged"

    invoke-virtual {v2, v4, v3}, Lmiui/util/IMiCharge;->setTypeCCommonInfo(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_2

    .line 225
    .end local v1    # "phoneBatteryLevel":I
    :pswitch_1
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$4;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryServiceImpl;)Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    move-result-object v1

    const/16 v2, 0x19

    invoke-virtual {v1, v2, v6, v7}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessageDelayed(IJ)V

    .line 226
    goto :goto_2

    .line 217
    :pswitch_2
    invoke-virtual {p2, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/hardware/usb/UsbDevice;

    .line 218
    .local v1, "detachedDevice":Landroid/hardware/usb/UsbDevice;
    if-eqz v1, :cond_1

    .line 219
    invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getVendorId()I

    move-result v2

    if-ne v4, v2, :cond_1

    .line 220
    invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getProductId()I

    move-result v2

    if-ne v3, v2, :cond_1

    .line 221
    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$4;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryServiceImpl;)Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    move-result-object v2

    const/16 v3, 0x18

    invoke-virtual {v2, v3, v1, v6, v7}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessageDelayed(ILjava/lang/Object;J)V

    goto :goto_2

    .line 209
    .end local v1    # "detachedDevice":Landroid/hardware/usb/UsbDevice;
    :pswitch_3
    invoke-virtual {p2, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/hardware/usb/UsbDevice;

    .line 210
    .local v1, "attachedDevice":Landroid/hardware/usb/UsbDevice;
    if-eqz v1, :cond_1

    .line 211
    invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getVendorId()I

    move-result v2

    if-ne v4, v2, :cond_1

    .line 212
    invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getProductId()I

    move-result v2

    if-ne v3, v2, :cond_1

    .line 213
    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$4;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryServiceImpl;)Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    move-result-object v2

    const/16 v3, 0x17

    invoke-virtual {v2, v3, v1, v6, v7}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessageDelayed(ILjava/lang/Object;J)V

    .line 235
    .end local v1    # "attachedDevice":Landroid/hardware/usb/UsbDevice;
    :cond_1
    :goto_2
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x7e02a835 -> :sswitch_3
        -0x5fdc9a67 -> :sswitch_2
        -0x5bb23923 -> :sswitch_1
        0x2f94f923 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
