class com.android.server.ScoutSystemMonitor$1 implements java.util.concurrent.Callable {
	 /* .source "ScoutSystemMonitor.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/ScoutSystemMonitor;->getScoutSystemDetailsAsync(Ljava/util/List;)Ljava/lang/String; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/Object;", */
/* "Ljava/util/concurrent/Callable<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* # instance fields */
final com.android.server.ScoutSystemMonitor this$0; //synthetic
final java.util.List val$handlerCheckers; //synthetic
/* # direct methods */
 com.android.server.ScoutSystemMonitor$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/ScoutSystemMonitor; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 531 */
this.this$0 = p1;
this.val$handlerCheckers = p2;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public java.lang.Object call ( ) { //bridge//synthethic
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 531 */
(( com.android.server.ScoutSystemMonitor$1 ) p0 ).call ( ); // invoke-virtual {p0}, Lcom/android/server/ScoutSystemMonitor$1;->call()Ljava/lang/String;
} // .end method
public java.lang.String call ( ) {
/* .locals 11 */
/* .line 533 */
int v0 = 0; // const/4 v0, 0x0
/* .line 534 */
/* .local v0, "stackTraces":Ljava/lang/String; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 535 */
/* .local v1, "details":Ljava/lang/StringBuilder; */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
v3 = v3 = this.val$handlerCheckers;
/* if-ge v2, v3, :cond_2 */
/* .line 536 */
v3 = this.val$handlerCheckers;
/* check-cast v3, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker; */
/* .line 537 */
/* .local v3, "mCheck":Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker; */
v4 = (( com.android.server.ScoutSystemMonitor$ScoutHandlerChecker ) v3 ).getThreadTid ( ); // invoke-virtual {v3}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->getThreadTid()I
/* .line 539 */
/* .local v4, "tid":I */
/* sget-boolean v5, Lcom/android/server/ScoutHelper;->IS_INTERNATIONAL_BUILD:Z */
/* if-nez v5, :cond_0 */
/* if-lez v4, :cond_0 */
/* .line 540 */
com.android.server.ScoutHelper .getMiuiStackTraceByTid ( v4 );
/* .local v5, "st":[Ljava/lang/StackTraceElement; */
/* .line 542 */
} // .end local v5 # "st":[Ljava/lang/StackTraceElement;
} // :cond_0
(( com.android.server.ScoutSystemMonitor$ScoutHandlerChecker ) v3 ).getThread ( ); // invoke-virtual {v3}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->getThread()Ljava/lang/Thread;
(( java.lang.Thread ) v5 ).getStackTrace ( ); // invoke-virtual {v5}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;
/* .line 544 */
/* .restart local v5 # "st":[Ljava/lang/StackTraceElement; */
} // :goto_1
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 545 */
/* array-length v6, v5 */
int v7 = 0; // const/4 v7, 0x0
} // :goto_2
/* if-ge v7, v6, :cond_1 */
/* aget-object v8, v5, v7 */
/* .line 546 */
/* .local v8, "element":Ljava/lang/StackTraceElement; */
final String v9 = " at "; // const-string v9, " at "
(( java.lang.StringBuilder ) v1 ).append ( v9 ); // invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v8 ); // invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v10 = "\n"; // const-string v10, "\n"
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 545 */
} // .end local v8 # "element":Ljava/lang/StackTraceElement;
/* add-int/lit8 v7, v7, 0x1 */
/* .line 549 */
} // :cond_1
final String v6 = "\n\n"; // const-string v6, "\n\n"
(( java.lang.StringBuilder ) v1 ).append ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 535 */
} // .end local v3 # "mCheck":Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;
} // .end local v4 # "tid":I
} // .end local v5 # "st":[Ljava/lang/StackTraceElement;
/* add-int/lit8 v2, v2, 0x1 */
/* .line 551 */
} // .end local v2 # "i":I
} // :cond_2
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 552 */
/* if-nez v0, :cond_3 */
int v2 = 0; // const/4 v2, 0x0
} // :cond_3
/* new-instance v2, Ljava/lang/String; */
/* invoke-direct {v2, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V */
} // :goto_3
} // .end method
