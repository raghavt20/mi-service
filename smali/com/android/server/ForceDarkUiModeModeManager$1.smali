.class Lcom/android/server/ForceDarkUiModeModeManager$1;
.super Ljava/lang/Object;
.source "ForceDarkUiModeModeManager.java"

# interfaces
.implements Lcom/android/server/ForceDarkAppConfigProvider$ForceDarkAppConfigChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/ForceDarkUiModeModeManager;->initForceDarkAppConfig()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/ForceDarkUiModeModeManager;


# direct methods
.method constructor <init>(Lcom/android/server/ForceDarkUiModeModeManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/ForceDarkUiModeModeManager;

    .line 263
    iput-object p1, p0, Lcom/android/server/ForceDarkUiModeModeManager$1;->this$0:Lcom/android/server/ForceDarkUiModeModeManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChange()V
    .locals 3

    .line 266
    invoke-static {}, Lcom/android/server/ForceDarkUiModeModeManager;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onForceDarkConfigChanged"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    invoke-static {}, Landroid/view/ForceDarkHelperStub;->getInstance()Landroid/view/ForceDarkHelperStub;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/ForceDarkUiModeModeManager$1;->this$0:Lcom/android/server/ForceDarkUiModeModeManager;

    invoke-static {v1}, Lcom/android/server/ForceDarkUiModeModeManager;->-$$Nest$fgetmUiModeManagerService(Lcom/android/server/ForceDarkUiModeModeManager;)Lcom/android/server/UiModeManagerService;

    move-result-object v2

    .line 269
    invoke-virtual {v2}, Lcom/android/server/UiModeManagerService;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/server/ForceDarkUiModeModeManager;->-$$Nest$mgetForceDarkAppConfig(Lcom/android/server/ForceDarkUiModeModeManager;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 268
    invoke-virtual {v0, v1}, Landroid/view/ForceDarkHelperStub;->onForceDarkConfigChanged(Ljava/lang/String;)V

    .line 270
    return-void
.end method
