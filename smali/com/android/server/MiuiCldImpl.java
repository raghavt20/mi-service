class com.android.server.MiuiCldImpl extends com.android.server.MiuiCldStub {
	 /* .source "MiuiCldImpl.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.MiuiCldStub$$" */
} // .end annotation
/* # static fields */
private static final Integer CLD_ERROR;
private static final Integer CLD_FREQUENT;
private static final Integer CLD_NOT_SUPPORT;
private static final Integer CLD_SUCCESS;
private static final Long DEFAULT_MINIMUM_CLD_INTERVAL;
private static final java.lang.String LAST_CLD_FILE;
private static final java.lang.String MIUI_CLD_PROCESSED_DONE;
private static final java.lang.String TAG;
/* # instance fields */
private final android.os.IVoldTaskListener mCldListener;
private android.content.Context mContext;
private java.io.File mLastCldFile;
private volatile android.os.IVold mVold;
/* # direct methods */
 com.android.server.MiuiCldImpl ( ) {
	 /* .locals 1 */
	 /* .line 25 */
	 /* invoke-direct {p0}, Lcom/android/server/MiuiCldStub;-><init>()V */
	 /* .line 39 */
	 /* new-instance v0, Lcom/android/server/MiuiCldImpl$1; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/MiuiCldImpl$1;-><init>(Lcom/android/server/MiuiCldImpl;)V */
	 this.mCldListener = v0;
	 return;
} // .end method
/* # virtual methods */
public Integer getCldFragLevel ( ) {
	 /* .locals 5 */
	 /* .line 88 */
	 final String v0 = "MiuiCldImpl"; // const-string v0, "MiuiCldImpl"
	 try { // :try_start_0
		 /* const-class v1, Landroid/os/IVold; */
		 final String v2 = "getCldFragLevel"; // const-string v2, "getCldFragLevel"
		 int v3 = 0; // const/4 v3, 0x0
		 /* new-array v4, v3, [Ljava/lang/Class; */
		 (( java.lang.Class ) v1 ).getDeclaredMethod ( v2, v4 ); // invoke-virtual {v1, v2, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
		 /* .line 89 */
		 /* .local v1, "method":Ljava/lang/reflect/Method; */
		 v2 = this.mVold;
		 /* new-array v3, v3, [Ljava/lang/Object; */
		 (( java.lang.reflect.Method ) v1 ).invoke ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
		 /* check-cast v2, Ljava/lang/Integer; */
		 v2 = 		 (( java.lang.Integer ) v2 ).intValue ( ); // invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
		 /* .line 90 */
		 /* .local v2, "fragLevel":I */
		 /* new-instance v3, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v4 = "frag level: "; // const-string v4, "frag level: "
		 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Slog .i ( v0,v3 );
		 /* :try_end_0 */
		 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 91 */
		 /* .line 92 */
	 } // .end local v1 # "method":Ljava/lang/reflect/Method;
} // .end local v2 # "fragLevel":I
/* :catch_0 */
/* move-exception v1 */
/* .line 93 */
/* .local v1, "e":Ljava/lang/Exception; */
android.util.Slog .wtf ( v0,v1 );
/* .line 94 */
int v0 = -1; // const/4 v0, -0x1
} // .end method
public void initCldListener ( android.os.IVold p0, android.content.Context p1 ) {
/* .locals 8 */
/* .param p1, "vold" # Landroid/os/IVold; */
/* .param p2, "context" # Landroid/content/Context; */
/* .line 53 */
this.mVold = p1;
/* .line 54 */
this.mContext = p2;
/* .line 55 */
android.os.Environment .getDataDirectory ( );
/* .line 56 */
/* .local v0, "dataDir":Ljava/io/File; */
/* new-instance v1, Ljava/io/File; */
/* const-string/jumbo v2, "system" */
/* invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* .line 57 */
/* .local v1, "systemDir":Ljava/io/File; */
/* new-instance v2, Ljava/io/File; */
final String v3 = "last-cld"; // const-string v3, "last-cld"
/* invoke-direct {v2, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
this.mLastCldFile = v2;
/* .line 59 */
try { // :try_start_0
	 /* const-class v2, Landroid/os/IVold; */
	 /* const-string/jumbo v3, "setCldListener" */
	 int v4 = 1; // const/4 v4, 0x1
	 /* new-array v5, v4, [Ljava/lang/Class; */
	 /* const-class v6, Landroid/os/IVoldTaskListener; */
	 int v7 = 0; // const/4 v7, 0x0
	 /* aput-object v6, v5, v7 */
	 (( java.lang.Class ) v2 ).getDeclaredMethod ( v3, v5 ); // invoke-virtual {v2, v3, v5}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
	 /* .line 60 */
	 /* .local v2, "setCldListener":Ljava/lang/reflect/Method; */
	 v3 = this.mVold;
	 /* new-array v4, v4, [Ljava/lang/Object; */
	 v5 = this.mCldListener;
	 /* aput-object v5, v4, v7 */
	 (( java.lang.reflect.Method ) v2 ).invoke ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 63 */
	 /* nop */
} // .end local v2 # "setCldListener":Ljava/lang/reflect/Method;
/* .line 61 */
/* :catch_0 */
/* move-exception v2 */
/* .line 62 */
/* .local v2, "e":Ljava/lang/Exception; */
final String v3 = "MiuiCldImpl"; // const-string v3, "MiuiCldImpl"
final String v4 = "Failed on initCldListener"; // const-string v4, "Failed on initCldListener"
android.util.Slog .wtf ( v3,v4,v2 );
/* .line 64 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
Boolean needPostCldResult ( ) {
/* .locals 5 */
/* .line 99 */
final String v0 = "MiuiCldImpl"; // const-string v0, "MiuiCldImpl"
int v1 = 0; // const/4 v1, 0x0
/* .line 100 */
/* .local v1, "post":Z */
try { // :try_start_0
/* new-instance v2, Ljava/io/FileInputStream; */
v3 = this.mLastCldFile;
/* invoke-direct {v2, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V */
/* :try_end_0 */
/* .catch Ljava/io/FileNotFoundException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 101 */
/* .local v2, "fis":Ljava/io/FileInputStream; */
try { // :try_start_1
v3 = (( java.io.FileInputStream ) v2 ).read ( ); // invoke-virtual {v2}, Ljava/io/FileInputStream;->read()I
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
int v4 = 1; // const/4 v4, 0x1
/* if-ne v4, v3, :cond_0 */
} // :cond_0
int v4 = 0; // const/4 v4, 0x0
} // :goto_0
/* move v1, v4 */
/* .line 102 */
try { // :try_start_2
(( java.io.FileInputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
/* :try_end_2 */
/* .catch Ljava/io/FileNotFoundException; {:try_start_2 ..:try_end_2} :catch_1 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 100 */
/* :catchall_0 */
/* move-exception v3 */
try { // :try_start_3
(( java.io.FileInputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v4 */
try { // :try_start_4
	 (( java.lang.Throwable ) v3 ).addSuppressed ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v1 # "post":Z
} // .end local p0 # "this":Lcom/android/server/MiuiCldImpl;
} // :goto_1
/* throw v3 */
/* :try_end_4 */
/* .catch Ljava/io/FileNotFoundException; {:try_start_4 ..:try_end_4} :catch_1 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 105 */
} // .end local v2 # "fis":Ljava/io/FileInputStream;
/* .restart local v1 # "post":Z */
/* .restart local p0 # "this":Lcom/android/server/MiuiCldImpl; */
/* :catch_0 */
/* move-exception v2 */
/* .line 106 */
/* .local v2, "e":Ljava/io/IOException; */
final String v3 = "Failed reading last-cld"; // const-string v3, "Failed reading last-cld"
android.util.Slog .wtf ( v0,v3,v2 );
/* .line 102 */
} // .end local v2 # "e":Ljava/io/IOException;
/* :catch_1 */
/* move-exception v2 */
/* .line 104 */
/* .local v2, "e":Ljava/io/FileNotFoundException; */
final String v3 = "File last-cld not exist"; // const-string v3, "File last-cld not exist"
android.util.Slog .i ( v0,v3 );
/* .line 107 */
} // .end local v2 # "e":Ljava/io/FileNotFoundException;
} // :goto_2
/* nop */
/* .line 108 */
} // :goto_3
} // .end method
void postCldResult ( android.os.PersistableBundle p0 ) {
/* .locals 6 */
/* .param p1, "extras" # Landroid/os/PersistableBundle; */
/* .line 113 */
int v0 = -1; // const/4 v0, -0x1
final String v1 = "frag_level"; // const-string v1, "frag_level"
v0 = (( android.os.PersistableBundle ) p1 ).getInt ( v1, v0 ); // invoke-virtual {p1, v1, v0}, Landroid/os/PersistableBundle;->getInt(Ljava/lang/String;I)I
/* .line 114 */
/* .local v0, "frag_level":I */
/* new-instance v2, Landroid/content/Intent; */
final String v3 = "miui.intent.action.MIUI_CLD_PROCESSED_DONE"; // const-string v3, "miui.intent.action.MIUI_CLD_PROCESSED_DONE"
/* invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 115 */
/* .local v2, "intent":Landroid/content/Intent; */
(( android.content.Intent ) v2 ).putExtra ( v1, v0 ); // invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 116 */
v1 = this.mContext;
v3 = android.os.UserHandle.ALL;
(( android.content.Context ) v1 ).sendBroadcastAsUser ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
/* .line 117 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Broadcast cld processed done complete with level "; // const-string v3, "Broadcast cld processed done complete with level "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MiuiCldImpl"; // const-string v3, "MiuiCldImpl"
android.util.Slog .i ( v3,v1 );
/* .line 120 */
try { // :try_start_0
/* new-instance v1, Ljava/io/FileOutputStream; */
v4 = this.mLastCldFile;
/* invoke-direct {v1, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 121 */
/* .local v1, "fos":Ljava/io/FileOutputStream; */
int v4 = 0; // const/4 v4, 0x0
try { // :try_start_1
(( java.io.FileOutputStream ) v1 ).write ( v4 ); // invoke-virtual {v1, v4}, Ljava/io/FileOutputStream;->write(I)V
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 122 */
try { // :try_start_2
(( java.io.FileOutputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 124 */
} // .end local v1 # "fos":Ljava/io/FileOutputStream;
/* .line 120 */
/* .restart local v1 # "fos":Ljava/io/FileOutputStream; */
/* :catchall_0 */
/* move-exception v4 */
try { // :try_start_3
(( java.io.FileOutputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v5 */
try { // :try_start_4
(( java.lang.Throwable ) v4 ).addSuppressed ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "frag_level":I
} // .end local v2 # "intent":Landroid/content/Intent;
} // .end local p0 # "this":Lcom/android/server/MiuiCldImpl;
} // .end local p1 # "extras":Landroid/os/PersistableBundle;
} // :goto_0
/* throw v4 */
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 122 */
} // .end local v1 # "fos":Ljava/io/FileOutputStream;
/* .restart local v0 # "frag_level":I */
/* .restart local v2 # "intent":Landroid/content/Intent; */
/* .restart local p0 # "this":Lcom/android/server/MiuiCldImpl; */
/* .restart local p1 # "extras":Landroid/os/PersistableBundle; */
/* :catch_0 */
/* move-exception v1 */
/* .line 123 */
/* .local v1, "e":Ljava/io/IOException; */
final String v4 = "Failed recording last-cld"; // const-string v4, "Failed recording last-cld"
android.util.Slog .wtf ( v3,v4,v1 );
/* .line 125 */
} // .end local v1 # "e":Ljava/io/IOException;
} // :goto_1
return;
} // .end method
public Integer triggerCld ( ) {
/* .locals 7 */
/* .line 68 */
int v0 = 2; // const/4 v0, 0x2
/* .line 69 */
/* .local v0, "result":I */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v1 */
v3 = this.mLastCldFile;
(( java.io.File ) v3 ).lastModified ( ); // invoke-virtual {v3}, Ljava/io/File;->lastModified()J
/* move-result-wide v3 */
/* sub-long/2addr v1, v3 */
/* .line 70 */
/* .local v1, "timeSinceLast":J */
v3 = this.mLastCldFile;
v3 = (( java.io.File ) v3 ).exists ( ); // invoke-virtual {v3}, Ljava/io/File;->exists()Z
final String v4 = "MiuiCldImpl"; // const-string v4, "MiuiCldImpl"
if ( v3 != null) { // if-eqz v3, :cond_1
/* const-wide/32 v5, 0x5265c00 */
/* cmp-long v3, v1, v5 */
/* if-lez v3, :cond_0 */
/* .line 79 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* .line 80 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Last cld run in "; // const-string v5, "Last cld run in "
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-wide/16 v5, 0x3e8 */
/* div-long v5, v1, v5 */
(( java.lang.StringBuilder ) v3 ).append ( v5, v6 ); // invoke-virtual {v3, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v5 = "s ago.donot run cld too frequently"; // const-string v5, "s ago.donot run cld too frequently"
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v4,v3 );
/* .line 71 */
} // :cond_1
} // :goto_0
try { // :try_start_0
/* new-instance v3, Ljava/io/FileOutputStream; */
v5 = this.mLastCldFile;
/* invoke-direct {v3, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 72 */
/* .local v3, "fos":Ljava/io/FileOutputStream; */
int v5 = 1; // const/4 v5, 0x1
try { // :try_start_1
(( java.io.FileOutputStream ) v3 ).write ( v5 ); // invoke-virtual {v3, v5}, Ljava/io/FileOutputStream;->write(I)V
/* .line 73 */
int v0 = 0; // const/4 v0, 0x0
/* .line 74 */
final String v5 = "Trigger cld success"; // const-string v5, "Trigger cld success"
android.util.Slog .i ( v4,v5 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 75 */
try { // :try_start_2
(( java.io.FileOutputStream ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 71 */
/* :catchall_0 */
/* move-exception v5 */
try { // :try_start_3
(( java.io.FileOutputStream ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v6 */
try { // :try_start_4
(( java.lang.Throwable ) v5 ).addSuppressed ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "result":I
} // .end local v1 # "timeSinceLast":J
} // .end local p0 # "this":Lcom/android/server/MiuiCldImpl;
} // :goto_1
/* throw v5 */
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 75 */
} // .end local v3 # "fos":Ljava/io/FileOutputStream;
/* .restart local v0 # "result":I */
/* .restart local v1 # "timeSinceLast":J */
/* .restart local p0 # "this":Lcom/android/server/MiuiCldImpl; */
/* :catch_0 */
/* move-exception v3 */
/* .line 76 */
/* .local v3, "e":Ljava/io/IOException; */
final String v5 = "Failed recording last-cld"; // const-string v5, "Failed recording last-cld"
android.util.Slog .wtf ( v4,v5,v3 );
/* .line 77 */
} // .end local v3 # "e":Ljava/io/IOException;
} // :goto_2
/* nop */
/* .line 82 */
} // :goto_3
} // .end method
