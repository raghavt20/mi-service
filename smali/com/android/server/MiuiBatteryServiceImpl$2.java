class com.android.server.MiuiBatteryServiceImpl$2 extends android.content.BroadcastReceiver {
	 /* .source "MiuiBatteryServiceImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/MiuiBatteryServiceImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.MiuiBatteryServiceImpl this$0; //synthetic
/* # direct methods */
 com.android.server.MiuiBatteryServiceImpl$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/MiuiBatteryServiceImpl; */
/* .line 164 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 167 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 168 */
/* .local v0, "action":Ljava/lang/String; */
final String v1 = "android.bluetooth.device.action.ACL_CONNECTED"; // const-string v1, "android.bluetooth.device.action.ACL_CONNECTED"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 169 */
	 v1 = this.this$0;
	 int v2 = 2; // const/4 v2, 0x2
	 com.android.server.MiuiBatteryServiceImpl .-$$Nest$fputmBtConnectState ( v1,v2 );
	 /* .line 170 */
	 v1 = this.this$0;
	 com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmHandler ( v1 );
	 v2 = this.this$0;
	 com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetshutDownRnuable ( v2 );
	 (( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) v1 ).removeCallbacks ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->removeCallbacks(Ljava/lang/Runnable;)V
	 /* .line 171 */
} // :cond_0
final String v1 = "android.bluetooth.device.action.ACL_DISCONNECTED"; // const-string v1, "android.bluetooth.device.action.ACL_DISCONNECTED"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
	 /* .line 172 */
	 v1 = this.this$0;
	 int v2 = 0; // const/4 v2, 0x0
	 com.android.server.MiuiBatteryServiceImpl .-$$Nest$fputmBtConnectState ( v1,v2 );
	 /* .line 173 */
	 v1 = this.this$0;
	 v1 = 	 com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetisDisCharging ( v1 );
	 if ( v1 != null) { // if-eqz v1, :cond_1
		 /* .line 174 */
		 v1 = this.this$0;
		 com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetwakeLock ( v1 );
		 /* const-wide/32 v2, 0x57e40 */
		 (( android.os.PowerManager$WakeLock ) v1 ).acquire ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/os/PowerManager$WakeLock;->acquire(J)V
		 /* .line 175 */
		 v1 = this.this$0;
		 com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmHandler ( v1 );
		 v2 = this.this$0;
		 com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetshutDownRnuable ( v2 );
		 (( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) v1 ).removeCallbacks ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->removeCallbacks(Ljava/lang/Runnable;)V
		 /* .line 176 */
		 v1 = this.this$0;
		 com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmHandler ( v1 );
		 v2 = this.this$0;
		 com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetshutDownRnuable ( v2 );
		 /* const-wide/32 v3, 0x493e0 */
		 (( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) v1 ).postDelayed ( v2, v3, v4 ); // invoke-virtual {v1, v2, v3, v4}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->postDelayed(Ljava/lang/Runnable;J)Z
		 /* .line 180 */
	 } // :cond_1
} // :goto_0
return;
} // .end method
