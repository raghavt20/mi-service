.class Lcom/android/server/cameracovered/CameraBlackCoveredManager$1;
.super Ljava/lang/Object;
.source "CameraBlackCoveredManager.java"

# interfaces
.implements Landroid/hardware/display/DisplayManager$DisplayListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/cameracovered/CameraBlackCoveredManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;


# direct methods
.method constructor <init>(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    .line 127
    iput-object p1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$1;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDisplayAdded(I)V
    .locals 0
    .param p1, "displayId"    # I

    .line 130
    return-void
.end method

.method public onDisplayChanged(I)V
    .locals 2
    .param p1, "displayId"    # I

    .line 138
    iget-object v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$1;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v0}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmDisplay(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Landroid/view/Display;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$1;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v1}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmDisplayInfo(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Landroid/view/DisplayInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/Display;->getDisplayInfo(Landroid/view/DisplayInfo;)Z

    .line 139
    iget-object v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$1;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v0}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmLastDisplayRotation(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)I

    move-result v0

    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$1;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v1}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmDisplayInfo(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Landroid/view/DisplayInfo;

    move-result-object v1

    iget v1, v1, Landroid/view/DisplayInfo;->rotation:I

    if-eq v0, v1, :cond_0

    .line 140
    const-string v0, "CameraBlackCoveredManager"

    const-string v1, "onDisplayChanged"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    iget-object v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$1;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-virtual {v0}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->stopCameraAnimation()V

    .line 142
    iget-object v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$1;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->startCameraAnimation(Z)V

    .line 144
    :cond_0
    return-void
.end method

.method public onDisplayRemoved(I)V
    .locals 0
    .param p1, "displayId"    # I

    .line 134
    return-void
.end method
