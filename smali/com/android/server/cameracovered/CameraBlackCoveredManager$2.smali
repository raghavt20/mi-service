.class Lcom/android/server/cameracovered/CameraBlackCoveredManager$2;
.super Landroid/hardware/camera2/CameraManager$AvailabilityCallback;
.source "CameraBlackCoveredManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/cameracovered/CameraBlackCoveredManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;


# direct methods
.method constructor <init>(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    .line 195
    iput-object p1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$2;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-direct {p0}, Landroid/hardware/camera2/CameraManager$AvailabilityCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onCameraAvailable(Ljava/lang/String;)V
    .locals 4
    .param p1, "cameraId"    # Ljava/lang/String;

    .line 198
    invoke-super {p0, p1}, Landroid/hardware/camera2/CameraManager$AvailabilityCallback;->onCameraAvailable(Ljava/lang/String;)V

    .line 199
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 200
    .local v0, "id":I
    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$2;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v1}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmFrontCameraID(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 201
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "wz_debug onCameraAvailable"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "CameraBlackCoveredManager"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$2;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v1}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmDisplayManager(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Landroid/hardware/display/DisplayManager;

    move-result-object v1

    iget-object v3, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$2;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v3}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmDisplayListener(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Landroid/hardware/display/DisplayManager$DisplayListener;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/hardware/display/DisplayManager;->unregisterDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;)V

    .line 203
    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$2;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-virtual {v1}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->stopCameraAnimation()V

    .line 204
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onCameraAvailable "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    :cond_0
    return-void
.end method

.method public onCameraUnavailable(Ljava/lang/String;)V
    .locals 4
    .param p1, "cameraId"    # Ljava/lang/String;

    .line 210
    iget-object v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$2;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v0}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmFrontCameraID(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 211
    iget-object v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$2;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v0}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$minitCameraId(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)V

    .line 213
    :cond_0
    invoke-super {p0, p1}, Landroid/hardware/camera2/CameraManager$AvailabilityCallback;->onCameraUnavailable(Ljava/lang/String;)V

    .line 214
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 215
    .local v0, "id":I
    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$2;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v1}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmFrontCameraID(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 216
    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$2;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v1}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmDisplayManager(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Landroid/hardware/display/DisplayManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$2;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v2}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmDisplayListener(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Landroid/hardware/display/DisplayManager$DisplayListener;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$2;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v3}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmHandler(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/hardware/display/DisplayManager;->registerDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;Landroid/os/Handler;)V

    .line 217
    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$2;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v1}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmDisplay(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Landroid/view/Display;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$2;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v2}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmDisplayInfo(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Landroid/view/DisplayInfo;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/Display;->getDisplayInfo(Landroid/view/DisplayInfo;)Z

    .line 218
    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$2;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->startCameraAnimation(Z)V

    .line 219
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCameraUnavailable "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "CameraBlackCoveredManager"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    :cond_1
    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$2;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v1, v0}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fputmLastUnavailableCameraId(Lcom/android/server/cameracovered/CameraBlackCoveredManager;I)V

    .line 222
    return-void
.end method
