.class public Lcom/android/server/cameracovered/CameraCircleBlackView;
.super Landroid/widget/ImageView;
.source "CameraCircleBlackView.java"


# instance fields
.field mAttrs:Landroid/util/AttributeSet;

.field private mContext:Landroid/content/Context;

.field private mInternalRadio:F

.field private mPaint:Landroid/graphics/Paint;

.field private mViewCenterX:I

.field private mViewCenterY:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 24
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/server/cameracovered/CameraCircleBlackView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 28
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/server/cameracovered/CameraCircleBlackView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 32
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 19
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/server/cameracovered/CameraCircleBlackView;->mPaint:Landroid/graphics/Paint;

    .line 33
    iput-object p1, p0, Lcom/android/server/cameracovered/CameraCircleBlackView;->mContext:Landroid/content/Context;

    .line 34
    iput-object p2, p0, Lcom/android/server/cameracovered/CameraCircleBlackView;->mAttrs:Landroid/util/AttributeSet;

    .line 35
    invoke-virtual {p0}, Lcom/android/server/cameracovered/CameraCircleBlackView;->init()V

    .line 36
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .line 74
    invoke-super {p0, p1}, Landroid/widget/ImageView;->draw(Landroid/graphics/Canvas;)V

    .line 75
    iget v0, p0, Lcom/android/server/cameracovered/CameraCircleBlackView;->mViewCenterX:I

    int-to-float v0, v0

    iget v1, p0, Lcom/android/server/cameracovered/CameraCircleBlackView;->mViewCenterY:I

    int-to-float v1, v1

    iget v2, p0, Lcom/android/server/cameracovered/CameraCircleBlackView;->mInternalRadio:F

    iget-object v3, p0, Lcom/android/server/cameracovered/CameraCircleBlackView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 76
    return-void
.end method

.method public init()V
    .locals 2

    .line 56
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/android/server/cameracovered/CameraCircleBlackView;->mPaint:Landroid/graphics/Paint;

    .line 57
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 58
    iget-object v0, p0, Lcom/android/server/cameracovered/CameraCircleBlackView;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 59
    iget-object v0, p0, Lcom/android/server/cameracovered/CameraCircleBlackView;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 60
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 3
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .line 64
    invoke-super/range {p0 .. p5}, Landroid/widget/ImageView;->onLayout(ZIIII)V

    .line 66
    invoke-virtual {p0}, Lcom/android/server/cameracovered/CameraCircleBlackView;->getMeasuredWidth()I

    move-result v0

    .line 67
    .local v0, "viewWidth":I
    invoke-virtual {p0}, Lcom/android/server/cameracovered/CameraCircleBlackView;->getMeasuredHeight()I

    move-result v1

    .line 68
    .local v1, "viewHeight":I
    div-int/lit8 v2, v0, 0x2

    iput v2, p0, Lcom/android/server/cameracovered/CameraCircleBlackView;->mViewCenterX:I

    .line 69
    div-int/lit8 v2, v1, 0x2

    iput v2, p0, Lcom/android/server/cameracovered/CameraCircleBlackView;->mViewCenterY:I

    .line 70
    return-void
.end method

.method public setRadius(F)V
    .locals 0
    .param p1, "radius"    # F

    .line 39
    iput p1, p0, Lcom/android/server/cameracovered/CameraCircleBlackView;->mInternalRadio:F

    .line 40
    return-void
.end method

.method public setRadiusColor(I)V
    .locals 2
    .param p1, "color"    # I

    .line 43
    const/high16 v0, -0x1000000

    packed-switch p1, :pswitch_data_0

    .line 51
    iget-object v1, p0, Lcom/android/server/cameracovered/CameraCircleBlackView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_0

    .line 48
    :pswitch_0
    iget-object v1, p0, Lcom/android/server/cameracovered/CameraCircleBlackView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 49
    goto :goto_0

    .line 45
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/cameracovered/CameraCircleBlackView;->mPaint:Landroid/graphics/Paint;

    const v1, -0xff0100

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 46
    nop

    .line 53
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
