.class public Lcom/android/server/cameracovered/CupMuraCoveredView;
.super Landroid/widget/ImageView;
.source "CupMuraCoveredView.java"


# static fields
.field static final TAG:Ljava/lang/String; = "CupMuraCoveredView"


# instance fields
.field protected mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 20
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/server/cameracovered/CupMuraCoveredView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 24
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/server/cameracovered/CupMuraCoveredView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .line 28
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    iput-object p1, p0, Lcom/android/server/cameracovered/CupMuraCoveredView;->mContext:Landroid/content/Context;

    .line 30
    return-void
.end method


# virtual methods
.method public setRadius(F)V
    .locals 5
    .param p1, "r"    # F

    .line 34
    float-to-int v0, p1

    .line 36
    .local v0, "radius":I
    iget-object v1, p0, Lcom/android/server/cameracovered/CupMuraCoveredView;->mContext:Landroid/content/Context;

    const-string v2, "CupMuraCoveredView"

    if-nez v1, :cond_0

    .line 37
    const-string/jumbo v1, "setRadius error because mContext is null"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 38
    return-void

    .line 41
    :cond_0
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x1108018e

    invoke-static {v1, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 43
    .local v1, "bmp":Landroid/graphics/Bitmap;
    if-nez v1, :cond_1

    .line 44
    const-string/jumbo v3, "the bitmap from decodeResource is null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    return-void

    .line 48
    :cond_1
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    mul-int/lit8 v3, v0, 0x2

    if-ne v2, v3, :cond_3

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    mul-int/lit8 v3, v0, 0x2

    if-eq v2, v3, :cond_2

    goto :goto_0

    .line 51
    :cond_2
    move-object v2, v1

    .local v2, "sbmp":Landroid/graphics/Bitmap;
    goto :goto_1

    .line 49
    .end local v2    # "sbmp":Landroid/graphics/Bitmap;
    :cond_3
    :goto_0
    mul-int/lit8 v2, v0, 0x2

    mul-int/lit8 v3, v0, 0x2

    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 53
    .restart local v2    # "sbmp":Landroid/graphics/Bitmap;
    :goto_1
    invoke-virtual {p0, v2}, Lcom/android/server/cameracovered/CupMuraCoveredView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 54
    sget-object v3, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v3}, Lcom/android/server/cameracovered/CupMuraCoveredView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 55
    return-void
.end method
