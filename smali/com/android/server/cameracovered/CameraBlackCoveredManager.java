public class com.android.server.cameracovered.CameraBlackCoveredManager {
	 /* .source "CameraBlackCoveredManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;, */
	 /* Lcom/android/server/cameracovered/CameraBlackCoveredManager$HomeGestureReceiver; */
	 /* } */
} // .end annotation
/* # static fields */
public static final java.lang.String ACTION_FULLSCREEN_STATE_CHANGE;
public static final java.lang.String ACTION_LAUNCH_HOME_FROM_HOTKEY;
private static final Integer COVERED_BLACK_TYPE;
private static final Integer COVERED_HBM_TYPE;
private static final Integer COVERED_MURA_TYPE;
private static final Integer CUP_MURA_COVER_ENTER;
private static final Integer CUP_MURA_COVER_EXIT;
public static final java.lang.String EXTRA_PACKAGE_NAME;
public static final java.lang.String EXTRA_STATE_NAME;
private static final java.lang.String FORCE_BLACK_V2;
private static final Integer FRONT_CAMERA_CLOSE;
private static final Integer FRONT_CAMERA_HIDE;
private static final Integer FRONT_CAMERA_OPEN;
private static final Integer FRONT_CAMERA_SHOW;
private static final Integer HBM_ENTER;
private static final Integer HBM_EXIT;
private static final java.lang.String PERMISSION_INTERNAL_GENERAL_API;
public static final java.lang.String STATE_CLOSE_WINDOW;
public static final java.lang.String STATE_QUICK_SWITCH_ANIM_END;
public static final java.lang.String STATE_TO_ANOTHER_APP;
public static final java.lang.String STATE_TO_HOME;
public static final java.lang.String STATE_TO_RECENTS;
public static final java.lang.String TAG;
private static final Integer VIRTUAL_CAMERA_BOUNDARY;
public static final java.util.ArrayList sBlackList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public static final java.util.ArrayList sBroadcastBlackList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public static final java.util.ArrayList sForceShowList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private android.hardware.camera2.CameraManager$AvailabilityCallback mAvailabilityCallback;
private java.util.List mBackCameraID;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.android.server.cameracovered.CameraCircleBlackView mBlackCoveredView;
private android.hardware.camera2.CameraManager mCameraManager;
private android.content.Context mContext;
private android.view.Display mDisplay;
private android.view.DisplayInfo mDisplayInfo;
private final android.hardware.display.DisplayManager$DisplayListener mDisplayListener;
private android.hardware.display.DisplayManager mDisplayManager;
private android.content.IntentFilter mFilter_launch_home;
private android.content.IntentFilter mFilter_state_change;
private miui.process.IForegroundInfoListener$Stub mForegroundInfoChangeListener;
private java.lang.String mForegroundPackage;
private java.util.List mFrontCameraID;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.android.server.cameracovered.CameraHBMCoveredView mHBMCoveredView;
private android.os.Handler mHandler;
private Boolean mHideNotch;
private android.database.ContentObserver mHideNotchObserver;
private com.android.server.cameracovered.CameraBlackCoveredManager$HomeGestureReceiver mHomeGestureReceiver;
private Boolean mIsAddedBlackView;
private Boolean mIsAddedHBMView;
private Boolean mIsAddedMuraView;
private Boolean mIsAddedView;
private Integer mLastDisplayRotation;
private Integer mLastUnavailableCameraId;
private com.android.server.cameracovered.CupMuraCoveredView mMuraCoveredView;
private java.lang.String mOccupiedPackage;
private android.view.WindowManager mWindowManager;
private com.android.server.wm.WindowManagerInternal mWindowManagerService;
/* # direct methods */
static com.android.server.cameracovered.CameraCircleBlackView -$$Nest$fgetmBlackCoveredView ( com.android.server.cameracovered.CameraBlackCoveredManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mBlackCoveredView;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.android.server.cameracovered.CameraBlackCoveredManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static android.view.Display -$$Nest$fgetmDisplay ( com.android.server.cameracovered.CameraBlackCoveredManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mDisplay;
} // .end method
static android.view.DisplayInfo -$$Nest$fgetmDisplayInfo ( com.android.server.cameracovered.CameraBlackCoveredManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mDisplayInfo;
} // .end method
static android.hardware.display.DisplayManager$DisplayListener -$$Nest$fgetmDisplayListener ( com.android.server.cameracovered.CameraBlackCoveredManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mDisplayListener;
} // .end method
static android.hardware.display.DisplayManager -$$Nest$fgetmDisplayManager ( com.android.server.cameracovered.CameraBlackCoveredManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mDisplayManager;
} // .end method
static java.lang.String -$$Nest$fgetmForegroundPackage ( com.android.server.cameracovered.CameraBlackCoveredManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mForegroundPackage;
} // .end method
static java.util.List -$$Nest$fgetmFrontCameraID ( com.android.server.cameracovered.CameraBlackCoveredManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mFrontCameraID;
} // .end method
static com.android.server.cameracovered.CameraHBMCoveredView -$$Nest$fgetmHBMCoveredView ( com.android.server.cameracovered.CameraBlackCoveredManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHBMCoveredView;
} // .end method
static android.os.Handler -$$Nest$fgetmHandler ( com.android.server.cameracovered.CameraBlackCoveredManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHandler;
} // .end method
static Boolean -$$Nest$fgetmIsAddedBlackView ( com.android.server.cameracovered.CameraBlackCoveredManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mIsAddedBlackView:Z */
} // .end method
static Boolean -$$Nest$fgetmIsAddedHBMView ( com.android.server.cameracovered.CameraBlackCoveredManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mIsAddedHBMView:Z */
} // .end method
static Boolean -$$Nest$fgetmIsAddedMuraView ( com.android.server.cameracovered.CameraBlackCoveredManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mIsAddedMuraView:Z */
} // .end method
static Integer -$$Nest$fgetmLastDisplayRotation ( com.android.server.cameracovered.CameraBlackCoveredManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mLastDisplayRotation:I */
} // .end method
static com.android.server.cameracovered.CupMuraCoveredView -$$Nest$fgetmMuraCoveredView ( com.android.server.cameracovered.CameraBlackCoveredManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mMuraCoveredView;
} // .end method
static java.lang.String -$$Nest$fgetmOccupiedPackage ( com.android.server.cameracovered.CameraBlackCoveredManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mOccupiedPackage;
} // .end method
static android.view.WindowManager -$$Nest$fgetmWindowManager ( com.android.server.cameracovered.CameraBlackCoveredManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mWindowManager;
} // .end method
static void -$$Nest$fputmForegroundPackage ( com.android.server.cameracovered.CameraBlackCoveredManager p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.mForegroundPackage = p1;
return;
} // .end method
static void -$$Nest$fputmHideNotch ( com.android.server.cameracovered.CameraBlackCoveredManager p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mHideNotch:Z */
return;
} // .end method
static void -$$Nest$fputmIsAddedBlackView ( com.android.server.cameracovered.CameraBlackCoveredManager p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mIsAddedBlackView:Z */
return;
} // .end method
static void -$$Nest$fputmIsAddedHBMView ( com.android.server.cameracovered.CameraBlackCoveredManager p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mIsAddedHBMView:Z */
return;
} // .end method
static void -$$Nest$fputmIsAddedMuraView ( com.android.server.cameracovered.CameraBlackCoveredManager p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mIsAddedMuraView:Z */
return;
} // .end method
static void -$$Nest$fputmLastUnavailableCameraId ( com.android.server.cameracovered.CameraBlackCoveredManager p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mLastUnavailableCameraId:I */
return;
} // .end method
static android.view.WindowManager$LayoutParams -$$Nest$mgetWindowParam ( com.android.server.cameracovered.CameraBlackCoveredManager p0, Integer p1, Integer p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->getWindowParam(II)Landroid/view/WindowManager$LayoutParams; */
} // .end method
static void -$$Nest$mhandleHomeGestureReceiver ( com.android.server.cameracovered.CameraBlackCoveredManager p0, android.content.Intent p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->handleHomeGestureReceiver(Landroid/content/Intent;)V */
return;
} // .end method
static void -$$Nest$minitCameraId ( com.android.server.cameracovered.CameraBlackCoveredManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->initCameraId()V */
return;
} // .end method
static com.android.server.cameracovered.CameraBlackCoveredManager ( ) {
/* .locals 2 */
/* .line 103 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 106 */
final String v1 = "com.miui.home"; // const-string v1, "com.miui.home"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 107 */
final String v1 = "com.miui.securitycenter"; // const-string v1, "com.miui.securitycenter"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 108 */
final String v1 = "com.android.systemui"; // const-string v1, "com.android.systemui"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 109 */
final String v1 = "com.miui.face"; // const-string v1, "com.miui.face"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 110 */
final String v1 = "light.sensor.service"; // const-string v1, "light.sensor.service"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 113 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 116 */
final String v1 = "com.tencent.mm"; // const-string v1, "com.tencent.mm"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 117 */
final String v1 = "com.tencent.mobileqq"; // const-string v1, "com.tencent.mobileqq"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 118 */
final String v1 = "com.skype.rover"; // const-string v1, "com.skype.rover"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 121 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 124 */
final String v1 = "com.android.camera"; // const-string v1, "com.android.camera"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 125 */
return;
} // .end method
public com.android.server.cameracovered.CameraBlackCoveredManager ( ) {
/* .locals 6 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "service" # Lcom/android/server/wm/WindowManagerInternal; */
/* .line 147 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 78 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
this.mFilter_state_change = v0;
/* .line 79 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
this.mFilter_launch_home = v0;
/* .line 82 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mFrontCameraID = v0;
/* .line 83 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mBackCameraID = v0;
/* .line 94 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mIsAddedBlackView:Z */
/* .line 95 */
/* iput-boolean v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mIsAddedHBMView:Z */
/* .line 96 */
/* iput-boolean v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mIsAddedMuraView:Z */
/* .line 100 */
/* iput v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mLastDisplayRotation:I */
/* .line 127 */
/* new-instance v1, Lcom/android/server/cameracovered/CameraBlackCoveredManager$1; */
/* invoke-direct {v1, p0}, Lcom/android/server/cameracovered/CameraBlackCoveredManager$1;-><init>(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)V */
this.mDisplayListener = v1;
/* .line 194 */
/* new-instance v1, Lcom/android/server/cameracovered/CameraBlackCoveredManager$2; */
/* invoke-direct {v1, p0}, Lcom/android/server/cameracovered/CameraBlackCoveredManager$2;-><init>(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)V */
this.mAvailabilityCallback = v1;
/* .line 311 */
/* new-instance v1, Lcom/android/server/cameracovered/CameraBlackCoveredManager$3; */
v2 = this.mHandler;
/* invoke-direct {v1, p0, v2}, Lcom/android/server/cameracovered/CameraBlackCoveredManager$3;-><init>(Lcom/android/server/cameracovered/CameraBlackCoveredManager;Landroid/os/Handler;)V */
this.mHideNotchObserver = v1;
/* .line 319 */
/* new-instance v1, Lcom/android/server/cameracovered/CameraBlackCoveredManager$4; */
/* invoke-direct {v1, p0}, Lcom/android/server/cameracovered/CameraBlackCoveredManager$4;-><init>(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)V */
this.mForegroundInfoChangeListener = v1;
/* .line 148 */
this.mContext = p1;
/* .line 149 */
this.mWindowManagerService = p2;
/* .line 150 */
/* const-string/jumbo v1, "window" */
(( android.content.Context ) p1 ).getSystemService ( v1 ); // invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v1, Landroid/view/WindowManager; */
this.mWindowManager = v1;
/* .line 151 */
final String v1 = "display"; // const-string v1, "display"
(( android.content.Context ) p1 ).getSystemService ( v1 ); // invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v1, Landroid/hardware/display/DisplayManager; */
this.mDisplayManager = v1;
/* .line 152 */
(( android.hardware.display.DisplayManager ) v1 ).getDisplay ( v0 ); // invoke-virtual {v1, v0}, Landroid/hardware/display/DisplayManager;->getDisplay(I)Landroid/view/Display;
this.mDisplay = v1;
/* .line 153 */
/* new-instance v1, Landroid/view/DisplayInfo; */
/* invoke-direct {v1}, Landroid/view/DisplayInfo;-><init>()V */
this.mDisplayInfo = v1;
/* .line 154 */
v2 = this.mDisplay;
(( android.view.Display ) v2 ).getDisplayInfo ( v1 ); // invoke-virtual {v2, v1}, Landroid/view/Display;->getDisplayInfo(Landroid/view/DisplayInfo;)Z
/* .line 155 */
v1 = this.mDisplayInfo;
/* iget v1, v1, Landroid/view/DisplayInfo;->rotation:I */
/* iput v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mLastDisplayRotation:I */
/* .line 156 */
/* new-instance v1, Landroid/os/HandlerThread; */
final String v2 = "camera_animation"; // const-string v2, "camera_animation"
int v3 = -2; // const/4 v3, -0x2
/* invoke-direct {v1, v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V */
/* .line 157 */
/* .local v1, "t":Landroid/os/HandlerThread; */
(( android.os.HandlerThread ) v1 ).start ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V
/* .line 158 */
/* new-instance v2, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H; */
(( android.os.HandlerThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v2, p0, v3}, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;-><init>(Lcom/android/server/cameracovered/CameraBlackCoveredManager;Landroid/os/Looper;)V */
this.mHandler = v2;
/* .line 159 */
/* new-instance v2, Lcom/android/server/cameracovered/CameraCircleBlackView; */
/* invoke-direct {v2, p1}, Lcom/android/server/cameracovered/CameraCircleBlackView;-><init>(Landroid/content/Context;)V */
this.mBlackCoveredView = v2;
/* .line 160 */
/* new-instance v2, Lcom/android/server/cameracovered/CameraHBMCoveredView; */
/* invoke-direct {v2, p1}, Lcom/android/server/cameracovered/CameraHBMCoveredView;-><init>(Landroid/content/Context;)V */
this.mHBMCoveredView = v2;
/* .line 161 */
v2 = this.mBlackCoveredView;
(( com.android.server.cameracovered.CameraCircleBlackView ) v2 ).setForceDarkAllowed ( v0 ); // invoke-virtual {v2, v0}, Lcom/android/server/cameracovered/CameraCircleBlackView;->setForceDarkAllowed(Z)V
/* .line 162 */
v2 = this.mHBMCoveredView;
(( com.android.server.cameracovered.CameraHBMCoveredView ) v2 ).setForceDarkAllowed ( v0 ); // invoke-virtual {v2, v0}, Lcom/android/server/cameracovered/CameraHBMCoveredView;->setForceDarkAllowed(Z)V
/* .line 163 */
/* new-instance v2, Lcom/android/server/cameracovered/CupMuraCoveredView; */
/* invoke-direct {v2, p1}, Lcom/android/server/cameracovered/CupMuraCoveredView;-><init>(Landroid/content/Context;)V */
this.mMuraCoveredView = v2;
/* .line 164 */
(( com.android.server.cameracovered.CupMuraCoveredView ) v2 ).setForceDarkAllowed ( v0 ); // invoke-virtual {v2, v0}, Lcom/android/server/cameracovered/CupMuraCoveredView;->setForceDarkAllowed(Z)V
/* .line 165 */
/* iput-boolean v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mIsAddedView:Z */
/* .line 166 */
v2 = this.mContext;
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 167 */
final String v3 = "force_black_v2"; // const-string v3, "force_black_v2"
android.provider.Settings$Global .getUriFor ( v3 );
v4 = this.mHideNotchObserver;
/* .line 166 */
int v5 = -1; // const/4 v5, -0x1
(( android.content.ContentResolver ) v2 ).registerContentObserver ( v3, v0, v4, v5 ); // invoke-virtual {v2, v3, v0, v4, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 168 */
v2 = this.mHideNotchObserver;
(( android.database.ContentObserver ) v2 ).onChange ( v0 ); // invoke-virtual {v2, v0}, Landroid/database/ContentObserver;->onChange(Z)V
/* .line 169 */
v0 = android.os.Build.DEVICE;
/* .line 170 */
/* .local v0, "deviceNM":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_0
final String v2 = "odin"; // const-string v2, "odin"
v2 = (( java.lang.String ) v0 ).equals ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_2 */
} // :cond_0
if ( v0 != null) { // if-eqz v0, :cond_1
final String v2 = "mona"; // const-string v2, "mona"
v2 = (( java.lang.String ) v0 ).equals ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_2 */
} // :cond_1
if ( v0 != null) { // if-eqz v0, :cond_3
/* const-string/jumbo v2, "zijin" */
v2 = (( java.lang.String ) v0 ).equals ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 171 */
} // :cond_2
v2 = com.android.server.cameracovered.CameraBlackCoveredManager.sBlackList;
final String v3 = "com.android.camera"; // const-string v3, "com.android.camera"
(( java.util.ArrayList ) v2 ).add ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 173 */
} // :cond_3
return;
} // .end method
private android.view.WindowManager$LayoutParams getWindowParam ( Integer p0, Integer p1 ) {
/* .locals 9 */
/* .param p1, "ori" # I */
/* .param p2, "type" # I */
/* .line 339 */
final String v0 = "persist.sys.cameraHeight"; // const-string v0, "persist.sys.cameraHeight"
final String v1 = "0"; // const-string v1, "0"
android.os.SystemProperties .get ( v0,v1 );
v0 = java.lang.Integer .parseInt ( v0 );
/* .line 340 */
/* .local v0, "h":I */
final String v2 = "persist.sys.cameraWidth"; // const-string v2, "persist.sys.cameraWidth"
android.os.SystemProperties .get ( v2,v1 );
v1 = java.lang.Integer .parseInt ( v1 );
/* .line 341 */
/* .local v1, "w":I */
if ( v0 != null) { // if-eqz v0, :cond_0
/* if-nez v1, :cond_1 */
/* .line 342 */
} // :cond_0
v2 = this.mContext;
(( android.content.Context ) v2 ).getResources ( ); // invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v3, 0x105000b */
v0 = (( android.content.res.Resources ) v2 ).getDimensionPixelSize ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I
/* .line 343 */
v2 = this.mContext;
(( android.content.Context ) v2 ).getResources ( ); // invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v3, 0x105000d */
v1 = (( android.content.res.Resources ) v2 ).getDimensionPixelSize ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I
/* .line 345 */
} // :cond_1
/* packed-switch p1, :pswitch_data_0 */
/* .line 351 */
/* :pswitch_0 */
/* move v2, v1 */
/* .line 352 */
/* .local v2, "tempW":I */
/* move v1, v0 */
/* .line 353 */
/* move v0, v2 */
/* .line 354 */
/* .line 348 */
} // .end local v2 # "tempW":I
/* :pswitch_1 */
/* nop */
/* .line 359 */
} // :goto_0
/* new-instance v2, Landroid/view/WindowManager$LayoutParams; */
/* const/16 v6, 0x7df */
/* const/16 v7, 0x1538 */
int v8 = -3; // const/4 v8, -0x3
/* move-object v3, v2 */
/* move v4, v1 */
/* move v5, v0 */
/* invoke-direct/range {v3 ..v8}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V */
/* .line 372 */
/* .local v2, "lp":Landroid/view/WindowManager$LayoutParams; */
/* iget v3, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I */
/* or-int/lit8 v3, v3, 0x40 */
/* iput v3, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I */
/* .line 373 */
/* iget v3, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I */
/* or-int/lit8 v3, v3, 0x10 */
/* iput v3, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I */
/* .line 374 */
int v3 = 1; // const/4 v3, 0x1
/* iput v3, v2, Landroid/view/WindowManager$LayoutParams;->layoutInDisplayCutoutMode:I */
/* .line 375 */
/* packed-switch p1, :pswitch_data_1 */
/* .line 386 */
/* :pswitch_2 */
/* const/16 v4, 0x35 */
/* iput v4, v2, Landroid/view/WindowManager$LayoutParams;->gravity:I */
/* .line 387 */
/* .line 383 */
/* :pswitch_3 */
/* const/16 v4, 0x55 */
/* iput v4, v2, Landroid/view/WindowManager$LayoutParams;->gravity:I */
/* .line 384 */
/* .line 380 */
/* :pswitch_4 */
/* const/16 v4, 0x53 */
/* iput v4, v2, Landroid/view/WindowManager$LayoutParams;->gravity:I */
/* .line 381 */
/* .line 377 */
/* :pswitch_5 */
/* const/16 v4, 0x33 */
/* iput v4, v2, Landroid/view/WindowManager$LayoutParams;->gravity:I */
/* .line 378 */
/* nop */
/* .line 392 */
} // :goto_1
/* if-ne p2, v3, :cond_2 */
/* .line 393 */
final String v3 = "cameraBlackCovered"; // const-string v3, "cameraBlackCovered"
(( android.view.WindowManager$LayoutParams ) v2 ).setTitle ( v3 ); // invoke-virtual {v2, v3}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V
/* .line 394 */
} // :cond_2
int v3 = 2; // const/4 v3, 0x2
/* if-ne p2, v3, :cond_3 */
/* .line 395 */
final String v3 = "cameraHBMCovered"; // const-string v3, "cameraHBMCovered"
(( android.view.WindowManager$LayoutParams ) v2 ).setTitle ( v3 ); // invoke-virtual {v2, v3}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V
/* .line 396 */
} // :cond_3
int v3 = 3; // const/4 v3, 0x3
/* if-ne p2, v3, :cond_4 */
/* .line 397 */
final String v3 = "cupMuraCovered"; // const-string v3, "cupMuraCovered"
(( android.view.WindowManager$LayoutParams ) v2 ).setTitle ( v3 ); // invoke-virtual {v2, v3}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V
/* .line 398 */
/* const/16 v3, 0x7d6 */
/* iput v3, v2, Landroid/view/WindowManager$LayoutParams;->type:I */
/* .line 400 */
} // :cond_4
} // :goto_2
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
/* :pswitch_0 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
/* :pswitch_data_1 */
/* .packed-switch 0x0 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
} // .end packed-switch
} // .end method
private void handleHomeGestureReceiver ( android.content.Intent p0 ) {
/* .locals 8 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .line 508 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "handleHomeGestureReceiver receive broadcast action is "; // const-string v1, "handleHomeGestureReceiver receive broadcast action is "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.content.Intent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "CameraBlackCoveredManager"; // const-string v1, "CameraBlackCoveredManager"
android.util.Slog .d ( v1,v0 );
/* .line 509 */
v0 = this.mBlackCoveredView;
if ( v0 != null) { // if-eqz v0, :cond_7
/* iget-boolean v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mIsAddedBlackView:Z */
/* if-nez v0, :cond_0 */
/* goto/16 :goto_4 */
/* .line 513 */
} // :cond_0
v0 = this.mWindowManagerService;
int v2 = 5; // const/4 v2, 0x5
(( com.android.server.wm.WindowManagerInternal ) v0 ).getTopStackPackageName ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/wm/WindowManagerInternal;->getTopStackPackageName(I)Ljava/lang/String;
/* .line 514 */
/* .local v0, "freeformPackageName":Ljava/lang/String; */
v3 = com.android.server.cameracovered.CameraBlackCoveredManager.sBroadcastBlackList;
v4 = this.mOccupiedPackage;
v3 = (( java.util.ArrayList ) v3 ).contains ( v4 ); // invoke-virtual {v3, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 515 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
v3 = this.mOccupiedPackage;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " is in sBroadcastBlackList"; // const-string v3, " is in sBroadcastBlackList"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 516 */
return;
/* .line 517 */
} // :cond_1
if ( v0 != null) { // if-eqz v0, :cond_2
v3 = this.mOccupiedPackage;
if ( v3 != null) { // if-eqz v3, :cond_2
v3 = (( java.lang.String ) v3 ).equals ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 518 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "The top app "; // const-string v3, "The top app "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " is freeform mode and using the camera service!"; // const-string v3, " is freeform mode and using the camera service!"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 519 */
return;
/* .line 521 */
} // :cond_2
(( android.content.Intent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;
v4 = (( java.lang.String ) v3 ).hashCode ( ); // invoke-virtual {v3}, Ljava/lang/String;->hashCode()I
/* sparse-switch v4, :sswitch_data_0 */
} // :cond_3
/* :sswitch_0 */
final String v4 = "com.miui.launch_home_from_hotkey"; // const-string v4, "com.miui.launch_home_from_hotkey"
v3 = (( java.lang.String ) v3 ).equals ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_3
int v3 = 1; // const/4 v3, 0x1
/* :sswitch_1 */
final String v4 = "com.miui.fullscreen_state_change"; // const-string v4, "com.miui.fullscreen_state_change"
v3 = (( java.lang.String ) v3 ).equals ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_3
int v3 = 0; // const/4 v3, 0x0
} // :goto_0
int v3 = -1; // const/4 v3, -0x1
} // :goto_1
int v4 = 4; // const/4 v4, 0x4
/* packed-switch v3, :pswitch_data_0 */
/* .line 539 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "handleHomeGestureReceiver action: "; // const-string v3, "handleHomeGestureReceiver action: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.content.Intent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v2 );
/* goto/16 :goto_3 */
/* .line 535 */
/* :pswitch_0 */
final String v2 = "handleHomeGestureReceiver action: com.miui.launch_home_from_hotkey"; // const-string v2, "handleHomeGestureReceiver action: com.miui.launch_home_from_hotkey"
android.util.Log .d ( v1,v2 );
/* .line 536 */
v1 = this.mHandler;
(( android.os.Handler ) v1 ).sendEmptyMessage ( v4 ); // invoke-virtual {v1, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 537 */
/* goto/16 :goto_3 */
/* .line 523 */
/* :pswitch_1 */
(( android.content.Intent ) p1 ).getExtras ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;
/* const-string/jumbo v5, "state" */
(( android.os.Bundle ) v3 ).getString ( v5 ); // invoke-virtual {v3, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 524 */
/* .local v3, "state":Ljava/lang/String; */
(( android.content.Intent ) p1 ).getExtras ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;
final String v6 = "package_name"; // const-string v6, "package_name"
(( android.os.Bundle ) v5 ).getString ( v6 ); // invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 525 */
/* .local v5, "packageName":Ljava/lang/String; */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "handleHomeGestureReceiver action: com.miui.fullscreen_state_changeEXTRA_STATE_NAME = "; // const-string v7, "handleHomeGestureReceiver action: com.miui.fullscreen_state_changeEXTRA_STATE_NAME = "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v3 ); // invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v6 );
/* .line 526 */
/* const-string/jumbo v1, "toRecents" */
v1 = (( java.lang.String ) v1 ).equals ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_6 */
/* const-string/jumbo v1, "toHome" */
v1 = (( java.lang.String ) v1 ).equals ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_6 */
/* const-string/jumbo v1, "toAnotherApp" */
v1 = (( java.lang.String ) v1 ).equals ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_6 */
final String v1 = "closeWindow"; // const-string v1, "closeWindow"
v1 = (( java.lang.String ) v1 ).equals ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 528 */
} // :cond_4
v1 = this.mOccupiedPackage;
if ( v1 != null) { // if-eqz v1, :cond_5
v1 = (( java.lang.String ) v1 ).equals ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_5
final String v1 = "quickSwitchAnimEnd"; // const-string v1, "quickSwitchAnimEnd"
v1 = (( java.lang.String ) v1 ).equals ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 529 */
v1 = this.mHandler;
(( android.os.Handler ) v1 ).sendEmptyMessage ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 531 */
} // :cond_5
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "wz_debug handleHomeGestureReceiver state = " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "TAG"; // const-string v2, "TAG"
android.util.Log .d ( v2,v1 );
/* .line 533 */
/* .line 527 */
} // :cond_6
} // :goto_2
v1 = this.mHandler;
(( android.os.Handler ) v1 ).sendEmptyMessage ( v4 ); // invoke-virtual {v1, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 542 */
} // .end local v3 # "state":Ljava/lang/String;
} // .end local v5 # "packageName":Ljava/lang/String;
} // :goto_3
return;
/* .line 510 */
} // .end local v0 # "freeformPackageName":Ljava/lang/String;
} // :cond_7
} // :goto_4
return;
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x35d637b1 -> :sswitch_1 */
/* 0x6a69ff62 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private void initCameraId ( ) {
/* .locals 10 */
/* .line 257 */
final String v0 = "CameraBlackCoveredManager"; // const-string v0, "CameraBlackCoveredManager"
try { // :try_start_0
v1 = this.mCameraManager;
(( android.hardware.camera2.CameraManager ) v1 ).getCameraIdList ( ); // invoke-virtual {v1}, Landroid/hardware/camera2/CameraManager;->getCameraIdList()[Ljava/lang/String;
/* .line 258 */
/* .local v1, "ids":[Ljava/lang/String; */
if ( v1 != null) { // if-eqz v1, :cond_3
/* array-length v2, v1 */
/* if-lez v2, :cond_3 */
/* .line 259 */
/* array-length v2, v1 */
int v3 = 0; // const/4 v3, 0x0
} // :goto_0
/* if-ge v3, v2, :cond_3 */
/* aget-object v4, v1, v3 */
/* .line 260 */
/* .local v4, "sid":Ljava/lang/String; */
java.lang.Integer .valueOf ( v4 );
v5 = (( java.lang.Integer ) v5 ).intValue ( ); // invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I
/* .line 261 */
/* .local v5, "id":I */
/* const/16 v6, 0x64 */
/* if-lt v5, v6, :cond_0 */
/* .line 262 */
/* .line 264 */
} // :cond_0
v6 = this.mCameraManager;
(( android.hardware.camera2.CameraManager ) v6 ).getCameraCharacteristics ( v4 ); // invoke-virtual {v6, v4}, Landroid/hardware/camera2/CameraManager;->getCameraCharacteristics(Ljava/lang/String;)Landroid/hardware/camera2/CameraCharacteristics;
/* .line 265 */
/* .local v6, "cc":Landroid/hardware/camera2/CameraCharacteristics; */
v7 = android.hardware.camera2.CameraCharacteristics.LENS_FACING;
(( android.hardware.camera2.CameraCharacteristics ) v6 ).get ( v7 ); // invoke-virtual {v6, v7}, Landroid/hardware/camera2/CameraCharacteristics;->get(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;
/* check-cast v7, Ljava/lang/Integer; */
v7 = (( java.lang.Integer ) v7 ).intValue ( ); // invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I
/* .line 266 */
/* .local v7, "facing":I */
/* if-nez v7, :cond_1 */
/* .line 267 */
v8 = this.mFrontCameraID;
java.lang.Integer .valueOf ( v5 );
/* .line 268 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v9, "wz_debug mFrontCameraID" */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v5 ); // invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v8 );
/* .line 269 */
} // :cond_1
int v8 = 1; // const/4 v8, 0x1
/* if-ne v7, v8, :cond_2 */
/* .line 270 */
v8 = this.mBackCameraID;
java.lang.Integer .valueOf ( v5 );
/* .line 271 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v9, "wz_debug mBackCameraID" */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v5 ); // invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v8 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 259 */
} // .end local v4 # "sid":Ljava/lang/String;
} // .end local v5 # "id":I
} // .end local v6 # "cc":Landroid/hardware/camera2/CameraCharacteristics;
} // .end local v7 # "facing":I
} // :cond_2
} // :goto_1
/* add-int/lit8 v3, v3, 0x1 */
/* .line 277 */
} // .end local v1 # "ids":[Ljava/lang/String;
} // :cond_3
/* .line 275 */
/* :catch_0 */
/* move-exception v1 */
/* .line 276 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v2 = "Can\'t initCameraId"; // const-string v2, "Can\'t initCameraId"
android.util.Slog .d ( v0,v2 );
/* .line 278 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_2
return;
} // .end method
/* # virtual methods */
public Boolean canShowBlackCovered ( Boolean p0 ) {
/* .locals 5 */
/* .param p1, "forceShow" # Z */
/* .line 304 */
int v0 = 1; // const/4 v0, 0x1
int v1 = 0; // const/4 v1, 0x0
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 305 */
/* iget-boolean v2, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mHideNotch:Z */
/* if-nez v2, :cond_0 */
v2 = com.android.server.cameracovered.CameraBlackCoveredManager.sForceShowList;
v3 = this.mOccupiedPackage;
v2 = (( java.util.ArrayList ) v2 ).contains ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
} // :cond_0
/* move v0, v1 */
} // :goto_0
/* .line 307 */
} // :cond_1
/* const-string/jumbo v2, "vendor.debug.camera.pkgname" */
final String v3 = ""; // const-string v3, ""
android.os.SystemProperties .get ( v2,v3 );
/* .line 308 */
/* .local v2, "cameraPackageFromProp":Ljava/lang/String; */
/* iget-boolean v3, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mHideNotch:Z */
/* if-nez v3, :cond_2 */
v3 = com.android.server.cameracovered.CameraBlackCoveredManager.sBlackList;
v4 = this.mOccupiedPackage;
v4 = (( java.util.ArrayList ) v3 ).contains ( v4 ); // invoke-virtual {v3, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* if-nez v4, :cond_2 */
v3 = (( java.util.ArrayList ) v3 ).contains ( v2 ); // invoke-virtual {v3, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* if-nez v3, :cond_2 */
} // :cond_2
/* move v0, v1 */
} // :goto_1
} // .end method
public void cupMuraCoveredAnimation ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "type" # I */
/* .line 241 */
int v0 = 1; // const/4 v0, 0x1
/* if-ne p1, v0, :cond_0 */
/* iget-boolean v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mHideNotch:Z */
/* if-nez v0, :cond_0 */
/* .line 242 */
android.os.Message .obtain ( );
/* .line 243 */
/* .local v0, "msg":Landroid/os/Message; */
int v1 = 6; // const/4 v1, 0x6
/* iput v1, v0, Landroid/os/Message;->what:I */
/* .line 244 */
v1 = this.mDisplay;
v2 = this.mDisplayInfo;
(( android.view.Display ) v1 ).getDisplayInfo ( v2 ); // invoke-virtual {v1, v2}, Landroid/view/Display;->getDisplayInfo(Landroid/view/DisplayInfo;)Z
/* .line 245 */
v1 = this.mDisplayInfo;
/* iget v1, v1, Landroid/view/DisplayInfo;->rotation:I */
/* iput v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mLastDisplayRotation:I */
/* .line 246 */
java.lang.Integer .valueOf ( v1 );
this.obj = v1;
/* .line 247 */
v1 = this.mHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 248 */
} // .end local v0 # "msg":Landroid/os/Message;
} // :cond_0
/* if-nez p1, :cond_1 */
/* .line 249 */
v0 = this.mHandler;
int v1 = 7; // const/4 v1, 0x7
(( android.os.Handler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 251 */
} // :cond_1
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "cupMuraCoveredAnimation "; // const-string v1, "cupMuraCoveredAnimation "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " error!"; // const-string v1, " error!"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "CameraBlackCoveredManager"; // const-string v1, "CameraBlackCoveredManager"
android.util.Slog .d ( v1,v0 );
/* .line 253 */
} // :goto_0
return;
} // .end method
public void hbmCoveredAnimation ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "type" # I */
/* .line 226 */
int v0 = 1; // const/4 v0, 0x1
/* if-ne p1, v0, :cond_0 */
/* iget-boolean v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mHideNotch:Z */
/* if-nez v0, :cond_0 */
/* .line 227 */
android.os.Message .obtain ( );
/* .line 228 */
/* .local v0, "msg":Landroid/os/Message; */
int v1 = 2; // const/4 v1, 0x2
/* iput v1, v0, Landroid/os/Message;->what:I */
/* .line 229 */
v1 = this.mDisplay;
v2 = this.mDisplayInfo;
(( android.view.Display ) v1 ).getDisplayInfo ( v2 ); // invoke-virtual {v1, v2}, Landroid/view/Display;->getDisplayInfo(Landroid/view/DisplayInfo;)Z
/* .line 230 */
v1 = this.mDisplayInfo;
/* iget v1, v1, Landroid/view/DisplayInfo;->rotation:I */
/* iput v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mLastDisplayRotation:I */
/* .line 231 */
java.lang.Integer .valueOf ( v1 );
this.obj = v1;
/* .line 232 */
v1 = this.mHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 233 */
} // .end local v0 # "msg":Landroid/os/Message;
} // :cond_0
/* if-nez p1, :cond_1 */
/* .line 234 */
v0 = this.mHandler;
int v1 = 3; // const/4 v1, 0x3
(( android.os.Handler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 236 */
} // :cond_1
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "hbmCoveredAnimation for HBM the type "; // const-string v1, "hbmCoveredAnimation for HBM the type "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " error!"; // const-string v1, " error!"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "CameraBlackCoveredManager"; // const-string v1, "CameraBlackCoveredManager"
android.util.Slog .e ( v1,v0 );
/* .line 238 */
} // :goto_0
return;
} // .end method
public void hideCoveredBlackView ( ) {
/* .locals 2 */
/* .line 281 */
/* iget-boolean v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mIsAddedBlackView:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 282 */
v0 = this.mHandler;
int v1 = 4; // const/4 v1, 0x4
(( android.os.Handler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 284 */
} // :cond_0
return;
} // .end method
public void registerForegroundInfoChangeListener ( ) {
/* .locals 1 */
/* .line 334 */
v0 = this.mForegroundInfoChangeListener;
miui.process.ProcessManager .unregisterForegroundInfoListener ( v0 );
/* .line 335 */
v0 = this.mForegroundInfoChangeListener;
miui.process.ProcessManager .registerForegroundInfoListener ( v0 );
/* .line 336 */
return;
} // .end method
public void setCoverdPackageName ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 191 */
this.mOccupiedPackage = p1;
/* .line 192 */
return;
} // .end method
public void startCameraAnimation ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "forceShow" # Z */
/* .line 287 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 288 */
v0 = this.mDisplay;
v1 = this.mDisplayInfo;
(( android.view.Display ) v0 ).getDisplayInfo ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/Display;->getDisplayInfo(Landroid/view/DisplayInfo;)Z
/* .line 290 */
} // :cond_0
v0 = (( com.android.server.cameracovered.CameraBlackCoveredManager ) p0 ).canShowBlackCovered ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->canShowBlackCovered(Z)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 291 */
android.os.Message .obtain ( );
/* .line 292 */
/* .local v0, "msg":Landroid/os/Message; */
int v1 = 0; // const/4 v1, 0x0
/* iput v1, v0, Landroid/os/Message;->what:I */
/* .line 293 */
v1 = this.mDisplayInfo;
/* iget v1, v1, Landroid/view/DisplayInfo;->rotation:I */
/* iput v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mLastDisplayRotation:I */
/* .line 294 */
java.lang.Integer .valueOf ( v1 );
this.obj = v1;
/* .line 295 */
v1 = this.mHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 297 */
} // .end local v0 # "msg":Landroid/os/Message;
} // :cond_1
return;
} // .end method
public void stopCameraAnimation ( ) {
/* .locals 2 */
/* .line 300 */
v0 = this.mHandler;
int v1 = 1; // const/4 v1, 0x1
(( android.os.Handler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 301 */
return;
} // .end method
public void systemReady ( ) {
/* .locals 8 */
/* .line 176 */
v0 = this.mContext;
final String v1 = "camera"; // const-string v1, "camera"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/hardware/camera2/CameraManager; */
this.mCameraManager = v0;
/* .line 177 */
v1 = this.mAvailabilityCallback;
v2 = this.mHandler;
(( android.hardware.camera2.CameraManager ) v0 ).registerAvailabilityCallback ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/hardware/camera2/CameraManager;->registerAvailabilityCallback(Landroid/hardware/camera2/CameraManager$AvailabilityCallback;Landroid/os/Handler;)V
/* .line 178 */
(( com.android.server.cameracovered.CameraBlackCoveredManager ) p0 ).registerForegroundInfoChangeListener ( ); // invoke-virtual {p0}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->registerForegroundInfoChangeListener()V
/* .line 179 */
/* invoke-direct {p0}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->initCameraId()V */
/* .line 181 */
/* new-instance v0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$HomeGestureReceiver; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, p0, v1}, Lcom/android/server/cameracovered/CameraBlackCoveredManager$HomeGestureReceiver;-><init>(Lcom/android/server/cameracovered/CameraBlackCoveredManager;Lcom/android/server/cameracovered/CameraBlackCoveredManager$HomeGestureReceiver-IA;)V */
this.mHomeGestureReceiver = v0;
/* .line 182 */
v0 = this.mFilter_state_change;
final String v1 = "com.miui.fullscreen_state_change"; // const-string v1, "com.miui.fullscreen_state_change"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 183 */
v0 = this.mContext;
v1 = this.mHomeGestureReceiver;
v2 = this.mFilter_state_change;
(( android.content.Context ) v0 ).registerReceiver ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 185 */
v0 = this.mFilter_launch_home;
final String v1 = "com.miui.launch_home_from_hotkey"; // const-string v1, "com.miui.launch_home_from_hotkey"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 186 */
v2 = this.mContext;
v3 = this.mHomeGestureReceiver;
v4 = android.os.UserHandle.CURRENT;
v5 = this.mFilter_launch_home;
final String v6 = "miui.permission.USE_INTERNAL_GENERAL_API"; // const-string v6, "miui.permission.USE_INTERNAL_GENERAL_API"
int v7 = 0; // const/4 v7, 0x0
/* invoke-virtual/range {v2 ..v7}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent; */
/* .line 188 */
return;
} // .end method
