public class com.android.server.cameracovered.CameraCircleBlackView extends android.widget.ImageView {
	 /* .source "CameraCircleBlackView.java" */
	 /* # instance fields */
	 android.util.AttributeSet mAttrs;
	 private android.content.Context mContext;
	 private Float mInternalRadio;
	 private android.graphics.Paint mPaint;
	 private Integer mViewCenterX;
	 private Integer mViewCenterY;
	 /* # direct methods */
	 public com.android.server.cameracovered.CameraCircleBlackView ( ) {
		 /* .locals 1 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .line 24 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* invoke-direct {p0, p1, v0}, Lcom/android/server/cameracovered/CameraCircleBlackView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V */
		 /* .line 25 */
		 return;
	 } // .end method
	 public com.android.server.cameracovered.CameraCircleBlackView ( ) {
		 /* .locals 1 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .param p2, "attrs" # Landroid/util/AttributeSet; */
		 /* .line 28 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* invoke-direct {p0, p1, p2, v0}, Lcom/android/server/cameracovered/CameraCircleBlackView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V */
		 /* .line 29 */
		 return;
	 } // .end method
	 public com.android.server.cameracovered.CameraCircleBlackView ( ) {
		 /* .locals 1 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .param p2, "attrs" # Landroid/util/AttributeSet; */
		 /* .param p3, "defStyleAttr" # I */
		 /* .line 32 */
		 /* invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V */
		 /* .line 19 */
		 /* new-instance v0, Landroid/graphics/Paint; */
		 /* invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V */
		 this.mPaint = v0;
		 /* .line 33 */
		 this.mContext = p1;
		 /* .line 34 */
		 this.mAttrs = p2;
		 /* .line 35 */
		 (( com.android.server.cameracovered.CameraCircleBlackView ) p0 ).init ( ); // invoke-virtual {p0}, Lcom/android/server/cameracovered/CameraCircleBlackView;->init()V
		 /* .line 36 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public void draw ( android.graphics.Canvas p0 ) {
		 /* .locals 4 */
		 /* .param p1, "canvas" # Landroid/graphics/Canvas; */
		 /* .line 74 */
		 /* invoke-super {p0, p1}, Landroid/widget/ImageView;->draw(Landroid/graphics/Canvas;)V */
		 /* .line 75 */
		 /* iget v0, p0, Lcom/android/server/cameracovered/CameraCircleBlackView;->mViewCenterX:I */
		 /* int-to-float v0, v0 */
		 /* iget v1, p0, Lcom/android/server/cameracovered/CameraCircleBlackView;->mViewCenterY:I */
		 /* int-to-float v1, v1 */
		 /* iget v2, p0, Lcom/android/server/cameracovered/CameraCircleBlackView;->mInternalRadio:F */
		 v3 = this.mPaint;
		 (( android.graphics.Canvas ) p1 ).drawCircle ( v0, v1, v2, v3 ); // invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V
		 /* .line 76 */
		 return;
	 } // .end method
	 public void init ( ) {
		 /* .locals 2 */
		 /* .line 56 */
		 /* new-instance v0, Landroid/graphics/Paint; */
		 int v1 = 1; // const/4 v1, 0x1
		 /* invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V */
		 this.mPaint = v0;
		 /* .line 57 */
		 (( android.graphics.Paint ) v0 ).setAntiAlias ( v1 ); // invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V
		 /* .line 58 */
		 v0 = this.mPaint;
		 v1 = android.graphics.Paint$Style.FILL_AND_STROKE;
		 (( android.graphics.Paint ) v0 ).setStyle ( v1 ); // invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V
		 /* .line 59 */
		 v0 = this.mPaint;
		 /* const/high16 v1, -0x1000000 */
		 (( android.graphics.Paint ) v0 ).setColor ( v1 ); // invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V
		 /* .line 60 */
		 return;
	 } // .end method
	 protected void onLayout ( Boolean p0, Integer p1, Integer p2, Integer p3, Integer p4 ) {
		 /* .locals 3 */
		 /* .param p1, "changed" # Z */
		 /* .param p2, "left" # I */
		 /* .param p3, "top" # I */
		 /* .param p4, "right" # I */
		 /* .param p5, "bottom" # I */
		 /* .line 64 */
		 /* invoke-super/range {p0 ..p5}, Landroid/widget/ImageView;->onLayout(ZIIII)V */
		 /* .line 66 */
		 v0 = 		 (( com.android.server.cameracovered.CameraCircleBlackView ) p0 ).getMeasuredWidth ( ); // invoke-virtual {p0}, Lcom/android/server/cameracovered/CameraCircleBlackView;->getMeasuredWidth()I
		 /* .line 67 */
		 /* .local v0, "viewWidth":I */
		 v1 = 		 (( com.android.server.cameracovered.CameraCircleBlackView ) p0 ).getMeasuredHeight ( ); // invoke-virtual {p0}, Lcom/android/server/cameracovered/CameraCircleBlackView;->getMeasuredHeight()I
		 /* .line 68 */
		 /* .local v1, "viewHeight":I */
		 /* div-int/lit8 v2, v0, 0x2 */
		 /* iput v2, p0, Lcom/android/server/cameracovered/CameraCircleBlackView;->mViewCenterX:I */
		 /* .line 69 */
		 /* div-int/lit8 v2, v1, 0x2 */
		 /* iput v2, p0, Lcom/android/server/cameracovered/CameraCircleBlackView;->mViewCenterY:I */
		 /* .line 70 */
		 return;
	 } // .end method
	 public void setRadius ( Float p0 ) {
		 /* .locals 0 */
		 /* .param p1, "radius" # F */
		 /* .line 39 */
		 /* iput p1, p0, Lcom/android/server/cameracovered/CameraCircleBlackView;->mInternalRadio:F */
		 /* .line 40 */
		 return;
	 } // .end method
	 public void setRadiusColor ( Integer p0 ) {
		 /* .locals 2 */
		 /* .param p1, "color" # I */
		 /* .line 43 */
		 /* const/high16 v0, -0x1000000 */
		 /* packed-switch p1, :pswitch_data_0 */
		 /* .line 51 */
		 v1 = this.mPaint;
		 (( android.graphics.Paint ) v1 ).setColor ( v0 ); // invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V
		 /* .line 48 */
		 /* :pswitch_0 */
		 v1 = this.mPaint;
		 (( android.graphics.Paint ) v1 ).setColor ( v0 ); // invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V
		 /* .line 49 */
		 /* .line 45 */
		 /* :pswitch_1 */
		 v0 = this.mPaint;
		 /* const v1, -0xff0100 */
		 (( android.graphics.Paint ) v0 ).setColor ( v1 ); // invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V
		 /* .line 46 */
		 /* nop */
		 /* .line 53 */
	 } // :goto_0
	 return;
	 /* nop */
	 /* :pswitch_data_0 */
	 /* .packed-switch 0x1 */
	 /* :pswitch_1 */
	 /* :pswitch_0 */
} // .end packed-switch
} // .end method
