class com.android.server.cameracovered.CameraBlackCoveredManager$1 implements android.hardware.display.DisplayManager$DisplayListener {
	 /* .source "CameraBlackCoveredManager.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/cameracovered/CameraBlackCoveredManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.cameracovered.CameraBlackCoveredManager this$0; //synthetic
/* # direct methods */
 com.android.server.cameracovered.CameraBlackCoveredManager$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/cameracovered/CameraBlackCoveredManager; */
/* .line 127 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onDisplayAdded ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "displayId" # I */
/* .line 130 */
return;
} // .end method
public void onDisplayChanged ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "displayId" # I */
/* .line 138 */
v0 = this.this$0;
com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmDisplay ( v0 );
v1 = this.this$0;
com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmDisplayInfo ( v1 );
(( android.view.Display ) v0 ).getDisplayInfo ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/Display;->getDisplayInfo(Landroid/view/DisplayInfo;)Z
/* .line 139 */
v0 = this.this$0;
v0 = com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmLastDisplayRotation ( v0 );
v1 = this.this$0;
com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmDisplayInfo ( v1 );
/* iget v1, v1, Landroid/view/DisplayInfo;->rotation:I */
/* if-eq v0, v1, :cond_0 */
/* .line 140 */
final String v0 = "CameraBlackCoveredManager"; // const-string v0, "CameraBlackCoveredManager"
final String v1 = "onDisplayChanged"; // const-string v1, "onDisplayChanged"
android.util.Slog .d ( v0,v1 );
/* .line 141 */
v0 = this.this$0;
(( com.android.server.cameracovered.CameraBlackCoveredManager ) v0 ).stopCameraAnimation ( ); // invoke-virtual {v0}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->stopCameraAnimation()V
/* .line 142 */
v0 = this.this$0;
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.cameracovered.CameraBlackCoveredManager ) v0 ).startCameraAnimation ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->startCameraAnimation(Z)V
/* .line 144 */
} // :cond_0
return;
} // .end method
public void onDisplayRemoved ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "displayId" # I */
/* .line 134 */
return;
} // .end method
