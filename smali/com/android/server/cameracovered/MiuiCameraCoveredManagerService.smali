.class public final Lcom/android/server/cameracovered/MiuiCameraCoveredManagerService;
.super Landroid/cameracovered/IMiuiCameraCoveredManager$Stub;
.source "MiuiCameraCoveredManagerService.java"

# interfaces
.implements Lcom/miui/app/MiuiCameraCoveredManagerServiceInternal;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/cameracovered/MiuiCameraCoveredManagerService$Lifecycle;
    }
.end annotation


# static fields
.field private static Packages:Ljava/lang/String; = null

.field private static final SCREEN_ANTI_BURN_DEFAULT_DATA:Ljava/lang/String; = "{\"com.ss.android.ugc.aweme\": {    \"mode\": 1000,    \"layout\": [\"MainBottomTab\"],    \"enable_offset\": false,    \"dim_ratio\": \"0.5f\"},\"com.ss.android.ugc.aweme.lite\": {    \"mode\": 1000,    \"layout\": [\"MainBottomTab\"],    \"enable_offset\": false,    \"dim_ratio\": \"0.5f\"}}"

.field public static final SERVICE_NAME:Ljava/lang/String; = "camera_covered_service"

.field private static final SRCREEN_ANTI_BURN_MODULE:Ljava/lang/String; = "screen_anti_burn"

.field public static final TAG:Ljava/lang/String; = "CameraCoveredManager"

.field private static Test_Dynamic_Cutout:Ljava/lang/String;


# instance fields
.field private mCameraBlackCoveredManager:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

.field private mContext:Landroid/content/Context;

.field private mScreenAntiBurnData:Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

.field private mWindowManagerService:Lcom/android/server/wm/WindowManagerInternal;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 31
    const-string/jumbo v0, "testDynamicCutout"

    sput-object v0, Lcom/android/server/cameracovered/MiuiCameraCoveredManagerService;->Test_Dynamic_Cutout:Ljava/lang/String;

    .line 32
    const-string v0, "packages"

    sput-object v0, Lcom/android/server/cameracovered/MiuiCameraCoveredManagerService;->Packages:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 48
    invoke-direct {p0}, Landroid/cameracovered/IMiuiCameraCoveredManager$Stub;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/android/server/cameracovered/MiuiCameraCoveredManagerService;->mContext:Landroid/content/Context;

    .line 50
    const-class v0, Lcom/miui/app/MiuiCameraCoveredManagerServiceInternal;

    invoke-static {v0, p0}, Lcom/android/server/LocalServices;->addService(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 51
    const-class v0, Lcom/android/server/wm/WindowManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/WindowManagerInternal;

    iput-object v0, p0, Lcom/android/server/cameracovered/MiuiCameraCoveredManagerService;->mWindowManagerService:Lcom/android/server/wm/WindowManagerInternal;

    .line 53
    invoke-virtual {p0}, Lcom/android/server/cameracovered/MiuiCameraCoveredManagerService;->initScreenAntiBurnData()V

    .line 54
    invoke-direct {p0}, Lcom/android/server/cameracovered/MiuiCameraCoveredManagerService;->registerScreenAntiBurnDataObserver()V

    .line 56
    return-void
.end method

.method private registerScreenAntiBurnDataObserver()V
    .locals 4

    .line 147
    iget-object v0, p0, Lcom/android/server/cameracovered/MiuiCameraCoveredManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 148
    invoke-static {}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataNotifyUri()Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lcom/android/server/cameracovered/MiuiCameraCoveredManagerService$1;

    .line 149
    invoke-static {}, Lcom/android/server/MiuiBgThread;->getHandler()Landroid/os/Handler;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/android/server/cameracovered/MiuiCameraCoveredManagerService$1;-><init>(Lcom/android/server/cameracovered/MiuiCameraCoveredManagerService;Landroid/os/Handler;)V

    .line 147
    const/4 v3, 0x1

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 156
    return-void
.end method


# virtual methods
.method public addCoveredBlackView()V
    .locals 2

    .line 97
    iget-object v0, p0, Lcom/android/server/cameracovered/MiuiCameraCoveredManagerService;->mCameraBlackCoveredManager:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    if-eqz v0, :cond_0

    .line 98
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->startCameraAnimation(Z)V

    .line 100
    :cond_0
    return-void
.end method

.method public cupMuraCoveredAnimation(I)V
    .locals 1
    .param p1, "type"    # I

    .line 85
    iget-object v0, p0, Lcom/android/server/cameracovered/MiuiCameraCoveredManagerService;->mCameraBlackCoveredManager:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    if-eqz v0, :cond_0

    .line 86
    invoke-virtual {v0, p1}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->cupMuraCoveredAnimation(I)V

    .line 88
    :cond_0
    return-void
.end method

.method public getScreenAntiBurnData(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .line 168
    iget-object v0, p0, Lcom/android/server/cameracovered/MiuiCameraCoveredManagerService;->mScreenAntiBurnData:Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hbmCoveredAnimation(I)V
    .locals 1
    .param p1, "type"    # I

    .line 91
    iget-object v0, p0, Lcom/android/server/cameracovered/MiuiCameraCoveredManagerService;->mCameraBlackCoveredManager:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    if-eqz v0, :cond_0

    .line 92
    invoke-virtual {v0, p1}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->hbmCoveredAnimation(I)V

    .line 94
    :cond_0
    return-void
.end method

.method public hideCoveredBlackView()V
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/android/server/cameracovered/MiuiCameraCoveredManagerService;->mCameraBlackCoveredManager:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    if-eqz v0, :cond_0

    .line 80
    invoke-virtual {v0}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->hideCoveredBlackView()V

    .line 82
    :cond_0
    return-void
.end method

.method public initScreenAntiBurnData()V
    .locals 4

    .line 159
    iget-object v0, p0, Lcom/android/server/cameracovered/MiuiCameraCoveredManagerService;->mContext:Landroid/content/Context;

    .line 160
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 159
    const-string v1, "screen_anti_burn"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v2, v3}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataSingle(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/cameracovered/MiuiCameraCoveredManagerService;->mScreenAntiBurnData:Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    .line 161
    if-nez v0, :cond_0

    .line 162
    const-string v0, "CameraCoveredManager"

    const-string/jumbo v1, "without screen_anti_burn data."

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    new-instance v0, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    const-string/jumbo v1, "{\"com.ss.android.ugc.aweme\": {    \"mode\": 1000,    \"layout\": [\"MainBottomTab\"],    \"enable_offset\": false,    \"dim_ratio\": \"0.5f\"},\"com.ss.android.ugc.aweme.lite\": {    \"mode\": 1000,    \"layout\": [\"MainBottomTab\"],    \"enable_offset\": false,    \"dim_ratio\": \"0.5f\"}}"

    invoke-direct {v0, v1}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/cameracovered/MiuiCameraCoveredManagerService;->mScreenAntiBurnData:Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    .line 165
    :cond_0
    return-void
.end method

.method public needDisableCutout(Ljava/lang/String;)Z
    .locals 9
    .param p1, "packageName"    # Ljava/lang/String;

    .line 103
    const-string v0, "CameraCoveredManager"

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 105
    .local v1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 107
    .local v2, "ident":J
    const/4 v4, 0x0

    :try_start_0
    iget-object v5, p0, Lcom/android/server/cameracovered/MiuiCameraCoveredManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    if-eqz v5, :cond_4

    if-nez p1, :cond_0

    goto :goto_1

    .line 111
    :cond_0
    iget-object v5, p0, Lcom/android/server/cameracovered/MiuiCameraCoveredManagerService;->mContext:Landroid/content/Context;

    .line 112
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/android/server/cameracovered/MiuiCameraCoveredManagerService;->Test_Dynamic_Cutout:Ljava/lang/String;

    sget-object v7, Lcom/android/server/cameracovered/MiuiCameraCoveredManagerService;->Packages:Ljava/lang/String;

    .line 111
    const/4 v8, 0x0

    invoke-static {v5, v6, v7, v8}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 113
    .local v5, "data":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 114
    new-instance v6, Lorg/json/JSONArray;

    invoke-direct {v6, v5}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 115
    .local v6, "apps":Lorg/json/JSONArray;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "needDisableCutout: packageName: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v7, v8, :cond_2

    .line 117
    invoke-virtual {v6, v7}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v8, :cond_1

    .line 118
    nop

    .line 126
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 118
    const/4 v0, 0x1

    return v0

    .line 116
    :cond_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 121
    .end local v7    # "i":I
    :cond_2
    nop

    .line 126
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 121
    return v4

    .line 126
    .end local v5    # "data":Ljava/lang/String;
    .end local v6    # "apps":Lorg/json/JSONArray;
    :cond_3
    goto :goto_2

    .line 108
    :cond_4
    :goto_1
    :try_start_1
    const-string v5, "mContext.getContentResolver() or packageName is null"

    invoke-static {v0, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 109
    nop

    .line 126
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 109
    return v4

    .line 126
    :catchall_0
    move-exception v0

    goto :goto_3

    .line 123
    :catch_0
    move-exception v5

    .line 124
    .local v5, "e":Lorg/json/JSONException;
    :try_start_2
    const-string v6, "exception when updateForceRestartAppList: "

    invoke-static {v0, v6, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 126
    nop

    .end local v5    # "e":Lorg/json/JSONException;
    :goto_2
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 127
    nop

    .line 128
    return v4

    .line 126
    :goto_3
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 127
    throw v0
.end method

.method public setCoveredPackageName(Ljava/lang/String;)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 73
    iget-object v0, p0, Lcom/android/server/cameracovered/MiuiCameraCoveredManagerService;->mCameraBlackCoveredManager:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    if-eqz v0, :cond_0

    .line 74
    invoke-virtual {v0, p1}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->setCoverdPackageName(Ljava/lang/String;)V

    .line 76
    :cond_0
    return-void
.end method

.method public systemBooted()V
    .locals 4

    .line 61
    const-string v0, "CameraCoveredManager"

    :try_start_0
    sget-boolean v1, Lmiui/os/DeviceFeature;->SUPPORT_FRONTCAMERA_CIRCLE_BLACK:Z

    if-eqz v1, :cond_0

    .line 62
    new-instance v1, Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    iget-object v2, p0, Lcom/android/server/cameracovered/MiuiCameraCoveredManagerService;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/server/cameracovered/MiuiCameraCoveredManagerService;->mWindowManagerService:Lcom/android/server/wm/WindowManagerInternal;

    invoke-direct {v1, v2, v3}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;-><init>(Landroid/content/Context;Lcom/android/server/wm/WindowManagerInternal;)V

    iput-object v1, p0, Lcom/android/server/cameracovered/MiuiCameraCoveredManagerService;->mCameraBlackCoveredManager:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    .line 64
    invoke-virtual {v1}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->systemReady()V

    .line 65
    const-string/jumbo v1, "systemBooted: current device need camera covered feature!"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 69
    :cond_0
    goto :goto_0

    .line 67
    :catch_0
    move-exception v1

    .line 68
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method
