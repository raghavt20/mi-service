.class public Lcom/android/server/cameracovered/CameraBlackCoveredManager;
.super Ljava/lang/Object;
.source "CameraBlackCoveredManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;,
        Lcom/android/server/cameracovered/CameraBlackCoveredManager$HomeGestureReceiver;
    }
.end annotation


# static fields
.field public static final ACTION_FULLSCREEN_STATE_CHANGE:Ljava/lang/String; = "com.miui.fullscreen_state_change"

.field public static final ACTION_LAUNCH_HOME_FROM_HOTKEY:Ljava/lang/String; = "com.miui.launch_home_from_hotkey"

.field private static final COVERED_BLACK_TYPE:I = 0x1

.field private static final COVERED_HBM_TYPE:I = 0x2

.field private static final COVERED_MURA_TYPE:I = 0x3

.field private static final CUP_MURA_COVER_ENTER:I = 0x6

.field private static final CUP_MURA_COVER_EXIT:I = 0x7

.field public static final EXTRA_PACKAGE_NAME:Ljava/lang/String; = "package_name"

.field public static final EXTRA_STATE_NAME:Ljava/lang/String; = "state"

.field private static final FORCE_BLACK_V2:Ljava/lang/String; = "force_black_v2"

.field private static final FRONT_CAMERA_CLOSE:I = 0x1

.field private static final FRONT_CAMERA_HIDE:I = 0x4

.field private static final FRONT_CAMERA_OPEN:I = 0x0

.field private static final FRONT_CAMERA_SHOW:I = 0x5

.field private static final HBM_ENTER:I = 0x2

.field private static final HBM_EXIT:I = 0x3

.field private static final PERMISSION_INTERNAL_GENERAL_API:Ljava/lang/String; = "miui.permission.USE_INTERNAL_GENERAL_API"

.field public static final STATE_CLOSE_WINDOW:Ljava/lang/String; = "closeWindow"

.field public static final STATE_QUICK_SWITCH_ANIM_END:Ljava/lang/String; = "quickSwitchAnimEnd"

.field public static final STATE_TO_ANOTHER_APP:Ljava/lang/String; = "toAnotherApp"

.field public static final STATE_TO_HOME:Ljava/lang/String; = "toHome"

.field public static final STATE_TO_RECENTS:Ljava/lang/String; = "toRecents"

.field public static final TAG:Ljava/lang/String; = "CameraBlackCoveredManager"

.field private static final VIRTUAL_CAMERA_BOUNDARY:I = 0x64

.field public static final sBlackList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final sBroadcastBlackList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final sForceShowList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAvailabilityCallback:Landroid/hardware/camera2/CameraManager$AvailabilityCallback;

.field private mBackCameraID:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mBlackCoveredView:Lcom/android/server/cameracovered/CameraCircleBlackView;

.field private mCameraManager:Landroid/hardware/camera2/CameraManager;

.field private mContext:Landroid/content/Context;

.field private mDisplay:Landroid/view/Display;

.field private mDisplayInfo:Landroid/view/DisplayInfo;

.field private final mDisplayListener:Landroid/hardware/display/DisplayManager$DisplayListener;

.field private mDisplayManager:Landroid/hardware/display/DisplayManager;

.field private mFilter_launch_home:Landroid/content/IntentFilter;

.field private mFilter_state_change:Landroid/content/IntentFilter;

.field private mForegroundInfoChangeListener:Lmiui/process/IForegroundInfoListener$Stub;

.field private mForegroundPackage:Ljava/lang/String;

.field private mFrontCameraID:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mHBMCoveredView:Lcom/android/server/cameracovered/CameraHBMCoveredView;

.field private mHandler:Landroid/os/Handler;

.field private mHideNotch:Z

.field private mHideNotchObserver:Landroid/database/ContentObserver;

.field private mHomeGestureReceiver:Lcom/android/server/cameracovered/CameraBlackCoveredManager$HomeGestureReceiver;

.field private mIsAddedBlackView:Z

.field private mIsAddedHBMView:Z

.field private mIsAddedMuraView:Z

.field private mIsAddedView:Z

.field private mLastDisplayRotation:I

.field private mLastUnavailableCameraId:I

.field private mMuraCoveredView:Lcom/android/server/cameracovered/CupMuraCoveredView;

.field private mOccupiedPackage:Ljava/lang/String;

.field private mWindowManager:Landroid/view/WindowManager;

.field private mWindowManagerService:Lcom/android/server/wm/WindowManagerInternal;


# direct methods
.method static bridge synthetic -$$Nest$fgetmBlackCoveredView(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Lcom/android/server/cameracovered/CameraCircleBlackView;
    .locals 0

    iget-object p0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mBlackCoveredView:Lcom/android/server/cameracovered/CameraCircleBlackView;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmDisplay(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Landroid/view/Display;
    .locals 0

    iget-object p0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mDisplay:Landroid/view/Display;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmDisplayInfo(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Landroid/view/DisplayInfo;
    .locals 0

    iget-object p0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mDisplayInfo:Landroid/view/DisplayInfo;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmDisplayListener(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Landroid/hardware/display/DisplayManager$DisplayListener;
    .locals 0

    iget-object p0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mDisplayListener:Landroid/hardware/display/DisplayManager$DisplayListener;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmDisplayManager(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Landroid/hardware/display/DisplayManager;
    .locals 0

    iget-object p0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmForegroundPackage(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mForegroundPackage:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmFrontCameraID(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mFrontCameraID:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHBMCoveredView(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Lcom/android/server/cameracovered/CameraHBMCoveredView;
    .locals 0

    iget-object p0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mHBMCoveredView:Lcom/android/server/cameracovered/CameraHBMCoveredView;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsAddedBlackView(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mIsAddedBlackView:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsAddedHBMView(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mIsAddedHBMView:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsAddedMuraView(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mIsAddedMuraView:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLastDisplayRotation(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)I
    .locals 0

    iget p0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mLastDisplayRotation:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmMuraCoveredView(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Lcom/android/server/cameracovered/CupMuraCoveredView;
    .locals 0

    iget-object p0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mMuraCoveredView:Lcom/android/server/cameracovered/CupMuraCoveredView;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmOccupiedPackage(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mOccupiedPackage:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmWindowManager(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Landroid/view/WindowManager;
    .locals 0

    iget-object p0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mWindowManager:Landroid/view/WindowManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmForegroundPackage(Lcom/android/server/cameracovered/CameraBlackCoveredManager;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mForegroundPackage:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmHideNotch(Lcom/android/server/cameracovered/CameraBlackCoveredManager;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mHideNotch:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsAddedBlackView(Lcom/android/server/cameracovered/CameraBlackCoveredManager;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mIsAddedBlackView:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsAddedHBMView(Lcom/android/server/cameracovered/CameraBlackCoveredManager;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mIsAddedHBMView:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsAddedMuraView(Lcom/android/server/cameracovered/CameraBlackCoveredManager;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mIsAddedMuraView:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLastUnavailableCameraId(Lcom/android/server/cameracovered/CameraBlackCoveredManager;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mLastUnavailableCameraId:I

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetWindowParam(Lcom/android/server/cameracovered/CameraBlackCoveredManager;II)Landroid/view/WindowManager$LayoutParams;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->getWindowParam(II)Landroid/view/WindowManager$LayoutParams;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mhandleHomeGestureReceiver(Lcom/android/server/cameracovered/CameraBlackCoveredManager;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->handleHomeGestureReceiver(Landroid/content/Intent;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$minitCameraId(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->initCameraId()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 103
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->sBlackList:Ljava/util/ArrayList;

    .line 106
    const-string v1, "com.miui.home"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 107
    const-string v1, "com.miui.securitycenter"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 108
    const-string v1, "com.android.systemui"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 109
    const-string v1, "com.miui.face"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 110
    const-string v1, "light.sensor.service"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 113
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->sBroadcastBlackList:Ljava/util/ArrayList;

    .line 116
    const-string v1, "com.tencent.mm"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 117
    const-string v1, "com.tencent.mobileqq"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 118
    const-string v1, "com.skype.rover"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 121
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->sForceShowList:Ljava/util/ArrayList;

    .line 124
    const-string v1, "com.android.camera"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/server/wm/WindowManagerInternal;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "service"    # Lcom/android/server/wm/WindowManagerInternal;

    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mFilter_state_change:Landroid/content/IntentFilter;

    .line 79
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mFilter_launch_home:Landroid/content/IntentFilter;

    .line 82
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mFrontCameraID:Ljava/util/List;

    .line 83
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mBackCameraID:Ljava/util/List;

    .line 94
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mIsAddedBlackView:Z

    .line 95
    iput-boolean v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mIsAddedHBMView:Z

    .line 96
    iput-boolean v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mIsAddedMuraView:Z

    .line 100
    iput v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mLastDisplayRotation:I

    .line 127
    new-instance v1, Lcom/android/server/cameracovered/CameraBlackCoveredManager$1;

    invoke-direct {v1, p0}, Lcom/android/server/cameracovered/CameraBlackCoveredManager$1;-><init>(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)V

    iput-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mDisplayListener:Landroid/hardware/display/DisplayManager$DisplayListener;

    .line 194
    new-instance v1, Lcom/android/server/cameracovered/CameraBlackCoveredManager$2;

    invoke-direct {v1, p0}, Lcom/android/server/cameracovered/CameraBlackCoveredManager$2;-><init>(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)V

    iput-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mAvailabilityCallback:Landroid/hardware/camera2/CameraManager$AvailabilityCallback;

    .line 311
    new-instance v1, Lcom/android/server/cameracovered/CameraBlackCoveredManager$3;

    iget-object v2, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mHandler:Landroid/os/Handler;

    invoke-direct {v1, p0, v2}, Lcom/android/server/cameracovered/CameraBlackCoveredManager$3;-><init>(Lcom/android/server/cameracovered/CameraBlackCoveredManager;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mHideNotchObserver:Landroid/database/ContentObserver;

    .line 319
    new-instance v1, Lcom/android/server/cameracovered/CameraBlackCoveredManager$4;

    invoke-direct {v1, p0}, Lcom/android/server/cameracovered/CameraBlackCoveredManager$4;-><init>(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)V

    iput-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mForegroundInfoChangeListener:Lmiui/process/IForegroundInfoListener$Stub;

    .line 148
    iput-object p1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mContext:Landroid/content/Context;

    .line 149
    iput-object p2, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mWindowManagerService:Lcom/android/server/wm/WindowManagerInternal;

    .line 150
    const-string/jumbo v1, "window"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    iput-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mWindowManager:Landroid/view/WindowManager;

    .line 151
    const-string v1, "display"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/display/DisplayManager;

    iput-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    .line 152
    invoke-virtual {v1, v0}, Landroid/hardware/display/DisplayManager;->getDisplay(I)Landroid/view/Display;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mDisplay:Landroid/view/Display;

    .line 153
    new-instance v1, Landroid/view/DisplayInfo;

    invoke-direct {v1}, Landroid/view/DisplayInfo;-><init>()V

    iput-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mDisplayInfo:Landroid/view/DisplayInfo;

    .line 154
    iget-object v2, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mDisplay:Landroid/view/Display;

    invoke-virtual {v2, v1}, Landroid/view/Display;->getDisplayInfo(Landroid/view/DisplayInfo;)Z

    .line 155
    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mDisplayInfo:Landroid/view/DisplayInfo;

    iget v1, v1, Landroid/view/DisplayInfo;->rotation:I

    iput v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mLastDisplayRotation:I

    .line 156
    new-instance v1, Landroid/os/HandlerThread;

    const-string v2, "camera_animation"

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 157
    .local v1, "t":Landroid/os/HandlerThread;
    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    .line 158
    new-instance v2, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;-><init>(Lcom/android/server/cameracovered/CameraBlackCoveredManager;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mHandler:Landroid/os/Handler;

    .line 159
    new-instance v2, Lcom/android/server/cameracovered/CameraCircleBlackView;

    invoke-direct {v2, p1}, Lcom/android/server/cameracovered/CameraCircleBlackView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mBlackCoveredView:Lcom/android/server/cameracovered/CameraCircleBlackView;

    .line 160
    new-instance v2, Lcom/android/server/cameracovered/CameraHBMCoveredView;

    invoke-direct {v2, p1}, Lcom/android/server/cameracovered/CameraHBMCoveredView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mHBMCoveredView:Lcom/android/server/cameracovered/CameraHBMCoveredView;

    .line 161
    iget-object v2, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mBlackCoveredView:Lcom/android/server/cameracovered/CameraCircleBlackView;

    invoke-virtual {v2, v0}, Lcom/android/server/cameracovered/CameraCircleBlackView;->setForceDarkAllowed(Z)V

    .line 162
    iget-object v2, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mHBMCoveredView:Lcom/android/server/cameracovered/CameraHBMCoveredView;

    invoke-virtual {v2, v0}, Lcom/android/server/cameracovered/CameraHBMCoveredView;->setForceDarkAllowed(Z)V

    .line 163
    new-instance v2, Lcom/android/server/cameracovered/CupMuraCoveredView;

    invoke-direct {v2, p1}, Lcom/android/server/cameracovered/CupMuraCoveredView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mMuraCoveredView:Lcom/android/server/cameracovered/CupMuraCoveredView;

    .line 164
    invoke-virtual {v2, v0}, Lcom/android/server/cameracovered/CupMuraCoveredView;->setForceDarkAllowed(Z)V

    .line 165
    iput-boolean v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mIsAddedView:Z

    .line 166
    iget-object v2, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 167
    const-string v3, "force_black_v2"

    invoke-static {v3}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mHideNotchObserver:Landroid/database/ContentObserver;

    .line 166
    const/4 v5, -0x1

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 168
    iget-object v2, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mHideNotchObserver:Landroid/database/ContentObserver;

    invoke-virtual {v2, v0}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 169
    sget-object v0, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    .line 170
    .local v0, "deviceNM":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v2, "odin"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    if-eqz v0, :cond_1

    const-string v2, "mona"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    :cond_1
    if-eqz v0, :cond_3

    const-string/jumbo v2, "zijin"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 171
    :cond_2
    sget-object v2, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->sBlackList:Ljava/util/ArrayList;

    const-string v3, "com.android.camera"

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 173
    :cond_3
    return-void
.end method

.method private getWindowParam(II)Landroid/view/WindowManager$LayoutParams;
    .locals 9
    .param p1, "ori"    # I
    .param p2, "type"    # I

    .line 339
    const-string v0, "persist.sys.cameraHeight"

    const-string v1, "0"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 340
    .local v0, "h":I
    const-string v2, "persist.sys.cameraWidth"

    invoke-static {v2, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 341
    .local v1, "w":I
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 342
    :cond_0
    iget-object v2, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x105000b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 343
    iget-object v2, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x105000d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 345
    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 351
    :pswitch_0
    move v2, v1

    .line 352
    .local v2, "tempW":I
    move v1, v0

    .line 353
    move v0, v2

    .line 354
    goto :goto_0

    .line 348
    .end local v2    # "tempW":I
    :pswitch_1
    nop

    .line 359
    :goto_0
    new-instance v2, Landroid/view/WindowManager$LayoutParams;

    const/16 v6, 0x7df

    const/16 v7, 0x1538

    const/4 v8, -0x3

    move-object v3, v2

    move v4, v1

    move v5, v0

    invoke-direct/range {v3 .. v8}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    .line 372
    .local v2, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v3, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit8 v3, v3, 0x40

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 373
    iget v3, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit8 v3, v3, 0x10

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 374
    const/4 v3, 0x1

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->layoutInDisplayCutoutMode:I

    .line 375
    packed-switch p1, :pswitch_data_1

    goto :goto_1

    .line 386
    :pswitch_2
    const/16 v4, 0x35

    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 387
    goto :goto_1

    .line 383
    :pswitch_3
    const/16 v4, 0x55

    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 384
    goto :goto_1

    .line 380
    :pswitch_4
    const/16 v4, 0x53

    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 381
    goto :goto_1

    .line 377
    :pswitch_5
    const/16 v4, 0x33

    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 378
    nop

    .line 392
    :goto_1
    if-ne p2, v3, :cond_2

    .line 393
    const-string v3, "cameraBlackCovered"

    invoke-virtual {v2, v3}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 394
    :cond_2
    const/4 v3, 0x2

    if-ne p2, v3, :cond_3

    .line 395
    const-string v3, "cameraHBMCovered"

    invoke-virtual {v2, v3}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 396
    :cond_3
    const/4 v3, 0x3

    if-ne p2, v3, :cond_4

    .line 397
    const-string v3, "cupMuraCovered"

    invoke-virtual {v2, v3}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 398
    const/16 v3, 0x7d6

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 400
    :cond_4
    :goto_2
    return-object v2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method private handleHomeGestureReceiver(Landroid/content/Intent;)V
    .locals 8
    .param p1, "intent"    # Landroid/content/Intent;

    .line 508
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "handleHomeGestureReceiver receive broadcast action is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CameraBlackCoveredManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 509
    iget-object v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mBlackCoveredView:Lcom/android/server/cameracovered/CameraCircleBlackView;

    if-eqz v0, :cond_7

    iget-boolean v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mIsAddedBlackView:Z

    if-nez v0, :cond_0

    goto/16 :goto_4

    .line 513
    :cond_0
    iget-object v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mWindowManagerService:Lcom/android/server/wm/WindowManagerInternal;

    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Lcom/android/server/wm/WindowManagerInternal;->getTopStackPackageName(I)Ljava/lang/String;

    move-result-object v0

    .line 514
    .local v0, "freeformPackageName":Ljava/lang/String;
    sget-object v3, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->sBroadcastBlackList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mOccupiedPackage:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 515
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mOccupiedPackage:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is in sBroadcastBlackList"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 516
    return-void

    .line 517
    :cond_1
    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mOccupiedPackage:Ljava/lang/String;

    if-eqz v3, :cond_2

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 518
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "The top app "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is freeform mode and using the camera service!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 519
    return-void

    .line 521
    :cond_2
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_3
    goto :goto_0

    :sswitch_0
    const-string v4, "com.miui.launch_home_from_hotkey"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x1

    goto :goto_1

    :sswitch_1
    const-string v4, "com.miui.fullscreen_state_change"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x0

    goto :goto_1

    :goto_0
    const/4 v3, -0x1

    :goto_1
    const/4 v4, 0x4

    packed-switch v3, :pswitch_data_0

    .line 539
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleHomeGestureReceiver action: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 535
    :pswitch_0
    const-string v2, "handleHomeGestureReceiver action: com.miui.launch_home_from_hotkey"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 536
    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 537
    goto/16 :goto_3

    .line 523
    :pswitch_1
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string/jumbo v5, "state"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 524
    .local v3, "state":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "package_name"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 525
    .local v5, "packageName":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "handleHomeGestureReceiver action: com.miui.fullscreen_state_changeEXTRA_STATE_NAME = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 526
    const-string/jumbo v1, "toRecents"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    const-string/jumbo v1, "toHome"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    const-string/jumbo v1, "toAnotherApp"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, "closeWindow"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    goto :goto_2

    .line 528
    :cond_4
    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mOccupiedPackage:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "quickSwitchAnimEnd"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 529
    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_3

    .line 531
    :cond_5
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "wz_debug handleHomeGestureReceiver state = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "TAG"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 533
    goto :goto_3

    .line 527
    :cond_6
    :goto_2
    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 542
    .end local v3    # "state":Ljava/lang/String;
    .end local v5    # "packageName":Ljava/lang/String;
    :goto_3
    return-void

    .line 510
    .end local v0    # "freeformPackageName":Ljava/lang/String;
    :cond_7
    :goto_4
    return-void

    :sswitch_data_0
    .sparse-switch
        0x35d637b1 -> :sswitch_1
        0x6a69ff62 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private initCameraId()V
    .locals 10

    .line 257
    const-string v0, "CameraBlackCoveredManager"

    :try_start_0
    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mCameraManager:Landroid/hardware/camera2/CameraManager;

    invoke-virtual {v1}, Landroid/hardware/camera2/CameraManager;->getCameraIdList()[Ljava/lang/String;

    move-result-object v1

    .line 258
    .local v1, "ids":[Ljava/lang/String;
    if-eqz v1, :cond_3

    array-length v2, v1

    if-lez v2, :cond_3

    .line 259
    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_3

    aget-object v4, v1, v3

    .line 260
    .local v4, "sid":Ljava/lang/String;
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 261
    .local v5, "id":I
    const/16 v6, 0x64

    if-lt v5, v6, :cond_0

    .line 262
    goto :goto_1

    .line 264
    :cond_0
    iget-object v6, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mCameraManager:Landroid/hardware/camera2/CameraManager;

    invoke-virtual {v6, v4}, Landroid/hardware/camera2/CameraManager;->getCameraCharacteristics(Ljava/lang/String;)Landroid/hardware/camera2/CameraCharacteristics;

    move-result-object v6

    .line 265
    .local v6, "cc":Landroid/hardware/camera2/CameraCharacteristics;
    sget-object v7, Landroid/hardware/camera2/CameraCharacteristics;->LENS_FACING:Landroid/hardware/camera2/CameraCharacteristics$Key;

    invoke-virtual {v6, v7}, Landroid/hardware/camera2/CameraCharacteristics;->get(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 266
    .local v7, "facing":I
    if-nez v7, :cond_1

    .line 267
    iget-object v8, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mFrontCameraID:Ljava/util/List;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 268
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "wz_debug mFrontCameraID"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v0, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 269
    :cond_1
    const/4 v8, 0x1

    if-ne v7, v8, :cond_2

    .line 270
    iget-object v8, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mBackCameraID:Ljava/util/List;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 271
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "wz_debug mBackCameraID"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v0, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 259
    .end local v4    # "sid":Ljava/lang/String;
    .end local v5    # "id":I
    .end local v6    # "cc":Landroid/hardware/camera2/CameraCharacteristics;
    .end local v7    # "facing":I
    :cond_2
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 277
    .end local v1    # "ids":[Ljava/lang/String;
    :cond_3
    goto :goto_2

    .line 275
    :catch_0
    move-exception v1

    .line 276
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "Can\'t initCameraId"

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_2
    return-void
.end method


# virtual methods
.method public canShowBlackCovered(Z)Z
    .locals 5
    .param p1, "forceShow"    # Z

    .line 304
    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_1

    .line 305
    iget-boolean v2, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mHideNotch:Z

    if-nez v2, :cond_0

    sget-object v2, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->sForceShowList:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mOccupiedPackage:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    return v0

    .line 307
    :cond_1
    const-string/jumbo v2, "vendor.debug.camera.pkgname"

    const-string v3, ""

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 308
    .local v2, "cameraPackageFromProp":Ljava/lang/String;
    iget-boolean v3, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mHideNotch:Z

    if-nez v3, :cond_2

    sget-object v3, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->sBlackList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mOccupiedPackage:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    goto :goto_1

    :cond_2
    move v0, v1

    :goto_1
    return v0
.end method

.method public cupMuraCoveredAnimation(I)V
    .locals 3
    .param p1, "type"    # I

    .line 241
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mHideNotch:Z

    if-nez v0, :cond_0

    .line 242
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 243
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x6

    iput v1, v0, Landroid/os/Message;->what:I

    .line 244
    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mDisplay:Landroid/view/Display;

    iget-object v2, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mDisplayInfo:Landroid/view/DisplayInfo;

    invoke-virtual {v1, v2}, Landroid/view/Display;->getDisplayInfo(Landroid/view/DisplayInfo;)Z

    .line 245
    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mDisplayInfo:Landroid/view/DisplayInfo;

    iget v1, v1, Landroid/view/DisplayInfo;->rotation:I

    iput v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mLastDisplayRotation:I

    .line 246
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 247
    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 248
    .end local v0    # "msg":Landroid/os/Message;
    goto :goto_0

    :cond_0
    if-nez p1, :cond_1

    .line 249
    iget-object v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 251
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "cupMuraCoveredAnimation "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " error!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CameraBlackCoveredManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 253
    :goto_0
    return-void
.end method

.method public hbmCoveredAnimation(I)V
    .locals 3
    .param p1, "type"    # I

    .line 226
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mHideNotch:Z

    if-nez v0, :cond_0

    .line 227
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 228
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x2

    iput v1, v0, Landroid/os/Message;->what:I

    .line 229
    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mDisplay:Landroid/view/Display;

    iget-object v2, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mDisplayInfo:Landroid/view/DisplayInfo;

    invoke-virtual {v1, v2}, Landroid/view/Display;->getDisplayInfo(Landroid/view/DisplayInfo;)Z

    .line 230
    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mDisplayInfo:Landroid/view/DisplayInfo;

    iget v1, v1, Landroid/view/DisplayInfo;->rotation:I

    iput v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mLastDisplayRotation:I

    .line 231
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 232
    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 233
    .end local v0    # "msg":Landroid/os/Message;
    goto :goto_0

    :cond_0
    if-nez p1, :cond_1

    .line 234
    iget-object v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 236
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "hbmCoveredAnimation for HBM the type "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " error!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CameraBlackCoveredManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    :goto_0
    return-void
.end method

.method public hideCoveredBlackView()V
    .locals 2

    .line 281
    iget-boolean v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mIsAddedBlackView:Z

    if-eqz v0, :cond_0

    .line 282
    iget-object v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 284
    :cond_0
    return-void
.end method

.method public registerForegroundInfoChangeListener()V
    .locals 1

    .line 334
    iget-object v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mForegroundInfoChangeListener:Lmiui/process/IForegroundInfoListener$Stub;

    invoke-static {v0}, Lmiui/process/ProcessManager;->unregisterForegroundInfoListener(Lmiui/process/IForegroundInfoListener;)V

    .line 335
    iget-object v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mForegroundInfoChangeListener:Lmiui/process/IForegroundInfoListener$Stub;

    invoke-static {v0}, Lmiui/process/ProcessManager;->registerForegroundInfoListener(Lmiui/process/IForegroundInfoListener;)V

    .line 336
    return-void
.end method

.method public setCoverdPackageName(Ljava/lang/String;)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;

    .line 191
    iput-object p1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mOccupiedPackage:Ljava/lang/String;

    .line 192
    return-void
.end method

.method public startCameraAnimation(Z)V
    .locals 2
    .param p1, "forceShow"    # Z

    .line 287
    if-eqz p1, :cond_0

    .line 288
    iget-object v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mDisplay:Landroid/view/Display;

    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mDisplayInfo:Landroid/view/DisplayInfo;

    invoke-virtual {v0, v1}, Landroid/view/Display;->getDisplayInfo(Landroid/view/DisplayInfo;)Z

    .line 290
    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->canShowBlackCovered(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 291
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 292
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x0

    iput v1, v0, Landroid/os/Message;->what:I

    .line 293
    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mDisplayInfo:Landroid/view/DisplayInfo;

    iget v1, v1, Landroid/view/DisplayInfo;->rotation:I

    iput v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mLastDisplayRotation:I

    .line 294
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 295
    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 297
    .end local v0    # "msg":Landroid/os/Message;
    :cond_1
    return-void
.end method

.method public stopCameraAnimation()V
    .locals 2

    .line 300
    iget-object v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 301
    return-void
.end method

.method public systemReady()V
    .locals 8

    .line 176
    iget-object v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mContext:Landroid/content/Context;

    const-string v1, "camera"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/camera2/CameraManager;

    iput-object v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mCameraManager:Landroid/hardware/camera2/CameraManager;

    .line 177
    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mAvailabilityCallback:Landroid/hardware/camera2/CameraManager$AvailabilityCallback;

    iget-object v2, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1, v2}, Landroid/hardware/camera2/CameraManager;->registerAvailabilityCallback(Landroid/hardware/camera2/CameraManager$AvailabilityCallback;Landroid/os/Handler;)V

    .line 178
    invoke-virtual {p0}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->registerForegroundInfoChangeListener()V

    .line 179
    invoke-direct {p0}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->initCameraId()V

    .line 181
    new-instance v0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$HomeGestureReceiver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/server/cameracovered/CameraBlackCoveredManager$HomeGestureReceiver;-><init>(Lcom/android/server/cameracovered/CameraBlackCoveredManager;Lcom/android/server/cameracovered/CameraBlackCoveredManager$HomeGestureReceiver-IA;)V

    iput-object v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mHomeGestureReceiver:Lcom/android/server/cameracovered/CameraBlackCoveredManager$HomeGestureReceiver;

    .line 182
    iget-object v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mFilter_state_change:Landroid/content/IntentFilter;

    const-string v1, "com.miui.fullscreen_state_change"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 183
    iget-object v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mHomeGestureReceiver:Lcom/android/server/cameracovered/CameraBlackCoveredManager$HomeGestureReceiver;

    iget-object v2, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mFilter_state_change:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 185
    iget-object v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mFilter_launch_home:Landroid/content/IntentFilter;

    const-string v1, "com.miui.launch_home_from_hotkey"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 186
    iget-object v2, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mHomeGestureReceiver:Lcom/android/server/cameracovered/CameraBlackCoveredManager$HomeGestureReceiver;

    sget-object v4, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    iget-object v5, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->mFilter_launch_home:Landroid/content/IntentFilter;

    const-string v6, "miui.permission.USE_INTERNAL_GENERAL_API"

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 188
    return-void
.end method
