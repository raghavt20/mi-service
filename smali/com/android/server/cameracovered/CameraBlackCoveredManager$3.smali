.class Lcom/android/server/cameracovered/CameraBlackCoveredManager$3;
.super Landroid/database/ContentObserver;
.source "CameraBlackCoveredManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/cameracovered/CameraBlackCoveredManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;


# direct methods
.method constructor <init>(Lcom/android/server/cameracovered/CameraBlackCoveredManager;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/cameracovered/CameraBlackCoveredManager;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 311
    iput-object p1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$3;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 4
    .param p1, "selfChange"    # Z

    .line 314
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 315
    iget-object v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$3;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v0}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmContext(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "force_black_v2"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v3, 0x1

    :cond_0
    invoke-static {v0, v3}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fputmHideNotch(Lcom/android/server/cameracovered/CameraBlackCoveredManager;Z)V

    .line 316
    return-void
.end method
