.class Lcom/android/server/cameracovered/CameraBlackCoveredManager$4;
.super Lmiui/process/IForegroundInfoListener$Stub;
.source "CameraBlackCoveredManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/cameracovered/CameraBlackCoveredManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;


# direct methods
.method constructor <init>(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    .line 320
    iput-object p1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$4;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-direct {p0}, Lmiui/process/IForegroundInfoListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onForegroundInfoChanged(Lmiui/process/ForegroundInfo;)V
    .locals 2
    .param p1, "foregroundInfo"    # Lmiui/process/ForegroundInfo;

    .line 323
    iget-object v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$4;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    iget-object v1, p1, Lmiui/process/ForegroundInfo;->mForegroundPackageName:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fputmForegroundPackage(Lcom/android/server/cameracovered/CameraBlackCoveredManager;Ljava/lang/String;)V

    .line 324
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mForegroundPackage = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$4;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v1}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmForegroundPackage(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mOccupiedPackage = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$4;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v1}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmOccupiedPackage(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CameraBlackCoveredManager"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    iget-object v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$4;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v0}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmIsAddedBlackView(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lmiui/process/ForegroundInfo;->mForegroundPackageName:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$4;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v1}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmOccupiedPackage(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 327
    iget-object v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$4;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v0}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmHandler(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 329
    :cond_0
    iget-object v0, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$4;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fputmLastUnavailableCameraId(Lcom/android/server/cameracovered/CameraBlackCoveredManager;I)V

    .line 330
    return-void
.end method
