public class com.android.server.cameracovered.CupMuraCoveredView extends android.widget.ImageView {
	 /* .source "CupMuraCoveredView.java" */
	 /* # static fields */
	 static final java.lang.String TAG;
	 /* # instance fields */
	 protected android.content.Context mContext;
	 /* # direct methods */
	 public com.android.server.cameracovered.CupMuraCoveredView ( ) {
		 /* .locals 1 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .line 20 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* invoke-direct {p0, p1, v0}, Lcom/android/server/cameracovered/CupMuraCoveredView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V */
		 /* .line 21 */
		 return;
	 } // .end method
	 public com.android.server.cameracovered.CupMuraCoveredView ( ) {
		 /* .locals 1 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .param p2, "attrs" # Landroid/util/AttributeSet; */
		 /* .line 24 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* invoke-direct {p0, p1, p2, v0}, Lcom/android/server/cameracovered/CupMuraCoveredView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V */
		 /* .line 25 */
		 return;
	 } // .end method
	 public com.android.server.cameracovered.CupMuraCoveredView ( ) {
		 /* .locals 0 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .param p2, "attrs" # Landroid/util/AttributeSet; */
		 /* .param p3, "defStyle" # I */
		 /* .line 28 */
		 /* invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V */
		 /* .line 29 */
		 this.mContext = p1;
		 /* .line 30 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public void setRadius ( Float p0 ) {
		 /* .locals 5 */
		 /* .param p1, "r" # F */
		 /* .line 34 */
		 /* float-to-int v0, p1 */
		 /* .line 36 */
		 /* .local v0, "radius":I */
		 v1 = this.mContext;
		 final String v2 = "CupMuraCoveredView"; // const-string v2, "CupMuraCoveredView"
		 /* if-nez v1, :cond_0 */
		 /* .line 37 */
		 /* const-string/jumbo v1, "setRadius error because mContext is null" */
		 android.util.Log .e ( v2,v1 );
		 /* .line 38 */
		 return;
		 /* .line 41 */
	 } // :cond_0
	 (( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
	 /* const v3, 0x1108018e */
	 android.graphics.BitmapFactory .decodeResource ( v1,v3 );
	 /* .line 43 */
	 /* .local v1, "bmp":Landroid/graphics/Bitmap; */
	 /* if-nez v1, :cond_1 */
	 /* .line 44 */
	 /* const-string/jumbo v3, "the bitmap from decodeResource is null" */
	 android.util.Log .e ( v2,v3 );
	 /* .line 45 */
	 return;
	 /* .line 48 */
} // :cond_1
v2 = (( android.graphics.Bitmap ) v1 ).getWidth ( ); // invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I
/* mul-int/lit8 v3, v0, 0x2 */
/* if-ne v2, v3, :cond_3 */
v2 = (( android.graphics.Bitmap ) v1 ).getHeight ( ); // invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I
/* mul-int/lit8 v3, v0, 0x2 */
/* if-eq v2, v3, :cond_2 */
/* .line 51 */
} // :cond_2
/* move-object v2, v1 */
/* .local v2, "sbmp":Landroid/graphics/Bitmap; */
/* .line 49 */
} // .end local v2 # "sbmp":Landroid/graphics/Bitmap;
} // :cond_3
} // :goto_0
/* mul-int/lit8 v2, v0, 0x2 */
/* mul-int/lit8 v3, v0, 0x2 */
int v4 = 1; // const/4 v4, 0x1
android.graphics.Bitmap .createScaledBitmap ( v1,v2,v3,v4 );
/* .line 53 */
/* .restart local v2 # "sbmp":Landroid/graphics/Bitmap; */
} // :goto_1
(( com.android.server.cameracovered.CupMuraCoveredView ) p0 ).setImageBitmap ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/cameracovered/CupMuraCoveredView;->setImageBitmap(Landroid/graphics/Bitmap;)V
/* .line 54 */
v3 = android.widget.ImageView$ScaleType.CENTER;
(( com.android.server.cameracovered.CupMuraCoveredView ) p0 ).setScaleType ( v3 ); // invoke-virtual {p0, v3}, Lcom/android/server/cameracovered/CupMuraCoveredView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V
/* .line 55 */
return;
} // .end method
