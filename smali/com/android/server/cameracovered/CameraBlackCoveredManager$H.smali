.class Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;
.super Landroid/os/Handler;
.source "CameraBlackCoveredManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/cameracovered/CameraBlackCoveredManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "H"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;


# direct methods
.method public constructor <init>(Lcom/android/server/cameracovered/CameraBlackCoveredManager;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 405
    iput-object p1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    .line 406
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 407
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 13
    .param p1, "msg"    # Landroid/os/Message;

    .line 413
    const v0, 0x3a83126f    # 0.001f

    .line 415
    .local v0, "alpha":F
    iget v1, p1, Landroid/os/Message;->what:I

    const v2, 0x105000c

    const-wide/16 v3, 0x0

    const-string v5, "0.0"

    const-string v6, "persist.sys.cameraRadius"

    const/4 v7, 0x1

    const/4 v8, 0x0

    const-string v9, "CameraBlackCoveredManager"

    packed-switch v1, :pswitch_data_0

    goto/16 :goto_0

    .line 476
    :pswitch_0
    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v1}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmMuraCoveredView(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Lcom/android/server/cameracovered/CupMuraCoveredView;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v1}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmIsAddedMuraView(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 477
    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v1}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmWindowManager(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Landroid/view/WindowManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v2}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmMuraCoveredView(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Lcom/android/server/cameracovered/CupMuraCoveredView;

    move-result-object v2

    invoke-interface {v1, v2}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 478
    const-string v1, "CUP_MURA_COVER is removed"

    invoke-static {v9, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 479
    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v1, v8}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fputmIsAddedMuraView(Lcom/android/server/cameracovered/CameraBlackCoveredManager;Z)V

    goto/16 :goto_0

    .line 461
    :pswitch_1
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 462
    .local v1, "ori":I
    iget-object v8, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    const/4 v10, 0x3

    invoke-static {v8, v1, v10}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$mgetWindowParam(Lcom/android/server/cameracovered/CameraBlackCoveredManager;II)Landroid/view/WindowManager$LayoutParams;

    move-result-object v8

    .line 463
    .local v8, "layoutParams":Landroid/view/WindowManager$LayoutParams;
    iget-object v10, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v10}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmMuraCoveredView(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Lcom/android/server/cameracovered/CupMuraCoveredView;

    move-result-object v10

    if-eqz v10, :cond_3

    iget-object v10, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v10}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmIsAddedMuraView(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Z

    move-result v10

    if-nez v10, :cond_3

    .line 464
    invoke-static {v6, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    .line 465
    .local v5, "radius":F
    float-to-double v10, v5

    cmpl-double v3, v10, v3

    if-nez v3, :cond_0

    .line 466
    iget-object v3, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v3}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmContext(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    .line 468
    :cond_0
    iget-object v2, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v2}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmMuraCoveredView(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Lcom/android/server/cameracovered/CupMuraCoveredView;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/android/server/cameracovered/CupMuraCoveredView;->setRadius(F)V

    .line 469
    iput v0, v8, Landroid/view/WindowManager$LayoutParams;->alpha:F

    .line 470
    iget-object v2, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v2}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmWindowManager(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Landroid/view/WindowManager;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v3}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmMuraCoveredView(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Lcom/android/server/cameracovered/CupMuraCoveredView;

    move-result-object v3

    invoke-interface {v2, v3, v8}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 471
    const-string v2, "CUP_MURA_COVER is added"

    invoke-static {v9, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 472
    iget-object v2, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v2, v7}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fputmIsAddedMuraView(Lcom/android/server/cameracovered/CameraBlackCoveredManager;Z)V

    goto/16 :goto_0

    .line 489
    .end local v1    # "ori":I
    .end local v5    # "radius":F
    .end local v8    # "layoutParams":Landroid/view/WindowManager$LayoutParams;
    :pswitch_2
    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v1}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmBlackCoveredView(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Lcom/android/server/cameracovered/CameraCircleBlackView;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v1}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmIsAddedBlackView(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 490
    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v1}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmBlackCoveredView(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Lcom/android/server/cameracovered/CameraCircleBlackView;

    move-result-object v1

    invoke-virtual {v1, v8}, Lcom/android/server/cameracovered/CameraCircleBlackView;->setVisibility(I)V

    .line 491
    const-string v1, "FrontCamera_BlackView is showed."

    invoke-static {v9, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 483
    :pswitch_3
    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v1}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmBlackCoveredView(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Lcom/android/server/cameracovered/CameraCircleBlackView;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v1}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmIsAddedBlackView(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 484
    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v1}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmBlackCoveredView(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Lcom/android/server/cameracovered/CameraCircleBlackView;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/android/server/cameracovered/CameraCircleBlackView;->setVisibility(I)V

    .line 485
    const-string v1, "FrontCamera_BlackView is hided."

    invoke-static {v9, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 454
    :pswitch_4
    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v1}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmHBMCoveredView(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Lcom/android/server/cameracovered/CameraHBMCoveredView;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v1}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmIsAddedHBMView(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 455
    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v1}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmWindowManager(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Landroid/view/WindowManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v2}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmHBMCoveredView(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Lcom/android/server/cameracovered/CameraHBMCoveredView;

    move-result-object v2

    invoke-interface {v1, v2}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 456
    const-string v1, "FrontCamera_HBMView is removed"

    invoke-static {v9, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v1, v8}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fputmIsAddedHBMView(Lcom/android/server/cameracovered/CameraBlackCoveredManager;Z)V

    goto/16 :goto_0

    .line 440
    :pswitch_5
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 441
    .restart local v1    # "ori":I
    iget-object v8, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    const/4 v10, 0x2

    invoke-static {v8, v1, v10}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$mgetWindowParam(Lcom/android/server/cameracovered/CameraBlackCoveredManager;II)Landroid/view/WindowManager$LayoutParams;

    move-result-object v8

    .line 442
    .restart local v8    # "layoutParams":Landroid/view/WindowManager$LayoutParams;
    iget-object v10, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v10}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmHBMCoveredView(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Lcom/android/server/cameracovered/CameraHBMCoveredView;

    move-result-object v10

    if-eqz v10, :cond_3

    iget-object v10, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v10}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmIsAddedHBMView(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Z

    move-result v10

    if-nez v10, :cond_3

    .line 443
    invoke-static {v6, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    .line 444
    .restart local v5    # "radius":F
    float-to-double v10, v5

    cmpl-double v3, v10, v3

    if-nez v3, :cond_1

    .line 445
    iget-object v3, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v3}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmContext(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    .line 447
    :cond_1
    iget-object v2, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v2}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmHBMCoveredView(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Lcom/android/server/cameracovered/CameraHBMCoveredView;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/android/server/cameracovered/CameraHBMCoveredView;->setRadius(F)V

    .line 448
    iget-object v2, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v2}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmWindowManager(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Landroid/view/WindowManager;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v3}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmHBMCoveredView(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Lcom/android/server/cameracovered/CameraHBMCoveredView;

    move-result-object v3

    invoke-interface {v2, v3, v8}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 449
    const-string v2, "FrontCamera_HBMView is added"

    invoke-static {v9, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 450
    iget-object v2, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v2, v7}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fputmIsAddedHBMView(Lcom/android/server/cameracovered/CameraBlackCoveredManager;Z)V

    goto/16 :goto_0

    .line 433
    .end local v1    # "ori":I
    .end local v5    # "radius":F
    .end local v8    # "layoutParams":Landroid/view/WindowManager$LayoutParams;
    :pswitch_6
    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v1}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmBlackCoveredView(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Lcom/android/server/cameracovered/CameraCircleBlackView;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v1}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmIsAddedBlackView(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 434
    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v1}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmWindowManager(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Landroid/view/WindowManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v2}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmBlackCoveredView(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Lcom/android/server/cameracovered/CameraCircleBlackView;

    move-result-object v2

    invoke-interface {v1, v2}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 435
    const-string v1, "FrontCamera_BlackView is removed"

    invoke-static {v9, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 436
    iget-object v1, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v1, v8}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fputmIsAddedBlackView(Lcom/android/server/cameracovered/CameraBlackCoveredManager;Z)V

    goto :goto_0

    .line 417
    :pswitch_7
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 418
    .restart local v1    # "ori":I
    iget-object v10, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v10, v1, v7}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$mgetWindowParam(Lcom/android/server/cameracovered/CameraBlackCoveredManager;II)Landroid/view/WindowManager$LayoutParams;

    move-result-object v10

    .line 419
    .local v10, "layoutParams":Landroid/view/WindowManager$LayoutParams;
    const-string v11, "FrontCamera_BlackView is added FRONT_CAMERA_OPEN"

    invoke-static {v9, v11}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 420
    iget-object v11, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v11}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmBlackCoveredView(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Lcom/android/server/cameracovered/CameraCircleBlackView;

    move-result-object v11

    if-eqz v11, :cond_3

    iget-object v11, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v11}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmIsAddedBlackView(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Z

    move-result v11

    if-nez v11, :cond_3

    .line 421
    invoke-static {v6, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    .line 422
    .restart local v5    # "radius":F
    float-to-double v11, v5

    cmpl-double v3, v11, v3

    if-nez v3, :cond_2

    .line 423
    iget-object v3, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v3}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmContext(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    .line 425
    :cond_2
    iget-object v2, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v2}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmBlackCoveredView(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Lcom/android/server/cameracovered/CameraCircleBlackView;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/android/server/cameracovered/CameraCircleBlackView;->setRadius(F)V

    .line 426
    iget-object v2, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v2}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmWindowManager(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Landroid/view/WindowManager;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v3}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmBlackCoveredView(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Lcom/android/server/cameracovered/CameraCircleBlackView;

    move-result-object v3

    invoke-interface {v2, v3, v10}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 427
    const-string v2, "FrontCamera_BlackView is added"

    invoke-static {v9, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 428
    iget-object v2, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v2, v7}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fputmIsAddedBlackView(Lcom/android/server/cameracovered/CameraBlackCoveredManager;Z)V

    .line 429
    iget-object v2, p0, Lcom/android/server/cameracovered/CameraBlackCoveredManager$H;->this$0:Lcom/android/server/cameracovered/CameraBlackCoveredManager;

    invoke-static {v2}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->-$$Nest$fgetmBlackCoveredView(Lcom/android/server/cameracovered/CameraBlackCoveredManager;)Lcom/android/server/cameracovered/CameraCircleBlackView;

    move-result-object v2

    invoke-virtual {v2, v8}, Lcom/android/server/cameracovered/CameraCircleBlackView;->setVisibility(I)V

    .line 497
    .end local v1    # "ori":I
    .end local v5    # "radius":F
    .end local v10    # "layoutParams":Landroid/view/WindowManager$LayoutParams;
    :cond_3
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
