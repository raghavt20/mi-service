class com.android.server.cameracovered.CameraBlackCoveredManager$2 extends android.hardware.camera2.CameraManager$AvailabilityCallback {
	 /* .source "CameraBlackCoveredManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/cameracovered/CameraBlackCoveredManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.cameracovered.CameraBlackCoveredManager this$0; //synthetic
/* # direct methods */
 com.android.server.cameracovered.CameraBlackCoveredManager$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/cameracovered/CameraBlackCoveredManager; */
/* .line 195 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/hardware/camera2/CameraManager$AvailabilityCallback;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onCameraAvailable ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "cameraId" # Ljava/lang/String; */
/* .line 198 */
/* invoke-super {p0, p1}, Landroid/hardware/camera2/CameraManager$AvailabilityCallback;->onCameraAvailable(Ljava/lang/String;)V */
/* .line 199 */
java.lang.Integer .valueOf ( p1 );
v0 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
/* .line 200 */
/* .local v0, "id":I */
v1 = this.this$0;
com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmFrontCameraID ( v1 );
v1 = java.lang.Integer .valueOf ( v0 );
if ( v1 != null) { // if-eqz v1, :cond_0
	 int v1 = 1; // const/4 v1, 0x1
	 /* if-ne v0, v1, :cond_0 */
	 /* .line 201 */
	 /* new-instance v1, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
	 /* const-string/jumbo v2, "wz_debug onCameraAvailable" */
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v2 = "CameraBlackCoveredManager"; // const-string v2, "CameraBlackCoveredManager"
	 android.util.Slog .d ( v2,v1 );
	 /* .line 202 */
	 v1 = this.this$0;
	 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmDisplayManager ( v1 );
	 v3 = this.this$0;
	 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmDisplayListener ( v3 );
	 (( android.hardware.display.DisplayManager ) v1 ).unregisterDisplayListener ( v3 ); // invoke-virtual {v1, v3}, Landroid/hardware/display/DisplayManager;->unregisterDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;)V
	 /* .line 203 */
	 v1 = this.this$0;
	 (( com.android.server.cameracovered.CameraBlackCoveredManager ) v1 ).stopCameraAnimation ( ); // invoke-virtual {v1}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->stopCameraAnimation()V
	 /* .line 204 */
	 /* new-instance v1, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v3 = "onCameraAvailable "; // const-string v3, "onCameraAvailable "
	 (( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .d ( v2,v1 );
	 /* .line 206 */
} // :cond_0
return;
} // .end method
public void onCameraUnavailable ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "cameraId" # Ljava/lang/String; */
/* .line 210 */
v0 = this.this$0;
v0 = com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmFrontCameraID ( v0 );
/* if-nez v0, :cond_0 */
/* .line 211 */
v0 = this.this$0;
com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$minitCameraId ( v0 );
/* .line 213 */
} // :cond_0
/* invoke-super {p0, p1}, Landroid/hardware/camera2/CameraManager$AvailabilityCallback;->onCameraUnavailable(Ljava/lang/String;)V */
/* .line 214 */
java.lang.Integer .valueOf ( p1 );
v0 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
/* .line 215 */
/* .local v0, "id":I */
v1 = this.this$0;
com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmFrontCameraID ( v1 );
v1 = java.lang.Integer .valueOf ( v0 );
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 216 */
v1 = this.this$0;
com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmDisplayManager ( v1 );
v2 = this.this$0;
com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmDisplayListener ( v2 );
v3 = this.this$0;
com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmHandler ( v3 );
(( android.hardware.display.DisplayManager ) v1 ).registerDisplayListener ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/hardware/display/DisplayManager;->registerDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;Landroid/os/Handler;)V
/* .line 217 */
v1 = this.this$0;
com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmDisplay ( v1 );
v2 = this.this$0;
com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmDisplayInfo ( v2 );
(( android.view.Display ) v1 ).getDisplayInfo ( v2 ); // invoke-virtual {v1, v2}, Landroid/view/Display;->getDisplayInfo(Landroid/view/DisplayInfo;)Z
/* .line 218 */
v1 = this.this$0;
int v2 = 0; // const/4 v2, 0x0
(( com.android.server.cameracovered.CameraBlackCoveredManager ) v1 ).startCameraAnimation ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/cameracovered/CameraBlackCoveredManager;->startCameraAnimation(Z)V
/* .line 219 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "onCameraUnavailable "; // const-string v2, "onCameraUnavailable "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "CameraBlackCoveredManager"; // const-string v2, "CameraBlackCoveredManager"
android.util.Slog .d ( v2,v1 );
/* .line 221 */
} // :cond_1
v1 = this.this$0;
com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fputmLastUnavailableCameraId ( v1,v0 );
/* .line 222 */
return;
} // .end method
