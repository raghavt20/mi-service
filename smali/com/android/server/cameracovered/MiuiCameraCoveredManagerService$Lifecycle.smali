.class public final Lcom/android/server/cameracovered/MiuiCameraCoveredManagerService$Lifecycle;
.super Lcom/android/server/SystemService;
.source "MiuiCameraCoveredManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/cameracovered/MiuiCameraCoveredManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Lifecycle"
.end annotation


# instance fields
.field private final mService:Lcom/android/server/cameracovered/MiuiCameraCoveredManagerService;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 38
    invoke-direct {p0, p1}, Lcom/android/server/SystemService;-><init>(Landroid/content/Context;)V

    .line 39
    new-instance v0, Lcom/android/server/cameracovered/MiuiCameraCoveredManagerService;

    invoke-direct {v0, p1}, Lcom/android/server/cameracovered/MiuiCameraCoveredManagerService;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/cameracovered/MiuiCameraCoveredManagerService$Lifecycle;->mService:Lcom/android/server/cameracovered/MiuiCameraCoveredManagerService;

    .line 40
    return-void
.end method


# virtual methods
.method public onStart()V
    .locals 2

    .line 44
    const-string v0, "camera_covered_service"

    iget-object v1, p0, Lcom/android/server/cameracovered/MiuiCameraCoveredManagerService$Lifecycle;->mService:Lcom/android/server/cameracovered/MiuiCameraCoveredManagerService;

    invoke-virtual {p0, v0, v1}, Lcom/android/server/cameracovered/MiuiCameraCoveredManagerService$Lifecycle;->publishBinderService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 45
    return-void
.end method
