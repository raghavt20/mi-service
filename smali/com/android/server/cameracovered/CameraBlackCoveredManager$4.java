class com.android.server.cameracovered.CameraBlackCoveredManager$4 extends miui.process.IForegroundInfoListener$Stub {
	 /* .source "CameraBlackCoveredManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/cameracovered/CameraBlackCoveredManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.cameracovered.CameraBlackCoveredManager this$0; //synthetic
/* # direct methods */
 com.android.server.cameracovered.CameraBlackCoveredManager$4 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/cameracovered/CameraBlackCoveredManager; */
/* .line 320 */
this.this$0 = p1;
/* invoke-direct {p0}, Lmiui/process/IForegroundInfoListener$Stub;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onForegroundInfoChanged ( miui.process.ForegroundInfo p0 ) {
/* .locals 2 */
/* .param p1, "foregroundInfo" # Lmiui/process/ForegroundInfo; */
/* .line 323 */
v0 = this.this$0;
v1 = this.mForegroundPackageName;
com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fputmForegroundPackage ( v0,v1 );
/* .line 324 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "mForegroundPackage = "; // const-string v1, "mForegroundPackage = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.this$0;
com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmForegroundPackage ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " mOccupiedPackage = "; // const-string v1, " mOccupiedPackage = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.this$0;
com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmOccupiedPackage ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "CameraBlackCoveredManager"; // const-string v1, "CameraBlackCoveredManager"
android.util.Log .d ( v1,v0 );
/* .line 325 */
v0 = this.this$0;
v0 = com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmIsAddedBlackView ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
	 v0 = this.mForegroundPackageName;
	 v1 = this.this$0;
	 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmOccupiedPackage ( v1 );
	 v0 = 	 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 327 */
		 v0 = this.this$0;
		 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmHandler ( v0 );
		 int v1 = 5; // const/4 v1, 0x5
		 (( android.os.Handler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
		 /* .line 329 */
	 } // :cond_0
	 v0 = this.this$0;
	 int v1 = -1; // const/4 v1, -0x1
	 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fputmLastUnavailableCameraId ( v0,v1 );
	 /* .line 330 */
	 return;
} // .end method
