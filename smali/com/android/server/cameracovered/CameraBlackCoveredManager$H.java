class com.android.server.cameracovered.CameraBlackCoveredManager$H extends android.os.Handler {
	 /* .source "CameraBlackCoveredManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/cameracovered/CameraBlackCoveredManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "H" */
} // .end annotation
/* # instance fields */
final com.android.server.cameracovered.CameraBlackCoveredManager this$0; //synthetic
/* # direct methods */
public com.android.server.cameracovered.CameraBlackCoveredManager$H ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 405 */
this.this$0 = p1;
/* .line 406 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 407 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 13 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 413 */
/* const v0, 0x3a83126f # 0.001f */
/* .line 415 */
/* .local v0, "alpha":F */
/* iget v1, p1, Landroid/os/Message;->what:I */
/* const v2, 0x105000c */
/* const-wide/16 v3, 0x0 */
final String v5 = "0.0"; // const-string v5, "0.0"
final String v6 = "persist.sys.cameraRadius"; // const-string v6, "persist.sys.cameraRadius"
int v7 = 1; // const/4 v7, 0x1
int v8 = 0; // const/4 v8, 0x0
final String v9 = "CameraBlackCoveredManager"; // const-string v9, "CameraBlackCoveredManager"
/* packed-switch v1, :pswitch_data_0 */
/* goto/16 :goto_0 */
/* .line 476 */
/* :pswitch_0 */
v1 = this.this$0;
com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmMuraCoveredView ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_3
	 v1 = this.this$0;
	 v1 = 	 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmIsAddedMuraView ( v1 );
	 if ( v1 != null) { // if-eqz v1, :cond_3
		 /* .line 477 */
		 v1 = this.this$0;
		 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmWindowManager ( v1 );
		 v2 = this.this$0;
		 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmMuraCoveredView ( v2 );
		 /* .line 478 */
		 final String v1 = "CUP_MURA_COVER is removed"; // const-string v1, "CUP_MURA_COVER is removed"
		 android.util.Slog .d ( v9,v1 );
		 /* .line 479 */
		 v1 = this.this$0;
		 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fputmIsAddedMuraView ( v1,v8 );
		 /* goto/16 :goto_0 */
		 /* .line 461 */
		 /* :pswitch_1 */
		 v1 = this.obj;
		 /* check-cast v1, Ljava/lang/Integer; */
		 v1 = 		 (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
		 /* .line 462 */
		 /* .local v1, "ori":I */
		 v8 = this.this$0;
		 int v10 = 3; // const/4 v10, 0x3
		 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$mgetWindowParam ( v8,v1,v10 );
		 /* .line 463 */
		 /* .local v8, "layoutParams":Landroid/view/WindowManager$LayoutParams; */
		 v10 = this.this$0;
		 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmMuraCoveredView ( v10 );
		 if ( v10 != null) { // if-eqz v10, :cond_3
			 v10 = this.this$0;
			 v10 = 			 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmIsAddedMuraView ( v10 );
			 /* if-nez v10, :cond_3 */
			 /* .line 464 */
			 android.os.SystemProperties .get ( v6,v5 );
			 v5 = 			 java.lang.Float .parseFloat ( v5 );
			 /* .line 465 */
			 /* .local v5, "radius":F */
			 /* float-to-double v10, v5 */
			 /* cmpl-double v3, v10, v3 */
			 /* if-nez v3, :cond_0 */
			 /* .line 466 */
			 v3 = this.this$0;
			 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmContext ( v3 );
			 (( android.content.Context ) v3 ).getResources ( ); // invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
			 v5 = 			 (( android.content.res.Resources ) v3 ).getDimension ( v2 ); // invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getDimension(I)F
			 /* .line 468 */
		 } // :cond_0
		 v2 = this.this$0;
		 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmMuraCoveredView ( v2 );
		 (( com.android.server.cameracovered.CupMuraCoveredView ) v2 ).setRadius ( v5 ); // invoke-virtual {v2, v5}, Lcom/android/server/cameracovered/CupMuraCoveredView;->setRadius(F)V
		 /* .line 469 */
		 /* iput v0, v8, Landroid/view/WindowManager$LayoutParams;->alpha:F */
		 /* .line 470 */
		 v2 = this.this$0;
		 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmWindowManager ( v2 );
		 v3 = this.this$0;
		 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmMuraCoveredView ( v3 );
		 /* .line 471 */
		 final String v2 = "CUP_MURA_COVER is added"; // const-string v2, "CUP_MURA_COVER is added"
		 android.util.Slog .d ( v9,v2 );
		 /* .line 472 */
		 v2 = this.this$0;
		 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fputmIsAddedMuraView ( v2,v7 );
		 /* goto/16 :goto_0 */
		 /* .line 489 */
	 } // .end local v1 # "ori":I
} // .end local v5 # "radius":F
} // .end local v8 # "layoutParams":Landroid/view/WindowManager$LayoutParams;
/* :pswitch_2 */
v1 = this.this$0;
com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmBlackCoveredView ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_3
v1 = this.this$0;
v1 = com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmIsAddedBlackView ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_3
	 /* .line 490 */
	 v1 = this.this$0;
	 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmBlackCoveredView ( v1 );
	 (( com.android.server.cameracovered.CameraCircleBlackView ) v1 ).setVisibility ( v8 ); // invoke-virtual {v1, v8}, Lcom/android/server/cameracovered/CameraCircleBlackView;->setVisibility(I)V
	 /* .line 491 */
	 final String v1 = "FrontCamera_BlackView is showed."; // const-string v1, "FrontCamera_BlackView is showed."
	 android.util.Slog .d ( v9,v1 );
	 /* goto/16 :goto_0 */
	 /* .line 483 */
	 /* :pswitch_3 */
	 v1 = this.this$0;
	 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmBlackCoveredView ( v1 );
	 if ( v1 != null) { // if-eqz v1, :cond_3
		 v1 = this.this$0;
		 v1 = 		 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmIsAddedBlackView ( v1 );
		 if ( v1 != null) { // if-eqz v1, :cond_3
			 /* .line 484 */
			 v1 = this.this$0;
			 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmBlackCoveredView ( v1 );
			 int v2 = 4; // const/4 v2, 0x4
			 (( com.android.server.cameracovered.CameraCircleBlackView ) v1 ).setVisibility ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/cameracovered/CameraCircleBlackView;->setVisibility(I)V
			 /* .line 485 */
			 final String v1 = "FrontCamera_BlackView is hided."; // const-string v1, "FrontCamera_BlackView is hided."
			 android.util.Slog .d ( v9,v1 );
			 /* goto/16 :goto_0 */
			 /* .line 454 */
			 /* :pswitch_4 */
			 v1 = this.this$0;
			 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmHBMCoveredView ( v1 );
			 if ( v1 != null) { // if-eqz v1, :cond_3
				 v1 = this.this$0;
				 v1 = 				 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmIsAddedHBMView ( v1 );
				 if ( v1 != null) { // if-eqz v1, :cond_3
					 /* .line 455 */
					 v1 = this.this$0;
					 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmWindowManager ( v1 );
					 v2 = this.this$0;
					 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmHBMCoveredView ( v2 );
					 /* .line 456 */
					 final String v1 = "FrontCamera_HBMView is removed"; // const-string v1, "FrontCamera_HBMView is removed"
					 android.util.Slog .d ( v9,v1 );
					 /* .line 457 */
					 v1 = this.this$0;
					 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fputmIsAddedHBMView ( v1,v8 );
					 /* goto/16 :goto_0 */
					 /* .line 440 */
					 /* :pswitch_5 */
					 v1 = this.obj;
					 /* check-cast v1, Ljava/lang/Integer; */
					 v1 = 					 (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
					 /* .line 441 */
					 /* .restart local v1 # "ori":I */
					 v8 = this.this$0;
					 int v10 = 2; // const/4 v10, 0x2
					 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$mgetWindowParam ( v8,v1,v10 );
					 /* .line 442 */
					 /* .restart local v8 # "layoutParams":Landroid/view/WindowManager$LayoutParams; */
					 v10 = this.this$0;
					 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmHBMCoveredView ( v10 );
					 if ( v10 != null) { // if-eqz v10, :cond_3
						 v10 = this.this$0;
						 v10 = 						 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmIsAddedHBMView ( v10 );
						 /* if-nez v10, :cond_3 */
						 /* .line 443 */
						 android.os.SystemProperties .get ( v6,v5 );
						 v5 = 						 java.lang.Float .parseFloat ( v5 );
						 /* .line 444 */
						 /* .restart local v5 # "radius":F */
						 /* float-to-double v10, v5 */
						 /* cmpl-double v3, v10, v3 */
						 /* if-nez v3, :cond_1 */
						 /* .line 445 */
						 v3 = this.this$0;
						 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmContext ( v3 );
						 (( android.content.Context ) v3 ).getResources ( ); // invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
						 v5 = 						 (( android.content.res.Resources ) v3 ).getDimension ( v2 ); // invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getDimension(I)F
						 /* .line 447 */
					 } // :cond_1
					 v2 = this.this$0;
					 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmHBMCoveredView ( v2 );
					 (( com.android.server.cameracovered.CameraHBMCoveredView ) v2 ).setRadius ( v5 ); // invoke-virtual {v2, v5}, Lcom/android/server/cameracovered/CameraHBMCoveredView;->setRadius(F)V
					 /* .line 448 */
					 v2 = this.this$0;
					 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmWindowManager ( v2 );
					 v3 = this.this$0;
					 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmHBMCoveredView ( v3 );
					 /* .line 449 */
					 final String v2 = "FrontCamera_HBMView is added"; // const-string v2, "FrontCamera_HBMView is added"
					 android.util.Slog .d ( v9,v2 );
					 /* .line 450 */
					 v2 = this.this$0;
					 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fputmIsAddedHBMView ( v2,v7 );
					 /* goto/16 :goto_0 */
					 /* .line 433 */
				 } // .end local v1 # "ori":I
			 } // .end local v5 # "radius":F
		 } // .end local v8 # "layoutParams":Landroid/view/WindowManager$LayoutParams;
		 /* :pswitch_6 */
		 v1 = this.this$0;
		 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmBlackCoveredView ( v1 );
		 if ( v1 != null) { // if-eqz v1, :cond_3
			 v1 = this.this$0;
			 v1 = 			 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmIsAddedBlackView ( v1 );
			 if ( v1 != null) { // if-eqz v1, :cond_3
				 /* .line 434 */
				 v1 = this.this$0;
				 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmWindowManager ( v1 );
				 v2 = this.this$0;
				 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmBlackCoveredView ( v2 );
				 /* .line 435 */
				 final String v1 = "FrontCamera_BlackView is removed"; // const-string v1, "FrontCamera_BlackView is removed"
				 android.util.Slog .d ( v9,v1 );
				 /* .line 436 */
				 v1 = this.this$0;
				 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fputmIsAddedBlackView ( v1,v8 );
				 /* .line 417 */
				 /* :pswitch_7 */
				 v1 = this.obj;
				 /* check-cast v1, Ljava/lang/Integer; */
				 v1 = 				 (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
				 /* .line 418 */
				 /* .restart local v1 # "ori":I */
				 v10 = this.this$0;
				 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$mgetWindowParam ( v10,v1,v7 );
				 /* .line 419 */
				 /* .local v10, "layoutParams":Landroid/view/WindowManager$LayoutParams; */
				 final String v11 = "FrontCamera_BlackView is added FRONT_CAMERA_OPEN"; // const-string v11, "FrontCamera_BlackView is added FRONT_CAMERA_OPEN"
				 android.util.Slog .d ( v9,v11 );
				 /* .line 420 */
				 v11 = this.this$0;
				 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmBlackCoveredView ( v11 );
				 if ( v11 != null) { // if-eqz v11, :cond_3
					 v11 = this.this$0;
					 v11 = 					 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmIsAddedBlackView ( v11 );
					 /* if-nez v11, :cond_3 */
					 /* .line 421 */
					 android.os.SystemProperties .get ( v6,v5 );
					 v5 = 					 java.lang.Float .parseFloat ( v5 );
					 /* .line 422 */
					 /* .restart local v5 # "radius":F */
					 /* float-to-double v11, v5 */
					 /* cmpl-double v3, v11, v3 */
					 /* if-nez v3, :cond_2 */
					 /* .line 423 */
					 v3 = this.this$0;
					 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmContext ( v3 );
					 (( android.content.Context ) v3 ).getResources ( ); // invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
					 v5 = 					 (( android.content.res.Resources ) v3 ).getDimension ( v2 ); // invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getDimension(I)F
					 /* .line 425 */
				 } // :cond_2
				 v2 = this.this$0;
				 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmBlackCoveredView ( v2 );
				 (( com.android.server.cameracovered.CameraCircleBlackView ) v2 ).setRadius ( v5 ); // invoke-virtual {v2, v5}, Lcom/android/server/cameracovered/CameraCircleBlackView;->setRadius(F)V
				 /* .line 426 */
				 v2 = this.this$0;
				 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmWindowManager ( v2 );
				 v3 = this.this$0;
				 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmBlackCoveredView ( v3 );
				 /* .line 427 */
				 final String v2 = "FrontCamera_BlackView is added"; // const-string v2, "FrontCamera_BlackView is added"
				 android.util.Slog .d ( v9,v2 );
				 /* .line 428 */
				 v2 = this.this$0;
				 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fputmIsAddedBlackView ( v2,v7 );
				 /* .line 429 */
				 v2 = this.this$0;
				 com.android.server.cameracovered.CameraBlackCoveredManager .-$$Nest$fgetmBlackCoveredView ( v2 );
				 (( com.android.server.cameracovered.CameraCircleBlackView ) v2 ).setVisibility ( v8 ); // invoke-virtual {v2, v8}, Lcom/android/server/cameracovered/CameraCircleBlackView;->setVisibility(I)V
				 /* .line 497 */
			 } // .end local v1 # "ori":I
		 } // .end local v5 # "radius":F
	 } // .end local v10 # "layoutParams":Landroid/view/WindowManager$LayoutParams;
} // :cond_3
} // :goto_0
return;
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
