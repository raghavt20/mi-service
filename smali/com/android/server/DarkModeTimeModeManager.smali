.class public Lcom/android/server/DarkModeTimeModeManager;
.super Ljava/lang/Object;
.source "DarkModeTimeModeManager.java"


# static fields
.field public static final DARK_MODE_ENABLE:Ljava/lang/String; = "dark_mode_enable"

.field public static final SCREEN_DARKMODE:I = 0x26

.field public static final SUPPORT_DARK_MODE_NOTIFY:Z

.field private static final TAG:Ljava/lang/String; = "DarkModeTimeModeManager"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDarkModeObserver:Landroid/database/ContentObserver;

.field private mDarkModeSuggestProvider:Lcom/android/server/DarkModeSuggestProvider;

.field private mDarkModeTimeModeReceiver:Landroid/content/BroadcastReceiver;

.field private mUiModeManager:Landroid/app/UiModeManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 61
    nop

    .line 62
    const-string/jumbo v0, "support_dark_mode_notify"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/DarkModeTimeModeManager;->SUPPORT_DARK_MODE_NOTIFY:Z

    .line 61
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object p1, p0, Lcom/android/server/DarkModeTimeModeManager;->mContext:Landroid/content/Context;

    .line 71
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 72
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    invoke-direct {p0, v0, p1}, Lcom/android/server/DarkModeTimeModeManager;->addAction(Landroid/content/IntentFilter;Landroid/content/Context;)V

    .line 73
    new-instance v1, Lcom/android/server/DarkModeTimeModeManager$1;

    invoke-direct {v1, p0}, Lcom/android/server/DarkModeTimeModeManager$1;-><init>(Lcom/android/server/DarkModeTimeModeManager;)V

    iput-object v1, p0, Lcom/android/server/DarkModeTimeModeManager;->mDarkModeTimeModeReceiver:Landroid/content/BroadcastReceiver;

    .line 95
    const/4 v2, 0x2

    invoke-virtual {p1, v1, v0, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;

    .line 96
    iget-object v1, p0, Lcom/android/server/DarkModeTimeModeManager;->mContext:Landroid/content/Context;

    const-class v2, Landroid/app/UiModeManager;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/UiModeManager;

    iput-object v1, p0, Lcom/android/server/DarkModeTimeModeManager;->mUiModeManager:Landroid/app/UiModeManager;

    .line 97
    invoke-static {p1}, Lcom/android/server/DarkModeTimeModeHelper;->getDarkModeSuggestCount(Landroid/content/Context;)I

    move-result v1

    const/4 v2, 0x3

    if-ge v1, v2, :cond_0

    .line 99
    invoke-static {}, Lcom/android/server/DarkModeSuggestProvider;->getInstance()Lcom/android/server/DarkModeSuggestProvider;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/DarkModeTimeModeManager;->mDarkModeSuggestProvider:Lcom/android/server/DarkModeSuggestProvider;

    .line 100
    invoke-virtual {v1, p1}, Lcom/android/server/DarkModeSuggestProvider;->registerDataObserver(Landroid/content/Context;)V

    .line 102
    :cond_0
    new-instance v1, Lcom/android/server/DarkModeTimeModeManager$2;

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    invoke-direct {v1, p0, v2, p1}, Lcom/android/server/DarkModeTimeModeManager$2;-><init>(Lcom/android/server/DarkModeTimeModeManager;Landroid/os/Handler;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/server/DarkModeTimeModeManager;->mDarkModeObserver:Landroid/database/ContentObserver;

    .line 112
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 113
    const-string v2, "dark_mode_enable"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/DarkModeTimeModeManager;->mDarkModeObserver:Landroid/database/ContentObserver;

    .line 112
    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 114
    return-void
.end method

.method private addAction(Landroid/content/IntentFilter;Landroid/content/Context;)V
    .locals 2
    .param p1, "it"    # Landroid/content/IntentFilter;
    .param p2, "context"    # Landroid/content/Context;

    .line 117
    const-string v0, "miui.action.intent.DARK_MODE_SUGGEST_MESSAGE"

    invoke-virtual {p1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 118
    const-string v0, "miui.action.intent.DARK_MODE_SUGGEST_ENABLE"

    invoke-virtual {p1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 119
    invoke-static {p2}, Lcom/android/server/DarkModeTimeModeHelper;->getDarkModeSuggestCount(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x3

    if-lt v0, v1, :cond_0

    sget-boolean v0, Lcom/android/server/DarkModeStatusTracker;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 121
    :cond_0
    const-string v0, "android.intent.action.USER_PRESENT"

    invoke-virtual {p1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 123
    :cond_1
    return-void
.end method

.method private creatNoticifationChannel(Ljava/lang/String;Ljava/lang/CharSequence;ILjava/lang/String;Landroid/app/NotificationManager;)V
    .locals 1
    .param p1, "channelId"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/CharSequence;
    .param p3, "importance"    # I
    .param p4, "description"    # Ljava/lang/String;
    .param p5, "manager"    # Landroid/app/NotificationManager;

    .line 271
    new-instance v0, Landroid/app/NotificationChannel;

    invoke-direct {v0, p1, p2, p3}, Landroid/app/NotificationChannel;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V

    .line 272
    .local v0, "channel":Landroid/app/NotificationChannel;
    invoke-virtual {v0, p4}, Landroid/app/NotificationChannel;->setDescription(Ljava/lang/String;)V

    .line 273
    invoke-virtual {p5, v0}, Landroid/app/NotificationManager;->createNotificationChannel(Landroid/app/NotificationChannel;)V

    .line 274
    return-void
.end method

.method private initNotification(Landroid/content/Context;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;

    .line 221
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x110f01a8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 222
    .local v0, "title":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x110f01a7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 224
    .local v1, "message":Ljava/lang/String;
    new-instance v2, Landroid/content/Intent;

    const-string v3, "miui.action.intent.DARK_MODE_SUGGEST_MESSAGE"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 225
    .local v2, "intentSettings":Landroid/content/Intent;
    const/4 v3, 0x0

    const/high16 v4, 0x4000000

    invoke-static {p1, v3, v2, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    .line 228
    .local v5, "pendingIntentSettings":Landroid/app/PendingIntent;
    new-instance v6, Landroid/content/Intent;

    const-string v7, "miui.action.intent.DARK_MODE_SUGGEST_ENABLE"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 229
    .local v6, "intentEnable":Landroid/content/Intent;
    nop

    .line 230
    invoke-static {p1, v3, v6, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 232
    .local v3, "pendingIntentEnable":Landroid/app/PendingIntent;
    const-class v4, Landroid/app/NotificationManager;

    .line 233
    invoke-virtual {p1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/NotificationManager;

    .line 235
    .local v4, "notificationManager":Landroid/app/NotificationManager;
    const-string v8, "dark_mode_suggest_id"

    const-string v9, "dark_mode"

    const/4 v10, 0x4

    const-string v11, "des"

    move-object v7, p0

    move-object v12, v4

    invoke-direct/range {v7 .. v12}, Lcom/android/server/DarkModeTimeModeManager;->creatNoticifationChannel(Ljava/lang/String;Ljava/lang/CharSequence;ILjava/lang/String;Landroid/app/NotificationManager;)V

    .line 238
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 239
    .local v7, "arg":Landroid/os/Bundle;
    const v8, 0x1108018f

    invoke-static {p1, v8}, Landroid/graphics/drawable/Icon;->createWithResource(Landroid/content/Context;I)Landroid/graphics/drawable/Icon;

    move-result-object v9

    const-string v10, "miui.appIcon"

    invoke-virtual {v7, v10, v9}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 241
    const-string v9, "miui.showAction"

    const/4 v10, 0x1

    invoke-virtual {v7, v9, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 242
    new-instance v9, Landroid/app/Notification$Builder;

    const-string v11, "dark_mode_suggest_id"

    invoke-direct {v9, p1, v11}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 243
    invoke-virtual {v9, v7}, Landroid/app/Notification$Builder;->addExtras(Landroid/os/Bundle;)Landroid/app/Notification$Builder;

    move-result-object v9

    .line 244
    invoke-virtual {v9, v8}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v8

    .line 245
    invoke-virtual {v8, v0}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v8

    .line 246
    invoke-virtual {v8, v1}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v8

    new-instance v9, Landroid/app/Notification$BigTextStyle;

    invoke-direct {v9}, Landroid/app/Notification$BigTextStyle;-><init>()V

    .line 247
    invoke-virtual {v9, v1}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/app/Notification$Builder;->setStyle(Landroid/app/Notification$Style;)Landroid/app/Notification$Builder;

    move-result-object v8

    .line 248
    invoke-virtual {v8, v5}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v8

    .line 250
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v11, 0x110f01a6

    invoke-virtual {v9, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 249
    const v11, 0x1108014e

    invoke-virtual {v8, v11, v9, v3}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v8

    .line 252
    invoke-virtual {v8, v10}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v8

    .line 253
    const-wide/16 v11, 0x1388

    invoke-virtual {v8, v11, v12}, Landroid/app/Notification$Builder;->setTimeoutAfter(J)Landroid/app/Notification$Builder;

    move-result-object v8

    .line 254
    invoke-virtual {v8}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v8

    .line 256
    .local v8, "notification":Landroid/app/Notification;
    const-string v9, "DarkModeTimeModeManager"

    sget-object v11, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {v4, v9, v10, v8, v11}, Landroid/app/NotificationManager;->notifyAsUser(Ljava/lang/String;ILandroid/app/Notification;Landroid/os/UserHandle;)V

    .line 258
    return-void
.end method

.method private updateButtonStatus(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 307
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/android/server/DarkModeTimeModeHelper;->setDarkModeTimeEnable(Landroid/content/Context;Z)V

    .line 309
    const/4 v1, 0x0

    invoke-static {p1, v1}, Lcom/android/server/DarkModeTimeModeHelper;->setDarkModeAutoTimeEnable(Landroid/content/Context;Z)V

    .line 310
    invoke-static {p1, v0}, Lcom/android/server/DarkModeTimeModeHelper;->setSunRiseSunSetMode(Landroid/content/Context;Z)V

    .line 311
    const/4 v0, 0x2

    invoke-static {p1, v0}, Lcom/android/server/DarkModeTimeModeHelper;->setDarkModeTimeType(Landroid/content/Context;I)V

    .line 312
    return-void
.end method

.method private updateDarkModeTimeModeStatus(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 134
    invoke-static {p1}, Lcom/android/server/DarkModeTimeModeHelper;->isDarkModeTimeEnable(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x0

    const-string v2, "dark_mode_switch_now"

    if-nez v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/android/server/DarkModeTimeModeManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    .line 136
    return-void

    .line 139
    :cond_0
    iget-object v0, p0, Lcom/android/server/DarkModeTimeModeManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v3, 0x1

    invoke-static {v0, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 140
    invoke-static {p1}, Lcom/android/server/DarkModeTimeModeHelper;->isSuntimeType(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 141
    iget-object v0, p0, Lcom/android/server/DarkModeTimeModeManager;->mUiModeManager:Landroid/app/UiModeManager;

    invoke-virtual {v0, v1}, Landroid/app/UiModeManager;->setNightMode(I)V

    goto :goto_0

    .line 143
    :cond_1
    iget-object v0, p0, Lcom/android/server/DarkModeTimeModeManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/DarkModeTimeModeHelper;->getDarkModeStartTime(Landroid/content/Context;)I

    move-result v0

    .line 144
    .local v0, "time":I
    div-int/lit8 v1, v0, 0x3c

    rem-int/lit8 v2, v0, 0x3c

    invoke-static {v1, v2}, Ljava/time/LocalTime;->of(II)Ljava/time/LocalTime;

    move-result-object v1

    .line 145
    .local v1, "localTime":Ljava/time/LocalTime;
    iget-object v2, p0, Lcom/android/server/DarkModeTimeModeManager;->mUiModeManager:Landroid/app/UiModeManager;

    invoke-virtual {v2, v1}, Landroid/app/UiModeManager;->setCustomNightModeStart(Ljava/time/LocalTime;)V

    .line 146
    iget-object v2, p0, Lcom/android/server/DarkModeTimeModeManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/server/DarkModeTimeModeHelper;->getDarkModeEndTime(Landroid/content/Context;)I

    move-result v0

    .line 147
    div-int/lit8 v2, v0, 0x3c

    rem-int/lit8 v3, v0, 0x3c

    invoke-static {v2, v3}, Ljava/time/LocalTime;->of(II)Ljava/time/LocalTime;

    move-result-object v1

    .line 148
    iget-object v2, p0, Lcom/android/server/DarkModeTimeModeManager;->mUiModeManager:Landroid/app/UiModeManager;

    invoke-virtual {v2, v1}, Landroid/app/UiModeManager;->setCustomNightModeEnd(Ljava/time/LocalTime;)V

    .line 149
    iget-object v2, p0, Lcom/android/server/DarkModeTimeModeManager;->mUiModeManager:Landroid/app/UiModeManager;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Landroid/app/UiModeManager;->setNightMode(I)V

    .line 151
    .end local v0    # "time":I
    .end local v1    # "localTime":Ljava/time/LocalTime;
    :goto_0
    return-void
.end method


# virtual methods
.method public canShowDarkModeSuggestNotifocation(Landroid/content/Context;)Z
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .line 160
    sget-boolean v0, Lcom/android/server/DarkModeStatusTracker;->DEBUG:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 161
    return v1

    .line 164
    :cond_0
    invoke-static {p1}, Lcom/android/server/DarkModeTimeModeHelper;->isDarkModeSuggestEnable(Landroid/content/Context;)Z

    move-result v0

    const/4 v2, 0x0

    const-string v3, "DarkModeTimeModeManager"

    if-nez v0, :cond_1

    .line 165
    const-string v0, "not get suggest from cloud"

    invoke-static {v3, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    return v2

    .line 170
    :cond_1
    invoke-static {p1}, Lcom/android/server/DarkModeTimeModeHelper;->getDarkModeSuggestCount(Landroid/content/Context;)I

    move-result v0

    const/4 v4, 0x3

    if-lt v0, v4, :cond_2

    .line 171
    const-string v0, "count >= 3"

    invoke-static {v3, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    return v2

    .line 175
    :cond_2
    invoke-static {p1}, Lcom/android/server/DarkModeTimeModeHelper;->isDarkModeOpen(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 176
    const-string v0, "darkMode is open"

    invoke-static {v3, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    return v2

    .line 181
    :cond_3
    invoke-static {p1}, Lcom/android/server/DarkModeTimeModeHelper;->isInNight(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 182
    const-string v0, "not in night"

    invoke-static {v3, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    return v2

    .line 186
    :cond_4
    invoke-static {}, Lcom/android/server/DarkModeTimeModeHelper;->getNowTimeInMills()J

    move-result-wide v4

    .line 187
    invoke-static {p1}, Lcom/android/server/DarkModeTimeModeHelper;->getLastSuggestTime(Landroid/content/Context;)J

    move-result-wide v6

    sub-long/2addr v4, v6

    const-wide/32 v6, 0x240c8400

    cmp-long v0, v4, v6

    if-gez v0, :cond_5

    .line 188
    const-string v0, "less than 7 days ago"

    invoke-static {v3, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    return v2

    .line 192
    :cond_5
    invoke-static {p1}, Lcom/android/server/DarkModeTimeModeHelper;->isOnHome(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 193
    const-string v0, "not on home"

    invoke-static {v3, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    return v2

    .line 196
    :cond_6
    return v1
.end method

.method public enterSettingsFromNotification(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .line 322
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "enter_setting_by_notification"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 324
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.DISPLAY_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 325
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 326
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 328
    const/4 v1, 0x3

    invoke-static {p1, v1}, Lcom/android/server/DarkModeTimeModeHelper;->setDarkModeSuggestCount(Landroid/content/Context;I)V

    .line 329
    new-instance v1, Lcom/android/server/DarkModeStauesEvent;

    invoke-direct {v1}, Lcom/android/server/DarkModeStauesEvent;-><init>()V

    .line 330
    const-string v3, "darkModeSuggest"

    invoke-virtual {v1, v3}, Lcom/android/server/DarkModeStauesEvent;->setEventName(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;

    move-result-object v1

    .line 331
    const-string v3, ""

    invoke-virtual {v1, v3}, Lcom/android/server/DarkModeStauesEvent;->setTip(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;

    move-result-object v1

    .line 332
    invoke-virtual {v1, v2}, Lcom/android/server/DarkModeStauesEvent;->setSuggestClick(I)Lcom/android/server/DarkModeStauesEvent;

    move-result-object v1

    .line 333
    .local v1, "event":Lcom/android/server/DarkModeStauesEvent;
    invoke-static {p1, v1}, Lcom/android/server/DarkModeOneTrackHelper;->uploadToOneTrack(Landroid/content/Context;Lcom/android/server/DarkModeEvent;)V

    .line 335
    invoke-static {}, Lcom/android/server/MiuiBgThread;->getHandler()Landroid/os/Handler;

    move-result-object v2

    new-instance v3, Lcom/android/server/DarkModeTimeModeManager$3;

    invoke-direct {v3, p0, p1}, Lcom/android/server/DarkModeTimeModeManager$3;-><init>(Lcom/android/server/DarkModeTimeModeManager;Landroid/content/Context;)V

    const-wide/32 v4, 0xea60

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 350
    return-void
.end method

.method public onBootPhase(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .line 126
    invoke-direct {p0, p1}, Lcom/android/server/DarkModeTimeModeManager;->updateDarkModeTimeModeStatus(Landroid/content/Context;)V

    .line 127
    return-void
.end method

.method public showDarkModeSuggestNotification(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 205
    const-string v0, "DarkModeTimeModeManager"

    const-string/jumbo v1, "showDarkModeSuggestNotification"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    invoke-direct {p0, p1}, Lcom/android/server/DarkModeTimeModeManager;->initNotification(Landroid/content/Context;)V

    .line 207
    nop

    .line 208
    invoke-static {p1}, Lcom/android/server/DarkModeTimeModeHelper;->getDarkModeSuggestCount(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x1

    add-int/2addr v0, v1

    .line 207
    invoke-static {p1, v0}, Lcom/android/server/DarkModeTimeModeHelper;->setDarkModeSuggestCount(Landroid/content/Context;I)V

    .line 210
    invoke-static {}, Lcom/android/server/DarkModeSuggestProvider;->getInstance()Lcom/android/server/DarkModeSuggestProvider;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/DarkModeSuggestProvider;->unRegisterDataObserver(Landroid/content/Context;)V

    .line 212
    invoke-static {}, Lcom/android/server/DarkModeTimeModeHelper;->getNowTimeInMills()J

    move-result-wide v2

    invoke-static {p1, v2, v3}, Lcom/android/server/DarkModeTimeModeHelper;->setLastSuggestTime(Landroid/content/Context;J)V

    .line 213
    new-instance v0, Lcom/android/server/DarkModeStauesEvent;

    invoke-direct {v0}, Lcom/android/server/DarkModeStauesEvent;-><init>()V

    .line 214
    const-string v2, "darkModeSuggest"

    invoke-virtual {v0, v2}, Lcom/android/server/DarkModeStauesEvent;->setEventName(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;

    move-result-object v0

    .line 215
    const-string v2, ""

    invoke-virtual {v0, v2}, Lcom/android/server/DarkModeStauesEvent;->setTip(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;

    move-result-object v0

    .line 216
    invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setSuggest(I)Lcom/android/server/DarkModeStauesEvent;

    move-result-object v0

    .line 217
    .local v0, "event":Lcom/android/server/DarkModeStauesEvent;
    invoke-static {p1, v0}, Lcom/android/server/DarkModeOneTrackHelper;->uploadToOneTrack(Landroid/content/Context;Lcom/android/server/DarkModeEvent;)V

    .line 218
    return-void
.end method

.method public showDarkModeSuggestToast(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 288
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "open_sun_time_channel"

    const-string/jumbo v2, "\u5f39\u7a97"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 290
    invoke-direct {p0, p1}, Lcom/android/server/DarkModeTimeModeManager;->updateButtonStatus(Landroid/content/Context;)V

    .line 291
    new-instance v0, Lcom/android/server/DarkModeStauesEvent;

    invoke-direct {v0}, Lcom/android/server/DarkModeStauesEvent;-><init>()V

    .line 292
    const-string v1, "darkModeSuggest"

    invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setEventName(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;

    move-result-object v0

    .line 293
    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setTip(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;

    move-result-object v0

    .line 294
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setSuggestEnable(I)Lcom/android/server/DarkModeStauesEvent;

    move-result-object v0

    .line 295
    .local v0, "event":Lcom/android/server/DarkModeStauesEvent;
    invoke-static {p1, v0}, Lcom/android/server/DarkModeOneTrackHelper;->uploadToOneTrack(Landroid/content/Context;Lcom/android/server/DarkModeEvent;)V

    .line 297
    const/4 v1, 0x3

    invoke-static {p1, v1}, Lcom/android/server/DarkModeTimeModeHelper;->setDarkModeSuggestCount(Landroid/content/Context;I)V

    .line 298
    nop

    .line 299
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x110f01a9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 298
    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 300
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 301
    return-void
.end method
