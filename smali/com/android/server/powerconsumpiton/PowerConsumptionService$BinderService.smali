.class final Lcom/android/server/powerconsumpiton/PowerConsumptionService$BinderService;
.super Landroid/powerconsumption/IMiuiPowerConsumptionManager$Stub;
.source "PowerConsumptionService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/powerconsumpiton/PowerConsumptionService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "BinderService"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/powerconsumpiton/PowerConsumptionService;


# direct methods
.method private constructor <init>(Lcom/android/server/powerconsumpiton/PowerConsumptionService;)V
    .locals 0

    .line 64
    iput-object p1, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionService$BinderService;->this$0:Lcom/android/server/powerconsumpiton/PowerConsumptionService;

    invoke-direct {p0}, Landroid/powerconsumption/IMiuiPowerConsumptionManager$Stub;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/powerconsumpiton/PowerConsumptionService;Lcom/android/server/powerconsumpiton/PowerConsumptionService$BinderService-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/powerconsumpiton/PowerConsumptionService$BinderService;-><init>(Lcom/android/server/powerconsumpiton/PowerConsumptionService;)V

    return-void
.end method

.method private doDump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 7
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "pw"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;

    .line 83
    iget-object v0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionService$BinderService;->this$0:Lcom/android/server/powerconsumpiton/PowerConsumptionService;

    invoke-static {v0}, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->-$$Nest$fgetmContext(Lcom/android/server/powerconsumpiton/PowerConsumptionService;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "PowerConsumptionService"

    invoke-static {v0, v1, p2}, Lcom/android/internal/util/DumpUtils;->checkDumpPermission(Landroid/content/Context;Ljava/lang/String;Ljava/io/PrintWriter;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 85
    :cond_0
    const/4 v0, 0x0

    .line 86
    .local v0, "opti":I
    array-length v1, p3

    const-string v2, "; use -h for help"

    const/4 v3, 0x1

    if-ge v0, v1, :cond_9

    .line 87
    aget-object v1, p3, v0

    .line 88
    .local v1, "opt":Ljava/lang/String;
    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_9

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x2d

    if-eq v5, v6, :cond_1

    .line 89
    goto/16 :goto_1

    .line 91
    :cond_1
    add-int/lit8 v0, v0, 0x1

    .line 92
    const-string v5, "-a"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 93
    goto/16 :goto_1

    .line 94
    :cond_2
    const-string v5, "-h"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 95
    const-string v2, "Power consumption dump options:"

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 96
    const-string v2, " [-a] [-h] [-debug] [cmd] ..."

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 97
    const-string v2, "  cmd may be one of:"

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 98
    const-string v2, "    s[status]: system status"

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 99
    const-string v2, "    d[data]: cloud data"

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 100
    const-string v2, "    p[policy]: current policy"

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 101
    const-string v2, "  -a: include all available server state."

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 102
    const-string v2, "  -debug: debug switch in powerconsumption."

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 103
    iget-object v2, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionService$BinderService;->this$0:Lcom/android/server/powerconsumpiton/PowerConsumptionService;

    invoke-static {v2}, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->-$$Nest$fgetmIsSupportPCS(Lcom/android/server/powerconsumpiton/PowerConsumptionService;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 104
    const-string v2, " this device  donot support PowerConsumption"

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 106
    :cond_3
    const-string v5, "-debug"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 107
    array-length v2, p3

    if-ge v0, v2, :cond_6

    .line 108
    aget-object v1, p3, v0

    .line 109
    const-string/jumbo v2, "true"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 110
    invoke-static {v3}, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->-$$Nest$sfputsDEBUG(Z)V

    .line 111
    const-string v2, "debug is true"

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 112
    :cond_4
    const-string v2, "false"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 113
    invoke-static {v4}, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->-$$Nest$sfputsDEBUG(Z)V

    .line 114
    const-string v2, "debug is false"

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 116
    :cond_5
    const-string/jumbo v2, "wrong argument for debug; use -h for help"

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 119
    :cond_6
    const-string v2, "empty argument for debug; use -h for help"

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 122
    :cond_7
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown argument: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 124
    :cond_8
    :goto_0
    return-void

    .line 128
    .end local v1    # "opt":Ljava/lang/String;
    :cond_9
    :goto_1
    array-length v1, p3

    if-ge v0, v1, :cond_10

    .line 129
    aget-object v1, p3, v0

    .line 130
    .local v1, "cmd":Ljava/lang/String;
    add-int/2addr v0, v3

    .line 131
    const-string/jumbo v3, "status"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_f

    const-string v3, "s"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    goto :goto_4

    .line 133
    :cond_a
    const-string v3, "data"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_e

    const-string v3, "d"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    goto :goto_3

    .line 135
    :cond_b
    const-string v3, "policy"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_d

    const-string v3, "p"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    goto :goto_2

    .line 138
    :cond_c
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Bad command: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_5

    .line 136
    :cond_d
    :goto_2
    invoke-direct {p0, p2}, Lcom/android/server/powerconsumpiton/PowerConsumptionService$BinderService;->dumpPolicy(Ljava/io/PrintWriter;)V

    goto :goto_5

    .line 134
    :cond_e
    :goto_3
    invoke-direct {p0, p2}, Lcom/android/server/powerconsumpiton/PowerConsumptionService$BinderService;->dumpCouldData(Ljava/io/PrintWriter;)V

    goto :goto_5

    .line 132
    :cond_f
    :goto_4
    invoke-direct {p0, p2}, Lcom/android/server/powerconsumpiton/PowerConsumptionService$BinderService;->dumpSystemStatus(Ljava/io/PrintWriter;)V

    .line 140
    :goto_5
    return-void

    .line 142
    .end local v1    # "cmd":Ljava/lang/String;
    :cond_10
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    .line 143
    const-string v1, "-------------------------------------------------------------------------------"

    .line 145
    .local v1, "separator":Ljava/lang/String;
    const-string v2, "-------------------------------------------------------------------------------"

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 146
    invoke-direct {p0, p2}, Lcom/android/server/powerconsumpiton/PowerConsumptionService$BinderService;->dumpSystemStatus(Ljava/io/PrintWriter;)V

    .line 147
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 148
    invoke-direct {p0, p2}, Lcom/android/server/powerconsumpiton/PowerConsumptionService$BinderService;->dumpCouldData(Ljava/io/PrintWriter;)V

    .line 149
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 150
    invoke-direct {p0, p2}, Lcom/android/server/powerconsumpiton/PowerConsumptionService$BinderService;->dumpPolicy(Ljava/io/PrintWriter;)V

    .line 151
    return-void
.end method

.method private dumpCouldData(Ljava/io/PrintWriter;)V
    .locals 1
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 154
    const-string v0, "POWER CONSUMPTION Cloud Data (dumpsys powerconsumption data)"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 155
    iget-object v0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionService$BinderService;->this$0:Lcom/android/server/powerconsumpiton/PowerConsumptionService;

    invoke-static {v0}, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->-$$Nest$fgetmPowerConsumptionServiceCouldData(Lcom/android/server/powerconsumpiton/PowerConsumptionService;)Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;->dump(Ljava/io/PrintWriter;)V

    .line 156
    return-void
.end method

.method private dumpPolicy(Ljava/io/PrintWriter;)V
    .locals 1
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 164
    const-string v0, "POWER CONSUMPTION CURRENT POLICY (dumpsys powerconsumption policy)"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 165
    iget-object v0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionService$BinderService;->this$0:Lcom/android/server/powerconsumpiton/PowerConsumptionService;

    invoke-static {v0}, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->-$$Nest$fgetmPowerConsumptionServiceController(Lcom/android/server/powerconsumpiton/PowerConsumptionService;)Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->dump(Ljava/io/PrintWriter;)V

    .line 166
    return-void
.end method

.method private dumpSystemStatus(Ljava/io/PrintWriter;)V
    .locals 1
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 159
    const-string v0, "POWER CONSUMPTION SYSTEM STATE (dumpsys powerconsumption status)"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 160
    iget-object v0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionService$BinderService;->this$0:Lcom/android/server/powerconsumpiton/PowerConsumptionService;

    invoke-static {v0}, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->-$$Nest$fgetmSystemStatusListener(Lcom/android/server/powerconsumpiton/PowerConsumptionService;)Lcom/android/server/powerconsumpiton/SystemStatusListener;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/powerconsumpiton/SystemStatusListener;->dump(Ljava/io/PrintWriter;)V

    .line 161
    return-void
.end method


# virtual methods
.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 0
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "pw"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;

    .line 79
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/powerconsumpiton/PowerConsumptionService$BinderService;->doDump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 80
    return-void
.end method

.method public isContentKeyInPowerConsumptionWhiteList()Z
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionService$BinderService;->this$0:Lcom/android/server/powerconsumpiton/PowerConsumptionService;

    invoke-static {v0}, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->-$$Nest$fgetmPowerConsumptionServiceController(Lcom/android/server/powerconsumpiton/PowerConsumptionService;)Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->getContentKeyInWhiteList()Z

    move-result v0

    return v0
.end method

.method public setPowerConsumptionPolicy(I)V
    .locals 2
    .param p1, "flag"    # I

    .line 67
    invoke-static {}, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->-$$Nest$sfgetsDEBUG()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setPowerConsumptionPolicy flag "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PowerConsumptionService"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    :cond_0
    return-void
.end method
