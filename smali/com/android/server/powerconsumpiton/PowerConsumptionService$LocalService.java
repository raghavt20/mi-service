class com.android.server.powerconsumpiton.PowerConsumptionService$LocalService extends com.android.server.PowerConsumptionServiceInternal {
	 /* .source "PowerConsumptionService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/powerconsumpiton/PowerConsumptionService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "LocalService" */
} // .end annotation
/* # instance fields */
final com.android.server.powerconsumpiton.PowerConsumptionService this$0; //synthetic
/* # direct methods */
private com.android.server.powerconsumpiton.PowerConsumptionService$LocalService ( ) {
/* .locals 0 */
/* .line 169 */
this.this$0 = p1;
/* invoke-direct {p0}, Lcom/android/server/PowerConsumptionServiceInternal;-><init>()V */
return;
} // .end method
 com.android.server.powerconsumpiton.PowerConsumptionService$LocalService ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/powerconsumpiton/PowerConsumptionService$LocalService;-><init>(Lcom/android/server/powerconsumpiton/PowerConsumptionService;)V */
return;
} // .end method
/* # virtual methods */
public void noteFoucsChangeForPowerConsumption ( java.lang.String p0, com.android.server.wm.IMiuiWindowStateEx p1 ) {
/* .locals 2 */
/* .param p1, "name" # Ljava/lang/String; */
/* .param p2, "miuiWindowStateEx" # Lcom/android/server/wm/IMiuiWindowStateEx; */
/* .line 184 */
v0 = this.this$0;
v0 = com.android.server.powerconsumpiton.PowerConsumptionService .-$$Nest$fgetmIsSupportPCS ( v0 );
/* if-nez v0, :cond_0 */
/* .line 185 */
return;
/* .line 187 */
} // :cond_0
v0 = com.android.server.powerconsumpiton.PowerConsumptionService .-$$Nest$sfgetsDEBUG ( );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 188 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "noteFoucsChangeForPowerConsumption "; // const-string v1, "noteFoucsChangeForPowerConsumption "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "PowerConsumptionService"; // const-string v1, "PowerConsumptionService"
android.util.Slog .d ( v1,v0 );
/* .line 190 */
} // :cond_1
/* new-instance v0, Lcom/android/server/powerconsumpiton/PowerConsumptionService$LocalService$$ExternalSyntheticLambda0; */
/* invoke-direct {v0}, Lcom/android/server/powerconsumpiton/PowerConsumptionService$LocalService$$ExternalSyntheticLambda0;-><init>()V */
v1 = this.this$0;
com.android.server.powerconsumpiton.PowerConsumptionService .-$$Nest$fgetmPowerConsumptionServiceController ( v1 );
com.android.internal.util.function.pooled.PooledLambda .obtainMessage ( v0,v1,p1,p2 );
/* .line 191 */
/* .local v0, "message":Landroid/os/Message; */
v1 = this.this$0;
com.android.server.powerconsumpiton.PowerConsumptionService .-$$Nest$fgetmPCFgHandler ( v1 );
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 192 */
return;
} // .end method
public void noteStartActivityForPowerConsumption ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "record" # Ljava/lang/String; */
/* .line 172 */
v0 = this.this$0;
v0 = com.android.server.powerconsumpiton.PowerConsumptionService .-$$Nest$fgetmIsSupportPCS ( v0 );
/* if-nez v0, :cond_0 */
/* .line 173 */
return;
/* .line 175 */
} // :cond_0
v0 = com.android.server.powerconsumpiton.PowerConsumptionService .-$$Nest$sfgetsDEBUG ( );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 176 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "noteStartActivityForPowerConsumption "; // const-string v1, "noteStartActivityForPowerConsumption "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "PowerConsumptionService"; // const-string v1, "PowerConsumptionService"
android.util.Slog .d ( v1,v0 );
/* .line 178 */
} // :cond_1
/* new-instance v0, Lcom/android/server/powerconsumpiton/PowerConsumptionService$LocalService$$ExternalSyntheticLambda1; */
/* invoke-direct {v0}, Lcom/android/server/powerconsumpiton/PowerConsumptionService$LocalService$$ExternalSyntheticLambda1;-><init>()V */
v1 = this.this$0;
com.android.server.powerconsumpiton.PowerConsumptionService .-$$Nest$fgetmPowerConsumptionServiceController ( v1 );
com.android.internal.util.function.pooled.PooledLambda .obtainMessage ( v0,v1,p1 );
/* .line 179 */
/* .local v0, "message":Landroid/os/Message; */
v1 = this.this$0;
com.android.server.powerconsumpiton.PowerConsumptionService .-$$Nest$fgetmPCFgHandler ( v1 );
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 180 */
return;
} // .end method
