.class public final Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;
.super Ljava/lang/Object;
.source "PowerConsumptionServiceController.java"


# static fields
.field protected static final TAG:Ljava/lang/String; = "PowerConsumptionServiceController"

.field private static sDEBUG:Z


# instance fields
.field private mActivityPolicies:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "[I>;"
        }
    .end annotation
.end field

.field private mClickScreenTime:J

.field private mContext:Landroid/content/Context;

.field private mCurActivityName:Ljava/lang/String;

.field private mCurMiuiWindowStateEx:Lcom/android/server/wm/IMiuiWindowStateEx;

.field private mCurPolicies:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "[I>;"
        }
    .end annotation
.end field

.field private mCurWindowName:Ljava/lang/String;

.field private mDimWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mExeHandler:Landroid/os/Handler;

.field private mIsContentKeyInWhiteList:Z

.field private final mPowerConsumptionServiceCouldData:Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;

.field private mPowerManager:Landroid/os/PowerManager;

.field private mPowerManagerServiceImpl:Lcom/android/server/power/PowerManagerServiceStub;

.field private mSystemStatusListener:Lcom/android/server/powerconsumpiton/SystemStatusListener;

.field private mWindowPolicies:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "[I>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 19
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->sDEBUG:Z

    return-void
.end method

.method public constructor <init>(Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;Lcom/android/server/powerconsumpiton/SystemStatusListener;Landroid/os/Handler;Landroid/content/Context;)V
    .locals 1
    .param p1, "powerConsumptionServiceCouldData"    # Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;
    .param p2, "systemStatusListener"    # Lcom/android/server/powerconsumpiton/SystemStatusListener;
    .param p3, "exeHandler"    # Landroid/os/Handler;
    .param p4, "context"    # Landroid/content/Context;

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mIsContentKeyInWhiteList:Z

    .line 32
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    iput-object v0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mCurActivityName:Ljava/lang/String;

    .line 33
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    iput-object v0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mCurWindowName:Ljava/lang/String;

    .line 39
    iput-object p1, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mPowerConsumptionServiceCouldData:Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;

    .line 40
    iput-object p2, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mSystemStatusListener:Lcom/android/server/powerconsumpiton/SystemStatusListener;

    .line 41
    iput-object p3, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mExeHandler:Landroid/os/Handler;

    .line 42
    iput-object p4, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mContext:Landroid/content/Context;

    .line 43
    const-string v0, "power"

    invoke-virtual {p4, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mPowerManager:Landroid/os/PowerManager;

    .line 44
    invoke-direct {p0}, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->init()V

    .line 45
    return-void
.end method

.method private getActivityPolicy(Ljava/lang/String;)[I
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .line 64
    const/4 v0, 0x0

    new-array v0, v0, [I

    .line 65
    .local v0, "policy":[I
    iget-object v1, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mActivityPolicies:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, [I

    .line 66
    return-object v0
.end method

.method private getWindowPolicies(Ljava/lang/String;)[I
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .line 70
    const/4 v0, 0x0

    new-array v0, v0, [I

    .line 71
    .local v0, "policy":[I
    iget-object v1, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mWindowPolicies:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, [I

    .line 72
    return-object v0
.end method

.method private init()V
    .locals 3

    .line 48
    iget-object v0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mPowerConsumptionServiceCouldData:Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;

    invoke-virtual {v0}, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;->getActivityPolicy()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mActivityPolicies:Ljava/util/HashMap;

    .line 49
    iget-object v0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mPowerConsumptionServiceCouldData:Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;

    invoke-virtual {v0}, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;->getWindowPolicies()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mWindowPolicies:Ljava/util/HashMap;

    .line 50
    invoke-static {}, Lcom/android/server/power/PowerManagerServiceStub;->get()Lcom/android/server/power/PowerManagerServiceStub;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mPowerManagerServiceImpl:Lcom/android/server/power/PowerManagerServiceStub;

    .line 51
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mCurPolicies:Ljava/util/HashMap;

    .line 52
    iget-object v0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mPowerManager:Landroid/os/PowerManager;

    const/4 v1, 0x6

    const-string v2, "WAKEUP-FROM-CLOUD-DIM"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mDimWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 53
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 54
    return-void
.end method


# virtual methods
.method dump(Ljava/io/PrintWriter;)V
    .locals 5
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 181
    iget-object v0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mCurPolicies:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mCurActivityName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    .line 182
    .local v0, "activityPolicy":[I
    iget-object v1, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mCurPolicies:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mCurWindowName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [I

    .line 183
    .local v1, "windowPolicy":[I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "  mCurActivityName="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mCurActivityName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; mCurPolicy="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 184
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  mCurWindowName="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mCurWindowName:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v1}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 185
    return-void
.end method

.method public getContentKeyInWhiteList()Z
    .locals 2

    .line 57
    sget-boolean v0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->sDEBUG:Z

    if-eqz v0, :cond_0

    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mIsContentKeyInWhiteList "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mIsContentKeyInWhiteList:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PowerConsumptionServiceController"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mIsContentKeyInWhiteList:Z

    return v0
.end method

.method public noteFoucsChangeForPowerConsumptionLocked(Ljava/lang/String;Lcom/android/server/wm/IMiuiWindowStateEx;)V
    .locals 11
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "miuiWindowStateEx"    # Lcom/android/server/wm/IMiuiWindowStateEx;

    .line 103
    invoke-direct {p0, p1}, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->getWindowPolicies(Ljava/lang/String;)[I

    move-result-object v0

    .line 104
    .local v0, "windowPolicies":[I
    iget-object v1, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mCurPolicies:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mCurWindowName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [I

    .line 105
    .local v1, "curPolicies":[I
    iget-object v2, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mCurWindowName:Ljava/lang/String;

    const/4 v3, 0x0

    if-eq v2, p1, :cond_0

    if-eqz v1, :cond_0

    .line 106
    aget v4, v1, v3

    and-int/lit8 v4, v4, -0x21

    aput v4, v1, v3

    .line 107
    iget-object v4, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mCurPolicies:Ljava/util/HashMap;

    invoke-virtual {v4, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    :cond_0
    iget-object v2, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mCurMiuiWindowStateEx:Lcom/android/server/wm/IMiuiWindowStateEx;

    if-eqz v2, :cond_1

    invoke-interface {v2}, Lcom/android/server/wm/IMiuiWindowStateEx;->isDimWindow()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 110
    iget-object v2, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mCurMiuiWindowStateEx:Lcom/android/server/wm/IMiuiWindowStateEx;

    const/4 v4, -0x1

    invoke-interface {v2, v4}, Lcom/android/server/wm/IMiuiWindowStateEx;->setMiuiUserActivityTimeOut(I)V

    .line 111
    iget-object v2, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mDimWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 112
    iget-object v2, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mDimWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 115
    :cond_1
    iput-object p1, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mCurWindowName:Ljava/lang/String;

    .line 116
    iput-object p2, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mCurMiuiWindowStateEx:Lcom/android/server/wm/IMiuiWindowStateEx;

    .line 117
    const/4 v2, 0x0

    .line 119
    .local v2, "isUpdate":Z
    if-eqz v0, :cond_5

    array-length v4, v0

    if-lez v4, :cond_5

    .line 120
    aget v4, v0, v3

    .line 121
    .local v4, "policy":I
    and-int/lit8 v5, v4, 0x20

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mSystemStatusListener:Lcom/android/server/powerconsumpiton/SystemStatusListener;

    .line 122
    invoke-virtual {v5}, Lcom/android/server/powerconsumpiton/SystemStatusListener;->isCameraOpen()Z

    move-result v5

    if-nez v5, :cond_4

    .line 123
    array-length v5, v0

    const/4 v6, 0x1

    if-le v5, v6, :cond_4

    .line 124
    aget v5, v0, v6

    .line 125
    .local v5, "dimTime":I
    const/4 v7, 0x2

    aget v7, v0, v7

    if-ne v7, v6, :cond_2

    move v7, v6

    goto :goto_0

    :cond_2
    move v7, v3

    .line 126
    .local v7, "isDimAssist":Z
    :goto_0
    move-object v8, p2

    .line 127
    .local v8, "powerConsumptionWindow":Lcom/android/server/wm/IMiuiWindowStateEx;
    invoke-interface {v8}, Lcom/android/server/wm/IMiuiWindowStateEx;->isCloudDimWindowingMode()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 128
    invoke-interface {v8, v5}, Lcom/android/server/wm/IMiuiWindowStateEx;->setMiuiUserActivityTimeOut(I)V

    .line 129
    invoke-interface {v8, v7}, Lcom/android/server/wm/IMiuiWindowStateEx;->setDimAssist(Z)V

    .line 130
    invoke-interface {v8, v6}, Lcom/android/server/wm/IMiuiWindowStateEx;->setIsDimWindow(Z)V

    .line 131
    const/4 v2, 0x1

    .line 132
    invoke-interface {v8}, Lcom/android/server/wm/IMiuiWindowStateEx;->isKeepScreenOnFlag()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 133
    iget-object v6, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mDimWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v6}, Landroid/os/PowerManager$WakeLock;->acquire()V

    goto :goto_1

    .line 135
    :cond_3
    invoke-interface {v8}, Lcom/android/server/wm/IMiuiWindowStateEx;->getScreenOffTime()J

    move-result-wide v9

    .line 136
    .local v9, "timeout":J
    iget-object v6, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mDimWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v6, v9, v10}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    .line 138
    .end local v9    # "timeout":J
    :goto_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v9

    iput-wide v9, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mClickScreenTime:J

    .line 143
    .end local v5    # "dimTime":I
    .end local v7    # "isDimAssist":Z
    .end local v8    # "powerConsumptionWindow":Lcom/android/server/wm/IMiuiWindowStateEx;
    :cond_4
    if-eqz v2, :cond_5

    .line 144
    array-length v5, v0

    new-array v5, v5, [I

    .line 145
    .local v5, "policyCopy":[I
    array-length v6, v0

    invoke-static {v0, v3, v5, v3, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 146
    iget-object v3, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mCurPolicies:Ljava/util/HashMap;

    invoke-virtual {v3, p1, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    .end local v4    # "policy":I
    .end local v5    # "policyCopy":[I
    :cond_5
    return-void
.end method

.method public noteStartActivityForPowerConsumption(Ljava/lang/String;)V
    .locals 6
    .param p1, "name"    # Ljava/lang/String;

    .line 76
    invoke-direct {p0, p1}, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->getActivityPolicy(Ljava/lang/String;)[I

    move-result-object v0

    .line 77
    .local v0, "activityPolicies":[I
    iput-object p1, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mCurActivityName:Ljava/lang/String;

    .line 78
    const/4 v1, 0x0

    if-eqz v0, :cond_3

    array-length v2, v0

    if-lez v2, :cond_3

    iget-object v2, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mSystemStatusListener:Lcom/android/server/powerconsumpiton/SystemStatusListener;

    .line 79
    invoke-virtual {v2}, Lcom/android/server/powerconsumpiton/SystemStatusListener;->isPowerSaveMode()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 80
    aget v2, v0, v1

    .line 81
    .local v2, "flag":I
    const/4 v3, 0x0

    .line 82
    .local v3, "isUpdate":Z
    and-int/lit8 v4, v2, 0x10

    if-eqz v4, :cond_0

    .line 83
    const/4 v3, 0x1

    .line 84
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mIsContentKeyInWhiteList:Z

    .line 86
    :cond_0
    if-eqz v3, :cond_1

    .line 87
    array-length v4, v0

    new-array v4, v4, [I

    .line 88
    .local v4, "policyCopy":[I
    array-length v5, v0

    invoke-static {v0, v1, v4, v1, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 89
    iget-object v1, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mCurPolicies:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    .end local v4    # "policyCopy":[I
    goto :goto_0

    .line 92
    :cond_1
    sget-boolean v1, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->sDEBUG:Z

    if-eqz v1, :cond_2

    .line 93
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "this policy "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " is working now "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v4, "PowerConsumptionServiceController"

    invoke-static {v4, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    .end local v2    # "flag":I
    .end local v3    # "isUpdate":Z
    :cond_2
    :goto_0
    goto :goto_1

    .line 98
    :cond_3
    iput-boolean v1, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mIsContentKeyInWhiteList:Z

    .line 100
    :goto_1
    return-void
.end method

.method public resetVsyncRate(Lcom/android/server/wm/IMiuiWindowStateEx;)V
    .locals 2
    .param p1, "w"    # Lcom/android/server/wm/IMiuiWindowStateEx;

    .line 174
    sget-boolean v0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->sDEBUG:Z

    if-eqz v0, :cond_0

    .line 175
    const-string v0, "PowerConsumptionServiceController"

    const-string v1, "Reset Vsync Rate."

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    :cond_0
    invoke-interface {p1}, Lcom/android/server/wm/IMiuiWindowStateEx;->resetVsyncRate()V

    .line 178
    return-void
.end method

.method public setScreenOffTimeOutDelay()V
    .locals 3

    .line 156
    iget-object v0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mCurMiuiWindowStateEx:Lcom/android/server/wm/IMiuiWindowStateEx;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/android/server/wm/IMiuiWindowStateEx;->isDimWindow()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 157
    iget-object v0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mCurMiuiWindowStateEx:Lcom/android/server/wm/IMiuiWindowStateEx;

    invoke-interface {v0}, Lcom/android/server/wm/IMiuiWindowStateEx;->isKeepScreenOnFlag()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    return-void

    .line 160
    :cond_0
    iget-object v0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mDimWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_1

    .line 161
    iget-object v1, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mCurMiuiWindowStateEx:Lcom/android/server/wm/IMiuiWindowStateEx;

    invoke-interface {v1}, Lcom/android/server/wm/IMiuiWindowStateEx;->getScreenOffTime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    .line 164
    :cond_1
    return-void
.end method

.method public setVsyncRate(Lcom/android/server/wm/IMiuiWindowStateEx;I)V
    .locals 2
    .param p1, "w"    # Lcom/android/server/wm/IMiuiWindowStateEx;
    .param p2, "rate"    # I

    .line 167
    sget-boolean v0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->sDEBUG:Z

    if-eqz v0, :cond_0

    .line 168
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Set Vsync Rate, VSync Rate is: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PowerConsumptionServiceController"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    :cond_0
    invoke-interface {p1, p2}, Lcom/android/server/wm/IMiuiWindowStateEx;->setVsyncRate(I)V

    .line 171
    return-void
.end method

.method public updateDimState(ZJ)V
    .locals 1
    .param p1, "dimEnable"    # Z
    .param p2, "brightWaitTime"    # J

    .line 152
    iget-object v0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->mPowerManagerServiceImpl:Lcom/android/server/power/PowerManagerServiceStub;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/power/PowerManagerServiceStub;->updateDimState(ZJ)V

    .line 153
    return-void
.end method
