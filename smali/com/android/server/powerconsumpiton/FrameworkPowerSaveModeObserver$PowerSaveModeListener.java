class com.android.server.powerconsumpiton.FrameworkPowerSaveModeObserver$PowerSaveModeListener extends android.os.FileObserver {
	 /* .source "FrameworkPowerSaveModeObserver.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "PowerSaveModeListener" */
} // .end annotation
/* # instance fields */
final com.android.server.powerconsumpiton.FrameworkPowerSaveModeObserver this$0; //synthetic
/* # direct methods */
public com.android.server.powerconsumpiton.FrameworkPowerSaveModeObserver$PowerSaveModeListener ( ) {
/* .locals 0 */
/* .param p2, "path" # Ljava/lang/String; */
/* .line 63 */
this.this$0 = p1;
/* .line 64 */
/* invoke-direct {p0, p2}, Landroid/os/FileObserver;-><init>(Ljava/lang/String;)V */
/* .line 65 */
com.android.server.powerconsumpiton.FrameworkPowerSaveModeObserver .-$$Nest$mcheckPowerSaveModeState ( p1,p2 );
/* .line 66 */
return;
} // .end method
/* # virtual methods */
public void onEvent ( Integer p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "event" # I */
/* .param p2, "path" # Ljava/lang/String; */
/* .line 70 */
/* packed-switch p1, :pswitch_data_0 */
/* .line 72 */
/* :pswitch_0 */
v0 = this.this$0;
final String v1 = "/sys/class/thermal/power_save/powersave_mode"; // const-string v1, "/sys/class/thermal/power_save/powersave_mode"
com.android.server.powerconsumpiton.FrameworkPowerSaveModeObserver .-$$Nest$mcheckPowerSaveModeState ( v0,v1 );
/* .line 73 */
/* nop */
/* .line 77 */
} // :goto_0
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x2 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
