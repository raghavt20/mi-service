.class public Lcom/android/server/powerconsumpiton/PowerConsumptionService;
.super Lcom/android/server/SystemService;
.source "PowerConsumptionService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/powerconsumpiton/PowerConsumptionService$LocalService;,
        Lcom/android/server/powerconsumpiton/PowerConsumptionService$BinderService;
    }
.end annotation


# static fields
.field public static final SERVICE_NAME:Ljava/lang/String; = "powerconsumption"

.field private static final TAG:Ljava/lang/String; = "PowerConsumptionService"

.field private static sDEBUG:Z


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mIsSupportPCS:Z

.field private mPCBgHandler:Landroid/os/Handler;

.field private mPCFgHandler:Landroid/os/Handler;

.field private mPowerConsumptionServiceController:Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;

.field private mPowerConsumptionServiceCouldData:Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;

.field private mSystemStatusListener:Lcom/android/server/powerconsumpiton/SystemStatusListener;


# direct methods
.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/powerconsumpiton/PowerConsumptionService;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsSupportPCS(Lcom/android/server/powerconsumpiton/PowerConsumptionService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->mIsSupportPCS:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmPCFgHandler(Lcom/android/server/powerconsumpiton/PowerConsumptionService;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->mPCFgHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPowerConsumptionServiceController(Lcom/android/server/powerconsumpiton/PowerConsumptionService;)Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;
    .locals 0

    iget-object p0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->mPowerConsumptionServiceController:Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPowerConsumptionServiceCouldData(Lcom/android/server/powerconsumpiton/PowerConsumptionService;)Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;
    .locals 0

    iget-object p0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->mPowerConsumptionServiceCouldData:Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSystemStatusListener(Lcom/android/server/powerconsumpiton/PowerConsumptionService;)Lcom/android/server/powerconsumpiton/SystemStatusListener;
    .locals 0

    iget-object p0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->mSystemStatusListener:Lcom/android/server/powerconsumpiton/SystemStatusListener;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$sfgetsDEBUG()Z
    .locals 1

    sget-boolean v0, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->sDEBUG:Z

    return v0
.end method

.method static bridge synthetic -$$Nest$sfputsDEBUG(Z)V
    .locals 0

    sput-boolean p0, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->sDEBUG:Z

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 32
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->sDEBUG:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 50
    invoke-direct {p0, p1}, Lcom/android/server/SystemService;-><init>(Landroid/content/Context;)V

    .line 51
    iput-object p1, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->mContext:Landroid/content/Context;

    .line 52
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Lcom/android/server/MiuiFgThread;->get()Lcom/android/server/MiuiFgThread;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/MiuiFgThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->mPCFgHandler:Landroid/os/Handler;

    .line 53
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Lcom/android/server/MiuiBgThread;->get()Lcom/android/server/MiuiBgThread;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/MiuiBgThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->mPCBgHandler:Landroid/os/Handler;

    .line 54
    new-instance v1, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;

    invoke-direct {v1, p1, v0}, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->mPowerConsumptionServiceCouldData:Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;

    .line 55
    new-instance v0, Lcom/android/server/powerconsumpiton/SystemStatusListener;

    iget-object v1, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->mPCFgHandler:Landroid/os/Handler;

    invoke-direct {v0, v1, p1}, Lcom/android/server/powerconsumpiton/SystemStatusListener;-><init>(Landroid/os/Handler;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->mSystemStatusListener:Lcom/android/server/powerconsumpiton/SystemStatusListener;

    .line 56
    new-instance v0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;

    iget-object v1, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->mPowerConsumptionServiceCouldData:Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;

    iget-object v2, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->mSystemStatusListener:Lcom/android/server/powerconsumpiton/SystemStatusListener;

    iget-object v3, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->mPCFgHandler:Landroid/os/Handler;

    invoke-direct {v0, v1, v2, v3, p1}, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;-><init>(Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;Lcom/android/server/powerconsumpiton/SystemStatusListener;Landroid/os/Handler;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->mPowerConsumptionServiceController:Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;

    .line 57
    iget-object v1, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->mSystemStatusListener:Lcom/android/server/powerconsumpiton/SystemStatusListener;

    invoke-virtual {v1, v0}, Lcom/android/server/powerconsumpiton/SystemStatusListener;->setPowerConsumptionServiceController(Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;)V

    .line 58
    const-string/jumbo v0, "support_power_consumption"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->mIsSupportPCS:Z

    .line 59
    if-nez v0, :cond_0

    .line 60
    const-string v0, "PowerConsumptionService"

    const-string/jumbo v1, "this device not support PowerConsumption"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    :cond_0
    return-void
.end method


# virtual methods
.method public onStart()V
    .locals 3

    .line 42
    const-class v0, Lcom/android/server/PowerConsumptionServiceInternal;

    new-instance v1, Lcom/android/server/powerconsumpiton/PowerConsumptionService$LocalService;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/android/server/powerconsumpiton/PowerConsumptionService$LocalService;-><init>(Lcom/android/server/powerconsumpiton/PowerConsumptionService;Lcom/android/server/powerconsumpiton/PowerConsumptionService$LocalService-IA;)V

    invoke-virtual {p0, v0, v1}, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->publishLocalService(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 43
    new-instance v0, Lcom/android/server/powerconsumpiton/PowerConsumptionService$BinderService;

    invoke-direct {v0, p0, v2}, Lcom/android/server/powerconsumpiton/PowerConsumptionService$BinderService;-><init>(Lcom/android/server/powerconsumpiton/PowerConsumptionService;Lcom/android/server/powerconsumpiton/PowerConsumptionService$BinderService-IA;)V

    const-string v1, "powerconsumption"

    invoke-virtual {p0, v1, v0}, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->publishBinderService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 44
    sget-boolean v0, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->sDEBUG:Z

    if-eqz v0, :cond_0

    .line 45
    const-string v0, "PowerConsumptionService"

    const-string v1, "PowerConsumptionService start"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    :cond_0
    return-void
.end method
