.class final Lcom/android/server/powerconsumpiton/SystemStatusListener$SettingsObserver;
.super Landroid/database/ContentObserver;
.source "SystemStatusListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/powerconsumpiton/SystemStatusListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SettingsObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/powerconsumpiton/SystemStatusListener;


# direct methods
.method public constructor <init>(Lcom/android/server/powerconsumpiton/SystemStatusListener;Landroid/os/Handler;)V
    .locals 0
    .param p2, "handler"    # Landroid/os/Handler;

    .line 113
    iput-object p1, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener$SettingsObserver;->this$0:Lcom/android/server/powerconsumpiton/SystemStatusListener;

    .line 114
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 115
    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 1
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 119
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 120
    iget-object v0, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener$SettingsObserver;->this$0:Lcom/android/server/powerconsumpiton/SystemStatusListener;

    invoke-static {v0, p2}, Lcom/android/server/powerconsumpiton/SystemStatusListener;->-$$Nest$mhandleSettingsChangedLocked(Lcom/android/server/powerconsumpiton/SystemStatusListener;Landroid/net/Uri;)V

    .line 121
    return-void
.end method
