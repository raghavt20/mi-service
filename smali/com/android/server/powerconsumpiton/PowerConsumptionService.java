public class com.android.server.powerconsumpiton.PowerConsumptionService extends com.android.server.SystemService {
	 /* .source "PowerConsumptionService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/powerconsumpiton/PowerConsumptionService$LocalService;, */
	 /* Lcom/android/server/powerconsumpiton/PowerConsumptionService$BinderService; */
	 /* } */
} // .end annotation
/* # static fields */
public static final java.lang.String SERVICE_NAME;
private static final java.lang.String TAG;
private static Boolean sDEBUG;
/* # instance fields */
private final android.content.Context mContext;
private Boolean mIsSupportPCS;
private android.os.Handler mPCBgHandler;
private android.os.Handler mPCFgHandler;
private com.android.server.powerconsumpiton.PowerConsumptionServiceController mPowerConsumptionServiceController;
private com.android.server.powerconsumpiton.PowerConsumptionServiceCouldData mPowerConsumptionServiceCouldData;
private com.android.server.powerconsumpiton.SystemStatusListener mSystemStatusListener;
/* # direct methods */
static android.content.Context -$$Nest$fgetmContext ( com.android.server.powerconsumpiton.PowerConsumptionService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mContext;
} // .end method
static Boolean -$$Nest$fgetmIsSupportPCS ( com.android.server.powerconsumpiton.PowerConsumptionService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->mIsSupportPCS:Z */
} // .end method
static android.os.Handler -$$Nest$fgetmPCFgHandler ( com.android.server.powerconsumpiton.PowerConsumptionService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mPCFgHandler;
} // .end method
static com.android.server.powerconsumpiton.PowerConsumptionServiceController -$$Nest$fgetmPowerConsumptionServiceController ( com.android.server.powerconsumpiton.PowerConsumptionService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mPowerConsumptionServiceController;
} // .end method
static com.android.server.powerconsumpiton.PowerConsumptionServiceCouldData -$$Nest$fgetmPowerConsumptionServiceCouldData ( com.android.server.powerconsumpiton.PowerConsumptionService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mPowerConsumptionServiceCouldData;
} // .end method
static com.android.server.powerconsumpiton.SystemStatusListener -$$Nest$fgetmSystemStatusListener ( com.android.server.powerconsumpiton.PowerConsumptionService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mSystemStatusListener;
} // .end method
static Boolean -$$Nest$sfgetsDEBUG ( ) { //bridge//synthethic
	 /* .locals 1 */
	 /* sget-boolean v0, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->sDEBUG:Z */
} // .end method
static void -$$Nest$sfputsDEBUG ( Boolean p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 com.android.server.powerconsumpiton.PowerConsumptionService.sDEBUG = (p0!= 0);
	 return;
} // .end method
static com.android.server.powerconsumpiton.PowerConsumptionService ( ) {
	 /* .locals 1 */
	 /* .line 32 */
	 int v0 = 0; // const/4 v0, 0x0
	 com.android.server.powerconsumpiton.PowerConsumptionService.sDEBUG = (v0!= 0);
	 return;
} // .end method
public com.android.server.powerconsumpiton.PowerConsumptionService ( ) {
	 /* .locals 4 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .line 50 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/SystemService;-><init>(Landroid/content/Context;)V */
	 /* .line 51 */
	 this.mContext = p1;
	 /* .line 52 */
	 /* new-instance v0, Landroid/os/Handler; */
	 com.android.server.MiuiFgThread .get ( );
	 (( com.android.server.MiuiFgThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Lcom/android/server/MiuiFgThread;->getLooper()Landroid/os/Looper;
	 /* invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
	 this.mPCFgHandler = v0;
	 /* .line 53 */
	 /* new-instance v0, Landroid/os/Handler; */
	 com.android.server.MiuiBgThread .get ( );
	 (( com.android.server.MiuiBgThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Lcom/android/server/MiuiBgThread;->getLooper()Landroid/os/Looper;
	 /* invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
	 this.mPCBgHandler = v0;
	 /* .line 54 */
	 /* new-instance v1, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData; */
	 /* invoke-direct {v1, p1, v0}, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;-><init>(Landroid/content/Context;Landroid/os/Handler;)V */
	 this.mPowerConsumptionServiceCouldData = v1;
	 /* .line 55 */
	 /* new-instance v0, Lcom/android/server/powerconsumpiton/SystemStatusListener; */
	 v1 = this.mPCFgHandler;
	 /* invoke-direct {v0, v1, p1}, Lcom/android/server/powerconsumpiton/SystemStatusListener;-><init>(Landroid/os/Handler;Landroid/content/Context;)V */
	 this.mSystemStatusListener = v0;
	 /* .line 56 */
	 /* new-instance v0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController; */
	 v1 = this.mPowerConsumptionServiceCouldData;
	 v2 = this.mSystemStatusListener;
	 v3 = this.mPCFgHandler;
	 /* invoke-direct {v0, v1, v2, v3, p1}, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;-><init>(Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;Lcom/android/server/powerconsumpiton/SystemStatusListener;Landroid/os/Handler;Landroid/content/Context;)V */
	 this.mPowerConsumptionServiceController = v0;
	 /* .line 57 */
	 v1 = this.mSystemStatusListener;
	 (( com.android.server.powerconsumpiton.SystemStatusListener ) v1 ).setPowerConsumptionServiceController ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/powerconsumpiton/SystemStatusListener;->setPowerConsumptionServiceController(Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;)V
	 /* .line 58 */
	 /* const-string/jumbo v0, "support_power_consumption" */
	 int v1 = 0; // const/4 v1, 0x0
	 v0 = 	 miui.util.FeatureParser .getBoolean ( v0,v1 );
	 /* iput-boolean v0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->mIsSupportPCS:Z */
	 /* .line 59 */
	 /* if-nez v0, :cond_0 */
	 /* .line 60 */
	 final String v0 = "PowerConsumptionService"; // const-string v0, "PowerConsumptionService"
	 /* const-string/jumbo v1, "this device not support PowerConsumption" */
	 android.util.Slog .d ( v0,v1 );
	 /* .line 62 */
} // :cond_0
return;
} // .end method
/* # virtual methods */
public void onStart ( ) {
/* .locals 3 */
/* .line 42 */
/* const-class v0, Lcom/android/server/PowerConsumptionServiceInternal; */
/* new-instance v1, Lcom/android/server/powerconsumpiton/PowerConsumptionService$LocalService; */
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {v1, p0, v2}, Lcom/android/server/powerconsumpiton/PowerConsumptionService$LocalService;-><init>(Lcom/android/server/powerconsumpiton/PowerConsumptionService;Lcom/android/server/powerconsumpiton/PowerConsumptionService$LocalService-IA;)V */
(( com.android.server.powerconsumpiton.PowerConsumptionService ) p0 ).publishLocalService ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->publishLocalService(Ljava/lang/Class;Ljava/lang/Object;)V
/* .line 43 */
/* new-instance v0, Lcom/android/server/powerconsumpiton/PowerConsumptionService$BinderService; */
/* invoke-direct {v0, p0, v2}, Lcom/android/server/powerconsumpiton/PowerConsumptionService$BinderService;-><init>(Lcom/android/server/powerconsumpiton/PowerConsumptionService;Lcom/android/server/powerconsumpiton/PowerConsumptionService$BinderService-IA;)V */
final String v1 = "powerconsumption"; // const-string v1, "powerconsumption"
(( com.android.server.powerconsumpiton.PowerConsumptionService ) p0 ).publishBinderService ( v1, v0 ); // invoke-virtual {p0, v1, v0}, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->publishBinderService(Ljava/lang/String;Landroid/os/IBinder;)V
/* .line 44 */
/* sget-boolean v0, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->sDEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 45 */
	 final String v0 = "PowerConsumptionService"; // const-string v0, "PowerConsumptionService"
	 final String v1 = "PowerConsumptionService start"; // const-string v1, "PowerConsumptionService start"
	 android.util.Slog .d ( v0,v1 );
	 /* .line 47 */
} // :cond_0
return;
} // .end method
