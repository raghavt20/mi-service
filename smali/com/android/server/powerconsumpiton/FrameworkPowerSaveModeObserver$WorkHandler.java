class com.android.server.powerconsumpiton.FrameworkPowerSaveModeObserver$WorkHandler extends android.os.Handler {
	 /* .source "FrameworkPowerSaveModeObserver.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "WorkHandler" */
} // .end annotation
/* # instance fields */
final com.android.server.powerconsumpiton.FrameworkPowerSaveModeObserver this$0; //synthetic
/* # direct methods */
public com.android.server.powerconsumpiton.FrameworkPowerSaveModeObserver$WorkHandler ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 46 */
this.this$0 = p1;
/* .line 47 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 48 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 3 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 52 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* packed-switch v0, :pswitch_data_0 */
/* .line 54 */
/* :pswitch_0 */
v0 = this.this$0;
/* iget v1, p1, Landroid/os/Message;->arg1:I */
int v2 = 1; // const/4 v2, 0x1
/* if-ne v1, v2, :cond_0 */
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
com.android.server.powerconsumpiton.FrameworkPowerSaveModeObserver .-$$Nest$fputmIsPowerSaveMode ( v0,v2 );
/* .line 55 */
/* nop */
/* .line 59 */
} // :goto_1
return;
/* :pswitch_data_0 */
/* .packed-switch 0x3e9 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
