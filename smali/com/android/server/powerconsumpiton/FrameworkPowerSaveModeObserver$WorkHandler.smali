.class Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver$WorkHandler;
.super Landroid/os/Handler;
.source "FrameworkPowerSaveModeObserver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WorkHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;


# direct methods
.method public constructor <init>(Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 46
    iput-object p1, p0, Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver$WorkHandler;->this$0:Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;

    .line 47
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 48
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .line 52
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_1

    .line 54
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver$WorkHandler;->this$0:Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;

    iget v1, p1, Landroid/os/Message;->arg1:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    invoke-static {v0, v2}, Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;->-$$Nest$fputmIsPowerSaveMode(Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;Z)V

    .line 55
    nop

    .line 59
    :goto_1
    return-void

    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_0
    .end packed-switch
.end method
