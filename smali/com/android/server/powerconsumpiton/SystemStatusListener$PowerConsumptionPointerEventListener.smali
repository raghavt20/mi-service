.class Lcom/android/server/powerconsumpiton/SystemStatusListener$PowerConsumptionPointerEventListener;
.super Ljava/lang/Object;
.source "SystemStatusListener.java"

# interfaces
.implements Landroid/view/WindowManagerPolicyConstants$PointerEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/powerconsumpiton/SystemStatusListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PowerConsumptionPointerEventListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/powerconsumpiton/SystemStatusListener;


# direct methods
.method private constructor <init>(Lcom/android/server/powerconsumpiton/SystemStatusListener;)V
    .locals 0

    .line 166
    iput-object p1, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener$PowerConsumptionPointerEventListener;->this$0:Lcom/android/server/powerconsumpiton/SystemStatusListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/powerconsumpiton/SystemStatusListener;Lcom/android/server/powerconsumpiton/SystemStatusListener$PowerConsumptionPointerEventListener-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/powerconsumpiton/SystemStatusListener$PowerConsumptionPointerEventListener;-><init>(Lcom/android/server/powerconsumpiton/SystemStatusListener;)V

    return-void
.end method


# virtual methods
.method public onPointerEvent(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1, "motionEvent"    # Landroid/view/MotionEvent;

    .line 169
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 171
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener$PowerConsumptionPointerEventListener;->this$0:Lcom/android/server/powerconsumpiton/SystemStatusListener;

    invoke-static {v0}, Lcom/android/server/powerconsumpiton/SystemStatusListener;->-$$Nest$fgetmPowerConsumptionServiceController(Lcom/android/server/powerconsumpiton/SystemStatusListener;)Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->setScreenOffTimeOutDelay()V

    .line 175
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
