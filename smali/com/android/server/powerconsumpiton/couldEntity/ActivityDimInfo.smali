.class public Lcom/android/server/powerconsumpiton/couldEntity/ActivityDimInfo;
.super Ljava/lang/Object;
.source "ActivityDimInfo.java"


# instance fields
.field private mActivityDimInfoConfigList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mVersion:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getActivityDimInfoConfigList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 13
    iget-object v0, p0, Lcom/android/server/powerconsumpiton/couldEntity/ActivityDimInfo;->mActivityDimInfoConfigList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/android/server/powerconsumpiton/couldEntity/ActivityDimInfo;->mVersion:Ljava/lang/String;

    return-object v0
.end method
