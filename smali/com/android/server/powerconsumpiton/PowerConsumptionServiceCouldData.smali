.class public final Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;
.super Ljava/lang/Object;
.source "PowerConsumptionServiceCouldData.java"


# static fields
.field private static final POWER_CONSUMPTION_CLOUD_FILE:Ljava/lang/String; = "power_consumption_cloud_config.xml"


# instance fields
.field private final mActivityPolicies:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "[I>;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mMiuiCommonCloudHelper:Lcom/android/server/MiuiCommonCloudHelperStub;

.field private mPCBgHandler:Landroid/os/Handler;

.field private final mWindowPolicies:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "[I>;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$mupdateActivityPolicies(Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;->updateActivityPolicies()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateWindowPolicies(Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;->updateWindowPolicies()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;->mActivityPolicies:Ljava/util/HashMap;

    .line 21
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;->mWindowPolicies:Ljava/util/HashMap;

    .line 27
    iput-object p1, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;->mContext:Landroid/content/Context;

    .line 28
    iput-object p2, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;->mPCBgHandler:Landroid/os/Handler;

    .line 29
    invoke-direct {p0}, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;->init()V

    .line 30
    return-void
.end method

.method private init()V
    .locals 5

    .line 33
    invoke-static {}, Lcom/android/server/MiuiCommonCloudHelperStub;->getInstance()Lcom/android/server/MiuiCommonCloudHelperStub;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;->mMiuiCommonCloudHelper:Lcom/android/server/MiuiCommonCloudHelperStub;

    .line 34
    iget-object v1, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;->mPCBgHandler:Landroid/os/Handler;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getDownloadCacheDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/test.xml"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/MiuiCommonCloudHelperStub;->init(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;)V

    .line 36
    iget-object v0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;->mMiuiCommonCloudHelper:Lcom/android/server/MiuiCommonCloudHelperStub;

    new-instance v1, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData$1;

    invoke-direct {v1, p0}, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData$1;-><init>(Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;)V

    invoke-virtual {v0, v1}, Lcom/android/server/MiuiCommonCloudHelperStub;->registerObserver(Lcom/android/server/MiuiCommonCloudHelperStub$Observer;)V

    .line 44
    iget-object v0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;->mMiuiCommonCloudHelper:Lcom/android/server/MiuiCommonCloudHelperStub;

    const-string v1, "power_consumption_cloud_config.xml"

    invoke-virtual {v0, v1}, Lcom/android/server/MiuiCommonCloudHelperStub;->initFromAssets(Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;->mMiuiCommonCloudHelper:Lcom/android/server/MiuiCommonCloudHelperStub;

    invoke-virtual {v0}, Lcom/android/server/MiuiCommonCloudHelperStub;->startCloudObserve()V

    .line 46
    return-void
.end method

.method private updateActivityPolicies()V
    .locals 8

    .line 58
    iget-object v0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;->mMiuiCommonCloudHelper:Lcom/android/server/MiuiCommonCloudHelperStub;

    const-string v1, "decline_control"

    invoke-virtual {v0, v1}, Lcom/android/server/MiuiCommonCloudHelperStub;->getDataByModuleName(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/powerconsumpiton/couldEntity/DeclineWhiteInfo;

    .line 59
    .local v0, "declineWhiteInfo":Lcom/android/server/powerconsumpiton/couldEntity/DeclineWhiteInfo;
    if-eqz v0, :cond_1

    .line 60
    iget-object v1, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;->mActivityPolicies:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 61
    invoke-virtual {v0}, Lcom/android/server/powerconsumpiton/couldEntity/DeclineWhiteInfo;->getDeclineWhiteConfigList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 62
    .local v2, "data":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 63
    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 64
    .local v3, "dataArray":[Ljava/lang/String;
    const/4 v4, 0x0

    aget-object v4, v3, v4

    .line 65
    .local v4, "activity":Ljava/lang/String;
    const/4 v5, 0x1

    aget-object v5, v3, v5

    const/4 v6, 0x2

    invoke-static {v5, v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v5

    .line 66
    .local v5, "policy":I
    iget-object v6, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;->mActivityPolicies:Ljava/util/HashMap;

    filled-new-array {v5}, [I

    move-result-object v7

    invoke-virtual {v6, v4, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    .end local v2    # "data":Ljava/lang/String;
    .end local v3    # "dataArray":[Ljava/lang/String;
    .end local v4    # "activity":Ljava/lang/String;
    .end local v5    # "policy":I
    :cond_0
    goto :goto_0

    .line 70
    :cond_1
    return-void
.end method

.method private updateWindowPolicies()V
    .locals 10

    .line 74
    iget-object v0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;->mMiuiCommonCloudHelper:Lcom/android/server/MiuiCommonCloudHelperStub;

    const-string v1, "dim_control"

    invoke-virtual {v0, v1}, Lcom/android/server/MiuiCommonCloudHelperStub;->getDataByModuleName(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/powerconsumpiton/couldEntity/ActivityDimInfo;

    .line 75
    .local v0, "activityDimInfo":Lcom/android/server/powerconsumpiton/couldEntity/ActivityDimInfo;
    if-eqz v0, :cond_1

    .line 76
    iget-object v1, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;->mWindowPolicies:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 77
    invoke-virtual {v0}, Lcom/android/server/powerconsumpiton/couldEntity/ActivityDimInfo;->getActivityDimInfoConfigList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 78
    .local v2, "data":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 79
    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 80
    .local v3, "dataArray":[Ljava/lang/String;
    const/4 v4, 0x0

    aget-object v4, v3, v4

    .line 81
    .local v4, "window":Ljava/lang/String;
    const/4 v5, 0x1

    aget-object v5, v3, v5

    const/4 v6, 0x2

    invoke-static {v5, v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v5

    .line 82
    .local v5, "policy":I
    aget-object v6, v3, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 83
    .local v6, "dimTime":I
    const/4 v7, 0x3

    aget-object v7, v3, v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 84
    .local v7, "isReleaseWakeLock":I
    iget-object v8, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;->mWindowPolicies:Ljava/util/HashMap;

    filled-new-array {v5, v6, v7}, [I

    move-result-object v9

    invoke-virtual {v8, v4, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    .end local v2    # "data":Ljava/lang/String;
    .end local v3    # "dataArray":[Ljava/lang/String;
    .end local v4    # "window":Ljava/lang/String;
    .end local v5    # "policy":I
    .end local v6    # "dimTime":I
    .end local v7    # "isReleaseWakeLock":I
    :cond_0
    goto :goto_0

    .line 88
    :cond_1
    return-void
.end method


# virtual methods
.method dump(Ljava/io/PrintWriter;)V
    .locals 9
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 91
    const-string v0, "  Activities cloud data"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 92
    iget-object v0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;->mActivityPolicies:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 93
    .local v0, "activities":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const-string v3, "; policy is "

    const-string v4, "    "

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 94
    .local v2, "activity":Ljava/lang/String;
    iget-object v5, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;->mActivityPolicies:Ljava/util/HashMap;

    invoke-virtual {v5, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [I

    .line 95
    .local v5, "policy":[I
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v5}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 96
    .end local v2    # "activity":Ljava/lang/String;
    .end local v5    # "policy":[I
    goto :goto_0

    .line 97
    :cond_0
    const-string v1, "  Windows cloud data"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 98
    iget-object v1, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;->mWindowPolicies:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    .line 99
    .local v1, "windows":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 100
    .local v5, "window":Ljava/lang/String;
    iget-object v6, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;->mWindowPolicies:Ljava/util/HashMap;

    invoke-virtual {v6, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [I

    .line 101
    .local v6, "policy":[I
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v6}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 102
    .end local v5    # "window":Ljava/lang/String;
    .end local v6    # "policy":[I
    goto :goto_1

    .line 103
    :cond_1
    return-void
.end method

.method public getActivityPolicy()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "[I>;"
        }
    .end annotation

    .line 49
    iget-object v0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;->mActivityPolicies:Ljava/util/HashMap;

    return-object v0
.end method

.method public getWindowPolicies()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "[I>;"
        }
    .end annotation

    .line 53
    iget-object v0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceCouldData;->mWindowPolicies:Ljava/util/HashMap;

    return-object v0
.end method
