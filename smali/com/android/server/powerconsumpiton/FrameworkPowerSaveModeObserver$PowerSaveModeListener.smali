.class Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver$PowerSaveModeListener;
.super Landroid/os/FileObserver;
.source "FrameworkPowerSaveModeObserver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PowerSaveModeListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;


# direct methods
.method public constructor <init>(Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;Ljava/lang/String;)V
    .locals 0
    .param p2, "path"    # Ljava/lang/String;

    .line 63
    iput-object p1, p0, Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver$PowerSaveModeListener;->this$0:Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;

    .line 64
    invoke-direct {p0, p2}, Landroid/os/FileObserver;-><init>(Ljava/lang/String;)V

    .line 65
    invoke-static {p1, p2}, Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;->-$$Nest$mcheckPowerSaveModeState(Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;Ljava/lang/String;)V

    .line 66
    return-void
.end method


# virtual methods
.method public onEvent(ILjava/lang/String;)V
    .locals 2
    .param p1, "event"    # I
    .param p2, "path"    # Ljava/lang/String;

    .line 70
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 72
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver$PowerSaveModeListener;->this$0:Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;

    const-string v1, "/sys/class/thermal/power_save/powersave_mode"

    invoke-static {v0, v1}, Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;->-$$Nest$mcheckPowerSaveModeState(Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;Ljava/lang/String;)V

    .line 73
    nop

    .line 77
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method
