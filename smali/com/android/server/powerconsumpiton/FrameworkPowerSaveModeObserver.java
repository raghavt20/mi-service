class com.android.server.powerconsumpiton.FrameworkPowerSaveModeObserver {
	 /* .source "FrameworkPowerSaveModeObserver.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver$PowerSaveModeListener;, */
	 /* Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver$WorkHandler; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer MSG_FRAMEWORK_POWER_SAVE_POLICY;
private static final Integer MSG_SET_FRAMEWORK_POWER_SAVE_POLICY;
private static final java.lang.String POWERSAVE_MODE_PATH;
private static final java.lang.String TAG;
/* # instance fields */
private final android.content.Context mContext;
private android.os.Handler mHandler;
private Boolean mIsPowerSaveMode;
private com.android.server.powerconsumpiton.FrameworkPowerSaveModeObserver$PowerSaveModeListener mPowerSaveModeListener;
/* # direct methods */
static void -$$Nest$fputmIsPowerSaveMode ( com.android.server.powerconsumpiton.FrameworkPowerSaveModeObserver p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;->mIsPowerSaveMode:Z */
	 return;
} // .end method
static void -$$Nest$mcheckPowerSaveModeState ( com.android.server.powerconsumpiton.FrameworkPowerSaveModeObserver p0, java.lang.String p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;->checkPowerSaveModeState(Ljava/lang/String;)V */
	 return;
} // .end method
public com.android.server.powerconsumpiton.FrameworkPowerSaveModeObserver ( ) {
	 /* .locals 1 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .param p2, "looper" # Landroid/os/Looper; */
	 /* .line 26 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 23 */
	 int v0 = 0; // const/4 v0, 0x0
	 this.mPowerSaveModeListener = v0;
	 /* .line 24 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* iput-boolean v0, p0, Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;->mIsPowerSaveMode:Z */
	 /* .line 27 */
	 this.mContext = p1;
	 /* .line 28 */
	 /* new-instance v0, Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver$WorkHandler; */
	 /* invoke-direct {v0, p0, p2}, Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver$WorkHandler;-><init>(Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;Landroid/os/Looper;)V */
	 this.mHandler = v0;
	 /* .line 29 */
	 /* invoke-direct {p0}, Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;->initPowerSaveModeListener()V */
	 /* .line 30 */
	 return;
} // .end method
private void checkPowerSaveModeState ( java.lang.String p0 ) {
	 /* .locals 6 */
	 /* .param p1, "path" # Ljava/lang/String; */
	 /* .line 83 */
	 final String v0 = "FrameworkPowerSaveModeObserver"; // const-string v0, "FrameworkPowerSaveModeObserver"
	 try { // :try_start_0
		 /* invoke-direct {p0, p1}, Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;->getContentFromFile(Ljava/lang/String;)Ljava/lang/String; */
		 /* move-object v2, v1 */
		 /* .local v2, "line":Ljava/lang/String; */
		 if ( v1 != null) { // if-eqz v1, :cond_0
			 /* .line 84 */
			 v1 = 			 java.lang.Integer .parseInt ( v2 );
			 /* .line 85 */
			 /* .local v1, "power_save_mode":I */
			 /* new-instance v3, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
			 final String v4 = "power_save_mode is: "; // const-string v4, "power_save_mode is: "
			 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
			 final String v4 = " path: "; // const-string v4, " path: "
			 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 android.util.Slog .d ( v0,v3 );
			 /* .line 86 */
			 final String v3 = "/sys/class/thermal/power_save/powersave_mode"; // const-string v3, "/sys/class/thermal/power_save/powersave_mode"
			 v3 = 			 (( java.lang.String ) v3 ).equals ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
			 if ( v3 != null) { // if-eqz v3, :cond_0
				 /* .line 87 */
				 v3 = this.mHandler;
				 /* const/16 v4, 0x3e9 */
				 (( android.os.Handler ) v3 ).removeMessages ( v4 ); // invoke-virtual {v3, v4}, Landroid/os/Handler;->removeMessages(I)V
				 /* .line 88 */
				 v3 = this.mHandler;
				 int v5 = 0; // const/4 v5, 0x0
				 (( android.os.Handler ) v3 ).obtainMessage ( v4, v1, v5 ); // invoke-virtual {v3, v4, v1, v5}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;
				 /* .line 90 */
				 /* .local v3, "message":Landroid/os/Message; */
				 v4 = this.mHandler;
				 (( android.os.Handler ) v4 ).sendMessage ( v3 ); // invoke-virtual {v4, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
				 /* :try_end_0 */
				 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
				 /* .line 95 */
			 } // .end local v1 # "power_save_mode":I
		 } // .end local v3 # "message":Landroid/os/Message;
	 } // :cond_0
	 /* .line 93 */
} // .end local v2 # "line":Ljava/lang/String;
/* :catch_0 */
/* move-exception v1 */
/* .line 94 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v2 = "checkPowerSaveModeState: "; // const-string v2, "checkPowerSaveModeState: "
android.util.Slog .e ( v0,v2,v1 );
/* .line 96 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private java.lang.String getContentFromFile ( java.lang.String p0 ) {
/* .locals 8 */
/* .param p1, "filePath" # Ljava/lang/String; */
/* .line 99 */
final String v0 = "can not close FileInputStream "; // const-string v0, "can not close FileInputStream "
final String v1 = "FrameworkPowerSaveModeObserver"; // const-string v1, "FrameworkPowerSaveModeObserver"
int v2 = 0; // const/4 v2, 0x0
/* .line 100 */
/* .local v2, "is":Ljava/io/FileInputStream; */
int v3 = 0; // const/4 v3, 0x0
/* .line 102 */
/* .local v3, "content":Ljava/lang/String; */
try { // :try_start_0
/* new-instance v4, Ljava/io/File; */
/* invoke-direct {v4, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 103 */
/* .local v4, "file":Ljava/io/File; */
/* new-instance v5, Ljava/io/FileInputStream; */
/* invoke-direct {v5, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V */
/* move-object v2, v5 */
/* .line 104 */
com.android.server.powerconsumpiton.FrameworkPowerSaveModeObserver .readInputStream ( v2 );
/* .line 105 */
/* .local v5, "data":[B */
/* new-instance v6, Ljava/lang/String; */
/* invoke-direct {v6, v5}, Ljava/lang/String;-><init>([B)V */
(( java.lang.String ) v6 ).trim ( ); // invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;
/* move-object v3, v6 */
/* .line 106 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( p1 ); // invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = " content is "; // const-string v7, " content is "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v3 ); // invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v6 );
/* :try_end_0 */
/* .catch Ljava/io/FileNotFoundException; {:try_start_0 ..:try_end_0} :catch_3 */
/* .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 112 */
/* nop */
/* .line 114 */
} // .end local v4 # "file":Ljava/io/File;
} // .end local v5 # "data":[B
try { // :try_start_1
(( java.io.FileInputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 117 */
} // :goto_0
/* .line 115 */
/* :catch_0 */
/* move-exception v4 */
/* .line 116 */
/* .local v4, "e":Ljava/io/IOException; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
} // :goto_1
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v1,v0 );
} // .end local v4 # "e":Ljava/io/IOException;
/* .line 112 */
/* :catchall_0 */
/* move-exception v4 */
/* .line 109 */
/* :catch_1 */
/* move-exception v4 */
/* .line 110 */
/* .local v4, "e":Ljava/lang/IndexOutOfBoundsException; */
try { // :try_start_2
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "index exception: "; // const-string v6, "index exception: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v1,v5 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 112 */
/* nop */
} // .end local v4 # "e":Ljava/lang/IndexOutOfBoundsException;
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 114 */
try { // :try_start_3
(( java.io.FileInputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_2 */
/* .line 115 */
/* :catch_2 */
/* move-exception v4 */
/* .line 116 */
/* .local v4, "e":Ljava/io/IOException; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 107 */
} // .end local v4 # "e":Ljava/io/IOException;
/* :catch_3 */
/* move-exception v4 */
/* .line 108 */
/* .local v4, "e":Ljava/io/FileNotFoundException; */
try { // :try_start_4
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "can\'t find file "; // const-string v6, "can\'t find file "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( p1 ); // invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v1,v5 );
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* .line 112 */
/* nop */
} // .end local v4 # "e":Ljava/io/FileNotFoundException;
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 114 */
try { // :try_start_5
(( java.io.FileInputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
/* :try_end_5 */
/* .catch Ljava/io/IOException; {:try_start_5 ..:try_end_5} :catch_4 */
/* .line 115 */
/* :catch_4 */
/* move-exception v4 */
/* .line 116 */
/* .local v4, "e":Ljava/io/IOException; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 120 */
} // .end local v4 # "e":Ljava/io/IOException;
} // :cond_0
} // :goto_2
/* .line 112 */
} // :goto_3
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 114 */
try { // :try_start_6
(( java.io.FileInputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
/* :try_end_6 */
/* .catch Ljava/io/IOException; {:try_start_6 ..:try_end_6} :catch_5 */
/* .line 117 */
/* .line 115 */
/* :catch_5 */
/* move-exception v5 */
/* .line 116 */
/* .local v5, "e":Ljava/io/IOException; */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v1,v0 );
/* .line 119 */
} // .end local v5 # "e":Ljava/io/IOException;
} // :cond_1
} // :goto_4
/* throw v4 */
} // .end method
private void initPowerSaveModeListener ( ) {
/* .locals 3 */
/* .line 34 */
try { // :try_start_0
/* new-instance v0, Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver$PowerSaveModeListener; */
final String v1 = "/sys/class/thermal/power_save/powersave_mode"; // const-string v1, "/sys/class/thermal/power_save/powersave_mode"
/* invoke-direct {v0, p0, v1}, Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver$PowerSaveModeListener;-><init>(Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;Ljava/lang/String;)V */
this.mPowerSaveModeListener = v0;
/* .line 35 */
(( com.android.server.powerconsumpiton.FrameworkPowerSaveModeObserver$PowerSaveModeListener ) v0 ).startWatching ( ); // invoke-virtual {v0}, Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver$PowerSaveModeListener;->startWatching()V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 38 */
/* .line 36 */
/* :catch_0 */
/* move-exception v0 */
/* .line 37 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "FrameworkPowerSaveModeObserver"; // const-string v1, "FrameworkPowerSaveModeObserver"
final String v2 = "initPowerSaveModeListener init failed"; // const-string v2, "initPowerSaveModeListener init failed"
android.util.Slog .e ( v1,v2 );
/* .line 39 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private static readInputStream ( java.io.FileInputStream p0 ) {
/* .locals 9 */
/* .param p0, "is" # Ljava/io/FileInputStream; */
/* .line 124 */
final String v0 = "can not close readInputStream "; // const-string v0, "can not close readInputStream "
final String v1 = "FrameworkPowerSaveModeObserver"; // const-string v1, "FrameworkPowerSaveModeObserver"
/* new-instance v2, Ljava/io/ByteArrayOutputStream; */
/* invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V */
/* .line 125 */
/* .local v2, "byteStream":Ljava/io/ByteArrayOutputStream; */
/* const/16 v3, 0x200 */
/* .line 126 */
/* .local v3, "blockSize":I */
/* const/16 v4, 0x200 */
/* new-array v5, v4, [B */
/* .line 127 */
/* .local v5, "buffer":[B */
int v6 = 0; // const/4 v6, 0x0
/* .line 129 */
/* .local v6, "count":I */
} // :goto_0
int v7 = 0; // const/4 v7, 0x0
try { // :try_start_0
v8 = (( java.io.FileInputStream ) p0 ).read ( v5, v7, v4 ); // invoke-virtual {p0, v5, v7, v4}, Ljava/io/FileInputStream;->read([BII)I
/* move v6, v8 */
/* if-lez v8, :cond_0 */
/* .line 130 */
(( java.io.ByteArrayOutputStream ) v2 ).write ( v5, v7, v6 ); // invoke-virtual {v2, v5, v7, v6}, Ljava/io/ByteArrayOutputStream;->write([BII)V
/* .line 132 */
} // :cond_0
(( java.io.ByteArrayOutputStream ) v2 ).toByteArray ( ); // invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 136 */
/* nop */
/* .line 138 */
try { // :try_start_1
(( java.io.ByteArrayOutputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 141 */
/* .line 139 */
/* :catch_0 */
/* move-exception v7 */
/* .line 140 */
/* .local v7, "e":Ljava/io/IOException; */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v8 ).append ( v0 ); // invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v1,v0 );
/* .line 132 */
} // .end local v7 # "e":Ljava/io/IOException;
} // :goto_1
/* .line 136 */
/* :catchall_0 */
/* move-exception v4 */
/* .line 133 */
/* :catch_1 */
/* move-exception v4 */
/* .line 134 */
/* .local v4, "e":Ljava/lang/Exception; */
try { // :try_start_2
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "readInputStream "; // const-string v8, "readInputStream "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v1,v7 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 136 */
/* nop */
/* .line 138 */
} // .end local v4 # "e":Ljava/lang/Exception;
try { // :try_start_3
(( java.io.ByteArrayOutputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_2 */
/* .line 141 */
} // :goto_2
/* .line 139 */
/* :catch_2 */
/* move-exception v4 */
/* .line 140 */
/* .local v4, "e":Ljava/io/IOException; */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v0 ); // invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v1,v0 );
} // .end local v4 # "e":Ljava/io/IOException;
/* .line 144 */
} // :goto_3
int v0 = 0; // const/4 v0, 0x0
/* .line 138 */
} // :goto_4
try { // :try_start_4
(( java.io.ByteArrayOutputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_3 */
/* .line 141 */
/* .line 139 */
/* :catch_3 */
/* move-exception v7 */
/* .line 140 */
/* .restart local v7 # "e":Ljava/io/IOException; */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v8 ).append ( v0 ); // invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v1,v0 );
/* .line 143 */
} // .end local v7 # "e":Ljava/io/IOException;
} // :goto_5
/* throw v4 */
} // .end method
/* # virtual methods */
protected Boolean isPowerSaveMode ( ) {
/* .locals 1 */
/* .line 42 */
/* iget-boolean v0, p0, Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;->mIsPowerSaveMode:Z */
} // .end method
