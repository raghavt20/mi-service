.class Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;
.super Ljava/lang/Object;
.source "FrameworkPowerSaveModeObserver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver$PowerSaveModeListener;,
        Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver$WorkHandler;
    }
.end annotation


# static fields
.field private static final MSG_FRAMEWORK_POWER_SAVE_POLICY:I = 0x3e8

.field private static final MSG_SET_FRAMEWORK_POWER_SAVE_POLICY:I = 0x3e9

.field private static final POWERSAVE_MODE_PATH:Ljava/lang/String; = "/sys/class/thermal/power_save/powersave_mode"

.field private static final TAG:Ljava/lang/String; = "FrameworkPowerSaveModeObserver"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mIsPowerSaveMode:Z

.field private mPowerSaveModeListener:Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver$PowerSaveModeListener;


# direct methods
.method static bridge synthetic -$$Nest$fputmIsPowerSaveMode(Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;->mIsPowerSaveMode:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mcheckPowerSaveModeState(Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;->checkPowerSaveModeState(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;->mPowerSaveModeListener:Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver$PowerSaveModeListener;

    .line 24
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;->mIsPowerSaveMode:Z

    .line 27
    iput-object p1, p0, Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;->mContext:Landroid/content/Context;

    .line 28
    new-instance v0, Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver$WorkHandler;

    invoke-direct {v0, p0, p2}, Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver$WorkHandler;-><init>(Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;->mHandler:Landroid/os/Handler;

    .line 29
    invoke-direct {p0}, Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;->initPowerSaveModeListener()V

    .line 30
    return-void
.end method

.method private checkPowerSaveModeState(Ljava/lang/String;)V
    .locals 6
    .param p1, "path"    # Ljava/lang/String;

    .line 83
    const-string v0, "FrameworkPowerSaveModeObserver"

    :try_start_0
    invoke-direct {p0, p1}, Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;->getContentFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .local v2, "line":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 84
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 85
    .local v1, "power_save_mode":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "power_save_mode is: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " path: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    const-string v3, "/sys/class/thermal/power_save/powersave_mode"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 87
    iget-object v3, p0, Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;->mHandler:Landroid/os/Handler;

    const/16 v4, 0x3e9

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 88
    iget-object v3, p0, Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;->mHandler:Landroid/os/Handler;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v1, v5}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v3

    .line 90
    .local v3, "message":Landroid/os/Message;
    iget-object v4, p0, Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    .end local v1    # "power_save_mode":I
    .end local v3    # "message":Landroid/os/Message;
    :cond_0
    goto :goto_0

    .line 93
    .end local v2    # "line":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 94
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "checkPowerSaveModeState: "

    invoke-static {v0, v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 96
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private getContentFromFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "filePath"    # Ljava/lang/String;

    .line 99
    const-string v0, "can not close FileInputStream "

    const-string v1, "FrameworkPowerSaveModeObserver"

    const/4 v2, 0x0

    .line 100
    .local v2, "is":Ljava/io/FileInputStream;
    const/4 v3, 0x0

    .line 102
    .local v3, "content":Ljava/lang/String;
    :try_start_0
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 103
    .local v4, "file":Ljava/io/File;
    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    move-object v2, v5

    .line 104
    invoke-static {v2}, Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;->readInputStream(Ljava/io/FileInputStream;)[B

    move-result-object v5

    .line 105
    .local v5, "data":[B
    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v5}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    move-object v3, v6

    .line 106
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " content is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 112
    nop

    .line 114
    .end local v4    # "file":Ljava/io/File;
    .end local v5    # "data":[B
    :try_start_1
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 117
    :goto_0
    goto :goto_2

    .line 115
    :catch_0
    move-exception v4

    .line 116
    .local v4, "e":Ljava/io/IOException;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    :goto_1
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .end local v4    # "e":Ljava/io/IOException;
    goto :goto_0

    .line 112
    :catchall_0
    move-exception v4

    goto :goto_3

    .line 109
    :catch_1
    move-exception v4

    .line 110
    .local v4, "e":Ljava/lang/IndexOutOfBoundsException;
    :try_start_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "index exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 112
    nop

    .end local v4    # "e":Ljava/lang/IndexOutOfBoundsException;
    if-eqz v2, :cond_0

    .line 114
    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 115
    :catch_2
    move-exception v4

    .line 116
    .local v4, "e":Ljava/io/IOException;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    goto :goto_1

    .line 107
    .end local v4    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v4

    .line 108
    .local v4, "e":Ljava/io/FileNotFoundException;
    :try_start_4
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "can\'t find file "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 112
    nop

    .end local v4    # "e":Ljava/io/FileNotFoundException;
    if-eqz v2, :cond_0

    .line 114
    :try_start_5
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_0

    .line 115
    :catch_4
    move-exception v4

    .line 116
    .local v4, "e":Ljava/io/IOException;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    goto :goto_1

    .line 120
    .end local v4    # "e":Ljava/io/IOException;
    :cond_0
    :goto_2
    return-object v3

    .line 112
    :goto_3
    if-eqz v2, :cond_1

    .line 114
    :try_start_6
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    .line 117
    goto :goto_4

    .line 115
    :catch_5
    move-exception v5

    .line 116
    .local v5, "e":Ljava/io/IOException;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    .end local v5    # "e":Ljava/io/IOException;
    :cond_1
    :goto_4
    throw v4
.end method

.method private initPowerSaveModeListener()V
    .locals 3

    .line 34
    :try_start_0
    new-instance v0, Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver$PowerSaveModeListener;

    const-string v1, "/sys/class/thermal/power_save/powersave_mode"

    invoke-direct {v0, p0, v1}, Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver$PowerSaveModeListener;-><init>(Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;->mPowerSaveModeListener:Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver$PowerSaveModeListener;

    .line 35
    invoke-virtual {v0}, Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver$PowerSaveModeListener;->startWatching()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 38
    goto :goto_0

    .line 36
    :catch_0
    move-exception v0

    .line 37
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "FrameworkPowerSaveModeObserver"

    const-string v2, "initPowerSaveModeListener init failed"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 39
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private static readInputStream(Ljava/io/FileInputStream;)[B
    .locals 9
    .param p0, "is"    # Ljava/io/FileInputStream;

    .line 124
    const-string v0, "can not close readInputStream "

    const-string v1, "FrameworkPowerSaveModeObserver"

    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 125
    .local v2, "byteStream":Ljava/io/ByteArrayOutputStream;
    const/16 v3, 0x200

    .line 126
    .local v3, "blockSize":I
    const/16 v4, 0x200

    new-array v5, v4, [B

    .line 127
    .local v5, "buffer":[B
    const/4 v6, 0x0

    .line 129
    .local v6, "count":I
    :goto_0
    const/4 v7, 0x0

    :try_start_0
    invoke-virtual {p0, v5, v7, v4}, Ljava/io/FileInputStream;->read([BII)I

    move-result v8

    move v6, v8

    if-lez v8, :cond_0

    .line 130
    invoke-virtual {v2, v5, v7, v6}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0

    .line 132
    :cond_0
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 136
    nop

    .line 138
    :try_start_1
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 141
    goto :goto_1

    .line 139
    :catch_0
    move-exception v7

    .line 140
    .local v7, "e":Ljava/io/IOException;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    .end local v7    # "e":Ljava/io/IOException;
    :goto_1
    return-object v4

    .line 136
    :catchall_0
    move-exception v4

    goto :goto_4

    .line 133
    :catch_1
    move-exception v4

    .line 134
    .local v4, "e":Ljava/lang/Exception;
    :try_start_2
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "readInputStream "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 136
    nop

    .line 138
    .end local v4    # "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 141
    :goto_2
    goto :goto_3

    .line 139
    :catch_2
    move-exception v4

    .line 140
    .local v4, "e":Ljava/io/IOException;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .end local v4    # "e":Ljava/io/IOException;
    goto :goto_2

    .line 144
    :goto_3
    const/4 v0, 0x0

    return-object v0

    .line 138
    :goto_4
    :try_start_4
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 141
    goto :goto_5

    .line 139
    :catch_3
    move-exception v7

    .line 140
    .restart local v7    # "e":Ljava/io/IOException;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    .end local v7    # "e":Ljava/io/IOException;
    :goto_5
    throw v4
.end method


# virtual methods
.method protected isPowerSaveMode()Z
    .locals 1

    .line 42
    iget-boolean v0, p0, Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;->mIsPowerSaveMode:Z

    return v0
.end method
