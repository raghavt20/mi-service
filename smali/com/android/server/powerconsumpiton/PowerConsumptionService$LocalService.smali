.class Lcom/android/server/powerconsumpiton/PowerConsumptionService$LocalService;
.super Lcom/android/server/PowerConsumptionServiceInternal;
.source "PowerConsumptionService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/powerconsumpiton/PowerConsumptionService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LocalService"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/powerconsumpiton/PowerConsumptionService;


# direct methods
.method private constructor <init>(Lcom/android/server/powerconsumpiton/PowerConsumptionService;)V
    .locals 0

    .line 169
    iput-object p1, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionService$LocalService;->this$0:Lcom/android/server/powerconsumpiton/PowerConsumptionService;

    invoke-direct {p0}, Lcom/android/server/PowerConsumptionServiceInternal;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/powerconsumpiton/PowerConsumptionService;Lcom/android/server/powerconsumpiton/PowerConsumptionService$LocalService-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/powerconsumpiton/PowerConsumptionService$LocalService;-><init>(Lcom/android/server/powerconsumpiton/PowerConsumptionService;)V

    return-void
.end method


# virtual methods
.method public noteFoucsChangeForPowerConsumption(Ljava/lang/String;Lcom/android/server/wm/IMiuiWindowStateEx;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "miuiWindowStateEx"    # Lcom/android/server/wm/IMiuiWindowStateEx;

    .line 184
    iget-object v0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionService$LocalService;->this$0:Lcom/android/server/powerconsumpiton/PowerConsumptionService;

    invoke-static {v0}, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->-$$Nest$fgetmIsSupportPCS(Lcom/android/server/powerconsumpiton/PowerConsumptionService;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 185
    return-void

    .line 187
    :cond_0
    invoke-static {}, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->-$$Nest$sfgetsDEBUG()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 188
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "noteFoucsChangeForPowerConsumption "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PowerConsumptionService"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    :cond_1
    new-instance v0, Lcom/android/server/powerconsumpiton/PowerConsumptionService$LocalService$$ExternalSyntheticLambda0;

    invoke-direct {v0}, Lcom/android/server/powerconsumpiton/PowerConsumptionService$LocalService$$ExternalSyntheticLambda0;-><init>()V

    iget-object v1, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionService$LocalService;->this$0:Lcom/android/server/powerconsumpiton/PowerConsumptionService;

    invoke-static {v1}, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->-$$Nest$fgetmPowerConsumptionServiceController(Lcom/android/server/powerconsumpiton/PowerConsumptionService;)Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;

    move-result-object v1

    invoke-static {v0, v1, p1, p2}, Lcom/android/internal/util/function/pooled/PooledLambda;->obtainMessage(Lcom/android/internal/util/function/TriConsumer;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 191
    .local v0, "message":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionService$LocalService;->this$0:Lcom/android/server/powerconsumpiton/PowerConsumptionService;

    invoke-static {v1}, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->-$$Nest$fgetmPCFgHandler(Lcom/android/server/powerconsumpiton/PowerConsumptionService;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 192
    return-void
.end method

.method public noteStartActivityForPowerConsumption(Ljava/lang/String;)V
    .locals 2
    .param p1, "record"    # Ljava/lang/String;

    .line 172
    iget-object v0, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionService$LocalService;->this$0:Lcom/android/server/powerconsumpiton/PowerConsumptionService;

    invoke-static {v0}, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->-$$Nest$fgetmIsSupportPCS(Lcom/android/server/powerconsumpiton/PowerConsumptionService;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 173
    return-void

    .line 175
    :cond_0
    invoke-static {}, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->-$$Nest$sfgetsDEBUG()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 176
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "noteStartActivityForPowerConsumption "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PowerConsumptionService"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    :cond_1
    new-instance v0, Lcom/android/server/powerconsumpiton/PowerConsumptionService$LocalService$$ExternalSyntheticLambda1;

    invoke-direct {v0}, Lcom/android/server/powerconsumpiton/PowerConsumptionService$LocalService$$ExternalSyntheticLambda1;-><init>()V

    iget-object v1, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionService$LocalService;->this$0:Lcom/android/server/powerconsumpiton/PowerConsumptionService;

    invoke-static {v1}, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->-$$Nest$fgetmPowerConsumptionServiceController(Lcom/android/server/powerconsumpiton/PowerConsumptionService;)Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;

    move-result-object v1

    invoke-static {v0, v1, p1}, Lcom/android/internal/util/function/pooled/PooledLambda;->obtainMessage(Ljava/util/function/BiConsumer;Ljava/lang/Object;Ljava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 179
    .local v0, "message":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/powerconsumpiton/PowerConsumptionService$LocalService;->this$0:Lcom/android/server/powerconsumpiton/PowerConsumptionService;

    invoke-static {v1}, Lcom/android/server/powerconsumpiton/PowerConsumptionService;->-$$Nest$fgetmPCFgHandler(Lcom/android/server/powerconsumpiton/PowerConsumptionService;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 180
    return-void
.end method
