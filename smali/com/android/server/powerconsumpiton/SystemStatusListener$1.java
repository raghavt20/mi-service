class com.android.server.powerconsumpiton.SystemStatusListener$1 extends android.hardware.camera2.CameraManager$AvailabilityCallback {
	 /* .source "SystemStatusListener.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/powerconsumpiton/SystemStatusListener; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.powerconsumpiton.SystemStatusListener this$0; //synthetic
/* # direct methods */
 com.android.server.powerconsumpiton.SystemStatusListener$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/powerconsumpiton/SystemStatusListener; */
/* .line 89 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/hardware/camera2/CameraManager$AvailabilityCallback;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onCameraAvailable ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "cameraId" # Ljava/lang/String; */
/* .line 92 */
/* invoke-super {p0, p1}, Landroid/hardware/camera2/CameraManager$AvailabilityCallback;->onCameraAvailable(Ljava/lang/String;)V */
/* .line 93 */
v0 = java.lang.Integer .parseInt ( p1 );
/* .line 94 */
/* .local v0, "id":I */
/* const/16 v1, 0x64 */
/* if-lt v0, v1, :cond_0 */
/* .line 95 */
return;
/* .line 97 */
} // :cond_0
v1 = this.this$0;
com.android.server.powerconsumpiton.SystemStatusListener .-$$Nest$fgetmOpeningCameraID ( v1 );
java.lang.Integer .valueOf ( v0 );
/* .line 99 */
return;
} // .end method
public void onCameraUnavailable ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "cameraId" # Ljava/lang/String; */
/* .line 103 */
/* invoke-super {p0, p1}, Landroid/hardware/camera2/CameraManager$AvailabilityCallback;->onCameraUnavailable(Ljava/lang/String;)V */
/* .line 104 */
v0 = java.lang.Integer .parseInt ( p1 );
/* .line 105 */
/* .local v0, "id":I */
/* const/16 v1, 0x64 */
/* if-lt v0, v1, :cond_0 */
/* .line 106 */
return;
/* .line 108 */
} // :cond_0
v1 = this.this$0;
com.android.server.powerconsumpiton.SystemStatusListener .-$$Nest$fgetmOpeningCameraID ( v1 );
java.lang.Integer .valueOf ( v0 );
/* .line 109 */
return;
} // .end method
