.class Lcom/android/server/powerconsumpiton/SystemStatusListener$1;
.super Landroid/hardware/camera2/CameraManager$AvailabilityCallback;
.source "SystemStatusListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/powerconsumpiton/SystemStatusListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/powerconsumpiton/SystemStatusListener;


# direct methods
.method constructor <init>(Lcom/android/server/powerconsumpiton/SystemStatusListener;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/powerconsumpiton/SystemStatusListener;

    .line 89
    iput-object p1, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener$1;->this$0:Lcom/android/server/powerconsumpiton/SystemStatusListener;

    invoke-direct {p0}, Landroid/hardware/camera2/CameraManager$AvailabilityCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onCameraAvailable(Ljava/lang/String;)V
    .locals 3
    .param p1, "cameraId"    # Ljava/lang/String;

    .line 92
    invoke-super {p0, p1}, Landroid/hardware/camera2/CameraManager$AvailabilityCallback;->onCameraAvailable(Ljava/lang/String;)V

    .line 93
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 94
    .local v0, "id":I
    const/16 v1, 0x64

    if-lt v0, v1, :cond_0

    .line 95
    return-void

    .line 97
    :cond_0
    iget-object v1, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener$1;->this$0:Lcom/android/server/powerconsumpiton/SystemStatusListener;

    invoke-static {v1}, Lcom/android/server/powerconsumpiton/SystemStatusListener;->-$$Nest$fgetmOpeningCameraID(Lcom/android/server/powerconsumpiton/SystemStatusListener;)Ljava/util/Set;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 99
    return-void
.end method

.method public onCameraUnavailable(Ljava/lang/String;)V
    .locals 3
    .param p1, "cameraId"    # Ljava/lang/String;

    .line 103
    invoke-super {p0, p1}, Landroid/hardware/camera2/CameraManager$AvailabilityCallback;->onCameraUnavailable(Ljava/lang/String;)V

    .line 104
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 105
    .local v0, "id":I
    const/16 v1, 0x64

    if-lt v0, v1, :cond_0

    .line 106
    return-void

    .line 108
    :cond_0
    iget-object v1, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener$1;->this$0:Lcom/android/server/powerconsumpiton/SystemStatusListener;

    invoke-static {v1}, Lcom/android/server/powerconsumpiton/SystemStatusListener;->-$$Nest$fgetmOpeningCameraID(Lcom/android/server/powerconsumpiton/SystemStatusListener;)Ljava/util/Set;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 109
    return-void
.end method
