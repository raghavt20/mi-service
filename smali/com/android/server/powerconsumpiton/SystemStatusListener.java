public class com.android.server.powerconsumpiton.SystemStatusListener {
	 /* .source "SystemStatusListener.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/powerconsumpiton/SystemStatusListener$PowerConsumptionPointerEventListener;, */
	 /* Lcom/android/server/powerconsumpiton/SystemStatusListener$SettingsObserver; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Long DECREASE_BRIGHTNESS_WAIT_TIME;
private static final java.lang.String GAME_MODE_URI;
private static final java.lang.String GTB_BRIGHTNESS_ADJUST;
private static final Boolean SUPPORT_GAME_DIM;
private static final Boolean SUPPORT_VIDEO_IDLE_DIM;
private static final java.lang.String VIDEO_IDLE_STATUS;
private static final java.lang.String VIDEO_IDLE_TIMEOUT;
private static final Integer VIRTUAL_CAMERA_BOUNDARY;
/* # instance fields */
private final android.hardware.camera2.CameraManager$AvailabilityCallback mAvailabilityCallback;
private Long mBrightWaitTime;
private android.hardware.camera2.CameraManager mCameraManager;
private android.content.Context mContext;
private Boolean mDimEnable;
private android.os.Handler mHandler;
private final java.util.Set mOpeningCameraID;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.android.server.powerconsumpiton.SystemStatusListener$PowerConsumptionPointerEventListener mPowerConsumptionPointerEventListener;
private com.android.server.powerconsumpiton.PowerConsumptionServiceController mPowerConsumptionServiceController;
private com.android.server.powerconsumpiton.FrameworkPowerSaveModeObserver mPowerSaveModeObserver;
private android.content.ContentResolver mResolver;
private com.android.server.powerconsumpiton.SystemStatusListener$SettingsObserver mSettingsObserver;
private Boolean mVideoIdleStatus;
private com.android.server.wm.WindowManagerService mWindowManagerService;
/* # direct methods */
static java.util.Set -$$Nest$fgetmOpeningCameraID ( com.android.server.powerconsumpiton.SystemStatusListener p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mOpeningCameraID;
} // .end method
static com.android.server.powerconsumpiton.PowerConsumptionServiceController -$$Nest$fgetmPowerConsumptionServiceController ( com.android.server.powerconsumpiton.SystemStatusListener p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mPowerConsumptionServiceController;
} // .end method
static void -$$Nest$mhandleSettingsChangedLocked ( com.android.server.powerconsumpiton.SystemStatusListener p0, android.net.Uri p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/powerconsumpiton/SystemStatusListener;->handleSettingsChangedLocked(Landroid/net/Uri;)V */
return;
} // .end method
static com.android.server.powerconsumpiton.SystemStatusListener ( ) {
/* .locals 2 */
/* .line 27 */
/* const-string/jumbo v0, "support_game_dim" */
int v1 = 0; // const/4 v1, 0x0
v0 = miui.util.FeatureParser .getBoolean ( v0,v1 );
com.android.server.powerconsumpiton.SystemStatusListener.SUPPORT_GAME_DIM = (v0!= 0);
/* .line 31 */
/* const-string/jumbo v0, "support_video_idle_dim" */
v0 = miui.util.FeatureParser .getBoolean ( v0,v1 );
com.android.server.powerconsumpiton.SystemStatusListener.SUPPORT_VIDEO_IDLE_DIM = (v0!= 0);
return;
} // .end method
public com.android.server.powerconsumpiton.SystemStatusListener ( ) {
/* .locals 2 */
/* .param p1, "handler" # Landroid/os/Handler; */
/* .param p2, "context" # Landroid/content/Context; */
/* .line 56 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 37 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.mOpeningCameraID = v0;
/* .line 48 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mDimEnable:Z */
/* .line 49 */
/* const-wide/32 v0, 0x2bf20 */
/* iput-wide v0, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mBrightWaitTime:J */
/* .line 89 */
/* new-instance v0, Lcom/android/server/powerconsumpiton/SystemStatusListener$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/powerconsumpiton/SystemStatusListener$1;-><init>(Lcom/android/server/powerconsumpiton/SystemStatusListener;)V */
this.mAvailabilityCallback = v0;
/* .line 57 */
this.mHandler = p1;
/* .line 58 */
this.mContext = p2;
/* .line 59 */
final String v0 = "camera"; // const-string v0, "camera"
(( android.content.Context ) p2 ).getSystemService ( v0 ); // invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/hardware/camera2/CameraManager; */
this.mCameraManager = v0;
/* .line 60 */
/* nop */
/* .line 61 */
/* const-string/jumbo v0, "window" */
android.os.ServiceManager .getService ( v0 );
/* check-cast v0, Lcom/android/server/wm/WindowManagerService; */
this.mWindowManagerService = v0;
/* .line 62 */
/* new-instance v0, Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver; */
v1 = this.mHandler;
(( android.os.Handler ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p2, v1}, Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;-><init>(Landroid/content/Context;Landroid/os/Looper;)V */
this.mPowerSaveModeObserver = v0;
/* .line 63 */
/* new-instance v0, Lcom/android/server/powerconsumpiton/SystemStatusListener$PowerConsumptionPointerEventListener; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, p0, v1}, Lcom/android/server/powerconsumpiton/SystemStatusListener$PowerConsumptionPointerEventListener;-><init>(Lcom/android/server/powerconsumpiton/SystemStatusListener;Lcom/android/server/powerconsumpiton/SystemStatusListener$PowerConsumptionPointerEventListener-IA;)V */
this.mPowerConsumptionPointerEventListener = v0;
/* .line 64 */
/* invoke-direct {p0}, Lcom/android/server/powerconsumpiton/SystemStatusListener;->initSettingsObserver()V */
/* .line 65 */
return;
} // .end method
private void handleSettingsChangedLocked ( android.net.Uri p0 ) {
/* .locals 2 */
/* .param p1, "uri" # Landroid/net/Uri; */
/* .line 125 */
(( android.net.Uri ) p1 ).getLastPathSegment ( ); // invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;
v1 = (( java.lang.String ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/String;->hashCode()I
/* sparse-switch v1, :sswitch_data_0 */
} // :cond_0
/* :sswitch_0 */
final String v1 = "gb_brightness"; // const-string v1, "gb_brightness"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
/* :sswitch_1 */
final String v1 = "gb_boosting"; // const-string v1, "gb_boosting"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 0; // const/4 v0, 0x0
/* :sswitch_2 */
/* const-string/jumbo v1, "video_idle_timeout" */
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
	 int v0 = 3; // const/4 v0, 0x3
	 /* :sswitch_3 */
	 /* const-string/jumbo v1, "video_idle_status" */
	 v0 = 	 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 int v0 = 2; // const/4 v0, 0x2
	 } // :goto_0
	 int v0 = -1; // const/4 v0, -0x1
} // :goto_1
/* packed-switch v0, :pswitch_data_0 */
/* .line 132 */
/* :pswitch_0 */
/* invoke-direct {p0}, Lcom/android/server/powerconsumpiton/SystemStatusListener;->updateVideoDimState()V */
/* .line 133 */
/* .line 128 */
/* :pswitch_1 */
/* invoke-direct {p0}, Lcom/android/server/powerconsumpiton/SystemStatusListener;->updateGameDimState()V */
/* .line 129 */
/* nop */
/* .line 137 */
} // :goto_2
return;
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x5d7c9707 -> :sswitch_3 */
/* -0x2f532ac6 -> :sswitch_2 */
/* -0x167de7bd -> :sswitch_1 */
/* 0x50396e35 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
/* :pswitch_1 */
/* :pswitch_0 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private void initSettingsObserver ( ) {
/* .locals 5 */
/* .line 68 */
v0 = this.mCameraManager;
v1 = this.mAvailabilityCallback;
v2 = this.mHandler;
(( android.hardware.camera2.CameraManager ) v0 ).registerAvailabilityCallback ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/hardware/camera2/CameraManager;->registerAvailabilityCallback(Landroid/hardware/camera2/CameraManager$AvailabilityCallback;Landroid/os/Handler;)V
/* .line 69 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
this.mResolver = v0;
/* .line 70 */
/* new-instance v0, Lcom/android/server/powerconsumpiton/SystemStatusListener$SettingsObserver; */
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/powerconsumpiton/SystemStatusListener$SettingsObserver;-><init>(Lcom/android/server/powerconsumpiton/SystemStatusListener;Landroid/os/Handler;)V */
this.mSettingsObserver = v0;
/* .line 72 */
v0 = this.mResolver;
final String v1 = "gb_boosting"; // const-string v1, "gb_boosting"
android.provider.Settings$Secure .getUriFor ( v1 );
v2 = this.mSettingsObserver;
int v3 = 1; // const/4 v3, 0x1
int v4 = -1; // const/4 v4, -0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 74 */
v0 = this.mResolver;
final String v1 = "gb_brightness"; // const-string v1, "gb_brightness"
android.provider.Settings$Secure .getUriFor ( v1 );
v2 = this.mSettingsObserver;
int v3 = 0; // const/4 v3, 0x0
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 76 */
/* sget-boolean v0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->SUPPORT_VIDEO_IDLE_DIM:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 77 */
v0 = this.mResolver;
/* const-string/jumbo v1, "video_idle_status" */
android.provider.Settings$Secure .getUriFor ( v1 );
v2 = this.mSettingsObserver;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 79 */
v0 = this.mResolver;
/* const-string/jumbo v1, "video_idle_timeout" */
android.provider.Settings$Secure .getUriFor ( v1 );
v2 = this.mSettingsObserver;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 82 */
} // :cond_0
v0 = this.mWindowManagerService;
v1 = this.mPowerConsumptionPointerEventListener;
(( com.android.server.wm.WindowManagerService ) v0 ).registerPointerEventListener ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lcom/android/server/wm/WindowManagerService;->registerPointerEventListener(Landroid/view/WindowManagerPolicyConstants$PointerEventListener;I)V
/* .line 83 */
return;
} // .end method
private void updateGameDimState ( ) {
/* .locals 7 */
/* .line 140 */
v0 = this.mResolver;
final String v1 = "gb_brightness"; // const-string v1, "gb_brightness"
int v2 = 1; // const/4 v2, 0x1
int v3 = -2; // const/4 v3, -0x2
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v1,v2,v3 );
int v1 = 0; // const/4 v1, 0x0
/* if-ne v0, v2, :cond_0 */
/* move v0, v2 */
} // :cond_0
/* move v0, v1 */
/* .line 142 */
/* .local v0, "gameBrightnessEnable":Z */
} // :goto_0
v4 = this.mResolver;
final String v5 = "gb_boosting"; // const-string v5, "gb_boosting"
v3 = android.provider.Settings$Secure .getIntForUser ( v4,v5,v1,v3 );
/* if-ne v3, v2, :cond_1 */
/* move v3, v2 */
} // :cond_1
/* move v3, v1 */
/* .line 144 */
/* .local v3, "gameModeEnable":Z */
} // :goto_1
/* const-wide/32 v4, 0x2bf20 */
/* iput-wide v4, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mBrightWaitTime:J */
/* .line 145 */
if ( v0 != null) { // if-eqz v0, :cond_2
if ( v3 != null) { // if-eqz v3, :cond_2
/* sget-boolean v6, Lcom/android/server/powerconsumpiton/SystemStatusListener;->SUPPORT_GAME_DIM:Z */
if ( v6 != null) { // if-eqz v6, :cond_2
} // :cond_2
/* move v2, v1 */
} // :goto_2
/* iput-boolean v2, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mDimEnable:Z */
/* .line 146 */
v1 = this.mPowerConsumptionServiceController;
(( com.android.server.powerconsumpiton.PowerConsumptionServiceController ) v1 ).updateDimState ( v2, v4, v5 ); // invoke-virtual {v1, v2, v4, v5}, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->updateDimState(ZJ)V
/* .line 147 */
return;
} // .end method
private void updateVideoDimState ( ) {
/* .locals 7 */
/* .line 150 */
v0 = this.mResolver;
/* const-string/jumbo v1, "video_idle_status" */
int v2 = 0; // const/4 v2, 0x0
int v3 = -2; // const/4 v3, -0x2
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v1,v2,v3 );
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* move v0, v1 */
} // :cond_0
/* move v0, v2 */
} // :goto_0
/* iput-boolean v0, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mVideoIdleStatus:Z */
/* .line 152 */
v0 = this.mResolver;
/* const-string/jumbo v4, "video_idle_timeout" */
/* const-wide/32 v5, 0x2bf20 */
android.provider.Settings$Secure .getLongForUser ( v0,v4,v5,v6,v3 );
/* move-result-wide v3 */
/* iput-wide v3, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mBrightWaitTime:J */
/* .line 154 */
/* iget-boolean v0, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mVideoIdleStatus:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* sget-boolean v0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->SUPPORT_VIDEO_IDLE_DIM:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* move v2, v1 */
} // :cond_1
/* iput-boolean v2, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mDimEnable:Z */
/* .line 155 */
v0 = this.mPowerConsumptionServiceController;
(( com.android.server.powerconsumpiton.PowerConsumptionServiceController ) v0 ).updateDimState ( v2, v3, v4 ); // invoke-virtual {v0, v2, v3, v4}, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->updateDimState(ZJ)V
/* .line 156 */
return;
} // .end method
/* # virtual methods */
void dump ( java.io.PrintWriter p0 ) {
/* .locals 3 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 179 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " isCameraOpen="; // const-string v1, " isCameraOpen="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = (( com.android.server.powerconsumpiton.SystemStatusListener ) p0 ).isCameraOpen ( ); // invoke-virtual {p0}, Lcom/android/server/powerconsumpiton/SystemStatusListener;->isCameraOpen()Z
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 180 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mDimEnable="; // const-string v1, " mDimEnable="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mDimEnable:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 181 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mBrightWaitTime="; // const-string v1, " mBrightWaitTime="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mBrightWaitTime:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 182 */
return;
} // .end method
public Boolean isCameraOpen ( ) {
/* .locals 1 */
/* .line 159 */
v0 = v0 = this.mOpeningCameraID;
/* if-lez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
protected Boolean isPowerSaveMode ( ) {
/* .locals 1 */
/* .line 163 */
v0 = this.mPowerSaveModeObserver;
v0 = (( com.android.server.powerconsumpiton.FrameworkPowerSaveModeObserver ) v0 ).isPowerSaveMode ( ); // invoke-virtual {v0}, Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;->isPowerSaveMode()Z
} // .end method
public void setPowerConsumptionServiceController ( com.android.server.powerconsumpiton.PowerConsumptionServiceController p0 ) {
/* .locals 0 */
/* .param p1, "powerConsumptionServiceController" # Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController; */
/* .line 86 */
this.mPowerConsumptionServiceController = p1;
/* .line 87 */
return;
} // .end method
