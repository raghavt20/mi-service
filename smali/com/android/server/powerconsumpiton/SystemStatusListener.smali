.class public Lcom/android/server/powerconsumpiton/SystemStatusListener;
.super Ljava/lang/Object;
.source "SystemStatusListener.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/powerconsumpiton/SystemStatusListener$PowerConsumptionPointerEventListener;,
        Lcom/android/server/powerconsumpiton/SystemStatusListener$SettingsObserver;
    }
.end annotation


# static fields
.field private static final DECREASE_BRIGHTNESS_WAIT_TIME:J = 0x2bf20L

.field private static final GAME_MODE_URI:Ljava/lang/String; = "gb_boosting"

.field private static final GTB_BRIGHTNESS_ADJUST:Ljava/lang/String; = "gb_brightness"

.field private static final SUPPORT_GAME_DIM:Z

.field private static final SUPPORT_VIDEO_IDLE_DIM:Z

.field private static final VIDEO_IDLE_STATUS:Ljava/lang/String; = "video_idle_status"

.field private static final VIDEO_IDLE_TIMEOUT:Ljava/lang/String; = "video_idle_timeout"

.field private static final VIRTUAL_CAMERA_BOUNDARY:I = 0x64


# instance fields
.field private final mAvailabilityCallback:Landroid/hardware/camera2/CameraManager$AvailabilityCallback;

.field private mBrightWaitTime:J

.field private mCameraManager:Landroid/hardware/camera2/CameraManager;

.field private mContext:Landroid/content/Context;

.field private mDimEnable:Z

.field private mHandler:Landroid/os/Handler;

.field private final mOpeningCameraID:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mPowerConsumptionPointerEventListener:Lcom/android/server/powerconsumpiton/SystemStatusListener$PowerConsumptionPointerEventListener;

.field private mPowerConsumptionServiceController:Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;

.field private mPowerSaveModeObserver:Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;

.field private mResolver:Landroid/content/ContentResolver;

.field private mSettingsObserver:Lcom/android/server/powerconsumpiton/SystemStatusListener$SettingsObserver;

.field private mVideoIdleStatus:Z

.field private mWindowManagerService:Lcom/android/server/wm/WindowManagerService;


# direct methods
.method static bridge synthetic -$$Nest$fgetmOpeningCameraID(Lcom/android/server/powerconsumpiton/SystemStatusListener;)Ljava/util/Set;
    .locals 0

    iget-object p0, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mOpeningCameraID:Ljava/util/Set;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPowerConsumptionServiceController(Lcom/android/server/powerconsumpiton/SystemStatusListener;)Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;
    .locals 0

    iget-object p0, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mPowerConsumptionServiceController:Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mhandleSettingsChangedLocked(Lcom/android/server/powerconsumpiton/SystemStatusListener;Landroid/net/Uri;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/powerconsumpiton/SystemStatusListener;->handleSettingsChangedLocked(Landroid/net/Uri;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 27
    const-string/jumbo v0, "support_game_dim"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->SUPPORT_GAME_DIM:Z

    .line 31
    const-string/jumbo v0, "support_video_idle_dim"

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->SUPPORT_VIDEO_IDLE_DIM:Z

    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;Landroid/content/Context;)V
    .locals 2
    .param p1, "handler"    # Landroid/os/Handler;
    .param p2, "context"    # Landroid/content/Context;

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mOpeningCameraID:Ljava/util/Set;

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mDimEnable:Z

    .line 49
    const-wide/32 v0, 0x2bf20

    iput-wide v0, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mBrightWaitTime:J

    .line 89
    new-instance v0, Lcom/android/server/powerconsumpiton/SystemStatusListener$1;

    invoke-direct {v0, p0}, Lcom/android/server/powerconsumpiton/SystemStatusListener$1;-><init>(Lcom/android/server/powerconsumpiton/SystemStatusListener;)V

    iput-object v0, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mAvailabilityCallback:Landroid/hardware/camera2/CameraManager$AvailabilityCallback;

    .line 57
    iput-object p1, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mHandler:Landroid/os/Handler;

    .line 58
    iput-object p2, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mContext:Landroid/content/Context;

    .line 59
    const-string v0, "camera"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/camera2/CameraManager;

    iput-object v0, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mCameraManager:Landroid/hardware/camera2/CameraManager;

    .line 60
    nop

    .line 61
    const-string/jumbo v0, "window"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/WindowManagerService;

    iput-object v0, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mWindowManagerService:Lcom/android/server/wm/WindowManagerService;

    .line 62
    new-instance v0, Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;

    iget-object v1, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p2, v1}, Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;-><init>(Landroid/content/Context;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mPowerSaveModeObserver:Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;

    .line 63
    new-instance v0, Lcom/android/server/powerconsumpiton/SystemStatusListener$PowerConsumptionPointerEventListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/server/powerconsumpiton/SystemStatusListener$PowerConsumptionPointerEventListener;-><init>(Lcom/android/server/powerconsumpiton/SystemStatusListener;Lcom/android/server/powerconsumpiton/SystemStatusListener$PowerConsumptionPointerEventListener-IA;)V

    iput-object v0, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mPowerConsumptionPointerEventListener:Lcom/android/server/powerconsumpiton/SystemStatusListener$PowerConsumptionPointerEventListener;

    .line 64
    invoke-direct {p0}, Lcom/android/server/powerconsumpiton/SystemStatusListener;->initSettingsObserver()V

    .line 65
    return-void
.end method

.method private handleSettingsChangedLocked(Landroid/net/Uri;)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .line 125
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    goto :goto_0

    :sswitch_0
    const-string v1, "gb_brightness"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_1
    const-string v1, "gb_boosting"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_2
    const-string/jumbo v1, "video_idle_timeout"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    goto :goto_1

    :sswitch_3
    const-string/jumbo v1, "video_idle_status"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    goto :goto_1

    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_0

    goto :goto_2

    .line 132
    :pswitch_0
    invoke-direct {p0}, Lcom/android/server/powerconsumpiton/SystemStatusListener;->updateVideoDimState()V

    .line 133
    goto :goto_2

    .line 128
    :pswitch_1
    invoke-direct {p0}, Lcom/android/server/powerconsumpiton/SystemStatusListener;->updateGameDimState()V

    .line 129
    nop

    .line 137
    :goto_2
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x5d7c9707 -> :sswitch_3
        -0x2f532ac6 -> :sswitch_2
        -0x167de7bd -> :sswitch_1
        0x50396e35 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private initSettingsObserver()V
    .locals 5

    .line 68
    iget-object v0, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mCameraManager:Landroid/hardware/camera2/CameraManager;

    iget-object v1, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mAvailabilityCallback:Landroid/hardware/camera2/CameraManager$AvailabilityCallback;

    iget-object v2, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1, v2}, Landroid/hardware/camera2/CameraManager;->registerAvailabilityCallback(Landroid/hardware/camera2/CameraManager$AvailabilityCallback;Landroid/os/Handler;)V

    .line 69
    iget-object v0, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mResolver:Landroid/content/ContentResolver;

    .line 70
    new-instance v0, Lcom/android/server/powerconsumpiton/SystemStatusListener$SettingsObserver;

    iget-object v1, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/android/server/powerconsumpiton/SystemStatusListener$SettingsObserver;-><init>(Lcom/android/server/powerconsumpiton/SystemStatusListener;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mSettingsObserver:Lcom/android/server/powerconsumpiton/SystemStatusListener$SettingsObserver;

    .line 72
    iget-object v0, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "gb_boosting"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mSettingsObserver:Lcom/android/server/powerconsumpiton/SystemStatusListener$SettingsObserver;

    const/4 v3, 0x1

    const/4 v4, -0x1

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 74
    iget-object v0, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "gb_brightness"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mSettingsObserver:Lcom/android/server/powerconsumpiton/SystemStatusListener$SettingsObserver;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 76
    sget-boolean v0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->SUPPORT_VIDEO_IDLE_DIM:Z

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mResolver:Landroid/content/ContentResolver;

    const-string/jumbo v1, "video_idle_status"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mSettingsObserver:Lcom/android/server/powerconsumpiton/SystemStatusListener$SettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 79
    iget-object v0, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mResolver:Landroid/content/ContentResolver;

    const-string/jumbo v1, "video_idle_timeout"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mSettingsObserver:Lcom/android/server/powerconsumpiton/SystemStatusListener$SettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 82
    :cond_0
    iget-object v0, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mWindowManagerService:Lcom/android/server/wm/WindowManagerService;

    iget-object v1, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mPowerConsumptionPointerEventListener:Lcom/android/server/powerconsumpiton/SystemStatusListener$PowerConsumptionPointerEventListener;

    invoke-virtual {v0, v1, v3}, Lcom/android/server/wm/WindowManagerService;->registerPointerEventListener(Landroid/view/WindowManagerPolicyConstants$PointerEventListener;I)V

    .line 83
    return-void
.end method

.method private updateGameDimState()V
    .locals 7

    .line 140
    iget-object v0, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "gb_brightness"

    const/4 v2, 0x1

    const/4 v3, -0x2

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    const/4 v1, 0x0

    if-ne v0, v2, :cond_0

    move v0, v2

    goto :goto_0

    :cond_0
    move v0, v1

    .line 142
    .local v0, "gameBrightnessEnable":Z
    :goto_0
    iget-object v4, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mResolver:Landroid/content/ContentResolver;

    const-string v5, "gb_boosting"

    invoke-static {v4, v5, v1, v3}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v3

    if-ne v3, v2, :cond_1

    move v3, v2

    goto :goto_1

    :cond_1
    move v3, v1

    .line 144
    .local v3, "gameModeEnable":Z
    :goto_1
    const-wide/32 v4, 0x2bf20

    iput-wide v4, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mBrightWaitTime:J

    .line 145
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    sget-boolean v6, Lcom/android/server/powerconsumpiton/SystemStatusListener;->SUPPORT_GAME_DIM:Z

    if-eqz v6, :cond_2

    goto :goto_2

    :cond_2
    move v2, v1

    :goto_2
    iput-boolean v2, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mDimEnable:Z

    .line 146
    iget-object v1, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mPowerConsumptionServiceController:Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;

    invoke-virtual {v1, v2, v4, v5}, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->updateDimState(ZJ)V

    .line 147
    return-void
.end method

.method private updateVideoDimState()V
    .locals 7

    .line 150
    iget-object v0, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mResolver:Landroid/content/ContentResolver;

    const-string/jumbo v1, "video_idle_status"

    const/4 v2, 0x0

    const/4 v3, -0x2

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v2

    :goto_0
    iput-boolean v0, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mVideoIdleStatus:Z

    .line 152
    iget-object v0, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mResolver:Landroid/content/ContentResolver;

    const-string/jumbo v4, "video_idle_timeout"

    const-wide/32 v5, 0x2bf20

    invoke-static {v0, v4, v5, v6, v3}, Landroid/provider/Settings$Secure;->getLongForUser(Landroid/content/ContentResolver;Ljava/lang/String;JI)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mBrightWaitTime:J

    .line 154
    iget-boolean v0, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mVideoIdleStatus:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->SUPPORT_VIDEO_IDLE_DIM:Z

    if-eqz v0, :cond_1

    move v2, v1

    :cond_1
    iput-boolean v2, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mDimEnable:Z

    .line 155
    iget-object v0, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mPowerConsumptionServiceController:Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;

    invoke-virtual {v0, v2, v3, v4}, Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;->updateDimState(ZJ)V

    .line 156
    return-void
.end method


# virtual methods
.method dump(Ljava/io/PrintWriter;)V
    .locals 3
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 179
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  isCameraOpen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/server/powerconsumpiton/SystemStatusListener;->isCameraOpen()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 180
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mDimEnable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mDimEnable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 181
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mBrightWaitTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mBrightWaitTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 182
    return-void
.end method

.method public isCameraOpen()Z
    .locals 1

    .line 159
    iget-object v0, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mOpeningCameraID:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method protected isPowerSaveMode()Z
    .locals 1

    .line 163
    iget-object v0, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mPowerSaveModeObserver:Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;

    invoke-virtual {v0}, Lcom/android/server/powerconsumpiton/FrameworkPowerSaveModeObserver;->isPowerSaveMode()Z

    move-result v0

    return v0
.end method

.method public setPowerConsumptionServiceController(Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;)V
    .locals 0
    .param p1, "powerConsumptionServiceController"    # Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;

    .line 86
    iput-object p1, p0, Lcom/android/server/powerconsumpiton/SystemStatusListener;->mPowerConsumptionServiceController:Lcom/android/server/powerconsumpiton/PowerConsumptionServiceController;

    .line 87
    return-void
.end method
