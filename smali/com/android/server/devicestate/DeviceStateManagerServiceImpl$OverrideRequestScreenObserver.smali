.class Lcom/android/server/devicestate/DeviceStateManagerServiceImpl$OverrideRequestScreenObserver;
.super Ljava/lang/Object;
.source "DeviceStateManagerServiceImpl.java"

# interfaces
.implements Lcom/android/server/wm/ActivityTaskManagerInternal$ScreenObserver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OverrideRequestScreenObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;


# direct methods
.method private constructor <init>(Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;)V
    .locals 0

    .line 215
    iput-object p1, p0, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl$OverrideRequestScreenObserver;->this$0:Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;Lcom/android/server/devicestate/DeviceStateManagerServiceImpl$OverrideRequestScreenObserver-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl$OverrideRequestScreenObserver;-><init>(Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;)V

    return-void
.end method


# virtual methods
.method public onAwakeStateChanged(Z)V
    .locals 1
    .param p1, "isAwake"    # Z

    .line 220
    iget-object v0, p0, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl$OverrideRequestScreenObserver;->this$0:Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;

    invoke-static {v0, p1}, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->-$$Nest$fputmInteractive(Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;Z)V

    .line 221
    return-void
.end method

.method public onKeyguardStateChanged(Z)V
    .locals 0
    .param p1, "isShowing"    # Z

    .line 225
    return-void
.end method
