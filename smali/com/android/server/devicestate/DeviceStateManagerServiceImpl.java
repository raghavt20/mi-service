public class com.android.server.devicestate.DeviceStateManagerServiceImpl extends com.android.server.devicestate.DeviceStateManagerServiceStub {
	 /* .source "DeviceStateManagerServiceImpl.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.devicestate.DeviceStateManagerServiceStub$$" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lcom/android/server/devicestate/DeviceStateManagerServiceImpl$OverrideRequestScreenObserver; */
/* } */
} // .end annotation
/* # static fields */
private static final java.lang.String CURRENT_DEVICE_STATE;
private static final java.lang.String DEVICE_POSTURE;
private static final Integer FLAG_TENT_MODE;
private static final Integer FOLDABLE_DEVICE_STATE_CLOSED;
private static final Integer FOLDABLE_DEVICE_STATE_HALF_OPENED;
private static final Integer FOLDABLE_DEVICE_STATE_OPENED;
private static final Integer FOLDABLE_DEVICE_STATE_OPENED_PRESENTATION;
private static final Integer FOLDABLE_DEVICE_STATE_OPENED_REVERSE;
private static final Integer FOLDABLE_DEVICE_STATE_OPENED_REVERSE_PRESENTATION;
private static final Integer FOLDABLE_DEVICE_STATE_TENT;
private static final Integer POSTURE_CLOSED;
private static final Integer POSTURE_FLIPPED;
private static final Integer POSTURE_HALF_OPENED;
private static final Integer POSTURE_OPENED;
private static final Integer POSTURE_UNKNOWN;
public static final java.lang.String TAG;
private static final Boolean sDebug;
/* # instance fields */
private com.android.server.wm.ActivityTaskManagerInternal mActivityTaskManagerInternal;
private android.content.Context mContext;
private java.util.Set mFoldedDeviceStates;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Boolean mInteractive;
private java.lang.Object mLock;
private com.android.server.devicestate.OverrideRequestController mOverrideRequestController;
private com.android.server.wm.ActivityTaskManagerInternal$ScreenObserver mOverrideRequestScreenObserver;
private com.android.server.policy.WindowManagerPolicy mPolicy;
private mReversedFoldedDeviceStates;
private com.android.server.devicestate.DeviceStateManagerService mService;
private mStrictFoldedDeviceStates;
/* # direct methods */
static void -$$Nest$fputmInteractive ( com.android.server.devicestate.DeviceStateManagerServiceImpl p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->mInteractive:Z */
return;
} // .end method
public com.android.server.devicestate.DeviceStateManagerServiceImpl ( ) {
/* .locals 2 */
/* .line 22 */
/* invoke-direct {p0}, Lcom/android/server/devicestate/DeviceStateManagerServiceStub;-><init>()V */
/* .line 58 */
/* new-instance v0, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl$OverrideRequestScreenObserver; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, p0, v1}, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl$OverrideRequestScreenObserver;-><init>(Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;Lcom/android/server/devicestate/DeviceStateManagerServiceImpl$OverrideRequestScreenObserver-IA;)V */
this.mOverrideRequestScreenObserver = v0;
/* .line 64 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->mInteractive:Z */
return;
} // .end method
private void cancelStateRequest ( ) {
/* .locals 1 */
/* .line 212 */
v0 = this.mService;
(( com.android.server.devicestate.DeviceStateManagerService ) v0 ).cancelStateRequestInternal ( ); // invoke-virtual {v0}, Lcom/android/server/devicestate/DeviceStateManagerService;->cancelStateRequestInternal()V
/* .line 213 */
return;
} // .end method
private Boolean isReversedState ( ) {
/* .locals 2 */
/* .line 196 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 197 */
try { // :try_start_0
v1 = /* invoke-direct {p0}, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->isReversedStateLocked()Z */
/* monitor-exit v0 */
/* .line 198 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private Boolean isReversedStateLocked ( ) {
/* .locals 2 */
/* .line 202 */
v0 = this.mService;
(( com.android.server.devicestate.DeviceStateManagerService ) v0 ).getCommittedState ( ); // invoke-virtual {v0}, Lcom/android/server/devicestate/DeviceStateManagerService;->getCommittedState()Ljava/util/Optional;
v0 = (( java.util.Optional ) v0 ).isPresent ( ); // invoke-virtual {v0}, Ljava/util/Optional;->isPresent()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 203 */
v0 = this.mService;
(( com.android.server.devicestate.DeviceStateManagerService ) v0 ).getCommittedState ( ); // invoke-virtual {v0}, Lcom/android/server/devicestate/DeviceStateManagerService;->getCommittedState()Ljava/util/Optional;
(( java.util.Optional ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/Optional;->get()Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/devicestate/DeviceState; */
v0 = (( com.android.server.devicestate.DeviceState ) v0 ).getIdentifier ( ); // invoke-virtual {v0}, Lcom/android/server/devicestate/DeviceState;->getIdentifier()I
/* .line 204 */
/* .local v0, "currentState":I */
v1 = this.mReversedFoldedDeviceStates;
v1 = com.android.internal.util.ArrayUtils .contains ( v1,v0 );
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 205 */
	 int v1 = 1; // const/4 v1, 0x1
	 /* .line 208 */
} // .end local v0 # "currentState":I
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private void updateDevicePosture ( Integer p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "baseStateIdentifier" # I */
/* .param p2, "committedStateIdentifier" # I */
/* .line 165 */
v0 = this.mFoldedDeviceStates;
java.lang.Integer .valueOf ( p2 );
v0 = com.android.internal.util.ArrayUtils .contains ( v0,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 166 */
int v0 = 1; // const/4 v0, 0x1
/* .local v0, "devicePosture":I */
/* .line 167 */
} // .end local v0 # "devicePosture":I
} // :cond_0
int v0 = 2; // const/4 v0, 0x2
/* if-ne p2, v0, :cond_1 */
/* .line 168 */
int v0 = 2; // const/4 v0, 0x2
/* .restart local v0 # "devicePosture":I */
/* .line 169 */
} // .end local v0 # "devicePosture":I
} // :cond_1
int v1 = 5; // const/4 v1, 0x5
/* if-ne p2, v1, :cond_2 */
/* if-ne p1, v0, :cond_2 */
/* .line 171 */
int v0 = 2; // const/4 v0, 0x2
/* .restart local v0 # "devicePosture":I */
/* .line 173 */
} // .end local v0 # "devicePosture":I
} // :cond_2
int v0 = 3; // const/4 v0, 0x3
/* .line 176 */
/* .restart local v0 # "devicePosture":I */
} // :goto_0
v1 = this.mContext;
/* .line 177 */
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 176 */
final String v2 = "device_posture"; // const-string v2, "device_posture"
int v3 = -1; // const/4 v3, -0x1
v1 = android.provider.Settings$Global .getInt ( v1,v2,v3 );
/* .line 178 */
/* .local v1, "curDevicePosture":I */
/* if-eq v1, v0, :cond_3 */
/* .line 179 */
v3 = this.mContext;
(( android.content.Context ) v3 ).getContentResolver ( ); // invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$Global .putInt ( v3,v2,v0 );
/* .line 182 */
} // :cond_3
return;
} // .end method
/* # virtual methods */
public Boolean adjustConditionSatisfiedLocked ( Boolean p0, com.android.server.devicestate.DeviceState p1, Integer p2 ) {
/* .locals 1 */
/* .param p1, "conditionSatisfied" # Z */
/* .param p2, "deviceState" # Lcom/android/server/devicestate/DeviceState; */
/* .param p3, "lastReportedState" # I */
/* .line 97 */
if ( p1 != null) { // if-eqz p1, :cond_4
/* if-nez p2, :cond_0 */
/* .line 106 */
} // :cond_0
/* const/high16 v0, -0x80000000 */
v0 = (( com.android.server.devicestate.DeviceState ) p2 ).hasFlag ( v0 ); // invoke-virtual {p2, v0}, Lcom/android/server/devicestate/DeviceState;->hasFlag(I)Z
if ( v0 != null) { // if-eqz v0, :cond_3
v0 = this.mStrictFoldedDeviceStates;
/* .line 107 */
v0 = com.android.internal.util.ArrayUtils .contains ( v0,p3 );
/* if-nez v0, :cond_1 */
/* .line 108 */
v0 = (( com.android.server.devicestate.DeviceState ) p2 ).getIdentifier ( ); // invoke-virtual {p2}, Lcom/android/server/devicestate/DeviceState;->getIdentifier()I
/* if-ne p3, v0, :cond_2 */
} // :cond_1
/* iget-boolean v0, p0, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->mInteractive:Z */
/* if-nez v0, :cond_3 */
/* .line 109 */
} // :cond_2
int p1 = 0; // const/4 p1, 0x0
/* .line 112 */
} // :cond_3
/* .line 98 */
} // :cond_4
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void handleBaseStateChangedLocked ( Integer p0 ) {
/* .locals 7 */
/* .param p1, "lastBaseStateIdentifier" # I */
/* .line 117 */
v0 = this.mFoldedDeviceStates;
/* if-nez v0, :cond_0 */
/* .line 118 */
final String v0 = "DeviceStateManagerServiceImpl"; // const-string v0, "DeviceStateManagerServiceImpl"
final String v1 = "FoldedDeviceStates is null"; // const-string v1, "FoldedDeviceStates is null"
android.util.Slog .i ( v0,v1 );
/* .line 119 */
return;
/* .line 122 */
} // :cond_0
v0 = this.mService;
(( com.android.server.devicestate.DeviceStateManagerService ) v0 ).getBaseState ( ); // invoke-virtual {v0}, Lcom/android/server/devicestate/DeviceStateManagerService;->getBaseState()Ljava/util/Optional;
/* .line 123 */
/* .local v0, "curBaseState":Ljava/util/Optional;, "Ljava/util/Optional<Lcom/android/server/devicestate/DeviceState;>;" */
v1 = this.mService;
(( com.android.server.devicestate.DeviceStateManagerService ) v1 ).getCommittedState ( ); // invoke-virtual {v1}, Lcom/android/server/devicestate/DeviceStateManagerService;->getCommittedState()Ljava/util/Optional;
/* .line 125 */
/* .local v1, "committedState":Ljava/util/Optional;, "Ljava/util/Optional<Lcom/android/server/devicestate/DeviceState;>;" */
int v2 = -1; // const/4 v2, -0x1
int v3 = 1; // const/4 v3, 0x1
int v4 = 0; // const/4 v4, 0x0
/* if-eq p1, v2, :cond_1 */
v2 = this.mFoldedDeviceStates;
/* .line 126 */
v2 = java.lang.Integer .valueOf ( p1 );
if ( v2 != null) { // if-eqz v2, :cond_1
/* move v2, v3 */
} // :cond_1
/* move v2, v4 */
/* .line 127 */
/* .local v2, "isCurrentStateFolded":Z */
} // :goto_0
v5 = (( java.util.Optional ) v0 ).isPresent ( ); // invoke-virtual {v0}, Ljava/util/Optional;->isPresent()Z
if ( v5 != null) { // if-eqz v5, :cond_2
v5 = this.mFoldedDeviceStates;
/* .line 128 */
(( java.util.Optional ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/Optional;->get()Ljava/lang/Object;
/* check-cast v6, Lcom/android/server/devicestate/DeviceState; */
v6 = (( com.android.server.devicestate.DeviceState ) v6 ).getIdentifier ( ); // invoke-virtual {v6}, Lcom/android/server/devicestate/DeviceState;->getIdentifier()I
v5 = java.lang.Integer .valueOf ( v6 );
if ( v5 != null) { // if-eqz v5, :cond_2
} // :cond_2
/* move v3, v4 */
/* .line 130 */
/* .local v3, "isNewStateFolded":Z */
} // :goto_1
/* if-eq v2, v3, :cond_3 */
/* .line 131 */
v4 = this.mOverrideRequestController;
(( com.android.server.devicestate.OverrideRequestController ) v4 ).handleFoldStateChanged ( ); // invoke-virtual {v4}, Lcom/android/server/devicestate/OverrideRequestController;->handleFoldStateChanged()V
/* .line 134 */
} // :cond_3
v4 = (( java.util.Optional ) v0 ).isPresent ( ); // invoke-virtual {v0}, Ljava/util/Optional;->isPresent()Z
if ( v4 != null) { // if-eqz v4, :cond_4
v4 = (( java.util.Optional ) v1 ).isPresent ( ); // invoke-virtual {v1}, Ljava/util/Optional;->isPresent()Z
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 135 */
(( java.util.Optional ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/Optional;->get()Ljava/lang/Object;
/* check-cast v4, Lcom/android/server/devicestate/DeviceState; */
v4 = (( com.android.server.devicestate.DeviceState ) v4 ).getIdentifier ( ); // invoke-virtual {v4}, Lcom/android/server/devicestate/DeviceState;->getIdentifier()I
/* .line 136 */
(( java.util.Optional ) v1 ).get ( ); // invoke-virtual {v1}, Ljava/util/Optional;->get()Ljava/lang/Object;
/* check-cast v5, Lcom/android/server/devicestate/DeviceState; */
v5 = (( com.android.server.devicestate.DeviceState ) v5 ).getIdentifier ( ); // invoke-virtual {v5}, Lcom/android/server/devicestate/DeviceState;->getIdentifier()I
/* .line 135 */
/* invoke-direct {p0, v4, v5}, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->updateDevicePosture(II)V */
/* .line 138 */
} // :cond_4
return;
} // .end method
public void handleCommittedStateChangedLocked ( ) {
/* .locals 6 */
/* .line 142 */
v0 = this.mService;
(( com.android.server.devicestate.DeviceStateManagerService ) v0 ).getBaseState ( ); // invoke-virtual {v0}, Lcom/android/server/devicestate/DeviceStateManagerService;->getBaseState()Ljava/util/Optional;
/* .line 143 */
/* .local v0, "baseState":Ljava/util/Optional;, "Ljava/util/Optional<Lcom/android/server/devicestate/DeviceState;>;" */
v1 = this.mService;
(( com.android.server.devicestate.DeviceStateManagerService ) v1 ).getCommittedState ( ); // invoke-virtual {v1}, Lcom/android/server/devicestate/DeviceStateManagerService;->getCommittedState()Ljava/util/Optional;
/* .line 144 */
/* .local v1, "committedState":Ljava/util/Optional;, "Ljava/util/Optional<Lcom/android/server/devicestate/DeviceState;>;" */
v2 = (( java.util.Optional ) v0 ).isPresent ( ); // invoke-virtual {v0}, Ljava/util/Optional;->isPresent()Z
if ( v2 != null) { // if-eqz v2, :cond_1
v2 = (( java.util.Optional ) v1 ).isPresent ( ); // invoke-virtual {v1}, Ljava/util/Optional;->isPresent()Z
/* if-nez v2, :cond_0 */
/* .line 150 */
} // :cond_0
(( java.util.Optional ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/Optional;->get()Ljava/lang/Object;
/* check-cast v2, Lcom/android/server/devicestate/DeviceState; */
v2 = (( com.android.server.devicestate.DeviceState ) v2 ).getIdentifier ( ); // invoke-virtual {v2}, Lcom/android/server/devicestate/DeviceState;->getIdentifier()I
/* .line 151 */
/* .local v2, "baseStateIdentifier":I */
(( java.util.Optional ) v1 ).get ( ); // invoke-virtual {v1}, Ljava/util/Optional;->get()Ljava/lang/Object;
/* check-cast v3, Lcom/android/server/devicestate/DeviceState; */
v3 = (( com.android.server.devicestate.DeviceState ) v3 ).getIdentifier ( ); // invoke-virtual {v3}, Lcom/android/server/devicestate/DeviceState;->getIdentifier()I
/* .line 152 */
/* .local v3, "committedStateIdentifier":I */
/* invoke-direct {p0, v2, v3}, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->updateDevicePosture(II)V */
/* .line 156 */
v4 = this.mContext;
(( android.content.Context ) v4 ).getContentResolver ( ); // invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v5 = "current_device_state"; // const-string v5, "current_device_state"
android.provider.Settings$Global .putInt ( v4,v5,v3 );
/* .line 158 */
return;
/* .line 145 */
} // .end local v2 # "baseStateIdentifier":I
} // .end local v3 # "committedStateIdentifier":I
} // :cond_1
} // :goto_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "skip handle committed state changed.(" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 146 */
v3 = (( java.util.Optional ) v0 ).isPresent ( ); // invoke-virtual {v0}, Ljava/util/Optional;->isPresent()Z
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v3 = ","; // const-string v3, ","
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = (( java.util.Optional ) v1 ).isPresent ( ); // invoke-virtual {v1}, Ljava/util/Optional;->isPresent()Z
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v3 = ")"; // const-string v3, ")"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 145 */
final String v3 = "DeviceStateManagerServiceImpl"; // const-string v3, "DeviceStateManagerServiceImpl"
android.util.Slog .i ( v3,v2 );
/* .line 147 */
return;
} // .end method
public void init ( android.content.Context p0, com.android.server.devicestate.DeviceStateManagerService p1, java.lang.Object p2, com.android.server.devicestate.OverrideRequestController p3 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "service" # Lcom/android/server/devicestate/DeviceStateManagerService; */
/* .param p3, "lock" # Ljava/lang/Object; */
/* .param p4, "overrideRequestController" # Lcom/android/server/devicestate/OverrideRequestController; */
/* .line 69 */
this.mContext = p1;
/* .line 70 */
this.mOverrideRequestController = p4;
/* .line 71 */
this.mService = p2;
/* .line 72 */
this.mLock = p3;
/* .line 74 */
/* const-class v0, Lcom/android/server/wm/ActivityTaskManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/wm/ActivityTaskManagerInternal; */
this.mActivityTaskManagerInternal = v0;
/* .line 75 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x10700a4 */
(( android.content.res.Resources ) v0 ).getIntArray ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I
this.mReversedFoldedDeviceStates = v0;
/* .line 77 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x10700bb */
(( android.content.res.Resources ) v0 ).getIntArray ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I
this.mStrictFoldedDeviceStates = v0;
/* .line 79 */
return;
} // .end method
public void onStart ( java.util.Set p0 ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/Integer;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 83 */
/* .local p1, "foldedDeviceStates":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
this.mFoldedDeviceStates = p1;
/* .line 84 */
v0 = this.mActivityTaskManagerInternal;
v1 = this.mOverrideRequestScreenObserver;
(( com.android.server.wm.ActivityTaskManagerInternal ) v0 ).registerScreenObserver ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/ActivityTaskManagerInternal;->registerScreenObserver(Lcom/android/server/wm/ActivityTaskManagerInternal$ScreenObserver;)V
/* .line 85 */
return;
} // .end method
public void setBootPhase ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "phase" # I */
/* .line 89 */
/* const/16 v0, 0x1f4 */
/* if-ne p1, v0, :cond_0 */
/* .line 90 */
/* const-class v0, Lcom/android/server/policy/WindowManagerPolicy; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/policy/WindowManagerPolicy; */
this.mPolicy = v0;
/* .line 92 */
} // :cond_0
return;
} // .end method
public Boolean skipDueToReversedState ( ) {
/* .locals 2 */
/* .line 186 */
v0 = this.mPolicy;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = /* invoke-direct {p0}, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->isReversedState()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 187 */
/* invoke-direct {p0}, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->cancelStateRequest()V */
/* .line 188 */
v0 = this.mPolicy;
int v1 = 0; // const/4 v1, 0x0
/* .line 189 */
final String v0 = "DeviceStateManagerServiceImpl"; // const-string v0, "DeviceStateManagerServiceImpl"
final String v1 = "Do not sleep but lock device when device state is reversed."; // const-string v1, "Do not sleep but lock device when device state is reversed."
android.util.Slog .i ( v0,v1 );
/* .line 190 */
int v0 = 1; // const/4 v0, 0x1
/* .line 192 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
