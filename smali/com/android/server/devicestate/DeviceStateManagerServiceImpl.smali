.class public Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;
.super Lcom/android/server/devicestate/DeviceStateManagerServiceStub;
.source "DeviceStateManagerServiceImpl.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.devicestate.DeviceStateManagerServiceStub$$"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/devicestate/DeviceStateManagerServiceImpl$OverrideRequestScreenObserver;
    }
.end annotation


# static fields
.field private static final CURRENT_DEVICE_STATE:Ljava/lang/String; = "current_device_state"

.field private static final DEVICE_POSTURE:Ljava/lang/String; = "device_posture"

.field private static final FLAG_TENT_MODE:I = -0x80000000

.field private static final FOLDABLE_DEVICE_STATE_CLOSED:I = 0x0

.field private static final FOLDABLE_DEVICE_STATE_HALF_OPENED:I = 0x2

.field private static final FOLDABLE_DEVICE_STATE_OPENED:I = 0x3

.field private static final FOLDABLE_DEVICE_STATE_OPENED_PRESENTATION:I = 0x5

.field private static final FOLDABLE_DEVICE_STATE_OPENED_REVERSE:I = 0x4

.field private static final FOLDABLE_DEVICE_STATE_OPENED_REVERSE_PRESENTATION:I = 0x6

.field private static final FOLDABLE_DEVICE_STATE_TENT:I = 0x1

.field private static final POSTURE_CLOSED:I = 0x1

.field private static final POSTURE_FLIPPED:I = 0x4

.field private static final POSTURE_HALF_OPENED:I = 0x2

.field private static final POSTURE_OPENED:I = 0x3

.field private static final POSTURE_UNKNOWN:I = 0x0

.field public static final TAG:Ljava/lang/String; = "DeviceStateManagerServiceImpl"

.field private static final sDebug:Z = false


# instance fields
.field private mActivityTaskManagerInternal:Lcom/android/server/wm/ActivityTaskManagerInternal;

.field private mContext:Landroid/content/Context;

.field private mFoldedDeviceStates:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mInteractive:Z

.field private mLock:Ljava/lang/Object;

.field private mOverrideRequestController:Lcom/android/server/devicestate/OverrideRequestController;

.field private mOverrideRequestScreenObserver:Lcom/android/server/wm/ActivityTaskManagerInternal$ScreenObserver;

.field private mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

.field private mReversedFoldedDeviceStates:[I

.field private mService:Lcom/android/server/devicestate/DeviceStateManagerService;

.field private mStrictFoldedDeviceStates:[I


# direct methods
.method static bridge synthetic -$$Nest$fputmInteractive(Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->mInteractive:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 22
    invoke-direct {p0}, Lcom/android/server/devicestate/DeviceStateManagerServiceStub;-><init>()V

    .line 58
    new-instance v0, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl$OverrideRequestScreenObserver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl$OverrideRequestScreenObserver;-><init>(Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;Lcom/android/server/devicestate/DeviceStateManagerServiceImpl$OverrideRequestScreenObserver-IA;)V

    iput-object v0, p0, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->mOverrideRequestScreenObserver:Lcom/android/server/wm/ActivityTaskManagerInternal$ScreenObserver;

    .line 64
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->mInteractive:Z

    return-void
.end method

.method private cancelStateRequest()V
    .locals 1

    .line 212
    iget-object v0, p0, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->mService:Lcom/android/server/devicestate/DeviceStateManagerService;

    invoke-virtual {v0}, Lcom/android/server/devicestate/DeviceStateManagerService;->cancelStateRequestInternal()V

    .line 213
    return-void
.end method

.method private isReversedState()Z
    .locals 2

    .line 196
    iget-object v0, p0, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 197
    :try_start_0
    invoke-direct {p0}, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->isReversedStateLocked()Z

    move-result v1

    monitor-exit v0

    return v1

    .line 198
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private isReversedStateLocked()Z
    .locals 2

    .line 202
    iget-object v0, p0, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->mService:Lcom/android/server/devicestate/DeviceStateManagerService;

    invoke-virtual {v0}, Lcom/android/server/devicestate/DeviceStateManagerService;->getCommittedState()Ljava/util/Optional;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Optional;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->mService:Lcom/android/server/devicestate/DeviceStateManagerService;

    invoke-virtual {v0}, Lcom/android/server/devicestate/DeviceStateManagerService;->getCommittedState()Ljava/util/Optional;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Optional;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/devicestate/DeviceState;

    invoke-virtual {v0}, Lcom/android/server/devicestate/DeviceState;->getIdentifier()I

    move-result v0

    .line 204
    .local v0, "currentState":I
    iget-object v1, p0, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->mReversedFoldedDeviceStates:[I

    invoke-static {v1, v0}, Lcom/android/internal/util/ArrayUtils;->contains([II)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 205
    const/4 v1, 0x1

    return v1

    .line 208
    .end local v0    # "currentState":I
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private updateDevicePosture(II)V
    .locals 4
    .param p1, "baseStateIdentifier"    # I
    .param p2, "committedStateIdentifier"    # I

    .line 165
    iget-object v0, p0, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->mFoldedDeviceStates:Ljava/util/Set;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/internal/util/ArrayUtils;->contains(Ljava/util/Collection;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    const/4 v0, 0x1

    .local v0, "devicePosture":I
    goto :goto_0

    .line 167
    .end local v0    # "devicePosture":I
    :cond_0
    const/4 v0, 0x2

    if-ne p2, v0, :cond_1

    .line 168
    const/4 v0, 0x2

    .restart local v0    # "devicePosture":I
    goto :goto_0

    .line 169
    .end local v0    # "devicePosture":I
    :cond_1
    const/4 v1, 0x5

    if-ne p2, v1, :cond_2

    if-ne p1, v0, :cond_2

    .line 171
    const/4 v0, 0x2

    .restart local v0    # "devicePosture":I
    goto :goto_0

    .line 173
    .end local v0    # "devicePosture":I
    :cond_2
    const/4 v0, 0x3

    .line 176
    .restart local v0    # "devicePosture":I
    :goto_0
    iget-object v1, p0, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->mContext:Landroid/content/Context;

    .line 177
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 176
    const-string v2, "device_posture"

    const/4 v3, -0x1

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 178
    .local v1, "curDevicePosture":I
    if-eq v1, v0, :cond_3

    .line 179
    iget-object v3, p0, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {v3, v2, v0}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 182
    :cond_3
    return-void
.end method


# virtual methods
.method public adjustConditionSatisfiedLocked(ZLcom/android/server/devicestate/DeviceState;I)Z
    .locals 1
    .param p1, "conditionSatisfied"    # Z
    .param p2, "deviceState"    # Lcom/android/server/devicestate/DeviceState;
    .param p3, "lastReportedState"    # I

    .line 97
    if-eqz p1, :cond_4

    if-nez p2, :cond_0

    goto :goto_0

    .line 106
    :cond_0
    const/high16 v0, -0x80000000

    invoke-virtual {p2, v0}, Lcom/android/server/devicestate/DeviceState;->hasFlag(I)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->mStrictFoldedDeviceStates:[I

    .line 107
    invoke-static {v0, p3}, Lcom/android/internal/util/ArrayUtils;->contains([II)Z

    move-result v0

    if-nez v0, :cond_1

    .line 108
    invoke-virtual {p2}, Lcom/android/server/devicestate/DeviceState;->getIdentifier()I

    move-result v0

    if-ne p3, v0, :cond_2

    :cond_1
    iget-boolean v0, p0, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->mInteractive:Z

    if-nez v0, :cond_3

    .line 109
    :cond_2
    const/4 p1, 0x0

    .line 112
    :cond_3
    return p1

    .line 98
    :cond_4
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method public handleBaseStateChangedLocked(I)V
    .locals 7
    .param p1, "lastBaseStateIdentifier"    # I

    .line 117
    iget-object v0, p0, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->mFoldedDeviceStates:Ljava/util/Set;

    if-nez v0, :cond_0

    .line 118
    const-string v0, "DeviceStateManagerServiceImpl"

    const-string v1, "FoldedDeviceStates is null"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    return-void

    .line 122
    :cond_0
    iget-object v0, p0, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->mService:Lcom/android/server/devicestate/DeviceStateManagerService;

    invoke-virtual {v0}, Lcom/android/server/devicestate/DeviceStateManagerService;->getBaseState()Ljava/util/Optional;

    move-result-object v0

    .line 123
    .local v0, "curBaseState":Ljava/util/Optional;, "Ljava/util/Optional<Lcom/android/server/devicestate/DeviceState;>;"
    iget-object v1, p0, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->mService:Lcom/android/server/devicestate/DeviceStateManagerService;

    invoke-virtual {v1}, Lcom/android/server/devicestate/DeviceStateManagerService;->getCommittedState()Ljava/util/Optional;

    move-result-object v1

    .line 125
    .local v1, "committedState":Ljava/util/Optional;, "Ljava/util/Optional<Lcom/android/server/devicestate/DeviceState;>;"
    const/4 v2, -0x1

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eq p1, v2, :cond_1

    iget-object v2, p0, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->mFoldedDeviceStates:Ljava/util/Set;

    .line 126
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v3

    goto :goto_0

    :cond_1
    move v2, v4

    .line 127
    .local v2, "isCurrentStateFolded":Z
    :goto_0
    invoke-virtual {v0}, Ljava/util/Optional;->isPresent()Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->mFoldedDeviceStates:Ljava/util/Set;

    .line 128
    invoke-virtual {v0}, Ljava/util/Optional;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/devicestate/DeviceState;

    invoke-virtual {v6}, Lcom/android/server/devicestate/DeviceState;->getIdentifier()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    goto :goto_1

    :cond_2
    move v3, v4

    .line 130
    .local v3, "isNewStateFolded":Z
    :goto_1
    if-eq v2, v3, :cond_3

    .line 131
    iget-object v4, p0, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->mOverrideRequestController:Lcom/android/server/devicestate/OverrideRequestController;

    invoke-virtual {v4}, Lcom/android/server/devicestate/OverrideRequestController;->handleFoldStateChanged()V

    .line 134
    :cond_3
    invoke-virtual {v0}, Ljava/util/Optional;->isPresent()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {v1}, Ljava/util/Optional;->isPresent()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 135
    invoke-virtual {v0}, Ljava/util/Optional;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/devicestate/DeviceState;

    invoke-virtual {v4}, Lcom/android/server/devicestate/DeviceState;->getIdentifier()I

    move-result v4

    .line 136
    invoke-virtual {v1}, Ljava/util/Optional;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/server/devicestate/DeviceState;

    invoke-virtual {v5}, Lcom/android/server/devicestate/DeviceState;->getIdentifier()I

    move-result v5

    .line 135
    invoke-direct {p0, v4, v5}, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->updateDevicePosture(II)V

    .line 138
    :cond_4
    return-void
.end method

.method public handleCommittedStateChangedLocked()V
    .locals 6

    .line 142
    iget-object v0, p0, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->mService:Lcom/android/server/devicestate/DeviceStateManagerService;

    invoke-virtual {v0}, Lcom/android/server/devicestate/DeviceStateManagerService;->getBaseState()Ljava/util/Optional;

    move-result-object v0

    .line 143
    .local v0, "baseState":Ljava/util/Optional;, "Ljava/util/Optional<Lcom/android/server/devicestate/DeviceState;>;"
    iget-object v1, p0, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->mService:Lcom/android/server/devicestate/DeviceStateManagerService;

    invoke-virtual {v1}, Lcom/android/server/devicestate/DeviceStateManagerService;->getCommittedState()Ljava/util/Optional;

    move-result-object v1

    .line 144
    .local v1, "committedState":Ljava/util/Optional;, "Ljava/util/Optional<Lcom/android/server/devicestate/DeviceState;>;"
    invoke-virtual {v0}, Ljava/util/Optional;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Ljava/util/Optional;->isPresent()Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    .line 150
    :cond_0
    invoke-virtual {v0}, Ljava/util/Optional;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/devicestate/DeviceState;

    invoke-virtual {v2}, Lcom/android/server/devicestate/DeviceState;->getIdentifier()I

    move-result v2

    .line 151
    .local v2, "baseStateIdentifier":I
    invoke-virtual {v1}, Ljava/util/Optional;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/devicestate/DeviceState;

    invoke-virtual {v3}, Lcom/android/server/devicestate/DeviceState;->getIdentifier()I

    move-result v3

    .line 152
    .local v3, "committedStateIdentifier":I
    invoke-direct {p0, v2, v3}, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->updateDevicePosture(II)V

    .line 156
    iget-object v4, p0, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "current_device_state"

    invoke-static {v4, v5, v3}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 158
    return-void

    .line 145
    .end local v2    # "baseStateIdentifier":I
    .end local v3    # "committedStateIdentifier":I
    :cond_1
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "skip handle committed state changed. ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 146
    invoke-virtual {v0}, Ljava/util/Optional;->isPresent()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/util/Optional;->isPresent()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 145
    const-string v3, "DeviceStateManagerServiceImpl"

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    return-void
.end method

.method public init(Landroid/content/Context;Lcom/android/server/devicestate/DeviceStateManagerService;Ljava/lang/Object;Lcom/android/server/devicestate/OverrideRequestController;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "service"    # Lcom/android/server/devicestate/DeviceStateManagerService;
    .param p3, "lock"    # Ljava/lang/Object;
    .param p4, "overrideRequestController"    # Lcom/android/server/devicestate/OverrideRequestController;

    .line 69
    iput-object p1, p0, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->mContext:Landroid/content/Context;

    .line 70
    iput-object p4, p0, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->mOverrideRequestController:Lcom/android/server/devicestate/OverrideRequestController;

    .line 71
    iput-object p2, p0, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->mService:Lcom/android/server/devicestate/DeviceStateManagerService;

    .line 72
    iput-object p3, p0, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->mLock:Ljava/lang/Object;

    .line 74
    const-class v0, Lcom/android/server/wm/ActivityTaskManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/ActivityTaskManagerInternal;

    iput-object v0, p0, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->mActivityTaskManagerInternal:Lcom/android/server/wm/ActivityTaskManagerInternal;

    .line 75
    iget-object v0, p0, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x10700a4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->mReversedFoldedDeviceStates:[I

    .line 77
    iget-object v0, p0, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x10700bb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->mStrictFoldedDeviceStates:[I

    .line 79
    return-void
.end method

.method public onStart(Ljava/util/Set;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 83
    .local p1, "foldedDeviceStates":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    iput-object p1, p0, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->mFoldedDeviceStates:Ljava/util/Set;

    .line 84
    iget-object v0, p0, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->mActivityTaskManagerInternal:Lcom/android/server/wm/ActivityTaskManagerInternal;

    iget-object v1, p0, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->mOverrideRequestScreenObserver:Lcom/android/server/wm/ActivityTaskManagerInternal$ScreenObserver;

    invoke-virtual {v0, v1}, Lcom/android/server/wm/ActivityTaskManagerInternal;->registerScreenObserver(Lcom/android/server/wm/ActivityTaskManagerInternal$ScreenObserver;)V

    .line 85
    return-void
.end method

.method public setBootPhase(I)V
    .locals 1
    .param p1, "phase"    # I

    .line 89
    const/16 v0, 0x1f4

    if-ne p1, v0, :cond_0

    .line 90
    const-class v0, Lcom/android/server/policy/WindowManagerPolicy;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/policy/WindowManagerPolicy;

    iput-object v0, p0, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    .line 92
    :cond_0
    return-void
.end method

.method public skipDueToReversedState()Z
    .locals 2

    .line 186
    iget-object v0, p0, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->isReversedState()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    invoke-direct {p0}, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->cancelStateRequest()V

    .line 188
    iget-object v0, p0, Lcom/android/server/devicestate/DeviceStateManagerServiceImpl;->mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/android/server/policy/WindowManagerPolicy;->lockNow(Landroid/os/Bundle;)V

    .line 189
    const-string v0, "DeviceStateManagerServiceImpl"

    const-string v1, "Do not sleep but lock device when device state is reversed."

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    const/4 v0, 0x1

    return v0

    .line 192
    :cond_0
    const/4 v0, 0x0

    return v0
.end method
