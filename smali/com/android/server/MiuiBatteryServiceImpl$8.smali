.class Lcom/android/server/MiuiBatteryServiceImpl$8;
.super Landroid/content/BroadcastReceiver;
.source "MiuiBatteryServiceImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/MiuiBatteryServiceImpl;->init(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/MiuiBatteryServiceImpl;


# direct methods
.method constructor <init>(Lcom/android/server/MiuiBatteryServiceImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/MiuiBatteryServiceImpl;

    .line 274
    iput-object p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$8;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 277
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 278
    .local v0, "action":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$8;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetDEBUG(Lcom/android/server/MiuiBatteryServiceImpl;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "action = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiBatteryServiceImpl"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    :cond_0
    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 280
    const-string v1, "plugged"

    const/4 v2, -0x1

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 281
    .local v1, "plugType":I
    const-string v3, "level"

    invoke-virtual {p2, v3, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 282
    .local v2, "batteryLevel":I
    const/4 v3, 0x0

    if-eqz v1, :cond_1

    const/4 v4, 0x1

    goto :goto_0

    :cond_1
    move v4, v3

    .line 283
    .local v4, "plugged":Z
    :goto_0
    iget-object v5, p0, Lcom/android/server/MiuiBatteryServiceImpl$8;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmIsTestMode(Lcom/android/server/MiuiBatteryServiceImpl;)Z

    move-result v5

    if-eqz v5, :cond_3

    const/16 v5, 0x4b

    if-ge v2, v5, :cond_2

    const/16 v5, 0x28

    if-gt v2, v5, :cond_3

    .line 284
    :cond_2
    iget-object v5, p0, Lcom/android/server/MiuiBatteryServiceImpl$8;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryServiceImpl;)Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    move-result-object v5

    const/16 v6, 0x1a

    invoke-virtual {v5, v6, v2}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessage(II)V

    .line 286
    :cond_3
    iget-object v5, p0, Lcom/android/server/MiuiBatteryServiceImpl$8;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmSupportWirelessCharge(Lcom/android/server/MiuiBatteryServiceImpl;)Z

    move-result v5

    if-eqz v5, :cond_4

    if-nez v4, :cond_4

    iget-object v5, p0, Lcom/android/server/MiuiBatteryServiceImpl$8;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryServiceImpl;)Lmiui/util/IMiCharge;

    move-result-object v5

    invoke-virtual {v5}, Lmiui/util/IMiCharge;->getWirelessChargingStatus()I

    move-result v5

    if-nez v5, :cond_4

    .line 288
    iget-object v5, p0, Lcom/android/server/MiuiBatteryServiceImpl$8;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryServiceImpl;)Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    move-result-object v5

    invoke-virtual {v5, v3, v2}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessage(II)V

    .line 290
    .end local v1    # "plugType":I
    .end local v2    # "batteryLevel":I
    .end local v4    # "plugged":Z
    :cond_4
    goto :goto_1

    :cond_5
    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const-wide/16 v2, 0x0

    if-eqz v1, :cond_6

    .line 291
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$8;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryServiceImpl;)Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    move-result-object v1

    const/16 v4, 0x12

    invoke-virtual {v1, v4, v2, v3}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessageDelayed(IJ)V

    goto :goto_1

    .line 292
    :cond_6
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 293
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$8;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryServiceImpl;)Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    move-result-object v1

    const/16 v4, 0x13

    invoke-virtual {v1, v4, v2, v3}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessageDelayed(IJ)V

    .line 295
    :cond_7
    :goto_1
    return-void
.end method
