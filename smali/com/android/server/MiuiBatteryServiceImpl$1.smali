.class Lcom/android/server/MiuiBatteryServiceImpl$1;
.super Landroid/content/BroadcastReceiver;
.source "MiuiBatteryServiceImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/MiuiBatteryServiceImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/MiuiBatteryServiceImpl;


# direct methods
.method constructor <init>(Lcom/android/server/MiuiBatteryServiceImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/MiuiBatteryServiceImpl;

    .line 139
    iput-object p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$1;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 142
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 143
    .local v0, "action":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, -0x1

    packed-switch v1, :pswitch_data_0

    :cond_0
    goto :goto_0

    :pswitch_0
    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v2

    goto :goto_1

    :goto_0
    move v1, v3

    :goto_1
    packed-switch v1, :pswitch_data_1

    goto :goto_2

    .line 145
    :pswitch_1
    const-string/jumbo v1, "status"

    invoke-virtual {p2, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 146
    .local v1, "status":I
    iget-object v3, p0, Lcom/android/server/MiuiBatteryServiceImpl$1;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    const/4 v4, 0x3

    if-ne v1, v4, :cond_1

    const/4 v2, 0x1

    :cond_1
    invoke-static {v3, v2}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fputisDisCharging(Lcom/android/server/MiuiBatteryServiceImpl;Z)V

    .line 147
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mChargingStateReceiver:isDisCharging = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/MiuiBatteryServiceImpl$1;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v3}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetisDisCharging(Lcom/android/server/MiuiBatteryServiceImpl;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MiuiBatteryServiceImpl"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$1;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetisDisCharging(Lcom/android/server/MiuiBatteryServiceImpl;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$1;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmBtConnectState(Lcom/android/server/MiuiBatteryServiceImpl;)I

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$1;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryServiceImpl;)Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/MiuiBatteryServiceImpl$1;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v3}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetshutDownRnuable(Lcom/android/server/MiuiBatteryServiceImpl;)Ljava/lang/Runnable;

    move-result-object v3

    .line 149
    invoke-virtual {v2, v3}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->hasCallbacks(Ljava/lang/Runnable;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 150
    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$1;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetwakeLock(Lcom/android/server/MiuiBatteryServiceImpl;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    const-wide/32 v3, 0x57e40

    invoke-virtual {v2, v3, v4}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    .line 151
    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$1;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryServiceImpl;)Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/MiuiBatteryServiceImpl$1;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v3}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetshutDownRnuable(Lcom/android/server/MiuiBatteryServiceImpl;)Ljava/lang/Runnable;

    move-result-object v3

    const-wide/32 v4, 0x493e0

    invoke-virtual {v2, v3, v4, v5}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 155
    .end local v1    # "status":I
    :cond_2
    :goto_2
    return-void

    :pswitch_data_0
    .packed-switch -0x5bb23923
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
