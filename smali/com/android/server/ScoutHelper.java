public class com.android.server.ScoutHelper {
	 /* .source "ScoutHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/ScoutHelper$ScoutBinderInfo;, */
	 /* Lcom/android/server/ScoutHelper$ProcStatus;, */
	 /* Lcom/android/server/ScoutHelper$Action; */
	 /* } */
} // .end annotation
/* # static fields */
public static final java.lang.String ACTION_CAT;
public static final java.lang.String ACTION_CP;
public static final java.lang.String ACTION_DMABUF_DUMP;
public static final java.lang.String ACTION_DUMPSYS;
public static final java.lang.String ACTION_LOGCAT;
public static final java.lang.String ACTION_MV;
public static final java.lang.String ACTION_PS;
public static final java.lang.String ACTION_TOP;
public static final java.lang.String BINDER_ASYNC;
public static final java.lang.String BINDER_ASYNC_GKI;
public static final java.lang.String BINDER_ASYNC_MIUI;
public static final java.lang.String BINDER_CONTEXT;
public static final java.lang.String BINDER_DUR;
public static final java.lang.String BINDER_FS_PATH_GKI;
public static final java.lang.String BINDER_FS_PATH_MIUI;
public static final Boolean BINDER_FULL_KILL_PROC;
public static final Integer BINDER_FULL_KILL_SCORE_ADJ_MIN;
public static final java.lang.String BINDER_INCOMING;
public static final java.lang.String BINDER_INCOMING_GKI;
public static final java.lang.String BINDER_INCOMING_MIUI;
public static final java.lang.String BINDER_OUTGOING;
public static final java.lang.String BINDER_OUTGOING_GKI;
public static final java.lang.String BINDER_OUTGOING_MIUI;
public static final java.lang.String BINDER_PENDING;
public static final java.lang.String BINDER_PENDING_MIUI;
public static final Integer BINDER_WAITTIME_THRESHOLD;
public static final Integer CALL_TYPE_APP;
public static final Integer CALL_TYPE_SYSTEM;
private static java.lang.String CONSOLE_RAMOOPS_0_PATH;
private static java.lang.String CONSOLE_RAMOOPS_PATH;
private static final Boolean DEBUG;
public static final Integer DEFAULT_RUN_COMMAND_TIMEOUT;
public static final Boolean DISABLE_AOSP_ANR_TRACE_POLICY;
public static final Boolean ENABLED_SCOUT;
public static final Boolean ENABLED_SCOUT_DEBUG;
public static final java.lang.String FILE_DIR_MQSAS;
public static final java.lang.String FILE_DIR_SCOUT;
public static final java.lang.String FILE_DIR_STABILITY;
public static final Boolean IS_INTERNATIONAL_BUILD;
public static final Integer JAVA_PROCESS;
private static final java.util.List JAVA_SHELL_PROCESS;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public static final Integer MONKEY_BLOCK_THRESHOLD;
private static final java.lang.String MONKEY_PROCESS;
private static final java.lang.String MQSASD;
public static final java.lang.String MQS_PSTORE_DIR;
public static final Integer NATIVE_PROCESS;
public static final Integer OOM_SCORE_ADJ_MAX;
public static final Integer OOM_SCORE_ADJ_MIN;
public static final Integer OOM_SCORE_ADJ_SHELL;
public static final Boolean PANIC_ANR_D_THREAD;
public static final Boolean PANIC_D_THREAD;
private static java.lang.String PROC_VERSION;
public static final Integer PS_MODE_FULL;
public static final Integer PS_MODE_NORMAL;
private static final java.lang.String REGEX_PATTERN;
private static final java.lang.String REGEX_PATTERN_LINUX_VERSION;
private static final java.lang.String REGEX_PATTERN_NEW;
public static final Boolean SCOUT_BINDER_GKI;
public static final Integer SHELL_PROCESS;
private static final java.lang.String STATUS_KEYS;
public static final Boolean SYSRQ_ANR_D_THREAD;
public static final Integer SYSTEM_ADJ;
private static final java.lang.String TAG;
public static final Integer UNKNOW_PROCESS;
public static Double linuxVersion;
private static miui.mqsas.IMQSNative mDaemon;
private static final java.lang.String sSkipProcs;
private static java.lang.Boolean supportNewBinderLog;
/* # direct methods */
static com.android.server.ScoutHelper ( ) {
/* .locals 10 */
/* .line 50 */
final String v0 = "ScoutHelper"; // const-string v0, "ScoutHelper"
/* .line 51 */
final String v1 = "persist.sys.miui_scout_enable"; // const-string v1, "persist.sys.miui_scout_enable"
int v2 = 0; // const/4 v2, 0x0
v1 = android.os.SystemProperties .getBoolean ( v1,v2 );
com.android.server.ScoutHelper.ENABLED_SCOUT = (v1!= 0);
/* .line 52 */
/* nop */
/* .line 53 */
final String v1 = "persist.sys.miui_scout_debug"; // const-string v1, "persist.sys.miui_scout_debug"
v1 = android.os.SystemProperties .getBoolean ( v1,v2 );
com.android.server.ScoutHelper.ENABLED_SCOUT_DEBUG = (v1!= 0);
/* .line 54 */
/* nop */
/* .line 55 */
final String v1 = "persist.sys.miui_scout_binder_full_kill_process"; // const-string v1, "persist.sys.miui_scout_binder_full_kill_process"
v1 = android.os.SystemProperties .getBoolean ( v1,v2 );
com.android.server.ScoutHelper.BINDER_FULL_KILL_PROC = (v1!= 0);
/* .line 56 */
/* nop */
/* .line 57 */
final String v1 = "persist.sys.panicOnWatchdog_D_state"; // const-string v1, "persist.sys.panicOnWatchdog_D_state"
v1 = android.os.SystemProperties .getBoolean ( v1,v2 );
com.android.server.ScoutHelper.PANIC_D_THREAD = (v1!= 0);
/* .line 58 */
/* nop */
/* .line 59 */
final String v1 = "persist.sys.sysrqOnAnr_D_state"; // const-string v1, "persist.sys.sysrqOnAnr_D_state"
v1 = android.os.SystemProperties .getBoolean ( v1,v2 );
final String v3 = "persist.sys.panicOnAnr_D_state"; // const-string v3, "persist.sys.panicOnAnr_D_state"
int v4 = 1; // const/4 v4, 0x1
/* if-nez v1, :cond_1 */
/* .line 60 */
v1 = android.os.SystemProperties .getBoolean ( v3,v2 );
if ( v1 != null) { // if-eqz v1, :cond_0
} // :cond_0
/* move v1, v2 */
} // :cond_1
} // :goto_0
/* move v1, v4 */
} // :goto_1
com.android.server.ScoutHelper.SYSRQ_ANR_D_THREAD = (v1!= 0);
/* .line 61 */
/* nop */
/* .line 62 */
v1 = android.os.SystemProperties .getBoolean ( v3,v2 );
com.android.server.ScoutHelper.PANIC_ANR_D_THREAD = (v1!= 0);
/* .line 63 */
/* nop */
/* .line 64 */
final String v1 = "persist.sys.scout_binder_gki"; // const-string v1, "persist.sys.scout_binder_gki"
v1 = android.os.SystemProperties .getBoolean ( v1,v2 );
com.android.server.ScoutHelper.SCOUT_BINDER_GKI = (v1!= 0);
/* .line 65 */
/* nop */
/* .line 66 */
final String v1 = "persist.sys.disable_aosp_anr_policy"; // const-string v1, "persist.sys.disable_aosp_anr_policy"
v1 = android.os.SystemProperties .getBoolean ( v1,v2 );
com.android.server.ScoutHelper.DISABLE_AOSP_ANR_TRACE_POLICY = (v1!= 0);
/* .line 68 */
/* sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
com.android.server.ScoutHelper.IS_INTERNATIONAL_BUILD = (v1!= 0);
/* .line 93 */
final String v1 = "/sys/fs/pstore/console-ramoops"; // const-string v1, "/sys/fs/pstore/console-ramoops"
/* .line 94 */
final String v1 = "/sys/fs/pstore/console-ramoops-0"; // const-string v1, "/sys/fs/pstore/console-ramoops-0"
/* .line 95 */
final String v1 = "proc/version"; // const-string v1, "proc/version"
/* .line 136 */
/* const-wide/16 v5, 0x0 */
/* sput-wide v5, Lcom/android/server/ScoutHelper;->linuxVersion:D */
/* .line 138 */
java.lang.Boolean .valueOf ( v2 );
/* .line 143 */
int v1 = 0; // const/4 v1, 0x0
/* .line 144 */
/* .local v1, "cmdLine":Ljava/lang/String; */
try { // :try_start_0
/* new-instance v2, Ljava/io/BufferedReader; */
/* new-instance v3, Ljava/io/FileReader; */
v5 = com.android.server.ScoutHelper.PROC_VERSION;
/* invoke-direct {v3, v5}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V */
/* invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 145 */
/* .local v2, "versionReader":Ljava/io/BufferedReader; */
try { // :try_start_1
(( java.io.BufferedReader ) v2 ).readLine ( ); // invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
(( java.lang.String ) v3 ).trim ( ); // invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;
/* move-object v1, v3 */
/* .line 146 */
final String v3 = "\\bLinux version\\s+(\\d+\\.\\d+)."; // const-string v3, "\\bLinux version\\s+(\\d+\\.\\d+)."
java.util.regex.Pattern .compile ( v3 );
/* .line 147 */
/* .local v3, "pattern":Ljava/util/regex/Pattern; */
(( java.util.regex.Pattern ) v3 ).matcher ( v1 ); // invoke-virtual {v3, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
/* .line 148 */
/* .local v5, "matcher":Ljava/util/regex/Matcher; */
v6 = (( java.util.regex.Matcher ) v5 ).find ( ); // invoke-virtual {v5}, Ljava/util/regex/Matcher;->find()Z
if ( v6 != null) { // if-eqz v6, :cond_3
/* .line 149 */
(( java.util.regex.Matcher ) v5 ).group ( v4 ); // invoke-virtual {v5, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;
java.lang.Double .valueOf ( v6 );
(( java.lang.Double ) v6 ).doubleValue ( ); // invoke-virtual {v6}, Ljava/lang/Double;->doubleValue()D
/* move-result-wide v6 */
/* sput-wide v6, Lcom/android/server/ScoutHelper;->linuxVersion:D */
/* .line 151 */
/* const-wide v8, 0x4018666666666666L # 6.1 */
/* cmpl-double v6, v6, v8 */
/* if-ltz v6, :cond_2 */
/* .line 152 */
java.lang.Boolean .valueOf ( v4 );
/* .line 154 */
} // :cond_2
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "The current Linux version is "; // const-string v6, "The current Linux version is "
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-wide v6, Lcom/android/server/ScoutHelper;->linuxVersion:D */
(( java.lang.StringBuilder ) v4 ).append ( v6, v7 ); // invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
final String v6 = " supportNewBinderLog = "; // const-string v6, " supportNewBinderLog = "
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = com.android.server.ScoutHelper.supportNewBinderLog;
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v4 );
/* .line 157 */
} // :cond_3
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Parsing Linux version failed.info : "; // const-string v6, "Parsing Linux version failed.info : "
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v0,v4 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 159 */
} // .end local v3 # "pattern":Ljava/util/regex/Pattern;
} // .end local v5 # "matcher":Ljava/util/regex/Matcher;
} // :goto_2
try { // :try_start_2
(( java.io.BufferedReader ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 161 */
} // .end local v2 # "versionReader":Ljava/io/BufferedReader;
/* .line 144 */
/* .restart local v2 # "versionReader":Ljava/io/BufferedReader; */
/* :catchall_0 */
/* move-exception v3 */
try { // :try_start_3
(( java.io.BufferedReader ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v4 */
try { // :try_start_4
(( java.lang.Throwable ) v3 ).addSuppressed ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v1 # "cmdLine":Ljava/lang/String;
} // :goto_3
/* throw v3 */
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 159 */
} // .end local v2 # "versionReader":Ljava/io/BufferedReader;
/* .restart local v1 # "cmdLine":Ljava/lang/String; */
/* :catch_0 */
/* move-exception v2 */
/* .line 160 */
/* .local v2, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Read PROC_VERSION Error "; // const-string v4, "Read PROC_VERSION Error "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v0,v3 );
/* .line 164 */
} // .end local v1 # "cmdLine":Ljava/lang/String;
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_4
final String v0 = "com.android.commands.monkey"; // const-string v0, "com.android.commands.monkey"
/* filled-new-array {v0}, [Ljava/lang/String; */
/* .line 165 */
java.util.Arrays .asList ( v0 );
/* .line 695 */
final String v0 = "com.android.networkstack.process"; // const-string v0, "com.android.networkstack.process"
/* filled-new-array {v0}, [Ljava/lang/String; */
/* .line 985 */
final String v0 = "TracerPid:"; // const-string v0, "TracerPid:"
/* filled-new-array {v0}, [Ljava/lang/String; */
return;
} // .end method
public com.android.server.ScoutHelper ( ) {
/* .locals 0 */
/* .line 46 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
public static java.lang.Boolean CheckDState ( java.lang.String p0, Integer p1 ) {
/* .locals 1 */
/* .param p0, "tag" # Ljava/lang/String; */
/* .param p1, "pid" # I */
/* .line 320 */
int v0 = 0; // const/4 v0, 0x0
com.android.server.ScoutHelper .CheckDState ( p0,p1,v0 );
} // .end method
public static java.lang.Boolean CheckDState ( java.lang.String p0, Integer p1, com.android.server.ScoutHelper$ScoutBinderInfo p2 ) {
/* .locals 35 */
/* .param p0, "tag" # Ljava/lang/String; */
/* .param p1, "pid" # I */
/* .param p2, "scoutInfo" # Lcom/android/server/ScoutHelper$ScoutBinderInfo; */
/* .line 324 */
/* move-object/from16 v1, p0 */
/* move/from16 v2, p1 */
/* move-object/from16 v3, p2 */
final String v4 = "/"; // const-string v4, "/"
final String v0 = "/proc/"; // const-string v0, "/proc/"
int v5 = 0; // const/4 v5, 0x0
/* .line 325 */
/* .local v5, "result":Z */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 327 */
/* .local v6, "procInfo":Ljava/lang/StringBuilder; */
try { // :try_start_0
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v0 ); // invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* invoke-static/range {p1 ..p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String; */
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = "/task"; // const-string v8, "/task"
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 328 */
/* .local v7, "taskPath":Ljava/lang/String; */
/* new-instance v8, Ljava/io/File; */
/* invoke-direct {v8, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 329 */
/* .local v8, "taskDir":Ljava/io/File; */
(( java.io.File ) v8 ).listFiles ( ); // invoke-virtual {v8}, Ljava/io/File;->listFiles()[Ljava/io/File;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_b */
/* .line 330 */
/* .local v9, "threadDirs":[Ljava/io/File; */
final String v10 = "("; // const-string v10, "("
final String v11 = "; tracerPid="; // const-string v11, "; tracerPid="
final String v13 = "\n"; // const-string v13, "\n"
final String v14 = ") have "; // const-string v14, ") have "
final String v15 = "Pid("; // const-string v15, "Pid("
final String v12 = "T"; // const-string v12, "T"
/* move/from16 v16, v5 */
} // .end local v5 # "result":Z
/* .local v16, "result":Z */
final String v5 = "Z"; // const-string v5, "Z"
/* move-object/from16 v17, v8 */
} // .end local v8 # "taskDir":Ljava/io/File;
/* .local v17, "taskDir":Ljava/io/File; */
final String v8 = "D"; // const-string v8, "D"
final String v3 = "\\s+"; // const-string v3, "\\s+"
/* move-object/from16 v18, v6 */
} // .end local v6 # "procInfo":Ljava/lang/StringBuilder;
/* .local v18, "procInfo":Ljava/lang/StringBuilder; */
final String v6 = "/stat"; // const-string v6, "/stat"
/* move-object/from16 v20, v13 */
final String v13 = "/stack"; // const-string v13, "/stack"
/* const/16 v21, 0x0 */
/* move-object/from16 v22, v13 */
final String v13 = ")"; // const-string v13, ")"
/* const-string/jumbo v1, "t" */
/* move-object/from16 v23, v10 */
if ( v9 != null) { // if-eqz v9, :cond_7
/* .line 331 */
try { // :try_start_1
/* array-length v10, v9 */
/* move-object/from16 v24, v11 */
/* move/from16 v11, v21 */
} // :goto_0
/* if-ge v11, v10, :cond_6 */
/* aget-object v0, v9, v11 */
/* move-object/from16 v25, v0 */
/* .line 332 */
/* .local v25, "threadDir":Ljava/io/File; */
/* new-instance v0, Ljava/io/BufferedReader; */
/* move-object/from16 v26, v9 */
} // .end local v9 # "threadDirs":[Ljava/io/File;
/* .local v26, "threadDirs":[Ljava/io/File; */
/* new-instance v9, Ljava/io/FileReader; */
/* move/from16 v27, v10 */
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v10 ).append ( v7 ); // invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v4 ); // invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 333 */
/* move/from16 v28, v11 */
/* invoke-virtual/range {v25 ..v25}, Ljava/io/File;->getName()Ljava/lang/String; */
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v6 ); // invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v9, v10}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V */
/* invoke-direct {v0, v9}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_6 */
/* move-object v9, v0 */
/* .line 334 */
/* .local v9, "threadStat":Ljava/io/BufferedReader; */
try { // :try_start_2
(( java.io.BufferedReader ) v9 ).readLine ( ); // invoke-virtual {v9}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v10, v0 */
/* .line 335 */
/* .local v10, "statInfo":Ljava/lang/String; */
/* const/16 v11, 0x28 */
v0 = (( java.lang.String ) v10 ).indexOf ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/String;->indexOf(I)I
/* add-int/lit8 v0, v0, 0x1 */
/* .line 336 */
/* move-object/from16 v29, v6 */
/* const/16 v11, 0x29 */
v6 = (( java.lang.String ) v10 ).indexOf ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/String;->indexOf(I)I
/* .line 335 */
(( java.lang.String ) v10 ).substring ( v0, v6 ); // invoke-virtual {v10, v0, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;
/* move-object v6, v0 */
/* .line 337 */
/* .local v6, "threadName":Ljava/lang/String; */
v0 = (( java.lang.String ) v10 ).indexOf ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/String;->indexOf(I)I
/* add-int/lit8 v0, v0, 0x2 */
(( java.lang.String ) v10 ).substring ( v0 ); // invoke-virtual {v10, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;
/* .line 338 */
(( java.lang.String ) v0 ).split ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* aget-object v0, v0, v21 */
/* move-object v11, v0 */
/* .line 339 */
/* .local v11, "threadState":Ljava/lang/String; */
v0 = (( java.lang.String ) v11 ).equals ( v8 ); // invoke-virtual {v11, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
v0 = (( java.lang.String ) v11 ).equals ( v5 ); // invoke-virtual {v11, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
v0 = (( java.lang.String ) v11 ).equals ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
/* .line 340 */
v0 = (( java.lang.String ) v11 ).equals ( v1 ); // invoke-virtual {v11, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
/* move-object v6, v1 */
/* move-object/from16 v34, v4 */
/* move-object/from16 v30, v20 */
/* move-object/from16 v11, v22 */
/* move-object/from16 v1, p0 */
/* goto/16 :goto_7 */
/* .line 341 */
} // :cond_1
} // :goto_1
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* move-object/from16 v30, v0 */
/* .line 342 */
/* .local v30, "logInfo":Ljava/lang/StringBuilder; */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v15 ); // invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v14 ); // invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v11 ); // invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-object/from16 v31, v10 */
} // .end local v10 # "statInfo":Ljava/lang/String;
/* .local v31, "statInfo":Ljava/lang/String; */
final String v10 = " state thread(tid:"; // const-string v10, " state thread(tid:"
(( java.lang.StringBuilder ) v0 ).append ( v10 ); // invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 343 */
/* invoke-virtual/range {v25 ..v25}, Ljava/io/File;->getName()Ljava/lang/String; */
(( java.lang.StringBuilder ) v0 ).append ( v10 ); // invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v10 = " name:"; // const-string v10, " name:"
(( java.lang.StringBuilder ) v0 ).append ( v10 ); // invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v13 ); // invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 342 */
/* move-object/from16 v10, v30 */
} // .end local v30 # "logInfo":Ljava/lang/StringBuilder;
/* .local v10, "logInfo":Ljava/lang/StringBuilder; */
(( java.lang.StringBuilder ) v10 ).append ( v0 ); // invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 344 */
v0 = (( java.lang.String ) v11 ).equals ( v1 ); // invoke-virtual {v11, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 345 */
/* invoke-static/range {p1 ..p1}, Lcom/android/server/ScoutHelper;->getProcStatus(I)Lcom/android/server/ScoutHelper$ProcStatus; */
/* .line 346 */
/* .local v0, "procStatus":Lcom/android/server/ScoutHelper$ProcStatus; */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 347 */
/* move-object/from16 v30, v1 */
/* iget v1, v0, Lcom/android/server/ScoutHelper$ProcStatus;->tracerPid:I */
/* .line 348 */
/* .local v1, "tracerPid":I */
/* move-object/from16 v32, v6 */
int v6 = -1; // const/4 v6, -0x1
} // .end local v6 # "threadName":Ljava/lang/String;
/* .local v32, "threadName":Ljava/lang/String; */
/* if-le v1, v6, :cond_2 */
/* .line 349 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
/* move-object/from16 v33, v11 */
/* move-object/from16 v11, v24 */
} // .end local v11 # "threadState":Ljava/lang/String;
/* .local v33, "threadState":Ljava/lang/String; */
(( java.lang.StringBuilder ) v6 ).append ( v11 ); // invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v1 ); // invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* move-object/from16 v24, v11 */
/* move-object/from16 v11, v23 */
(( java.lang.StringBuilder ) v6 ).append ( v11 ); // invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 350 */
/* move-object/from16 v23, v0 */
} // .end local v0 # "procStatus":Lcom/android/server/ScoutHelper$ProcStatus;
/* .local v23, "procStatus":Lcom/android/server/ScoutHelper$ProcStatus; */
com.android.server.ScoutHelper .getProcessCmdline ( v1 );
(( java.lang.StringBuilder ) v6 ).append ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v13 ); // invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 349 */
(( java.lang.StringBuilder ) v10 ).append ( v0 ); // invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 348 */
} // .end local v23 # "procStatus":Lcom/android/server/ScoutHelper$ProcStatus;
} // .end local v33 # "threadState":Ljava/lang/String;
/* .restart local v0 # "procStatus":Lcom/android/server/ScoutHelper$ProcStatus; */
/* .restart local v11 # "threadState":Ljava/lang/String; */
} // :cond_2
/* move-object/from16 v33, v11 */
/* move-object/from16 v11, v23 */
/* move-object/from16 v23, v0 */
} // .end local v0 # "procStatus":Lcom/android/server/ScoutHelper$ProcStatus;
} // .end local v11 # "threadState":Ljava/lang/String;
/* .restart local v23 # "procStatus":Lcom/android/server/ScoutHelper$ProcStatus; */
/* .restart local v33 # "threadState":Ljava/lang/String; */
/* .line 346 */
} // .end local v1 # "tracerPid":I
} // .end local v23 # "procStatus":Lcom/android/server/ScoutHelper$ProcStatus;
} // .end local v32 # "threadName":Ljava/lang/String;
} // .end local v33 # "threadState":Ljava/lang/String;
/* .restart local v0 # "procStatus":Lcom/android/server/ScoutHelper$ProcStatus; */
/* .restart local v6 # "threadName":Ljava/lang/String; */
/* .restart local v11 # "threadState":Ljava/lang/String; */
} // :cond_3
/* move-object/from16 v30, v1 */
/* move-object/from16 v32, v6 */
/* move-object/from16 v33, v11 */
/* move-object/from16 v11, v23 */
/* move-object/from16 v23, v0 */
} // .end local v0 # "procStatus":Lcom/android/server/ScoutHelper$ProcStatus;
} // .end local v6 # "threadName":Ljava/lang/String;
} // .end local v11 # "threadState":Ljava/lang/String;
/* .restart local v23 # "procStatus":Lcom/android/server/ScoutHelper$ProcStatus; */
/* .restart local v32 # "threadName":Ljava/lang/String; */
/* .restart local v33 # "threadState":Ljava/lang/String; */
/* .line 344 */
} // .end local v23 # "procStatus":Lcom/android/server/ScoutHelper$ProcStatus;
} // .end local v32 # "threadName":Ljava/lang/String;
} // .end local v33 # "threadState":Ljava/lang/String;
/* .restart local v6 # "threadName":Ljava/lang/String; */
/* .restart local v11 # "threadState":Ljava/lang/String; */
} // :cond_4
/* move-object/from16 v30, v1 */
/* move-object/from16 v32, v6 */
/* move-object/from16 v33, v11 */
/* move-object/from16 v11, v23 */
/* .line 354 */
} // .end local v6 # "threadName":Ljava/lang/String;
} // .end local v11 # "threadState":Ljava/lang/String;
/* .restart local v32 # "threadName":Ljava/lang/String; */
/* .restart local v33 # "threadState":Ljava/lang/String; */
} // :goto_2
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_4 */
/* move-object/from16 v1, p0 */
/* move-object/from16 v6, v30 */
try { // :try_start_3
android.util.Slog .d ( v1,v0 );
/* .line 355 */
/* move-object/from16 v23, v11 */
/* move-object/from16 v11, v20 */
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 356 */
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_3 */
/* move-object/from16 v20, v10 */
/* move-object/from16 v10, v18 */
} // .end local v18 # "procInfo":Ljava/lang/StringBuilder;
/* .local v10, "procInfo":Ljava/lang/StringBuilder; */
/* .local v20, "logInfo":Ljava/lang/StringBuilder; */
try { // :try_start_4
(( java.lang.StringBuilder ) v10 ).append ( v0 ); // invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_2 */
/* .line 357 */
try { // :try_start_5
/* new-instance v0, Ljava/io/BufferedReader; */
/* :try_end_5 */
/* .catch Ljava/lang/Exception; {:try_start_5 ..:try_end_5} :catch_4 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_2 */
/* move-object/from16 v18, v10 */
} // .end local v10 # "procInfo":Ljava/lang/StringBuilder;
/* .restart local v18 # "procInfo":Ljava/lang/StringBuilder; */
try { // :try_start_6
/* new-instance v10, Ljava/io/FileReader; */
/* :try_end_6 */
/* .catch Ljava/lang/Exception; {:try_start_6 ..:try_end_6} :catch_3 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_3 */
/* move-object/from16 v30, v11 */
try { // :try_start_7
/* new-instance v11, Ljava/lang/StringBuilder; */
/* invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v11 ).append ( v7 ); // invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).append ( v4 ); // invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* :try_end_7 */
/* .catch Ljava/lang/Exception; {:try_start_7 ..:try_end_7} :catch_2 */
/* .catchall {:try_start_7 ..:try_end_7} :catchall_3 */
/* .line 358 */
/* move-object/from16 v34, v4 */
try { // :try_start_8
/* invoke-virtual/range {v25 ..v25}, Ljava/io/File;->getName()Ljava/lang/String; */
(( java.lang.StringBuilder ) v11 ).append ( v4 ); // invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* :try_end_8 */
/* .catch Ljava/lang/Exception; {:try_start_8 ..:try_end_8} :catch_1 */
/* .catchall {:try_start_8 ..:try_end_8} :catchall_3 */
/* move-object/from16 v11, v22 */
try { // :try_start_9
(( java.lang.StringBuilder ) v4 ).append ( v11 ); // invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v10, v4}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V */
/* invoke-direct {v0, v10}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* :try_end_9 */
/* .catch Ljava/lang/Exception; {:try_start_9 ..:try_end_9} :catch_0 */
/* .catchall {:try_start_9 ..:try_end_9} :catchall_3 */
/* move-object v4, v0 */
/* .line 359 */
/* .local v4, "threadStack":Ljava/io/BufferedReader; */
int v0 = 0; // const/4 v0, 0x0
/* .line 360 */
/* .local v0, "stackLineInfo":Ljava/lang/String; */
} // :goto_3
try { // :try_start_a
(( java.io.BufferedReader ) v4 ).readLine ( ); // invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v0, v10 */
if ( v10 != null) { // if-eqz v10, :cond_5
/* .line 361 */
android.util.Slog .d ( v1,v0 );
/* :try_end_a */
/* .catchall {:try_start_a ..:try_end_a} :catchall_0 */
/* .line 363 */
} // .end local v0 # "stackLineInfo":Ljava/lang/String;
} // :cond_5
try { // :try_start_b
(( java.io.BufferedReader ) v4 ).close ( ); // invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
/* :try_end_b */
/* .catch Ljava/lang/Exception; {:try_start_b ..:try_end_b} :catch_0 */
/* .catchall {:try_start_b ..:try_end_b} :catchall_3 */
/* .line 364 */
} // .end local v4 # "threadStack":Ljava/io/BufferedReader;
/* .line 357 */
/* .restart local v4 # "threadStack":Ljava/io/BufferedReader; */
/* :catchall_0 */
/* move-exception v0 */
/* move-object v10, v0 */
try { // :try_start_c
(( java.io.BufferedReader ) v4 ).close ( ); // invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
/* :try_end_c */
/* .catchall {:try_start_c ..:try_end_c} :catchall_1 */
/* move-object/from16 v22, v4 */
/* :catchall_1 */
/* move-exception v0 */
/* move-object/from16 v22, v4 */
/* move-object v4, v0 */
} // .end local v4 # "threadStack":Ljava/io/BufferedReader;
/* .local v22, "threadStack":Ljava/io/BufferedReader; */
try { // :try_start_d
(( java.lang.Throwable ) v10 ).addSuppressed ( v4 ); // invoke-virtual {v10, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v7 # "taskPath":Ljava/lang/String;
} // .end local v9 # "threadStat":Ljava/io/BufferedReader;
} // .end local v16 # "result":Z
} // .end local v17 # "taskDir":Ljava/io/File;
} // .end local v18 # "procInfo":Ljava/lang/StringBuilder;
} // .end local v20 # "logInfo":Ljava/lang/StringBuilder;
} // .end local v25 # "threadDir":Ljava/io/File;
} // .end local v26 # "threadDirs":[Ljava/io/File;
} // .end local v31 # "statInfo":Ljava/lang/String;
} // .end local v32 # "threadName":Ljava/lang/String;
} // .end local v33 # "threadState":Ljava/lang/String;
} // .end local p0 # "tag":Ljava/lang/String;
} // .end local p1 # "pid":I
} // .end local p2 # "scoutInfo":Lcom/android/server/ScoutHelper$ScoutBinderInfo;
} // :goto_4
/* throw v10 */
/* :try_end_d */
/* .catch Ljava/lang/Exception; {:try_start_d ..:try_end_d} :catch_0 */
/* .catchall {:try_start_d ..:try_end_d} :catchall_3 */
/* .line 363 */
} // .end local v22 # "threadStack":Ljava/io/BufferedReader;
/* .restart local v7 # "taskPath":Ljava/lang/String; */
/* .restart local v9 # "threadStat":Ljava/io/BufferedReader; */
/* .restart local v16 # "result":Z */
/* .restart local v17 # "taskDir":Ljava/io/File; */
/* .restart local v18 # "procInfo":Ljava/lang/StringBuilder; */
/* .restart local v20 # "logInfo":Ljava/lang/StringBuilder; */
/* .restart local v25 # "threadDir":Ljava/io/File; */
/* .restart local v26 # "threadDirs":[Ljava/io/File; */
/* .restart local v31 # "statInfo":Ljava/lang/String; */
/* .restart local v32 # "threadName":Ljava/lang/String; */
/* .restart local v33 # "threadState":Ljava/lang/String; */
/* .restart local p0 # "tag":Ljava/lang/String; */
/* .restart local p1 # "pid":I */
/* .restart local p2 # "scoutInfo":Lcom/android/server/ScoutHelper$ScoutBinderInfo; */
/* :catch_0 */
/* move-exception v0 */
/* :catch_1 */
/* move-exception v0 */
/* :catch_2 */
/* move-exception v0 */
/* move-object/from16 v34, v4 */
/* :catch_3 */
/* move-exception v0 */
/* move-object/from16 v34, v4 */
/* move-object/from16 v30, v11 */
} // :goto_5
/* move-object/from16 v11, v22 */
} // .end local v18 # "procInfo":Ljava/lang/StringBuilder;
/* .restart local v10 # "procInfo":Ljava/lang/StringBuilder; */
/* :catch_4 */
/* move-exception v0 */
/* move-object/from16 v34, v4 */
/* move-object/from16 v18, v10 */
/* move-object/from16 v30, v11 */
/* move-object/from16 v11, v22 */
/* .line 365 */
} // .end local v10 # "procInfo":Ljava/lang/StringBuilder;
/* .restart local v18 # "procInfo":Ljava/lang/StringBuilder; */
} // :goto_6
int v0 = 1; // const/4 v0, 0x1
/* move/from16 v16, v0 */
/* .line 367 */
} // .end local v20 # "logInfo":Ljava/lang/StringBuilder;
} // .end local v31 # "statInfo":Ljava/lang/String;
} // .end local v32 # "threadName":Ljava/lang/String;
} // .end local v33 # "threadState":Ljava/lang/String;
} // :goto_7
try { // :try_start_e
(( java.io.BufferedReader ) v9 ).close ( ); // invoke-virtual {v9}, Ljava/io/BufferedReader;->close()V
/* :try_end_e */
/* .catch Ljava/lang/Exception; {:try_start_e ..:try_end_e} :catch_5 */
/* .line 331 */
} // .end local v9 # "threadStat":Ljava/io/BufferedReader;
} // .end local v25 # "threadDir":Ljava/io/File;
/* add-int/lit8 v0, v28, 0x1 */
/* move-object v1, v6 */
/* move-object/from16 v22, v11 */
/* move-object/from16 v9, v26 */
/* move/from16 v10, v27 */
/* move-object/from16 v6, v29 */
/* move-object/from16 v20, v30 */
/* move-object/from16 v4, v34 */
/* move v11, v0 */
/* goto/16 :goto_0 */
/* .line 332 */
} // .end local v18 # "procInfo":Ljava/lang/StringBuilder;
/* .restart local v9 # "threadStat":Ljava/io/BufferedReader; */
/* .restart local v10 # "procInfo":Ljava/lang/StringBuilder; */
/* .restart local v25 # "threadDir":Ljava/io/File; */
/* :catchall_2 */
/* move-exception v0 */
/* move-object/from16 v18, v10 */
/* move-object v3, v0 */
} // .end local v10 # "procInfo":Ljava/lang/StringBuilder;
/* .restart local v18 # "procInfo":Ljava/lang/StringBuilder; */
/* :catchall_3 */
/* move-exception v0 */
/* :catchall_4 */
/* move-exception v0 */
/* move-object/from16 v1, p0 */
} // :goto_8
/* move-object v3, v0 */
} // :goto_9
try { // :try_start_f
(( java.io.BufferedReader ) v9 ).close ( ); // invoke-virtual {v9}, Ljava/io/BufferedReader;->close()V
/* :try_end_f */
/* .catchall {:try_start_f ..:try_end_f} :catchall_5 */
/* :catchall_5 */
/* move-exception v0 */
/* move-object v4, v0 */
try { // :try_start_10
(( java.lang.Throwable ) v3 ).addSuppressed ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v16 # "result":Z
} // .end local v18 # "procInfo":Ljava/lang/StringBuilder;
} // .end local p0 # "tag":Ljava/lang/String;
} // .end local p1 # "pid":I
} // .end local p2 # "scoutInfo":Lcom/android/server/ScoutHelper$ScoutBinderInfo;
} // :goto_a
/* throw v3 */
/* :try_end_10 */
/* .catch Ljava/lang/Exception; {:try_start_10 ..:try_end_10} :catch_5 */
/* .line 408 */
} // .end local v7 # "taskPath":Ljava/lang/String;
} // .end local v9 # "threadStat":Ljava/io/BufferedReader;
} // .end local v17 # "taskDir":Ljava/io/File;
} // .end local v25 # "threadDir":Ljava/io/File;
} // .end local v26 # "threadDirs":[Ljava/io/File;
/* .restart local v16 # "result":Z */
/* .restart local v18 # "procInfo":Ljava/lang/StringBuilder; */
/* .restart local p0 # "tag":Ljava/lang/String; */
/* .restart local p1 # "pid":I */
/* .restart local p2 # "scoutInfo":Lcom/android/server/ScoutHelper$ScoutBinderInfo; */
/* :catch_5 */
/* move-exception v0 */
/* .line 331 */
/* .restart local v7 # "taskPath":Ljava/lang/String; */
/* .local v9, "threadDirs":[Ljava/io/File; */
/* .restart local v17 # "taskDir":Ljava/io/File; */
} // :cond_6
/* move-object/from16 v1, p0 */
/* move-object/from16 v26, v9 */
} // .end local v9 # "threadDirs":[Ljava/io/File;
/* .restart local v26 # "threadDirs":[Ljava/io/File; */
/* move-object/from16 v6, v18 */
/* goto/16 :goto_11 */
/* .line 408 */
} // .end local v7 # "taskPath":Ljava/lang/String;
} // .end local v17 # "taskDir":Ljava/io/File;
} // .end local v26 # "threadDirs":[Ljava/io/File;
/* :catch_6 */
/* move-exception v0 */
/* move-object/from16 v1, p0 */
} // :goto_b
/* move/from16 v5, v16 */
/* move-object/from16 v6, v18 */
/* goto/16 :goto_14 */
/* .line 370 */
/* .restart local v7 # "taskPath":Ljava/lang/String; */
/* .restart local v9 # "threadDirs":[Ljava/io/File; */
/* .restart local v17 # "taskDir":Ljava/io/File; */
} // :cond_7
/* move-object/from16 v29, v6 */
/* move-object/from16 v26, v9 */
/* move-object/from16 v24, v11 */
/* move-object/from16 v30, v20 */
/* move-object/from16 v11, v22 */
/* move-object v6, v1 */
/* move-object/from16 v1, p0 */
} // .end local v9 # "threadDirs":[Ljava/io/File;
/* .restart local v26 # "threadDirs":[Ljava/io/File; */
try { // :try_start_11
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* invoke-static/range {p1 ..p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String; */
(( java.lang.StringBuilder ) v4 ).append ( v9 ); // invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v11 ); // invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 371 */
/* .local v4, "stackPath":Ljava/lang/String; */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v9 ).append ( v0 ); // invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* invoke-static/range {p1 ..p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String; */
(( java.lang.StringBuilder ) v0 ).append ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-object/from16 v9, v29 */
(( java.lang.StringBuilder ) v0 ).append ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* move-object v9, v0 */
/* .line 372 */
/* .local v9, "statPath":Ljava/lang/String; */
/* new-instance v0, Ljava/io/BufferedReader; */
/* new-instance v10, Ljava/io/FileReader; */
/* invoke-direct {v10, v9}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V */
/* invoke-direct {v0, v10}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* :try_end_11 */
/* .catch Ljava/lang/Exception; {:try_start_11 ..:try_end_11} :catch_a */
/* move-object v10, v0 */
/* .line 373 */
/* .local v10, "procStat":Ljava/io/BufferedReader; */
try { // :try_start_12
(( java.io.BufferedReader ) v10 ).readLine ( ); // invoke-virtual {v10}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* :try_end_12 */
/* .catchall {:try_start_12 ..:try_end_12} :catchall_c */
/* move-object v11, v0 */
/* .line 374 */
/* .local v11, "statInfo":Ljava/lang/String; */
/* move-object/from16 v20, v9 */
/* const/16 v9, 0x28 */
} // .end local v9 # "statPath":Ljava/lang/String;
/* .local v20, "statPath":Ljava/lang/String; */
try { // :try_start_13
v0 = (( java.lang.String ) v11 ).indexOf ( v9 ); // invoke-virtual {v11, v9}, Ljava/lang/String;->indexOf(I)I
/* :try_end_13 */
/* .catchall {:try_start_13 ..:try_end_13} :catchall_b */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 375 */
/* move-object/from16 v19, v10 */
/* const/16 v9, 0x29 */
} // .end local v10 # "procStat":Ljava/io/BufferedReader;
/* .local v19, "procStat":Ljava/io/BufferedReader; */
try { // :try_start_14
v10 = (( java.lang.String ) v11 ).indexOf ( v9 ); // invoke-virtual {v11, v9}, Ljava/lang/String;->indexOf(I)I
/* .line 374 */
(( java.lang.String ) v11 ).substring ( v0, v10 ); // invoke-virtual {v11, v0, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;
/* move-object v10, v0 */
/* .line 376 */
/* .local v10, "procName":Ljava/lang/String; */
v0 = (( java.lang.String ) v11 ).indexOf ( v9 ); // invoke-virtual {v11, v9}, Ljava/lang/String;->indexOf(I)I
/* add-int/lit8 v0, v0, 0x2 */
(( java.lang.String ) v11 ).substring ( v0 ); // invoke-virtual {v11, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;
/* .line 377 */
(( java.lang.String ) v0 ).split ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* aget-object v0, v0, v21 */
/* move-object v3, v0 */
/* .line 378 */
/* .local v3, "procState":Ljava/lang/String; */
v0 = (( java.lang.String ) v3 ).equals ( v8 ); // invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_14 */
/* .catchall {:try_start_14 ..:try_end_14} :catchall_a */
/* if-nez v0, :cond_9 */
try { // :try_start_15
v0 = (( java.lang.String ) v3 ).equals ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_9 */
v0 = (( java.lang.String ) v3 ).equals ( v12 ); // invoke-virtual {v3, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_9 */
/* .line 379 */
v0 = (( java.lang.String ) v3 ).equals ( v6 ); // invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_15 */
/* .catchall {:try_start_15 ..:try_end_15} :catchall_6 */
if ( v0 != null) { // if-eqz v0, :cond_8
} // :cond_8
/* move/from16 v5, v16 */
/* move-object/from16 v6, v18 */
/* goto/16 :goto_10 */
/* .line 372 */
} // .end local v3 # "procState":Ljava/lang/String;
} // .end local v10 # "procName":Ljava/lang/String;
} // .end local v11 # "statInfo":Ljava/lang/String;
/* :catchall_6 */
/* move-exception v0 */
/* move-object v3, v0 */
/* move-object/from16 v6, v18 */
/* goto/16 :goto_12 */
/* .line 380 */
/* .restart local v3 # "procState":Ljava/lang/String; */
/* .restart local v10 # "procName":Ljava/lang/String; */
/* .restart local v11 # "statInfo":Ljava/lang/String; */
} // :cond_9
} // :goto_c
try { // :try_start_16
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* move-object v5, v0 */
/* .line 381 */
/* .local v5, "logInfo":Ljava/lang/StringBuilder; */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v15 ); // invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v14 ); // invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = " state in main thread ( name:"; // const-string v8, " state in main thread ( name:"
(( java.lang.StringBuilder ) v0 ).append ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v10 ); // invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v13 ); // invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 383 */
v0 = (( java.lang.String ) v3 ).equals ( v6 ); // invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_16 */
/* .catchall {:try_start_16 ..:try_end_16} :catchall_a */
if ( v0 != null) { // if-eqz v0, :cond_a
/* .line 384 */
try { // :try_start_17
/* invoke-static/range {p1 ..p1}, Lcom/android/server/ScoutHelper;->getProcStatus(I)Lcom/android/server/ScoutHelper$ProcStatus; */
/* .line 385 */
/* .local v0, "procStatus":Lcom/android/server/ScoutHelper$ProcStatus; */
if ( v0 != null) { // if-eqz v0, :cond_a
/* .line 386 */
/* iget v6, v0, Lcom/android/server/ScoutHelper$ProcStatus;->tracerPid:I */
/* .line 387 */
/* .local v6, "tracerPid":I */
int v8 = -1; // const/4 v8, -0x1
/* if-le v6, v8, :cond_a */
/* .line 388 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
/* move-object/from16 v9, v24 */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v6 ); // invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* move-object/from16 v9, v23 */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 389 */
com.android.server.ScoutHelper .getProcessCmdline ( v6 );
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v13 ); // invoke-virtual {v8, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 388 */
(( java.lang.StringBuilder ) v5 ).append ( v8 ); // invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* :try_end_17 */
/* .catchall {:try_start_17 ..:try_end_17} :catchall_6 */
/* .line 393 */
} // .end local v0 # "procStatus":Lcom/android/server/ScoutHelper$ProcStatus;
} // .end local v6 # "tracerPid":I
} // :cond_a
try { // :try_start_18
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 394 */
/* move-object/from16 v6, v30 */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 395 */
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* :try_end_18 */
/* .catchall {:try_start_18 ..:try_end_18} :catchall_a */
/* move-object/from16 v6, v18 */
} // .end local v18 # "procInfo":Ljava/lang/StringBuilder;
/* .local v6, "procInfo":Ljava/lang/StringBuilder; */
try { // :try_start_19
(( java.lang.StringBuilder ) v6 ).append ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* :try_end_19 */
/* .catchall {:try_start_19 ..:try_end_19} :catchall_9 */
/* .line 396 */
try { // :try_start_1a
/* new-instance v0, Ljava/io/BufferedReader; */
/* new-instance v8, Ljava/io/FileReader; */
/* invoke-direct {v8, v4}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V */
/* invoke-direct {v0, v8}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* :try_end_1a */
/* .catch Ljava/lang/Exception; {:try_start_1a ..:try_end_1a} :catch_7 */
/* .catchall {:try_start_1a ..:try_end_1a} :catchall_9 */
/* move-object v8, v0 */
/* .line 397 */
/* .local v8, "procStack":Ljava/io/BufferedReader; */
int v0 = 0; // const/4 v0, 0x0
/* .line 398 */
/* .local v0, "stackLineInfo":Ljava/lang/String; */
} // :goto_d
try { // :try_start_1b
(( java.io.BufferedReader ) v8 ).readLine ( ); // invoke-virtual {v8}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v0, v9 */
if ( v9 != null) { // if-eqz v9, :cond_b
/* .line 399 */
android.util.Slog .d ( v1,v0 );
/* :try_end_1b */
/* .catchall {:try_start_1b ..:try_end_1b} :catchall_7 */
/* .line 401 */
} // .end local v0 # "stackLineInfo":Ljava/lang/String;
} // :cond_b
try { // :try_start_1c
(( java.io.BufferedReader ) v8 ).close ( ); // invoke-virtual {v8}, Ljava/io/BufferedReader;->close()V
/* :try_end_1c */
/* .catch Ljava/lang/Exception; {:try_start_1c ..:try_end_1c} :catch_7 */
/* .catchall {:try_start_1c ..:try_end_1c} :catchall_9 */
/* .line 403 */
} // .end local v8 # "procStack":Ljava/io/BufferedReader;
/* .line 396 */
/* .restart local v8 # "procStack":Ljava/io/BufferedReader; */
/* :catchall_7 */
/* move-exception v0 */
/* move-object v9, v0 */
try { // :try_start_1d
(( java.io.BufferedReader ) v8 ).close ( ); // invoke-virtual {v8}, Ljava/io/BufferedReader;->close()V
/* :try_end_1d */
/* .catchall {:try_start_1d ..:try_end_1d} :catchall_8 */
/* :catchall_8 */
/* move-exception v0 */
/* move-object v12, v0 */
try { // :try_start_1e
(( java.lang.Throwable ) v9 ).addSuppressed ( v12 ); // invoke-virtual {v9, v12}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v3 # "procState":Ljava/lang/String;
} // .end local v4 # "stackPath":Ljava/lang/String;
} // .end local v5 # "logInfo":Ljava/lang/StringBuilder;
} // .end local v6 # "procInfo":Ljava/lang/StringBuilder;
} // .end local v7 # "taskPath":Ljava/lang/String;
} // .end local v10 # "procName":Ljava/lang/String;
} // .end local v11 # "statInfo":Ljava/lang/String;
} // .end local v16 # "result":Z
} // .end local v17 # "taskDir":Ljava/io/File;
} // .end local v19 # "procStat":Ljava/io/BufferedReader;
} // .end local v20 # "statPath":Ljava/lang/String;
} // .end local v26 # "threadDirs":[Ljava/io/File;
} // .end local p0 # "tag":Ljava/lang/String;
} // .end local p1 # "pid":I
} // .end local p2 # "scoutInfo":Lcom/android/server/ScoutHelper$ScoutBinderInfo;
} // :goto_e
/* throw v9 */
/* :try_end_1e */
/* .catch Ljava/lang/Exception; {:try_start_1e ..:try_end_1e} :catch_7 */
/* .catchall {:try_start_1e ..:try_end_1e} :catchall_9 */
/* .line 401 */
} // .end local v8 # "procStack":Ljava/io/BufferedReader;
/* .restart local v3 # "procState":Ljava/lang/String; */
/* .restart local v4 # "stackPath":Ljava/lang/String; */
/* .restart local v5 # "logInfo":Ljava/lang/StringBuilder; */
/* .restart local v6 # "procInfo":Ljava/lang/StringBuilder; */
/* .restart local v7 # "taskPath":Ljava/lang/String; */
/* .restart local v10 # "procName":Ljava/lang/String; */
/* .restart local v11 # "statInfo":Ljava/lang/String; */
/* .restart local v16 # "result":Z */
/* .restart local v17 # "taskDir":Ljava/io/File; */
/* .restart local v19 # "procStat":Ljava/io/BufferedReader; */
/* .restart local v20 # "statPath":Ljava/lang/String; */
/* .restart local v26 # "threadDirs":[Ljava/io/File; */
/* .restart local p0 # "tag":Ljava/lang/String; */
/* .restart local p1 # "pid":I */
/* .restart local p2 # "scoutInfo":Lcom/android/server/ScoutHelper$ScoutBinderInfo; */
/* :catch_7 */
/* move-exception v0 */
/* .line 402 */
/* .local v0, "e":Ljava/lang/Exception; */
try { // :try_start_1f
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "Failed to read "; // const-string v9, "Failed to read "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v7 ); // invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v9 = " :"; // const-string v9, " :"
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v1,v8 );
/* :try_end_1f */
/* .catchall {:try_start_1f ..:try_end_1f} :catchall_9 */
/* .line 404 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_f
int v0 = 1; // const/4 v0, 0x1
/* move v5, v0 */
/* .line 406 */
} // .end local v3 # "procState":Ljava/lang/String;
} // .end local v10 # "procName":Ljava/lang/String;
} // .end local v11 # "statInfo":Ljava/lang/String;
} // .end local v16 # "result":Z
/* .local v5, "result":Z */
} // :goto_10
try { // :try_start_20
/* invoke-virtual/range {v19 ..v19}, Ljava/io/BufferedReader;->close()V */
/* :try_end_20 */
/* .catch Ljava/lang/Exception; {:try_start_20 ..:try_end_20} :catch_8 */
/* move/from16 v16, v5 */
/* .line 410 */
} // .end local v4 # "stackPath":Ljava/lang/String;
} // .end local v5 # "result":Z
} // .end local v7 # "taskPath":Ljava/lang/String;
} // .end local v17 # "taskDir":Ljava/io/File;
} // .end local v19 # "procStat":Ljava/io/BufferedReader;
} // .end local v20 # "statPath":Ljava/lang/String;
} // .end local v26 # "threadDirs":[Ljava/io/File;
/* .restart local v16 # "result":Z */
} // :goto_11
/* .line 408 */
} // .end local v16 # "result":Z
/* .restart local v5 # "result":Z */
/* :catch_8 */
/* move-exception v0 */
/* .line 372 */
} // .end local v5 # "result":Z
/* .restart local v4 # "stackPath":Ljava/lang/String; */
/* .restart local v7 # "taskPath":Ljava/lang/String; */
/* .restart local v16 # "result":Z */
/* .restart local v17 # "taskDir":Ljava/io/File; */
/* .restart local v19 # "procStat":Ljava/io/BufferedReader; */
/* .restart local v20 # "statPath":Ljava/lang/String; */
/* .restart local v26 # "threadDirs":[Ljava/io/File; */
/* :catchall_9 */
/* move-exception v0 */
/* move-object v3, v0 */
} // .end local v6 # "procInfo":Ljava/lang/StringBuilder;
/* .restart local v18 # "procInfo":Ljava/lang/StringBuilder; */
/* :catchall_a */
/* move-exception v0 */
/* move-object/from16 v6, v18 */
/* move-object v3, v0 */
} // .end local v18 # "procInfo":Ljava/lang/StringBuilder;
/* .restart local v6 # "procInfo":Ljava/lang/StringBuilder; */
} // .end local v6 # "procInfo":Ljava/lang/StringBuilder;
} // .end local v19 # "procStat":Ljava/io/BufferedReader;
/* .local v10, "procStat":Ljava/io/BufferedReader; */
/* .restart local v18 # "procInfo":Ljava/lang/StringBuilder; */
/* :catchall_b */
/* move-exception v0 */
/* move-object/from16 v19, v10 */
/* move-object/from16 v6, v18 */
/* move-object v3, v0 */
} // .end local v10 # "procStat":Ljava/io/BufferedReader;
} // .end local v18 # "procInfo":Ljava/lang/StringBuilder;
/* .restart local v6 # "procInfo":Ljava/lang/StringBuilder; */
/* .restart local v19 # "procStat":Ljava/io/BufferedReader; */
} // .end local v6 # "procInfo":Ljava/lang/StringBuilder;
} // .end local v19 # "procStat":Ljava/io/BufferedReader;
} // .end local v20 # "statPath":Ljava/lang/String;
/* .restart local v9 # "statPath":Ljava/lang/String; */
/* .restart local v10 # "procStat":Ljava/io/BufferedReader; */
/* .restart local v18 # "procInfo":Ljava/lang/StringBuilder; */
/* :catchall_c */
/* move-exception v0 */
/* move-object/from16 v20, v9 */
/* move-object/from16 v19, v10 */
/* move-object/from16 v6, v18 */
/* move-object v3, v0 */
} // .end local v9 # "statPath":Ljava/lang/String;
} // .end local v10 # "procStat":Ljava/io/BufferedReader;
} // .end local v18 # "procInfo":Ljava/lang/StringBuilder;
/* .restart local v6 # "procInfo":Ljava/lang/StringBuilder; */
/* .restart local v19 # "procStat":Ljava/io/BufferedReader; */
/* .restart local v20 # "statPath":Ljava/lang/String; */
} // :goto_12
try { // :try_start_21
/* invoke-virtual/range {v19 ..v19}, Ljava/io/BufferedReader;->close()V */
/* :try_end_21 */
/* .catchall {:try_start_21 ..:try_end_21} :catchall_d */
/* :catchall_d */
/* move-exception v0 */
/* move-object v5, v0 */
try { // :try_start_22
(( java.lang.Throwable ) v3 ).addSuppressed ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v6 # "procInfo":Ljava/lang/StringBuilder;
} // .end local v16 # "result":Z
} // .end local p0 # "tag":Ljava/lang/String;
} // .end local p1 # "pid":I
} // .end local p2 # "scoutInfo":Lcom/android/server/ScoutHelper$ScoutBinderInfo;
} // :goto_13
/* throw v3 */
/* :try_end_22 */
/* .catch Ljava/lang/Exception; {:try_start_22 ..:try_end_22} :catch_9 */
/* .line 408 */
} // .end local v4 # "stackPath":Ljava/lang/String;
} // .end local v7 # "taskPath":Ljava/lang/String;
} // .end local v17 # "taskDir":Ljava/io/File;
} // .end local v19 # "procStat":Ljava/io/BufferedReader;
} // .end local v20 # "statPath":Ljava/lang/String;
} // .end local v26 # "threadDirs":[Ljava/io/File;
/* .restart local v6 # "procInfo":Ljava/lang/StringBuilder; */
/* .restart local v16 # "result":Z */
/* .restart local p0 # "tag":Ljava/lang/String; */
/* .restart local p1 # "pid":I */
/* .restart local p2 # "scoutInfo":Lcom/android/server/ScoutHelper$ScoutBinderInfo; */
/* :catch_9 */
/* move-exception v0 */
/* move/from16 v5, v16 */
} // .end local v6 # "procInfo":Ljava/lang/StringBuilder;
/* .restart local v18 # "procInfo":Ljava/lang/StringBuilder; */
/* :catch_a */
/* move-exception v0 */
/* move-object/from16 v6, v18 */
/* move/from16 v5, v16 */
} // .end local v18 # "procInfo":Ljava/lang/StringBuilder;
/* .restart local v6 # "procInfo":Ljava/lang/StringBuilder; */
} // .end local v16 # "result":Z
/* .restart local v5 # "result":Z */
/* :catch_b */
/* move-exception v0 */
/* move/from16 v16, v5 */
/* .line 409 */
/* .restart local v0 # "e":Ljava/lang/Exception; */
} // :goto_14
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Failed to read thread stat: "; // const-string v4, "Failed to read thread stat: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v1,v3 );
/* move/from16 v16, v5 */
/* .line 411 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // .end local v5 # "result":Z
/* .restart local v16 # "result":Z */
} // :goto_15
/* move-object/from16 v3, p2 */
if ( v3 != null) { // if-eqz v3, :cond_c
v0 = (( java.lang.StringBuilder ) v6 ).length ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I
if ( v0 != null) { // if-eqz v0, :cond_c
/* .line 412 */
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.android.server.ScoutHelper$ScoutBinderInfo ) v3 ).setProcInfo ( v0 ); // invoke-virtual {v3, v0}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->setProcInfo(Ljava/lang/String;)V
/* .line 414 */
} // :cond_c
/* invoke-static/range {v16 ..v16}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean; */
} // .end method
public static Boolean addPidtoList ( java.lang.String p0, Integer p1, java.util.ArrayList p2, java.util.ArrayList p3 ) {
/* .locals 5 */
/* .param p0, "tag" # Ljava/lang/String; */
/* .param p1, "pid" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "I", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Integer;", */
/* ">;", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Integer;", */
/* ">;)Z" */
/* } */
} // .end annotation
/* .line 437 */
/* .local p2, "javaProcess":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
/* .local p3, "nativeProcess":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
v0 = com.android.server.ScoutHelper .getOomAdjOfPid ( p0,p1 );
/* .line 438 */
/* .local v0, "adj":I */
v1 = com.android.server.ScoutHelper .checkIsJavaOrNativeProcess ( v0 );
/* .line 439 */
/* .local v1, "isJavaOrNativeProcess":I */
int v2 = 0; // const/4 v2, 0x0
/* if-nez v1, :cond_0 */
/* .line 440 */
} // :cond_0
int v3 = 1; // const/4 v3, 0x1
/* if-ne v1, v3, :cond_1 */
/* .line 441 */
java.lang.Integer .valueOf ( p1 );
v4 = (( java.util.ArrayList ) p2 ).contains ( v4 ); // invoke-virtual {p2, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* if-nez v4, :cond_1 */
/* .line 442 */
java.lang.Integer .valueOf ( p1 );
(( java.util.ArrayList ) p2 ).add ( v2 ); // invoke-virtual {p2, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 443 */
/* .line 444 */
} // :cond_1
int v4 = 2; // const/4 v4, 0x2
/* if-ne v1, v4, :cond_2 */
/* .line 445 */
java.lang.Integer .valueOf ( p1 );
v4 = (( java.util.ArrayList ) p3 ).contains ( v4 ); // invoke-virtual {p3, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* if-nez v4, :cond_2 */
/* .line 446 */
java.lang.Integer .valueOf ( p1 );
(( java.util.ArrayList ) p3 ).add ( v2 ); // invoke-virtual {p3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 447 */
/* .line 449 */
} // :cond_2
} // .end method
public static void captureLog ( java.lang.String p0, java.lang.String p1, java.util.List p2, java.util.List p3, Boolean p4, Integer p5, Boolean p6, java.lang.String p7, java.util.List p8 ) {
/* .locals 15 */
/* .param p0, "type" # Ljava/lang/String; */
/* .param p1, "headline" # Ljava/lang/String; */
/* .param p4, "offline" # Z */
/* .param p5, "id" # I */
/* .param p6, "upload" # Z */
/* .param p7, "where" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;ZIZ", */
/* "Ljava/lang/String;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 834 */
/* .local p2, "actions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* .local p3, "params":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* .local p8, "includeFiles":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
com.android.server.ScoutHelper .getmDaemon ( );
/* .line 835 */
/* .local v12, "mClient":Lmiui/mqsas/IMQSNative; */
final String v13 = "ScoutHelper"; // const-string v13, "ScoutHelper"
/* if-nez v12, :cond_0 */
/* .line 836 */
final String v0 = "CaptureLog no mqsasd!"; // const-string v0, "CaptureLog no mqsasd!"
android.util.Slog .e ( v13,v0 );
/* .line 837 */
return;
/* .line 840 */
} // :cond_0
v0 = android.text.TextUtils .isEmpty ( p0 );
/* if-nez v0, :cond_3 */
v0 = /* invoke-static/range {p1 ..p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 844 */
} // :cond_1
/* if-nez p7, :cond_2 */
/* .line 845 */
final String v0 = ""; // const-string v0, ""
/* .line 846 */
} // .end local p7 # "where":Ljava/lang/String;
/* .local v0, "where":Ljava/lang/String; */
final String v1 = "CaptureLog where is null!"; // const-string v1, "CaptureLog where is null!"
android.util.Slog .d ( v13,v1 );
/* move-object v14, v0 */
/* .line 844 */
} // .end local v0 # "where":Ljava/lang/String;
/* .restart local p7 # "where":Ljava/lang/String; */
} // :cond_2
/* move-object/from16 v14, p7 */
/* .line 849 */
} // .end local p7 # "where":Ljava/lang/String;
/* .local v14, "where":Ljava/lang/String; */
} // :goto_0
int v11 = 0; // const/4 v11, 0x0
/* move-object v1, v12 */
/* move-object v2, p0 */
/* move-object/from16 v3, p1 */
/* move-object/from16 v4, p2 */
/* move-object/from16 v5, p3 */
/* move/from16 v6, p4 */
/* move/from16 v7, p5 */
/* move/from16 v8, p6 */
/* move-object v9, v14 */
/* move-object/from16 v10, p8 */
try { // :try_start_0
/* invoke-interface/range {v1 ..v11}, Lmiui/mqsas/IMQSNative;->captureLog(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;ZIZLjava/lang/String;Ljava/util/List;Z)V */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 854 */
} // :goto_1
/* .line 852 */
/* :catch_0 */
/* move-exception v0 */
/* move-object v1, v0 */
/* move-object v0, v1 */
/* .line 853 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "CaptureLog failed! unknown error"; // const-string v1, "CaptureLog failed! unknown error"
android.util.Slog .e ( v13,v1,v0 );
/* .line 850 */
} // .end local v0 # "e":Ljava/lang/Exception;
/* :catch_1 */
/* move-exception v0 */
/* move-object v1, v0 */
/* move-object v0, v1 */
/* .line 851 */
/* .local v0, "e":Landroid/os/RemoteException; */
final String v1 = "CaptureLog failed!"; // const-string v1, "CaptureLog failed!"
android.util.Slog .e ( v13,v1,v0 );
} // .end local v0 # "e":Landroid/os/RemoteException;
/* .line 855 */
} // :goto_2
return;
/* .line 841 */
} // .end local v14 # "where":Ljava/lang/String;
/* .restart local p7 # "where":Ljava/lang/String; */
} // :cond_3
} // :goto_3
final String v0 = "CaptureLog type or headline is null!"; // const-string v0, "CaptureLog type or headline is null!"
android.util.Slog .e ( v13,v0 );
/* .line 842 */
return;
} // .end method
public static Boolean checkAsyncBinderCallPidList ( Integer p0, Integer p1, com.android.server.ScoutHelper$ScoutBinderInfo p2, java.util.ArrayList p3, java.util.ArrayList p4 ) {
/* .locals 28 */
/* .param p0, "anrFromPid" # I */
/* .param p1, "anrToPid" # I */
/* .param p2, "info" # Lcom/android/server/ScoutHelper$ScoutBinderInfo; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(II", */
/* "Lcom/android/server/ScoutHelper$ScoutBinderInfo;", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Integer;", */
/* ">;", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Integer;", */
/* ">;)Z" */
/* } */
} // .end annotation
/* .line 455 */
/* .local p3, "javaProcess":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
/* .local p4, "nativeProcess":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
/* move/from16 v1, p0 */
/* move/from16 v2, p1 */
/* move-object/from16 v3, p2 */
/* move-object/from16 v4, p3 */
/* move-object/from16 v5, p4 */
final String v6 = "):"; // const-string v6, "):"
int v7 = 0; // const/4 v7, 0x0
/* .line 456 */
/* .local v7, "isBlockSystem":Z */
/* invoke-virtual/range {p2 ..p2}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->getTag()Ljava/lang/String; */
/* .line 457 */
/* .local v8, "tag":Ljava/lang/String; */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "Check async binder info with Broadcast ANR Pid="; // const-string v9, "Check async binder info with Broadcast ANR Pid="
(( java.lang.StringBuilder ) v0 ).append ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v9 = " SystemPid="; // const-string v9, " SystemPid="
(( java.lang.StringBuilder ) v0 ).append ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v8,v0 );
/* .line 459 */
com.android.server.ScoutHelper .getBinderLogFile ( v8,v2 );
/* .line 460 */
/* .local v9, "fileBinderReader":Ljava/io/File; */
/* if-nez v9, :cond_0 */
/* .line 461 */
/* .line 463 */
} // :cond_0
try { // :try_start_0
/* new-instance v0, Ljava/io/BufferedReader; */
/* new-instance v10, Ljava/io/FileReader; */
/* invoke-direct {v10, v9}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V */
/* invoke-direct {v0, v10}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_a */
/* move-object v10, v0 */
/* .line 464 */
/* .local v10, "BinderReader":Ljava/io/BufferedReader; */
int v0 = 0; // const/4 v0, 0x0
/* .line 465 */
/* .local v0, "binderTransactionInfo":Ljava/lang/String; */
int v11 = 0; // const/4 v11, 0x0
/* .line 466 */
/* .local v11, "fromPid":I */
} // :goto_0
try { // :try_start_1
(( java.io.BufferedReader ) v10 ).readLine ( ); // invoke-virtual {v10}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v13, v12 */
} // .end local v0 # "binderTransactionInfo":Ljava/lang/String;
/* .local v13, "binderTransactionInfo":Ljava/lang/String; */
if ( v12 != null) { // if-eqz v12, :cond_9
/* .line 467 */
final String v0 = "MIUI pending async transaction"; // const-string v0, "MIUI pending async transaction"
v0 = (( java.lang.String ) v13 ).startsWith ( v0 ); // invoke-virtual {v13, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_6 */
/* if-nez v0, :cond_1 */
try { // :try_start_2
final String v0 = "pending async transaction"; // const-string v0, "pending async transaction"
/* .line 468 */
v0 = (( java.lang.String ) v13 ).startsWith ( v0 ); // invoke-virtual {v13, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
/* if-nez v0, :cond_1 */
v0 = com.android.server.ScoutHelper.supportNewBinderLog;
/* .line 469 */
v0 = (( java.lang.Boolean ) v0 ).booleanValue ( ); // invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
if ( v0 != null) { // if-eqz v0, :cond_3
final String v0 = " pending async transaction"; // const-string v0, " pending async transaction"
v0 = (( java.lang.String ) v13 ).startsWith ( v0 ); // invoke-virtual {v13, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 463 */
} // .end local v11 # "fromPid":I
} // .end local v13 # "binderTransactionInfo":Ljava/lang/String;
/* :catchall_0 */
/* move-exception v0 */
/* move-object v6, v0 */
/* move-object/from16 v18, v9 */
/* move-object/from16 v23, v10 */
/* goto/16 :goto_7 */
/* .line 471 */
/* .restart local v11 # "fromPid":I */
/* .restart local v13 # "binderTransactionInfo":Ljava/lang/String; */
} // :cond_1
} // :goto_1
try { // :try_start_3
final String v0 = "\\bfrom((?:\\s+)?\\d+):((?:\\s+)?\\d+)\\s+to((?:\\s+)?\\d+):((?:\\s+)?\\d+)+.*code:((?:\\s+)?\\d+).*duration:((?:\\s+)?(\\d+(?:\\.\\d+)?))\\b"; // const-string v0, "\\bfrom((?:\\s+)?\\d+):((?:\\s+)?\\d+)\\s+to((?:\\s+)?\\d+):((?:\\s+)?\\d+)+.*code:((?:\\s+)?\\d+).*duration:((?:\\s+)?(\\d+(?:\\.\\d+)?))\\b"
/* .line 472 */
/* .local v0, "binderRegexPattern":Ljava/lang/String; */
v12 = com.android.server.ScoutHelper.supportNewBinderLog;
v12 = (( java.lang.Boolean ) v12 ).booleanValue ( ); // invoke-virtual {v12}, Ljava/lang/Boolean;->booleanValue()Z
/* :try_end_3 */
/* .catch Ljava/lang/Exception; {:try_start_3 ..:try_end_3} :catch_7 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_6 */
if ( v12 != null) { // if-eqz v12, :cond_2
try { // :try_start_4
final String v12 = "\\bfrom((?:\\s+)?\\d+):((?:\\s+)?\\d+)\\s+to((?:\\s+)?\\d+):((?:\\s+)?\\d+)+.*code((?:\\s+)?\\d+)+.*elapsed((?:\\s+)?\\d+)ms+.*"; // const-string v12, "\\bfrom((?:\\s+)?\\d+):((?:\\s+)?\\d+)\\s+to((?:\\s+)?\\d+):((?:\\s+)?\\d+)+.*code((?:\\s+)?\\d+)+.*elapsed((?:\\s+)?\\d+)ms+.*"
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_0 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* move-object v0, v12 */
/* .line 508 */
} // .end local v0 # "binderRegexPattern":Ljava/lang/String;
/* :catch_0 */
/* move-exception v0 */
/* move-object/from16 v18, v9 */
/* move-object/from16 v23, v10 */
/* move-object/from16 v21, v13 */
/* goto/16 :goto_6 */
/* .line 473 */
/* .restart local v0 # "binderRegexPattern":Ljava/lang/String; */
} // :cond_2
} // :goto_2
try { // :try_start_5
java.util.regex.Pattern .compile ( v0 );
/* .line 474 */
/* .local v12, "pattern":Ljava/util/regex/Pattern; */
(( java.util.regex.Pattern ) v12 ).matcher ( v13 ); // invoke-virtual {v12, v13}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
/* .line 475 */
/* .local v14, "matcher":Ljava/util/regex/Matcher; */
v15 = (( java.util.regex.Matcher ) v14 ).find ( ); // invoke-virtual {v14}, Ljava/util/regex/Matcher;->find()Z
/* :try_end_5 */
/* .catch Ljava/lang/Exception; {:try_start_5 ..:try_end_5} :catch_7 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_6 */
/* move-object/from16 v16, v0 */
} // .end local v0 # "binderRegexPattern":Ljava/lang/String;
/* .local v16, "binderRegexPattern":Ljava/lang/String; */
final String v0 = "("; // const-string v0, "("
/* if-nez v15, :cond_4 */
/* .line 476 */
try { // :try_start_6
/* new-instance v15, Ljava/lang/StringBuilder; */
/* invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v15 ).append ( v0 ); // invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v13 ); // invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v15 = ") regex match failed"; // const-string v15, ") regex match failed"
(( java.lang.StringBuilder ) v0 ).append ( v15 ); // invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v8,v0 );
/* :try_end_6 */
/* .catch Ljava/lang/Exception; {:try_start_6 ..:try_end_6} :catch_0 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_0 */
/* .line 477 */
/* nop */
/* .line 466 */
} // .end local v12 # "pattern":Ljava/util/regex/Pattern;
} // .end local v14 # "matcher":Ljava/util/regex/Matcher;
} // .end local v16 # "binderRegexPattern":Ljava/lang/String;
} // :cond_3
/* move-object v0, v13 */
/* .line 479 */
/* .restart local v12 # "pattern":Ljava/util/regex/Pattern; */
/* .restart local v14 # "matcher":Ljava/util/regex/Matcher; */
/* .restart local v16 # "binderRegexPattern":Ljava/lang/String; */
} // :cond_4
int v15 = 1; // const/4 v15, 0x1
try { // :try_start_7
(( java.util.regex.Matcher ) v14 ).group ( v15 ); // invoke-virtual {v14, v15}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;
(( java.lang.String ) v15 ).trim ( ); // invoke-virtual {v15}, Ljava/lang/String;->trim()Ljava/lang/String;
v15 = java.lang.Integer .parseInt ( v15 );
/* move v11, v15 */
/* .line 480 */
int v15 = 2; // const/4 v15, 0x2
(( java.util.regex.Matcher ) v14 ).group ( v15 ); // invoke-virtual {v14, v15}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;
(( java.lang.String ) v15 ).trim ( ); // invoke-virtual {v15}, Ljava/lang/String;->trim()Ljava/lang/String;
v15 = java.lang.Integer .parseInt ( v15 );
/* :try_end_7 */
/* .catch Ljava/lang/Exception; {:try_start_7 ..:try_end_7} :catch_7 */
/* .catchall {:try_start_7 ..:try_end_7} :catchall_6 */
/* .line 481 */
/* .local v15, "fromTid":I */
/* move/from16 v17, v7 */
} // .end local v7 # "isBlockSystem":Z
/* .local v17, "isBlockSystem":Z */
int v7 = 3; // const/4 v7, 0x3
try { // :try_start_8
(( java.util.regex.Matcher ) v14 ).group ( v7 ); // invoke-virtual {v14, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;
(( java.lang.String ) v7 ).trim ( ); // invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;
v7 = java.lang.Integer .parseInt ( v7 );
/* :try_end_8 */
/* .catch Ljava/lang/Exception; {:try_start_8 ..:try_end_8} :catch_6 */
/* .catchall {:try_start_8 ..:try_end_8} :catchall_4 */
/* .line 482 */
/* .local v7, "toPid":I */
/* move-object/from16 v18, v9 */
} // .end local v9 # "fileBinderReader":Ljava/io/File;
/* .local v18, "fileBinderReader":Ljava/io/File; */
int v9 = 4; // const/4 v9, 0x4
try { // :try_start_9
(( java.util.regex.Matcher ) v14 ).group ( v9 ); // invoke-virtual {v14, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;
(( java.lang.String ) v9 ).trim ( ); // invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;
v9 = java.lang.Integer .parseInt ( v9 );
/* .line 483 */
/* .local v9, "toTid":I */
/* move-object/from16 v19, v12 */
} // .end local v12 # "pattern":Ljava/util/regex/Pattern;
/* .local v19, "pattern":Ljava/util/regex/Pattern; */
int v12 = 5; // const/4 v12, 0x5
(( java.util.regex.Matcher ) v14 ).group ( v12 ); // invoke-virtual {v14, v12}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;
(( java.lang.String ) v12 ).trim ( ); // invoke-virtual {v12}, Ljava/lang/String;->trim()Ljava/lang/String;
v12 = java.lang.Integer .parseInt ( v12 );
/* .line 484 */
/* .local v12, "code":I */
/* const-wide/16 v20, 0x0 */
/* .line 485 */
/* .local v20, "waitTime":D */
v22 = com.android.server.ScoutHelper.supportNewBinderLog;
v22 = /* invoke-virtual/range {v22 ..v22}, Ljava/lang/Boolean;->booleanValue()Z */
/* :try_end_9 */
/* .catch Ljava/lang/Exception; {:try_start_9 ..:try_end_9} :catch_5 */
/* .catchall {:try_start_9 ..:try_end_9} :catchall_3 */
/* move-object/from16 v23, v10 */
} // .end local v10 # "BinderReader":Ljava/io/BufferedReader;
/* .local v23, "BinderReader":Ljava/io/BufferedReader; */
int v10 = 6; // const/4 v10, 0x6
if ( v22 != null) { // if-eqz v22, :cond_5
/* .line 486 */
try { // :try_start_a
(( java.util.regex.Matcher ) v14 ).group ( v10 ); // invoke-virtual {v14, v10}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;
(( java.lang.String ) v10 ).trim ( ); // invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;
java.lang.Double .valueOf ( v10 );
/* .line 487 */
/* .local v10, "elapsed":Ljava/lang/Double; */
(( java.lang.Double ) v10 ).doubleValue ( ); // invoke-virtual {v10}, Ljava/lang/Double;->doubleValue()D
/* move-result-wide v24 */
/* :try_end_a */
/* .catch Ljava/lang/Exception; {:try_start_a ..:try_end_a} :catch_1 */
/* .catchall {:try_start_a ..:try_end_a} :catchall_2 */
/* const-wide v26, 0x408f400000000000L # 1000.0 */
/* div-double v24, v24, v26 */
/* .line 488 */
} // .end local v10 # "elapsed":Ljava/lang/Double;
} // .end local v20 # "waitTime":D
/* .local v24, "waitTime":D */
/* move-object v10, v13 */
/* move-object/from16 v20, v14 */
/* move-wide/from16 v13, v24 */
/* .line 508 */
} // .end local v7 # "toPid":I
} // .end local v9 # "toTid":I
} // .end local v12 # "code":I
} // .end local v14 # "matcher":Ljava/util/regex/Matcher;
} // .end local v15 # "fromTid":I
} // .end local v16 # "binderRegexPattern":Ljava/lang/String;
} // .end local v19 # "pattern":Ljava/util/regex/Pattern;
} // .end local v24 # "waitTime":D
/* :catch_1 */
/* move-exception v0 */
/* move-object/from16 v21, v13 */
/* move/from16 v7, v17 */
/* goto/16 :goto_6 */
/* .line 489 */
/* .restart local v7 # "toPid":I */
/* .restart local v9 # "toTid":I */
/* .restart local v12 # "code":I */
/* .restart local v14 # "matcher":Ljava/util/regex/Matcher; */
/* .restart local v15 # "fromTid":I */
/* .restart local v16 # "binderRegexPattern":Ljava/lang/String; */
/* .restart local v19 # "pattern":Ljava/util/regex/Pattern; */
/* .restart local v20 # "waitTime":D */
} // :cond_5
try { // :try_start_b
(( java.util.regex.Matcher ) v14 ).group ( v10 ); // invoke-virtual {v14, v10}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;
(( java.lang.String ) v10 ).trim ( ); // invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;
java.lang.Double .valueOf ( v10 );
(( java.lang.Double ) v10 ).doubleValue ( ); // invoke-virtual {v10}, Ljava/lang/Double;->doubleValue()D
/* move-result-wide v24 */
/* :try_end_b */
/* .catch Ljava/lang/Exception; {:try_start_b ..:try_end_b} :catch_4 */
/* .catchall {:try_start_b ..:try_end_b} :catchall_2 */
} // .end local v20 # "waitTime":D
/* .restart local v24 # "waitTime":D */
/* move-object v10, v13 */
/* move-object/from16 v20, v14 */
/* move-wide/from16 v13, v24 */
/* .line 491 */
} // .end local v14 # "matcher":Ljava/util/regex/Matcher;
} // .end local v24 # "waitTime":D
/* .local v10, "binderTransactionInfo":Ljava/lang/String; */
/* .local v13, "waitTime":D */
/* .local v20, "matcher":Ljava/util/regex/Matcher; */
} // :goto_3
/* move-object/from16 v21, v10 */
} // .end local v10 # "binderTransactionInfo":Ljava/lang/String;
/* .local v21, "binderTransactionInfo":Ljava/lang/String; */
try { // :try_start_c
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
/* :try_end_c */
/* .catch Ljava/lang/Exception; {:try_start_c ..:try_end_c} :catch_3 */
/* .catchall {:try_start_c ..:try_end_c} :catchall_2 */
try { // :try_start_d
final String v4 = "pending async transaction from "; // const-string v4, "pending async transaction from "
(( java.lang.StringBuilder ) v10 ).append ( v4 ); // invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v11 ); // invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 492 */
com.android.server.ScoutHelper .getProcessCmdline ( v11 );
(( java.lang.StringBuilder ) v4 ).append ( v10 ); // invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v15 ); // invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 493 */
com.android.server.ScoutHelper .getProcessComm ( v11,v15 );
(( java.lang.StringBuilder ) v4 ).append ( v10 ); // invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v10 = ") to "; // const-string v10, ") to "
(( java.lang.StringBuilder ) v4 ).append ( v10 ); // invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v7 ); // invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 494 */
com.android.server.ScoutHelper .getProcessCmdline ( v7 );
(( java.lang.StringBuilder ) v4 ).append ( v10 ); // invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v9 ); // invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 495 */
com.android.server.ScoutHelper .getProcessComm ( v7,v9 );
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ") elapsed: "; // const-string v4, ") elapsed: "
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v13, v14 ); // invoke-virtual {v0, v13, v14}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
final String v4 = " s code: "; // const-string v4, " s code: "
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v12 ); // invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 498 */
/* .local v0, "binderLog":Ljava/lang/String; */
/* if-ne v11, v1, :cond_7 */
/* if-ne v7, v2, :cond_7 */
/* .line 499 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "Binder Info:"; // const-string v10, "Binder Info:"
(( java.lang.StringBuilder ) v4 ).append ( v10 ); // invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v8,v4 );
/* .line 500 */
(( com.android.server.ScoutHelper$ScoutBinderInfo ) v3 ).addBinderTransInfo ( v0 ); // invoke-virtual {v3, v0}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->addBinderTransInfo(Ljava/lang/String;)V
/* :try_end_d */
/* .catch Ljava/lang/Exception; {:try_start_d ..:try_end_d} :catch_2 */
/* .catchall {:try_start_d ..:try_end_d} :catchall_1 */
/* .line 501 */
/* const-wide/high16 v24, 0x4000000000000000L # 2.0 */
/* cmpl-double v4, v13, v24 */
/* if-lez v4, :cond_6 */
/* if-ne v11, v15, :cond_6 */
/* .line 502 */
int v4 = 1; // const/4 v4, 0x1
/* move/from16 v17, v4 */
/* .line 504 */
} // :cond_6
/* move-object/from16 v4, p3 */
try { // :try_start_e
v10 = com.android.server.ScoutHelper .addPidtoList ( v8,v7,v4,v5 );
if ( v10 != null) { // if-eqz v10, :cond_8
/* .line 505 */
com.android.server.ScoutHelper .checkBinderCallPidList ( v7,v3,v4,v5 );
/* :try_end_e */
/* .catch Ljava/lang/Exception; {:try_start_e ..:try_end_e} :catch_3 */
/* .catchall {:try_start_e ..:try_end_e} :catchall_2 */
/* .line 498 */
} // :cond_7
/* move-object/from16 v4, p3 */
/* .line 510 */
} // .end local v0 # "binderLog":Ljava/lang/String;
} // .end local v7 # "toPid":I
} // .end local v9 # "toTid":I
} // .end local v12 # "code":I
} // .end local v13 # "waitTime":D
} // .end local v15 # "fromTid":I
} // .end local v16 # "binderRegexPattern":Ljava/lang/String;
} // .end local v19 # "pattern":Ljava/util/regex/Pattern;
} // .end local v20 # "matcher":Ljava/util/regex/Matcher;
} // :cond_8
} // :goto_4
/* move/from16 v7, v17 */
} // .end local v17 # "isBlockSystem":Z
/* .local v7, "isBlockSystem":Z */
/* move-object/from16 v9, v18 */
/* move-object/from16 v0, v21 */
/* move-object/from16 v10, v23 */
/* goto/16 :goto_0 */
/* .line 463 */
} // .end local v7 # "isBlockSystem":Z
} // .end local v11 # "fromPid":I
} // .end local v21 # "binderTransactionInfo":Ljava/lang/String;
/* .restart local v17 # "isBlockSystem":Z */
/* :catchall_1 */
/* move-exception v0 */
/* move-object/from16 v4, p3 */
/* .line 508 */
/* .restart local v11 # "fromPid":I */
/* .restart local v21 # "binderTransactionInfo":Ljava/lang/String; */
/* :catch_2 */
/* move-exception v0 */
/* move-object/from16 v4, p3 */
/* move/from16 v7, v17 */
/* :catch_3 */
/* move-exception v0 */
/* move/from16 v7, v17 */
/* .line 463 */
} // .end local v11 # "fromPid":I
} // .end local v21 # "binderTransactionInfo":Ljava/lang/String;
/* :catchall_2 */
/* move-exception v0 */
} // :goto_5
/* move-object v6, v0 */
/* move/from16 v7, v17 */
/* goto/16 :goto_7 */
/* .line 508 */
/* .restart local v11 # "fromPid":I */
/* .local v13, "binderTransactionInfo":Ljava/lang/String; */
/* :catch_4 */
/* move-exception v0 */
/* move-object/from16 v21, v13 */
/* move/from16 v7, v17 */
} // .end local v13 # "binderTransactionInfo":Ljava/lang/String;
/* .restart local v21 # "binderTransactionInfo":Ljava/lang/String; */
/* .line 463 */
} // .end local v11 # "fromPid":I
} // .end local v21 # "binderTransactionInfo":Ljava/lang/String;
} // .end local v23 # "BinderReader":Ljava/io/BufferedReader;
/* .local v10, "BinderReader":Ljava/io/BufferedReader; */
/* :catchall_3 */
/* move-exception v0 */
/* move-object/from16 v23, v10 */
/* move-object v6, v0 */
/* move/from16 v7, v17 */
} // .end local v10 # "BinderReader":Ljava/io/BufferedReader;
/* .restart local v23 # "BinderReader":Ljava/io/BufferedReader; */
/* goto/16 :goto_7 */
/* .line 508 */
} // .end local v23 # "BinderReader":Ljava/io/BufferedReader;
/* .restart local v10 # "BinderReader":Ljava/io/BufferedReader; */
/* .restart local v11 # "fromPid":I */
/* .restart local v13 # "binderTransactionInfo":Ljava/lang/String; */
/* :catch_5 */
/* move-exception v0 */
/* move-object/from16 v23, v10 */
/* move-object/from16 v21, v13 */
/* move/from16 v7, v17 */
} // .end local v10 # "BinderReader":Ljava/io/BufferedReader;
} // .end local v13 # "binderTransactionInfo":Ljava/lang/String;
/* .restart local v21 # "binderTransactionInfo":Ljava/lang/String; */
/* .restart local v23 # "BinderReader":Ljava/io/BufferedReader; */
/* .line 463 */
} // .end local v11 # "fromPid":I
} // .end local v18 # "fileBinderReader":Ljava/io/File;
} // .end local v21 # "binderTransactionInfo":Ljava/lang/String;
} // .end local v23 # "BinderReader":Ljava/io/BufferedReader;
/* .local v9, "fileBinderReader":Ljava/io/File; */
/* .restart local v10 # "BinderReader":Ljava/io/BufferedReader; */
/* :catchall_4 */
/* move-exception v0 */
/* move-object/from16 v18, v9 */
/* move-object/from16 v23, v10 */
/* move-object v6, v0 */
/* move/from16 v7, v17 */
} // .end local v9 # "fileBinderReader":Ljava/io/File;
} // .end local v10 # "BinderReader":Ljava/io/BufferedReader;
/* .restart local v18 # "fileBinderReader":Ljava/io/File; */
/* .restart local v23 # "BinderReader":Ljava/io/BufferedReader; */
/* .line 508 */
} // .end local v18 # "fileBinderReader":Ljava/io/File;
} // .end local v23 # "BinderReader":Ljava/io/BufferedReader;
/* .restart local v9 # "fileBinderReader":Ljava/io/File; */
/* .restart local v10 # "BinderReader":Ljava/io/BufferedReader; */
/* .restart local v11 # "fromPid":I */
/* .restart local v13 # "binderTransactionInfo":Ljava/lang/String; */
/* :catch_6 */
/* move-exception v0 */
/* move-object/from16 v18, v9 */
/* move-object/from16 v23, v10 */
/* move-object/from16 v21, v13 */
/* move/from16 v7, v17 */
} // .end local v9 # "fileBinderReader":Ljava/io/File;
} // .end local v10 # "BinderReader":Ljava/io/BufferedReader;
} // .end local v13 # "binderTransactionInfo":Ljava/lang/String;
/* .restart local v18 # "fileBinderReader":Ljava/io/File; */
/* .restart local v21 # "binderTransactionInfo":Ljava/lang/String; */
/* .restart local v23 # "BinderReader":Ljava/io/BufferedReader; */
} // .end local v17 # "isBlockSystem":Z
} // .end local v18 # "fileBinderReader":Ljava/io/File;
} // .end local v21 # "binderTransactionInfo":Ljava/lang/String;
} // .end local v23 # "BinderReader":Ljava/io/BufferedReader;
/* .restart local v7 # "isBlockSystem":Z */
/* .restart local v9 # "fileBinderReader":Ljava/io/File; */
/* .restart local v10 # "BinderReader":Ljava/io/BufferedReader; */
/* .restart local v13 # "binderTransactionInfo":Ljava/lang/String; */
/* :catch_7 */
/* move-exception v0 */
/* move/from16 v17, v7 */
/* move-object/from16 v18, v9 */
/* move-object/from16 v23, v10 */
/* move-object/from16 v21, v13 */
/* .line 509 */
} // .end local v9 # "fileBinderReader":Ljava/io/File;
} // .end local v10 # "BinderReader":Ljava/io/BufferedReader;
} // .end local v13 # "binderTransactionInfo":Ljava/lang/String;
/* .local v0, "e":Ljava/lang/Exception; */
/* .restart local v18 # "fileBinderReader":Ljava/io/File; */
/* .restart local v21 # "binderTransactionInfo":Ljava/lang/String; */
/* .restart local v23 # "BinderReader":Ljava/io/BufferedReader; */
} // :goto_6
try { // :try_start_f
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "regular match error, String:"; // const-string v10, "regular match error, String:"
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-object/from16 v10, v21 */
} // .end local v21 # "binderTransactionInfo":Ljava/lang/String;
/* .local v10, "binderTransactionInfo":Ljava/lang/String; */
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v8,v9 );
/* :try_end_f */
/* .catchall {:try_start_f ..:try_end_f} :catchall_5 */
/* .line 510 */
/* move-object v0, v10 */
/* move-object/from16 v9, v18 */
/* move-object/from16 v10, v23 */
} // .end local v0 # "e":Ljava/lang/Exception;
/* goto/16 :goto_0 */
/* .line 463 */
} // .end local v10 # "binderTransactionInfo":Ljava/lang/String;
} // .end local v11 # "fromPid":I
/* :catchall_5 */
/* move-exception v0 */
/* move-object v6, v0 */
/* .line 466 */
} // .end local v18 # "fileBinderReader":Ljava/io/File;
} // .end local v23 # "BinderReader":Ljava/io/BufferedReader;
/* .restart local v9 # "fileBinderReader":Ljava/io/File; */
/* .local v10, "BinderReader":Ljava/io/BufferedReader; */
/* .restart local v11 # "fromPid":I */
/* .restart local v13 # "binderTransactionInfo":Ljava/lang/String; */
} // :cond_9
/* move/from16 v17, v7 */
/* move-object/from16 v18, v9 */
/* move-object/from16 v23, v10 */
/* move-object v10, v13 */
/* .line 513 */
} // .end local v7 # "isBlockSystem":Z
} // .end local v9 # "fileBinderReader":Ljava/io/File;
} // .end local v10 # "BinderReader":Ljava/io/BufferedReader;
} // .end local v11 # "fromPid":I
} // .end local v13 # "binderTransactionInfo":Ljava/lang/String;
/* .restart local v17 # "isBlockSystem":Z */
/* .restart local v18 # "fileBinderReader":Ljava/io/File; */
/* .restart local v23 # "BinderReader":Ljava/io/BufferedReader; */
try { // :try_start_10
/* invoke-virtual/range {v23 ..v23}, Ljava/io/BufferedReader;->close()V */
/* :try_end_10 */
/* .catch Ljava/lang/Exception; {:try_start_10 ..:try_end_10} :catch_8 */
/* .line 516 */
} // .end local v23 # "BinderReader":Ljava/io/BufferedReader;
/* move/from16 v7, v17 */
/* .line 513 */
/* :catch_8 */
/* move-exception v0 */
/* move/from16 v7, v17 */
/* .line 463 */
} // .end local v17 # "isBlockSystem":Z
} // .end local v18 # "fileBinderReader":Ljava/io/File;
/* .restart local v7 # "isBlockSystem":Z */
/* .restart local v9 # "fileBinderReader":Ljava/io/File; */
/* .restart local v10 # "BinderReader":Ljava/io/BufferedReader; */
/* :catchall_6 */
/* move-exception v0 */
/* move/from16 v17, v7 */
/* move-object/from16 v18, v9 */
/* move-object/from16 v23, v10 */
/* move-object v6, v0 */
} // .end local v9 # "fileBinderReader":Ljava/io/File;
} // .end local v10 # "BinderReader":Ljava/io/BufferedReader;
/* .restart local v18 # "fileBinderReader":Ljava/io/File; */
/* .restart local v23 # "BinderReader":Ljava/io/BufferedReader; */
} // :goto_7
try { // :try_start_11
/* invoke-virtual/range {v23 ..v23}, Ljava/io/BufferedReader;->close()V */
/* :try_end_11 */
/* .catchall {:try_start_11 ..:try_end_11} :catchall_7 */
/* :catchall_7 */
/* move-exception v0 */
/* move-object v9, v0 */
try { // :try_start_12
(( java.lang.Throwable ) v6 ).addSuppressed ( v9 ); // invoke-virtual {v6, v9}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v7 # "isBlockSystem":Z
} // .end local v8 # "tag":Ljava/lang/String;
} // .end local v18 # "fileBinderReader":Ljava/io/File;
} // .end local p0 # "anrFromPid":I
} // .end local p1 # "anrToPid":I
} // .end local p2 # "info":Lcom/android/server/ScoutHelper$ScoutBinderInfo;
} // .end local p3 # "javaProcess":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
} // .end local p4 # "nativeProcess":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
} // :goto_8
/* throw v6 */
/* :try_end_12 */
/* .catch Ljava/lang/Exception; {:try_start_12 ..:try_end_12} :catch_9 */
/* .line 513 */
} // .end local v23 # "BinderReader":Ljava/io/BufferedReader;
/* .restart local v7 # "isBlockSystem":Z */
/* .restart local v8 # "tag":Ljava/lang/String; */
/* .restart local v18 # "fileBinderReader":Ljava/io/File; */
/* .restart local p0 # "anrFromPid":I */
/* .restart local p1 # "anrToPid":I */
/* .restart local p2 # "info":Lcom/android/server/ScoutHelper$ScoutBinderInfo; */
/* .restart local p3 # "javaProcess":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
/* .restart local p4 # "nativeProcess":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
/* :catch_9 */
/* move-exception v0 */
} // .end local v18 # "fileBinderReader":Ljava/io/File;
/* .restart local v9 # "fileBinderReader":Ljava/io/File; */
/* :catch_a */
/* move-exception v0 */
/* move-object/from16 v18, v9 */
/* .line 514 */
} // .end local v9 # "fileBinderReader":Ljava/io/File;
/* .restart local v0 # "e":Ljava/lang/Exception; */
/* .restart local v18 # "fileBinderReader":Ljava/io/File; */
} // :goto_9
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 515 */
final String v6 = "Read binder Proc async transaction Error:"; // const-string v6, "Read binder Proc async transaction Error:"
android.util.Slog .w ( v8,v6,v0 );
/* .line 517 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_a
} // .end method
public static Boolean checkBinderCallPidList ( Integer p0, com.android.server.ScoutHelper$ScoutBinderInfo p1, java.util.ArrayList p2, java.util.ArrayList p3 ) {
/* .locals 38 */
/* .param p0, "Pid" # I */
/* .param p1, "info" # Lcom/android/server/ScoutHelper$ScoutBinderInfo; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I", */
/* "Lcom/android/server/ScoutHelper$ScoutBinderInfo;", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Integer;", */
/* ">;", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Integer;", */
/* ">;)Z" */
/* } */
} // .end annotation
/* .line 522 */
/* .local p2, "javaProcess":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
/* .local p3, "nativeProcess":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
/* move/from16 v1, p0 */
/* move-object/from16 v2, p1 */
/* move-object/from16 v3, p2 */
/* move-object/from16 v4, p3 */
final String v5 = "):"; // const-string v5, "):"
int v6 = 0; // const/4 v6, 0x0
/* .line 523 */
/* .local v6, "isBlockSystem":Z */
/* invoke-virtual/range {p1 ..p1}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->getTag()Ljava/lang/String; */
/* .line 524 */
/* .local v7, "tag":Ljava/lang/String; */
v8 = /* invoke-virtual/range {p1 ..p1}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->getCallType()I */
/* .line 525 */
/* .local v8, "callType":I */
int v0 = 0; // const/4 v0, 0x0
int v9 = 1; // const/4 v9, 0x1
/* if-ne v8, v9, :cond_0 */
/* sget-boolean v10, Lcom/android/server/ScoutHelper;->SYSRQ_ANR_D_THREAD:Z */
if ( v10 != null) { // if-eqz v10, :cond_0
/* move v10, v9 */
} // :cond_0
/* move v10, v0 */
/* .line 526 */
/* .local v10, "isCheckAppDThread":Z */
} // :goto_0
/* if-nez v8, :cond_1 */
/* move v0, v9 */
} // :cond_1
/* move v11, v0 */
/* .line 527 */
/* .local v11, "isCheckSystemDThread":Z */
/* if-nez v10, :cond_2 */
if ( v11 != null) { // if-eqz v11, :cond_3
} // :cond_2
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 528 */
com.android.server.ScoutHelper .CheckDState ( v7,v1,v2 );
v0 = (( java.lang.Boolean ) v0 ).booleanValue ( ); // invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 529 */
(( com.android.server.ScoutHelper$ScoutBinderInfo ) v2 ).setDThreadState ( v9 ); // invoke-virtual {v2, v9}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->setDThreadState(Z)V
/* .line 531 */
} // :cond_3
com.android.server.ScoutHelper .getBinderLogFile ( v7,v1 );
/* .line 532 */
/* .local v12, "fileBinderReader":Ljava/io/File; */
/* if-nez v12, :cond_4 */
/* .line 533 */
/* .line 536 */
} // :cond_4
try { // :try_start_0
/* new-instance v0, Ljava/io/BufferedReader; */
/* new-instance v13, Ljava/io/FileReader; */
/* invoke-direct {v13, v12}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V */
/* invoke-direct {v0, v13}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_d */
/* move-object v13, v0 */
/* .line 537 */
/* .local v13, "BinderReader":Ljava/io/BufferedReader; */
int v0 = 0; // const/4 v0, 0x0
/* .line 538 */
/* .local v0, "binderTransactionInfo":Ljava/lang/String; */
int v14 = 0; // const/4 v14, 0x0
/* .line 539 */
/* .local v14, "fromPid":I */
} // :goto_1
try { // :try_start_1
(( java.io.BufferedReader ) v13 ).readLine ( ); // invoke-virtual {v13}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object/from16 v16, v15 */
} // .end local v0 # "binderTransactionInfo":Ljava/lang/String;
/* .local v16, "binderTransactionInfo":Ljava/lang/String; */
if ( v15 != null) { // if-eqz v15, :cond_14
/* .line 540 */
final String v0 = "MIUI outgoing transaction"; // const-string v0, "MIUI outgoing transaction"
/* move-object/from16 v15, v16 */
} // .end local v16 # "binderTransactionInfo":Ljava/lang/String;
/* .local v15, "binderTransactionInfo":Ljava/lang/String; */
v0 = (( java.lang.String ) v15 ).startsWith ( v0 ); // invoke-virtual {v15, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_9 */
/* if-nez v0, :cond_6 */
try { // :try_start_2
final String v0 = "outgoing transaction"; // const-string v0, "outgoing transaction"
/* .line 541 */
v0 = (( java.lang.String ) v15 ).startsWith ( v0 ); // invoke-virtual {v15, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
/* if-nez v0, :cond_6 */
v0 = com.android.server.ScoutHelper.supportNewBinderLog;
/* .line 542 */
v0 = (( java.lang.Boolean ) v0 ).booleanValue ( ); // invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
if ( v0 != null) { // if-eqz v0, :cond_5
final String v0 = " outgoing transaction"; // const-string v0, " outgoing transaction"
v0 = (( java.lang.String ) v15 ).startsWith ( v0 ); // invoke-virtual {v15, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
if ( v0 != null) { // if-eqz v0, :cond_5
} // :cond_5
/* move/from16 v17, v6 */
/* goto/16 :goto_4 */
/* .line 536 */
} // .end local v14 # "fromPid":I
} // .end local v15 # "binderTransactionInfo":Ljava/lang/String;
/* :catchall_0 */
/* move-exception v0 */
/* move-object v1, v0 */
/* move/from16 v18, v8 */
/* move/from16 v28, v10 */
/* move/from16 v25, v11 */
/* move-object/from16 v21, v12 */
/* move-object/from16 v23, v13 */
/* goto/16 :goto_a */
/* .line 544 */
/* .restart local v14 # "fromPid":I */
/* .restart local v15 # "binderTransactionInfo":Ljava/lang/String; */
} // :cond_6
} // :goto_2
try { // :try_start_3
final String v0 = "\\bfrom((?:\\s+)?\\d+):((?:\\s+)?\\d+)\\s+to((?:\\s+)?\\d+):((?:\\s+)?\\d+)+.*code:((?:\\s+)?\\d+).*duration:((?:\\s+)?(\\d+(?:\\.\\d+)?))\\b"; // const-string v0, "\\bfrom((?:\\s+)?\\d+):((?:\\s+)?\\d+)\\s+to((?:\\s+)?\\d+):((?:\\s+)?\\d+)+.*code:((?:\\s+)?\\d+).*duration:((?:\\s+)?(\\d+(?:\\.\\d+)?))\\b"
/* .line 545 */
/* .local v0, "binderRegexPattern":Ljava/lang/String; */
v16 = com.android.server.ScoutHelper.supportNewBinderLog;
v16 = /* invoke-virtual/range {v16 ..v16}, Ljava/lang/Boolean;->booleanValue()Z */
/* :try_end_3 */
/* .catch Ljava/lang/Exception; {:try_start_3 ..:try_end_3} :catch_a */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_9 */
if ( v16 != null) { // if-eqz v16, :cond_7
try { // :try_start_4
final String v16 = "\\bfrom((?:\\s+)?\\d+):((?:\\s+)?\\d+)\\s+to((?:\\s+)?\\d+):((?:\\s+)?\\d+)+.*code((?:\\s+)?\\d+)+.*elapsed((?:\\s+)?\\d+)ms+.*"; // const-string v16, "\\bfrom((?:\\s+)?\\d+):((?:\\s+)?\\d+)\\s+to((?:\\s+)?\\d+):((?:\\s+)?\\d+)+.*code((?:\\s+)?\\d+)+.*elapsed((?:\\s+)?\\d+)ms+.*"
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_0 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* move-object/from16 v0, v16 */
/* .line 614 */
} // .end local v0 # "binderRegexPattern":Ljava/lang/String;
/* :catch_0 */
/* move-exception v0 */
/* move-object/from16 v30, v5 */
/* move/from16 v18, v8 */
/* move/from16 v28, v10 */
/* move/from16 v25, v11 */
/* move-object/from16 v21, v12 */
/* move-object/from16 v23, v13 */
/* move-object/from16 v32, v15 */
/* goto/16 :goto_9 */
/* .line 546 */
/* .restart local v0 # "binderRegexPattern":Ljava/lang/String; */
} // :cond_7
} // :goto_3
try { // :try_start_5
java.util.regex.Pattern .compile ( v0 );
/* move-object/from16 v17, v16 */
/* .line 547 */
/* .local v17, "pattern":Ljava/util/regex/Pattern; */
/* move-object/from16 v9, v17 */
} // .end local v17 # "pattern":Ljava/util/regex/Pattern;
/* .local v9, "pattern":Ljava/util/regex/Pattern; */
(( java.util.regex.Pattern ) v9 ).matcher ( v15 ); // invoke-virtual {v9, v15}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
/* move-object/from16 v18, v17 */
/* .line 548 */
/* .local v18, "matcher":Ljava/util/regex/Matcher; */
v17 = /* invoke-virtual/range {v18 ..v18}, Ljava/util/regex/Matcher;->find()Z */
/* :try_end_5 */
/* .catch Ljava/lang/Exception; {:try_start_5 ..:try_end_5} :catch_a */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_9 */
/* move-object/from16 v19, v0 */
} // .end local v0 # "binderRegexPattern":Ljava/lang/String;
/* .local v19, "binderRegexPattern":Ljava/lang/String; */
final String v0 = "("; // const-string v0, "("
/* if-nez v17, :cond_8 */
/* .line 549 */
/* move/from16 v17, v6 */
} // .end local v6 # "isBlockSystem":Z
/* .local v17, "isBlockSystem":Z */
try { // :try_start_6
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v15 ); // invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = ") regex match failed"; // const-string v6, ") regex match failed"
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v7,v0 );
/* :try_end_6 */
/* .catch Ljava/lang/Exception; {:try_start_6 ..:try_end_6} :catch_1 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_1 */
/* .line 550 */
/* nop */
/* .line 539 */
} // .end local v9 # "pattern":Ljava/util/regex/Pattern;
} // .end local v17 # "isBlockSystem":Z
} // .end local v18 # "matcher":Ljava/util/regex/Matcher;
} // .end local v19 # "binderRegexPattern":Ljava/lang/String;
/* .restart local v6 # "isBlockSystem":Z */
} // :goto_4
/* move-object v0, v15 */
/* move/from16 v6, v17 */
int v9 = 1; // const/4 v9, 0x1
} // .end local v6 # "isBlockSystem":Z
/* .restart local v17 # "isBlockSystem":Z */
/* goto/16 :goto_1 */
/* .line 536 */
} // .end local v14 # "fromPid":I
} // .end local v15 # "binderTransactionInfo":Ljava/lang/String;
/* :catchall_1 */
/* move-exception v0 */
/* move-object v1, v0 */
/* move/from16 v18, v8 */
/* move/from16 v28, v10 */
/* move/from16 v25, v11 */
/* move-object/from16 v21, v12 */
/* move-object/from16 v23, v13 */
/* move/from16 v6, v17 */
/* goto/16 :goto_a */
/* .line 614 */
/* .restart local v14 # "fromPid":I */
/* .restart local v15 # "binderTransactionInfo":Ljava/lang/String; */
/* :catch_1 */
/* move-exception v0 */
/* move-object/from16 v30, v5 */
/* move/from16 v18, v8 */
/* move/from16 v28, v10 */
/* move/from16 v25, v11 */
/* move-object/from16 v21, v12 */
/* move-object/from16 v23, v13 */
/* move-object/from16 v32, v15 */
/* move/from16 v6, v17 */
/* goto/16 :goto_9 */
/* .line 552 */
} // .end local v17 # "isBlockSystem":Z
/* .restart local v6 # "isBlockSystem":Z */
/* .restart local v9 # "pattern":Ljava/util/regex/Pattern; */
/* .restart local v18 # "matcher":Ljava/util/regex/Matcher; */
/* .restart local v19 # "binderRegexPattern":Ljava/lang/String; */
} // :cond_8
/* move/from16 v17, v6 */
} // .end local v6 # "isBlockSystem":Z
/* .restart local v17 # "isBlockSystem":Z */
/* move-object/from16 v6, v18 */
/* move/from16 v18, v8 */
int v8 = 1; // const/4 v8, 0x1
} // .end local v8 # "callType":I
/* .local v6, "matcher":Ljava/util/regex/Matcher; */
/* .local v18, "callType":I */
try { // :try_start_7
(( java.util.regex.Matcher ) v6 ).group ( v8 ); // invoke-virtual {v6, v8}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;
/* invoke-virtual/range {v20 ..v20}, Ljava/lang/String;->trim()Ljava/lang/String; */
v8 = java.lang.Integer .parseInt ( v8 );
/* move v14, v8 */
/* .line 553 */
int v8 = 2; // const/4 v8, 0x2
(( java.util.regex.Matcher ) v6 ).group ( v8 ); // invoke-virtual {v6, v8}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;
/* invoke-virtual/range {v20 ..v20}, Ljava/lang/String;->trim()Ljava/lang/String; */
v20 = /* invoke-static/range {v20 ..v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I */
/* move/from16 v21, v20 */
/* .line 554 */
/* .local v21, "fromTid":I */
int v8 = 3; // const/4 v8, 0x3
(( java.util.regex.Matcher ) v6 ).group ( v8 ); // invoke-virtual {v6, v8}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;
/* invoke-virtual/range {v22 ..v22}, Ljava/lang/String;->trim()Ljava/lang/String; */
v22 = /* invoke-static/range {v22 ..v22}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I */
/* move/from16 v23, v22 */
/* .line 555 */
/* .local v23, "toPid":I */
int v8 = 4; // const/4 v8, 0x4
(( java.util.regex.Matcher ) v6 ).group ( v8 ); // invoke-virtual {v6, v8}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;
(( java.lang.String ) v8 ).trim ( ); // invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;
v8 = java.lang.Integer .parseInt ( v8 );
/* .line 556 */
/* .local v8, "toTid":I */
/* move-object/from16 v24, v9 */
} // .end local v9 # "pattern":Ljava/util/regex/Pattern;
/* .local v24, "pattern":Ljava/util/regex/Pattern; */
int v9 = 5; // const/4 v9, 0x5
(( java.util.regex.Matcher ) v6 ).group ( v9 ); // invoke-virtual {v6, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;
(( java.lang.String ) v9 ).trim ( ); // invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;
v9 = java.lang.Integer .parseInt ( v9 );
/* .line 557 */
/* .local v9, "code":I */
/* const-wide/16 v25, 0x0 */
/* .line 558 */
/* .local v25, "waitTime":D */
v27 = com.android.server.ScoutHelper.supportNewBinderLog;
v27 = /* invoke-virtual/range {v27 ..v27}, Ljava/lang/Boolean;->booleanValue()Z */
/* :try_end_7 */
/* .catch Ljava/lang/Exception; {:try_start_7 ..:try_end_7} :catch_9 */
/* .catchall {:try_start_7 ..:try_end_7} :catchall_7 */
/* move/from16 v28, v10 */
} // .end local v10 # "isCheckAppDThread":Z
/* .local v28, "isCheckAppDThread":Z */
int v10 = 6; // const/4 v10, 0x6
if ( v27 != null) { // if-eqz v27, :cond_9
/* .line 559 */
try { // :try_start_8
(( java.util.regex.Matcher ) v6 ).group ( v10 ); // invoke-virtual {v6, v10}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;
(( java.lang.String ) v10 ).trim ( ); // invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;
java.lang.Double .valueOf ( v10 );
/* .line 560 */
/* .local v10, "elapsed":Ljava/lang/Double; */
(( java.lang.Double ) v10 ).doubleValue ( ); // invoke-virtual {v10}, Ljava/lang/Double;->doubleValue()D
/* move-result-wide v29 */
/* :try_end_8 */
/* .catch Ljava/lang/Exception; {:try_start_8 ..:try_end_8} :catch_2 */
/* .catchall {:try_start_8 ..:try_end_8} :catchall_2 */
/* const-wide v31, 0x408f400000000000L # 1000.0 */
/* div-double v29, v29, v31 */
/* .line 561 */
} // .end local v10 # "elapsed":Ljava/lang/Double;
} // .end local v25 # "waitTime":D
/* .local v29, "waitTime":D */
/* move/from16 v25, v11 */
/* move-wide/from16 v10, v29 */
/* .line 536 */
} // .end local v6 # "matcher":Ljava/util/regex/Matcher;
} // .end local v8 # "toTid":I
} // .end local v9 # "code":I
} // .end local v14 # "fromPid":I
} // .end local v15 # "binderTransactionInfo":Ljava/lang/String;
} // .end local v19 # "binderRegexPattern":Ljava/lang/String;
} // .end local v21 # "fromTid":I
} // .end local v23 # "toPid":I
} // .end local v24 # "pattern":Ljava/util/regex/Pattern;
} // .end local v29 # "waitTime":D
/* :catchall_2 */
/* move-exception v0 */
/* move-object v1, v0 */
/* move/from16 v25, v11 */
/* move-object/from16 v21, v12 */
/* move-object/from16 v23, v13 */
/* move/from16 v6, v17 */
/* goto/16 :goto_a */
/* .line 614 */
/* .restart local v14 # "fromPid":I */
/* .restart local v15 # "binderTransactionInfo":Ljava/lang/String; */
/* :catch_2 */
/* move-exception v0 */
/* move-object/from16 v30, v5 */
/* move/from16 v25, v11 */
/* move-object/from16 v21, v12 */
/* move-object/from16 v23, v13 */
/* move-object/from16 v32, v15 */
/* move/from16 v6, v17 */
/* goto/16 :goto_9 */
/* .line 562 */
/* .restart local v6 # "matcher":Ljava/util/regex/Matcher; */
/* .restart local v8 # "toTid":I */
/* .restart local v9 # "code":I */
/* .restart local v19 # "binderRegexPattern":Ljava/lang/String; */
/* .restart local v21 # "fromTid":I */
/* .restart local v23 # "toPid":I */
/* .restart local v24 # "pattern":Ljava/util/regex/Pattern; */
/* .restart local v25 # "waitTime":D */
} // :cond_9
try { // :try_start_9
(( java.util.regex.Matcher ) v6 ).group ( v10 ); // invoke-virtual {v6, v10}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;
(( java.lang.String ) v10 ).trim ( ); // invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;
java.lang.Double .valueOf ( v10 );
(( java.lang.Double ) v10 ).doubleValue ( ); // invoke-virtual {v10}, Ljava/lang/Double;->doubleValue()D
/* move-result-wide v29 */
/* :try_end_9 */
/* .catch Ljava/lang/Exception; {:try_start_9 ..:try_end_9} :catch_8 */
/* .catchall {:try_start_9 ..:try_end_9} :catchall_6 */
} // .end local v25 # "waitTime":D
/* .restart local v29 # "waitTime":D */
/* move/from16 v25, v11 */
/* move-wide/from16 v10, v29 */
/* .line 564 */
} // .end local v11 # "isCheckSystemDThread":Z
} // .end local v29 # "waitTime":D
/* .local v10, "waitTime":D */
/* .local v25, "isCheckSystemDThread":Z */
} // :goto_5
try { // :try_start_a
com.android.server.ScoutHelper .getProcessCmdline ( v14 );
/* move-object/from16 v27, v26 */
/* .line 565 */
/* .local v27, "fromProcessName":Ljava/lang/String; */
/* move-object/from16 v26, v6 */
/* move/from16 v6, v21 */
} // .end local v21 # "fromTid":I
/* .local v6, "fromTid":I */
/* .local v26, "matcher":Ljava/util/regex/Matcher; */
com.android.server.ScoutHelper .getProcessComm ( v14,v6 );
/* move-object/from16 v29, v21 */
/* .line 566 */
/* .local v29, "fromThreadName":Ljava/lang/String; */
/* invoke-static/range {v23 ..v23}, Lcom/android/server/ScoutHelper;->getProcessCmdline(I)Ljava/lang/String; */
/* :try_end_a */
/* .catch Ljava/lang/Exception; {:try_start_a ..:try_end_a} :catch_7 */
/* .catchall {:try_start_a ..:try_end_a} :catchall_5 */
/* move-object/from16 v30, v21 */
/* .line 567 */
/* .local v30, "toProcess":Ljava/lang/String; */
/* move-object/from16 v21, v12 */
/* move/from16 v12, v23 */
} // .end local v23 # "toPid":I
/* .local v12, "toPid":I */
/* .local v21, "fileBinderReader":Ljava/io/File; */
try { // :try_start_b
com.android.server.ScoutHelper .getProcessComm ( v12,v8 );
/* :try_end_b */
/* .catch Ljava/lang/Exception; {:try_start_b ..:try_end_b} :catch_6 */
/* .catchall {:try_start_b ..:try_end_b} :catchall_4 */
/* move-object/from16 v31, v23 */
/* .line 568 */
/* .local v31, "toThreadName":Ljava/lang/String; */
/* move-object/from16 v23, v13 */
} // .end local v13 # "BinderReader":Ljava/io/BufferedReader;
/* .local v23, "BinderReader":Ljava/io/BufferedReader; */
try { // :try_start_c
/* new-instance v13, Ljava/lang/StringBuilder; */
/* invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V */
/* :try_end_c */
/* .catch Ljava/lang/Exception; {:try_start_c ..:try_end_c} :catch_5 */
/* .catchall {:try_start_c ..:try_end_c} :catchall_3 */
/* move-object/from16 v32, v15 */
} // .end local v15 # "binderTransactionInfo":Ljava/lang/String;
/* .local v32, "binderTransactionInfo":Ljava/lang/String; */
try { // :try_start_d
final String v15 = "outgoing transaction from "; // const-string v15, "outgoing transaction from "
(( java.lang.StringBuilder ) v13 ).append ( v15 ); // invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).append ( v14 ); // invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).append ( v0 ); // invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-object/from16 v15, v27 */
} // .end local v27 # "fromProcessName":Ljava/lang/String;
/* .local v15, "fromProcessName":Ljava/lang/String; */
(( java.lang.StringBuilder ) v13 ).append ( v15 ); // invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).append ( v5 ); // invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).append ( v6 ); // invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).append ( v0 ); // invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-object/from16 v27, v15 */
/* move-object/from16 v15, v29 */
} // .end local v29 # "fromThreadName":Ljava/lang/String;
/* .local v15, "fromThreadName":Ljava/lang/String; */
/* .restart local v27 # "fromProcessName":Ljava/lang/String; */
(( java.lang.StringBuilder ) v13 ).append ( v15 ); // invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-object/from16 v29, v15 */
} // .end local v15 # "fromThreadName":Ljava/lang/String;
/* .restart local v29 # "fromThreadName":Ljava/lang/String; */
final String v15 = ") to "; // const-string v15, ") to "
(( java.lang.StringBuilder ) v13 ).append ( v15 ); // invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).append ( v12 ); // invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).append ( v0 ); // invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-object/from16 v15, v30 */
} // .end local v30 # "toProcess":Ljava/lang/String;
/* .local v15, "toProcess":Ljava/lang/String; */
(( java.lang.StringBuilder ) v13 ).append ( v15 ); // invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).append ( v5 ); // invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).append ( v8 ); // invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).append ( v0 ); // invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* :try_end_d */
/* .catch Ljava/lang/Exception; {:try_start_d ..:try_end_d} :catch_4 */
/* .catchall {:try_start_d ..:try_end_d} :catchall_3 */
/* move-object/from16 v30, v5 */
/* move-object/from16 v5, v31 */
} // .end local v31 # "toThreadName":Ljava/lang/String;
/* .local v5, "toThreadName":Ljava/lang/String; */
try { // :try_start_e
(( java.lang.StringBuilder ) v13 ).append ( v5 ); // invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-object/from16 v31, v5 */
} // .end local v5 # "toThreadName":Ljava/lang/String;
/* .restart local v31 # "toThreadName":Ljava/lang/String; */
final String v5 = ") elapsed: "; // const-string v5, ") elapsed: "
(( java.lang.StringBuilder ) v13 ).append ( v5 ); // invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v10, v11 ); // invoke-virtual {v5, v10, v11}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
final String v13 = " s code: "; // const-string v13, " s code: "
(( java.lang.StringBuilder ) v5 ).append ( v13 ); // invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v9 ); // invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 575 */
/* .local v5, "binderLog":Ljava/lang/String; */
/* if-ne v14, v1, :cond_12 */
/* if-lez v12, :cond_12 */
/* if-eq v12, v1, :cond_12 */
/* .line 576 */
/* new-instance v13, Ljava/lang/StringBuilder; */
/* invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Binder Info:"; // const-string v1, "Binder Info:"
(( java.lang.StringBuilder ) v13 ).append ( v1 ); // invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v7,v1 );
/* .line 577 */
(( com.android.server.ScoutHelper$ScoutBinderInfo ) v2 ).addBinderTransInfo ( v5 ); // invoke-virtual {v2, v5}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->addBinderTransInfo(Ljava/lang/String;)V
/* .line 578 */
v1 = com.android.server.ScoutHelper .getOomAdjOfPid ( v7,v12 );
/* .line 579 */
/* .local v1, "adj":I */
v13 = com.android.server.ScoutHelper .checkIsJavaOrNativeProcess ( v1 );
/* .line 580 */
/* .local v13, "isJavaOrNativeProcess":I */
/* if-nez v13, :cond_a */
/* move/from16 v1, p0 */
/* move/from16 v6, v17 */
/* move/from16 v8, v18 */
/* move-object/from16 v12, v21 */
/* move-object/from16 v13, v23 */
/* move/from16 v11, v25 */
/* move/from16 v10, v28 */
/* move-object/from16 v5, v30 */
/* move-object/from16 v0, v32 */
int v9 = 1; // const/4 v9, 0x1
/* goto/16 :goto_1 */
/* .line 581 */
} // :cond_a
/* const-wide/high16 v33, 0x4000000000000000L # 2.0 */
/* move-object/from16 v35, v5 */
int v5 = 1; // const/4 v5, 0x1
} // .end local v5 # "binderLog":Ljava/lang/String;
/* .local v35, "binderLog":Ljava/lang/String; */
/* if-ne v13, v5, :cond_c */
/* .line 582 */
java.lang.Integer .valueOf ( v12 );
v5 = (( java.util.ArrayList ) v3 ).contains ( v5 ); // invoke-virtual {v3, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* if-nez v5, :cond_c */
/* .line 583 */
/* const/16 v0, -0x384 */
/* if-ne v1, v0, :cond_b */
/* if-ne v14, v6, :cond_b */
/* cmpl-double v0, v10, v33 */
/* if-lez v0, :cond_b */
/* .line 585 */
int v0 = 1; // const/4 v0, 0x1
/* move/from16 v17, v0 */
/* .line 587 */
} // :cond_b
java.lang.Integer .valueOf ( v12 );
(( java.util.ArrayList ) v3 ).add ( v0 ); // invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 588 */
com.android.server.ScoutHelper .checkBinderCallPidList ( v12,v2,v3,v4 );
/* :try_end_e */
/* .catch Ljava/lang/Exception; {:try_start_e ..:try_end_e} :catch_3 */
/* .catchall {:try_start_e ..:try_end_e} :catchall_3 */
/* move/from16 v6, v17 */
/* goto/16 :goto_8 */
/* .line 589 */
} // :cond_c
final String v5 = ")"; // const-string v5, ")"
/* move/from16 v36, v1 */
} // .end local v1 # "adj":I
/* .local v36, "adj":I */
final String v1 = "check blocked in monkey, will kill pid: "; // const-string v1, "check blocked in monkey, will kill pid: "
/* move/from16 v37, v8 */
int v8 = 2; // const/4 v8, 0x2
} // .end local v8 # "toTid":I
/* .local v37, "toTid":I */
/* if-ne v13, v8, :cond_f */
/* .line 590 */
try { // :try_start_f
java.lang.Integer .valueOf ( v12 );
v8 = (( java.util.ArrayList ) v4 ).contains ( v8 ); // invoke-virtual {v4, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* if-nez v8, :cond_f */
/* .line 591 */
/* if-ne v14, v6, :cond_d */
/* cmpl-double v8, v10, v33 */
/* if-lez v8, :cond_d */
/* .line 592 */
int v8 = 1; // const/4 v8, 0x1
/* move/from16 v17, v8 */
/* .line 594 */
} // :cond_d
v8 = android.os.Process .myPid ( );
/* if-ne v8, v14, :cond_e */
v8 = com.android.server.ScoutHelper .checkIsMonkey ( v15,v10,v11 );
if ( v8 != null) { // if-eqz v8, :cond_e
/* .line 595 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v8 ).append ( v1 ); // invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v12 ); // invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v15 ); // invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v7,v0 );
/* .line 596 */
(( com.android.server.ScoutHelper$ScoutBinderInfo ) v2 ).setMonkeyPid ( v12 ); // invoke-virtual {v2, v12}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->setMonkeyPid(I)V
/* .line 598 */
} // :cond_e
java.lang.Integer .valueOf ( v12 );
(( java.util.ArrayList ) v4 ).add ( v0 ); // invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 599 */
com.android.server.ScoutHelper .checkBinderCallPidList ( v12,v2,v3,v4 );
/* move/from16 v6, v17 */
/* .line 600 */
} // :cond_f
int v8 = 3; // const/4 v8, 0x3
/* if-ne v13, v8, :cond_13 */
/* .line 601 */
java.lang.Integer .valueOf ( v12 );
v8 = (( java.util.ArrayList ) v3 ).contains ( v8 ); // invoke-virtual {v3, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* if-nez v8, :cond_13 */
java.lang.Integer .valueOf ( v12 );
v8 = (( java.util.ArrayList ) v4 ).contains ( v8 ); // invoke-virtual {v4, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* if-nez v8, :cond_13 */
/* .line 602 */
v8 = android.os.Process .myPid ( );
/* if-ne v8, v14, :cond_10 */
v8 = com.android.server.ScoutHelper .checkIsMonkey ( v15,v10,v11 );
if ( v8 != null) { // if-eqz v8, :cond_10
/* .line 603 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v8 ).append ( v1 ); // invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v12 ); // invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v15 ); // invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v7,v0 );
/* .line 604 */
(( com.android.server.ScoutHelper$ScoutBinderInfo ) v2 ).setMonkeyPid ( v12 ); // invoke-virtual {v2, v12}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->setMonkeyPid(I)V
/* .line 606 */
} // :cond_10
v0 = v0 = com.android.server.ScoutHelper.JAVA_SHELL_PROCESS;
if ( v0 != null) { // if-eqz v0, :cond_11
/* .line 607 */
java.lang.Integer .valueOf ( v12 );
(( java.util.ArrayList ) v3 ).add ( v0 ); // invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 609 */
} // :cond_11
java.lang.Integer .valueOf ( v12 );
(( java.util.ArrayList ) v4 ).add ( v0 ); // invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 611 */
} // :goto_6
com.android.server.ScoutHelper .checkBinderCallPidList ( v12,v2,v3,v4 );
/* :try_end_f */
/* .catch Ljava/lang/Exception; {:try_start_f ..:try_end_f} :catch_3 */
/* .catchall {:try_start_f ..:try_end_f} :catchall_3 */
/* .line 575 */
} // .end local v13 # "isJavaOrNativeProcess":I
} // .end local v35 # "binderLog":Ljava/lang/String;
} // .end local v36 # "adj":I
} // .end local v37 # "toTid":I
/* .restart local v5 # "binderLog":Ljava/lang/String; */
/* .restart local v8 # "toTid":I */
} // :cond_12
/* move-object/from16 v35, v5 */
/* move/from16 v37, v8 */
/* .line 616 */
} // .end local v5 # "binderLog":Ljava/lang/String;
} // .end local v6 # "fromTid":I
} // .end local v8 # "toTid":I
} // .end local v9 # "code":I
} // .end local v10 # "waitTime":D
} // .end local v12 # "toPid":I
} // .end local v15 # "toProcess":Ljava/lang/String;
} // .end local v19 # "binderRegexPattern":Ljava/lang/String;
} // .end local v24 # "pattern":Ljava/util/regex/Pattern;
} // .end local v26 # "matcher":Ljava/util/regex/Matcher;
} // .end local v27 # "fromProcessName":Ljava/lang/String;
} // .end local v29 # "fromThreadName":Ljava/lang/String;
} // .end local v31 # "toThreadName":Ljava/lang/String;
} // :cond_13
} // :goto_7
/* move/from16 v6, v17 */
} // .end local v17 # "isBlockSystem":Z
/* .local v6, "isBlockSystem":Z */
} // :goto_8
/* move/from16 v1, p0 */
/* move/from16 v8, v18 */
/* move-object/from16 v12, v21 */
/* move-object/from16 v13, v23 */
/* move/from16 v11, v25 */
/* move/from16 v10, v28 */
/* move-object/from16 v5, v30 */
/* move-object/from16 v0, v32 */
int v9 = 1; // const/4 v9, 0x1
/* goto/16 :goto_1 */
/* .line 614 */
} // .end local v6 # "isBlockSystem":Z
/* .restart local v17 # "isBlockSystem":Z */
/* :catch_3 */
/* move-exception v0 */
/* move/from16 v6, v17 */
/* goto/16 :goto_9 */
/* :catch_4 */
/* move-exception v0 */
/* move-object/from16 v30, v5 */
/* move/from16 v6, v17 */
/* goto/16 :goto_9 */
/* .line 536 */
} // .end local v14 # "fromPid":I
} // .end local v32 # "binderTransactionInfo":Ljava/lang/String;
/* :catchall_3 */
/* move-exception v0 */
/* move-object v1, v0 */
/* move/from16 v6, v17 */
/* goto/16 :goto_a */
/* .line 614 */
/* .restart local v14 # "fromPid":I */
/* .local v15, "binderTransactionInfo":Ljava/lang/String; */
/* :catch_5 */
/* move-exception v0 */
/* move-object/from16 v30, v5 */
/* move-object/from16 v32, v15 */
/* move/from16 v6, v17 */
} // .end local v15 # "binderTransactionInfo":Ljava/lang/String;
/* .restart local v32 # "binderTransactionInfo":Ljava/lang/String; */
/* goto/16 :goto_9 */
/* .line 536 */
} // .end local v14 # "fromPid":I
} // .end local v23 # "BinderReader":Ljava/io/BufferedReader;
} // .end local v32 # "binderTransactionInfo":Ljava/lang/String;
/* .local v13, "BinderReader":Ljava/io/BufferedReader; */
/* :catchall_4 */
/* move-exception v0 */
/* move-object/from16 v23, v13 */
/* move-object v1, v0 */
/* move/from16 v6, v17 */
} // .end local v13 # "BinderReader":Ljava/io/BufferedReader;
/* .restart local v23 # "BinderReader":Ljava/io/BufferedReader; */
/* goto/16 :goto_a */
/* .line 614 */
} // .end local v23 # "BinderReader":Ljava/io/BufferedReader;
/* .restart local v13 # "BinderReader":Ljava/io/BufferedReader; */
/* .restart local v14 # "fromPid":I */
/* .restart local v15 # "binderTransactionInfo":Ljava/lang/String; */
/* :catch_6 */
/* move-exception v0 */
/* move-object/from16 v30, v5 */
/* move-object/from16 v23, v13 */
/* move-object/from16 v32, v15 */
/* move/from16 v6, v17 */
} // .end local v13 # "BinderReader":Ljava/io/BufferedReader;
} // .end local v15 # "binderTransactionInfo":Ljava/lang/String;
/* .restart local v23 # "BinderReader":Ljava/io/BufferedReader; */
/* .restart local v32 # "binderTransactionInfo":Ljava/lang/String; */
/* goto/16 :goto_9 */
/* .line 536 */
} // .end local v14 # "fromPid":I
} // .end local v21 # "fileBinderReader":Ljava/io/File;
} // .end local v23 # "BinderReader":Ljava/io/BufferedReader;
} // .end local v32 # "binderTransactionInfo":Ljava/lang/String;
/* .local v12, "fileBinderReader":Ljava/io/File; */
/* .restart local v13 # "BinderReader":Ljava/io/BufferedReader; */
/* :catchall_5 */
/* move-exception v0 */
/* move-object/from16 v21, v12 */
/* move-object/from16 v23, v13 */
/* move-object v1, v0 */
/* move/from16 v6, v17 */
} // .end local v12 # "fileBinderReader":Ljava/io/File;
} // .end local v13 # "BinderReader":Ljava/io/BufferedReader;
/* .restart local v21 # "fileBinderReader":Ljava/io/File; */
/* .restart local v23 # "BinderReader":Ljava/io/BufferedReader; */
/* goto/16 :goto_a */
/* .line 614 */
} // .end local v21 # "fileBinderReader":Ljava/io/File;
} // .end local v23 # "BinderReader":Ljava/io/BufferedReader;
/* .restart local v12 # "fileBinderReader":Ljava/io/File; */
/* .restart local v13 # "BinderReader":Ljava/io/BufferedReader; */
/* .restart local v14 # "fromPid":I */
/* .restart local v15 # "binderTransactionInfo":Ljava/lang/String; */
/* :catch_7 */
/* move-exception v0 */
/* move-object/from16 v30, v5 */
/* move-object/from16 v21, v12 */
/* move-object/from16 v23, v13 */
/* move-object/from16 v32, v15 */
/* move/from16 v6, v17 */
} // .end local v12 # "fileBinderReader":Ljava/io/File;
} // .end local v13 # "BinderReader":Ljava/io/BufferedReader;
} // .end local v15 # "binderTransactionInfo":Ljava/lang/String;
/* .restart local v21 # "fileBinderReader":Ljava/io/File; */
/* .restart local v23 # "BinderReader":Ljava/io/BufferedReader; */
/* .restart local v32 # "binderTransactionInfo":Ljava/lang/String; */
/* .line 536 */
} // .end local v14 # "fromPid":I
} // .end local v21 # "fileBinderReader":Ljava/io/File;
} // .end local v23 # "BinderReader":Ljava/io/BufferedReader;
} // .end local v25 # "isCheckSystemDThread":Z
} // .end local v32 # "binderTransactionInfo":Ljava/lang/String;
/* .restart local v11 # "isCheckSystemDThread":Z */
/* .restart local v12 # "fileBinderReader":Ljava/io/File; */
/* .restart local v13 # "BinderReader":Ljava/io/BufferedReader; */
/* :catchall_6 */
/* move-exception v0 */
/* move/from16 v25, v11 */
/* move-object/from16 v21, v12 */
/* move-object/from16 v23, v13 */
/* move-object v1, v0 */
/* move/from16 v6, v17 */
} // .end local v11 # "isCheckSystemDThread":Z
} // .end local v12 # "fileBinderReader":Ljava/io/File;
} // .end local v13 # "BinderReader":Ljava/io/BufferedReader;
/* .restart local v21 # "fileBinderReader":Ljava/io/File; */
/* .restart local v23 # "BinderReader":Ljava/io/BufferedReader; */
/* .restart local v25 # "isCheckSystemDThread":Z */
/* goto/16 :goto_a */
/* .line 614 */
} // .end local v21 # "fileBinderReader":Ljava/io/File;
} // .end local v23 # "BinderReader":Ljava/io/BufferedReader;
} // .end local v25 # "isCheckSystemDThread":Z
/* .restart local v11 # "isCheckSystemDThread":Z */
/* .restart local v12 # "fileBinderReader":Ljava/io/File; */
/* .restart local v13 # "BinderReader":Ljava/io/BufferedReader; */
/* .restart local v14 # "fromPid":I */
/* .restart local v15 # "binderTransactionInfo":Ljava/lang/String; */
/* :catch_8 */
/* move-exception v0 */
/* move-object/from16 v30, v5 */
/* move/from16 v25, v11 */
/* move-object/from16 v21, v12 */
/* move-object/from16 v23, v13 */
/* move-object/from16 v32, v15 */
/* move/from16 v6, v17 */
} // .end local v11 # "isCheckSystemDThread":Z
} // .end local v12 # "fileBinderReader":Ljava/io/File;
} // .end local v13 # "BinderReader":Ljava/io/BufferedReader;
} // .end local v15 # "binderTransactionInfo":Ljava/lang/String;
/* .restart local v21 # "fileBinderReader":Ljava/io/File; */
/* .restart local v23 # "BinderReader":Ljava/io/BufferedReader; */
/* .restart local v25 # "isCheckSystemDThread":Z */
/* .restart local v32 # "binderTransactionInfo":Ljava/lang/String; */
/* .line 536 */
} // .end local v14 # "fromPid":I
} // .end local v21 # "fileBinderReader":Ljava/io/File;
} // .end local v23 # "BinderReader":Ljava/io/BufferedReader;
} // .end local v25 # "isCheckSystemDThread":Z
} // .end local v28 # "isCheckAppDThread":Z
} // .end local v32 # "binderTransactionInfo":Ljava/lang/String;
/* .local v10, "isCheckAppDThread":Z */
/* .restart local v11 # "isCheckSystemDThread":Z */
/* .restart local v12 # "fileBinderReader":Ljava/io/File; */
/* .restart local v13 # "BinderReader":Ljava/io/BufferedReader; */
/* :catchall_7 */
/* move-exception v0 */
/* move/from16 v28, v10 */
/* move/from16 v25, v11 */
/* move-object/from16 v21, v12 */
/* move-object/from16 v23, v13 */
/* move-object v1, v0 */
/* move/from16 v6, v17 */
} // .end local v10 # "isCheckAppDThread":Z
} // .end local v11 # "isCheckSystemDThread":Z
} // .end local v12 # "fileBinderReader":Ljava/io/File;
} // .end local v13 # "BinderReader":Ljava/io/BufferedReader;
/* .restart local v21 # "fileBinderReader":Ljava/io/File; */
/* .restart local v23 # "BinderReader":Ljava/io/BufferedReader; */
/* .restart local v25 # "isCheckSystemDThread":Z */
/* .restart local v28 # "isCheckAppDThread":Z */
/* goto/16 :goto_a */
/* .line 614 */
} // .end local v21 # "fileBinderReader":Ljava/io/File;
} // .end local v23 # "BinderReader":Ljava/io/BufferedReader;
} // .end local v25 # "isCheckSystemDThread":Z
} // .end local v28 # "isCheckAppDThread":Z
/* .restart local v10 # "isCheckAppDThread":Z */
/* .restart local v11 # "isCheckSystemDThread":Z */
/* .restart local v12 # "fileBinderReader":Ljava/io/File; */
/* .restart local v13 # "BinderReader":Ljava/io/BufferedReader; */
/* .restart local v14 # "fromPid":I */
/* .restart local v15 # "binderTransactionInfo":Ljava/lang/String; */
/* :catch_9 */
/* move-exception v0 */
/* move-object/from16 v30, v5 */
/* move/from16 v28, v10 */
/* move/from16 v25, v11 */
/* move-object/from16 v21, v12 */
/* move-object/from16 v23, v13 */
/* move-object/from16 v32, v15 */
/* move/from16 v6, v17 */
} // .end local v10 # "isCheckAppDThread":Z
} // .end local v11 # "isCheckSystemDThread":Z
} // .end local v12 # "fileBinderReader":Ljava/io/File;
} // .end local v13 # "BinderReader":Ljava/io/BufferedReader;
} // .end local v15 # "binderTransactionInfo":Ljava/lang/String;
/* .restart local v21 # "fileBinderReader":Ljava/io/File; */
/* .restart local v23 # "BinderReader":Ljava/io/BufferedReader; */
/* .restart local v25 # "isCheckSystemDThread":Z */
/* .restart local v28 # "isCheckAppDThread":Z */
/* .restart local v32 # "binderTransactionInfo":Ljava/lang/String; */
} // .end local v17 # "isBlockSystem":Z
} // .end local v18 # "callType":I
} // .end local v21 # "fileBinderReader":Ljava/io/File;
} // .end local v23 # "BinderReader":Ljava/io/BufferedReader;
} // .end local v25 # "isCheckSystemDThread":Z
} // .end local v28 # "isCheckAppDThread":Z
} // .end local v32 # "binderTransactionInfo":Ljava/lang/String;
/* .restart local v6 # "isBlockSystem":Z */
/* .local v8, "callType":I */
/* .restart local v10 # "isCheckAppDThread":Z */
/* .restart local v11 # "isCheckSystemDThread":Z */
/* .restart local v12 # "fileBinderReader":Ljava/io/File; */
/* .restart local v13 # "BinderReader":Ljava/io/BufferedReader; */
/* .restart local v15 # "binderTransactionInfo":Ljava/lang/String; */
/* :catch_a */
/* move-exception v0 */
/* move-object/from16 v30, v5 */
/* move/from16 v17, v6 */
/* move/from16 v18, v8 */
/* move/from16 v28, v10 */
/* move/from16 v25, v11 */
/* move-object/from16 v21, v12 */
/* move-object/from16 v23, v13 */
/* move-object/from16 v32, v15 */
/* .line 615 */
} // .end local v8 # "callType":I
} // .end local v10 # "isCheckAppDThread":Z
} // .end local v11 # "isCheckSystemDThread":Z
} // .end local v12 # "fileBinderReader":Ljava/io/File;
} // .end local v13 # "BinderReader":Ljava/io/BufferedReader;
} // .end local v15 # "binderTransactionInfo":Ljava/lang/String;
/* .local v0, "e":Ljava/lang/Exception; */
/* .restart local v18 # "callType":I */
/* .restart local v21 # "fileBinderReader":Ljava/io/File; */
/* .restart local v23 # "BinderReader":Ljava/io/BufferedReader; */
/* .restart local v25 # "isCheckSystemDThread":Z */
/* .restart local v28 # "isCheckAppDThread":Z */
/* .restart local v32 # "binderTransactionInfo":Ljava/lang/String; */
} // :goto_9
try { // :try_start_10
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "regular match error, String:"; // const-string v5, "regular match error, String:"
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-object/from16 v5, v32 */
} // .end local v32 # "binderTransactionInfo":Ljava/lang/String;
/* .local v5, "binderTransactionInfo":Ljava/lang/String; */
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v7,v1 );
/* :try_end_10 */
/* .catchall {:try_start_10 ..:try_end_10} :catchall_8 */
/* .line 616 */
/* move/from16 v1, p0 */
/* move-object v0, v5 */
/* move/from16 v8, v18 */
/* move-object/from16 v12, v21 */
/* move-object/from16 v13, v23 */
/* move/from16 v11, v25 */
/* move/from16 v10, v28 */
/* move-object/from16 v5, v30 */
int v9 = 1; // const/4 v9, 0x1
} // .end local v0 # "e":Ljava/lang/Exception;
/* goto/16 :goto_1 */
/* .line 536 */
} // .end local v5 # "binderTransactionInfo":Ljava/lang/String;
} // .end local v14 # "fromPid":I
/* :catchall_8 */
/* move-exception v0 */
/* move-object v1, v0 */
/* .line 539 */
} // .end local v18 # "callType":I
} // .end local v21 # "fileBinderReader":Ljava/io/File;
} // .end local v23 # "BinderReader":Ljava/io/BufferedReader;
} // .end local v25 # "isCheckSystemDThread":Z
} // .end local v28 # "isCheckAppDThread":Z
/* .restart local v8 # "callType":I */
/* .restart local v10 # "isCheckAppDThread":Z */
/* .restart local v11 # "isCheckSystemDThread":Z */
/* .restart local v12 # "fileBinderReader":Ljava/io/File; */
/* .restart local v13 # "BinderReader":Ljava/io/BufferedReader; */
/* .restart local v14 # "fromPid":I */
/* .restart local v16 # "binderTransactionInfo":Ljava/lang/String; */
} // :cond_14
/* move/from16 v17, v6 */
/* move/from16 v18, v8 */
/* move/from16 v28, v10 */
/* move/from16 v25, v11 */
/* move-object/from16 v21, v12 */
/* move-object/from16 v23, v13 */
/* move-object/from16 v5, v16 */
/* .line 619 */
} // .end local v6 # "isBlockSystem":Z
} // .end local v8 # "callType":I
} // .end local v10 # "isCheckAppDThread":Z
} // .end local v11 # "isCheckSystemDThread":Z
} // .end local v12 # "fileBinderReader":Ljava/io/File;
} // .end local v13 # "BinderReader":Ljava/io/BufferedReader;
} // .end local v14 # "fromPid":I
} // .end local v16 # "binderTransactionInfo":Ljava/lang/String;
/* .restart local v17 # "isBlockSystem":Z */
/* .restart local v18 # "callType":I */
/* .restart local v21 # "fileBinderReader":Ljava/io/File; */
/* .restart local v23 # "BinderReader":Ljava/io/BufferedReader; */
/* .restart local v25 # "isCheckSystemDThread":Z */
/* .restart local v28 # "isCheckAppDThread":Z */
try { // :try_start_11
/* invoke-virtual/range {v23 ..v23}, Ljava/io/BufferedReader;->close()V */
/* :try_end_11 */
/* .catch Ljava/lang/Exception; {:try_start_11 ..:try_end_11} :catch_b */
/* .line 622 */
} // .end local v23 # "BinderReader":Ljava/io/BufferedReader;
/* move/from16 v6, v17 */
/* .line 619 */
/* :catch_b */
/* move-exception v0 */
/* move/from16 v6, v17 */
/* .line 536 */
} // .end local v17 # "isBlockSystem":Z
} // .end local v18 # "callType":I
} // .end local v21 # "fileBinderReader":Ljava/io/File;
} // .end local v25 # "isCheckSystemDThread":Z
} // .end local v28 # "isCheckAppDThread":Z
/* .restart local v6 # "isBlockSystem":Z */
/* .restart local v8 # "callType":I */
/* .restart local v10 # "isCheckAppDThread":Z */
/* .restart local v11 # "isCheckSystemDThread":Z */
/* .restart local v12 # "fileBinderReader":Ljava/io/File; */
/* .restart local v13 # "BinderReader":Ljava/io/BufferedReader; */
/* :catchall_9 */
/* move-exception v0 */
/* move/from16 v17, v6 */
/* move/from16 v18, v8 */
/* move/from16 v28, v10 */
/* move/from16 v25, v11 */
/* move-object/from16 v21, v12 */
/* move-object/from16 v23, v13 */
/* move-object v1, v0 */
} // .end local v8 # "callType":I
} // .end local v10 # "isCheckAppDThread":Z
} // .end local v11 # "isCheckSystemDThread":Z
} // .end local v12 # "fileBinderReader":Ljava/io/File;
} // .end local v13 # "BinderReader":Ljava/io/BufferedReader;
/* .restart local v18 # "callType":I */
/* .restart local v21 # "fileBinderReader":Ljava/io/File; */
/* .restart local v23 # "BinderReader":Ljava/io/BufferedReader; */
/* .restart local v25 # "isCheckSystemDThread":Z */
/* .restart local v28 # "isCheckAppDThread":Z */
} // :goto_a
try { // :try_start_12
/* invoke-virtual/range {v23 ..v23}, Ljava/io/BufferedReader;->close()V */
/* :try_end_12 */
/* .catchall {:try_start_12 ..:try_end_12} :catchall_a */
/* :catchall_a */
/* move-exception v0 */
/* move-object v5, v0 */
try { // :try_start_13
(( java.lang.Throwable ) v1 ).addSuppressed ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v6 # "isBlockSystem":Z
} // .end local v7 # "tag":Ljava/lang/String;
} // .end local v18 # "callType":I
} // .end local v21 # "fileBinderReader":Ljava/io/File;
} // .end local v25 # "isCheckSystemDThread":Z
} // .end local v28 # "isCheckAppDThread":Z
} // .end local p0 # "Pid":I
} // .end local p1 # "info":Lcom/android/server/ScoutHelper$ScoutBinderInfo;
} // .end local p2 # "javaProcess":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
} // .end local p3 # "nativeProcess":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
} // :goto_b
/* throw v1 */
/* :try_end_13 */
/* .catch Ljava/lang/Exception; {:try_start_13 ..:try_end_13} :catch_c */
/* .line 619 */
} // .end local v23 # "BinderReader":Ljava/io/BufferedReader;
/* .restart local v6 # "isBlockSystem":Z */
/* .restart local v7 # "tag":Ljava/lang/String; */
/* .restart local v18 # "callType":I */
/* .restart local v21 # "fileBinderReader":Ljava/io/File; */
/* .restart local v25 # "isCheckSystemDThread":Z */
/* .restart local v28 # "isCheckAppDThread":Z */
/* .restart local p0 # "Pid":I */
/* .restart local p1 # "info":Lcom/android/server/ScoutHelper$ScoutBinderInfo; */
/* .restart local p2 # "javaProcess":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
/* .restart local p3 # "nativeProcess":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
/* :catch_c */
/* move-exception v0 */
} // .end local v18 # "callType":I
} // .end local v21 # "fileBinderReader":Ljava/io/File;
} // .end local v25 # "isCheckSystemDThread":Z
} // .end local v28 # "isCheckAppDThread":Z
/* .restart local v8 # "callType":I */
/* .restart local v10 # "isCheckAppDThread":Z */
/* .restart local v11 # "isCheckSystemDThread":Z */
/* .restart local v12 # "fileBinderReader":Ljava/io/File; */
/* :catch_d */
/* move-exception v0 */
/* move/from16 v18, v8 */
/* move/from16 v28, v10 */
/* move/from16 v25, v11 */
/* move-object/from16 v21, v12 */
/* .line 620 */
} // .end local v8 # "callType":I
} // .end local v10 # "isCheckAppDThread":Z
} // .end local v11 # "isCheckSystemDThread":Z
} // .end local v12 # "fileBinderReader":Ljava/io/File;
/* .restart local v0 # "e":Ljava/lang/Exception; */
/* .restart local v18 # "callType":I */
/* .restart local v21 # "fileBinderReader":Ljava/io/File; */
/* .restart local v25 # "isCheckSystemDThread":Z */
/* .restart local v28 # "isCheckAppDThread":Z */
} // :goto_c
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 621 */
final String v1 = "Read binder Proc transaction Error:"; // const-string v1, "Read binder Proc transaction Error:"
android.util.Slog .w ( v7,v1,v0 );
/* .line 623 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_d
} // .end method
public static void checkBinderThreadFull ( Integer p0, com.android.server.ScoutHelper$ScoutBinderInfo p1, java.util.TreeMap p2, java.util.ArrayList p3, java.util.ArrayList p4 ) {
/* .locals 25 */
/* .param p0, "Pid" # I */
/* .param p1, "info" # Lcom/android/server/ScoutHelper$ScoutBinderInfo; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I", */
/* "Lcom/android/server/ScoutHelper$ScoutBinderInfo;", */
/* "Ljava/util/TreeMap<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/lang/Integer;", */
/* ">;", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Integer;", */
/* ">;", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Integer;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 634 */
/* .local p2, "inPidMap":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Ljava/lang/Integer;>;" */
/* .local p3, "javaProcess":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
/* .local p4, "nativeProcess":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
/* move-object/from16 v1, p2 */
final String v2 = "):"; // const-string v2, "):"
/* invoke-virtual/range {p1 ..p1}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->getTag()Ljava/lang/String; */
/* .line 635 */
/* .local v3, "tag":Ljava/lang/String; */
/* move/from16 v4, p0 */
com.android.server.ScoutHelper .getBinderLogFile ( v3,v4 );
/* .line 636 */
/* .local v5, "fileBinderReader":Ljava/io/File; */
/* if-nez v5, :cond_0 */
/* .line 637 */
return;
/* .line 639 */
} // :cond_0
try { // :try_start_0
/* new-instance v0, Ljava/io/BufferedReader; */
/* new-instance v6, Ljava/io/FileReader; */
/* invoke-direct {v6, v5}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V */
/* invoke-direct {v0, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_6 */
/* move-object v6, v0 */
/* .line 640 */
/* .local v6, "BinderReader":Ljava/io/BufferedReader; */
int v0 = 0; // const/4 v0, 0x0
/* .line 641 */
/* .local v0, "binderTransactionInfo":Ljava/lang/String; */
int v7 = 0; // const/4 v7, 0x0
/* .line 642 */
/* .local v7, "fromPid":I */
} // :goto_0
try { // :try_start_1
(( java.io.BufferedReader ) v6 ).readLine ( ); // invoke-virtual {v6}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v9, v8 */
} // .end local v0 # "binderTransactionInfo":Ljava/lang/String;
/* .local v9, "binderTransactionInfo":Ljava/lang/String; */
if ( v8 != null) { // if-eqz v8, :cond_9
/* .line 643 */
final String v0 = "MIUI incoming transaction"; // const-string v0, "MIUI incoming transaction"
v0 = (( java.lang.String ) v9 ).startsWith ( v0 ); // invoke-virtual {v9, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_4 */
final String v8 = "incoming transaction"; // const-string v8, "incoming transaction"
/* if-nez v0, :cond_1 */
/* .line 644 */
try { // :try_start_2
v0 = (( java.lang.String ) v9 ).startsWith ( v8 ); // invoke-virtual {v9, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
/* if-nez v0, :cond_1 */
v0 = com.android.server.ScoutHelper.supportNewBinderLog;
/* .line 645 */
v0 = (( java.lang.Boolean ) v0 ).booleanValue ( ); // invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
if ( v0 != null) { // if-eqz v0, :cond_4
final String v0 = " incoming transaction"; // const-string v0, " incoming transaction"
v0 = (( java.lang.String ) v9 ).startsWith ( v0 ); // invoke-virtual {v9, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 639 */
} // .end local v7 # "fromPid":I
} // .end local v9 # "binderTransactionInfo":Ljava/lang/String;
/* :catchall_0 */
/* move-exception v0 */
/* move-object/from16 v10, p1 */
/* move-object/from16 v13, p3 */
/* move-object/from16 v1, p4 */
/* move-object v2, v0 */
/* move-object/from16 v17, v5 */
/* goto/16 :goto_a */
/* .line 647 */
/* .restart local v7 # "fromPid":I */
/* .restart local v9 # "binderTransactionInfo":Ljava/lang/String; */
} // :cond_1
} // :goto_1
try { // :try_start_3
final String v0 = "\\bfrom((?:\\s+)?\\d+):((?:\\s+)?\\d+)\\s+to((?:\\s+)?\\d+):((?:\\s+)?\\d+)+.*code:((?:\\s+)?\\d+).*duration:((?:\\s+)?(\\d+(?:\\.\\d+)?))\\b"; // const-string v0, "\\bfrom((?:\\s+)?\\d+):((?:\\s+)?\\d+)\\s+to((?:\\s+)?\\d+):((?:\\s+)?\\d+)+.*code:((?:\\s+)?\\d+).*duration:((?:\\s+)?(\\d+(?:\\.\\d+)?))\\b"
/* .line 648 */
/* .local v0, "binderRegexPattern":Ljava/lang/String; */
v10 = com.android.server.ScoutHelper.supportNewBinderLog;
v10 = (( java.lang.Boolean ) v10 ).booleanValue ( ); // invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z
/* :try_end_3 */
/* .catch Ljava/lang/Exception; {:try_start_3 ..:try_end_3} :catch_4 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_4 */
if ( v10 != null) { // if-eqz v10, :cond_2
try { // :try_start_4
final String v10 = "\\bfrom((?:\\s+)?\\d+):((?:\\s+)?\\d+)\\s+to((?:\\s+)?\\d+):((?:\\s+)?\\d+)+.*code((?:\\s+)?\\d+)+.*elapsed((?:\\s+)?\\d+)ms+.*"; // const-string v10, "\\bfrom((?:\\s+)?\\d+):((?:\\s+)?\\d+)\\s+to((?:\\s+)?\\d+):((?:\\s+)?\\d+)+.*code((?:\\s+)?\\d+)+.*elapsed((?:\\s+)?\\d+)ms+.*"
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_0 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* move-object v0, v10 */
/* .line 684 */
} // .end local v0 # "binderRegexPattern":Ljava/lang/String;
/* :catch_0 */
/* move-exception v0 */
/* move-object/from16 v10, p1 */
/* move-object/from16 v13, p3 */
/* move-object/from16 v1, p4 */
/* move-object/from16 v17, v5 */
/* goto/16 :goto_8 */
/* .line 649 */
/* .restart local v0 # "binderRegexPattern":Ljava/lang/String; */
} // :cond_2
} // :goto_2
try { // :try_start_5
java.util.regex.Pattern .compile ( v0 );
/* .line 650 */
/* .local v10, "pattern":Ljava/util/regex/Pattern; */
(( java.util.regex.Pattern ) v10 ).matcher ( v9 ); // invoke-virtual {v10, v9}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
/* .line 651 */
/* .local v11, "matcher":Ljava/util/regex/Matcher; */
v12 = (( java.util.regex.Matcher ) v11 ).find ( ); // invoke-virtual {v11}, Ljava/util/regex/Matcher;->find()Z
/* :try_end_5 */
/* .catch Ljava/lang/Exception; {:try_start_5 ..:try_end_5} :catch_4 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_4 */
final String v13 = "("; // const-string v13, "("
/* if-nez v12, :cond_5 */
/* .line 652 */
try { // :try_start_6
v8 = (( java.lang.String ) v9 ).startsWith ( v8 ); // invoke-virtual {v9, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
/* if-nez v8, :cond_3 */
/* .line 653 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v8 ).append ( v13 ); // invoke-virtual {v8, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v12 = ") regex match failed"; // const-string v12, ") regex match failed"
(( java.lang.StringBuilder ) v8 ).append ( v12 ); // invoke-virtual {v8, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v3,v8 );
/* :try_end_6 */
/* .catch Ljava/lang/Exception; {:try_start_6 ..:try_end_6} :catch_0 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_0 */
/* .line 655 */
} // :cond_3
/* nop */
/* .line 642 */
} // .end local v0 # "binderRegexPattern":Ljava/lang/String;
} // .end local v10 # "pattern":Ljava/util/regex/Pattern;
} // .end local v11 # "matcher":Ljava/util/regex/Matcher;
} // :cond_4
/* move-object v0, v9 */
/* .line 657 */
/* .restart local v0 # "binderRegexPattern":Ljava/lang/String; */
/* .restart local v10 # "pattern":Ljava/util/regex/Pattern; */
/* .restart local v11 # "matcher":Ljava/util/regex/Matcher; */
} // :cond_5
int v8 = 1; // const/4 v8, 0x1
try { // :try_start_7
(( java.util.regex.Matcher ) v11 ).group ( v8 ); // invoke-virtual {v11, v8}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;
(( java.lang.String ) v12 ).trim ( ); // invoke-virtual {v12}, Ljava/lang/String;->trim()Ljava/lang/String;
v12 = java.lang.Integer .parseInt ( v12 );
/* move v7, v12 */
/* .line 658 */
int v12 = 2; // const/4 v12, 0x2
(( java.util.regex.Matcher ) v11 ).group ( v12 ); // invoke-virtual {v11, v12}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;
(( java.lang.String ) v12 ).trim ( ); // invoke-virtual {v12}, Ljava/lang/String;->trim()Ljava/lang/String;
v12 = java.lang.Integer .parseInt ( v12 );
/* .line 659 */
/* .local v12, "fromTid":I */
int v14 = 3; // const/4 v14, 0x3
(( java.util.regex.Matcher ) v11 ).group ( v14 ); // invoke-virtual {v11, v14}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;
(( java.lang.String ) v14 ).trim ( ); // invoke-virtual {v14}, Ljava/lang/String;->trim()Ljava/lang/String;
v14 = java.lang.Integer .parseInt ( v14 );
/* .line 660 */
/* .local v14, "toPid":I */
int v15 = 4; // const/4 v15, 0x4
(( java.util.regex.Matcher ) v11 ).group ( v15 ); // invoke-virtual {v11, v15}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;
(( java.lang.String ) v15 ).trim ( ); // invoke-virtual {v15}, Ljava/lang/String;->trim()Ljava/lang/String;
v15 = java.lang.Integer .parseInt ( v15 );
/* .line 661 */
/* .local v15, "toTid":I */
int v8 = 5; // const/4 v8, 0x5
(( java.util.regex.Matcher ) v11 ).group ( v8 ); // invoke-virtual {v11, v8}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;
(( java.lang.String ) v8 ).trim ( ); // invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;
v8 = java.lang.Integer .parseInt ( v8 );
/* .line 662 */
/* .local v8, "code":I */
/* const-wide/16 v17, 0x0 */
/* .line 663 */
/* .local v17, "waitTime":D */
v19 = com.android.server.ScoutHelper.supportNewBinderLog;
v19 = /* invoke-virtual/range {v19 ..v19}, Ljava/lang/Boolean;->booleanValue()Z */
/* :try_end_7 */
/* .catch Ljava/lang/Exception; {:try_start_7 ..:try_end_7} :catch_4 */
/* .catchall {:try_start_7 ..:try_end_7} :catchall_4 */
/* move-object/from16 v20, v0 */
} // .end local v0 # "binderRegexPattern":Ljava/lang/String;
/* .local v20, "binderRegexPattern":Ljava/lang/String; */
int v0 = 6; // const/4 v0, 0x6
if ( v19 != null) { // if-eqz v19, :cond_6
/* .line 664 */
try { // :try_start_8
(( java.util.regex.Matcher ) v11 ).group ( v0 ); // invoke-virtual {v11, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;
(( java.lang.String ) v0 ).trim ( ); // invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;
java.lang.Double .valueOf ( v0 );
/* .line 665 */
/* .local v0, "elapsed":Ljava/lang/Double; */
(( java.lang.Double ) v0 ).doubleValue ( ); // invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D
/* move-result-wide v21 */
/* :try_end_8 */
/* .catch Ljava/lang/Exception; {:try_start_8 ..:try_end_8} :catch_0 */
/* .catchall {:try_start_8 ..:try_end_8} :catchall_0 */
/* const-wide v23, 0x408f400000000000L # 1000.0 */
/* div-double v21, v21, v23 */
/* .line 666 */
} // .end local v0 # "elapsed":Ljava/lang/Double;
} // .end local v17 # "waitTime":D
/* .local v21, "waitTime":D */
/* move-object/from16 v17, v5 */
/* move-wide/from16 v4, v21 */
/* .line 667 */
} // .end local v21 # "waitTime":D
/* .restart local v17 # "waitTime":D */
} // :cond_6
try { // :try_start_9
(( java.util.regex.Matcher ) v11 ).group ( v0 ); // invoke-virtual {v11, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;
(( java.lang.String ) v0 ).trim ( ); // invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;
java.lang.Double .valueOf ( v0 );
(( java.lang.Double ) v0 ).doubleValue ( ); // invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D
/* move-result-wide v21 */
/* :try_end_9 */
/* .catch Ljava/lang/Exception; {:try_start_9 ..:try_end_9} :catch_4 */
/* .catchall {:try_start_9 ..:try_end_9} :catchall_4 */
} // .end local v17 # "waitTime":D
/* .restart local v21 # "waitTime":D */
/* move-object/from16 v17, v5 */
/* move-wide/from16 v4, v21 */
/* .line 669 */
} // .end local v5 # "fileBinderReader":Ljava/io/File;
} // .end local v21 # "waitTime":D
/* .local v4, "waitTime":D */
/* .local v17, "fileBinderReader":Ljava/io/File; */
} // :goto_3
try { // :try_start_a
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* move-object/from16 v18, v10 */
} // .end local v10 # "pattern":Ljava/util/regex/Pattern;
/* .local v18, "pattern":Ljava/util/regex/Pattern; */
final String v10 = "incoming transaction from "; // const-string v10, "incoming transaction from "
(( java.lang.StringBuilder ) v0 ).append ( v10 ); // invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v13 ); // invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 670 */
com.android.server.ScoutHelper .getProcessCmdline ( v7 );
(( java.lang.StringBuilder ) v0 ).append ( v10 ); // invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v12 ); // invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v13 ); // invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 671 */
com.android.server.ScoutHelper .getProcessComm ( v7,v12 );
(( java.lang.StringBuilder ) v0 ).append ( v10 ); // invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v10 = ") to "; // const-string v10, ") to "
(( java.lang.StringBuilder ) v0 ).append ( v10 ); // invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v14 ); // invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v13 ); // invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 672 */
com.android.server.ScoutHelper .getProcessCmdline ( v14 );
(( java.lang.StringBuilder ) v0 ).append ( v10 ); // invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v15 ); // invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v13 ); // invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 673 */
com.android.server.ScoutHelper .getProcessComm ( v14,v15 );
(( java.lang.StringBuilder ) v0 ).append ( v10 ); // invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v10 = ") elapsed: "; // const-string v10, ") elapsed: "
(( java.lang.StringBuilder ) v0 ).append ( v10 ); // invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v4, v5 ); // invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
final String v10 = " s code: "; // const-string v10, " s code: "
(( java.lang.StringBuilder ) v0 ).append ( v10 ); // invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* :try_end_a */
/* .catch Ljava/lang/Exception; {:try_start_a ..:try_end_a} :catch_3 */
/* .catchall {:try_start_a ..:try_end_a} :catchall_2 */
/* .line 676 */
/* .local v0, "binderLog":Ljava/lang/String; */
/* move-object/from16 v10, p1 */
try { // :try_start_b
(( com.android.server.ScoutHelper$ScoutBinderInfo ) v10 ).addBinderTransInfo ( v0 ); // invoke-virtual {v10, v0}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->addBinderTransInfo(Ljava/lang/String;)V
/* .line 677 */
int v13 = 1; // const/4 v13, 0x1
/* if-ge v7, v13, :cond_7 */
/* move-object/from16 v13, p3 */
/* move-object/from16 v1, p4 */
/* .line 678 */
} // :cond_7
java.lang.Integer .valueOf ( v7 );
v13 = (( java.util.TreeMap ) v1 ).containsKey ( v13 ); // invoke-virtual {v1, v13}, Ljava/util/TreeMap;->containsKey(Ljava/lang/Object;)Z
if ( v13 != null) { // if-eqz v13, :cond_8
/* .line 679 */
java.lang.Integer .valueOf ( v7 );
/* move-object/from16 v19, v0 */
} // .end local v0 # "binderLog":Ljava/lang/String;
/* .local v19, "binderLog":Ljava/lang/String; */
java.lang.Integer .valueOf ( v7 );
(( java.util.TreeMap ) v1 ).get ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/Integer; */
v0 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
/* const/16 v16, 0x1 */
/* add-int/lit8 v0, v0, 0x1 */
java.lang.Integer .valueOf ( v0 );
(( java.util.TreeMap ) v1 ).put ( v13, v0 ); // invoke-virtual {v1, v13, v0}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* move-object/from16 v13, p3 */
/* move-object/from16 v1, p4 */
/* .line 681 */
} // .end local v19 # "binderLog":Ljava/lang/String;
/* .restart local v0 # "binderLog":Ljava/lang/String; */
} // :cond_8
/* move-object/from16 v19, v0 */
} // .end local v0 # "binderLog":Ljava/lang/String;
/* .restart local v19 # "binderLog":Ljava/lang/String; */
java.lang.Integer .valueOf ( v7 );
int v13 = 1; // const/4 v13, 0x1
java.lang.Integer .valueOf ( v13 );
(( java.util.TreeMap ) v1 ).put ( v0, v13 ); // invoke-virtual {v1, v0, v13}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_b */
/* .catch Ljava/lang/Exception; {:try_start_b ..:try_end_b} :catch_2 */
/* .catchall {:try_start_b ..:try_end_b} :catchall_1 */
/* .line 682 */
/* move-object/from16 v13, p3 */
/* move-object/from16 v1, p4 */
try { // :try_start_c
com.android.server.ScoutHelper .addPidtoList ( v3,v7,v13,v1 );
/* :try_end_c */
/* .catch Ljava/lang/Exception; {:try_start_c ..:try_end_c} :catch_1 */
/* .catchall {:try_start_c ..:try_end_c} :catchall_3 */
/* .line 686 */
} // .end local v4 # "waitTime":D
} // .end local v8 # "code":I
} // .end local v11 # "matcher":Ljava/util/regex/Matcher;
} // .end local v12 # "fromTid":I
} // .end local v14 # "toPid":I
} // .end local v15 # "toTid":I
} // .end local v18 # "pattern":Ljava/util/regex/Pattern;
} // .end local v19 # "binderLog":Ljava/lang/String;
} // .end local v20 # "binderRegexPattern":Ljava/lang/String;
} // :goto_4
/* nop */
/* .line 642 */
} // :goto_5
/* move/from16 v4, p0 */
/* move-object/from16 v1, p2 */
/* move-object v0, v9 */
/* move-object/from16 v5, v17 */
/* goto/16 :goto_0 */
/* .line 684 */
/* :catch_1 */
/* move-exception v0 */
/* .line 639 */
} // .end local v7 # "fromPid":I
} // .end local v9 # "binderTransactionInfo":Ljava/lang/String;
/* :catchall_1 */
/* move-exception v0 */
/* .line 684 */
/* .restart local v7 # "fromPid":I */
/* .restart local v9 # "binderTransactionInfo":Ljava/lang/String; */
/* :catch_2 */
/* move-exception v0 */
/* .line 639 */
} // .end local v7 # "fromPid":I
} // .end local v9 # "binderTransactionInfo":Ljava/lang/String;
/* :catchall_2 */
/* move-exception v0 */
/* move-object/from16 v10, p1 */
} // :goto_6
/* move-object/from16 v13, p3 */
/* move-object/from16 v1, p4 */
/* .line 684 */
/* .restart local v7 # "fromPid":I */
/* .restart local v9 # "binderTransactionInfo":Ljava/lang/String; */
/* :catch_3 */
/* move-exception v0 */
/* move-object/from16 v10, p1 */
} // :goto_7
/* move-object/from16 v13, p3 */
/* move-object/from16 v1, p4 */
} // .end local v17 # "fileBinderReader":Ljava/io/File;
/* .restart local v5 # "fileBinderReader":Ljava/io/File; */
/* :catch_4 */
/* move-exception v0 */
/* move-object/from16 v10, p1 */
/* move-object/from16 v13, p3 */
/* move-object/from16 v1, p4 */
/* move-object/from16 v17, v5 */
/* .line 685 */
} // .end local v5 # "fileBinderReader":Ljava/io/File;
/* .local v0, "e":Ljava/lang/Exception; */
/* .restart local v17 # "fileBinderReader":Ljava/io/File; */
} // :goto_8
try { // :try_start_d
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "regular match error, String:"; // const-string v5, "regular match error, String:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v9 ); // invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v3,v4 );
/* :try_end_d */
/* .catchall {:try_start_d ..:try_end_d} :catchall_3 */
/* .line 686 */
/* move/from16 v4, p0 */
/* move-object/from16 v1, p2 */
/* move-object v0, v9 */
/* move-object/from16 v5, v17 */
} // .end local v0 # "e":Ljava/lang/Exception;
/* goto/16 :goto_0 */
/* .line 639 */
} // .end local v7 # "fromPid":I
} // .end local v9 # "binderTransactionInfo":Ljava/lang/String;
/* :catchall_3 */
/* move-exception v0 */
} // :goto_9
/* move-object v2, v0 */
/* .line 642 */
} // .end local v17 # "fileBinderReader":Ljava/io/File;
/* .restart local v5 # "fileBinderReader":Ljava/io/File; */
/* .restart local v7 # "fromPid":I */
/* .restart local v9 # "binderTransactionInfo":Ljava/lang/String; */
} // :cond_9
/* move-object/from16 v10, p1 */
/* move-object/from16 v13, p3 */
/* move-object/from16 v1, p4 */
/* move-object/from16 v17, v5 */
/* .line 689 */
} // .end local v5 # "fileBinderReader":Ljava/io/File;
} // .end local v7 # "fromPid":I
} // .end local v9 # "binderTransactionInfo":Ljava/lang/String;
/* .restart local v17 # "fileBinderReader":Ljava/io/File; */
try { // :try_start_e
(( java.io.BufferedReader ) v6 ).close ( ); // invoke-virtual {v6}, Ljava/io/BufferedReader;->close()V
/* :try_end_e */
/* .catch Ljava/lang/Exception; {:try_start_e ..:try_end_e} :catch_5 */
/* .line 692 */
} // .end local v6 # "BinderReader":Ljava/io/BufferedReader;
/* .line 689 */
/* :catch_5 */
/* move-exception v0 */
/* .line 639 */
} // .end local v17 # "fileBinderReader":Ljava/io/File;
/* .restart local v5 # "fileBinderReader":Ljava/io/File; */
/* .restart local v6 # "BinderReader":Ljava/io/BufferedReader; */
/* :catchall_4 */
/* move-exception v0 */
/* move-object/from16 v10, p1 */
/* move-object/from16 v13, p3 */
/* move-object/from16 v1, p4 */
/* move-object/from16 v17, v5 */
/* move-object v2, v0 */
} // .end local v5 # "fileBinderReader":Ljava/io/File;
/* .restart local v17 # "fileBinderReader":Ljava/io/File; */
} // :goto_a
try { // :try_start_f
(( java.io.BufferedReader ) v6 ).close ( ); // invoke-virtual {v6}, Ljava/io/BufferedReader;->close()V
/* :try_end_f */
/* .catchall {:try_start_f ..:try_end_f} :catchall_5 */
/* :catchall_5 */
/* move-exception v0 */
/* move-object v4, v0 */
try { // :try_start_10
(( java.lang.Throwable ) v2 ).addSuppressed ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v3 # "tag":Ljava/lang/String;
} // .end local v17 # "fileBinderReader":Ljava/io/File;
} // .end local p0 # "Pid":I
} // .end local p1 # "info":Lcom/android/server/ScoutHelper$ScoutBinderInfo;
} // .end local p2 # "inPidMap":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
} // .end local p3 # "javaProcess":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
} // .end local p4 # "nativeProcess":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
} // :goto_b
/* throw v2 */
/* :try_end_10 */
/* .catch Ljava/lang/Exception; {:try_start_10 ..:try_end_10} :catch_5 */
/* .line 689 */
} // .end local v6 # "BinderReader":Ljava/io/BufferedReader;
/* .restart local v3 # "tag":Ljava/lang/String; */
/* .restart local v5 # "fileBinderReader":Ljava/io/File; */
/* .restart local p0 # "Pid":I */
/* .restart local p1 # "info":Lcom/android/server/ScoutHelper$ScoutBinderInfo; */
/* .restart local p2 # "inPidMap":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Ljava/lang/Integer;>;" */
/* .restart local p3 # "javaProcess":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
/* .restart local p4 # "nativeProcess":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
/* :catch_6 */
/* move-exception v0 */
/* move-object/from16 v10, p1 */
/* move-object/from16 v13, p3 */
/* move-object/from16 v1, p4 */
/* move-object/from16 v17, v5 */
/* .line 690 */
} // .end local v5 # "fileBinderReader":Ljava/io/File;
/* .restart local v0 # "e":Ljava/lang/Exception; */
/* .restart local v17 # "fileBinderReader":Ljava/io/File; */
} // :goto_c
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 691 */
final String v2 = "Read binder Proc transaction Error:"; // const-string v2, "Read binder Proc transaction Error:"
android.util.Slog .w ( v3,v2,v0 );
/* .line 693 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_d
return;
} // .end method
public static Integer checkIsJavaOrNativeProcess ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "adj" # I */
/* .line 761 */
/* const/16 v0, -0x3e8 */
/* if-ne p0, v0, :cond_0 */
int v0 = 2; // const/4 v0, 0x2
/* .line 762 */
} // :cond_0
/* const/16 v0, -0x3b6 */
/* if-ne p0, v0, :cond_1 */
int v0 = 3; // const/4 v0, 0x3
/* .line 763 */
} // :cond_1
/* const/16 v0, -0x384 */
/* if-lt p0, v0, :cond_2 */
/* const/16 v0, 0x3e8 */
/* if-gt p0, v0, :cond_2 */
int v0 = 1; // const/4 v0, 0x1
/* .line 764 */
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
} // .end method
public static Boolean checkIsMonkey ( java.lang.String p0, Double p1 ) {
/* .locals 2 */
/* .param p0, "processName" # Ljava/lang/String; */
/* .param p1, "time" # D */
/* .line 627 */
final String v0 = "com.android.commands.monkey"; // const-string v0, "com.android.commands.monkey"
v0 = (( java.lang.String ) v0 ).equals ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const-wide/high16 v0, 0x4034000000000000L # 20.0 */
/* cmpl-double v0, p1, v0 */
/* if-lez v0, :cond_0 */
/* .line 628 */
v0 = miui.mqsas.scout.ScoutUtils .isLibraryTest ( );
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 627 */
} // :goto_0
} // .end method
public static void copyRamoopsFileToMqs ( ) {
/* .locals 8 */
/* .line 885 */
final String v0 = "ScoutHelper"; // const-string v0, "ScoutHelper"
/* new-instance v1, Ljava/io/File; */
v2 = com.android.server.ScoutHelper.CONSOLE_RAMOOPS_PATH;
/* invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 886 */
/* .local v1, "ramoopsFile":Ljava/io/File; */
/* new-instance v2, Ljava/io/File; */
v3 = com.android.server.ScoutHelper.CONSOLE_RAMOOPS_0_PATH;
/* invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 887 */
/* .local v2, "ramoops_0File":Ljava/io/File; */
v3 = (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
/* if-nez v3, :cond_0 */
v3 = (( java.io.File ) v2 ).exists ( ); // invoke-virtual {v2}, Ljava/io/File;->exists()Z
/* if-nez v3, :cond_0 */
/* .line 888 */
return;
/* .line 890 */
} // :cond_0
/* const-string/jumbo v3, "sys.system_server.start_count" */
int v4 = 1; // const/4 v4, 0x1
v3 = android.os.SystemProperties .getInt ( v3,v4 );
/* if-ne v3, v4, :cond_2 */
/* .line 891 */
v3 = (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* move-object v3, v1 */
} // :cond_1
/* move-object v3, v2 */
/* .line 892 */
/* .local v3, "lastKmsgFile":Ljava/io/File; */
} // :goto_0
try { // :try_start_0
/* new-instance v4, Ljava/io/FileOutputStream; */
com.android.server.ScoutHelper .getMqsRamoopsFile ( );
/* invoke-direct {v4, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 893 */
/* .local v4, "fos":Ljava/io/FileOutputStream; */
try { // :try_start_1
/* new-instance v5, Ljava/io/FileInputStream; */
/* invoke-direct {v5, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_2 */
/* .line 894 */
/* .local v5, "ramoopsInput":Ljava/io/FileInputStream; */
try { // :try_start_2
android.os.FileUtils .copy ( v5,v4 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 895 */
try { // :try_start_3
(( java.io.FileInputStream ) v5 ).close ( ); // invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_2 */
} // .end local v5 # "ramoopsInput":Ljava/io/FileInputStream;
try { // :try_start_4
(( java.io.FileOutputStream ) v4 ).close ( ); // invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 892 */
/* .restart local v5 # "ramoopsInput":Ljava/io/FileInputStream; */
/* :catchall_0 */
/* move-exception v6 */
try { // :try_start_5
(( java.io.FileInputStream ) v5 ).close ( ); // invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_1 */
/* :catchall_1 */
/* move-exception v7 */
try { // :try_start_6
(( java.lang.Throwable ) v6 ).addSuppressed ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v1 # "ramoopsFile":Ljava/io/File;
} // .end local v2 # "ramoops_0File":Ljava/io/File;
} // .end local v3 # "lastKmsgFile":Ljava/io/File;
} // .end local v4 # "fos":Ljava/io/FileOutputStream;
} // :goto_1
/* throw v6 */
/* :try_end_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_2 */
} // .end local v5 # "ramoopsInput":Ljava/io/FileInputStream;
/* .restart local v1 # "ramoopsFile":Ljava/io/File; */
/* .restart local v2 # "ramoops_0File":Ljava/io/File; */
/* .restart local v3 # "lastKmsgFile":Ljava/io/File; */
/* .restart local v4 # "fos":Ljava/io/FileOutputStream; */
/* :catchall_2 */
/* move-exception v5 */
try { // :try_start_7
(( java.io.FileOutputStream ) v4 ).close ( ); // invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
/* :try_end_7 */
/* .catchall {:try_start_7 ..:try_end_7} :catchall_3 */
/* :catchall_3 */
/* move-exception v6 */
try { // :try_start_8
(( java.lang.Throwable ) v5 ).addSuppressed ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v1 # "ramoopsFile":Ljava/io/File;
} // .end local v2 # "ramoops_0File":Ljava/io/File;
} // .end local v3 # "lastKmsgFile":Ljava/io/File;
} // :goto_2
/* throw v5 */
/* :try_end_8 */
/* .catch Ljava/io/IOException; {:try_start_8 ..:try_end_8} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_8 ..:try_end_8} :catch_0 */
/* .line 898 */
} // .end local v4 # "fos":Ljava/io/FileOutputStream;
/* .restart local v1 # "ramoopsFile":Ljava/io/File; */
/* .restart local v2 # "ramoops_0File":Ljava/io/File; */
/* .restart local v3 # "lastKmsgFile":Ljava/io/File; */
/* :catch_0 */
/* move-exception v4 */
/* .line 899 */
/* .local v4, "e":Ljava/lang/Exception; */
final String v5 = "UNKown Exception: copyRamoopsFileToMqs fail"; // const-string v5, "UNKown Exception: copyRamoopsFileToMqs fail"
android.util.Slog .w ( v0,v5 );
/* .line 900 */
(( java.lang.Exception ) v4 ).printStackTrace ( ); // invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V
/* .line 895 */
} // .end local v4 # "e":Ljava/lang/Exception;
/* :catch_1 */
/* move-exception v4 */
/* .line 896 */
/* .local v4, "e":Ljava/io/IOException; */
final String v5 = "IOException: copyRamoopsFileToMqs fail"; // const-string v5, "IOException: copyRamoopsFileToMqs fail"
android.util.Slog .w ( v0,v5 );
/* .line 897 */
(( java.io.IOException ) v4 ).printStackTrace ( ); // invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V
/* .line 901 */
} // .end local v4 # "e":Ljava/io/IOException;
} // :goto_3
/* nop */
/* .line 903 */
} // .end local v3 # "lastKmsgFile":Ljava/io/File;
} // :cond_2
} // :goto_4
return;
} // .end method
public static void doSysRqInterface ( Object p0 ) {
/* .locals 3 */
/* .param p0, "c" # C */
/* .line 794 */
try { // :try_start_0
/* new-instance v0, Ljava/io/FileWriter; */
final String v1 = "/proc/sysrq-trigger"; // const-string v1, "/proc/sysrq-trigger"
/* invoke-direct {v0, v1}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 795 */
/* .local v0, "sysrq_trigger":Ljava/io/FileWriter; */
try { // :try_start_1
(( java.io.FileWriter ) v0 ).write ( p0 ); // invoke-virtual {v0, p0}, Ljava/io/FileWriter;->write(I)V
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 796 */
try { // :try_start_2
(( java.io.FileWriter ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/FileWriter;->close()V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 798 */
} // .end local v0 # "sysrq_trigger":Ljava/io/FileWriter;
/* .line 794 */
/* .restart local v0 # "sysrq_trigger":Ljava/io/FileWriter; */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_3
(( java.io.FileWriter ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/FileWriter;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v2 */
try { // :try_start_4
(( java.lang.Throwable ) v1 ).addSuppressed ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local p0 # "c":C
} // :goto_0
/* throw v1 */
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 796 */
} // .end local v0 # "sysrq_trigger":Ljava/io/FileWriter;
/* .restart local p0 # "c":C */
/* :catch_0 */
/* move-exception v0 */
/* .line 797 */
/* .local v0, "e":Ljava/io/IOException; */
final String v1 = "ScoutHelper"; // const-string v1, "ScoutHelper"
final String v2 = "Failed to write to /proc/sysrq-trigger"; // const-string v2, "Failed to write to /proc/sysrq-trigger"
android.util.Slog .w ( v1,v2,v0 );
/* .line 799 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :goto_1
return;
} // .end method
public static java.lang.String dumpOfflineLog ( java.lang.String p0, com.android.server.ScoutHelper$Action p1, java.lang.String p2, java.lang.String p3 ) {
/* .locals 20 */
/* .param p0, "reason" # Ljava/lang/String; */
/* .param p1, "action" # Lcom/android/server/ScoutHelper$Action; */
/* .param p2, "type" # Ljava/lang/String; */
/* .param p3, "where" # Ljava/lang/String; */
/* .line 858 */
/* move-object/from16 v1, p0 */
/* move-object/from16 v11, p2 */
/* move-object/from16 v12, p3 */
final String v0 = "_"; // const-string v0, "_"
final String v13 = ""; // const-string v13, ""
/* .line 860 */
/* .local v13, "zipPath":Ljava/lang/String; */
try { // :try_start_0
/* new-instance v2, Ljava/text/SimpleDateFormat; */
/* const-string/jumbo v3, "yyyy-MM-dd-HH-mm-ss-SSS" */
v4 = java.util.Locale.US;
/* invoke-direct {v2, v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V */
/* move-object v14, v2 */
/* .line 862 */
/* .local v14, "offlineLogDateFormat":Ljava/text/SimpleDateFormat; */
/* new-instance v2, Ljava/util/Date; */
/* invoke-direct {v2}, Ljava/util/Date;-><init>()V */
(( java.text.SimpleDateFormat ) v14 ).format ( v2 ); // invoke-virtual {v14, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
/* move-object v15, v2 */
/* .line 863 */
/* .local v15, "formattedDate":Ljava/lang/String; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v12 ); // invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = java.io.File.separator;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* move-object v10, v2 */
/* .line 864 */
/* .local v10, "fileDir":Ljava/lang/String; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v15 ); // invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* move-object v9, v2 */
/* .line 866 */
/* .local v9, "fileName":Ljava/lang/String; */
/* new-instance v2, Ljava/io/File; */
/* invoke-direct {v2, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* move-object/from16 v16, v2 */
/* .line 867 */
/* .local v16, "offlineLogDir":Ljava/io/File; */
v2 = /* invoke-virtual/range {v16 ..v16}, Ljava/io/File;->exists()Z */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
final String v3 = "ScoutHelper"; // const-string v3, "ScoutHelper"
/* if-nez v2, :cond_0 */
/* .line 868 */
try { // :try_start_1
v2 = /* invoke-virtual/range {v16 ..v16}, Ljava/io/File;->mkdirs()Z */
/* if-nez v2, :cond_0 */
/* .line 869 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Cannot create "; // const-string v2, "Cannot create "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v10 ); // invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v0 );
/* .line 870 */
/* .line 873 */
} // :cond_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "dumpOfflineLog reason:"; // const-string v4, "dumpOfflineLog reason:"
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " action="; // const-string v4, " action="
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-object/from16 v8, p1 */
(( java.lang.StringBuilder ) v2 ).append ( v8 ); // invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v4 = " type="; // const-string v4, " type="
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v11 ); // invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " where="; // const-string v4, " where="
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v12 ); // invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v2 );
/* .line 875 */
/* invoke-static/range {p1 ..p1}, Lcom/android/server/ScoutHelper$Action;->-$$Nest$fgetactions(Lcom/android/server/ScoutHelper$Action;)Ljava/util/List; */
/* invoke-static/range {p1 ..p1}, Lcom/android/server/ScoutHelper$Action;->-$$Nest$fgetparams(Lcom/android/server/ScoutHelper$Action;)Ljava/util/List; */
int v6 = 1; // const/4 v6, 0x1
int v7 = 1; // const/4 v7, 0x1
/* const/16 v17, 0x0 */
/* invoke-static/range {p1 ..p1}, Lcom/android/server/ScoutHelper$Action;->-$$Nest$fgetincludeFiles(Lcom/android/server/ScoutHelper$Action;)Ljava/util/List; */
/* move-object/from16 v2, p2 */
/* move-object v3, v9 */
/* move/from16 v8, v17 */
/* move-object/from16 v19, v9 */
} // .end local v9 # "fileName":Ljava/lang/String;
/* .local v19, "fileName":Ljava/lang/String; */
/* move-object v9, v10 */
/* move-object/from16 v17, v10 */
} // .end local v10 # "fileDir":Ljava/lang/String;
/* .local v17, "fileDir":Ljava/lang/String; */
/* move-object/from16 v10, v18 */
/* invoke-static/range {v2 ..v10}, Lcom/android/server/ScoutHelper;->captureLog(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;ZIZLjava/lang/String;Ljava/util/List;)V */
/* .line 877 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v12 ); // invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v11 ); // invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-object/from16 v2, v19 */
} // .end local v19 # "fileName":Ljava/lang/String;
/* .local v2, "fileName":Ljava/lang/String; */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "_0.zip"; // const-string v3, "_0.zip"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* move-object v13, v0 */
/* .line 880 */
} // .end local v2 # "fileName":Ljava/lang/String;
} // .end local v14 # "offlineLogDateFormat":Ljava/text/SimpleDateFormat;
} // .end local v15 # "formattedDate":Ljava/lang/String;
} // .end local v16 # "offlineLogDir":Ljava/io/File;
} // .end local v17 # "fileDir":Ljava/lang/String;
/* .line 878 */
/* :catch_0 */
/* move-exception v0 */
/* .line 879 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 881 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
} // .end method
public static void dumpUithreadPeriodHistoryMessage ( Long p0, Integer p1 ) {
/* .locals 6 */
/* .param p0, "anrTime" # J */
/* .param p2, "duration" # I */
/* .line 936 */
final String v0 = "MIUIScout ANR"; // const-string v0, "MIUIScout ANR"
int v1 = 0; // const/4 v1, 0x0
/* .line 938 */
/* .local v1, "monitor":Landroid/os/perfdebug/MessageMonitor; */
try { // :try_start_0
com.android.server.UiThread .get ( );
(( com.android.server.UiThread ) v2 ).getLooper ( ); // invoke-virtual {v2}, Lcom/android/server/UiThread;->getLooper()Landroid/os/Looper;
(( android.os.Looper ) v2 ).getMessageMonitor ( ); // invoke-virtual {v2}, Landroid/os/Looper;->getMessageMonitor()Landroid/os/perfdebug/MessageMonitor;
/* move-object v1, v2 */
/* .line 940 */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 941 */
(( android.os.perfdebug.MessageMonitor ) v1 ).getHistoryMsgInfoStringInPeriod ( p0, p1, p2 ); // invoke-virtual {v1, p0, p1, p2}, Landroid/os/perfdebug/MessageMonitor;->getHistoryMsgInfoStringInPeriod(JI)Ljava/util/List;
/* .line 942 */
/* .local v2, "historymsg":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
v4 = } // :goto_0
/* if-ge v3, v4, :cond_0 */
/* .line 943 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "get period history msg from android.ui:"; // const-string v5, "get period history msg from android.ui:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* check-cast v5, Ljava/lang/String; */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v4 );
/* .line 942 */
/* add-int/lit8 v3, v3, 0x1 */
} // .end local v3 # "i":I
} // :cond_0
/* .line 946 */
} // .end local v2 # "historymsg":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // :cond_1
final String v2 = "Can\'t dumpPeriodHistoryMessage because of null MessageMonitor"; // const-string v2, "Can\'t dumpPeriodHistoryMessage because of null MessageMonitor"
android.util.Slog .w ( v0,v2 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 950 */
} // :goto_1
/* .line 948 */
/* :catch_0 */
/* move-exception v2 */
/* .line 949 */
/* .local v2, "e":Ljava/lang/Exception; */
final String v3 = "AnrScout failed to get period history msg"; // const-string v3, "AnrScout failed to get period history msg"
android.util.Slog .w ( v0,v3,v2 );
/* .line 951 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_2
com.android.server.UiThread .get ( );
(( com.android.server.UiThread ) v0 ).getLooper ( ); // invoke-virtual {v0}, Lcom/android/server/UiThread;->getLooper()Landroid/os/Looper;
int v2 = 5; // const/4 v2, 0x5
(( android.os.Looper ) v0 ).printLoopInfo ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Looper;->printLoopInfo(I)V
/* .line 952 */
return;
} // .end method
public static java.io.File getBinderLogFile ( java.lang.String p0, Integer p1 ) {
/* .locals 5 */
/* .param p0, "tag" # Ljava/lang/String; */
/* .param p1, "pid" # I */
/* .line 422 */
int v0 = 0; // const/4 v0, 0x0
/* .line 423 */
/* .local v0, "fileBinderLog":Ljava/io/File; */
/* new-instance v1, Ljava/io/File; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "/dev/binderfs/binder_logs/proc_transaction/"; // const-string v3, "/dev/binderfs/binder_logs/proc_transaction/"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.String .valueOf ( p1 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 424 */
/* .local v1, "fileBinderMIUI":Ljava/io/File; */
/* new-instance v2, Ljava/io/File; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "/dev/binderfs/binder_logs/proc/"; // const-string v4, "/dev/binderfs/binder_logs/proc/"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.String .valueOf ( p1 );
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 425 */
/* .local v2, "fileBinderGKI":Ljava/io/File; */
v3 = (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 426 */
/* move-object v0, v1 */
/* .line 427 */
} // :cond_0
com.android.server.ScoutHelper .supportGKI ( );
v3 = (( java.lang.Boolean ) v3 ).booleanValue ( ); // invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z
if ( v3 != null) { // if-eqz v3, :cond_1
v3 = (( java.io.File ) v2 ).exists ( ); // invoke-virtual {v2}, Ljava/io/File;->exists()Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 428 */
/* move-object v0, v2 */
/* .line 430 */
} // :cond_1
final String v3 = "gki binder logfs or miui binder logfs are not exist"; // const-string v3, "gki binder logfs or miui binder logfs are not exist"
android.util.Slog .w ( p0,v3 );
/* .line 432 */
} // :goto_0
} // .end method
public static java.lang.StackTraceElement getMiuiStackTraceByTid ( Integer p0 ) {
/* .locals 8 */
/* .param p0, "tid" # I */
/* .line 920 */
int v0 = 0; // const/4 v0, 0x0
/* .line 923 */
/* .local v0, "st":[Ljava/lang/StackTraceElement; */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
final String v2 = "dalvik.system.VMStack"; // const-string v2, "dalvik.system.VMStack"
java.lang.Class .forName ( v2 );
/* .line 924 */
/* .local v2, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
final String v3 = "getMiuiStackTraceByTid"; // const-string v3, "getMiuiStackTraceByTid"
int v4 = 1; // const/4 v4, 0x1
/* new-array v5, v4, [Ljava/lang/Class; */
v6 = java.lang.Integer.TYPE;
int v7 = 0; // const/4 v7, 0x0
/* aput-object v6, v5, v7 */
(( java.lang.Class ) v2 ).getDeclaredMethod ( v3, v5 ); // invoke-virtual {v2, v3, v5}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 925 */
/* .local v3, "method":Ljava/lang/reflect/Method; */
(( java.lang.reflect.Method ) v3 ).setAccessible ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/reflect/Method;->setAccessible(Z)V
/* .line 926 */
/* new-array v4, v4, [Ljava/lang/Object; */
java.lang.Integer .valueOf ( p0 );
/* aput-object v5, v4, v7 */
(( java.lang.reflect.Method ) v3 ).invoke ( v1, v4 ); // invoke-virtual {v3, v1, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v4, [Ljava/lang/StackTraceElement; */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object v0, v4 */
/* .line 930 */
} // .end local v2 # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
} // .end local v3 # "method":Ljava/lang/reflect/Method;
/* nop */
/* .line 931 */
/* sget-boolean v1, Lcom/android/server/ScoutHelper;->ENABLED_SCOUT_DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "getMiuiStackTraceByTid tid:"; // const-string v2, "getMiuiStackTraceByTid tid:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p0 ); // invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "ScoutHelper"; // const-string v2, "ScoutHelper"
android.util.Slog .d ( v2,v1 );
/* .line 932 */
} // :cond_0
/* .line 927 */
/* :catch_0 */
/* move-exception v2 */
/* .line 928 */
/* .local v2, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
/* .line 929 */
} // .end method
private static java.io.File getMqsRamoopsFile ( ) {
/* .locals 4 */
/* .line 906 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/data/mqsas/temp/pstore/"; // const-string v1, "/data/mqsas/temp/pstore/"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 908 */
/* .local v0, "mqsFsDir":Ljava/io/File; */
try { // :try_start_0
v1 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
/* if-nez v1, :cond_0 */
v1 = (( java.io.File ) v0 ).isDirectory ( ); // invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z
/* if-nez v1, :cond_0 */
/* .line 909 */
(( java.io.File ) v0 ).mkdirs ( ); // invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z
/* .line 910 */
/* const/16 v1, 0x1fc */
int v2 = -1; // const/4 v2, -0x1
android.os.FileUtils .setPermissions ( v0,v1,v2,v2 );
/* .line 912 */
} // :cond_0
/* new-instance v1, Ljava/io/File; */
(( java.io.File ) v0 ).getAbsolutePath ( ); // invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
final String v3 = "console-ramoops"; // const-string v3, "console-ramoops"
/* invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 913 */
/* :catch_0 */
/* move-exception v1 */
/* .line 914 */
/* .local v1, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 916 */
} // .end local v1 # "e":Ljava/lang/Exception;
int v1 = 0; // const/4 v1, 0x0
} // .end method
public static Integer getOomAdjOfPid ( java.lang.String p0, Integer p1 ) {
/* .locals 5 */
/* .param p0, "tag" # Ljava/lang/String; */
/* .param p1, "Pid" # I */
/* .line 768 */
/* const/16 v0, -0x3e8 */
/* .line 769 */
/* .local v0, "adj":I */
try { // :try_start_0
/* new-instance v1, Ljava/io/BufferedReader; */
/* new-instance v2, Ljava/io/FileReader; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "/proc/"; // const-string v4, "/proc/"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 770 */
java.lang.String .valueOf ( p1 );
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = "/oom_score_adj"; // const-string v4, "/oom_score_adj"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v2, v3}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V */
/* invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 771 */
/* .local v1, "adjReader":Ljava/io/BufferedReader; */
try { // :try_start_1
(( java.io.BufferedReader ) v1 ).readLine ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
v2 = java.lang.Integer .parseInt ( v2 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* move v0, v2 */
/* .line 772 */
try { // :try_start_2
(( java.io.BufferedReader ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 774 */
} // .end local v1 # "adjReader":Ljava/io/BufferedReader;
/* .line 769 */
/* .restart local v1 # "adjReader":Ljava/io/BufferedReader; */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_3
(( java.io.BufferedReader ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v3 */
try { // :try_start_4
(( java.lang.Throwable ) v2 ).addSuppressed ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "adj":I
} // .end local p0 # "tag":Ljava/lang/String;
} // .end local p1 # "Pid":I
} // :goto_0
/* throw v2 */
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 772 */
} // .end local v1 # "adjReader":Ljava/io/BufferedReader;
/* .restart local v0 # "adj":I */
/* .restart local p0 # "tag":Ljava/lang/String; */
/* .restart local p1 # "Pid":I */
/* :catch_0 */
/* move-exception v1 */
/* .line 773 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v2 = "Check is java or native process Error:"; // const-string v2, "Check is java or native process Error:"
android.util.Slog .w ( p0,v2,v1 );
/* .line 775 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_1
} // .end method
public static com.android.server.ScoutHelper$ProcStatus getProcStatus ( Integer p0 ) {
/* .locals 7 */
/* .param p0, "Pid" # I */
/* .line 990 */
v0 = com.android.server.ScoutHelper.STATUS_KEYS;
/* array-length v1, v0 */
/* new-array v1, v1, [J */
/* .line 991 */
/* .local v1, "output":[J */
int v2 = 0; // const/4 v2, 0x0
/* const-wide/16 v3, -0x1 */
/* aput-wide v3, v1, v2 */
/* .line 992 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "/proc/"; // const-string v6, "/proc/"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( p0 ); // invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = "/status"; // const-string v6, "/status"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.os.Process .readProcLines ( v5,v0,v1 );
/* .line 993 */
/* aget-wide v5, v1, v2 */
/* cmp-long v0, v5, v3 */
/* if-nez v0, :cond_0 */
/* .line 995 */
int v0 = 0; // const/4 v0, 0x0
/* .line 997 */
} // :cond_0
/* new-instance v0, Lcom/android/server/ScoutHelper$ProcStatus; */
/* invoke-direct {v0}, Lcom/android/server/ScoutHelper$ProcStatus;-><init>()V */
/* .line 998 */
/* .local v0, "procStatus":Lcom/android/server/ScoutHelper$ProcStatus; */
/* aget-wide v2, v1, v2 */
/* long-to-int v2, v2 */
/* iput v2, v0, Lcom/android/server/ScoutHelper$ProcStatus;->tracerPid:I */
/* .line 999 */
} // .end method
public static java.lang.String getProcessCmdline ( Integer p0 ) {
/* .locals 5 */
/* .param p0, "pid" # I */
/* .line 967 */
/* const-string/jumbo v0, "unknow" */
/* .line 968 */
/* .local v0, "cmdline":Ljava/lang/String; */
/* if-nez p0, :cond_0 */
/* .line 969 */
} // :cond_0
try { // :try_start_0
/* new-instance v1, Ljava/io/BufferedReader; */
/* new-instance v2, Ljava/io/FileReader; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "proc/"; // const-string v4, "proc/"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p0 ); // invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = "/cmdline"; // const-string v4, "/cmdline"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v2, v3}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V */
/* invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 970 */
/* .local v1, "cmdlineReader":Ljava/io/BufferedReader; */
try { // :try_start_1
(( java.io.BufferedReader ) v1 ).readLine ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
(( java.lang.String ) v2 ).trim ( ); // invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* move-object v0, v2 */
/* .line 971 */
try { // :try_start_2
(( java.io.BufferedReader ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 973 */
} // .end local v1 # "cmdlineReader":Ljava/io/BufferedReader;
/* .line 969 */
/* .restart local v1 # "cmdlineReader":Ljava/io/BufferedReader; */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_3
(( java.io.BufferedReader ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v3 */
try { // :try_start_4
(( java.lang.Throwable ) v2 ).addSuppressed ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "cmdline":Ljava/lang/String;
} // .end local p0 # "pid":I
} // :goto_0
/* throw v2 */
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 971 */
} // .end local v1 # "cmdlineReader":Ljava/io/BufferedReader;
/* .restart local v0 # "cmdline":Ljava/lang/String; */
/* .restart local p0 # "pid":I */
/* :catch_0 */
/* move-exception v1 */
/* .line 972 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Read process("; // const-string v3, "Read process("
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p0 ); // invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ") cmdline Error"; // const-string v3, ") cmdline Error"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "ScoutHelper"; // const-string v3, "ScoutHelper"
android.util.Slog .w ( v3,v2 );
/* .line 974 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_1
} // .end method
public static java.lang.String getProcessComm ( Integer p0, Integer p1 ) {
/* .locals 5 */
/* .param p0, "pid" # I */
/* .param p1, "tid" # I */
/* .line 955 */
/* const-string/jumbo v0, "unknow" */
/* .line 956 */
/* .local v0, "comm":Ljava/lang/String; */
/* if-nez p1, :cond_0 */
/* .line 957 */
} // :cond_0
/* if-ne p0, p1, :cond_1 */
com.android.server.ScoutHelper .getProcessCmdline ( p0 );
/* .line 958 */
} // :cond_1
try { // :try_start_0
/* new-instance v1, Ljava/io/BufferedReader; */
/* new-instance v2, Ljava/io/FileReader; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "proc/"; // const-string v4, "proc/"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = "/comm"; // const-string v4, "/comm"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v2, v3}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V */
/* invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 959 */
/* .local v1, "commReader":Ljava/io/BufferedReader; */
try { // :try_start_1
(( java.io.BufferedReader ) v1 ).readLine ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
(( java.lang.String ) v2 ).trim ( ); // invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* move-object v0, v2 */
/* .line 960 */
try { // :try_start_2
(( java.io.BufferedReader ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 962 */
} // .end local v1 # "commReader":Ljava/io/BufferedReader;
/* .line 958 */
/* .restart local v1 # "commReader":Ljava/io/BufferedReader; */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_3
(( java.io.BufferedReader ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v3 */
try { // :try_start_4
(( java.lang.Throwable ) v2 ).addSuppressed ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "comm":Ljava/lang/String;
} // .end local p0 # "pid":I
} // .end local p1 # "tid":I
} // :goto_0
/* throw v2 */
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 960 */
} // .end local v1 # "commReader":Ljava/io/BufferedReader;
/* .restart local v0 # "comm":Ljava/lang/String; */
/* .restart local p0 # "pid":I */
/* .restart local p1 # "tid":I */
/* :catch_0 */
/* move-exception v1 */
/* .line 961 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Read process("; // const-string v3, "Read process("
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ") comm Error"; // const-string v3, ") comm Error"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "ScoutHelper"; // const-string v3, "ScoutHelper"
android.util.Slog .w ( v3,v2 );
/* .line 963 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_1
} // .end method
public static java.lang.String getPsInfo ( Integer p0 ) {
/* .locals 11 */
/* .param p0, "mode" # I */
/* .line 1003 */
final String v0 = ""; // const-string v0, ""
final String v1 = "close file stream error"; // const-string v1, "close file stream error"
final String v2 = "ScoutHelper"; // const-string v2, "ScoutHelper"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 1005 */
/* .local v3, "psInfo":Ljava/lang/StringBuilder; */
/* packed-switch p0, :pswitch_data_0 */
/* .line 1011 */
final String v4 = " -A"; // const-string v4, " -A"
/* .local v4, "args":Ljava/lang/String; */
/* .line 1007 */
} // .end local v4 # "args":Ljava/lang/String;
/* :pswitch_0 */
final String v4 = " -AT"; // const-string v4, " -AT"
/* .line 1008 */
/* .restart local v4 # "args":Ljava/lang/String; */
/* nop */
/* .line 1013 */
} // :goto_0
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "ps"; // const-string v6, "ps"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1014 */
/* .local v5, "command":Ljava/lang/String; */
int v6 = 0; // const/4 v6, 0x0
/* .line 1016 */
/* .local v6, "reader":Ljava/io/BufferedReader; */
int v7 = 0; // const/4 v7, 0x0
/* .line 1018 */
/* .local v7, "in":Ljava/io/InputStreamReader; */
try { // :try_start_0
java.lang.Runtime .getRuntime ( );
(( java.lang.Runtime ) v8 ).exec ( v5 ); // invoke-virtual {v8, v5}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_7 */
/* .line 1022 */
/* .local v8, "process":Ljava/lang/Process; */
/* nop */
/* .line 1024 */
try { // :try_start_1
/* new-instance v9, Ljava/io/InputStreamReader; */
(( java.lang.Process ) v8 ).getInputStream ( ); // invoke-virtual {v8}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;
/* invoke-direct {v9, v10}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V */
/* move-object v7, v9 */
/* .line 1025 */
/* new-instance v9, Ljava/io/BufferedReader; */
/* invoke-direct {v9, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* move-object v6, v9 */
/* .line 1027 */
} // :goto_1
(( java.io.BufferedReader ) v6 ).readLine ( ); // invoke-virtual {v6}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v10, v9 */
/* .local v10, "line":Ljava/lang/String; */
if ( v9 != null) { // if-eqz v9, :cond_0
/* .line 1028 */
(( java.lang.StringBuilder ) v3 ).append ( v10 ); // invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1029 */
/* const/16 v9, 0xa */
(( java.lang.StringBuilder ) v3 ).append ( v9 ); // invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_2 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1035 */
} // .end local v10 # "line":Ljava/lang/String;
} // :cond_0
try { // :try_start_2
(( java.io.BufferedReader ) v6 ).close ( ); // invoke-virtual {v6}, Ljava/io/BufferedReader;->close()V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 1038 */
/* .line 1036 */
/* :catch_0 */
/* move-exception v9 */
/* .line 1037 */
/* .local v9, "e":Ljava/io/IOException; */
android.util.Slog .e ( v2,v1,v9 );
/* .line 1040 */
} // .end local v9 # "e":Ljava/io/IOException;
} // :goto_2
try { // :try_start_3
(( java.io.InputStreamReader ) v7 ).close ( ); // invoke-virtual {v7}, Ljava/io/InputStreamReader;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_1 */
/* .line 1041 */
/* :catch_1 */
/* move-exception v9 */
/* .line 1034 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 1031 */
/* :catch_2 */
/* move-exception v9 */
/* .line 1032 */
/* .restart local v9 # "e":Ljava/io/IOException; */
try { // :try_start_4
final String v10 = "io error when read stream"; // const-string v10, "io error when read stream"
android.util.Slog .e ( v2,v10,v9 );
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* .line 1035 */
} // .end local v9 # "e":Ljava/io/IOException;
if ( v6 != null) { // if-eqz v6, :cond_1
try { // :try_start_5
(( java.io.BufferedReader ) v6 ).close ( ); // invoke-virtual {v6}, Ljava/io/BufferedReader;->close()V
/* :try_end_5 */
/* .catch Ljava/io/IOException; {:try_start_5 ..:try_end_5} :catch_3 */
/* .line 1036 */
/* :catch_3 */
/* move-exception v9 */
/* .line 1037 */
/* .restart local v9 # "e":Ljava/io/IOException; */
android.util.Slog .e ( v2,v1,v9 );
/* .line 1038 */
} // .end local v9 # "e":Ljava/io/IOException;
} // :cond_1
} // :goto_3
/* nop */
/* .line 1040 */
} // :goto_4
if ( v7 != null) { // if-eqz v7, :cond_2
try { // :try_start_6
(( java.io.InputStreamReader ) v7 ).close ( ); // invoke-virtual {v7}, Ljava/io/InputStreamReader;->close()V
/* :try_end_6 */
/* .catch Ljava/io/IOException; {:try_start_6 ..:try_end_6} :catch_4 */
/* .line 1041 */
/* :catch_4 */
/* move-exception v9 */
/* .line 1042 */
/* .restart local v9 # "e":Ljava/io/IOException; */
} // :goto_5
android.util.Slog .e ( v2,v1,v9 );
/* .line 1043 */
} // .end local v9 # "e":Ljava/io/IOException;
} // :cond_2
} // :goto_6
/* nop */
/* .line 1044 */
} // :goto_7
(( java.lang.Process ) v8 ).destroy ( ); // invoke-virtual {v8}, Ljava/lang/Process;->destroy()V
/* .line 1045 */
/* nop */
/* .line 1046 */
v1 = (( java.lang.StringBuilder ) v3 ).length ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I
/* if-lez v1, :cond_3 */
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // :cond_3
/* .line 1035 */
} // :goto_8
if ( v6 != null) { // if-eqz v6, :cond_4
try { // :try_start_7
(( java.io.BufferedReader ) v6 ).close ( ); // invoke-virtual {v6}, Ljava/io/BufferedReader;->close()V
/* :try_end_7 */
/* .catch Ljava/io/IOException; {:try_start_7 ..:try_end_7} :catch_5 */
/* .line 1036 */
/* :catch_5 */
/* move-exception v9 */
/* .line 1037 */
/* .restart local v9 # "e":Ljava/io/IOException; */
android.util.Slog .e ( v2,v1,v9 );
/* .line 1038 */
} // .end local v9 # "e":Ljava/io/IOException;
} // :cond_4
} // :goto_9
/* nop */
/* .line 1040 */
} // :goto_a
if ( v7 != null) { // if-eqz v7, :cond_5
try { // :try_start_8
(( java.io.InputStreamReader ) v7 ).close ( ); // invoke-virtual {v7}, Ljava/io/InputStreamReader;->close()V
/* :try_end_8 */
/* .catch Ljava/io/IOException; {:try_start_8 ..:try_end_8} :catch_6 */
/* .line 1041 */
/* :catch_6 */
/* move-exception v9 */
/* .line 1042 */
/* .restart local v9 # "e":Ljava/io/IOException; */
android.util.Slog .e ( v2,v1,v9 );
/* .line 1043 */
} // .end local v9 # "e":Ljava/io/IOException;
} // :cond_5
} // :goto_b
/* nop */
/* .line 1044 */
} // :goto_c
(( java.lang.Process ) v8 ).destroy ( ); // invoke-virtual {v8}, Ljava/lang/Process;->destroy()V
/* .line 1045 */
/* throw v0 */
/* .line 1019 */
} // .end local v8 # "process":Ljava/lang/Process;
/* :catch_7 */
/* move-exception v1 */
/* .line 1020 */
/* .local v1, "e":Ljava/io/IOException; */
final String v8 = "can\'t exec the cmd "; // const-string v8, "can\'t exec the cmd "
android.util.Slog .e ( v2,v8,v1 );
/* .line 1021 */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private static miui.mqsas.IMQSNative getmDaemon ( ) {
/* .locals 2 */
/* .line 169 */
v0 = com.android.server.ScoutHelper.mDaemon;
/* if-nez v0, :cond_0 */
/* .line 170 */
final String v0 = "miui.mqsas.IMQSNative"; // const-string v0, "miui.mqsas.IMQSNative"
android.os.ServiceManager .getService ( v0 );
miui.mqsas.IMQSNative$Stub .asInterface ( v0 );
/* .line 171 */
/* if-nez v0, :cond_0 */
/* .line 172 */
final String v0 = "ScoutHelper"; // const-string v0, "ScoutHelper"
final String v1 = "mqsasd not available!"; // const-string v1, "mqsasd not available!"
android.util.Slog .e ( v0,v1 );
/* .line 175 */
} // :cond_0
v0 = com.android.server.ScoutHelper.mDaemon;
} // .end method
public static Boolean isDebugpolicyed ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p0, "tag" # Ljava/lang/String; */
/* .line 783 */
final String v0 = "ro.boot.dp"; // const-string v0, "ro.boot.dp"
/* const-string/jumbo v1, "unknown" */
android.os.SystemProperties .get ( v0,v1 );
/* .line 784 */
/* .local v0, "dp":Ljava/lang/String; */
final String v1 = "0xB"; // const-string v1, "0xB"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_1 */
final String v1 = "1"; // const-string v1, "1"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_1 */
final String v1 = "2"; // const-string v1, "2"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 788 */
} // :cond_0
/* const-string/jumbo v1, "this device didn\'t flash Debugpolicy" */
android.util.Slog .i ( p0,v1 );
/* .line 789 */
int v1 = 0; // const/4 v1, 0x0
/* .line 785 */
} // :cond_1
} // :goto_0
/* const-string/jumbo v1, "this device has falshed Debugpolicy" */
android.util.Slog .i ( p0,v1 );
/* .line 786 */
int v1 = 1; // const/4 v1, 0x1
} // .end method
public static Boolean isEnabelPanicDThread ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p0, "tag" # Ljava/lang/String; */
/* .line 779 */
/* sget-boolean v0, Lcom/android/server/ScoutHelper;->PANIC_D_THREAD:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = com.android.server.ScoutHelper .isDebugpolicyed ( p0 );
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public static void printfProcBinderInfo ( Integer p0, java.lang.String p1 ) {
/* .locals 5 */
/* .param p0, "Pid" # I */
/* .param p1, "tag" # Ljava/lang/String; */
/* .line 731 */
final String v0 = "ScoutHelper"; // const-string v0, "ScoutHelper"
com.android.server.ScoutHelper .getBinderLogFile ( v0,p0 );
/* .line 732 */
/* .local v1, "fileBinderReader":Ljava/io/File; */
/* if-nez v1, :cond_0 */
/* .line 733 */
return;
/* .line 735 */
} // :cond_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Pid "; // const-string v3, "Pid "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p0 ); // invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " Binder Info:"; // const-string v3, " Binder Info:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( p1,v2 );
/* .line 736 */
try { // :try_start_0
/* new-instance v2, Ljava/io/BufferedReader; */
/* new-instance v3, Ljava/io/FileReader; */
/* invoke-direct {v3, v1}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V */
/* invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 737 */
/* .local v2, "BinderReader":Ljava/io/BufferedReader; */
int v3 = 0; // const/4 v3, 0x0
/* .line 738 */
/* .local v3, "binderTransactionInfo":Ljava/lang/String; */
} // :cond_1
} // :goto_0
try { // :try_start_1
(( java.io.BufferedReader ) v2 ).readLine ( ); // invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v3, v4 */
if ( v4 != null) { // if-eqz v4, :cond_5
/* .line 739 */
v4 = com.android.server.ScoutHelper.supportNewBinderLog;
v4 = (( java.lang.Boolean ) v4 ).booleanValue ( ); // invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 740 */
final String v4 = " outgoing transaction"; // const-string v4, " outgoing transaction"
v4 = (( java.lang.String ) v3 ).startsWith ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
/* if-nez v4, :cond_2 */
final String v4 = " incoming transaction"; // const-string v4, " incoming transaction"
/* .line 741 */
v4 = (( java.lang.String ) v3 ).startsWith ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
/* if-nez v4, :cond_2 */
final String v4 = " pending async transaction"; // const-string v4, " pending async transaction"
/* .line 742 */
v4 = (( java.lang.String ) v3 ).startsWith ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
/* if-nez v4, :cond_2 */
final String v4 = " pending transaction"; // const-string v4, " pending transaction"
/* .line 743 */
v4 = (( java.lang.String ) v3 ).startsWith ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 744 */
} // :cond_2
android.util.Slog .w ( p1,v3 );
/* .line 746 */
} // :cond_3
/* sget-boolean v4, Lcom/android/server/ScoutHelper;->SCOUT_BINDER_GKI:Z */
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 747 */
final String v4 = "MIUI"; // const-string v4, "MIUI"
v4 = (( java.lang.String ) v3 ).startsWith ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 748 */
android.util.Slog .w ( p1,v3 );
/* .line 751 */
} // :cond_4
android.util.Slog .w ( p1,v3 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 754 */
} // .end local v3 # "binderTransactionInfo":Ljava/lang/String;
} // :cond_5
try { // :try_start_2
(( java.io.BufferedReader ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 757 */
} // .end local v2 # "BinderReader":Ljava/io/BufferedReader;
/* .line 736 */
/* .restart local v2 # "BinderReader":Ljava/io/BufferedReader; */
/* :catchall_0 */
/* move-exception v3 */
try { // :try_start_3
(( java.io.BufferedReader ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v4 */
try { // :try_start_4
(( java.lang.Throwable ) v3 ).addSuppressed ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v1 # "fileBinderReader":Ljava/io/File;
} // .end local p0 # "Pid":I
} // .end local p1 # "tag":Ljava/lang/String;
} // :goto_1
/* throw v3 */
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 754 */
} // .end local v2 # "BinderReader":Ljava/io/BufferedReader;
/* .restart local v1 # "fileBinderReader":Ljava/io/File; */
/* .restart local p0 # "Pid":I */
/* .restart local p1 # "tag":Ljava/lang/String; */
/* :catch_0 */
/* move-exception v2 */
/* .line 755 */
/* .local v2, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
/* .line 756 */
final String v3 = "Read binder Proc transaction Error:"; // const-string v3, "Read binder Proc transaction Error:"
android.util.Slog .w ( v0,v3,v2 );
/* .line 758 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_2
return;
} // .end method
public static java.lang.String resumeBinderThreadFull ( java.lang.String p0, java.util.TreeMap p1 ) {
/* .locals 11 */
/* .param p0, "tag" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "Ljava/util/TreeMap<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/lang/Integer;", */
/* ">;)", */
/* "Ljava/lang/String;" */
/* } */
} // .end annotation
/* .line 700 */
/* .local p1, "inPidMap":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Ljava/lang/Integer;>;" */
/* sget-boolean v0, Lcom/android/server/ScoutHelper;->ENABLED_SCOUT_DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
final String v0 = "Debug: resumeBinderFull"; // const-string v0, "Debug: resumeBinderFull"
android.util.Slog .d ( p0,v0 );
/* .line 701 */
} // :cond_0
/* new-instance v0, Ljava/util/ArrayList; */
(( java.util.TreeMap ) p1 ).entrySet ( ); // invoke-virtual {p1}, Ljava/util/TreeMap;->entrySet()Ljava/util/Set;
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* .line 703 */
/* .local v0, "list":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;>;" */
/* new-instance v1, Lcom/android/server/ScoutHelper$1; */
/* invoke-direct {v1}, Lcom/android/server/ScoutHelper$1;-><init>()V */
java.util.Collections .sort ( v0,v1 );
/* .line 709 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 710 */
/* .local v1, "minfo":Ljava/lang/StringBuilder; */
final String v2 = "Incoming Binder Procsss Info:"; // const-string v2, "Incoming Binder Procsss Info:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 711 */
int v2 = 0; // const/4 v2, 0x0
/* .line 712 */
/* .local v2, "isKilled":Z */
v3 = com.android.server.ScoutHelper.sSkipProcs;
android.os.Process .getPidsForCommands ( v3 );
/* .line 713 */
/* .local v3, "skipPids":[I */
v5 = } // :goto_0
if ( v5 != null) { // if-eqz v5, :cond_2
/* check-cast v5, Ljava/util/Map$Entry; */
/* .line 714 */
/* .local v5, "mapping":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;" */
/* check-cast v6, Ljava/lang/Integer; */
v6 = (( java.lang.Integer ) v6 ).intValue ( ); // invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I
/* .line 715 */
/* .local v6, "inPid":I */
/* check-cast v7, Ljava/lang/Integer; */
v7 = (( java.lang.Integer ) v7 ).intValue ( ); // invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I
/* .line 716 */
/* .local v7, "count":I */
v8 = com.android.server.ScoutHelper .getOomAdjOfPid ( p0,v6 );
/* .line 717 */
/* .local v8, "oomAdj":I */
/* sget-boolean v9, Lcom/android/server/ScoutHelper;->BINDER_FULL_KILL_PROC:Z */
if ( v9 != null) { // if-eqz v9, :cond_1
/* const/16 v9, -0x320 */
/* if-lt v8, v9, :cond_1 */
/* if-nez v2, :cond_1 */
/* if-lez v6, :cond_1 */
/* .line 718 */
v9 = com.android.internal.util.ArrayUtils .contains ( v3,v6 );
/* if-nez v9, :cond_1 */
/* .line 719 */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "Pid("; // const-string v10, "Pid("
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v6 ); // invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v10 = ") adj("; // const-string v10, ") adj("
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v8 ); // invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v10 = ") is Killed, Because it use "; // const-string v10, ") is Killed, Because it use "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v7 ); // invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v10 = " binder thread of System_server"; // const-string v10, " binder thread of System_server"
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( p0,v9 );
/* .line 721 */
android.os.Process .killProcess ( v6 );
/* .line 722 */
int v2 = 1; // const/4 v2, 0x1
/* .line 724 */
} // :cond_1
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "\nPid "; // const-string v10, "\nPid "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v10 = "(adj : "; // const-string v10, "(adj : "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v8 ); // invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v10 = ") count "; // const-string v10, ") count "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v9 ); // invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 725 */
} // .end local v5 # "mapping":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
} // .end local v6 # "inPid":I
} // .end local v7 # "count":I
} // .end local v8 # "oomAdj":I
/* goto/16 :goto_0 */
/* .line 726 */
} // :cond_2
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( p0,v4 );
/* .line 727 */
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
public static void runCommand ( java.lang.String p0, java.lang.String p1, Integer p2 ) {
/* .locals 6 */
/* .param p0, "action" # Ljava/lang/String; */
/* .param p1, "params" # Ljava/lang/String; */
/* .param p2, "timeout" # I */
/* .line 802 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "runCommand action "; // const-string v1, "runCommand action "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " params "; // const-string v1, " params "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "ScoutHelper"; // const-string v1, "ScoutHelper"
android.util.Slog .e ( v1,v0 );
/* .line 803 */
com.android.server.ScoutHelper .getmDaemon ( );
/* .line 804 */
/* .local v0, "mClient":Lmiui/mqsas/IMQSNative; */
/* if-nez v0, :cond_0 */
/* .line 805 */
final String v2 = "runCommand no mqsasd!"; // const-string v2, "runCommand no mqsasd!"
android.util.Slog .e ( v1,v2 );
/* .line 806 */
return;
/* .line 808 */
} // :cond_0
if ( p0 != null) { // if-eqz p0, :cond_4
/* if-nez p1, :cond_1 */
/* .line 813 */
} // :cond_1
/* const/16 v2, 0x3c */
/* if-ge p2, v2, :cond_2 */
/* .line 814 */
/* const/16 v2, 0x3c */
/* .local v2, "cmd_timeout":I */
/* .line 816 */
} // .end local v2 # "cmd_timeout":I
} // :cond_2
/* move v2, p2 */
/* .line 820 */
/* .restart local v2 # "cmd_timeout":I */
} // :goto_0
v3 = try { // :try_start_0
/* .line 821 */
/* .local v3, "result":I */
/* if-gez v3, :cond_3 */
/* .line 822 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "runCommanxd Fail result = "; // const-string v5, "runCommanxd Fail result = "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v4 );
/* .line 824 */
} // :cond_3
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "runCommanxd result = "; // const-string v5, "runCommanxd result = "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v4 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 829 */
/* nop */
/* .line 830 */
return;
/* .line 825 */
} // .end local v3 # "result":I
/* :catch_0 */
/* move-exception v3 */
/* .line 826 */
/* .local v3, "e":Ljava/lang/Exception; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "runCommanxd Exception "; // const-string v5, "runCommanxd Exception "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v4 );
/* .line 827 */
(( java.lang.Exception ) v3 ).printStackTrace ( ); // invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V
/* .line 828 */
return;
/* .line 809 */
} // .end local v2 # "cmd_timeout":I
} // .end local v3 # "e":Ljava/lang/Exception;
} // :cond_4
} // :goto_1
final String v2 = "runCommand Wrong parameters!"; // const-string v2, "runCommand Wrong parameters!"
android.util.Slog .e ( v1,v2 );
/* .line 810 */
return;
} // .end method
private static java.lang.Boolean supportGKI ( ) {
/* .locals 1 */
/* .line 418 */
v0 = com.android.server.ScoutHelper.supportNewBinderLog;
v0 = (( java.lang.Boolean ) v0 ).booleanValue ( ); // invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
/* if-nez v0, :cond_1 */
/* sget-boolean v0, Lcom/android/server/ScoutHelper;->SCOUT_BINDER_GKI:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
java.lang.Boolean .valueOf ( v0 );
} // .end method
