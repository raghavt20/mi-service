.class public final Lcom/android/server/WatchdogStub$$;
.super Ljava/lang/Object;
.source "WatchdogStub$$.java"

# interfaces
.implements Lcom/miui/base/MiuiStubRegistry$ImplProviderManifest;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final collectImplProviders(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/miui/base/MiuiStubRegistry$ImplProvider<",
            "*>;>;)V"
        }
    .end annotation

    .line 24
    .local p1, "outProviders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/miui/base/MiuiStubRegistry$ImplProvider<*>;>;"
    new-instance v0, Lcom/android/server/ScoutSystemMonitor$Provider;

    invoke-direct {v0}, Lcom/android/server/ScoutSystemMonitor$Provider;-><init>()V

    const-string v1, "com.android.server.ScoutStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    new-instance v0, Lcom/android/server/ProcHunterImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/ProcHunterImpl$Provider;-><init>()V

    const-string v1, "com.android.server.ProcHunterStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    new-instance v0, Lcom/android/server/RescuePartyImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/RescuePartyImpl$Provider;-><init>()V

    const-string v1, "com.android.server.RescuePartyStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    new-instance v0, Lcom/android/server/BootKeeperStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/BootKeeperStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.BootKeeperStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    new-instance v0, Lcom/android/server/am/BinderProxyMonitorImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/am/BinderProxyMonitorImpl$Provider;-><init>()V

    const-string v1, "com.android.server.am.BinderProxyMonitor"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    new-instance v0, Lcom/android/server/PackageWatchdogImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/PackageWatchdogImpl$Provider;-><init>()V

    const-string v1, "com.android.server.PackageWatchdogStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    new-instance v0, Lcom/android/server/WatchdogImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/WatchdogImpl$Provider;-><init>()V

    const-string v1, "com.android.server.WatchdogStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    new-instance v0, Lcom/miui/server/stability/LockPerfImpl$Provider;

    invoke-direct {v0}, Lcom/miui/server/stability/LockPerfImpl$Provider;-><init>()V

    const-string v1, "com.android.server.LockPerfStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    new-instance v0, Lcom/android/server/MiuiFallbackHelper$Provider;

    invoke-direct {v0}, Lcom/android/server/MiuiFallbackHelper$Provider;-><init>()V

    const-string v1, "com.android.server.MiuiFallbackHelperStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    new-instance v0, Lcom/android/server/recoverysystem/RecoverySystemServiceImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/recoverysystem/RecoverySystemServiceImpl$Provider;-><init>()V

    const-string v1, "com.android.server.recoverysystem.RecoverySystemServiceStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    new-instance v0, Lcom/android/server/am/AppProfilerImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/am/AppProfilerImpl$Provider;-><init>()V

    const-string v1, "com.android.server.am.AppProfilerStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    new-instance v0, Lcom/android/server/wm/MiuiWindowMonitorImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/MiuiWindowMonitorImpl$Provider;-><init>()V

    const-string v1, "com.android.server.wm.MiuiWindowMonitorStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    new-instance v0, Lcom/android/server/miuibpf/MiuiBpfServiceImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/miuibpf/MiuiBpfServiceImpl$Provider;-><init>()V

    const-string v1, "com.android.server.miuibpf.MiuiBpfServiceStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    return-void
.end method
