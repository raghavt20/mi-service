.class final enum Lcom/android/server/BootKeeperStubImpl$BOOTKEEPER_STRATEGY;
.super Ljava/lang/Enum;
.source "BootKeeperStubImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/BootKeeperStubImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "BOOTKEEPER_STRATEGY"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/android/server/BootKeeperStubImpl$BOOTKEEPER_STRATEGY;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/server/BootKeeperStubImpl$BOOTKEEPER_STRATEGY;

.field public static final enum ALL:Lcom/android/server/BootKeeperStubImpl$BOOTKEEPER_STRATEGY;

.field public static final enum CLIP:Lcom/android/server/BootKeeperStubImpl$BOOTKEEPER_STRATEGY;


# direct methods
.method private static synthetic $values()[Lcom/android/server/BootKeeperStubImpl$BOOTKEEPER_STRATEGY;
    .locals 2

    .line 46
    sget-object v0, Lcom/android/server/BootKeeperStubImpl$BOOTKEEPER_STRATEGY;->ALL:Lcom/android/server/BootKeeperStubImpl$BOOTKEEPER_STRATEGY;

    sget-object v1, Lcom/android/server/BootKeeperStubImpl$BOOTKEEPER_STRATEGY;->CLIP:Lcom/android/server/BootKeeperStubImpl$BOOTKEEPER_STRATEGY;

    filled-new-array {v0, v1}, [Lcom/android/server/BootKeeperStubImpl$BOOTKEEPER_STRATEGY;

    move-result-object v0

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 47
    new-instance v0, Lcom/android/server/BootKeeperStubImpl$BOOTKEEPER_STRATEGY;

    const-string v1, "ALL"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/android/server/BootKeeperStubImpl$BOOTKEEPER_STRATEGY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/BootKeeperStubImpl$BOOTKEEPER_STRATEGY;->ALL:Lcom/android/server/BootKeeperStubImpl$BOOTKEEPER_STRATEGY;

    .line 48
    new-instance v0, Lcom/android/server/BootKeeperStubImpl$BOOTKEEPER_STRATEGY;

    const-string v1, "CLIP"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/android/server/BootKeeperStubImpl$BOOTKEEPER_STRATEGY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/BootKeeperStubImpl$BOOTKEEPER_STRATEGY;->CLIP:Lcom/android/server/BootKeeperStubImpl$BOOTKEEPER_STRATEGY;

    .line 46
    invoke-static {}, Lcom/android/server/BootKeeperStubImpl$BOOTKEEPER_STRATEGY;->$values()[Lcom/android/server/BootKeeperStubImpl$BOOTKEEPER_STRATEGY;

    move-result-object v0

    sput-object v0, Lcom/android/server/BootKeeperStubImpl$BOOTKEEPER_STRATEGY;->$VALUES:[Lcom/android/server/BootKeeperStubImpl$BOOTKEEPER_STRATEGY;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 46
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/server/BootKeeperStubImpl$BOOTKEEPER_STRATEGY;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .line 46
    const-class v0, Lcom/android/server/BootKeeperStubImpl$BOOTKEEPER_STRATEGY;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/server/BootKeeperStubImpl$BOOTKEEPER_STRATEGY;

    return-object v0
.end method

.method public static values()[Lcom/android/server/BootKeeperStubImpl$BOOTKEEPER_STRATEGY;
    .locals 1

    .line 46
    sget-object v0, Lcom/android/server/BootKeeperStubImpl$BOOTKEEPER_STRATEGY;->$VALUES:[Lcom/android/server/BootKeeperStubImpl$BOOTKEEPER_STRATEGY;

    invoke-virtual {v0}, [Lcom/android/server/BootKeeperStubImpl$BOOTKEEPER_STRATEGY;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/server/BootKeeperStubImpl$BOOTKEEPER_STRATEGY;

    return-object v0
.end method
