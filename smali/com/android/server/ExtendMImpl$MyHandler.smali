.class Lcom/android/server/ExtendMImpl$MyHandler;
.super Landroid/os/Handler;
.source "ExtendMImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/ExtendMImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/ExtendMImpl;


# direct methods
.method public constructor <init>(Lcom/android/server/ExtendMImpl;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 1337
    iput-object p1, p0, Lcom/android/server/ExtendMImpl$MyHandler;->this$0:Lcom/android/server/ExtendMImpl;

    .line 1338
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 1339
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .line 1343
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 1357
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/ExtendMImpl$MyHandler;->this$0:Lcom/android/server/ExtendMImpl;

    invoke-static {v0}, Lcom/android/server/ExtendMImpl;->-$$Nest$mtryToMarkPages(Lcom/android/server/ExtendMImpl;)Z

    .line 1358
    goto :goto_0

    .line 1354
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/ExtendMImpl$MyHandler;->this$0:Lcom/android/server/ExtendMImpl;

    invoke-static {v0}, Lcom/android/server/ExtendMImpl;->-$$Nest$mstartExtM(Lcom/android/server/ExtendMImpl;)V

    .line 1355
    goto :goto_0

    .line 1360
    :pswitch_2
    iget-object v0, p0, Lcom/android/server/ExtendMImpl$MyHandler;->this$0:Lcom/android/server/ExtendMImpl;

    invoke-static {v0}, Lcom/android/server/ExtendMImpl;->-$$Nest$fgetisFlushFinished(Lcom/android/server/ExtendMImpl;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1361
    const-string v0, "ExtM"

    const-string v1, "flush time out , stop it"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1362
    iget-object v0, p0, Lcom/android/server/ExtendMImpl$MyHandler;->this$0:Lcom/android/server/ExtendMImpl;

    invoke-static {v0}, Lcom/android/server/ExtendMImpl;->-$$Nest$mstopFlush(Lcom/android/server/ExtendMImpl;)V

    goto :goto_0

    .line 1351
    :pswitch_3
    iget-object v0, p0, Lcom/android/server/ExtendMImpl$MyHandler;->this$0:Lcom/android/server/ExtendMImpl;

    invoke-static {}, Lcom/android/server/ExtendMImpl;->-$$Nest$sfgetFLUSH_HIGH_LEVEL()I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/ExtendMImpl;->-$$Nest$mrunFlush(Lcom/android/server/ExtendMImpl;I)Z

    .line 1352
    goto :goto_0

    .line 1348
    :pswitch_4
    iget-object v0, p0, Lcom/android/server/ExtendMImpl$MyHandler;->this$0:Lcom/android/server/ExtendMImpl;

    invoke-static {}, Lcom/android/server/ExtendMImpl;->-$$Nest$sfgetFLUSH_MEDIUM_LEVEL()I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/ExtendMImpl;->-$$Nest$mrunFlush(Lcom/android/server/ExtendMImpl;I)Z

    .line 1349
    goto :goto_0

    .line 1345
    :pswitch_5
    iget-object v0, p0, Lcom/android/server/ExtendMImpl$MyHandler;->this$0:Lcom/android/server/ExtendMImpl;

    invoke-static {}, Lcom/android/server/ExtendMImpl;->-$$Nest$sfgetFLUSH_LOW_LEVEL()I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/ExtendMImpl;->-$$Nest$mrunFlush(Lcom/android/server/ExtendMImpl;I)Z

    .line 1346
    nop

    .line 1368
    :cond_0
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
