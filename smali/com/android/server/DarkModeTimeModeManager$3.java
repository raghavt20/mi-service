class com.android.server.DarkModeTimeModeManager$3 implements java.lang.Runnable {
	 /* .source "DarkModeTimeModeManager.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/DarkModeTimeModeManager;->enterSettingsFromNotification(Landroid/content/Context;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.DarkModeTimeModeManager this$0; //synthetic
final android.content.Context val$context; //synthetic
/* # direct methods */
 com.android.server.DarkModeTimeModeManager$3 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/DarkModeTimeModeManager; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 335 */
this.this$0 = p1;
this.val$context = p2;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 3 */
/* .line 338 */
v0 = this.val$context;
v0 = com.android.server.DarkModeTimeModeHelper .isDarkModeOpen ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 339 */
/* new-instance v0, Lcom/android/server/DarkModeStauesEvent; */
/* invoke-direct {v0}, Lcom/android/server/DarkModeStauesEvent;-><init>()V */
/* .line 340 */
final String v1 = "darkModeSuggest"; // const-string v1, "darkModeSuggest"
(( com.android.server.DarkModeStauesEvent ) v0 ).setEventName ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setEventName(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
/* .line 341 */
final String v1 = ""; // const-string v1, ""
(( com.android.server.DarkModeStauesEvent ) v0 ).setTip ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setTip(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
/* .line 342 */
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.DarkModeStauesEvent ) v0 ).setSuggestOpenInSetting ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setSuggestOpenInSetting(I)Lcom/android/server/DarkModeStauesEvent;
/* .line 343 */
/* .local v0, "event":Lcom/android/server/DarkModeStauesEvent; */
v1 = this.val$context;
com.android.server.DarkModeOneTrackHelper .uploadToOneTrack ( v1,v0 );
/* .line 346 */
} // .end local v0 # "event":Lcom/android/server/DarkModeStauesEvent;
} // :cond_0
v0 = this.val$context;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "enter_setting_by_notification"; // const-string v1, "enter_setting_by_notification"
int v2 = 0; // const/4 v2, 0x0
android.provider.Settings$System .putInt ( v0,v1,v2 );
/* .line 348 */
return;
} // .end method
