.class public Lcom/android/server/ForceDarkUiModeModeManager;
.super Ljava/lang/Object;
.source "ForceDarkUiModeModeManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/ForceDarkUiModeModeManager$AppDarkModeObserverRegistration;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field final mAppDarkModeObservers:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList<",
            "Landroid/app/IAppDarkModeObserver;",
            ">;"
        }
    .end annotation
.end field

.field private mDarkModeTimeModeManager:Lcom/android/server/DarkModeTimeModeManager;

.field private mForceDarkAppConfigProvider:Lcom/android/server/ForceDarkAppConfigProvider;

.field private mForceDarkAppListManager:Lcom/android/server/ForceDarkAppListManager;

.field private mForceDarkConfigData:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mForceDarkDebug:I

.field private mUiModeManagerService:Lcom/android/server/UiModeManagerService;


# direct methods
.method static bridge synthetic -$$Nest$fgetmUiModeManagerService(Lcom/android/server/ForceDarkUiModeModeManager;)Lcom/android/server/UiModeManagerService;
    .locals 0

    iget-object p0, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mUiModeManagerService:Lcom/android/server/UiModeManagerService;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mgetForceDarkAppConfig(Lcom/android/server/ForceDarkUiModeModeManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/ForceDarkUiModeModeManager;->getForceDarkAppConfig(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$sfgetTAG()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/server/ForceDarkUiModeModeManager;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 30
    const-class v0, Landroid/app/UiModeManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/ForceDarkUiModeModeManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/android/server/UiModeManagerService;)V
    .locals 2
    .param p1, "uiModeManagerService"    # Lcom/android/server/UiModeManagerService;

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    iput-object v0, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mAppDarkModeObservers:Landroid/os/RemoteCallbackList;

    .line 47
    iput-object p1, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mUiModeManagerService:Lcom/android/server/UiModeManagerService;

    .line 48
    const-string/jumbo v0, "sys.forcedark.debug"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mForceDarkDebug:I

    .line 49
    invoke-direct {p0}, Lcom/android/server/ForceDarkUiModeModeManager;->initForceDarkAppConfig()V

    .line 50
    new-instance v0, Lcom/android/server/ForceDarkAppListManager;

    invoke-virtual {p1}, Lcom/android/server/UiModeManagerService;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/android/server/ForceDarkAppListManager;-><init>(Landroid/content/Context;Lcom/android/server/ForceDarkUiModeModeManager;)V

    iput-object v0, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mForceDarkAppListManager:Lcom/android/server/ForceDarkAppListManager;

    .line 52
    new-instance v0, Lcom/android/server/DarkModeTimeModeManager;

    iget-object v1, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mUiModeManagerService:Lcom/android/server/UiModeManagerService;

    invoke-virtual {v1}, Lcom/android/server/UiModeManagerService;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/server/DarkModeTimeModeManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mDarkModeTimeModeManager:Lcom/android/server/DarkModeTimeModeManager;

    .line 54
    return-void
.end method

.method private dumpMiuiUiModeManagerStub()V
    .locals 3

    .line 251
    iget-object v0, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mUiModeManagerService:Lcom/android/server/UiModeManagerService;

    invoke-virtual {v0}, Lcom/android/server/UiModeManagerService;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "android.permission.DUMP"

    invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 253
    return-void

    .line 256
    :cond_0
    const-class v0, Landroid/app/UiModeManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mAppDarkModeObservers callback size = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mAppDarkModeObservers:Landroid/os/RemoteCallbackList;

    .line 257
    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->getRegisteredCallbackCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 256
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    return-void
.end method

.method private getForceDarkAppConfig(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 275
    iget-object v0, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mForceDarkAppConfigProvider:Lcom/android/server/ForceDarkAppConfigProvider;

    invoke-virtual {v0, p1}, Lcom/android/server/ForceDarkAppConfigProvider;->getForceDarkAppConfig(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private initForceDarkAppConfig()V
    .locals 2

    .line 261
    invoke-static {}, Lcom/android/server/ForceDarkAppConfigProvider;->getInstance()Lcom/android/server/ForceDarkAppConfigProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mForceDarkAppConfigProvider:Lcom/android/server/ForceDarkAppConfigProvider;

    .line 262
    new-instance v1, Lcom/android/server/ForceDarkUiModeModeManager$1;

    invoke-direct {v1, p0}, Lcom/android/server/ForceDarkUiModeModeManager$1;-><init>(Lcom/android/server/ForceDarkUiModeModeManager;)V

    invoke-virtual {v0, v1}, Lcom/android/server/ForceDarkAppConfigProvider;->setAppConfigChangeListener(Lcom/android/server/ForceDarkAppConfigProvider$ForceDarkAppConfigChangeListener;)V

    .line 272
    return-void
.end method

.method private isSystemUiProcess(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 300
    const-string v0, "com.android.systemui"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public dump(Ljava/io/PrintWriter;)V
    .locals 9
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 209
    iget v0, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mForceDarkDebug:I

    if-nez v0, :cond_0

    .line 210
    return-void

    .line 212
    :cond_0
    const-string v0, "ForceDarkUiModeManager"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 213
    const-string/jumbo v0, "sys.debug.darkmode.analysis"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    move v1, v2

    :cond_1
    sput-boolean v1, Lcom/android/server/DarkModeStatusTracker;->DEBUG:Z

    .line 214
    iget-object v0, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mUiModeManagerService:Lcom/android/server/UiModeManagerService;

    invoke-virtual {v0}, Lcom/android/server/UiModeManagerService;->getInnerLock()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 215
    :try_start_0
    iget-object v1, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mAppDarkModeObservers:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v1

    .line 216
    .local v1, "i":I
    :goto_0
    add-int/lit8 v2, v1, -0x1

    .end local v1    # "i":I
    .local v2, "i":I
    if-lez v1, :cond_4

    .line 217
    iget-object v1, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mAppDarkModeObservers:Landroid/os/RemoteCallbackList;

    .line 218
    invoke-virtual {v1, v2}, Landroid/os/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/ForceDarkUiModeModeManager$AppDarkModeObserverRegistration;

    .line 219
    .local v1, "registration":Lcom/android/server/ForceDarkUiModeModeManager$AppDarkModeObserverRegistration;
    iget-object v3, v1, Lcom/android/server/ForceDarkUiModeModeManager$AppDarkModeObserverRegistration;->mPackageName:Ljava/lang/String;

    if-eqz v3, :cond_3

    .line 220
    iget-object v3, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mAppDarkModeObservers:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Landroid/app/IAppDarkModeObserver;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 222
    .local v3, "observer":Landroid/app/IAppDarkModeObserver;
    :try_start_1
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 223
    .local v4, "bundle":Landroid/os/Bundle;
    invoke-interface {v3, v4}, Landroid/app/IAppDarkModeObserver;->onDump(Landroid/os/Bundle;)V

    .line 224
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "package: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v1, Lcom/android/server/ForceDarkUiModeModeManager$AppDarkModeObserverRegistration;->mPackageName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 225
    invoke-virtual {v4}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 226
    .local v6, "key":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ": "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 227
    .end local v6    # "key":Ljava/lang/String;
    goto :goto_1

    .line 228
    :cond_2
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 231
    .end local v4    # "bundle":Landroid/os/Bundle;
    goto :goto_2

    .line 229
    :catch_0
    move-exception v4

    .line 230
    .local v4, "e":Landroid/os/RemoteException;
    :try_start_2
    invoke-virtual {v4}, Landroid/os/RemoteException;->printStackTrace()V

    .line 233
    .end local v1    # "registration":Lcom/android/server/ForceDarkUiModeModeManager$AppDarkModeObserverRegistration;
    .end local v3    # "observer":Landroid/app/IAppDarkModeObserver;
    .end local v4    # "e":Landroid/os/RemoteException;
    :cond_3
    :goto_2
    move v1, v2

    goto :goto_0

    .line 234
    :cond_4
    iget-object v1, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mAppDarkModeObservers:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 235
    .end local v2    # "i":I
    monitor-exit v0

    .line 236
    return-void

    .line 235
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public notifyAppListChanged()V
    .locals 5

    .line 279
    iget-object v0, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mUiModeManagerService:Lcom/android/server/UiModeManagerService;

    invoke-virtual {v0}, Lcom/android/server/UiModeManagerService;->getInnerLock()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 280
    :try_start_0
    iget-object v1, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mAppDarkModeObservers:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v1

    .line 282
    .local v1, "i":I
    :goto_0
    add-int/lit8 v2, v1, -0x1

    .end local v1    # "i":I
    .local v2, "i":I
    if-lez v1, :cond_1

    .line 283
    iget-object v1, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mAppDarkModeObservers:Landroid/os/RemoteCallbackList;

    .line 284
    invoke-virtual {v1, v2}, Landroid/os/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/ForceDarkUiModeModeManager$AppDarkModeObserverRegistration;

    .line 285
    .local v1, "registration":Lcom/android/server/ForceDarkUiModeModeManager$AppDarkModeObserverRegistration;
    iget-object v3, v1, Lcom/android/server/ForceDarkUiModeModeManager$AppDarkModeObserverRegistration;->mPackageName:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/android/server/ForceDarkUiModeModeManager;->isSystemUiProcess(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 286
    iget-object v3, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mAppDarkModeObservers:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Landroid/app/IAppDarkModeObserver;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 288
    .local v3, "observer":Landroid/app/IAppDarkModeObserver;
    :try_start_1
    iget-object v4, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mForceDarkAppListManager:Lcom/android/server/ForceDarkAppListManager;

    invoke-virtual {v4}, Lcom/android/server/ForceDarkAppListManager;->getAllForceDarkMapForSplashScreen()Ljava/util/HashMap;

    move-result-object v4

    .line 289
    .local v4, "forceDarkForSplashScreen":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface {v3, v4}, Landroid/app/IAppDarkModeObserver;->onAppSplashScreenChanged(Ljava/util/Map;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 292
    .end local v4    # "forceDarkForSplashScreen":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    goto :goto_1

    .line 290
    :catch_0
    move-exception v4

    .line 291
    .local v4, "e":Landroid/os/RemoteException;
    :try_start_2
    invoke-virtual {v4}, Landroid/os/RemoteException;->printStackTrace()V

    .line 294
    .end local v1    # "registration":Lcom/android/server/ForceDarkUiModeModeManager$AppDarkModeObserverRegistration;
    .end local v3    # "observer":Landroid/app/IAppDarkModeObserver;
    .end local v4    # "e":Landroid/os/RemoteException;
    :cond_0
    :goto_1
    move v1, v2

    goto :goto_0

    .line 295
    :cond_1
    iget-object v1, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mAppDarkModeObservers:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 296
    .end local v2    # "i":I
    monitor-exit v0

    .line 297
    return-void

    .line 296
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public onBootPhase(I)V
    .locals 3
    .param p1, "phase"    # I

    .line 142
    const/16 v0, 0x226

    if-ne p1, v0, :cond_0

    .line 143
    const-string v0, "forceDarkCouldData"

    const-string v1, "onBootPhase activity manager ready"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    iget-object v0, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mForceDarkAppListManager:Lcom/android/server/ForceDarkAppListManager;

    iget-object v1, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mUiModeManagerService:Lcom/android/server/UiModeManagerService;

    invoke-virtual {v1}, Lcom/android/server/UiModeManagerService;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/android/server/ForceDarkAppListManager;->onBootPhase(ILandroid/content/Context;)V

    .line 146
    iget-object v0, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mForceDarkAppConfigProvider:Lcom/android/server/ForceDarkAppConfigProvider;

    iget-object v1, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mUiModeManagerService:Lcom/android/server/UiModeManagerService;

    invoke-virtual {v1}, Lcom/android/server/UiModeManagerService;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/android/server/ForceDarkAppConfigProvider;->onBootPhase(ILandroid/content/Context;)V

    .line 147
    invoke-static {}, Landroid/view/ForceDarkHelperStub;->getInstance()Landroid/view/ForceDarkHelperStub;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mUiModeManagerService:Lcom/android/server/UiModeManagerService;

    invoke-virtual {v1}, Lcom/android/server/UiModeManagerService;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ForceDarkHelperStub;->initialize(Landroid/content/Context;)V

    .line 148
    iget-object v0, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mDarkModeTimeModeManager:Lcom/android/server/DarkModeTimeModeManager;

    iget-object v1, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mUiModeManagerService:Lcom/android/server/UiModeManagerService;

    invoke-virtual {v1}, Lcom/android/server/UiModeManagerService;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/DarkModeTimeModeManager;->onBootPhase(Landroid/content/Context;)V

    .line 149
    invoke-static {}, Lcom/android/server/DarkModeStatusTracker;->getIntance()Lcom/android/server/DarkModeStatusTracker;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mUiModeManagerService:Lcom/android/server/UiModeManagerService;

    invoke-virtual {v1}, Lcom/android/server/UiModeManagerService;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mForceDarkAppListManager:Lcom/android/server/ForceDarkAppListManager;

    invoke-virtual {v0, v1, v2}, Lcom/android/server/DarkModeStatusTracker;->init(Landroid/content/Context;Lcom/android/server/ForceDarkAppListManager;)V

    .line 152
    :cond_0
    return-void
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 6
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 61
    const/4 v0, 0x0

    const/4 v1, 0x1

    const-string v2, "android.app.IUiModeManager"

    packed-switch p1, :pswitch_data_0

    .line 137
    return v0

    .line 64
    :pswitch_0
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 65
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 66
    .local v0, "packageName":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readBoolean()Z

    move-result v2

    .line 67
    .local v2, "enable":Z
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 68
    .local v3, "userId":I
    invoke-virtual {p0, v0, v2, v3}, Lcom/android/server/ForceDarkUiModeModeManager;->setAppDarkModeEnable(Ljava/lang/String;ZI)V

    .line 69
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 70
    return v1

    .line 72
    .end local v0    # "packageName":Ljava/lang/String;
    .end local v2    # "enable":Z
    .end local v3    # "userId":I
    :pswitch_1
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 73
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 74
    .restart local v0    # "packageName":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 75
    .local v2, "userId":I
    iget-object v3, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mForceDarkAppListManager:Lcom/android/server/ForceDarkAppListManager;

    invoke-virtual {v3, v0, v2}, Lcom/android/server/ForceDarkAppListManager;->getAppDarkModeEnable(Ljava/lang/String;I)Z

    move-result v3

    .line 76
    .local v3, "appDarkModeEnable":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 77
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 78
    return v1

    .line 81
    .end local v0    # "packageName":Ljava/lang/String;
    .end local v2    # "userId":I
    .end local v3    # "appDarkModeEnable":Z
    :pswitch_2
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 82
    nop

    .line 83
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/IAppDarkModeObserver$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/IAppDarkModeObserver;

    move-result-object v0

    .line 84
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 82
    invoke-virtual {p0, v0, v2, v3, v4}, Lcom/android/server/ForceDarkUiModeModeManager;->registerAppDarkModeCallback(Landroid/app/IAppDarkModeObserver;Ljava/lang/String;II)V

    .line 85
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 86
    return v1

    .line 89
    :pswitch_3
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 90
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 91
    .restart local v0    # "packageName":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 92
    invoke-direct {p0, v0}, Lcom/android/server/ForceDarkUiModeModeManager;->getForceDarkAppConfig(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 93
    .local v2, "config":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    const-string/jumbo v4, "systemserver package:"

    if-eqz v3, :cond_0

    .line 94
    sget-object v3, Lcom/android/server/ForceDarkUiModeModeManager;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " null"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 96
    :cond_0
    sget-object v3, Lcom/android/server/ForceDarkUiModeModeManager;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " configLength:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 97
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 96
    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    :goto_0
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 100
    return v1

    .line 103
    .end local v0    # "packageName":Ljava/lang/String;
    .end local v2    # "config":Ljava/lang/String;
    :pswitch_4
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 104
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    .line 105
    .local v2, "appLastUpdateTime":J
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 106
    .local v4, "userId":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 107
    iget-object v5, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mForceDarkAppListManager:Lcom/android/server/ForceDarkAppListManager;

    invoke-virtual {v5, v2, v3, v4}, Lcom/android/server/ForceDarkAppListManager;->getDarkModeAppList(JI)Lcom/miui/darkmode/DarkModeAppData;

    move-result-object v5

    .line 108
    .local v5, "darkModeAppData":Lcom/miui/darkmode/DarkModeAppData;
    invoke-virtual {p3, v5, v0}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 109
    return v1

    .line 111
    .end local v2    # "appLastUpdateTime":J
    .end local v4    # "userId":I
    .end local v5    # "darkModeAppData":Lcom/miui/darkmode/DarkModeAppData;
    :pswitch_5
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 112
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 113
    .restart local v0    # "packageName":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 114
    iget-object v2, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mForceDarkAppListManager:Lcom/android/server/ForceDarkAppListManager;

    invoke-virtual {v2, v0}, Lcom/android/server/ForceDarkAppListManager;->getAppForceDarkOrigin(Ljava/lang/String;)Z

    move-result v2

    .line 115
    .local v2, "forceDarkOriginState":Z
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 116
    return v1

    .line 125
    .end local v0    # "packageName":Ljava/lang/String;
    .end local v2    # "forceDarkOriginState":Z
    :pswitch_6
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 126
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 127
    .restart local v0    # "packageName":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 128
    .local v2, "userId":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 129
    iget-object v3, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mForceDarkAppListManager:Lcom/android/server/ForceDarkAppListManager;

    invoke-virtual {v3, v0, v2}, Lcom/android/server/ForceDarkAppListManager;->getInterceptRelaunchType(Ljava/lang/String;I)I

    move-result v3

    .line 130
    .local v3, "interceptType":I
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 131
    return v1

    .line 119
    .end local v0    # "packageName":Ljava/lang/String;
    .end local v2    # "userId":I
    .end local v3    # "interceptType":I
    :pswitch_7
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 120
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 121
    iget-object v0, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mForceDarkAppListManager:Lcom/android/server/ForceDarkAppListManager;

    invoke-virtual {v0}, Lcom/android/server/ForceDarkAppListManager;->getAllForceDarkMapForSplashScreen()Ljava/util/HashMap;

    move-result-object v0

    .line 122
    .local v0, "forceDarkForSplashScreen":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 123
    return v1

    nop

    :pswitch_data_0
    .packed-switch 0xfffff7
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public registerAppDarkModeCallback(Landroid/app/IAppDarkModeObserver;Ljava/lang/String;II)V
    .locals 5
    .param p1, "observer"    # Landroid/app/IAppDarkModeObserver;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "processId"    # I
    .param p4, "userId"    # I

    .line 186
    if-eqz p1, :cond_3

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 191
    :cond_0
    iget-object v0, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mUiModeManagerService:Lcom/android/server/UiModeManagerService;

    invoke-virtual {v0}, Lcom/android/server/UiModeManagerService;->getInnerLock()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 192
    :try_start_0
    iget-object v1, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mAppDarkModeObservers:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v1

    .line 193
    .local v1, "i":I
    :goto_0
    add-int/lit8 v2, v1, -0x1

    .end local v1    # "i":I
    .local v2, "i":I
    if-lez v1, :cond_2

    .line 194
    iget-object v1, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mAppDarkModeObservers:Landroid/os/RemoteCallbackList;

    .line 195
    invoke-virtual {v1, v2}, Landroid/os/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/ForceDarkUiModeModeManager$AppDarkModeObserverRegistration;

    .line 196
    .local v1, "registration":Lcom/android/server/ForceDarkUiModeModeManager$AppDarkModeObserverRegistration;
    iget-object v3, v1, Lcom/android/server/ForceDarkUiModeModeManager$AppDarkModeObserverRegistration;->mPackageName:Ljava/lang/String;

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget v3, v1, Lcom/android/server/ForceDarkUiModeModeManager$AppDarkModeObserverRegistration;->mProcessId:I

    if-ne v3, p3, :cond_1

    iget v3, v1, Lcom/android/server/ForceDarkUiModeModeManager$AppDarkModeObserverRegistration;->mUserId:I

    if-ne v3, p4, :cond_1

    .line 199
    iget-object v3, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mAppDarkModeObservers:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v4

    check-cast v4, Landroid/app/IAppDarkModeObserver;

    invoke-virtual {v3, v4}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    .line 201
    .end local v1    # "registration":Lcom/android/server/ForceDarkUiModeModeManager$AppDarkModeObserverRegistration;
    :cond_1
    move v1, v2

    goto :goto_0

    .line 202
    :cond_2
    iget-object v1, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mAppDarkModeObservers:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 203
    iget-object v1, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mAppDarkModeObservers:Landroid/os/RemoteCallbackList;

    new-instance v3, Lcom/android/server/ForceDarkUiModeModeManager$AppDarkModeObserverRegistration;

    invoke-direct {v3, p2, p3, p4}, Lcom/android/server/ForceDarkUiModeModeManager$AppDarkModeObserverRegistration;-><init>(Ljava/lang/String;II)V

    invoke-virtual {v1, p1, v3}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;Ljava/lang/Object;)Z

    .line 205
    nop

    .end local v2    # "i":I
    monitor-exit v0

    .line 206
    return-void

    .line 205
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 187
    :cond_3
    :goto_1
    sget-object v0, Lcom/android/server/ForceDarkUiModeModeManager;->TAG:Ljava/lang/String;

    const-string v1, "registAppDarkModeCallback param is null"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    return-void
.end method

.method public setAppDarkModeEnable(Ljava/lang/String;ZI)V
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "enable"    # Z
    .param p3, "userId"    # I

    .line 155
    iget-object v0, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mUiModeManagerService:Lcom/android/server/UiModeManagerService;

    invoke-virtual {v0}, Lcom/android/server/UiModeManagerService;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "android.permission.MODIFY_DAY_NIGHT_MODE"

    invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 157
    sget-object v0, Lcom/android/server/ForceDarkUiModeModeManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "setAppDarkModeEnable failed, requires MODIFY_DAY_NIGHT_MODE permission"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    return-void

    .line 162
    :cond_0
    iget-object v0, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mForceDarkAppListManager:Lcom/android/server/ForceDarkAppListManager;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/ForceDarkAppListManager;->setAppDarkModeEnable(Ljava/lang/String;ZI)V

    .line 164
    iget-object v0, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mUiModeManagerService:Lcom/android/server/UiModeManagerService;

    invoke-virtual {v0}, Lcom/android/server/UiModeManagerService;->getInnerLock()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 165
    :try_start_0
    iget-object v1, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mAppDarkModeObservers:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v1

    .line 166
    .local v1, "i":I
    :goto_0
    add-int/lit8 v2, v1, -0x1

    .end local v1    # "i":I
    .local v2, "i":I
    if-lez v1, :cond_2

    .line 167
    iget-object v1, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mAppDarkModeObservers:Landroid/os/RemoteCallbackList;

    .line 168
    invoke-virtual {v1, v2}, Landroid/os/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/ForceDarkUiModeModeManager$AppDarkModeObserverRegistration;

    .line 169
    .local v1, "registration":Lcom/android/server/ForceDarkUiModeModeManager$AppDarkModeObserverRegistration;
    iget-object v3, v1, Lcom/android/server/ForceDarkUiModeModeManager$AppDarkModeObserverRegistration;->mPackageName:Ljava/lang/String;

    if-eqz v3, :cond_1

    if-eqz p1, :cond_1

    iget-object v3, v1, Lcom/android/server/ForceDarkUiModeModeManager$AppDarkModeObserverRegistration;->mPackageName:Ljava/lang/String;

    .line 170
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 171
    iget-object v3, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mAppDarkModeObservers:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Landroid/app/IAppDarkModeObserver;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 173
    .local v3, "observer":Landroid/app/IAppDarkModeObserver;
    :try_start_1
    invoke-interface {v3, p2}, Landroid/app/IAppDarkModeObserver;->onAppDarkModeChanged(Z)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 176
    goto :goto_1

    .line 174
    :catch_0
    move-exception v4

    .line 175
    .local v4, "e":Landroid/os/RemoteException;
    :try_start_2
    invoke-virtual {v4}, Landroid/os/RemoteException;->printStackTrace()V

    .line 178
    .end local v1    # "registration":Lcom/android/server/ForceDarkUiModeModeManager$AppDarkModeObserverRegistration;
    .end local v3    # "observer":Landroid/app/IAppDarkModeObserver;
    .end local v4    # "e":Landroid/os/RemoteException;
    :cond_1
    :goto_1
    move v1, v2

    goto :goto_0

    .line 179
    :cond_2
    iget-object v1, p0, Lcom/android/server/ForceDarkUiModeModeManager;->mAppDarkModeObservers:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 180
    .end local v2    # "i":I
    monitor-exit v0

    .line 181
    return-void

    .line 180
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method
