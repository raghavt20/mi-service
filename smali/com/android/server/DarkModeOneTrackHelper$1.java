class com.android.server.DarkModeOneTrackHelper$1 implements java.lang.Runnable {
	 /* .source "DarkModeOneTrackHelper.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/DarkModeOneTrackHelper;->uploadToOneTrack(Landroid/content/Context;Lcom/android/server/DarkModeEvent;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final android.content.Context val$context; //synthetic
final com.android.server.DarkModeEvent val$newEvent; //synthetic
/* # direct methods */
 com.android.server.DarkModeOneTrackHelper$1 ( ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 93 */
this.val$context = p1;
this.val$newEvent = p2;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 4 */
/* .line 96 */
final String v0 = "DarkModeOneTrackHelper"; // const-string v0, "DarkModeOneTrackHelper"
/* new-instance v1, Landroid/content/Intent; */
final String v2 = "onetrack.action.TRACK_EVENT"; // const-string v2, "onetrack.action.TRACK_EVENT"
/* invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 97 */
/* .local v1, "intent":Landroid/content/Intent; */
final String v2 = "com.miui.analytics"; // const-string v2, "com.miui.analytics"
(( android.content.Intent ) v1 ).setPackage ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 98 */
final String v2 = "APP_ID"; // const-string v2, "APP_ID"
final String v3 = "31000000485"; // const-string v3, "31000000485"
(( android.content.Intent ) v1 ).putExtra ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 99 */
v2 = this.val$context;
(( android.content.Context ) v2 ).getPackageName ( ); // invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;
final String v3 = "PACKAGE"; // const-string v3, "PACKAGE"
(( android.content.Intent ) v1 ).putExtra ( v3, v2 ); // invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 100 */
v2 = this.val$newEvent;
com.android.server.DarkModeOneTrackHelper .-$$Nest$smupdateDarkModeIntent ( v1,v2 );
/* .line 102 */
try { // :try_start_0
v2 = this.val$context;
v3 = android.os.UserHandle.CURRENT;
(( android.content.Context ) v2 ).startServiceAsUser ( v1, v3 ); // invoke-virtual {v2, v1, v3}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
/* .line 103 */
/* const-string/jumbo v2, "uploadDataToOneTrack Success" */
android.util.Slog .d ( v0,v2 );
/* :try_end_0 */
/* .catch Ljava/lang/IllegalStateException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 106 */
/* .line 104 */
/* :catch_0 */
/* move-exception v2 */
/* .line 105 */
/* .local v2, "e":Ljava/lang/IllegalStateException; */
final String v3 = "Filed to upload DarkModeOneTrackInfo "; // const-string v3, "Filed to upload DarkModeOneTrackInfo "
android.util.Slog .e ( v0,v3,v2 );
/* .line 107 */
} // .end local v2 # "e":Ljava/lang/IllegalStateException;
} // :goto_0
return;
} // .end method
