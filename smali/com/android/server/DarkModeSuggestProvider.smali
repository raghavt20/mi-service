.class public Lcom/android/server/DarkModeSuggestProvider;
.super Ljava/lang/Object;
.source "DarkModeSuggestProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/DarkModeSuggestProvider$DarkModeSuggestObserver;
    }
.end annotation


# static fields
.field private static final DARK_MODE_DATA_DISABLE_REGION:Ljava/lang/String; = "dark_mode_data_disable_region"

.field private static final DARK_MODE_SUGGEST_MODULE_NAME:Ljava/lang/String; = "dark_mode_suggest"

.field private static final TAG:Ljava/lang/String;

.field private static volatile sDarkModeSuggestProvider:Lcom/android/server/DarkModeSuggestProvider;


# instance fields
.field private mDarkModeSuggestObserver:Landroid/database/ContentObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    const-class v0, Lcom/android/server/DarkModeSuggestProvider;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/DarkModeSuggestProvider;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/android/server/DarkModeSuggestProvider;
    .locals 2

    .line 38
    sget-object v0, Lcom/android/server/DarkModeSuggestProvider;->sDarkModeSuggestProvider:Lcom/android/server/DarkModeSuggestProvider;

    if-nez v0, :cond_1

    .line 39
    const-class v0, Lcom/android/server/DarkModeSuggestProvider;

    monitor-enter v0

    .line 40
    :try_start_0
    sget-object v1, Lcom/android/server/DarkModeSuggestProvider;->sDarkModeSuggestProvider:Lcom/android/server/DarkModeSuggestProvider;

    if-nez v1, :cond_0

    .line 41
    new-instance v1, Lcom/android/server/DarkModeSuggestProvider;

    invoke-direct {v1}, Lcom/android/server/DarkModeSuggestProvider;-><init>()V

    sput-object v1, Lcom/android/server/DarkModeSuggestProvider;->sDarkModeSuggestProvider:Lcom/android/server/DarkModeSuggestProvider;

    .line 43
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 45
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/DarkModeSuggestProvider;->sDarkModeSuggestProvider:Lcom/android/server/DarkModeSuggestProvider;

    return-object v0
.end method


# virtual methods
.method public registerDataObserver(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 86
    sget-object v0, Lcom/android/server/DarkModeSuggestProvider;->TAG:Ljava/lang/String;

    const-string v1, "registerDataObserver"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    new-instance v0, Lcom/android/server/DarkModeSuggestProvider$DarkModeSuggestObserver;

    invoke-static {}, Lcom/android/server/MiuiBgThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1}, Lcom/android/server/DarkModeSuggestProvider$DarkModeSuggestObserver;-><init>(Lcom/android/server/DarkModeSuggestProvider;Landroid/os/Handler;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/DarkModeSuggestProvider;->mDarkModeSuggestObserver:Landroid/database/ContentObserver;

    .line 88
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataNotifyUri()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/android/server/DarkModeSuggestProvider;->mDarkModeSuggestObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 90
    return-void
.end method

.method public unRegisterDataObserver(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 93
    sget-object v0, Lcom/android/server/DarkModeSuggestProvider;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "unRegisterDataObserver"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    iget-object v0, p0, Lcom/android/server/DarkModeSuggestProvider;->mDarkModeSuggestObserver:Landroid/database/ContentObserver;

    if-nez v0, :cond_0

    .line 95
    return-void

    .line 97
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/DarkModeSuggestProvider;->mDarkModeSuggestObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 98
    return-void
.end method

.method public updateCloudDataForDarkModeSuggest(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .line 50
    const-string v0, "dark_mode_suggest"

    .line 51
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v1, v0, v0, v2, v3}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataSingle(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    move-result-object v1

    .line 53
    .local v1, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 54
    sget-object v2, Lcom/android/server/DarkModeSuggestProvider;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "suggestData: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    invoke-virtual {v1}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v4

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    invoke-virtual {v0, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    .line 56
    .local v0, "suggestArray":Lorg/json/JSONObject;
    const-string/jumbo v3, "showDarkModeSuggest"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    invoke-static {p1, v3}, Lcom/android/server/DarkModeTimeModeHelper;->setDarkModeSuggestEnable(Landroid/content/Context;Z)V

    .line 57
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sHasGetSuggestFromCloud: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p1}, Lcom/android/server/DarkModeTimeModeHelper;->isDarkModeSuggestEnable(Landroid/content/Context;)Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 61
    .end local v0    # "suggestArray":Lorg/json/JSONObject;
    .end local v1    # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    :cond_0
    goto :goto_0

    .line 59
    :catch_0
    move-exception v0

    .line 60
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/android/server/DarkModeSuggestProvider;->TAG:Ljava/lang/String;

    const-string v2, "exception when updateCloudDataForDarkModeSuggest"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 62
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public updateCloudDataForDisableRegion(Landroid/content/Context;)Ljava/util/Set;
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 66
    const-string v0, "dark_mode_data_disable_region"

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 69
    .local v1, "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    nop

    .line 70
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 69
    const/4 v3, 0x0

    invoke-static {v2, v0, v0, v3}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 71
    .local v0, "data":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 72
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 73
    .local v2, "apps":Lorg/json/JSONArray;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 74
    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 73
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 77
    .end local v2    # "apps":Lorg/json/JSONArray;
    .end local v3    # "i":I
    :cond_0
    sget-object v2, Lcom/android/server/DarkModeSuggestProvider;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "uploadCloudDataForDisableRegion: CloudList: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 80
    nop

    .end local v0    # "data":Ljava/lang/String;
    goto :goto_1

    .line 78
    :catch_0
    move-exception v0

    .line 79
    .local v0, "e":Lorg/json/JSONException;
    sget-object v2, Lcom/android/server/DarkModeSuggestProvider;->TAG:Ljava/lang/String;

    const-string v3, "exception when updateDisableRegionList: "

    invoke-static {v2, v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 81
    .end local v0    # "e":Lorg/json/JSONException;
    :goto_1
    return-object v1
.end method
