.class public Lcom/android/server/DarkModeStatusTracker;
.super Ljava/lang/Object;
.source "DarkModeStatusTracker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/DarkModeStatusTracker$SettingsObserver;
    }
.end annotation


# static fields
.field public static DEBUG:Z = false

.field private static final ONE_MINUTE:J = 0xea60L

.field private static volatile sInstance:Lcom/android/server/DarkModeStatusTracker;


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mAlarmListener:Landroid/app/AlarmManager$OnAlarmListener;

.field private mAlarmManager:Landroid/app/AlarmManager;

.field private mContext:Landroid/content/Context;

.field private mDarkModeStatusHandler:Landroid/os/Handler;

.field private mDarkModeSwitchEvent:Lcom/android/server/DarkModeStauesEvent;

.field private mForceDarkAppListManager:Lcom/android/server/ForceDarkAppListManager;

.field private mPackageManager:Landroid/content/pm/PackageManager;

.field private mSettingsObserver:Landroid/database/ContentObserver;


# direct methods
.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/DarkModeStatusTracker;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/DarkModeStatusTracker;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$msetUploadDarkModeSwitchAlarm(Lcom/android/server/DarkModeStatusTracker;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/DarkModeStatusTracker;->setUploadDarkModeSwitchAlarm(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$muploadDarkModeStatusEvent(Lcom/android/server/DarkModeStatusTracker;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/DarkModeStatusTracker;->uploadDarkModeStatusEvent(Ljava/lang/String;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 76
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/server/DarkModeStatusTracker;->DEBUG:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    const-string v0, "DarkModeStatusTracker"

    iput-object v0, p0, Lcom/android/server/DarkModeStatusTracker;->TAG:Ljava/lang/String;

    .line 307
    new-instance v0, Lcom/android/server/DarkModeStatusTracker$2;

    invoke-direct {v0, p0}, Lcom/android/server/DarkModeStatusTracker$2;-><init>(Lcom/android/server/DarkModeStatusTracker;)V

    iput-object v0, p0, Lcom/android/server/DarkModeStatusTracker;->mAlarmListener:Landroid/app/AlarmManager$OnAlarmListener;

    return-void
.end method

.method public static getIntance()Lcom/android/server/DarkModeStatusTracker;
    .locals 2

    .line 89
    sget-object v0, Lcom/android/server/DarkModeStatusTracker;->sInstance:Lcom/android/server/DarkModeStatusTracker;

    if-nez v0, :cond_1

    .line 90
    const-class v0, Lcom/android/server/DarkModeStatusTracker;

    monitor-enter v0

    .line 91
    :try_start_0
    sget-object v1, Lcom/android/server/DarkModeStatusTracker;->sInstance:Lcom/android/server/DarkModeStatusTracker;

    if-nez v1, :cond_0

    .line 92
    new-instance v1, Lcom/android/server/DarkModeStatusTracker;

    invoke-direct {v1}, Lcom/android/server/DarkModeStatusTracker;-><init>()V

    sput-object v1, Lcom/android/server/DarkModeStatusTracker;->sInstance:Lcom/android/server/DarkModeStatusTracker;

    .line 94
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 96
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/DarkModeStatusTracker;->sInstance:Lcom/android/server/DarkModeStatusTracker;

    return-object v0
.end method

.method private initDarkModeAppList()Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 113
    iget-object v0, p0, Lcom/android/server/DarkModeStatusTracker;->mForceDarkAppListManager:Lcom/android/server/ForceDarkAppListManager;

    const-wide/16 v1, 0x0

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/ForceDarkAppListManager;->getDarkModeAppList(JI)Lcom/miui/darkmode/DarkModeAppData;

    move-result-object v0

    .line 114
    .local v0, "mDarkModeAppData":Lcom/miui/darkmode/DarkModeAppData;
    invoke-virtual {v0}, Lcom/miui/darkmode/DarkModeAppData;->getDarkModeAppDetailInfoList()Ljava/util/List;

    move-result-object v1

    .line 115
    .local v1, "appInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/darkmode/DarkModeAppDetailInfo;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 116
    .local v2, "appList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 119
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/miui/darkmode/DarkModeAppDetailInfo;

    .line 121
    .local v4, "info":Lcom/miui/darkmode/DarkModeAppDetailInfo;
    :try_start_0
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 122
    .local v5, "appStatus":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v6, p0, Lcom/android/server/DarkModeStatusTracker;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v4}, Lcom/miui/darkmode/DarkModeAppDetailInfo;->getPkgName()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v6

    iget-object v6, v6, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v7, p0, Lcom/android/server/DarkModeStatusTracker;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 123
    invoke-virtual {v6, v7}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 124
    .local v6, "appName":Ljava/lang/String;
    const-string v7, "app_package_name"

    invoke-virtual {v4}, Lcom/miui/darkmode/DarkModeAppDetailInfo;->getPkgName()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v5, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    const-string v7, "app_name"

    invoke-interface {v5, v7, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    const-string v7, "app_switch_status"

    invoke-virtual {v4}, Lcom/miui/darkmode/DarkModeAppDetailInfo;->isEnabled()Z

    move-result v8

    if-eqz v8, :cond_0

    const-string/jumbo v8, "\u5f00"

    goto :goto_1

    :cond_0
    const-string/jumbo v8, "\u5173"

    :goto_1
    invoke-interface {v5, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 130
    goto :goto_2

    .line 128
    .end local v5    # "appStatus":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v6    # "appName":Ljava/lang/String;
    :catch_0
    move-exception v5

    .line 129
    .local v5, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v5}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 131
    .end local v4    # "info":Lcom/miui/darkmode/DarkModeAppDetailInfo;
    .end local v5    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :goto_2
    goto :goto_0

    .line 133
    :cond_1
    return-object v2
.end method

.method private putTimeModeStatus(Lcom/android/server/DarkModeStauesEvent;)V
    .locals 3
    .param p1, "event"    # Lcom/android/server/DarkModeStauesEvent;

    .line 276
    iget-object v0, p0, Lcom/android/server/DarkModeStatusTracker;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/DarkModeTimeModeHelper;->isDarkModeTimeEnable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "\u5f00"

    goto :goto_0

    :cond_0
    const-string/jumbo v0, "\u5173"

    :goto_0
    invoke-virtual {p1, v0}, Lcom/android/server/DarkModeStauesEvent;->setTimeModeStatus(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;

    .line 277
    iget-object v0, p0, Lcom/android/server/DarkModeStatusTracker;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/DarkModeTimeModeHelper;->isDarkModeTimeEnable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 278
    iget-object v0, p0, Lcom/android/server/DarkModeStatusTracker;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/DarkModeTimeModeHelper;->isSuntimeType(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 279
    const-string/jumbo v0, "\u65e5\u51fa\u65e5\u843d\u6a21\u5f0f"

    goto :goto_1

    :cond_1
    const-string/jumbo v0, "\u81ea\u5b9a\u4e49\u65f6\u95f4"

    .line 278
    :goto_1
    invoke-virtual {p1, v0}, Lcom/android/server/DarkModeStauesEvent;->setTimeModePattern(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;

    .line 280
    iget-object v0, p0, Lcom/android/server/DarkModeStatusTracker;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/DarkModeTimeModeHelper;->isSuntimeType(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 281
    iget-object v0, p0, Lcom/android/server/DarkModeStatusTracker;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/DarkModeTimeModeHelper;->getSunSetTime(Landroid/content/Context;)I

    move-result v0

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/android/server/DarkModeStatusTracker;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/DarkModeTimeModeHelper;->getDarkModeStartTime(Landroid/content/Context;)I

    move-result v0

    .line 282
    .local v0, "startTime":I
    :goto_2
    iget-object v1, p0, Lcom/android/server/DarkModeStatusTracker;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/server/DarkModeTimeModeHelper;->isSuntimeType(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 283
    iget-object v1, p0, Lcom/android/server/DarkModeStatusTracker;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/server/DarkModeTimeModeHelper;->getSunRiseTime(Landroid/content/Context;)I

    move-result v1

    goto :goto_3

    :cond_3
    iget-object v1, p0, Lcom/android/server/DarkModeStatusTracker;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/server/DarkModeTimeModeHelper;->getDarkModeEndTime(Landroid/content/Context;)I

    move-result v1

    .line 284
    .local v1, "endTime":I
    :goto_3
    invoke-static {v0}, Lcom/android/server/DarkModeTimeModeHelper;->getTimeInString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/android/server/DarkModeStauesEvent;->setBeginTime(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;

    .line 285
    invoke-static {v1}, Lcom/android/server/DarkModeTimeModeHelper;->getTimeInString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/android/server/DarkModeStauesEvent;->setEndTime(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;

    .line 287
    .end local v0    # "startTime":I
    .end local v1    # "endTime":I
    :cond_4
    return-void
.end method

.method private registerSettingsObserver()V
    .locals 5

    .line 137
    iget-object v0, p0, Lcom/android/server/DarkModeStatusTracker;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 138
    .local v0, "observer":Landroid/content/ContentResolver;
    const-string v1, "dark_mode_enable"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/DarkModeStatusTracker;->mSettingsObserver:Landroid/database/ContentObserver;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 140
    const-string v1, "dark_mode_time_enable"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/DarkModeStatusTracker;->mSettingsObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 142
    const-string v1, "dark_mode_time_type"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/DarkModeStatusTracker;->mSettingsObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 144
    const-string v1, "dark_mode_contrast_enable"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/DarkModeStatusTracker;->mSettingsObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 146
    const-string v1, "last_app_dark_mode_pkg"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/DarkModeStatusTracker;->mSettingsObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 148
    invoke-static {}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataNotifyUri()Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lcom/android/server/DarkModeStatusTracker$1;

    .line 149
    invoke-static {}, Lcom/android/server/MiuiBgThread;->getHandler()Landroid/os/Handler;

    move-result-object v4

    invoke-direct {v2, p0, v4}, Lcom/android/server/DarkModeStatusTracker$1;-><init>(Lcom/android/server/DarkModeStatusTracker;Landroid/os/Handler;)V

    .line 148
    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 157
    return-void
.end method

.method private setUploadDarkModeSwitchAlarm(Z)V
    .locals 11
    .param p1, "init"    # Z

    .line 296
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 298
    .local v0, "nowTime":J
    sget-boolean v2, Lcom/android/server/DarkModeStatusTracker;->DEBUG:Z

    if-eqz v2, :cond_0

    const-wide/32 v2, 0xea60

    goto :goto_0

    :cond_0
    if-eqz p1, :cond_1

    const-wide/32 v2, 0x1b7740

    goto :goto_0

    .line 299
    :cond_1
    const-wide/32 v2, 0x5265c00

    :goto_0
    add-long/2addr v2, v0

    .line 300
    .local v2, "nextTime":J
    iget-object v4, p0, Lcom/android/server/DarkModeStatusTracker;->mAlarmManager:Landroid/app/AlarmManager;

    const/4 v5, 0x1

    const-string/jumbo v8, "upload_dark_mode_switch"

    iget-object v9, p0, Lcom/android/server/DarkModeStatusTracker;->mAlarmListener:Landroid/app/AlarmManager$OnAlarmListener;

    iget-object v10, p0, Lcom/android/server/DarkModeStatusTracker;->mDarkModeStatusHandler:Landroid/os/Handler;

    move-wide v6, v2

    invoke-virtual/range {v4 .. v10}, Landroid/app/AlarmManager;->setExact(IJLjava/lang/String;Landroid/app/AlarmManager$OnAlarmListener;Landroid/os/Handler;)V

    .line 302
    sget-boolean v4, Lcom/android/server/DarkModeStatusTracker;->DEBUG:Z

    if-eqz v4, :cond_2

    .line 303
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "setUploadDarkModeSwitchAlarm nextTime = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v2, v3}, Landroid/util/TimeUtils;->formatDuration(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "DarkModeStatusTracker"

    invoke-static {v5, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    :cond_2
    return-void
.end method

.method private updateAppSetting(Lcom/android/server/DarkModeStauesEvent;)V
    .locals 7
    .param p1, "event"    # Lcom/android/server/DarkModeStauesEvent;

    .line 237
    const-string/jumbo v0, "setting"

    invoke-virtual {p1, v0}, Lcom/android/server/DarkModeStauesEvent;->setEventName(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;

    .line 238
    iget-object v0, p0, Lcom/android/server/DarkModeStatusTracker;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "last_app_dark_mode_pkg"

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 239
    .local v0, "appSetting":Ljava/lang/String;
    const-string v1, "DarkModeStatusTracker"

    if-nez v0, :cond_0

    .line 240
    const-string v2, "get app setting error"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    return-void

    .line 243
    :cond_0
    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 244
    .local v2, "appInfo":[Ljava/lang/String;
    if-eqz v2, :cond_3

    array-length v3, v2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    goto :goto_2

    .line 248
    :cond_1
    const/4 v1, 0x0

    aget-object v3, v2, v1

    .line 249
    .local v3, "appPkg":Ljava/lang/String;
    const/4 v4, 0x1

    aget-object v4, v2, v4

    .line 250
    .local v4, "appEnable":Ljava/lang/String;
    const/4 v5, 0x0

    .line 252
    .local v5, "appName":Ljava/lang/String;
    :try_start_0
    iget-object v6, p0, Lcom/android/server/DarkModeStatusTracker;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v6, v3, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v6, p0, Lcom/android/server/DarkModeStatusTracker;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v1, v6}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v5, v1

    .line 255
    goto :goto_0

    .line 253
    :catch_0
    move-exception v1

    .line 254
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 256
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :goto_0
    invoke-virtual {p1, v5}, Lcom/android/server/DarkModeStauesEvent;->setAppName(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;

    .line 257
    invoke-virtual {p1, v3}, Lcom/android/server/DarkModeStauesEvent;->setAppPkg(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;

    .line 258
    const-string/jumbo v1, "true"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string/jumbo v1, "\u5f00"

    goto :goto_1

    :cond_2
    const-string/jumbo v1, "\u5173"

    :goto_1
    invoke-virtual {p1, v1}, Lcom/android/server/DarkModeStauesEvent;->setAppEnable(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;

    .line 259
    return-void

    .line 245
    .end local v3    # "appPkg":Ljava/lang/String;
    .end local v4    # "appEnable":Ljava/lang/String;
    .end local v5    # "appName":Ljava/lang/String;
    :cond_3
    :goto_2
    const-string v3, "get app setting info error"

    invoke-static {v1, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    return-void
.end method

.method private updateAppStatus(Lcom/android/server/DarkModeStauesEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/android/server/DarkModeStauesEvent;

    .line 213
    iget-object v0, p0, Lcom/android/server/DarkModeStatusTracker;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/DarkModeTimeModeHelper;->isDarkModeOpen(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 214
    const-string/jumbo v0, "status"

    invoke-virtual {p1, v0}, Lcom/android/server/DarkModeStauesEvent;->setEventName(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;

    .line 215
    invoke-direct {p0}, Lcom/android/server/DarkModeStatusTracker;->initDarkModeAppList()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/android/server/DarkModeStauesEvent;->setAppList(Ljava/util/List;)Lcom/android/server/DarkModeStauesEvent;

    .line 217
    :cond_0
    return-void
.end method

.method private updateAutoSwitch(Lcom/android/server/DarkModeStauesEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/android/server/DarkModeStauesEvent;

    .line 270
    const-string v0, "auto_switch"

    invoke-virtual {p1, v0}, Lcom/android/server/DarkModeStauesEvent;->setEventName(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;

    .line 271
    iget-object v0, p0, Lcom/android/server/DarkModeStatusTracker;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/DarkModeTimeModeHelper;->isDarkModeEnable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 272
    const-string/jumbo v0, "\u4ece\u6d45\u5230\u6df1"

    goto :goto_0

    :cond_0
    const-string/jumbo v0, "\u4ece\u6df1\u5230\u6d45"

    .line 271
    :goto_0
    invoke-virtual {p1, v0}, Lcom/android/server/DarkModeStauesEvent;->setAutoSwitch(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;

    .line 273
    return-void
.end method

.method private updateContrastSetting(Lcom/android/server/DarkModeStauesEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/android/server/DarkModeStauesEvent;

    .line 229
    const-string/jumbo v0, "setting"

    invoke-virtual {p1, v0}, Lcom/android/server/DarkModeStauesEvent;->setEventName(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;

    .line 230
    iget-object v0, p0, Lcom/android/server/DarkModeStatusTracker;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/DarkModeTimeModeHelper;->isDarkModeOpen(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 231
    iget-object v0, p0, Lcom/android/server/DarkModeStatusTracker;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/DarkModeTimeModeHelper;->isDarkModeContrastEnable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 232
    const-string/jumbo v0, "\u5f00"

    goto :goto_0

    :cond_0
    const-string/jumbo v0, "\u5173"

    .line 231
    :goto_0
    invoke-virtual {p1, v0}, Lcom/android/server/DarkModeStauesEvent;->setContrastStatus(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;

    .line 234
    :cond_1
    return-void
.end method

.method private updateDarkModeSetting(Lcom/android/server/DarkModeStauesEvent;)V
    .locals 3
    .param p1, "event"    # Lcom/android/server/DarkModeStauesEvent;

    .line 262
    const-string/jumbo v0, "setting"

    invoke-virtual {p1, v0}, Lcom/android/server/DarkModeStauesEvent;->setEventName(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;

    .line 263
    iget-object v0, p0, Lcom/android/server/DarkModeStatusTracker;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/DarkModeTimeModeHelper;->isDarkModeEnable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 264
    const-string/jumbo v0, "\u5f00"

    goto :goto_0

    :cond_0
    const-string/jumbo v0, "\u5173"

    .line 263
    :goto_0
    invoke-virtual {p1, v0}, Lcom/android/server/DarkModeStauesEvent;->setDarkModeStatus(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;

    .line 265
    iget-object v0, p0, Lcom/android/server/DarkModeStatusTracker;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "open_dark_mode_channel"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/android/server/DarkModeStauesEvent;->setSettingChannel(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;

    .line 267
    return-void
.end method

.method private updateDarkModeStatus(Lcom/android/server/DarkModeStauesEvent;)V
    .locals 3
    .param p1, "event"    # Lcom/android/server/DarkModeStauesEvent;

    .line 202
    const-string/jumbo v0, "status"

    invoke-virtual {p1, v0}, Lcom/android/server/DarkModeStauesEvent;->setEventName(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;

    .line 203
    iget-object v0, p0, Lcom/android/server/DarkModeStatusTracker;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/DarkModeTimeModeHelper;->isDarkModeEnable(Landroid/content/Context;)Z

    move-result v0

    const-string/jumbo v1, "\u5f00"

    const-string/jumbo v2, "\u5173"

    if-eqz v0, :cond_0

    .line 204
    move-object v0, v1

    goto :goto_0

    :cond_0
    move-object v0, v2

    .line 203
    :goto_0
    invoke-virtual {p1, v0}, Lcom/android/server/DarkModeStauesEvent;->setDarkModeStatus(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;

    .line 205
    invoke-direct {p0, p1}, Lcom/android/server/DarkModeStatusTracker;->putTimeModeStatus(Lcom/android/server/DarkModeStauesEvent;)V

    .line 206
    iget-object v0, p0, Lcom/android/server/DarkModeStatusTracker;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/DarkModeTimeModeHelper;->isDarkModeOpen(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 207
    iget-object v0, p0, Lcom/android/server/DarkModeStatusTracker;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/DarkModeTimeModeHelper;->isDarkModeContrastEnable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 208
    goto :goto_1

    :cond_1
    move-object v1, v2

    .line 207
    :goto_1
    invoke-virtual {p1, v1}, Lcom/android/server/DarkModeStauesEvent;->setContrastStatus(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;

    .line 210
    :cond_2
    return-void
.end method

.method private updateSwitchEvent(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
    .locals 2
    .param p1, "tip"    # Ljava/lang/String;

    .line 169
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 170
    const/4 v0, 0x0

    return-object v0

    .line 172
    :cond_0
    new-instance v0, Lcom/android/server/DarkModeStauesEvent;

    invoke-direct {v0}, Lcom/android/server/DarkModeStauesEvent;-><init>()V

    invoke-virtual {v0, p1}, Lcom/android/server/DarkModeStauesEvent;->setTip(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;

    move-result-object v0

    .line 173
    .local v0, "event":Lcom/android/server/DarkModeStauesEvent;
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_1
    goto :goto_0

    :sswitch_0
    const-string v1, "577.5.0.1.23096"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x6

    goto :goto_1

    :sswitch_1
    const-string v1, "577.3.2.1.23093"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x3

    goto :goto_1

    :sswitch_2
    const-string v1, "577.1.2.1.23090"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :sswitch_3
    const-string v1, "577.3.3.1.23094"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x4

    goto :goto_1

    :sswitch_4
    const-string v1, "577.4.0.1.23106"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x5

    goto :goto_1

    :sswitch_5
    const-string v1, "577.2.0.1.23091"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    goto :goto_1

    :sswitch_6
    const-string v1, "577.1.1.1.23089"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    goto :goto_1

    :goto_0
    const/4 v1, -0x1

    :goto_1
    packed-switch v1, :pswitch_data_0

    goto :goto_2

    .line 193
    :pswitch_0
    invoke-direct {p0, v0}, Lcom/android/server/DarkModeStatusTracker;->updateAutoSwitch(Lcom/android/server/DarkModeStauesEvent;)V

    .line 194
    goto :goto_2

    .line 190
    :pswitch_1
    invoke-direct {p0, v0}, Lcom/android/server/DarkModeStatusTracker;->updateDarkModeSetting(Lcom/android/server/DarkModeStauesEvent;)V

    .line 191
    goto :goto_2

    .line 187
    :pswitch_2
    invoke-direct {p0, v0}, Lcom/android/server/DarkModeStatusTracker;->updateAppSetting(Lcom/android/server/DarkModeStauesEvent;)V

    .line 188
    goto :goto_2

    .line 184
    :pswitch_3
    invoke-direct {p0, v0}, Lcom/android/server/DarkModeStatusTracker;->updateContrastSetting(Lcom/android/server/DarkModeStauesEvent;)V

    .line 185
    goto :goto_2

    .line 181
    :pswitch_4
    invoke-direct {p0, v0}, Lcom/android/server/DarkModeStatusTracker;->updateTimeModeSetting(Lcom/android/server/DarkModeStauesEvent;)V

    .line 182
    goto :goto_2

    .line 178
    :pswitch_5
    invoke-direct {p0, v0}, Lcom/android/server/DarkModeStatusTracker;->updateAppStatus(Lcom/android/server/DarkModeStauesEvent;)V

    .line 179
    goto :goto_2

    .line 175
    :pswitch_6
    invoke-direct {p0, v0}, Lcom/android/server/DarkModeStatusTracker;->updateDarkModeStatus(Lcom/android/server/DarkModeStauesEvent;)V

    .line 176
    nop

    .line 198
    :goto_2
    return-object v0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x5ce9f380 -> :sswitch_6
        -0x5c49afa9 -> :sswitch_5
        -0x32804778 -> :sswitch_4
        -0xa97afe2 -> :sswitch_3
        0x375a7b97 -> :sswitch_2
        0x6123e11c -> :sswitch_1
        0x6264689f -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private updateTimeModeSetting(Lcom/android/server/DarkModeStauesEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/android/server/DarkModeStauesEvent;

    .line 220
    const-string/jumbo v0, "setting"

    invoke-virtual {p1, v0}, Lcom/android/server/DarkModeStauesEvent;->setEventName(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;

    .line 221
    invoke-direct {p0, p1}, Lcom/android/server/DarkModeStatusTracker;->putTimeModeStatus(Lcom/android/server/DarkModeStauesEvent;)V

    .line 222
    iget-object v0, p0, Lcom/android/server/DarkModeStatusTracker;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/DarkModeTimeModeHelper;->isDarkModeTimeEnable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/DarkModeStatusTracker;->mContext:Landroid/content/Context;

    .line 223
    invoke-static {v0}, Lcom/android/server/DarkModeTimeModeHelper;->isSuntimeType(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 224
    iget-object v0, p0, Lcom/android/server/DarkModeStatusTracker;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "open_sun_time_channel"

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/android/server/DarkModeStauesEvent;->setSettingChannel(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;

    .line 226
    :cond_0
    return-void
.end method

.method private uploadDarkModeStatusEvent(Ljava/lang/String;)V
    .locals 2
    .param p1, "eventTip"    # Ljava/lang/String;

    .line 161
    invoke-direct {p0, p1}, Lcom/android/server/DarkModeStatusTracker;->updateSwitchEvent(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/DarkModeStatusTracker;->mDarkModeSwitchEvent:Lcom/android/server/DarkModeStauesEvent;

    .line 162
    iget-object v1, p0, Lcom/android/server/DarkModeStatusTracker;->mContext:Landroid/content/Context;

    invoke-direct {p0, v1, v0}, Lcom/android/server/DarkModeStatusTracker;->uploadSwitchToOnetrack(Landroid/content/Context;Lcom/android/server/DarkModeEvent;)V

    .line 163
    sget-boolean v0, Lcom/android/server/DarkModeStatusTracker;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 164
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "uploadDarkModeStatusEvent "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/DarkModeStatusTracker;->mDarkModeSwitchEvent:Lcom/android/server/DarkModeStauesEvent;

    invoke-virtual {v1}, Lcom/android/server/DarkModeStauesEvent;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "DarkModeStatusTracker"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    :cond_0
    return-void
.end method

.method private uploadSwitchToOnetrack(Landroid/content/Context;Lcom/android/server/DarkModeEvent;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "event"    # Lcom/android/server/DarkModeEvent;

    .line 290
    if-eqz p2, :cond_0

    .line 291
    invoke-static {p1, p2}, Lcom/android/server/DarkModeOneTrackHelper;->uploadToOneTrack(Landroid/content/Context;Lcom/android/server/DarkModeEvent;)V

    .line 293
    :cond_0
    return-void
.end method


# virtual methods
.method public init(Landroid/content/Context;Lcom/android/server/ForceDarkAppListManager;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "forceDarkAppListManager"    # Lcom/android/server/ForceDarkAppListManager;

    .line 100
    iput-object p1, p0, Lcom/android/server/DarkModeStatusTracker;->mContext:Landroid/content/Context;

    .line 101
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Lcom/android/server/MiuiBgThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/DarkModeStatusTracker;->mDarkModeStatusHandler:Landroid/os/Handler;

    .line 102
    new-instance v0, Lcom/android/server/DarkModeStatusTracker$SettingsObserver;

    iget-object v1, p0, Lcom/android/server/DarkModeStatusTracker;->mDarkModeStatusHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/android/server/DarkModeStatusTracker$SettingsObserver;-><init>(Lcom/android/server/DarkModeStatusTracker;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/server/DarkModeStatusTracker;->mSettingsObserver:Landroid/database/ContentObserver;

    .line 103
    iget-object v0, p0, Lcom/android/server/DarkModeStatusTracker;->mContext:Landroid/content/Context;

    const-string v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iput-object v0, p0, Lcom/android/server/DarkModeStatusTracker;->mAlarmManager:Landroid/app/AlarmManager;

    .line 104
    iget-object v0, p0, Lcom/android/server/DarkModeStatusTracker;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/DarkModeStatusTracker;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 105
    iput-object p2, p0, Lcom/android/server/DarkModeStatusTracker;->mForceDarkAppListManager:Lcom/android/server/ForceDarkAppListManager;

    .line 106
    invoke-direct {p0}, Lcom/android/server/DarkModeStatusTracker;->registerSettingsObserver()V

    .line 107
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/server/DarkModeStatusTracker;->setUploadDarkModeSwitchAlarm(Z)V

    .line 108
    invoke-static {}, Lcom/android/server/DarkModeSuggestProvider;->getInstance()Lcom/android/server/DarkModeSuggestProvider;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/DarkModeStatusTracker;->mContext:Landroid/content/Context;

    .line 109
    invoke-virtual {v0, v1}, Lcom/android/server/DarkModeSuggestProvider;->updateCloudDataForDisableRegion(Landroid/content/Context;)Ljava/util/Set;

    move-result-object v0

    .line 108
    invoke-static {v0}, Lcom/android/server/DarkModeOneTrackHelper;->setDataDisableRegion(Ljava/util/Set;)V

    .line 110
    return-void
.end method
