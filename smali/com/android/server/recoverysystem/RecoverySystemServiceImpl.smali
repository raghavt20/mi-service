.class public Lcom/android/server/recoverysystem/RecoverySystemServiceImpl;
.super Lcom/android/server/recoverysystem/RecoverySystemServiceStub;
.source "RecoverySystemServiceImpl.java"


# static fields
.field private static final MQSASD:Ljava/lang/String; = "miui.mqsas.IMQSNative"

.field private static final TAG:Ljava/lang/String; = "RecoverySystemServiceStub"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Lcom/android/server/recoverysystem/RecoverySystemServiceStub;-><init>()V

    return-void
.end method


# virtual methods
.method public doWipeRescuePartition()V
    .locals 4

    .line 17
    const-string v0, "miui.mqsas.IMQSNative"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lmiui/mqsas/IMQSNative$Stub;->asInterface(Landroid/os/IBinder;)Lmiui/mqsas/IMQSNative;

    move-result-object v0

    .line 18
    .local v0, "daemon":Lmiui/mqsas/IMQSNative;
    const-string v1, "RecoverySystemServiceStub"

    if-nez v0, :cond_0

    .line 19
    const-string v2, "mqsasd not available!"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 20
    return-void

    .line 22
    :cond_0
    const-string v2, "Trigger wipe rescue partition!"

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 24
    :try_start_0
    invoke-interface {v0}, Lmiui/mqsas/IMQSNative;->FactoryResetWipeRescuePartition()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 27
    goto :goto_0

    .line 25
    :catch_0
    move-exception v2

    .line 26
    .local v2, "e":Landroid/os/RemoteException;
    const-string v3, "Trigger wipe rescue partition failed!"

    invoke-static {v1, v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 28
    .end local v2    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method
