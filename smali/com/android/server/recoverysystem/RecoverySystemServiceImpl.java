public class com.android.server.recoverysystem.RecoverySystemServiceImpl extends com.android.server.recoverysystem.RecoverySystemServiceStub {
	 /* .source "RecoverySystemServiceImpl.java" */
	 /* # static fields */
	 private static final java.lang.String MQSASD;
	 private static final java.lang.String TAG;
	 /* # direct methods */
	 public com.android.server.recoverysystem.RecoverySystemServiceImpl ( ) {
		 /* .locals 0 */
		 /* .line 11 */
		 /* invoke-direct {p0}, Lcom/android/server/recoverysystem/RecoverySystemServiceStub;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public void doWipeRescuePartition ( ) {
		 /* .locals 4 */
		 /* .line 17 */
		 final String v0 = "miui.mqsas.IMQSNative"; // const-string v0, "miui.mqsas.IMQSNative"
		 android.os.ServiceManager .getService ( v0 );
		 miui.mqsas.IMQSNative$Stub .asInterface ( v0 );
		 /* .line 18 */
		 /* .local v0, "daemon":Lmiui/mqsas/IMQSNative; */
		 final String v1 = "RecoverySystemServiceStub"; // const-string v1, "RecoverySystemServiceStub"
		 /* if-nez v0, :cond_0 */
		 /* .line 19 */
		 final String v2 = "mqsasd not available!"; // const-string v2, "mqsasd not available!"
		 android.util.Slog .e ( v1,v2 );
		 /* .line 20 */
		 return;
		 /* .line 22 */
	 } // :cond_0
	 final String v2 = "Trigger wipe rescue partition!"; // const-string v2, "Trigger wipe rescue partition!"
	 android.util.Slog .w ( v1,v2 );
	 /* .line 24 */
	 try { // :try_start_0
		 /* :try_end_0 */
		 /* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 27 */
		 /* .line 25 */
		 /* :catch_0 */
		 /* move-exception v2 */
		 /* .line 26 */
		 /* .local v2, "e":Landroid/os/RemoteException; */
		 final String v3 = "Trigger wipe rescue partition failed!"; // const-string v3, "Trigger wipe rescue partition failed!"
		 android.util.Slog .e ( v1,v3,v2 );
		 /* .line 28 */
	 } // .end local v2 # "e":Landroid/os/RemoteException;
} // :goto_0
return;
} // .end method
