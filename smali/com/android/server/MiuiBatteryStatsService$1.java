class com.android.server.MiuiBatteryStatsService$1 extends android.content.BroadcastReceiver {
	 /* .source "MiuiBatteryStatsService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/MiuiBatteryStatsService;-><init>(Landroid/content/Context;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.MiuiBatteryStatsService this$0; //synthetic
/* # direct methods */
 com.android.server.MiuiBatteryStatsService$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/MiuiBatteryStatsService; */
/* .line 135 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 24 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 138 */
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p2 */
/* invoke-virtual/range {p2 ..p2}, Landroid/content/Intent;->getAction()Ljava/lang/String; */
/* .line 139 */
/* .local v2, "action":Ljava/lang/String; */
v3 = this.this$0;
v3 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetDEBUG ( v3 );
final String v4 = "MiuiBatteryStatsService"; // const-string v4, "MiuiBatteryStatsService"
if ( v3 != null) { // if-eqz v3, :cond_0
	 /* new-instance v3, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v5 = "action = "; // const-string v5, "action = "
	 (( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .d ( v4,v3 );
	 /* .line 140 */
} // :cond_0
final String v3 = "android.intent.action.BATTERY_CHANGED"; // const-string v3, "android.intent.action.BATTERY_CHANGED"
v3 = (( java.lang.String ) v3 ).equals ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v7 = -1; // const/4 v7, -0x1
int v9 = 0; // const/4 v9, 0x0
int v10 = 1; // const/4 v10, 0x1
if ( v3 != null) { // if-eqz v3, :cond_20
	 /* .line 141 */
	 final String v3 = "plugged"; // const-string v3, "plugged"
	 v3 = 	 (( android.content.Intent ) v1 ).getIntExtra ( v3, v7 ); // invoke-virtual {v1, v3, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
	 /* .line 142 */
	 /* .local v3, "plugType":I */
	 /* const-string/jumbo v13, "status" */
	 v13 = 	 (( android.content.Intent ) v1 ).getIntExtra ( v13, v10 ); // invoke-virtual {v1, v13, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
	 /* .line 143 */
	 /* .local v13, "batteryStatus":I */
	 /* const-string/jumbo v14, "temperature" */
	 v14 = 	 (( android.content.Intent ) v1 ).getIntExtra ( v14, v9 ); // invoke-virtual {v1, v14, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
	 /* .line 144 */
	 /* .local v14, "batteryTemp":I */
	 /* const-string/jumbo v15, "voltage" */
	 v15 = 	 (( android.content.Intent ) v1 ).getIntExtra ( v15, v9 ); // invoke-virtual {v1, v15, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
	 /* .line 145 */
	 /* .local v15, "batteryVoltage":I */
	 final String v5 = "level"; // const-string v5, "level"
	 v6 = 	 (( android.content.Intent ) v1 ).getIntExtra ( v5, v7 ); // invoke-virtual {v1, v5, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
	 /* .line 146 */
	 /* .local v6, "soc":I */
	 /* if-lez v3, :cond_1 */
	 /* move/from16 v16, v10 */
} // :cond_1
/* move/from16 v16, v9 */
} // :goto_0
/* move/from16 v17, v16 */
/* .line 148 */
/* .local v17, "plugged":Z */
/* move/from16 v8, v17 */
} // .end local v17 # "plugged":Z
/* .local v8, "plugged":Z */
if ( v8 != null) { // if-eqz v8, :cond_2
v9 = this.this$0;
v9 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmLastPlugged ( v9 );
if ( v9 != null) { // if-eqz v9, :cond_3
} // :cond_2
/* if-nez v8, :cond_8 */
v9 = this.this$0;
v9 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmLastPlugged ( v9 );
if ( v9 != null) { // if-eqz v9, :cond_8
/* .line 149 */
} // :cond_3
if ( v8 != null) { // if-eqz v8, :cond_4
/* .line 150 */
v5 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fputmChargeMaxTemp ( v5,v14 );
/* .line 151 */
v5 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fputmChargeMinTemp ( v5,v14 );
/* .line 152 */
v5 = this.this$0;
java.lang.System .currentTimeMillis ( );
/* move-result-wide v10 */
com.android.server.MiuiBatteryStatsService .-$$Nest$fputmChargeStartTime ( v5,v10,v11 );
/* .line 153 */
v5 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fputmChargeStartCapacity ( v5,v6 );
/* .line 154 */
v5 = this.this$0;
v5 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmIsScreenOn ( v5 );
if ( v5 != null) { // if-eqz v5, :cond_5
	 /* .line 155 */
	 v5 = this.this$0;
	 java.lang.System .currentTimeMillis ( );
	 /* move-result-wide v10 */
	 com.android.server.MiuiBatteryStatsService .-$$Nest$fputmScreenOnChargingStart ( v5,v10,v11 );
	 /* .line 158 */
} // :cond_4
v10 = this.this$0;
java.lang.System .currentTimeMillis ( );
/* move-result-wide v11 */
com.android.server.MiuiBatteryStatsService .-$$Nest$fputmChargeEndTime ( v10,v11,v12 );
/* .line 159 */
v10 = this.this$0;
v5 = (( android.content.Intent ) v1 ).getIntExtra ( v5, v7 ); // invoke-virtual {v1, v5, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
com.android.server.MiuiBatteryStatsService .-$$Nest$fputmChargeEndCapacity ( v10,v5 );
/* .line 160 */
v5 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmScreenOnChargingStart ( v5 );
/* move-result-wide v10 */
/* const-wide/16 v18, 0x0 */
/* cmp-long v5, v10, v18 */
if ( v5 != null) { // if-eqz v5, :cond_5
	 /* .line 161 */
	 v5 = this.this$0;
	 com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmScreenOnTime ( v5 );
	 /* move-result-wide v10 */
	 java.lang.System .currentTimeMillis ( );
	 /* move-result-wide v20 */
	 v7 = this.this$0;
	 com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmScreenOnChargingStart ( v7 );
	 /* move-result-wide v22 */
	 /* sub-long v20, v20, v22 */
	 /* add-long v10, v10, v20 */
	 com.android.server.MiuiBatteryStatsService .-$$Nest$fputmScreenOnTime ( v5,v10,v11 );
	 /* .line 162 */
	 v5 = this.this$0;
	 /* const-wide/16 v10, 0x0 */
	 com.android.server.MiuiBatteryStatsService .-$$Nest$fputmScreenOnChargingStart ( v5,v10,v11 );
	 /* .line 166 */
} // :cond_5
} // :goto_1
v5 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmHandler ( v5 );
(( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) v5 ).updateChargeInfo ( v8 ); // invoke-virtual {v5, v8}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->updateChargeInfo(Z)V
/* .line 167 */
v5 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmHandler ( v5 );
(( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) v5 ).switchTimer ( v8, v6 ); // invoke-virtual {v5, v8, v6}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->switchTimer(ZI)V
/* .line 169 */
v5 = this.this$0;
v5 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmIsTablet ( v5 );
/* if-nez v5, :cond_8 */
/* .line 170 */
v5 = this.this$0;
v5 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetDEBUG ( v5 );
if ( v5 != null) { // if-eqz v5, :cond_6
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "plugType = "; // const-string v7, "plugType = "
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v5 );
/* .line 171 */
} // :cond_6
int v5 = 1; // const/4 v5, 0x1
/* if-ne v3, v5, :cond_7 */
/* .line 172 */
v5 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmHandler ( v5 );
/* const/16 v7, 0xc */
/* const-wide/16 v10, 0x1388 */
(( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) v5 ).sendMessageDelayed ( v7, v10, v11 ); // invoke-virtual {v5, v7, v10, v11}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(IJ)V
/* .line 173 */
} // :cond_7
/* if-gtz v3, :cond_8 */
v5 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmPendingIntentLimitTime ( v5 );
if ( v5 != null) { // if-eqz v5, :cond_8
/* .line 174 */
v5 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmAlarmManager ( v5 );
v7 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmPendingIntentLimitTime ( v7 );
(( android.app.AlarmManager ) v5 ).cancel ( v7 ); // invoke-virtual {v5, v7}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V
/* .line 178 */
} // :cond_8
} // :goto_2
/* const/16 v5, 0x64 */
if ( v8 != null) { // if-eqz v8, :cond_9
/* const/16 v7, 0x5a */
/* if-lt v6, v7, :cond_9 */
/* if-ge v6, v5, :cond_9 */
/* .line 179 */
v5 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmHandler ( v5 );
/* const/16 v7, 0x14 */
/* const-wide/16 v10, 0x1388 */
(( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) v5 ).sendMessageDelayed ( v7, v10, v11 ); // invoke-virtual {v5, v7, v10, v11}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(IJ)V
/* .line 180 */
} // :cond_9
if ( v8 != null) { // if-eqz v8, :cond_a
/* if-ne v6, v5, :cond_b */
/* .line 181 */
} // :cond_a
v5 = this.this$0;
int v7 = 0; // const/4 v7, 0x0
com.android.server.MiuiBatteryStatsService .-$$Nest$fputmIsOrderedCheckSocTimer ( v5,v7 );
/* .line 182 */
v5 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmAlarmManager ( v5 );
if ( v5 != null) { // if-eqz v5, :cond_b
v5 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmPendingIntentCheckSoc ( v5 );
if ( v5 != null) { // if-eqz v5, :cond_b
/* .line 183 */
v5 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmAlarmManager ( v5 );
v7 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmPendingIntentCheckSoc ( v7 );
(( android.app.AlarmManager ) v5 ).cancel ( v7 ); // invoke-virtual {v5, v7}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V
/* .line 186 */
} // :cond_b
} // :goto_3
v5 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmFullChargeStartTime ( v5 );
/* move-result-wide v10 */
/* const-wide/16 v18, 0x0 */
/* cmp-long v5, v10, v18 */
/* if-nez v5, :cond_c */
int v5 = 5; // const/4 v5, 0x5
/* if-ne v13, v5, :cond_c */
v7 = this.this$0;
v7 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmLastBatteryStatus ( v7 );
/* if-eq v7, v5, :cond_c */
/* .line 188 */
v5 = this.this$0;
java.lang.System .currentTimeMillis ( );
/* move-result-wide v10 */
com.android.server.MiuiBatteryStatsService .-$$Nest$fputmFullChargeStartTime ( v5,v10,v11 );
/* .line 189 */
} // :cond_c
int v5 = 5; // const/4 v5, 0x5
/* if-eq v13, v5, :cond_d */
v7 = this.this$0;
v7 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmLastBatteryStatus ( v7 );
/* if-ne v7, v5, :cond_d */
/* .line 191 */
v5 = this.this$0;
java.lang.System .currentTimeMillis ( );
/* move-result-wide v10 */
com.android.server.MiuiBatteryStatsService .-$$Nest$fputmFullChargeEndTime ( v5,v10,v11 );
/* .line 193 */
} // :cond_d
} // :goto_4
v5 = this.this$0;
v5 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetDEBUG ( v5 );
if ( v5 != null) { // if-eqz v5, :cond_e
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "mLastBatteryTemp = "; // const-string v7, "mLastBatteryTemp = "
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = this.this$0;
v7 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmLastBatteryTemp ( v7 );
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = ", batteryTemp = "; // const-string v7, ", batteryTemp = "
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v14 ); // invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v5 );
/* .line 194 */
} // :cond_e
v5 = this.this$0;
v5 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmLastBatteryTemp ( v5 );
/* if-ne v14, v5, :cond_f */
v5 = this.this$0;
v5 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmLastBatteryVoltage ( v5 );
/* if-ne v15, v5, :cond_f */
v5 = this.this$0;
v5 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmLastBatteryStatus ( v5 );
/* if-eq v13, v5, :cond_13 */
/* .line 196 */
} // :cond_f
v5 = this.this$0;
v5 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetDEBUG ( v5 );
if ( v5 != null) { // if-eqz v5, :cond_10
/* .line 197 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "mLastBatteryVoltage = "; // const-string v7, "mLastBatteryVoltage = "
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = this.this$0;
v7 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmLastBatteryVoltage ( v7 );
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = ", batteryVoltage = "; // const-string v7, ", batteryVoltage = "
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v15 ); // invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v5 );
/* .line 200 */
} // :cond_10
v5 = this.this$0;
v5 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmLastBatteryTemp ( v5 );
/* if-eq v14, v5, :cond_11 */
/* .line 201 */
v5 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmHandler ( v5 );
int v7 = 6; // const/4 v7, 0x6
/* const-wide/16 v10, 0x0 */
(( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) v5 ).sendMessageDelayed ( v7, v1, v10, v11 ); // invoke-virtual {v5, v7, v1, v10, v11}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(ILjava/lang/Object;J)V
/* .line 203 */
} // :cond_11
int v5 = 5; // const/4 v5, 0x5
/* if-ne v13, v5, :cond_12 */
/* .line 204 */
v5 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmBatteryInfoFull ( v5 );
(( com.android.server.MiuiBatteryStatsService$BatteryTempVoltageTimeInfo ) v5 ).collectTime ( v14, v15, v13 ); // invoke-virtual {v5, v14, v15, v13}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->collectTime(III)V
/* .line 206 */
} // :cond_12
v5 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmBatteryInfoNormal ( v5 );
(( com.android.server.MiuiBatteryStatsService$BatteryTempVoltageTimeInfo ) v5 ).collectTime ( v14, v15, v13 ); // invoke-virtual {v5, v14, v15, v13}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->collectTime(III)V
/* .line 209 */
} // :cond_13
} // :goto_5
v5 = this.this$0;
v5 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmBootCompleted ( v5 );
if ( v5 != null) { // if-eqz v5, :cond_19
v5 = this.this$0;
v5 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmLastBatteryTemp ( v5 );
/* if-ne v14, v5, :cond_14 */
v5 = this.this$0;
v5 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmLastSoc ( v5 );
/* if-eq v6, v5, :cond_19 */
/* .line 210 */
} // :cond_14
v5 = this.this$0;
v5 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmSupportedSB ( v5 );
if ( v5 != null) { // if-eqz v5, :cond_18
/* .line 211 */
v5 = this.this$0;
v5 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetDEBUG ( v5 );
if ( v5 != null) { // if-eqz v5, :cond_15
/* .line 212 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "mLastSoc="; // const-string v7, "mLastSoc="
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = this.this$0;
v7 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmLastSoc ( v7 );
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = " soc="; // const-string v7, " soc="
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = " System.currentTimeMillis()="; // const-string v7, " System.currentTimeMillis()="
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmHandler ( v7 );
/* .line 213 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v10 */
com.android.server.MiuiBatteryStatsService$BatteryStatsHandler .-$$Nest$mformatTime ( v7,v10,v11 );
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = " stopCollectTempSocTime="; // const-string v7, " stopCollectTempSocTime="
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmHandler ( v7 );
v10 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmBatteryTempSocTime ( v10 );
com.android.server.MiuiBatteryStatsService$BatteryTempSocTimeInfo .-$$Nest$fgetstopCollectTempSocTime ( v10 );
/* move-result-wide v10 */
/* .line 214 */
com.android.server.MiuiBatteryStatsService$BatteryStatsHandler .-$$Nest$mformatTime ( v7,v10,v11 );
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 212 */
android.util.Slog .d ( v4,v5 );
/* .line 215 */
} // :cond_15
v5 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmBatteryTempSocTime ( v5 );
com.android.server.MiuiBatteryStatsService$BatteryTempSocTimeInfo .-$$Nest$fgetstopCollectTempSocTime ( v5 );
/* move-result-wide v10 */
/* const-wide/16 v16, 0x0 */
/* cmp-long v5, v10, v16 */
/* if-nez v5, :cond_16 */
/* .line 216 */
v5 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmBatteryTempSocTime ( v5 );
(( com.android.server.MiuiBatteryStatsService$BatteryTempSocTimeInfo ) v5 ).collectTempSocData ( v14, v6 ); // invoke-virtual {v5, v14, v6}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->collectTempSocData(II)V
/* .line 218 */
} // :cond_16
java.lang.System .currentTimeMillis ( );
/* move-result-wide v10 */
v5 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmBatteryTempSocTime ( v5 );
com.android.server.MiuiBatteryStatsService$BatteryTempSocTimeInfo .-$$Nest$fgetstopCollectTempSocTime ( v5 );
/* move-result-wide v16 */
/* cmp-long v5, v10, v16 */
/* if-gez v5, :cond_17 */
/* .line 219 */
v5 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmBatteryTempSocTime ( v5 );
(( com.android.server.MiuiBatteryStatsService$BatteryTempSocTimeInfo ) v5 ).collectTempSocData ( v14, v6 ); // invoke-virtual {v5, v14, v6}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->collectTempSocData(II)V
/* .line 221 */
} // :cond_17
v5 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmBatteryTempSocTime ( v5 );
(( com.android.server.MiuiBatteryStatsService$BatteryTempSocTimeInfo ) v5 ).processTempSocData ( ); // invoke-virtual {v5}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->processTempSocData()V
/* .line 225 */
} // :cond_18
} // :goto_6
v5 = this.this$0;
v5 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmSupportedCellVolt ( v5 );
if ( v5 != null) { // if-eqz v5, :cond_19
/* .line 226 */
v5 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmHandler ( v5 );
/* const/16 v7, 0xf */
/* const-wide/16 v10, 0x0 */
(( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) v5 ).sendMessageDelayed ( v7, v1, v10, v11 ); // invoke-virtual {v5, v7, v1, v10, v11}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(ILjava/lang/Object;J)V
/* .line 229 */
} // :cond_19
if ( v8 != null) { // if-eqz v8, :cond_1b
v5 = this.this$0;
v5 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmLastPlugged ( v5 );
/* if-nez v5, :cond_1b */
v5 = this.this$0;
v5 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmStartRecordDischarging ( v5 );
/* if-nez v5, :cond_1b */
/* .line 230 */
v5 = this.this$0;
v5 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetDEBUG ( v5 );
if ( v5 != null) { // if-eqz v5, :cond_1a
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "5s start mDischargingCount = "; // const-string v7, "5s start mDischargingCount = "
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = this.this$0;
v7 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmDischargingCount ( v7 );
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v5 );
/* .line 231 */
} // :cond_1a
v5 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmHandler ( v5 );
/* const/16 v7, 0xe */
/* const-wide/16 v10, 0x1388 */
(( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) v5 ).sendMessageDelayed ( v7, v10, v11 ); // invoke-virtual {v5, v7, v10, v11}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(IJ)V
/* .line 232 */
v5 = this.this$0;
int v7 = 1; // const/4 v7, 0x1
com.android.server.MiuiBatteryStatsService .-$$Nest$fputmStartRecordDischarging ( v5,v7 );
/* .line 233 */
v5 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fputmIsHandleIntermittentCharge ( v5,v7 );
/* .line 235 */
} // :cond_1b
/* if-nez v8, :cond_1c */
v5 = this.this$0;
v5 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmLastPlugged ( v5 );
if ( v5 != null) { // if-eqz v5, :cond_1c
v5 = this.this$0;
v5 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmIsHandleIntermittentCharge ( v5 );
/* if-nez v5, :cond_1c */
/* .line 236 */
v5 = this.this$0;
int v7 = 0; // const/4 v7, 0x0
com.android.server.MiuiBatteryStatsService .-$$Nest$fputmStartRecordDischarging ( v5,v7 );
/* .line 238 */
} // :cond_1c
v5 = this.this$0;
v5 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmStartRecordDischarging ( v5 );
if ( v5 != null) { // if-eqz v5, :cond_1d
int v5 = 3; // const/4 v5, 0x3
/* if-ne v13, v5, :cond_1d */
/* .line 239 */
v5 = this.this$0;
v7 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmDischargingCount ( v5 );
int v9 = 1; // const/4 v9, 0x1
/* add-int/2addr v7, v9 */
com.android.server.MiuiBatteryStatsService .-$$Nest$fputmDischargingCount ( v5,v7 );
/* .line 240 */
v5 = this.this$0;
v5 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetDEBUG ( v5 );
if ( v5 != null) { // if-eqz v5, :cond_1d
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "record mDischargingCount = "; // const-string v7, "record mDischargingCount = "
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = this.this$0;
v7 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmDischargingCount ( v7 );
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v5 );
/* .line 242 */
} // :cond_1d
v4 = this.this$0;
v4 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmLastSoc ( v4 );
/* if-eq v6, v4, :cond_1e */
/* .line 243 */
v4 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmHandler ( v4 );
com.android.server.MiuiBatteryStatsService$BatteryStatsHandler .-$$Nest$mhandleNonlinearChangeOfCapacity ( v4,v6 );
/* .line 245 */
} // :cond_1e
v4 = this.this$0;
v4 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmPlugType ( v4 );
/* if-eq v3, v4, :cond_1f */
/* .line 246 */
v4 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fputmPlugType ( v4,v3 );
/* .line 247 */
v4 = this.this$0;
v4 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmPlugType ( v4 );
/* if-lez v4, :cond_1f */
/* .line 248 */
v4 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmHandler ( v4 );
/* const/16 v5, 0x11 */
/* const-wide/16 v9, 0x0 */
(( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) v4 ).sendMessageDelayed ( v5, v9, v10 ); // invoke-virtual {v4, v5, v9, v10}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(IJ)V
/* .line 251 */
} // :cond_1f
v4 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fputmLastBatteryStatus ( v4,v13 );
/* .line 252 */
v4 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fputmLastPlugged ( v4,v8 );
/* .line 253 */
v4 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fputmLastBatteryVoltage ( v4,v15 );
/* .line 254 */
v4 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fputmLastBatteryTemp ( v4,v14 );
/* .line 255 */
v4 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fputmLastSoc ( v4,v6 );
/* .line 256 */
} // .end local v3 # "plugType":I
} // .end local v6 # "soc":I
} // .end local v8 # "plugged":Z
} // .end local v13 # "batteryStatus":I
} // .end local v14 # "batteryTemp":I
} // .end local v15 # "batteryVoltage":I
/* goto/16 :goto_8 */
} // :cond_20
final String v3 = "miui.intent.action.LIMIT_TIME"; // const-string v3, "miui.intent.action.LIMIT_TIME"
v3 = (( java.lang.String ) v3 ).equals ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_21
/* .line 257 */
v3 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmHandler ( v3 );
/* const/16 v4, 0xd */
/* const-wide/16 v5, 0x0 */
(( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) v3 ).sendMessageDelayed ( v4, v5, v6 ); // invoke-virtual {v3, v4, v5, v6}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(IJ)V
/* goto/16 :goto_8 */
/* .line 258 */
} // :cond_21
final String v3 = "miui.intent.action.CYCLE_CHECK"; // const-string v3, "miui.intent.action.CYCLE_CHECK"
v3 = (( java.lang.String ) v3 ).equals ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v5 = 2; // const/4 v5, 0x2
if ( v3 != null) { // if-eqz v3, :cond_22
v3 = this.this$0;
v3 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmLastPlugged ( v3 );
if ( v3 != null) { // if-eqz v3, :cond_22
/* .line 259 */
v3 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmHandler ( v3 );
/* const/16 v4, 0x10 */
/* const-wide/16 v6, 0x0 */
(( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) v3 ).sendMessageDelayed ( v4, v6, v7 ); // invoke-virtual {v3, v4, v6, v7}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(IJ)V
/* .line 260 */
v3 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmAlarmManager ( v3 );
/* .line 261 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v6 */
/* const-wide/16 v8, 0x7530 */
/* add-long/2addr v6, v8 */
v4 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmPendingIntentCycleCheck ( v4 );
/* .line 260 */
(( android.app.AlarmManager ) v3 ).setExactAndAllowWhileIdle ( v5, v6, v7, v4 ); // invoke-virtual {v3, v5, v6, v7, v4}, Landroid/app/AlarmManager;->setExactAndAllowWhileIdle(IJLandroid/app/PendingIntent;)V
/* goto/16 :goto_8 */
/* .line 262 */
} // :cond_22
final String v3 = "miui.intent.action.CHECK_SOC"; // const-string v3, "miui.intent.action.CHECK_SOC"
v3 = (( java.lang.String ) v3 ).equals ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_23
v3 = this.this$0;
v3 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmPlugType ( v3 );
/* if-lez v3, :cond_23 */
/* .line 263 */
v3 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmHandler ( v3 );
/* const/16 v4, 0x15 */
/* const-wide/16 v5, 0x0 */
(( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) v3 ).sendMessageDelayed ( v4, v5, v6 ); // invoke-virtual {v3, v4, v5, v6}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(IJ)V
/* goto/16 :goto_8 */
/* .line 264 */
} // :cond_23
final String v3 = "android.hardware.usb.action.USB_STATE"; // const-string v3, "android.hardware.usb.action.USB_STATE"
v3 = (( java.lang.String ) v3 ).equals ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_25
/* .line 265 */
final String v3 = "configured"; // const-string v3, "configured"
int v4 = 0; // const/4 v4, 0x0
v3 = (( android.content.Intent ) v1 ).getBooleanExtra ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z
/* .line 266 */
/* .local v3, "configured":Z */
if ( v3 != null) { // if-eqz v3, :cond_24
/* .line 268 */
v4 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmHandler ( v4 );
/* const-wide/16 v5, 0xbb8 */
int v7 = 3; // const/4 v7, 0x3
(( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) v4 ).sendMessageDelayed ( v7, v1, v5, v6 ); // invoke-virtual {v4, v7, v1, v5, v6}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(ILjava/lang/Object;J)V
/* .line 270 */
} // .end local v3 # "configured":Z
} // :cond_24
/* goto/16 :goto_8 */
} // :cond_25
final String v3 = "miui.intent.action.UPDATE_BATTERY_DATA"; // const-string v3, "miui.intent.action.UPDATE_BATTERY_DATA"
v3 = (( java.lang.String ) v3 ).equals ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_26
/* .line 272 */
v3 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmHandler ( v3 );
int v4 = 0; // const/4 v4, 0x0
/* const-wide/16 v6, 0x0 */
(( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) v3 ).sendMessageDelayed ( v4, v6, v7 ); // invoke-virtual {v3, v4, v6, v7}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(IJ)V
/* .line 274 */
v3 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmHandler ( v3 );
int v4 = 7; // const/4 v4, 0x7
(( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) v3 ).sendMessageDelayed ( v4, v6, v7 ); // invoke-virtual {v3, v4, v6, v7}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(IJ)V
/* .line 276 */
v3 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmHandler ( v3 );
/* const/16 v4, 0x8 */
(( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) v3 ).sendMessageDelayed ( v4, v6, v7 ); // invoke-virtual {v3, v4, v6, v7}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(IJ)V
/* .line 278 */
v3 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmHandler ( v3 );
/* const/16 v4, 0x13 */
(( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) v3 ).sendMessageDelayed ( v4, v6, v7 ); // invoke-virtual {v3, v4, v6, v7}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(IJ)V
/* .line 279 */
v3 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmAlarmManager ( v3 );
/* .line 280 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v6 */
com.android.server.MiuiBatteryStatsService .-$$Nest$sfgetDAY ( );
/* move-result-wide v8 */
/* add-long/2addr v6, v8 */
v4 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmPendingIntent ( v4 );
/* .line 279 */
(( android.app.AlarmManager ) v3 ).setExactAndAllowWhileIdle ( v5, v6, v7, v4 ); // invoke-virtual {v3, v5, v6, v7, v4}, Landroid/app/AlarmManager;->setExactAndAllowWhileIdle(IJLandroid/app/PendingIntent;)V
/* goto/16 :goto_8 */
/* .line 281 */
} // :cond_26
final String v3 = "miui.intent.action.ACTION_SHUTDOWN_DELAY"; // const-string v3, "miui.intent.action.ACTION_SHUTDOWN_DELAY"
v3 = (( java.lang.String ) v3 ).equals ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_27
/* .line 282 */
final String v3 = "miui.intent.extra.shutdown_delay"; // const-string v3, "miui.intent.extra.shutdown_delay"
v3 = (( android.content.Intent ) v1 ).getIntExtra ( v3, v7 ); // invoke-virtual {v1, v3, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
int v4 = 1; // const/4 v4, 0x1
/* if-ne v3, v4, :cond_2d */
/* .line 283 */
v3 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmHandler ( v3 );
int v4 = 5; // const/4 v4, 0x5
int v5 = 0; // const/4 v5, 0x0
/* const-wide/16 v6, 0x0 */
(( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) v3 ).sendMessageDelayed ( v4, v5, v6, v7 ); // invoke-virtual {v3, v4, v5, v6, v7}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(IZJ)V
/* goto/16 :goto_8 */
/* .line 285 */
} // :cond_27
final String v3 = "android.intent.action.BOOT_COMPLETED"; // const-string v3, "android.intent.action.BOOT_COMPLETED"
v3 = (( java.lang.String ) v3 ).equals ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_28
/* .line 286 */
v3 = this.this$0;
int v4 = 1; // const/4 v4, 0x1
com.android.server.MiuiBatteryStatsService .-$$Nest$fputmBootCompleted ( v3,v4 );
/* .line 287 */
v3 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmHandler ( v3 );
/* const/16 v4, 0xa */
/* const-wide/16 v5, 0x0 */
(( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) v3 ).sendMessageDelayed ( v4, v5, v6 ); // invoke-virtual {v3, v4, v5, v6}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(IJ)V
/* goto/16 :goto_8 */
/* .line 288 */
} // :cond_28
/* const-wide/16 v5, 0x0 */
final String v3 = "android.intent.action.ACTION_SHUTDOWN"; // const-string v3, "android.intent.action.ACTION_SHUTDOWN"
v3 = (( java.lang.String ) v3 ).equals ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_29
/* .line 289 */
v3 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmHandler ( v3 );
/* const/16 v4, 0x9 */
(( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) v3 ).sendMessageDelayed ( v4, v5, v6 ); // invoke-virtual {v3, v4, v5, v6}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(IJ)V
/* goto/16 :goto_8 */
/* .line 290 */
} // :cond_29
final String v3 = "android.intent.action.SCREEN_ON"; // const-string v3, "android.intent.action.SCREEN_ON"
v3 = (( java.lang.String ) v3 ).equals ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_2a
/* .line 291 */
v3 = this.this$0;
int v4 = 1; // const/4 v4, 0x1
com.android.server.MiuiBatteryStatsService .-$$Nest$fputmIsScreenOn ( v3,v4 );
/* .line 292 */
v3 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmScreenOnChargingStart ( v3 );
/* move-result-wide v3 */
/* const-wide/16 v5, 0x0 */
/* cmp-long v3, v3, v5 */
/* if-nez v3, :cond_2d */
v3 = this.this$0;
v3 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmLastPlugged ( v3 );
if ( v3 != null) { // if-eqz v3, :cond_2d
/* .line 293 */
v3 = this.this$0;
java.lang.System .currentTimeMillis ( );
/* move-result-wide v4 */
com.android.server.MiuiBatteryStatsService .-$$Nest$fputmScreenOnChargingStart ( v3,v4,v5 );
/* goto/16 :goto_8 */
/* .line 295 */
} // :cond_2a
final String v3 = "android.intent.action.SCREEN_OFF"; // const-string v3, "android.intent.action.SCREEN_OFF"
v3 = (( java.lang.String ) v3 ).equals ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_2b
/* .line 296 */
v3 = this.this$0;
int v5 = 0; // const/4 v5, 0x0
com.android.server.MiuiBatteryStatsService .-$$Nest$fputmIsScreenOn ( v3,v5 );
/* .line 297 */
v3 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmScreenOnChargingStart ( v3 );
/* move-result-wide v3 */
/* const-wide/16 v5, 0x0 */
/* cmp-long v3, v3, v5 */
if ( v3 != null) { // if-eqz v3, :cond_2d
v3 = this.this$0;
v3 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmLastPlugged ( v3 );
if ( v3 != null) { // if-eqz v3, :cond_2d
/* .line 298 */
v3 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmScreenOnTime ( v3 );
/* move-result-wide v4 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v6 */
v8 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmScreenOnChargingStart ( v8 );
/* move-result-wide v8 */
/* sub-long/2addr v6, v8 */
/* add-long/2addr v4, v6 */
com.android.server.MiuiBatteryStatsService .-$$Nest$fputmScreenOnTime ( v3,v4,v5 );
/* .line 299 */
v3 = this.this$0;
/* const-wide/16 v4, 0x0 */
com.android.server.MiuiBatteryStatsService .-$$Nest$fputmScreenOnChargingStart ( v3,v4,v5 );
/* .line 302 */
} // :cond_2b
int v5 = 0; // const/4 v5, 0x0
final String v3 = "android.hardware.usb.action.USB_PORT_CHANGED"; // const-string v3, "android.hardware.usb.action.USB_PORT_CHANGED"
v3 = (( java.lang.String ) v3 ).equals ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_2d
/* .line 303 */
final String v3 = "portStatus"; // const-string v3, "portStatus"
(( android.content.Intent ) v1 ).getParcelableExtra ( v3 ); // invoke-virtual {v1, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;
/* check-cast v3, Landroid/hardware/usb/UsbPortStatus; */
/* .line 304 */
/* .local v3, "status":Landroid/hardware/usb/UsbPortStatus; */
if ( v3 != null) { // if-eqz v3, :cond_2d
/* .line 305 */
v6 = (( android.hardware.usb.UsbPortStatus ) v3 ).getCurrentDataRole ( ); // invoke-virtual {v3}, Landroid/hardware/usb/UsbPortStatus;->getCurrentDataRole()I
int v7 = 1; // const/4 v7, 0x1
/* if-ne v6, v7, :cond_2c */
/* move v9, v7 */
} // :cond_2c
/* move v9, v5 */
} // :goto_7
/* move v5, v9 */
/* .line 306 */
/* .local v5, "currentOtgConnectd":Z */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "port change currentOtgConnectd ="; // const-string v7, "port change currentOtgConnectd ="
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v6 );
/* .line 307 */
v4 = this.this$0;
v4 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmOtgConnected ( v4 );
/* if-eq v5, v4, :cond_2d */
/* .line 308 */
v4 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fputmOtgConnected ( v4,v5 );
/* .line 309 */
v4 = this.this$0;
v4 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmOtgConnected ( v4 );
if ( v4 != null) { // if-eqz v4, :cond_2d
/* .line 311 */
v4 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmHandler ( v4 );
/* const-wide/32 v6, 0xea60 */
/* const/16 v8, 0x11 */
(( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) v4 ).sendMessageDelayed ( v8, v6, v7 ); // invoke-virtual {v4, v8, v6, v7}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(IJ)V
/* .line 315 */
} // .end local v3 # "status":Landroid/hardware/usb/UsbPortStatus;
} // .end local v5 # "currentOtgConnectd":Z
} // :cond_2d
} // :goto_8
return;
} // .end method
