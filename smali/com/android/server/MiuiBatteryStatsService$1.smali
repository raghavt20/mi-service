.class Lcom/android/server/MiuiBatteryStatsService$1;
.super Landroid/content/BroadcastReceiver;
.source "MiuiBatteryStatsService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/MiuiBatteryStatsService;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/MiuiBatteryStatsService;


# direct methods
.method constructor <init>(Lcom/android/server/MiuiBatteryStatsService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/MiuiBatteryStatsService;

    .line 135
    iput-object p1, p0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 24
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 138
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 139
    .local v2, "action":Ljava/lang/String;
    iget-object v3, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v3}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetDEBUG(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v3

    const-string v4, "MiuiBatteryStatsService"

    if-eqz v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "action = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    :cond_0
    const-string v3, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    const/4 v7, -0x1

    const/4 v9, 0x0

    const/4 v10, 0x1

    if-eqz v3, :cond_20

    .line 141
    const-string v3, "plugged"

    invoke-virtual {v1, v3, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 142
    .local v3, "plugType":I
    const-string/jumbo v13, "status"

    invoke-virtual {v1, v13, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v13

    .line 143
    .local v13, "batteryStatus":I
    const-string/jumbo v14, "temperature"

    invoke-virtual {v1, v14, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v14

    .line 144
    .local v14, "batteryTemp":I
    const-string/jumbo v15, "voltage"

    invoke-virtual {v1, v15, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v15

    .line 145
    .local v15, "batteryVoltage":I
    const-string v5, "level"

    invoke-virtual {v1, v5, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    .line 146
    .local v6, "soc":I
    if-lez v3, :cond_1

    move/from16 v16, v10

    goto :goto_0

    :cond_1
    move/from16 v16, v9

    :goto_0
    move/from16 v17, v16

    .line 148
    .local v17, "plugged":Z
    move/from16 v8, v17

    .end local v17    # "plugged":Z
    .local v8, "plugged":Z
    if-eqz v8, :cond_2

    iget-object v9, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v9}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmLastPlugged(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v9

    if-eqz v9, :cond_3

    :cond_2
    if-nez v8, :cond_8

    iget-object v9, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v9}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmLastPlugged(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 149
    :cond_3
    if-eqz v8, :cond_4

    .line 150
    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5, v14}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fputmChargeMaxTemp(Lcom/android/server/MiuiBatteryStatsService;I)V

    .line 151
    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5, v14}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fputmChargeMinTemp(Lcom/android/server/MiuiBatteryStatsService;I)V

    .line 152
    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-static {v5, v10, v11}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fputmChargeStartTime(Lcom/android/server/MiuiBatteryStatsService;J)V

    .line 153
    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5, v6}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fputmChargeStartCapacity(Lcom/android/server/MiuiBatteryStatsService;I)V

    .line 154
    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmIsScreenOn(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 155
    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-static {v5, v10, v11}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fputmScreenOnChargingStart(Lcom/android/server/MiuiBatteryStatsService;J)V

    goto :goto_1

    .line 158
    :cond_4
    iget-object v10, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v11

    invoke-static {v10, v11, v12}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fputmChargeEndTime(Lcom/android/server/MiuiBatteryStatsService;J)V

    .line 159
    iget-object v10, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-virtual {v1, v5, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    invoke-static {v10, v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fputmChargeEndCapacity(Lcom/android/server/MiuiBatteryStatsService;I)V

    .line 160
    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmScreenOnChargingStart(Lcom/android/server/MiuiBatteryStatsService;)J

    move-result-wide v10

    const-wide/16 v18, 0x0

    cmp-long v5, v10, v18

    if-eqz v5, :cond_5

    .line 161
    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmScreenOnTime(Lcom/android/server/MiuiBatteryStatsService;)J

    move-result-wide v10

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    iget-object v7, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v7}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmScreenOnChargingStart(Lcom/android/server/MiuiBatteryStatsService;)J

    move-result-wide v22

    sub-long v20, v20, v22

    add-long v10, v10, v20

    invoke-static {v5, v10, v11}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fputmScreenOnTime(Lcom/android/server/MiuiBatteryStatsService;J)V

    .line 162
    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    const-wide/16 v10, 0x0

    invoke-static {v5, v10, v11}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fputmScreenOnChargingStart(Lcom/android/server/MiuiBatteryStatsService;J)V

    .line 166
    :cond_5
    :goto_1
    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    move-result-object v5

    invoke-virtual {v5, v8}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->updateChargeInfo(Z)V

    .line 167
    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    move-result-object v5

    invoke-virtual {v5, v8, v6}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->switchTimer(ZI)V

    .line 169
    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmIsTablet(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v5

    if-nez v5, :cond_8

    .line 170
    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetDEBUG(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v5

    if-eqz v5, :cond_6

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "plugType = "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    :cond_6
    const/4 v5, 0x1

    if-ne v3, v5, :cond_7

    .line 172
    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    move-result-object v5

    const/16 v7, 0xc

    const-wide/16 v10, 0x1388

    invoke-virtual {v5, v7, v10, v11}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(IJ)V

    goto :goto_2

    .line 173
    :cond_7
    if-gtz v3, :cond_8

    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmPendingIntentLimitTime(Lcom/android/server/MiuiBatteryStatsService;)Landroid/app/PendingIntent;

    move-result-object v5

    if-eqz v5, :cond_8

    .line 174
    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmAlarmManager(Lcom/android/server/MiuiBatteryStatsService;)Landroid/app/AlarmManager;

    move-result-object v5

    iget-object v7, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v7}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmPendingIntentLimitTime(Lcom/android/server/MiuiBatteryStatsService;)Landroid/app/PendingIntent;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 178
    :cond_8
    :goto_2
    const/16 v5, 0x64

    if-eqz v8, :cond_9

    const/16 v7, 0x5a

    if-lt v6, v7, :cond_9

    if-ge v6, v5, :cond_9

    .line 179
    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    move-result-object v5

    const/16 v7, 0x14

    const-wide/16 v10, 0x1388

    invoke-virtual {v5, v7, v10, v11}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(IJ)V

    goto :goto_3

    .line 180
    :cond_9
    if-eqz v8, :cond_a

    if-ne v6, v5, :cond_b

    .line 181
    :cond_a
    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    const/4 v7, 0x0

    invoke-static {v5, v7}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fputmIsOrderedCheckSocTimer(Lcom/android/server/MiuiBatteryStatsService;Z)V

    .line 182
    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmAlarmManager(Lcom/android/server/MiuiBatteryStatsService;)Landroid/app/AlarmManager;

    move-result-object v5

    if-eqz v5, :cond_b

    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmPendingIntentCheckSoc(Lcom/android/server/MiuiBatteryStatsService;)Landroid/app/PendingIntent;

    move-result-object v5

    if-eqz v5, :cond_b

    .line 183
    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmAlarmManager(Lcom/android/server/MiuiBatteryStatsService;)Landroid/app/AlarmManager;

    move-result-object v5

    iget-object v7, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v7}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmPendingIntentCheckSoc(Lcom/android/server/MiuiBatteryStatsService;)Landroid/app/PendingIntent;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 186
    :cond_b
    :goto_3
    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmFullChargeStartTime(Lcom/android/server/MiuiBatteryStatsService;)J

    move-result-wide v10

    const-wide/16 v18, 0x0

    cmp-long v5, v10, v18

    if-nez v5, :cond_c

    const/4 v5, 0x5

    if-ne v13, v5, :cond_c

    iget-object v7, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v7}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmLastBatteryStatus(Lcom/android/server/MiuiBatteryStatsService;)I

    move-result v7

    if-eq v7, v5, :cond_c

    .line 188
    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-static {v5, v10, v11}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fputmFullChargeStartTime(Lcom/android/server/MiuiBatteryStatsService;J)V

    goto :goto_4

    .line 189
    :cond_c
    const/4 v5, 0x5

    if-eq v13, v5, :cond_d

    iget-object v7, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v7}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmLastBatteryStatus(Lcom/android/server/MiuiBatteryStatsService;)I

    move-result v7

    if-ne v7, v5, :cond_d

    .line 191
    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-static {v5, v10, v11}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fputmFullChargeEndTime(Lcom/android/server/MiuiBatteryStatsService;J)V

    .line 193
    :cond_d
    :goto_4
    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetDEBUG(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v5

    if-eqz v5, :cond_e

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mLastBatteryTemp = "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v7, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v7}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmLastBatteryTemp(Lcom/android/server/MiuiBatteryStatsService;)I

    move-result v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", batteryTemp = "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    :cond_e
    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmLastBatteryTemp(Lcom/android/server/MiuiBatteryStatsService;)I

    move-result v5

    if-ne v14, v5, :cond_f

    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmLastBatteryVoltage(Lcom/android/server/MiuiBatteryStatsService;)I

    move-result v5

    if-ne v15, v5, :cond_f

    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmLastBatteryStatus(Lcom/android/server/MiuiBatteryStatsService;)I

    move-result v5

    if-eq v13, v5, :cond_13

    .line 196
    :cond_f
    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetDEBUG(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v5

    if-eqz v5, :cond_10

    .line 197
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mLastBatteryVoltage = "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v7, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v7}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmLastBatteryVoltage(Lcom/android/server/MiuiBatteryStatsService;)I

    move-result v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", batteryVoltage = "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    :cond_10
    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmLastBatteryTemp(Lcom/android/server/MiuiBatteryStatsService;)I

    move-result v5

    if-eq v14, v5, :cond_11

    .line 201
    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    move-result-object v5

    const/4 v7, 0x6

    const-wide/16 v10, 0x0

    invoke-virtual {v5, v7, v1, v10, v11}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(ILjava/lang/Object;J)V

    .line 203
    :cond_11
    const/4 v5, 0x5

    if-ne v13, v5, :cond_12

    .line 204
    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmBatteryInfoFull(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;

    move-result-object v5

    invoke-virtual {v5, v14, v15, v13}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->collectTime(III)V

    goto :goto_5

    .line 206
    :cond_12
    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmBatteryInfoNormal(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;

    move-result-object v5

    invoke-virtual {v5, v14, v15, v13}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->collectTime(III)V

    .line 209
    :cond_13
    :goto_5
    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmBootCompleted(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v5

    if-eqz v5, :cond_19

    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmLastBatteryTemp(Lcom/android/server/MiuiBatteryStatsService;)I

    move-result v5

    if-ne v14, v5, :cond_14

    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmLastSoc(Lcom/android/server/MiuiBatteryStatsService;)I

    move-result v5

    if-eq v6, v5, :cond_19

    .line 210
    :cond_14
    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmSupportedSB(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v5

    if-eqz v5, :cond_18

    .line 211
    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetDEBUG(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v5

    if-eqz v5, :cond_15

    .line 212
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mLastSoc="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v7, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v7}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmLastSoc(Lcom/android/server/MiuiBatteryStatsService;)I

    move-result v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " soc="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " System.currentTimeMillis()="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v7, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v7}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    move-result-object v7

    .line 213
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-static {v7, v10, v11}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->-$$Nest$mformatTime(Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " stopCollectTempSocTime="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v7, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v7}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    move-result-object v7

    iget-object v10, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v10}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmBatteryTempSocTime(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;

    move-result-object v10

    invoke-static {v10}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->-$$Nest$fgetstopCollectTempSocTime(Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;)J

    move-result-wide v10

    .line 214
    invoke-static {v7, v10, v11}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->-$$Nest$mformatTime(Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 212
    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    :cond_15
    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmBatteryTempSocTime(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;

    move-result-object v5

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->-$$Nest$fgetstopCollectTempSocTime(Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;)J

    move-result-wide v10

    const-wide/16 v16, 0x0

    cmp-long v5, v10, v16

    if-nez v5, :cond_16

    .line 216
    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmBatteryTempSocTime(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;

    move-result-object v5

    invoke-virtual {v5, v14, v6}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->collectTempSocData(II)V

    goto :goto_6

    .line 218
    :cond_16
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmBatteryTempSocTime(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;

    move-result-object v5

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->-$$Nest$fgetstopCollectTempSocTime(Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;)J

    move-result-wide v16

    cmp-long v5, v10, v16

    if-gez v5, :cond_17

    .line 219
    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmBatteryTempSocTime(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;

    move-result-object v5

    invoke-virtual {v5, v14, v6}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->collectTempSocData(II)V

    goto :goto_6

    .line 221
    :cond_17
    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmBatteryTempSocTime(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->processTempSocData()V

    .line 225
    :cond_18
    :goto_6
    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmSupportedCellVolt(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v5

    if-eqz v5, :cond_19

    .line 226
    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    move-result-object v5

    const/16 v7, 0xf

    const-wide/16 v10, 0x0

    invoke-virtual {v5, v7, v1, v10, v11}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(ILjava/lang/Object;J)V

    .line 229
    :cond_19
    if-eqz v8, :cond_1b

    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmLastPlugged(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v5

    if-nez v5, :cond_1b

    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmStartRecordDischarging(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v5

    if-nez v5, :cond_1b

    .line 230
    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetDEBUG(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v5

    if-eqz v5, :cond_1a

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "5s start mDischargingCount = "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v7, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v7}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmDischargingCount(Lcom/android/server/MiuiBatteryStatsService;)I

    move-result v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    :cond_1a
    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    move-result-object v5

    const/16 v7, 0xe

    const-wide/16 v10, 0x1388

    invoke-virtual {v5, v7, v10, v11}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(IJ)V

    .line 232
    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    const/4 v7, 0x1

    invoke-static {v5, v7}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fputmStartRecordDischarging(Lcom/android/server/MiuiBatteryStatsService;Z)V

    .line 233
    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5, v7}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fputmIsHandleIntermittentCharge(Lcom/android/server/MiuiBatteryStatsService;Z)V

    .line 235
    :cond_1b
    if-nez v8, :cond_1c

    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmLastPlugged(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v5

    if-eqz v5, :cond_1c

    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmIsHandleIntermittentCharge(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v5

    if-nez v5, :cond_1c

    .line 236
    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    const/4 v7, 0x0

    invoke-static {v5, v7}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fputmStartRecordDischarging(Lcom/android/server/MiuiBatteryStatsService;Z)V

    .line 238
    :cond_1c
    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmStartRecordDischarging(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v5

    if-eqz v5, :cond_1d

    const/4 v5, 0x3

    if-ne v13, v5, :cond_1d

    .line 239
    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmDischargingCount(Lcom/android/server/MiuiBatteryStatsService;)I

    move-result v7

    const/4 v9, 0x1

    add-int/2addr v7, v9

    invoke-static {v5, v7}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fputmDischargingCount(Lcom/android/server/MiuiBatteryStatsService;I)V

    .line 240
    iget-object v5, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetDEBUG(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v5

    if-eqz v5, :cond_1d

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "record mDischargingCount = "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v7, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v7}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmDischargingCount(Lcom/android/server/MiuiBatteryStatsService;)I

    move-result v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    :cond_1d
    iget-object v4, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v4}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmLastSoc(Lcom/android/server/MiuiBatteryStatsService;)I

    move-result v4

    if-eq v6, v4, :cond_1e

    .line 243
    iget-object v4, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v4}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    move-result-object v4

    invoke-static {v4, v6}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->-$$Nest$mhandleNonlinearChangeOfCapacity(Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;I)V

    .line 245
    :cond_1e
    iget-object v4, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v4}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmPlugType(Lcom/android/server/MiuiBatteryStatsService;)I

    move-result v4

    if-eq v3, v4, :cond_1f

    .line 246
    iget-object v4, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v4, v3}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fputmPlugType(Lcom/android/server/MiuiBatteryStatsService;I)V

    .line 247
    iget-object v4, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v4}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmPlugType(Lcom/android/server/MiuiBatteryStatsService;)I

    move-result v4

    if-lez v4, :cond_1f

    .line 248
    iget-object v4, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v4}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    move-result-object v4

    const/16 v5, 0x11

    const-wide/16 v9, 0x0

    invoke-virtual {v4, v5, v9, v10}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(IJ)V

    .line 251
    :cond_1f
    iget-object v4, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v4, v13}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fputmLastBatteryStatus(Lcom/android/server/MiuiBatteryStatsService;I)V

    .line 252
    iget-object v4, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v4, v8}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fputmLastPlugged(Lcom/android/server/MiuiBatteryStatsService;Z)V

    .line 253
    iget-object v4, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v4, v15}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fputmLastBatteryVoltage(Lcom/android/server/MiuiBatteryStatsService;I)V

    .line 254
    iget-object v4, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v4, v14}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fputmLastBatteryTemp(Lcom/android/server/MiuiBatteryStatsService;I)V

    .line 255
    iget-object v4, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v4, v6}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fputmLastSoc(Lcom/android/server/MiuiBatteryStatsService;I)V

    .line 256
    .end local v3    # "plugType":I
    .end local v6    # "soc":I
    .end local v8    # "plugged":Z
    .end local v13    # "batteryStatus":I
    .end local v14    # "batteryTemp":I
    .end local v15    # "batteryVoltage":I
    goto/16 :goto_8

    :cond_20
    const-string v3, "miui.intent.action.LIMIT_TIME"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_21

    .line 257
    iget-object v3, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v3}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    move-result-object v3

    const/16 v4, 0xd

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(IJ)V

    goto/16 :goto_8

    .line 258
    :cond_21
    const-string v3, "miui.intent.action.CYCLE_CHECK"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    const/4 v5, 0x2

    if-eqz v3, :cond_22

    iget-object v3, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v3}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmLastPlugged(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v3

    if-eqz v3, :cond_22

    .line 259
    iget-object v3, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v3}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    move-result-object v3

    const/16 v4, 0x10

    const-wide/16 v6, 0x0

    invoke-virtual {v3, v4, v6, v7}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(IJ)V

    .line 260
    iget-object v3, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v3}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmAlarmManager(Lcom/android/server/MiuiBatteryStatsService;)Landroid/app/AlarmManager;

    move-result-object v3

    .line 261
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    const-wide/16 v8, 0x7530

    add-long/2addr v6, v8

    iget-object v4, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v4}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmPendingIntentCycleCheck(Lcom/android/server/MiuiBatteryStatsService;)Landroid/app/PendingIntent;

    move-result-object v4

    .line 260
    invoke-virtual {v3, v5, v6, v7, v4}, Landroid/app/AlarmManager;->setExactAndAllowWhileIdle(IJLandroid/app/PendingIntent;)V

    goto/16 :goto_8

    .line 262
    :cond_22
    const-string v3, "miui.intent.action.CHECK_SOC"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_23

    iget-object v3, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v3}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmPlugType(Lcom/android/server/MiuiBatteryStatsService;)I

    move-result v3

    if-lez v3, :cond_23

    .line 263
    iget-object v3, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v3}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    move-result-object v3

    const/16 v4, 0x15

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(IJ)V

    goto/16 :goto_8

    .line 264
    :cond_23
    const-string v3, "android.hardware.usb.action.USB_STATE"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_25

    .line 265
    const-string v3, "configured"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 266
    .local v3, "configured":Z
    if-eqz v3, :cond_24

    .line 268
    iget-object v4, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v4}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    move-result-object v4

    const-wide/16 v5, 0xbb8

    const/4 v7, 0x3

    invoke-virtual {v4, v7, v1, v5, v6}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(ILjava/lang/Object;J)V

    .line 270
    .end local v3    # "configured":Z
    :cond_24
    goto/16 :goto_8

    :cond_25
    const-string v3, "miui.intent.action.UPDATE_BATTERY_DATA"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_26

    .line 272
    iget-object v3, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v3}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    move-result-object v3

    const/4 v4, 0x0

    const-wide/16 v6, 0x0

    invoke-virtual {v3, v4, v6, v7}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(IJ)V

    .line 274
    iget-object v3, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v3}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    move-result-object v3

    const/4 v4, 0x7

    invoke-virtual {v3, v4, v6, v7}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(IJ)V

    .line 276
    iget-object v3, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v3}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4, v6, v7}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(IJ)V

    .line 278
    iget-object v3, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v3}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    move-result-object v3

    const/16 v4, 0x13

    invoke-virtual {v3, v4, v6, v7}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(IJ)V

    .line 279
    iget-object v3, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v3}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmAlarmManager(Lcom/android/server/MiuiBatteryStatsService;)Landroid/app/AlarmManager;

    move-result-object v3

    .line 280
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    invoke-static {}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$sfgetDAY()J

    move-result-wide v8

    add-long/2addr v6, v8

    iget-object v4, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v4}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmPendingIntent(Lcom/android/server/MiuiBatteryStatsService;)Landroid/app/PendingIntent;

    move-result-object v4

    .line 279
    invoke-virtual {v3, v5, v6, v7, v4}, Landroid/app/AlarmManager;->setExactAndAllowWhileIdle(IJLandroid/app/PendingIntent;)V

    goto/16 :goto_8

    .line 281
    :cond_26
    const-string v3, "miui.intent.action.ACTION_SHUTDOWN_DELAY"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_27

    .line 282
    const-string v3, "miui.intent.extra.shutdown_delay"

    invoke-virtual {v1, v3, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2d

    .line 283
    iget-object v3, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v3}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    move-result-object v3

    const/4 v4, 0x5

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(IZJ)V

    goto/16 :goto_8

    .line 285
    :cond_27
    const-string v3, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_28

    .line 286
    iget-object v3, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fputmBootCompleted(Lcom/android/server/MiuiBatteryStatsService;Z)V

    .line 287
    iget-object v3, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v3}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    move-result-object v3

    const/16 v4, 0xa

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(IJ)V

    goto/16 :goto_8

    .line 288
    :cond_28
    const-wide/16 v5, 0x0

    const-string v3, "android.intent.action.ACTION_SHUTDOWN"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_29

    .line 289
    iget-object v3, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v3}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    move-result-object v3

    const/16 v4, 0x9

    invoke-virtual {v3, v4, v5, v6}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(IJ)V

    goto/16 :goto_8

    .line 290
    :cond_29
    const-string v3, "android.intent.action.SCREEN_ON"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2a

    .line 291
    iget-object v3, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fputmIsScreenOn(Lcom/android/server/MiuiBatteryStatsService;Z)V

    .line 292
    iget-object v3, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v3}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmScreenOnChargingStart(Lcom/android/server/MiuiBatteryStatsService;)J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-nez v3, :cond_2d

    iget-object v3, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v3}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmLastPlugged(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v3

    if-eqz v3, :cond_2d

    .line 293
    iget-object v3, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fputmScreenOnChargingStart(Lcom/android/server/MiuiBatteryStatsService;J)V

    goto/16 :goto_8

    .line 295
    :cond_2a
    const-string v3, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2b

    .line 296
    iget-object v3, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    const/4 v5, 0x0

    invoke-static {v3, v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fputmIsScreenOn(Lcom/android/server/MiuiBatteryStatsService;Z)V

    .line 297
    iget-object v3, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v3}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmScreenOnChargingStart(Lcom/android/server/MiuiBatteryStatsService;)J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-eqz v3, :cond_2d

    iget-object v3, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v3}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmLastPlugged(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v3

    if-eqz v3, :cond_2d

    .line 298
    iget-object v3, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v3}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmScreenOnTime(Lcom/android/server/MiuiBatteryStatsService;)J

    move-result-wide v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iget-object v8, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v8}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmScreenOnChargingStart(Lcom/android/server/MiuiBatteryStatsService;)J

    move-result-wide v8

    sub-long/2addr v6, v8

    add-long/2addr v4, v6

    invoke-static {v3, v4, v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fputmScreenOnTime(Lcom/android/server/MiuiBatteryStatsService;J)V

    .line 299
    iget-object v3, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    const-wide/16 v4, 0x0

    invoke-static {v3, v4, v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fputmScreenOnChargingStart(Lcom/android/server/MiuiBatteryStatsService;J)V

    goto :goto_8

    .line 302
    :cond_2b
    const/4 v5, 0x0

    const-string v3, "android.hardware.usb.action.USB_PORT_CHANGED"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2d

    .line 303
    const-string v3, "portStatus"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/hardware/usb/UsbPortStatus;

    .line 304
    .local v3, "status":Landroid/hardware/usb/UsbPortStatus;
    if-eqz v3, :cond_2d

    .line 305
    invoke-virtual {v3}, Landroid/hardware/usb/UsbPortStatus;->getCurrentDataRole()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_2c

    move v9, v7

    goto :goto_7

    :cond_2c
    move v9, v5

    :goto_7
    move v5, v9

    .line 306
    .local v5, "currentOtgConnectd":Z
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "port change currentOtgConnectd ="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 307
    iget-object v4, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v4}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmOtgConnected(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v4

    if-eq v5, v4, :cond_2d

    .line 308
    iget-object v4, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v4, v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fputmOtgConnected(Lcom/android/server/MiuiBatteryStatsService;Z)V

    .line 309
    iget-object v4, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v4}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmOtgConnected(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v4

    if-eqz v4, :cond_2d

    .line 311
    iget-object v4, v0, Lcom/android/server/MiuiBatteryStatsService$1;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v4}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    move-result-object v4

    const-wide/32 v6, 0xea60

    const/16 v8, 0x11

    invoke-virtual {v4, v8, v6, v7}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(IJ)V

    .line 315
    .end local v3    # "status":Landroid/hardware/usb/UsbPortStatus;
    .end local v5    # "currentOtgConnectd":Z
    :cond_2d
    :goto_8
    return-void
.end method
