.class public Lcom/android/server/ScoutSystemMonitor;
.super Lcom/android/server/ScoutStub;
.source "ScoutSystemMonitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/ScoutSystemMonitor$SystemWorkerHandler;,
        Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;,
        Lcom/android/server/ScoutSystemMonitor$ScreenStateReceiver;,
        Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;
    }
.end annotation


# static fields
.field public static final SCOUT_COMPLETED:I = 0x0

.field public static final SCOUT_FW_LEVEL_NORMAL:I = 0x0

.field public static final SCOUT_MEM_CHECK_MSG:I = 0x0

.field public static final SCOUT_MEM_CRITICAL_MSG:I = 0x2

.field public static final SCOUT_MEM_DUMP_MSG:I = 0x1

.field public static final SCOUT_OVERDUE:I = 0x3

.field public static final SCOUT_SYSTEM_IO_TIMEOUT:J = 0x7530L

.field public static final SCOUT_SYSTEM_TIMEOUT:J = 0x2710L

.field public static final SCOUT_WAITED_HALF:I = 0x2

.field public static final SCOUT_WAITING:I = 0x1

.field private static final TAG:Ljava/lang/String; = "ScoutSystemMonitor"

.field public static final mScoutHandlerCheckers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mContext:Landroid/content/Context;

.field private mScoutBinderMonitorChecker:Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;

.field private mScoutMonitorChecker:Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;

.field private mScoutSysLock:Ljava/lang/Object;

.field private mService:Lcom/android/server/am/ActivityManagerService;

.field private mSysMonitorThread:Landroid/os/HandlerThread;

.field private mSysServiceMonitorThread:Landroid/os/HandlerThread;

.field private mSysWorkThread:Landroid/os/HandlerThread;

.field private volatile mSysWorkerHandler:Lcom/android/server/ScoutSystemMonitor$SystemWorkerHandler;

.field private mUiScoutStateMachine:Landroid/app/AppScoutStateMachine;

.field private miuiFboService:Lcom/miui/app/MiuiFboServiceInternal;

.field private preScoutLevel:I

.field private scoutLevel:I


# direct methods
.method static bridge synthetic -$$Nest$fgetmScoutSysLock(Lcom/android/server/ScoutSystemMonitor;)Ljava/lang/Object;
    .locals 0

    iget-object p0, p0, Lcom/android/server/ScoutSystemMonitor;->mScoutSysLock:Ljava/lang/Object;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmiuiFboService(Lcom/android/server/ScoutSystemMonitor;)Lcom/miui/app/MiuiFboServiceInternal;
    .locals 0

    iget-object p0, p0, Lcom/android/server/ScoutSystemMonitor;->miuiFboService:Lcom/miui/app/MiuiFboServiceInternal;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmiuiFboService(Lcom/android/server/ScoutSystemMonitor;Lcom/miui/app/MiuiFboServiceInternal;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/ScoutSystemMonitor;->miuiFboService:Lcom/miui/app/MiuiFboServiceInternal;

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 81
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/ScoutSystemMonitor;->mScoutHandlerCheckers:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 125
    invoke-direct {p0}, Lcom/android/server/ScoutStub;-><init>()V

    .line 79
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/ScoutSystemMonitor;->mUiScoutStateMachine:Landroid/app/AppScoutStateMachine;

    .line 98
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/ScoutSystemMonitor;->scoutLevel:I

    .line 99
    iput v0, p0, Lcom/android/server/ScoutSystemMonitor;->preScoutLevel:I

    .line 126
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "ScoutSystemMonitor"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/ScoutSystemMonitor;->mSysMonitorThread:Landroid/os/HandlerThread;

    .line 127
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "ScoutSystemWork"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/ScoutSystemMonitor;->mSysWorkThread:Landroid/os/HandlerThread;

    .line 128
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "ScoutSystemServiceMonitor"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/ScoutSystemMonitor;->mSysServiceMonitorThread:Landroid/os/HandlerThread;

    .line 129
    invoke-static {}, Lcom/android/server/ScoutSystemMonitor;->initNativeScout()V

    .line 130
    invoke-static {}, Lcom/android/server/ScoutSystemMonitor;->registerThermalTempListener()V

    .line 131
    return-void
.end method

.method private addScoutBinderMonitor(Lcom/android/server/Watchdog$Monitor;)V
    .locals 1
    .param p1, "monitor"    # Lcom/android/server/Watchdog$Monitor;

    .line 822
    sget-boolean v0, Lcom/android/server/ScoutHelper;->ENABLED_SCOUT:Z

    if-nez v0, :cond_0

    return-void

    .line 823
    :cond_0
    monitor-enter p0

    .line 824
    :try_start_0
    iget-object v0, p0, Lcom/android/server/ScoutSystemMonitor;->mScoutBinderMonitorChecker:Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;

    invoke-virtual {v0, p1}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->addMonitorLocked(Lcom/android/server/Watchdog$Monitor;)V

    .line 825
    monitor-exit p0

    .line 826
    return-void

    .line 825
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static describeScoutCheckersLocked(Ljava/util/List;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 862
    .local p0, "checkers":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 863
    .local v0, "builder":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 864
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 865
    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 867
    :cond_0
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;

    invoke-virtual {v2}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->describeBlockedStateLocked()Ljava/lang/String;

    move-result-object v2

    .line 868
    .local v2, "info":Ljava/lang/String;
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 863
    .end local v2    # "info":Ljava/lang/String;
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 870
    .end local v1    # "i":I
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static evaluateCheckerScoutCompletionLocked()I
    .locals 4

    .line 838
    const/4 v0, 0x0

    .line 839
    .local v0, "state":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v2, Lcom/android/server/ScoutSystemMonitor;->mScoutHandlerCheckers:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 840
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;

    .line 841
    .local v2, "hc":Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;
    invoke-virtual {v2}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->getCompletionStateLocked()I

    move-result v3

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 839
    .end local v2    # "hc":Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 843
    .end local v1    # "i":I
    :cond_0
    return v0
.end method

.method public static getInstance()Lcom/android/server/ScoutSystemMonitor;
    .locals 1

    .line 105
    invoke-static {}, Lcom/android/server/ScoutStub;->getInstance()Lcom/android/server/ScoutStub;

    move-result-object v0

    check-cast v0, Lcom/android/server/ScoutSystemMonitor;

    return-object v0
.end method

.method public static getScoutBlockedCheckersLocked(Z)Ljava/util/ArrayList;
    .locals 7
    .param p0, "isHalf"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/ArrayList<",
            "Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;",
            ">;"
        }
    .end annotation

    .line 847
    sget-boolean v0, Lcom/android/server/ScoutHelper;->ENABLED_SCOUT_DEBUG:Z

    .line 848
    .local v0, "debug":Z
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 849
    .local v1, "checkers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    sget-object v3, Lcom/android/server/ScoutSystemMonitor;->mScoutHandlerCheckers:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v2, v4, :cond_4

    .line 850
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;

    .line 851
    .local v3, "hc":Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;
    invoke-virtual {v3}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->isOverdueLocked()Z

    move-result v4

    const-string v5, "ScoutSystemMonitor"

    if-nez v4, :cond_1

    if-eqz p0, :cond_0

    invoke-virtual {v3}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->isHalfLocked()Z

    move-result v4

    if-eqz v4, :cond_0

    goto :goto_1

    .line 855
    :cond_0
    if-eqz v0, :cond_3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Debug: no Block getScoutCheckersLocked Block : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->describeBlockedStateLocked()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 852
    :cond_1
    :goto_1
    if-eqz v0, :cond_2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Debug: getScoutCheckersLocked Block : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->describeBlockedStateLocked()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 853
    :cond_2
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 849
    .end local v3    # "hc":Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;
    :cond_3
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 858
    .end local v2    # "i":I
    :cond_4
    return-object v1
.end method

.method private getScoutSystemDetails(Ljava/util/List;)Ljava/lang/String;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 581
    .local p1, "handlerCheckers":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;>;"
    invoke-static {}, Lmiui/mqsas/scout/ScoutUtils;->isMtbfTest()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 582
    return-object v1

    .line 584
    :cond_0
    if-eqz p1, :cond_4

    .line 585
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 586
    .local v0, "details":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 587
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;

    .line 588
    .local v2, "mCheck":Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;
    invoke-virtual {v2}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->getThreadTid()I

    move-result v3

    .line 590
    .local v3, "tid":I
    sget-boolean v4, Lcom/android/server/ScoutHelper;->IS_INTERNATIONAL_BUILD:Z

    if-nez v4, :cond_1

    if-lez v3, :cond_1

    .line 591
    invoke-static {v3}, Lcom/android/server/ScoutHelper;->getMiuiStackTraceByTid(I)[Ljava/lang/StackTraceElement;

    move-result-object v4

    .local v4, "st":[Ljava/lang/StackTraceElement;
    goto :goto_1

    .line 593
    .end local v4    # "st":[Ljava/lang/StackTraceElement;
    :cond_1
    invoke-virtual {v2}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->getThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    .line 595
    .restart local v4    # "st":[Ljava/lang/StackTraceElement;
    :goto_1
    if-eqz v4, :cond_2

    .line 596
    array-length v5, v4

    const/4 v6, 0x0

    :goto_2
    if-ge v6, v5, :cond_2

    aget-object v7, v4, v6

    .line 597
    .local v7, "element":Ljava/lang/StackTraceElement;
    const-string v8, "    at "

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 596
    .end local v7    # "element":Ljava/lang/StackTraceElement;
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 600
    :cond_2
    const-string v5, "\n\n"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 586
    .end local v2    # "mCheck":Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;
    .end local v3    # "tid":I
    .end local v4    # "st":[Ljava/lang/StackTraceElement;
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 602
    .end local v1    # "i":I
    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 604
    .end local v0    # "details":Ljava/lang/StringBuilder;
    :cond_4
    return-object v1
.end method

.method private getScoutSystemDetailsAsync(Ljava/util/List;)Ljava/lang/String;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 527
    .local p1, "handlerCheckers":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;>;"
    const-string v0, "getScoutSystemDetails shutdown exp "

    const-string v1, "ScoutSystemMonitor"

    invoke-static {}, Lmiui/mqsas/scout/ScoutUtils;->isMtbfTest()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 529
    :cond_0
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    .line 530
    .local v2, "executor":Ljava/util/concurrent/ExecutorService;
    new-instance v3, Ljava/util/concurrent/FutureTask;

    new-instance v4, Lcom/android/server/ScoutSystemMonitor$1;

    invoke-direct {v4, p0, p1}, Lcom/android/server/ScoutSystemMonitor$1;-><init>(Lcom/android/server/ScoutSystemMonitor;Ljava/util/List;)V

    invoke-direct {v3, v4}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 555
    .local v3, "futureTask":Ljava/util/concurrent/FutureTask;, "Ljava/util/concurrent/FutureTask<Ljava/lang/String;>;"
    invoke-interface {v2, v3}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 556
    const/4 v4, 0x0

    .line 558
    .local v4, "result":Ljava/lang/String;
    :try_start_0
    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v6, 0xbb8

    invoke-virtual {v3, v6, v7, v5}, Ljava/util/concurrent/FutureTask;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v4, v5

    .line 559
    if-eqz v4, :cond_1

    .line 560
    nop

    .line 571
    :try_start_1
    invoke-interface {v2}, Ljava/util/concurrent/ExecutorService;->shutdown()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 574
    goto :goto_0

    .line 572
    :catch_0
    move-exception v5

    .line 573
    .local v5, "e":Ljava/lang/Exception;
    invoke-static {v1, v0, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 560
    .end local v5    # "e":Ljava/lang/Exception;
    :goto_0
    return-object v4

    .line 571
    :cond_1
    :try_start_2
    invoke-interface {v2}, Ljava/util/concurrent/ExecutorService;->shutdown()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 574
    :goto_1
    goto :goto_3

    .line 572
    :catch_1
    move-exception v5

    .line 573
    .restart local v5    # "e":Ljava/lang/Exception;
    invoke-static {v1, v0, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 575
    .end local v5    # "e":Ljava/lang/Exception;
    goto :goto_3

    .line 570
    :catchall_0
    move-exception v5

    goto :goto_4

    .line 562
    :catch_2
    move-exception v5

    .line 563
    .restart local v5    # "e":Ljava/lang/Exception;
    :try_start_3
    const-string v6, "getScoutSystemDetails exp is "

    invoke-static {v1, v6, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 565
    const/4 v6, 0x1

    :try_start_4
    invoke-virtual {v3, v6}, Ljava/util/concurrent/FutureTask;->cancel(Z)Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 568
    goto :goto_2

    .line 566
    :catch_3
    move-exception v6

    .line 567
    .local v6, "e1":Ljava/lang/Exception;
    :try_start_5
    const-string v7, "getScoutSystemDetails futureTask.cancel exp "

    invoke-static {v1, v7, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 571
    .end local v5    # "e":Ljava/lang/Exception;
    .end local v6    # "e1":Ljava/lang/Exception;
    :goto_2
    :try_start_6
    invoke-interface {v2}, Ljava/util/concurrent/ExecutorService;->shutdown()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_1

    .line 576
    :goto_3
    const-string v0, "getScoutSystemDetails fail"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 577
    return-object v4

    .line 571
    :goto_4
    :try_start_7
    invoke-interface {v2}, Ljava/util/concurrent/ExecutorService;->shutdown()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4

    .line 574
    goto :goto_5

    .line 572
    :catch_4
    move-exception v6

    .line 573
    .local v6, "e":Ljava/lang/Exception;
    invoke-static {v1, v0, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 575
    .end local v6    # "e":Ljava/lang/Exception;
    :goto_5
    throw v5
.end method

.method private static initNativeScout()V
    .locals 3

    .line 110
    const-string v0, "ScoutSystemMonitor"

    :try_start_0
    const-string v1, "Load libscout"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    const-string v1, "scout"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 114
    goto :goto_0

    .line 112
    :catch_0
    move-exception v1

    .line 113
    .local v1, "e":Ljava/lang/UnsatisfiedLinkError;
    const-string v2, "can\'t loadLibrary libscout"

    invoke-static {v0, v2, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 115
    .end local v1    # "e":Ljava/lang/UnsatisfiedLinkError;
    :goto_0
    return-void
.end method

.method static synthetic lambda$registerThermalTempListener$0(IJ)V
    .locals 1
    .param p0, "temp"    # I
    .param p1, "timestamp"    # J

    .line 121
    invoke-static {}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->getInstance()Lmiui/mqsas/sdk/MQSEventManagerDelegate;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->reportThermalTempChange(IJ)V

    return-void
.end method

.method static synthetic lambda$registerThermalTempListener$1(I)V
    .locals 4
    .param p0, "temp"    # I

    .line 119
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 120
    .local v0, "timestamp":J
    invoke-static {}, Lcom/android/server/MiuiBgThread;->getHandler()Landroid/os/Handler;

    move-result-object v2

    new-instance v3, Lcom/android/server/ScoutSystemMonitor$$ExternalSyntheticLambda1;

    invoke-direct {v3, p0, v0, v1}, Lcom/android/server/ScoutSystemMonitor$$ExternalSyntheticLambda1;-><init>(IJ)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 122
    return-void
.end method

.method static onFwScout(ILjava/io/File;Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;Ljava/lang/String;)V
    .locals 3
    .param p0, "type"    # I
    .param p1, "trace"    # Ljava/io/File;
    .param p2, "info"    # Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;
    .param p3, "mOtherMsg"    # Ljava/lang/String;

    .line 934
    invoke-static {}, Landroid/os/Debug;->isDebuggerConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 935
    return-void

    .line 941
    :cond_0
    new-instance v0, Lmiui/mqsas/sdk/event/SysScoutEvent;

    invoke-direct {v0}, Lmiui/mqsas/sdk/event/SysScoutEvent;-><init>()V

    .line 942
    .local v0, "event":Lmiui/mqsas/sdk/event/SysScoutEvent;
    invoke-virtual {v0, p0}, Lmiui/mqsas/sdk/event/SysScoutEvent;->setType(I)V

    .line 943
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/SysScoutEvent;->setPid(I)V

    .line 944
    const-string/jumbo v1, "system_server"

    invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/SysScoutEvent;->setProcessName(Ljava/lang/String;)V

    .line 945
    invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/SysScoutEvent;->setPackageName(Ljava/lang/String;)V

    .line 946
    invoke-virtual {p2}, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->getTimeStamp()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lmiui/mqsas/sdk/event/SysScoutEvent;->setTimeStamp(J)V

    .line 947
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/SysScoutEvent;->setSystem(Z)V

    .line 948
    invoke-virtual {p2}, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->getDescribeInfo()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/SysScoutEvent;->setSummary(Ljava/lang/String;)V

    .line 949
    invoke-virtual {p2}, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->getDetails()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 950
    invoke-virtual {p2}, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->getDetails()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/SysScoutEvent;->setDetails(Ljava/lang/String;)V

    goto :goto_0

    .line 952
    :cond_1
    invoke-virtual {p2}, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->getDescribeInfo()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/SysScoutEvent;->setDetails(Ljava/lang/String;)V

    .line 954
    :goto_0
    invoke-virtual {v0, p3}, Lmiui/mqsas/sdk/event/SysScoutEvent;->setOtherMsg(Ljava/lang/String;)V

    .line 955
    invoke-virtual {p2}, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->getScoutLevel()I

    move-result v1

    invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/SysScoutEvent;->setScoutLevel(I)V

    .line 956
    invoke-virtual {p2}, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->getPreScoutLevel()I

    move-result v1

    invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/SysScoutEvent;->setPreScoutLevel(I)V

    .line 957
    if-eqz p1, :cond_2

    .line 958
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/SysScoutEvent;->setLogName(Ljava/lang/String;)V

    .line 960
    :cond_2
    invoke-virtual {p2}, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->getBinderTransInfo()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/SysScoutEvent;->setBinderTransactionInfo(Ljava/lang/String;)V

    .line 961
    invoke-virtual {p2}, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->getUuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/SysScoutEvent;->setUuid(Ljava/lang/String;)V

    .line 962
    invoke-static {}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->getInstance()Lmiui/mqsas/sdk/MQSEventManagerDelegate;

    move-result-object v1

    invoke-virtual {v1, v0}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->reportSysScoutEvent(Lmiui/mqsas/sdk/event/SysScoutEvent;)V

    .line 963
    return-void
.end method

.method private registerScreenStateReceiver()V
    .locals 3

    .line 273
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 274
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 275
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 276
    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->setPriority(I)V

    .line 277
    iget-object v1, p0, Lcom/android/server/ScoutSystemMonitor;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/android/server/ScoutSystemMonitor$ScreenStateReceiver;

    invoke-direct {v2, p0}, Lcom/android/server/ScoutSystemMonitor$ScreenStateReceiver;-><init>(Lcom/android/server/ScoutSystemMonitor;)V

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 278
    return-void
.end method

.method private static registerThermalTempListener()V
    .locals 2

    .line 118
    invoke-static {}, Lcom/android/server/am/SystemPressureControllerStub;->getInstance()Lcom/android/server/am/SystemPressureControllerStub;

    move-result-object v0

    new-instance v1, Lcom/android/server/ScoutSystemMonitor$$ExternalSyntheticLambda0;

    invoke-direct {v1}, Lcom/android/server/ScoutSystemMonitor$$ExternalSyntheticLambda0;-><init>()V

    invoke-virtual {v0, v1}, Lcom/android/server/am/SystemPressureControllerStub;->registerThermalTempListener(Lcom/android/server/am/ThermalTempListener;)V

    .line 123
    return-void
.end method

.method private runSystemMonitor(JIZLcom/android/server/ScoutWatchdogInfo$ScoutId;)V
    .locals 30
    .param p1, "timeout"    # J
    .param p3, "count"    # I
    .param p4, "waitedHalf"    # Z
    .param p5, "scoutId"    # Lcom/android/server/ScoutWatchdogInfo$ScoutId;

    .line 609
    move-object/from16 v1, p0

    move/from16 v2, p3

    const-wide/16 v3, 0x2710

    .line 610
    .local v3, "waitTime":J
    const-wide/16 v5, 0x2710

    cmp-long v0, p1, v5

    if-gez v0, :cond_0

    .line 611
    move-wide/from16 v3, p1

    move-wide v11, v3

    goto :goto_0

    .line 610
    :cond_0
    move-wide v11, v3

    .line 614
    .end local v3    # "waitTime":J
    .local v11, "waitTime":J
    :goto_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    sget-object v3, Lcom/android/server/ScoutSystemMonitor;->mScoutHandlerCheckers:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 615
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;

    .line 616
    .local v3, "hc":Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;
    invoke-virtual {v3}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->scheduleCheckLocked()V

    .line 614
    .end local v3    # "hc":Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 619
    .end local v0    # "i":I
    :cond_1
    move-wide v3, v11

    .line 620
    .local v3, "scoutTimeout":J
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v13

    move-wide v9, v3

    .line 621
    .end local v3    # "scoutTimeout":J
    .local v9, "scoutTimeout":J
    .local v13, "scoutStart":J
    :goto_2
    const-wide/16 v3, 0x0

    cmp-long v0, v9, v3

    if-lez v0, :cond_2

    .line 623
    :try_start_0
    iget-object v0, v1, Lcom/android/server/ScoutSystemMonitor;->mScoutSysLock:Ljava/lang/Object;

    invoke-virtual {v0, v9, v10}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 626
    goto :goto_3

    .line 624
    :catch_0
    move-exception v0

    .line 625
    .local v0, "ex":Ljava/lang/InterruptedException;
    const-string v3, "ScoutSystemMonitor"

    invoke-static {v3, v0}, Landroid/util/Slog;->wtf(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 627
    .end local v0    # "ex":Ljava/lang/InterruptedException;
    :goto_3
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    sub-long/2addr v3, v13

    sub-long v9, v11, v3

    goto :goto_2

    .line 631
    :cond_2
    invoke-static {}, Lcom/android/server/ScoutSystemMonitor;->evaluateCheckerScoutCompletionLocked()I

    move-result v0

    .line 632
    .local v0, "waitState":I
    invoke-virtual/range {p5 .. p5}, Lcom/android/server/ScoutWatchdogInfo$ScoutId;->getUuid()Ljava/lang/String;

    move-result-object v15

    .line 633
    .local v15, "uuid":Ljava/lang/String;
    iget v7, v1, Lcom/android/server/ScoutSystemMonitor;->scoutLevel:I

    iput v7, v1, Lcom/android/server/ScoutSystemMonitor;->preScoutLevel:I

    .line 634
    const/4 v3, 0x3

    const-string v8, "ms"

    const-string v6, " waittime : "

    const-string v5, "; Scout Check count : "

    const-string v4, " to "

    move-object/from16 v16, v8

    const-string v8, "MIUIScout System"

    move-wide/from16 v17, v13

    .end local v13    # "scoutStart":J
    .local v17, "scoutStart":J
    const/4 v13, 0x1

    const/4 v14, 0x2

    if-eq v0, v3, :cond_7

    if-ne v0, v14, :cond_3

    if-ne v2, v14, :cond_3

    move-wide/from16 v19, v9

    move-object v10, v4

    move-object v9, v5

    move-object v4, v8

    move-object/from16 v5, v16

    move-object v8, v6

    move-object/from16 v6, p5

    goto/16 :goto_5

    .line 663
    :cond_3
    const/4 v14, 0x0

    iput v14, v1, Lcom/android/server/ScoutSystemMonitor;->scoutLevel:I

    .line 664
    if-le v7, v13, :cond_5

    .line 665
    new-instance v13, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;

    const-string v19, ""

    const-string v20, ""

    const/16 v21, 0x0

    move-object v3, v13

    move-object/from16 v22, v4

    move-object/from16 v4, v19

    move-object/from16 v23, v5

    move-object/from16 v5, v20

    move-object/from16 v24, v6

    move v6, v14

    move-object/from16 v25, v8

    move-object/from16 v14, v16

    move-object/from16 v8, v21

    move-wide/from16 v19, v9

    .end local v9    # "scoutTimeout":J
    .local v19, "scoutTimeout":J
    move/from16 v9, p4

    move-object v10, v15

    invoke-direct/range {v3 .. v10}, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;-><init>(Ljava/lang/String;Ljava/lang/String;IILjava/util/ArrayList;ZLjava/lang/String;)V

    .line 666
    .local v3, "info":Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->setTimeStamp(J)V

    .line 667
    const-string v4, ""

    const/4 v5, 0x0

    if-eqz p4, :cond_4

    .line 668
    const/16 v6, 0x192

    invoke-virtual {v3, v6}, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->setEvent(I)V

    .line 669
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Enter FW_SCOUT_NORMALLY from Level "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v1, Lcom/android/server/ScoutSystemMonitor;->preScoutLevel:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v10, v22

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v1, Lcom/android/server/ScoutSystemMonitor;->scoutLevel:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v9, v23

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v8, v24

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v13, v25

    invoke-static {v13, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 672
    invoke-static {v6, v5, v3, v4}, Lcom/android/server/ScoutSystemMonitor;->onFwScout(ILjava/io/File;Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 674
    :cond_4
    move-object/from16 v10, v22

    move-object/from16 v9, v23

    move-object/from16 v8, v24

    move-object/from16 v13, v25

    const/16 v6, 0x193

    invoke-virtual {v3, v6}, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->setEvent(I)V

    .line 675
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Enter FW_SCOUT_SLOW from Level "

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v7, v1, Lcom/android/server/ScoutSystemMonitor;->preScoutLevel:I

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v7, v1, Lcom/android/server/ScoutSystemMonitor;->scoutLevel:I

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v13, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 678
    const/4 v5, 0x0

    invoke-static {v6, v5, v3, v4}, Lcom/android/server/ScoutSystemMonitor;->onFwScout(ILjava/io/File;Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;Ljava/lang/String;)V

    goto :goto_4

    .line 680
    .end local v3    # "info":Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;
    .end local v19    # "scoutTimeout":J
    .restart local v9    # "scoutTimeout":J
    :cond_5
    move-object v13, v8

    move-wide/from16 v19, v9

    move-object/from16 v14, v16

    move-object v10, v4

    move-object v9, v5

    move-object v8, v6

    .end local v9    # "scoutTimeout":J
    .restart local v19    # "scoutTimeout":J
    if-lez v7, :cond_6

    .line 681
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "FW Resume from Level "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Lcom/android/server/ScoutSystemMonitor;->preScoutLevel:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Lcom/android/server/ScoutSystemMonitor;->scoutLevel:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v13, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 685
    :cond_6
    :goto_4
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v15

    .line 686
    move-object/from16 v6, p5

    invoke-virtual {v6, v15}, Lcom/android/server/ScoutWatchdogInfo$ScoutId;->setUuid(Ljava/lang/String;)V

    move/from16 v24, v0

    move-wide v4, v11

    goto/16 :goto_9

    .line 634
    .end local v19    # "scoutTimeout":J
    .restart local v9    # "scoutTimeout":J
    :cond_7
    move-wide/from16 v19, v9

    move-object v10, v4

    move-object v9, v5

    move-object v4, v8

    move-object/from16 v5, v16

    move-object v8, v6

    move-object/from16 v6, p5

    .line 635
    .end local v9    # "scoutTimeout":J
    .restart local v19    # "scoutTimeout":J
    :goto_5
    add-int/2addr v7, v13

    iput v7, v1, Lcom/android/server/ScoutSystemMonitor;->scoutLevel:I

    .line 636
    if-ne v2, v14, :cond_9

    if-nez p4, :cond_9

    if-gt v7, v14, :cond_8

    goto :goto_6

    :cond_8
    move/from16 v24, v0

    move-wide v4, v11

    goto/16 :goto_9

    :cond_9
    :goto_6
    if-ne v2, v14, :cond_a

    if-eqz p4, :cond_a

    const/4 v3, 0x5

    if-gt v7, v3, :cond_8

    .line 638
    :cond_a
    if-ne v0, v14, :cond_b

    move v3, v13

    goto :goto_7

    :cond_b
    const/4 v3, 0x0

    :goto_7
    move v14, v3

    .line 639
    .local v14, "isHalf":Z
    invoke-static {v14}, Lcom/android/server/ScoutSystemMonitor;->getScoutBlockedCheckersLocked(Z)Ljava/util/ArrayList;

    move-result-object v3

    .line 640
    .local v3, "handlerChecke":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;>;"
    invoke-static {v3}, Lcom/android/server/ScoutSystemMonitor;->describeScoutCheckersLocked(Ljava/util/List;)Ljava/lang/String;

    move-result-object v13

    .line 641
    .local v13, "scoutDescribe":Ljava/lang/String;
    invoke-direct {v1, v3}, Lcom/android/server/ScoutSystemMonitor;->getScoutSystemDetailsAsync(Ljava/util/List;)Ljava/lang/String;

    move-result-object v21

    .line 642
    .local v21, "scoutDetails":Ljava/lang/String;
    new-instance v22, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;

    iget v7, v1, Lcom/android/server/ScoutSystemMonitor;->scoutLevel:I

    move/from16 v24, v0

    .end local v0    # "waitState":I
    .local v24, "waitState":I
    iget v0, v1, Lcom/android/server/ScoutSystemMonitor;->preScoutLevel:I

    move-object/from16 v25, v3

    .end local v3    # "handlerChecke":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;>;"
    .local v25, "handlerChecke":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;>;"
    move-object/from16 v3, v22

    move/from16 v26, v14

    move-object v14, v4

    .end local v14    # "isHalf":Z
    .local v26, "isHalf":Z
    move-object v4, v13

    move-object/from16 v27, v14

    move-object v14, v5

    move-object/from16 v5, v21

    move v6, v7

    move v7, v0

    move-object v0, v8

    move-object/from16 v8, v25

    move-object/from16 v23, v14

    move-object v14, v9

    move/from16 v9, p4

    move-wide/from16 v28, v11

    move-object v11, v10

    .end local v11    # "waitTime":J
    .local v28, "waitTime":J
    move-object v10, v15

    invoke-direct/range {v3 .. v10}, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;-><init>(Ljava/lang/String;Ljava/lang/String;IILjava/util/ArrayList;ZLjava/lang/String;)V

    .line 645
    .local v3, "info":Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;
    const-string v4, "BinderThreadMonitor"

    invoke-virtual {v13, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 646
    const/16 v4, 0x191

    invoke-virtual {v3, v4}, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->setEvent(I)V

    .line 647
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Enter FW_SCOUT_BINDER_FULL from Level "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v1, Lcom/android/server/ScoutSystemMonitor;->preScoutLevel:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v1, Lcom/android/server/ScoutSystemMonitor;->scoutLevel:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    move-wide/from16 v4, v28

    .end local v28    # "waitTime":J
    .local v4, "waitTime":J
    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    move-object/from16 v6, v23

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object/from16 v7, v27

    invoke-static {v7, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 650
    iget-object v0, v1, Lcom/android/server/ScoutSystemMonitor;->mSysWorkerHandler:Lcom/android/server/ScoutSystemMonitor$SystemWorkerHandler;

    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lcom/android/server/ScoutSystemMonitor$SystemWorkerHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .local v0, "msg":Landroid/os/Message;
    goto :goto_8

    .line 652
    .end local v0    # "msg":Landroid/os/Message;
    .end local v4    # "waitTime":J
    .restart local v28    # "waitTime":J
    :cond_c
    move-object/from16 v6, v23

    move-object/from16 v7, v27

    move-wide/from16 v4, v28

    .end local v28    # "waitTime":J
    .restart local v4    # "waitTime":J
    const/16 v8, 0x190

    invoke-virtual {v3, v8}, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->setEvent(I)V

    .line 653
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Enter FW_SCOUT_HANG from Level "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v1, Lcom/android/server/ScoutSystemMonitor;->preScoutLevel:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v1, Lcom/android/server/ScoutSystemMonitor;->scoutLevel:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 656
    iget-object v0, v1, Lcom/android/server/ScoutSystemMonitor;->mSysWorkerHandler:Lcom/android/server/ScoutSystemMonitor$SystemWorkerHandler;

    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/android/server/ScoutSystemMonitor$SystemWorkerHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 658
    .restart local v0    # "msg":Landroid/os/Message;
    :goto_8
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->setTimeStamp(J)V

    .line 659
    iput-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 660
    iget-object v6, v1, Lcom/android/server/ScoutSystemMonitor;->mSysWorkerHandler:Lcom/android/server/ScoutSystemMonitor$SystemWorkerHandler;

    invoke-virtual {v6, v0}, Lcom/android/server/ScoutSystemMonitor$SystemWorkerHandler;->sendMessage(Landroid/os/Message;)Z

    .line 661
    .end local v0    # "msg":Landroid/os/Message;
    .end local v13    # "scoutDescribe":Ljava/lang/String;
    .end local v21    # "scoutDetails":Ljava/lang/String;
    .end local v25    # "handlerChecke":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;>;"
    .end local v26    # "isHalf":Z
    nop

    .line 688
    .end local v3    # "info":Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;
    :goto_9
    return-void
.end method


# virtual methods
.method public addScoutMonitor(Lcom/android/server/Watchdog$Monitor;)V
    .locals 1
    .param p1, "monitor"    # Lcom/android/server/Watchdog$Monitor;

    .line 815
    sget-boolean v0, Lcom/android/server/ScoutHelper;->ENABLED_SCOUT:Z

    if-nez v0, :cond_0

    return-void

    .line 816
    :cond_0
    monitor-enter p0

    .line 817
    :try_start_0
    iget-object v0, p0, Lcom/android/server/ScoutSystemMonitor;->mScoutMonitorChecker:Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;

    invoke-virtual {v0, p1}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->addMonitorLocked(Lcom/android/server/Watchdog$Monitor;)V

    .line 818
    monitor-exit p0

    .line 819
    return-void

    .line 818
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addScoutThread(Landroid/os/Handler;)V
    .locals 8
    .param p1, "thread"    # Landroid/os/Handler;

    .line 830
    sget-boolean v0, Lcom/android/server/ScoutHelper;->ENABLED_SCOUT:Z

    if-nez v0, :cond_0

    return-void

    .line 831
    :cond_0
    monitor-enter p0

    .line 832
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v4

    .line 833
    .local v4, "name":Ljava/lang/String;
    sget-object v0, Lcom/android/server/ScoutSystemMonitor;->mScoutHandlerCheckers:Ljava/util/ArrayList;

    new-instance v7, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;

    const-wide/16 v5, 0x2710

    move-object v1, v7

    move-object v2, p0

    move-object v3, p1

    invoke-direct/range {v1 .. v6}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;-><init>(Lcom/android/server/ScoutSystemMonitor;Landroid/os/Handler;Ljava/lang/String;J)V

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 834
    nop

    .end local v4    # "name":Ljava/lang/String;
    monitor-exit p0

    .line 835
    return-void

    .line 834
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public crashIfHasDThread(Z)V
    .locals 2
    .param p1, "mHasDThread"    # Z

    .line 173
    if-eqz p1, :cond_0

    const-string v0, "ScoutSystemMonitor"

    invoke-static {v0}, Lcom/android/server/ScoutHelper;->isEnabelPanicDThread(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 174
    const-string/jumbo v1, "trigger kernel crash: Has D state thread"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    const-wide/16 v0, 0xbb8

    invoke-static {v0, v1}, Landroid/os/SystemClock;->sleep(J)V

    .line 178
    const/16 v0, 0x63

    invoke-static {v0}, Lcom/android/server/ScoutHelper;->doSysRqInterface(C)V

    .line 180
    :cond_0
    return-void
.end method

.method public getSystemWorkerHandler()Landroid/os/Handler;
    .locals 1

    .line 269
    iget-object v0, p0, Lcom/android/server/ScoutSystemMonitor;->mSysWorkerHandler:Lcom/android/server/ScoutSystemMonitor$SystemWorkerHandler;

    return-object v0
.end method

.method public getUiScoutStateMachine()Landroid/app/AppScoutStateMachine;
    .locals 1

    .line 911
    iget-object v0, p0, Lcom/android/server/ScoutSystemMonitor;->mUiScoutStateMachine:Landroid/app/AppScoutStateMachine;

    return-object v0
.end method

.method public init(Ljava/lang/Object;)V
    .locals 14
    .param p1, "mLock"    # Ljava/lang/Object;

    .line 134
    iput-object p1, p0, Lcom/android/server/ScoutSystemMonitor;->mScoutSysLock:Ljava/lang/Object;

    .line 135
    iget-object v0, p0, Lcom/android/server/ScoutSystemMonitor;->mSysWorkThread:Landroid/os/HandlerThread;

    if-eqz v0, :cond_0

    .line 136
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 137
    new-instance v0, Lcom/android/server/ScoutSystemMonitor$SystemWorkerHandler;

    iget-object v1, p0, Lcom/android/server/ScoutSystemMonitor;->mSysWorkThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/server/ScoutSystemMonitor$SystemWorkerHandler;-><init>(Lcom/android/server/ScoutSystemMonitor;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/ScoutSystemMonitor;->mSysWorkerHandler:Lcom/android/server/ScoutSystemMonitor$SystemWorkerHandler;

    .line 139
    :cond_0
    iget-object v0, p0, Lcom/android/server/ScoutSystemMonitor;->mSysMonitorThread:Landroid/os/HandlerThread;

    if-eqz v0, :cond_1

    .line 140
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 141
    new-instance v0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;

    iget-object v1, p0, Lcom/android/server/ScoutSystemMonitor;->mSysMonitorThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getThreadHandler()Landroid/os/Handler;

    move-result-object v3

    const-string v4, "ScoutSystemMonitor thread"

    const-wide/16 v5, 0x2710

    move-object v1, v0

    move-object v2, p0

    invoke-direct/range {v1 .. v6}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;-><init>(Lcom/android/server/ScoutSystemMonitor;Landroid/os/Handler;Ljava/lang/String;J)V

    iput-object v0, p0, Lcom/android/server/ScoutSystemMonitor;->mScoutBinderMonitorChecker:Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;

    .line 143
    sget-object v1, Lcom/android/server/ScoutSystemMonitor;->mScoutHandlerCheckers:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 145
    :cond_1
    iget-object v0, p0, Lcom/android/server/ScoutSystemMonitor;->mSysServiceMonitorThread:Landroid/os/HandlerThread;

    if-eqz v0, :cond_2

    .line 146
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 147
    new-instance v0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;

    iget-object v1, p0, Lcom/android/server/ScoutSystemMonitor;->mSysServiceMonitorThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getThreadHandler()Landroid/os/Handler;

    move-result-object v3

    const-string v4, "ScoutSystemServiceMonitor thread"

    const-wide/16 v5, 0x2710

    move-object v1, v0

    move-object v2, p0

    invoke-direct/range {v1 .. v6}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;-><init>(Lcom/android/server/ScoutSystemMonitor;Landroid/os/Handler;Ljava/lang/String;J)V

    iput-object v0, p0, Lcom/android/server/ScoutSystemMonitor;->mScoutMonitorChecker:Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;

    .line 150
    :cond_2
    sget-object v0, Lcom/android/server/ScoutSystemMonitor;->mScoutHandlerCheckers:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/server/ScoutSystemMonitor;->mScoutMonitorChecker:Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 151
    new-instance v1, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;

    new-instance v4, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v4, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    const-string v5, "main thread"

    const-wide/16 v6, 0x2710

    move-object v2, v1

    move-object v3, p0

    invoke-direct/range {v2 .. v7}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;-><init>(Lcom/android/server/ScoutSystemMonitor;Landroid/os/Handler;Ljava/lang/String;J)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 153
    new-instance v1, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;

    invoke-static {}, Lcom/android/server/UiThread;->getHandler()Landroid/os/Handler;

    move-result-object v10

    const-string/jumbo v11, "ui thread"

    const-wide/16 v12, 0x2710

    move-object v8, v1

    move-object v9, p0

    invoke-direct/range {v8 .. v13}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;-><init>(Lcom/android/server/ScoutSystemMonitor;Landroid/os/Handler;Ljava/lang/String;J)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 155
    new-instance v1, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;

    invoke-static {}, Lcom/android/server/IoThread;->getHandler()Landroid/os/Handler;

    move-result-object v4

    const-string v5, "i/o thread"

    const-wide/16 v6, 0x7530

    move-object v2, v1

    invoke-direct/range {v2 .. v7}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;-><init>(Lcom/android/server/ScoutSystemMonitor;Landroid/os/Handler;Ljava/lang/String;J)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 157
    new-instance v1, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;

    invoke-static {}, Lcom/android/server/DisplayThread;->getHandler()Landroid/os/Handler;

    move-result-object v10

    const-string v11, "display thread"

    move-object v8, v1

    invoke-direct/range {v8 .. v13}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;-><init>(Lcom/android/server/ScoutSystemMonitor;Landroid/os/Handler;Ljava/lang/String;J)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 159
    new-instance v1, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;

    invoke-static {}, Lcom/android/server/AnimationThread;->getHandler()Landroid/os/Handler;

    move-result-object v4

    const-string v5, "animation thread"

    const-wide/16 v6, 0x2710

    move-object v2, v1

    invoke-direct/range {v2 .. v7}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;-><init>(Lcom/android/server/ScoutSystemMonitor;Landroid/os/Handler;Ljava/lang/String;J)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 161
    new-instance v1, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;

    invoke-static {}, Lcom/android/server/wm/SurfaceAnimationThread;->getHandler()Landroid/os/Handler;

    move-result-object v10

    const-string/jumbo v11, "surface animation thread"

    move-object v8, v1

    invoke-direct/range {v8 .. v13}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;-><init>(Lcom/android/server/ScoutSystemMonitor;Landroid/os/Handler;Ljava/lang/String;J)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 163
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/server/ScoutSystemMonitor;->updateScreenState(Z)V

    .line 164
    invoke-static {}, Lcom/android/server/ScoutHelper;->copyRamoopsFileToMqs()V

    .line 165
    invoke-static {}, Lcom/miui/server/stability/ScoutLibraryTestManager;->getInstance()Lcom/miui/server/stability/ScoutLibraryTestManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/server/stability/ScoutLibraryTestManager;->init()V

    .line 166
    return-void
.end method

.method public pauseScoutWatchingCurrentThread(Ljava/lang/String;)V
    .locals 4
    .param p1, "reason"    # Ljava/lang/String;

    .line 875
    sget-boolean v0, Lcom/android/server/ScoutHelper;->ENABLED_SCOUT:Z

    if-nez v0, :cond_0

    return-void

    .line 876
    :cond_0
    monitor-enter p0

    .line 877
    :try_start_0
    sget-object v0, Lcom/android/server/ScoutSystemMonitor;->mScoutHandlerCheckers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;

    .line 878
    .local v1, "hc":Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v1}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->getThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 879
    invoke-virtual {v1, p1}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->pauseLocked(Ljava/lang/String;)V

    .line 881
    .end local v1    # "hc":Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;
    :cond_1
    goto :goto_0

    .line 882
    :cond_2
    monitor-exit p0

    .line 883
    return-void

    .line 882
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public reportRebootNullEventtoMqs(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "processName"    # Ljava/lang/String;
    .param p2, "processId"    # I
    .param p3, "triggerWay"    # Ljava/lang/String;
    .param p4, "intentName"    # Ljava/lang/String;

    .line 243
    invoke-virtual {p0}, Lcom/android/server/ScoutSystemMonitor;->shouldRebootReasonCheckNull()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    const-string v1, "debug.record.rebootnull"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_2

    .line 244
    const/4 v0, 0x0

    .line 245
    .local v0, "details":Ljava/lang/String;
    const-string v2, "intent"

    invoke-virtual {p3, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 246
    const-string v2, "Shutdown intent checkpoint recorded intent=%s from package=%s"

    filled-new-array {p4, p1}, [Ljava/lang/Object;

    move-result-object v3

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 247
    const-string/jumbo v2, "true"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 248
    :cond_0
    const-string v1, "binder"

    invoke-virtual {p3, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 249
    invoke-static {p2}, Lcom/android/server/am/ProcessUtils;->getProcessNameByPid(I)Ljava/lang/String;

    move-result-object p1

    .line 250
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Binder shutdown checkpoint recorded with package="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 252
    :cond_1
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " by "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 253
    .local v1, "caller":Ljava/lang/String;
    new-instance v2, Lmiui/mqsas/sdk/event/RebootNullEvent;

    invoke-direct {v2}, Lmiui/mqsas/sdk/event/RebootNullEvent;-><init>()V

    .line 254
    .local v2, "event":Lmiui/mqsas/sdk/event/RebootNullEvent;
    invoke-virtual {v2, p1}, Lmiui/mqsas/sdk/event/RebootNullEvent;->setProcessName(Ljava/lang/String;)V

    .line 255
    invoke-virtual {v2, p1}, Lmiui/mqsas/sdk/event/RebootNullEvent;->setPackageName(Ljava/lang/String;)V

    .line 256
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lmiui/mqsas/sdk/event/RebootNullEvent;->setTimeStamp(J)V

    .line 257
    invoke-virtual {v2, v1}, Lmiui/mqsas/sdk/event/RebootNullEvent;->setCaller(Ljava/lang/String;)V

    .line 258
    const-string v3, "reboot or shutdown with null reason"

    invoke-virtual {v2, v3}, Lmiui/mqsas/sdk/event/RebootNullEvent;->setSummary(Ljava/lang/String;)V

    .line 259
    invoke-virtual {v2, v0}, Lmiui/mqsas/sdk/event/RebootNullEvent;->setDetails(Ljava/lang/String;)V

    .line 260
    invoke-static {}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->getInstance()Lmiui/mqsas/sdk/MQSEventManagerDelegate;

    move-result-object v3

    invoke-virtual {v3, v2}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->reportRebootNullEvent(Lmiui/mqsas/sdk/event/RebootNullEvent;)V

    .line 262
    .end local v0    # "details":Ljava/lang/String;
    .end local v1    # "caller":Ljava/lang/String;
    .end local v2    # "event":Lmiui/mqsas/sdk/event/RebootNullEvent;
    :cond_2
    return-void
.end method

.method public resetClipProp(Ljava/lang/String;)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .line 926
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 927
    const-string v0, "com.milink.service"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 928
    const-string v0, "persist.sys.debug.app.clipdata"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 930
    :cond_0
    return-void
.end method

.method public resumeScoutWatchingCurrentThread(Ljava/lang/String;)V
    .locals 4
    .param p1, "reason"    # Ljava/lang/String;

    .line 887
    sget-boolean v0, Lcom/android/server/ScoutHelper;->ENABLED_SCOUT:Z

    if-nez v0, :cond_0

    return-void

    .line 888
    :cond_0
    monitor-enter p0

    .line 889
    :try_start_0
    sget-object v0, Lcom/android/server/ScoutSystemMonitor;->mScoutHandlerCheckers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;

    .line 890
    .local v1, "hc":Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v1}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->getThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 891
    invoke-virtual {v1, p1}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->resumeLocked(Ljava/lang/String;)V

    .line 893
    .end local v1    # "hc":Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;
    :cond_1
    goto :goto_0

    .line 894
    :cond_2
    monitor-exit p0

    .line 895
    return-void

    .line 894
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public scoutSystemCheckBinderCallChain(Ljava/util/ArrayList;Ljava/util/ArrayList;Lcom/android/server/ScoutWatchdogInfo;)V
    .locals 8
    .param p3, "watchdoginfo"    # Lcom/android/server/ScoutWatchdogInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/android/server/ScoutWatchdogInfo;",
            ")V"
        }
    .end annotation

    .line 186
    .local p1, "pids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .local p2, "nativePids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 187
    .local v0, "scoutJavaPids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    move-object v1, v2

    .line 188
    .local v1, "scoutNativePids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v2, Lcom/android/server/ScoutHelper$ScoutBinderInfo;

    .line 189
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    const/4 v4, 0x0

    const-string v5, "MIUIScout Watchdog"

    invoke-direct {v2, v3, v4, v5}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;-><init>(IILjava/lang/String;)V

    .line 191
    .local v2, "scoutBinderInfo":Lcom/android/server/ScoutHelper$ScoutBinderInfo;
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 192
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    invoke-static {v3, v2, v0, v1}, Lcom/android/server/ScoutHelper;->checkBinderCallPidList(ILcom/android/server/ScoutHelper$ScoutBinderInfo;Ljava/util/ArrayList;Ljava/util/ArrayList;)Z

    .line 194
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->getBinderTransInfo()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 195
    invoke-virtual {v2}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->getProcInfo()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 194
    invoke-virtual {p3, v3}, Lcom/android/server/ScoutWatchdogInfo;->setBinderTransInfo(Ljava/lang/String;)V

    .line 196
    invoke-virtual {v2}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->getDThreadState()Z

    move-result v3

    invoke-virtual {p3, v3}, Lcom/android/server/ScoutWatchdogInfo;->setDThreadState(Z)V

    .line 197
    invoke-virtual {v2}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->getMonkeyPid()I

    move-result v3

    invoke-virtual {p3, v3}, Lcom/android/server/ScoutWatchdogInfo;->setMonkeyPid(I)V

    .line 199
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    const-string v4, "Dump Trace: add java proc "

    const-string v5, "ScoutSystemMonitor"

    if-lez v3, :cond_1

    .line 200
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 201
    .local v6, "javaPid":I
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {p1, v7}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 202
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {p1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 203
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    .end local v6    # "javaPid":I
    :cond_0
    goto :goto_0

    .line 208
    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_3

    .line 209
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 210
    .local v6, "nativePid":I
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {p2, v7}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 211
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {p2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 212
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    .end local v6    # "nativePid":I
    :cond_2
    goto :goto_1

    .line 216
    :cond_3
    return-void
.end method

.method public scoutSystemMonitorEnable()Z
    .locals 1

    .line 220
    sget-boolean v0, Lcom/android/server/ScoutHelper;->ENABLED_SCOUT:Z

    return v0
.end method

.method public scoutSystemMonitorInit(Lcom/android/server/Watchdog$Monitor;Ljava/lang/Object;)V
    .locals 0
    .param p1, "monitor"    # Lcom/android/server/Watchdog$Monitor;
    .param p2, "mLock"    # Ljava/lang/Object;

    .line 225
    invoke-virtual {p0, p2}, Lcom/android/server/ScoutSystemMonitor;->init(Ljava/lang/Object;)V

    .line 226
    invoke-direct {p0, p1}, Lcom/android/server/ScoutSystemMonitor;->addScoutBinderMonitor(Lcom/android/server/Watchdog$Monitor;)V

    .line 227
    return-void
.end method

.method public scoutSystemMonitorInitContext(Landroid/content/Context;Lcom/android/server/am/ActivityManagerService;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "activity"    # Lcom/android/server/am/ActivityManagerService;

    .line 236
    iput-object p1, p0, Lcom/android/server/ScoutSystemMonitor;->mContext:Landroid/content/Context;

    .line 237
    iput-object p2, p0, Lcom/android/server/ScoutSystemMonitor;->mService:Lcom/android/server/am/ActivityManagerService;

    .line 238
    invoke-direct {p0}, Lcom/android/server/ScoutSystemMonitor;->registerScreenStateReceiver()V

    .line 239
    return-void
.end method

.method public scoutSystemMonitorWork(JIZLcom/android/server/ScoutWatchdogInfo$ScoutId;)V
    .locals 0
    .param p1, "timeout"    # J
    .param p3, "count"    # I
    .param p4, "waitedHalf"    # Z
    .param p5, "scoutId"    # Lcom/android/server/ScoutWatchdogInfo$ScoutId;

    .line 231
    invoke-direct/range {p0 .. p5}, Lcom/android/server/ScoutSystemMonitor;->runSystemMonitor(JIZLcom/android/server/ScoutWatchdogInfo$ScoutId;)V

    .line 232
    return-void
.end method

.method public setWorkMessage(I)V
    .locals 2
    .param p1, "msgId"    # I

    .line 508
    iget-object v0, p0, Lcom/android/server/ScoutSystemMonitor;->mSysWorkerHandler:Lcom/android/server/ScoutSystemMonitor$SystemWorkerHandler;

    if-nez v0, :cond_0

    return-void

    .line 510
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 521
    return-void

    .line 518
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/ScoutSystemMonitor;->mSysWorkerHandler:Lcom/android/server/ScoutSystemMonitor$SystemWorkerHandler;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/android/server/ScoutSystemMonitor$SystemWorkerHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 519
    .local v0, "msg":Landroid/os/Message;
    goto :goto_0

    .line 515
    .end local v0    # "msg":Landroid/os/Message;
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/ScoutSystemMonitor;->mSysWorkerHandler:Lcom/android/server/ScoutSystemMonitor$SystemWorkerHandler;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Lcom/android/server/ScoutSystemMonitor$SystemWorkerHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 516
    .restart local v0    # "msg":Landroid/os/Message;
    goto :goto_0

    .line 512
    .end local v0    # "msg":Landroid/os/Message;
    :pswitch_2
    iget-object v0, p0, Lcom/android/server/ScoutSystemMonitor;->mSysWorkerHandler:Lcom/android/server/ScoutSystemMonitor$SystemWorkerHandler;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/android/server/ScoutSystemMonitor$SystemWorkerHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 513
    .restart local v0    # "msg":Landroid/os/Message;
    nop

    .line 523
    :goto_0
    iget-object v1, p0, Lcom/android/server/ScoutSystemMonitor;->mSysWorkerHandler:Lcom/android/server/ScoutSystemMonitor$SystemWorkerHandler;

    invoke-virtual {v1, v0}, Lcom/android/server/ScoutSystemMonitor$SystemWorkerHandler;->sendMessage(Landroid/os/Message;)Z

    .line 524
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public shouldRebootReasonCheckNull()Z
    .locals 2

    .line 265
    const-string v0, "persist.sys.stability.rebootreason_check"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public skipClipDataAppAnr(II)Z
    .locals 3
    .param p1, "pid"    # I
    .param p2, "uid"    # I

    .line 916
    const-string v0, "persist.sys.debug.app.clipdata"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 917
    .local v0, "clipProcess":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 918
    const/4 v1, 0x0

    return v1

    .line 920
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 921
    .local v1, "currentAnrApp":Ljava/lang/String;
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    return v2
.end method

.method public updateScreenState(Z)V
    .locals 3
    .param p1, "screenOn"    # Z

    .line 898
    sget-boolean v0, Lcom/android/server/ScoutHelper;->ENABLED_SCOUT:Z

    if-eqz v0, :cond_3

    sget-boolean v0, Lmiui/mqsas/scout/ScoutUtils;->REBOOT_COREDUMP:Z

    if-nez v0, :cond_3

    sget-boolean v0, Lmiui/mqsas/scout/ScoutUtils;->MTBF_MIUI_TEST:Z

    if-eqz v0, :cond_0

    goto :goto_1

    .line 901
    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/server/ScoutSystemMonitor;->mUiScoutStateMachine:Landroid/app/AppScoutStateMachine;

    if-nez v0, :cond_1

    .line 902
    nop

    .line 903
    invoke-static {}, Lcom/android/server/UiThread;->get()Lcom/android/server/UiThread;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 902
    const-string v2, "UiThread"

    invoke-static {v0, v2, v1}, Landroid/app/AppScoutStateMachine;->CreateAppScoutStateMachine(Landroid/os/HandlerThread;Ljava/lang/String;Ljava/lang/Boolean;)Landroid/app/AppScoutStateMachine;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/ScoutSystemMonitor;->mUiScoutStateMachine:Landroid/app/AppScoutStateMachine;

    goto :goto_0

    .line 904
    :cond_1
    if-nez p1, :cond_2

    iget-object v0, p0, Lcom/android/server/ScoutSystemMonitor;->mUiScoutStateMachine:Landroid/app/AppScoutStateMachine;

    if-eqz v0, :cond_2

    .line 905
    invoke-virtual {v0}, Landroid/app/AppScoutStateMachine;->quit()V

    .line 906
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/ScoutSystemMonitor;->mUiScoutStateMachine:Landroid/app/AppScoutStateMachine;

    .line 908
    :cond_2
    :goto_0
    return-void

    .line 899
    :cond_3
    :goto_1
    return-void
.end method
