class com.android.server.MiuiBatteryStatsService$TrackBatteryUsbInfo {
	 /* .source "MiuiBatteryStatsService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/MiuiBatteryStatsService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "TrackBatteryUsbInfo" */
} // .end annotation
/* # static fields */
public static final java.lang.String ACTION_TRACK_EVENT;
public static final java.lang.String ANALYTICS_PACKAGE;
public static final java.lang.String APP_TIME;
public static final java.lang.String AVETEMP;
public static final java.lang.String BATTERY_APP_ID;
public static final java.lang.String BATTERY_CHARGE_ACTION_EVENT;
public static final java.lang.String BATTERY_CHARGE_EVENT;
public static final java.lang.String BATTERY_HEALTH_EVENT;
public static final java.lang.String BATTERY_HIGH_TEMP_VOLTAGE;
public static final java.lang.String BATTERY_LPD_COUNT_EVENT;
public static final java.lang.String BATTERY_LPD_INFOMATION;
public static final java.lang.String BATTERY_POWER_OFF_EVENT;
public static final java.lang.String BATTERY_TEMP_EVENT;
public static final java.lang.String BATT_AUTH;
public static final java.lang.String BATT_RESISTANCE;
public static final java.lang.String BATT_THERMAL_LEVEL;
public static final java.lang.String CAPACITY_CHANGE_VALUE;
public static final java.lang.String CAPACITY_NONLINEAR_CHANGE_EVENT;
public static final java.lang.String CAPCAITY;
public static final java.lang.String CC_SHORT_VBUS;
public static final java.lang.String CHARGE_BAT_MAX_TEMP;
public static final java.lang.String CHARGE_BAT_MIN_TEMP;
public static final java.lang.String CHARGE_END_CAPACITY;
public static final java.lang.String CHARGE_END_TIME;
public static final java.lang.String CHARGE_FULL;
public static final java.lang.String CHARGE_POWER;
public static final java.lang.String CHARGE_START_CAPACITY;
public static final java.lang.String CHARGE_START_TIME;
public static final java.lang.String CHARGE_STATUS;
public static final java.lang.String CHARGE_TOTAL_TIME;
public static final java.lang.String CHARGE_TYPE;
public static final java.lang.String CYCLE_COUNT;
public static final java.lang.String DATA_UNLOCK;
public static final Integer FLAG_NON_ANONYMOUS;
public static final java.lang.String FULL_CHARGE_END_TIME;
public static final java.lang.String FULL_CHARGE_START_TIME;
public static final java.lang.String FULL_CHARGE_TOTAL_TIME;
public static final java.lang.String HIGH_TEMP_TIME;
public static final java.lang.String HIGH_TEMP_VOLTAGE_TIME;
public static final java.lang.String HIGH_VOLTAGE_TIME;
public static final java.lang.String IBAT;
public static final java.lang.String INTERMITTENT_CHARGE;
public static final java.lang.String INTERMITTENT_CHARGE_EVENT;
public static final java.lang.String LPD_COUNT;
public static final java.lang.String LPD_DM_RES;
public static final java.lang.String LPD_DP_RES;
public static final java.lang.String LPD_SBU1_RES;
public static final java.lang.String LPD_SBU2_RES;
public static final java.lang.String MAXTEMP;
public static final java.lang.String MINTEMP;
public static final java.lang.String NOT_FULLY_CHARGED;
public static final java.lang.String NOT_FULLY_CHARGED_EVENT;
public static final java.lang.String PARAM_APP_ID;
public static final java.lang.String PARAM_EVENT_NAME;
public static final java.lang.String PARAM_PACKAGE;
public static final java.lang.String PD_APDO_MAX;
public static final java.lang.String PD_AUTHENTICATION;
public static final java.lang.String SCENE;
public static final java.lang.String SCREEN_OFF_TIME;
public static final java.lang.String SCREEN_ON_TIME;
public static final java.lang.String SERIES_DELTA_VOLTAGE;
public static final java.lang.String SERIES_DELTA_VOLTAGE_EVENT;
public static final java.lang.String SHUTDOWN_DELAY;
public static final java.lang.String SLOW_CHARGE_EVENT;
public static final java.lang.String SLOW_CHARGE_TYPE;
public static final java.lang.String SOH;
public static final java.lang.String TBAT;
public static final java.lang.String TX_ADAPTER;
public static final java.lang.String TX_UUID;
public static final java.lang.String USB32;
public static final java.lang.String USB_APP_ID;
public static final java.lang.String USB_CURRENT;
public static final java.lang.String USB_DEVICE_EVENT;
public static final java.lang.String USB_FUNCTION;
public static final java.lang.String USB_VOLTAGE;
public static final java.lang.String VBAT;
public static final java.lang.String VBUS_DISABLE;
public static final java.lang.String VBUS_DISABLE_EVENT;
public static final java.lang.String WIRELESS_COMPOSITE;
public static final java.lang.String WIRELESS_COMPOSITE_EVENT;
public static final java.lang.String WIRELESS_REVERSE_CHARGE;
public static final java.lang.String WIRELESS_REVERSE_CHARGE_EVENT;
/* # instance fields */
final com.android.server.MiuiBatteryStatsService this$0; //synthetic
/* # direct methods */
 com.android.server.MiuiBatteryStatsService$TrackBatteryUsbInfo ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/MiuiBatteryStatsService; */
/* .line 1835 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
