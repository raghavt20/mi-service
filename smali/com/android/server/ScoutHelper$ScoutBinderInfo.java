public class com.android.server.ScoutHelper$ScoutBinderInfo {
	 /* .source "ScoutHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/ScoutHelper; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "ScoutBinderInfo" */
} // .end annotation
/* # instance fields */
private java.lang.StringBuilder mBinderTransInfo;
private Integer mCallType;
private Integer mFromPid;
private Boolean mHasDThread;
private Integer mMonkeyPid;
private Integer mPid;
private java.lang.StringBuilder mProcInfo;
private java.lang.String mTag;
/* # direct methods */
public com.android.server.ScoutHelper$ScoutBinderInfo ( ) {
/* .locals 1 */
/* .param p1, "mPid" # I */
/* .param p2, "mFromPid" # I */
/* .param p3, "mCallType" # I */
/* .param p4, "mTag" # Ljava/lang/String; */
/* .line 254 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 255 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->mHasDThread:Z */
/* .line 256 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
this.mBinderTransInfo = v0;
/* .line 257 */
/* iput p1, p0, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->mPid:I */
/* .line 258 */
/* iput p2, p0, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->mFromPid:I */
/* .line 259 */
this.mTag = p4;
/* .line 260 */
/* iput p3, p0, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->mCallType:I */
/* .line 261 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
this.mProcInfo = v0;
/* .line 262 */
return;
} // .end method
public com.android.server.ScoutHelper$ScoutBinderInfo ( ) {
/* .locals 1 */
/* .param p1, "mPid" # I */
/* .param p2, "mCallType" # I */
/* .param p3, "mTag" # Ljava/lang/String; */
/* .line 251 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, p1, v0, p2, p3}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;-><init>(IIILjava/lang/String;)V */
/* .line 252 */
return;
} // .end method
/* # virtual methods */
public void addBinderTransInfo ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "sInfo" # Ljava/lang/String; */
/* .line 281 */
v0 = this.mBinderTransInfo;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "\n"; // const-string v2, "\n"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 282 */
return;
} // .end method
public java.lang.String getBinderTransInfo ( ) {
/* .locals 3 */
/* .line 285 */
v0 = this.mBinderTransInfo;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 286 */
/* .local v0, "info":Ljava/lang/String; */
final String v1 = ""; // const-string v1, ""
/* if-ne v0, v1, :cond_0 */
/* .line 287 */
final String v1 = "Here are no Binder-related exception messages available."; // const-string v1, "Here are no Binder-related exception messages available."
/* .line 289 */
} // :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Binder Tracsaction Info:\n"; // const-string v2, "Binder Tracsaction Info:\n"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
public Integer getCallType ( ) {
/* .locals 1 */
/* .line 302 */
/* iget v0, p0, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->mCallType:I */
} // .end method
public Boolean getDThreadState ( ) {
/* .locals 1 */
/* .line 273 */
/* iget-boolean v0, p0, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->mHasDThread:Z */
} // .end method
public Integer getFromPid ( ) {
/* .locals 1 */
/* .line 298 */
/* iget v0, p0, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->mFromPid:I */
} // .end method
public Integer getMonkeyPid ( ) {
/* .locals 1 */
/* .line 277 */
/* iget v0, p0, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->mMonkeyPid:I */
} // .end method
public Integer getPid ( ) {
/* .locals 1 */
/* .line 294 */
/* iget v0, p0, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->mPid:I */
} // .end method
public java.lang.String getProcInfo ( ) {
/* .locals 1 */
/* .line 314 */
v0 = this.mProcInfo;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 315 */
/* .local v0, "info":Ljava/lang/String; */
} // .end method
public java.lang.String getTag ( ) {
/* .locals 1 */
/* .line 306 */
v0 = this.mTag;
} // .end method
public void setDThreadState ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "mHasDThread" # Z */
/* .line 265 */
/* iput-boolean p1, p0, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->mHasDThread:Z */
/* .line 266 */
return;
} // .end method
public void setMonkeyPid ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "monkeyPid" # I */
/* .line 269 */
/* iput p1, p0, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->mMonkeyPid:I */
/* .line 270 */
return;
} // .end method
public void setProcInfo ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "procInfo" # Ljava/lang/String; */
/* .line 310 */
v0 = this.mProcInfo;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "\n"; // const-string v2, "\n"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 311 */
return;
} // .end method
