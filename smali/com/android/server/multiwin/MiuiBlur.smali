.class public final Lcom/android/server/multiwin/MiuiBlur;
.super Ljava/lang/Object;
.source "MiuiBlur.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "MiuiBlur"

.field private static mStaticAlgorithm:Lcom/miui/iimagekit/blur/BlurAlgorithm;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 16
    new-instance v0, Lcom/miui/iimagekit/blur/BlurAlgorithm;

    const/4 v1, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v0, v3, v1, v2}, Lcom/miui/iimagekit/blur/BlurAlgorithm;-><init>(Landroid/content/Context;IZ)V

    sput-object v0, Lcom/android/server/multiwin/MiuiBlur;->mStaticAlgorithm:Lcom/miui/iimagekit/blur/BlurAlgorithm;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static blur(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    .locals 3
    .param p0, "input"    # Landroid/graphics/Bitmap;
    .param p1, "radius"    # I
    .param p2, "downscale"    # I
    .param p3, "isNeedSkipNavBar"    # Z

    .line 19
    const-string v0, "MiuiBlur"

    if-eqz p0, :cond_4

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_1

    .line 24
    :cond_0
    if-lez p1, :cond_3

    if-gtz p2, :cond_1

    goto :goto_0

    .line 29
    :cond_1
    div-int v1, p1, p2

    .line 30
    .local v1, "blurRadius":I
    if-nez v1, :cond_2

    .line 31
    const-string v2, "blur: blur radius downscale must < radius"

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 32
    return-object p0

    .line 34
    :cond_2
    invoke-static {p0, p2, p3}, Lcom/android/server/multiwin/MiuiBlur;->getBitmapForBlur(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/android/server/multiwin/MiuiBlur;->getBlurredBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0

    .line 25
    .end local v1    # "blurRadius":I
    :cond_3
    :goto_0
    const-string v1, "blur: blur radius and downscale must > 0"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 26
    return-object p0

    .line 20
    :cond_4
    :goto_1
    const-string v1, "blur: input bitmap is null or bitmap size is 0"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 21
    return-object p0
.end method

.method private static getBitmapForBlur(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;
    .locals 9
    .param p0, "input"    # Landroid/graphics/Bitmap;
    .param p1, "downscale"    # I
    .param p2, "isNeedSkipNavBar"    # Z

    .line 38
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    div-int/2addr v0, p1

    const/4 v1, 0x1

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 39
    .local v0, "scaledW":I
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    div-int/2addr v2, p1

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 40
    .local v2, "scaledH":I
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_4444:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 41
    .local v3, "bitmapForBlur":Landroid/graphics/Bitmap;
    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 42
    .local v4, "blurCanvas":Landroid/graphics/Canvas;
    new-instance v5, Landroid/graphics/RectF;

    int-to-float v6, v0

    int-to-float v7, v2

    const/4 v8, 0x0

    invoke-direct {v5, v8, v8, v6, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 43
    .local v5, "rectF":Landroid/graphics/RectF;
    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {p0, v6, v1}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 44
    .local v1, "temp":Landroid/graphics/Bitmap;
    new-instance v6, Landroid/graphics/Paint;

    invoke-direct {v6}, Landroid/graphics/Paint;-><init>()V

    .line 45
    .local v6, "paint":Landroid/graphics/Paint;
    invoke-static {v1, v3, p2}, Lcom/android/server/multiwin/MiuiBlur;->getBitmapScaleShader(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Z)Landroid/graphics/BitmapShader;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 46
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 47
    return-object v3
.end method

.method private static getBitmapScaleShader(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Z)Landroid/graphics/BitmapShader;
    .locals 6
    .param p0, "in"    # Landroid/graphics/Bitmap;
    .param p1, "out"    # Landroid/graphics/Bitmap;
    .param p2, "isNeedSkipNavBar"    # Z

    .line 53
    if-eqz p2, :cond_0

    .line 54
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 55
    .local v0, "scaleRateX":F
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    .local v1, "scaleRateY":F
    goto :goto_0

    .line 57
    .end local v0    # "scaleRateX":F
    .end local v1    # "scaleRateY":F
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 58
    .restart local v0    # "scaleRateX":F
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 61
    .restart local v1    # "scaleRateY":F
    :goto_0
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 62
    .local v2, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v2, v0, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 63
    new-instance v3, Landroid/graphics/BitmapShader;

    sget-object v4, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    sget-object v5, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct {v3, p0, v4, v5}, Landroid/graphics/BitmapShader;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    .line 64
    .local v3, "shader":Landroid/graphics/BitmapShader;
    invoke-virtual {v3, v2}, Landroid/graphics/BitmapShader;->setLocalMatrix(Landroid/graphics/Matrix;)V

    .line 65
    return-object v3
.end method

.method private static getBlurredBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 3
    .param p0, "bitmapForBlur"    # Landroid/graphics/Bitmap;
    .param p1, "radius"    # I

    .line 69
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    .line 70
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_4444:Landroid/graphics/Bitmap$Config;

    .line 69
    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 71
    .local v0, "blurredBitmap":Landroid/graphics/Bitmap;
    sget-object v1, Lcom/android/server/multiwin/MiuiBlur;->mStaticAlgorithm:Lcom/miui/iimagekit/blur/BlurAlgorithm;

    if-eqz v1, :cond_0

    .line 72
    invoke-virtual {v1, p0, v0, p1}, Lcom/miui/iimagekit/blur/BlurAlgorithm;->blur(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;I)I

    .line 74
    :cond_0
    return-object v0
.end method
