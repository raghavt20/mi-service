public class inal {
	 /* .source "MiuiBlur.java" */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 private static com.miui.iimagekit.blur.BlurAlgorithm mStaticAlgorithm;
	 /* # direct methods */
	 static inal ( ) {
		 /* .locals 4 */
		 /* .line 16 */
		 /* new-instance v0, Lcom/miui/iimagekit/blur/BlurAlgorithm; */
		 int v1 = 2; // const/4 v1, 0x2
		 int v2 = 1; // const/4 v2, 0x1
		 int v3 = 0; // const/4 v3, 0x0
		 /* invoke-direct {v0, v3, v1, v2}, Lcom/miui/iimagekit/blur/BlurAlgorithm;-><init>(Landroid/content/Context;IZ)V */
		 return;
	 } // .end method
	 public inal ( ) {
		 /* .locals 0 */
		 /* .line 14 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static android.graphics.Bitmap blur ( android.graphics.Bitmap p0, Integer p1, Integer p2, Boolean p3 ) {
		 /* .locals 3 */
		 /* .param p0, "input" # Landroid/graphics/Bitmap; */
		 /* .param p1, "radius" # I */
		 /* .param p2, "downscale" # I */
		 /* .param p3, "isNeedSkipNavBar" # Z */
		 /* .line 19 */
		 final String v0 = "MiuiBlur"; // const-string v0, "MiuiBlur"
		 if ( p0 != null) { // if-eqz p0, :cond_4
			 v1 = 			 (( android.graphics.Bitmap ) p0 ).getWidth ( ); // invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I
			 if ( v1 != null) { // if-eqz v1, :cond_4
				 v1 = 				 (( android.graphics.Bitmap ) p0 ).getHeight ( ); // invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I
				 /* if-nez v1, :cond_0 */
				 /* .line 24 */
			 } // :cond_0
			 /* if-lez p1, :cond_3 */
			 /* if-gtz p2, :cond_1 */
			 /* .line 29 */
		 } // :cond_1
		 /* div-int v1, p1, p2 */
		 /* .line 30 */
		 /* .local v1, "blurRadius":I */
		 /* if-nez v1, :cond_2 */
		 /* .line 31 */
		 final String v2 = "blur: blur radius downscale must < radius"; // const-string v2, "blur: blur radius downscale must < radius"
		 android.util.Slog .e ( v0,v2 );
		 /* .line 32 */
		 /* .line 34 */
	 } // :cond_2
	 com.android.server.multiwin.MiuiBlur .getBitmapForBlur ( p0,p2,p3 );
	 com.android.server.multiwin.MiuiBlur .getBlurredBitmap ( v0,v1 );
	 /* .line 25 */
} // .end local v1 # "blurRadius":I
} // :cond_3
} // :goto_0
final String v1 = "blur: blur radius and downscale must > 0"; // const-string v1, "blur: blur radius and downscale must > 0"
android.util.Slog .e ( v0,v1 );
/* .line 26 */
/* .line 20 */
} // :cond_4
} // :goto_1
final String v1 = "blur: input bitmap is null or bitmap size is 0"; // const-string v1, "blur: input bitmap is null or bitmap size is 0"
android.util.Slog .e ( v0,v1 );
/* .line 21 */
} // .end method
private static android.graphics.Bitmap getBitmapForBlur ( android.graphics.Bitmap p0, Integer p1, Boolean p2 ) {
/* .locals 9 */
/* .param p0, "input" # Landroid/graphics/Bitmap; */
/* .param p1, "downscale" # I */
/* .param p2, "isNeedSkipNavBar" # Z */
/* .line 38 */
v0 = (( android.graphics.Bitmap ) p0 ).getWidth ( ); // invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I
/* div-int/2addr v0, p1 */
int v1 = 1; // const/4 v1, 0x1
v0 = java.lang.Math .max ( v1,v0 );
/* .line 39 */
/* .local v0, "scaledW":I */
v2 = (( android.graphics.Bitmap ) p0 ).getHeight ( ); // invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I
/* div-int/2addr v2, p1 */
v2 = java.lang.Math .max ( v1,v2 );
/* .line 40 */
/* .local v2, "scaledH":I */
v3 = android.graphics.Bitmap$Config.ARGB_4444;
android.graphics.Bitmap .createBitmap ( v0,v2,v3 );
/* .line 41 */
/* .local v3, "bitmapForBlur":Landroid/graphics/Bitmap; */
/* new-instance v4, Landroid/graphics/Canvas; */
/* invoke-direct {v4, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V */
/* .line 42 */
/* .local v4, "blurCanvas":Landroid/graphics/Canvas; */
/* new-instance v5, Landroid/graphics/RectF; */
/* int-to-float v6, v0 */
/* int-to-float v7, v2 */
int v8 = 0; // const/4 v8, 0x0
/* invoke-direct {v5, v8, v8, v6, v7}, Landroid/graphics/RectF;-><init>(FFFF)V */
/* .line 43 */
/* .local v5, "rectF":Landroid/graphics/RectF; */
v6 = android.graphics.Bitmap$Config.ARGB_8888;
(( android.graphics.Bitmap ) p0 ).copy ( v6, v1 ); // invoke-virtual {p0, v6, v1}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;
/* .line 44 */
/* .local v1, "temp":Landroid/graphics/Bitmap; */
/* new-instance v6, Landroid/graphics/Paint; */
/* invoke-direct {v6}, Landroid/graphics/Paint;-><init>()V */
/* .line 45 */
/* .local v6, "paint":Landroid/graphics/Paint; */
com.android.server.multiwin.MiuiBlur .getBitmapScaleShader ( v1,v3,p2 );
(( android.graphics.Paint ) v6 ).setShader ( v7 ); // invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;
/* .line 46 */
(( android.graphics.Canvas ) v4 ).drawRect ( v5, v6 ); // invoke-virtual {v4, v5, v6}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V
/* .line 47 */
} // .end method
private static android.graphics.BitmapShader getBitmapScaleShader ( android.graphics.Bitmap p0, android.graphics.Bitmap p1, Boolean p2 ) {
/* .locals 6 */
/* .param p0, "in" # Landroid/graphics/Bitmap; */
/* .param p1, "out" # Landroid/graphics/Bitmap; */
/* .param p2, "isNeedSkipNavBar" # Z */
/* .line 53 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 54 */
v0 = (( android.graphics.Bitmap ) p1 ).getWidth ( ); // invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I
/* int-to-float v0, v0 */
v1 = (( android.graphics.Bitmap ) p0 ).getWidth ( ); // invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I
/* int-to-float v1, v1 */
/* div-float/2addr v0, v1 */
/* .line 55 */
/* .local v0, "scaleRateX":F */
v1 = (( android.graphics.Bitmap ) p1 ).getHeight ( ); // invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I
/* int-to-float v1, v1 */
v2 = (( android.graphics.Bitmap ) p0 ).getHeight ( ); // invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I
/* int-to-float v2, v2 */
/* div-float/2addr v1, v2 */
/* .local v1, "scaleRateY":F */
/* .line 57 */
} // .end local v0 # "scaleRateX":F
} // .end local v1 # "scaleRateY":F
} // :cond_0
v0 = (( android.graphics.Bitmap ) p1 ).getWidth ( ); // invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I
/* int-to-float v0, v0 */
v1 = (( android.graphics.Bitmap ) p0 ).getWidth ( ); // invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I
/* int-to-float v1, v1 */
/* div-float/2addr v0, v1 */
/* .line 58 */
/* .restart local v0 # "scaleRateX":F */
v1 = (( android.graphics.Bitmap ) p1 ).getHeight ( ); // invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I
/* int-to-float v1, v1 */
v2 = (( android.graphics.Bitmap ) p0 ).getHeight ( ); // invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I
/* int-to-float v2, v2 */
/* div-float/2addr v1, v2 */
/* .line 61 */
/* .restart local v1 # "scaleRateY":F */
} // :goto_0
/* new-instance v2, Landroid/graphics/Matrix; */
/* invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V */
/* .line 62 */
/* .local v2, "matrix":Landroid/graphics/Matrix; */
(( android.graphics.Matrix ) v2 ).postScale ( v0, v1 ); // invoke-virtual {v2, v0, v1}, Landroid/graphics/Matrix;->postScale(FF)Z
/* .line 63 */
/* new-instance v3, Landroid/graphics/BitmapShader; */
v4 = android.graphics.Shader$TileMode.CLAMP;
v5 = android.graphics.Shader$TileMode.CLAMP;
/* invoke-direct {v3, p0, v4, v5}, Landroid/graphics/BitmapShader;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V */
/* .line 64 */
/* .local v3, "shader":Landroid/graphics/BitmapShader; */
(( android.graphics.BitmapShader ) v3 ).setLocalMatrix ( v2 ); // invoke-virtual {v3, v2}, Landroid/graphics/BitmapShader;->setLocalMatrix(Landroid/graphics/Matrix;)V
/* .line 65 */
} // .end method
private static android.graphics.Bitmap getBlurredBitmap ( android.graphics.Bitmap p0, Integer p1 ) {
/* .locals 3 */
/* .param p0, "bitmapForBlur" # Landroid/graphics/Bitmap; */
/* .param p1, "radius" # I */
/* .line 69 */
v0 = (( android.graphics.Bitmap ) p0 ).getWidth ( ); // invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I
/* .line 70 */
v1 = (( android.graphics.Bitmap ) p0 ).getHeight ( ); // invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I
v2 = android.graphics.Bitmap$Config.ARGB_4444;
/* .line 69 */
android.graphics.Bitmap .createBitmap ( v0,v1,v2 );
/* .line 71 */
/* .local v0, "blurredBitmap":Landroid/graphics/Bitmap; */
v1 = com.android.server.multiwin.MiuiBlur.mStaticAlgorithm;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 72 */
(( com.miui.iimagekit.blur.BlurAlgorithm ) v1 ).blur ( p0, v0, p1 ); // invoke-virtual {v1, p0, v0, p1}, Lcom/miui/iimagekit/blur/BlurAlgorithm;->blur(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;I)I
/* .line 74 */
} // :cond_0
} // .end method
