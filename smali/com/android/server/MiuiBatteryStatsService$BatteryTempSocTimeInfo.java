class com.android.server.MiuiBatteryStatsService$BatteryTempSocTimeInfo {
	 /* .source "MiuiBatteryStatsService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/MiuiBatteryStatsService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "BatteryTempSocTimeInfo" */
} // .end annotation
/* # instance fields */
private final java.lang.String BATTERT_INFO_FILE;
private final Integer COLLECT_DAYS;
private final java.lang.String IS_SATISFY_TEMP_SOC_CONDITION;
private java.lang.String RValue;
private final java.lang.String STOP_COLLECT_DATA_TIME;
private final java.lang.String TOTAL_TIME_IN38;
private final java.lang.String TOTAL_TIME_IN43;
private final java.lang.String TOTAL_TIME_IN_HIGH_SOC;
private java.util.List allDaysData;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
private Long startTimeIn38C;
private Long startTimeIn43C;
private Long startTimeInHighSoc;
private Long stopCollectTempSocTime;
final com.android.server.MiuiBatteryStatsService this$0; //synthetic
private Long totalTimeIn38C;
private Long totalTimeIn43C;
private Long totalTimeInHighSoc;
/* # direct methods */
static Long -$$Nest$fgetstopCollectTempSocTime ( com.android.server.MiuiBatteryStatsService$BatteryTempSocTimeInfo p0 ) { //bridge//synthethic
/* .locals 2 */
/* iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->stopCollectTempSocTime:J */
/* return-wide v0 */
} // .end method
static void -$$Nest$mreadDataFromFile ( com.android.server.MiuiBatteryStatsService$BatteryTempSocTimeInfo p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->readDataFromFile()V */
return;
} // .end method
static void -$$Nest$mwriteDataToFile ( com.android.server.MiuiBatteryStatsService$BatteryTempSocTimeInfo p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->writeDataToFile()V */
return;
} // .end method
 com.android.server.MiuiBatteryStatsService$BatteryTempSocTimeInfo ( ) {
/* .locals 2 */
/* .param p1, "this$0" # Lcom/android/server/MiuiBatteryStatsService; */
/* .line 1457 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 1458 */
final String v0 = "/data/system/battery-info.txt"; // const-string v0, "/data/system/battery-info.txt"
this.BATTERT_INFO_FILE = v0;
/* .line 1459 */
final String v0 = "mIsSatisfyTempSocCondition="; // const-string v0, "mIsSatisfyTempSocCondition="
this.IS_SATISFY_TEMP_SOC_CONDITION = v0;
/* .line 1460 */
/* const-string/jumbo v0, "stopCollectTempSocTime=" */
this.STOP_COLLECT_DATA_TIME = v0;
/* .line 1461 */
/* const-string/jumbo v0, "totalTimeIn38C=" */
this.TOTAL_TIME_IN38 = v0;
/* .line 1462 */
/* const-string/jumbo v0, "totalTimeIn43C=" */
this.TOTAL_TIME_IN43 = v0;
/* .line 1463 */
/* const-string/jumbo v0, "totalTimeInHighSoc=" */
this.TOTAL_TIME_IN_HIGH_SOC = v0;
/* .line 1464 */
int v0 = 7; // const/4 v0, 0x7
/* iput v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->COLLECT_DAYS:I */
/* .line 1470 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeIn38C:J */
/* .line 1471 */
/* iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeIn43C:J */
/* .line 1472 */
/* iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeInHighSoc:J */
/* .line 1473 */
/* iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->stopCollectTempSocTime:J */
/* .line 1475 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.allDaysData = v0;
return;
} // .end method
private Integer getRValue ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "index" # I */
/* .line 1576 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1577 */
/* .local v0, "sum":I */
v1 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmHandler ( v1 );
v2 = this.allDaysData;
int v3 = 6; // const/4 v3, 0x6
/* check-cast v2, Ljava/util/List; */
/* check-cast v2, Ljava/lang/String; */
v1 = (( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) v1 ).parseInt ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I
v2 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmHandler ( v2 );
v3 = this.allDaysData;
int v4 = 0; // const/4 v4, 0x0
/* check-cast v3, Ljava/util/List; */
/* check-cast v3, Ljava/lang/String; */
v2 = (( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) v2 ).parseInt ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I
/* sub-int/2addr v1, v2 */
/* .line 1578 */
} // .end local v0 # "sum":I
/* .local v1, "sum":I */
} // .end method
private Long getTotalTime ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "index" # I */
/* .line 1569 */
/* const-wide/16 v0, 0x0 */
/* .line 1570 */
/* .local v0, "sum":J */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
v3 = v3 = this.allDaysData;
/* if-ge v2, v3, :cond_0 */
/* .line 1571 */
v3 = this.allDaysData;
/* check-cast v3, Ljava/util/List; */
/* check-cast v3, Ljava/lang/String; */
/* invoke-direct {p0, v3}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->parseLong(Ljava/lang/String;)J */
/* move-result-wide v3 */
/* add-long/2addr v0, v3 */
/* .line 1570 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 1573 */
} // .end local v2 # "i":I
} // :cond_0
/* return-wide v0 */
} // .end method
private Long parseLong ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "time" # Ljava/lang/String; */
/* .line 1657 */
try { // :try_start_0
java.lang.Long .parseLong ( p1 );
/* move-result-wide v0 */
/* :try_end_0 */
/* .catch Ljava/lang/NumberFormatException; {:try_start_0 ..:try_end_0} :catch_0 */
/* return-wide v0 */
/* .line 1658 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1659 */
/* .local v0, "e":Ljava/lang/NumberFormatException; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Invalid string time "; // const-string v2, "Invalid string time "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiBatteryStatsService"; // const-string v2, "MiuiBatteryStatsService"
android.util.Slog .e ( v2,v1 );
/* .line 1660 */
/* const-wide/16 v1, 0x0 */
/* return-wide v1 */
} // .end method
private java.lang.String parseString ( Long p0 ) {
/* .locals 1 */
/* .param p1, "time" # J */
/* .line 1653 */
java.lang.String .valueOf ( p1,p2 );
} // .end method
private void readDataFromFile ( ) {
/* .locals 13 */
/* .line 1606 */
final String v0 = ","; // const-string v0, ","
final String v1 = ", "; // const-string v1, ", "
/* new-instance v2, Ljava/io/File; */
final String v3 = "/data/system/battery-info.txt"; // const-string v3, "/data/system/battery-info.txt"
/* invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 1607 */
/* .local v2, "battInfoFile":Ljava/io/File; */
v3 = (( java.io.File ) v2 ).exists ( ); // invoke-virtual {v2}, Ljava/io/File;->exists()Z
final String v4 = "MiuiBatteryStatsService"; // const-string v4, "MiuiBatteryStatsService"
/* if-nez v3, :cond_0 */
/* .line 1608 */
final String v0 = "Can\'t find battery info file"; // const-string v0, "Can\'t find battery info file"
android.util.Slog .i ( v4,v0 );
/* .line 1609 */
return;
/* .line 1612 */
} // :cond_0
int v3 = 0; // const/4 v3, 0x0
int v5 = 0; // const/4 v5, 0x0
try { // :try_start_0
android.os.FileUtils .readTextFile ( v2,v3,v5 );
/* .line 1613 */
/* .local v3, "text":Ljava/lang/String; */
final String v5 = "\n"; // const-string v5, "\n"
(( java.lang.String ) v3 ).split ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 1614 */
/* .local v5, "lines":[Ljava/lang/String; */
int v6 = 0; // const/4 v6, 0x0
/* .local v6, "i":I */
} // :goto_0
v7 = com.android.internal.util.ArrayUtils .size ( v5 );
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* const-string/jumbo v8, "totalTimeInHighSoc=" */
/* const-string/jumbo v9, "totalTimeIn43C=" */
/* const-string/jumbo v10, "totalTimeIn38C=" */
/* const-string/jumbo v11, "stopCollectTempSocTime=" */
final String v12 = "mIsSatisfyTempSocCondition="; // const-string v12, "mIsSatisfyTempSocCondition="
/* if-ge v6, v7, :cond_8 */
/* .line 1615 */
try { // :try_start_1
/* aget-object v7, v5, v6 */
v7 = android.text.TextUtils .isEmpty ( v7 );
if ( v7 != null) { // if-eqz v7, :cond_1
/* .line 1616 */
/* goto/16 :goto_1 */
/* .line 1618 */
} // :cond_1
/* aget-object v7, v5, v6 */
v7 = (( java.lang.String ) v7 ).startsWith ( v12 ); // invoke-virtual {v7, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v7 != null) { // if-eqz v7, :cond_2
/* .line 1619 */
/* aget-object v7, v5, v6 */
v12 = (( java.lang.String ) v12 ).length ( ); // invoke-virtual {v12}, Ljava/lang/String;->length()I
(( java.lang.String ) v7 ).substring ( v12 ); // invoke-virtual {v7, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;
v7 = java.lang.Boolean .parseBoolean ( v7 );
com.android.server.MiuiBatteryStatsService .-$$Nest$sfputmIsSatisfyTempSocCondition ( v7 );
/* .line 1621 */
} // :cond_2
/* aget-object v7, v5, v6 */
v7 = (( java.lang.String ) v7 ).startsWith ( v11 ); // invoke-virtual {v7, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v7 != null) { // if-eqz v7, :cond_3
/* .line 1622 */
/* aget-object v7, v5, v6 */
v8 = (( java.lang.String ) v11 ).length ( ); // invoke-virtual {v11}, Ljava/lang/String;->length()I
(( java.lang.String ) v7 ).substring ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;
/* invoke-direct {p0, v7}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->parseLong(Ljava/lang/String;)J */
/* move-result-wide v7 */
/* iput-wide v7, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->stopCollectTempSocTime:J */
/* .line 1623 */
/* .line 1625 */
} // :cond_3
/* aget-object v7, v5, v6 */
v7 = (( java.lang.String ) v7 ).startsWith ( v10 ); // invoke-virtual {v7, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v7 != null) { // if-eqz v7, :cond_4
/* .line 1626 */
/* aget-object v7, v5, v6 */
v8 = (( java.lang.String ) v10 ).length ( ); // invoke-virtual {v10}, Ljava/lang/String;->length()I
(( java.lang.String ) v7 ).substring ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;
/* invoke-direct {p0, v7}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->parseLong(Ljava/lang/String;)J */
/* move-result-wide v7 */
/* iput-wide v7, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeIn38C:J */
/* .line 1627 */
/* .line 1629 */
} // :cond_4
/* aget-object v7, v5, v6 */
v7 = (( java.lang.String ) v7 ).startsWith ( v9 ); // invoke-virtual {v7, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v7 != null) { // if-eqz v7, :cond_5
/* .line 1630 */
/* aget-object v7, v5, v6 */
v8 = (( java.lang.String ) v9 ).length ( ); // invoke-virtual {v9}, Ljava/lang/String;->length()I
(( java.lang.String ) v7 ).substring ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;
/* invoke-direct {p0, v7}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->parseLong(Ljava/lang/String;)J */
/* move-result-wide v7 */
/* iput-wide v7, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeIn43C:J */
/* .line 1631 */
/* .line 1633 */
} // :cond_5
/* aget-object v7, v5, v6 */
v7 = (( java.lang.String ) v7 ).startsWith ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v7 != null) { // if-eqz v7, :cond_6
/* .line 1634 */
/* aget-object v7, v5, v6 */
v8 = (( java.lang.String ) v8 ).length ( ); // invoke-virtual {v8}, Ljava/lang/String;->length()I
(( java.lang.String ) v7 ).substring ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;
/* invoke-direct {p0, v7}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->parseLong(Ljava/lang/String;)J */
/* move-result-wide v7 */
/* iput-wide v7, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeInHighSoc:J */
/* .line 1635 */
/* .line 1637 */
} // :cond_6
/* aget-object v7, v5, v6 */
(( java.lang.String ) v7 ).split ( v0 ); // invoke-virtual {v7, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
v7 = com.android.internal.util.ArrayUtils .size ( v7 );
int v8 = 4; // const/4 v8, 0x4
/* if-ne v7, v8, :cond_7 */
/* .line 1638 */
/* aget-object v7, v5, v6 */
(( java.lang.String ) v7 ).split ( v0 ); // invoke-virtual {v7, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 1639 */
/* .local v7, "listOfCurrentline":[Ljava/lang/String; */
v8 = this.allDaysData;
java.util.Arrays .asList ( v7 );
/* .line 1614 */
} // .end local v7 # "listOfCurrentline":[Ljava/lang/String;
} // :cond_7
} // :goto_1
/* add-int/lit8 v6, v6, 0x1 */
/* goto/16 :goto_0 */
/* .line 1642 */
} // .end local v6 # "i":I
} // :cond_8
v0 = this.this$0;
v0 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetDEBUG ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_9
/* .line 1643 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v12 ); // invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = com.android.server.MiuiBatteryStatsService .-$$Nest$sfgetmIsSatisfyTempSocCondition ( );
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v11 ); // invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmHandler ( v6 );
/* iget-wide v11, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->stopCollectTempSocTime:J */
/* .line 1644 */
com.android.server.MiuiBatteryStatsService$BatteryStatsHandler .-$$Nest$mformatTime ( v6,v11,v12 );
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v10 ); // invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v6, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeIn38C:J */
(( java.lang.StringBuilder ) v0 ).append ( v6, v7 ); // invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v6, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeIn43C:J */
(( java.lang.StringBuilder ) v0 ).append ( v6, v7 ); // invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v6, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeInHighSoc:J */
(( java.lang.StringBuilder ) v0 ).append ( v6, v7 ); // invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = ", allDaysData="; // const-string v1, ", allDaysData="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.allDaysData;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1643 */
android.util.Slog .d ( v4,v0 );
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 1650 */
} // .end local v3 # "text":Ljava/lang/String;
} // .end local v5 # "lines":[Ljava/lang/String;
} // :cond_9
/* .line 1648 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1649 */
/* .local v0, "e":Ljava/io/IOException; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "can\'t read file : "; // const-string v3, "can\'t read file : "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v4,v1 );
/* .line 1651 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :goto_2
return;
} // .end method
private void writeDataToFile ( ) {
/* .locals 8 */
/* .line 1581 */
(( com.android.server.MiuiBatteryStatsService$BatteryTempSocTimeInfo ) p0 ).stopCollectTempSocData ( ); // invoke-virtual {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->stopCollectTempSocData()V
/* .line 1582 */
/* new-instance v0, Landroid/util/AtomicFile; */
/* new-instance v1, Ljava/io/File; */
final String v2 = "/data/system/battery-info.txt"; // const-string v2, "/data/system/battery-info.txt"
/* invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* invoke-direct {v0, v1}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V */
/* .line 1583 */
/* .local v0, "file":Landroid/util/AtomicFile; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 1584 */
/* .local v1, "lines":Ljava/lang/StringBuilder; */
final String v2 = "mIsSatisfyTempSocCondition="; // const-string v2, "mIsSatisfyTempSocCondition="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = com.android.server.MiuiBatteryStatsService .-$$Nest$sfgetmIsSatisfyTempSocCondition ( );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v3 = "\n"; // const-string v3, "\n"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1585 */
/* const-string/jumbo v2, "stopCollectTempSocTime=" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v4, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->stopCollectTempSocTime:J */
(( java.lang.StringBuilder ) v2 ).append ( v4, v5 ); // invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1586 */
/* const-string/jumbo v2, "totalTimeIn38C=" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v4, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeIn38C:J */
(( java.lang.StringBuilder ) v2 ).append ( v4, v5 ); // invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1587 */
/* const-string/jumbo v2, "totalTimeIn43C=" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v4, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeIn43C:J */
(( java.lang.StringBuilder ) v2 ).append ( v4, v5 ); // invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1588 */
/* const-string/jumbo v2, "totalTimeInHighSoc=" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v4, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeInHighSoc:J */
(( java.lang.StringBuilder ) v2 ).append ( v4, v5 ); // invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1589 */
v2 = this.allDaysData;
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_1
/* check-cast v4, Ljava/util/List; */
/* .line 1590 */
/* .local v4, "list":Ljava/util/List; */
int v5 = 0; // const/4 v5, 0x0
/* .local v5, "i":I */
v6 = } // :goto_1
/* if-ge v5, v6, :cond_0 */
/* .line 1591 */
(( java.lang.StringBuilder ) v1 ).append ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v7 = ","; // const-string v7, ","
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1590 */
/* add-int/lit8 v5, v5, 0x1 */
/* .line 1593 */
} // .end local v5 # "i":I
} // :cond_0
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1594 */
} // .end local v4 # "list":Ljava/util/List;
/* .line 1595 */
} // :cond_1
int v2 = 0; // const/4 v2, 0x0
/* .line 1597 */
/* .local v2, "fos":Ljava/io/FileOutputStream; */
try { // :try_start_0
(( android.util.AtomicFile ) v0 ).startWrite ( ); // invoke-virtual {v0}, Landroid/util/AtomicFile;->startWrite()Ljava/io/FileOutputStream;
/* move-object v2, v3 */
/* .line 1598 */
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.String ) v3 ).getBytes ( ); // invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B
(( java.io.FileOutputStream ) v2 ).write ( v3 ); // invoke-virtual {v2, v3}, Ljava/io/FileOutputStream;->write([B)V
/* .line 1599 */
(( android.util.AtomicFile ) v0 ).finishWrite ( v2 ); // invoke-virtual {v0, v2}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1603 */
/* .line 1600 */
/* :catch_0 */
/* move-exception v3 */
/* .line 1601 */
/* .local v3, "e":Ljava/io/IOException; */
(( android.util.AtomicFile ) v0 ).failWrite ( v2 ); // invoke-virtual {v0, v2}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V
/* .line 1602 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "failed to write battery_info.txt : "; // const-string v5, "failed to write battery_info.txt : "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "MiuiBatteryStatsService"; // const-string v5, "MiuiBatteryStatsService"
android.util.Slog .e ( v5,v4 );
/* .line 1604 */
} // .end local v3 # "e":Ljava/io/IOException;
} // :goto_2
return;
} // .end method
/* # virtual methods */
public void clearData ( ) {
/* .locals 2 */
/* .line 1563 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeIn38C:J */
/* .line 1564 */
/* iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeIn43C:J */
/* .line 1565 */
/* iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeInHighSoc:J */
/* .line 1566 */
/* iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->stopCollectTempSocTime:J */
/* .line 1567 */
return;
} // .end method
public void collectTempSocData ( Integer p0, Integer p1 ) {
/* .locals 11 */
/* .param p1, "temp" # I */
/* .param p2, "soc" # I */
/* .line 1478 */
/* iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->stopCollectTempSocTime:J */
/* const-wide/16 v2, 0x0 */
/* cmp-long v0, v0, v2 */
/* if-nez v0, :cond_0 */
/* .line 1479 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* const-wide/32 v4, 0x5265c00 */
/* add-long/2addr v0, v4 */
/* iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->stopCollectTempSocTime:J */
/* .line 1480 */
v0 = this.this$0;
v0 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetDEBUG ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "stopCollectTempSocTime=" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmHandler ( v1 );
/* iget-wide v4, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->stopCollectTempSocTime:J */
com.android.server.MiuiBatteryStatsService$BatteryStatsHandler .-$$Nest$mformatTime ( v1,v4,v5 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiBatteryStatsService"; // const-string v1, "MiuiBatteryStatsService"
android.util.Slog .d ( v1,v0 );
/* .line 1482 */
} // :cond_0
/* const/16 v0, 0x17c */
/* if-lt p1, v0, :cond_1 */
/* .line 1483 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v4 */
/* iput-wide v4, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeIn38C:J */
/* .line 1485 */
} // :cond_1
/* const/16 v1, 0x1ae */
/* if-lt p1, v1, :cond_2 */
/* .line 1486 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v4 */
/* iput-wide v4, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeIn43C:J */
/* .line 1488 */
} // :cond_2
/* const/16 v4, 0x63 */
/* if-lt p2, v4, :cond_3 */
/* .line 1489 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v5 */
/* iput-wide v5, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeInHighSoc:J */
/* .line 1491 */
} // :cond_3
/* if-ge p1, v0, :cond_4 */
/* iget-wide v5, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeIn38C:J */
/* cmp-long v0, v5, v2 */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 1492 */
/* iget-wide v5, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeIn38C:J */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v7 */
/* iget-wide v9, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeIn38C:J */
/* sub-long/2addr v7, v9 */
/* add-long/2addr v5, v7 */
/* iput-wide v5, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeIn38C:J */
/* .line 1493 */
/* iput-wide v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeIn38C:J */
/* .line 1495 */
} // :cond_4
/* if-ge p1, v1, :cond_5 */
/* iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeIn43C:J */
/* cmp-long v0, v0, v2 */
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 1496 */
/* iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeIn43C:J */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v5 */
/* iget-wide v7, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeIn43C:J */
/* sub-long/2addr v5, v7 */
/* add-long/2addr v0, v5 */
/* iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeIn43C:J */
/* .line 1497 */
/* iput-wide v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeIn43C:J */
/* .line 1499 */
} // :cond_5
/* if-ge p2, v4, :cond_6 */
/* iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeInHighSoc:J */
/* cmp-long v0, v0, v2 */
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 1500 */
/* iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeInHighSoc:J */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v4 */
/* iget-wide v6, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeInHighSoc:J */
/* sub-long/2addr v4, v6 */
/* add-long/2addr v0, v4 */
/* iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeInHighSoc:J */
/* .line 1501 */
/* iput-wide v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeInHighSoc:J */
/* .line 1503 */
} // :cond_6
return;
} // .end method
public void processTempSocData ( ) {
/* .locals 12 */
/* .line 1505 */
(( com.android.server.MiuiBatteryStatsService$BatteryTempSocTimeInfo ) p0 ).stopCollectTempSocData ( ); // invoke-virtual {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->stopCollectTempSocData()V
/* .line 1506 */
final String v0 = "persist.vendor.smart.battMntor"; // const-string v0, "persist.vendor.smart.battMntor"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1507 */
v0 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmMiCharge ( v0 );
final String v2 = "calc_rvalue"; // const-string v2, "calc_rvalue"
(( miui.util.IMiCharge ) v0 ).getMiChargePath ( v2 ); // invoke-virtual {v0, v2}, Lmiui/util/IMiCharge;->getMiChargePath(Ljava/lang/String;)Ljava/lang/String;
this.RValue = v0;
/* .line 1509 */
} // :cond_0
final String v0 = "0"; // const-string v0, "0"
this.RValue = v0;
/* .line 1511 */
} // :goto_0
v0 = v0 = this.allDaysData;
final String v2 = "MiuiBatteryStatsService"; // const-string v2, "MiuiBatteryStatsService"
int v3 = 7; // const/4 v3, 0x7
/* if-ge v0, v3, :cond_1 */
/* .line 1512 */
(( com.android.server.MiuiBatteryStatsService$BatteryTempSocTimeInfo ) p0 ).setDataToList ( ); // invoke-virtual {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->setDataToList()V
/* .line 1513 */
v0 = this.this$0;
v0 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetDEBUG ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "allDaysData.size()="; // const-string v4, "allDaysData.size()="
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = v4 = this.allDaysData;
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v0 );
/* .line 1515 */
} // :cond_1
v0 = v0 = this.allDaysData;
/* if-ne v0, v3, :cond_9 */
/* .line 1517 */
/* invoke-direct {p0, v1}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->getTotalTime(I)J */
/* move-result-wide v3 */
/* const-wide/32 v5, 0x134fd900 */
/* cmp-long v0, v3, v5 */
int v3 = 1; // const/4 v3, 0x1
/* if-ltz v0, :cond_2 */
/* move v0, v3 */
} // :cond_2
/* move v0, v1 */
/* .line 1519 */
/* .local v0, "isBattTempContinueAbove38C":Z */
} // :goto_1
/* invoke-direct {p0, v3}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->getTotalTime(I)J */
/* move-result-wide v4 */
/* const-wide/32 v6, 0xf053700 */
/* cmp-long v4, v4, v6 */
/* if-ltz v4, :cond_3 */
/* move v4, v3 */
} // :cond_3
/* move v4, v1 */
/* .line 1521 */
/* .local v4, "isBattTempContinueAbove43C":Z */
} // :goto_2
int v5 = 2; // const/4 v5, 0x2
/* invoke-direct {p0, v5}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->getTotalTime(I)J */
/* move-result-wide v6 */
/* const-wide/32 v8, 0x15752a00 */
/* cmp-long v6, v6, v8 */
/* if-ltz v6, :cond_4 */
/* move v6, v3 */
} // :cond_4
/* move v6, v1 */
/* .line 1523 */
/* .local v6, "isSocContinueAbove99":Z */
} // :goto_3
int v7 = 3; // const/4 v7, 0x3
v8 = /* invoke-direct {p0, v7}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->getRValue(I)I */
/* const/16 v9, 0xfc */
/* if-lt v8, v9, :cond_5 */
/* move v8, v3 */
} // :cond_5
/* move v8, v1 */
/* .line 1524 */
/* .local v8, "isSatisfyRCondition":Z */
} // :goto_4
v9 = this.this$0;
v9 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetDEBUG ( v9 );
if ( v9 != null) { // if-eqz v9, :cond_6
/* .line 1525 */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "Time of battery temp above 38c in 7 Days is "; // const-string v10, "Time of battery temp above 38c in 7 Days is "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* invoke-direct {p0, v1}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->getTotalTime(I)J */
/* move-result-wide v10 */
(( java.lang.StringBuilder ) v9 ).append ( v10, v11 ); // invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v10 = "(ms), battery temp above 43c in 7 Days is "; // const-string v10, "(ms), battery temp above 43c in 7 Days is "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1526 */
/* invoke-direct {p0, v3}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->getTotalTime(I)J */
/* move-result-wide v10 */
(( java.lang.StringBuilder ) v9 ).append ( v10, v11 ); // invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v10 = "(ms), soc above 99 in 7 Days is "; // const-string v10, "(ms), soc above 99 in 7 Days is "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1527 */
/* invoke-direct {p0, v5}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->getTotalTime(I)J */
/* move-result-wide v10 */
(( java.lang.StringBuilder ) v9 ).append ( v10, v11 ); // invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v9 = "(ms) , R value in 7 days is "; // const-string v9, "(ms) , R value in 7 days is "
(( java.lang.StringBuilder ) v5 ).append ( v9 ); // invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = /* invoke-direct {p0, v7}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->getRValue(I)I */
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1525 */
android.util.Slog .d ( v2,v5 );
/* .line 1528 */
} // :cond_6
/* if-nez v0, :cond_8 */
/* if-nez v4, :cond_8 */
/* if-nez v6, :cond_8 */
if ( v8 != null) { // if-eqz v8, :cond_7
/* .line 1532 */
} // :cond_7
com.android.server.MiuiBatteryStatsService .-$$Nest$sfputmIsSatisfyTempSocCondition ( v1 );
/* .line 1533 */
v2 = this.allDaysData;
/* .line 1529 */
} // :cond_8
} // :goto_5
com.android.server.MiuiBatteryStatsService .-$$Nest$sfputmIsSatisfyTempSocCondition ( v3 );
/* .line 1530 */
v1 = this.allDaysData;
/* .line 1535 */
} // :goto_6
v1 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmHandler ( v1 );
com.android.server.MiuiBatteryStatsService$BatteryStatsHandler .-$$Nest$msendAdjustVolBroadcast ( v1 );
/* .line 1537 */
} // .end local v0 # "isBattTempContinueAbove38C":Z
} // .end local v4 # "isBattTempContinueAbove43C":Z
} // .end local v6 # "isSocContinueAbove99":Z
} // .end local v8 # "isSatisfyRCondition":Z
} // :cond_9
(( com.android.server.MiuiBatteryStatsService$BatteryTempSocTimeInfo ) p0 ).clearData ( ); // invoke-virtual {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->clearData()V
/* .line 1538 */
return;
} // .end method
public void setDataToList ( ) {
/* .locals 3 */
/* .line 1554 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 1555 */
/* .local v0, "dataList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* iget-wide v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeIn38C:J */
/* invoke-direct {p0, v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->parseString(J)Ljava/lang/String; */
/* .line 1556 */
/* iget-wide v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeIn43C:J */
/* invoke-direct {p0, v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->parseString(J)Ljava/lang/String; */
/* .line 1557 */
/* iget-wide v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeInHighSoc:J */
/* invoke-direct {p0, v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->parseString(J)Ljava/lang/String; */
/* .line 1558 */
v1 = this.RValue;
/* .line 1559 */
v1 = this.allDaysData;
/* .line 1560 */
v1 = this.this$0;
v1 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetDEBUG ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "allDaysData="; // const-string v2, "allDaysData="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.allDaysData;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiBatteryStatsService"; // const-string v2, "MiuiBatteryStatsService"
android.util.Slog .d ( v2,v1 );
/* .line 1561 */
} // :cond_0
return;
} // .end method
public void stopCollectTempSocData ( ) {
/* .locals 8 */
/* .line 1540 */
/* iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeIn38C:J */
/* const-wide/16 v2, 0x0 */
/* cmp-long v0, v0, v2 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1541 */
/* iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeIn38C:J */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v4 */
/* iget-wide v6, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeIn38C:J */
/* sub-long/2addr v4, v6 */
/* add-long/2addr v0, v4 */
/* iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeIn38C:J */
/* .line 1542 */
/* iput-wide v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeIn38C:J */
/* .line 1544 */
} // :cond_0
/* iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeIn43C:J */
/* cmp-long v0, v0, v2 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1545 */
/* iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeIn43C:J */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v4 */
/* iget-wide v6, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeIn43C:J */
/* sub-long/2addr v4, v6 */
/* add-long/2addr v0, v4 */
/* iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeIn43C:J */
/* .line 1546 */
/* iput-wide v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeIn43C:J */
/* .line 1548 */
} // :cond_1
/* iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeInHighSoc:J */
/* cmp-long v0, v0, v2 */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1549 */
/* iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeInHighSoc:J */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v4 */
/* iget-wide v6, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeInHighSoc:J */
/* sub-long/2addr v4, v6 */
/* add-long/2addr v0, v4 */
/* iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->totalTimeInHighSoc:J */
/* .line 1550 */
/* iput-wide v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->startTimeInHighSoc:J */
/* .line 1552 */
} // :cond_2
return;
} // .end method
