class com.android.server.MiuiInputFilter$ClickableRect {
	 /* .source "MiuiInputFilter.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/MiuiInputFilter; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "ClickableRect" */
} // .end annotation
/* # instance fields */
public java.lang.Runnable mClickListener;
public android.graphics.Rect mRect;
/* # direct methods */
public com.android.server.MiuiInputFilter$ClickableRect ( ) {
/* .locals 0 */
/* .param p1, "rect" # Landroid/graphics/Rect; */
/* .param p2, "listener" # Ljava/lang/Runnable; */
/* .line 102 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 103 */
this.mRect = p1;
/* .line 104 */
this.mClickListener = p2;
/* .line 105 */
return;
} // .end method
