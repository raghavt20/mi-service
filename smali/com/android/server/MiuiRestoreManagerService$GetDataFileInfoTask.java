class com.android.server.MiuiRestoreManagerService$GetDataFileInfoTask implements java.lang.Runnable {
	 /* .source "MiuiRestoreManagerService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/MiuiRestoreManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "GetDataFileInfoTask" */
} // .end annotation
/* # instance fields */
final miui.app.backup.IGetFileInfoCallback callback;
final java.lang.String path;
final com.android.server.MiuiRestoreManagerService this$0; //synthetic
/* # direct methods */
public com.android.server.MiuiRestoreManagerService$GetDataFileInfoTask ( ) {
/* .locals 0 */
/* .param p2, "path" # Ljava/lang/String; */
/* .param p3, "callback" # Lmiui/app/backup/IGetFileInfoCallback; */
/* .line 585 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 586 */
this.path = p2;
/* .line 587 */
this.callback = p3;
/* .line 588 */
return;
} // .end method
private java.lang.String hex2Base64 ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "hex" # Ljava/lang/String; */
/* .line 625 */
v0 = android.text.TextUtils .isEmpty ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 626 */
	 final String v0 = ""; // const-string v0, ""
	 /* .line 628 */
} // :cond_0
java.util.HexFormat .of ( );
(( java.util.HexFormat ) v0 ).parseHex ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HexFormat;->parseHex(Ljava/lang/CharSequence;)[B
int v1 = 2; // const/4 v1, 0x2
android.util.Base64 .encodeToString ( v0,v1 );
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 15 */
/* .line 593 */
final String v0 = "MiuiRestoreManagerService"; // const-string v0, "MiuiRestoreManagerService"
int v1 = 0; // const/4 v1, 0x0
/* .line 595 */
/* .local v1, "info":Lmiui/app/backup/BackupFileInfo; */
try { // :try_start_0
	 /* new-instance v2, Ljava/util/ArrayList; */
	 /* invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V */
	 /* .line 596 */
	 /* .local v2, "infoList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
	 v3 = this.this$0;
	 com.android.server.MiuiRestoreManagerService .-$$Nest$fgetmInstaller ( v3 );
	 v4 = this.path;
	 v3 = 	 (( com.android.server.pm.Installer ) v3 ).getDataFileInfo ( v4, v2 ); // invoke-virtual {v3, v4, v2}, Lcom/android/server/pm/Installer;->getDataFileInfo(Ljava/lang/String;Ljava/util/List;)I
	 /* .line 597 */
	 /* .local v3, "errorCode":I */
	 /* if-nez v3, :cond_1 */
	 v4 = 	 /* .line 598 */
	 int v5 = 1; // const/4 v5, 0x1
	 if ( v4 != null) { // if-eqz v4, :cond_0
		 /* .line 599 */
		 /* new-instance v4, Lmiui/app/backup/BackupFileInfo; */
		 v6 = this.path;
		 /* invoke-direct {v4, v6, v5}, Lmiui/app/backup/BackupFileInfo;-><init>(Ljava/lang/String;Z)V */
		 /* move-object v1, v4 */
		 /* .line 601 */
	 } // :cond_0
	 int v4 = 0; // const/4 v4, 0x0
	 /* check-cast v4, Ljava/lang/String; */
	 /* .line 602 */
	 /* .local v4, "md5Hex":Ljava/lang/String; */
	 /* check-cast v5, Ljava/lang/String; */
	 /* .line 603 */
	 /* .local v5, "sha1Hex":Ljava/lang/String; */
	 int v6 = 2; // const/4 v6, 0x2
	 /* check-cast v6, Ljava/lang/String; */
	 /* .line 604 */
	 /* .local v6, "size":Ljava/lang/String; */
	 /* new-instance v14, Lmiui/app/backup/BackupFileInfo; */
	 v8 = this.path;
	 int v9 = 0; // const/4 v9, 0x0
	 /* .line 607 */
	 /* invoke-direct {p0, v4}, Lcom/android/server/MiuiRestoreManagerService$GetDataFileInfoTask;->hex2Base64(Ljava/lang/String;)Ljava/lang/String; */
	 /* .line 608 */
	 /* invoke-direct {p0, v5}, Lcom/android/server/MiuiRestoreManagerService$GetDataFileInfoTask;->hex2Base64(Ljava/lang/String;)Ljava/lang/String; */
	 /* .line 609 */
	 java.lang.Long .parseLong ( v6 );
	 /* move-result-wide v12 */
	 /* move-object v7, v14 */
	 /* invoke-direct/range {v7 ..v13}, Lmiui/app/backup/BackupFileInfo;-><init>(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;J)V */
	 /* :try_end_0 */
	 /* .catch Lcom/android/server/pm/Installer$InstallerException; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* move-object v1, v14 */
	 /* .line 615 */
} // .end local v2 # "infoList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // .end local v4 # "md5Hex":Ljava/lang/String;
} // .end local v5 # "sha1Hex":Ljava/lang/String;
} // .end local v6 # "size":Ljava/lang/String;
} // :cond_1
} // :goto_0
/* .line 612 */
} // .end local v3 # "errorCode":I
/* :catch_0 */
/* move-exception v2 */
/* .line 613 */
/* .local v2, "e":Lcom/android/server/pm/Installer$InstallerException; */
int v3 = -1; // const/4 v3, -0x1
/* .line 614 */
/* .restart local v3 # "errorCode":I */
final String v4 = "get data file info error "; // const-string v4, "get data file info error "
android.util.Slog .w ( v0,v4,v2 );
/* .line 618 */
} // .end local v2 # "e":Lcom/android/server/pm/Installer$InstallerException;
} // :goto_1
try { // :try_start_1
v2 = this.callback;
v4 = this.path;
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 621 */
/* .line 619 */
/* :catch_1 */
/* move-exception v2 */
/* .line 620 */
/* .local v2, "e":Landroid/os/RemoteException; */
final String v4 = "error when notify onGetDataFileInfoEnd "; // const-string v4, "error when notify onGetDataFileInfoEnd "
android.util.Slog .e ( v0,v4,v2 );
/* .line 622 */
} // .end local v2 # "e":Landroid/os/RemoteException;
} // :goto_2
return;
} // .end method
