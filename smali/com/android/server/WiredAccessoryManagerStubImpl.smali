.class public Lcom/android/server/WiredAccessoryManagerStubImpl;
.super Ljava/lang/Object;
.source "WiredAccessoryManagerStubImpl.java"

# interfaces
.implements Lcom/android/server/WiredAccessoryManagerStub;


# static fields
.field private static final NOTE_USB_UNSUPPORT_HEADSET_PLUG:I = 0x53488888

.field private static final SW_JACK_UNSUPPORTED_INSERT_BIT:I = 0x80000

.field private static final SW_JACK_UNSUPPORTED_INSERT_BIT_21:I = 0x100000

.field private static final SW_JACK_VIDEOOUT_INSERT:I = 0x100

.field private static final TAG:Ljava/lang/String; = "WiredAccessoryManagerStubImpl"


# instance fields
.field private mNm:Landroid/app/NotificationManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private createNotification(Landroid/content/Context;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .line 80
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.android.settings"

    const-string v2, "com.android.settings.Settings$UsbHeadsetUnSupportActivity"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Landroid/content/Intent;->makeRestartActivityTask(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 82
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x4000000

    const/4 v2, 0x0

    invoke-static {p1, v2, v0, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 84
    .local v1, "pit":Landroid/app/PendingIntent;
    sget-object v3, Lcom/android/internal/notification/SystemNotificationChannels;->USB_HEADSET:Ljava/lang/String;

    .line 85
    .local v3, "channel":Ljava/lang/String;
    const v4, 0x110f0228

    .line 87
    .local v4, "title_unsupported":I
    const v5, 0x110f0226

    .line 89
    .local v5, "text_unsupported":I
    new-instance v6, Landroid/app/Notification$Builder;

    invoke-direct {v6, p1, v3}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 90
    const v7, 0x10808b9

    invoke-virtual {v6, v7}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v6

    .line 91
    const-wide/16 v7, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v6

    .line 92
    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v6

    .line 93
    invoke-virtual {v6, v2}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;

    move-result-object v2

    .line 94
    const v6, 0x106001c

    invoke-virtual {p1, v6}, Landroid/content/Context;->getColor(I)I

    move-result v6

    invoke-virtual {v2, v6}, Landroid/app/Notification$Builder;->setColor(I)Landroid/app/Notification$Builder;

    move-result-object v2

    .line 97
    const-string/jumbo v6, "sys"

    invoke-virtual {v2, v6}, Landroid/app/Notification$Builder;->setCategory(Ljava/lang/String;)Landroid/app/Notification$Builder;

    move-result-object v2

    .line 98
    invoke-virtual {v2, v7}, Landroid/app/Notification$Builder;->setVisibility(I)Landroid/app/Notification$Builder;

    move-result-object v2

    .line 99
    invoke-virtual {v2, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v2

    .line 100
    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v2

    .line 101
    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v2

    .line 102
    .local v2, "builder":Landroid/app/Notification$Builder;
    invoke-virtual {v2}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v6

    .line 103
    .local v6, "notify":Landroid/app/Notification;
    iget-object v7, p0, Lcom/android/server/WiredAccessoryManagerStubImpl;->mNm:Landroid/app/NotificationManager;

    const v8, 0x53488888

    invoke-virtual {v7, v8, v6}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 104
    return-void
.end method

.method private isUnsupportedBit(I)Z
    .locals 2
    .param p1, "switchMask"    # I

    .line 44
    const-string v0, "lmi"

    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 45
    const/high16 v0, 0x100000

    and-int/2addr v0, p1

    if-eqz v0, :cond_1

    .line 46
    return v1

    .line 49
    :cond_0
    const/high16 v0, 0x80000

    and-int/2addr v0, p1

    if-nez v0, :cond_2

    const/16 v0, 0x114

    if-ne p1, v0, :cond_1

    goto :goto_0

    .line 55
    :cond_1
    const/4 v0, 0x0

    return v0

    .line 52
    :cond_2
    :goto_0
    return v1
.end method

.method private showUnsupportDeviceNotification(Landroid/content/Context;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "switchValues"    # I

    .line 59
    if-eqz p1, :cond_3

    .line 60
    iget-object v0, p0, Lcom/android/server/WiredAccessoryManagerStubImpl;->mNm:Landroid/app/NotificationManager;

    if-nez v0, :cond_0

    .line 61
    const-string v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/android/server/WiredAccessoryManagerStubImpl;->mNm:Landroid/app/NotificationManager;

    .line 63
    :cond_0
    const v0, 0x80014

    const-string v1, "WiredAccessoryManagerStubImpl"

    if-eq p2, v0, :cond_2

    const/16 v0, 0x114

    if-eq p2, v0, :cond_2

    const v0, 0x100014

    if-ne p2, v0, :cond_1

    goto :goto_0

    .line 72
    :cond_1
    if-nez p2, :cond_3

    .line 73
    const-string v0, "Unsupported headset removed"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    iget-object v0, p0, Lcom/android/server/WiredAccessoryManagerStubImpl;->mNm:Landroid/app/NotificationManager;

    const v1, 0x53488888

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    goto :goto_1

    .line 70
    :cond_2
    :goto_0
    const-string v0, "Unsupported headset inserted"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    invoke-direct {p0, p1}, Lcom/android/server/WiredAccessoryManagerStubImpl;->createNotification(Landroid/content/Context;)V

    .line 77
    :cond_3
    :goto_1
    return-void
.end method


# virtual methods
.method public isDeviceUnsupported(Landroid/content/Context;II)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "switchValues"    # I
    .param p3, "switchMask"    # I

    .line 35
    invoke-direct {p0, p3}, Lcom/android/server/WiredAccessoryManagerStubImpl;->isUnsupportedBit(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    const-string v0, "WiredAccessoryManagerStubImpl"

    const-string v1, "Device unsupported"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    invoke-direct {p0, p1, p2}, Lcom/android/server/WiredAccessoryManagerStubImpl;->showUnsupportDeviceNotification(Landroid/content/Context;I)V

    .line 38
    const/4 v0, 0x1

    return v0

    .line 40
    :cond_0
    const/4 v0, 0x0

    return v0
.end method
