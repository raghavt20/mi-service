class com.android.server.MiuiBatteryAuthentic$BatteryAuthenticHandler extends android.os.Handler {
	 /* .source "MiuiBatteryAuthentic.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/MiuiBatteryAuthentic; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "BatteryAuthenticHandler" */
} // .end annotation
/* # static fields */
static final Integer CONFIRM;
private static final java.lang.String CONTENT_TYPE;
private static final Integer DEFAULT_CONNECT_TIME_OUT;
private static final Integer DEFAULT_READ_TIME_OUT;
private static final Integer DELAY_SET_TIME;
static final Integer INIT;
static final Integer MSG_BATTERY_AUTHENTIC;
static final Integer MSG_SIGN_RESULT;
static final Integer MSG_VERITY_SIGN;
static final Integer MSG_VERITY_SIGN_CONFIRM;
private static final java.lang.String TYPE;
static final Integer VERIFY;
/* # instance fields */
private java.lang.String mAndroidId;
private java.lang.String mBatteryAuthentic;
private java.lang.String mChallenge;
private java.lang.String mConFirmChallenge;
private Integer mConfirmResultCount;
private Integer mGetFromServerCount;
private Boolean mIsConfirm;
private Boolean mIsGetServerInfo;
private java.lang.String mProjectName;
private Integer mTryGetBatteryCount;
final com.android.server.MiuiBatteryAuthentic this$0; //synthetic
/* # direct methods */
public com.android.server.MiuiBatteryAuthentic$BatteryAuthenticHandler ( ) {
/* .locals 1 */
/* .param p1, "this$0" # Lcom/android/server/MiuiBatteryAuthentic; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 155 */
this.this$0 = p1;
/* .line 156 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 144 */
int v0 = 0; // const/4 v0, 0x0
this.mAndroidId = v0;
/* .line 145 */
this.mProjectName = v0;
/* .line 146 */
this.mChallenge = v0;
/* .line 147 */
this.mConFirmChallenge = v0;
/* .line 148 */
this.mBatteryAuthentic = v0;
/* .line 149 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mGetFromServerCount:I */
/* .line 150 */
/* iput v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mConfirmResultCount:I */
/* .line 151 */
/* iput v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mTryGetBatteryCount:I */
/* .line 152 */
/* iput-boolean v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mIsConfirm:Z */
/* .line 153 */
/* iput-boolean v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mIsGetServerInfo:Z */
/* .line 157 */
return;
} // .end method
private java.lang.String SHA256 ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "strText" # Ljava/lang/String; */
/* .line 389 */
final String v0 = ""; // const-string v0, ""
/* .line 390 */
/* .local v0, "strResult":Ljava/lang/String; */
v1 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v1, :cond_0 */
/* .line 392 */
try { // :try_start_0
	 final String v1 = "SHA-256"; // const-string v1, "SHA-256"
	 java.security.MessageDigest .getInstance ( v1 );
	 /* .line 393 */
	 /* .local v1, "messageDigest":Ljava/security/MessageDigest; */
	 (( java.lang.String ) p1 ).getBytes ( ); // invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B
	 (( java.security.MessageDigest ) v1 ).update ( v2 ); // invoke-virtual {v1, v2}, Ljava/security/MessageDigest;->update([B)V
	 /* .line 394 */
	 (( java.security.MessageDigest ) v1 ).digest ( ); // invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B
	 /* .line 395 */
	 /* .local v2, "byteBuffer":[B */
	 (( com.android.server.MiuiBatteryAuthentic$BatteryAuthenticHandler ) p0 ).bytesToHexString ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->bytesToHexString([B)Ljava/lang/String;
	 /* :try_end_0 */
	 /* .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 ..:try_end_0} :catch_1 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* move-object v0, v3 */
	 /* .line 400 */
} // .end local v1 # "messageDigest":Ljava/security/MessageDigest;
} // .end local v2 # "byteBuffer":[B
} // :goto_0
/* .line 398 */
/* :catch_0 */
/* move-exception v1 */
/* .line 399 */
/* .local v1, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 396 */
} // .end local v1 # "e":Ljava/lang/Exception;
/* :catch_1 */
/* move-exception v1 */
/* .line 397 */
/* .local v1, "e":Ljava/security/NoSuchAlgorithmException; */
(( java.security.NoSuchAlgorithmException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V
} // .end local v1 # "e":Ljava/security/NoSuchAlgorithmException;
/* .line 402 */
} // :cond_0
} // :goto_1
} // .end method
private void closeBufferedReader ( java.io.BufferedReader p0 ) {
/* .locals 1 */
/* .param p1, "br" # Ljava/io/BufferedReader; */
/* .line 644 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 646 */
try { // :try_start_0
(( java.io.BufferedReader ) p1 ).close ( ); // invoke-virtual {p1}, Ljava/io/BufferedReader;->close()V
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 649 */
/* .line 647 */
/* :catch_0 */
/* move-exception v0 */
/* .line 648 */
/* .local v0, "e":Ljava/io/IOException; */
(( java.io.IOException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
/* .line 651 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :cond_0
} // :goto_0
return;
} // .end method
private java.lang.String getAndroidId ( ) {
/* .locals 2 */
/* .line 288 */
final String v0 = "ro.serialno"; // const-string v0, "ro.serialno"
/* const-string/jumbo v1, "unknown" */
android.os.SystemProperties .get ( v0,v1 );
/* .line 289 */
/* .local v0, "androidID":Ljava/lang/String; */
v1 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v1, :cond_0 */
/* .line 290 */
/* .line 292 */
} // :cond_0
final String v1 = ""; // const-string v1, ""
} // .end method
private java.lang.String getImei ( ) {
/* .locals 3 */
/* .line 358 */
miui.telephony.TelephonyManager .getDefault ( );
(( miui.telephony.TelephonyManager ) v0 ).getImeiList ( ); // invoke-virtual {v0}, Lmiui/telephony/TelephonyManager;->getImeiList()Ljava/util/List;
/* .line 359 */
/* .local v0, "imeis":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v1 = if ( v0 != null) { // if-eqz v0, :cond_1
/* if-nez v1, :cond_1 */
/* .line 360 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
v2 = } // :goto_0
/* if-ge v1, v2, :cond_1 */
/* .line 361 */
/* check-cast v2, Ljava/lang/CharSequence; */
v2 = android.text.TextUtils .isEmpty ( v2 );
/* if-nez v2, :cond_0 */
/* .line 362 */
/* check-cast v2, Ljava/lang/String; */
/* .line 360 */
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 366 */
} // .end local v1 # "i":I
} // :cond_1
final String v1 = "1234567890"; // const-string v1, "1234567890"
} // .end method
private java.lang.String getProjectName ( ) {
/* .locals 2 */
/* .line 296 */
final String v0 = "ro.product.device"; // const-string v0, "ro.product.device"
/* const-string/jumbo v1, "unknown" */
android.os.SystemProperties .get ( v0,v1 );
/* .line 297 */
/* .local v0, "device":Ljava/lang/String; */
v1 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v1, :cond_0 */
/* .line 298 */
/* .line 300 */
} // :cond_0
final String v1 = ""; // const-string v1, ""
} // .end method
private Object toByte ( Object p0 ) {
/* .locals 1 */
/* .param p1, "c" # C */
/* .line 353 */
final String v0 = "0123456789abcdef"; // const-string v0, "0123456789abcdef"
v0 = (( java.lang.String ) v0 ).indexOf ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->indexOf(I)I
/* int-to-byte v0, v0 */
/* .line 354 */
/* .local v0, "b":B */
} // .end method
private Boolean verifyECDSA ( ) {
/* .locals 9 */
/* .line 244 */
int v0 = 0; // const/4 v0, 0x0
/* .line 247 */
/* .local v0, "result":Z */
try { // :try_start_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
v2 = this.this$0;
v2 = this.mBatterySn;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ":"; // const-string v2, ":"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.this$0;
v2 = this.mImei;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 248 */
/* .local v1, "str":Ljava/lang/String; */
v2 = this.this$0;
v2 = this.mCloudSign;
v2 = android.text.TextUtils .isEmpty ( v2 );
/* if-nez v2, :cond_0 */
/* .line 249 */
v2 = this.this$0;
v2 = this.mCloudSign;
/* .line 253 */
/* .local v2, "signString":Ljava/lang/String; */
(( com.android.server.MiuiBatteryAuthentic$BatteryAuthenticHandler ) p0 ).hexStringToByte ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->hexStringToByte(Ljava/lang/String;)[B
/* .line 255 */
/* .local v3, "sign":[B */
/* new-instance v4, Ljava/security/spec/X509EncodedKeySpec; */
final String v5 = "3059301306072a8648ce3d020106082a8648ce3d030107034200045846fce7eaab1053c62f76cd7c61ae09a8411a5c106cad7a95c11c26dd25e507e963e2ae8f2c9672db92fe9834584dc41996454c8c929fc26e9d512e4096f450"; // const-string v5, "3059301306072a8648ce3d020106082a8648ce3d030107034200045846fce7eaab1053c62f76cd7c61ae09a8411a5c106cad7a95c11c26dd25e507e963e2ae8f2c9672db92fe9834584dc41996454c8c929fc26e9d512e4096f450"
(( com.android.server.MiuiBatteryAuthentic$BatteryAuthenticHandler ) p0 ).hexStringToByte ( v5 ); // invoke-virtual {p0, v5}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->hexStringToByte(Ljava/lang/String;)[B
/* invoke-direct {v4, v5}, Ljava/security/spec/X509EncodedKeySpec;-><init>([B)V */
/* .line 256 */
/* .local v4, "x509EncodedKeySpec":Ljava/security/spec/X509EncodedKeySpec; */
final String v5 = "EC"; // const-string v5, "EC"
java.security.KeyFactory .getInstance ( v5 );
/* .line 257 */
/* .local v5, "keyFactory":Ljava/security/KeyFactory; */
(( java.security.KeyFactory ) v5 ).generatePublic ( v4 ); // invoke-virtual {v5, v4}, Ljava/security/KeyFactory;->generatePublic(Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;
/* .line 258 */
/* .local v6, "publicKey":Ljava/security/PublicKey; */
final String v7 = "SHA256withECDSA"; // const-string v7, "SHA256withECDSA"
java.security.Signature .getInstance ( v7 );
/* .line 259 */
/* .local v7, "signature":Ljava/security/Signature; */
(( java.security.Signature ) v7 ).initVerify ( v6 ); // invoke-virtual {v7, v6}, Ljava/security/Signature;->initVerify(Ljava/security/PublicKey;)V
/* .line 260 */
(( java.lang.String ) v1 ).getBytes ( ); // invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B
(( java.security.Signature ) v7 ).update ( v8 ); // invoke-virtual {v7, v8}, Ljava/security/Signature;->update([B)V
/* .line 261 */
v8 = (( java.security.Signature ) v7 ).verify ( v3 ); // invoke-virtual {v7, v3}, Ljava/security/Signature;->verify([B)Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* move v0, v8 */
/* .line 265 */
} // .end local v1 # "str":Ljava/lang/String;
} // .end local v3 # "sign":[B
} // .end local v4 # "x509EncodedKeySpec":Ljava/security/spec/X509EncodedKeySpec;
} // .end local v5 # "keyFactory":Ljava/security/KeyFactory;
} // .end local v6 # "publicKey":Ljava/security/PublicKey;
} // .end local v7 # "signature":Ljava/security/Signature;
/* .line 251 */
} // .end local v2 # "signString":Ljava/lang/String;
/* .restart local v1 # "str":Ljava/lang/String; */
} // :cond_0
/* .line 263 */
} // .end local v1 # "str":Ljava/lang/String;
/* :catch_0 */
/* move-exception v1 */
/* .line 264 */
/* .local v1, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 266 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
} // .end method
private Boolean verifyFromServer ( ) {
/* .locals 3 */
/* .line 270 */
(( com.android.server.MiuiBatteryAuthentic$BatteryAuthenticHandler ) p0 ).batteryVerifyInit ( ); // invoke-virtual {p0}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->batteryVerifyInit()V
/* .line 271 */
v0 = this.mChallenge;
v0 = android.text.TextUtils .isEmpty ( v0 );
final String v1 = "MiuiBatteryAuthentic"; // const-string v1, "MiuiBatteryAuthentic"
/* if-nez v0, :cond_1 */
/* .line 272 */
(( com.android.server.MiuiBatteryAuthentic$BatteryAuthenticHandler ) p0 ).batteryVerify ( ); // invoke-virtual {p0}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->batteryVerify()V
/* .line 273 */
v0 = this.this$0;
v0 = this.mCloudSign;
v0 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v0, :cond_0 */
/* .line 274 */
int v0 = 2; // const/4 v0, 0x2
/* const-wide/16 v1, 0x0 */
(( com.android.server.MiuiBatteryAuthentic$BatteryAuthenticHandler ) p0 ).sendMessageDelayed ( v0, v1, v2 ); // invoke-virtual {p0, v0, v1, v2}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->sendMessageDelayed(IJ)V
/* .line 275 */
int v0 = 1; // const/4 v0, 0x1
(( com.android.server.MiuiBatteryAuthentic$BatteryAuthenticHandler ) p0 ).sendMessageDelayed ( v0, v1, v2 ); // invoke-virtual {p0, v0, v1, v2}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->sendMessageDelayed(IJ)V
/* .line 276 */
/* .line 278 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
this.mChallenge = v0;
/* .line 279 */
/* const-string/jumbo v0, "verify error failed, get sign failed" */
android.util.Slog .e ( v1,v0 );
/* .line 282 */
} // :cond_1
/* const-string/jumbo v0, "verify error failed, get challenge failed" */
android.util.Slog .e ( v1,v0 );
/* .line 284 */
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
/* # virtual methods */
public void batteryVerify ( ) {
/* .locals 14 */
/* .line 460 */
final String v0 = "MiuiBatteryAuthentic"; // const-string v0, "MiuiBatteryAuthentic"
final String v1 = "https://battery.mioffice.cn/api/bat/verify"; // const-string v1, "https://battery.mioffice.cn/api/bat/verify"
/* .line 462 */
/* .local v1, "url":Ljava/lang/String; */
int v2 = 0; // const/4 v2, 0x0
/* .line 463 */
/* .local v2, "conn":Ljavax/net/ssl/HttpsURLConnection; */
int v3 = 0; // const/4 v3, 0x0
/* .line 464 */
/* .local v3, "writer":Ljava/io/OutputStreamWriter; */
int v4 = 0; // const/4 v4, 0x0
/* .line 466 */
/* .local v4, "br":Ljava/io/BufferedReader; */
int v5 = 2; // const/4 v5, 0x2
(( com.android.server.MiuiBatteryAuthentic$BatteryAuthenticHandler ) p0 ).collectData ( v5 ); // invoke-virtual {p0, v5}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->collectData(I)Lorg/json/JSONObject;
/* .line 469 */
/* .local v5, "jsonObject":Lorg/json/JSONObject; */
try { // :try_start_0
/* new-instance v6, Ljava/net/URL; */
/* invoke-direct {v6, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V */
(( java.net.URL ) v6 ).openConnection ( ); // invoke-virtual {v6}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;
/* check-cast v6, Ljavax/net/ssl/HttpsURLConnection; */
/* move-object v2, v6 */
/* .line 470 */
/* const/16 v6, 0x7d0 */
(( javax.net.ssl.HttpsURLConnection ) v2 ).setConnectTimeout ( v6 ); // invoke-virtual {v2, v6}, Ljavax/net/ssl/HttpsURLConnection;->setConnectTimeout(I)V
/* .line 471 */
(( javax.net.ssl.HttpsURLConnection ) v2 ).setReadTimeout ( v6 ); // invoke-virtual {v2, v6}, Ljavax/net/ssl/HttpsURLConnection;->setReadTimeout(I)V
/* .line 472 */
int v6 = 1; // const/4 v6, 0x1
(( javax.net.ssl.HttpsURLConnection ) v2 ).setDoOutput ( v6 ); // invoke-virtual {v2, v6}, Ljavax/net/ssl/HttpsURLConnection;->setDoOutput(Z)V
/* .line 473 */
(( javax.net.ssl.HttpsURLConnection ) v2 ).setDoInput ( v6 ); // invoke-virtual {v2, v6}, Ljavax/net/ssl/HttpsURLConnection;->setDoInput(Z)V
/* .line 474 */
int v7 = 0; // const/4 v7, 0x0
(( javax.net.ssl.HttpsURLConnection ) v2 ).setUseCaches ( v7 ); // invoke-virtual {v2, v7}, Ljavax/net/ssl/HttpsURLConnection;->setUseCaches(Z)V
/* .line 475 */
final String v7 = "Content-Type"; // const-string v7, "Content-Type"
final String v8 = "application/json; charset=UTF-8"; // const-string v8, "application/json; charset=UTF-8"
(( javax.net.ssl.HttpsURLConnection ) v2 ).setRequestProperty ( v7, v8 ); // invoke-virtual {v2, v7, v8}, Ljavax/net/ssl/HttpsURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
/* .line 476 */
final String v7 = "POST"; // const-string v7, "POST"
(( javax.net.ssl.HttpsURLConnection ) v2 ).setRequestMethod ( v7 ); // invoke-virtual {v2, v7}, Ljavax/net/ssl/HttpsURLConnection;->setRequestMethod(Ljava/lang/String;)V
/* .line 477 */
(( javax.net.ssl.HttpsURLConnection ) v2 ).connect ( ); // invoke-virtual {v2}, Ljavax/net/ssl/HttpsURLConnection;->connect()V
/* .line 479 */
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 480 */
/* new-instance v7, Ljava/io/OutputStreamWriter; */
(( javax.net.ssl.HttpsURLConnection ) v2 ).getOutputStream ( ); // invoke-virtual {v2}, Ljavax/net/ssl/HttpsURLConnection;->getOutputStream()Ljava/io/OutputStream;
/* invoke-direct {v7, v8}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V */
/* move-object v3, v7 */
/* .line 481 */
(( org.json.JSONObject ) v5 ).toString ( ); // invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
(( java.io.OutputStreamWriter ) v3 ).write ( v7 ); // invoke-virtual {v3, v7}, Ljava/io/OutputStreamWriter;->write(Ljava/lang/String;)V
/* .line 482 */
(( java.io.OutputStreamWriter ) v3 ).flush ( ); // invoke-virtual {v3}, Ljava/io/OutputStreamWriter;->flush()V
/* .line 483 */
(( java.io.OutputStreamWriter ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/OutputStreamWriter;->close()V
/* .line 486 */
} // :cond_0
v7 = (( javax.net.ssl.HttpsURLConnection ) v2 ).getResponseCode ( ); // invoke-virtual {v2}, Ljavax/net/ssl/HttpsURLConnection;->getResponseCode()I
/* const/16 v8, 0xc8 */
/* if-ne v7, v8, :cond_4 */
/* .line 487 */
/* new-instance v7, Ljava/io/BufferedReader; */
/* new-instance v8, Ljava/io/InputStreamReader; */
(( javax.net.ssl.HttpsURLConnection ) v2 ).getInputStream ( ); // invoke-virtual {v2}, Ljavax/net/ssl/HttpsURLConnection;->getInputStream()Ljava/io/InputStream;
/* invoke-direct {v8, v9}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V */
/* const/16 v9, 0x400 */
/* invoke-direct {v7, v8, v9}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V */
/* move-object v4, v7 */
/* .line 488 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 490 */
/* .local v7, "sb":Ljava/lang/StringBuilder; */
} // :goto_0
(( java.io.BufferedReader ) v4 ).readLine ( ); // invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v9, v8 */
/* .local v9, "line":Ljava/lang/String; */
if ( v8 != null) { // if-eqz v8, :cond_1
/* .line 491 */
(( java.lang.StringBuilder ) v7 ).append ( v9 ); // invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 495 */
} // :cond_1
/* new-instance v8, Lorg/json/JSONObject; */
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v8, v10}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V */
/* .line 497 */
/* .local v8, "result":Lorg/json/JSONObject; */
final String v10 = "code"; // const-string v10, "code"
v10 = (( org.json.JSONObject ) v8 ).getInt ( v10 ); // invoke-virtual {v8, v10}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* .line 498 */
/* .local v10, "code":I */
/* if-nez v10, :cond_2 */
/* .line 499 */
final String v11 = "data"; // const-string v11, "data"
(( org.json.JSONObject ) v8 ).getJSONObject ( v11 ); // invoke-virtual {v8, v11}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
/* .line 500 */
/* .local v11, "data":Lorg/json/JSONObject; */
v12 = this.this$0;
/* const-string/jumbo v13, "sign" */
(( org.json.JSONObject ) v11 ).getString ( v13 ); // invoke-virtual {v11, v13}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
this.mCloudSign = v13;
/* .line 501 */
final String v12 = "rk"; // const-string v12, "rk"
(( org.json.JSONObject ) v11 ).getString ( v12 ); // invoke-virtual {v11, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
this.mConFirmChallenge = v12;
/* .line 502 */
/* iput-boolean v6, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mIsGetServerInfo:Z */
/* .line 503 */
} // .end local v11 # "data":Lorg/json/JSONObject;
} // :cond_2
/* const/16 v11, 0x2715 */
/* if-ne v10, v11, :cond_3 */
/* .line 504 */
/* iput-boolean v6, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mIsGetServerInfo:Z */
/* .line 506 */
} // :cond_3
final String v6 = "request result is failed"; // const-string v6, "request result is failed"
android.util.Slog .e ( v0,v6 );
/* :try_end_0 */
/* .catch Ljava/net/ProtocolException; {:try_start_0 ..:try_end_0} :catch_2 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 516 */
} // .end local v7 # "sb":Ljava/lang/StringBuilder;
} // .end local v8 # "result":Lorg/json/JSONObject;
} // .end local v9 # "line":Ljava/lang/String;
} // .end local v10 # "code":I
} // :cond_4
} // :goto_1
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 517 */
/* .line 516 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 513 */
/* :catch_0 */
/* move-exception v6 */
/* .line 514 */
/* .local v6, "e":Ljava/lang/Exception; */
try { // :try_start_1
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "batteryVerify "; // const-string v8, "batteryVerify "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v7 );
/* .line 516 */
/* nop */
} // .end local v6 # "e":Ljava/lang/Exception;
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 517 */
/* .line 511 */
/* :catch_1 */
/* move-exception v6 */
/* .line 512 */
/* .local v6, "e":Ljava/io/IOException; */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "batteryVerify IOException "; // const-string v8, "batteryVerify IOException "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v7 );
/* .line 516 */
/* nop */
} // .end local v6 # "e":Ljava/io/IOException;
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 517 */
/* .line 509 */
/* :catch_2 */
/* move-exception v6 */
/* .line 510 */
/* .local v6, "e":Ljava/net/ProtocolException; */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "batteryVerify ProtocolException "; // const-string v8, "batteryVerify ProtocolException "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v7 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 516 */
/* nop */
} // .end local v6 # "e":Ljava/net/ProtocolException;
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 517 */
} // :goto_2
(( javax.net.ssl.HttpsURLConnection ) v2 ).disconnect ( ); // invoke-virtual {v2}, Ljavax/net/ssl/HttpsURLConnection;->disconnect()V
/* .line 519 */
} // :cond_5
/* invoke-direct {p0, v4}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->closeBufferedReader(Ljava/io/BufferedReader;)V */
/* .line 520 */
/* nop */
/* .line 521 */
return;
/* .line 516 */
} // :goto_3
if ( v2 != null) { // if-eqz v2, :cond_6
/* .line 517 */
(( javax.net.ssl.HttpsURLConnection ) v2 ).disconnect ( ); // invoke-virtual {v2}, Ljavax/net/ssl/HttpsURLConnection;->disconnect()V
/* .line 519 */
} // :cond_6
/* invoke-direct {p0, v4}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->closeBufferedReader(Ljava/io/BufferedReader;)V */
/* .line 520 */
/* throw v0 */
} // .end method
public void batteryVerifyConfirm ( ) {
/* .locals 12 */
/* .line 584 */
final String v0 = "MiuiBatteryAuthentic"; // const-string v0, "MiuiBatteryAuthentic"
final String v1 = "https://battery.mioffice.cn/api/bat/confirm"; // const-string v1, "https://battery.mioffice.cn/api/bat/confirm"
/* .line 586 */
/* .local v1, "url":Ljava/lang/String; */
int v2 = 0; // const/4 v2, 0x0
/* .line 587 */
/* .local v2, "conn":Ljavax/net/ssl/HttpsURLConnection; */
int v3 = 0; // const/4 v3, 0x0
/* .line 588 */
/* .local v3, "writer":Ljava/io/OutputStreamWriter; */
int v4 = 0; // const/4 v4, 0x0
/* .line 590 */
/* .local v4, "br":Ljava/io/BufferedReader; */
int v5 = 3; // const/4 v5, 0x3
(( com.android.server.MiuiBatteryAuthentic$BatteryAuthenticHandler ) p0 ).collectData ( v5 ); // invoke-virtual {p0, v5}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->collectData(I)Lorg/json/JSONObject;
/* .line 593 */
/* .local v5, "jsonObject":Lorg/json/JSONObject; */
try { // :try_start_0
/* new-instance v6, Ljava/net/URL; */
/* invoke-direct {v6, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V */
(( java.net.URL ) v6 ).openConnection ( ); // invoke-virtual {v6}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;
/* check-cast v6, Ljavax/net/ssl/HttpsURLConnection; */
/* move-object v2, v6 */
/* .line 594 */
/* const/16 v6, 0x7d0 */
(( javax.net.ssl.HttpsURLConnection ) v2 ).setConnectTimeout ( v6 ); // invoke-virtual {v2, v6}, Ljavax/net/ssl/HttpsURLConnection;->setConnectTimeout(I)V
/* .line 595 */
(( javax.net.ssl.HttpsURLConnection ) v2 ).setReadTimeout ( v6 ); // invoke-virtual {v2, v6}, Ljavax/net/ssl/HttpsURLConnection;->setReadTimeout(I)V
/* .line 596 */
int v6 = 1; // const/4 v6, 0x1
(( javax.net.ssl.HttpsURLConnection ) v2 ).setDoOutput ( v6 ); // invoke-virtual {v2, v6}, Ljavax/net/ssl/HttpsURLConnection;->setDoOutput(Z)V
/* .line 597 */
(( javax.net.ssl.HttpsURLConnection ) v2 ).setDoInput ( v6 ); // invoke-virtual {v2, v6}, Ljavax/net/ssl/HttpsURLConnection;->setDoInput(Z)V
/* .line 598 */
int v6 = 0; // const/4 v6, 0x0
(( javax.net.ssl.HttpsURLConnection ) v2 ).setUseCaches ( v6 ); // invoke-virtual {v2, v6}, Ljavax/net/ssl/HttpsURLConnection;->setUseCaches(Z)V
/* .line 599 */
final String v6 = "Content-Type"; // const-string v6, "Content-Type"
final String v7 = "application/json; charset=UTF-8"; // const-string v7, "application/json; charset=UTF-8"
(( javax.net.ssl.HttpsURLConnection ) v2 ).setRequestProperty ( v6, v7 ); // invoke-virtual {v2, v6, v7}, Ljavax/net/ssl/HttpsURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
/* .line 600 */
final String v6 = "POST"; // const-string v6, "POST"
(( javax.net.ssl.HttpsURLConnection ) v2 ).setRequestMethod ( v6 ); // invoke-virtual {v2, v6}, Ljavax/net/ssl/HttpsURLConnection;->setRequestMethod(Ljava/lang/String;)V
/* .line 601 */
(( javax.net.ssl.HttpsURLConnection ) v2 ).connect ( ); // invoke-virtual {v2}, Ljavax/net/ssl/HttpsURLConnection;->connect()V
/* .line 603 */
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 604 */
/* new-instance v6, Ljava/io/OutputStreamWriter; */
(( javax.net.ssl.HttpsURLConnection ) v2 ).getOutputStream ( ); // invoke-virtual {v2}, Ljavax/net/ssl/HttpsURLConnection;->getOutputStream()Ljava/io/OutputStream;
/* invoke-direct {v6, v7}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V */
/* move-object v3, v6 */
/* .line 605 */
(( org.json.JSONObject ) v5 ).toString ( ); // invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
(( java.io.OutputStreamWriter ) v3 ).write ( v6 ); // invoke-virtual {v3, v6}, Ljava/io/OutputStreamWriter;->write(Ljava/lang/String;)V
/* .line 606 */
(( java.io.OutputStreamWriter ) v3 ).flush ( ); // invoke-virtual {v3}, Ljava/io/OutputStreamWriter;->flush()V
/* .line 607 */
(( java.io.OutputStreamWriter ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/OutputStreamWriter;->close()V
/* .line 610 */
} // :cond_0
v6 = (( javax.net.ssl.HttpsURLConnection ) v2 ).getResponseCode ( ); // invoke-virtual {v2}, Ljavax/net/ssl/HttpsURLConnection;->getResponseCode()I
/* const/16 v7, 0xc8 */
/* if-ne v6, v7, :cond_3 */
/* .line 611 */
/* new-instance v6, Ljava/io/BufferedReader; */
/* new-instance v7, Ljava/io/InputStreamReader; */
(( javax.net.ssl.HttpsURLConnection ) v2 ).getInputStream ( ); // invoke-virtual {v2}, Ljavax/net/ssl/HttpsURLConnection;->getInputStream()Ljava/io/InputStream;
/* invoke-direct {v7, v8}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V */
/* const/16 v8, 0x400 */
/* invoke-direct {v6, v7, v8}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V */
/* move-object v4, v6 */
/* .line 612 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 614 */
/* .local v6, "sb":Ljava/lang/StringBuilder; */
} // :goto_0
(( java.io.BufferedReader ) v4 ).readLine ( ); // invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v8, v7 */
/* .local v8, "line":Ljava/lang/String; */
if ( v7 != null) { // if-eqz v7, :cond_1
/* .line 615 */
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 619 */
} // :cond_1
/* new-instance v7, Lorg/json/JSONObject; */
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v7, v9}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V */
/* .line 621 */
/* .local v7, "result":Lorg/json/JSONObject; */
final String v9 = "code"; // const-string v9, "code"
v9 = (( org.json.JSONObject ) v7 ).getInt ( v9 ); // invoke-virtual {v7, v9}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* .line 622 */
/* .local v9, "code":I */
/* if-nez v9, :cond_2 */
/* .line 623 */
final String v10 = "data"; // const-string v10, "data"
(( org.json.JSONObject ) v7 ).getJSONObject ( v10 ); // invoke-virtual {v7, v10}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
/* .line 624 */
/* .local v10, "data":Lorg/json/JSONObject; */
final String v11 = "ret"; // const-string v11, "ret"
v11 = (( org.json.JSONObject ) v10 ).getBoolean ( v11 ); // invoke-virtual {v10, v11}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z
/* iput-boolean v11, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mIsConfirm:Z */
/* .line 625 */
} // .end local v10 # "data":Lorg/json/JSONObject;
/* .line 626 */
} // :cond_2
final String v10 = "confirm request is failed"; // const-string v10, "confirm request is failed"
android.util.Slog .e ( v0,v10 );
/* :try_end_0 */
/* .catch Ljava/net/ProtocolException; {:try_start_0 ..:try_end_0} :catch_2 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 636 */
} // .end local v6 # "sb":Ljava/lang/StringBuilder;
} // .end local v7 # "result":Lorg/json/JSONObject;
} // .end local v8 # "line":Ljava/lang/String;
} // .end local v9 # "code":I
} // :cond_3
} // :goto_1
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 637 */
/* .line 636 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 633 */
/* :catch_0 */
/* move-exception v6 */
/* .line 634 */
/* .local v6, "e":Ljava/lang/Exception; */
try { // :try_start_1
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "batteryVerifyConfirm "; // const-string v8, "batteryVerifyConfirm "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v7 );
/* .line 636 */
/* nop */
} // .end local v6 # "e":Ljava/lang/Exception;
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 637 */
/* .line 631 */
/* :catch_1 */
/* move-exception v6 */
/* .line 632 */
/* .local v6, "e":Ljava/io/IOException; */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "batteryVerifyConfirm IOException "; // const-string v8, "batteryVerifyConfirm IOException "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v7 );
/* .line 636 */
/* nop */
} // .end local v6 # "e":Ljava/io/IOException;
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 637 */
/* .line 629 */
/* :catch_2 */
/* move-exception v6 */
/* .line 630 */
/* .local v6, "e":Ljava/net/ProtocolException; */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "batteryVerifyConfirm ProtocolException "; // const-string v8, "batteryVerifyConfirm ProtocolException "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v7 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 636 */
/* nop */
} // .end local v6 # "e":Ljava/net/ProtocolException;
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 637 */
} // :goto_2
(( javax.net.ssl.HttpsURLConnection ) v2 ).disconnect ( ); // invoke-virtual {v2}, Ljavax/net/ssl/HttpsURLConnection;->disconnect()V
/* .line 639 */
} // :cond_4
/* invoke-direct {p0, v4}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->closeBufferedReader(Ljava/io/BufferedReader;)V */
/* .line 640 */
/* nop */
/* .line 641 */
return;
/* .line 636 */
} // :goto_3
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 637 */
(( javax.net.ssl.HttpsURLConnection ) v2 ).disconnect ( ); // invoke-virtual {v2}, Ljavax/net/ssl/HttpsURLConnection;->disconnect()V
/* .line 639 */
} // :cond_5
/* invoke-direct {p0, v4}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->closeBufferedReader(Ljava/io/BufferedReader;)V */
/* .line 640 */
/* throw v0 */
} // .end method
public void batteryVerifyInit ( ) {
/* .locals 12 */
/* .line 524 */
final String v0 = "MiuiBatteryAuthentic"; // const-string v0, "MiuiBatteryAuthentic"
final String v1 = "https://battery.mioffice.cn/api/bat/init"; // const-string v1, "https://battery.mioffice.cn/api/bat/init"
/* .line 526 */
/* .local v1, "url":Ljava/lang/String; */
int v2 = 0; // const/4 v2, 0x0
/* .line 527 */
/* .local v2, "conn":Ljavax/net/ssl/HttpsURLConnection; */
int v3 = 0; // const/4 v3, 0x0
/* .line 528 */
/* .local v3, "writer":Ljava/io/OutputStreamWriter; */
int v4 = 0; // const/4 v4, 0x0
/* .line 530 */
/* .local v4, "br":Ljava/io/BufferedReader; */
int v5 = 1; // const/4 v5, 0x1
(( com.android.server.MiuiBatteryAuthentic$BatteryAuthenticHandler ) p0 ).collectData ( v5 ); // invoke-virtual {p0, v5}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->collectData(I)Lorg/json/JSONObject;
/* .line 533 */
/* .local v6, "jsonObject":Lorg/json/JSONObject; */
try { // :try_start_0
/* new-instance v7, Ljava/net/URL; */
/* invoke-direct {v7, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V */
(( java.net.URL ) v7 ).openConnection ( ); // invoke-virtual {v7}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;
/* check-cast v7, Ljavax/net/ssl/HttpsURLConnection; */
/* move-object v2, v7 */
/* .line 534 */
/* const/16 v7, 0x7d0 */
(( javax.net.ssl.HttpsURLConnection ) v2 ).setConnectTimeout ( v7 ); // invoke-virtual {v2, v7}, Ljavax/net/ssl/HttpsURLConnection;->setConnectTimeout(I)V
/* .line 535 */
(( javax.net.ssl.HttpsURLConnection ) v2 ).setReadTimeout ( v7 ); // invoke-virtual {v2, v7}, Ljavax/net/ssl/HttpsURLConnection;->setReadTimeout(I)V
/* .line 536 */
(( javax.net.ssl.HttpsURLConnection ) v2 ).setDoOutput ( v5 ); // invoke-virtual {v2, v5}, Ljavax/net/ssl/HttpsURLConnection;->setDoOutput(Z)V
/* .line 537 */
(( javax.net.ssl.HttpsURLConnection ) v2 ).setDoInput ( v5 ); // invoke-virtual {v2, v5}, Ljavax/net/ssl/HttpsURLConnection;->setDoInput(Z)V
/* .line 538 */
int v5 = 0; // const/4 v5, 0x0
(( javax.net.ssl.HttpsURLConnection ) v2 ).setUseCaches ( v5 ); // invoke-virtual {v2, v5}, Ljavax/net/ssl/HttpsURLConnection;->setUseCaches(Z)V
/* .line 539 */
final String v5 = "Content-Type"; // const-string v5, "Content-Type"
final String v7 = "application/json; charset=UTF-8"; // const-string v7, "application/json; charset=UTF-8"
(( javax.net.ssl.HttpsURLConnection ) v2 ).setRequestProperty ( v5, v7 ); // invoke-virtual {v2, v5, v7}, Ljavax/net/ssl/HttpsURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
/* .line 540 */
final String v5 = "POST"; // const-string v5, "POST"
(( javax.net.ssl.HttpsURLConnection ) v2 ).setRequestMethod ( v5 ); // invoke-virtual {v2, v5}, Ljavax/net/ssl/HttpsURLConnection;->setRequestMethod(Ljava/lang/String;)V
/* .line 541 */
(( javax.net.ssl.HttpsURLConnection ) v2 ).connect ( ); // invoke-virtual {v2}, Ljavax/net/ssl/HttpsURLConnection;->connect()V
/* .line 543 */
if ( v6 != null) { // if-eqz v6, :cond_0
/* .line 544 */
/* new-instance v5, Ljava/io/OutputStreamWriter; */
(( javax.net.ssl.HttpsURLConnection ) v2 ).getOutputStream ( ); // invoke-virtual {v2}, Ljavax/net/ssl/HttpsURLConnection;->getOutputStream()Ljava/io/OutputStream;
/* invoke-direct {v5, v7}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V */
/* move-object v3, v5 */
/* .line 545 */
(( org.json.JSONObject ) v6 ).toString ( ); // invoke-virtual {v6}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
(( java.io.OutputStreamWriter ) v3 ).write ( v5 ); // invoke-virtual {v3, v5}, Ljava/io/OutputStreamWriter;->write(Ljava/lang/String;)V
/* .line 546 */
(( java.io.OutputStreamWriter ) v3 ).flush ( ); // invoke-virtual {v3}, Ljava/io/OutputStreamWriter;->flush()V
/* .line 547 */
(( java.io.OutputStreamWriter ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/OutputStreamWriter;->close()V
/* .line 550 */
} // :cond_0
v5 = (( javax.net.ssl.HttpsURLConnection ) v2 ).getResponseCode ( ); // invoke-virtual {v2}, Ljavax/net/ssl/HttpsURLConnection;->getResponseCode()I
/* const/16 v7, 0xc8 */
/* if-ne v5, v7, :cond_3 */
/* .line 551 */
/* new-instance v5, Ljava/io/BufferedReader; */
/* new-instance v7, Ljava/io/InputStreamReader; */
(( javax.net.ssl.HttpsURLConnection ) v2 ).getInputStream ( ); // invoke-virtual {v2}, Ljavax/net/ssl/HttpsURLConnection;->getInputStream()Ljava/io/InputStream;
/* invoke-direct {v7, v8}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V */
/* const/16 v8, 0x400 */
/* invoke-direct {v5, v7, v8}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V */
/* move-object v4, v5 */
/* .line 552 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 554 */
/* .local v5, "sb":Ljava/lang/StringBuilder; */
} // :goto_0
(( java.io.BufferedReader ) v4 ).readLine ( ); // invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v8, v7 */
/* .local v8, "line":Ljava/lang/String; */
if ( v7 != null) { // if-eqz v7, :cond_1
/* .line 555 */
(( java.lang.StringBuilder ) v5 ).append ( v8 ); // invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 559 */
} // :cond_1
/* new-instance v7, Lorg/json/JSONObject; */
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v7, v9}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V */
/* .line 561 */
/* .local v7, "result":Lorg/json/JSONObject; */
final String v9 = "code"; // const-string v9, "code"
v9 = (( org.json.JSONObject ) v7 ).getInt ( v9 ); // invoke-virtual {v7, v9}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* .line 562 */
/* .local v9, "code":I */
/* if-nez v9, :cond_2 */
/* .line 563 */
final String v10 = "data"; // const-string v10, "data"
(( org.json.JSONObject ) v7 ).getJSONObject ( v10 ); // invoke-virtual {v7, v10}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
/* .line 564 */
/* .local v10, "data":Lorg/json/JSONObject; */
final String v11 = "rk"; // const-string v11, "rk"
(( org.json.JSONObject ) v10 ).getString ( v11 ); // invoke-virtual {v10, v11}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
this.mChallenge = v11;
/* .line 565 */
} // .end local v10 # "data":Lorg/json/JSONObject;
/* .line 566 */
} // :cond_2
final String v10 = "init request is failed"; // const-string v10, "init request is failed"
android.util.Slog .e ( v0,v10 );
/* :try_end_0 */
/* .catch Ljava/net/ProtocolException; {:try_start_0 ..:try_end_0} :catch_2 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 576 */
} // .end local v5 # "sb":Ljava/lang/StringBuilder;
} // .end local v7 # "result":Lorg/json/JSONObject;
} // .end local v8 # "line":Ljava/lang/String;
} // .end local v9 # "code":I
} // :cond_3
} // :goto_1
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 577 */
/* .line 576 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 573 */
/* :catch_0 */
/* move-exception v5 */
/* .line 574 */
/* .local v5, "e":Ljava/lang/Exception; */
try { // :try_start_1
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "batteryVerifyInit "; // const-string v8, "batteryVerifyInit "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v5 ); // invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v7 );
/* .line 576 */
/* nop */
} // .end local v5 # "e":Ljava/lang/Exception;
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 577 */
/* .line 571 */
/* :catch_1 */
/* move-exception v5 */
/* .line 572 */
/* .local v5, "e":Ljava/io/IOException; */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "batteryVerifyInit IOException "; // const-string v8, "batteryVerifyInit IOException "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v5 ); // invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v7 );
/* .line 576 */
/* nop */
} // .end local v5 # "e":Ljava/io/IOException;
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 577 */
/* .line 569 */
/* :catch_2 */
/* move-exception v5 */
/* .line 570 */
/* .local v5, "e":Ljava/net/ProtocolException; */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "batteryVerifyInit ProtocolException "; // const-string v8, "batteryVerifyInit ProtocolException "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v5 ); // invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v7 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 576 */
/* nop */
} // .end local v5 # "e":Ljava/net/ProtocolException;
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 577 */
} // :goto_2
(( javax.net.ssl.HttpsURLConnection ) v2 ).disconnect ( ); // invoke-virtual {v2}, Ljavax/net/ssl/HttpsURLConnection;->disconnect()V
/* .line 579 */
} // :cond_4
/* invoke-direct {p0, v4}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->closeBufferedReader(Ljava/io/BufferedReader;)V */
/* .line 580 */
/* nop */
/* .line 581 */
return;
/* .line 576 */
} // :goto_3
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 577 */
(( javax.net.ssl.HttpsURLConnection ) v2 ).disconnect ( ); // invoke-virtual {v2}, Ljavax/net/ssl/HttpsURLConnection;->disconnect()V
/* .line 579 */
} // :cond_5
/* invoke-direct {p0, v4}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->closeBufferedReader(Ljava/io/BufferedReader;)V */
/* .line 580 */
/* throw v0 */
} // .end method
public java.lang.String bytesToHexString ( Object[] p0 ) {
/* .locals 6 */
/* .param p1, "src" # [B */
/* .line 326 */
/* new-instance v0, Ljava/lang/StringBuilder; */
final String v1 = ""; // const-string v1, ""
/* invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V */
/* .line 327 */
/* .local v0, "stringBuilder":Ljava/lang/StringBuilder; */
if ( p1 != null) { // if-eqz p1, :cond_3
/* array-length v1, p1 */
/* if-gtz v1, :cond_0 */
/* .line 330 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* array-length v2, p1 */
/* if-ge v1, v2, :cond_2 */
/* .line 331 */
/* aget-byte v2, p1, v1 */
/* and-int/lit16 v2, v2, 0xff */
/* .line 332 */
/* .local v2, "v":I */
java.lang.Integer .toHexString ( v2 );
/* .line 333 */
/* .local v3, "hv":Ljava/lang/String; */
v4 = (( java.lang.String ) v3 ).length ( ); // invoke-virtual {v3}, Ljava/lang/String;->length()I
int v5 = 2; // const/4 v5, 0x2
/* if-ge v4, v5, :cond_1 */
/* .line 334 */
int v4 = 0; // const/4 v4, 0x0
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* .line 336 */
} // :cond_1
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 330 */
} // .end local v2 # "v":I
} // .end local v3 # "hv":Ljava/lang/String;
/* add-int/lit8 v1, v1, 0x1 */
/* .line 338 */
} // .end local v1 # "i":I
} // :cond_2
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 328 */
} // :cond_3
} // :goto_1
int v1 = 0; // const/4 v1, 0x0
} // .end method
public org.json.JSONObject collectData ( Integer p0 ) {
/* .locals 7 */
/* .param p1, "pushMethod" # I */
/* .line 406 */
/* new-instance v0, Lorg/json/JSONObject; */
/* invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V */
/* .line 407 */
/* .local v0, "jsonObject":Lorg/json/JSONObject; */
int v1 = 0; // const/4 v1, 0x0
/* .line 409 */
/* .local v1, "BEREncoding":Ljava/lang/String; */
int v2 = 1; // const/4 v2, 0x1
/* if-ne p1, v2, :cond_3 */
/* .line 410 */
v3 = this.this$0;
v3 = this.mImei;
v3 = android.text.TextUtils .isEmpty ( v3 );
/* if-nez v3, :cond_0 */
v3 = this.this$0;
v3 = this.mImei;
final String v4 = "1234567890"; // const-string v4, "1234567890"
v3 = (( java.lang.String ) v4 ).equals ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 411 */
} // :cond_0
v3 = this.this$0;
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->getImei()Ljava/lang/String; */
this.mImei = v4;
/* .line 413 */
} // :cond_1
v3 = this.this$0;
v3 = this.mBatterySn;
v3 = android.text.TextUtils .isEmpty ( v3 );
/* if-nez v3, :cond_2 */
v3 = this.this$0;
v3 = this.mBatterySn;
final String v4 = "111111111111111111111111"; // const-string v4, "111111111111111111111111"
v3 = (( java.lang.String ) v4 ).equals ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 414 */
} // :cond_2
v3 = this.this$0;
(( com.android.server.MiuiBatteryAuthentic$BatteryAuthenticHandler ) p0 ).getBatterySn ( ); // invoke-virtual {p0}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->getBatterySn()Ljava/lang/String;
this.mBatterySn = v4;
/* .line 417 */
} // :cond_3
v3 = this.mAndroidId;
/* if-nez v3, :cond_4 */
/* .line 418 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->getAndroidId()Ljava/lang/String; */
this.mAndroidId = v3;
/* .line 420 */
} // :cond_4
v3 = this.mProjectName;
/* if-nez v3, :cond_5 */
/* .line 421 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->getProjectName()Ljava/lang/String; */
this.mProjectName = v3;
/* .line 423 */
} // :cond_5
v3 = this.mBatteryAuthentic;
/* if-nez v3, :cond_6 */
/* .line 424 */
v3 = this.this$0;
com.android.server.MiuiBatteryAuthentic .-$$Nest$fgetmMiCharge ( v3 );
final String v4 = "authentic"; // const-string v4, "authentic"
(( miui.util.IMiCharge ) v3 ).getMiChargePath ( v4 ); // invoke-virtual {v3, v4}, Lmiui/util/IMiCharge;->getMiChargePath(Ljava/lang/String;)Ljava/lang/String;
this.mBatteryAuthentic = v3;
/* .line 427 */
} // :cond_6
/* if-ne p1, v2, :cond_8 */
/* .line 428 */
try { // :try_start_0
v3 = this.this$0;
v3 = this.mBatterySn;
(( java.lang.String ) v3 ).getBytes ( ); // invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B
(( com.android.server.MiuiBatteryAuthentic$BatteryAuthenticHandler ) p0 ).bytesToHexString ( v3 ); // invoke-virtual {p0, v3}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->bytesToHexString([B)Ljava/lang/String;
/* .line 429 */
/* .local v3, "data":Ljava/lang/String; */
v4 = this.this$0;
v4 = this.mIMTService;
v5 = this.this$0;
v5 = this.mBatterySn;
/* invoke-direct {p0, v5}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->SHA256(Ljava/lang/String;)Ljava/lang/String; */
(( com.android.server.MiuiBatteryAuthentic$IMTService ) v4 ).eccSign ( v2, v5 ); // invoke-virtual {v4, v2, v5}, Lcom/android/server/MiuiBatteryAuthentic$IMTService;->eccSign(ILjava/lang/String;)Ljava/lang/String;
/* .line 430 */
/* .local v2, "sign":Ljava/lang/String; */
if ( v2 != null) { // if-eqz v2, :cond_7
v4 = (( java.lang.String ) v2 ).length ( ); // invoke-virtual {v2}, Ljava/lang/String;->length()I
/* const/16 v5, 0x40 */
/* if-le v4, v5, :cond_7 */
/* .line 431 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "30440220"; // const-string v6, "30440220"
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
int v6 = 0; // const/4 v6, 0x0
(( java.lang.String ) v2 ).substring ( v6, v5 ); // invoke-virtual {v2, v6, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = "0220"; // const-string v6, "0220"
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 432 */
(( java.lang.String ) v2 ).substring ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* move-object v1, v4 */
/* .line 434 */
} // :cond_7
v4 = this.this$0;
v4 = this.mIMTService;
(( com.android.server.MiuiBatteryAuthentic$IMTService ) v4 ).getFid ( ); // invoke-virtual {v4}, Lcom/android/server/MiuiBatteryAuthentic$IMTService;->getFid()Ljava/lang/String;
/* .line 436 */
/* .local v4, "fid":Ljava/lang/String; */
final String v5 = "fid"; // const-string v5, "fid"
(( org.json.JSONObject ) v0 ).put ( v5, v4 ); // invoke-virtual {v0, v5, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 437 */
/* const-string/jumbo v5, "sign" */
(( org.json.JSONObject ) v0 ).put ( v5, v1 ); // invoke-virtual {v0, v5, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 438 */
final String v5 = "content"; // const-string v5, "content"
(( org.json.JSONObject ) v0 ).put ( v5, v3 ); // invoke-virtual {v0, v5, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 439 */
/* nop */
} // .end local v2 # "sign":Ljava/lang/String;
} // .end local v3 # "data":Ljava/lang/String;
} // .end local v4 # "fid":Ljava/lang/String;
/* .line 453 */
/* :catch_0 */
/* move-exception v2 */
/* .line 451 */
/* :catch_1 */
/* move-exception v2 */
/* .line 440 */
} // :cond_8
int v2 = 2; // const/4 v2, 0x2
final String v3 = "rk"; // const-string v3, "rk"
/* if-ne p1, v2, :cond_9 */
/* .line 441 */
try { // :try_start_1
v2 = this.mChallenge;
(( org.json.JSONObject ) v0 ).put ( v3, v2 ); // invoke-virtual {v0, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 442 */
} // :cond_9
int v2 = 3; // const/4 v2, 0x3
/* if-ne p1, v2, :cond_a */
/* .line 443 */
v2 = this.mConFirmChallenge;
(( org.json.JSONObject ) v0 ).put ( v3, v2 ); // invoke-virtual {v0, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 444 */
final String v2 = "hostAuthentication"; // const-string v2, "hostAuthentication"
v3 = this.mBatteryAuthentic;
(( org.json.JSONObject ) v0 ).put ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 447 */
} // :cond_a
} // :goto_0
final String v2 = "imei"; // const-string v2, "imei"
v3 = this.this$0;
v3 = this.mImei;
(( org.json.JSONObject ) v0 ).put ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 448 */
/* const-string/jumbo v2, "sn" */
v3 = this.this$0;
v3 = this.mBatterySn;
(( org.json.JSONObject ) v0 ).put ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 449 */
final String v2 = "androidId"; // const-string v2, "androidId"
v3 = this.mAndroidId;
(( org.json.JSONObject ) v0 ).put ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 450 */
final String v2 = "project"; // const-string v2, "project"
v3 = this.mProjectName;
(( org.json.JSONObject ) v0 ).put ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* :try_end_1 */
/* .catch Lorg/json/JSONException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 454 */
/* .local v2, "e":Ljava/lang/Exception; */
} // :goto_1
(( java.lang.Exception ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
/* .line 452 */
/* .local v2, "e":Lorg/json/JSONException; */
} // :goto_2
(( org.json.JSONException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V
/* .line 455 */
} // .end local v2 # "e":Lorg/json/JSONException;
} // :goto_3
/* nop */
/* .line 456 */
} // :goto_4
} // .end method
public java.lang.String getBatterySn ( ) {
/* .locals 9 */
/* .line 304 */
/* new-instance v0, Ljava/lang/StringBuilder; */
final String v1 = ""; // const-string v1, ""
/* invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V */
/* .line 305 */
/* .local v0, "stringBuilder":Ljava/lang/StringBuilder; */
v1 = this.this$0;
com.android.server.MiuiBatteryAuthentic .-$$Nest$fgetmMiCharge ( v1 );
/* const-string/jumbo v2, "server_sn" */
(( miui.util.IMiCharge ) v1 ).getMiChargePath ( v2 ); // invoke-virtual {v1, v2}, Lmiui/util/IMiCharge;->getMiChargePath(Ljava/lang/String;)Ljava/lang/String;
/* .line 307 */
/* .local v1, "str":Ljava/lang/String; */
int v2 = 0; // const/4 v2, 0x0
try { // :try_start_0
final String v3 = " "; // const-string v3, " "
(( java.lang.String ) v1 ).split ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 308 */
/* .local v3, "splitString":[Ljava/lang/String; */
/* array-length v4, v3 */
/* move v5, v2 */
} // :goto_0
/* if-ge v5, v4, :cond_0 */
/* aget-object v6, v3, v5 */
/* .line 309 */
/* .local v6, "s":Ljava/lang/String; */
v7 = (( java.lang.String ) v6 ).length ( ); // invoke-virtual {v6}, Ljava/lang/String;->length()I
int v8 = 2; // const/4 v8, 0x2
(( java.lang.String ) v6 ).substring ( v8, v7 ); // invoke-virtual {v6, v8, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;
/* const/16 v8, 0x10 */
java.lang.Integer .valueOf ( v7,v8 );
v7 = (( java.lang.Integer ) v7 ).intValue ( ); // invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I
/* .line 310 */
/* .local v7, "number":I */
/* int-to-char v8, v7 */
/* .line 311 */
/* .local v8, "c":C */
(( java.lang.StringBuilder ) v0 ).append ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 308 */
/* nop */
} // .end local v6 # "s":Ljava/lang/String;
} // .end local v7 # "number":I
} // .end local v8 # "c":C
/* add-int/lit8 v5, v5, 0x1 */
/* .line 315 */
} // .end local v3 # "splitString":[Ljava/lang/String;
} // :cond_0
/* .line 313 */
/* :catch_0 */
/* move-exception v3 */
/* .line 314 */
/* .local v3, "e":Ljava/lang/Exception; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "getBatterySn failed "; // const-string v5, "getBatterySn failed "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "MiuiBatteryAuthentic"; // const-string v5, "MiuiBatteryAuthentic"
android.util.Slog .e ( v5,v4 );
/* .line 316 */
} // .end local v3 # "e":Ljava/lang/Exception;
} // :goto_1
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 317 */
/* .local v3, "result":Ljava/lang/String; */
v4 = (( java.lang.String ) v3 ).length ( ); // invoke-virtual {v3}, Ljava/lang/String;->length()I
/* if-nez v4, :cond_1 */
/* .line 318 */
final String v3 = "111111111111111111111111"; // const-string v3, "111111111111111111111111"
/* .line 319 */
} // :cond_1
v4 = (( java.lang.String ) v3 ).length ( ); // invoke-virtual {v3}, Ljava/lang/String;->length()I
/* const/16 v5, 0x18 */
/* if-lt v4, v5, :cond_2 */
/* .line 320 */
(( java.lang.String ) v3 ).substring ( v2, v5 ); // invoke-virtual {v3, v2, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;
/* .line 322 */
} // :cond_2
} // :goto_2
} // .end method
public void handleMessage ( android.os.Message p0 ) {
/* .locals 9 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 174 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* const-wide/16 v1, 0x1388 */
int v3 = 3; // const/4 v3, 0x3
/* const-wide/16 v4, 0x0 */
int v6 = 0; // const/4 v6, 0x0
int v7 = 1; // const/4 v7, 0x1
/* packed-switch v0, :pswitch_data_0 */
/* .line 239 */
final String v0 = "MiuiBatteryAuthentic"; // const-string v0, "MiuiBatteryAuthentic"
final String v1 = "No Message"; // const-string v1, "No Message"
android.util.Slog .d ( v0,v1 );
/* goto/16 :goto_1 */
/* .line 228 */
/* :pswitch_0 */
/* iget v0, p1, Landroid/os/Message;->arg1:I */
/* if-ne v0, v7, :cond_0 */
/* move v6, v7 */
} // :cond_0
/* move v0, v6 */
/* .line 229 */
/* .local v0, "success":Z */
int v4 = 0; // const/4 v4, 0x0
/* .line 230 */
/* .local v4, "callResult":Z */
/* const-string/jumbo v5, "server_result" */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 231 */
v6 = this.this$0;
com.android.server.MiuiBatteryAuthentic .-$$Nest$fgetmMiCharge ( v6 );
final String v7 = "1"; // const-string v7, "1"
v4 = (( miui.util.IMiCharge ) v6 ).setMiChargePath ( v5, v7 ); // invoke-virtual {v6, v5, v7}, Lmiui/util/IMiCharge;->setMiChargePath(Ljava/lang/String;Ljava/lang/String;)Z
/* .line 233 */
} // :cond_1
v6 = this.this$0;
com.android.server.MiuiBatteryAuthentic .-$$Nest$fgetmMiCharge ( v6 );
final String v7 = "0"; // const-string v7, "0"
v4 = (( miui.util.IMiCharge ) v6 ).setMiChargePath ( v5, v7 ); // invoke-virtual {v6, v5, v7}, Lmiui/util/IMiCharge;->setMiChargePath(Ljava/lang/String;Ljava/lang/String;)Z
/* .line 235 */
} // :goto_0
/* if-nez v4, :cond_b */
/* .line 236 */
(( com.android.server.MiuiBatteryAuthentic$BatteryAuthenticHandler ) p0 ).sendMessageDelayed ( v3, v0, v1, v2 ); // invoke-virtual {p0, v3, v0, v1, v2}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->sendMessageDelayed(IZJ)V
/* goto/16 :goto_1 */
/* .line 214 */
} // .end local v0 # "success":Z
} // .end local v4 # "callResult":Z
/* :pswitch_1 */
v0 = this.this$0;
v0 = this.mContext;
v0 = (( com.android.server.MiuiBatteryAuthentic$BatteryAuthenticHandler ) p0 ).isNetworkConnected ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->isNetworkConnected(Landroid/content/Context;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = (( com.android.server.MiuiBatteryAuthentic$BatteryAuthenticHandler ) p0 ).isDeviceProvisioned ( ); // invoke-virtual {p0}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->isDeviceProvisioned()Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 215 */
(( com.android.server.MiuiBatteryAuthentic$BatteryAuthenticHandler ) p0 ).batteryVerifyConfirm ( ); // invoke-virtual {p0}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->batteryVerifyConfirm()V
/* .line 217 */
} // :cond_2
/* iget-boolean v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mIsConfirm:Z */
/* if-nez v0, :cond_3 */
/* iget v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mConfirmResultCount:I */
/* const/16 v1, 0x3c */
/* if-ge v0, v1, :cond_3 */
/* .line 218 */
/* add-int/2addr v0, v7 */
/* iput v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mConfirmResultCount:I */
/* .line 219 */
int v0 = 2; // const/4 v0, 0x2
/* const-wide/32 v1, 0xea60 */
(( com.android.server.MiuiBatteryAuthentic$BatteryAuthenticHandler ) p0 ).sendMessageDelayed ( v0, v1, v2 ); // invoke-virtual {p0, v0, v1, v2}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->sendMessageDelayed(IJ)V
/* goto/16 :goto_1 */
/* .line 221 */
} // :cond_3
v0 = /* invoke-direct {p0}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->verifyECDSA()Z */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 222 */
v0 = this.this$0;
v0 = this.mContentResolver;
v1 = this.this$0;
v1 = this.mCloudSign;
final String v2 = "battery_authentic_certificate"; // const-string v2, "battery_authentic_certificate"
android.provider.Settings$System .putString ( v0,v2,v1 );
/* .line 224 */
} // :cond_4
/* iput v6, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mConfirmResultCount:I */
/* .line 226 */
/* goto/16 :goto_1 */
/* .line 196 */
/* :pswitch_2 */
v0 = this.this$0;
v0 = this.mImei;
v0 = android.text.TextUtils .isEmpty ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 197 */
v0 = this.this$0;
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->getImei()Ljava/lang/String; */
this.mImei = v1;
/* .line 199 */
} // :cond_5
v0 = this.this$0;
v0 = this.mBatterySn;
v0 = android.text.TextUtils .isEmpty ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 200 */
v0 = this.this$0;
(( com.android.server.MiuiBatteryAuthentic$BatteryAuthenticHandler ) p0 ).getBatterySn ( ); // invoke-virtual {p0}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->getBatterySn()Ljava/lang/String;
this.mBatterySn = v1;
/* .line 202 */
} // :cond_6
v0 = this.mAndroidId;
/* if-nez v0, :cond_7 */
/* .line 203 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->getAndroidId()Ljava/lang/String; */
this.mAndroidId = v0;
/* .line 205 */
} // :cond_7
v0 = /* invoke-direct {p0}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->verifyECDSA()Z */
if ( v0 != null) { // if-eqz v0, :cond_8
/* .line 206 */
v0 = this.this$0;
/* iput-boolean v7, v0, Lcom/android/server/MiuiBatteryAuthentic;->mIsVerified:Z */
/* .line 207 */
(( com.android.server.MiuiBatteryAuthentic$BatteryAuthenticHandler ) p0 ).sendMessageDelayed ( v3, v7, v4, v5 ); // invoke-virtual {p0, v3, v7, v4, v5}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->sendMessageDelayed(IZJ)V
/* .line 208 */
} // :cond_8
v0 = this.this$0;
/* iget-boolean v0, v0, Lcom/android/server/MiuiBatteryAuthentic;->mIsVerified:Z */
/* if-nez v0, :cond_b */
/* .line 209 */
v0 = this.this$0;
int v1 = 0; // const/4 v1, 0x0
this.mCloudSign = v1;
/* .line 210 */
(( com.android.server.MiuiBatteryAuthentic$BatteryAuthenticHandler ) p0 ).sendMessageDelayed ( v6, v4, v5 ); // invoke-virtual {p0, v6, v4, v5}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->sendMessageDelayed(IJ)V
/* .line 176 */
/* :pswitch_3 */
v0 = this.this$0;
v0 = this.mContext;
v0 = (( com.android.server.MiuiBatteryAuthentic$BatteryAuthenticHandler ) p0 ).isNetworkConnected ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->isNetworkConnected(Landroid/content/Context;)Z
if ( v0 != null) { // if-eqz v0, :cond_b
v0 = (( com.android.server.MiuiBatteryAuthentic$BatteryAuthenticHandler ) p0 ).isDeviceProvisioned ( ); // invoke-virtual {p0}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->isDeviceProvisioned()Z
if ( v0 != null) { // if-eqz v0, :cond_b
/* .line 177 */
v0 = this.this$0;
v0 = this.mBatterySn;
final String v8 = "111111111111111111111111"; // const-string v8, "111111111111111111111111"
v0 = (( java.lang.String ) v8 ).equals ( v0 ); // invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_9
/* iget v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mTryGetBatteryCount:I */
/* const/16 v8, 0xa */
/* if-ge v0, v8, :cond_9 */
/* .line 178 */
(( com.android.server.MiuiBatteryAuthentic$BatteryAuthenticHandler ) p0 ).sendMessageDelayed ( v6, v1, v2 ); // invoke-virtual {p0, v6, v1, v2}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->sendMessageDelayed(IJ)V
/* .line 179 */
v0 = this.this$0;
(( com.android.server.MiuiBatteryAuthentic$BatteryAuthenticHandler ) p0 ).getBatterySn ( ); // invoke-virtual {p0}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->getBatterySn()Ljava/lang/String;
this.mBatterySn = v1;
/* .line 180 */
/* iget v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mTryGetBatteryCount:I */
/* add-int/2addr v0, v7 */
/* iput v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mTryGetBatteryCount:I */
/* .line 181 */
return;
/* .line 183 */
} // :cond_9
v0 = /* invoke-direct {p0}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->verifyFromServer()Z */
/* if-nez v0, :cond_a */
/* iget v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mGetFromServerCount:I */
int v1 = 4; // const/4 v1, 0x4
/* if-ge v0, v1, :cond_a */
/* .line 184 */
(( com.android.server.MiuiBatteryAuthentic$BatteryAuthenticHandler ) p0 ).sendMessageDelayed ( v6, v4, v5 ); // invoke-virtual {p0, v6, v4, v5}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->sendMessageDelayed(IJ)V
/* .line 185 */
/* iget v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mGetFromServerCount:I */
/* add-int/2addr v0, v7 */
/* iput v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mGetFromServerCount:I */
/* .line 188 */
} // :cond_a
v0 = this.this$0;
/* iput-boolean v7, v0, Lcom/android/server/MiuiBatteryAuthentic;->mIsVerified:Z */
/* .line 189 */
/* iget-boolean v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mIsGetServerInfo:Z */
if ( v0 != null) { // if-eqz v0, :cond_b
/* .line 190 */
(( com.android.server.MiuiBatteryAuthentic$BatteryAuthenticHandler ) p0 ).sendMessageDelayed ( v3, v6, v4, v5 ); // invoke-virtual {p0, v3, v6, v4, v5}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->sendMessageDelayed(IZJ)V
/* .line 241 */
} // :cond_b
} // :goto_1
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public hexStringToByte ( java.lang.String p0 ) {
/* .locals 7 */
/* .param p1, "hex" # Ljava/lang/String; */
/* .line 342 */
v0 = (( java.lang.String ) p1 ).length ( ); // invoke-virtual {p1}, Ljava/lang/String;->length()I
/* div-int/lit8 v0, v0, 0x2 */
/* .line 343 */
/* .local v0, "len":I */
/* new-array v1, v0, [B */
/* .line 344 */
/* .local v1, "result":[B */
(( java.lang.String ) p1 ).toCharArray ( ); // invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C
/* .line 345 */
/* .local v2, "achar":[C */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
/* if-ge v3, v0, :cond_0 */
/* .line 346 */
/* mul-int/lit8 v4, v3, 0x2 */
/* .line 347 */
/* .local v4, "pos":I */
/* aget-char v5, v2, v4 */
v5 = /* invoke-direct {p0, v5}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->toByte(C)B */
/* shl-int/lit8 v5, v5, 0x4 */
/* add-int/lit8 v6, v4, 0x1 */
/* aget-char v6, v2, v6 */
v6 = /* invoke-direct {p0, v6}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->toByte(C)B */
/* or-int/2addr v5, v6 */
/* int-to-byte v5, v5 */
/* aput-byte v5, v1, v3 */
/* .line 345 */
} // .end local v4 # "pos":I
/* add-int/lit8 v3, v3, 0x1 */
/* .line 349 */
} // .end local v3 # "i":I
} // :cond_0
} // .end method
public Boolean isDeviceProvisioned ( ) {
/* .locals 3 */
/* .line 370 */
v0 = this.this$0;
v0 = this.mContentResolver;
final String v1 = "device_provisioned"; // const-string v1, "device_provisioned"
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.Settings$Global .getInt ( v0,v1,v2 );
if ( v0 != null) { // if-eqz v0, :cond_0
int v2 = 1; // const/4 v2, 0x1
} // :cond_0
} // .end method
public Boolean isNetworkConnected ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 374 */
int v0 = 0; // const/4 v0, 0x0
/* .line 376 */
/* .local v0, "isAvailable":Z */
try { // :try_start_0
final String v1 = "connectivity"; // const-string v1, "connectivity"
/* .line 377 */
(( android.content.Context ) p1 ).getSystemService ( v1 ); // invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v1, Landroid/net/ConnectivityManager; */
/* .line 378 */
/* .local v1, "connectivityManager":Landroid/net/ConnectivityManager; */
(( android.net.ConnectivityManager ) v1 ).getActiveNetworkInfo ( ); // invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;
/* .line 379 */
/* .local v2, "networkInfo":Landroid/net/NetworkInfo; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 380 */
v3 = (( android.net.NetworkInfo ) v2 ).isAvailable ( ); // invoke-virtual {v2}, Landroid/net/NetworkInfo;->isAvailable()Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* move v0, v3 */
/* .line 384 */
} // .end local v1 # "connectivityManager":Landroid/net/ConnectivityManager;
} // .end local v2 # "networkInfo":Landroid/net/NetworkInfo;
} // :cond_0
/* .line 382 */
/* :catch_0 */
/* move-exception v1 */
/* .line 383 */
/* .local v1, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 385 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
} // .end method
public void sendMessageDelayed ( Integer p0, Long p1 ) {
/* .locals 1 */
/* .param p1, "what" # I */
/* .param p2, "delayMillis" # J */
/* .line 160 */
(( com.android.server.MiuiBatteryAuthentic$BatteryAuthenticHandler ) p0 ).removeMessages ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->removeMessages(I)V
/* .line 161 */
android.os.Message .obtain ( p0,p1 );
/* .line 162 */
/* .local v0, "m":Landroid/os/Message; */
(( com.android.server.MiuiBatteryAuthentic$BatteryAuthenticHandler ) p0 ).sendMessageDelayed ( v0, p2, p3 ); // invoke-virtual {p0, v0, p2, p3}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 163 */
return;
} // .end method
public void sendMessageDelayed ( Integer p0, Boolean p1, Long p2 ) {
/* .locals 1 */
/* .param p1, "what" # I */
/* .param p2, "arg" # Z */
/* .param p3, "delayMillis" # J */
/* .line 166 */
(( com.android.server.MiuiBatteryAuthentic$BatteryAuthenticHandler ) p0 ).removeMessages ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->removeMessages(I)V
/* .line 167 */
android.os.Message .obtain ( p0,p1 );
/* .line 168 */
/* .local v0, "m":Landroid/os/Message; */
/* iput p2, v0, Landroid/os/Message;->arg1:I */
/* .line 169 */
(( com.android.server.MiuiBatteryAuthentic$BatteryAuthenticHandler ) p0 ).sendMessageDelayed ( v0, p3, p4 ); // invoke-virtual {p0, v0, p3, p4}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 170 */
return;
} // .end method
