.class Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl$StatusRecord;
.super Ljava/lang/Object;
.source "StatusBarManagerServiceStubImpl.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "StatusRecord"
.end annotation


# instance fields
.field action:Ljava/lang/String;

.field final synthetic this$0:Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl;

.field token:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl;

    .line 46
    iput-object p1, p0, Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl$StatusRecord;->this$0:Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public binderDied()V
    .locals 8

    .line 51
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "binder died for action="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl$StatusRecord;->action:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "StatusBarManagerServiceStubImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    iget-object v2, p0, Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl$StatusRecord;->this$0:Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl$StatusRecord;->token:Landroid/os/IBinder;

    iget-object v5, p0, Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl$StatusRecord;->action:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static {v2}, Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl;->-$$Nest$fgetmService(Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl$StatusRecord;->this$0:Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl;

    invoke-static {v0}, Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl;->-$$Nest$fgetmService(Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/internal/statusbar/IStatusBar;

    :goto_0
    move-object v7, v0

    invoke-virtual/range {v2 .. v7}, Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl;->setStatus(ILandroid/os/IBinder;Ljava/lang/String;Landroid/os/Bundle;Lcom/android/internal/statusbar/IStatusBar;)V

    .line 53
    iget-object v0, p0, Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl$StatusRecord;->token:Landroid/os/IBinder;

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 54
    return-void
.end method
