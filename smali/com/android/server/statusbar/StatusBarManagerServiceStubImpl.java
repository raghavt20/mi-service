public class com.android.server.statusbar.StatusBarManagerServiceStubImpl extends com.android.server.statusbar.StatusBarManagerServiceStub {
	 /* .source "StatusBarManagerServiceStubImpl.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.statusbar.StatusBarManagerServiceStub$$" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl$StatusRecord; */
/* } */
} // .end annotation
/* # static fields */
private static final Boolean SPEW;
private static final java.lang.String TAG;
/* # instance fields */
final java.lang.Object mLock;
private java.lang.ref.WeakReference mService;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/ref/WeakReference<", */
/* "Lcom/android/internal/statusbar/IStatusBar;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
java.util.ArrayList mStatusRecords;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl$StatusRecord;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static java.lang.ref.WeakReference -$$Nest$fgetmService ( com.android.server.statusbar.StatusBarManagerServiceStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mService;
} // .end method
public com.android.server.statusbar.StatusBarManagerServiceStubImpl ( ) {
/* .locals 1 */
/* .line 18 */
/* invoke-direct {p0}, Lcom/android/server/statusbar/StatusBarManagerServiceStub;-><init>()V */
/* .line 23 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mStatusRecords = v0;
/* .line 25 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
this.mLock = v0;
return;
} // .end method
/* # virtual methods */
void manageStatusListLocked ( Integer p0, android.os.IBinder p1, java.lang.String p2 ) {
/* .locals 5 */
/* .param p1, "what" # I */
/* .param p2, "token" # Landroid/os/IBinder; */
/* .param p3, "action" # Ljava/lang/String; */
/* .line 62 */
v0 = this.mStatusRecords;
v0 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* .line 63 */
/* .local v0, "N":I */
int v1 = 0; // const/4 v1, 0x0
/* .line 65 */
/* .local v1, "tok":Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl$StatusRecord; */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
/* if-ge v2, v0, :cond_1 */
/* .line 66 */
v3 = this.mStatusRecords;
(( java.util.ArrayList ) v3 ).get ( v2 ); // invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v3, Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl$StatusRecord; */
/* .line 67 */
/* .local v3, "t":Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl$StatusRecord; */
v4 = this.token;
/* if-ne v4, p2, :cond_0 */
/* .line 68 */
/* move-object v1, v3 */
/* .line 69 */
/* .line 65 */
} // .end local v3 # "t":Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl$StatusRecord;
} // :cond_0
/* add-int/lit8 v2, v2, 0x1 */
/* .line 72 */
} // :cond_1
} // :goto_1
int v3 = 0; // const/4 v3, 0x0
v4 = if ( p1 != null) { // if-eqz p1, :cond_4
/* if-nez v4, :cond_2 */
/* .line 78 */
} // :cond_2
/* if-nez v1, :cond_3 */
/* .line 79 */
/* new-instance v4, Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl$StatusRecord; */
/* invoke-direct {v4, p0}, Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl$StatusRecord;-><init>(Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl;)V */
/* move-object v1, v4 */
/* .line 81 */
try { // :try_start_0
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 85 */
/* nop */
/* .line 86 */
v3 = this.mStatusRecords;
(( java.util.ArrayList ) v3 ).add ( v1 ); // invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 83 */
/* :catch_0 */
/* move-exception v3 */
/* .line 84 */
/* .local v3, "ex":Landroid/os/RemoteException; */
return;
/* .line 88 */
} // .end local v3 # "ex":Landroid/os/RemoteException;
} // :cond_3
} // :goto_2
this.token = p2;
/* .line 89 */
this.action = p3;
/* .line 73 */
} // :cond_4
} // :goto_3
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 74 */
v4 = this.mStatusRecords;
(( java.util.ArrayList ) v4 ).remove ( v2 ); // invoke-virtual {v4, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;
/* .line 75 */
v4 = this.token;
/* .line 91 */
} // :cond_5
} // :goto_4
return;
} // .end method
public void setStatus ( Integer p0, android.os.IBinder p1, java.lang.String p2, android.os.Bundle p3, com.android.internal.statusbar.IStatusBar p4 ) {
/* .locals 4 */
/* .param p1, "what" # I */
/* .param p2, "token" # Landroid/os/IBinder; */
/* .param p3, "action" # Ljava/lang/String; */
/* .param p4, "ext" # Landroid/os/Bundle; */
/* .param p5, "statusBar" # Lcom/android/internal/statusbar/IStatusBar; */
/* .line 28 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 29 */
try { // :try_start_0
(( com.android.server.statusbar.StatusBarManagerServiceStubImpl ) p0 ).manageStatusListLocked ( p1, p2, p3 ); // invoke-virtual {p0, p1, p2, p3}, Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl;->manageStatusListLocked(ILandroid/os/IBinder;Ljava/lang/String;)V
/* .line 30 */
/* if-nez p5, :cond_0 */
v1 = this.mService;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 31 */
(( java.lang.ref.WeakReference ) v1 ).get ( ); // invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
/* check-cast v1, Lcom/android/internal/statusbar/IStatusBar; */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move-object p5, v1 */
/* .line 33 */
} // :cond_0
if ( p5 != null) { // if-eqz p5, :cond_2
/* .line 35 */
try { // :try_start_1
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 38 */
/* .line 36 */
/* :catch_0 */
/* move-exception v1 */
/* .line 37 */
/* .local v1, "ex":Landroid/os/RemoteException; */
try { // :try_start_2
final String v2 = "StatusBarManagerServiceStubImpl RE"; // const-string v2, "StatusBarManagerServiceStubImpl RE"
(( android.os.RemoteException ) v1 ).toString ( ); // invoke-virtual {v1}, Landroid/os/RemoteException;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v3 );
/* .line 39 */
} // .end local v1 # "ex":Landroid/os/RemoteException;
} // :goto_0
v1 = this.mService;
if ( v1 != null) { // if-eqz v1, :cond_1
(( java.lang.ref.WeakReference ) v1 ).get ( ); // invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
/* if-eq v1, p5, :cond_2 */
/* .line 40 */
} // :cond_1
/* new-instance v1, Ljava/lang/ref/WeakReference; */
/* invoke-direct {v1, p5}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V */
this.mService = v1;
/* .line 43 */
} // :cond_2
/* monitor-exit v0 */
/* .line 44 */
return;
/* .line 43 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
} // .end method
