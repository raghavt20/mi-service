.class public Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl;
.super Lcom/android/server/statusbar/StatusBarManagerServiceStub;
.source "StatusBarManagerServiceStubImpl.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.statusbar.StatusBarManagerServiceStub$$"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl$StatusRecord;
    }
.end annotation


# static fields
.field private static final SPEW:Z = false

.field private static final TAG:Ljava/lang/String; = "StatusBarManagerServiceStubImpl"


# instance fields
.field final mLock:Ljava/lang/Object;

.field private mService:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/android/internal/statusbar/IStatusBar;",
            ">;"
        }
    .end annotation
.end field

.field mStatusRecords:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl$StatusRecord;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$fgetmService(Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl;)Ljava/lang/ref/WeakReference;
    .locals 0

    iget-object p0, p0, Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl;->mService:Ljava/lang/ref/WeakReference;

    return-object p0
.end method

.method public constructor <init>()V
    .locals 1

    .line 18
    invoke-direct {p0}, Lcom/android/server/statusbar/StatusBarManagerServiceStub;-><init>()V

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl;->mStatusRecords:Ljava/util/ArrayList;

    .line 25
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl;->mLock:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method manageStatusListLocked(ILandroid/os/IBinder;Ljava/lang/String;)V
    .locals 5
    .param p1, "what"    # I
    .param p2, "token"    # Landroid/os/IBinder;
    .param p3, "action"    # Ljava/lang/String;

    .line 62
    iget-object v0, p0, Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl;->mStatusRecords:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 63
    .local v0, "N":I
    const/4 v1, 0x0

    .line 65
    .local v1, "tok":Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl$StatusRecord;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_1

    .line 66
    iget-object v3, p0, Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl;->mStatusRecords:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl$StatusRecord;

    .line 67
    .local v3, "t":Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl$StatusRecord;
    iget-object v4, v3, Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl$StatusRecord;->token:Landroid/os/IBinder;

    if-ne v4, p2, :cond_0

    .line 68
    move-object v1, v3

    .line 69
    goto :goto_1

    .line 65
    .end local v3    # "t":Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl$StatusRecord;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 72
    :cond_1
    :goto_1
    const/4 v3, 0x0

    if-eqz p1, :cond_4

    invoke-interface {p2}, Landroid/os/IBinder;->isBinderAlive()Z

    move-result v4

    if-nez v4, :cond_2

    goto :goto_3

    .line 78
    :cond_2
    if-nez v1, :cond_3

    .line 79
    new-instance v4, Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl$StatusRecord;

    invoke-direct {v4, p0}, Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl$StatusRecord;-><init>(Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl;)V

    move-object v1, v4

    .line 81
    :try_start_0
    invoke-interface {p2, v1, v3}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 85
    nop

    .line 86
    iget-object v3, p0, Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl;->mStatusRecords:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 83
    :catch_0
    move-exception v3

    .line 84
    .local v3, "ex":Landroid/os/RemoteException;
    return-void

    .line 88
    .end local v3    # "ex":Landroid/os/RemoteException;
    :cond_3
    :goto_2
    iput-object p2, v1, Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl$StatusRecord;->token:Landroid/os/IBinder;

    .line 89
    iput-object p3, v1, Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl$StatusRecord;->action:Ljava/lang/String;

    goto :goto_4

    .line 73
    :cond_4
    :goto_3
    if-eqz v1, :cond_5

    .line 74
    iget-object v4, p0, Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl;->mStatusRecords:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 75
    iget-object v4, v1, Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl$StatusRecord;->token:Landroid/os/IBinder;

    invoke-interface {v4, v1, v3}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 91
    :cond_5
    :goto_4
    return-void
.end method

.method public setStatus(ILandroid/os/IBinder;Ljava/lang/String;Landroid/os/Bundle;Lcom/android/internal/statusbar/IStatusBar;)V
    .locals 4
    .param p1, "what"    # I
    .param p2, "token"    # Landroid/os/IBinder;
    .param p3, "action"    # Ljava/lang/String;
    .param p4, "ext"    # Landroid/os/Bundle;
    .param p5, "statusBar"    # Lcom/android/internal/statusbar/IStatusBar;

    .line 28
    iget-object v0, p0, Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 29
    :try_start_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl;->manageStatusListLocked(ILandroid/os/IBinder;Ljava/lang/String;)V

    .line 30
    if-nez p5, :cond_0

    iget-object v1, p0, Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl;->mService:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_0

    .line 31
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/internal/statusbar/IStatusBar;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object p5, v1

    .line 33
    :cond_0
    if-eqz p5, :cond_2

    .line 35
    :try_start_1
    invoke-interface {p5, p1, p3, p4}, Lcom/android/internal/statusbar/IStatusBar;->setStatus(ILjava/lang/String;Landroid/os/Bundle;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 38
    goto :goto_0

    .line 36
    :catch_0
    move-exception v1

    .line 37
    .local v1, "ex":Landroid/os/RemoteException;
    :try_start_2
    const-string v2, "StatusBarManagerServiceStubImpl RE"

    invoke-virtual {v1}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 39
    .end local v1    # "ex":Landroid/os/RemoteException;
    :goto_0
    iget-object v1, p0, Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl;->mService:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-eq v1, p5, :cond_2

    .line 40
    :cond_1
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p5}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl;->mService:Ljava/lang/ref/WeakReference;

    .line 43
    :cond_2
    monitor-exit v0

    .line 44
    return-void

    .line 43
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method
