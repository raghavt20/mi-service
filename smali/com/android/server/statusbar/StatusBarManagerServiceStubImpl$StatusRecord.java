class com.android.server.statusbar.StatusBarManagerServiceStubImpl$StatusRecord implements android.os.IBinder$DeathRecipient {
	 /* .source "StatusBarManagerServiceStubImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "StatusRecord" */
} // .end annotation
/* # instance fields */
java.lang.String action;
final com.android.server.statusbar.StatusBarManagerServiceStubImpl this$0; //synthetic
android.os.IBinder token;
/* # direct methods */
 com.android.server.statusbar.StatusBarManagerServiceStubImpl$StatusRecord ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl; */
/* .line 46 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void binderDied ( ) {
/* .locals 8 */
/* .line 51 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "binder died for action="; // const-string v1, "binder died for action="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.action;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "StatusBarManagerServiceStubImpl"; // const-string v1, "StatusBarManagerServiceStubImpl"
android.util.Slog .i ( v1,v0 );
/* .line 52 */
v2 = this.this$0;
int v3 = 0; // const/4 v3, 0x0
v4 = this.token;
v5 = this.action;
int v6 = 0; // const/4 v6, 0x0
com.android.server.statusbar.StatusBarManagerServiceStubImpl .-$$Nest$fgetmService ( v2 );
/* if-nez v0, :cond_0 */
int v0 = 0; // const/4 v0, 0x0
} // :cond_0
v0 = this.this$0;
com.android.server.statusbar.StatusBarManagerServiceStubImpl .-$$Nest$fgetmService ( v0 );
(( java.lang.ref.WeakReference ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
/* check-cast v0, Lcom/android/internal/statusbar/IStatusBar; */
} // :goto_0
/* move-object v7, v0 */
/* invoke-virtual/range {v2 ..v7}, Lcom/android/server/statusbar/StatusBarManagerServiceStubImpl;->setStatus(ILandroid/os/IBinder;Ljava/lang/String;Landroid/os/Bundle;Lcom/android/internal/statusbar/IStatusBar;)V */
/* .line 53 */
v0 = this.token;
int v1 = 0; // const/4 v1, 0x0
/* .line 54 */
return;
} // .end method
