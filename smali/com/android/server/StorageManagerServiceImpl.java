public class com.android.server.StorageManagerServiceImpl extends com.android.server.StorageManagerServiceStub {
	 /* .source "StorageManagerServiceImpl.java" */
	 /* # static fields */
	 public static final Integer EUA_SUPPORTEDSIZE;
	 private static final java.lang.String HAL_DEFAULT;
	 private static final java.lang.String HAL_INTERFACE_DESCRIPTOR;
	 private static final java.lang.String HAL_SERVICE_NAME;
	 private static final java.lang.String PROP_ABREUSE_SIZE;
	 private static final java.lang.String TAG;
	 private static final Long THRESHOLD_1;
	 private static final Long THRESHOLD_2;
	 private static final Long THRESHOLD_3;
	 private static final Integer TRANSACTION_EUASUPPORTEDSIZE;
	 public static Boolean sLowStorage;
	 /* # direct methods */
	 static com.android.server.StorageManagerServiceImpl ( ) {
		 /* .locals 3 */
		 /* .line 24 */
		 v0 = android.util.DataUnit.GIBIBYTES;
		 /* const-wide/16 v1, 0x0 */
		 (( android.util.DataUnit ) v0 ).toBytes ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/util/DataUnit;->toBytes(J)J
		 /* move-result-wide v0 */
		 /* sput-wide v0, Lcom/android/server/StorageManagerServiceImpl;->THRESHOLD_1:J */
		 /* .line 25 */
		 v0 = android.util.DataUnit.GIBIBYTES;
		 /* const-wide/16 v1, 0x5 */
		 (( android.util.DataUnit ) v0 ).toBytes ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/util/DataUnit;->toBytes(J)J
		 /* move-result-wide v0 */
		 /* sput-wide v0, Lcom/android/server/StorageManagerServiceImpl;->THRESHOLD_2:J */
		 /* .line 26 */
		 v0 = android.util.DataUnit.GIBIBYTES;
		 /* const-wide/16 v1, 0xa */
		 (( android.util.DataUnit ) v0 ).toBytes ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/util/DataUnit;->toBytes(J)J
		 /* move-result-wide v0 */
		 /* sput-wide v0, Lcom/android/server/StorageManagerServiceImpl;->THRESHOLD_3:J */
		 /* .line 28 */
		 int v0 = 0; // const/4 v0, 0x0
		 com.android.server.StorageManagerServiceImpl.sLowStorage = (v0!= 0);
		 /* .line 35 */
		 v0 = 		 com.android.server.StorageManagerServiceImpl .getEUASupportSizeInternal ( );
		 return;
	 } // .end method
	 public com.android.server.StorageManagerServiceImpl ( ) {
		 /* .locals 0 */
		 /* .line 22 */
		 /* invoke-direct {p0}, Lcom/android/server/StorageManagerServiceStub;-><init>()V */
		 return;
	 } // .end method
	 private static Integer getEUASupportSizeInternal ( ) {
		 /* .locals 8 */
		 /* .line 93 */
		 /* const-string/jumbo v0, "vendor.xiaomi.hardware.fbo@1.0::IFbo" */
		 final String v1 = "StorageManagerService"; // const-string v1, "StorageManagerService"
		 /* new-instance v2, Landroid/os/HwParcel; */
		 /* invoke-direct {v2}, Landroid/os/HwParcel;-><init>()V */
		 /* .line 95 */
		 /* .local v2, "hidlReply":Landroid/os/HwParcel; */
		 int v3 = 0; // const/4 v3, 0x0
		 try { // :try_start_0
			 final String v4 = "default"; // const-string v4, "default"
			 android.os.HwBinder .getService ( v0,v4 );
			 /* .line 96 */
			 /* .local v4, "hwService":Landroid/os/IHwBinder; */
			 if ( v4 != null) { // if-eqz v4, :cond_0
				 /* .line 97 */
				 /* new-instance v5, Landroid/os/HwParcel; */
				 /* invoke-direct {v5}, Landroid/os/HwParcel;-><init>()V */
				 /* .line 98 */
				 /* .local v5, "hidlRequest":Landroid/os/HwParcel; */
				 (( android.os.HwParcel ) v5 ).writeInterfaceToken ( v0 ); // invoke-virtual {v5, v0}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
				 /* .line 99 */
				 /* const/16 v0, 0x9 */
				 /* .line 100 */
				 (( android.os.HwParcel ) v2 ).verifySuccess ( ); // invoke-virtual {v2}, Landroid/os/HwParcel;->verifySuccess()V
				 /* .line 101 */
				 (( android.os.HwParcel ) v5 ).releaseTemporaryStorage ( ); // invoke-virtual {v5}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
				 /* .line 102 */
				 v0 = 				 (( android.os.HwParcel ) v2 ).readInt32 ( ); // invoke-virtual {v2}, Landroid/os/HwParcel;->readInt32()I
				 /* .line 103 */
				 /* .local v0, "size":I */
				 /* new-instance v6, Ljava/lang/StringBuilder; */
				 /* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
				 final String v7 = "return hidlReply.readInt();"; // const-string v7, "return hidlReply.readInt();"
				 (( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
				 (( java.lang.StringBuilder ) v6 ).append ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
				 (( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
				 android.util.Slog .d ( v1,v6 );
				 /* :try_end_0 */
				 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
				 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
				 /* .line 104 */
				 /* nop */
				 /* .line 111 */
				 (( android.os.HwParcel ) v2 ).release ( ); // invoke-virtual {v2}, Landroid/os/HwParcel;->release()V
				 /* .line 104 */
				 /* .line 106 */
			 } // .end local v0 # "size":I
		 } // .end local v5 # "hidlRequest":Landroid/os/HwParcel;
	 } // :cond_0
	 try { // :try_start_1
		 final String v0 = "Failed to get MiuiFboService for EUA support size"; // const-string v0, "Failed to get MiuiFboService for EUA support size"
		 android.util.Slog .e ( v1,v0 );
		 /* :try_end_1 */
		 /* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
		 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
		 /* .line 111 */
		 /* nop */
	 } // .end local v4 # "hwService":Landroid/os/IHwBinder;
} // :goto_0
(( android.os.HwParcel ) v2 ).release ( ); // invoke-virtual {v2}, Landroid/os/HwParcel;->release()V
/* .line 112 */
/* .line 111 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 108 */
/* :catch_0 */
/* move-exception v0 */
/* .line 109 */
/* .local v0, "e":Ljava/lang/Exception; */
try { // :try_start_2
	 /* new-instance v4, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v5 = "fail to user MiuiFboService EUA_SupportSize:"; // const-string v5, "fail to user MiuiFboService EUA_SupportSize:"
	 (( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .e ( v1,v4 );
	 /* :try_end_2 */
	 /* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
	 /* .line 111 */
	 /* nop */
} // .end local v0 # "e":Ljava/lang/Exception;
/* .line 113 */
} // :goto_1
/* .line 111 */
} // :goto_2
(( android.os.HwParcel ) v2 ).release ( ); // invoke-virtual {v2}, Landroid/os/HwParcel;->release()V
/* .line 112 */
/* throw v0 */
} // .end method
public static Long getSizeInternal ( Long p0, Long p1, Long p2 ) {
/* .locals 3 */
/* .param p0, "abreuseSize" # J */
/* .param p2, "availableUserdataSize" # J */
/* .param p4, "primaryStorageSize" # J */
/* .line 56 */
/* sget-boolean v0, Lcom/android/server/StorageManagerServiceImpl;->sLowStorage:Z */
com.android.server.StorageManagerServiceImpl .getThreshold ( p4,p5 );
/* move-result-wide v1 */
/* cmp-long v1, p2, v1 */
/* if-gtz v1, :cond_0 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
/* or-int/2addr v0, v1 */
com.android.server.StorageManagerServiceImpl.sLowStorage = (v0!= 0);
/* .line 57 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* const-wide/16 v0, 0x0 */
} // :cond_1
/* move-wide v0, p0 */
} // :goto_1
/* add-long/2addr v0, p2 */
/* return-wide v0 */
} // .end method
private static Long getThreshold ( Long p0 ) {
/* .locals 4 */
/* .param p0, "totalStorageSize" # J */
/* .line 42 */
v0 = android.util.DataUnit.GIGABYTES;
/* add-int/lit8 v2, v1, 0x40 */
/* int-to-long v2, v2 */
(( android.util.DataUnit ) v0 ).toBytes ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/util/DataUnit;->toBytes(J)J
/* move-result-wide v2 */
/* cmp-long v0, p0, v2 */
/* if-gez v0, :cond_0 */
/* .line 43 */
/* sget-wide v0, Lcom/android/server/StorageManagerServiceImpl;->THRESHOLD_1:J */
/* return-wide v0 */
/* .line 44 */
} // :cond_0
v0 = android.util.DataUnit.GIGABYTES;
/* add-int/lit16 v1, v1, 0x200 */
/* int-to-long v1, v1 */
(( android.util.DataUnit ) v0 ).toBytes ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/util/DataUnit;->toBytes(J)J
/* move-result-wide v0 */
/* cmp-long v0, p0, v0 */
/* if-gez v0, :cond_1 */
/* .line 45 */
/* sget-wide v0, Lcom/android/server/StorageManagerServiceImpl;->THRESHOLD_2:J */
/* return-wide v0 */
/* .line 47 */
} // :cond_1
/* sget-wide v0, Lcom/android/server/StorageManagerServiceImpl;->THRESHOLD_3:J */
/* return-wide v0 */
} // .end method
/* # virtual methods */
public Long getAvailableStorageSize ( android.content.Context p0, Long p1 ) {
/* .locals 12 */
/* .param p1, "mContext" # Landroid/content/Context; */
/* .param p2, "availableBytes" # J */
/* .line 73 */
/* const-class v0, Landroid/os/storage/StorageManager; */
(( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
/* check-cast v0, Landroid/os/storage/StorageManager; */
/* .line 74 */
/* .local v0, "storage":Landroid/os/storage/StorageManager; */
(( android.os.storage.StorageManager ) v0 ).getPrimaryStorageSize ( ); // invoke-virtual {v0}, Landroid/os/storage/StorageManager;->getPrimaryStorageSize()J
/* move-result-wide v7 */
/* .line 76 */
/* .local v7, "primaryStorageSize":J */
/* const-string/jumbo v1, "sys.abreuse.size" */
final String v2 = "0"; // const-string v2, "0"
android.os.SystemProperties .get ( v1,v2 );
/* .line 77 */
/* .local v9, "abreuseSizeStr":Ljava/lang/String; */
/* const-wide/16 v1, 0x0 */
/* .line 79 */
/* .local v1, "abreuseSize":J */
try { // :try_start_0
java.lang.Long .parseLong ( v9 );
/* move-result-wide v3 */
/* :try_end_0 */
/* .catch Ljava/lang/NumberFormatException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-wide v1, v3 */
/* .line 82 */
/* move-wide v10, v1 */
/* .line 80 */
/* :catch_0 */
/* move-exception v3 */
/* .line 81 */
/* .local v3, "e":Ljava/lang/NumberFormatException; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "abnormal reuse size "; // const-string v5, "abnormal reuse size "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v9 ); // invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "StorageManagerService"; // const-string v5, "StorageManagerService"
android.util.Slog .w ( v5,v4 );
/* move-wide v10, v1 */
/* .line 84 */
} // .end local v1 # "abreuseSize":J
} // .end local v3 # "e":Ljava/lang/NumberFormatException;
/* .local v10, "abreuseSize":J */
} // :goto_0
/* move-wide v1, v10 */
/* move-wide v3, p2 */
/* move-wide v5, v7 */
/* invoke-static/range {v1 ..v6}, Lcom/android/server/StorageManagerServiceImpl;->getSizeInternal(JJJ)J */
/* move-result-wide v1 */
/* return-wide v1 */
} // .end method
public Integer getEUASupportSize ( ) {
/* .locals 1 */
/* .line 132 */
} // .end method
public Long getPrimaryStorageSizeInternal ( Long p0 ) {
/* .locals 6 */
/* .param p1, "totalSize" # J */
/* .line 124 */
v0 = android.util.DataUnit.GIBIBYTES;
/* int-to-long v2, v1 */
/* .line 125 */
(( android.util.DataUnit ) v0 ).toBytes ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/util/DataUnit;->toBytes(J)J
/* move-result-wide v2 */
/* sub-long v2, p1, v2 */
/* .line 124 */
android.os.FileUtils .squareStorageSize ( v2,v3 );
/* move-result-wide v2 */
/* .line 126 */
/* .local v2, "ufsIntrinsic":J */
v0 = android.util.DataUnit.GIGABYTES;
/* int-to-long v4, v1 */
(( android.util.DataUnit ) v0 ).toBytes ( v4, v5 ); // invoke-virtual {v0, v4, v5}, Landroid/util/DataUnit;->toBytes(J)J
/* move-result-wide v0 */
/* add-long/2addr v0, v2 */
/* return-wide v0 */
} // .end method
