class com.android.server.MiuiRestoreManagerService$ListDataDirTask implements java.lang.Runnable {
	 /* .source "MiuiRestoreManagerService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/MiuiRestoreManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "ListDataDirTask" */
} // .end annotation
/* # instance fields */
final miui.app.backup.IListDirCallback callback;
final Integer maxCount;
final java.lang.String path;
final Long start;
final com.android.server.MiuiRestoreManagerService this$0; //synthetic
/* # direct methods */
public com.android.server.MiuiRestoreManagerService$ListDataDirTask ( ) {
/* .locals 0 */
/* .param p2, "path" # Ljava/lang/String; */
/* .param p3, "start" # J */
/* .param p5, "maxCount" # I */
/* .param p6, "callback" # Lmiui/app/backup/IListDirCallback; */
/* .line 638 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 639 */
this.path = p2;
/* .line 640 */
/* iput-wide p3, p0, Lcom/android/server/MiuiRestoreManagerService$ListDataDirTask;->start:J */
/* .line 641 */
/* iput p5, p0, Lcom/android/server/MiuiRestoreManagerService$ListDataDirTask;->maxCount:I */
/* .line 642 */
this.callback = p6;
/* .line 643 */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 13 */
/* .line 648 */
final String v0 = "MiuiRestoreManagerService"; // const-string v0, "MiuiRestoreManagerService"
int v1 = 0; // const/4 v1, 0x0
/* new-array v2, v1, [Ljava/lang/String; */
/* .line 649 */
/* .local v2, "data":[Ljava/lang/String; */
int v3 = 1; // const/4 v3, 0x1
/* new-array v3, v3, [J */
/* .line 651 */
/* .local v3, "offset":[J */
try { // :try_start_0
	 /* new-instance v4, Ljava/util/ArrayList; */
	 /* invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V */
	 /* move-object v12, v4 */
	 /* .line 652 */
	 /* .local v12, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
	 v4 = this.this$0;
	 com.android.server.MiuiRestoreManagerService .-$$Nest$fgetmInstaller ( v4 );
	 v5 = this.path;
	 /* iget-wide v6, p0, Lcom/android/server/MiuiRestoreManagerService$ListDataDirTask;->start:J */
	 /* iget v8, p0, Lcom/android/server/MiuiRestoreManagerService$ListDataDirTask;->maxCount:I */
	 /* int-to-long v8, v8 */
	 /* move-object v10, v12 */
	 /* move-object v11, v3 */
	 v4 = 	 /* invoke-virtual/range {v4 ..v11}, Lcom/android/server/pm/Installer;->listDataDir(Ljava/lang/String;JJLjava/util/List;[J)I */
	 /* .line 653 */
	 /* .local v4, "errorCode":I */
	 /* if-nez v4, :cond_0 */
	 /* .line 654 */
	 /* check-cast v5, [Ljava/lang/String; */
	 /* :try_end_0 */
	 /* .catch Lcom/android/server/pm/Installer$InstallerException; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* move-object v2, v5 */
	 /* .line 659 */
} // .end local v12 # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // :cond_0
/* .line 656 */
} // .end local v4 # "errorCode":I
/* :catch_0 */
/* move-exception v4 */
/* .line 657 */
/* .local v4, "e":Lcom/android/server/pm/Installer$InstallerException; */
int v5 = -1; // const/4 v5, -0x1
/* .line 658 */
/* .local v5, "errorCode":I */
final String v6 = "list data dir error "; // const-string v6, "list data dir error "
android.util.Slog .w ( v0,v6,v4 );
/* move v4, v5 */
/* .line 662 */
} // .end local v5 # "errorCode":I
/* .local v4, "errorCode":I */
} // :goto_0
try { // :try_start_1
v7 = this.callback;
v9 = this.path;
/* aget-wide v11, v3, v1 */
/* move v8, v4 */
/* move-object v10, v2 */
/* invoke-interface/range {v7 ..v12}, Lmiui/app/backup/IListDirCallback;->onListDataDirEnd(ILjava/lang/String;[Ljava/lang/String;J)V */
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 665 */
/* .line 663 */
/* :catch_1 */
/* move-exception v1 */
/* .line 664 */
/* .local v1, "e":Landroid/os/RemoteException; */
final String v5 = "error when notify onListDataDirEnd "; // const-string v5, "error when notify onListDataDirEnd "
android.util.Slog .e ( v0,v5,v1 );
/* .line 666 */
} // .end local v1 # "e":Landroid/os/RemoteException;
} // :goto_1
return;
} // .end method
