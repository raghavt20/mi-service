.class Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;
.super Ljava/lang/Object;
.source "MiuiRestoreManagerService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/MiuiRestoreManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MoveDataTask"
.end annotation


# instance fields
.field destPath:Ljava/lang/String;

.field flag:I

.field installer:Lcom/android/server/pm/Installer;

.field packageName:Ljava/lang/String;

.field packageUserId:I

.field seInfo:Ljava/lang/String;

.field service:Lcom/android/server/MiuiRestoreManagerService;

.field sourcePath:Ljava/lang/String;

.field userId:I


# direct methods
.method private constructor <init>(Lcom/android/server/MiuiRestoreManagerService;Lcom/android/server/pm/Installer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;I)V
    .locals 0
    .param p1, "service"    # Lcom/android/server/MiuiRestoreManagerService;
    .param p2, "installer"    # Lcom/android/server/pm/Installer;
    .param p3, "sourcePath"    # Ljava/lang/String;
    .param p4, "destPath"    # Ljava/lang/String;
    .param p5, "packageName"    # Ljava/lang/String;
    .param p6, "packageUserId"    # I
    .param p7, "userId"    # I
    .param p8, "seInfo"    # Ljava/lang/String;
    .param p9, "flag"    # I

    .line 425
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 426
    iput-object p1, p0, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;->service:Lcom/android/server/MiuiRestoreManagerService;

    .line 427
    iput-object p2, p0, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;->installer:Lcom/android/server/pm/Installer;

    .line 428
    iput-object p3, p0, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;->sourcePath:Ljava/lang/String;

    .line 429
    iput-object p4, p0, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;->destPath:Ljava/lang/String;

    .line 430
    iput-object p5, p0, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;->packageName:Ljava/lang/String;

    .line 431
    iput p6, p0, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;->packageUserId:I

    .line 432
    iput p7, p0, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;->userId:I

    .line 433
    iput-object p8, p0, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;->seInfo:Ljava/lang/String;

    .line 434
    iput p9, p0, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;->flag:I

    .line 435
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/MiuiRestoreManagerService;Lcom/android/server/pm/Installer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;ILcom/android/server/MiuiRestoreManagerService$MoveDataTask-IA;)V
    .locals 0

    invoke-direct/range {p0 .. p9}, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;-><init>(Lcom/android/server/MiuiRestoreManagerService;Lcom/android/server/pm/Installer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .line 439
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "execute move:dp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;->destPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",pkg="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ",userId="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;->userId:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ",uid="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;->packageUserId:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v3, "MiuiRestoreManagerService"

    invoke-static {v3, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 441
    const/4 v0, 0x0

    .line 442
    .local v0, "result":Z
    const/4 v4, 0x0

    .line 443
    .local v4, "tag":I
    iget v5, p0, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;->flag:I

    and-int/lit8 v6, v5, 0x2

    if-eqz v6, :cond_0

    .line 444
    or-int/lit8 v4, v4, 0x2

    .line 446
    :cond_0
    and-int/lit8 v5, v5, 0x4

    if-eqz v5, :cond_1

    .line 447
    or-int/lit8 v4, v4, 0x4

    .line 452
    :cond_1
    :try_start_0
    invoke-static {}, Lcom/android/server/appcacheopt/AppCacheOptimizerStub;->getInstance()Lcom/android/server/appcacheopt/AppCacheOptimizerStub;

    move-result-object v5

    iget-object v6, p0, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;->packageName:Ljava/lang/String;

    iget v7, p0, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;->userId:I

    invoke-interface {v5, v6, v7}, Lcom/android/server/appcacheopt/AppCacheOptimizerStub;->umountByPackageName(Ljava/lang/String;I)V

    .line 454
    iget-object v5, p0, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;->installer:Lcom/android/server/pm/Installer;

    iget-object v6, p0, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;->sourcePath:Ljava/lang/String;

    iget-object v7, p0, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;->destPath:Ljava/lang/String;

    iget v9, p0, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;->packageUserId:I

    iget-object v10, p0, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;->seInfo:Ljava/lang/String;

    move v8, v9

    move v11, v4

    invoke-virtual/range {v5 .. v11}, Lcom/android/server/pm/Installer;->moveData(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;I)Z

    move-result v5

    move v0, v5

    .line 462
    invoke-static {}, Lcom/android/server/appcacheopt/AppCacheOptimizerStub;->getInstance()Lcom/android/server/appcacheopt/AppCacheOptimizerStub;

    move-result-object v5

    iget-object v6, p0, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;->packageName:Ljava/lang/String;

    invoke-interface {v5, v6}, Lcom/android/server/appcacheopt/AppCacheOptimizerStub;->mountByPackageName(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 466
    goto :goto_0

    .line 464
    :catch_0
    move-exception v5

    .line 465
    .local v5, "e":Ljava/lang/Exception;
    const-string v6, "move data error "

    invoke-static {v3, v6, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 468
    .end local v5    # "e":Ljava/lang/Exception;
    :goto_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "result="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v5, p0, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;->userId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 469
    iget-object v1, p0, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;->service:Lcom/android/server/MiuiRestoreManagerService;

    iget-object v2, p0, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;->packageName:Ljava/lang/String;

    iget v3, p0, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;->userId:I

    if-eqz v0, :cond_2

    const/4 v5, 0x0

    goto :goto_1

    :cond_2
    const/4 v5, 0x1

    :goto_1
    invoke-static {v1, v2, v3, v5}, Lcom/android/server/MiuiRestoreManagerService;->-$$Nest$mnotifyMoveTaskEnd(Lcom/android/server/MiuiRestoreManagerService;Ljava/lang/String;II)V

    .line 470
    return-void
.end method
