public class com.android.server.inputmethod.StylusInputMethodSwitcher extends com.android.server.inputmethod.BaseInputMethodSwitcher {
	 /* .source "StylusInputMethodSwitcher.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/inputmethod/StylusInputMethodSwitcher$SettingsObserver; */
	 /* } */
} // .end annotation
/* # static fields */
public static final Boolean DEBUG;
private static final java.lang.String ENABLE_STYLUS_HAND_WRITING;
public static final java.lang.String HAND_WRITING_KEYBOARD_TYPE;
private static final java.lang.String MIUI_STYLUS_INPUT_METHOD_APP_PKG_NAME;
private static final Boolean SUPPORT_STYLUS_INPUT_METHOD;
public static final java.lang.String TAG;
/* # instance fields */
private com.android.server.inputmethod.StylusInputMethodSwitcher$SettingsObserver mSettingsObserver;
private Boolean mStylusEnabled;
/* # direct methods */
static Boolean -$$Nest$fgetmStylusEnabled ( com.android.server.inputmethod.StylusInputMethodSwitcher p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->mStylusEnabled:Z */
} // .end method
static void -$$Nest$fputmStylusEnabled ( com.android.server.inputmethod.StylusInputMethodSwitcher p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->mStylusEnabled:Z */
	 return;
} // .end method
static void -$$Nest$mupdateFromSettingsLocked ( com.android.server.inputmethod.StylusInputMethodSwitcher p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->updateFromSettingsLocked()V */
	 return;
} // .end method
static Boolean -$$Nest$sfgetSUPPORT_STYLUS_INPUT_METHOD ( ) { //bridge//synthethic
	 /* .locals 1 */
	 /* sget-boolean v0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->SUPPORT_STYLUS_INPUT_METHOD:Z */
} // .end method
static com.android.server.inputmethod.StylusInputMethodSwitcher ( ) {
	 /* .locals 2 */
	 /* .line 38 */
	 /* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
	 int v1 = 0; // const/4 v1, 0x0
	 /* if-nez v0, :cond_0 */
	 /* .line 39 */
	 /* const-string/jumbo v0, "support_stylus_gesture" */
	 v0 = 	 miui.util.FeatureParser .getBoolean ( v0,v1 );
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 int v1 = 1; // const/4 v1, 0x1
	 } // :cond_0
	 /* nop */
} // :goto_0
com.android.server.inputmethod.StylusInputMethodSwitcher.SUPPORT_STYLUS_INPUT_METHOD = (v1!= 0);
/* .line 38 */
return;
} // .end method
public com.android.server.inputmethod.StylusInputMethodSwitcher ( ) {
/* .locals 0 */
/* .line 34 */
/* invoke-direct {p0}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;-><init>()V */
return;
} // .end method
private java.lang.String getStylusMethodIdLocked ( ) {
/* .locals 3 */
/* .line 278 */
v0 = this.mService;
v0 = this.mMethodMap;
(( android.util.ArrayMap ) v0 ).entrySet ( ); // invoke-virtual {v0}, Landroid/util/ArrayMap;->entrySet()Ljava/util/Set;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Ljava/util/Map$Entry; */
/* .line 279 */
/* .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Landroid/view/inputmethod/InputMethodInfo;>;" */
/* check-cast v2, Ljava/lang/String; */
v2 = /* invoke-direct {p0, v2}, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->isStylusMethodLocked(Ljava/lang/String;)Z */
if ( v2 != null) { // if-eqz v2, :cond_0
	 /* .line 280 */
	 /* check-cast v0, Ljava/lang/String; */
	 /* .line 282 */
} // .end local v1 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Landroid/view/inputmethod/InputMethodInfo;>;"
} // :cond_0
/* .line 283 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean isEditorInDefaultImeApp ( android.view.inputmethod.EditorInfo p0 ) {
/* .locals 4 */
/* .param p1, "editor" # Landroid/view/inputmethod/EditorInfo; */
/* .line 295 */
v0 = this.packageName;
/* .line 296 */
/* .local v0, "pkg":Ljava/lang/String; */
v1 = this.mService;
v1 = this.mSettings;
(( com.android.server.inputmethod.InputMethodUtils$InputMethodSettings ) v1 ).getSelectedInputMethod ( ); // invoke-virtual {v1}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getSelectedInputMethod()Ljava/lang/String;
/* .line 297 */
/* .local v1, "defaultIme":Ljava/lang/String; */
int v2 = 0; // const/4 v2, 0x0
/* .line 298 */
/* .local v2, "cn":Landroid/content/ComponentName; */
v3 = android.text.TextUtils .isEmpty ( v1 );
/* if-nez v3, :cond_0 */
/* .line 299 */
android.content.ComponentName .unflattenFromString ( v1 );
/* move-object v2, v3 */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 300 */
(( android.content.ComponentName ) v2 ).getPackageName ( ); // invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
v3 = android.text.TextUtils .equals ( v0,v3 );
if ( v3 != null) { // if-eqz v3, :cond_0
int v3 = 1; // const/4 v3, 0x1
} // :cond_0
int v3 = 0; // const/4 v3, 0x0
/* .line 298 */
} // :goto_0
} // .end method
private Boolean isStylusDeviceConnected ( ) {
/* .locals 8 */
/* .line 166 */
v0 = this.mService;
v0 = this.mContext;
final String v1 = "input"; // const-string v1, "input"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/hardware/input/InputManager; */
/* .line 167 */
/* .local v0, "inputManager":Landroid/hardware/input/InputManager; */
(( android.hardware.input.InputManager ) v0 ).getInputDeviceIds ( ); // invoke-virtual {v0}, Landroid/hardware/input/InputManager;->getInputDeviceIds()[I
/* .line 168 */
/* .local v1, "inputDeviceIds":[I */
/* array-length v2, v1 */
int v3 = 0; // const/4 v3, 0x0
/* move v4, v3 */
} // :goto_0
/* if-ge v4, v2, :cond_1 */
/* aget v5, v1, v4 */
/* .line 169 */
/* .local v5, "inputDeviceId":I */
(( android.hardware.input.InputManager ) v0 ).getInputDevice ( v5 ); // invoke-virtual {v0, v5}, Landroid/hardware/input/InputManager;->getInputDevice(I)Landroid/view/InputDevice;
/* .line 170 */
/* .local v6, "inputDevice":Landroid/view/InputDevice; */
v7 = /* invoke-direct {p0, v6}, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->isXiaomiStylus(Landroid/view/InputDevice;)Z */
if ( v7 != null) { // if-eqz v7, :cond_0
/* .line 171 */
int v2 = 1; // const/4 v2, 0x1
/* .line 168 */
} // .end local v5 # "inputDeviceId":I
} // .end local v6 # "inputDevice":Landroid/view/InputDevice;
} // :cond_0
/* add-int/lit8 v4, v4, 0x1 */
/* .line 174 */
} // :cond_1
} // .end method
private Boolean isStylusMethodLocked ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "methodId" # Ljava/lang/String; */
/* .line 273 */
v0 = this.mService;
v0 = this.mMethodMap;
(( android.util.ArrayMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Landroid/view/inputmethod/InputMethodInfo; */
/* .line 274 */
/* .local v0, "imi":Landroid/view/inputmethod/InputMethodInfo; */
if ( v0 != null) { // if-eqz v0, :cond_0
(( android.view.inputmethod.InputMethodInfo ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;
final String v2 = "com.miui.handwriting"; // const-string v2, "com.miui.handwriting"
v1 = android.text.TextUtils .equals ( v1,v2 );
if ( v1 != null) { // if-eqz v1, :cond_0
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
} // .end method
private Boolean isXiaomiStylus ( android.view.InputDevice p0 ) {
/* .locals 2 */
/* .param p1, "inputDevice" # Landroid/view/InputDevice; */
/* .line 181 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_0 */
/* .line 182 */
/* .line 184 */
} // :cond_0
v1 = (( android.view.InputDevice ) p1 ).isXiaomiStylus ( ); // invoke-virtual {p1}, Landroid/view/InputDevice;->isXiaomiStylus()I
/* if-lez v1, :cond_1 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
} // .end method
private void updateFromSettingsLocked ( ) {
/* .locals 2 */
/* .line 287 */
v0 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v0 ).getSelectedMethodIdLocked ( ); // invoke-virtual {v0}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->mStylusEnabled:Z */
/* if-nez v0, :cond_0 */
v0 = this.mService;
/* .line 288 */
(( com.android.server.inputmethod.InputMethodManagerService ) v0 ).getSelectedMethodIdLocked ( ); // invoke-virtual {v0}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;
/* .line 287 */
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->isStylusMethodLocked(Ljava/lang/String;)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 289 */
v0 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v0 ).clearClientSessionsLocked ( ); // invoke-virtual {v0}, Lcom/android/server/inputmethod/InputMethodManagerService;->clearClientSessionsLocked()V
/* .line 290 */
v0 = this.mService;
int v1 = 2; // const/4 v1, 0x2
(( com.android.server.inputmethod.InputMethodManagerService ) v0 ).unbindCurrentClientLocked ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/inputmethod/InputMethodManagerService;->unbindCurrentClientLocked(I)V
/* .line 292 */
} // :cond_0
return;
} // .end method
/* # virtual methods */
public java.util.List filterMethodLocked ( java.util.List p0 ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Landroid/view/inputmethod/InputMethodInfo;", */
/* ">;)", */
/* "Ljava/util/List<", */
/* "Landroid/view/inputmethod/InputMethodInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 212 */
/* .local p1, "methodInfos":Ljava/util/List;, "Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;" */
/* sget-boolean v0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->SUPPORT_STYLUS_INPUT_METHOD:Z */
/* if-nez v0, :cond_0 */
/* .line 213 */
/* .line 215 */
} // :cond_0
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 216 */
/* .local v0, "noStylusMethodInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/inputmethod/InputMethodInfo;>;" */
if ( p1 != null) { // if-eqz p1, :cond_2
/* .line 217 */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Landroid/view/inputmethod/InputMethodInfo; */
/* .line 218 */
/* .local v2, "methodInfo":Landroid/view/inputmethod/InputMethodInfo; */
(( android.view.inputmethod.InputMethodInfo ) v2 ).getId ( ); // invoke-virtual {v2}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;
v3 = /* invoke-direct {p0, v3}, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->isStylusMethodLocked(Ljava/lang/String;)Z */
/* if-nez v3, :cond_1 */
/* .line 219 */
(( java.util.ArrayList ) v0 ).add ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 221 */
} // .end local v2 # "methodInfo":Landroid/view/inputmethod/InputMethodInfo;
} // :cond_1
/* .line 223 */
} // :cond_2
} // .end method
public Boolean mayChangeInputMethodLocked ( android.view.inputmethod.EditorInfo p0 ) {
/* .locals 18 */
/* .param p1, "attribute" # Landroid/view/inputmethod/EditorInfo; */
/* .line 71 */
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p1 */
/* sget-boolean v2, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->SUPPORT_STYLUS_INPUT_METHOD:Z */
int v3 = 0; // const/4 v3, 0x0
/* if-nez v2, :cond_0 */
/* .line 72 */
/* .line 74 */
} // :cond_0
v2 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v2 ).getSelectedMethodIdLocked ( ); // invoke-virtual {v2}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;
final String v4 = "StylusInputMethodSwitcher"; // const-string v4, "StylusInputMethodSwitcher"
/* if-nez v2, :cond_1 */
/* .line 75 */
final String v2 = "input_service has no current_method_id"; // const-string v2, "input_service has no current_method_id"
android.util.Slog .w ( v4,v2 );
/* .line 76 */
/* .line 78 */
} // :cond_1
/* if-nez v1, :cond_2 */
/* .line 79 */
final String v2 = "editor_info is null, we cannot judge"; // const-string v2, "editor_info is null, we cannot judge"
android.util.Slog .w ( v4,v2 );
/* .line 80 */
/* .line 82 */
} // :cond_2
v2 = this.mService;
v2 = this.mMethodMap;
v5 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v5 ).getSelectedMethodIdLocked ( ); // invoke-virtual {v5}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;
(( android.util.ArrayMap ) v2 ).get ( v5 ); // invoke-virtual {v2, v5}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v2, Landroid/view/inputmethod/InputMethodInfo; */
/* .line 83 */
/* .local v2, "curMethodInfo":Landroid/view/inputmethod/InputMethodInfo; */
/* if-nez v2, :cond_3 */
/* .line 84 */
final String v5 = "fail to find current_method_info in the map"; // const-string v5, "fail to find current_method_info in the map"
android.util.Slog .w ( v4,v5 );
/* .line 85 */
/* .line 87 */
} // :cond_3
com.android.server.inputmethod.InputMethodManagerServiceImpl .getInstance ( );
v5 = this.mBindingController;
/* if-nez v5, :cond_4 */
/* .line 88 */
final String v5 = "IMMS_IMPL has not init"; // const-string v5, "IMMS_IMPL has not init"
android.util.Slog .w ( v4,v5 );
/* .line 89 */
/* .line 91 */
} // :cond_4
/* iget v5, v1, Landroid/view/inputmethod/EditorInfo;->inputType:I */
v5 = com.android.server.inputmethod.StylusInputMethodSwitcher .isPasswdInputType ( v5 );
if ( v5 != null) { // if-eqz v5, :cond_5
/* .line 92 */
/* .line 94 */
} // :cond_5
v5 = this.extras;
/* .line 95 */
/* .local v5, "extras":Landroid/os/Bundle; */
/* if-nez v5, :cond_6 */
/* .line 96 */
/* .line 98 */
} // :cond_6
final String v6 = "hand_writing_keyboard_type"; // const-string v6, "hand_writing_keyboard_type"
v6 = (( android.os.Bundle ) v5 ).getInt ( v6, v3 ); // invoke-virtual {v5, v6, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I
/* .line 99 */
/* .local v6, "realToolType":I */
v7 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v7 ).getSelectedMethodIdLocked ( ); // invoke-virtual {v7}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;
v7 = /* invoke-direct {v0, v7}, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->isStylusMethodLocked(Ljava/lang/String;)Z */
int v8 = 2; // const/4 v8, 0x2
int v9 = 1; // const/4 v9, 0x1
/* if-nez v7, :cond_7 */
/* .line 100 */
v7 = /* invoke-direct/range {p0 ..p0}, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->isStylusDeviceConnected()Z */
if ( v7 != null) { // if-eqz v7, :cond_7
/* iget-boolean v7, v0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->mStylusEnabled:Z */
if ( v7 != null) { // if-eqz v7, :cond_7
/* if-ne v6, v8, :cond_7 */
/* .line 101 */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->getStylusMethodIdLocked()Ljava/lang/String; */
v7 = android.text.TextUtils .isEmpty ( v7 );
/* if-nez v7, :cond_7 */
/* .line 102 */
v7 = /* invoke-direct/range {p0 ..p1}, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->isEditorInDefaultImeApp(Landroid/view/inputmethod/EditorInfo;)Z */
/* if-nez v7, :cond_7 */
/* move v7, v9 */
} // :cond_7
/* move v7, v3 */
/* .line 103 */
/* .local v7, "switchToStylusInput":Z */
} // :goto_0
v10 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v10 ).getSelectedMethodIdLocked ( ); // invoke-virtual {v10}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;
v10 = /* invoke-direct {v0, v10}, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->isStylusMethodLocked(Ljava/lang/String;)Z */
if ( v10 != null) { // if-eqz v10, :cond_9
/* if-eq v6, v9, :cond_8 */
/* iget-boolean v10, v0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->mStylusEnabled:Z */
if ( v10 != null) { // if-eqz v10, :cond_8
/* .line 104 */
v10 = /* invoke-direct/range {p0 ..p0}, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->isStylusDeviceConnected()Z */
/* if-nez v10, :cond_9 */
/* .line 105 */
} // :cond_8
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->getStylusMethodIdLocked()Ljava/lang/String; */
v10 = android.text.TextUtils .isEmpty ( v10 );
/* if-nez v10, :cond_9 */
/* .line 106 */
v10 = /* invoke-direct/range {p0 ..p1}, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->isEditorInDefaultImeApp(Landroid/view/inputmethod/EditorInfo;)Z */
/* if-nez v10, :cond_9 */
/* move v10, v9 */
} // :cond_9
/* move v10, v3 */
/* .line 107 */
/* .local v10, "switchFromStylusInput":Z */
} // :goto_1
int v11 = -1; // const/4 v11, -0x1
if ( v7 != null) { // if-eqz v7, :cond_b
/* .line 108 */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->getStylusMethodIdLocked()Ljava/lang/String; */
/* .line 109 */
/* .local v12, "stylusMethodId":Ljava/lang/String; */
v13 = android.text.TextUtils .isEmpty ( v12 );
if ( v13 != null) { // if-eqz v13, :cond_a
/* .line 110 */
final String v8 = "fail to find stylus_input_method in input_method_list"; // const-string v8, "fail to find stylus_input_method in input_method_list"
android.util.Slog .w ( v4,v8 );
/* .line 111 */
/* .line 113 */
} // :cond_a
com.android.server.inputmethod.InputMethodManagerServiceImpl .getInstance ( );
v3 = this.mBindingController;
/* .line 114 */
(( com.android.server.inputmethod.InputMethodBindingController ) v3 ).setSelectedMethodId ( v12 ); // invoke-virtual {v3, v12}, Lcom/android/server/inputmethod/InputMethodBindingController;->setSelectedMethodId(Ljava/lang/String;)V
/* .line 115 */
v3 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v3 ).clearClientSessionsLocked ( ); // invoke-virtual {v3}, Lcom/android/server/inputmethod/InputMethodManagerService;->clearClientSessionsLocked()V
/* .line 116 */
v3 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v3 ).unbindCurrentClientLocked ( v8 ); // invoke-virtual {v3, v8}, Lcom/android/server/inputmethod/InputMethodManagerService;->unbindCurrentClientLocked(I)V
/* .line 117 */
com.android.server.inputmethod.InputMethodManagerServiceImpl .getInstance ( );
v3 = this.mBindingController;
(( com.android.server.inputmethod.InputMethodBindingController ) v3 ).unbindCurrentMethod ( ); // invoke-virtual {v3}, Lcom/android/server/inputmethod/InputMethodBindingController;->unbindCurrentMethod()V
/* .line 118 */
v3 = this.mService;
v4 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v4 ).getSelectedMethodIdLocked ( ); // invoke-virtual {v4}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;
(( com.android.server.inputmethod.InputMethodManagerService ) v3 ).setInputMethodLocked ( v4, v11 ); // invoke-virtual {v3, v4, v11}, Lcom/android/server/inputmethod/InputMethodManagerService;->setInputMethodLocked(Ljava/lang/String;I)V
/* .line 119 */
/* .line 120 */
} // .end local v12 # "stylusMethodId":Ljava/lang/String;
} // :cond_b
if ( v10 != null) { // if-eqz v10, :cond_13
/* .line 121 */
v12 = this.mService;
v12 = this.mSettings;
(( com.android.server.inputmethod.InputMethodUtils$InputMethodSettings ) v12 ).getSelectedInputMethod ( ); // invoke-virtual {v12}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getSelectedInputMethod()Ljava/lang/String;
/* .line 122 */
/* .local v12, "selectedInputMethod":Ljava/lang/String; */
v13 = android.text.TextUtils .isEmpty ( v12 );
if ( v13 != null) { // if-eqz v13, :cond_10
/* .line 123 */
/* const-string/jumbo v13, "something is weired, maybe the input method app are uninstalled" */
android.util.Slog .w ( v4,v13 );
/* .line 124 */
v13 = this.mService;
v13 = this.mSettings;
/* .line 125 */
(( com.android.server.inputmethod.InputMethodUtils$InputMethodSettings ) v13 ).getEnabledInputMethodListLocked ( ); // invoke-virtual {v13}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getEnabledInputMethodListLocked()Ljava/util/ArrayList;
/* .line 124 */
com.android.server.inputmethod.InputMethodInfoUtils .getMostApplicableDefaultIME ( v13 );
/* .line 126 */
/* .local v13, "imi":Landroid/view/inputmethod/InputMethodInfo; */
final String v14 = "com.miui.handwriting"; // const-string v14, "com.miui.handwriting"
if ( v13 != null) { // if-eqz v13, :cond_c
(( android.view.inputmethod.InputMethodInfo ) v13 ).getPackageName ( ); // invoke-virtual {v13}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;
v15 = android.text.TextUtils .equals ( v15,v14 );
if ( v15 != null) { // if-eqz v15, :cond_10
/* .line 127 */
} // :cond_c
final String v15 = "fail to find a most applicable default ime"; // const-string v15, "fail to find a most applicable default ime"
android.util.Slog .w ( v4,v15 );
/* .line 128 */
v15 = this.mService;
v15 = this.mSettings;
(( com.android.server.inputmethod.InputMethodUtils$InputMethodSettings ) v15 ).getEnabledInputMethodListLocked ( ); // invoke-virtual {v15}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getEnabledInputMethodListLocked()Ljava/util/ArrayList;
/* .line 129 */
/* .local v15, "imiList":Ljava/util/List;, "Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;" */
v16 = if ( v15 != null) { // if-eqz v15, :cond_f
/* if-nez v16, :cond_d */
/* .line 133 */
} // :cond_d
} // :goto_2
v17 = /* invoke-interface/range {v16 ..v16}, Ljava/util/Iterator;->hasNext()Z */
if ( v17 != null) { // if-eqz v17, :cond_10
/* invoke-interface/range {v16 ..v16}, Ljava/util/Iterator;->next()Ljava/lang/Object; */
/* check-cast v17, Landroid/view/inputmethod/InputMethodInfo; */
/* .line 134 */
/* .local v17, "inputMethodInfo":Landroid/view/inputmethod/InputMethodInfo; */
/* invoke-virtual/range {v17 ..v17}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String; */
v9 = android.text.TextUtils .equals ( v9,v14 );
/* if-nez v9, :cond_e */
/* .line 135 */
/* invoke-virtual/range {v17 ..v17}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String; */
/* .line 136 */
/* .line 138 */
} // .end local v17 # "inputMethodInfo":Landroid/view/inputmethod/InputMethodInfo;
} // :cond_e
int v9 = 1; // const/4 v9, 0x1
/* .line 130 */
} // :cond_f
} // :goto_3
/* const-string/jumbo v8, "there is no enabled method list" */
android.util.Slog .w ( v4,v8 );
/* .line 131 */
/* .line 141 */
} // .end local v13 # "imi":Landroid/view/inputmethod/InputMethodInfo;
} // .end local v15 # "imiList":Ljava/util/List;, "Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;"
} // :cond_10
} // :goto_4
v9 = android.text.TextUtils .isEmpty ( v12 );
if ( v9 != null) { // if-eqz v9, :cond_11
/* .line 142 */
final String v8 = "finally, we still fail to find default input method"; // const-string v8, "finally, we still fail to find default input method"
android.util.Slog .w ( v4,v8 );
/* .line 143 */
/* .line 145 */
} // :cond_11
v9 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v9 ).getSelectedMethodIdLocked ( ); // invoke-virtual {v9}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;
v9 = android.text.TextUtils .equals ( v9,v12 );
if ( v9 != null) { // if-eqz v9, :cond_12
/* .line 146 */
final String v8 = "It looks like there is only miui_stylus_input_method in the system"; // const-string v8, "It looks like there is only miui_stylus_input_method in the system"
android.util.Slog .w ( v4,v8 );
/* .line 147 */
/* .line 149 */
} // :cond_12
com.android.server.inputmethod.InputMethodManagerServiceImpl .getInstance ( );
v3 = this.mBindingController;
/* .line 150 */
(( com.android.server.inputmethod.InputMethodBindingController ) v3 ).setSelectedMethodId ( v12 ); // invoke-virtual {v3, v12}, Lcom/android/server/inputmethod/InputMethodBindingController;->setSelectedMethodId(Ljava/lang/String;)V
/* .line 151 */
v3 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v3 ).clearClientSessionsLocked ( ); // invoke-virtual {v3}, Lcom/android/server/inputmethod/InputMethodManagerService;->clearClientSessionsLocked()V
/* .line 152 */
v3 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v3 ).unbindCurrentClientLocked ( v8 ); // invoke-virtual {v3, v8}, Lcom/android/server/inputmethod/InputMethodManagerService;->unbindCurrentClientLocked(I)V
/* .line 153 */
com.android.server.inputmethod.InputMethodManagerServiceImpl .getInstance ( );
v3 = this.mBindingController;
(( com.android.server.inputmethod.InputMethodBindingController ) v3 ).unbindCurrentMethod ( ); // invoke-virtual {v3}, Lcom/android/server/inputmethod/InputMethodBindingController;->unbindCurrentMethod()V
/* .line 154 */
v3 = this.mService;
v4 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v4 ).getSelectedMethodIdLocked ( ); // invoke-virtual {v4}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;
(( com.android.server.inputmethod.InputMethodManagerService ) v3 ).setInputMethodLocked ( v4, v11 ); // invoke-virtual {v3, v4, v11}, Lcom/android/server/inputmethod/InputMethodManagerService;->setInputMethodLocked(Ljava/lang/String;I)V
/* .line 155 */
int v3 = 1; // const/4 v3, 0x1
/* .line 157 */
} // .end local v12 # "selectedInputMethod":Ljava/lang/String;
} // :cond_13
} // .end method
public void onSwitchUserLocked ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "newUserId" # I */
/* .line 61 */
/* sget-boolean v0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->SUPPORT_STYLUS_INPUT_METHOD:Z */
/* if-nez v0, :cond_0 */
/* .line 62 */
return;
/* .line 64 */
} // :cond_0
v0 = this.mSettingsObserver;
(( com.android.server.inputmethod.StylusInputMethodSwitcher$SettingsObserver ) v0 ).registerContentObserverLocked ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/inputmethod/StylusInputMethodSwitcher$SettingsObserver;->registerContentObserverLocked(I)V
/* .line 65 */
v0 = this.mService;
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v1, "stylus_handwriting_enable" */
int v2 = 1; // const/4 v2, 0x1
v0 = android.provider.Settings$System .getIntForUser ( v0,v1,v2,p1 );
if ( v0 != null) { // if-eqz v0, :cond_1
} // :cond_1
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
/* iput-boolean v2, p0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->mStylusEnabled:Z */
/* .line 67 */
return;
} // .end method
public void onSystemRunningLocked ( ) {
/* .locals 4 */
/* .line 48 */
/* sget-boolean v0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->SUPPORT_STYLUS_INPUT_METHOD:Z */
/* if-nez v0, :cond_0 */
/* .line 49 */
return;
/* .line 51 */
} // :cond_0
/* new-instance v0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher$SettingsObserver; */
com.android.server.inputmethod.InputMethodManagerServiceImpl .getInstance ( );
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/inputmethod/StylusInputMethodSwitcher$SettingsObserver;-><init>(Lcom/android/server/inputmethod/StylusInputMethodSwitcher;Landroid/os/Handler;)V */
this.mSettingsObserver = v0;
/* .line 52 */
v1 = this.mService;
v1 = this.mSettings;
v1 = (( com.android.server.inputmethod.InputMethodUtils$InputMethodSettings ) v1 ).getCurrentUserId ( ); // invoke-virtual {v1}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getCurrentUserId()I
(( com.android.server.inputmethod.StylusInputMethodSwitcher$SettingsObserver ) v0 ).registerContentObserverLocked ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/inputmethod/StylusInputMethodSwitcher$SettingsObserver;->registerContentObserverLocked(I)V
/* .line 53 */
v0 = this.mService;
v0 = this.mContext;
/* .line 54 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v1 = this.mService;
v1 = this.mSettings;
/* .line 56 */
v1 = (( com.android.server.inputmethod.InputMethodUtils$InputMethodSettings ) v1 ).getCurrentUserId ( ); // invoke-virtual {v1}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getCurrentUserId()I
/* .line 53 */
/* const-string/jumbo v2, "stylus_handwriting_enable" */
int v3 = 1; // const/4 v3, 0x1
v0 = android.provider.Settings$System .getIntForUser ( v0,v2,v3,v1 );
if ( v0 != null) { // if-eqz v0, :cond_1
} // :cond_1
int v3 = 0; // const/4 v3, 0x0
} // :goto_0
/* iput-boolean v3, p0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->mStylusEnabled:Z */
/* .line 57 */
return;
} // .end method
public void removeMethod ( java.util.List p0 ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 197 */
/* .local p1, "imList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;>;" */
/* sget-boolean v0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->SUPPORT_STYLUS_INPUT_METHOD:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
v0 = if ( p1 != null) { // if-eqz p1, :cond_3
/* if-nez v0, :cond_0 */
/* .line 200 */
} // :cond_0
/* .line 201 */
/* .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;>;" */
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 202 */
/* check-cast v1, Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem; */
/* .line 203 */
/* .local v1, "imeSubtypeListItem":Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem; */
v2 = this.mImi;
/* .line 204 */
/* .local v2, "imi":Landroid/view/inputmethod/InputMethodInfo; */
(( android.view.inputmethod.InputMethodInfo ) v2 ).getPackageName ( ); // invoke-virtual {v2}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;
final String v4 = "com.miui.handwriting"; // const-string v4, "com.miui.handwriting"
v3 = android.text.TextUtils .equals ( v3,v4 );
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 205 */
/* .line 207 */
} // .end local v1 # "imeSubtypeListItem":Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;
} // .end local v2 # "imi":Landroid/view/inputmethod/InputMethodInfo;
} // :cond_1
/* .line 208 */
} // :cond_2
return;
/* .line 198 */
} // .end local v0 # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;>;"
} // :cond_3
} // :goto_1
return;
} // .end method
public Boolean shouldHideImeSwitcherLocked ( ) {
/* .locals 1 */
/* .line 189 */
/* sget-boolean v0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->SUPPORT_STYLUS_INPUT_METHOD:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v0 ).getSelectedMethodIdLocked ( ); // invoke-virtual {v0}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->isStylusMethodLocked(Ljava/lang/String;)Z */
/* if-nez v0, :cond_1 */
/* .line 190 */
} // :cond_0
com.android.server.inputmethod.InputMethodManagerServiceImpl .getInstance ( );
v0 = this.mBindingController;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 191 */
com.android.server.inputmethod.InputMethodManagerServiceImpl .getInstance ( );
v0 = this.mBindingController;
(( com.android.server.inputmethod.InputMethodBindingController ) v0 ).getCurMethod ( ); // invoke-virtual {v0}, Lcom/android/server/inputmethod/InputMethodBindingController;->getCurMethod()Lcom/android/server/inputmethod/IInputMethodInvoker;
/* if-nez v0, :cond_2 */
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
/* .line 189 */
} // :goto_0
} // .end method
