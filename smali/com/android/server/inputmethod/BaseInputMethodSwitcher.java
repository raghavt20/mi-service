public class com.android.server.inputmethod.BaseInputMethodSwitcher implements com.android.server.inputmethod.MiuiInputMethodStub {
	 /* .source "BaseInputMethodSwitcher.java" */
	 /* # interfaces */
	 /* # static fields */
	 public static final Integer NUMBER_PASSWORD;
	 public static final Integer TEXT_MASK;
	 public static final Integer TEXT_PASSWORD;
	 public static final Integer TEXT_VISIBLE_PASSWORD;
	 public static final Integer TEXT_WEB_PASSWORD;
	 public static final Integer WEB_EDIT_TEXT;
	 /* # instance fields */
	 protected com.android.server.inputmethod.InputMethodManagerService mService;
	 /* # direct methods */
	 public com.android.server.inputmethod.BaseInputMethodSwitcher ( ) {
		 /* .locals 0 */
		 /* .line 6 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 protected static Boolean isPasswdInputType ( Integer p0 ) {
		 /* .locals 4 */
		 /* .param p0, "inputType" # I */
		 /* .line 26 */
		 /* and-int/lit16 v0, p0, 0xa0 */
		 int v1 = 0; // const/4 v1, 0x0
		 int v2 = 1; // const/4 v2, 0x1
		 /* const/16 v3, 0xa0 */
		 /* if-ne v0, v3, :cond_1 */
		 /* .line 27 */
		 /* and-int/lit16 v0, p0, 0xfff */
		 /* const/16 v3, 0xe1 */
		 /* if-ne v0, v3, :cond_0 */
		 /* move v1, v2 */
	 } // :cond_0
	 /* .line 29 */
} // :cond_1
/* and-int/lit16 v0, p0, 0xfff */
/* const/16 v3, 0x81 */
/* if-eq v0, v3, :cond_2 */
/* and-int/lit16 v0, p0, 0xfff */
/* const/16 v3, 0x91 */
/* if-eq v0, v3, :cond_2 */
/* and-int/lit16 v0, p0, 0xfff */
/* const/16 v3, 0x12 */
/* if-ne v0, v3, :cond_3 */
} // :cond_2
/* move v1, v2 */
} // :cond_3
} // .end method
/* # virtual methods */
public void init ( com.android.server.inputmethod.InputMethodManagerService p0 ) {
/* .locals 0 */
/* .param p1, "service" # Lcom/android/server/inputmethod/InputMethodManagerService; */
/* .line 22 */
this.mService = p1;
/* .line 23 */
return;
} // .end method
