class com.android.server.inputmethod.InputMethodManagerServiceImpl$3 implements android.view.View$OnApplyWindowInsetsListener {
	 /* .source "InputMethodManagerServiceImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->setDialogImmersive(Lmiuix/appcompat/app/AlertDialog;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.inputmethod.InputMethodManagerServiceImpl this$0; //synthetic
final android.view.View val$decorView; //synthetic
/* # direct methods */
 com.android.server.inputmethod.InputMethodManagerServiceImpl$3 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/inputmethod/InputMethodManagerServiceImpl; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 463 */
this.this$0 = p1;
this.val$decorView = p2;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public android.view.WindowInsets onApplyWindowInsets ( android.view.View p0, android.view.WindowInsets p1 ) {
/* .locals 7 */
/* .param p1, "v" # Landroid/view/View; */
/* .param p2, "insets" # Landroid/view/WindowInsets; */
/* .line 466 */
/* nop */
/* .line 467 */
v0 = android.view.WindowInsets$Type .mandatorySystemGestures ( );
/* .line 466 */
(( android.view.WindowInsets ) p2 ).getInsets ( v0 ); // invoke-virtual {p2, v0}, Landroid/view/WindowInsets;->getInsets(I)Landroid/graphics/Insets;
/* .line 468 */
/* .local v0, "gestureInsets":Landroid/graphics/Insets; */
/* iget v1, v0, Landroid/graphics/Insets;->bottom:I */
/* .line 469 */
/* .local v1, "mHeight":I */
/* if-lez v1, :cond_0 */
/* .line 470 */
v2 = this.val$decorView;
v3 = (( android.view.View ) v2 ).getPaddingStart ( ); // invoke-virtual {v2}, Landroid/view/View;->getPaddingStart()I
v4 = this.val$decorView;
/* .line 471 */
v4 = (( android.view.View ) v4 ).getPaddingTop ( ); // invoke-virtual {v4}, Landroid/view/View;->getPaddingTop()I
v5 = this.val$decorView;
v5 = (( android.view.View ) v5 ).getPaddingEnd ( ); // invoke-virtual {v5}, Landroid/view/View;->getPaddingEnd()I
v6 = this.val$decorView;
/* .line 472 */
v6 = (( android.view.View ) v6 ).getPaddingBottom ( ); // invoke-virtual {v6}, Landroid/view/View;->getPaddingBottom()I
/* add-int/2addr v6, v1 */
/* .line 470 */
(( android.view.View ) v2 ).setPaddingRelative ( v3, v4, v5, v6 ); // invoke-virtual {v2, v3, v4, v5, v6}, Landroid/view/View;->setPaddingRelative(IIII)V
/* .line 475 */
} // :cond_0
v2 = this.val$decorView;
int v3 = 0; // const/4 v3, 0x0
(( android.view.View ) v2 ).setOnApplyWindowInsetsListener ( v3 ); // invoke-virtual {v2, v3}, Landroid/view/View;->setOnApplyWindowInsetsListener(Landroid/view/View$OnApplyWindowInsetsListener;)V
/* .line 476 */
v2 = android.view.WindowInsets.CONSUMED;
} // .end method
