class com.android.server.inputmethod.StylusInputMethodSwitcher$SettingsObserver extends android.database.ContentObserver {
	 /* .source "StylusInputMethodSwitcher.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/inputmethod/StylusInputMethodSwitcher; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "SettingsObserver" */
} // .end annotation
/* # instance fields */
Boolean mRegistered;
Integer mUserId;
final com.android.server.inputmethod.StylusInputMethodSwitcher this$0; //synthetic
/* # direct methods */
 com.android.server.inputmethod.StylusInputMethodSwitcher$SettingsObserver ( ) {
/* .locals 1 */
/* .param p1, "this$0" # Lcom/android/server/inputmethod/StylusInputMethodSwitcher; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 230 */
this.this$0 = p1;
/* .line 231 */
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
/* .line 228 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher$SettingsObserver;->mRegistered:Z */
/* .line 232 */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 7 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 256 */
/* const-string/jumbo v0, "stylus_handwriting_enable" */
android.provider.Settings$System .getUriFor ( v0 );
/* .line 257 */
/* .local v0, "stylusIMEUri":Landroid/net/Uri; */
v1 = this.this$0;
v1 = this.mService;
v1 = this.mMethodMap;
/* monitor-enter v1 */
/* .line 258 */
try { // :try_start_0
	 v2 = 	 (( android.net.Uri ) v0 ).equals ( p2 ); // invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
	 if ( v2 != null) { // if-eqz v2, :cond_1
		 /* .line 259 */
		 v2 = this.this$0;
		 v3 = this.mService;
		 v3 = this.mContext;
		 /* .line 260 */
		 (( android.content.Context ) v3 ).getContentResolver ( ); // invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
		 /* const-string/jumbo v4, "stylus_handwriting_enable" */
		 v5 = this.this$0;
		 v5 = this.mService;
		 v5 = this.mSettings;
		 /* .line 262 */
		 v5 = 		 (( com.android.server.inputmethod.InputMethodUtils$InputMethodSettings ) v5 ).getCurrentUserId ( ); // invoke-virtual {v5}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getCurrentUserId()I
		 /* .line 259 */
		 int v6 = 1; // const/4 v6, 0x1
		 v3 = 		 android.provider.Settings$System .getIntForUser ( v3,v4,v6,v5 );
		 if ( v3 != null) { // if-eqz v3, :cond_0
		 } // :cond_0
		 int v6 = 0; // const/4 v6, 0x0
	 } // :goto_0
	 com.android.server.inputmethod.StylusInputMethodSwitcher .-$$Nest$fputmStylusEnabled ( v2,v6 );
	 /* .line 263 */
	 v2 = this.this$0;
	 com.android.server.inputmethod.StylusInputMethodSwitcher .-$$Nest$mupdateFromSettingsLocked ( v2 );
	 /* .line 265 */
	 final String v2 = "StylusInputMethodSwitcher"; // const-string v2, "StylusInputMethodSwitcher"
	 /* new-instance v3, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v4 = "enable status change: "; // const-string v4, "enable status change: "
	 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v4 = this.this$0;
	 v4 = 	 com.android.server.inputmethod.StylusInputMethodSwitcher .-$$Nest$fgetmStylusEnabled ( v4 );
	 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .d ( v2,v3 );
	 /* .line 268 */
} // :cond_1
/* monitor-exit v1 */
/* .line 269 */
return;
/* .line 268 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
void registerContentObserverLocked ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "userId" # I */
/* .line 235 */
v0 = com.android.server.inputmethod.StylusInputMethodSwitcher .-$$Nest$sfgetSUPPORT_STYLUS_INPUT_METHOD ( );
/* if-nez v0, :cond_0 */
/* .line 236 */
return;
/* .line 238 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher$SettingsObserver;->mRegistered:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget v0, p0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher$SettingsObserver;->mUserId:I */
/* if-ne v0, p1, :cond_1 */
/* .line 239 */
return;
/* .line 241 */
} // :cond_1
v0 = this.this$0;
v0 = this.mService;
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 242 */
/* .local v0, "resolver":Landroid/content/ContentResolver; */
/* iget-boolean v1, p0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher$SettingsObserver;->mRegistered:Z */
int v2 = 0; // const/4 v2, 0x0
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 243 */
(( android.content.ContentResolver ) v0 ).unregisterContentObserver ( p0 ); // invoke-virtual {v0, p0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V
/* .line 244 */
/* iput-boolean v2, p0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher$SettingsObserver;->mRegistered:Z */
/* .line 246 */
} // :cond_2
/* iget v1, p0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher$SettingsObserver;->mUserId:I */
/* if-eq v1, p1, :cond_3 */
/* .line 247 */
/* iput p1, p0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher$SettingsObserver;->mUserId:I */
/* .line 249 */
} // :cond_3
/* const-string/jumbo v1, "stylus_handwriting_enable" */
android.provider.Settings$System .getUriFor ( v1 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, p1 ); // invoke-virtual {v0, v1, v2, p0, p1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 251 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher$SettingsObserver;->mRegistered:Z */
/* .line 252 */
return;
} // .end method
