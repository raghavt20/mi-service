public class com.android.server.inputmethod.InputMethodMonitor {
	 /* .source "InputMethodMonitor.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/inputmethod/InputMethodMonitor$SINGLETON;, */
	 /* Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String DUMP_DIR;
static final Integer LEAK_LOW_WATERMARK;
private static final java.lang.String TAG;
/* # instance fields */
private final java.util.ArrayList mCache;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/ref/WeakReference<", */
/* "Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
private Integer mHighWatermark;
private final java.util.WeakHashMap mRecord;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/WeakHashMap<", */
/* "Lcom/android/internal/inputmethod/IInputMethodSessionCallback$Stub;", */
/* "Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
Boolean mWarning;
/* # direct methods */
private com.android.server.inputmethod.InputMethodMonitor ( ) {
/* .locals 1 */
/* .line 70 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 52 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/inputmethod/InputMethodMonitor;->mHighWatermark:I */
/* .line 57 */
/* new-instance v0, Ljava/util/WeakHashMap; */
/* invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V */
this.mRecord = v0;
/* .line 59 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mCache = v0;
/* .line 71 */
return;
} // .end method
 com.android.server.inputmethod.InputMethodMonitor ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/inputmethod/InputMethodMonitor;-><init>()V */
return;
} // .end method
private void doAppDeath ( Boolean p0 ) {
/* .locals 9 */
/* .param p1, "inTest" # Z */
/* .line 139 */
(( com.android.server.inputmethod.InputMethodMonitor ) p0 ).findTheEvil ( ); // invoke-virtual {p0}, Lcom/android/server/inputmethod/InputMethodMonitor;->findTheEvil()Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;
/* .line 141 */
/* .local v0, "evil":Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo; */
if ( v0 != null) { // if-eqz v0, :cond_4
if ( p1 != null) { // if-eqz p1, :cond_0
/* goto/16 :goto_0 */
/* .line 144 */
} // :cond_0
/* const-class v1, Landroid/content/pm/PackageManagerInternal; */
com.android.server.LocalServices .getService ( v1 );
/* check-cast v1, Landroid/content/pm/PackageManagerInternal; */
/* .line 145 */
/* .local v1, "pm":Landroid/content/pm/PackageManagerInternal; */
/* new-instance v2, Landroid/content/Intent; */
/* invoke-direct {v2}, Landroid/content/Intent;-><init>()V */
/* move-object v8, v2 */
/* .line 146 */
/* .local v8, "intent":Landroid/content/Intent; */
v2 = this.component;
(( android.content.Intent ) v8 ).setComponent ( v2 ); // invoke-virtual {v8, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 147 */
/* const-wide/16 v4, 0x0 */
int v6 = 0; // const/4 v6, 0x0
/* iget v7, v0, Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;->userId:I */
/* move-object v2, v1 */
/* move-object v3, v8 */
/* invoke-virtual/range {v2 ..v7}, Landroid/content/pm/PackageManagerInternal;->queryIntentServices(Landroid/content/Intent;JII)Ljava/util/List; */
/* .line 150 */
v3 = /* .local v2, "serviceInfos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;" */
int v4 = 1; // const/4 v4, 0x1
/* if-eq v3, v4, :cond_1 */
/* .line 151 */
return;
/* .line 153 */
} // :cond_1
int v3 = 0; // const/4 v3, 0x0
/* check-cast v4, Landroid/content/pm/ResolveInfo; */
v4 = this.serviceInfo;
v4 = this.processName;
/* .line 154 */
/* .local v4, "process":Ljava/lang/String; */
v5 = this.component;
(( android.content.ComponentName ) v5 ).getPackageName ( ); // invoke-virtual {v5}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 155 */
/* .local v5, "packageName":Ljava/lang/String; */
/* if-nez v4, :cond_2 */
/* move-object v4, v5 */
/* .line 156 */
} // :cond_2
v6 = miui.mqsas.scout.ScoutUtils .isLibraryTest ( );
if ( v6 != null) { // if-eqz v6, :cond_3
/* .line 157 */
com.android.server.inputmethod.InputMethodMonitor .dumpBacktrace ( v4 );
/* .line 158 */
com.android.server.inputmethod.InputMethodMonitor .dumpHprof ( v0,v4 );
/* .line 161 */
} // :cond_3
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Too many createSession requests unhandled, killing "; // const-string v7, "Too many createSession requests unhandled, killing "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = ", userId="; // const-string v7, ", userId="
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, v0, Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;->userId:I */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v7 = "InputMethodMonitor"; // const-string v7, "InputMethodMonitor"
android.util.Log .i ( v7,v6 );
/* .line 163 */
/* const-class v6, Landroid/app/ActivityManagerInternal; */
com.android.server.LocalServices .getService ( v6 );
/* check-cast v6, Landroid/app/ActivityManagerInternal; */
/* .line 164 */
/* .local v6, "am":Landroid/app/ActivityManagerInternal; */
/* check-cast v3, Landroid/content/pm/ResolveInfo; */
v3 = this.serviceInfo;
v3 = this.applicationInfo;
/* iget v3, v3, Landroid/content/pm/ApplicationInfo;->uid:I */
final String v7 = "ime_hang"; // const-string v7, "ime_hang"
(( android.app.ActivityManagerInternal ) v6 ).killProcess ( v4, v3, v7 ); // invoke-virtual {v6, v4, v3, v7}, Landroid/app/ActivityManagerInternal;->killProcess(Ljava/lang/String;ILjava/lang/String;)V
/* .line 165 */
return;
/* .line 142 */
} // .end local v1 # "pm":Landroid/content/pm/PackageManagerInternal;
} // .end local v2 # "serviceInfos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
} // .end local v4 # "process":Ljava/lang/String;
} // .end local v5 # "packageName":Ljava/lang/String;
} // .end local v6 # "am":Landroid/app/ActivityManagerInternal;
} // .end local v8 # "intent":Landroid/content/Intent;
} // :cond_4
} // :goto_0
return;
} // .end method
private static void dumpBacktrace ( java.lang.String p0 ) {
/* .locals 7 */
/* .param p0, "process" # Ljava/lang/String; */
/* .line 198 */
final String v0 = "/data/miuilog/stability/resleak/fdtrack/"; // const-string v0, "/data/miuilog/stability/resleak/fdtrack/"
miui.mqsas.scout.ScoutUtils .ensureDumpDir ( v0 );
/* .line 199 */
int v1 = 2; // const/4 v1, 0x2
final String v2 = ".trace"; // const-string v2, ".trace"
miui.mqsas.scout.ScoutUtils .removeHistoricalDumps ( v0,v2,v1 );
/* .line 200 */
/* new-instance v1, Ljava/text/SimpleDateFormat; */
/* const-string/jumbo v3, "yyyy_MM_dd_HH_mm_ss" */
v4 = java.util.Locale.US;
/* invoke-direct {v1, v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V */
/* .line 201 */
/* .local v1, "dateFormat":Ljava/text/SimpleDateFormat; */
/* new-instance v3, Ljava/util/Date; */
/* invoke-direct {v3}, Ljava/util/Date;-><init>()V */
(( java.text.SimpleDateFormat ) v1 ).format ( v3 ); // invoke-virtual {v1, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
/* .line 202 */
/* .local v3, "datetime":Ljava/lang/String; */
/* new-instance v4, Ljava/io/File; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "inputmethod_hang_"; // const-string v6, "inputmethod_hang_"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( p0 ); // invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = "_"; // const-string v6, "_"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v4, v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* move-object v0, v4 */
/* .line 203 */
/* .local v0, "traceFile":Ljava/io/File; */
/* filled-new-array {p0}, [Ljava/lang/String; */
android.os.Process .getPidsForCommands ( v2 );
/* .line 204 */
/* .local v2, "pids":[I */
if ( v2 != null) { // if-eqz v2, :cond_2
/* array-length v4, v2 */
/* if-nez v4, :cond_0 */
/* .line 208 */
} // :cond_0
com.android.server.am.ActivityManagerServiceStub .get ( );
(( com.android.server.am.ActivityManagerServiceStub ) v4 ).dumpMiuiStackTraces ( v2 ); // invoke-virtual {v4, v2}, Lcom/android/server/am/ActivityManagerServiceStub;->dumpMiuiStackTraces([I)Ljava/lang/String;
/* .line 209 */
/* .local v4, "path":Ljava/lang/String; */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 210 */
/* new-instance v5, Ljava/io/File; */
/* invoke-direct {v5, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 211 */
/* .local v5, "file":Ljava/io/File; */
(( java.io.File ) v5 ).renameTo ( v0 ); // invoke-virtual {v5, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z
/* .line 213 */
} // .end local v5 # "file":Ljava/io/File;
} // :cond_1
return;
/* .line 205 */
} // .end local v4 # "path":Ljava/lang/String;
} // :cond_2
} // :goto_0
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "failed to dump trace of "; // const-string v5, "failed to dump trace of "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p0 ); // invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = ": process disappeared"; // const-string v5, ": process disappeared"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "InputMethodMonitor"; // const-string v5, "InputMethodMonitor"
android.util.Log .d ( v5,v4 );
/* .line 206 */
return;
} // .end method
private static void dumpHprof ( com.android.server.inputmethod.InputMethodMonitor$InputMethodInfo p0, java.lang.String p1 ) {
/* .locals 17 */
/* .param p0, "app" # Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo; */
/* .param p1, "process" # Ljava/lang/String; */
/* .line 217 */
/* move-object/from16 v10, p1 */
final String v11 = "failed to dump hprof of "; // const-string v11, "failed to dump hprof of "
final String v12 = "InputMethodMonitor"; // const-string v12, "InputMethodMonitor"
/* new-instance v0, Ljava/io/File; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "inputmethod_"; // const-string v2, "inputmethod_"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v10 ); // invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ".hprof"; // const-string v2, ".hprof"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "/data/miuilog/stability/resleak/fdtrack/"; // const-string v2, "/data/miuilog/stability/resleak/fdtrack/"
/* invoke-direct {v0, v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* move-object v13, v0 */
/* .line 218 */
/* .local v13, "heapFile":Ljava/io/File; */
/* new-instance v0, Ljava/util/concurrent/CountDownLatch; */
int v1 = 1; // const/4 v1, 0x1
/* invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V */
/* move-object v14, v0 */
/* .line 219 */
/* .local v14, "latch":Ljava/util/concurrent/CountDownLatch; */
/* new-instance v0, Ljava/io/File; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "/data/data/android/cache/"; // const-string v2, "/data/data/android/cache/"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.File ) v13 ).getName ( ); // invoke-virtual {v13}, Ljava/io/File;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ".tmp"; // const-string v2, ".tmp"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* move-object v15, v0 */
/* .line 222 */
/* .local v15, "temp":Ljava/io/File; */
/* const/high16 v0, 0x38000000 */
/* .line 223 */
/* .local v0, "mode":I */
try { // :try_start_0
android.os.ParcelFileDescriptor .open ( v15,v0 );
/* .line 224 */
/* .local v8, "fd":Landroid/os/ParcelFileDescriptor; */
android.app.ActivityManager .getService ( );
/* move-object/from16 v9, p0 */
/* iget v3, v9, Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;->userId:I */
int v4 = 1; // const/4 v4, 0x1
int v5 = 0; // const/4 v5, 0x0
/* .line 226 */
(( java.io.File ) v15 ).getAbsolutePath ( ); // invoke-virtual {v15}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
/* new-instance v2, Landroid/os/RemoteCallback; */
/* new-instance v6, Lcom/android/server/inputmethod/InputMethodMonitor$$ExternalSyntheticLambda1; */
/* invoke-direct {v6, v14}, Lcom/android/server/inputmethod/InputMethodMonitor$$ExternalSyntheticLambda1;-><init>(Ljava/util/concurrent/CountDownLatch;)V */
/* invoke-direct {v2, v6}, Landroid/os/RemoteCallback;-><init>(Landroid/os/RemoteCallback$OnResultListener;)V */
/* .line 224 */
/* move-object/from16 v16, v2 */
/* move-object/from16 v2, p1 */
int v6 = 0; // const/4 v6, 0x0
/* move-object/from16 v9, v16 */
v1 = /* invoke-interface/range {v1 ..v9}, Landroid/app/IActivityManager;->dumpHeap(Ljava/lang/String;IZZZLjava/lang/String;Landroid/os/ParcelFileDescriptor;Landroid/os/RemoteCallback;)Z */
/* .line 230 */
/* .local v1, "dumping":Z */
if ( v1 != null) { // if-eqz v1, :cond_2
v2 = java.util.concurrent.TimeUnit.SECONDS;
/* const-wide/16 v3, 0x3c */
v2 = (( java.util.concurrent.CountDownLatch ) v14 ).await ( v3, v4, v2 ); // invoke-virtual {v14, v3, v4, v2}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
/* if-nez v2, :cond_0 */
/* .line 234 */
} // :cond_0
(( java.io.File ) v15 ).length ( ); // invoke-virtual {v15}, Ljava/io/File;->length()J
/* move-result-wide v2 */
/* const-wide/16 v4, 0x0 */
/* cmp-long v2, v2, v4 */
/* if-lez v2, :cond_1 */
/* .line 237 */
android.os.FileUtils .copy ( v15,v13 );
/* .line 238 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "hprof saved to "; // const-string v3, "hprof saved to "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v13 ); // invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v12,v2 );
/* .line 243 */
} // .end local v0 # "mode":I
} // .end local v1 # "dumping":Z
} // .end local v8 # "fd":Landroid/os/ParcelFileDescriptor;
} // :cond_1
/* .line 231 */
/* .restart local v0 # "mode":I */
/* .restart local v1 # "dumping":Z */
/* .restart local v8 # "fd":Landroid/os/ParcelFileDescriptor; */
} // :cond_2
} // :goto_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v11 ); // invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v10 ); // invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = ": timeout"; // const-string v3, ": timeout"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .w ( v12,v2 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 243 */
(( java.io.File ) v15 ).delete ( ); // invoke-virtual {v15}, Ljava/io/File;->delete()Z
/* .line 232 */
return;
/* .line 243 */
} // .end local v0 # "mode":I
} // .end local v1 # "dumping":Z
} // .end local v8 # "fd":Landroid/os/ParcelFileDescriptor;
/* :catchall_0 */
/* move-exception v0 */
/* .line 240 */
/* :catch_0 */
/* move-exception v0 */
/* .line 241 */
/* .local v0, "e":Ljava/lang/Exception; */
try { // :try_start_1
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v11 ); // invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v10 ); // invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ": "; // const-string v2, ": "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v0 ).getMessage ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .w ( v12,v1 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 243 */
/* nop */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
(( java.io.File ) v15 ).delete ( ); // invoke-virtual {v15}, Ljava/io/File;->delete()Z
/* .line 244 */
/* nop */
/* .line 245 */
return;
/* .line 243 */
} // :goto_2
(( java.io.File ) v15 ).delete ( ); // invoke-virtual {v15}, Ljava/io/File;->delete()Z
/* .line 244 */
/* throw v0 */
} // .end method
private void expungeStaleItems ( ) {
/* .locals 2 */
/* .line 94 */
v0 = this.mCache;
v0 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* add-int/lit8 v0, v0, -0x1 */
/* .local v0, "i":I */
} // :goto_0
/* if-ltz v0, :cond_1 */
/* .line 95 */
v1 = this.mCache;
(( java.util.ArrayList ) v1 ).get ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/ref/WeakReference; */
(( java.lang.ref.WeakReference ) v1 ).get ( ); // invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
/* if-nez v1, :cond_0 */
/* .line 96 */
v1 = this.mCache;
(( java.util.ArrayList ) v1 ).remove ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;
/* .line 94 */
} // :cond_0
/* add-int/lit8 v0, v0, -0x1 */
/* .line 99 */
} // .end local v0 # "i":I
} // :cond_1
return;
} // .end method
private com.android.server.inputmethod.InputMethodMonitor$InputMethodInfo getInputMethodInfo ( Integer p0, android.content.ComponentName p1 ) {
/* .locals 4 */
/* .param p1, "userId" # I */
/* .param p2, "componentName" # Landroid/content/ComponentName; */
/* .line 103 */
v0 = this.mCache;
(( java.util.ArrayList ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Ljava/lang/ref/WeakReference; */
/* .line 104 */
/* .local v1, "ref":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;>;" */
(( java.lang.ref.WeakReference ) v1 ).get ( ); // invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
/* check-cast v2, Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo; */
/* .line 105 */
/* .local v2, "info":Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo; */
if ( v2 != null) { // if-eqz v2, :cond_0
v3 = this.component;
v3 = (( android.content.ComponentName ) v3 ).equals ( p2 ); // invoke-virtual {v3, p2}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* iget v3, v2, Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;->userId:I */
/* if-ne v3, p1, :cond_0 */
/* .line 106 */
/* .line 108 */
} // .end local v1 # "ref":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;>;"
} // .end local v2 # "info":Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;
} // :cond_0
/* .line 109 */
} // :cond_1
/* new-instance v0, Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo; */
/* invoke-direct {v0, p2, p1}, Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;-><init>(Landroid/content/ComponentName;I)V */
/* .line 110 */
/* .local v0, "info":Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo; */
v1 = this.mCache;
/* new-instance v2, Ljava/lang/ref/WeakReference; */
/* invoke-direct {v2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V */
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 111 */
} // .end method
static com.android.server.inputmethod.InputMethodMonitor getInstance ( ) {
/* .locals 1 */
/* .line 63 */
com.android.server.inputmethod.InputMethodMonitor$SINGLETON .-$$Nest$sfgetINSTANCE ( );
} // .end method
static void lambda$dumpHprof$0 ( java.util.concurrent.CountDownLatch p0, android.os.Bundle p1 ) { //synthethic
/* .locals 0 */
/* .param p0, "latch" # Ljava/util/concurrent/CountDownLatch; */
/* .param p1, "result" # Landroid/os/Bundle; */
/* .line 227 */
(( java.util.concurrent.CountDownLatch ) p0 ).countDown ( ); // invoke-virtual {p0}, Ljava/util/concurrent/CountDownLatch;->countDown()V
/* .line 228 */
return;
} // .end method
/* # virtual methods */
public synchronized void enable ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "highWatermark" # I */
/* monitor-enter p0 */
/* .line 67 */
/* const/16 v0, 0xc8 */
try { // :try_start_0
v0 = java.lang.Math .max ( p1,v0 );
/* iput v0, p0, Lcom/android/server/inputmethod/InputMethodMonitor;->mHighWatermark:I */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 68 */
/* monitor-exit p0 */
return;
/* .line 66 */
} // .end local p0 # "this":Lcom/android/server/inputmethod/InputMethodMonitor;
} // .end local p1 # "highWatermark":I
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
com.android.server.inputmethod.InputMethodMonitor$InputMethodInfo findTheEvil ( ) {
/* .locals 8 */
/* .line 170 */
/* new-instance v0, Landroid/util/ArraySet; */
/* invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V */
/* .line 171 */
/* .local v0, "infos":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;>;" */
/* monitor-enter p0 */
/* .line 172 */
try { // :try_start_0
v1 = this.mRecord;
(( java.util.WeakHashMap ) v1 ).entrySet ( ); // invoke-virtual {v1}, Ljava/util/WeakHashMap;->entrySet()Ljava/util/Set;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Ljava/util/Map$Entry; */
/* .line 173 */
/* .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/android/internal/inputmethod/IInputMethodSessionCallback$Stub;Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;>;" */
/* check-cast v3, Lcom/android/internal/inputmethod/IInputMethodSessionCallback$Stub; */
/* .line 174 */
/* .local v3, "k":Lcom/android/internal/inputmethod/IInputMethodSessionCallback$Stub; */
/* check-cast v4, Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo; */
/* .line 175 */
/* .local v4, "v":Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo; */
if ( v3 != null) { // if-eqz v3, :cond_1
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 176 */
v5 = (( android.util.ArraySet ) v0 ).add ( v4 ); // invoke-virtual {v0, v4}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 177 */
int v5 = 0; // const/4 v5, 0x0
/* iput v5, v4, Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;->count_:I */
/* .line 179 */
} // :cond_0
/* iget v5, v4, Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;->count_:I */
/* add-int/lit8 v5, v5, 0x1 */
/* iput v5, v4, Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;->count_:I */
/* .line 181 */
} // .end local v2 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/android/internal/inputmethod/IInputMethodSessionCallback$Stub;Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;>;"
} // .end local v3 # "k":Lcom/android/internal/inputmethod/IInputMethodSessionCallback$Stub;
} // .end local v4 # "v":Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;
} // :cond_1
/* .line 182 */
} // :cond_2
/* monitor-exit p0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 183 */
int v1 = 0; // const/4 v1, 0x0
/* .line 184 */
/* .local v1, "evil":Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo; */
/* const/16 v2, 0x64 */
/* .line 185 */
/* .local v2, "maxCount":I */
(( android.util.ArraySet ) v0 ).iterator ( ); // invoke-virtual {v0}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;
v4 = } // :goto_1
if ( v4 != null) { // if-eqz v4, :cond_4
/* check-cast v4, Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo; */
/* .line 186 */
/* .local v4, "app":Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo; */
final String v5 = "InputMethodMonitor"; // const-string v5, "InputMethodMonitor"
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
/* iget v7, v4, Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;->count_:I */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = " createSession requests sent to "; // const-string v7, " createSession requests sent to "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = this.component;
/* .line 187 */
(( android.content.ComponentName ) v7 ).getPackageName ( ); // invoke-virtual {v7}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 186 */
android.util.Log .i ( v5,v6 );
/* .line 188 */
/* iget v5, v4, Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;->count_:I */
/* if-le v5, v2, :cond_3 */
/* .line 189 */
/* iget v2, v4, Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;->count_:I */
/* .line 190 */
/* move-object v1, v4 */
/* .line 192 */
} // .end local v4 # "app":Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;
} // :cond_3
/* .line 193 */
} // :cond_4
/* .line 182 */
} // .end local v1 # "evil":Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;
} // .end local v2 # "maxCount":I
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit p0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
public synchronized void finishCreateSession ( com.android.internal.inputmethod.IInputMethodSessionCallback$Stub p0 ) {
/* .locals 1 */
/* .param p1, "callback" # Lcom/android/internal/inputmethod/IInputMethodSessionCallback$Stub; */
/* monitor-enter p0 */
/* .line 87 */
try { // :try_start_0
/* iget v0, p0, Lcom/android/server/inputmethod/InputMethodMonitor;->mHighWatermark:I */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* if-nez v0, :cond_0 */
/* monitor-exit p0 */
return;
/* .line 88 */
} // :cond_0
try { // :try_start_1
/* invoke-direct {p0}, Lcom/android/server/inputmethod/InputMethodMonitor;->expungeStaleItems()V */
/* .line 89 */
v0 = this.mRecord;
(( java.util.WeakHashMap ) v0 ).remove ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 90 */
/* monitor-exit p0 */
return;
/* .line 86 */
} // .end local p0 # "this":Lcom/android/server/inputmethod/InputMethodMonitor;
} // .end local p1 # "callback":Lcom/android/internal/inputmethod/IInputMethodSessionCallback$Stub;
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
void scheduleAppDeath ( ) {
/* .locals 8 */
/* .line 117 */
v0 = android.app.ActivityThread .isSystem ( );
/* xor-int/lit8 v0, v0, 0x1 */
/* .line 118 */
/* .local v0, "inTest":Z */
/* invoke-direct {p0, v0}, Lcom/android/server/inputmethod/InputMethodMonitor;->doAppDeath(Z)V */
/* .line 119 */
/* const-wide/16 v1, 0x5 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* const-wide/16 v3, 0x1 */
} // :cond_0
v3 = java.util.concurrent.TimeUnit.SECONDS;
(( java.util.concurrent.TimeUnit ) v3 ).toMillis ( v1, v2 ); // invoke-virtual {v3, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J
/* move-result-wide v3 */
} // :goto_0
android.os.SystemClock .sleep ( v3,v4 );
/* .line 121 */
java.lang.System .gc ( );
/* .line 122 */
java.lang.System .runFinalization ( );
/* .line 123 */
/* monitor-enter p0 */
/* .line 124 */
int v3 = 0; // const/4 v3, 0x0
if ( v0 != null) { // if-eqz v0, :cond_1
/* move v4, v3 */
} // :cond_1
/* const/16 v4, 0xa */
/* .line 125 */
/* .local v4, "waitCount":I */
} // :goto_1
try { // :try_start_0
v5 = this.mRecord;
v5 = (( java.util.WeakHashMap ) v5 ).size ( ); // invoke-virtual {v5}, Ljava/util/WeakHashMap;->size()I
/* iget v6, p0, Lcom/android/server/inputmethod/InputMethodMonitor;->mHighWatermark:I */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* if-lt v5, v6, :cond_3 */
/* add-int/lit8 v5, v4, -0x1 */
} // .end local v4 # "waitCount":I
/* .local v5, "waitCount":I */
/* if-lez v4, :cond_2 */
/* .line 127 */
try { // :try_start_1
v4 = java.util.concurrent.TimeUnit.SECONDS;
(( java.util.concurrent.TimeUnit ) v4 ).toMillis ( v1, v2 ); // invoke-virtual {v4, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J
/* move-result-wide v6 */
(( java.lang.Object ) p0 ).wait ( v6, v7 ); // invoke-virtual {p0, v6, v7}, Ljava/lang/Object;->wait(J)V
/* :try_end_1 */
/* .catch Ljava/lang/InterruptedException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 130 */
/* move v4, v5 */
/* .line 128 */
/* :catch_0 */
/* move-exception v1 */
/* .line 129 */
/* .local v1, "e":Ljava/lang/InterruptedException; */
try { // :try_start_2
/* new-instance v2, Ljava/lang/RuntimeException; */
/* invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V */
} // .end local v0 # "inTest":Z
} // .end local p0 # "this":Lcom/android/server/inputmethod/InputMethodMonitor;
/* throw v2 */
/* .line 125 */
} // .end local v1 # "e":Ljava/lang/InterruptedException;
/* .restart local v0 # "inTest":Z */
/* .restart local p0 # "this":Lcom/android/server/inputmethod/InputMethodMonitor; */
} // :cond_2
/* move v4, v5 */
/* .line 133 */
} // .end local v5 # "waitCount":I
/* .restart local v4 # "waitCount":I */
} // :cond_3
/* iput-boolean v3, p0, Lcom/android/server/inputmethod/InputMethodMonitor;->mWarning:Z */
/* .line 134 */
} // .end local v4 # "waitCount":I
/* monitor-exit p0 */
/* .line 135 */
return;
/* .line 134 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit p0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
} // .end method
public synchronized void startCreateSession ( Integer p0, android.content.ComponentName p1, com.android.internal.inputmethod.IInputMethodSessionCallback$Stub p2 ) {
/* .locals 4 */
/* .param p1, "userId" # I */
/* .param p2, "componentName" # Landroid/content/ComponentName; */
/* .param p3, "callback" # Lcom/android/internal/inputmethod/IInputMethodSessionCallback$Stub; */
/* monitor-enter p0 */
/* .line 75 */
try { // :try_start_0
/* iget v0, p0, Lcom/android/server/inputmethod/InputMethodMonitor;->mHighWatermark:I */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* if-nez v0, :cond_0 */
/* monitor-exit p0 */
return;
/* .line 76 */
} // :cond_0
try { // :try_start_1
/* invoke-direct {p0}, Lcom/android/server/inputmethod/InputMethodMonitor;->expungeStaleItems()V */
/* .line 77 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/inputmethod/InputMethodMonitor;->getInputMethodInfo(ILandroid/content/ComponentName;)Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo; */
/* .line 78 */
/* .local v0, "info":Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo; */
v1 = this.mRecord;
(( java.util.WeakHashMap ) v1 ).put ( p3, v0 ); // invoke-virtual {v1, p3, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 79 */
v1 = this.mRecord;
v1 = (( java.util.WeakHashMap ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/WeakHashMap;->size()I
/* iget v2, p0, Lcom/android/server/inputmethod/InputMethodMonitor;->mHighWatermark:I */
/* if-ne v1, v2, :cond_1 */
/* iget-boolean v1, p0, Lcom/android/server/inputmethod/InputMethodMonitor;->mWarning:Z */
/* if-nez v1, :cond_1 */
/* .line 80 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/android/server/inputmethod/InputMethodMonitor;->mWarning:Z */
/* .line 81 */
/* new-instance v1, Ljava/lang/Thread; */
/* new-instance v2, Lcom/android/server/inputmethod/InputMethodMonitor$$ExternalSyntheticLambda0; */
/* invoke-direct {v2, p0}, Lcom/android/server/inputmethod/InputMethodMonitor$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/inputmethod/InputMethodMonitor;)V */
final String v3 = "InputMethodMonitor"; // const-string v3, "InputMethodMonitor"
/* invoke-direct {v1, v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V */
(( java.lang.Thread ) v1 ).start ( ); // invoke-virtual {v1}, Ljava/lang/Thread;->start()V
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 83 */
} // .end local p0 # "this":Lcom/android/server/inputmethod/InputMethodMonitor;
} // :cond_1
/* monitor-exit p0 */
return;
/* .line 74 */
} // .end local v0 # "info":Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;
} // .end local p1 # "userId":I
} // .end local p2 # "componentName":Landroid/content/ComponentName;
} // .end local p3 # "callback":Lcom/android/internal/inputmethod/IInputMethodSessionCallback$Stub;
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
