.class public Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;
.super Lcom/android/server/inputmethod/InputMethodManagerServiceStub;
.source "InputMethodManagerServiceImpl.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.inputmethod.InputMethodManagerServiceStub$$"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$MiuiInputMethodEventListener;
    }
.end annotation


# static fields
.field public static final DEBUG:Z = true

.field private static final IME_LIST_VIEW_VISIBLE_NUMBER:I = 0x3

.field private static final MIRROR_INPUT_STATE:Ljava/lang/String; = "mirror_input_state"

.field public static final MIUIXPACKAGE:Ljava/lang/String; = "miuix.stub"

.field public static final MIUI_HOME:Ljava/lang/String; = "com.miui.home"

.field public static final TAG:Ljava/lang/String; = "InputMethodManagerServiceImpl"

.field private static final customizedInputMethodList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private callStartInputOrWindowGainedFocusFlag:Z

.field dialogContext:Landroid/content/Context;

.field private isExpanded:Z

.field mBindingController:Lcom/android/server/inputmethod/InputMethodBindingController;

.field mContext:Landroid/content/Context;

.field mHandler:Landroid/os/Handler;

.field private mIsFromRemoteDevice:Z

.field private volatile mLastAcceptStatus:I

.field private mMirrorDisplayId:I

.field private mMirrorInputState:Z

.field private mMiuiInputMethodEventListener:Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$MiuiInputMethodEventListener;

.field private mMonitorBinder:Landroid/os/IBinder;

.field public mNoCustomizedIms:[Landroid/view/inputmethod/InputMethodInfo;

.field public mNoCustomizedSubtypeIds:[I

.field private mObserver:Landroid/database/ContentObserver;

.field private mScreenOffLastTime:Z

.field private mScreenStatus:Z

.field private mSessionId:I

.field private volatile mSynergyOperate:I

.field public noCustomizedCheckedItem:I


# direct methods
.method static bridge synthetic -$$Nest$fgetisExpanded(Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->isExpanded:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsFromRemoteDevice(Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mIsFromRemoteDevice:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputisExpanded(Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->isExpanded:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsFromRemoteDevice(Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mIsFromRemoteDevice:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmMirrorDisplayId(Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mMirrorDisplayId:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmMirrorInputState(Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mMirrorInputState:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$smsetNoCustomizedInputMethodListViewHeight(Landroid/widget/ListView;Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->setNoCustomizedInputMethodListViewHeight(Landroid/widget/ListView;Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 78
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->customizedInputMethodList:Ljava/util/List;

    .line 113
    const-string v1, "com.iflytek.inputmethod.miui"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 114
    const-string v1, "com.baidu.input_mi"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 115
    const-string v1, "com.sohu.inputmethod.sogou.xiaomi"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 117
    const-string v1, "com.android.cts.mockime"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 118
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 68
    invoke-direct {p0}, Lcom/android/server/inputmethod/InputMethodManagerServiceStub;-><init>()V

    .line 74
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mMonitorBinder:Landroid/os/IBinder;

    .line 75
    const/4 v1, 0x0

    iput v1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mSynergyOperate:I

    .line 76
    iput v1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mLastAcceptStatus:I

    .line 80
    const/4 v2, -0x1

    iput v2, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->noCustomizedCheckedItem:I

    .line 81
    iput-object v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mNoCustomizedIms:[Landroid/view/inputmethod/InputMethodInfo;

    .line 82
    iput-object v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mNoCustomizedSubtypeIds:[I

    .line 87
    iput-boolean v1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mScreenStatus:Z

    .line 91
    iput-boolean v1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->isExpanded:Z

    .line 92
    iput-boolean v1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mScreenOffLastTime:Z

    .line 93
    iput-boolean v1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->callStartInputOrWindowGainedFocusFlag:Z

    .line 96
    iput v1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mSessionId:I

    .line 102
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mMirrorInputState:Z

    .line 107
    iput v1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mMirrorDisplayId:I

    .line 108
    iput-boolean v1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mIsFromRemoteDevice:Z

    return-void
.end method

.method private static clearFitSystemWindow(Landroid/view/View;)V
    .locals 2
    .param p0, "view"    # Landroid/view/View;

    .line 487
    if-eqz p0, :cond_0

    .line 488
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setFitsSystemWindows(Z)V

    .line 489
    instance-of v0, p0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 490
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    move-object v1, p0

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 491
    move-object v1, p0

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v1}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->clearFitSystemWindow(Landroid/view/View;)V

    .line 490
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 495
    .end local v0    # "i":I
    :cond_0
    return-void
.end method

.method public static getInstance()Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;
    .locals 1

    .line 121
    invoke-static {}, Lcom/android/server/inputmethod/InputMethodManagerServiceStub;->getInstance()Lcom/android/server/inputmethod/InputMethodManagerServiceStub;

    move-result-object v0

    check-cast v0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    return-object v0
.end method

.method private registerPointerEventListener()V
    .locals 2

    .line 742
    iget-object v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    return-void

    .line 743
    :cond_0
    iget-object v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mMiuiInputMethodEventListener:Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$MiuiInputMethodEventListener;

    if-nez v0, :cond_1

    .line 744
    new-instance v0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$MiuiInputMethodEventListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$MiuiInputMethodEventListener;-><init>(Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$MiuiInputMethodEventListener-IA;)V

    iput-object v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mMiuiInputMethodEventListener:Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$MiuiInputMethodEventListener;

    .line 746
    :cond_1
    iget-object v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/gesture/MiuiGestureMonitor;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mMiuiInputMethodEventListener:Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$MiuiInputMethodEventListener;

    invoke-virtual {v0, v1}, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->registerPointerEventListener(Lcom/miui/server/input/gesture/MiuiGestureListener;)V

    .line 748
    return-void
.end method

.method private static setNoCustomizedInputMethodListViewHeight(Landroid/widget/ListView;Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;)V
    .locals 5
    .param p0, "noCustomListView"    # Landroid/widget/ListView;
    .param p1, "adapter"    # Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;

    .line 502
    invoke-virtual {p1}, Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;->getCount()I

    move-result v0

    .line 503
    .local v0, "childrenCount":I
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p1, v2, v1, p0}, Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 504
    .local v1, "childView":Landroid/view/View;
    invoke-virtual {v1, v2, v2}, Landroid/view/View;->measure(II)V

    .line 505
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 507
    .local v2, "childHeight":I
    const/4 v3, 0x0

    .line 508
    .local v3, "height":I
    const/4 v4, 0x3

    if-ge v0, v4, :cond_0

    .line 509
    mul-int v3, v2, v0

    goto :goto_0

    .line 511
    :cond_0
    mul-int/lit8 v3, v2, 0x3

    .line 513
    :goto_0
    invoke-virtual {p0}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .line 514
    .local v4, "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    iput v3, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 515
    invoke-virtual {p0, v4}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 516
    return-void
.end method

.method private unregisterPointerEventListener()V
    .locals 2

    .line 751
    iget-object v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    return-void

    .line 752
    :cond_0
    iget-object v1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mMiuiInputMethodEventListener:Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$MiuiInputMethodEventListener;

    if-eqz v1, :cond_1

    .line 753
    invoke-static {v0}, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/gesture/MiuiGestureMonitor;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mMiuiInputMethodEventListener:Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$MiuiInputMethodEventListener;

    invoke-virtual {v0, v1}, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->unregisterPointerEventListener(Lcom/miui/server/input/gesture/MiuiGestureListener;)V

    .line 755
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mMiuiInputMethodEventListener:Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$MiuiInputMethodEventListener;

    .line 757
    :cond_1
    return-void
.end method


# virtual methods
.method public buildImeNotification(Landroid/app/Notification$Builder;Landroid/content/Context;)Landroid/app/Notification;
    .locals 4
    .param p1, "imeSwitcherNotification"    # Landroid/app/Notification$Builder;
    .param p2, "context"    # Landroid/content/Context;

    .line 586
    invoke-virtual {p1}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    .line 587
    .local v0, "notification":Landroid/app/Notification;
    iget-object v1, v0, Landroid/app/Notification;->extras:Landroid/os/Bundle;

    .line 588
    const v2, 0x1108021a

    invoke-static {p2, v2}, Landroid/graphics/drawable/Icon;->createWithResource(Landroid/content/Context;I)Landroid/graphics/drawable/Icon;

    move-result-object v2

    .line 587
    const-string v3, "miui.appIcon"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 589
    return-object v0
.end method

.method public commitTextForSynergy(Lcom/android/internal/inputmethod/IRemoteInputConnection;Ljava/lang/String;I)V
    .locals 7
    .param p1, "inputContext"    # Lcom/android/internal/inputmethod/IRemoteInputConnection;
    .param p2, "text"    # Ljava/lang/String;
    .param p3, "newCursorPosition"    # I

    .line 675
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 676
    .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v1, "commitTextForSynergy"

    const/4 v2, 0x2

    new-array v3, v2, [Ljava/lang/Class;

    const-class v4, Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v6, 0x1

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 678
    .local v1, "declaredMethod":Ljava/lang/reflect/Method;
    new-array v2, v2, [Ljava/lang/Object;

    aput-object p2, v2, v5

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {v1, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 681
    nop

    .end local v0    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v1    # "declaredMethod":Ljava/lang/reflect/Method;
    goto :goto_0

    .line 679
    :catch_0
    move-exception v0

    .line 680
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "InputMethodManagerServiceImpl"

    const-string v2, "Reflect commitTextForSynergy exception!"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 682
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public createSession(ILcom/android/internal/inputmethod/IInputMethodSessionCallback$Stub;)V
    .locals 3
    .param p1, "userId"    # I
    .param p2, "callback"    # Lcom/android/internal/inputmethod/IInputMethodSessionCallback$Stub;

    .line 725
    sget-boolean v0, Lmiui/os/Build;->IS_DEBUGGABLE:Z

    if-eqz v0, :cond_0

    .line 726
    iget-object v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mBindingController:Lcom/android/server/inputmethod/InputMethodBindingController;

    invoke-virtual {v0}, Lcom/android/server/inputmethod/InputMethodBindingController;->getCurIntent()Landroid/content/Intent;

    move-result-object v0

    .line 728
    .local v0, "intent":Landroid/content/Intent;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    move-object v2, v1

    .local v2, "component":Landroid/content/ComponentName;
    if-eqz v1, :cond_0

    .line 729
    invoke-static {}, Lcom/android/server/inputmethod/InputMethodMonitor;->getInstance()Lcom/android/server/inputmethod/InputMethodMonitor;

    move-result-object v1

    invoke-virtual {v1, p1, v2, p2}, Lcom/android/server/inputmethod/InputMethodMonitor;->startCreateSession(ILandroid/content/ComponentName;Lcom/android/internal/inputmethod/IInputMethodSessionCallback$Stub;)V

    .line 732
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v2    # "component":Landroid/content/ComponentName;
    :cond_0
    return-void
.end method

.method public dismissWithoutAnimation(Lmiuix/appcompat/app/AlertDialog;)V
    .locals 4
    .param p1, "alertDialog"    # Lmiuix/appcompat/app/AlertDialog;

    .line 577
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "dismissWithoutAnimation"

    const/4 v2, 0x0

    new-array v3, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 578
    .local v0, "method":Ljava/lang/reflect/Method;
    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {v0, p1, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 581
    nop

    .end local v0    # "method":Ljava/lang/reflect/Method;
    goto :goto_0

    .line 579
    :catch_0
    move-exception v0

    .line 580
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "InputMethodManagerServiceImpl"

    const-string v2, "Reflect dismissWithoutAnimation exception!"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 582
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public enableInputMethodMonitor(I)V
    .locals 1
    .param p1, "highWatermark"    # I

    .line 717
    sget-boolean v0, Lmiui/os/Build;->IS_DEBUGGABLE:Z

    if-eqz v0, :cond_0

    .line 718
    invoke-static {}, Lcom/android/server/inputmethod/InputMethodMonitor;->getInstance()Lcom/android/server/inputmethod/InputMethodMonitor;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/inputmethod/InputMethodMonitor;->enable(I)V

    .line 720
    :cond_0
    return-void
.end method

.method public enableSystemIMEsIfThereIsNoEnabledIME(Ljava/util/List;Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;)V
    .locals 8
    .param p2, "settings"    # Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/view/inputmethod/InputMethodInfo;",
            ">;",
            "Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;",
            ")V"
        }
    .end annotation

    .line 147
    .local p1, "methodList":Ljava/util/List;, "Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;"
    sget-boolean v0, Lmiui/os/Build;->IS_CM_CUSTOMIZATION_TEST:Z

    if-nez v0, :cond_6

    if-eqz p1, :cond_6

    if-nez p2, :cond_0

    goto :goto_2

    .line 151
    :cond_0
    nop

    .line 152
    invoke-virtual {p2}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getEnabledInputMethodsAndSubtypeListLocked()Ljava/util/List;

    move-result-object v0

    .line 154
    .local v0, "enabledInputMethodsList":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/util/ArrayList<Ljava/lang/String;>;>;>;"
    const/4 v1, 0x0

    .line 156
    .local v1, "systemInputMethod":Landroid/view/inputmethod/InputMethodInfo;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_4

    .line 157
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/inputmethod/InputMethodInfo;

    .line 158
    .local v3, "inputMethodInfo":Landroid/view/inputmethod/InputMethodInfo;
    invoke-virtual {v3}, Landroid/view/inputmethod/InputMethodInfo;->getServiceInfo()Landroid/content/pm/ServiceInfo;

    move-result-object v4

    iget-object v4, v4, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v4, v4, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_1

    .line 160
    move-object v1, v3

    .line 163
    :cond_1
    if-eqz v0, :cond_3

    .line 164
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/util/Pair;

    .line 165
    .local v5, "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/util/ArrayList<Ljava/lang/String;>;>;"
    iget-object v6, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v6, Ljava/lang/CharSequence;

    invoke-virtual {v3}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 166
    return-void

    .line 168
    .end local v5    # "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/util/ArrayList<Ljava/lang/String;>;>;"
    :cond_2
    goto :goto_1

    .line 156
    .end local v3    # "inputMethodInfo":Landroid/view/inputmethod/InputMethodInfo;
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 172
    .end local v2    # "i":I
    :cond_4
    if-eqz v1, :cond_5

    .line 173
    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->appendAndPutEnabledInputMethodLocked(Ljava/lang/String;Z)V

    .line 175
    :cond_5
    return-void

    .line 148
    .end local v0    # "enabledInputMethodsList":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/util/ArrayList<Ljava/lang/String;>;>;>;"
    .end local v1    # "systemInputMethod":Landroid/view/inputmethod/InputMethodInfo;
    :cond_6
    :goto_2
    return-void
.end method

.method public filterNotCustomziedInputMethodList(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;",
            ">;"
        }
    .end annotation

    .line 336
    .local p1, "imList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;>;"
    const-string v0, "InputMethodManagerServiceImpl"

    const-string v1, "filterNotCustomziedInputMethodList."

    invoke-static {v0, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 337
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 339
    .local v0, "noCustomizedInputMethodList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 340
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 341
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;

    .line 342
    .local v2, "item":Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;
    iget-object v3, v2, Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;->mImi:Landroid/view/inputmethod/InputMethodInfo;

    invoke-virtual {v3}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->isCustomizedInputMethod(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 343
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 344
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 346
    .end local v2    # "item":Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;
    :cond_0
    goto :goto_0

    .line 347
    :cond_1
    return-object v0
.end method

.method public generateView(ZLjava/util/List;Lmiuix/appcompat/app/AlertDialog$Builder;Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;Landroid/content/Context;)V
    .locals 23
    .param p1, "isNotContainsCustomizedInputMethod"    # Z
    .param p3, "builder"    # Lmiuix/appcompat/app/AlertDialog$Builder;
    .param p4, "noCustomziedImeListAdapter"    # Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;
    .param p5, "customziedImeListAdapter"    # Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;
    .param p6, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List<",
            "Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;",
            ">;",
            "Lmiuix/appcompat/app/AlertDialog$Builder;",
            "Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;",
            "Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .line 357
    .local p2, "imList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;>;"
    move-object/from16 v7, p0

    move-object/from16 v8, p4

    move-object/from16 v9, p5

    const-string v0, "Start genertating customized view."

    const-string v10, "InputMethodManagerServiceImpl"

    invoke-static {v10, v0}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    move-object/from16 v11, p6

    iput-object v11, v7, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->dialogContext:Landroid/content/Context;

    .line 359
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 360
    return-void

    .line 362
    :cond_0
    iget-object v0, v7, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->dialogContext:Landroid/content/Context;

    const-class v1, Landroid/view/LayoutInflater;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Landroid/view/LayoutInflater;

    .line 364
    .local v12, "inflater":Landroid/view/LayoutInflater;
    iget-object v0, v7, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->dialogContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 365
    const-string v1, "input_method_switch_no_customized_list_view"

    const-string v2, "layout"

    const-string v13, "miuix.stub"

    invoke-virtual {v0, v1, v2, v13}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v14

    .line 366
    .local v14, "customViewId":I
    const/4 v0, 0x0

    invoke-virtual {v12, v14, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v15

    .line 367
    .local v15, "customView":Landroid/view/View;
    if-nez v15, :cond_1

    .line 368
    return-void

    .line 371
    :cond_1
    iget-object v0, v7, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->dialogContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "noCustomizedListView"

    const-string v6, "id"

    invoke-virtual {v0, v1, v6, v13}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    .line 373
    .local v5, "noCustomizedListViewId":I
    nop

    .line 374
    invoke-virtual {v15, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/widget/ListView;

    .line 375
    .local v4, "noCustomListView":Landroid/widget/ListView;
    if-nez v4, :cond_2

    .line 376
    return-void

    .line 379
    :cond_2
    iget-object v0, v7, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->dialogContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "indicator"

    invoke-virtual {v0, v1, v6, v13}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 381
    .local v3, "indicatorId":I
    invoke-virtual {v15, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/widget/ImageView;

    .line 383
    .local v2, "indicator":Landroid/widget/ImageView;
    iget-object v0, v7, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->dialogContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "noCustomizedListViewTitle"

    invoke-virtual {v0, v1, v6, v13}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 385
    .local v1, "noCustomizedListViewTitleId":I
    invoke-virtual {v15, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 387
    .local v0, "noCustomizedListViewTitle":Landroid/view/View;
    move-object/from16 v16, v0

    .end local v0    # "noCustomizedListViewTitle":Landroid/view/View;
    .local v16, "noCustomizedListViewTitle":Landroid/view/View;
    iget v0, v9, Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;->mCheckedItem:I

    move/from16 v17, v1

    .end local v1    # "noCustomizedListViewTitleId":I
    .local v17, "noCustomizedListViewTitleId":I
    const/4 v1, -0x1

    move/from16 v18, v3

    .end local v3    # "indicatorId":I
    .local v18, "indicatorId":I
    if-ne v0, v1, :cond_3

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, v7, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->isExpanded:Z

    .line 389
    iget-object v0, v7, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->dialogContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "listview_open_indicator"

    const-string v3, "drawable"

    invoke-virtual {v0, v1, v3, v13}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 391
    .local v1, "openIndicatorDrawableId":I
    iget-object v0, v7, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->dialogContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    move/from16 v20, v5

    .end local v5    # "noCustomizedListViewId":I
    .local v20, "noCustomizedListViewId":I
    const-string v5, "listview_close_indicator"

    invoke-virtual {v0, v5, v3, v13}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v21

    .line 394
    .local v21, "closeIndicatorDrawableId":I
    iget-boolean v0, v7, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->isExpanded:Z

    const/16 v5, 0x8

    if-nez v0, :cond_4

    .line 395
    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_1

    .line 397
    :cond_4
    const/4 v0, 0x0

    invoke-virtual {v4, v0}, Landroid/widget/ListView;->setVisibility(I)V

    .line 398
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 399
    invoke-static {v4, v8}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->setNoCustomizedInputMethodListViewHeight(Landroid/widget/ListView;Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;)V

    .line 403
    :goto_1
    invoke-virtual {v4, v8}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 404
    new-instance v0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$1;

    invoke-direct {v0, v7, v9, v8}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$1;-><init>(Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;)V

    invoke-virtual {v4, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 414
    new-instance v3, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$2;

    move-object/from16 v8, v16

    .end local v16    # "noCustomizedListViewTitle":Landroid/view/View;
    .local v8, "noCustomizedListViewTitle":Landroid/view/View;
    move-object v0, v3

    move/from16 v16, v17

    move/from16 v17, v1

    .end local v1    # "openIndicatorDrawableId":I
    .local v16, "noCustomizedListViewTitleId":I
    .local v17, "openIndicatorDrawableId":I
    move-object/from16 v1, p0

    move-object/from16 v19, v2

    .end local v2    # "indicator":Landroid/widget/ImageView;
    .local v19, "indicator":Landroid/widget/ImageView;
    move-object v9, v3

    move/from16 v3, v17

    move-object/from16 v22, v4

    .end local v4    # "noCustomListView":Landroid/widget/ListView;
    .local v22, "noCustomListView":Landroid/widget/ListView;
    move v11, v5

    move-object/from16 v5, p4

    move-object v11, v6

    move/from16 v6, v21

    invoke-direct/range {v0 .. v6}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$2;-><init>(Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;Landroid/widget/ImageView;ILandroid/widget/ListView;Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;I)V

    invoke-virtual {v8, v9}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 431
    move-object/from16 v0, p3

    invoke-virtual {v0, v15}, Lmiuix/appcompat/app/AlertDialog$Builder;->setView(Landroid/view/View;)Lmiuix/appcompat/app/AlertDialog$Builder;

    .line 432
    if-eqz p1, :cond_6

    .line 433
    const-string v1, "There\'s no customized ime currently, so customized ime view will hidden"

    invoke-static {v10, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 434
    iget-object v1, v7, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->dialogContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "dividerLine"

    invoke-virtual {v1, v2, v11, v13}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 436
    .local v1, "dividerLineId":I
    invoke-virtual {v15, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 437
    .local v2, "dividerLine":Landroid/view/View;
    if-nez v2, :cond_5

    .line 438
    return-void

    .line 441
    :cond_5
    const/16 v3, 0x8

    invoke-virtual {v8, v3}, Landroid/view/View;->setVisibility(I)V

    .line 442
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 444
    .end local v1    # "dividerLineId":I
    .end local v2    # "dividerLine":Landroid/view/View;
    :cond_6
    return-void
.end method

.method public getMirrorInputState()Z
    .locals 1

    .line 712
    iget-boolean v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mMirrorInputState:Z

    return v0
.end method

.method public getNoCustomizedCheckedItem()I
    .locals 1

    .line 553
    iget v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->noCustomizedCheckedItem:I

    return v0
.end method

.method public getNoCustomizedIms()[Landroid/view/inputmethod/InputMethodInfo;
    .locals 1

    .line 558
    iget-object v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mNoCustomizedIms:[Landroid/view/inputmethod/InputMethodInfo;

    return-object v0
.end method

.method public getNoCustomizedSubtypeIds()[I
    .locals 1

    .line 563
    iget-object v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mNoCustomizedSubtypeIds:[I

    return-object v0
.end method

.method public init(Landroid/os/Handler;Lcom/android/server/inputmethod/InputMethodBindingController;Landroid/content/Context;)V
    .locals 3
    .param p1, "handler"    # Landroid/os/Handler;
    .param p2, "bindingController"    # Lcom/android/server/inputmethod/InputMethodBindingController;
    .param p3, "context"    # Landroid/content/Context;

    .line 127
    iput-object p1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mHandler:Landroid/os/Handler;

    .line 128
    iput-object p2, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mBindingController:Lcom/android/server/inputmethod/InputMethodBindingController;

    .line 129
    iput-object p3, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mContext:Landroid/content/Context;

    .line 130
    invoke-virtual {p0, p3}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->registerObserver(Landroid/content/Context;)V

    .line 132
    :try_start_0
    iget-object v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_1

    .line 133
    nop

    .line 134
    nop

    .line 133
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "mirror_input_state"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 134
    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    nop

    :goto_0
    iput-boolean v1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mMirrorInputState:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 138
    :cond_1
    goto :goto_1

    .line 136
    :catch_0
    move-exception v0

    .line 137
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception cause:Failed to read Settings for MIRROR_INPUT_STATE:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "InputMethodManagerServiceImpl"

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    return-void
.end method

.method public initNoCustomizedInputMethodData(Ljava/util/List;Ljava/lang/String;II)V
    .locals 5
    .param p2, "lastInputMethodId"    # Ljava/lang/String;
    .param p3, "lastInputMethodSubtypeId"    # I
    .param p4, "NOT_A_SUBTYPE_ID"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;",
            ">;",
            "Ljava/lang/String;",
            "II)V"
        }
    .end annotation

    .line 307
    .local p1, "noCustomizedInputMethodList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "initNoCustomizedInputMethodData  noCustomizedInputMethodList:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "InputMethodManagerServiceImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 309
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->noCustomizedCheckedItem:I

    .line 310
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    .line 311
    .local v0, "N":I
    new-array v1, v0, [Landroid/view/inputmethod/InputMethodInfo;

    iput-object v1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mNoCustomizedIms:[Landroid/view/inputmethod/InputMethodInfo;

    .line 312
    new-array v1, v0, [I

    iput-object v1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mNoCustomizedSubtypeIds:[I

    .line 313
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_3

    .line 314
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;

    .line 315
    .local v2, "item":Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;
    iget-object v3, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mNoCustomizedIms:[Landroid/view/inputmethod/InputMethodInfo;

    iget-object v4, v2, Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;->mImi:Landroid/view/inputmethod/InputMethodInfo;

    aput-object v4, v3, v1

    .line 316
    iget-object v3, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mNoCustomizedSubtypeIds:[I

    iget v4, v2, Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;->mSubtypeId:I

    aput v4, v3, v1

    .line 317
    iget-object v3, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mNoCustomizedIms:[Landroid/view/inputmethod/InputMethodInfo;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 318
    iget-object v3, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mNoCustomizedSubtypeIds:[I

    aget v3, v3, v1

    .line 319
    .local v3, "subtypeId":I
    if-eq v3, p4, :cond_1

    if-ne p3, p4, :cond_0

    if-eqz v3, :cond_1

    :cond_0
    if-ne v3, p3, :cond_2

    .line 322
    :cond_1
    iput v1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->noCustomizedCheckedItem:I

    .line 313
    .end local v2    # "item":Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;
    .end local v3    # "subtypeId":I
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 326
    .end local v1    # "i":I
    :cond_3
    return-void
.end method

.method public isCallingBetweenCustomIME(Landroid/content/Context;ILjava/lang/String;)Z
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uid"    # I
    .param p3, "targetPkgName"    # Ljava/lang/String;

    .line 615
    sget-object v0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->customizedInputMethodList:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 616
    return v1

    .line 618
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v0

    .line 619
    .local v0, "packages":[Ljava/lang/String;
    if-eqz v0, :cond_4

    array-length v2, v0

    if-nez v2, :cond_1

    goto :goto_1

    .line 623
    :cond_1
    array-length v2, v0

    move v3, v1

    :goto_0
    if-ge v3, v2, :cond_3

    aget-object v4, v0, v3

    .line 624
    .local v4, "packageName":Ljava/lang/String;
    sget-object v5, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->customizedInputMethodList:Ljava/util/List;

    invoke-interface {v5, p3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 625
    const/4 v1, 0x1

    return v1

    .line 623
    .end local v4    # "packageName":Ljava/lang/String;
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 628
    :cond_3
    return v1

    .line 620
    :cond_4
    :goto_1
    return v1
.end method

.method public isCustomizedInputMethod(Ljava/lang/String;)Z
    .locals 3
    .param p1, "inputMethodId"    # Ljava/lang/String;

    .line 287
    const-string v0, ""

    .line 288
    .local v0, "inputMethodName":Ljava/lang/String;
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    .line 289
    const/16 v1, 0x2f

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 290
    .local v1, "endIndex":I
    if-lez v1, :cond_0

    .line 291
    invoke-virtual {p1, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 294
    .end local v1    # "endIndex":I
    :cond_0
    sget-object v1, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->customizedInputMethodList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 295
    const/4 v1, 0x1

    return v1

    .line 297
    :cond_1
    return v2
.end method

.method public isInputMethodWindowInvisibleByRecentTask(I)Z
    .locals 2
    .param p1, "imeWindowVis"    # I

    .line 632
    iget-boolean v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->callStartInputOrWindowGainedFocusFlag:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 633
    iput-boolean v1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->callStartInputOrWindowGainedFocusFlag:Z

    .line 634
    if-nez p1, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1

    .line 636
    :cond_1
    return v1
.end method

.method public isInputMethodWindowInvisibleByScreenOn(I)Z
    .locals 3
    .param p1, "imeWindowVis"    # I

    .line 644
    iget-boolean v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mScreenStatus:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-boolean v2, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mScreenOffLastTime:Z

    if-eqz v2, :cond_0

    if-nez p1, :cond_0

    .line 645
    iput-boolean v1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mScreenOffLastTime:Z

    .line 646
    const/4 v0, 0x1

    return v0

    .line 648
    :cond_0
    if-eqz v0, :cond_1

    .line 649
    iput-boolean v1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mScreenOffLastTime:Z

    .line 651
    :cond_1
    return v1
.end method

.method public isPad()Z
    .locals 1

    .line 283
    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    return v0
.end method

.method public isSupportedCustomizedDialog()Z
    .locals 1

    .line 685
    const/4 v0, 0x1

    return v0
.end method

.method public notifyAcceptInput(I)Z
    .locals 4
    .param p1, "status"    # I

    .line 243
    iget-object v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mMonitorBinder:Landroid/os/IBinder;

    if-eqz v0, :cond_0

    .line 244
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 245
    .local v0, "request":Landroid/os/Parcel;
    iput p1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mLastAcceptStatus:I

    .line 247
    const/4 v1, 0x0

    :try_start_0
    const-string v2, "com.android.synergy.Callback"

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 248
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 249
    iget v2, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mMirrorDisplayId:I

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 250
    iget-object v2, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mMonitorBinder:Landroid/os/IBinder;

    const/4 v3, 0x1

    invoke-interface {v2, v3, v0, v1, v3}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 252
    nop

    .line 257
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 252
    return v3

    .line 257
    :catchall_0
    move-exception v1

    goto :goto_0

    .line 253
    :catch_0
    move-exception v2

    .line 254
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    iput-object v1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mMonitorBinder:Landroid/os/IBinder;

    .line 255
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 257
    .end local v2    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 258
    goto :goto_1

    .line 257
    :goto_0
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 258
    throw v1

    .line 260
    .end local v0    # "request":Landroid/os/Parcel;
    :cond_0
    :goto_1
    const/4 v0, 0x0

    return v0
.end method

.method public onMiuiTransact(ILandroid/os/Parcel;Landroid/os/Parcel;ILcom/android/internal/inputmethod/IRemoteInputConnection;ILandroid/os/Handler;)Z
    .locals 5
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .param p5, "inputContext"    # Lcom/android/internal/inputmethod/IRemoteInputConnection;
    .param p6, "imeWindowVis"    # I
    .param p7, "handler"    # Landroid/os/Handler;

    .line 190
    const/4 v0, 0x0

    const/4 v1, 0x1

    packed-switch p1, :pswitch_data_0

    .line 234
    return v0

    .line 192
    :pswitch_0
    :try_start_0
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    .line 193
    .local v2, "callingUid":I
    const/16 v3, 0x3e8

    if-ne v2, v3, :cond_2

    .line 196
    and-int/lit8 v3, p6, 0x2

    if-eqz v3, :cond_0

    .line 197
    const-class v3, Lcom/android/server/inputmethod/InputMethodManagerInternal;

    .line 198
    invoke-static {v3}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/inputmethod/InputMethodManagerInternal;

    .line 199
    .local v3, "service":Lcom/android/server/inputmethod/InputMethodManagerInternal;
    if-eqz v3, :cond_0

    .line 200
    const/16 v4, 0x10

    invoke-virtual {v3, v4}, Lcom/android/server/inputmethod/InputMethodManagerInternal;->hideCurrentInputMethod(I)V

    .line 203
    .end local v3    # "service":Lcom/android/server/inputmethod/InputMethodManagerInternal;
    :cond_0
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 204
    .local v3, "text":Ljava/lang/String;
    if-eqz p5, :cond_1

    .line 205
    invoke-virtual {p0, p5, v3, v1}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->commitTextForSynergy(Lcom/android/internal/inputmethod/IRemoteInputConnection;Ljava/lang/String;I)V

    .line 207
    :cond_1
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 208
    const/4 v4, 0x1

    .line 209
    .local v4, "res":I
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 210
    return v1

    .line 194
    .end local v3    # "text":Ljava/lang/String;
    .end local v4    # "res":I
    :cond_2
    new-instance v1, Ljava/lang/SecurityException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Calling CODE_INPUT_TEXT unsafe by "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    .end local p0    # "this":Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;
    .end local p1    # "code":I
    .end local p2    # "data":Landroid/os/Parcel;
    .end local p3    # "reply":Landroid/os/Parcel;
    .end local p4    # "flags":I
    .end local p5    # "inputContext":Lcom/android/internal/inputmethod/IRemoteInputConnection;
    .end local p6    # "imeWindowVis":I
    .end local p7    # "handler":Landroid/os/Handler;
    throw v1

    .line 212
    .end local v2    # "callingUid":I
    .restart local p0    # "this":Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;
    .restart local p1    # "code":I
    .restart local p2    # "data":Landroid/os/Parcel;
    .restart local p3    # "reply":Landroid/os/Parcel;
    .restart local p4    # "flags":I
    .restart local p5    # "inputContext":Lcom/android/internal/inputmethod/IRemoteInputConnection;
    .restart local p6    # "imeWindowVis":I
    .restart local p7    # "handler":Landroid/os/Handler;
    :pswitch_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mMonitorBinder:Landroid/os/IBinder;

    .line 213
    invoke-direct {p0}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->registerPointerEventListener()V

    .line 214
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 215
    return v1

    .line 217
    :pswitch_2
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mMonitorBinder:Landroid/os/IBinder;

    .line 218
    invoke-direct {p0}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->unregisterPointerEventListener()V

    .line 219
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 220
    return v1

    .line 222
    :pswitch_3
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mSynergyOperate:I

    .line 223
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 224
    return v1

    .line 227
    :pswitch_4
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mMirrorDisplayId:I

    .line 228
    if-eqz v2, :cond_3

    move v2, v1

    goto :goto_0

    :cond_3
    move v2, v0

    :goto_0
    iput-boolean v2, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mIsFromRemoteDevice:Z

    .line 229
    const-string v2, "InputMethodManagerServiceImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "set mIsFromRemoteDevice as:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mIsFromRemoteDevice:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",displayId as:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mMirrorDisplayId:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " via mirror"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 232
    return v1

    .line 236
    :catch_0
    move-exception v1

    .line 237
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 239
    .end local v1    # "e":Ljava/lang/Exception;
    return v0

    nop

    :pswitch_data_0
    .packed-switch 0xfffffa
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onSessionCreated(Lcom/android/internal/inputmethod/IInputMethodSessionCallback$Stub;)V
    .locals 1
    .param p1, "callback"    # Lcom/android/internal/inputmethod/IInputMethodSessionCallback$Stub;

    .line 736
    sget-boolean v0, Lmiui/os/Build;->IS_DEBUGGABLE:Z

    if-eqz v0, :cond_0

    .line 737
    invoke-static {}, Lcom/android/server/inputmethod/InputMethodMonitor;->getInstance()Lcom/android/server/inputmethod/InputMethodMonitor;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/inputmethod/InputMethodMonitor;->finishCreateSession(Lcom/android/internal/inputmethod/IInputMethodSessionCallback$Stub;)V

    .line 739
    :cond_0
    return-void
.end method

.method public onSwitchIME(Landroid/content/Context;Landroid/view/inputmethod/InputMethodInfo;Ljava/lang/String;Ljava/util/List;Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;Landroid/util/ArrayMap;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "curInputMethodInfo"    # Landroid/view/inputmethod/InputMethodInfo;
    .param p3, "lastInputMethodId"    # Ljava/lang/String;
    .param p5, "inputMethodSettings"    # Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/view/inputmethod/InputMethodInfo;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;",
            ">;",
            "Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;",
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "Landroid/view/inputmethod/InputMethodInfo;",
            ">;)V"
        }
    .end annotation

    .line 179
    .local p4, "imList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;>;"
    .local p6, "methodMap":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Landroid/view/inputmethod/InputMethodInfo;>;"
    invoke-virtual {p2}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 180
    return-void

    .line 182
    :cond_0
    invoke-virtual {p2}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/inputmethodservice/InputMethodAnalyticsUtil;->addNotificationPanelRecord(Landroid/content/Context;Ljava/lang/String;)V

    .line 183
    return-void
.end method

.method public registerObserver(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 690
    new-instance v0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$4;

    invoke-static {}, Landroid/os/Handler;->getMain()Landroid/os/Handler;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$4;-><init>(Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;Landroid/os/Handler;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mObserver:Landroid/database/ContentObserver;

    .line 707
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "mirror_input_state"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mObserver:Landroid/database/ContentObserver;

    const/4 v3, -0x1

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 709
    return-void
.end method

.method public removeCustomTitle(Lmiuix/appcompat/app/AlertDialog$Builder;Landroid/view/View;)V
    .locals 1
    .param p1, "dialogBuilder"    # Lmiuix/appcompat/app/AlertDialog$Builder;
    .param p2, "switchingDialogTitleView"    # Landroid/view/View;

    .line 570
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lmiuix/appcompat/app/AlertDialog$Builder;->setCustomTitle(Landroid/view/View;)Lmiuix/appcompat/app/AlertDialog$Builder;

    .line 571
    const v0, 0x104090b

    invoke-virtual {p1, v0}, Lmiuix/appcompat/app/AlertDialog$Builder;->setTitle(I)Lmiuix/appcompat/app/AlertDialog$Builder;

    .line 572
    const/4 p2, 0x0

    .line 573
    return-void
.end method

.method public sendKeyboardCaps()V
    .locals 2

    .line 274
    const-string v0, "InputMethodManagerServiceImpl"

    const-string v1, "Send caps event from keyboard."

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    invoke-virtual {p0}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->isPad()Z

    .line 276
    return-void
.end method

.method public setDialogImmersive(Lmiuix/appcompat/app/AlertDialog;)V
    .locals 3
    .param p1, "dialog"    # Lmiuix/appcompat/app/AlertDialog;

    .line 450
    invoke-virtual {p0}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->isPad()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 452
    return-void

    .line 454
    :cond_0
    invoke-virtual {p1}, Lmiuix/appcompat/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 455
    .local v0, "w":Landroid/view/Window;
    const v1, -0x7ffff700

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 459
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/WindowManager$LayoutParams;->setFitInsetsSides(I)V

    .line 461
    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    .line 462
    .local v1, "decorView":Landroid/view/View;
    invoke-static {v1}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->clearFitSystemWindow(Landroid/view/View;)V

    .line 463
    new-instance v2, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$3;

    invoke-direct {v2, p0, v1}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$3;-><init>(Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnApplyWindowInsetsListener(Landroid/view/View$OnApplyWindowInsetsListener;)V

    .line 479
    return-void
.end method

.method public setScreenOff(Z)V
    .locals 0
    .param p1, "inputMethodWindowDisplayed"    # Z

    .line 655
    iput-boolean p1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mScreenOffLastTime:Z

    .line 656
    return-void
.end method

.method public setScreenStatus(Z)V
    .locals 0
    .param p1, "screenStatus"    # Z

    .line 659
    iput-boolean p1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mScreenStatus:Z

    .line 660
    return-void
.end method

.method public setWindowGainedbyRecentTask(Z)V
    .locals 0
    .param p1, "inputMethodWindowDisplayed"    # Z

    .line 640
    iput-boolean p1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->callStartInputOrWindowGainedFocusFlag:Z

    .line 641
    return-void
.end method

.method public shouldClearShowForcedFlag(Landroid/content/Context;I)Z
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uid"    # I

    .line 602
    const/4 v0, 0x0

    .line 603
    .local v0, "result":Z
    if-nez p1, :cond_0

    return v0

    .line 604
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v1

    .line 605
    .local v1, "packages":[Ljava/lang/String;
    if-eqz v1, :cond_3

    array-length v2, v1

    if-nez v2, :cond_1

    goto :goto_1

    .line 608
    :cond_1
    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_2

    aget-object v4, v1, v3

    .line 609
    .local v4, "packageName":Ljava/lang/String;
    const-string v5, "com.miui.home"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    or-int/2addr v0, v5

    .line 608
    .end local v4    # "packageName":Ljava/lang/String;
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 611
    :cond_2
    return v0

    .line 606
    :cond_3
    :goto_1
    return v0
.end method

.method public synergyOperate()Z
    .locals 2

    .line 265
    iget-object v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mMonitorBinder:Landroid/os/IBinder;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mSynergyOperate:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-boolean v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mMirrorInputState:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mIsFromRemoteDevice:Z

    if-eqz v0, :cond_1

    .line 267
    :cond_0
    return v1

    .line 269
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public updateItemView(Landroid/view/View;Ljava/lang/CharSequence;Ljava/lang/CharSequence;IILandroid/view/ViewGroup;)V
    .locals 9
    .param p1, "view"    # Landroid/view/View;
    .param p2, "imeName"    # Ljava/lang/CharSequence;
    .param p3, "subtypeName"    # Ljava/lang/CharSequence;
    .param p4, "position"    # I
    .param p5, "checkedItem"    # I
    .param p6, "parent"    # Landroid/view/ViewGroup;

    .line 524
    iget-object v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->dialogContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string/jumbo v1, "title"

    const-string v2, "id"

    const-string v3, "miuix.stub"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 526
    .local v0, "firstTextViewId":I
    iget-object v1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->dialogContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string/jumbo v4, "summary"

    invoke-virtual {v1, v4, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 528
    .local v1, "secondTextViewId":I
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 529
    .local v4, "firstTextView":Landroid/widget/TextView;
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 530
    .local v5, "secondTextView":Landroid/widget/TextView;
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    const/4 v7, 0x0

    if-eqz v6, :cond_0

    .line 531
    invoke-virtual {v4, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 532
    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 534
    :cond_0
    invoke-virtual {v4, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 535
    invoke-virtual {v5, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 536
    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 539
    :goto_0
    iget-object v6, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->dialogContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const-string v8, "radio"

    invoke-virtual {v6, v8, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 541
    .local v2, "radioButtonId":I
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RadioButton;

    .line 542
    .local v3, "radioButton":Landroid/widget/RadioButton;
    const/4 v6, 0x1

    if-ne p4, p5, :cond_1

    move v8, v6

    goto :goto_1

    :cond_1
    move v8, v7

    :goto_1
    invoke-virtual {v3, v8}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 543
    move-object v8, p6

    check-cast v8, Landroid/widget/ListView;

    invoke-virtual {v8, v7}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 544
    if-ne p4, p5, :cond_2

    .line 545
    invoke-virtual {p1, v6}, Landroid/view/View;->setActivated(Z)V

    goto :goto_2

    .line 547
    :cond_2
    invoke-virtual {p1, v7}, Landroid/view/View;->setActivated(Z)V

    .line 549
    :goto_2
    return-void
.end method

.method public updateSessionId(I)V
    .locals 1
    .param p1, "sessionId"    # I

    .line 666
    invoke-virtual {p0}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->synergyOperate()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 667
    :cond_0
    iput p1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mSessionId:I

    .line 668
    return-void
.end method
