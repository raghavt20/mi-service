class com.android.server.inputmethod.InputMethodManagerServiceImpl$1 implements android.widget.AdapterView$OnItemClickListener {
	 /* .source "InputMethodManagerServiceImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->generateView(ZLjava/util/List;Lmiuix/appcompat/app/AlertDialog$Builder;Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;Landroid/content/Context;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.inputmethod.InputMethodManagerServiceImpl this$0; //synthetic
final com.android.server.inputmethod.InputMethodMenuController$ImeSubtypeListAdapter val$customziedImeListAdapter; //synthetic
final com.android.server.inputmethod.InputMethodMenuController$ImeSubtypeListAdapter val$noCustomziedImeListAdapter; //synthetic
/* # direct methods */
 com.android.server.inputmethod.InputMethodManagerServiceImpl$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/inputmethod/InputMethodManagerServiceImpl; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 404 */
this.this$0 = p1;
this.val$customziedImeListAdapter = p2;
this.val$noCustomziedImeListAdapter = p3;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onItemClick ( android.widget.AdapterView p0, android.view.View p1, Integer p2, Long p3 ) {
/* .locals 2 */
/* .param p2, "view" # Landroid/view/View; */
/* .param p3, "position" # I */
/* .param p4, "id" # J */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/widget/AdapterView<", */
/* "*>;", */
/* "Landroid/view/View;", */
/* "IJ)V" */
/* } */
} // .end annotation
/* .line 407 */
/* .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;" */
v0 = this.val$customziedImeListAdapter;
int v1 = -1; // const/4 v1, -0x1
/* iput v1, v0, Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;->mCheckedItem:I */
/* .line 408 */
v0 = this.val$customziedImeListAdapter;
(( com.android.server.inputmethod.InputMethodMenuController$ImeSubtypeListAdapter ) v0 ).notifyDataSetChanged ( ); // invoke-virtual {v0}, Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;->notifyDataSetChanged()V
/* .line 409 */
v0 = this.val$noCustomziedImeListAdapter;
/* iput p3, v0, Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;->mCheckedItem:I */
/* .line 410 */
v0 = this.val$noCustomziedImeListAdapter;
(( com.android.server.inputmethod.InputMethodMenuController$ImeSubtypeListAdapter ) v0 ).notifyDataSetChanged ( ); // invoke-virtual {v0}, Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;->notifyDataSetChanged()V
/* .line 411 */
return;
} // .end method
