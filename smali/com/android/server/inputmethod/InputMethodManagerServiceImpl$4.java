class com.android.server.inputmethod.InputMethodManagerServiceImpl$4 extends android.database.ContentObserver {
	 /* .source "InputMethodManagerServiceImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->registerObserver(Landroid/content/Context;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.inputmethod.InputMethodManagerServiceImpl this$0; //synthetic
final android.content.Context val$context; //synthetic
/* # direct methods */
 com.android.server.inputmethod.InputMethodManagerServiceImpl$4 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/inputmethod/InputMethodManagerServiceImpl; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 690 */
this.this$0 = p1;
this.val$context = p3;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 4 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 694 */
final String v0 = "mirror_input_state"; // const-string v0, "mirror_input_state"
int v1 = 0; // const/4 v1, 0x0
/* .line 695 */
/* .local v1, "mirrorInputState":I */
int v2 = 1; // const/4 v2, 0x1
try { // :try_start_0
	 android.provider.Settings$Secure .getUriFor ( v0 );
	 v3 = 	 (( android.net.Uri ) v3 ).equals ( p2 ); // invoke-virtual {v3, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
	 if ( v3 != null) { // if-eqz v3, :cond_1
		 v3 = this.val$context;
		 if ( v3 != null) { // if-eqz v3, :cond_1
			 /* .line 697 */
			 (( android.content.Context ) v3 ).getContentResolver ( ); // invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
			 v0 = 			 android.provider.Settings$Secure .getInt ( v3,v0 );
			 /* .line 699 */
		 } // .end local v1 # "mirrorInputState":I
		 /* .local v0, "mirrorInputState":I */
		 v1 = this.this$0;
		 /* if-ne v0, v2, :cond_0 */
		 int v3 = 0; // const/4 v3, 0x0
	 } // :cond_0
	 /* move v3, v2 */
} // :goto_0
com.android.server.inputmethod.InputMethodManagerServiceImpl .-$$Nest$fputmMirrorInputState ( v1,v3 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 704 */
} // .end local v0 # "mirrorInputState":I
} // :cond_1
/* .line 701 */
/* :catch_0 */
/* move-exception v0 */
/* .line 702 */
/* .local v0, "e":Ljava/lang/Exception; */
v1 = this.this$0;
com.android.server.inputmethod.InputMethodManagerServiceImpl .-$$Nest$fputmMirrorInputState ( v1,v2 );
/* .line 703 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Exception caused:"; // const-string v2, "Exception caused:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v2 = ", set mMirrorInputState to true"; // const-string v2, ", set mMirrorInputState to true"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "InputMethodManagerServiceImpl"; // const-string v2, "InputMethodManagerServiceImpl"
android.util.Slog .e ( v2,v1 );
/* .line 705 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
return;
} // .end method
