.class Lcom/android/server/inputmethod/SecurityInputMethodSwitcher$SettingsObserver;
.super Landroid/database/ContentObserver;
.source "SecurityInputMethodSwitcher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SettingsObserver"
.end annotation


# instance fields
.field mRegistered:Z

.field mUserId:I

.field final synthetic this$0:Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;


# direct methods
.method constructor <init>(Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;Landroid/os/Handler;)V
    .locals 1
    .param p1, "this$0"    # Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 227
    iput-object p1, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher$SettingsObserver;->this$0:Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;

    .line 228
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 225
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher$SettingsObserver;->mRegistered:Z

    .line 229
    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 6
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 254
    const-string v0, "enable_miui_security_ime"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 256
    .local v0, "secIMEUri":Landroid/net/Uri;
    iget-object v1, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher$SettingsObserver;->this$0:Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;

    iget-object v1, v1, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v1, v1, Lcom/android/server/inputmethod/InputMethodManagerService;->mMethodMap:Landroid/util/ArrayMap;

    monitor-enter v1

    .line 257
    :try_start_0
    invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 258
    iget-object v2, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher$SettingsObserver;->this$0:Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;

    iget-object v3, v2, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v3, v3, Lcom/android/server/inputmethod/InputMethodManagerService;->mSettings:Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;

    invoke-virtual {v3}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getCurrentUserId()I

    move-result v3

    invoke-static {v2, v3}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->-$$Nest$mgetParentUserId(Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;I)I

    move-result v2

    .line 259
    .local v2, "parentUserId":I
    iget-object v3, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher$SettingsObserver;->this$0:Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;

    invoke-static {v3, v2}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->-$$Nest$misSecEnabled(Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;I)Z

    move-result v4

    invoke-static {v3, v4}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->-$$Nest$fputmSecEnabled(Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;Z)V

    .line 260
    iget-object v3, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher$SettingsObserver;->this$0:Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;

    invoke-static {v3}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->-$$Nest$mupdateFromSettingsLocked(Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;)V

    .line 262
    const-string v3, "SecurityInputMethodSwitcher"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "enable status change: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher$SettingsObserver;->this$0:Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;

    invoke-static {v5}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->-$$Nest$fgetmSecEnabled(Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;)Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    .end local v2    # "parentUserId":I
    :cond_0
    monitor-exit v1

    .line 266
    return-void

    .line 265
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method registerContentObserverLocked(I)V
    .locals 3
    .param p1, "userId"    # I

    .line 232
    sget-boolean v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->SUPPORT_SEC_INPUT_METHOD:Z

    if-nez v0, :cond_0

    .line 233
    return-void

    .line 235
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher$SettingsObserver;->mRegistered:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher$SettingsObserver;->mUserId:I

    if-ne v0, p1, :cond_1

    .line 236
    return-void

    .line 238
    :cond_1
    iget-object v0, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher$SettingsObserver;->this$0:Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;

    iget-object v0, v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v0, v0, Lcom/android/server/inputmethod/InputMethodManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 239
    .local v0, "resolver":Landroid/content/ContentResolver;
    iget-boolean v1, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher$SettingsObserver;->mRegistered:Z

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    .line 240
    invoke-virtual {v0, p0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 241
    iput-boolean v2, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher$SettingsObserver;->mRegistered:Z

    .line 243
    :cond_2
    iget v1, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher$SettingsObserver;->mUserId:I

    if-eq v1, p1, :cond_3

    .line 244
    iput p1, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher$SettingsObserver;->mUserId:I

    .line 246
    :cond_3
    const-string v1, "enable_miui_security_ime"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, p0, p1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 249
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher$SettingsObserver;->mRegistered:Z

    .line 250
    return-void
.end method
