class com.android.server.inputmethod.SecurityInputMethodSwitcher$SettingsObserver extends android.database.ContentObserver {
	 /* .source "SecurityInputMethodSwitcher.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/inputmethod/SecurityInputMethodSwitcher; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "SettingsObserver" */
} // .end annotation
/* # instance fields */
Boolean mRegistered;
Integer mUserId;
final com.android.server.inputmethod.SecurityInputMethodSwitcher this$0; //synthetic
/* # direct methods */
 com.android.server.inputmethod.SecurityInputMethodSwitcher$SettingsObserver ( ) {
/* .locals 1 */
/* .param p1, "this$0" # Lcom/android/server/inputmethod/SecurityInputMethodSwitcher; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 227 */
this.this$0 = p1;
/* .line 228 */
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
/* .line 225 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher$SettingsObserver;->mRegistered:Z */
/* .line 229 */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 6 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 254 */
final String v0 = "enable_miui_security_ime"; // const-string v0, "enable_miui_security_ime"
android.provider.Settings$Secure .getUriFor ( v0 );
/* .line 256 */
/* .local v0, "secIMEUri":Landroid/net/Uri; */
v1 = this.this$0;
v1 = this.mService;
v1 = this.mMethodMap;
/* monitor-enter v1 */
/* .line 257 */
try { // :try_start_0
	 v2 = 	 (( android.net.Uri ) v0 ).equals ( p2 ); // invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
	 if ( v2 != null) { // if-eqz v2, :cond_0
		 /* .line 258 */
		 v2 = this.this$0;
		 v3 = this.mService;
		 v3 = this.mSettings;
		 v3 = 		 (( com.android.server.inputmethod.InputMethodUtils$InputMethodSettings ) v3 ).getCurrentUserId ( ); // invoke-virtual {v3}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getCurrentUserId()I
		 v2 = 		 com.android.server.inputmethod.SecurityInputMethodSwitcher .-$$Nest$mgetParentUserId ( v2,v3 );
		 /* .line 259 */
		 /* .local v2, "parentUserId":I */
		 v3 = this.this$0;
		 v4 = 		 com.android.server.inputmethod.SecurityInputMethodSwitcher .-$$Nest$misSecEnabled ( v3,v2 );
		 com.android.server.inputmethod.SecurityInputMethodSwitcher .-$$Nest$fputmSecEnabled ( v3,v4 );
		 /* .line 260 */
		 v3 = this.this$0;
		 com.android.server.inputmethod.SecurityInputMethodSwitcher .-$$Nest$mupdateFromSettingsLocked ( v3 );
		 /* .line 262 */
		 final String v3 = "SecurityInputMethodSwitcher"; // const-string v3, "SecurityInputMethodSwitcher"
		 /* new-instance v4, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v5 = "enable status change: "; // const-string v5, "enable status change: "
		 (( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 v5 = this.this$0;
		 v5 = 		 com.android.server.inputmethod.SecurityInputMethodSwitcher .-$$Nest$fgetmSecEnabled ( v5 );
		 (( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Slog .d ( v3,v4 );
		 /* .line 265 */
	 } // .end local v2 # "parentUserId":I
} // :cond_0
/* monitor-exit v1 */
/* .line 266 */
return;
/* .line 265 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
void registerContentObserverLocked ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "userId" # I */
/* .line 232 */
/* sget-boolean v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->SUPPORT_SEC_INPUT_METHOD:Z */
/* if-nez v0, :cond_0 */
/* .line 233 */
return;
/* .line 235 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher$SettingsObserver;->mRegistered:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget v0, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher$SettingsObserver;->mUserId:I */
/* if-ne v0, p1, :cond_1 */
/* .line 236 */
return;
/* .line 238 */
} // :cond_1
v0 = this.this$0;
v0 = this.mService;
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 239 */
/* .local v0, "resolver":Landroid/content/ContentResolver; */
/* iget-boolean v1, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher$SettingsObserver;->mRegistered:Z */
int v2 = 0; // const/4 v2, 0x0
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 240 */
(( android.content.ContentResolver ) v0 ).unregisterContentObserver ( p0 ); // invoke-virtual {v0, p0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V
/* .line 241 */
/* iput-boolean v2, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher$SettingsObserver;->mRegistered:Z */
/* .line 243 */
} // :cond_2
/* iget v1, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher$SettingsObserver;->mUserId:I */
/* if-eq v1, p1, :cond_3 */
/* .line 244 */
/* iput p1, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher$SettingsObserver;->mUserId:I */
/* .line 246 */
} // :cond_3
final String v1 = "enable_miui_security_ime"; // const-string v1, "enable_miui_security_ime"
android.provider.Settings$Secure .getUriFor ( v1 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, p1 ); // invoke-virtual {v0, v1, v2, p0, p1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 249 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher$SettingsObserver;->mRegistered:Z */
/* .line 250 */
return;
} // .end method
