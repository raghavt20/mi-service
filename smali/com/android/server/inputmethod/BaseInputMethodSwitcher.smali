.class public Lcom/android/server/inputmethod/BaseInputMethodSwitcher;
.super Ljava/lang/Object;
.source "BaseInputMethodSwitcher.java"

# interfaces
.implements Lcom/android/server/inputmethod/MiuiInputMethodStub;


# static fields
.field public static final NUMBER_PASSWORD:I = 0x12

.field public static final TEXT_MASK:I = 0xfff

.field public static final TEXT_PASSWORD:I = 0x81

.field public static final TEXT_VISIBLE_PASSWORD:I = 0x91

.field public static final TEXT_WEB_PASSWORD:I = 0xe1

.field public static final WEB_EDIT_TEXT:I = 0xa0


# instance fields
.field protected mService:Lcom/android/server/inputmethod/InputMethodManagerService;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static isPasswdInputType(I)Z
    .locals 4
    .param p0, "inputType"    # I

    .line 26
    and-int/lit16 v0, p0, 0xa0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/16 v3, 0xa0

    if-ne v0, v3, :cond_1

    .line 27
    and-int/lit16 v0, p0, 0xfff

    const/16 v3, 0xe1

    if-ne v0, v3, :cond_0

    move v1, v2

    :cond_0
    return v1

    .line 29
    :cond_1
    and-int/lit16 v0, p0, 0xfff

    const/16 v3, 0x81

    if-eq v0, v3, :cond_2

    and-int/lit16 v0, p0, 0xfff

    const/16 v3, 0x91

    if-eq v0, v3, :cond_2

    and-int/lit16 v0, p0, 0xfff

    const/16 v3, 0x12

    if-ne v0, v3, :cond_3

    :cond_2
    move v1, v2

    :cond_3
    return v1
.end method


# virtual methods
.method public init(Lcom/android/server/inputmethod/InputMethodManagerService;)V
    .locals 0
    .param p1, "service"    # Lcom/android/server/inputmethod/InputMethodManagerService;

    .line 22
    iput-object p1, p0, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    .line 23
    return-void
.end method
