public class com.android.server.inputmethod.SogouInputMethodSwitcher extends com.android.server.inputmethod.BaseInputMethodSwitcher {
	 /* .source "SogouInputMethodSwitcher.java" */
	 /* # static fields */
	 public static final Boolean DEBUG;
	 private static final java.lang.String PKG_SOGOU;
	 public static final java.lang.String TAG;
	 /* # direct methods */
	 public com.android.server.inputmethod.SogouInputMethodSwitcher ( ) {
		 /* .locals 0 */
		 /* .line 16 */
		 /* invoke-direct {p0}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;-><init>()V */
		 return;
	 } // .end method
	 private java.lang.String getSogouMethodIdLocked ( ) {
		 /* .locals 3 */
		 /* .line 166 */
		 v0 = this.mService;
		 v0 = this.mMethodMap;
		 (( android.util.ArrayMap ) v0 ).entrySet ( ); // invoke-virtual {v0}, Landroid/util/ArrayMap;->entrySet()Ljava/util/Set;
	 v1 = 	 } // :goto_0
	 if ( v1 != null) { // if-eqz v1, :cond_1
		 /* check-cast v1, Ljava/util/Map$Entry; */
		 /* .line 167 */
		 /* .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Landroid/view/inputmethod/InputMethodInfo;>;" */
		 /* check-cast v2, Ljava/lang/String; */
		 v2 = 		 /* invoke-direct {p0, v2}, Lcom/android/server/inputmethod/SogouInputMethodSwitcher;->isSogouMethodLocked(Ljava/lang/String;)Z */
		 if ( v2 != null) { // if-eqz v2, :cond_0
			 /* .line 168 */
			 /* check-cast v0, Ljava/lang/String; */
			 /* .line 170 */
		 } // .end local v1 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Landroid/view/inputmethod/InputMethodInfo;>;"
	 } // :cond_0
	 /* .line 171 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean isEditorInDefaultImeApp ( android.view.inputmethod.EditorInfo p0 ) {
/* .locals 4 */
/* .param p1, "editor" # Landroid/view/inputmethod/EditorInfo; */
/* .line 180 */
v0 = this.packageName;
/* .line 181 */
/* .local v0, "pkg":Ljava/lang/String; */
v1 = this.mService;
v1 = this.mSettings;
(( com.android.server.inputmethod.InputMethodUtils$InputMethodSettings ) v1 ).getSelectedInputMethod ( ); // invoke-virtual {v1}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getSelectedInputMethod()Ljava/lang/String;
/* .line 182 */
/* .local v1, "defaultIme":Ljava/lang/String; */
int v2 = 0; // const/4 v2, 0x0
/* .line 183 */
/* .local v2, "cn":Landroid/content/ComponentName; */
v3 = android.text.TextUtils .isEmpty ( v1 );
/* if-nez v3, :cond_0 */
/* .line 184 */
android.content.ComponentName .unflattenFromString ( v1 );
/* move-object v2, v3 */
if ( v3 != null) { // if-eqz v3, :cond_0
	 /* .line 185 */
	 (( android.content.ComponentName ) v2 ).getPackageName ( ); // invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
	 v3 = 	 android.text.TextUtils .equals ( v0,v3 );
	 if ( v3 != null) { // if-eqz v3, :cond_0
		 int v3 = 1; // const/4 v3, 0x1
	 } // :cond_0
	 int v3 = 0; // const/4 v3, 0x0
	 /* .line 183 */
} // :goto_0
} // .end method
private Boolean isSogouMethodLocked ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "methodId" # Ljava/lang/String; */
/* .line 175 */
v0 = this.mService;
v0 = this.mMethodMap;
(( android.util.ArrayMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Landroid/view/inputmethod/InputMethodInfo; */
/* .line 176 */
/* .local v0, "imi":Landroid/view/inputmethod/InputMethodInfo; */
if ( v0 != null) { // if-eqz v0, :cond_0
	 (( android.view.inputmethod.InputMethodInfo ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;
	 final String v2 = "com.sohu.inputmethod.sogou.xiaomi"; // const-string v2, "com.sohu.inputmethod.sogou.xiaomi"
	 v1 = 	 android.text.TextUtils .equals ( v1,v2 );
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 int v1 = 1; // const/4 v1, 0x1
	 } // :cond_0
	 int v1 = 0; // const/4 v1, 0x0
} // :goto_0
} // .end method
/* # virtual methods */
public java.util.List filterMethodLocked ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Landroid/view/inputmethod/InputMethodInfo;", */
/* ">;)", */
/* "Ljava/util/List<", */
/* "Landroid/view/inputmethod/InputMethodInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 162 */
/* .local p1, "methodInfos":Ljava/util/List;, "Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;" */
} // .end method
public Boolean mayChangeInputMethodLocked ( android.view.inputmethod.EditorInfo p0 ) {
/* .locals 18 */
/* .param p1, "attribute" # Landroid/view/inputmethod/EditorInfo; */
/* .line 33 */
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p1 */
v2 = com.android.server.inputmethod.DeviceUtils .isFlipDevice ( );
int v3 = 0; // const/4 v3, 0x0
/* if-nez v2, :cond_0 */
/* .line 34 */
/* .line 37 */
} // :cond_0
/* iget v2, v1, Landroid/view/inputmethod/EditorInfo;->inputType:I */
v2 = com.android.server.inputmethod.SogouInputMethodSwitcher .isPasswdInputType ( v2 );
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 38 */
/* .line 41 */
} // :cond_1
v2 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v2 ).getSelectedMethodIdLocked ( ); // invoke-virtual {v2}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;
final String v4 = "SogouInputMethodSwitcher"; // const-string v4, "SogouInputMethodSwitcher"
/* if-nez v2, :cond_2 */
/* .line 42 */
final String v2 = "input_service has no current_method_id"; // const-string v2, "input_service has no current_method_id"
android.util.Slog .w ( v4,v2 );
/* .line 43 */
/* .line 46 */
} // :cond_2
/* if-nez v1, :cond_3 */
/* .line 47 */
final String v2 = "editor_info is null, we cannot judge"; // const-string v2, "editor_info is null, we cannot judge"
android.util.Slog .w ( v4,v2 );
/* .line 48 */
/* .line 51 */
} // :cond_3
v2 = this.mService;
v2 = this.mMethodMap;
v5 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v5 ).getSelectedMethodIdLocked ( ); // invoke-virtual {v5}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;
(( android.util.ArrayMap ) v2 ).get ( v5 ); // invoke-virtual {v2, v5}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v2, Landroid/view/inputmethod/InputMethodInfo; */
/* .line 52 */
/* .local v2, "curMethodInfo":Landroid/view/inputmethod/InputMethodInfo; */
/* if-nez v2, :cond_4 */
/* .line 53 */
final String v5 = "fail to find current_method_info in the map"; // const-string v5, "fail to find current_method_info in the map"
android.util.Slog .w ( v4,v5 );
/* .line 54 */
/* .line 57 */
} // :cond_4
com.android.server.inputmethod.InputMethodManagerServiceImpl .getInstance ( );
v5 = this.mBindingController;
/* if-nez v5, :cond_5 */
/* .line 58 */
final String v5 = "IMMS_IMPL has not init"; // const-string v5, "IMMS_IMPL has not init"
android.util.Slog .w ( v4,v5 );
/* .line 59 */
/* .line 62 */
} // :cond_5
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/inputmethod/SogouInputMethodSwitcher;->getSogouMethodIdLocked()Ljava/lang/String; */
/* .line 63 */
/* .local v5, "sogouMethodId":Ljava/lang/String; */
v6 = android.text.TextUtils .isEmpty ( v5 );
if ( v6 != null) { // if-eqz v6, :cond_6
/* .line 64 */
final String v6 = "fail to find sogou_input_method in input_method_list"; // const-string v6, "fail to find sogou_input_method in input_method_list"
android.util.Slog .w ( v4,v6 );
/* .line 65 */
/* .line 68 */
} // :cond_6
v6 = /* invoke-direct/range {p0 ..p1}, Lcom/android/server/inputmethod/SogouInputMethodSwitcher;->isEditorInDefaultImeApp(Landroid/view/inputmethod/EditorInfo;)Z */
/* .line 69 */
/* .local v6, "editorInDefaultImeApp":Z */
if ( v6 != null) { // if-eqz v6, :cond_7
/* .line 70 */
final String v7 = "editor in default ime app"; // const-string v7, "editor in default ime app"
android.util.Slog .w ( v4,v7 );
/* .line 71 */
/* .line 74 */
} // :cond_7
v7 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v7 ).getSelectedMethodIdLocked ( ); // invoke-virtual {v7}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;
v7 = /* invoke-direct {v0, v7}, Lcom/android/server/inputmethod/SogouInputMethodSwitcher;->isSogouMethodLocked(Ljava/lang/String;)Z */
/* .line 75 */
/* .local v7, "isSogouInputNow":Z */
v8 = this.mService;
v8 = this.mContext;
v8 = com.android.server.inputmethod.DeviceUtils .isFlipTinyScreen ( v8 );
/* .line 77 */
/* .local v8, "isFlipTinyScreen":Z */
int v9 = 1; // const/4 v9, 0x1
/* if-nez v7, :cond_8 */
if ( v8 != null) { // if-eqz v8, :cond_8
/* move v10, v9 */
} // :cond_8
/* move v10, v3 */
/* .line 78 */
/* .local v10, "switchToSogouInput":Z */
} // :goto_0
if ( v7 != null) { // if-eqz v7, :cond_9
/* if-nez v8, :cond_9 */
/* move v11, v9 */
} // :cond_9
/* move v11, v3 */
/* .line 80 */
/* .local v11, "switchFromSogouInput":Z */
} // :goto_1
int v12 = -1; // const/4 v12, -0x1
int v13 = 2; // const/4 v13, 0x2
if ( v10 != null) { // if-eqz v10, :cond_a
/* .line 81 */
com.android.server.inputmethod.InputMethodManagerServiceImpl .getInstance ( );
v3 = this.mBindingController;
/* .line 82 */
(( com.android.server.inputmethod.InputMethodBindingController ) v3 ).setSelectedMethodId ( v5 ); // invoke-virtual {v3, v5}, Lcom/android/server/inputmethod/InputMethodBindingController;->setSelectedMethodId(Ljava/lang/String;)V
/* .line 83 */
v3 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v3 ).clearClientSessionsLocked ( ); // invoke-virtual {v3}, Lcom/android/server/inputmethod/InputMethodManagerService;->clearClientSessionsLocked()V
/* .line 84 */
v3 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v3 ).unbindCurrentClientLocked ( v13 ); // invoke-virtual {v3, v13}, Lcom/android/server/inputmethod/InputMethodManagerService;->unbindCurrentClientLocked(I)V
/* .line 85 */
com.android.server.inputmethod.InputMethodManagerServiceImpl .getInstance ( );
v3 = this.mBindingController;
(( com.android.server.inputmethod.InputMethodBindingController ) v3 ).unbindCurrentMethod ( ); // invoke-virtual {v3}, Lcom/android/server/inputmethod/InputMethodBindingController;->unbindCurrentMethod()V
/* .line 86 */
v3 = this.mService;
v4 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v4 ).getSelectedMethodIdLocked ( ); // invoke-virtual {v4}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;
(( com.android.server.inputmethod.InputMethodManagerService ) v3 ).setInputMethodLocked ( v4, v12 ); // invoke-virtual {v3, v4, v12}, Lcom/android/server/inputmethod/InputMethodManagerService;->setInputMethodLocked(Ljava/lang/String;I)V
/* .line 87 */
/* .line 89 */
} // :cond_a
if ( v11 != null) { // if-eqz v11, :cond_12
/* .line 90 */
v14 = this.mService;
v14 = this.mSettings;
(( com.android.server.inputmethod.InputMethodUtils$InputMethodSettings ) v14 ).getSelectedInputMethod ( ); // invoke-virtual {v14}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getSelectedInputMethod()Ljava/lang/String;
/* .line 91 */
/* .local v14, "selectedInputMethod":Ljava/lang/String; */
v15 = android.text.TextUtils .isEmpty ( v14 );
if ( v15 != null) { // if-eqz v15, :cond_f
/* .line 92 */
/* const-string/jumbo v15, "something is weired, maybe the input method app are uninstalled" */
android.util.Slog .w ( v4,v15 );
/* .line 93 */
v15 = this.mService;
v15 = this.mSettings;
/* .line 94 */
(( com.android.server.inputmethod.InputMethodUtils$InputMethodSettings ) v15 ).getEnabledInputMethodListLocked ( ); // invoke-virtual {v15}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getEnabledInputMethodListLocked()Ljava/util/ArrayList;
/* .line 93 */
com.android.server.inputmethod.InputMethodInfoUtils .getMostApplicableDefaultIME ( v15 );
/* .line 95 */
/* .local v15, "imi":Landroid/view/inputmethod/InputMethodInfo; */
final String v9 = "com.sohu.inputmethod.sogou.xiaomi"; // const-string v9, "com.sohu.inputmethod.sogou.xiaomi"
if ( v15 != null) { // if-eqz v15, :cond_b
(( android.view.inputmethod.InputMethodInfo ) v15 ).getPackageName ( ); // invoke-virtual {v15}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;
v12 = android.text.TextUtils .equals ( v12,v9 );
if ( v12 != null) { // if-eqz v12, :cond_f
/* .line 96 */
} // :cond_b
final String v12 = "fail to find a most applicable default ime"; // const-string v12, "fail to find a most applicable default ime"
android.util.Slog .w ( v4,v12 );
/* .line 97 */
v12 = this.mService;
v12 = this.mSettings;
(( com.android.server.inputmethod.InputMethodUtils$InputMethodSettings ) v12 ).getEnabledInputMethodListLocked ( ); // invoke-virtual {v12}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getEnabledInputMethodListLocked()Ljava/util/ArrayList;
/* .line 98 */
/* .local v12, "imiList":Ljava/util/List;, "Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;" */
v16 = if ( v12 != null) { // if-eqz v12, :cond_e
/* if-nez v16, :cond_c */
/* .line 102 */
} // :cond_c
} // :goto_2
v17 = /* invoke-interface/range {v16 ..v16}, Ljava/util/Iterator;->hasNext()Z */
if ( v17 != null) { // if-eqz v17, :cond_f
/* invoke-interface/range {v16 ..v16}, Ljava/util/Iterator;->next()Ljava/lang/Object; */
/* check-cast v17, Landroid/view/inputmethod/InputMethodInfo; */
/* .line 103 */
/* .local v17, "inputMethodInfo":Landroid/view/inputmethod/InputMethodInfo; */
/* invoke-virtual/range {v17 ..v17}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String; */
v13 = android.text.TextUtils .equals ( v13,v9 );
/* if-nez v13, :cond_d */
/* .line 104 */
/* invoke-virtual/range {v17 ..v17}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String; */
/* .line 105 */
/* .line 107 */
} // .end local v17 # "inputMethodInfo":Landroid/view/inputmethod/InputMethodInfo;
} // :cond_d
int v13 = 2; // const/4 v13, 0x2
/* .line 99 */
} // :cond_e
} // :goto_3
/* const-string/jumbo v9, "there is no enabled method list" */
android.util.Slog .w ( v4,v9 );
/* .line 100 */
/* .line 111 */
} // .end local v12 # "imiList":Ljava/util/List;, "Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;"
} // .end local v15 # "imi":Landroid/view/inputmethod/InputMethodInfo;
} // :cond_f
} // :goto_4
v9 = android.text.TextUtils .isEmpty ( v14 );
if ( v9 != null) { // if-eqz v9, :cond_10
/* .line 112 */
final String v9 = "finally, we still fail to find default input method"; // const-string v9, "finally, we still fail to find default input method"
android.util.Slog .w ( v4,v9 );
/* .line 113 */
/* .line 116 */
} // :cond_10
v9 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v9 ).getSelectedMethodIdLocked ( ); // invoke-virtual {v9}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;
v9 = android.text.TextUtils .equals ( v9,v14 );
if ( v9 != null) { // if-eqz v9, :cond_11
/* .line 117 */
final String v9 = "It looks like there is sogou_input_method in the system"; // const-string v9, "It looks like there is sogou_input_method in the system"
android.util.Slog .w ( v4,v9 );
/* .line 118 */
/* .line 121 */
} // :cond_11
com.android.server.inputmethod.InputMethodManagerServiceImpl .getInstance ( );
v3 = this.mBindingController;
/* .line 122 */
(( com.android.server.inputmethod.InputMethodBindingController ) v3 ).setSelectedMethodId ( v14 ); // invoke-virtual {v3, v14}, Lcom/android/server/inputmethod/InputMethodBindingController;->setSelectedMethodId(Ljava/lang/String;)V
/* .line 123 */
v3 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v3 ).clearClientSessionsLocked ( ); // invoke-virtual {v3}, Lcom/android/server/inputmethod/InputMethodManagerService;->clearClientSessionsLocked()V
/* .line 124 */
v3 = this.mService;
int v4 = 2; // const/4 v4, 0x2
(( com.android.server.inputmethod.InputMethodManagerService ) v3 ).unbindCurrentClientLocked ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/inputmethod/InputMethodManagerService;->unbindCurrentClientLocked(I)V
/* .line 125 */
com.android.server.inputmethod.InputMethodManagerServiceImpl .getInstance ( );
v3 = this.mBindingController;
(( com.android.server.inputmethod.InputMethodBindingController ) v3 ).unbindCurrentMethod ( ); // invoke-virtual {v3}, Lcom/android/server/inputmethod/InputMethodBindingController;->unbindCurrentMethod()V
/* .line 126 */
v3 = this.mService;
v4 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v4 ).getSelectedMethodIdLocked ( ); // invoke-virtual {v4}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;
int v9 = -1; // const/4 v9, -0x1
(( com.android.server.inputmethod.InputMethodManagerService ) v3 ).setInputMethodLocked ( v4, v9 ); // invoke-virtual {v3, v4, v9}, Lcom/android/server/inputmethod/InputMethodManagerService;->setInputMethodLocked(Ljava/lang/String;I)V
/* .line 127 */
int v3 = 1; // const/4 v3, 0x1
/* .line 129 */
} // .end local v14 # "selectedInputMethod":Ljava/lang/String;
} // :cond_12
} // .end method
public void onSwitchUserLocked ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "newUserId" # I */
/* .line 29 */
return;
} // .end method
public void onSystemRunningLocked ( ) {
/* .locals 0 */
/* .line 24 */
return;
} // .end method
public void removeMethod ( java.util.List p0 ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 144 */
/* .local p1, "imList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;>;" */
v0 = this.mService;
v0 = this.mContext;
v0 = com.android.server.inputmethod.DeviceUtils .isFlipTinyScreen ( v0 );
/* if-nez v0, :cond_0 */
/* .line 145 */
return;
/* .line 147 */
} // :cond_0
v0 = if ( p1 != null) { // if-eqz p1, :cond_4
/* if-nez v0, :cond_1 */
/* .line 150 */
} // :cond_1
/* .line 151 */
/* .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;>;" */
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 152 */
/* check-cast v1, Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem; */
/* .line 153 */
/* .local v1, "imeSubtypeListItem":Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem; */
v2 = this.mImi;
/* .line 154 */
/* .local v2, "imi":Landroid/view/inputmethod/InputMethodInfo; */
(( android.view.inputmethod.InputMethodInfo ) v2 ).getPackageName ( ); // invoke-virtual {v2}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;
final String v4 = "com.sohu.inputmethod.sogou.xiaomi"; // const-string v4, "com.sohu.inputmethod.sogou.xiaomi"
v3 = android.text.TextUtils .equals ( v3,v4 );
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 155 */
/* .line 157 */
} // .end local v1 # "imeSubtypeListItem":Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;
} // .end local v2 # "imi":Landroid/view/inputmethod/InputMethodInfo;
} // :cond_2
/* .line 158 */
} // :cond_3
return;
/* .line 148 */
} // .end local v0 # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;>;"
} // :cond_4
} // :goto_1
return;
} // .end method
public Boolean shouldHideImeSwitcherLocked ( ) {
/* .locals 2 */
/* .line 134 */
v0 = this.mService;
v0 = this.mContext;
v0 = com.android.server.inputmethod.DeviceUtils .isFlipTinyScreen ( v0 );
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 135 */
/* .line 137 */
} // :cond_0
v0 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v0 ).getSelectedMethodIdLocked ( ); // invoke-virtual {v0}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/inputmethod/SogouInputMethodSwitcher;->isSogouMethodLocked(Ljava/lang/String;)Z */
/* if-nez v0, :cond_2 */
/* .line 138 */
com.android.server.inputmethod.InputMethodManagerServiceImpl .getInstance ( );
v0 = this.mBindingController;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 139 */
com.android.server.inputmethod.InputMethodManagerServiceImpl .getInstance ( );
v0 = this.mBindingController;
(( com.android.server.inputmethod.InputMethodBindingController ) v0 ).getCurMethod ( ); // invoke-virtual {v0}, Lcom/android/server/inputmethod/InputMethodBindingController;->getCurMethod()Lcom/android/server/inputmethod/IInputMethodInvoker;
/* if-nez v0, :cond_1 */
} // :cond_1
} // :cond_2
} // :goto_0
int v1 = 1; // const/4 v1, 0x1
/* .line 137 */
} // :goto_1
} // .end method
