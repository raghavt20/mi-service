.class public Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;
.super Lcom/android/server/inputmethod/BaseInputMethodSwitcher;
.source "SecurityInputMethodSwitcher.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/inputmethod/SecurityInputMethodSwitcher$SettingsObserver;
    }
.end annotation


# static fields
.field public static final DEBUG:Z = true

.field public static final MIUI_SEC_INPUT_METHOD_APP_PKG_NAME:Ljava/lang/String; = "com.miui.securityinputmethod"

.field public static final SUPPORT_SEC_INPUT_METHOD:Z

.field public static final TAG:Ljava/lang/String; = "SecurityInputMethodSwitcher"


# instance fields
.field private mSecEnabled:Z

.field private mSettingsObserver:Lcom/android/server/inputmethod/SecurityInputMethodSwitcher$SettingsObserver;


# direct methods
.method static bridge synthetic -$$Nest$fgetmSecEnabled(Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mSecEnabled:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmSecEnabled(Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mSecEnabled:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetParentUserId(Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;I)I
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->getParentUserId(I)I

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$misSecEnabled(Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;I)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->isSecEnabled(I)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mupdateFromSettingsLocked(Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->updateFromSettingsLocked()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 36
    nop

    .line 37
    const-string v0, "ro.miui.has_security_keyboard"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    sget-boolean v0, Lmiui/os/Build;->IS_GLOBAL_BUILD:Z

    if-nez v0, :cond_0

    move v1, v2

    :cond_0
    sput-boolean v1, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->SUPPORT_SEC_INPUT_METHOD:Z

    .line 36
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 33
    invoke-direct {p0}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;-><init>()V

    return-void
.end method

.method private getParentUserId(I)I
    .locals 3
    .param p1, "userId"    # I

    .line 277
    if-nez p1, :cond_0

    .line 278
    return p1

    .line 280
    :cond_0
    iget-object v0, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v0, v0, Lcom/android/server/inputmethod/InputMethodManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/os/UserManager;->get(Landroid/content/Context;)Landroid/os/UserManager;

    move-result-object v0

    .line 281
    .local v0, "userManager":Landroid/os/UserManager;
    invoke-virtual {v0, p1}, Landroid/os/UserManager;->getProfileParent(I)Landroid/content/pm/UserInfo;

    move-result-object v1

    .line 282
    .local v1, "parentUserInfo":Landroid/content/pm/UserInfo;
    if-eqz v1, :cond_1

    iget v2, v1, Landroid/content/pm/UserInfo;->id:I

    goto :goto_0

    :cond_1
    move v2, p1

    :goto_0
    return v2
.end method

.method private getSecMethodIdLocked()Ljava/lang/String;
    .locals 3

    .line 292
    iget-object v0, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v0, v0, Lcom/android/server/inputmethod/InputMethodManagerService;->mMethodMap:Landroid/util/ArrayMap;

    invoke-virtual {v0}, Landroid/util/ArrayMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 293
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Landroid/view/inputmethod/InputMethodInfo;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->isSecMethodLocked(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 294
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0

    .line 296
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Landroid/view/inputmethod/InputMethodInfo;>;"
    :cond_0
    goto :goto_0

    .line 297
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method private isEditorInDefaultImeApp(Landroid/view/inputmethod/EditorInfo;)Z
    .locals 4
    .param p1, "editor"    # Landroid/view/inputmethod/EditorInfo;

    .line 309
    iget-object v0, p1, Landroid/view/inputmethod/EditorInfo;->packageName:Ljava/lang/String;

    .line 310
    .local v0, "pkg":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v1, v1, Lcom/android/server/inputmethod/InputMethodManagerService;->mSettings:Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;

    invoke-virtual {v1}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getSelectedInputMethod()Ljava/lang/String;

    move-result-object v1

    .line 311
    .local v1, "defaultIme":Ljava/lang/String;
    const/4 v2, 0x0

    .line 312
    .local v2, "cn":Landroid/content/ComponentName;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 313
    invoke-static {v1}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v3

    move-object v2, v3

    if-eqz v3, :cond_0

    .line 314
    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    .line 312
    :goto_0
    return v3
.end method

.method private isSecEnabled(I)Z
    .locals 3
    .param p1, "userId"    # I

    .line 270
    iget-object v0, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v0, v0, Lcom/android/server/inputmethod/InputMethodManagerService;->mContext:Landroid/content/Context;

    .line 271
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 270
    const-string v1, "enable_miui_security_ime"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2, p1}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    return v2
.end method

.method private isSecMethodLocked(Ljava/lang/String;)Z
    .locals 3
    .param p1, "methodId"    # Ljava/lang/String;

    .line 286
    iget-object v0, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v0, v0, Lcom/android/server/inputmethod/InputMethodManagerService;->mMethodMap:Landroid/util/ArrayMap;

    invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodInfo;

    .line 287
    .local v0, "imi":Landroid/view/inputmethod/InputMethodInfo;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.miui.securityinputmethod"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private updateFromSettingsLocked()V
    .locals 2

    .line 301
    iget-object v0, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v0}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mSecEnabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    .line 302
    invoke-virtual {v0}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;

    move-result-object v0

    .line 301
    invoke-direct {p0, v0}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->isSecMethodLocked(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 303
    iget-object v0, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v0}, Lcom/android/server/inputmethod/InputMethodManagerService;->clearClientSessionsLocked()V

    .line 304
    iget-object v0, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/server/inputmethod/InputMethodManagerService;->unbindCurrentClientLocked(I)V

    .line 306
    :cond_0
    return-void
.end method


# virtual methods
.method public filterMethodLocked(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/view/inputmethod/InputMethodInfo;",
            ">;)",
            "Ljava/util/List<",
            "Landroid/view/inputmethod/InputMethodInfo;",
            ">;"
        }
    .end annotation

    .line 209
    .local p1, "methodInfos":Ljava/util/List;, "Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;"
    sget-boolean v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->SUPPORT_SEC_INPUT_METHOD:Z

    if-nez v0, :cond_0

    .line 210
    return-object p1

    .line 212
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 213
    .local v0, "noSecMethodInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/inputmethod/InputMethodInfo;>;"
    if-eqz p1, :cond_2

    .line 214
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/inputmethod/InputMethodInfo;

    .line 215
    .local v2, "methodInfo":Landroid/view/inputmethod/InputMethodInfo;
    invoke-virtual {v2}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->isSecMethodLocked(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 216
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 218
    .end local v2    # "methodInfo":Landroid/view/inputmethod/InputMethodInfo;
    :cond_1
    goto :goto_0

    .line 220
    :cond_2
    return-object v0
.end method

.method public mayChangeInputMethodLocked(Landroid/view/inputmethod/EditorInfo;I)Z
    .locals 17
    .param p1, "attribute"    # Landroid/view/inputmethod/EditorInfo;
    .param p2, "startInputReason"    # I

    .line 68
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    sget-boolean v3, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->SUPPORT_SEC_INPUT_METHOD:Z

    const/4 v4, 0x0

    if-nez v3, :cond_0

    .line 69
    return v4

    .line 71
    :cond_0
    iget-object v3, v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v3}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;

    move-result-object v3

    const-string v5, "SecurityInputMethodSwitcher"

    if-nez v3, :cond_1

    .line 72
    const-string v3, "input_service has no current_method_id"

    invoke-static {v5, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    return v4

    .line 75
    :cond_1
    if-nez v1, :cond_2

    .line 76
    const-string v3, "editor_info is null, we cannot judge"

    invoke-static {v5, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    return v4

    .line 79
    :cond_2
    invoke-static {}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->getInstance()Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    move-result-object v3

    iget-object v3, v3, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mBindingController:Lcom/android/server/inputmethod/InputMethodBindingController;

    if-nez v3, :cond_3

    .line 80
    const-string v3, "IMMS_IMPL has not init"

    invoke-static {v5, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    return v4

    .line 83
    :cond_3
    iget-object v3, v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v3, v3, Lcom/android/server/inputmethod/InputMethodManagerService;->mMethodMap:Landroid/util/ArrayMap;

    iget-object v6, v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v6}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/inputmethod/InputMethodInfo;

    .line 84
    .local v3, "curMethodInfo":Landroid/view/inputmethod/InputMethodInfo;
    if-nez v3, :cond_4

    .line 85
    const-string v6, "fail to find current_method_info in the map"

    invoke-static {v5, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    return v4

    .line 89
    :cond_4
    iget v6, v1, Landroid/view/inputmethod/EditorInfo;->inputType:I

    invoke-static {v6}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->isPasswdInputType(I)Z

    move-result v6

    const/4 v7, 0x1

    if-eqz v6, :cond_5

    iget-object v6, v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    .line 90
    invoke-virtual {v6}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v6}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->isSecMethodLocked(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_5

    iget-boolean v6, v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mSecEnabled:Z

    if-eqz v6, :cond_5

    .line 91
    invoke-direct/range {p0 .. p0}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->getSecMethodIdLocked()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_5

    invoke-direct/range {p0 .. p1}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->isEditorInDefaultImeApp(Landroid/view/inputmethod/EditorInfo;)Z

    move-result v6

    if-nez v6, :cond_5

    move v6, v7

    goto :goto_0

    :cond_5
    move v6, v4

    .line 94
    .local v6, "switchToSecInput":Z
    :goto_0
    invoke-static {}, Lcom/android/server/inputmethod/MiuiInputMethodStub;->getInstance()Lcom/android/server/inputmethod/MiuiInputMethodStub;

    move-result-object v8

    invoke-interface {v8}, Lcom/android/server/inputmethod/MiuiInputMethodStub;->mayChangeToMiuiSecurityInputMethod()Z

    move-result v8

    if-eqz v8, :cond_6

    iget-object v8, v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    .line 95
    invoke-virtual {v8}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v0, v8}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->isSecMethodLocked(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_6

    iget v8, v1, Landroid/view/inputmethod/EditorInfo;->inputType:I

    if-eqz v8, :cond_6

    iget-boolean v8, v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mSecEnabled:Z

    if-eqz v8, :cond_6

    move v8, v7

    goto :goto_1

    :cond_6
    move v8, v4

    :goto_1
    or-int/2addr v6, v8

    .line 97
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "first check switch to security ime: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    if-nez v6, :cond_8

    .line 100
    invoke-static {}, Lcom/android/server/inputmethod/MiuiInputMethodStub;->getInstance()Lcom/android/server/inputmethod/MiuiInputMethodStub;

    move-result-object v8

    .line 101
    invoke-interface {v8}, Lcom/android/server/inputmethod/MiuiInputMethodStub;->mayChangeToMiuiSecurityInputMethod()Z

    move-result v8

    if-nez v8, :cond_7

    iget-object v8, v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    .line 102
    invoke-virtual {v8}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v0, v8}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->isSecMethodLocked(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_7

    .line 103
    invoke-static {}, Lcom/android/server/inputmethod/MiuiInputMethodStub;->getInstance()Lcom/android/server/inputmethod/MiuiInputMethodStub;

    move-result-object v8

    iget-object v9, v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v9, v9, Lcom/android/server/inputmethod/InputMethodManagerService;->mCurFocusedWindow:Landroid/os/IBinder;

    .line 104
    invoke-interface {v8, v9}, Lcom/android/server/inputmethod/MiuiInputMethodStub;->mayChangeToMiuiSecurityInputMethod(Landroid/os/IBinder;)Z

    move-result v8

    if-eqz v8, :cond_7

    iget v8, v1, Landroid/view/inputmethod/EditorInfo;->inputType:I

    if-eqz v8, :cond_7

    iget-boolean v8, v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mSecEnabled:Z

    if-eqz v8, :cond_7

    move v8, v7

    goto :goto_2

    :cond_7
    move v8, v4

    :goto_2
    or-int/2addr v6, v8

    .line 107
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "double check switch to security ime: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    :cond_8
    invoke-static {}, Lcom/android/server/inputmethod/MiuiInputMethodStub;->getInstance()Lcom/android/server/inputmethod/MiuiInputMethodStub;

    move-result-object v8

    invoke-interface {v8}, Lcom/android/server/inputmethod/MiuiInputMethodStub;->isInputMethodChangedBySelf()Z

    move-result v8

    const/16 v9, 0x8

    if-eqz v8, :cond_a

    iget v8, v1, Landroid/view/inputmethod/EditorInfo;->inputType:I

    .line 110
    invoke-static {v8}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->isPasswdInputType(I)Z

    move-result v8

    if-nez v8, :cond_a

    const/16 v8, 0x9

    if-eq v2, v8, :cond_9

    if-ne v2, v9, :cond_a

    .line 113
    :cond_9
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "switch to the security ime byself: "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, " startInputReason: "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    const/4 v6, 0x0

    .line 117
    :cond_a
    if-ne v2, v9, :cond_b

    .line 118
    invoke-static {}, Lcom/android/server/inputmethod/MiuiInputMethodStub;->getInstance()Lcom/android/server/inputmethod/MiuiInputMethodStub;

    move-result-object v8

    invoke-interface {v8}, Lcom/android/server/inputmethod/MiuiInputMethodStub;->clearInputMethodChangedBySelf()V

    .line 120
    :cond_b
    iget-object v8, v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v8}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v0, v8}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->isSecMethodLocked(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_d

    iget-boolean v8, v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mSecEnabled:Z

    if-eqz v8, :cond_c

    iget v8, v1, Landroid/view/inputmethod/EditorInfo;->inputType:I

    .line 121
    invoke-static {v8}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->isPasswdInputType(I)Z

    move-result v8

    if-eqz v8, :cond_c

    .line 122
    invoke-direct/range {p0 .. p1}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->isEditorInDefaultImeApp(Landroid/view/inputmethod/EditorInfo;)Z

    move-result v8

    if-eqz v8, :cond_d

    :cond_c
    move v8, v7

    goto :goto_3

    :cond_d
    move v8, v4

    .line 123
    .local v8, "switchFromSecInput":Z
    :goto_3
    nop

    .line 124
    invoke-static {}, Lcom/android/server/inputmethod/MiuiInputMethodStub;->getInstance()Lcom/android/server/inputmethod/MiuiInputMethodStub;

    move-result-object v9

    invoke-interface {v9}, Lcom/android/server/inputmethod/MiuiInputMethodStub;->mayChangeToMiuiSecurityInputMethod()Z

    move-result v9

    if-nez v9, :cond_e

    iget-object v9, v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v9, v9, Lcom/android/server/inputmethod/InputMethodManagerService;->mCurFocusedWindow:Landroid/os/IBinder;

    .line 125
    invoke-virtual {v0, v9}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mayChangeToMiuiSecurityInputMethod(Landroid/os/IBinder;)Z

    move-result v9

    if-nez v9, :cond_e

    move v9, v7

    goto :goto_4

    :cond_e
    move v9, v4

    :goto_4
    and-int/2addr v8, v9

    .line 126
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "switch to other input method from security ime:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " mayChangeToMiuiSecurityInputMethod: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 127
    invoke-static {}, Lcom/android/server/inputmethod/MiuiInputMethodStub;->getInstance()Lcom/android/server/inputmethod/MiuiInputMethodStub;

    move-result-object v10

    iget-object v11, v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v11, v11, Lcom/android/server/inputmethod/InputMethodManagerService;->mCurFocusedWindow:Landroid/os/IBinder;

    .line 128
    invoke-interface {v10, v11}, Lcom/android/server/inputmethod/MiuiInputMethodStub;->mayChangeToMiuiSecurityInputMethod(Landroid/os/IBinder;)Z

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " current focus window: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v10, v10, Lcom/android/server/inputmethod/InputMethodManagerService;->mCurFocusedWindow:Landroid/os/IBinder;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 126
    invoke-static {v5, v9}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    const/4 v9, -0x1

    const/4 v10, 0x2

    if-eqz v6, :cond_10

    .line 131
    invoke-direct/range {p0 .. p0}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->getSecMethodIdLocked()Ljava/lang/String;

    move-result-object v11

    .line 132
    .local v11, "secInputMethodId":Ljava/lang/String;
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_f

    .line 133
    const-string v7, "fail to find secure_input_method in input_method_list"

    invoke-static {v5, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    return v4

    .line 136
    :cond_f
    invoke-static {}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->getInstance()Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    move-result-object v4

    iget-object v4, v4, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mBindingController:Lcom/android/server/inputmethod/InputMethodBindingController;

    .line 137
    invoke-virtual {v4, v11}, Lcom/android/server/inputmethod/InputMethodBindingController;->setSelectedMethodId(Ljava/lang/String;)V

    .line 138
    iget-object v4, v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v4}, Lcom/android/server/inputmethod/InputMethodManagerService;->clearClientSessionsLocked()V

    .line 139
    iget-object v4, v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v4, v10}, Lcom/android/server/inputmethod/InputMethodManagerService;->unbindCurrentClientLocked(I)V

    .line 140
    invoke-static {}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->getInstance()Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    move-result-object v4

    iget-object v4, v4, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mBindingController:Lcom/android/server/inputmethod/InputMethodBindingController;

    invoke-virtual {v4}, Lcom/android/server/inputmethod/InputMethodBindingController;->unbindCurrentMethod()V

    .line 141
    iget-object v4, v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v5, v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v5}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v9}, Lcom/android/server/inputmethod/InputMethodManagerService;->setInputMethodLocked(Ljava/lang/String;I)V

    .line 142
    return v7

    .line 143
    .end local v11    # "secInputMethodId":Ljava/lang/String;
    :cond_10
    if-eqz v8, :cond_18

    .line 144
    iget-object v11, v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v11, v11, Lcom/android/server/inputmethod/InputMethodManagerService;->mSettings:Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;

    invoke-virtual {v11}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getSelectedInputMethod()Ljava/lang/String;

    move-result-object v11

    .line 145
    .local v11, "selectedInputMethod":Ljava/lang/String;
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_15

    .line 146
    const-string/jumbo v12, "something is weired, maybe the input method app are uninstalled"

    invoke-static {v5, v12}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    iget-object v12, v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v12, v12, Lcom/android/server/inputmethod/InputMethodManagerService;->mSettings:Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;

    .line 148
    invoke-virtual {v12}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getEnabledInputMethodListLocked()Ljava/util/ArrayList;

    move-result-object v12

    .line 147
    invoke-static {v12}, Lcom/android/server/inputmethod/InputMethodInfoUtils;->getMostApplicableDefaultIME(Ljava/util/List;)Landroid/view/inputmethod/InputMethodInfo;

    move-result-object v12

    .line 149
    .local v12, "imi":Landroid/view/inputmethod/InputMethodInfo;
    const-string v13, "com.miui.securityinputmethod"

    if-eqz v12, :cond_11

    invoke-virtual {v12}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14, v13}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_15

    .line 151
    :cond_11
    const-string v14, "fail to find a most applicable default ime"

    invoke-static {v5, v14}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    iget-object v14, v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v14, v14, Lcom/android/server/inputmethod/InputMethodManagerService;->mSettings:Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;

    .line 153
    invoke-virtual {v14}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getEnabledInputMethodListLocked()Ljava/util/ArrayList;

    move-result-object v14

    .line 154
    .local v14, "imiList":Ljava/util/List;, "Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;"
    if-eqz v14, :cond_14

    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v15

    if-nez v15, :cond_12

    goto :goto_6

    .line 158
    :cond_12
    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_5
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_15

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Landroid/view/inputmethod/InputMethodInfo;

    .line 159
    .local v16, "inputMethodInfo":Landroid/view/inputmethod/InputMethodInfo;
    invoke-virtual/range {v16 .. v16}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, v13}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_13

    .line 161
    invoke-virtual/range {v16 .. v16}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;

    move-result-object v11

    .line 162
    goto :goto_7

    .line 164
    .end local v16    # "inputMethodInfo":Landroid/view/inputmethod/InputMethodInfo;
    :cond_13
    const/4 v7, 0x1

    goto :goto_5

    .line 155
    :cond_14
    :goto_6
    const-string/jumbo v7, "there is no enabled method list"

    invoke-static {v5, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    return v4

    .line 167
    .end local v12    # "imi":Landroid/view/inputmethod/InputMethodInfo;
    .end local v14    # "imiList":Ljava/util/List;, "Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;"
    :cond_15
    :goto_7
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_16

    .line 168
    const-string v7, "finally, we still fail to find default input method"

    invoke-static {v5, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    return v4

    .line 171
    :cond_16
    iget-object v7, v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v7}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, v11}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_17

    .line 172
    const-string v7, "It looks like there is only miui_sec_input_method in the system"

    invoke-static {v5, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    return v4

    .line 175
    :cond_17
    invoke-static {}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->getInstance()Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    move-result-object v4

    iget-object v4, v4, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mBindingController:Lcom/android/server/inputmethod/InputMethodBindingController;

    .line 176
    invoke-virtual {v4, v11}, Lcom/android/server/inputmethod/InputMethodBindingController;->setSelectedMethodId(Ljava/lang/String;)V

    .line 177
    iget-object v4, v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v4}, Lcom/android/server/inputmethod/InputMethodManagerService;->clearClientSessionsLocked()V

    .line 178
    iget-object v4, v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v4, v10}, Lcom/android/server/inputmethod/InputMethodManagerService;->unbindCurrentClientLocked(I)V

    .line 179
    invoke-static {}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->getInstance()Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    move-result-object v4

    iget-object v4, v4, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mBindingController:Lcom/android/server/inputmethod/InputMethodBindingController;

    invoke-virtual {v4}, Lcom/android/server/inputmethod/InputMethodBindingController;->unbindCurrentMethod()V

    .line 180
    iget-object v4, v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v5, v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v5}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v9}, Lcom/android/server/inputmethod/InputMethodManagerService;->setInputMethodLocked(Ljava/lang/String;I)V

    .line 181
    const/4 v4, 0x1

    return v4

    .line 183
    .end local v11    # "selectedInputMethod":Ljava/lang/String;
    :cond_18
    return v4
.end method

.method public onSwitchUserLocked(I)V
    .locals 2
    .param p1, "newUserId"    # I

    .line 58
    sget-boolean v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->SUPPORT_SEC_INPUT_METHOD:Z

    if-nez v0, :cond_0

    .line 59
    return-void

    .line 61
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->getParentUserId(I)I

    move-result v0

    .line 62
    .local v0, "parentUserId":I
    iget-object v1, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mSettingsObserver:Lcom/android/server/inputmethod/SecurityInputMethodSwitcher$SettingsObserver;

    invoke-virtual {v1, v0}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher$SettingsObserver;->registerContentObserverLocked(I)V

    .line 63
    invoke-direct {p0, v0}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->isSecEnabled(I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mSecEnabled:Z

    .line 64
    return-void
.end method

.method public onSystemRunningLocked()V
    .locals 2

    .line 47
    sget-boolean v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->SUPPORT_SEC_INPUT_METHOD:Z

    if-nez v0, :cond_0

    .line 48
    return-void

    .line 50
    :cond_0
    new-instance v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher$SettingsObserver;

    invoke-static {}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->getInstance()Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    move-result-object v1

    iget-object v1, v1, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher$SettingsObserver;-><init>(Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mSettingsObserver:Lcom/android/server/inputmethod/SecurityInputMethodSwitcher$SettingsObserver;

    .line 51
    iget-object v0, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v0, v0, Lcom/android/server/inputmethod/InputMethodManagerService;->mSettings:Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;

    invoke-virtual {v0}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getCurrentUserId()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->getParentUserId(I)I

    move-result v0

    .line 52
    .local v0, "parentUserId":I
    iget-object v1, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mSettingsObserver:Lcom/android/server/inputmethod/SecurityInputMethodSwitcher$SettingsObserver;

    invoke-virtual {v1, v0}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher$SettingsObserver;->registerContentObserverLocked(I)V

    .line 53
    invoke-direct {p0, v0}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->isSecEnabled(I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mSecEnabled:Z

    .line 54
    return-void
.end method

.method public removeMethod(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;",
            ">;)V"
        }
    .end annotation

    .line 195
    .local p1, "imList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;>;"
    sget-boolean v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->SUPPORT_SEC_INPUT_METHOD:Z

    if-eqz v0, :cond_3

    if-eqz p1, :cond_3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 198
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 199
    .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 200
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;

    .line 201
    .local v1, "imeSubtypeListItem":Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;
    iget-object v2, v1, Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;->mImi:Landroid/view/inputmethod/InputMethodInfo;

    .line 202
    .local v2, "imi":Landroid/view/inputmethod/InputMethodInfo;
    invoke-virtual {v2}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.miui.securityinputmethod"

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 203
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 205
    .end local v1    # "imeSubtypeListItem":Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;
    .end local v2    # "imi":Landroid/view/inputmethod/InputMethodInfo;
    :cond_1
    goto :goto_0

    .line 206
    :cond_2
    return-void

    .line 196
    .end local v0    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;>;"
    :cond_3
    :goto_1
    return-void
.end method

.method public shouldHideImeSwitcherLocked()Z
    .locals 1

    .line 188
    sget-boolean v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->SUPPORT_SEC_INPUT_METHOD:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v0}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->isSecMethodLocked(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 189
    :cond_0
    invoke-static {}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->getInstance()Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    move-result-object v0

    iget-object v0, v0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mBindingController:Lcom/android/server/inputmethod/InputMethodBindingController;

    if-eqz v0, :cond_2

    .line 190
    invoke-static {}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->getInstance()Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    move-result-object v0

    iget-object v0, v0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mBindingController:Lcom/android/server/inputmethod/InputMethodBindingController;

    invoke-virtual {v0}, Lcom/android/server/inputmethod/InputMethodBindingController;->getCurMethod()Lcom/android/server/inputmethod/IInputMethodInvoker;

    move-result-object v0

    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    .line 188
    :goto_0
    return v0
.end method
