public class com.android.server.inputmethod.MiuiSecurityInputMethodHelper {
	 /* .source "MiuiSecurityInputMethodHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper$SettingsObserver; */
	 /* } */
} // .end annotation
/* # static fields */
public static final Boolean DEBUG;
public static final java.lang.String MIUI_SEC_INPUT_METHOD_APP_PKG_NAME;
public static final Integer NUMBER_PASSWORD;
public static final Boolean SUPPORT_SEC_INPUT_METHOD;
public static final java.lang.String TAG;
public static final Integer TEXT_MASK;
public static final Integer TEXT_PASSWORD;
public static final Integer TEXT_VISIBLE_PASSWORD;
public static final Integer TEXT_WEB_PASSWORD;
public static final Integer WEB_EDIT_TEXT;
/* # instance fields */
private Boolean mSecEnabled;
private com.android.server.inputmethod.InputMethodManagerService mService;
private com.android.server.inputmethod.MiuiSecurityInputMethodHelper$SettingsObserver mSettingsObserver;
/* # direct methods */
static Boolean -$$Nest$fgetmSecEnabled ( com.android.server.inputmethod.MiuiSecurityInputMethodHelper p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->mSecEnabled:Z */
} // .end method
static com.android.server.inputmethod.InputMethodManagerService -$$Nest$fgetmService ( com.android.server.inputmethod.MiuiSecurityInputMethodHelper p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mService;
} // .end method
static void -$$Nest$fputmSecEnabled ( com.android.server.inputmethod.MiuiSecurityInputMethodHelper p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->mSecEnabled:Z */
	 return;
} // .end method
static void -$$Nest$mupdateFromSettingsLocked ( com.android.server.inputmethod.MiuiSecurityInputMethodHelper p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->updateFromSettingsLocked()V */
	 return;
} // .end method
static com.android.server.inputmethod.MiuiSecurityInputMethodHelper ( ) {
	 /* .locals 3 */
	 /* .line 33 */
	 /* nop */
	 /* .line 34 */
	 final String v0 = "ro.miui.has_security_keyboard"; // const-string v0, "ro.miui.has_security_keyboard"
	 int v1 = 0; // const/4 v1, 0x0
	 v0 = 	 android.os.SystemProperties .getInt ( v0,v1 );
	 int v2 = 1; // const/4 v2, 0x1
	 /* if-ne v0, v2, :cond_0 */
	 /* sget-boolean v0, Lmiui/os/Build;->IS_GLOBAL_BUILD:Z */
	 /* if-nez v0, :cond_0 */
	 /* move v1, v2 */
} // :cond_0
com.android.server.inputmethod.MiuiSecurityInputMethodHelper.SUPPORT_SEC_INPUT_METHOD = (v1!= 0);
/* .line 33 */
return;
} // .end method
public com.android.server.inputmethod.MiuiSecurityInputMethodHelper ( ) {
/* .locals 0 */
/* .param p1, "service" # Lcom/android/server/inputmethod/InputMethodManagerService; */
/* .line 55 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 56 */
this.mService = p1;
/* .line 57 */
return;
} // .end method
private java.lang.String getSecMethodIdLocked ( ) {
/* .locals 3 */
/* .line 215 */
v0 = this.mService;
v0 = this.mMethodMap;
(( android.util.ArrayMap ) v0 ).entrySet ( ); // invoke-virtual {v0}, Landroid/util/ArrayMap;->entrySet()Ljava/util/Set;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Ljava/util/Map$Entry; */
/* .line 216 */
/* .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Landroid/view/inputmethod/InputMethodInfo;>;" */
/* check-cast v2, Ljava/lang/String; */
v2 = /* invoke-direct {p0, v2}, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->isSecMethodLocked(Ljava/lang/String;)Z */
if ( v2 != null) { // if-eqz v2, :cond_0
	 /* .line 217 */
	 /* check-cast v0, Ljava/lang/String; */
	 /* .line 219 */
} // .end local v1 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Landroid/view/inputmethod/InputMethodInfo;>;"
} // :cond_0
/* .line 220 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean isEditorInDefaultImeApp ( android.view.inputmethod.EditorInfo p0 ) {
/* .locals 4 */
/* .param p1, "editor" # Landroid/view/inputmethod/EditorInfo; */
/* .line 268 */
v0 = this.packageName;
/* .line 269 */
/* .local v0, "pkg":Ljava/lang/String; */
v1 = this.mService;
v1 = this.mSettings;
(( com.android.server.inputmethod.InputMethodUtils$InputMethodSettings ) v1 ).getSelectedInputMethod ( ); // invoke-virtual {v1}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getSelectedInputMethod()Ljava/lang/String;
/* .line 270 */
/* .local v1, "defaultIme":Ljava/lang/String; */
int v2 = 0; // const/4 v2, 0x0
/* .line 271 */
/* .local v2, "cn":Landroid/content/ComponentName; */
v3 = android.text.TextUtils .isEmpty ( v1 );
/* if-nez v3, :cond_0 */
/* .line 272 */
android.content.ComponentName .unflattenFromString ( v1 );
/* move-object v2, v3 */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 273 */
(( android.content.ComponentName ) v2 ).getPackageName ( ); // invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
v3 = android.text.TextUtils .equals ( v0,v3 );
if ( v3 != null) { // if-eqz v3, :cond_0
int v3 = 1; // const/4 v3, 0x1
} // :cond_0
int v3 = 0; // const/4 v3, 0x0
/* .line 271 */
} // :goto_0
} // .end method
private static Boolean isPasswdInputType ( Integer p0 ) {
/* .locals 4 */
/* .param p0, "inputType" # I */
/* .line 258 */
/* and-int/lit16 v0, p0, 0xa0 */
int v1 = 0; // const/4 v1, 0x0
int v2 = 1; // const/4 v2, 0x1
/* const/16 v3, 0xa0 */
/* if-ne v0, v3, :cond_1 */
/* .line 259 */
/* and-int/lit16 v0, p0, 0xfff */
/* const/16 v3, 0xe1 */
/* if-ne v0, v3, :cond_0 */
/* move v1, v2 */
} // :cond_0
/* .line 261 */
} // :cond_1
/* and-int/lit16 v0, p0, 0xfff */
/* const/16 v3, 0x81 */
/* if-eq v0, v3, :cond_2 */
/* and-int/lit16 v0, p0, 0xfff */
/* const/16 v3, 0x91 */
/* if-eq v0, v3, :cond_2 */
/* and-int/lit16 v0, p0, 0xfff */
/* const/16 v3, 0x12 */
/* if-ne v0, v3, :cond_3 */
} // :cond_2
/* move v1, v2 */
} // :cond_3
} // .end method
private Boolean isSecMethodLocked ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "methodId" # Ljava/lang/String; */
/* .line 210 */
v0 = this.mService;
v0 = this.mMethodMap;
(( android.util.ArrayMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Landroid/view/inputmethod/InputMethodInfo; */
/* .line 211 */
/* .local v0, "imi":Landroid/view/inputmethod/InputMethodInfo; */
if ( v0 != null) { // if-eqz v0, :cond_0
(( android.view.inputmethod.InputMethodInfo ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;
final String v2 = "com.miui.securityinputmethod"; // const-string v2, "com.miui.securityinputmethod"
v1 = android.text.TextUtils .equals ( v1,v2 );
if ( v1 != null) { // if-eqz v1, :cond_0
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
} // .end method
private void updateFromSettingsLocked ( ) {
/* .locals 2 */
/* .line 121 */
v0 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v0 ).getSelectedMethodIdLocked ( ); // invoke-virtual {v0}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->mSecEnabled:Z */
/* if-nez v0, :cond_0 */
/* .line 122 */
v0 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v0 ).getSelectedMethodIdLocked ( ); // invoke-virtual {v0}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->isSecMethodLocked(Ljava/lang/String;)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 123 */
v0 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v0 ).clearClientSessionsLocked ( ); // invoke-virtual {v0}, Lcom/android/server/inputmethod/InputMethodManagerService;->clearClientSessionsLocked()V
/* .line 124 */
v0 = this.mService;
int v1 = 2; // const/4 v1, 0x2
(( com.android.server.inputmethod.InputMethodManagerService ) v0 ).unbindCurrentClientLocked ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/inputmethod/InputMethodManagerService;->unbindCurrentClientLocked(I)V
/* .line 127 */
} // :cond_0
return;
} // .end method
/* # virtual methods */
java.util.ArrayList filterSecMethodLocked ( java.util.ArrayList p0 ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/ArrayList<", */
/* "Landroid/view/inputmethod/InputMethodInfo;", */
/* ">;)", */
/* "Ljava/util/ArrayList<", */
/* "Landroid/view/inputmethod/InputMethodInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 246 */
/* .local p1, "methodInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/inputmethod/InputMethodInfo;>;" */
if ( p1 != null) { // if-eqz p1, :cond_1
/* sget-boolean v0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->SUPPORT_SEC_INPUT_METHOD:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 247 */
(( java.util.ArrayList ) p1 ).iterator ( ); // invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Landroid/view/inputmethod/InputMethodInfo; */
/* .line 248 */
/* .local v1, "methodInfo":Landroid/view/inputmethod/InputMethodInfo; */
(( android.view.inputmethod.InputMethodInfo ) v1 ).getId ( ); // invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;
v2 = /* invoke-direct {p0, v2}, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->isSecMethodLocked(Ljava/lang/String;)Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 249 */
(( java.util.ArrayList ) p1 ).remove ( v1 ); // invoke-virtual {p1, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
/* .line 250 */
/* .line 252 */
} // .end local v1 # "methodInfo":Landroid/view/inputmethod/InputMethodInfo;
} // :cond_0
/* .line 254 */
} // :cond_1
} // :goto_1
} // .end method
Boolean mayChangeInputMethodLocked ( android.view.inputmethod.EditorInfo p0 ) {
/* .locals 10 */
/* .param p1, "attribute" # Landroid/view/inputmethod/EditorInfo; */
/* .line 130 */
/* sget-boolean v0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->SUPPORT_SEC_INPUT_METHOD:Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 131 */
/* .line 133 */
} // :cond_0
v0 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v0 ).getSelectedMethodIdLocked ( ); // invoke-virtual {v0}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;
final String v2 = "MiuiSecurityInputMethodHelper"; // const-string v2, "MiuiSecurityInputMethodHelper"
/* if-nez v0, :cond_1 */
/* .line 134 */
final String v0 = "input_service has no current_method_id"; // const-string v0, "input_service has no current_method_id"
android.util.Slog .w ( v2,v0 );
/* .line 135 */
/* .line 137 */
} // :cond_1
/* if-nez p1, :cond_2 */
/* .line 138 */
final String v0 = "editor_info is null, we cannot judge"; // const-string v0, "editor_info is null, we cannot judge"
android.util.Slog .w ( v2,v0 );
/* .line 139 */
/* .line 141 */
} // :cond_2
com.android.server.inputmethod.InputMethodManagerServiceImpl .getInstance ( );
v0 = this.mBindingController;
/* if-nez v0, :cond_3 */
/* .line 142 */
final String v0 = "IMMS_IMPL has not init"; // const-string v0, "IMMS_IMPL has not init"
android.util.Slog .w ( v2,v0 );
/* .line 143 */
/* .line 145 */
} // :cond_3
v0 = this.mService;
v0 = this.mMethodMap;
v3 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v3 ).getSelectedMethodIdLocked ( ); // invoke-virtual {v3}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;
(( android.util.ArrayMap ) v0 ).get ( v3 ); // invoke-virtual {v0, v3}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Landroid/view/inputmethod/InputMethodInfo; */
/* .line 146 */
/* .local v0, "curMethodInfo":Landroid/view/inputmethod/InputMethodInfo; */
/* if-nez v0, :cond_4 */
/* .line 147 */
final String v3 = "fail to find current_method_info in the map"; // const-string v3, "fail to find current_method_info in the map"
android.util.Slog .w ( v2,v3 );
/* .line 148 */
/* .line 151 */
} // :cond_4
/* iget v3, p1, Landroid/view/inputmethod/EditorInfo;->inputType:I */
v3 = com.android.server.inputmethod.MiuiSecurityInputMethodHelper .isPasswdInputType ( v3 );
int v4 = 1; // const/4 v4, 0x1
if ( v3 != null) { // if-eqz v3, :cond_5
v3 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v3 ).getSelectedMethodIdLocked ( ); // invoke-virtual {v3}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;
v3 = /* invoke-direct {p0, v3}, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->isSecMethodLocked(Ljava/lang/String;)Z */
/* if-nez v3, :cond_5 */
/* iget-boolean v3, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->mSecEnabled:Z */
if ( v3 != null) { // if-eqz v3, :cond_5
/* .line 152 */
/* invoke-direct {p0}, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->getSecMethodIdLocked()Ljava/lang/String; */
v3 = android.text.TextUtils .isEmpty ( v3 );
/* if-nez v3, :cond_5 */
v3 = /* invoke-direct {p0, p1}, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->isEditorInDefaultImeApp(Landroid/view/inputmethod/EditorInfo;)Z */
/* if-nez v3, :cond_5 */
/* move v3, v4 */
} // :cond_5
/* move v3, v1 */
/* .line 153 */
/* .local v3, "switchToSecInput":Z */
} // :goto_0
v5 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v5 ).getSelectedMethodIdLocked ( ); // invoke-virtual {v5}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;
v5 = /* invoke-direct {p0, v5}, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->isSecMethodLocked(Ljava/lang/String;)Z */
if ( v5 != null) { // if-eqz v5, :cond_7
/* iget-boolean v5, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->mSecEnabled:Z */
if ( v5 != null) { // if-eqz v5, :cond_6
/* iget v5, p1, Landroid/view/inputmethod/EditorInfo;->inputType:I */
/* .line 154 */
v5 = com.android.server.inputmethod.MiuiSecurityInputMethodHelper .isPasswdInputType ( v5 );
if ( v5 != null) { // if-eqz v5, :cond_6
v5 = /* invoke-direct {p0, p1}, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->isEditorInDefaultImeApp(Landroid/view/inputmethod/EditorInfo;)Z */
if ( v5 != null) { // if-eqz v5, :cond_7
} // :cond_6
/* move v5, v4 */
} // :cond_7
/* move v5, v1 */
/* .line 156 */
/* .local v5, "switchFromSecInput":Z */
} // :goto_1
int v6 = -1; // const/4 v6, -0x1
int v7 = 2; // const/4 v7, 0x2
if ( v3 != null) { // if-eqz v3, :cond_9
/* .line 157 */
/* invoke-direct {p0}, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->getSecMethodIdLocked()Ljava/lang/String; */
/* .line 158 */
/* .local v8, "secInputMethodId":Ljava/lang/String; */
v9 = android.text.TextUtils .isEmpty ( v8 );
if ( v9 != null) { // if-eqz v9, :cond_8
/* .line 159 */
final String v4 = "fail to find secure_input_method in input_method_list"; // const-string v4, "fail to find secure_input_method in input_method_list"
android.util.Slog .w ( v2,v4 );
/* .line 160 */
/* .line 162 */
} // :cond_8
com.android.server.inputmethod.InputMethodManagerServiceImpl .getInstance ( );
v1 = this.mBindingController;
/* .line 163 */
(( com.android.server.inputmethod.InputMethodBindingController ) v1 ).setSelectedMethodId ( v8 ); // invoke-virtual {v1, v8}, Lcom/android/server/inputmethod/InputMethodBindingController;->setSelectedMethodId(Ljava/lang/String;)V
/* .line 164 */
v1 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v1 ).clearClientSessionsLocked ( ); // invoke-virtual {v1}, Lcom/android/server/inputmethod/InputMethodManagerService;->clearClientSessionsLocked()V
/* .line 165 */
v1 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v1 ).unbindCurrentClientLocked ( v7 ); // invoke-virtual {v1, v7}, Lcom/android/server/inputmethod/InputMethodManagerService;->unbindCurrentClientLocked(I)V
/* .line 166 */
com.android.server.inputmethod.InputMethodManagerServiceImpl .getInstance ( );
v1 = this.mBindingController;
(( com.android.server.inputmethod.InputMethodBindingController ) v1 ).unbindCurrentMethod ( ); // invoke-virtual {v1}, Lcom/android/server/inputmethod/InputMethodBindingController;->unbindCurrentMethod()V
/* .line 167 */
v1 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v1 ).getSelectedMethodIdLocked ( ); // invoke-virtual {v1}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;
(( com.android.server.inputmethod.InputMethodManagerService ) v1 ).setInputMethodLocked ( v2, v6 ); // invoke-virtual {v1, v2, v6}, Lcom/android/server/inputmethod/InputMethodManagerService;->setInputMethodLocked(Ljava/lang/String;I)V
/* .line 168 */
/* .line 169 */
} // .end local v8 # "secInputMethodId":Ljava/lang/String;
} // :cond_9
if ( v5 != null) { // if-eqz v5, :cond_d
/* .line 170 */
v8 = this.mService;
v8 = this.mSettings;
(( com.android.server.inputmethod.InputMethodUtils$InputMethodSettings ) v8 ).getSelectedInputMethod ( ); // invoke-virtual {v8}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getSelectedInputMethod()Ljava/lang/String;
/* .line 171 */
/* .local v8, "selectedInputMethod":Ljava/lang/String; */
v9 = android.text.TextUtils .isEmpty ( v8 );
if ( v9 != null) { // if-eqz v9, :cond_a
/* .line 172 */
/* const-string/jumbo v9, "something is weired, maybe the input method app are uninstalled" */
android.util.Slog .w ( v2,v9 );
/* .line 190 */
} // :cond_a
v9 = android.text.TextUtils .isEmpty ( v8 );
if ( v9 != null) { // if-eqz v9, :cond_b
/* .line 191 */
final String v4 = "finally, we still fail to find default input method"; // const-string v4, "finally, we still fail to find default input method"
android.util.Slog .w ( v2,v4 );
/* .line 192 */
/* .line 194 */
} // :cond_b
v9 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v9 ).getSelectedMethodIdLocked ( ); // invoke-virtual {v9}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;
v9 = android.text.TextUtils .equals ( v9,v8 );
if ( v9 != null) { // if-eqz v9, :cond_c
/* .line 195 */
final String v4 = "It looks like there is only miui_sec_input_method in the system"; // const-string v4, "It looks like there is only miui_sec_input_method in the system"
android.util.Slog .w ( v2,v4 );
/* .line 196 */
/* .line 198 */
} // :cond_c
com.android.server.inputmethod.InputMethodManagerServiceImpl .getInstance ( );
v1 = this.mBindingController;
/* .line 199 */
(( com.android.server.inputmethod.InputMethodBindingController ) v1 ).setSelectedMethodId ( v8 ); // invoke-virtual {v1, v8}, Lcom/android/server/inputmethod/InputMethodBindingController;->setSelectedMethodId(Ljava/lang/String;)V
/* .line 200 */
v1 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v1 ).clearClientSessionsLocked ( ); // invoke-virtual {v1}, Lcom/android/server/inputmethod/InputMethodManagerService;->clearClientSessionsLocked()V
/* .line 201 */
v1 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v1 ).unbindCurrentClientLocked ( v7 ); // invoke-virtual {v1, v7}, Lcom/android/server/inputmethod/InputMethodManagerService;->unbindCurrentClientLocked(I)V
/* .line 202 */
com.android.server.inputmethod.InputMethodManagerServiceImpl .getInstance ( );
v1 = this.mBindingController;
(( com.android.server.inputmethod.InputMethodBindingController ) v1 ).unbindCurrentMethod ( ); // invoke-virtual {v1}, Lcom/android/server/inputmethod/InputMethodBindingController;->unbindCurrentMethod()V
/* .line 203 */
v1 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v1 ).getSelectedMethodIdLocked ( ); // invoke-virtual {v1}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;
(( com.android.server.inputmethod.InputMethodManagerService ) v1 ).setInputMethodLocked ( v2, v6 ); // invoke-virtual {v1, v2, v6}, Lcom/android/server/inputmethod/InputMethodManagerService;->setInputMethodLocked(Ljava/lang/String;I)V
/* .line 204 */
/* .line 206 */
} // .end local v8 # "selectedInputMethod":Ljava/lang/String;
} // :cond_d
} // .end method
void onSwitchUserLocked ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "newUserId" # I */
/* .line 68 */
v0 = this.mSettingsObserver;
(( com.android.server.inputmethod.MiuiSecurityInputMethodHelper$SettingsObserver ) v0 ).registerContentObserverLocked ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper$SettingsObserver;->registerContentObserverLocked(I)V
/* .line 69 */
v0 = this.mService;
v0 = this.mContext;
/* .line 70 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v1 = this.mService;
v1 = this.mSettings;
/* .line 71 */
v1 = (( com.android.server.inputmethod.InputMethodUtils$InputMethodSettings ) v1 ).getCurrentUserId ( ); // invoke-virtual {v1}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getCurrentUserId()I
/* .line 69 */
final String v2 = "enable_miui_security_ime"; // const-string v2, "enable_miui_security_ime"
int v3 = 1; // const/4 v3, 0x1
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v2,v3,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v3 = 0; // const/4 v3, 0x0
} // :goto_0
/* iput-boolean v3, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->mSecEnabled:Z */
/* .line 72 */
return;
} // .end method
void onSystemRunningLocked ( ) {
/* .locals 4 */
/* .line 60 */
/* new-instance v0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper$SettingsObserver; */
com.android.server.inputmethod.InputMethodManagerServiceImpl .getInstance ( );
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper$SettingsObserver;-><init>(Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;Landroid/os/Handler;)V */
this.mSettingsObserver = v0;
/* .line 61 */
v1 = this.mService;
v1 = this.mSettings;
v1 = (( com.android.server.inputmethod.InputMethodUtils$InputMethodSettings ) v1 ).getCurrentUserId ( ); // invoke-virtual {v1}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getCurrentUserId()I
(( com.android.server.inputmethod.MiuiSecurityInputMethodHelper$SettingsObserver ) v0 ).registerContentObserverLocked ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper$SettingsObserver;->registerContentObserverLocked(I)V
/* .line 62 */
v0 = this.mService;
v0 = this.mContext;
/* .line 63 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v1 = this.mService;
v1 = this.mSettings;
/* .line 64 */
v1 = (( com.android.server.inputmethod.InputMethodUtils$InputMethodSettings ) v1 ).getCurrentUserId ( ); // invoke-virtual {v1}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getCurrentUserId()I
/* .line 62 */
final String v2 = "enable_miui_security_ime"; // const-string v2, "enable_miui_security_ime"
int v3 = 1; // const/4 v3, 0x1
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v2,v3,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v3 = 0; // const/4 v3, 0x0
} // :goto_0
/* iput-boolean v3, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->mSecEnabled:Z */
/* .line 65 */
return;
} // .end method
void removeSecMethod ( java.util.List p0 ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 231 */
/* .local p1, "imList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;>;" */
/* sget-boolean v0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->SUPPORT_SEC_INPUT_METHOD:Z */
/* if-nez v0, :cond_0 */
/* .line 232 */
return;
/* .line 234 */
} // :cond_0
v0 = if ( p1 != null) { // if-eqz p1, :cond_2
/* if-lez v0, :cond_2 */
/* .line 235 */
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_2
/* check-cast v1, Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem; */
/* .line 236 */
/* .local v1, "imeSubtypeListItem":Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem; */
v2 = this.mImi;
/* .line 237 */
/* .local v2, "imi":Landroid/view/inputmethod/InputMethodInfo; */
(( android.view.inputmethod.InputMethodInfo ) v2 ).getPackageName ( ); // invoke-virtual {v2}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;
final String v4 = "com.miui.securityinputmethod"; // const-string v4, "com.miui.securityinputmethod"
v3 = android.text.TextUtils .equals ( v3,v4 );
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 238 */
/* .line 239 */
return;
/* .line 241 */
} // .end local v1 # "imeSubtypeListItem":Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;
} // .end local v2 # "imi":Landroid/view/inputmethod/InputMethodInfo;
} // :cond_1
/* .line 243 */
} // :cond_2
return;
} // .end method
Boolean shouldHideImeSwitcherLocked ( ) {
/* .locals 1 */
/* .line 224 */
/* sget-boolean v0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->SUPPORT_SEC_INPUT_METHOD:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v0 ).getSelectedMethodIdLocked ( ); // invoke-virtual {v0}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->isSecMethodLocked(Ljava/lang/String;)Z */
/* if-nez v0, :cond_1 */
/* .line 225 */
} // :cond_0
com.android.server.inputmethod.InputMethodManagerServiceImpl .getInstance ( );
v0 = this.mBindingController;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 226 */
com.android.server.inputmethod.InputMethodManagerServiceImpl .getInstance ( );
v0 = this.mBindingController;
(( com.android.server.inputmethod.InputMethodBindingController ) v0 ).getCurMethod ( ); // invoke-virtual {v0}, Lcom/android/server/inputmethod/InputMethodBindingController;->getCurMethod()Lcom/android/server/inputmethod/IInputMethodInvoker;
/* if-nez v0, :cond_2 */
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
/* .line 224 */
} // :goto_0
} // .end method
