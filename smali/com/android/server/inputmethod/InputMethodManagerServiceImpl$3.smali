.class Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$3;
.super Ljava/lang/Object;
.source "InputMethodManagerServiceImpl.java"

# interfaces
.implements Landroid/view/View$OnApplyWindowInsetsListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->setDialogImmersive(Lmiuix/appcompat/app/AlertDialog;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

.field final synthetic val$decorView:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;Landroid/view/View;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 463
    iput-object p1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$3;->this$0:Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    iput-object p2, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$3;->val$decorView:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onApplyWindowInsets(Landroid/view/View;Landroid/view/WindowInsets;)Landroid/view/WindowInsets;
    .locals 7
    .param p1, "v"    # Landroid/view/View;
    .param p2, "insets"    # Landroid/view/WindowInsets;

    .line 466
    nop

    .line 467
    invoke-static {}, Landroid/view/WindowInsets$Type;->mandatorySystemGestures()I

    move-result v0

    .line 466
    invoke-virtual {p2, v0}, Landroid/view/WindowInsets;->getInsets(I)Landroid/graphics/Insets;

    move-result-object v0

    .line 468
    .local v0, "gestureInsets":Landroid/graphics/Insets;
    iget v1, v0, Landroid/graphics/Insets;->bottom:I

    .line 469
    .local v1, "mHeight":I
    if-lez v1, :cond_0

    .line 470
    iget-object v2, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$3;->val$decorView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingStart()I

    move-result v3

    iget-object v4, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$3;->val$decorView:Landroid/view/View;

    .line 471
    invoke-virtual {v4}, Landroid/view/View;->getPaddingTop()I

    move-result v4

    iget-object v5, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$3;->val$decorView:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getPaddingEnd()I

    move-result v5

    iget-object v6, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$3;->val$decorView:Landroid/view/View;

    .line 472
    invoke-virtual {v6}, Landroid/view/View;->getPaddingBottom()I

    move-result v6

    add-int/2addr v6, v1

    .line 470
    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/view/View;->setPaddingRelative(IIII)V

    .line 475
    :cond_0
    iget-object v2, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$3;->val$decorView:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnApplyWindowInsetsListener(Landroid/view/View$OnApplyWindowInsetsListener;)V

    .line 476
    sget-object v2, Landroid/view/WindowInsets;->CONSUMED:Landroid/view/WindowInsets;

    return-object v2
.end method
