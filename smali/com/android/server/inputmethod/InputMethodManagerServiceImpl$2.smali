.class Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$2;
.super Ljava/lang/Object;
.source "InputMethodManagerServiceImpl.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->generateView(ZLjava/util/List;Lmiuix/appcompat/app/AlertDialog$Builder;Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

.field final synthetic val$closeIndicatorDrawableId:I

.field final synthetic val$indicator:Landroid/widget/ImageView;

.field final synthetic val$noCustomListView:Landroid/widget/ListView;

.field final synthetic val$noCustomziedImeListAdapter:Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;

.field final synthetic val$openIndicatorDrawableId:I


# direct methods
.method constructor <init>(Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;Landroid/widget/ImageView;ILandroid/widget/ListView;Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 414
    iput-object p1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$2;->this$0:Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    iput-object p2, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$2;->val$indicator:Landroid/widget/ImageView;

    iput p3, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$2;->val$openIndicatorDrawableId:I

    iput-object p4, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$2;->val$noCustomListView:Landroid/widget/ListView;

    iput-object p5, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$2;->val$noCustomziedImeListAdapter:Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;

    iput p6, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$2;->val$closeIndicatorDrawableId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .line 417
    iget-object v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$2;->this$0:Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    invoke-static {v0}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->-$$Nest$fgetisExpanded(Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 418
    iget-object v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$2;->val$indicator:Landroid/widget/ImageView;

    iget v2, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$2;->val$openIndicatorDrawableId:I

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 419
    iget-object v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$2;->val$noCustomListView:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 420
    iget-object v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$2;->val$noCustomListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$2;->val$noCustomziedImeListAdapter:Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;

    invoke-static {v0, v1}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->-$$Nest$smsetNoCustomizedInputMethodListViewHeight(Landroid/widget/ListView;Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;)V

    .line 422
    iget-object v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$2;->this$0:Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->-$$Nest$fputisExpanded(Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;Z)V

    goto :goto_0

    .line 424
    :cond_0
    iget-object v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$2;->val$indicator:Landroid/widget/ImageView;

    iget v2, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$2;->val$closeIndicatorDrawableId:I

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 425
    iget-object v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$2;->val$noCustomListView:Landroid/widget/ListView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 426
    iget-object v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$2;->this$0:Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    invoke-static {v0, v1}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->-$$Nest$fputisExpanded(Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;Z)V

    .line 428
    :goto_0
    return-void
.end method
