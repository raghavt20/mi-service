.class public Lcom/android/server/inputmethod/MiuiInputMethodStubImpl;
.super Ljava/lang/Object;
.source "MiuiInputMethodStubImpl.java"

# interfaces
.implements Lcom/android/server/inputmethod/MiuiInputMethodStub;


# static fields
.field private static final SECURE_INPUT_METHOD_PACKAGE_NAME:Ljava/lang/String; = "com.miui.securityinputmethod"

.field public static final TAG:Ljava/lang/String; = "StylusInputMethodSwitcher"


# instance fields
.field private mCallingUid:I

.field private mInputMethodChangedBySelf:Z

.field private mService:Lcom/android/server/inputmethod/InputMethodManagerService;

.field private securityInputMethodSwitcher:Lcom/android/server/inputmethod/BaseInputMethodSwitcher;

.field private sogouInputMethodSwitcher:Lcom/android/server/inputmethod/BaseInputMethodSwitcher;

.field private stylusInputMethodSwitcher:Lcom/android/server/inputmethod/BaseInputMethodSwitcher;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bootPhase(ILandroid/content/Context;)V
    .locals 1
    .param p1, "phase"    # I
    .param p2, "mContext"    # Landroid/content/Context;

    .line 88
    const/16 v0, 0x3e8

    if-ne p1, v0, :cond_0

    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v0, :cond_0

    .line 89
    invoke-static {p2}, Lcom/miui/server/InputMethodHelper;->init(Landroid/content/Context;)V

    .line 91
    :cond_0
    return-void
.end method

.method public clearInputMethodChangedBySelf()V
    .locals 1

    .line 100
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/inputmethod/MiuiInputMethodStubImpl;->mInputMethodChangedBySelf:Z

    .line 101
    return-void
.end method

.method public filterMethodLocked(Ljava/util/List;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/view/inputmethod/InputMethodInfo;",
            ">;)",
            "Ljava/util/List<",
            "Landroid/view/inputmethod/InputMethodInfo;",
            ">;"
        }
    .end annotation

    .line 81
    .local p1, "methodInfos":Ljava/util/List;, "Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;"
    iget-object v0, p0, Lcom/android/server/inputmethod/MiuiInputMethodStubImpl;->sogouInputMethodSwitcher:Lcom/android/server/inputmethod/BaseInputMethodSwitcher;

    iget-object v1, p0, Lcom/android/server/inputmethod/MiuiInputMethodStubImpl;->securityInputMethodSwitcher:Lcom/android/server/inputmethod/BaseInputMethodSwitcher;

    iget-object v2, p0, Lcom/android/server/inputmethod/MiuiInputMethodStubImpl;->stylusInputMethodSwitcher:Lcom/android/server/inputmethod/BaseInputMethodSwitcher;

    .line 83
    invoke-virtual {v2, p1}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;->filterMethodLocked(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 82
    invoke-virtual {v1, v2}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;->filterMethodLocked(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 81
    invoke-virtual {v0, v1}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;->filterMethodLocked(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public init(Lcom/android/server/inputmethod/InputMethodManagerService;)V
    .locals 1
    .param p1, "service"    # Lcom/android/server/inputmethod/InputMethodManagerService;

    .line 29
    new-instance v0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;

    invoke-direct {v0}, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;-><init>()V

    iput-object v0, p0, Lcom/android/server/inputmethod/MiuiInputMethodStubImpl;->stylusInputMethodSwitcher:Lcom/android/server/inputmethod/BaseInputMethodSwitcher;

    .line 30
    new-instance v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;

    invoke-direct {v0}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;-><init>()V

    iput-object v0, p0, Lcom/android/server/inputmethod/MiuiInputMethodStubImpl;->securityInputMethodSwitcher:Lcom/android/server/inputmethod/BaseInputMethodSwitcher;

    .line 31
    new-instance v0, Lcom/android/server/inputmethod/SogouInputMethodSwitcher;

    invoke-direct {v0}, Lcom/android/server/inputmethod/SogouInputMethodSwitcher;-><init>()V

    iput-object v0, p0, Lcom/android/server/inputmethod/MiuiInputMethodStubImpl;->sogouInputMethodSwitcher:Lcom/android/server/inputmethod/BaseInputMethodSwitcher;

    .line 32
    iget-object v0, p0, Lcom/android/server/inputmethod/MiuiInputMethodStubImpl;->stylusInputMethodSwitcher:Lcom/android/server/inputmethod/BaseInputMethodSwitcher;

    invoke-virtual {v0, p1}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;->init(Lcom/android/server/inputmethod/InputMethodManagerService;)V

    .line 33
    iget-object v0, p0, Lcom/android/server/inputmethod/MiuiInputMethodStubImpl;->securityInputMethodSwitcher:Lcom/android/server/inputmethod/BaseInputMethodSwitcher;

    invoke-virtual {v0, p1}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;->init(Lcom/android/server/inputmethod/InputMethodManagerService;)V

    .line 34
    iget-object v0, p0, Lcom/android/server/inputmethod/MiuiInputMethodStubImpl;->sogouInputMethodSwitcher:Lcom/android/server/inputmethod/BaseInputMethodSwitcher;

    invoke-virtual {v0, p1}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;->init(Lcom/android/server/inputmethod/InputMethodManagerService;)V

    .line 35
    iput-object p1, p0, Lcom/android/server/inputmethod/MiuiInputMethodStubImpl;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    .line 36
    return-void
.end method

.method public isInputMethodChangedBySelf()Z
    .locals 1

    .line 104
    iget-boolean v0, p0, Lcom/android/server/inputmethod/MiuiInputMethodStubImpl;->mInputMethodChangedBySelf:Z

    return v0
.end method

.method public mayChangeInputMethodLocked(Landroid/view/inputmethod/EditorInfo;I)Z
    .locals 1
    .param p1, "attribute"    # Landroid/view/inputmethod/EditorInfo;
    .param p2, "startInputReason"    # I

    .line 60
    iget-object v0, p0, Lcom/android/server/inputmethod/MiuiInputMethodStubImpl;->securityInputMethodSwitcher:Lcom/android/server/inputmethod/BaseInputMethodSwitcher;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;->mayChangeInputMethodLocked(Landroid/view/inputmethod/EditorInfo;I)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/server/inputmethod/MiuiInputMethodStubImpl;->sogouInputMethodSwitcher:Lcom/android/server/inputmethod/BaseInputMethodSwitcher;

    .line 61
    invoke-virtual {v0, p1}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;->mayChangeInputMethodLocked(Landroid/view/inputmethod/EditorInfo;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/server/inputmethod/MiuiInputMethodStubImpl;->stylusInputMethodSwitcher:Lcom/android/server/inputmethod/BaseInputMethodSwitcher;

    .line 62
    invoke-virtual {v0, p1}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;->mayChangeInputMethodLocked(Landroid/view/inputmethod/EditorInfo;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 60
    :goto_1
    return v0
.end method

.method public onSwitchUserLocked(I)V
    .locals 1
    .param p1, "newUserId"    # I

    .line 53
    iget-object v0, p0, Lcom/android/server/inputmethod/MiuiInputMethodStubImpl;->stylusInputMethodSwitcher:Lcom/android/server/inputmethod/BaseInputMethodSwitcher;

    invoke-virtual {v0, p1}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;->onSwitchUserLocked(I)V

    .line 54
    iget-object v0, p0, Lcom/android/server/inputmethod/MiuiInputMethodStubImpl;->securityInputMethodSwitcher:Lcom/android/server/inputmethod/BaseInputMethodSwitcher;

    invoke-virtual {v0, p1}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;->onSwitchUserLocked(I)V

    .line 55
    iget-object v0, p0, Lcom/android/server/inputmethod/MiuiInputMethodStubImpl;->sogouInputMethodSwitcher:Lcom/android/server/inputmethod/BaseInputMethodSwitcher;

    invoke-virtual {v0, p1}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;->onSwitchUserLocked(I)V

    .line 56
    return-void
.end method

.method public onSystemRunningLocked()V
    .locals 3

    .line 40
    iget-object v0, p0, Lcom/android/server/inputmethod/MiuiInputMethodStubImpl;->stylusInputMethodSwitcher:Lcom/android/server/inputmethod/BaseInputMethodSwitcher;

    invoke-virtual {v0}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;->onSystemRunningLocked()V

    .line 41
    iget-object v0, p0, Lcom/android/server/inputmethod/MiuiInputMethodStubImpl;->securityInputMethodSwitcher:Lcom/android/server/inputmethod/BaseInputMethodSwitcher;

    invoke-virtual {v0}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;->onSystemRunningLocked()V

    .line 42
    iget-object v0, p0, Lcom/android/server/inputmethod/MiuiInputMethodStubImpl;->sogouInputMethodSwitcher:Lcom/android/server/inputmethod/BaseInputMethodSwitcher;

    invoke-virtual {v0}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;->onSystemRunningLocked()V

    .line 44
    :try_start_0
    iget-object v0, p0, Lcom/android/server/inputmethod/MiuiInputMethodStubImpl;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v0, v0, Lcom/android/server/inputmethod/InputMethodManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.miui.securityinputmethod"

    .line 45
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageUid(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/server/inputmethod/MiuiInputMethodStubImpl;->mCallingUid:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 48
    goto :goto_0

    .line 46
    :catch_0
    move-exception v0

    .line 47
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, -0x1

    iput v1, p0, Lcom/android/server/inputmethod/MiuiInputMethodStubImpl;->mCallingUid:I

    .line 49
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public removeMethod(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;",
            ">;)V"
        }
    .end annotation

    .line 74
    .local p1, "imList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;>;"
    iget-object v0, p0, Lcom/android/server/inputmethod/MiuiInputMethodStubImpl;->stylusInputMethodSwitcher:Lcom/android/server/inputmethod/BaseInputMethodSwitcher;

    invoke-virtual {v0, p1}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;->removeMethod(Ljava/util/List;)V

    .line 75
    iget-object v0, p0, Lcom/android/server/inputmethod/MiuiInputMethodStubImpl;->securityInputMethodSwitcher:Lcom/android/server/inputmethod/BaseInputMethodSwitcher;

    invoke-virtual {v0, p1}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;->removeMethod(Ljava/util/List;)V

    .line 76
    iget-object v0, p0, Lcom/android/server/inputmethod/MiuiInputMethodStubImpl;->sogouInputMethodSwitcher:Lcom/android/server/inputmethod/BaseInputMethodSwitcher;

    invoke-virtual {v0, p1}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;->removeMethod(Ljava/util/List;)V

    .line 77
    return-void
.end method

.method public setInputMethodChangedBySelf(I)V
    .locals 1
    .param p1, "callingUid"    # I

    .line 93
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    return-void

    .line 94
    :cond_0
    iget v0, p0, Lcom/android/server/inputmethod/MiuiInputMethodStubImpl;->mCallingUid:I

    if-ne p1, v0, :cond_1

    .line 95
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/inputmethod/MiuiInputMethodStubImpl;->mInputMethodChangedBySelf:Z

    .line 97
    :cond_1
    return-void
.end method

.method public shouldHideImeSwitcherLocked()Z
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/android/server/inputmethod/MiuiInputMethodStubImpl;->stylusInputMethodSwitcher:Lcom/android/server/inputmethod/BaseInputMethodSwitcher;

    invoke-virtual {v0}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;->shouldHideImeSwitcherLocked()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/server/inputmethod/MiuiInputMethodStubImpl;->sogouInputMethodSwitcher:Lcom/android/server/inputmethod/BaseInputMethodSwitcher;

    .line 68
    invoke-virtual {v0}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;->shouldHideImeSwitcherLocked()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/server/inputmethod/MiuiInputMethodStubImpl;->securityInputMethodSwitcher:Lcom/android/server/inputmethod/BaseInputMethodSwitcher;

    .line 69
    invoke-virtual {v0}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;->shouldHideImeSwitcherLocked()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 67
    :goto_1
    return v0
.end method
