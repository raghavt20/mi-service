public class com.android.server.inputmethod.SecurityInputMethodSwitcher extends com.android.server.inputmethod.BaseInputMethodSwitcher {
	 /* .source "SecurityInputMethodSwitcher.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/inputmethod/SecurityInputMethodSwitcher$SettingsObserver; */
	 /* } */
} // .end annotation
/* # static fields */
public static final Boolean DEBUG;
public static final java.lang.String MIUI_SEC_INPUT_METHOD_APP_PKG_NAME;
public static final Boolean SUPPORT_SEC_INPUT_METHOD;
public static final java.lang.String TAG;
/* # instance fields */
private Boolean mSecEnabled;
private com.android.server.inputmethod.SecurityInputMethodSwitcher$SettingsObserver mSettingsObserver;
/* # direct methods */
static Boolean -$$Nest$fgetmSecEnabled ( com.android.server.inputmethod.SecurityInputMethodSwitcher p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mSecEnabled:Z */
} // .end method
static void -$$Nest$fputmSecEnabled ( com.android.server.inputmethod.SecurityInputMethodSwitcher p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mSecEnabled:Z */
	 return;
} // .end method
static Integer -$$Nest$mgetParentUserId ( com.android.server.inputmethod.SecurityInputMethodSwitcher p0, Integer p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = 	 /* invoke-direct {p0, p1}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->getParentUserId(I)I */
} // .end method
static Boolean -$$Nest$misSecEnabled ( com.android.server.inputmethod.SecurityInputMethodSwitcher p0, Integer p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = 	 /* invoke-direct {p0, p1}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->isSecEnabled(I)Z */
} // .end method
static void -$$Nest$mupdateFromSettingsLocked ( com.android.server.inputmethod.SecurityInputMethodSwitcher p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->updateFromSettingsLocked()V */
	 return;
} // .end method
static com.android.server.inputmethod.SecurityInputMethodSwitcher ( ) {
	 /* .locals 3 */
	 /* .line 36 */
	 /* nop */
	 /* .line 37 */
	 final String v0 = "ro.miui.has_security_keyboard"; // const-string v0, "ro.miui.has_security_keyboard"
	 int v1 = 0; // const/4 v1, 0x0
	 v0 = 	 android.os.SystemProperties .getInt ( v0,v1 );
	 int v2 = 1; // const/4 v2, 0x1
	 /* if-ne v0, v2, :cond_0 */
	 /* sget-boolean v0, Lmiui/os/Build;->IS_GLOBAL_BUILD:Z */
	 /* if-nez v0, :cond_0 */
	 /* move v1, v2 */
} // :cond_0
com.android.server.inputmethod.SecurityInputMethodSwitcher.SUPPORT_SEC_INPUT_METHOD = (v1!= 0);
/* .line 36 */
return;
} // .end method
public com.android.server.inputmethod.SecurityInputMethodSwitcher ( ) {
/* .locals 0 */
/* .line 33 */
/* invoke-direct {p0}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;-><init>()V */
return;
} // .end method
private Integer getParentUserId ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "userId" # I */
/* .line 277 */
/* if-nez p1, :cond_0 */
/* .line 278 */
/* .line 280 */
} // :cond_0
v0 = this.mService;
v0 = this.mContext;
android.os.UserManager .get ( v0 );
/* .line 281 */
/* .local v0, "userManager":Landroid/os/UserManager; */
(( android.os.UserManager ) v0 ).getProfileParent ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/UserManager;->getProfileParent(I)Landroid/content/pm/UserInfo;
/* .line 282 */
/* .local v1, "parentUserInfo":Landroid/content/pm/UserInfo; */
if ( v1 != null) { // if-eqz v1, :cond_1
/* iget v2, v1, Landroid/content/pm/UserInfo;->id:I */
} // :cond_1
/* move v2, p1 */
} // :goto_0
} // .end method
private java.lang.String getSecMethodIdLocked ( ) {
/* .locals 3 */
/* .line 292 */
v0 = this.mService;
v0 = this.mMethodMap;
(( android.util.ArrayMap ) v0 ).entrySet ( ); // invoke-virtual {v0}, Landroid/util/ArrayMap;->entrySet()Ljava/util/Set;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Ljava/util/Map$Entry; */
/* .line 293 */
/* .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Landroid/view/inputmethod/InputMethodInfo;>;" */
/* check-cast v2, Ljava/lang/String; */
v2 = /* invoke-direct {p0, v2}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->isSecMethodLocked(Ljava/lang/String;)Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 294 */
/* check-cast v0, Ljava/lang/String; */
/* .line 296 */
} // .end local v1 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Landroid/view/inputmethod/InputMethodInfo;>;"
} // :cond_0
/* .line 297 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean isEditorInDefaultImeApp ( android.view.inputmethod.EditorInfo p0 ) {
/* .locals 4 */
/* .param p1, "editor" # Landroid/view/inputmethod/EditorInfo; */
/* .line 309 */
v0 = this.packageName;
/* .line 310 */
/* .local v0, "pkg":Ljava/lang/String; */
v1 = this.mService;
v1 = this.mSettings;
(( com.android.server.inputmethod.InputMethodUtils$InputMethodSettings ) v1 ).getSelectedInputMethod ( ); // invoke-virtual {v1}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getSelectedInputMethod()Ljava/lang/String;
/* .line 311 */
/* .local v1, "defaultIme":Ljava/lang/String; */
int v2 = 0; // const/4 v2, 0x0
/* .line 312 */
/* .local v2, "cn":Landroid/content/ComponentName; */
v3 = android.text.TextUtils .isEmpty ( v1 );
/* if-nez v3, :cond_0 */
/* .line 313 */
android.content.ComponentName .unflattenFromString ( v1 );
/* move-object v2, v3 */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 314 */
(( android.content.ComponentName ) v2 ).getPackageName ( ); // invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
v3 = android.text.TextUtils .equals ( v0,v3 );
if ( v3 != null) { // if-eqz v3, :cond_0
int v3 = 1; // const/4 v3, 0x1
} // :cond_0
int v3 = 0; // const/4 v3, 0x0
/* .line 312 */
} // :goto_0
} // .end method
private Boolean isSecEnabled ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "userId" # I */
/* .line 270 */
v0 = this.mService;
v0 = this.mContext;
/* .line 271 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 270 */
final String v1 = "enable_miui_security_ime"; // const-string v1, "enable_miui_security_ime"
int v2 = 1; // const/4 v2, 0x1
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v1,v2,p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
} // .end method
private Boolean isSecMethodLocked ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "methodId" # Ljava/lang/String; */
/* .line 286 */
v0 = this.mService;
v0 = this.mMethodMap;
(( android.util.ArrayMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Landroid/view/inputmethod/InputMethodInfo; */
/* .line 287 */
/* .local v0, "imi":Landroid/view/inputmethod/InputMethodInfo; */
if ( v0 != null) { // if-eqz v0, :cond_0
(( android.view.inputmethod.InputMethodInfo ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;
final String v2 = "com.miui.securityinputmethod"; // const-string v2, "com.miui.securityinputmethod"
v1 = android.text.TextUtils .equals ( v1,v2 );
if ( v1 != null) { // if-eqz v1, :cond_0
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
} // .end method
private void updateFromSettingsLocked ( ) {
/* .locals 2 */
/* .line 301 */
v0 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v0 ).getSelectedMethodIdLocked ( ); // invoke-virtual {v0}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mSecEnabled:Z */
/* if-nez v0, :cond_0 */
v0 = this.mService;
/* .line 302 */
(( com.android.server.inputmethod.InputMethodManagerService ) v0 ).getSelectedMethodIdLocked ( ); // invoke-virtual {v0}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;
/* .line 301 */
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->isSecMethodLocked(Ljava/lang/String;)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 303 */
v0 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v0 ).clearClientSessionsLocked ( ); // invoke-virtual {v0}, Lcom/android/server/inputmethod/InputMethodManagerService;->clearClientSessionsLocked()V
/* .line 304 */
v0 = this.mService;
int v1 = 2; // const/4 v1, 0x2
(( com.android.server.inputmethod.InputMethodManagerService ) v0 ).unbindCurrentClientLocked ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/inputmethod/InputMethodManagerService;->unbindCurrentClientLocked(I)V
/* .line 306 */
} // :cond_0
return;
} // .end method
/* # virtual methods */
public java.util.List filterMethodLocked ( java.util.List p0 ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Landroid/view/inputmethod/InputMethodInfo;", */
/* ">;)", */
/* "Ljava/util/List<", */
/* "Landroid/view/inputmethod/InputMethodInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 209 */
/* .local p1, "methodInfos":Ljava/util/List;, "Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;" */
/* sget-boolean v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->SUPPORT_SEC_INPUT_METHOD:Z */
/* if-nez v0, :cond_0 */
/* .line 210 */
/* .line 212 */
} // :cond_0
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 213 */
/* .local v0, "noSecMethodInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/inputmethod/InputMethodInfo;>;" */
if ( p1 != null) { // if-eqz p1, :cond_2
/* .line 214 */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Landroid/view/inputmethod/InputMethodInfo; */
/* .line 215 */
/* .local v2, "methodInfo":Landroid/view/inputmethod/InputMethodInfo; */
(( android.view.inputmethod.InputMethodInfo ) v2 ).getId ( ); // invoke-virtual {v2}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;
v3 = /* invoke-direct {p0, v3}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->isSecMethodLocked(Ljava/lang/String;)Z */
/* if-nez v3, :cond_1 */
/* .line 216 */
(( java.util.ArrayList ) v0 ).add ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 218 */
} // .end local v2 # "methodInfo":Landroid/view/inputmethod/InputMethodInfo;
} // :cond_1
/* .line 220 */
} // :cond_2
} // .end method
public Boolean mayChangeInputMethodLocked ( android.view.inputmethod.EditorInfo p0, Integer p1 ) {
/* .locals 17 */
/* .param p1, "attribute" # Landroid/view/inputmethod/EditorInfo; */
/* .param p2, "startInputReason" # I */
/* .line 68 */
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p1 */
/* move/from16 v2, p2 */
/* sget-boolean v3, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->SUPPORT_SEC_INPUT_METHOD:Z */
int v4 = 0; // const/4 v4, 0x0
/* if-nez v3, :cond_0 */
/* .line 69 */
/* .line 71 */
} // :cond_0
v3 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v3 ).getSelectedMethodIdLocked ( ); // invoke-virtual {v3}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;
final String v5 = "SecurityInputMethodSwitcher"; // const-string v5, "SecurityInputMethodSwitcher"
/* if-nez v3, :cond_1 */
/* .line 72 */
final String v3 = "input_service has no current_method_id"; // const-string v3, "input_service has no current_method_id"
android.util.Slog .w ( v5,v3 );
/* .line 73 */
/* .line 75 */
} // :cond_1
/* if-nez v1, :cond_2 */
/* .line 76 */
final String v3 = "editor_info is null, we cannot judge"; // const-string v3, "editor_info is null, we cannot judge"
android.util.Slog .w ( v5,v3 );
/* .line 77 */
/* .line 79 */
} // :cond_2
com.android.server.inputmethod.InputMethodManagerServiceImpl .getInstance ( );
v3 = this.mBindingController;
/* if-nez v3, :cond_3 */
/* .line 80 */
final String v3 = "IMMS_IMPL has not init"; // const-string v3, "IMMS_IMPL has not init"
android.util.Slog .w ( v5,v3 );
/* .line 81 */
/* .line 83 */
} // :cond_3
v3 = this.mService;
v3 = this.mMethodMap;
v6 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v6 ).getSelectedMethodIdLocked ( ); // invoke-virtual {v6}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;
(( android.util.ArrayMap ) v3 ).get ( v6 ); // invoke-virtual {v3, v6}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v3, Landroid/view/inputmethod/InputMethodInfo; */
/* .line 84 */
/* .local v3, "curMethodInfo":Landroid/view/inputmethod/InputMethodInfo; */
/* if-nez v3, :cond_4 */
/* .line 85 */
final String v6 = "fail to find current_method_info in the map"; // const-string v6, "fail to find current_method_info in the map"
android.util.Slog .w ( v5,v6 );
/* .line 86 */
/* .line 89 */
} // :cond_4
/* iget v6, v1, Landroid/view/inputmethod/EditorInfo;->inputType:I */
v6 = com.android.server.inputmethod.SecurityInputMethodSwitcher .isPasswdInputType ( v6 );
int v7 = 1; // const/4 v7, 0x1
if ( v6 != null) { // if-eqz v6, :cond_5
v6 = this.mService;
/* .line 90 */
(( com.android.server.inputmethod.InputMethodManagerService ) v6 ).getSelectedMethodIdLocked ( ); // invoke-virtual {v6}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;
v6 = /* invoke-direct {v0, v6}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->isSecMethodLocked(Ljava/lang/String;)Z */
/* if-nez v6, :cond_5 */
/* iget-boolean v6, v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mSecEnabled:Z */
if ( v6 != null) { // if-eqz v6, :cond_5
/* .line 91 */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->getSecMethodIdLocked()Ljava/lang/String; */
v6 = android.text.TextUtils .isEmpty ( v6 );
/* if-nez v6, :cond_5 */
v6 = /* invoke-direct/range {p0 ..p1}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->isEditorInDefaultImeApp(Landroid/view/inputmethod/EditorInfo;)Z */
/* if-nez v6, :cond_5 */
/* move v6, v7 */
} // :cond_5
/* move v6, v4 */
/* .line 94 */
/* .local v6, "switchToSecInput":Z */
} // :goto_0
v8 = com.android.server.inputmethod.MiuiInputMethodStub .getInstance ( );
if ( v8 != null) { // if-eqz v8, :cond_6
v8 = this.mService;
/* .line 95 */
(( com.android.server.inputmethod.InputMethodManagerService ) v8 ).getSelectedMethodIdLocked ( ); // invoke-virtual {v8}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;
v8 = /* invoke-direct {v0, v8}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->isSecMethodLocked(Ljava/lang/String;)Z */
/* if-nez v8, :cond_6 */
/* iget v8, v1, Landroid/view/inputmethod/EditorInfo;->inputType:I */
if ( v8 != null) { // if-eqz v8, :cond_6
/* iget-boolean v8, v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mSecEnabled:Z */
if ( v8 != null) { // if-eqz v8, :cond_6
/* move v8, v7 */
} // :cond_6
/* move v8, v4 */
} // :goto_1
/* or-int/2addr v6, v8 */
/* .line 97 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "first check switch to security ime: "; // const-string v9, "first check switch to security ime: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v6 ); // invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v5,v8 );
/* .line 99 */
/* if-nez v6, :cond_8 */
/* .line 100 */
com.android.server.inputmethod.MiuiInputMethodStub .getInstance ( );
v8 = /* .line 101 */
/* if-nez v8, :cond_7 */
v8 = this.mService;
/* .line 102 */
(( com.android.server.inputmethod.InputMethodManagerService ) v8 ).getSelectedMethodIdLocked ( ); // invoke-virtual {v8}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;
v8 = /* invoke-direct {v0, v8}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->isSecMethodLocked(Ljava/lang/String;)Z */
/* if-nez v8, :cond_7 */
/* .line 103 */
com.android.server.inputmethod.MiuiInputMethodStub .getInstance ( );
v9 = this.mService;
v9 = this.mCurFocusedWindow;
v8 = /* .line 104 */
if ( v8 != null) { // if-eqz v8, :cond_7
/* iget v8, v1, Landroid/view/inputmethod/EditorInfo;->inputType:I */
if ( v8 != null) { // if-eqz v8, :cond_7
/* iget-boolean v8, v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mSecEnabled:Z */
if ( v8 != null) { // if-eqz v8, :cond_7
/* move v8, v7 */
} // :cond_7
/* move v8, v4 */
} // :goto_2
/* or-int/2addr v6, v8 */
/* .line 107 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "double check switch to security ime: "; // const-string v9, "double check switch to security ime: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v6 ); // invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v5,v8 );
/* .line 109 */
} // :cond_8
v8 = com.android.server.inputmethod.MiuiInputMethodStub .getInstance ( );
/* const/16 v9, 0x8 */
if ( v8 != null) { // if-eqz v8, :cond_a
/* iget v8, v1, Landroid/view/inputmethod/EditorInfo;->inputType:I */
/* .line 110 */
v8 = com.android.server.inputmethod.SecurityInputMethodSwitcher .isPasswdInputType ( v8 );
/* if-nez v8, :cond_a */
/* const/16 v8, 0x9 */
/* if-eq v2, v8, :cond_9 */
/* if-ne v2, v9, :cond_a */
/* .line 113 */
} // :cond_9
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v10, "switch to the security ime byself: " */
(( java.lang.StringBuilder ) v8 ).append ( v10 ); // invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v6 ); // invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v10 = " startInputReason: "; // const-string v10, " startInputReason: "
(( java.lang.StringBuilder ) v8 ).append ( v10 ); // invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v2 ); // invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v5,v8 );
/* .line 115 */
int v6 = 0; // const/4 v6, 0x0
/* .line 117 */
} // :cond_a
/* if-ne v2, v9, :cond_b */
/* .line 118 */
com.android.server.inputmethod.MiuiInputMethodStub .getInstance ( );
/* .line 120 */
} // :cond_b
v8 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v8 ).getSelectedMethodIdLocked ( ); // invoke-virtual {v8}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;
v8 = /* invoke-direct {v0, v8}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->isSecMethodLocked(Ljava/lang/String;)Z */
if ( v8 != null) { // if-eqz v8, :cond_d
/* iget-boolean v8, v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mSecEnabled:Z */
if ( v8 != null) { // if-eqz v8, :cond_c
/* iget v8, v1, Landroid/view/inputmethod/EditorInfo;->inputType:I */
/* .line 121 */
v8 = com.android.server.inputmethod.SecurityInputMethodSwitcher .isPasswdInputType ( v8 );
if ( v8 != null) { // if-eqz v8, :cond_c
/* .line 122 */
v8 = /* invoke-direct/range {p0 ..p1}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->isEditorInDefaultImeApp(Landroid/view/inputmethod/EditorInfo;)Z */
if ( v8 != null) { // if-eqz v8, :cond_d
} // :cond_c
/* move v8, v7 */
} // :cond_d
/* move v8, v4 */
/* .line 123 */
/* .local v8, "switchFromSecInput":Z */
} // :goto_3
/* nop */
/* .line 124 */
v9 = com.android.server.inputmethod.MiuiInputMethodStub .getInstance ( );
/* if-nez v9, :cond_e */
v9 = this.mService;
v9 = this.mCurFocusedWindow;
/* .line 125 */
v9 = (( com.android.server.inputmethod.SecurityInputMethodSwitcher ) v0 ).mayChangeToMiuiSecurityInputMethod ( v9 ); // invoke-virtual {v0, v9}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mayChangeToMiuiSecurityInputMethod(Landroid/os/IBinder;)Z
/* if-nez v9, :cond_e */
/* move v9, v7 */
} // :cond_e
/* move v9, v4 */
} // :goto_4
/* and-int/2addr v8, v9 */
/* .line 126 */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v10, "switch to other input method from security ime:" */
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v8 ); // invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v10 = " mayChangeToMiuiSecurityInputMethod: "; // const-string v10, " mayChangeToMiuiSecurityInputMethod: "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 127 */
com.android.server.inputmethod.MiuiInputMethodStub .getInstance ( );
v11 = this.mService;
v11 = this.mCurFocusedWindow;
v10 = /* .line 128 */
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v10 = " current focus window: "; // const-string v10, " current focus window: "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v10 = this.mService;
v10 = this.mCurFocusedWindow;
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 126 */
android.util.Slog .w ( v5,v9 );
/* .line 130 */
int v9 = -1; // const/4 v9, -0x1
int v10 = 2; // const/4 v10, 0x2
if ( v6 != null) { // if-eqz v6, :cond_10
/* .line 131 */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->getSecMethodIdLocked()Ljava/lang/String; */
/* .line 132 */
/* .local v11, "secInputMethodId":Ljava/lang/String; */
v12 = android.text.TextUtils .isEmpty ( v11 );
if ( v12 != null) { // if-eqz v12, :cond_f
/* .line 133 */
final String v7 = "fail to find secure_input_method in input_method_list"; // const-string v7, "fail to find secure_input_method in input_method_list"
android.util.Slog .w ( v5,v7 );
/* .line 134 */
/* .line 136 */
} // :cond_f
com.android.server.inputmethod.InputMethodManagerServiceImpl .getInstance ( );
v4 = this.mBindingController;
/* .line 137 */
(( com.android.server.inputmethod.InputMethodBindingController ) v4 ).setSelectedMethodId ( v11 ); // invoke-virtual {v4, v11}, Lcom/android/server/inputmethod/InputMethodBindingController;->setSelectedMethodId(Ljava/lang/String;)V
/* .line 138 */
v4 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v4 ).clearClientSessionsLocked ( ); // invoke-virtual {v4}, Lcom/android/server/inputmethod/InputMethodManagerService;->clearClientSessionsLocked()V
/* .line 139 */
v4 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v4 ).unbindCurrentClientLocked ( v10 ); // invoke-virtual {v4, v10}, Lcom/android/server/inputmethod/InputMethodManagerService;->unbindCurrentClientLocked(I)V
/* .line 140 */
com.android.server.inputmethod.InputMethodManagerServiceImpl .getInstance ( );
v4 = this.mBindingController;
(( com.android.server.inputmethod.InputMethodBindingController ) v4 ).unbindCurrentMethod ( ); // invoke-virtual {v4}, Lcom/android/server/inputmethod/InputMethodBindingController;->unbindCurrentMethod()V
/* .line 141 */
v4 = this.mService;
v5 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v5 ).getSelectedMethodIdLocked ( ); // invoke-virtual {v5}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;
(( com.android.server.inputmethod.InputMethodManagerService ) v4 ).setInputMethodLocked ( v5, v9 ); // invoke-virtual {v4, v5, v9}, Lcom/android/server/inputmethod/InputMethodManagerService;->setInputMethodLocked(Ljava/lang/String;I)V
/* .line 142 */
/* .line 143 */
} // .end local v11 # "secInputMethodId":Ljava/lang/String;
} // :cond_10
if ( v8 != null) { // if-eqz v8, :cond_18
/* .line 144 */
v11 = this.mService;
v11 = this.mSettings;
(( com.android.server.inputmethod.InputMethodUtils$InputMethodSettings ) v11 ).getSelectedInputMethod ( ); // invoke-virtual {v11}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getSelectedInputMethod()Ljava/lang/String;
/* .line 145 */
/* .local v11, "selectedInputMethod":Ljava/lang/String; */
v12 = android.text.TextUtils .isEmpty ( v11 );
if ( v12 != null) { // if-eqz v12, :cond_15
/* .line 146 */
/* const-string/jumbo v12, "something is weired, maybe the input method app are uninstalled" */
android.util.Slog .i ( v5,v12 );
/* .line 147 */
v12 = this.mService;
v12 = this.mSettings;
/* .line 148 */
(( com.android.server.inputmethod.InputMethodUtils$InputMethodSettings ) v12 ).getEnabledInputMethodListLocked ( ); // invoke-virtual {v12}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getEnabledInputMethodListLocked()Ljava/util/ArrayList;
/* .line 147 */
com.android.server.inputmethod.InputMethodInfoUtils .getMostApplicableDefaultIME ( v12 );
/* .line 149 */
/* .local v12, "imi":Landroid/view/inputmethod/InputMethodInfo; */
final String v13 = "com.miui.securityinputmethod"; // const-string v13, "com.miui.securityinputmethod"
if ( v12 != null) { // if-eqz v12, :cond_11
(( android.view.inputmethod.InputMethodInfo ) v12 ).getPackageName ( ); // invoke-virtual {v12}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;
v14 = android.text.TextUtils .equals ( v14,v13 );
if ( v14 != null) { // if-eqz v14, :cond_15
/* .line 151 */
} // :cond_11
final String v14 = "fail to find a most applicable default ime"; // const-string v14, "fail to find a most applicable default ime"
android.util.Slog .w ( v5,v14 );
/* .line 152 */
v14 = this.mService;
v14 = this.mSettings;
/* .line 153 */
(( com.android.server.inputmethod.InputMethodUtils$InputMethodSettings ) v14 ).getEnabledInputMethodListLocked ( ); // invoke-virtual {v14}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getEnabledInputMethodListLocked()Ljava/util/ArrayList;
/* .line 154 */
/* .local v14, "imiList":Ljava/util/List;, "Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;" */
v15 = if ( v14 != null) { // if-eqz v14, :cond_14
/* if-nez v15, :cond_12 */
/* .line 158 */
} // :cond_12
v16 = } // :goto_5
if ( v16 != null) { // if-eqz v16, :cond_15
/* check-cast v16, Landroid/view/inputmethod/InputMethodInfo; */
/* .line 159 */
/* .local v16, "inputMethodInfo":Landroid/view/inputmethod/InputMethodInfo; */
/* invoke-virtual/range {v16 ..v16}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String; */
v7 = android.text.TextUtils .equals ( v7,v13 );
/* if-nez v7, :cond_13 */
/* .line 161 */
/* invoke-virtual/range {v16 ..v16}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String; */
/* .line 162 */
/* .line 164 */
} // .end local v16 # "inputMethodInfo":Landroid/view/inputmethod/InputMethodInfo;
} // :cond_13
int v7 = 1; // const/4 v7, 0x1
/* .line 155 */
} // :cond_14
} // :goto_6
/* const-string/jumbo v7, "there is no enabled method list" */
android.util.Slog .w ( v5,v7 );
/* .line 156 */
/* .line 167 */
} // .end local v12 # "imi":Landroid/view/inputmethod/InputMethodInfo;
} // .end local v14 # "imiList":Ljava/util/List;, "Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;"
} // :cond_15
} // :goto_7
v7 = android.text.TextUtils .isEmpty ( v11 );
if ( v7 != null) { // if-eqz v7, :cond_16
/* .line 168 */
final String v7 = "finally, we still fail to find default input method"; // const-string v7, "finally, we still fail to find default input method"
android.util.Slog .w ( v5,v7 );
/* .line 169 */
/* .line 171 */
} // :cond_16
v7 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v7 ).getSelectedMethodIdLocked ( ); // invoke-virtual {v7}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;
v7 = android.text.TextUtils .equals ( v7,v11 );
if ( v7 != null) { // if-eqz v7, :cond_17
/* .line 172 */
final String v7 = "It looks like there is only miui_sec_input_method in the system"; // const-string v7, "It looks like there is only miui_sec_input_method in the system"
android.util.Slog .w ( v5,v7 );
/* .line 173 */
/* .line 175 */
} // :cond_17
com.android.server.inputmethod.InputMethodManagerServiceImpl .getInstance ( );
v4 = this.mBindingController;
/* .line 176 */
(( com.android.server.inputmethod.InputMethodBindingController ) v4 ).setSelectedMethodId ( v11 ); // invoke-virtual {v4, v11}, Lcom/android/server/inputmethod/InputMethodBindingController;->setSelectedMethodId(Ljava/lang/String;)V
/* .line 177 */
v4 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v4 ).clearClientSessionsLocked ( ); // invoke-virtual {v4}, Lcom/android/server/inputmethod/InputMethodManagerService;->clearClientSessionsLocked()V
/* .line 178 */
v4 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v4 ).unbindCurrentClientLocked ( v10 ); // invoke-virtual {v4, v10}, Lcom/android/server/inputmethod/InputMethodManagerService;->unbindCurrentClientLocked(I)V
/* .line 179 */
com.android.server.inputmethod.InputMethodManagerServiceImpl .getInstance ( );
v4 = this.mBindingController;
(( com.android.server.inputmethod.InputMethodBindingController ) v4 ).unbindCurrentMethod ( ); // invoke-virtual {v4}, Lcom/android/server/inputmethod/InputMethodBindingController;->unbindCurrentMethod()V
/* .line 180 */
v4 = this.mService;
v5 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v5 ).getSelectedMethodIdLocked ( ); // invoke-virtual {v5}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;
(( com.android.server.inputmethod.InputMethodManagerService ) v4 ).setInputMethodLocked ( v5, v9 ); // invoke-virtual {v4, v5, v9}, Lcom/android/server/inputmethod/InputMethodManagerService;->setInputMethodLocked(Ljava/lang/String;I)V
/* .line 181 */
int v4 = 1; // const/4 v4, 0x1
/* .line 183 */
} // .end local v11 # "selectedInputMethod":Ljava/lang/String;
} // :cond_18
} // .end method
public void onSwitchUserLocked ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "newUserId" # I */
/* .line 58 */
/* sget-boolean v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->SUPPORT_SEC_INPUT_METHOD:Z */
/* if-nez v0, :cond_0 */
/* .line 59 */
return;
/* .line 61 */
} // :cond_0
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->getParentUserId(I)I */
/* .line 62 */
/* .local v0, "parentUserId":I */
v1 = this.mSettingsObserver;
(( com.android.server.inputmethod.SecurityInputMethodSwitcher$SettingsObserver ) v1 ).registerContentObserverLocked ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher$SettingsObserver;->registerContentObserverLocked(I)V
/* .line 63 */
v1 = /* invoke-direct {p0, v0}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->isSecEnabled(I)Z */
/* iput-boolean v1, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mSecEnabled:Z */
/* .line 64 */
return;
} // .end method
public void onSystemRunningLocked ( ) {
/* .locals 2 */
/* .line 47 */
/* sget-boolean v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->SUPPORT_SEC_INPUT_METHOD:Z */
/* if-nez v0, :cond_0 */
/* .line 48 */
return;
/* .line 50 */
} // :cond_0
/* new-instance v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher$SettingsObserver; */
com.android.server.inputmethod.InputMethodManagerServiceImpl .getInstance ( );
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher$SettingsObserver;-><init>(Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;Landroid/os/Handler;)V */
this.mSettingsObserver = v0;
/* .line 51 */
v0 = this.mService;
v0 = this.mSettings;
v0 = (( com.android.server.inputmethod.InputMethodUtils$InputMethodSettings ) v0 ).getCurrentUserId ( ); // invoke-virtual {v0}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getCurrentUserId()I
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->getParentUserId(I)I */
/* .line 52 */
/* .local v0, "parentUserId":I */
v1 = this.mSettingsObserver;
(( com.android.server.inputmethod.SecurityInputMethodSwitcher$SettingsObserver ) v1 ).registerContentObserverLocked ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher$SettingsObserver;->registerContentObserverLocked(I)V
/* .line 53 */
v1 = /* invoke-direct {p0, v0}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->isSecEnabled(I)Z */
/* iput-boolean v1, p0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->mSecEnabled:Z */
/* .line 54 */
return;
} // .end method
public void removeMethod ( java.util.List p0 ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 195 */
/* .local p1, "imList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;>;" */
/* sget-boolean v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->SUPPORT_SEC_INPUT_METHOD:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
v0 = if ( p1 != null) { // if-eqz p1, :cond_3
/* if-nez v0, :cond_0 */
/* .line 198 */
} // :cond_0
/* .line 199 */
/* .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;>;" */
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 200 */
/* check-cast v1, Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem; */
/* .line 201 */
/* .local v1, "imeSubtypeListItem":Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem; */
v2 = this.mImi;
/* .line 202 */
/* .local v2, "imi":Landroid/view/inputmethod/InputMethodInfo; */
(( android.view.inputmethod.InputMethodInfo ) v2 ).getPackageName ( ); // invoke-virtual {v2}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;
final String v4 = "com.miui.securityinputmethod"; // const-string v4, "com.miui.securityinputmethod"
v3 = android.text.TextUtils .equals ( v3,v4 );
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 203 */
/* .line 205 */
} // .end local v1 # "imeSubtypeListItem":Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;
} // .end local v2 # "imi":Landroid/view/inputmethod/InputMethodInfo;
} // :cond_1
/* .line 206 */
} // :cond_2
return;
/* .line 196 */
} // .end local v0 # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;>;"
} // :cond_3
} // :goto_1
return;
} // .end method
public Boolean shouldHideImeSwitcherLocked ( ) {
/* .locals 1 */
/* .line 188 */
/* sget-boolean v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->SUPPORT_SEC_INPUT_METHOD:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mService;
(( com.android.server.inputmethod.InputMethodManagerService ) v0 ).getSelectedMethodIdLocked ( ); // invoke-virtual {v0}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;->isSecMethodLocked(Ljava/lang/String;)Z */
/* if-nez v0, :cond_1 */
/* .line 189 */
} // :cond_0
com.android.server.inputmethod.InputMethodManagerServiceImpl .getInstance ( );
v0 = this.mBindingController;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 190 */
com.android.server.inputmethod.InputMethodManagerServiceImpl .getInstance ( );
v0 = this.mBindingController;
(( com.android.server.inputmethod.InputMethodBindingController ) v0 ).getCurMethod ( ); // invoke-virtual {v0}, Lcom/android/server/inputmethod/InputMethodBindingController;->getCurMethod()Lcom/android/server/inputmethod/IInputMethodInvoker;
/* if-nez v0, :cond_2 */
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
/* .line 188 */
} // :goto_0
} // .end method
