.class Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$4;
.super Landroid/database/ContentObserver;
.source "InputMethodManagerServiceImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->registerObserver(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;Landroid/os/Handler;Landroid/content/Context;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 690
    iput-object p1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$4;->this$0:Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    iput-object p3, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$4;->val$context:Landroid/content/Context;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 4
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 694
    const-string v0, "mirror_input_state"

    const/4 v1, 0x0

    .line 695
    .local v1, "mirrorInputState":I
    const/4 v2, 0x1

    :try_start_0
    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$4;->val$context:Landroid/content/Context;

    if-eqz v3, :cond_1

    .line 697
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {v3, v0}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v0

    .line 699
    .end local v1    # "mirrorInputState":I
    .local v0, "mirrorInputState":I
    iget-object v1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$4;->this$0:Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    if-ne v0, v2, :cond_0

    const/4 v3, 0x0

    goto :goto_0

    :cond_0
    move v3, v2

    :goto_0
    invoke-static {v1, v3}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->-$$Nest$fputmMirrorInputState(Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 704
    .end local v0    # "mirrorInputState":I
    :cond_1
    goto :goto_1

    .line 701
    :catch_0
    move-exception v0

    .line 702
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$4;->this$0:Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    invoke-static {v1, v2}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->-$$Nest$fputmMirrorInputState(Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;Z)V

    .line 703
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception caused:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", set mMirrorInputState to true"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "InputMethodManagerServiceImpl"

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 705
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    return-void
.end method
