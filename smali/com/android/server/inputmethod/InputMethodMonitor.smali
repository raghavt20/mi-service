.class public Lcom/android/server/inputmethod/InputMethodMonitor;
.super Ljava/lang/Object;
.source "InputMethodMonitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/inputmethod/InputMethodMonitor$SINGLETON;,
        Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;
    }
.end annotation


# static fields
.field private static final DUMP_DIR:Ljava/lang/String; = "/data/miuilog/stability/resleak/fdtrack/"

.field static final LEAK_LOW_WATERMARK:I = 0x64

.field private static final TAG:Ljava/lang/String; = "InputMethodMonitor"


# instance fields
.field private final mCache:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/ref/WeakReference<",
            "Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;",
            ">;>;"
        }
    .end annotation
.end field

.field private mHighWatermark:I

.field private final mRecord:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap<",
            "Lcom/android/internal/inputmethod/IInputMethodSessionCallback$Stub;",
            "Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;",
            ">;"
        }
    .end annotation
.end field

.field mWarning:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/inputmethod/InputMethodMonitor;->mHighWatermark:I

    .line 57
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/inputmethod/InputMethodMonitor;->mRecord:Ljava/util/WeakHashMap;

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/inputmethod/InputMethodMonitor;->mCache:Ljava/util/ArrayList;

    .line 71
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/inputmethod/InputMethodMonitor-IA;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/inputmethod/InputMethodMonitor;-><init>()V

    return-void
.end method

.method private doAppDeath(Z)V
    .locals 9
    .param p1, "inTest"    # Z

    .line 139
    invoke-virtual {p0}, Lcom/android/server/inputmethod/InputMethodMonitor;->findTheEvil()Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;

    move-result-object v0

    .line 141
    .local v0, "evil":Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;
    if-eqz v0, :cond_4

    if-eqz p1, :cond_0

    goto/16 :goto_0

    .line 144
    :cond_0
    const-class v1, Landroid/content/pm/PackageManagerInternal;

    invoke-static {v1}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/PackageManagerInternal;

    .line 145
    .local v1, "pm":Landroid/content/pm/PackageManagerInternal;
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    move-object v8, v2

    .line 146
    .local v8, "intent":Landroid/content/Intent;
    iget-object v2, v0, Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;->component:Landroid/content/ComponentName;

    invoke-virtual {v8, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 147
    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    iget v7, v0, Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;->userId:I

    move-object v2, v1

    move-object v3, v8

    invoke-virtual/range {v2 .. v7}, Landroid/content/pm/PackageManagerInternal;->queryIntentServices(Landroid/content/Intent;JII)Ljava/util/List;

    move-result-object v2

    .line 150
    .local v2, "serviceInfos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    .line 151
    return-void

    .line 153
    :cond_1
    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/ResolveInfo;

    iget-object v4, v4, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v4, v4, Landroid/content/pm/ServiceInfo;->processName:Ljava/lang/String;

    .line 154
    .local v4, "process":Ljava/lang/String;
    iget-object v5, v0, Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;->component:Landroid/content/ComponentName;

    invoke-virtual {v5}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v5

    .line 155
    .local v5, "packageName":Ljava/lang/String;
    if-nez v4, :cond_2

    move-object v4, v5

    .line 156
    :cond_2
    invoke-static {}, Lmiui/mqsas/scout/ScoutUtils;->isLibraryTest()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 157
    invoke-static {v4}, Lcom/android/server/inputmethod/InputMethodMonitor;->dumpBacktrace(Ljava/lang/String;)V

    .line 158
    invoke-static {v0, v4}, Lcom/android/server/inputmethod/InputMethodMonitor;->dumpHprof(Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;Ljava/lang/String;)V

    .line 161
    :cond_3
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Too many createSession requests unhandled, killing "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", userId="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v0, Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;->userId:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "InputMethodMonitor"

    invoke-static {v7, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    const-class v6, Landroid/app/ActivityManagerInternal;

    invoke-static {v6}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/ActivityManagerInternal;

    .line 164
    .local v6, "am":Landroid/app/ActivityManagerInternal;
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/ResolveInfo;

    iget-object v3, v3, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v3, v3, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v3, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    const-string v7, "ime_hang"

    invoke-virtual {v6, v4, v3, v7}, Landroid/app/ActivityManagerInternal;->killProcess(Ljava/lang/String;ILjava/lang/String;)V

    .line 165
    return-void

    .line 142
    .end local v1    # "pm":Landroid/content/pm/PackageManagerInternal;
    .end local v2    # "serviceInfos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v4    # "process":Ljava/lang/String;
    .end local v5    # "packageName":Ljava/lang/String;
    .end local v6    # "am":Landroid/app/ActivityManagerInternal;
    .end local v8    # "intent":Landroid/content/Intent;
    :cond_4
    :goto_0
    return-void
.end method

.method private static dumpBacktrace(Ljava/lang/String;)V
    .locals 7
    .param p0, "process"    # Ljava/lang/String;

    .line 198
    const-string v0, "/data/miuilog/stability/resleak/fdtrack/"

    invoke-static {v0}, Lmiui/mqsas/scout/ScoutUtils;->ensureDumpDir(Ljava/lang/String;)Z

    .line 199
    const/4 v1, 0x2

    const-string v2, ".trace"

    invoke-static {v0, v2, v1}, Lmiui/mqsas/scout/ScoutUtils;->removeHistoricalDumps(Ljava/lang/String;Ljava/lang/String;I)V

    .line 200
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string/jumbo v3, "yyyy_MM_dd_HH_mm_ss"

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v1, v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 201
    .local v1, "dateFormat":Ljava/text/SimpleDateFormat;
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    .line 202
    .local v3, "datetime":Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "inputmethod_hang_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v4

    .line 203
    .local v0, "traceFile":Ljava/io/File;
    filled-new-array {p0}, [Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/os/Process;->getPidsForCommands([Ljava/lang/String;)[I

    move-result-object v2

    .line 204
    .local v2, "pids":[I
    if-eqz v2, :cond_2

    array-length v4, v2

    if-nez v4, :cond_0

    goto :goto_0

    .line 208
    :cond_0
    invoke-static {}, Lcom/android/server/am/ActivityManagerServiceStub;->get()Lcom/android/server/am/ActivityManagerServiceStub;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/android/server/am/ActivityManagerServiceStub;->dumpMiuiStackTraces([I)Ljava/lang/String;

    move-result-object v4

    .line 209
    .local v4, "path":Ljava/lang/String;
    if-eqz v4, :cond_1

    .line 210
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 211
    .local v5, "file":Ljava/io/File;
    invoke-virtual {v5, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 213
    .end local v5    # "file":Ljava/io/File;
    :cond_1
    return-void

    .line 205
    .end local v4    # "path":Ljava/lang/String;
    :cond_2
    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "failed to dump trace of "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": process disappeared"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "InputMethodMonitor"

    invoke-static {v5, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    return-void
.end method

.method private static dumpHprof(Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;Ljava/lang/String;)V
    .locals 17
    .param p0, "app"    # Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;
    .param p1, "process"    # Ljava/lang/String;

    .line 217
    move-object/from16 v10, p1

    const-string v11, "failed to dump hprof of "

    const-string v12, "InputMethodMonitor"

    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "inputmethod_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".hprof"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/data/miuilog/stability/resleak/fdtrack/"

    invoke-direct {v0, v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v13, v0

    .line 218
    .local v13, "heapFile":Ljava/io/File;
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    move-object v14, v0

    .line 219
    .local v14, "latch":Ljava/util/concurrent/CountDownLatch;
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/data/data/android/cache/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v13}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".tmp"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object v15, v0

    .line 222
    .local v15, "temp":Ljava/io/File;
    const/high16 v0, 0x38000000

    .line 223
    .local v0, "mode":I
    :try_start_0
    invoke-static {v15, v0}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v8

    .line 224
    .local v8, "fd":Landroid/os/ParcelFileDescriptor;
    invoke-static {}, Landroid/app/ActivityManager;->getService()Landroid/app/IActivityManager;

    move-result-object v1

    move-object/from16 v9, p0

    iget v3, v9, Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;->userId:I

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 226
    invoke-virtual {v15}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    new-instance v2, Landroid/os/RemoteCallback;

    new-instance v6, Lcom/android/server/inputmethod/InputMethodMonitor$$ExternalSyntheticLambda1;

    invoke-direct {v6, v14}, Lcom/android/server/inputmethod/InputMethodMonitor$$ExternalSyntheticLambda1;-><init>(Ljava/util/concurrent/CountDownLatch;)V

    invoke-direct {v2, v6}, Landroid/os/RemoteCallback;-><init>(Landroid/os/RemoteCallback$OnResultListener;)V

    .line 224
    move-object/from16 v16, v2

    move-object/from16 v2, p1

    const/4 v6, 0x0

    move-object/from16 v9, v16

    invoke-interface/range {v1 .. v9}, Landroid/app/IActivityManager;->dumpHeap(Ljava/lang/String;IZZZLjava/lang/String;Landroid/os/ParcelFileDescriptor;Landroid/os/RemoteCallback;)Z

    move-result v1

    .line 230
    .local v1, "dumping":Z
    if-eqz v1, :cond_2

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v3, 0x3c

    invoke-virtual {v14, v3, v4, v2}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    .line 234
    :cond_0
    invoke-virtual {v15}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 237
    invoke-static {v15, v13}, Landroid/os/FileUtils;->copy(Ljava/io/File;Ljava/io/File;)J

    .line 238
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "hprof saved to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v12, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    .end local v0    # "mode":I
    .end local v1    # "dumping":Z
    .end local v8    # "fd":Landroid/os/ParcelFileDescriptor;
    :cond_1
    goto :goto_1

    .line 231
    .restart local v0    # "mode":I
    .restart local v1    # "dumping":Z
    .restart local v8    # "fd":Landroid/os/ParcelFileDescriptor;
    :cond_2
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": timeout"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v12, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 243
    invoke-virtual {v15}, Ljava/io/File;->delete()Z

    .line 232
    return-void

    .line 243
    .end local v0    # "mode":I
    .end local v1    # "dumping":Z
    .end local v8    # "fd":Landroid/os/ParcelFileDescriptor;
    :catchall_0
    move-exception v0

    goto :goto_2

    .line 240
    :catch_0
    move-exception v0

    .line 241
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v12, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 243
    nop

    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    invoke-virtual {v15}, Ljava/io/File;->delete()Z

    .line 244
    nop

    .line 245
    return-void

    .line 243
    :goto_2
    invoke-virtual {v15}, Ljava/io/File;->delete()Z

    .line 244
    throw v0
.end method

.method private expungeStaleItems()V
    .locals 2

    .line 94
    iget-object v0, p0, Lcom/android/server/inputmethod/InputMethodMonitor;->mCache:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_1

    .line 95
    iget-object v1, p0, Lcom/android/server/inputmethod/InputMethodMonitor;->mCache:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 96
    iget-object v1, p0, Lcom/android/server/inputmethod/InputMethodMonitor;->mCache:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 94
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 99
    .end local v0    # "i":I
    :cond_1
    return-void
.end method

.method private getInputMethodInfo(ILandroid/content/ComponentName;)Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;
    .locals 4
    .param p1, "userId"    # I
    .param p2, "componentName"    # Landroid/content/ComponentName;

    .line 103
    iget-object v0, p0, Lcom/android/server/inputmethod/InputMethodMonitor;->mCache:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    .line 104
    .local v1, "ref":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;>;"
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;

    .line 105
    .local v2, "info":Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;
    if-eqz v2, :cond_0

    iget-object v3, v2, Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;->component:Landroid/content/ComponentName;

    invoke-virtual {v3, p2}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget v3, v2, Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;->userId:I

    if-ne v3, p1, :cond_0

    .line 106
    return-object v2

    .line 108
    .end local v1    # "ref":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;>;"
    .end local v2    # "info":Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;
    :cond_0
    goto :goto_0

    .line 109
    :cond_1
    new-instance v0, Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;

    invoke-direct {v0, p2, p1}, Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;-><init>(Landroid/content/ComponentName;I)V

    .line 110
    .local v0, "info":Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;
    iget-object v1, p0, Lcom/android/server/inputmethod/InputMethodMonitor;->mCache:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 111
    return-object v0
.end method

.method static getInstance()Lcom/android/server/inputmethod/InputMethodMonitor;
    .locals 1

    .line 63
    invoke-static {}, Lcom/android/server/inputmethod/InputMethodMonitor$SINGLETON;->-$$Nest$sfgetINSTANCE()Lcom/android/server/inputmethod/InputMethodMonitor;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$dumpHprof$0(Ljava/util/concurrent/CountDownLatch;Landroid/os/Bundle;)V
    .locals 0
    .param p0, "latch"    # Ljava/util/concurrent/CountDownLatch;
    .param p1, "result"    # Landroid/os/Bundle;

    .line 227
    invoke-virtual {p0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 228
    return-void
.end method


# virtual methods
.method public declared-synchronized enable(I)V
    .locals 1
    .param p1, "highWatermark"    # I

    monitor-enter p0

    .line 67
    const/16 v0, 0xc8

    :try_start_0
    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/android/server/inputmethod/InputMethodMonitor;->mHighWatermark:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 68
    monitor-exit p0

    return-void

    .line 66
    .end local p0    # "this":Lcom/android/server/inputmethod/InputMethodMonitor;
    .end local p1    # "highWatermark":I
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method findTheEvil()Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;
    .locals 8

    .line 170
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    .line 171
    .local v0, "infos":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;>;"
    monitor-enter p0

    .line 172
    :try_start_0
    iget-object v1, p0, Lcom/android/server/inputmethod/InputMethodMonitor;->mRecord:Ljava/util/WeakHashMap;

    invoke-virtual {v1}, Ljava/util/WeakHashMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 173
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/android/internal/inputmethod/IInputMethodSessionCallback$Stub;Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/internal/inputmethod/IInputMethodSessionCallback$Stub;

    .line 174
    .local v3, "k":Lcom/android/internal/inputmethod/IInputMethodSessionCallback$Stub;
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;

    .line 175
    .local v4, "v":Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;
    if-eqz v3, :cond_1

    if-eqz v4, :cond_1

    .line 176
    invoke-virtual {v0, v4}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 177
    const/4 v5, 0x0

    iput v5, v4, Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;->count_:I

    .line 179
    :cond_0
    iget v5, v4, Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;->count_:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v4, Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;->count_:I

    .line 181
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/android/internal/inputmethod/IInputMethodSessionCallback$Stub;Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;>;"
    .end local v3    # "k":Lcom/android/internal/inputmethod/IInputMethodSessionCallback$Stub;
    .end local v4    # "v":Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;
    :cond_1
    goto :goto_0

    .line 182
    :cond_2
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 183
    const/4 v1, 0x0

    .line 184
    .local v1, "evil":Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;
    const/16 v2, 0x64

    .line 185
    .local v2, "maxCount":I
    invoke-virtual {v0}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;

    .line 186
    .local v4, "app":Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;
    const-string v5, "InputMethodMonitor"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget v7, v4, Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;->count_:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " createSession requests sent to "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v4, Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;->component:Landroid/content/ComponentName;

    .line 187
    invoke-virtual {v7}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 186
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    iget v5, v4, Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;->count_:I

    if-le v5, v2, :cond_3

    .line 189
    iget v2, v4, Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;->count_:I

    .line 190
    move-object v1, v4

    .line 192
    .end local v4    # "app":Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;
    :cond_3
    goto :goto_1

    .line 193
    :cond_4
    return-object v1

    .line 182
    .end local v1    # "evil":Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;
    .end local v2    # "maxCount":I
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public declared-synchronized finishCreateSession(Lcom/android/internal/inputmethod/IInputMethodSessionCallback$Stub;)V
    .locals 1
    .param p1, "callback"    # Lcom/android/internal/inputmethod/IInputMethodSessionCallback$Stub;

    monitor-enter p0

    .line 87
    :try_start_0
    iget v0, p0, Lcom/android/server/inputmethod/InputMethodMonitor;->mHighWatermark:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    monitor-exit p0

    return-void

    .line 88
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/android/server/inputmethod/InputMethodMonitor;->expungeStaleItems()V

    .line 89
    iget-object v0, p0, Lcom/android/server/inputmethod/InputMethodMonitor;->mRecord:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 90
    monitor-exit p0

    return-void

    .line 86
    .end local p0    # "this":Lcom/android/server/inputmethod/InputMethodMonitor;
    .end local p1    # "callback":Lcom/android/internal/inputmethod/IInputMethodSessionCallback$Stub;
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method scheduleAppDeath()V
    .locals 8

    .line 117
    invoke-static {}, Landroid/app/ActivityThread;->isSystem()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    .line 118
    .local v0, "inTest":Z
    invoke-direct {p0, v0}, Lcom/android/server/inputmethod/InputMethodMonitor;->doAppDeath(Z)V

    .line 119
    const-wide/16 v1, 0x5

    if-eqz v0, :cond_0

    const-wide/16 v3, 0x1

    goto :goto_0

    :cond_0
    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v3

    :goto_0
    invoke-static {v3, v4}, Landroid/os/SystemClock;->sleep(J)V

    .line 121
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 122
    invoke-static {}, Ljava/lang/System;->runFinalization()V

    .line 123
    monitor-enter p0

    .line 124
    const/4 v3, 0x0

    if-eqz v0, :cond_1

    move v4, v3

    goto :goto_1

    :cond_1
    const/16 v4, 0xa

    .line 125
    .local v4, "waitCount":I
    :goto_1
    :try_start_0
    iget-object v5, p0, Lcom/android/server/inputmethod/InputMethodMonitor;->mRecord:Ljava/util/WeakHashMap;

    invoke-virtual {v5}, Ljava/util/WeakHashMap;->size()I

    move-result v5

    iget v6, p0, Lcom/android/server/inputmethod/InputMethodMonitor;->mHighWatermark:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lt v5, v6, :cond_3

    add-int/lit8 v5, v4, -0x1

    .end local v4    # "waitCount":I
    .local v5, "waitCount":I
    if-lez v4, :cond_2

    .line 127
    :try_start_1
    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    invoke-virtual {p0, v6, v7}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 130
    move v4, v5

    goto :goto_1

    .line 128
    :catch_0
    move-exception v1

    .line 129
    .local v1, "e":Ljava/lang/InterruptedException;
    :try_start_2
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    .end local v0    # "inTest":Z
    .end local p0    # "this":Lcom/android/server/inputmethod/InputMethodMonitor;
    throw v2

    .line 125
    .end local v1    # "e":Ljava/lang/InterruptedException;
    .restart local v0    # "inTest":Z
    .restart local p0    # "this":Lcom/android/server/inputmethod/InputMethodMonitor;
    :cond_2
    move v4, v5

    .line 133
    .end local v5    # "waitCount":I
    .restart local v4    # "waitCount":I
    :cond_3
    iput-boolean v3, p0, Lcom/android/server/inputmethod/InputMethodMonitor;->mWarning:Z

    .line 134
    .end local v4    # "waitCount":I
    monitor-exit p0

    .line 135
    return-void

    .line 134
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public declared-synchronized startCreateSession(ILandroid/content/ComponentName;Lcom/android/internal/inputmethod/IInputMethodSessionCallback$Stub;)V
    .locals 4
    .param p1, "userId"    # I
    .param p2, "componentName"    # Landroid/content/ComponentName;
    .param p3, "callback"    # Lcom/android/internal/inputmethod/IInputMethodSessionCallback$Stub;

    monitor-enter p0

    .line 75
    :try_start_0
    iget v0, p0, Lcom/android/server/inputmethod/InputMethodMonitor;->mHighWatermark:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    monitor-exit p0

    return-void

    .line 76
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/android/server/inputmethod/InputMethodMonitor;->expungeStaleItems()V

    .line 77
    invoke-direct {p0, p1, p2}, Lcom/android/server/inputmethod/InputMethodMonitor;->getInputMethodInfo(ILandroid/content/ComponentName;)Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;

    move-result-object v0

    .line 78
    .local v0, "info":Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;
    iget-object v1, p0, Lcom/android/server/inputmethod/InputMethodMonitor;->mRecord:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p3, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    iget-object v1, p0, Lcom/android/server/inputmethod/InputMethodMonitor;->mRecord:Ljava/util/WeakHashMap;

    invoke-virtual {v1}, Ljava/util/WeakHashMap;->size()I

    move-result v1

    iget v2, p0, Lcom/android/server/inputmethod/InputMethodMonitor;->mHighWatermark:I

    if-ne v1, v2, :cond_1

    iget-boolean v1, p0, Lcom/android/server/inputmethod/InputMethodMonitor;->mWarning:Z

    if-nez v1, :cond_1

    .line 80
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/server/inputmethod/InputMethodMonitor;->mWarning:Z

    .line 81
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/android/server/inputmethod/InputMethodMonitor$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0}, Lcom/android/server/inputmethod/InputMethodMonitor$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/inputmethod/InputMethodMonitor;)V

    const-string v3, "InputMethodMonitor"

    invoke-direct {v1, v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 83
    .end local p0    # "this":Lcom/android/server/inputmethod/InputMethodMonitor;
    :cond_1
    monitor-exit p0

    return-void

    .line 74
    .end local v0    # "info":Lcom/android/server/inputmethod/InputMethodMonitor$InputMethodInfo;
    .end local p1    # "userId":I
    .end local p2    # "componentName":Landroid/content/ComponentName;
    .end local p3    # "callback":Lcom/android/internal/inputmethod/IInputMethodSessionCallback$Stub;
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method
