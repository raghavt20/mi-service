class com.android.server.inputmethod.InputMethodManagerServiceImpl$2 implements android.view.View$OnClickListener {
	 /* .source "InputMethodManagerServiceImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->generateView(ZLjava/util/List;Lmiuix/appcompat/app/AlertDialog$Builder;Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;Landroid/content/Context;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.inputmethod.InputMethodManagerServiceImpl this$0; //synthetic
final Integer val$closeIndicatorDrawableId; //synthetic
final android.widget.ImageView val$indicator; //synthetic
final android.widget.ListView val$noCustomListView; //synthetic
final com.android.server.inputmethod.InputMethodMenuController$ImeSubtypeListAdapter val$noCustomziedImeListAdapter; //synthetic
final Integer val$openIndicatorDrawableId; //synthetic
/* # direct methods */
 com.android.server.inputmethod.InputMethodManagerServiceImpl$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/inputmethod/InputMethodManagerServiceImpl; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 414 */
this.this$0 = p1;
this.val$indicator = p2;
/* iput p3, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$2;->val$openIndicatorDrawableId:I */
this.val$noCustomListView = p4;
this.val$noCustomziedImeListAdapter = p5;
/* iput p6, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$2;->val$closeIndicatorDrawableId:I */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onClick ( android.view.View p0 ) {
/* .locals 3 */
/* .param p1, "v" # Landroid/view/View; */
/* .line 417 */
v0 = this.this$0;
v0 = com.android.server.inputmethod.InputMethodManagerServiceImpl .-$$Nest$fgetisExpanded ( v0 );
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 418 */
v0 = this.val$indicator;
/* iget v2, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$2;->val$openIndicatorDrawableId:I */
(( android.widget.ImageView ) v0 ).setImageResource ( v2 ); // invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V
/* .line 419 */
v0 = this.val$noCustomListView;
(( android.widget.ListView ) v0 ).setVisibility ( v1 ); // invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V
/* .line 420 */
v0 = this.val$noCustomListView;
v1 = this.val$noCustomziedImeListAdapter;
com.android.server.inputmethod.InputMethodManagerServiceImpl .-$$Nest$smsetNoCustomizedInputMethodListViewHeight ( v0,v1 );
/* .line 422 */
v0 = this.this$0;
int v1 = 1; // const/4 v1, 0x1
com.android.server.inputmethod.InputMethodManagerServiceImpl .-$$Nest$fputisExpanded ( v0,v1 );
/* .line 424 */
} // :cond_0
v0 = this.val$indicator;
/* iget v2, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$2;->val$closeIndicatorDrawableId:I */
(( android.widget.ImageView ) v0 ).setImageResource ( v2 ); // invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V
/* .line 425 */
v0 = this.val$noCustomListView;
/* const/16 v2, 0x8 */
(( android.widget.ListView ) v0 ).setVisibility ( v2 ); // invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V
/* .line 426 */
v0 = this.this$0;
com.android.server.inputmethod.InputMethodManagerServiceImpl .-$$Nest$fputisExpanded ( v0,v1 );
/* .line 428 */
} // :goto_0
return;
} // .end method
