public class com.android.server.inputmethod.MiuiInputMethodStubImpl implements com.android.server.inputmethod.MiuiInputMethodStub {
	 /* .source "MiuiInputMethodStubImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final java.lang.String SECURE_INPUT_METHOD_PACKAGE_NAME;
	 public static final java.lang.String TAG;
	 /* # instance fields */
	 private Integer mCallingUid;
	 private Boolean mInputMethodChangedBySelf;
	 private com.android.server.inputmethod.InputMethodManagerService mService;
	 private com.android.server.inputmethod.BaseInputMethodSwitcher securityInputMethodSwitcher;
	 private com.android.server.inputmethod.BaseInputMethodSwitcher sogouInputMethodSwitcher;
	 private com.android.server.inputmethod.BaseInputMethodSwitcher stylusInputMethodSwitcher;
	 /* # direct methods */
	 public com.android.server.inputmethod.MiuiInputMethodStubImpl ( ) {
		 /* .locals 0 */
		 /* .line 17 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public void bootPhase ( Integer p0, android.content.Context p1 ) {
		 /* .locals 1 */
		 /* .param p1, "phase" # I */
		 /* .param p2, "mContext" # Landroid/content/Context; */
		 /* .line 88 */
		 /* const/16 v0, 0x3e8 */
		 /* if-ne p1, v0, :cond_0 */
		 /* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
		 /* if-nez v0, :cond_0 */
		 /* .line 89 */
		 com.miui.server.InputMethodHelper .init ( p2 );
		 /* .line 91 */
	 } // :cond_0
	 return;
} // .end method
public void clearInputMethodChangedBySelf ( ) {
	 /* .locals 1 */
	 /* .line 100 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* iput-boolean v0, p0, Lcom/android/server/inputmethod/MiuiInputMethodStubImpl;->mInputMethodChangedBySelf:Z */
	 /* .line 101 */
	 return;
} // .end method
public java.util.List filterMethodLocked ( java.util.List p0 ) {
	 /* .locals 3 */
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "(", */
	 /* "Ljava/util/List<", */
	 /* "Landroid/view/inputmethod/InputMethodInfo;", */
	 /* ">;)", */
	 /* "Ljava/util/List<", */
	 /* "Landroid/view/inputmethod/InputMethodInfo;", */
	 /* ">;" */
	 /* } */
} // .end annotation
/* .line 81 */
/* .local p1, "methodInfos":Ljava/util/List;, "Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;" */
v0 = this.sogouInputMethodSwitcher;
v1 = this.securityInputMethodSwitcher;
v2 = this.stylusInputMethodSwitcher;
/* .line 83 */
(( com.android.server.inputmethod.BaseInputMethodSwitcher ) v2 ).filterMethodLocked ( p1 ); // invoke-virtual {v2, p1}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;->filterMethodLocked(Ljava/util/List;)Ljava/util/List;
/* .line 82 */
(( com.android.server.inputmethod.BaseInputMethodSwitcher ) v1 ).filterMethodLocked ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;->filterMethodLocked(Ljava/util/List;)Ljava/util/List;
/* .line 81 */
(( com.android.server.inputmethod.BaseInputMethodSwitcher ) v0 ).filterMethodLocked ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;->filterMethodLocked(Ljava/util/List;)Ljava/util/List;
} // .end method
public void init ( com.android.server.inputmethod.InputMethodManagerService p0 ) {
/* .locals 1 */
/* .param p1, "service" # Lcom/android/server/inputmethod/InputMethodManagerService; */
/* .line 29 */
/* new-instance v0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher; */
/* invoke-direct {v0}, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;-><init>()V */
this.stylusInputMethodSwitcher = v0;
/* .line 30 */
/* new-instance v0, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher; */
/* invoke-direct {v0}, Lcom/android/server/inputmethod/SecurityInputMethodSwitcher;-><init>()V */
this.securityInputMethodSwitcher = v0;
/* .line 31 */
/* new-instance v0, Lcom/android/server/inputmethod/SogouInputMethodSwitcher; */
/* invoke-direct {v0}, Lcom/android/server/inputmethod/SogouInputMethodSwitcher;-><init>()V */
this.sogouInputMethodSwitcher = v0;
/* .line 32 */
v0 = this.stylusInputMethodSwitcher;
(( com.android.server.inputmethod.BaseInputMethodSwitcher ) v0 ).init ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;->init(Lcom/android/server/inputmethod/InputMethodManagerService;)V
/* .line 33 */
v0 = this.securityInputMethodSwitcher;
(( com.android.server.inputmethod.BaseInputMethodSwitcher ) v0 ).init ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;->init(Lcom/android/server/inputmethod/InputMethodManagerService;)V
/* .line 34 */
v0 = this.sogouInputMethodSwitcher;
(( com.android.server.inputmethod.BaseInputMethodSwitcher ) v0 ).init ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;->init(Lcom/android/server/inputmethod/InputMethodManagerService;)V
/* .line 35 */
this.mService = p1;
/* .line 36 */
return;
} // .end method
public Boolean isInputMethodChangedBySelf ( ) {
/* .locals 1 */
/* .line 104 */
/* iget-boolean v0, p0, Lcom/android/server/inputmethod/MiuiInputMethodStubImpl;->mInputMethodChangedBySelf:Z */
} // .end method
public Boolean mayChangeInputMethodLocked ( android.view.inputmethod.EditorInfo p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "attribute" # Landroid/view/inputmethod/EditorInfo; */
/* .param p2, "startInputReason" # I */
/* .line 60 */
v0 = this.securityInputMethodSwitcher;
v0 = (( com.android.server.inputmethod.BaseInputMethodSwitcher ) v0 ).mayChangeInputMethodLocked ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;->mayChangeInputMethodLocked(Landroid/view/inputmethod/EditorInfo;I)Z
/* if-nez v0, :cond_1 */
v0 = this.sogouInputMethodSwitcher;
/* .line 61 */
v0 = (( com.android.server.inputmethod.BaseInputMethodSwitcher ) v0 ).mayChangeInputMethodLocked ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;->mayChangeInputMethodLocked(Landroid/view/inputmethod/EditorInfo;)Z
/* if-nez v0, :cond_1 */
v0 = this.stylusInputMethodSwitcher;
/* .line 62 */
v0 = (( com.android.server.inputmethod.BaseInputMethodSwitcher ) v0 ).mayChangeInputMethodLocked ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;->mayChangeInputMethodLocked(Landroid/view/inputmethod/EditorInfo;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
/* .line 60 */
} // :goto_1
} // .end method
public void onSwitchUserLocked ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "newUserId" # I */
/* .line 53 */
v0 = this.stylusInputMethodSwitcher;
(( com.android.server.inputmethod.BaseInputMethodSwitcher ) v0 ).onSwitchUserLocked ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;->onSwitchUserLocked(I)V
/* .line 54 */
v0 = this.securityInputMethodSwitcher;
(( com.android.server.inputmethod.BaseInputMethodSwitcher ) v0 ).onSwitchUserLocked ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;->onSwitchUserLocked(I)V
/* .line 55 */
v0 = this.sogouInputMethodSwitcher;
(( com.android.server.inputmethod.BaseInputMethodSwitcher ) v0 ).onSwitchUserLocked ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;->onSwitchUserLocked(I)V
/* .line 56 */
return;
} // .end method
public void onSystemRunningLocked ( ) {
/* .locals 3 */
/* .line 40 */
v0 = this.stylusInputMethodSwitcher;
(( com.android.server.inputmethod.BaseInputMethodSwitcher ) v0 ).onSystemRunningLocked ( ); // invoke-virtual {v0}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;->onSystemRunningLocked()V
/* .line 41 */
v0 = this.securityInputMethodSwitcher;
(( com.android.server.inputmethod.BaseInputMethodSwitcher ) v0 ).onSystemRunningLocked ( ); // invoke-virtual {v0}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;->onSystemRunningLocked()V
/* .line 42 */
v0 = this.sogouInputMethodSwitcher;
(( com.android.server.inputmethod.BaseInputMethodSwitcher ) v0 ).onSystemRunningLocked ( ); // invoke-virtual {v0}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;->onSystemRunningLocked()V
/* .line 44 */
try { // :try_start_0
v0 = this.mService;
v0 = this.mContext;
(( android.content.Context ) v0 ).getPackageManager ( ); // invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
final String v1 = "com.miui.securityinputmethod"; // const-string v1, "com.miui.securityinputmethod"
/* .line 45 */
int v2 = 0; // const/4 v2, 0x0
v0 = (( android.content.pm.PackageManager ) v0 ).getPackageUid ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageUid(Ljava/lang/String;I)I
/* iput v0, p0, Lcom/android/server/inputmethod/MiuiInputMethodStubImpl;->mCallingUid:I */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 48 */
/* .line 46 */
/* :catch_0 */
/* move-exception v0 */
/* .line 47 */
/* .local v0, "e":Ljava/lang/Exception; */
int v1 = -1; // const/4 v1, -0x1
/* iput v1, p0, Lcom/android/server/inputmethod/MiuiInputMethodStubImpl;->mCallingUid:I */
/* .line 49 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public void removeMethod ( java.util.List p0 ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 74 */
/* .local p1, "imList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;>;" */
v0 = this.stylusInputMethodSwitcher;
(( com.android.server.inputmethod.BaseInputMethodSwitcher ) v0 ).removeMethod ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;->removeMethod(Ljava/util/List;)V
/* .line 75 */
v0 = this.securityInputMethodSwitcher;
(( com.android.server.inputmethod.BaseInputMethodSwitcher ) v0 ).removeMethod ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;->removeMethod(Ljava/util/List;)V
/* .line 76 */
v0 = this.sogouInputMethodSwitcher;
(( com.android.server.inputmethod.BaseInputMethodSwitcher ) v0 ).removeMethod ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;->removeMethod(Ljava/util/List;)V
/* .line 77 */
return;
} // .end method
public void setInputMethodChangedBySelf ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "callingUid" # I */
/* .line 93 */
int v0 = -1; // const/4 v0, -0x1
/* if-ne p1, v0, :cond_0 */
return;
/* .line 94 */
} // :cond_0
/* iget v0, p0, Lcom/android/server/inputmethod/MiuiInputMethodStubImpl;->mCallingUid:I */
/* if-ne p1, v0, :cond_1 */
/* .line 95 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/inputmethod/MiuiInputMethodStubImpl;->mInputMethodChangedBySelf:Z */
/* .line 97 */
} // :cond_1
return;
} // .end method
public Boolean shouldHideImeSwitcherLocked ( ) {
/* .locals 1 */
/* .line 67 */
v0 = this.stylusInputMethodSwitcher;
v0 = (( com.android.server.inputmethod.BaseInputMethodSwitcher ) v0 ).shouldHideImeSwitcherLocked ( ); // invoke-virtual {v0}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;->shouldHideImeSwitcherLocked()Z
/* if-nez v0, :cond_1 */
v0 = this.sogouInputMethodSwitcher;
/* .line 68 */
v0 = (( com.android.server.inputmethod.BaseInputMethodSwitcher ) v0 ).shouldHideImeSwitcherLocked ( ); // invoke-virtual {v0}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;->shouldHideImeSwitcherLocked()Z
/* if-nez v0, :cond_1 */
v0 = this.securityInputMethodSwitcher;
/* .line 69 */
v0 = (( com.android.server.inputmethod.BaseInputMethodSwitcher ) v0 ).shouldHideImeSwitcherLocked ( ); // invoke-virtual {v0}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;->shouldHideImeSwitcherLocked()Z
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
/* .line 67 */
} // :goto_1
} // .end method
