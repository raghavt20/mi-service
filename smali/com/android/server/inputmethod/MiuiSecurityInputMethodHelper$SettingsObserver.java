class com.android.server.inputmethod.MiuiSecurityInputMethodHelper$SettingsObserver extends android.database.ContentObserver {
	 /* .source "MiuiSecurityInputMethodHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "SettingsObserver" */
} // .end annotation
/* # instance fields */
Boolean mRegistered;
Integer mUserId;
final com.android.server.inputmethod.MiuiSecurityInputMethodHelper this$0; //synthetic
/* # direct methods */
 com.android.server.inputmethod.MiuiSecurityInputMethodHelper$SettingsObserver ( ) {
/* .locals 1 */
/* .param p1, "this$0" # Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 78 */
this.this$0 = p1;
/* .line 79 */
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
/* .line 76 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper$SettingsObserver;->mRegistered:Z */
/* .line 80 */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 7 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 105 */
final String v0 = "enable_miui_security_ime"; // const-string v0, "enable_miui_security_ime"
android.provider.Settings$Secure .getUriFor ( v0 );
/* .line 106 */
/* .local v0, "secIMEUri":Landroid/net/Uri; */
v1 = this.this$0;
com.android.server.inputmethod.MiuiSecurityInputMethodHelper .-$$Nest$fgetmService ( v1 );
v1 = this.mMethodMap;
/* monitor-enter v1 */
/* .line 107 */
try { // :try_start_0
	 v2 = 	 (( android.net.Uri ) v0 ).equals ( p2 ); // invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
	 if ( v2 != null) { // if-eqz v2, :cond_1
		 /* .line 108 */
		 v2 = this.this$0;
		 com.android.server.inputmethod.MiuiSecurityInputMethodHelper .-$$Nest$fgetmService ( v2 );
		 v3 = this.mContext;
		 /* .line 109 */
		 (( android.content.Context ) v3 ).getContentResolver ( ); // invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
		 final String v4 = "enable_miui_security_ime"; // const-string v4, "enable_miui_security_ime"
		 v5 = this.this$0;
		 com.android.server.inputmethod.MiuiSecurityInputMethodHelper .-$$Nest$fgetmService ( v5 );
		 v5 = this.mSettings;
		 /* .line 110 */
		 v5 = 		 (( com.android.server.inputmethod.InputMethodUtils$InputMethodSettings ) v5 ).getCurrentUserId ( ); // invoke-virtual {v5}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getCurrentUserId()I
		 /* .line 108 */
		 int v6 = 1; // const/4 v6, 0x1
		 v3 = 		 android.provider.Settings$Secure .getIntForUser ( v3,v4,v6,v5 );
		 if ( v3 != null) { // if-eqz v3, :cond_0
		 } // :cond_0
		 int v6 = 0; // const/4 v6, 0x0
	 } // :goto_0
	 com.android.server.inputmethod.MiuiSecurityInputMethodHelper .-$$Nest$fputmSecEnabled ( v2,v6 );
	 /* .line 111 */
	 v2 = this.this$0;
	 com.android.server.inputmethod.MiuiSecurityInputMethodHelper .-$$Nest$mupdateFromSettingsLocked ( v2 );
	 /* .line 113 */
	 final String v2 = "MiuiSecurityInputMethodHelper"; // const-string v2, "MiuiSecurityInputMethodHelper"
	 /* new-instance v3, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v4 = "enable status change: "; // const-string v4, "enable status change: "
	 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v4 = this.this$0;
	 v4 = 	 com.android.server.inputmethod.MiuiSecurityInputMethodHelper .-$$Nest$fgetmSecEnabled ( v4 );
	 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .d ( v2,v3 );
	 /* .line 116 */
} // :cond_1
/* monitor-exit v1 */
/* .line 117 */
return;
/* .line 116 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
void registerContentObserverLocked ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "userId" # I */
/* .line 83 */
/* sget-boolean v0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->SUPPORT_SEC_INPUT_METHOD:Z */
/* if-nez v0, :cond_0 */
/* .line 84 */
return;
/* .line 86 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper$SettingsObserver;->mRegistered:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget v0, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper$SettingsObserver;->mUserId:I */
/* if-ne v0, p1, :cond_1 */
/* .line 87 */
return;
/* .line 89 */
} // :cond_1
v0 = this.this$0;
com.android.server.inputmethod.MiuiSecurityInputMethodHelper .-$$Nest$fgetmService ( v0 );
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 90 */
/* .local v0, "resolver":Landroid/content/ContentResolver; */
/* iget-boolean v1, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper$SettingsObserver;->mRegistered:Z */
int v2 = 0; // const/4 v2, 0x0
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 91 */
(( android.content.ContentResolver ) v0 ).unregisterContentObserver ( p0 ); // invoke-virtual {v0, p0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V
/* .line 92 */
/* iput-boolean v2, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper$SettingsObserver;->mRegistered:Z */
/* .line 94 */
} // :cond_2
/* iget v1, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper$SettingsObserver;->mUserId:I */
/* if-eq v1, p1, :cond_3 */
/* .line 95 */
/* iput p1, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper$SettingsObserver;->mUserId:I */
/* .line 97 */
} // :cond_3
final String v1 = "enable_miui_security_ime"; // const-string v1, "enable_miui_security_ime"
android.provider.Settings$Secure .getUriFor ( v1 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, p1 ); // invoke-virtual {v0, v1, v2, p0, p1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 100 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper$SettingsObserver;->mRegistered:Z */
/* .line 101 */
return;
} // .end method
