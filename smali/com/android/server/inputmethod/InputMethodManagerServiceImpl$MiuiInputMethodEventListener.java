class com.android.server.inputmethod.InputMethodManagerServiceImpl$MiuiInputMethodEventListener implements com.miui.server.input.gesture.MiuiGestureListener {
	 /* .source "InputMethodManagerServiceImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/inputmethod/InputMethodManagerServiceImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "MiuiInputMethodEventListener" */
} // .end annotation
/* # instance fields */
final com.android.server.inputmethod.InputMethodManagerServiceImpl this$0; //synthetic
/* # direct methods */
public static void $r8$lambda$2saszdD9J8z0eCwcb6tjEj8ZCCs ( com.android.server.inputmethod.InputMethodManagerServiceImpl$MiuiInputMethodEventListener p0, android.view.MotionEvent p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$MiuiInputMethodEventListener;->lambda$onPointerEvent$0(Landroid/view/MotionEvent;)V */
return;
} // .end method
private com.android.server.inputmethod.InputMethodManagerServiceImpl$MiuiInputMethodEventListener ( ) {
/* .locals 0 */
/* .line 759 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
 com.android.server.inputmethod.InputMethodManagerServiceImpl$MiuiInputMethodEventListener ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$MiuiInputMethodEventListener;-><init>(Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;)V */
return;
} // .end method
private void lambda$onPointerEvent$0 ( android.view.MotionEvent p0 ) { //synthethic
/* .locals 2 */
/* .param p1, "event" # Landroid/view/MotionEvent; */
/* .line 771 */
v0 = (( android.view.MotionEvent ) p1 ).getDeviceId ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getDeviceId()I
/* if-ltz v0, :cond_1 */
/* .line 772 */
v0 = this.this$0;
v0 = com.android.server.inputmethod.InputMethodManagerServiceImpl .-$$Nest$fgetmIsFromRemoteDevice ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 773 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 /* const-string/jumbo v1, "set mIsFromRemoteDevice as:" */
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v1 = this.this$0;
	 v1 = 	 com.android.server.inputmethod.InputMethodManagerServiceImpl .-$$Nest$fgetmIsFromRemoteDevice ( v1 );
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
	 final String v1 = " via Input event"; // const-string v1, " via Input event"
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v1 = "InputMethodManagerServiceImpl"; // const-string v1, "InputMethodManagerServiceImpl"
	 android.util.Slog .i ( v1,v0 );
	 /* .line 776 */
} // :cond_0
v0 = this.this$0;
int v1 = 0; // const/4 v1, 0x0
com.android.server.inputmethod.InputMethodManagerServiceImpl .-$$Nest$fputmIsFromRemoteDevice ( v0,v1 );
/* .line 777 */
v0 = this.this$0;
com.android.server.inputmethod.InputMethodManagerServiceImpl .-$$Nest$fputmMirrorDisplayId ( v0,v1 );
/* .line 779 */
} // :cond_1
(( android.view.MotionEvent ) p1 ).recycle ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->recycle()V
/* .line 780 */
return;
} // .end method
/* # virtual methods */
public void onPointerEvent ( android.view.MotionEvent p0 ) {
/* .locals 3 */
/* .param p1, "motionEvent" # Landroid/view/MotionEvent; */
/* .line 766 */
v0 = this.this$0;
v0 = this.mHandler;
/* if-nez v0, :cond_0 */
return;
/* .line 767 */
} // :cond_0
(( android.view.MotionEvent ) p1 ).copy ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->copy()Landroid/view/MotionEvent;
/* .line 768 */
/* .local v0, "event":Landroid/view/MotionEvent; */
v1 = this.this$0;
v1 = this.mHandler;
/* new-instance v2, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$MiuiInputMethodEventListener$$ExternalSyntheticLambda0; */
/* invoke-direct {v2, p0, v0}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$MiuiInputMethodEventListener$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$MiuiInputMethodEventListener;Landroid/view/MotionEvent;)V */
(( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 781 */
return;
} // .end method
