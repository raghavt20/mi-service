public class com.android.server.inputmethod.InputMethodManagerServiceImpl extends com.android.server.inputmethod.InputMethodManagerServiceStub {
	 /* .source "InputMethodManagerServiceImpl.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.inputmethod.InputMethodManagerServiceStub$$" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$MiuiInputMethodEventListener; */
/* } */
} // .end annotation
/* # static fields */
public static final Boolean DEBUG;
private static final Integer IME_LIST_VIEW_VISIBLE_NUMBER;
private static final java.lang.String MIRROR_INPUT_STATE;
public static final java.lang.String MIUIXPACKAGE;
public static final java.lang.String MIUI_HOME;
public static final java.lang.String TAG;
private static final java.util.List customizedInputMethodList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private Boolean callStartInputOrWindowGainedFocusFlag;
android.content.Context dialogContext;
private Boolean isExpanded;
com.android.server.inputmethod.InputMethodBindingController mBindingController;
android.content.Context mContext;
android.os.Handler mHandler;
private Boolean mIsFromRemoteDevice;
private volatile Integer mLastAcceptStatus;
private Integer mMirrorDisplayId;
private Boolean mMirrorInputState;
private com.android.server.inputmethod.InputMethodManagerServiceImpl$MiuiInputMethodEventListener mMiuiInputMethodEventListener;
private android.os.IBinder mMonitorBinder;
public android.view.inputmethod.InputMethodInfo mNoCustomizedIms;
public mNoCustomizedSubtypeIds;
private android.database.ContentObserver mObserver;
private Boolean mScreenOffLastTime;
private Boolean mScreenStatus;
private Integer mSessionId;
private volatile Integer mSynergyOperate;
public Integer noCustomizedCheckedItem;
/* # direct methods */
static Boolean -$$Nest$fgetisExpanded ( com.android.server.inputmethod.InputMethodManagerServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->isExpanded:Z */
} // .end method
static Boolean -$$Nest$fgetmIsFromRemoteDevice ( com.android.server.inputmethod.InputMethodManagerServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mIsFromRemoteDevice:Z */
} // .end method
static void -$$Nest$fputisExpanded ( com.android.server.inputmethod.InputMethodManagerServiceImpl p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->isExpanded:Z */
return;
} // .end method
static void -$$Nest$fputmIsFromRemoteDevice ( com.android.server.inputmethod.InputMethodManagerServiceImpl p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mIsFromRemoteDevice:Z */
return;
} // .end method
static void -$$Nest$fputmMirrorDisplayId ( com.android.server.inputmethod.InputMethodManagerServiceImpl p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mMirrorDisplayId:I */
return;
} // .end method
static void -$$Nest$fputmMirrorInputState ( com.android.server.inputmethod.InputMethodManagerServiceImpl p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mMirrorInputState:Z */
return;
} // .end method
static void -$$Nest$smsetNoCustomizedInputMethodListViewHeight ( android.widget.ListView p0, com.android.server.inputmethod.InputMethodMenuController$ImeSubtypeListAdapter p1 ) { //bridge//synthethic
/* .locals 0 */
com.android.server.inputmethod.InputMethodManagerServiceImpl .setNoCustomizedInputMethodListViewHeight ( p0,p1 );
return;
} // .end method
static com.android.server.inputmethod.InputMethodManagerServiceImpl ( ) {
/* .locals 2 */
/* .line 78 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 113 */
final String v1 = "com.iflytek.inputmethod.miui"; // const-string v1, "com.iflytek.inputmethod.miui"
/* .line 114 */
final String v1 = "com.baidu.input_mi"; // const-string v1, "com.baidu.input_mi"
/* .line 115 */
final String v1 = "com.sohu.inputmethod.sogou.xiaomi"; // const-string v1, "com.sohu.inputmethod.sogou.xiaomi"
/* .line 117 */
final String v1 = "com.android.cts.mockime"; // const-string v1, "com.android.cts.mockime"
/* .line 118 */
return;
} // .end method
public com.android.server.inputmethod.InputMethodManagerServiceImpl ( ) {
/* .locals 3 */
/* .line 68 */
/* invoke-direct {p0}, Lcom/android/server/inputmethod/InputMethodManagerServiceStub;-><init>()V */
/* .line 74 */
int v0 = 0; // const/4 v0, 0x0
this.mMonitorBinder = v0;
/* .line 75 */
int v1 = 0; // const/4 v1, 0x0
/* iput v1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mSynergyOperate:I */
/* .line 76 */
/* iput v1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mLastAcceptStatus:I */
/* .line 80 */
int v2 = -1; // const/4 v2, -0x1
/* iput v2, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->noCustomizedCheckedItem:I */
/* .line 81 */
this.mNoCustomizedIms = v0;
/* .line 82 */
this.mNoCustomizedSubtypeIds = v0;
/* .line 87 */
/* iput-boolean v1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mScreenStatus:Z */
/* .line 91 */
/* iput-boolean v1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->isExpanded:Z */
/* .line 92 */
/* iput-boolean v1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mScreenOffLastTime:Z */
/* .line 93 */
/* iput-boolean v1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->callStartInputOrWindowGainedFocusFlag:Z */
/* .line 96 */
/* iput v1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mSessionId:I */
/* .line 102 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mMirrorInputState:Z */
/* .line 107 */
/* iput v1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mMirrorDisplayId:I */
/* .line 108 */
/* iput-boolean v1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mIsFromRemoteDevice:Z */
return;
} // .end method
private static void clearFitSystemWindow ( android.view.View p0 ) {
/* .locals 2 */
/* .param p0, "view" # Landroid/view/View; */
/* .line 487 */
if ( p0 != null) { // if-eqz p0, :cond_0
/* .line 488 */
int v0 = 0; // const/4 v0, 0x0
(( android.view.View ) p0 ).setFitsSystemWindows ( v0 ); // invoke-virtual {p0, v0}, Landroid/view/View;->setFitsSystemWindows(Z)V
/* .line 489 */
/* instance-of v0, p0, Landroid/view/ViewGroup; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 490 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
/* move-object v1, p0 */
/* check-cast v1, Landroid/view/ViewGroup; */
v1 = (( android.view.ViewGroup ) v1 ).getChildCount ( ); // invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I
/* if-ge v0, v1, :cond_0 */
/* .line 491 */
/* move-object v1, p0 */
/* check-cast v1, Landroid/view/ViewGroup; */
(( android.view.ViewGroup ) v1 ).getChildAt ( v0 ); // invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;
com.android.server.inputmethod.InputMethodManagerServiceImpl .clearFitSystemWindow ( v1 );
/* .line 490 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 495 */
} // .end local v0 # "i":I
} // :cond_0
return;
} // .end method
public static com.android.server.inputmethod.InputMethodManagerServiceImpl getInstance ( ) {
/* .locals 1 */
/* .line 121 */
com.android.server.inputmethod.InputMethodManagerServiceStub .getInstance ( );
/* check-cast v0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl; */
} // .end method
private void registerPointerEventListener ( ) {
/* .locals 2 */
/* .line 742 */
v0 = this.mContext;
/* if-nez v0, :cond_0 */
return;
/* .line 743 */
} // :cond_0
v0 = this.mMiuiInputMethodEventListener;
/* if-nez v0, :cond_1 */
/* .line 744 */
/* new-instance v0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$MiuiInputMethodEventListener; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, p0, v1}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$MiuiInputMethodEventListener;-><init>(Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$MiuiInputMethodEventListener-IA;)V */
this.mMiuiInputMethodEventListener = v0;
/* .line 746 */
} // :cond_1
v0 = this.mContext;
com.miui.server.input.gesture.MiuiGestureMonitor .getInstance ( v0 );
v1 = this.mMiuiInputMethodEventListener;
(( com.miui.server.input.gesture.MiuiGestureMonitor ) v0 ).registerPointerEventListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->registerPointerEventListener(Lcom/miui/server/input/gesture/MiuiGestureListener;)V
/* .line 748 */
return;
} // .end method
private static void setNoCustomizedInputMethodListViewHeight ( android.widget.ListView p0, com.android.server.inputmethod.InputMethodMenuController$ImeSubtypeListAdapter p1 ) {
/* .locals 5 */
/* .param p0, "noCustomListView" # Landroid/widget/ListView; */
/* .param p1, "adapter" # Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter; */
/* .line 502 */
v0 = (( com.android.server.inputmethod.InputMethodMenuController$ImeSubtypeListAdapter ) p1 ).getCount ( ); // invoke-virtual {p1}, Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;->getCount()I
/* .line 503 */
/* .local v0, "childrenCount":I */
int v1 = 0; // const/4 v1, 0x0
int v2 = 0; // const/4 v2, 0x0
(( com.android.server.inputmethod.InputMethodMenuController$ImeSubtypeListAdapter ) p1 ).getView ( v2, v1, p0 ); // invoke-virtual {p1, v2, v1, p0}, Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
/* .line 504 */
/* .local v1, "childView":Landroid/view/View; */
(( android.view.View ) v1 ).measure ( v2, v2 ); // invoke-virtual {v1, v2, v2}, Landroid/view/View;->measure(II)V
/* .line 505 */
v2 = (( android.view.View ) v1 ).getMeasuredHeight ( ); // invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I
/* .line 507 */
/* .local v2, "childHeight":I */
int v3 = 0; // const/4 v3, 0x0
/* .line 508 */
/* .local v3, "height":I */
int v4 = 3; // const/4 v4, 0x3
/* if-ge v0, v4, :cond_0 */
/* .line 509 */
/* mul-int v3, v2, v0 */
/* .line 511 */
} // :cond_0
/* mul-int/lit8 v3, v2, 0x3 */
/* .line 513 */
} // :goto_0
(( android.widget.ListView ) p0 ).getLayoutParams ( ); // invoke-virtual {p0}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
/* .line 514 */
/* .local v4, "layoutParams":Landroid/view/ViewGroup$LayoutParams; */
/* iput v3, v4, Landroid/view/ViewGroup$LayoutParams;->height:I */
/* .line 515 */
(( android.widget.ListView ) p0 ).setLayoutParams ( v4 ); // invoke-virtual {p0, v4}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
/* .line 516 */
return;
} // .end method
private void unregisterPointerEventListener ( ) {
/* .locals 2 */
/* .line 751 */
v0 = this.mContext;
/* if-nez v0, :cond_0 */
return;
/* .line 752 */
} // :cond_0
v1 = this.mMiuiInputMethodEventListener;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 753 */
com.miui.server.input.gesture.MiuiGestureMonitor .getInstance ( v0 );
v1 = this.mMiuiInputMethodEventListener;
(( com.miui.server.input.gesture.MiuiGestureMonitor ) v0 ).unregisterPointerEventListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->unregisterPointerEventListener(Lcom/miui/server/input/gesture/MiuiGestureListener;)V
/* .line 755 */
int v0 = 0; // const/4 v0, 0x0
this.mMiuiInputMethodEventListener = v0;
/* .line 757 */
} // :cond_1
return;
} // .end method
/* # virtual methods */
public android.app.Notification buildImeNotification ( android.app.Notification$Builder p0, android.content.Context p1 ) {
/* .locals 4 */
/* .param p1, "imeSwitcherNotification" # Landroid/app/Notification$Builder; */
/* .param p2, "context" # Landroid/content/Context; */
/* .line 586 */
(( android.app.Notification$Builder ) p1 ).build ( ); // invoke-virtual {p1}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;
/* .line 587 */
/* .local v0, "notification":Landroid/app/Notification; */
v1 = this.extras;
/* .line 588 */
/* const v2, 0x1108021a */
android.graphics.drawable.Icon .createWithResource ( p2,v2 );
/* .line 587 */
final String v3 = "miui.appIcon"; // const-string v3, "miui.appIcon"
(( android.os.Bundle ) v1 ).putParcelable ( v3, v2 ); // invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
/* .line 589 */
} // .end method
public void commitTextForSynergy ( com.android.internal.inputmethod.IRemoteInputConnection p0, java.lang.String p1, Integer p2 ) {
/* .locals 7 */
/* .param p1, "inputContext" # Lcom/android/internal/inputmethod/IRemoteInputConnection; */
/* .param p2, "text" # Ljava/lang/String; */
/* .param p3, "newCursorPosition" # I */
/* .line 675 */
try { // :try_start_0
(( java.lang.Object ) p1 ).getClass ( ); // invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
/* .line 676 */
/* .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
final String v1 = "commitTextForSynergy"; // const-string v1, "commitTextForSynergy"
int v2 = 2; // const/4 v2, 0x2
/* new-array v3, v2, [Ljava/lang/Class; */
/* const-class v4, Ljava/lang/String; */
int v5 = 0; // const/4 v5, 0x0
/* aput-object v4, v3, v5 */
v4 = java.lang.Integer.TYPE;
int v6 = 1; // const/4 v6, 0x1
/* aput-object v4, v3, v6 */
(( java.lang.Class ) v0 ).getDeclaredMethod ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 678 */
/* .local v1, "declaredMethod":Ljava/lang/reflect/Method; */
/* new-array v2, v2, [Ljava/lang/Object; */
/* aput-object p2, v2, v5 */
java.lang.Integer .valueOf ( p3 );
/* aput-object v3, v2, v6 */
(( java.lang.reflect.Method ) v1 ).invoke ( p1, v2 ); // invoke-virtual {v1, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 681 */
/* nop */
} // .end local v0 # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
} // .end local v1 # "declaredMethod":Ljava/lang/reflect/Method;
/* .line 679 */
/* :catch_0 */
/* move-exception v0 */
/* .line 680 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "InputMethodManagerServiceImpl"; // const-string v1, "InputMethodManagerServiceImpl"
final String v2 = "Reflect commitTextForSynergy exception!"; // const-string v2, "Reflect commitTextForSynergy exception!"
android.util.Slog .e ( v1,v2 );
/* .line 682 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public void createSession ( Integer p0, com.android.internal.inputmethod.IInputMethodSessionCallback$Stub p1 ) {
/* .locals 3 */
/* .param p1, "userId" # I */
/* .param p2, "callback" # Lcom/android/internal/inputmethod/IInputMethodSessionCallback$Stub; */
/* .line 725 */
/* sget-boolean v0, Lmiui/os/Build;->IS_DEBUGGABLE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 726 */
v0 = this.mBindingController;
(( com.android.server.inputmethod.InputMethodBindingController ) v0 ).getCurIntent ( ); // invoke-virtual {v0}, Lcom/android/server/inputmethod/InputMethodBindingController;->getCurIntent()Landroid/content/Intent;
/* .line 728 */
/* .local v0, "intent":Landroid/content/Intent; */
if ( v0 != null) { // if-eqz v0, :cond_0
(( android.content.Intent ) v0 ).getComponent ( ); // invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
/* move-object v2, v1 */
/* .local v2, "component":Landroid/content/ComponentName; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 729 */
com.android.server.inputmethod.InputMethodMonitor .getInstance ( );
(( com.android.server.inputmethod.InputMethodMonitor ) v1 ).startCreateSession ( p1, v2, p2 ); // invoke-virtual {v1, p1, v2, p2}, Lcom/android/server/inputmethod/InputMethodMonitor;->startCreateSession(ILandroid/content/ComponentName;Lcom/android/internal/inputmethod/IInputMethodSessionCallback$Stub;)V
/* .line 732 */
} // .end local v0 # "intent":Landroid/content/Intent;
} // .end local v2 # "component":Landroid/content/ComponentName;
} // :cond_0
return;
} // .end method
public void dismissWithoutAnimation ( miuix.appcompat.app.AlertDialog p0 ) {
/* .locals 4 */
/* .param p1, "alertDialog" # Lmiuix/appcompat/app/AlertDialog; */
/* .line 577 */
try { // :try_start_0
(( java.lang.Object ) p1 ).getClass ( ); // invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
final String v1 = "dismissWithoutAnimation"; // const-string v1, "dismissWithoutAnimation"
int v2 = 0; // const/4 v2, 0x0
/* new-array v3, v2, [Ljava/lang/Class; */
(( java.lang.Class ) v0 ).getDeclaredMethod ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 578 */
/* .local v0, "method":Ljava/lang/reflect/Method; */
/* new-array v1, v2, [Ljava/lang/Object; */
(( java.lang.reflect.Method ) v0 ).invoke ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 581 */
/* nop */
} // .end local v0 # "method":Ljava/lang/reflect/Method;
/* .line 579 */
/* :catch_0 */
/* move-exception v0 */
/* .line 580 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "InputMethodManagerServiceImpl"; // const-string v1, "InputMethodManagerServiceImpl"
final String v2 = "Reflect dismissWithoutAnimation exception!"; // const-string v2, "Reflect dismissWithoutAnimation exception!"
android.util.Slog .e ( v1,v2 );
/* .line 582 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public void enableInputMethodMonitor ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "highWatermark" # I */
/* .line 717 */
/* sget-boolean v0, Lmiui/os/Build;->IS_DEBUGGABLE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 718 */
com.android.server.inputmethod.InputMethodMonitor .getInstance ( );
(( com.android.server.inputmethod.InputMethodMonitor ) v0 ).enable ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/inputmethod/InputMethodMonitor;->enable(I)V
/* .line 720 */
} // :cond_0
return;
} // .end method
public void enableSystemIMEsIfThereIsNoEnabledIME ( java.util.List p0, com.android.server.inputmethod.InputMethodUtils$InputMethodSettings p1 ) {
/* .locals 8 */
/* .param p2, "settings" # Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Landroid/view/inputmethod/InputMethodInfo;", */
/* ">;", */
/* "Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 147 */
/* .local p1, "methodList":Ljava/util/List;, "Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;" */
/* sget-boolean v0, Lmiui/os/Build;->IS_CM_CUSTOMIZATION_TEST:Z */
/* if-nez v0, :cond_6 */
if ( p1 != null) { // if-eqz p1, :cond_6
/* if-nez p2, :cond_0 */
/* .line 151 */
} // :cond_0
/* nop */
/* .line 152 */
(( com.android.server.inputmethod.InputMethodUtils$InputMethodSettings ) p2 ).getEnabledInputMethodsAndSubtypeListLocked ( ); // invoke-virtual {p2}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getEnabledInputMethodsAndSubtypeListLocked()Ljava/util/List;
/* .line 154 */
/* .local v0, "enabledInputMethodsList":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/util/ArrayList<Ljava/lang/String;>;>;>;" */
int v1 = 0; // const/4 v1, 0x0
/* .line 156 */
/* .local v1, "systemInputMethod":Landroid/view/inputmethod/InputMethodInfo; */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
v3 = } // :goto_0
/* if-ge v2, v3, :cond_4 */
/* .line 157 */
/* check-cast v3, Landroid/view/inputmethod/InputMethodInfo; */
/* .line 158 */
/* .local v3, "inputMethodInfo":Landroid/view/inputmethod/InputMethodInfo; */
(( android.view.inputmethod.InputMethodInfo ) v3 ).getServiceInfo ( ); // invoke-virtual {v3}, Landroid/view/inputmethod/InputMethodInfo;->getServiceInfo()Landroid/content/pm/ServiceInfo;
v4 = this.applicationInfo;
/* iget v4, v4, Landroid/content/pm/ApplicationInfo;->flags:I */
/* and-int/lit8 v4, v4, 0x1 */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 160 */
/* move-object v1, v3 */
/* .line 163 */
} // :cond_1
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 164 */
v5 = } // :goto_1
if ( v5 != null) { // if-eqz v5, :cond_3
/* check-cast v5, Landroid/util/Pair; */
/* .line 165 */
/* .local v5, "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/util/ArrayList<Ljava/lang/String;>;>;" */
v6 = this.first;
/* check-cast v6, Ljava/lang/CharSequence; */
(( android.view.inputmethod.InputMethodInfo ) v3 ).getId ( ); // invoke-virtual {v3}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;
v6 = android.text.TextUtils .equals ( v6,v7 );
if ( v6 != null) { // if-eqz v6, :cond_2
/* .line 166 */
return;
/* .line 168 */
} // .end local v5 # "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/util/ArrayList<Ljava/lang/String;>;>;"
} // :cond_2
/* .line 156 */
} // .end local v3 # "inputMethodInfo":Landroid/view/inputmethod/InputMethodInfo;
} // :cond_3
/* add-int/lit8 v2, v2, 0x1 */
/* .line 172 */
} // .end local v2 # "i":I
} // :cond_4
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 173 */
(( android.view.inputmethod.InputMethodInfo ) v1 ).getId ( ); // invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;
int v3 = 0; // const/4 v3, 0x0
(( com.android.server.inputmethod.InputMethodUtils$InputMethodSettings ) p2 ).appendAndPutEnabledInputMethodLocked ( v2, v3 ); // invoke-virtual {p2, v2, v3}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->appendAndPutEnabledInputMethodLocked(Ljava/lang/String;Z)V
/* .line 175 */
} // :cond_5
return;
/* .line 148 */
} // .end local v0 # "enabledInputMethodsList":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/util/ArrayList<Ljava/lang/String;>;>;>;"
} // .end local v1 # "systemInputMethod":Landroid/view/inputmethod/InputMethodInfo;
} // :cond_6
} // :goto_2
return;
} // .end method
public java.util.List filterNotCustomziedInputMethodList ( java.util.List p0 ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;", */
/* ">;)", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 336 */
/* .local p1, "imList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;>;" */
final String v0 = "InputMethodManagerServiceImpl"; // const-string v0, "InputMethodManagerServiceImpl"
final String v1 = "filterNotCustomziedInputMethodList."; // const-string v1, "filterNotCustomziedInputMethodList."
android.util.Slog .v ( v0,v1 );
/* .line 337 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 339 */
/* .local v0, "noCustomizedInputMethodList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;>;" */
/* .line 340 */
/* .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;>;" */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 341 */
/* check-cast v2, Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem; */
/* .line 342 */
/* .local v2, "item":Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem; */
v3 = this.mImi;
(( android.view.inputmethod.InputMethodInfo ) v3 ).getId ( ); // invoke-virtual {v3}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;
v3 = (( com.android.server.inputmethod.InputMethodManagerServiceImpl ) p0 ).isCustomizedInputMethod ( v3 ); // invoke-virtual {p0, v3}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->isCustomizedInputMethod(Ljava/lang/String;)Z
/* if-nez v3, :cond_0 */
/* .line 343 */
/* .line 344 */
/* .line 346 */
} // .end local v2 # "item":Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;
} // :cond_0
/* .line 347 */
} // :cond_1
} // .end method
public void generateView ( Boolean p0, java.util.List p1, miuix.appcompat.app.AlertDialog$Builder p2, com.android.server.inputmethod.InputMethodMenuController$ImeSubtypeListAdapter p3, com.android.server.inputmethod.InputMethodMenuController$ImeSubtypeListAdapter p4, android.content.Context p5 ) {
/* .locals 23 */
/* .param p1, "isNotContainsCustomizedInputMethod" # Z */
/* .param p3, "builder" # Lmiuix/appcompat/app/AlertDialog$Builder; */
/* .param p4, "noCustomziedImeListAdapter" # Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter; */
/* .param p5, "customziedImeListAdapter" # Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter; */
/* .param p6, "context" # Landroid/content/Context; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(Z", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;", */
/* ">;", */
/* "Lmiuix/appcompat/app/AlertDialog$Builder;", */
/* "Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;", */
/* "Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;", */
/* "Landroid/content/Context;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 357 */
/* .local p2, "imList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;>;" */
/* move-object/from16 v7, p0 */
/* move-object/from16 v8, p4 */
/* move-object/from16 v9, p5 */
final String v0 = "Start genertating customized view."; // const-string v0, "Start genertating customized view."
final String v10 = "InputMethodManagerServiceImpl"; // const-string v10, "InputMethodManagerServiceImpl"
android.util.Slog .v ( v10,v0 );
/* .line 358 */
/* move-object/from16 v11, p6 */
this.dialogContext = v11;
/* .line 359 */
v0 = /* invoke-interface/range {p2 ..p2}, Ljava/util/List;->size()I */
/* if-nez v0, :cond_0 */
/* .line 360 */
return;
/* .line 362 */
} // :cond_0
v0 = this.dialogContext;
/* const-class v1, Landroid/view/LayoutInflater; */
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
/* move-object v12, v0 */
/* check-cast v12, Landroid/view/LayoutInflater; */
/* .line 364 */
/* .local v12, "inflater":Landroid/view/LayoutInflater; */
v0 = this.dialogContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 365 */
final String v1 = "input_method_switch_no_customized_list_view"; // const-string v1, "input_method_switch_no_customized_list_view"
final String v2 = "layout"; // const-string v2, "layout"
final String v13 = "miuix.stub"; // const-string v13, "miuix.stub"
v14 = (( android.content.res.Resources ) v0 ).getIdentifier ( v1, v2, v13 ); // invoke-virtual {v0, v1, v2, v13}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
/* .line 366 */
/* .local v14, "customViewId":I */
int v0 = 0; // const/4 v0, 0x0
(( android.view.LayoutInflater ) v12 ).inflate ( v14, v0 ); // invoke-virtual {v12, v14, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;
/* .line 367 */
/* .local v15, "customView":Landroid/view/View; */
/* if-nez v15, :cond_1 */
/* .line 368 */
return;
/* .line 371 */
} // :cond_1
v0 = this.dialogContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
final String v1 = "noCustomizedListView"; // const-string v1, "noCustomizedListView"
final String v6 = "id"; // const-string v6, "id"
v5 = (( android.content.res.Resources ) v0 ).getIdentifier ( v1, v6, v13 ); // invoke-virtual {v0, v1, v6, v13}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
/* .line 373 */
/* .local v5, "noCustomizedListViewId":I */
/* nop */
/* .line 374 */
(( android.view.View ) v15 ).findViewById ( v5 ); // invoke-virtual {v15, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;
/* move-object v4, v0 */
/* check-cast v4, Landroid/widget/ListView; */
/* .line 375 */
/* .local v4, "noCustomListView":Landroid/widget/ListView; */
/* if-nez v4, :cond_2 */
/* .line 376 */
return;
/* .line 379 */
} // :cond_2
v0 = this.dialogContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
final String v1 = "indicator"; // const-string v1, "indicator"
v3 = (( android.content.res.Resources ) v0 ).getIdentifier ( v1, v6, v13 ); // invoke-virtual {v0, v1, v6, v13}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
/* .line 381 */
/* .local v3, "indicatorId":I */
(( android.view.View ) v15 ).findViewById ( v3 ); // invoke-virtual {v15, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;
/* move-object v2, v0 */
/* check-cast v2, Landroid/widget/ImageView; */
/* .line 383 */
/* .local v2, "indicator":Landroid/widget/ImageView; */
v0 = this.dialogContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
final String v1 = "noCustomizedListViewTitle"; // const-string v1, "noCustomizedListViewTitle"
v1 = (( android.content.res.Resources ) v0 ).getIdentifier ( v1, v6, v13 ); // invoke-virtual {v0, v1, v6, v13}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
/* .line 385 */
/* .local v1, "noCustomizedListViewTitleId":I */
(( android.view.View ) v15 ).findViewById ( v1 ); // invoke-virtual {v15, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;
/* .line 387 */
/* .local v0, "noCustomizedListViewTitle":Landroid/view/View; */
/* move-object/from16 v16, v0 */
} // .end local v0 # "noCustomizedListViewTitle":Landroid/view/View;
/* .local v16, "noCustomizedListViewTitle":Landroid/view/View; */
/* iget v0, v9, Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;->mCheckedItem:I */
/* move/from16 v17, v1 */
} // .end local v1 # "noCustomizedListViewTitleId":I
/* .local v17, "noCustomizedListViewTitleId":I */
int v1 = -1; // const/4 v1, -0x1
/* move/from16 v18, v3 */
} // .end local v3 # "indicatorId":I
/* .local v18, "indicatorId":I */
/* if-ne v0, v1, :cond_3 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_3
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
/* iput-boolean v0, v7, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->isExpanded:Z */
/* .line 389 */
v0 = this.dialogContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
final String v1 = "listview_open_indicator"; // const-string v1, "listview_open_indicator"
final String v3 = "drawable"; // const-string v3, "drawable"
v1 = (( android.content.res.Resources ) v0 ).getIdentifier ( v1, v3, v13 ); // invoke-virtual {v0, v1, v3, v13}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
/* .line 391 */
/* .local v1, "openIndicatorDrawableId":I */
v0 = this.dialogContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* move/from16 v20, v5 */
} // .end local v5 # "noCustomizedListViewId":I
/* .local v20, "noCustomizedListViewId":I */
final String v5 = "listview_close_indicator"; // const-string v5, "listview_close_indicator"
v21 = (( android.content.res.Resources ) v0 ).getIdentifier ( v5, v3, v13 ); // invoke-virtual {v0, v5, v3, v13}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
/* .line 394 */
/* .local v21, "closeIndicatorDrawableId":I */
/* iget-boolean v0, v7, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->isExpanded:Z */
/* const/16 v5, 0x8 */
/* if-nez v0, :cond_4 */
/* .line 395 */
(( android.widget.ListView ) v4 ).setVisibility ( v5 ); // invoke-virtual {v4, v5}, Landroid/widget/ListView;->setVisibility(I)V
/* .line 397 */
} // :cond_4
int v0 = 0; // const/4 v0, 0x0
(( android.widget.ListView ) v4 ).setVisibility ( v0 ); // invoke-virtual {v4, v0}, Landroid/widget/ListView;->setVisibility(I)V
/* .line 398 */
(( android.widget.ImageView ) v2 ).setImageResource ( v1 ); // invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageResource(I)V
/* .line 399 */
com.android.server.inputmethod.InputMethodManagerServiceImpl .setNoCustomizedInputMethodListViewHeight ( v4,v8 );
/* .line 403 */
} // :goto_1
(( android.widget.ListView ) v4 ).setAdapter ( v8 ); // invoke-virtual {v4, v8}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V
/* .line 404 */
/* new-instance v0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$1; */
/* invoke-direct {v0, v7, v9, v8}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$1;-><init>(Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;)V */
(( android.widget.ListView ) v4 ).setOnItemClickListener ( v0 ); // invoke-virtual {v4, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
/* .line 414 */
/* new-instance v3, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$2; */
/* move-object/from16 v8, v16 */
} // .end local v16 # "noCustomizedListViewTitle":Landroid/view/View;
/* .local v8, "noCustomizedListViewTitle":Landroid/view/View; */
/* move-object v0, v3 */
/* move/from16 v16, v17 */
/* move/from16 v17, v1 */
} // .end local v1 # "openIndicatorDrawableId":I
/* .local v16, "noCustomizedListViewTitleId":I */
/* .local v17, "openIndicatorDrawableId":I */
/* move-object/from16 v1, p0 */
/* move-object/from16 v19, v2 */
} // .end local v2 # "indicator":Landroid/widget/ImageView;
/* .local v19, "indicator":Landroid/widget/ImageView; */
/* move-object v9, v3 */
/* move/from16 v3, v17 */
/* move-object/from16 v22, v4 */
} // .end local v4 # "noCustomListView":Landroid/widget/ListView;
/* .local v22, "noCustomListView":Landroid/widget/ListView; */
/* move v11, v5 */
/* move-object/from16 v5, p4 */
/* move-object v11, v6 */
/* move/from16 v6, v21 */
/* invoke-direct/range {v0 ..v6}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$2;-><init>(Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;Landroid/widget/ImageView;ILandroid/widget/ListView;Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;I)V */
(( android.view.View ) v8 ).setOnClickListener ( v9 ); // invoke-virtual {v8, v9}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V
/* .line 431 */
/* move-object/from16 v0, p3 */
(( miuix.appcompat.app.AlertDialog$Builder ) v0 ).setView ( v15 ); // invoke-virtual {v0, v15}, Lmiuix/appcompat/app/AlertDialog$Builder;->setView(Landroid/view/View;)Lmiuix/appcompat/app/AlertDialog$Builder;
/* .line 432 */
if ( p1 != null) { // if-eqz p1, :cond_6
/* .line 433 */
final String v1 = "There\'s no customized ime currently, so customized ime view will hidden"; // const-string v1, "There\'s no customized ime currently, so customized ime view will hidden"
android.util.Slog .v ( v10,v1 );
/* .line 434 */
v1 = this.dialogContext;
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
final String v2 = "dividerLine"; // const-string v2, "dividerLine"
v1 = (( android.content.res.Resources ) v1 ).getIdentifier ( v2, v11, v13 ); // invoke-virtual {v1, v2, v11, v13}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
/* .line 436 */
/* .local v1, "dividerLineId":I */
(( android.view.View ) v15 ).findViewById ( v1 ); // invoke-virtual {v15, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;
/* .line 437 */
/* .local v2, "dividerLine":Landroid/view/View; */
/* if-nez v2, :cond_5 */
/* .line 438 */
return;
/* .line 441 */
} // :cond_5
/* const/16 v3, 0x8 */
(( android.view.View ) v8 ).setVisibility ( v3 ); // invoke-virtual {v8, v3}, Landroid/view/View;->setVisibility(I)V
/* .line 442 */
(( android.view.View ) v2 ).setVisibility ( v3 ); // invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V
/* .line 444 */
} // .end local v1 # "dividerLineId":I
} // .end local v2 # "dividerLine":Landroid/view/View;
} // :cond_6
return;
} // .end method
public Boolean getMirrorInputState ( ) {
/* .locals 1 */
/* .line 712 */
/* iget-boolean v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mMirrorInputState:Z */
} // .end method
public Integer getNoCustomizedCheckedItem ( ) {
/* .locals 1 */
/* .line 553 */
/* iget v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->noCustomizedCheckedItem:I */
} // .end method
public android.view.inputmethod.InputMethodInfo getNoCustomizedIms ( ) {
/* .locals 1 */
/* .line 558 */
v0 = this.mNoCustomizedIms;
} // .end method
public getNoCustomizedSubtypeIds ( ) {
/* .locals 1 */
/* .line 563 */
v0 = this.mNoCustomizedSubtypeIds;
} // .end method
public void init ( android.os.Handler p0, com.android.server.inputmethod.InputMethodBindingController p1, android.content.Context p2 ) {
/* .locals 3 */
/* .param p1, "handler" # Landroid/os/Handler; */
/* .param p2, "bindingController" # Lcom/android/server/inputmethod/InputMethodBindingController; */
/* .param p3, "context" # Landroid/content/Context; */
/* .line 127 */
this.mHandler = p1;
/* .line 128 */
this.mBindingController = p2;
/* .line 129 */
this.mContext = p3;
/* .line 130 */
(( com.android.server.inputmethod.InputMethodManagerServiceImpl ) p0 ).registerObserver ( p3 ); // invoke-virtual {p0, p3}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->registerObserver(Landroid/content/Context;)V
/* .line 132 */
try { // :try_start_0
v0 = this.mContext;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 133 */
/* nop */
/* .line 134 */
/* nop */
/* .line 133 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "mirror_input_state"; // const-string v1, "mirror_input_state"
v0 = android.provider.Settings$Secure .getInt ( v0,v1 );
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* .line 134 */
int v1 = 0; // const/4 v1, 0x0
} // :cond_0
/* nop */
} // :goto_0
/* iput-boolean v1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mMirrorInputState:Z */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 138 */
} // :cond_1
/* .line 136 */
/* :catch_0 */
/* move-exception v0 */
/* .line 137 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Exception cause:Failed to read Settings for MIRROR_INPUT_STATE:"; // const-string v2, "Exception cause:Failed to read Settings for MIRROR_INPUT_STATE:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "InputMethodManagerServiceImpl"; // const-string v2, "InputMethodManagerServiceImpl"
android.util.Slog .e ( v2,v1 );
/* .line 139 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
return;
} // .end method
public void initNoCustomizedInputMethodData ( java.util.List p0, java.lang.String p1, Integer p2, Integer p3 ) {
/* .locals 5 */
/* .param p2, "lastInputMethodId" # Ljava/lang/String; */
/* .param p3, "lastInputMethodSubtypeId" # I */
/* .param p4, "NOT_A_SUBTYPE_ID" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;", */
/* ">;", */
/* "Ljava/lang/String;", */
/* "II)V" */
/* } */
} // .end annotation
/* .line 307 */
/* .local p1, "noCustomizedInputMethodList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;>;" */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "initNoCustomizedInputMethodData noCustomizedInputMethodList:"; // const-string v1, "initNoCustomizedInputMethodData noCustomizedInputMethodList:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "InputMethodManagerServiceImpl"; // const-string v1, "InputMethodManagerServiceImpl"
android.util.Slog .v ( v1,v0 );
/* .line 309 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->noCustomizedCheckedItem:I */
v0 = /* .line 310 */
/* .line 311 */
/* .local v0, "N":I */
/* new-array v1, v0, [Landroid/view/inputmethod/InputMethodInfo; */
this.mNoCustomizedIms = v1;
/* .line 312 */
/* new-array v1, v0, [I */
this.mNoCustomizedSubtypeIds = v1;
/* .line 313 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* if-ge v1, v0, :cond_3 */
/* .line 314 */
/* check-cast v2, Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem; */
/* .line 315 */
/* .local v2, "item":Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem; */
v3 = this.mNoCustomizedIms;
v4 = this.mImi;
/* aput-object v4, v3, v1 */
/* .line 316 */
v3 = this.mNoCustomizedSubtypeIds;
/* iget v4, v2, Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;->mSubtypeId:I */
/* aput v4, v3, v1 */
/* .line 317 */
v3 = this.mNoCustomizedIms;
/* aget-object v3, v3, v1 */
(( android.view.inputmethod.InputMethodInfo ) v3 ).getId ( ); // invoke-virtual {v3}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;
v3 = (( java.lang.String ) v3 ).equals ( p2 ); // invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 318 */
v3 = this.mNoCustomizedSubtypeIds;
/* aget v3, v3, v1 */
/* .line 319 */
/* .local v3, "subtypeId":I */
/* if-eq v3, p4, :cond_1 */
/* if-ne p3, p4, :cond_0 */
if ( v3 != null) { // if-eqz v3, :cond_1
} // :cond_0
/* if-ne v3, p3, :cond_2 */
/* .line 322 */
} // :cond_1
/* iput v1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->noCustomizedCheckedItem:I */
/* .line 313 */
} // .end local v2 # "item":Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;
} // .end local v3 # "subtypeId":I
} // :cond_2
/* add-int/lit8 v1, v1, 0x1 */
/* .line 326 */
} // .end local v1 # "i":I
} // :cond_3
return;
} // .end method
public Boolean isCallingBetweenCustomIME ( android.content.Context p0, Integer p1, java.lang.String p2 ) {
/* .locals 6 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "uid" # I */
/* .param p3, "targetPkgName" # Ljava/lang/String; */
/* .line 615 */
v0 = v0 = com.android.server.inputmethod.InputMethodManagerServiceImpl.customizedInputMethodList;
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 616 */
/* .line 618 */
} // :cond_0
(( android.content.Context ) p1 ).getPackageManager ( ); // invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
(( android.content.pm.PackageManager ) v0 ).getPackagesForUid ( p2 ); // invoke-virtual {v0, p2}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;
/* .line 619 */
/* .local v0, "packages":[Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_4
/* array-length v2, v0 */
/* if-nez v2, :cond_1 */
/* .line 623 */
} // :cond_1
/* array-length v2, v0 */
/* move v3, v1 */
} // :goto_0
/* if-ge v3, v2, :cond_3 */
/* aget-object v4, v0, v3 */
/* .line 624 */
/* .local v4, "packageName":Ljava/lang/String; */
v5 = v5 = com.android.server.inputmethod.InputMethodManagerServiceImpl.customizedInputMethodList;
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 625 */
int v1 = 1; // const/4 v1, 0x1
/* .line 623 */
} // .end local v4 # "packageName":Ljava/lang/String;
} // :cond_2
/* add-int/lit8 v3, v3, 0x1 */
/* .line 628 */
} // :cond_3
/* .line 620 */
} // :cond_4
} // :goto_1
} // .end method
public Boolean isCustomizedInputMethod ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "inputMethodId" # Ljava/lang/String; */
/* .line 287 */
final String v0 = ""; // const-string v0, ""
/* .line 288 */
/* .local v0, "inputMethodName":Ljava/lang/String; */
v1 = android.text.TextUtils .isEmpty ( p1 );
int v2 = 0; // const/4 v2, 0x0
/* if-nez v1, :cond_0 */
/* .line 289 */
/* const/16 v1, 0x2f */
v1 = (( java.lang.String ) p1 ).indexOf ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(I)I
/* .line 290 */
/* .local v1, "endIndex":I */
/* if-lez v1, :cond_0 */
/* .line 291 */
(( java.lang.String ) p1 ).substring ( v2, v1 ); // invoke-virtual {p1, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;
/* .line 294 */
} // .end local v1 # "endIndex":I
} // :cond_0
v1 = v1 = com.android.server.inputmethod.InputMethodManagerServiceImpl.customizedInputMethodList;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 295 */
int v1 = 1; // const/4 v1, 0x1
/* .line 297 */
} // :cond_1
} // .end method
public Boolean isInputMethodWindowInvisibleByRecentTask ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "imeWindowVis" # I */
/* .line 632 */
/* iget-boolean v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->callStartInputOrWindowGainedFocusFlag:Z */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 633 */
/* iput-boolean v1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->callStartInputOrWindowGainedFocusFlag:Z */
/* .line 634 */
/* if-nez p1, :cond_0 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
/* .line 636 */
} // :cond_1
} // .end method
public Boolean isInputMethodWindowInvisibleByScreenOn ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "imeWindowVis" # I */
/* .line 644 */
/* iget-boolean v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mScreenStatus:Z */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v2, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mScreenOffLastTime:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* if-nez p1, :cond_0 */
/* .line 645 */
/* iput-boolean v1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mScreenOffLastTime:Z */
/* .line 646 */
int v0 = 1; // const/4 v0, 0x1
/* .line 648 */
} // :cond_0
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 649 */
/* iput-boolean v1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mScreenOffLastTime:Z */
/* .line 651 */
} // :cond_1
} // .end method
public Boolean isPad ( ) {
/* .locals 1 */
/* .line 283 */
/* sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z */
} // .end method
public Boolean isSupportedCustomizedDialog ( ) {
/* .locals 1 */
/* .line 685 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
public Boolean notifyAcceptInput ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "status" # I */
/* .line 243 */
v0 = this.mMonitorBinder;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 244 */
android.os.Parcel .obtain ( );
/* .line 245 */
/* .local v0, "request":Landroid/os/Parcel; */
/* iput p1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mLastAcceptStatus:I */
/* .line 247 */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
final String v2 = "com.android.synergy.Callback"; // const-string v2, "com.android.synergy.Callback"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 248 */
(( android.os.Parcel ) v0 ).writeInt ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 249 */
/* iget v2, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mMirrorDisplayId:I */
(( android.os.Parcel ) v0 ).writeInt ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
/* .line 250 */
v2 = this.mMonitorBinder;
int v3 = 1; // const/4 v3, 0x1
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 252 */
/* nop */
/* .line 257 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 252 */
/* .line 257 */
/* :catchall_0 */
/* move-exception v1 */
/* .line 253 */
/* :catch_0 */
/* move-exception v2 */
/* .line 254 */
/* .local v2, "e":Ljava/lang/Exception; */
try { // :try_start_1
this.mMonitorBinder = v1;
/* .line 255 */
(( java.lang.Exception ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 257 */
} // .end local v2 # "e":Ljava/lang/Exception;
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 258 */
/* .line 257 */
} // :goto_0
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 258 */
/* throw v1 */
/* .line 260 */
} // .end local v0 # "request":Landroid/os/Parcel;
} // :cond_0
} // :goto_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean onMiuiTransact ( Integer p0, android.os.Parcel p1, android.os.Parcel p2, Integer p3, com.android.internal.inputmethod.IRemoteInputConnection p4, Integer p5, android.os.Handler p6 ) {
/* .locals 5 */
/* .param p1, "code" # I */
/* .param p2, "data" # Landroid/os/Parcel; */
/* .param p3, "reply" # Landroid/os/Parcel; */
/* .param p4, "flags" # I */
/* .param p5, "inputContext" # Lcom/android/internal/inputmethod/IRemoteInputConnection; */
/* .param p6, "imeWindowVis" # I */
/* .param p7, "handler" # Landroid/os/Handler; */
/* .line 190 */
int v0 = 0; // const/4 v0, 0x0
int v1 = 1; // const/4 v1, 0x1
/* packed-switch p1, :pswitch_data_0 */
/* .line 234 */
/* .line 192 */
/* :pswitch_0 */
try { // :try_start_0
v2 = android.os.Binder .getCallingUid ( );
/* .line 193 */
/* .local v2, "callingUid":I */
/* const/16 v3, 0x3e8 */
/* if-ne v2, v3, :cond_2 */
/* .line 196 */
/* and-int/lit8 v3, p6, 0x2 */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 197 */
/* const-class v3, Lcom/android/server/inputmethod/InputMethodManagerInternal; */
/* .line 198 */
com.android.server.LocalServices .getService ( v3 );
/* check-cast v3, Lcom/android/server/inputmethod/InputMethodManagerInternal; */
/* .line 199 */
/* .local v3, "service":Lcom/android/server/inputmethod/InputMethodManagerInternal; */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 200 */
/* const/16 v4, 0x10 */
(( com.android.server.inputmethod.InputMethodManagerInternal ) v3 ).hideCurrentInputMethod ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/inputmethod/InputMethodManagerInternal;->hideCurrentInputMethod(I)V
/* .line 203 */
} // .end local v3 # "service":Lcom/android/server/inputmethod/InputMethodManagerInternal;
} // :cond_0
(( android.os.Parcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;
/* .line 204 */
/* .local v3, "text":Ljava/lang/String; */
if ( p5 != null) { // if-eqz p5, :cond_1
/* .line 205 */
(( com.android.server.inputmethod.InputMethodManagerServiceImpl ) p0 ).commitTextForSynergy ( p5, v3, v1 ); // invoke-virtual {p0, p5, v3, v1}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->commitTextForSynergy(Lcom/android/internal/inputmethod/IRemoteInputConnection;Ljava/lang/String;I)V
/* .line 207 */
} // :cond_1
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 208 */
int v4 = 1; // const/4 v4, 0x1
/* .line 209 */
/* .local v4, "res":I */
(( android.os.Parcel ) p3 ).writeInt ( v4 ); // invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V
/* .line 210 */
/* .line 194 */
} // .end local v3 # "text":Ljava/lang/String;
} // .end local v4 # "res":I
} // :cond_2
/* new-instance v1, Ljava/lang/SecurityException; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Calling CODE_INPUT_TEXT unsafe by "; // const-string v4, "Calling CODE_INPUT_TEXT unsafe by "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v1, v3}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
} // .end local p0 # "this":Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;
} // .end local p1 # "code":I
} // .end local p2 # "data":Landroid/os/Parcel;
} // .end local p3 # "reply":Landroid/os/Parcel;
} // .end local p4 # "flags":I
} // .end local p5 # "inputContext":Lcom/android/internal/inputmethod/IRemoteInputConnection;
} // .end local p6 # "imeWindowVis":I
} // .end local p7 # "handler":Landroid/os/Handler;
/* throw v1 */
/* .line 212 */
} // .end local v2 # "callingUid":I
/* .restart local p0 # "this":Lcom/android/server/inputmethod/InputMethodManagerServiceImpl; */
/* .restart local p1 # "code":I */
/* .restart local p2 # "data":Landroid/os/Parcel; */
/* .restart local p3 # "reply":Landroid/os/Parcel; */
/* .restart local p4 # "flags":I */
/* .restart local p5 # "inputContext":Lcom/android/internal/inputmethod/IRemoteInputConnection; */
/* .restart local p6 # "imeWindowVis":I */
/* .restart local p7 # "handler":Landroid/os/Handler; */
/* :pswitch_1 */
(( android.os.Parcel ) p2 ).readStrongBinder ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;
this.mMonitorBinder = v2;
/* .line 213 */
/* invoke-direct {p0}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->registerPointerEventListener()V */
/* .line 214 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 215 */
/* .line 217 */
/* :pswitch_2 */
int v2 = 0; // const/4 v2, 0x0
this.mMonitorBinder = v2;
/* .line 218 */
/* invoke-direct {p0}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->unregisterPointerEventListener()V */
/* .line 219 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 220 */
/* .line 222 */
/* :pswitch_3 */
v2 = (( android.os.Parcel ) p2 ).readInt ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
/* iput v2, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mSynergyOperate:I */
/* .line 223 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 224 */
/* .line 227 */
/* :pswitch_4 */
v2 = (( android.os.Parcel ) p2 ).readInt ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
/* iput v2, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mMirrorDisplayId:I */
/* .line 228 */
if ( v2 != null) { // if-eqz v2, :cond_3
/* move v2, v1 */
} // :cond_3
/* move v2, v0 */
} // :goto_0
/* iput-boolean v2, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mIsFromRemoteDevice:Z */
/* .line 229 */
final String v2 = "InputMethodManagerServiceImpl"; // const-string v2, "InputMethodManagerServiceImpl"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "set mIsFromRemoteDevice as:" */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v4, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mIsFromRemoteDevice:Z */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v4 = ",displayId as:"; // const-string v4, ",displayId as:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mMirrorDisplayId:I */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " via mirror"; // const-string v4, " via mirror"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v3 );
/* .line 231 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 232 */
/* .line 236 */
/* :catch_0 */
/* move-exception v1 */
/* .line 237 */
/* .local v1, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 239 */
} // .end local v1 # "e":Ljava/lang/Exception;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0xfffffa */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public void onSessionCreated ( com.android.internal.inputmethod.IInputMethodSessionCallback$Stub p0 ) {
/* .locals 1 */
/* .param p1, "callback" # Lcom/android/internal/inputmethod/IInputMethodSessionCallback$Stub; */
/* .line 736 */
/* sget-boolean v0, Lmiui/os/Build;->IS_DEBUGGABLE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 737 */
com.android.server.inputmethod.InputMethodMonitor .getInstance ( );
(( com.android.server.inputmethod.InputMethodMonitor ) v0 ).finishCreateSession ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/inputmethod/InputMethodMonitor;->finishCreateSession(Lcom/android/internal/inputmethod/IInputMethodSessionCallback$Stub;)V
/* .line 739 */
} // :cond_0
return;
} // .end method
public void onSwitchIME ( android.content.Context p0, android.view.inputmethod.InputMethodInfo p1, java.lang.String p2, java.util.List p3, com.android.server.inputmethod.InputMethodUtils$InputMethodSettings p4, android.util.ArrayMap p5 ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "curInputMethodInfo" # Landroid/view/inputmethod/InputMethodInfo; */
/* .param p3, "lastInputMethodId" # Ljava/lang/String; */
/* .param p5, "inputMethodSettings" # Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/content/Context;", */
/* "Landroid/view/inputmethod/InputMethodInfo;", */
/* "Ljava/lang/String;", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;", */
/* ">;", */
/* "Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;", */
/* "Landroid/util/ArrayMap<", */
/* "Ljava/lang/String;", */
/* "Landroid/view/inputmethod/InputMethodInfo;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 179 */
/* .local p4, "imList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;>;" */
/* .local p6, "methodMap":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Landroid/view/inputmethod/InputMethodInfo;>;" */
(( android.view.inputmethod.InputMethodInfo ) p2 ).getId ( ); // invoke-virtual {p2}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;
v0 = android.text.TextUtils .equals ( v0,p3 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 180 */
return;
/* .line 182 */
} // :cond_0
(( android.view.inputmethod.InputMethodInfo ) p2 ).getPackageName ( ); // invoke-virtual {p2}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;
android.inputmethodservice.InputMethodAnalyticsUtil .addNotificationPanelRecord ( p1,v0 );
/* .line 183 */
return;
} // .end method
public void registerObserver ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 690 */
/* new-instance v0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$4; */
android.os.Handler .getMain ( );
/* invoke-direct {v0, p0, v1, p1}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$4;-><init>(Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;Landroid/os/Handler;Landroid/content/Context;)V */
this.mObserver = v0;
/* .line 707 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "mirror_input_state"; // const-string v1, "mirror_input_state"
android.provider.Settings$Secure .getUriFor ( v1 );
v2 = this.mObserver;
int v3 = -1; // const/4 v3, -0x1
int v4 = 0; // const/4 v4, 0x0
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v4, v2, v3 ); // invoke-virtual {v0, v1, v4, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 709 */
return;
} // .end method
public void removeCustomTitle ( miuix.appcompat.app.AlertDialog$Builder p0, android.view.View p1 ) {
/* .locals 1 */
/* .param p1, "dialogBuilder" # Lmiuix/appcompat/app/AlertDialog$Builder; */
/* .param p2, "switchingDialogTitleView" # Landroid/view/View; */
/* .line 570 */
int v0 = 0; // const/4 v0, 0x0
(( miuix.appcompat.app.AlertDialog$Builder ) p1 ).setCustomTitle ( v0 ); // invoke-virtual {p1, v0}, Lmiuix/appcompat/app/AlertDialog$Builder;->setCustomTitle(Landroid/view/View;)Lmiuix/appcompat/app/AlertDialog$Builder;
/* .line 571 */
/* const v0, 0x104090b */
(( miuix.appcompat.app.AlertDialog$Builder ) p1 ).setTitle ( v0 ); // invoke-virtual {p1, v0}, Lmiuix/appcompat/app/AlertDialog$Builder;->setTitle(I)Lmiuix/appcompat/app/AlertDialog$Builder;
/* .line 572 */
int p2 = 0; // const/4 p2, 0x0
/* .line 573 */
return;
} // .end method
public void sendKeyboardCaps ( ) {
/* .locals 2 */
/* .line 274 */
final String v0 = "InputMethodManagerServiceImpl"; // const-string v0, "InputMethodManagerServiceImpl"
final String v1 = "Send caps event from keyboard."; // const-string v1, "Send caps event from keyboard."
android.util.Slog .d ( v0,v1 );
/* .line 275 */
(( com.android.server.inputmethod.InputMethodManagerServiceImpl ) p0 ).isPad ( ); // invoke-virtual {p0}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->isPad()Z
/* .line 276 */
return;
} // .end method
public void setDialogImmersive ( miuix.appcompat.app.AlertDialog p0 ) {
/* .locals 3 */
/* .param p1, "dialog" # Lmiuix/appcompat/app/AlertDialog; */
/* .line 450 */
v0 = (( com.android.server.inputmethod.InputMethodManagerServiceImpl ) p0 ).isPad ( ); // invoke-virtual {p0}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->isPad()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 452 */
return;
/* .line 454 */
} // :cond_0
(( miuix.appcompat.app.AlertDialog ) p1 ).getWindow ( ); // invoke-virtual {p1}, Lmiuix/appcompat/app/AlertDialog;->getWindow()Landroid/view/Window;
/* .line 455 */
/* .local v0, "w":Landroid/view/Window; */
/* const v1, -0x7ffff700 */
(( android.view.Window ) v0 ).addFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V
/* .line 459 */
(( android.view.Window ) v0 ).getAttributes ( ); // invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;
int v2 = 0; // const/4 v2, 0x0
(( android.view.WindowManager$LayoutParams ) v1 ).setFitInsetsSides ( v2 ); // invoke-virtual {v1, v2}, Landroid/view/WindowManager$LayoutParams;->setFitInsetsSides(I)V
/* .line 461 */
(( android.view.Window ) v0 ).getDecorView ( ); // invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;
/* .line 462 */
/* .local v1, "decorView":Landroid/view/View; */
com.android.server.inputmethod.InputMethodManagerServiceImpl .clearFitSystemWindow ( v1 );
/* .line 463 */
/* new-instance v2, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$3; */
/* invoke-direct {v2, p0, v1}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$3;-><init>(Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;Landroid/view/View;)V */
(( android.view.View ) v1 ).setOnApplyWindowInsetsListener ( v2 ); // invoke-virtual {v1, v2}, Landroid/view/View;->setOnApplyWindowInsetsListener(Landroid/view/View$OnApplyWindowInsetsListener;)V
/* .line 479 */
return;
} // .end method
public void setScreenOff ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "inputMethodWindowDisplayed" # Z */
/* .line 655 */
/* iput-boolean p1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mScreenOffLastTime:Z */
/* .line 656 */
return;
} // .end method
public void setScreenStatus ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "screenStatus" # Z */
/* .line 659 */
/* iput-boolean p1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mScreenStatus:Z */
/* .line 660 */
return;
} // .end method
public void setWindowGainedbyRecentTask ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "inputMethodWindowDisplayed" # Z */
/* .line 640 */
/* iput-boolean p1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->callStartInputOrWindowGainedFocusFlag:Z */
/* .line 641 */
return;
} // .end method
public Boolean shouldClearShowForcedFlag ( android.content.Context p0, Integer p1 ) {
/* .locals 6 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "uid" # I */
/* .line 602 */
int v0 = 0; // const/4 v0, 0x0
/* .line 603 */
/* .local v0, "result":Z */
/* if-nez p1, :cond_0 */
/* .line 604 */
} // :cond_0
(( android.content.Context ) p1 ).getPackageManager ( ); // invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
(( android.content.pm.PackageManager ) v1 ).getPackagesForUid ( p2 ); // invoke-virtual {v1, p2}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;
/* .line 605 */
/* .local v1, "packages":[Ljava/lang/String; */
if ( v1 != null) { // if-eqz v1, :cond_3
/* array-length v2, v1 */
/* if-nez v2, :cond_1 */
/* .line 608 */
} // :cond_1
/* array-length v2, v1 */
int v3 = 0; // const/4 v3, 0x0
} // :goto_0
/* if-ge v3, v2, :cond_2 */
/* aget-object v4, v1, v3 */
/* .line 609 */
/* .local v4, "packageName":Ljava/lang/String; */
final String v5 = "com.miui.home"; // const-string v5, "com.miui.home"
v5 = (( java.lang.String ) v5 ).equals ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* or-int/2addr v0, v5 */
/* .line 608 */
} // .end local v4 # "packageName":Ljava/lang/String;
/* add-int/lit8 v3, v3, 0x1 */
/* .line 611 */
} // :cond_2
/* .line 606 */
} // :cond_3
} // :goto_1
} // .end method
public Boolean synergyOperate ( ) {
/* .locals 2 */
/* .line 265 */
v0 = this.mMonitorBinder;
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mSynergyOperate:I */
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_1 */
/* iget-boolean v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mMirrorInputState:Z */
/* if-nez v0, :cond_0 */
/* iget-boolean v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mIsFromRemoteDevice:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 267 */
} // :cond_0
/* .line 269 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void updateItemView ( android.view.View p0, java.lang.CharSequence p1, java.lang.CharSequence p2, Integer p3, Integer p4, android.view.ViewGroup p5 ) {
/* .locals 9 */
/* .param p1, "view" # Landroid/view/View; */
/* .param p2, "imeName" # Ljava/lang/CharSequence; */
/* .param p3, "subtypeName" # Ljava/lang/CharSequence; */
/* .param p4, "position" # I */
/* .param p5, "checkedItem" # I */
/* .param p6, "parent" # Landroid/view/ViewGroup; */
/* .line 524 */
v0 = this.dialogContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const-string/jumbo v1, "title" */
final String v2 = "id"; // const-string v2, "id"
final String v3 = "miuix.stub"; // const-string v3, "miuix.stub"
v0 = (( android.content.res.Resources ) v0 ).getIdentifier ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
/* .line 526 */
/* .local v0, "firstTextViewId":I */
v1 = this.dialogContext;
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const-string/jumbo v4, "summary" */
v1 = (( android.content.res.Resources ) v1 ).getIdentifier ( v4, v2, v3 ); // invoke-virtual {v1, v4, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
/* .line 528 */
/* .local v1, "secondTextViewId":I */
(( android.view.View ) p1 ).findViewById ( v0 ); // invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;
/* check-cast v4, Landroid/widget/TextView; */
/* .line 529 */
/* .local v4, "firstTextView":Landroid/widget/TextView; */
(( android.view.View ) p1 ).findViewById ( v1 ); // invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;
/* check-cast v5, Landroid/widget/TextView; */
/* .line 530 */
/* .local v5, "secondTextView":Landroid/widget/TextView; */
v6 = android.text.TextUtils .isEmpty ( p3 );
int v7 = 0; // const/4 v7, 0x0
if ( v6 != null) { // if-eqz v6, :cond_0
/* .line 531 */
(( android.widget.TextView ) v4 ).setText ( p2 ); // invoke-virtual {v4, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
/* .line 532 */
/* const/16 v6, 0x8 */
(( android.widget.TextView ) v5 ).setVisibility ( v6 ); // invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V
/* .line 534 */
} // :cond_0
(( android.widget.TextView ) v4 ).setText ( p2 ); // invoke-virtual {v4, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
/* .line 535 */
(( android.widget.TextView ) v5 ).setText ( p3 ); // invoke-virtual {v5, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
/* .line 536 */
(( android.widget.TextView ) v5 ).setVisibility ( v7 ); // invoke-virtual {v5, v7}, Landroid/widget/TextView;->setVisibility(I)V
/* .line 539 */
} // :goto_0
v6 = this.dialogContext;
(( android.content.Context ) v6 ).getResources ( ); // invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
final String v8 = "radio"; // const-string v8, "radio"
v2 = (( android.content.res.Resources ) v6 ).getIdentifier ( v8, v2, v3 ); // invoke-virtual {v6, v8, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
/* .line 541 */
/* .local v2, "radioButtonId":I */
(( android.view.View ) p1 ).findViewById ( v2 ); // invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;
/* check-cast v3, Landroid/widget/RadioButton; */
/* .line 542 */
/* .local v3, "radioButton":Landroid/widget/RadioButton; */
int v6 = 1; // const/4 v6, 0x1
/* if-ne p4, p5, :cond_1 */
/* move v8, v6 */
} // :cond_1
/* move v8, v7 */
} // :goto_1
(( android.widget.RadioButton ) v3 ).setChecked ( v8 ); // invoke-virtual {v3, v8}, Landroid/widget/RadioButton;->setChecked(Z)V
/* .line 543 */
/* move-object v8, p6 */
/* check-cast v8, Landroid/widget/ListView; */
(( android.widget.ListView ) v8 ).setChoiceMode ( v7 ); // invoke-virtual {v8, v7}, Landroid/widget/ListView;->setChoiceMode(I)V
/* .line 544 */
/* if-ne p4, p5, :cond_2 */
/* .line 545 */
(( android.view.View ) p1 ).setActivated ( v6 ); // invoke-virtual {p1, v6}, Landroid/view/View;->setActivated(Z)V
/* .line 547 */
} // :cond_2
(( android.view.View ) p1 ).setActivated ( v7 ); // invoke-virtual {p1, v7}, Landroid/view/View;->setActivated(Z)V
/* .line 549 */
} // :goto_2
return;
} // .end method
public void updateSessionId ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "sessionId" # I */
/* .line 666 */
v0 = (( com.android.server.inputmethod.InputMethodManagerServiceImpl ) p0 ).synergyOperate ( ); // invoke-virtual {p0}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->synergyOperate()Z
/* if-nez v0, :cond_0 */
return;
/* .line 667 */
} // :cond_0
/* iput p1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mSessionId:I */
/* .line 668 */
return;
} // .end method
