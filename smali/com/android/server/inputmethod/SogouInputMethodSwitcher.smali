.class public Lcom/android/server/inputmethod/SogouInputMethodSwitcher;
.super Lcom/android/server/inputmethod/BaseInputMethodSwitcher;
.source "SogouInputMethodSwitcher.java"


# static fields
.field public static final DEBUG:Z = true

.field private static final PKG_SOGOU:Ljava/lang/String; = "com.sohu.inputmethod.sogou.xiaomi"

.field public static final TAG:Ljava/lang/String; = "SogouInputMethodSwitcher"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;-><init>()V

    return-void
.end method

.method private getSogouMethodIdLocked()Ljava/lang/String;
    .locals 3

    .line 166
    iget-object v0, p0, Lcom/android/server/inputmethod/SogouInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v0, v0, Lcom/android/server/inputmethod/InputMethodManagerService;->mMethodMap:Landroid/util/ArrayMap;

    invoke-virtual {v0}, Landroid/util/ArrayMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 167
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Landroid/view/inputmethod/InputMethodInfo;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/android/server/inputmethod/SogouInputMethodSwitcher;->isSogouMethodLocked(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 168
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0

    .line 170
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Landroid/view/inputmethod/InputMethodInfo;>;"
    :cond_0
    goto :goto_0

    .line 171
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method private isEditorInDefaultImeApp(Landroid/view/inputmethod/EditorInfo;)Z
    .locals 4
    .param p1, "editor"    # Landroid/view/inputmethod/EditorInfo;

    .line 180
    iget-object v0, p1, Landroid/view/inputmethod/EditorInfo;->packageName:Ljava/lang/String;

    .line 181
    .local v0, "pkg":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/inputmethod/SogouInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v1, v1, Lcom/android/server/inputmethod/InputMethodManagerService;->mSettings:Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;

    invoke-virtual {v1}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getSelectedInputMethod()Ljava/lang/String;

    move-result-object v1

    .line 182
    .local v1, "defaultIme":Ljava/lang/String;
    const/4 v2, 0x0

    .line 183
    .local v2, "cn":Landroid/content/ComponentName;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 184
    invoke-static {v1}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v3

    move-object v2, v3

    if-eqz v3, :cond_0

    .line 185
    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    .line 183
    :goto_0
    return v3
.end method

.method private isSogouMethodLocked(Ljava/lang/String;)Z
    .locals 3
    .param p1, "methodId"    # Ljava/lang/String;

    .line 175
    iget-object v0, p0, Lcom/android/server/inputmethod/SogouInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v0, v0, Lcom/android/server/inputmethod/InputMethodManagerService;->mMethodMap:Landroid/util/ArrayMap;

    invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodInfo;

    .line 176
    .local v0, "imi":Landroid/view/inputmethod/InputMethodInfo;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.sohu.inputmethod.sogou.xiaomi"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method


# virtual methods
.method public filterMethodLocked(Ljava/util/List;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/view/inputmethod/InputMethodInfo;",
            ">;)",
            "Ljava/util/List<",
            "Landroid/view/inputmethod/InputMethodInfo;",
            ">;"
        }
    .end annotation

    .line 162
    .local p1, "methodInfos":Ljava/util/List;, "Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;"
    return-object p1
.end method

.method public mayChangeInputMethodLocked(Landroid/view/inputmethod/EditorInfo;)Z
    .locals 18
    .param p1, "attribute"    # Landroid/view/inputmethod/EditorInfo;

    .line 33
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {}, Lcom/android/server/inputmethod/DeviceUtils;->isFlipDevice()Z

    move-result v2

    const/4 v3, 0x0

    if-nez v2, :cond_0

    .line 34
    return v3

    .line 37
    :cond_0
    iget v2, v1, Landroid/view/inputmethod/EditorInfo;->inputType:I

    invoke-static {v2}, Lcom/android/server/inputmethod/SogouInputMethodSwitcher;->isPasswdInputType(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 38
    return v3

    .line 41
    :cond_1
    iget-object v2, v0, Lcom/android/server/inputmethod/SogouInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v2}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;

    move-result-object v2

    const-string v4, "SogouInputMethodSwitcher"

    if-nez v2, :cond_2

    .line 42
    const-string v2, "input_service has no current_method_id"

    invoke-static {v4, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    return v3

    .line 46
    :cond_2
    if-nez v1, :cond_3

    .line 47
    const-string v2, "editor_info is null, we cannot judge"

    invoke-static {v4, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    return v3

    .line 51
    :cond_3
    iget-object v2, v0, Lcom/android/server/inputmethod/SogouInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v2, v2, Lcom/android/server/inputmethod/InputMethodManagerService;->mMethodMap:Landroid/util/ArrayMap;

    iget-object v5, v0, Lcom/android/server/inputmethod/SogouInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v5}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/inputmethod/InputMethodInfo;

    .line 52
    .local v2, "curMethodInfo":Landroid/view/inputmethod/InputMethodInfo;
    if-nez v2, :cond_4

    .line 53
    const-string v5, "fail to find current_method_info in the map"

    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    return v3

    .line 57
    :cond_4
    invoke-static {}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->getInstance()Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    move-result-object v5

    iget-object v5, v5, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mBindingController:Lcom/android/server/inputmethod/InputMethodBindingController;

    if-nez v5, :cond_5

    .line 58
    const-string v5, "IMMS_IMPL has not init"

    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    return v3

    .line 62
    :cond_5
    invoke-direct/range {p0 .. p0}, Lcom/android/server/inputmethod/SogouInputMethodSwitcher;->getSogouMethodIdLocked()Ljava/lang/String;

    move-result-object v5

    .line 63
    .local v5, "sogouMethodId":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 64
    const-string v6, "fail to find sogou_input_method in input_method_list"

    invoke-static {v4, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    return v3

    .line 68
    :cond_6
    invoke-direct/range {p0 .. p1}, Lcom/android/server/inputmethod/SogouInputMethodSwitcher;->isEditorInDefaultImeApp(Landroid/view/inputmethod/EditorInfo;)Z

    move-result v6

    .line 69
    .local v6, "editorInDefaultImeApp":Z
    if-eqz v6, :cond_7

    .line 70
    const-string v7, "editor in default ime app"

    invoke-static {v4, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    return v3

    .line 74
    :cond_7
    iget-object v7, v0, Lcom/android/server/inputmethod/SogouInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v7}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v0, v7}, Lcom/android/server/inputmethod/SogouInputMethodSwitcher;->isSogouMethodLocked(Ljava/lang/String;)Z

    move-result v7

    .line 75
    .local v7, "isSogouInputNow":Z
    iget-object v8, v0, Lcom/android/server/inputmethod/SogouInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v8, v8, Lcom/android/server/inputmethod/InputMethodManagerService;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/android/server/inputmethod/DeviceUtils;->isFlipTinyScreen(Landroid/content/Context;)Z

    move-result v8

    .line 77
    .local v8, "isFlipTinyScreen":Z
    const/4 v9, 0x1

    if-nez v7, :cond_8

    if-eqz v8, :cond_8

    move v10, v9

    goto :goto_0

    :cond_8
    move v10, v3

    .line 78
    .local v10, "switchToSogouInput":Z
    :goto_0
    if-eqz v7, :cond_9

    if-nez v8, :cond_9

    move v11, v9

    goto :goto_1

    :cond_9
    move v11, v3

    .line 80
    .local v11, "switchFromSogouInput":Z
    :goto_1
    const/4 v12, -0x1

    const/4 v13, 0x2

    if-eqz v10, :cond_a

    .line 81
    invoke-static {}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->getInstance()Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    move-result-object v3

    iget-object v3, v3, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mBindingController:Lcom/android/server/inputmethod/InputMethodBindingController;

    .line 82
    invoke-virtual {v3, v5}, Lcom/android/server/inputmethod/InputMethodBindingController;->setSelectedMethodId(Ljava/lang/String;)V

    .line 83
    iget-object v3, v0, Lcom/android/server/inputmethod/SogouInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v3}, Lcom/android/server/inputmethod/InputMethodManagerService;->clearClientSessionsLocked()V

    .line 84
    iget-object v3, v0, Lcom/android/server/inputmethod/SogouInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v3, v13}, Lcom/android/server/inputmethod/InputMethodManagerService;->unbindCurrentClientLocked(I)V

    .line 85
    invoke-static {}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->getInstance()Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    move-result-object v3

    iget-object v3, v3, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mBindingController:Lcom/android/server/inputmethod/InputMethodBindingController;

    invoke-virtual {v3}, Lcom/android/server/inputmethod/InputMethodBindingController;->unbindCurrentMethod()V

    .line 86
    iget-object v3, v0, Lcom/android/server/inputmethod/SogouInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v4, v0, Lcom/android/server/inputmethod/SogouInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v4}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v12}, Lcom/android/server/inputmethod/InputMethodManagerService;->setInputMethodLocked(Ljava/lang/String;I)V

    .line 87
    return v9

    .line 89
    :cond_a
    if-eqz v11, :cond_12

    .line 90
    iget-object v14, v0, Lcom/android/server/inputmethod/SogouInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v14, v14, Lcom/android/server/inputmethod/InputMethodManagerService;->mSettings:Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;

    invoke-virtual {v14}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getSelectedInputMethod()Ljava/lang/String;

    move-result-object v14

    .line 91
    .local v14, "selectedInputMethod":Ljava/lang/String;
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_f

    .line 92
    const-string/jumbo v15, "something is weired, maybe the input method app are uninstalled"

    invoke-static {v4, v15}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    iget-object v15, v0, Lcom/android/server/inputmethod/SogouInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v15, v15, Lcom/android/server/inputmethod/InputMethodManagerService;->mSettings:Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;

    .line 94
    invoke-virtual {v15}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getEnabledInputMethodListLocked()Ljava/util/ArrayList;

    move-result-object v15

    .line 93
    invoke-static {v15}, Lcom/android/server/inputmethod/InputMethodInfoUtils;->getMostApplicableDefaultIME(Ljava/util/List;)Landroid/view/inputmethod/InputMethodInfo;

    move-result-object v15

    .line 95
    .local v15, "imi":Landroid/view/inputmethod/InputMethodInfo;
    const-string v9, "com.sohu.inputmethod.sogou.xiaomi"

    if-eqz v15, :cond_b

    invoke-virtual {v15}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_f

    .line 96
    :cond_b
    const-string v12, "fail to find a most applicable default ime"

    invoke-static {v4, v12}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    iget-object v12, v0, Lcom/android/server/inputmethod/SogouInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v12, v12, Lcom/android/server/inputmethod/InputMethodManagerService;->mSettings:Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;

    invoke-virtual {v12}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getEnabledInputMethodListLocked()Ljava/util/ArrayList;

    move-result-object v12

    .line 98
    .local v12, "imiList":Ljava/util/List;, "Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;"
    if-eqz v12, :cond_e

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v16

    if-nez v16, :cond_c

    goto :goto_3

    .line 102
    :cond_c
    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :goto_2
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_f

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Landroid/view/inputmethod/InputMethodInfo;

    .line 103
    .local v17, "inputMethodInfo":Landroid/view/inputmethod/InputMethodInfo;
    invoke-virtual/range {v17 .. v17}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_d

    .line 104
    invoke-virtual/range {v17 .. v17}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;

    move-result-object v14

    .line 105
    goto :goto_4

    .line 107
    .end local v17    # "inputMethodInfo":Landroid/view/inputmethod/InputMethodInfo;
    :cond_d
    const/4 v13, 0x2

    goto :goto_2

    .line 99
    :cond_e
    :goto_3
    const-string/jumbo v9, "there is no enabled method list"

    invoke-static {v4, v9}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    return v3

    .line 111
    .end local v12    # "imiList":Ljava/util/List;, "Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;"
    .end local v15    # "imi":Landroid/view/inputmethod/InputMethodInfo;
    :cond_f
    :goto_4
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_10

    .line 112
    const-string v9, "finally, we still fail to find default input method"

    invoke-static {v4, v9}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    return v3

    .line 116
    :cond_10
    iget-object v9, v0, Lcom/android/server/inputmethod/SogouInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v9}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9, v14}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_11

    .line 117
    const-string v9, "It looks like there is sogou_input_method in the system"

    invoke-static {v4, v9}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    return v3

    .line 121
    :cond_11
    invoke-static {}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->getInstance()Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    move-result-object v3

    iget-object v3, v3, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mBindingController:Lcom/android/server/inputmethod/InputMethodBindingController;

    .line 122
    invoke-virtual {v3, v14}, Lcom/android/server/inputmethod/InputMethodBindingController;->setSelectedMethodId(Ljava/lang/String;)V

    .line 123
    iget-object v3, v0, Lcom/android/server/inputmethod/SogouInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v3}, Lcom/android/server/inputmethod/InputMethodManagerService;->clearClientSessionsLocked()V

    .line 124
    iget-object v3, v0, Lcom/android/server/inputmethod/SogouInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lcom/android/server/inputmethod/InputMethodManagerService;->unbindCurrentClientLocked(I)V

    .line 125
    invoke-static {}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->getInstance()Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    move-result-object v3

    iget-object v3, v3, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mBindingController:Lcom/android/server/inputmethod/InputMethodBindingController;

    invoke-virtual {v3}, Lcom/android/server/inputmethod/InputMethodBindingController;->unbindCurrentMethod()V

    .line 126
    iget-object v3, v0, Lcom/android/server/inputmethod/SogouInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v4, v0, Lcom/android/server/inputmethod/SogouInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v4}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;

    move-result-object v4

    const/4 v9, -0x1

    invoke-virtual {v3, v4, v9}, Lcom/android/server/inputmethod/InputMethodManagerService;->setInputMethodLocked(Ljava/lang/String;I)V

    .line 127
    const/4 v3, 0x1

    return v3

    .line 129
    .end local v14    # "selectedInputMethod":Ljava/lang/String;
    :cond_12
    return v3
.end method

.method public onSwitchUserLocked(I)V
    .locals 0
    .param p1, "newUserId"    # I

    .line 29
    return-void
.end method

.method public onSystemRunningLocked()V
    .locals 0

    .line 24
    return-void
.end method

.method public removeMethod(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;",
            ">;)V"
        }
    .end annotation

    .line 144
    .local p1, "imList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;>;"
    iget-object v0, p0, Lcom/android/server/inputmethod/SogouInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v0, v0, Lcom/android/server/inputmethod/InputMethodManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/inputmethod/DeviceUtils;->isFlipTinyScreen(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 145
    return-void

    .line 147
    :cond_0
    if-eqz p1, :cond_4

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    goto :goto_1

    .line 150
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 151
    .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 152
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;

    .line 153
    .local v1, "imeSubtypeListItem":Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;
    iget-object v2, v1, Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;->mImi:Landroid/view/inputmethod/InputMethodInfo;

    .line 154
    .local v2, "imi":Landroid/view/inputmethod/InputMethodInfo;
    invoke-virtual {v2}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.sohu.inputmethod.sogou.xiaomi"

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 155
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 157
    .end local v1    # "imeSubtypeListItem":Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;
    .end local v2    # "imi":Landroid/view/inputmethod/InputMethodInfo;
    :cond_2
    goto :goto_0

    .line 158
    :cond_3
    return-void

    .line 148
    .end local v0    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;>;"
    :cond_4
    :goto_1
    return-void
.end method

.method public shouldHideImeSwitcherLocked()Z
    .locals 2

    .line 134
    iget-object v0, p0, Lcom/android/server/inputmethod/SogouInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v0, v0, Lcom/android/server/inputmethod/InputMethodManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/inputmethod/DeviceUtils;->isFlipTinyScreen(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 135
    return v1

    .line 137
    :cond_0
    iget-object v0, p0, Lcom/android/server/inputmethod/SogouInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v0}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/inputmethod/SogouInputMethodSwitcher;->isSogouMethodLocked(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 138
    invoke-static {}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->getInstance()Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    move-result-object v0

    iget-object v0, v0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mBindingController:Lcom/android/server/inputmethod/InputMethodBindingController;

    if-eqz v0, :cond_1

    .line 139
    invoke-static {}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->getInstance()Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    move-result-object v0

    iget-object v0, v0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mBindingController:Lcom/android/server/inputmethod/InputMethodBindingController;

    invoke-virtual {v0}, Lcom/android/server/inputmethod/InputMethodBindingController;->getCurMethod()Lcom/android/server/inputmethod/IInputMethodInvoker;

    move-result-object v0

    if-nez v0, :cond_1

    goto :goto_0

    :cond_1
    goto :goto_1

    :cond_2
    :goto_0
    const/4 v1, 0x1

    .line 137
    :goto_1
    return v1
.end method
