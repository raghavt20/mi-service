.class Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$MiuiInputMethodEventListener;
.super Ljava/lang/Object;
.source "InputMethodManagerServiceImpl.java"

# interfaces
.implements Lcom/miui/server/input/gesture/MiuiGestureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MiuiInputMethodEventListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;


# direct methods
.method public static synthetic $r8$lambda$2saszdD9J8z0eCwcb6tjEj8ZCCs(Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$MiuiInputMethodEventListener;Landroid/view/MotionEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$MiuiInputMethodEventListener;->lambda$onPointerEvent$0(Landroid/view/MotionEvent;)V

    return-void
.end method

.method private constructor <init>(Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;)V
    .locals 0

    .line 759
    iput-object p1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$MiuiInputMethodEventListener;->this$0:Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$MiuiInputMethodEventListener-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$MiuiInputMethodEventListener;-><init>(Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;)V

    return-void
.end method

.method private synthetic lambda$onPointerEvent$0(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 771
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDeviceId()I

    move-result v0

    if-ltz v0, :cond_1

    .line 772
    iget-object v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$MiuiInputMethodEventListener;->this$0:Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    invoke-static {v0}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->-$$Nest$fgetmIsFromRemoteDevice(Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 773
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "set mIsFromRemoteDevice as:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$MiuiInputMethodEventListener;->this$0:Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    invoke-static {v1}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->-$$Nest$fgetmIsFromRemoteDevice(Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " via Input event"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "InputMethodManagerServiceImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 776
    :cond_0
    iget-object v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$MiuiInputMethodEventListener;->this$0:Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->-$$Nest$fputmIsFromRemoteDevice(Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;Z)V

    .line 777
    iget-object v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$MiuiInputMethodEventListener;->this$0:Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    invoke-static {v0, v1}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->-$$Nest$fputmMirrorDisplayId(Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;I)V

    .line 779
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->recycle()V

    .line 780
    return-void
.end method


# virtual methods
.method public onPointerEvent(Landroid/view/MotionEvent;)V
    .locals 3
    .param p1, "motionEvent"    # Landroid/view/MotionEvent;

    .line 766
    iget-object v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$MiuiInputMethodEventListener;->this$0:Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    iget-object v0, v0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    return-void

    .line 767
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->copy()Landroid/view/MotionEvent;

    move-result-object v0

    .line 768
    .local v0, "event":Landroid/view/MotionEvent;
    iget-object v1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$MiuiInputMethodEventListener;->this$0:Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    iget-object v1, v1, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$MiuiInputMethodEventListener$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0, v0}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$MiuiInputMethodEventListener$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$MiuiInputMethodEventListener;Landroid/view/MotionEvent;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 781
    return-void
.end method
