.class Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$1;
.super Ljava/lang/Object;
.source "InputMethodManagerServiceImpl.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->generateView(ZLjava/util/List;Lmiuix/appcompat/app/AlertDialog$Builder;Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

.field final synthetic val$customziedImeListAdapter:Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;

.field final synthetic val$noCustomziedImeListAdapter:Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;


# direct methods
.method constructor <init>(Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 404
    iput-object p1, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$1;->this$0:Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    iput-object p2, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$1;->val$customziedImeListAdapter:Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;

    iput-object p3, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$1;->val$noCustomziedImeListAdapter:Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .line 407
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$1;->val$customziedImeListAdapter:Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;

    const/4 v1, -0x1

    iput v1, v0, Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;->mCheckedItem:I

    .line 408
    iget-object v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$1;->val$customziedImeListAdapter:Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;

    invoke-virtual {v0}, Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;->notifyDataSetChanged()V

    .line 409
    iget-object v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$1;->val$noCustomziedImeListAdapter:Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;

    iput p3, v0, Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;->mCheckedItem:I

    .line 410
    iget-object v0, p0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl$1;->val$noCustomziedImeListAdapter:Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;

    invoke-virtual {v0}, Lcom/android/server/inputmethod/InputMethodMenuController$ImeSubtypeListAdapter;->notifyDataSetChanged()V

    .line 411
    return-void
.end method
