.class Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper$SettingsObserver;
.super Landroid/database/ContentObserver;
.source "MiuiSecurityInputMethodHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SettingsObserver"
.end annotation


# instance fields
.field mRegistered:Z

.field mUserId:I

.field final synthetic this$0:Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;


# direct methods
.method constructor <init>(Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;Landroid/os/Handler;)V
    .locals 1
    .param p1, "this$0"    # Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 78
    iput-object p1, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper$SettingsObserver;->this$0:Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;

    .line 79
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 76
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper$SettingsObserver;->mRegistered:Z

    .line 80
    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 7
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 105
    const-string v0, "enable_miui_security_ime"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 106
    .local v0, "secIMEUri":Landroid/net/Uri;
    iget-object v1, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper$SettingsObserver;->this$0:Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;

    invoke-static {v1}, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->-$$Nest$fgetmService(Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;)Lcom/android/server/inputmethod/InputMethodManagerService;

    move-result-object v1

    iget-object v1, v1, Lcom/android/server/inputmethod/InputMethodManagerService;->mMethodMap:Landroid/util/ArrayMap;

    monitor-enter v1

    .line 107
    :try_start_0
    invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 108
    iget-object v2, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper$SettingsObserver;->this$0:Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;

    invoke-static {v2}, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->-$$Nest$fgetmService(Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;)Lcom/android/server/inputmethod/InputMethodManagerService;

    move-result-object v3

    iget-object v3, v3, Lcom/android/server/inputmethod/InputMethodManagerService;->mContext:Landroid/content/Context;

    .line 109
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "enable_miui_security_ime"

    iget-object v5, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper$SettingsObserver;->this$0:Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;

    invoke-static {v5}, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->-$$Nest$fgetmService(Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;)Lcom/android/server/inputmethod/InputMethodManagerService;

    move-result-object v5

    iget-object v5, v5, Lcom/android/server/inputmethod/InputMethodManagerService;->mSettings:Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;

    .line 110
    invoke-virtual {v5}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getCurrentUserId()I

    move-result v5

    .line 108
    const/4 v6, 0x1

    invoke-static {v3, v4, v6, v5}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_0
    const/4 v6, 0x0

    :goto_0
    invoke-static {v2, v6}, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->-$$Nest$fputmSecEnabled(Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;Z)V

    .line 111
    iget-object v2, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper$SettingsObserver;->this$0:Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;

    invoke-static {v2}, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->-$$Nest$mupdateFromSettingsLocked(Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;)V

    .line 113
    const-string v2, "MiuiSecurityInputMethodHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "enable status change: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper$SettingsObserver;->this$0:Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;

    invoke-static {v4}, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->-$$Nest$fgetmSecEnabled(Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;)Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    :cond_1
    monitor-exit v1

    .line 117
    return-void

    .line 116
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method registerContentObserverLocked(I)V
    .locals 3
    .param p1, "userId"    # I

    .line 83
    sget-boolean v0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->SUPPORT_SEC_INPUT_METHOD:Z

    if-nez v0, :cond_0

    .line 84
    return-void

    .line 86
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper$SettingsObserver;->mRegistered:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper$SettingsObserver;->mUserId:I

    if-ne v0, p1, :cond_1

    .line 87
    return-void

    .line 89
    :cond_1
    iget-object v0, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper$SettingsObserver;->this$0:Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;

    invoke-static {v0}, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->-$$Nest$fgetmService(Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;)Lcom/android/server/inputmethod/InputMethodManagerService;

    move-result-object v0

    iget-object v0, v0, Lcom/android/server/inputmethod/InputMethodManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 90
    .local v0, "resolver":Landroid/content/ContentResolver;
    iget-boolean v1, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper$SettingsObserver;->mRegistered:Z

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    .line 91
    invoke-virtual {v0, p0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 92
    iput-boolean v2, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper$SettingsObserver;->mRegistered:Z

    .line 94
    :cond_2
    iget v1, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper$SettingsObserver;->mUserId:I

    if-eq v1, p1, :cond_3

    .line 95
    iput p1, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper$SettingsObserver;->mUserId:I

    .line 97
    :cond_3
    const-string v1, "enable_miui_security_ime"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, p0, p1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 100
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper$SettingsObserver;->mRegistered:Z

    .line 101
    return-void
.end method
