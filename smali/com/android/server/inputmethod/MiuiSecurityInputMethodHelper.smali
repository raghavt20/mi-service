.class public Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;
.super Ljava/lang/Object;
.source "MiuiSecurityInputMethodHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper$SettingsObserver;
    }
.end annotation


# static fields
.field public static final DEBUG:Z = true

.field public static final MIUI_SEC_INPUT_METHOD_APP_PKG_NAME:Ljava/lang/String; = "com.miui.securityinputmethod"

.field public static final NUMBER_PASSWORD:I = 0x12

.field public static final SUPPORT_SEC_INPUT_METHOD:Z

.field public static final TAG:Ljava/lang/String; = "MiuiSecurityInputMethodHelper"

.field public static final TEXT_MASK:I = 0xfff

.field public static final TEXT_PASSWORD:I = 0x81

.field public static final TEXT_VISIBLE_PASSWORD:I = 0x91

.field public static final TEXT_WEB_PASSWORD:I = 0xe1

.field public static final WEB_EDIT_TEXT:I = 0xa0


# instance fields
.field private mSecEnabled:Z

.field private mService:Lcom/android/server/inputmethod/InputMethodManagerService;

.field private mSettingsObserver:Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper$SettingsObserver;


# direct methods
.method static bridge synthetic -$$Nest$fgetmSecEnabled(Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->mSecEnabled:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmService(Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;)Lcom/android/server/inputmethod/InputMethodManagerService;
    .locals 0

    iget-object p0, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmSecEnabled(Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->mSecEnabled:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateFromSettingsLocked(Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->updateFromSettingsLocked()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 33
    nop

    .line 34
    const-string v0, "ro.miui.has_security_keyboard"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    sget-boolean v0, Lmiui/os/Build;->IS_GLOBAL_BUILD:Z

    if-nez v0, :cond_0

    move v1, v2

    :cond_0
    sput-boolean v1, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->SUPPORT_SEC_INPUT_METHOD:Z

    .line 33
    return-void
.end method

.method public constructor <init>(Lcom/android/server/inputmethod/InputMethodManagerService;)V
    .locals 0
    .param p1, "service"    # Lcom/android/server/inputmethod/InputMethodManagerService;

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    .line 57
    return-void
.end method

.method private getSecMethodIdLocked()Ljava/lang/String;
    .locals 3

    .line 215
    iget-object v0, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v0, v0, Lcom/android/server/inputmethod/InputMethodManagerService;->mMethodMap:Landroid/util/ArrayMap;

    invoke-virtual {v0}, Landroid/util/ArrayMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 216
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Landroid/view/inputmethod/InputMethodInfo;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->isSecMethodLocked(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 217
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0

    .line 219
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Landroid/view/inputmethod/InputMethodInfo;>;"
    :cond_0
    goto :goto_0

    .line 220
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method private isEditorInDefaultImeApp(Landroid/view/inputmethod/EditorInfo;)Z
    .locals 4
    .param p1, "editor"    # Landroid/view/inputmethod/EditorInfo;

    .line 268
    iget-object v0, p1, Landroid/view/inputmethod/EditorInfo;->packageName:Ljava/lang/String;

    .line 269
    .local v0, "pkg":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v1, v1, Lcom/android/server/inputmethod/InputMethodManagerService;->mSettings:Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;

    invoke-virtual {v1}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getSelectedInputMethod()Ljava/lang/String;

    move-result-object v1

    .line 270
    .local v1, "defaultIme":Ljava/lang/String;
    const/4 v2, 0x0

    .line 271
    .local v2, "cn":Landroid/content/ComponentName;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 272
    invoke-static {v1}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v3

    move-object v2, v3

    if-eqz v3, :cond_0

    .line 273
    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    .line 271
    :goto_0
    return v3
.end method

.method private static isPasswdInputType(I)Z
    .locals 4
    .param p0, "inputType"    # I

    .line 258
    and-int/lit16 v0, p0, 0xa0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/16 v3, 0xa0

    if-ne v0, v3, :cond_1

    .line 259
    and-int/lit16 v0, p0, 0xfff

    const/16 v3, 0xe1

    if-ne v0, v3, :cond_0

    move v1, v2

    :cond_0
    return v1

    .line 261
    :cond_1
    and-int/lit16 v0, p0, 0xfff

    const/16 v3, 0x81

    if-eq v0, v3, :cond_2

    and-int/lit16 v0, p0, 0xfff

    const/16 v3, 0x91

    if-eq v0, v3, :cond_2

    and-int/lit16 v0, p0, 0xfff

    const/16 v3, 0x12

    if-ne v0, v3, :cond_3

    :cond_2
    move v1, v2

    :cond_3
    return v1
.end method

.method private isSecMethodLocked(Ljava/lang/String;)Z
    .locals 3
    .param p1, "methodId"    # Ljava/lang/String;

    .line 210
    iget-object v0, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v0, v0, Lcom/android/server/inputmethod/InputMethodManagerService;->mMethodMap:Landroid/util/ArrayMap;

    invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodInfo;

    .line 211
    .local v0, "imi":Landroid/view/inputmethod/InputMethodInfo;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.miui.securityinputmethod"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private updateFromSettingsLocked()V
    .locals 2

    .line 121
    iget-object v0, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v0}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->mSecEnabled:Z

    if-nez v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v0}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->isSecMethodLocked(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v0}, Lcom/android/server/inputmethod/InputMethodManagerService;->clearClientSessionsLocked()V

    .line 124
    iget-object v0, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/server/inputmethod/InputMethodManagerService;->unbindCurrentClientLocked(I)V

    .line 127
    :cond_0
    return-void
.end method


# virtual methods
.method filterSecMethodLocked(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Landroid/view/inputmethod/InputMethodInfo;",
            ">;)",
            "Ljava/util/ArrayList<",
            "Landroid/view/inputmethod/InputMethodInfo;",
            ">;"
        }
    .end annotation

    .line 246
    .local p1, "methodInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/inputmethod/InputMethodInfo;>;"
    if-eqz p1, :cond_1

    sget-boolean v0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->SUPPORT_SEC_INPUT_METHOD:Z

    if-eqz v0, :cond_1

    .line 247
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodInfo;

    .line 248
    .local v1, "methodInfo":Landroid/view/inputmethod/InputMethodInfo;
    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->isSecMethodLocked(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 249
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 250
    goto :goto_1

    .line 252
    .end local v1    # "methodInfo":Landroid/view/inputmethod/InputMethodInfo;
    :cond_0
    goto :goto_0

    .line 254
    :cond_1
    :goto_1
    return-object p1
.end method

.method mayChangeInputMethodLocked(Landroid/view/inputmethod/EditorInfo;)Z
    .locals 10
    .param p1, "attribute"    # Landroid/view/inputmethod/EditorInfo;

    .line 130
    sget-boolean v0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->SUPPORT_SEC_INPUT_METHOD:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 131
    return v1

    .line 133
    :cond_0
    iget-object v0, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v0}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;

    move-result-object v0

    const-string v2, "MiuiSecurityInputMethodHelper"

    if-nez v0, :cond_1

    .line 134
    const-string v0, "input_service has no current_method_id"

    invoke-static {v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    return v1

    .line 137
    :cond_1
    if-nez p1, :cond_2

    .line 138
    const-string v0, "editor_info is null, we cannot judge"

    invoke-static {v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    return v1

    .line 141
    :cond_2
    invoke-static {}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->getInstance()Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    move-result-object v0

    iget-object v0, v0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mBindingController:Lcom/android/server/inputmethod/InputMethodBindingController;

    if-nez v0, :cond_3

    .line 142
    const-string v0, "IMMS_IMPL has not init"

    invoke-static {v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    return v1

    .line 145
    :cond_3
    iget-object v0, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v0, v0, Lcom/android/server/inputmethod/InputMethodManagerService;->mMethodMap:Landroid/util/ArrayMap;

    iget-object v3, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v3}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodInfo;

    .line 146
    .local v0, "curMethodInfo":Landroid/view/inputmethod/InputMethodInfo;
    if-nez v0, :cond_4

    .line 147
    const-string v3, "fail to find current_method_info in the map"

    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    return v1

    .line 151
    :cond_4
    iget v3, p1, Landroid/view/inputmethod/EditorInfo;->inputType:I

    invoke-static {v3}, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->isPasswdInputType(I)Z

    move-result v3

    const/4 v4, 0x1

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v3}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->isSecMethodLocked(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_5

    iget-boolean v3, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->mSecEnabled:Z

    if-eqz v3, :cond_5

    .line 152
    invoke-direct {p0}, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->getSecMethodIdLocked()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    invoke-direct {p0, p1}, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->isEditorInDefaultImeApp(Landroid/view/inputmethod/EditorInfo;)Z

    move-result v3

    if-nez v3, :cond_5

    move v3, v4

    goto :goto_0

    :cond_5
    move v3, v1

    .line 153
    .local v3, "switchToSecInput":Z
    :goto_0
    iget-object v5, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v5}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->isSecMethodLocked(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7

    iget-boolean v5, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->mSecEnabled:Z

    if-eqz v5, :cond_6

    iget v5, p1, Landroid/view/inputmethod/EditorInfo;->inputType:I

    .line 154
    invoke-static {v5}, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->isPasswdInputType(I)Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-direct {p0, p1}, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->isEditorInDefaultImeApp(Landroid/view/inputmethod/EditorInfo;)Z

    move-result v5

    if-eqz v5, :cond_7

    :cond_6
    move v5, v4

    goto :goto_1

    :cond_7
    move v5, v1

    .line 156
    .local v5, "switchFromSecInput":Z
    :goto_1
    const/4 v6, -0x1

    const/4 v7, 0x2

    if-eqz v3, :cond_9

    .line 157
    invoke-direct {p0}, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->getSecMethodIdLocked()Ljava/lang/String;

    move-result-object v8

    .line 158
    .local v8, "secInputMethodId":Ljava/lang/String;
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 159
    const-string v4, "fail to find secure_input_method in input_method_list"

    invoke-static {v2, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    return v1

    .line 162
    :cond_8
    invoke-static {}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->getInstance()Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    move-result-object v1

    iget-object v1, v1, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mBindingController:Lcom/android/server/inputmethod/InputMethodBindingController;

    .line 163
    invoke-virtual {v1, v8}, Lcom/android/server/inputmethod/InputMethodBindingController;->setSelectedMethodId(Ljava/lang/String;)V

    .line 164
    iget-object v1, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v1}, Lcom/android/server/inputmethod/InputMethodManagerService;->clearClientSessionsLocked()V

    .line 165
    iget-object v1, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v1, v7}, Lcom/android/server/inputmethod/InputMethodManagerService;->unbindCurrentClientLocked(I)V

    .line 166
    invoke-static {}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->getInstance()Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    move-result-object v1

    iget-object v1, v1, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mBindingController:Lcom/android/server/inputmethod/InputMethodBindingController;

    invoke-virtual {v1}, Lcom/android/server/inputmethod/InputMethodBindingController;->unbindCurrentMethod()V

    .line 167
    iget-object v1, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v1}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v6}, Lcom/android/server/inputmethod/InputMethodManagerService;->setInputMethodLocked(Ljava/lang/String;I)V

    .line 168
    return v4

    .line 169
    .end local v8    # "secInputMethodId":Ljava/lang/String;
    :cond_9
    if-eqz v5, :cond_d

    .line 170
    iget-object v8, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v8, v8, Lcom/android/server/inputmethod/InputMethodManagerService;->mSettings:Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;

    invoke-virtual {v8}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getSelectedInputMethod()Ljava/lang/String;

    move-result-object v8

    .line 171
    .local v8, "selectedInputMethod":Ljava/lang/String;
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_a

    .line 172
    const-string/jumbo v9, "something is weired, maybe the input method app are uninstalled"

    invoke-static {v2, v9}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    :cond_a
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_b

    .line 191
    const-string v4, "finally, we still fail to find default input method"

    invoke-static {v2, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    return v1

    .line 194
    :cond_b
    iget-object v9, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v9}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9, v8}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_c

    .line 195
    const-string v4, "It looks like there is only miui_sec_input_method in the system"

    invoke-static {v2, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    return v1

    .line 198
    :cond_c
    invoke-static {}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->getInstance()Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    move-result-object v1

    iget-object v1, v1, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mBindingController:Lcom/android/server/inputmethod/InputMethodBindingController;

    .line 199
    invoke-virtual {v1, v8}, Lcom/android/server/inputmethod/InputMethodBindingController;->setSelectedMethodId(Ljava/lang/String;)V

    .line 200
    iget-object v1, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v1}, Lcom/android/server/inputmethod/InputMethodManagerService;->clearClientSessionsLocked()V

    .line 201
    iget-object v1, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v1, v7}, Lcom/android/server/inputmethod/InputMethodManagerService;->unbindCurrentClientLocked(I)V

    .line 202
    invoke-static {}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->getInstance()Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    move-result-object v1

    iget-object v1, v1, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mBindingController:Lcom/android/server/inputmethod/InputMethodBindingController;

    invoke-virtual {v1}, Lcom/android/server/inputmethod/InputMethodBindingController;->unbindCurrentMethod()V

    .line 203
    iget-object v1, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v1}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v6}, Lcom/android/server/inputmethod/InputMethodManagerService;->setInputMethodLocked(Ljava/lang/String;I)V

    .line 204
    return v4

    .line 206
    .end local v8    # "selectedInputMethod":Ljava/lang/String;
    :cond_d
    return v1
.end method

.method onSwitchUserLocked(I)V
    .locals 4
    .param p1, "newUserId"    # I

    .line 68
    iget-object v0, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->mSettingsObserver:Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper$SettingsObserver;

    invoke-virtual {v0, p1}, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper$SettingsObserver;->registerContentObserverLocked(I)V

    .line 69
    iget-object v0, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v0, v0, Lcom/android/server/inputmethod/InputMethodManagerService;->mContext:Landroid/content/Context;

    .line 70
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v1, v1, Lcom/android/server/inputmethod/InputMethodManagerService;->mSettings:Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;

    .line 71
    invoke-virtual {v1}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getCurrentUserId()I

    move-result v1

    .line 69
    const-string v2, "enable_miui_security_ime"

    const/4 v3, 0x1

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    iput-boolean v3, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->mSecEnabled:Z

    .line 72
    return-void
.end method

.method onSystemRunningLocked()V
    .locals 4

    .line 60
    new-instance v0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper$SettingsObserver;

    invoke-static {}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->getInstance()Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    move-result-object v1

    iget-object v1, v1, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper$SettingsObserver;-><init>(Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->mSettingsObserver:Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper$SettingsObserver;

    .line 61
    iget-object v1, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v1, v1, Lcom/android/server/inputmethod/InputMethodManagerService;->mSettings:Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;

    invoke-virtual {v1}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getCurrentUserId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper$SettingsObserver;->registerContentObserverLocked(I)V

    .line 62
    iget-object v0, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v0, v0, Lcom/android/server/inputmethod/InputMethodManagerService;->mContext:Landroid/content/Context;

    .line 63
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v1, v1, Lcom/android/server/inputmethod/InputMethodManagerService;->mSettings:Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;

    .line 64
    invoke-virtual {v1}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getCurrentUserId()I

    move-result v1

    .line 62
    const-string v2, "enable_miui_security_ime"

    const/4 v3, 0x1

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    iput-boolean v3, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->mSecEnabled:Z

    .line 65
    return-void
.end method

.method removeSecMethod(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;",
            ">;)V"
        }
    .end annotation

    .line 231
    .local p1, "imList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;>;"
    sget-boolean v0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->SUPPORT_SEC_INPUT_METHOD:Z

    if-nez v0, :cond_0

    .line 232
    return-void

    .line 234
    :cond_0
    if-eqz p1, :cond_2

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 235
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;

    .line 236
    .local v1, "imeSubtypeListItem":Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;
    iget-object v2, v1, Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;->mImi:Landroid/view/inputmethod/InputMethodInfo;

    .line 237
    .local v2, "imi":Landroid/view/inputmethod/InputMethodInfo;
    invoke-virtual {v2}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.miui.securityinputmethod"

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 238
    invoke-interface {p1, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 239
    return-void

    .line 241
    .end local v1    # "imeSubtypeListItem":Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;
    .end local v2    # "imi":Landroid/view/inputmethod/InputMethodInfo;
    :cond_1
    goto :goto_0

    .line 243
    :cond_2
    return-void
.end method

.method shouldHideImeSwitcherLocked()Z
    .locals 1

    .line 224
    sget-boolean v0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->SUPPORT_SEC_INPUT_METHOD:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v0}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/inputmethod/MiuiSecurityInputMethodHelper;->isSecMethodLocked(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 225
    :cond_0
    invoke-static {}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->getInstance()Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    move-result-object v0

    iget-object v0, v0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mBindingController:Lcom/android/server/inputmethod/InputMethodBindingController;

    if-eqz v0, :cond_2

    .line 226
    invoke-static {}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->getInstance()Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    move-result-object v0

    iget-object v0, v0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mBindingController:Lcom/android/server/inputmethod/InputMethodBindingController;

    invoke-virtual {v0}, Lcom/android/server/inputmethod/InputMethodBindingController;->getCurMethod()Lcom/android/server/inputmethod/IInputMethodInvoker;

    move-result-object v0

    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    .line 224
    :goto_0
    return v0
.end method
