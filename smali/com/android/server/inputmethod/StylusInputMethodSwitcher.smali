.class public Lcom/android/server/inputmethod/StylusInputMethodSwitcher;
.super Lcom/android/server/inputmethod/BaseInputMethodSwitcher;
.source "StylusInputMethodSwitcher.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/inputmethod/StylusInputMethodSwitcher$SettingsObserver;
    }
.end annotation


# static fields
.field public static final DEBUG:Z = true

.field private static final ENABLE_STYLUS_HAND_WRITING:Ljava/lang/String; = "stylus_handwriting_enable"

.field public static final HAND_WRITING_KEYBOARD_TYPE:Ljava/lang/String; = "hand_writing_keyboard_type"

.field private static final MIUI_STYLUS_INPUT_METHOD_APP_PKG_NAME:Ljava/lang/String; = "com.miui.handwriting"

.field private static final SUPPORT_STYLUS_INPUT_METHOD:Z

.field public static final TAG:Ljava/lang/String; = "StylusInputMethodSwitcher"


# instance fields
.field private mSettingsObserver:Lcom/android/server/inputmethod/StylusInputMethodSwitcher$SettingsObserver;

.field private mStylusEnabled:Z


# direct methods
.method static bridge synthetic -$$Nest$fgetmStylusEnabled(Lcom/android/server/inputmethod/StylusInputMethodSwitcher;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->mStylusEnabled:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmStylusEnabled(Lcom/android/server/inputmethod/StylusInputMethodSwitcher;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->mStylusEnabled:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateFromSettingsLocked(Lcom/android/server/inputmethod/StylusInputMethodSwitcher;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->updateFromSettingsLocked()V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetSUPPORT_STYLUS_INPUT_METHOD()Z
    .locals 1

    sget-boolean v0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->SUPPORT_STYLUS_INPUT_METHOD:Z

    return v0
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 38
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 39
    const-string/jumbo v0, "support_stylus_gesture"

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    nop

    :goto_0
    sput-boolean v1, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->SUPPORT_STYLUS_INPUT_METHOD:Z

    .line 38
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 34
    invoke-direct {p0}, Lcom/android/server/inputmethod/BaseInputMethodSwitcher;-><init>()V

    return-void
.end method

.method private getStylusMethodIdLocked()Ljava/lang/String;
    .locals 3

    .line 278
    iget-object v0, p0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v0, v0, Lcom/android/server/inputmethod/InputMethodManagerService;->mMethodMap:Landroid/util/ArrayMap;

    invoke-virtual {v0}, Landroid/util/ArrayMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 279
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Landroid/view/inputmethod/InputMethodInfo;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->isStylusMethodLocked(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 280
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0

    .line 282
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Landroid/view/inputmethod/InputMethodInfo;>;"
    :cond_0
    goto :goto_0

    .line 283
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method private isEditorInDefaultImeApp(Landroid/view/inputmethod/EditorInfo;)Z
    .locals 4
    .param p1, "editor"    # Landroid/view/inputmethod/EditorInfo;

    .line 295
    iget-object v0, p1, Landroid/view/inputmethod/EditorInfo;->packageName:Ljava/lang/String;

    .line 296
    .local v0, "pkg":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v1, v1, Lcom/android/server/inputmethod/InputMethodManagerService;->mSettings:Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;

    invoke-virtual {v1}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getSelectedInputMethod()Ljava/lang/String;

    move-result-object v1

    .line 297
    .local v1, "defaultIme":Ljava/lang/String;
    const/4 v2, 0x0

    .line 298
    .local v2, "cn":Landroid/content/ComponentName;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 299
    invoke-static {v1}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v3

    move-object v2, v3

    if-eqz v3, :cond_0

    .line 300
    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    .line 298
    :goto_0
    return v3
.end method

.method private isStylusDeviceConnected()Z
    .locals 8

    .line 166
    iget-object v0, p0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v0, v0, Lcom/android/server/inputmethod/InputMethodManagerService;->mContext:Landroid/content/Context;

    const-string v1, "input"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/input/InputManager;

    .line 167
    .local v0, "inputManager":Landroid/hardware/input/InputManager;
    invoke-virtual {v0}, Landroid/hardware/input/InputManager;->getInputDeviceIds()[I

    move-result-object v1

    .line 168
    .local v1, "inputDeviceIds":[I
    array-length v2, v1

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    if-ge v4, v2, :cond_1

    aget v5, v1, v4

    .line 169
    .local v5, "inputDeviceId":I
    invoke-virtual {v0, v5}, Landroid/hardware/input/InputManager;->getInputDevice(I)Landroid/view/InputDevice;

    move-result-object v6

    .line 170
    .local v6, "inputDevice":Landroid/view/InputDevice;
    invoke-direct {p0, v6}, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->isXiaomiStylus(Landroid/view/InputDevice;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 171
    const/4 v2, 0x1

    return v2

    .line 168
    .end local v5    # "inputDeviceId":I
    .end local v6    # "inputDevice":Landroid/view/InputDevice;
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 174
    :cond_1
    return v3
.end method

.method private isStylusMethodLocked(Ljava/lang/String;)Z
    .locals 3
    .param p1, "methodId"    # Ljava/lang/String;

    .line 273
    iget-object v0, p0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v0, v0, Lcom/android/server/inputmethod/InputMethodManagerService;->mMethodMap:Landroid/util/ArrayMap;

    invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodInfo;

    .line 274
    .local v0, "imi":Landroid/view/inputmethod/InputMethodInfo;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.miui.handwriting"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private isXiaomiStylus(Landroid/view/InputDevice;)Z
    .locals 2
    .param p1, "inputDevice"    # Landroid/view/InputDevice;

    .line 181
    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 182
    return v0

    .line 184
    :cond_0
    invoke-virtual {p1}, Landroid/view/InputDevice;->isXiaomiStylus()I

    move-result v1

    if-lez v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method private updateFromSettingsLocked()V
    .locals 2

    .line 287
    iget-object v0, p0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v0}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->mStylusEnabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    .line 288
    invoke-virtual {v0}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;

    move-result-object v0

    .line 287
    invoke-direct {p0, v0}, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->isStylusMethodLocked(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 289
    iget-object v0, p0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v0}, Lcom/android/server/inputmethod/InputMethodManagerService;->clearClientSessionsLocked()V

    .line 290
    iget-object v0, p0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/server/inputmethod/InputMethodManagerService;->unbindCurrentClientLocked(I)V

    .line 292
    :cond_0
    return-void
.end method


# virtual methods
.method public filterMethodLocked(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/view/inputmethod/InputMethodInfo;",
            ">;)",
            "Ljava/util/List<",
            "Landroid/view/inputmethod/InputMethodInfo;",
            ">;"
        }
    .end annotation

    .line 212
    .local p1, "methodInfos":Ljava/util/List;, "Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;"
    sget-boolean v0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->SUPPORT_STYLUS_INPUT_METHOD:Z

    if-nez v0, :cond_0

    .line 213
    return-object p1

    .line 215
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 216
    .local v0, "noStylusMethodInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/inputmethod/InputMethodInfo;>;"
    if-eqz p1, :cond_2

    .line 217
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/inputmethod/InputMethodInfo;

    .line 218
    .local v2, "methodInfo":Landroid/view/inputmethod/InputMethodInfo;
    invoke-virtual {v2}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->isStylusMethodLocked(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 219
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 221
    .end local v2    # "methodInfo":Landroid/view/inputmethod/InputMethodInfo;
    :cond_1
    goto :goto_0

    .line 223
    :cond_2
    return-object v0
.end method

.method public mayChangeInputMethodLocked(Landroid/view/inputmethod/EditorInfo;)Z
    .locals 18
    .param p1, "attribute"    # Landroid/view/inputmethod/EditorInfo;

    .line 71
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    sget-boolean v2, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->SUPPORT_STYLUS_INPUT_METHOD:Z

    const/4 v3, 0x0

    if-nez v2, :cond_0

    .line 72
    return v3

    .line 74
    :cond_0
    iget-object v2, v0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v2}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;

    move-result-object v2

    const-string v4, "StylusInputMethodSwitcher"

    if-nez v2, :cond_1

    .line 75
    const-string v2, "input_service has no current_method_id"

    invoke-static {v4, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    return v3

    .line 78
    :cond_1
    if-nez v1, :cond_2

    .line 79
    const-string v2, "editor_info is null, we cannot judge"

    invoke-static {v4, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    return v3

    .line 82
    :cond_2
    iget-object v2, v0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v2, v2, Lcom/android/server/inputmethod/InputMethodManagerService;->mMethodMap:Landroid/util/ArrayMap;

    iget-object v5, v0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v5}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/inputmethod/InputMethodInfo;

    .line 83
    .local v2, "curMethodInfo":Landroid/view/inputmethod/InputMethodInfo;
    if-nez v2, :cond_3

    .line 84
    const-string v5, "fail to find current_method_info in the map"

    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    return v3

    .line 87
    :cond_3
    invoke-static {}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->getInstance()Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    move-result-object v5

    iget-object v5, v5, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mBindingController:Lcom/android/server/inputmethod/InputMethodBindingController;

    if-nez v5, :cond_4

    .line 88
    const-string v5, "IMMS_IMPL has not init"

    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    return v3

    .line 91
    :cond_4
    iget v5, v1, Landroid/view/inputmethod/EditorInfo;->inputType:I

    invoke-static {v5}, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->isPasswdInputType(I)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 92
    return v3

    .line 94
    :cond_5
    iget-object v5, v1, Landroid/view/inputmethod/EditorInfo;->extras:Landroid/os/Bundle;

    .line 95
    .local v5, "extras":Landroid/os/Bundle;
    if-nez v5, :cond_6

    .line 96
    return v3

    .line 98
    :cond_6
    const-string v6, "hand_writing_keyboard_type"

    invoke-virtual {v5, v6, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v6

    .line 99
    .local v6, "realToolType":I
    iget-object v7, v0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v7}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v0, v7}, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->isStylusMethodLocked(Ljava/lang/String;)Z

    move-result v7

    const/4 v8, 0x2

    const/4 v9, 0x1

    if-nez v7, :cond_7

    .line 100
    invoke-direct/range {p0 .. p0}, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->isStylusDeviceConnected()Z

    move-result v7

    if-eqz v7, :cond_7

    iget-boolean v7, v0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->mStylusEnabled:Z

    if-eqz v7, :cond_7

    if-ne v6, v8, :cond_7

    .line 101
    invoke-direct/range {p0 .. p0}, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->getStylusMethodIdLocked()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_7

    .line 102
    invoke-direct/range {p0 .. p1}, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->isEditorInDefaultImeApp(Landroid/view/inputmethod/EditorInfo;)Z

    move-result v7

    if-nez v7, :cond_7

    move v7, v9

    goto :goto_0

    :cond_7
    move v7, v3

    .line 103
    .local v7, "switchToStylusInput":Z
    :goto_0
    iget-object v10, v0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v10}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v0, v10}, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->isStylusMethodLocked(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_9

    if-eq v6, v9, :cond_8

    iget-boolean v10, v0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->mStylusEnabled:Z

    if-eqz v10, :cond_8

    .line 104
    invoke-direct/range {p0 .. p0}, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->isStylusDeviceConnected()Z

    move-result v10

    if-nez v10, :cond_9

    .line 105
    :cond_8
    invoke-direct/range {p0 .. p0}, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->getStylusMethodIdLocked()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_9

    .line 106
    invoke-direct/range {p0 .. p1}, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->isEditorInDefaultImeApp(Landroid/view/inputmethod/EditorInfo;)Z

    move-result v10

    if-nez v10, :cond_9

    move v10, v9

    goto :goto_1

    :cond_9
    move v10, v3

    .line 107
    .local v10, "switchFromStylusInput":Z
    :goto_1
    const/4 v11, -0x1

    if-eqz v7, :cond_b

    .line 108
    invoke-direct/range {p0 .. p0}, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->getStylusMethodIdLocked()Ljava/lang/String;

    move-result-object v12

    .line 109
    .local v12, "stylusMethodId":Ljava/lang/String;
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_a

    .line 110
    const-string v8, "fail to find stylus_input_method in input_method_list"

    invoke-static {v4, v8}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    return v3

    .line 113
    :cond_a
    invoke-static {}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->getInstance()Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    move-result-object v3

    iget-object v3, v3, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mBindingController:Lcom/android/server/inputmethod/InputMethodBindingController;

    .line 114
    invoke-virtual {v3, v12}, Lcom/android/server/inputmethod/InputMethodBindingController;->setSelectedMethodId(Ljava/lang/String;)V

    .line 115
    iget-object v3, v0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v3}, Lcom/android/server/inputmethod/InputMethodManagerService;->clearClientSessionsLocked()V

    .line 116
    iget-object v3, v0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v3, v8}, Lcom/android/server/inputmethod/InputMethodManagerService;->unbindCurrentClientLocked(I)V

    .line 117
    invoke-static {}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->getInstance()Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    move-result-object v3

    iget-object v3, v3, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mBindingController:Lcom/android/server/inputmethod/InputMethodBindingController;

    invoke-virtual {v3}, Lcom/android/server/inputmethod/InputMethodBindingController;->unbindCurrentMethod()V

    .line 118
    iget-object v3, v0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v4, v0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v4}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v11}, Lcom/android/server/inputmethod/InputMethodManagerService;->setInputMethodLocked(Ljava/lang/String;I)V

    .line 119
    return v9

    .line 120
    .end local v12    # "stylusMethodId":Ljava/lang/String;
    :cond_b
    if-eqz v10, :cond_13

    .line 121
    iget-object v12, v0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v12, v12, Lcom/android/server/inputmethod/InputMethodManagerService;->mSettings:Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;

    invoke-virtual {v12}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getSelectedInputMethod()Ljava/lang/String;

    move-result-object v12

    .line 122
    .local v12, "selectedInputMethod":Ljava/lang/String;
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_10

    .line 123
    const-string/jumbo v13, "something is weired, maybe the input method app are uninstalled"

    invoke-static {v4, v13}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    iget-object v13, v0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v13, v13, Lcom/android/server/inputmethod/InputMethodManagerService;->mSettings:Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;

    .line 125
    invoke-virtual {v13}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getEnabledInputMethodListLocked()Ljava/util/ArrayList;

    move-result-object v13

    .line 124
    invoke-static {v13}, Lcom/android/server/inputmethod/InputMethodInfoUtils;->getMostApplicableDefaultIME(Ljava/util/List;)Landroid/view/inputmethod/InputMethodInfo;

    move-result-object v13

    .line 126
    .local v13, "imi":Landroid/view/inputmethod/InputMethodInfo;
    const-string v14, "com.miui.handwriting"

    if-eqz v13, :cond_c

    invoke-virtual {v13}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15, v14}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_10

    .line 127
    :cond_c
    const-string v15, "fail to find a most applicable default ime"

    invoke-static {v4, v15}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    iget-object v15, v0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v15, v15, Lcom/android/server/inputmethod/InputMethodManagerService;->mSettings:Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;

    invoke-virtual {v15}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getEnabledInputMethodListLocked()Ljava/util/ArrayList;

    move-result-object v15

    .line 129
    .local v15, "imiList":Ljava/util/List;, "Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;"
    if-eqz v15, :cond_f

    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v16

    if-nez v16, :cond_d

    goto :goto_3

    .line 133
    :cond_d
    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :goto_2
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_10

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Landroid/view/inputmethod/InputMethodInfo;

    .line 134
    .local v17, "inputMethodInfo":Landroid/view/inputmethod/InputMethodInfo;
    invoke-virtual/range {v17 .. v17}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9, v14}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_e

    .line 135
    invoke-virtual/range {v17 .. v17}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;

    move-result-object v12

    .line 136
    goto :goto_4

    .line 138
    .end local v17    # "inputMethodInfo":Landroid/view/inputmethod/InputMethodInfo;
    :cond_e
    const/4 v9, 0x1

    goto :goto_2

    .line 130
    :cond_f
    :goto_3
    const-string/jumbo v8, "there is no enabled method list"

    invoke-static {v4, v8}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    return v3

    .line 141
    .end local v13    # "imi":Landroid/view/inputmethod/InputMethodInfo;
    .end local v15    # "imiList":Ljava/util/List;, "Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;"
    :cond_10
    :goto_4
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_11

    .line 142
    const-string v8, "finally, we still fail to find default input method"

    invoke-static {v4, v8}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    return v3

    .line 145
    :cond_11
    iget-object v9, v0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v9}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9, v12}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_12

    .line 146
    const-string v8, "It looks like there is only miui_stylus_input_method in the system"

    invoke-static {v4, v8}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    return v3

    .line 149
    :cond_12
    invoke-static {}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->getInstance()Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    move-result-object v3

    iget-object v3, v3, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mBindingController:Lcom/android/server/inputmethod/InputMethodBindingController;

    .line 150
    invoke-virtual {v3, v12}, Lcom/android/server/inputmethod/InputMethodBindingController;->setSelectedMethodId(Ljava/lang/String;)V

    .line 151
    iget-object v3, v0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v3}, Lcom/android/server/inputmethod/InputMethodManagerService;->clearClientSessionsLocked()V

    .line 152
    iget-object v3, v0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v3, v8}, Lcom/android/server/inputmethod/InputMethodManagerService;->unbindCurrentClientLocked(I)V

    .line 153
    invoke-static {}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->getInstance()Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    move-result-object v3

    iget-object v3, v3, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mBindingController:Lcom/android/server/inputmethod/InputMethodBindingController;

    invoke-virtual {v3}, Lcom/android/server/inputmethod/InputMethodBindingController;->unbindCurrentMethod()V

    .line 154
    iget-object v3, v0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v4, v0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v4}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v11}, Lcom/android/server/inputmethod/InputMethodManagerService;->setInputMethodLocked(Ljava/lang/String;I)V

    .line 155
    const/4 v3, 0x1

    return v3

    .line 157
    .end local v12    # "selectedInputMethod":Ljava/lang/String;
    :cond_13
    return v3
.end method

.method public onSwitchUserLocked(I)V
    .locals 3
    .param p1, "newUserId"    # I

    .line 61
    sget-boolean v0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->SUPPORT_STYLUS_INPUT_METHOD:Z

    if-nez v0, :cond_0

    .line 62
    return-void

    .line 64
    :cond_0
    iget-object v0, p0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->mSettingsObserver:Lcom/android/server/inputmethod/StylusInputMethodSwitcher$SettingsObserver;

    invoke-virtual {v0, p1}, Lcom/android/server/inputmethod/StylusInputMethodSwitcher$SettingsObserver;->registerContentObserverLocked(I)V

    .line 65
    iget-object v0, p0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v0, v0, Lcom/android/server/inputmethod/InputMethodManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "stylus_handwriting_enable"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2, p1}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    iput-boolean v2, p0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->mStylusEnabled:Z

    .line 67
    return-void
.end method

.method public onSystemRunningLocked()V
    .locals 4

    .line 48
    sget-boolean v0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->SUPPORT_STYLUS_INPUT_METHOD:Z

    if-nez v0, :cond_0

    .line 49
    return-void

    .line 51
    :cond_0
    new-instance v0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher$SettingsObserver;

    invoke-static {}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->getInstance()Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    move-result-object v1

    iget-object v1, v1, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/android/server/inputmethod/StylusInputMethodSwitcher$SettingsObserver;-><init>(Lcom/android/server/inputmethod/StylusInputMethodSwitcher;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->mSettingsObserver:Lcom/android/server/inputmethod/StylusInputMethodSwitcher$SettingsObserver;

    .line 52
    iget-object v1, p0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v1, v1, Lcom/android/server/inputmethod/InputMethodManagerService;->mSettings:Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;

    invoke-virtual {v1}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getCurrentUserId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/server/inputmethod/StylusInputMethodSwitcher$SettingsObserver;->registerContentObserverLocked(I)V

    .line 53
    iget-object v0, p0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v0, v0, Lcom/android/server/inputmethod/InputMethodManagerService;->mContext:Landroid/content/Context;

    .line 54
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    iget-object v1, v1, Lcom/android/server/inputmethod/InputMethodManagerService;->mSettings:Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;

    .line 56
    invoke-virtual {v1}, Lcom/android/server/inputmethod/InputMethodUtils$InputMethodSettings;->getCurrentUserId()I

    move-result v1

    .line 53
    const-string/jumbo v2, "stylus_handwriting_enable"

    const/4 v3, 0x1

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    :goto_0
    iput-boolean v3, p0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->mStylusEnabled:Z

    .line 57
    return-void
.end method

.method public removeMethod(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;",
            ">;)V"
        }
    .end annotation

    .line 197
    .local p1, "imList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;>;"
    sget-boolean v0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->SUPPORT_STYLUS_INPUT_METHOD:Z

    if-eqz v0, :cond_3

    if-eqz p1, :cond_3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 200
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 201
    .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 202
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;

    .line 203
    .local v1, "imeSubtypeListItem":Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;
    iget-object v2, v1, Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;->mImi:Landroid/view/inputmethod/InputMethodInfo;

    .line 204
    .local v2, "imi":Landroid/view/inputmethod/InputMethodInfo;
    invoke-virtual {v2}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.miui.handwriting"

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 205
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 207
    .end local v1    # "imeSubtypeListItem":Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;
    .end local v2    # "imi":Landroid/view/inputmethod/InputMethodInfo;
    :cond_1
    goto :goto_0

    .line 208
    :cond_2
    return-void

    .line 198
    .end local v0    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/inputmethod/InputMethodSubtypeSwitchingController$ImeSubtypeListItem;>;"
    :cond_3
    :goto_1
    return-void
.end method

.method public shouldHideImeSwitcherLocked()Z
    .locals 1

    .line 189
    sget-boolean v0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->SUPPORT_STYLUS_INPUT_METHOD:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->mService:Lcom/android/server/inputmethod/InputMethodManagerService;

    invoke-virtual {v0}, Lcom/android/server/inputmethod/InputMethodManagerService;->getSelectedMethodIdLocked()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/inputmethod/StylusInputMethodSwitcher;->isStylusMethodLocked(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 190
    :cond_0
    invoke-static {}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->getInstance()Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    move-result-object v0

    iget-object v0, v0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mBindingController:Lcom/android/server/inputmethod/InputMethodBindingController;

    if-eqz v0, :cond_2

    .line 191
    invoke-static {}, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->getInstance()Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;

    move-result-object v0

    iget-object v0, v0, Lcom/android/server/inputmethod/InputMethodManagerServiceImpl;->mBindingController:Lcom/android/server/inputmethod/InputMethodBindingController;

    invoke-virtual {v0}, Lcom/android/server/inputmethod/InputMethodBindingController;->getCurMethod()Lcom/android/server/inputmethod/IInputMethodInvoker;

    move-result-object v0

    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    .line 189
    :goto_0
    return v0
.end method
