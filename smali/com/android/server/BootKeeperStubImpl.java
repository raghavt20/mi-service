public class com.android.server.BootKeeperStubImpl implements com.android.server.BootKeeperStub {
	 /* .source "BootKeeperStubImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/BootKeeperStubImpl$BOOTKEEPER_STRATEGY;, */
	 /* Lcom/android/server/BootKeeperStubImpl$BootKeeperOneTrack; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Boolean ENABLE_BOOTKEEPER;
private static Long INIT_PLACEHOLDER_SIZE;
private static Long MIN_BOOT_BYTES;
private static java.lang.String PLACE_HOLDER_FILE;
private static Long SPACE_FREE_EACH;
private static final java.lang.String TAG;
/* # instance fields */
private com.android.server.BootKeeperStubImpl$BOOTKEEPER_STRATEGY mBootStrategy;
private Boolean mReleaseSpaceTrigger;
/* # direct methods */
static com.android.server.BootKeeperStubImpl ( ) {
	 /* .locals 2 */
	 /* .line 36 */
	 /* nop */
	 /* .line 37 */
	 final String v0 = "persist.sys.bootkeeper.support"; // const-string v0, "persist.sys.bootkeeper.support"
	 int v1 = 1; // const/4 v1, 0x1
	 v0 = 	 android.os.SystemProperties .getBoolean ( v0,v1 );
	 com.android.server.BootKeeperStubImpl.ENABLE_BOOTKEEPER = (v0!= 0);
	 /* .line 38 */
	 /* const-wide/32 v0, 0xa00000 */
	 /* sput-wide v0, Lcom/android/server/BootKeeperStubImpl;->MIN_BOOT_BYTES:J */
	 /* .line 39 */
	 /* sput-wide v0, Lcom/android/server/BootKeeperStubImpl;->SPACE_FREE_EACH:J */
	 /* .line 40 */
	 /* const-wide/32 v0, 0x3c00000 */
	 /* sput-wide v0, Lcom/android/server/BootKeeperStubImpl;->INIT_PLACEHOLDER_SIZE:J */
	 /* .line 41 */
	 final String v0 = "/data/system/placeholder"; // const-string v0, "/data/system/placeholder"
	 return;
} // .end method
 com.android.server.BootKeeperStubImpl ( ) {
	 /* .locals 1 */
	 /* .line 51 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 44 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* iput-boolean v0, p0, Lcom/android/server/BootKeeperStubImpl;->mReleaseSpaceTrigger:Z */
	 /* .line 52 */
	 /* invoke-direct {p0}, Lcom/android/server/BootKeeperStubImpl;->initialize()V */
	 /* .line 53 */
	 return;
} // .end method
private Long getAvailableSpace ( ) {
	 /* .locals 3 */
	 /* .line 97 */
	 /* new-instance v0, Landroid/os/StatFs; */
	 android.os.Environment .getDataDirectory ( );
	 /* .line 98 */
	 (( java.io.File ) v1 ).getAbsolutePath ( ); // invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
	 /* invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V */
	 /* .line 99 */
	 /* .local v0, "statFs":Landroid/os/StatFs; */
	 (( android.os.StatFs ) v0 ).getAvailableBytes ( ); // invoke-virtual {v0}, Landroid/os/StatFs;->getAvailableBytes()J
	 /* move-result-wide v1 */
	 /* .line 100 */
	 /* .local v1, "availableSpaceBytes":J */
	 /* return-wide v1 */
} // .end method
private Long getFreeSpace ( ) {
	 /* .locals 3 */
	 /* .line 104 */
	 /* new-instance v0, Landroid/os/StatFs; */
	 android.os.Environment .getDataDirectory ( );
	 /* .line 105 */
	 (( java.io.File ) v1 ).getAbsolutePath ( ); // invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
	 /* invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V */
	 /* .line 106 */
	 /* .local v0, "statFs":Landroid/os/StatFs; */
	 (( android.os.StatFs ) v0 ).getFreeBytes ( ); // invoke-virtual {v0}, Landroid/os/StatFs;->getFreeBytes()J
	 /* move-result-wide v1 */
	 /* .line 107 */
	 /* .local v1, "freeSpaceBytes":J */
	 /* return-wide v1 */
} // .end method
private Long getTotalSpace ( ) {
	 /* .locals 3 */
	 /* .line 111 */
	 /* new-instance v0, Landroid/os/StatFs; */
	 android.os.Environment .getDataDirectory ( );
	 /* .line 112 */
	 (( java.io.File ) v1 ).getAbsolutePath ( ); // invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
	 /* invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V */
	 /* .line 113 */
	 /* .local v0, "statFs":Landroid/os/StatFs; */
	 (( android.os.StatFs ) v0 ).getTotalBytes ( ); // invoke-virtual {v0}, Landroid/os/StatFs;->getTotalBytes()J
	 /* move-result-wide v1 */
	 /* .line 114 */
	 /* .local v1, "totalBytes":J */
	 /* return-wide v1 */
} // .end method
private void initialize ( ) {
	 /* .locals 1 */
	 /* .line 57 */
	 v0 = com.android.server.BootKeeperStubImpl$BOOTKEEPER_STRATEGY.CLIP;
	 this.mBootStrategy = v0;
	 /* .line 58 */
	 return;
} // .end method
private void onBootStart ( ) {
	 /* .locals 5 */
	 /* .line 81 */
	 /* invoke-direct {p0}, Lcom/android/server/BootKeeperStubImpl;->getAvailableSpace()J */
	 /* move-result-wide v0 */
	 /* .line 82 */
	 /* .local v0, "availableSpaceBytes":J */
	 /* sget-wide v2, Lcom/android/server/BootKeeperStubImpl;->MIN_BOOT_BYTES:J */
	 /* cmp-long v2, v0, v2 */
	 int v3 = 0; // const/4 v3, 0x0
	 /* if-gez v2, :cond_1 */
	 /* .line 83 */
	 v2 = 	 /* invoke-direct {p0}, Lcom/android/server/BootKeeperStubImpl;->releaseSpace()Z */
	 final String v4 = "BootKeeper"; // const-string v4, "BootKeeper"
	 if ( v2 != null) { // if-eqz v2, :cond_0
		 /* .line 84 */
		 final String v2 = "release space success:"; // const-string v2, "release space success:"
		 android.util.Slog .d ( v4,v2 );
		 /* .line 85 */
		 int v2 = 1; // const/4 v2, 0x1
		 /* iput-boolean v2, p0, Lcom/android/server/BootKeeperStubImpl;->mReleaseSpaceTrigger:Z */
		 /* .line 87 */
	 } // :cond_0
	 /* iput-boolean v3, p0, Lcom/android/server/BootKeeperStubImpl;->mReleaseSpaceTrigger:Z */
	 /* .line 88 */
	 final String v2 = " The placeholder file does not exist, so space cannot be freed"; // const-string v2, " The placeholder file does not exist, so space cannot be freed"
	 android.util.Slog .d ( v4,v2 );
	 /* .line 91 */
} // :cond_1
/* iput-boolean v3, p0, Lcom/android/server/BootKeeperStubImpl;->mReleaseSpaceTrigger:Z */
/* .line 94 */
} // :goto_0
return;
} // .end method
private void onBootSuccess ( ) {
/* .locals 9 */
/* .line 119 */
/* new-instance v0, Ljava/io/File; */
v1 = com.android.server.BootKeeperStubImpl.PLACE_HOLDER_FILE;
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 120 */
/* .local v0, "file":Ljava/io/File; */
v1 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
/* if-nez v1, :cond_1 */
/* .line 121 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "placeholder file doesn\'t exists, now start to create"; // const-string v2, "placeholder file doesn\'t exists, now start to create"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = com.android.server.BootKeeperStubImpl.PLACE_HOLDER_FILE;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "BootKeeper"; // const-string v2, "BootKeeper"
android.util.Slog .d ( v2,v1 );
/* .line 123 */
/* invoke-direct {p0}, Lcom/android/server/BootKeeperStubImpl;->getAvailableSpace()J */
/* move-result-wide v3 */
/* .line 124 */
/* .local v3, "availableSpaceBytes":J */
/* sget-wide v5, Lcom/android/server/BootKeeperStubImpl;->INIT_PLACEHOLDER_SIZE:J */
/* cmp-long v1, v3, v5 */
/* if-gez v1, :cond_0 */
/* .line 125 */
final String v1 = "available space is not enough to create placeholder file"; // const-string v1, "available space is not enough to create placeholder file"
android.util.Slog .e ( v2,v1 );
/* .line 126 */
return;
/* .line 128 */
} // :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "start create " */
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = com.android.server.BootKeeperStubImpl.PLACE_HOLDER_FILE;
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v1 );
/* .line 131 */
try { // :try_start_0
v1 = com.android.server.BootKeeperStubImpl.PLACE_HOLDER_FILE;
/* or-int/2addr v5, v6 */
/* const/16 v6, 0x1b6 */
android.system.Os .open ( v1,v5,v6 );
/* .line 133 */
/* .local v1, "fd":Ljava/io/FileDescriptor; */
/* sget-wide v5, Lcom/android/server/BootKeeperStubImpl;->INIT_PLACEHOLDER_SIZE:J */
/* const-wide/16 v7, 0x0 */
android.system.Os .posix_fallocate ( v1,v7,v8,v5,v6 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 136 */
} // .end local v1 # "fd":Ljava/io/FileDescriptor;
/* .line 134 */
/* :catch_0 */
/* move-exception v1 */
/* .line 135 */
/* .local v1, "ex":Ljava/lang/Exception; */
/* const-string/jumbo v5, "write file fail:" */
android.util.Log .e ( v2,v5,v1 );
/* .line 137 */
} // .end local v1 # "ex":Ljava/lang/Exception;
} // :goto_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "create PlaceHolderFile size="; // const-string v5, "create PlaceHolderFile size="
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.File ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/io/File;->length()J
/* move-result-wide v5 */
(( java.lang.StringBuilder ) v1 ).append ( v5, v6 ); // invoke-virtual {v1, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v1 );
/* .line 139 */
} // .end local v3 # "availableSpaceBytes":J
/* .line 140 */
} // :cond_1
(( com.android.server.BootKeeperStubImpl ) p0 ).takeUpSpace ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/BootKeeperStubImpl;->takeUpSpace(Ljava/io/File;)V
/* .line 142 */
} // :goto_1
return;
} // .end method
private Boolean releaseSpace ( ) {
/* .locals 6 */
/* .line 146 */
/* new-instance v0, Ljava/io/File; */
v1 = com.android.server.BootKeeperStubImpl.PLACE_HOLDER_FILE;
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 147 */
/* .local v0, "file":Ljava/io/File; */
v1 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
int v2 = 0; // const/4 v2, 0x0
/* if-nez v1, :cond_0 */
/* .line 148 */
/* .line 149 */
} // :cond_0
v1 = com.android.server.BootKeeperStubImpl$1.$SwitchMap$com$android$server$BootKeeperStubImpl$BOOTKEEPER_STRATEGY;
v3 = this.mBootStrategy;
v3 = (( com.android.server.BootKeeperStubImpl$BOOTKEEPER_STRATEGY ) v3 ).ordinal ( ); // invoke-virtual {v3}, Lcom/android/server/BootKeeperStubImpl$BOOTKEEPER_STRATEGY;->ordinal()I
/* aget v1, v1, v3 */
final String v3 = "BootKeeper"; // const-string v3, "BootKeeper"
/* packed-switch v1, :pswitch_data_0 */
/* .line 172 */
final String v1 = "releaseSpace: unKnow Boot keeper strategy"; // const-string v1, "releaseSpace: unKnow Boot keeper strategy"
android.util.Slog .e ( v3,v1 );
/* .line 173 */
/* .line 165 */
/* :pswitch_0 */
/* sget-wide v4, Lcom/android/server/BootKeeperStubImpl;->SPACE_FREE_EACH:J */
/* neg-long v4, v4 */
v1 = /* invoke-direct {p0, v0, v4, v5}, Lcom/android/server/BootKeeperStubImpl;->scale(Ljava/io/File;J)Z */
/* if-nez v1, :cond_1 */
/* .line 166 */
final String v1 = "releaseSpace fail:"; // const-string v1, "releaseSpace fail:"
android.util.Slog .e ( v3,v1 );
/* .line 167 */
/* .line 169 */
} // :cond_1
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "releaseSpace: "; // const-string v2, "releaseSpace: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-wide v4, Lcom/android/server/BootKeeperStubImpl;->SPACE_FREE_EACH:J */
(( java.lang.StringBuilder ) v1 ).append ( v4, v5 ); // invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v1 );
/* .line 170 */
/* .line 151 */
/* :pswitch_1 */
(( java.io.File ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/io/File;->length()J
/* move-result-wide v1 */
/* const-wide/16 v4, 0x0 */
/* cmp-long v1, v1, v4 */
/* if-gtz v1, :cond_2 */
/* .line 152 */
final String v1 = "canot release space,placeholder size is 0"; // const-string v1, "canot release space,placeholder size is 0"
android.util.Slog .e ( v3,v1 );
/* .line 155 */
} // :cond_2
try { // :try_start_0
/* new-instance v1, Ljava/io/FileWriter; */
/* invoke-direct {v1, v0}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V */
/* .line 156 */
/* .local v1, "fileWriter":Ljava/io/FileWriter; */
final String v2 = ""; // const-string v2, ""
(( java.io.FileWriter ) v1 ).write ( v2 ); // invoke-virtual {v1, v2}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V
/* .line 157 */
(( java.io.FileWriter ) v1 ).flush ( ); // invoke-virtual {v1}, Ljava/io/FileWriter;->flush()V
/* .line 158 */
(( java.io.FileWriter ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 161 */
} // .end local v1 # "fileWriter":Ljava/io/FileWriter;
/* .line 159 */
/* :catch_0 */
/* move-exception v1 */
/* .line 160 */
/* .local v1, "e":Ljava/io/IOException; */
(( java.io.IOException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
/* .line 162 */
} // .end local v1 # "e":Ljava/io/IOException;
} // :goto_0
final String v1 = "releaseSpace: clear placeholder content"; // const-string v1, "releaseSpace: clear placeholder content"
android.util.Slog .d ( v3,v1 );
/* .line 163 */
/* nop */
/* .line 175 */
} // :goto_1
int v1 = 1; // const/4 v1, 0x1
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private Boolean scale ( java.io.File p0, Long p1 ) {
/* .locals 12 */
/* .param p1, "file" # Ljava/io/File; */
/* .param p2, "deltaSpace" # J */
/* .line 179 */
int v0 = 1; // const/4 v0, 0x1
/* .line 180 */
/* .local v0, "status":Z */
int v1 = 0; // const/4 v1, 0x0
/* .line 181 */
/* .local v1, "inChannel":Ljava/nio/channels/FileChannel; */
int v2 = 0; // const/4 v2, 0x0
/* .line 183 */
/* .local v2, "out":Ljava/io/FileOutputStream; */
try { // :try_start_0
/* new-instance v3, Ljava/io/FileOutputStream; */
int v4 = 1; // const/4 v4, 0x1
/* invoke-direct {v3, p1, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V */
/* move-object v2, v3 */
/* .line 184 */
(( java.io.FileOutputStream ) v2 ).getChannel ( ); // invoke-virtual {v2}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;
/* move-object v1, v3 */
/* .line 185 */
/* const-wide/16 v3, 0x0 */
/* cmp-long v5, p2, v3 */
/* if-gez v5, :cond_0 */
/* .line 186 */
(( java.io.File ) p1 ).length ( ); // invoke-virtual {p1}, Ljava/io/File;->length()J
/* move-result-wide v3 */
/* add-long/2addr v3, p2 */
(( java.nio.channels.FileChannel ) v1 ).truncate ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Ljava/nio/channels/FileChannel;->truncate(J)Ljava/nio/channels/FileChannel;
/* .line 189 */
} // :cond_0
/* const/16 v5, 0x1000 */
/* new-array v6, v5, [B */
/* .line 190 */
/* .local v6, "buffer":[B */
/* array-length v7, v6 */
/* int-to-long v7, v7 */
/* div-long v7, p2, v7 */
/* long-to-int v7, v7 */
/* .line 191 */
/* .local v7, "n":I */
int v8 = 0; // const/4 v8, 0x0
/* .local v8, "i":I */
} // :goto_0
int v9 = 0; // const/4 v9, 0x0
/* if-ge v8, v7, :cond_1 */
/* .line 192 */
/* array-length v10, v6 */
(( java.io.FileOutputStream ) v2 ).write ( v6, v9, v10 ); // invoke-virtual {v2, v6, v9, v10}, Ljava/io/FileOutputStream;->write([BII)V
/* .line 191 */
/* add-int/lit8 v8, v8, 0x1 */
/* .line 194 */
} // .end local v8 # "i":I
} // :cond_1
/* array-length v8, v6 */
/* mul-int/2addr v8, v7 */
/* int-to-long v10, v8 */
/* sub-long v10, p2, v10 */
/* .line 195 */
/* .local v10, "diff":J */
/* cmp-long v3, v10, v3 */
/* if-lez v3, :cond_2 */
/* .line 196 */
/* new-array v3, v5, [B */
/* .line 197 */
/* .local v3, "extraBuffer":[B */
/* array-length v4, v3 */
(( java.io.FileOutputStream ) v2 ).write ( v3, v9, v4 ); // invoke-virtual {v2, v3, v9, v4}, Ljava/io/FileOutputStream;->write([BII)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 204 */
} // .end local v3 # "extraBuffer":[B
} // .end local v6 # "buffer":[B
} // .end local v7 # "n":I
} // .end local v10 # "diff":J
} // :cond_2
} // :goto_1
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 206 */
try { // :try_start_1
(( java.io.FileOutputStream ) v2 ).flush ( ); // invoke-virtual {v2}, Ljava/io/FileOutputStream;->flush()V
/* .line 207 */
(( java.io.FileOutputStream ) v2 ).getFD ( ); // invoke-virtual {v2}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;
(( java.io.FileDescriptor ) v3 ).sync ( ); // invoke-virtual {v3}, Ljava/io/FileDescriptor;->sync()V
/* .line 208 */
(( java.nio.channels.FileChannel ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 204 */
/* :catchall_0 */
/* move-exception v3 */
/* .line 200 */
/* :catch_0 */
/* move-exception v3 */
/* .line 201 */
/* .local v3, "e":Ljava/lang/Exception; */
try { // :try_start_2
final String v4 = "BootKeeper"; // const-string v4, "BootKeeper"
final String v5 = "scale file fail"; // const-string v5, "scale file fail"
android.util.Slog .e ( v4,v5,v3 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 202 */
int v0 = 0; // const/4 v0, 0x0
/* .line 204 */
} // .end local v3 # "e":Ljava/lang/Exception;
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 206 */
try { // :try_start_3
(( java.io.FileOutputStream ) v2 ).flush ( ); // invoke-virtual {v2}, Ljava/io/FileOutputStream;->flush()V
/* .line 207 */
(( java.io.FileOutputStream ) v2 ).getFD ( ); // invoke-virtual {v2}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;
(( java.io.FileDescriptor ) v3 ).sync ( ); // invoke-virtual {v3}, Ljava/io/FileDescriptor;->sync()V
/* .line 208 */
(( java.nio.channels.FileChannel ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_1 */
/* .line 209 */
/* :catch_1 */
/* move-exception v3 */
/* .line 210 */
/* .local v3, "e":Ljava/io/IOException; */
(( java.io.IOException ) v3 ).printStackTrace ( ); // invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
/* .line 211 */
int v0 = 0; // const/4 v0, 0x0
/* .line 212 */
} // .end local v3 # "e":Ljava/io/IOException;
} // :goto_2
/* nop */
/* .line 216 */
} // :cond_3
/* .line 204 */
} // :goto_3
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 206 */
try { // :try_start_4
(( java.io.FileOutputStream ) v2 ).flush ( ); // invoke-virtual {v2}, Ljava/io/FileOutputStream;->flush()V
/* .line 207 */
(( java.io.FileOutputStream ) v2 ).getFD ( ); // invoke-virtual {v2}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;
(( java.io.FileDescriptor ) v4 ).sync ( ); // invoke-virtual {v4}, Ljava/io/FileDescriptor;->sync()V
/* .line 208 */
(( java.nio.channels.FileChannel ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_2 */
/* .line 212 */
/* .line 209 */
/* :catch_2 */
/* move-exception v4 */
/* .line 210 */
/* .local v4, "e":Ljava/io/IOException; */
(( java.io.IOException ) v4 ).printStackTrace ( ); // invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V
/* .line 211 */
int v0 = 0; // const/4 v0, 0x0
/* .line 215 */
} // .end local v4 # "e":Ljava/io/IOException;
} // :cond_4
} // :goto_4
/* throw v3 */
} // .end method
/* # virtual methods */
public void afterBoot ( android.content.Context p0 ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 71 */
/* sget-boolean v0, Lcom/android/server/BootKeeperStubImpl;->ENABLE_BOOTKEEPER:Z */
/* if-nez v0, :cond_0 */
/* .line 72 */
return;
/* .line 74 */
} // :cond_0
/* invoke-direct {p0}, Lcom/android/server/BootKeeperStubImpl;->onBootSuccess()V */
/* .line 75 */
/* iget-boolean v0, p0, Lcom/android/server/BootKeeperStubImpl;->mReleaseSpaceTrigger:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 76 */
com.android.server.BootKeeperStubImpl$BootKeeperOneTrack .reportReleaseSpaceOneTrack ( p1,v0 );
/* .line 78 */
} // :cond_1
return;
} // .end method
public void beforeBoot ( ) {
/* .locals 1 */
/* .line 62 */
/* sget-boolean v0, Lcom/android/server/BootKeeperStubImpl;->ENABLE_BOOTKEEPER:Z */
/* if-nez v0, :cond_0 */
/* .line 63 */
return;
/* .line 65 */
} // :cond_0
/* invoke-direct {p0}, Lcom/android/server/BootKeeperStubImpl;->onBootStart()V */
/* .line 67 */
return;
} // .end method
void takeUpSpace ( java.io.File p0 ) {
/* .locals 9 */
/* .param p1, "placeholder" # Ljava/io/File; */
/* .line 220 */
(( java.io.File ) p1 ).length ( ); // invoke-virtual {p1}, Ljava/io/File;->length()J
/* move-result-wide v0 */
/* .line 221 */
/* .local v0, "placeHolderSize":J */
/* sget-wide v2, Lcom/android/server/BootKeeperStubImpl;->INIT_PLACEHOLDER_SIZE:J */
/* sub-long/2addr v2, v0 */
/* .line 222 */
/* .local v2, "diff":J */
/* const-wide/16 v4, 0x0 */
/* cmp-long v4, v2, v4 */
/* if-lez v4, :cond_1 */
/* .line 223 */
/* invoke-direct {p0}, Lcom/android/server/BootKeeperStubImpl;->getAvailableSpace()J */
/* move-result-wide v4 */
/* .line 224 */
/* .local v4, "availableSpaceBytes":J */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "availableSpaceBytes:"; // const-string v7, "availableSpaceBytes:"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v4, v5 ); // invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v7 = "BootKeeper"; // const-string v7, "BootKeeper"
android.util.Slog .d ( v7,v6 );
/* .line 225 */
v6 = com.android.server.BootKeeperStubImpl$1.$SwitchMap$com$android$server$BootKeeperStubImpl$BOOTKEEPER_STRATEGY;
v8 = this.mBootStrategy;
v8 = (( com.android.server.BootKeeperStubImpl$BOOTKEEPER_STRATEGY ) v8 ).ordinal ( ); // invoke-virtual {v8}, Lcom/android/server/BootKeeperStubImpl$BOOTKEEPER_STRATEGY;->ordinal()I
/* aget v6, v6, v8 */
/* const-string/jumbo v8, "take up space:" */
/* packed-switch v6, :pswitch_data_0 */
/* .line 243 */
final String v6 = "releaseSpace: unKnow Boot keeper strategy"; // const-string v6, "releaseSpace: unKnow Boot keeper strategy"
android.util.Slog .e ( v7,v6 );
/* .line 237 */
/* :pswitch_0 */
/* cmp-long v6, v4, v2 */
/* if-lez v6, :cond_1 */
/* .line 238 */
/* invoke-direct {p0, p1, v2, v3}, Lcom/android/server/BootKeeperStubImpl;->scale(Ljava/io/File;J)Z */
/* .line 239 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v2, v3 ); // invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v7,v6 );
/* .line 227 */
/* :pswitch_1 */
/* cmp-long v6, v4, v2 */
/* if-lez v6, :cond_0 */
/* .line 228 */
/* invoke-direct {p0, p1, v2, v3}, Lcom/android/server/BootKeeperStubImpl;->scale(Ljava/io/File;J)Z */
/* .line 229 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v2, v3 ); // invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v7,v6 );
/* .line 231 */
} // :cond_0
/* invoke-direct {p0, p1, v4, v5}, Lcom/android/server/BootKeeperStubImpl;->scale(Ljava/io/File;J)Z */
/* .line 232 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v4, v5 ); // invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v7,v6 );
/* .line 235 */
/* nop */
/* .line 246 */
} // .end local v4 # "availableSpaceBytes":J
} // :cond_1
} // :goto_0
return;
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
