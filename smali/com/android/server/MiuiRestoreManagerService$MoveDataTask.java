class com.android.server.MiuiRestoreManagerService$MoveDataTask implements java.lang.Runnable {
	 /* .source "MiuiRestoreManagerService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/MiuiRestoreManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "MoveDataTask" */
} // .end annotation
/* # instance fields */
java.lang.String destPath;
Integer flag;
com.android.server.pm.Installer installer;
java.lang.String packageName;
Integer packageUserId;
java.lang.String seInfo;
com.android.server.MiuiRestoreManagerService service;
java.lang.String sourcePath;
Integer userId;
/* # direct methods */
private com.android.server.MiuiRestoreManagerService$MoveDataTask ( ) {
/* .locals 0 */
/* .param p1, "service" # Lcom/android/server/MiuiRestoreManagerService; */
/* .param p2, "installer" # Lcom/android/server/pm/Installer; */
/* .param p3, "sourcePath" # Ljava/lang/String; */
/* .param p4, "destPath" # Ljava/lang/String; */
/* .param p5, "packageName" # Ljava/lang/String; */
/* .param p6, "packageUserId" # I */
/* .param p7, "userId" # I */
/* .param p8, "seInfo" # Ljava/lang/String; */
/* .param p9, "flag" # I */
/* .line 425 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 426 */
this.service = p1;
/* .line 427 */
this.installer = p2;
/* .line 428 */
this.sourcePath = p3;
/* .line 429 */
this.destPath = p4;
/* .line 430 */
this.packageName = p5;
/* .line 431 */
/* iput p6, p0, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;->packageUserId:I */
/* .line 432 */
/* iput p7, p0, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;->userId:I */
/* .line 433 */
this.seInfo = p8;
/* .line 434 */
/* iput p9, p0, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;->flag:I */
/* .line 435 */
return;
} // .end method
 com.android.server.MiuiRestoreManagerService$MoveDataTask ( ) { //synthethic
/* .locals 0 */
/* invoke-direct/range {p0 ..p9}, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;-><init>(Lcom/android/server/MiuiRestoreManagerService;Lcom/android/server/pm/Installer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;I)V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 12 */
/* .line 439 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "execute move:dp="; // const-string v1, "execute move:dp="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.destPath;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ",pkg="; // const-string v1, ",pkg="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.packageName;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ",userId="; // const-string v2, ",userId="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;->userId:I */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ",uid="; // const-string v3, ",uid="
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;->packageUserId:I */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MiuiRestoreManagerService"; // const-string v3, "MiuiRestoreManagerService"
android.util.Slog .d ( v3,v0 );
/* .line 441 */
int v0 = 0; // const/4 v0, 0x0
/* .line 442 */
/* .local v0, "result":Z */
int v4 = 0; // const/4 v4, 0x0
/* .line 443 */
/* .local v4, "tag":I */
/* iget v5, p0, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;->flag:I */
/* and-int/lit8 v6, v5, 0x2 */
if ( v6 != null) { // if-eqz v6, :cond_0
	 /* .line 444 */
	 /* or-int/lit8 v4, v4, 0x2 */
	 /* .line 446 */
} // :cond_0
/* and-int/lit8 v5, v5, 0x4 */
if ( v5 != null) { // if-eqz v5, :cond_1
	 /* .line 447 */
	 /* or-int/lit8 v4, v4, 0x4 */
	 /* .line 452 */
} // :cond_1
try { // :try_start_0
	 com.android.server.appcacheopt.AppCacheOptimizerStub .getInstance ( );
	 v6 = this.packageName;
	 /* iget v7, p0, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;->userId:I */
	 /* .line 454 */
	 v5 = this.installer;
	 v6 = this.sourcePath;
	 v7 = this.destPath;
	 /* iget v9, p0, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;->packageUserId:I */
	 v10 = this.seInfo;
	 /* move v8, v9 */
	 /* move v11, v4 */
	 v5 = 	 /* invoke-virtual/range {v5 ..v11}, Lcom/android/server/pm/Installer;->moveData(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;I)Z */
	 /* move v0, v5 */
	 /* .line 462 */
	 com.android.server.appcacheopt.AppCacheOptimizerStub .getInstance ( );
	 v6 = this.packageName;
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 466 */
	 /* .line 464 */
	 /* :catch_0 */
	 /* move-exception v5 */
	 /* .line 465 */
	 /* .local v5, "e":Ljava/lang/Exception; */
	 final String v6 = "move data error "; // const-string v6, "move data error "
	 android.util.Slog .w ( v3,v6,v5 );
	 /* .line 468 */
} // .end local v5 # "e":Ljava/lang/Exception;
} // :goto_0
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "result="; // const-string v6, "result="
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v1 ); // invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.packageName;
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;->userId:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v1 );
/* .line 469 */
v1 = this.service;
v2 = this.packageName;
/* iget v3, p0, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;->userId:I */
if ( v0 != null) { // if-eqz v0, :cond_2
int v5 = 0; // const/4 v5, 0x0
} // :cond_2
int v5 = 1; // const/4 v5, 0x1
} // :goto_1
com.android.server.MiuiRestoreManagerService .-$$Nest$mnotifyMoveTaskEnd ( v1,v2,v3,v5 );
/* .line 470 */
return;
} // .end method
