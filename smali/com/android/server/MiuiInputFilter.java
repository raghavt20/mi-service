public class com.android.server.MiuiInputFilter extends android.view.InputFilter {
	 /* .source "MiuiInputFilter.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/MiuiInputFilter$H;, */
	 /* Lcom/android/server/MiuiInputFilter$ClickableRect;, */
	 /* Lcom/android/server/MiuiInputFilter$KeyData; */
	 /* } */
} // .end annotation
/* # static fields */
static I ENTERED_LISTEN_COMBINATION_KEYS;
private static Integer MIDDLE_KEYCODE;
static I NOT_ENTERED_LISTEN_COMBINATION_KEYS;
private static final java.lang.String PERSIST_SYS_BACKTOUCH_PROPERTY;
private static final java.lang.String PERSIST_SYS_HANDSWAP_PROPERTY;
private static Boolean isDpadDevice;
private static Float sEdgeDistance;
/* # instance fields */
private final Double MAX_COS;
private Boolean mCitTestEnabled;
private com.android.server.MiuiInputFilter$ClickableRect mClickingRect;
private android.content.Context mContext;
private com.android.server.MiuiInputFilter$H mHandler;
private Boolean mInstalled;
private java.util.List mOutsideClickableRects;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/android/server/MiuiInputFilter$ClickableRect;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List mPendingKeys;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/android/server/MiuiInputFilter$KeyData;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.ArrayList mPoints;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Landroid/graphics/PointF;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Integer mSampleDura;
private android.view.MotionEvent$PointerCoords mTempPointerCoords;
private android.view.MotionEvent$PointerProperties mTempPointerProperties;
private Boolean mWasInside;
/* # direct methods */
static com.android.server.MiuiInputFilter ( ) {
/* .locals 5 */
/* .line 30 */
final String v0 = "middle_keycode_is_dpad_center"; // const-string v0, "middle_keycode_is_dpad_center"
int v1 = 0; // const/4 v1, 0x0
v0 = miui.util.FeatureParser .getBoolean ( v0,v1 );
com.android.server.MiuiInputFilter.isDpadDevice = (v0!= 0);
/* .line 31 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x17 */
} // :cond_0
int v0 = 3; // const/4 v0, 0x3
} // :goto_0
/* .line 38 */
int v1 = 4; // const/4 v1, 0x4
/* filled-new-array {v0, v1}, [I */
/* const/16 v3, 0x52 */
/* filled-new-array {v0, v3}, [I */
/* filled-new-array {v2, v4}, [[I */
/* .line 44 */
/* filled-new-array {v0, v1}, [I */
/* filled-new-array {v0, v3}, [I */
/* filled-new-array {v1, v0}, [I */
/* filled-new-array {v3, v0}, [I */
/* filled-new-array {v2, v4, v1, v0}, [[I */
return;
} // .end method
public com.android.server.MiuiInputFilter ( ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 109 */
com.android.server.DisplayThread .get ( );
(( com.android.server.DisplayThread ) v0 ).getLooper ( ); // invoke-virtual {v0}, Lcom/android/server/DisplayThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {p0, v0}, Landroid/view/InputFilter;-><init>(Landroid/os/Looper;)V */
/* .line 35 */
/* const-wide v0, 0x3fd657184ae74487L # 0.3490658503988659 */
java.lang.Math .cos ( v0,v1 );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/MiuiInputFilter;->MAX_COS:D */
/* .line 56 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mPendingKeys = v0;
/* .line 57 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mOutsideClickableRects = v0;
/* .line 67 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mPoints = v0;
/* .line 110 */
this.mContext = p1;
/* .line 111 */
/* new-instance v0, Lcom/android/server/MiuiInputFilter$H; */
com.android.server.DisplayThread .get ( );
(( com.android.server.DisplayThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Lcom/android/server/DisplayThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/MiuiInputFilter$H;-><init>(Lcom/android/server/MiuiInputFilter;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 113 */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
(( android.content.res.Resources ) v0 ).getDisplayMetrics ( ); // invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;
/* iget v0, v0, Landroid/util/DisplayMetrics;->density:F */
/* const/high16 v1, 0x41a00000 # 20.0f */
/* mul-float/2addr v0, v1 */
/* .line 114 */
return;
} // .end method
private void changeVolumeForBackTouch ( Integer p0 ) {
/* .locals 23 */
/* .param p1, "policyFlags" # I */
/* .line 166 */
/* move-object/from16 v0, p0 */
/* move/from16 v1, p1 */
v2 = this.mPoints;
int v3 = 0; // const/4 v3, 0x0
(( java.util.ArrayList ) v2 ).get ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v2, Landroid/graphics/PointF; */
/* .line 167 */
/* .local v2, "firstP":Landroid/graphics/PointF; */
v3 = this.mPoints;
int v4 = 1; // const/4 v4, 0x1
(( java.util.ArrayList ) v3 ).get ( v4 ); // invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v3, Landroid/graphics/PointF; */
/* .line 168 */
/* .local v3, "secondP":Landroid/graphics/PointF; */
v4 = this.mPoints;
int v5 = 2; // const/4 v5, 0x2
(( java.util.ArrayList ) v4 ).get ( v5 ); // invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v4, Landroid/graphics/PointF; */
/* .line 169 */
/* .local v4, "thirdP":Landroid/graphics/PointF; */
int v5 = 0; // const/4 v5, 0x0
/* .line 170 */
/* .local v5, "volumeChange":F */
/* iget v6, v3, Landroid/graphics/PointF;->x:F */
/* iget v7, v2, Landroid/graphics/PointF;->x:F */
/* sub-float/2addr v6, v7 */
/* iget v7, v4, Landroid/graphics/PointF;->x:F */
/* iget v8, v3, Landroid/graphics/PointF;->x:F */
/* sub-float/2addr v7, v8 */
/* mul-float/2addr v6, v7 */
/* iget v7, v3, Landroid/graphics/PointF;->y:F */
/* iget v8, v2, Landroid/graphics/PointF;->y:F */
/* sub-float/2addr v7, v8 */
/* iget v8, v4, Landroid/graphics/PointF;->y:F */
/* iget v9, v3, Landroid/graphics/PointF;->y:F */
/* sub-float/2addr v8, v9 */
/* mul-float/2addr v7, v8 */
/* add-float/2addr v6, v7 */
/* float-to-double v6, v6 */
/* iget v8, v3, Landroid/graphics/PointF;->x:F */
/* iget v9, v2, Landroid/graphics/PointF;->x:F */
/* sub-float/2addr v8, v9 */
/* float-to-double v8, v8 */
/* iget v10, v3, Landroid/graphics/PointF;->y:F */
/* iget v11, v2, Landroid/graphics/PointF;->y:F */
/* sub-float/2addr v10, v11 */
/* float-to-double v10, v10 */
/* .line 171 */
java.lang.Math .hypot ( v8,v9,v10,v11 );
/* move-result-wide v8 */
/* iget v10, v4, Landroid/graphics/PointF;->x:F */
/* iget v11, v3, Landroid/graphics/PointF;->x:F */
/* sub-float/2addr v10, v11 */
/* float-to-double v10, v10 */
/* iget v12, v4, Landroid/graphics/PointF;->y:F */
/* iget v13, v3, Landroid/graphics/PointF;->y:F */
/* sub-float/2addr v12, v13 */
/* float-to-double v12, v12 */
java.lang.Math .hypot ( v10,v11,v12,v13 );
/* move-result-wide v10 */
/* mul-double/2addr v8, v10 */
/* div-double/2addr v6, v8 */
/* .line 172 */
/* .local v6, "cosTheta":D */
java.lang.Math .abs ( v6,v7 );
/* move-result-wide v8 */
/* iget-wide v10, v0, Lcom/android/server/MiuiInputFilter;->MAX_COS:D */
/* cmpg-double v8, v8, v10 */
/* if-gez v8, :cond_0 */
/* .line 173 */
/* iget v8, v3, Landroid/graphics/PointF;->x:F */
/* iget v9, v2, Landroid/graphics/PointF;->x:F */
/* sub-float/2addr v8, v9 */
/* iget v9, v4, Landroid/graphics/PointF;->y:F */
/* iget v10, v3, Landroid/graphics/PointF;->y:F */
/* sub-float/2addr v9, v10 */
/* mul-float/2addr v8, v9 */
/* iget v9, v4, Landroid/graphics/PointF;->x:F */
/* iget v10, v3, Landroid/graphics/PointF;->x:F */
/* sub-float/2addr v9, v10 */
/* iget v10, v3, Landroid/graphics/PointF;->y:F */
/* iget v11, v2, Landroid/graphics/PointF;->y:F */
/* sub-float/2addr v10, v11 */
/* mul-float/2addr v9, v10 */
/* sub-float v5, v8, v9 */
/* .line 175 */
} // :cond_0
int v8 = 0; // const/4 v8, 0x0
/* cmpl-float v9, v5, v8 */
if ( v9 != null) { // if-eqz v9, :cond_3
/* .line 176 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v18 */
/* .line 177 */
/* .local v18, "time":J */
/* new-instance v9, Landroid/view/KeyEvent; */
int v15 = 0; // const/4 v15, 0x0
/* cmpl-float v10, v5, v8 */
/* const/16 v20, 0x18 */
/* const/16 v21, 0x19 */
/* if-lez v10, :cond_1 */
/* move/from16 v16, v20 */
} // :cond_1
/* move/from16 v16, v21 */
} // :goto_0
/* const/16 v17, 0x0 */
/* move-object v10, v9 */
/* move-wide/from16 v11, v18 */
/* move-wide/from16 v13, v18 */
/* invoke-direct/range {v10 ..v17}, Landroid/view/KeyEvent;-><init>(JJIII)V */
/* .line 178 */
/* .local v9, "evt":Landroid/view/KeyEvent; */
(( com.android.server.MiuiInputFilter ) v0 ).sendInputEvent ( v9, v1 ); // invoke-virtual {v0, v9, v1}, Lcom/android/server/MiuiInputFilter;->sendInputEvent(Landroid/view/InputEvent;I)V
/* .line 179 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v18 */
/* .line 180 */
/* new-instance v22, Landroid/view/KeyEvent; */
int v15 = 1; // const/4 v15, 0x1
/* cmpl-float v8, v5, v8 */
/* if-lez v8, :cond_2 */
/* move/from16 v16, v20 */
} // :cond_2
/* move/from16 v16, v21 */
} // :goto_1
/* const/16 v17, 0x0 */
/* move-object/from16 v10, v22 */
/* move-wide/from16 v11, v18 */
/* move-wide/from16 v13, v18 */
/* invoke-direct/range {v10 ..v17}, Landroid/view/KeyEvent;-><init>(JJIII)V */
/* move-object/from16 v8, v22 */
/* .line 181 */
} // .end local v9 # "evt":Landroid/view/KeyEvent;
/* .local v8, "evt":Landroid/view/KeyEvent; */
(( com.android.server.MiuiInputFilter ) v0 ).sendInputEvent ( v8, v1 ); // invoke-virtual {v0, v8, v1}, Lcom/android/server/MiuiInputFilter;->sendInputEvent(Landroid/view/InputEvent;I)V
/* .line 183 */
} // .end local v8 # "evt":Landroid/view/KeyEvent;
} // .end local v18 # "time":J
} // :cond_3
return;
} // .end method
private com.android.server.MiuiInputFilter$ClickableRect findClickableRect ( Float p0, Float p1 ) {
/* .locals 5 */
/* .param p1, "x" # F */
/* .param p2, "y" # F */
/* .line 216 */
v0 = this.mOutsideClickableRects;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Lcom/android/server/MiuiInputFilter$ClickableRect; */
/* .line 217 */
/* .local v1, "c":Lcom/android/server/MiuiInputFilter$ClickableRect; */
v2 = this.mRect;
/* float-to-int v3, p1 */
/* float-to-int v4, p2 */
v2 = (( android.graphics.Rect ) v2 ).contains ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Landroid/graphics/Rect;->contains(II)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 218 */
/* .line 220 */
} // .end local v1 # "c":Lcom/android/server/MiuiInputFilter$ClickableRect;
} // :cond_0
/* .line 221 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
private android.view.MotionEvent$PointerCoords getTempPointerCoordsWithMinSize ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "size" # I */
/* .line 225 */
v0 = this.mTempPointerCoords;
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* array-length v0, v0 */
} // :cond_0
/* move v0, v1 */
/* .line 226 */
/* .local v0, "oldSize":I */
} // :goto_0
/* if-ge v0, p1, :cond_1 */
/* .line 227 */
v2 = this.mTempPointerCoords;
/* .line 228 */
/* .local v2, "oldTempPointerCoords":[Landroid/view/MotionEvent$PointerCoords; */
/* new-array v3, p1, [Landroid/view/MotionEvent$PointerCoords; */
this.mTempPointerCoords = v3;
/* .line 229 */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 230 */
java.lang.System .arraycopy ( v2,v1,v3,v1,v0 );
/* .line 233 */
} // .end local v2 # "oldTempPointerCoords":[Landroid/view/MotionEvent$PointerCoords;
} // :cond_1
/* move v1, v0 */
/* .local v1, "i":I */
} // :goto_1
/* if-ge v1, p1, :cond_2 */
/* .line 234 */
v2 = this.mTempPointerCoords;
/* new-instance v3, Landroid/view/MotionEvent$PointerCoords; */
/* invoke-direct {v3}, Landroid/view/MotionEvent$PointerCoords;-><init>()V */
/* aput-object v3, v2, v1 */
/* .line 233 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 236 */
} // .end local v1 # "i":I
} // :cond_2
v1 = this.mTempPointerCoords;
} // .end method
private android.view.MotionEvent$PointerProperties getTempPointerPropertiesWithMinSize ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "size" # I */
/* .line 240 */
v0 = this.mTempPointerProperties;
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* array-length v0, v0 */
} // :cond_0
/* move v0, v1 */
/* .line 241 */
/* .local v0, "oldSize":I */
} // :goto_0
/* if-ge v0, p1, :cond_1 */
/* .line 242 */
v2 = this.mTempPointerProperties;
/* .line 243 */
/* .local v2, "oldTempPointerProperties":[Landroid/view/MotionEvent$PointerProperties; */
/* new-array v3, p1, [Landroid/view/MotionEvent$PointerProperties; */
this.mTempPointerProperties = v3;
/* .line 244 */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 245 */
java.lang.System .arraycopy ( v2,v1,v3,v1,v0 );
/* .line 248 */
} // .end local v2 # "oldTempPointerProperties":[Landroid/view/MotionEvent$PointerProperties;
} // :cond_1
/* move v1, v0 */
/* .local v1, "i":I */
} // :goto_1
/* if-ge v1, p1, :cond_2 */
/* .line 249 */
v2 = this.mTempPointerProperties;
/* new-instance v3, Landroid/view/MotionEvent$PointerProperties; */
/* invoke-direct {v3}, Landroid/view/MotionEvent$PointerProperties;-><init>()V */
/* aput-object v3, v2, v1 */
/* .line 248 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 251 */
} // .end local v1 # "i":I
} // :cond_2
v1 = this.mTempPointerProperties;
} // .end method
private Boolean needDelayKey ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "isSecondKey" # Z */
/* .line 255 */
} // .end method
private synchronized void onKeyEvent ( android.view.KeyEvent p0, Integer p1 ) {
/* .locals 13 */
/* .param p1, "keyEvent" # Landroid/view/KeyEvent; */
/* .param p2, "policyFlags" # I */
/* monitor-enter p0 */
/* .line 263 */
try { // :try_start_0
v0 = (( android.view.KeyEvent ) p1 ).getKeyCode ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I
/* if-nez v0, :cond_0 */
/* .line 264 */
/* invoke-super {p0, p1, p2}, Landroid/view/InputFilter;->onInputEvent(Landroid/view/InputEvent;I)V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 265 */
/* monitor-exit p0 */
return;
/* .line 267 */
} // .end local p0 # "this":Lcom/android/server/MiuiInputFilter;
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 268 */
/* .local v0, "needDelay":Z */
int v1 = 0; // const/4 v1, 0x0
/* .line 269 */
/* .local v1, "needTrigger":Z */
int v2 = 0; // const/4 v2, 0x0
/* .line 270 */
/* .local v2, "isSecondKey":Z */
try { // :try_start_1
v3 = v3 = this.mPendingKeys;
int v4 = 0; // const/4 v4, 0x0
int v5 = 1; // const/4 v5, 0x1
/* packed-switch v3, :pswitch_data_0 */
/* goto/16 :goto_5 */
/* .line 312 */
/* :pswitch_0 */
v3 = this.mPendingKeys;
int v6 = 2; // const/4 v6, 0x2
/* check-cast v3, Lcom/android/server/MiuiInputFilter$KeyData; */
v3 = this.keyEvent;
/* .line 313 */
/* .local v3, "pendingKeyEvent":Landroid/view/KeyEvent; */
v6 = (( android.view.KeyEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I
/* if-ne v6, v5, :cond_2 */
/* .line 314 */
v6 = (( android.view.KeyEvent ) p1 ).getKeyCode ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I
v7 = (( android.view.KeyEvent ) v3 ).getKeyCode ( ); // invoke-virtual {v3}, Landroid/view/KeyEvent;->getKeyCode()I
/* if-ne v6, v7, :cond_2 */
/* .line 315 */
int v1 = 1; // const/4 v1, 0x1
/* .line 316 */
int v0 = 1; // const/4 v0, 0x1
/* .line 317 */
(( android.view.KeyEvent ) p1 ).copy ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->copy()Landroid/view/KeyEvent;
int v9 = -1; // const/4 v9, -0x1
int v10 = 1; // const/4 v10, 0x1
/* if-nez v0, :cond_1 */
/* move v11, v5 */
} // :cond_1
/* move v11, v4 */
} // :goto_0
/* move-object v6, p0 */
/* move v8, p2 */
/* invoke-virtual/range {v6 ..v11}, Lcom/android/server/MiuiInputFilter;->addPendingData(Landroid/view/KeyEvent;IIZZ)V */
/* goto/16 :goto_5 */
/* .line 319 */
} // :cond_2
(( com.android.server.MiuiInputFilter ) p0 ).flushPending ( ); // invoke-virtual {p0}, Lcom/android/server/MiuiInputFilter;->flushPending()V
/* .line 321 */
/* goto/16 :goto_5 */
/* .line 293 */
} // .end local v3 # "pendingKeyEvent":Landroid/view/KeyEvent;
/* :pswitch_1 */
v3 = this.mPendingKeys;
/* check-cast v3, Lcom/android/server/MiuiInputFilter$KeyData; */
v3 = this.keyEvent;
/* .line 294 */
/* .local v3, "firstKeyEvent":Landroid/view/KeyEvent; */
v6 = this.mPendingKeys;
/* check-cast v6, Lcom/android/server/MiuiInputFilter$KeyData; */
v6 = this.keyEvent;
/* .line 295 */
/* .local v6, "lastKeyEvent":Landroid/view/KeyEvent; */
v7 = (( android.view.KeyEvent ) v6 ).getAction ( ); // invoke-virtual {v6}, Landroid/view/KeyEvent;->getAction()I
/* if-ne v7, v5, :cond_3 */
/* .line 296 */
v7 = (( android.view.KeyEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I
/* if-nez v7, :cond_3 */
/* .line 297 */
v7 = (( android.view.KeyEvent ) p1 ).getKeyCode ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I
v7 = (( com.android.server.MiuiInputFilter ) p0 ).checkSecondKey ( v7 ); // invoke-virtual {p0, v7}, Lcom/android/server/MiuiInputFilter;->checkSecondKey(I)Z
/* move v2, v7 */
/* if-nez v7, :cond_4 */
/* .line 298 */
} // :cond_3
v7 = (( android.view.KeyEvent ) v6 ).getAction ( ); // invoke-virtual {v6}, Landroid/view/KeyEvent;->getAction()I
/* if-nez v7, :cond_7 */
/* .line 299 */
v7 = (( android.view.KeyEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I
/* if-ne v7, v5, :cond_7 */
/* .line 300 */
v7 = (( android.view.KeyEvent ) p1 ).getKeyCode ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I
v8 = (( android.view.KeyEvent ) v3 ).getKeyCode ( ); // invoke-virtual {v3}, Landroid/view/KeyEvent;->getKeyCode()I
/* if-ne v7, v8, :cond_7 */
/* .line 302 */
} // :cond_4
v7 = /* invoke-direct {p0, v2}, Lcom/android/server/MiuiInputFilter;->needDelayKey(Z)Z */
/* move v0, v7 */
/* .line 303 */
(( android.view.KeyEvent ) p1 ).copy ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->copy()Landroid/view/KeyEvent;
/* .line 304 */
v7 = (( android.view.KeyEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I
/* if-nez v7, :cond_5 */
int v7 = -1; // const/4 v7, -0x1
/* move v10, v7 */
} // :cond_5
/* move v10, v5 */
} // :goto_1
int v11 = 1; // const/4 v11, 0x1
/* if-nez v0, :cond_6 */
/* move v12, v5 */
} // :cond_6
/* move v12, v4 */
/* .line 303 */
} // :goto_2
/* move-object v7, p0 */
/* move v9, p2 */
/* invoke-virtual/range {v7 ..v12}, Lcom/android/server/MiuiInputFilter;->addPendingData(Landroid/view/KeyEvent;IIZZ)V */
/* goto/16 :goto_5 */
/* .line 306 */
} // :cond_7
(( com.android.server.MiuiInputFilter ) p0 ).flushPending ( ); // invoke-virtual {p0}, Lcom/android/server/MiuiInputFilter;->flushPending()V
/* .line 308 */
/* goto/16 :goto_5 */
/* .line 280 */
} // .end local v3 # "firstKeyEvent":Landroid/view/KeyEvent;
} // .end local v6 # "lastKeyEvent":Landroid/view/KeyEvent;
/* :pswitch_2 */
v3 = (( android.view.KeyEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I
/* if-ne v3, v5, :cond_8 */
/* .line 281 */
v3 = (( android.view.KeyEvent ) p1 ).getKeyCode ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I
v6 = this.mPendingKeys;
/* check-cast v6, Lcom/android/server/MiuiInputFilter$KeyData; */
v6 = this.keyEvent;
v6 = (( android.view.KeyEvent ) v6 ).getKeyCode ( ); // invoke-virtual {v6}, Landroid/view/KeyEvent;->getKeyCode()I
/* if-eq v3, v6, :cond_9 */
/* .line 282 */
} // :cond_8
v3 = (( android.view.KeyEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I
/* if-nez v3, :cond_b */
/* .line 283 */
v3 = (( android.view.KeyEvent ) p1 ).getKeyCode ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I
v3 = (( com.android.server.MiuiInputFilter ) p0 ).checkSecondKey ( v3 ); // invoke-virtual {p0, v3}, Lcom/android/server/MiuiInputFilter;->checkSecondKey(I)Z
/* move v2, v3 */
if ( v3 != null) { // if-eqz v3, :cond_b
/* .line 284 */
} // :cond_9
v3 = /* invoke-direct {p0, v2}, Lcom/android/server/MiuiInputFilter;->needDelayKey(Z)Z */
/* move v0, v3 */
/* .line 285 */
(( android.view.KeyEvent ) p1 ).copy ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->copy()Landroid/view/KeyEvent;
int v9 = -1; // const/4 v9, -0x1
int v10 = 0; // const/4 v10, 0x0
/* if-nez v0, :cond_a */
/* move v11, v5 */
} // :cond_a
/* move v11, v4 */
} // :goto_3
/* move-object v6, p0 */
/* move v8, p2 */
/* invoke-virtual/range {v6 ..v11}, Lcom/android/server/MiuiInputFilter;->addPendingData(Landroid/view/KeyEvent;IIZZ)V */
/* .line 287 */
} // :cond_b
(( com.android.server.MiuiInputFilter ) p0 ).flushPending ( ); // invoke-virtual {p0}, Lcom/android/server/MiuiInputFilter;->flushPending()V
/* .line 289 */
/* .line 272 */
/* :pswitch_3 */
v3 = (( android.view.KeyEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I
/* if-nez v3, :cond_d */
/* .line 273 */
v3 = (( android.view.KeyEvent ) p1 ).getKeyCode ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I
v3 = (( com.android.server.MiuiInputFilter ) p0 ).checkKeyNeedListen ( v3 ); // invoke-virtual {p0, v3}, Lcom/android/server/MiuiInputFilter;->checkKeyNeedListen(I)Z
if ( v3 != null) { // if-eqz v3, :cond_d
/* .line 274 */
v3 = /* invoke-direct {p0, v4}, Lcom/android/server/MiuiInputFilter;->needDelayKey(Z)Z */
/* move v0, v3 */
/* .line 275 */
(( android.view.KeyEvent ) p1 ).copy ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->copy()Landroid/view/KeyEvent;
int v9 = -1; // const/4 v9, -0x1
int v10 = 1; // const/4 v10, 0x1
/* if-nez v0, :cond_c */
/* move v11, v5 */
} // :cond_c
/* move v11, v4 */
} // :goto_4
/* move-object v6, p0 */
/* move v8, p2 */
/* invoke-virtual/range {v6 ..v11}, Lcom/android/server/MiuiInputFilter;->addPendingData(Landroid/view/KeyEvent;IIZZ)V */
/* .line 325 */
} // :cond_d
} // :goto_5
/* if-nez v0, :cond_e */
/* .line 326 */
/* invoke-super {p0, p1, p2}, Landroid/view/InputFilter;->onInputEvent(Landroid/view/InputEvent;I)V */
/* .line 329 */
} // :cond_e
if ( v1 != null) { // if-eqz v1, :cond_f
/* .line 330 */
(( com.android.server.MiuiInputFilter ) p0 ).triggerCombinationClick ( ); // invoke-virtual {p0}, Lcom/android/server/MiuiInputFilter;->triggerCombinationClick()V
/* .line 331 */
(( com.android.server.MiuiInputFilter ) p0 ).clearPendingList ( ); // invoke-virtual {p0}, Lcom/android/server/MiuiInputFilter;->clearPendingList()V
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 333 */
} // :cond_f
/* monitor-exit p0 */
return;
/* .line 262 */
} // .end local v0 # "needDelay":Z
} // .end local v1 # "needTrigger":Z
} // .end local v2 # "isSecondKey":Z
} // .end local p1 # "keyEvent":Landroid/view/KeyEvent;
} // .end local p2 # "policyFlags":I
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
static Float processCoordinate ( Float p0, Float p1, Float p2, Float p3 ) {
/* .locals 1 */
/* .param p0, "coordValue" # F */
/* .param p1, "offset" # F */
/* .param p2, "scale" # F */
/* .param p3, "scalePivot" # F */
/* .line 212 */
/* sub-float v0, p3, p0 */
/* mul-float/2addr v0, p2 */
/* sub-float v0, p3, v0 */
/* sub-float/2addr v0, p1 */
} // .end method
private void processMotionEventForBackTouch ( android.view.MotionEvent p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "event" # Landroid/view/MotionEvent; */
/* .param p2, "policyFlags" # I */
/* .line 146 */
v0 = (( android.view.MotionEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I
int v1 = 0; // const/4 v1, 0x0
/* packed-switch v0, :pswitch_data_0 */
/* .line 148 */
/* :pswitch_0 */
/* new-instance v0, Landroid/graphics/PointF; */
v2 = (( android.view.MotionEvent ) p1 ).getRawX ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F
v3 = (( android.view.MotionEvent ) p1 ).getRawY ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F
/* invoke-direct {v0, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V */
/* .line 149 */
/* .local v0, "curPointF":Landroid/graphics/PointF; */
/* iget v2, p0, Lcom/android/server/MiuiInputFilter;->mSampleDura:I */
/* add-int/lit8 v2, v2, 0x1 */
/* iput v2, p0, Lcom/android/server/MiuiInputFilter;->mSampleDura:I */
int v3 = 5; // const/4 v3, 0x5
/* if-lt v2, v3, :cond_0 */
/* .line 150 */
v2 = this.mPoints;
(( java.util.ArrayList ) v2 ).add ( v0 ); // invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 151 */
/* iput v1, p0, Lcom/android/server/MiuiInputFilter;->mSampleDura:I */
/* .line 153 */
} // :cond_0
v1 = this.mPoints;
v1 = (( java.util.ArrayList ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->size()I
int v2 = 3; // const/4 v2, 0x3
/* if-lt v1, v2, :cond_1 */
/* .line 154 */
/* invoke-direct {p0, p2}, Lcom/android/server/MiuiInputFilter;->changeVolumeForBackTouch(I)V */
/* .line 155 */
v1 = this.mPoints;
(( java.util.ArrayList ) v1 ).clear ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V
/* .line 159 */
} // .end local v0 # "curPointF":Landroid/graphics/PointF;
/* :pswitch_1 */
/* iput v1, p0, Lcom/android/server/MiuiInputFilter;->mSampleDura:I */
/* .line 160 */
v0 = this.mPoints;
(( java.util.ArrayList ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
/* .line 163 */
} // :cond_1
} // :goto_0
return;
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
/* # virtual methods */
public void addOutsideClickableRect ( android.graphics.Rect p0, java.lang.Runnable p1 ) {
/* .locals 2 */
/* .param p1, "rect" # Landroid/graphics/Rect; */
/* .param p2, "listener" # Ljava/lang/Runnable; */
/* .line 117 */
v0 = this.mOutsideClickableRects;
/* new-instance v1, Lcom/android/server/MiuiInputFilter$ClickableRect; */
/* invoke-direct {v1, p1, p2}, Lcom/android/server/MiuiInputFilter$ClickableRect;-><init>(Landroid/graphics/Rect;Ljava/lang/Runnable;)V */
/* .line 118 */
return;
} // .end method
synchronized void addPendingData ( android.view.KeyEvent p0, Integer p1, Integer p2, Boolean p3, Boolean p4 ) {
/* .locals 2 */
/* .param p1, "keyEvent" # Landroid/view/KeyEvent; */
/* .param p2, "policyFlags" # I */
/* .param p3, "index" # I */
/* .param p4, "delayEnhance" # Z */
/* .param p5, "isSended" # Z */
/* monitor-enter p0 */
/* .line 379 */
try { // :try_start_0
v0 = this.mHandler;
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.MiuiInputFilter$H ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/MiuiInputFilter$H;->removeMessages(I)V
/* .line 381 */
/* new-instance v0, Lcom/android/server/MiuiInputFilter$KeyData; */
/* invoke-direct {v0}, Lcom/android/server/MiuiInputFilter$KeyData;-><init>()V */
/* .line 382 */
/* .local v0, "keyData":Lcom/android/server/MiuiInputFilter$KeyData; */
this.keyEvent = p1;
/* .line 383 */
/* iput p2, v0, Lcom/android/server/MiuiInputFilter$KeyData;->policyFlags:I */
/* .line 384 */
/* iput-boolean p5, v0, Lcom/android/server/MiuiInputFilter$KeyData;->isSended:Z */
/* .line 385 */
/* if-gez p3, :cond_0 */
/* .line 386 */
v1 = this.mPendingKeys;
/* .line 388 */
} // .end local p0 # "this":Lcom/android/server/MiuiInputFilter;
} // :cond_0
v1 = this.mPendingKeys;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 390 */
} // :goto_0
/* monitor-exit p0 */
return;
/* .line 378 */
} // .end local v0 # "keyData":Lcom/android/server/MiuiInputFilter$KeyData;
} // .end local p1 # "keyEvent":Landroid/view/KeyEvent;
} // .end local p2 # "policyFlags":I
} // .end local p3 # "index":I
} // .end local p4 # "delayEnhance":Z
} // .end local p5 # "isSended":Z
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
Boolean checkKeyNeedListen ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "keyCode" # I */
/* .line 336 */
(( com.android.server.MiuiInputFilter ) p0 ).getListenCombinationKeys ( ); // invoke-virtual {p0}, Lcom/android/server/MiuiInputFilter;->getListenCombinationKeys()[[I
/* .line 337 */
/* .local v0, "listenCombinationKeys":[[I */
/* array-length v1, v0 */
/* .line 338 */
/* .local v1, "N":I */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
int v3 = 0; // const/4 v3, 0x0
/* if-ge v2, v1, :cond_1 */
/* .line 339 */
/* aget-object v4, v0, v2 */
/* aget v3, v4, v3 */
/* if-ne v3, p1, :cond_0 */
/* .line 340 */
int v3 = 1; // const/4 v3, 0x1
/* .line 338 */
} // :cond_0
/* add-int/lit8 v2, v2, 0x1 */
/* .line 343 */
} // .end local v2 # "i":I
} // :cond_1
} // .end method
Boolean checkSecondKey ( Integer p0 ) {
/* .locals 8 */
/* .param p1, "secondKeyCode" # I */
/* .line 351 */
(( com.android.server.MiuiInputFilter ) p0 ).getListenCombinationKeys ( ); // invoke-virtual {p0}, Lcom/android/server/MiuiInputFilter;->getListenCombinationKeys()[[I
/* .line 352 */
/* .local v0, "listenCombinationKeys":[[I */
v1 = this.mPendingKeys;
int v2 = 0; // const/4 v2, 0x0
/* check-cast v1, Lcom/android/server/MiuiInputFilter$KeyData; */
v1 = this.keyEvent;
v1 = (( android.view.KeyEvent ) v1 ).getKeyCode ( ); // invoke-virtual {v1}, Landroid/view/KeyEvent;->getKeyCode()I
/* .line 353 */
/* .local v1, "firstKeyCode":I */
/* array-length v3, v0 */
/* .line 354 */
/* .local v3, "N":I */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
} // :goto_0
/* if-ge v4, v3, :cond_1 */
/* .line 355 */
/* aget-object v5, v0, v4 */
/* .line 356 */
/* .local v5, "keySequence":[I */
/* aget v6, v5, v2 */
/* if-ne v6, v1, :cond_0 */
int v6 = 1; // const/4 v6, 0x1
/* aget v7, v5, v6 */
/* if-ne v7, p1, :cond_0 */
/* .line 357 */
/* .line 354 */
} // .end local v5 # "keySequence":[I
} // :cond_0
/* add-int/lit8 v4, v4, 0x1 */
/* .line 360 */
} // .end local v4 # "i":I
} // :cond_1
} // .end method
synchronized void clearPendingList ( ) {
/* .locals 2 */
/* monitor-enter p0 */
/* .line 393 */
try { // :try_start_0
v0 = this.mHandler;
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.MiuiInputFilter$H ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/MiuiInputFilter$H;->removeMessages(I)V
/* .line 394 */
v0 = this.mPendingKeys;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 395 */
/* monitor-exit p0 */
return;
/* .line 392 */
} // .end local p0 # "this":Lcom/android/server/MiuiInputFilter;
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* throw v0 */
} // .end method
synchronized void flushPending ( ) {
/* .locals 4 */
/* monitor-enter p0 */
/* .line 369 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
try { // :try_start_0
v1 = v1 = this.mPendingKeys;
/* if-ge v0, v1, :cond_1 */
/* .line 370 */
v1 = this.mPendingKeys;
/* check-cast v1, Lcom/android/server/MiuiInputFilter$KeyData; */
/* .line 371 */
/* .local v1, "keyData":Lcom/android/server/MiuiInputFilter$KeyData; */
/* iget-boolean v2, v1, Lcom/android/server/MiuiInputFilter$KeyData;->isSended:Z */
/* if-nez v2, :cond_0 */
/* .line 372 */
v2 = this.keyEvent;
v3 = this.mPendingKeys;
/* check-cast v3, Lcom/android/server/MiuiInputFilter$KeyData; */
/* iget v3, v3, Lcom/android/server/MiuiInputFilter$KeyData;->policyFlags:I */
(( com.android.server.MiuiInputFilter ) p0 ).sendInputEvent ( v2, v3 ); // invoke-virtual {p0, v2, v3}, Lcom/android/server/MiuiInputFilter;->sendInputEvent(Landroid/view/InputEvent;I)V
/* .line 369 */
} // .end local v1 # "keyData":Lcom/android/server/MiuiInputFilter$KeyData;
} // .end local p0 # "this":Lcom/android/server/MiuiInputFilter;
} // :cond_0
/* add-int/lit8 v0, v0, 0x1 */
/* .line 375 */
} // .end local v0 # "i":I
} // :cond_1
(( com.android.server.MiuiInputFilter ) p0 ).clearPendingList ( ); // invoke-virtual {p0}, Lcom/android/server/MiuiInputFilter;->clearPendingList()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 376 */
/* monitor-exit p0 */
return;
/* .line 368 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* throw v0 */
} // .end method
I getListenCombinationKeys ( ) {
/* .locals 1 */
/* .line 347 */
v0 = com.android.server.MiuiInputFilter.ENTERED_LISTEN_COMBINATION_KEYS;
} // .end method
public Boolean isInstalled ( ) {
/* .locals 1 */
/* .line 72 */
/* iget-boolean v0, p0, Lcom/android/server/MiuiInputFilter;->mInstalled:Z */
} // .end method
public void onInputEvent ( android.view.InputEvent p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "event" # Landroid/view/InputEvent; */
/* .param p2, "policyFlags" # I */
/* .line 187 */
/* instance-of v0, p1, Landroid/view/MotionEvent; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* const/16 v0, 0x1002 */
v0 = (( android.view.InputEvent ) p1 ).isFromSource ( v0 ); // invoke-virtual {p1, v0}, Landroid/view/InputEvent;->isFromSource(I)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 188 */
(( android.view.InputEvent ) p1 ).getDevice ( ); // invoke-virtual {p1}, Landroid/view/InputEvent;->getDevice()Landroid/view/InputDevice;
if ( v0 != null) { // if-eqz v0, :cond_0
(( android.view.InputEvent ) p1 ).getDevice ( ); // invoke-virtual {p1}, Landroid/view/InputEvent;->getDevice()Landroid/view/InputDevice;
(( android.view.InputDevice ) v0 ).getName ( ); // invoke-virtual {v0}, Landroid/view/InputDevice;->getName()Ljava/lang/String;
final String v1 = "backtouch"; // const-string v1, "backtouch"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/MiuiInputFilter;->mCitTestEnabled:Z */
/* if-nez v0, :cond_0 */
/* .line 189 */
/* move-object v0, p1 */
/* check-cast v0, Landroid/view/MotionEvent; */
/* invoke-direct {p0, v0, p2}, Lcom/android/server/MiuiInputFilter;->processMotionEventForBackTouch(Landroid/view/MotionEvent;I)V */
/* .line 190 */
return;
/* .line 192 */
} // :cond_0
/* move-object v0, p1 */
/* check-cast v0, Landroid/view/MotionEvent; */
/* invoke-super {p0, v0, p2}, Landroid/view/InputFilter;->onInputEvent(Landroid/view/InputEvent;I)V */
/* .line 193 */
return;
/* .line 195 */
} // :cond_1
/* invoke-super {p0, p1, p2}, Landroid/view/InputFilter;->onInputEvent(Landroid/view/InputEvent;I)V */
/* .line 196 */
return;
} // .end method
public void onInstalled ( ) {
/* .locals 1 */
/* .line 200 */
/* invoke-super {p0}, Landroid/view/InputFilter;->onInstalled()V */
/* .line 201 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/MiuiInputFilter;->mInstalled:Z */
/* .line 202 */
return;
} // .end method
public void onUninstalled ( ) {
/* .locals 1 */
/* .line 206 */
/* invoke-super {p0}, Landroid/view/InputFilter;->onUninstalled()V */
/* .line 207 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/MiuiInputFilter;->mInstalled:Z */
/* .line 208 */
(( com.android.server.MiuiInputFilter ) p0 ).clearPendingList ( ); // invoke-virtual {p0}, Lcom/android/server/MiuiInputFilter;->clearPendingList()V
/* .line 209 */
return;
} // .end method
public void removeOutsideClickableRect ( java.lang.Runnable p0 ) {
/* .locals 2 */
/* .param p1, "listener" # Ljava/lang/Runnable; */
/* .line 121 */
v0 = v0 = this.mOutsideClickableRects;
/* add-int/lit8 v0, v0, -0x1 */
/* .local v0, "i":I */
} // :goto_0
/* if-ltz v0, :cond_1 */
/* .line 122 */
v1 = this.mOutsideClickableRects;
/* check-cast v1, Lcom/android/server/MiuiInputFilter$ClickableRect; */
v1 = this.mClickListener;
/* if-ne v1, p1, :cond_0 */
/* .line 123 */
v1 = this.mOutsideClickableRects;
/* .line 121 */
} // :cond_0
/* add-int/lit8 v0, v0, -0x1 */
/* .line 126 */
} // .end local v0 # "i":I
} // :cond_1
return;
} // .end method
public void setCitTestEnabled ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "enabled" # Z */
/* .line 142 */
/* iput-boolean p1, p0, Lcom/android/server/MiuiInputFilter;->mCitTestEnabled:Z */
/* .line 143 */
return;
} // .end method
synchronized void triggerCombinationClick ( ) {
/* .locals 2 */
/* monitor-enter p0 */
/* .line 364 */
try { // :try_start_0
final String v0 = "persist.sys.handswap"; // const-string v0, "persist.sys.handswap"
final String v1 = "0"; // const-string v1, "0"
android.os.SystemProperties .get ( v0,v1 );
/* .line 365 */
/* .local v0, "handswap":Ljava/lang/String; */
final String v1 = "1"; // const-string v1, "1"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 366 */
/* .local v1, "keyDirectionExchanged":Z */
/* monitor-exit p0 */
return;
/* .line 363 */
} // .end local v0 # "handswap":Ljava/lang/String;
} // .end local v1 # "keyDirectionExchanged":Z
} // .end local p0 # "this":Lcom/android/server/MiuiInputFilter;
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* throw v0 */
} // .end method
public void updateOutsideClickableRect ( android.graphics.Rect p0, java.lang.Runnable p1 ) {
/* .locals 3 */
/* .param p1, "rect" # Landroid/graphics/Rect; */
/* .param p2, "listener" # Ljava/lang/Runnable; */
/* .line 129 */
int v0 = 0; // const/4 v0, 0x0
/* .line 130 */
/* .local v0, "containListener":Z */
v1 = v1 = this.mOutsideClickableRects;
/* add-int/lit8 v1, v1, -0x1 */
/* .local v1, "i":I */
} // :goto_0
/* if-ltz v1, :cond_1 */
/* .line 131 */
v2 = this.mOutsideClickableRects;
/* check-cast v2, Lcom/android/server/MiuiInputFilter$ClickableRect; */
v2 = this.mClickListener;
/* if-ne v2, p2, :cond_0 */
/* .line 132 */
v2 = this.mOutsideClickableRects;
/* .line 133 */
int v0 = 1; // const/4 v0, 0x1
/* .line 130 */
} // :cond_0
/* add-int/lit8 v1, v1, -0x1 */
/* .line 136 */
} // .end local v1 # "i":I
} // :cond_1
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 137 */
v1 = this.mOutsideClickableRects;
/* new-instance v2, Lcom/android/server/MiuiInputFilter$ClickableRect; */
/* invoke-direct {v2, p1, p2}, Lcom/android/server/MiuiInputFilter$ClickableRect;-><init>(Landroid/graphics/Rect;Ljava/lang/Runnable;)V */
/* .line 139 */
} // :cond_2
return;
} // .end method
