class com.android.server.MiuiBatteryServiceImpl$1 extends android.content.BroadcastReceiver {
	 /* .source "MiuiBatteryServiceImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/MiuiBatteryServiceImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.MiuiBatteryServiceImpl this$0; //synthetic
/* # direct methods */
 com.android.server.MiuiBatteryServiceImpl$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/MiuiBatteryServiceImpl; */
/* .line 139 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 6 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 142 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 143 */
/* .local v0, "action":Ljava/lang/String; */
v1 = (( java.lang.String ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/String;->hashCode()I
int v2 = 0; // const/4 v2, 0x0
int v3 = -1; // const/4 v3, -0x1
/* packed-switch v1, :pswitch_data_0 */
} // :cond_0
/* :pswitch_0 */
final String v1 = "android.intent.action.BATTERY_CHANGED"; // const-string v1, "android.intent.action.BATTERY_CHANGED"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* move v1, v2 */
} // :goto_0
/* move v1, v3 */
} // :goto_1
/* packed-switch v1, :pswitch_data_1 */
/* .line 145 */
/* :pswitch_1 */
/* const-string/jumbo v1, "status" */
v1 = (( android.content.Intent ) p2 ).getIntExtra ( v1, v3 ); // invoke-virtual {p2, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
/* .line 146 */
/* .local v1, "status":I */
v3 = this.this$0;
int v4 = 3; // const/4 v4, 0x3
/* if-ne v1, v4, :cond_1 */
int v2 = 1; // const/4 v2, 0x1
} // :cond_1
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fputisDisCharging ( v3,v2 );
/* .line 147 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "mChargingStateReceiver:isDisCharging = "; // const-string v3, "mChargingStateReceiver:isDisCharging = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.this$0;
v3 = com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetisDisCharging ( v3 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MiuiBatteryServiceImpl"; // const-string v3, "MiuiBatteryServiceImpl"
android.util.Slog .d ( v3,v2 );
/* .line 148 */
v2 = this.this$0;
v2 = com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetisDisCharging ( v2 );
if ( v2 != null) { // if-eqz v2, :cond_2
v2 = this.this$0;
v2 = com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmBtConnectState ( v2 );
/* if-nez v2, :cond_2 */
v2 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmHandler ( v2 );
v3 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetshutDownRnuable ( v3 );
/* .line 149 */
v2 = (( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) v2 ).hasCallbacks ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->hasCallbacks(Ljava/lang/Runnable;)Z
/* if-nez v2, :cond_2 */
/* .line 150 */
v2 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetwakeLock ( v2 );
/* const-wide/32 v3, 0x57e40 */
(( android.os.PowerManager$WakeLock ) v2 ).acquire ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Landroid/os/PowerManager$WakeLock;->acquire(J)V
/* .line 151 */
v2 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmHandler ( v2 );
v3 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetshutDownRnuable ( v3 );
/* const-wide/32 v4, 0x493e0 */
(( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) v2 ).postDelayed ( v3, v4, v5 ); // invoke-virtual {v2, v3, v4, v5}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 155 */
} // .end local v1 # "status":I
} // :cond_2
} // :goto_2
return;
/* :pswitch_data_0 */
/* .packed-switch -0x5bb23923 */
/* :pswitch_0 */
} // .end packed-switch
/* :pswitch_data_1 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
