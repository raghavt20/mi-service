.class public final Lcom/android/server/MiuiRestoreManagerService$Lifecycle;
.super Lcom/android/server/SystemService;
.source "MiuiRestoreManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/MiuiRestoreManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Lifecycle"
.end annotation


# instance fields
.field private final mService:Lcom/android/server/MiuiRestoreManagerService;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/server/pm/Installer;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "installer"    # Lcom/android/server/pm/Installer;

    .line 89
    invoke-direct {p0, p1}, Lcom/android/server/SystemService;-><init>(Landroid/content/Context;)V

    .line 90
    new-instance v0, Lcom/android/server/MiuiRestoreManagerService;

    invoke-direct {v0, p1, p2}, Lcom/android/server/MiuiRestoreManagerService;-><init>(Landroid/content/Context;Lcom/android/server/pm/Installer;)V

    iput-object v0, p0, Lcom/android/server/MiuiRestoreManagerService$Lifecycle;->mService:Lcom/android/server/MiuiRestoreManagerService;

    .line 91
    return-void
.end method


# virtual methods
.method public onStart()V
    .locals 2

    .line 95
    const-string v0, "miui.restore.service"

    iget-object v1, p0, Lcom/android/server/MiuiRestoreManagerService$Lifecycle;->mService:Lcom/android/server/MiuiRestoreManagerService;

    invoke-virtual {p0, v0, v1}, Lcom/android/server/MiuiRestoreManagerService$Lifecycle;->publishBinderService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 96
    return-void
.end method
