.class public final Lcom/android/server/PsiParser;
.super Ljava/lang/Object;
.source "PsiParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/PsiParser$ResourceType;,
        Lcom/android/server/PsiParser$Psi;,
        Lcom/android/server/PsiParser$Prop;
    }
.end annotation


# static fields
.field private static final PSI_FILES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final PSI_ROOT:Ljava/lang/String; = "/proc/pressure"

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 15
    const-class v0, Lcom/android/server/PsiParser;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/PsiParser;->TAG:Ljava/lang/String;

    .line 16
    const-string v0, "/proc/pressure/cpu"

    const-string v1, "/proc/pressure/io"

    const-string v2, "/proc/pressure/memory"

    filled-new-array {v2, v0, v1}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/android/server/PsiParser;->PSI_FILES:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static currentParsedPsiState(Lcom/android/server/PsiParser$ResourceType;)Lcom/android/server/PsiParser$Psi;
    .locals 2
    .param p0, "type"    # Lcom/android/server/PsiParser$ResourceType;

    .line 32
    invoke-static {p0}, Lcom/android/server/PsiParser;->currentRawPsiString(Lcom/android/server/PsiParser$ResourceType;)Ljava/lang/String;

    move-result-object v0

    .line 33
    .local v0, "raw":Ljava/lang/String;
    invoke-static {v0}, Lcom/android/server/PsiParser$Psi;->parseFromRawString(Ljava/lang/String;)Lcom/android/server/PsiParser$Psi;

    move-result-object v1

    .line 34
    .local v1, "psi":Lcom/android/server/PsiParser$Psi;
    return-object v1
.end method

.method public static currentRawPsiString(Lcom/android/server/PsiParser$ResourceType;)Ljava/lang/String;
    .locals 6
    .param p0, "type"    # Lcom/android/server/PsiParser$ResourceType;

    .line 41
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskReads()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v0

    .line 42
    .local v0, "savedPolicy":Landroid/os/StrictMode$ThreadPolicy;
    sget-object v1, Lcom/android/server/PsiParser;->PSI_FILES:Ljava/util/List;

    invoke-virtual {p0}, Lcom/android/server/PsiParser$ResourceType;->ordinal()I

    move-result v2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 44
    .local v1, "filepath":Ljava/lang/String;
    :try_start_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 45
    invoke-static {v1}, Llibcore/io/IoUtils;->readFileAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 50
    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 45
    return-object v2

    .line 50
    :cond_0
    nop

    :goto_0
    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 51
    goto :goto_1

    .line 50
    :catchall_0
    move-exception v2

    goto :goto_2

    .line 47
    :catch_0
    move-exception v2

    .line 48
    .local v2, "e":Ljava/io/IOException;
    :try_start_1
    sget-object v3, Lcom/android/server/PsiParser;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Could not read "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 50
    nop

    .end local v2    # "e":Ljava/io/IOException;
    goto :goto_0

    .line 52
    :goto_1
    const-string v2, ""

    return-object v2

    .line 50
    :goto_2
    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 51
    throw v2
.end method
