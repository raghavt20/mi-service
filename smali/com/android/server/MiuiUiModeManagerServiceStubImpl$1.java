class com.android.server.MiuiUiModeManagerServiceStubImpl$1 implements android.hardware.display.DisplayManager$DisplayListener {
	 /* .source "MiuiUiModeManagerServiceStubImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/MiuiUiModeManagerServiceStubImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.MiuiUiModeManagerServiceStubImpl this$0; //synthetic
/* # direct methods */
 com.android.server.MiuiUiModeManagerServiceStubImpl$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/MiuiUiModeManagerServiceStubImpl; */
/* .line 63 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onDisplayAdded ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "displayId" # I */
/* .line 66 */
return;
} // .end method
public void onDisplayChanged ( Integer p0 ) {
/* .locals 6 */
/* .param p1, "displayId" # I */
/* .line 75 */
v0 = this.this$0;
com.android.server.MiuiUiModeManagerServiceStubImpl .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getDisplay ( ); // invoke-virtual {v0}, Landroid/content/Context;->getDisplay()Landroid/view/Display;
(( android.view.Display ) v0 ).getBrightnessInfo ( ); // invoke-virtual {v0}, Landroid/view/Display;->getBrightnessInfo()Landroid/hardware/display/BrightnessInfo;
/* .line 76 */
/* .local v0, "info":Landroid/hardware/display/BrightnessInfo; */
/* if-nez v0, :cond_0 */
/* .line 77 */
return;
/* .line 79 */
} // :cond_0
/* iget v1, v0, Landroid/hardware/display/BrightnessInfo;->brightnessMaximum:F */
/* .line 80 */
/* .local v1, "mMaximumBrightness":F */
/* iget v2, v0, Landroid/hardware/display/BrightnessInfo;->brightnessMinimum:F */
/* .line 81 */
/* .local v2, "mMinimumBrightness":F */
/* iget v3, v0, Landroid/hardware/display/BrightnessInfo;->brightness:F */
/* .line 82 */
/* .local v3, "mBrightnessValue":F */
v4 = this.this$0;
com.android.server.MiuiUiModeManagerServiceStubImpl .-$$Nest$fgetmContext ( v4 );
com.android.server.MiuiUiModeManagerServiceStubImpl .-$$Nest$mupdateAlpha ( v4,v5,v3,v1,v2 );
/* .line 83 */
return;
} // .end method
public void onDisplayRemoved ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "displayId" # I */
/* .line 70 */
return;
} // .end method
