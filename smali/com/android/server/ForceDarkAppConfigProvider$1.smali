.class Lcom/android/server/ForceDarkAppConfigProvider$1;
.super Landroid/database/ContentObserver;
.source "ForceDarkAppConfigProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/ForceDarkAppConfigProvider;->registerDataObserver(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/ForceDarkAppConfigProvider;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/android/server/ForceDarkAppConfigProvider;Landroid/os/Handler;Landroid/content/Context;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/ForceDarkAppConfigProvider;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 143
    iput-object p1, p0, Lcom/android/server/ForceDarkAppConfigProvider$1;->this$0:Lcom/android/server/ForceDarkAppConfigProvider;

    iput-object p3, p0, Lcom/android/server/ForceDarkAppConfigProvider$1;->val$context:Landroid/content/Context;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 2
    .param p1, "selfChange"    # Z

    .line 146
    invoke-static {}, Lcom/android/server/ForceDarkAppConfigProvider;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v0

    const-string v1, "forceDarkCouldData onChange--"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    iget-object v0, p0, Lcom/android/server/ForceDarkAppConfigProvider$1;->this$0:Lcom/android/server/ForceDarkAppConfigProvider;

    iget-object v1, p0, Lcom/android/server/ForceDarkAppConfigProvider$1;->val$context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/android/server/ForceDarkAppConfigProvider;->updateLocalForceDarkAppConfig(Landroid/content/Context;)V

    .line 148
    return-void
.end method
