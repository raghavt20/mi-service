.class public Lcom/android/server/AppOpsServiceStubImpl;
.super Lcom/android/server/appop/AppOpsServiceStub;
.source "AppOpsServiceStubImpl.java"

# interfaces
.implements Lcom/miui/app/AppOpsServiceInternal;


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.appop.AppOpsServiceStub$$"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/AppOpsServiceStubImpl$UserState;,
        Lcom/android/server/AppOpsServiceStubImpl$Callback;
    }
.end annotation


# static fields
.field public static final DEBUG:Z = false

.field private static final KEY_APP_BEHAVIOR_RECORD_ENABLE:Ljava/lang/String; = "key_app_behavior_record_enable"

.field private static final MESSAGE_NOT_RECORD:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final MESSAGE_NO_ASK:Ljava/lang/String; = "noteNoAsk"

.field private static final MESSAGE_NO_ASK_NO_RECORD:Ljava/lang/String; = "NoAskNoRecord"

.field private static final PLATFORM_PERMISSIONS:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "AppOpsServiceStubImpl"

.field private static final WIFI_MESSAGE_STARTWITH:Ljava/lang/String; = "WifiPermissionsUtil#noteAppOpAllowed"

.field private static final sCtsIgnore:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final supportVirtualGrant:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mActivityManagerInternal:Landroid/app/ActivityManagerInternal;

.field private mAppOpsService:Lcom/android/server/appop/AppOpsService;

.field private mContext:Landroid/content/Context;

.field private mPackageManagerInternal:Landroid/content/pm/PackageManagerInternal;

.field private mRecordEnable:Z

.field private mSecurityManagerInternal:Lmiui/security/SecurityManagerInternal;

.field final mUidStates:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/android/server/AppOpsServiceStubImpl$UserState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static synthetic $r8$lambda$GbgRas45nSDKpjmp1ouQqx7_qXg(Lcom/android/server/AppOpsServiceStubImpl;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/AppOpsServiceStubImpl;->lambda$startService$0(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mstartService(Lcom/android/server/AppOpsServiceStubImpl;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/AppOpsServiceStubImpl;->startService(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateRecordState(Lcom/android/server/AppOpsServiceStubImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/AppOpsServiceStubImpl;->updateRecordState()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 5

    .line 78
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/android/server/AppOpsServiceStubImpl;->sCtsIgnore:Ljava/util/Set;

    .line 81
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    sput-object v1, Lcom/android/server/AppOpsServiceStubImpl;->MESSAGE_NOT_RECORD:Ljava/util/Set;

    .line 107
    new-instance v2, Landroid/util/ArrayMap;

    invoke-direct {v2}, Landroid/util/ArrayMap;-><init>()V

    sput-object v2, Lcom/android/server/AppOpsServiceStubImpl;->PLATFORM_PERMISSIONS:Landroid/util/ArrayMap;

    .line 108
    const-string v3, "android.permission.SEND_SMS"

    const-string v4, "android.permission-group.SMS"

    invoke-virtual {v2, v3, v4}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    const-string v3, "android.permission.RECEIVE_SMS"

    invoke-virtual {v2, v3, v4}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    const-string v3, "android.permission.READ_SMS"

    invoke-virtual {v2, v3, v4}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    const-string v3, "android.permission.RECEIVE_MMS"

    invoke-virtual {v2, v3, v4}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    const-string v3, "android.permission.RECEIVE_WAP_PUSH"

    invoke-virtual {v2, v3, v4}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    const-string v3, "android.permission.READ_CELL_BROADCASTS"

    invoke-virtual {v2, v3, v4}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    const-string v2, "android.app.usage.cts"

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 116
    const-string v2, "com.android.cts.usepermission"

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 117
    const-string v2, "com.android.cts.permission"

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 118
    const-string v2, "com.android.cts.netlegacy22.permission"

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 119
    const-string v2, "android.netlegacy22.permission.cts"

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 120
    const-string v2, "android.provider.cts"

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 121
    const-string v2, "android.telephony2.cts"

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 122
    const-string v2, "android.permission.cts"

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 123
    const-string v2, "com.android.cts.writeexternalstorageapp"

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 124
    const-string v2, "com.android.cts.readexternalstorageapp"

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 125
    const-string v2, "com.android.cts.externalstorageapp"

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 126
    const-string v2, "android.server.alertwindowapp"

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 127
    const-string v2, "android.server.alertwindowappsdk25"

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 128
    const-string v2, "com.android.app2"

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 129
    const-string v2, "com.android.cts.appbinding.app"

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 130
    const-string v2, "com.android.cts.launcherapps.simplepremapp"

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 131
    const-string v0, "AutoStartManagerService#signalStopProcessesLocked"

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 132
    const-string v0, "ContentProvider#enforceReadPermission"

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 133
    const-string v0, "ContentProvider#enforceWritePermission"

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 134
    const-string v0, "AppOpsHelper#checkLocationAccess"

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 135
    const-string v0, "BroadcastQueueImpl#specifyBroadcast"

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 136
    const-string v0, "WifiPermissionsUtil#noteAppOpAllowed"

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 442
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/server/AppOpsServiceStubImpl;->supportVirtualGrant:Ljava/util/Map;

    .line 445
    const/16 v1, 0xe

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x272c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 446
    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x272d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 447
    const/16 v1, 0x8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x272e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 448
    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x272f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 449
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 69
    invoke-direct {p0}, Lcom/android/server/appop/AppOpsServiceStub;-><init>()V

    .line 95
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/server/AppOpsServiceStubImpl;->mUidStates:Landroid/util/SparseArray;

    .line 97
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/AppOpsServiceStubImpl;->mRecordEnable:Z

    return-void
.end method

.method private getActivityManagerInternal()Landroid/app/ActivityManagerInternal;
    .locals 1

    .line 205
    iget-object v0, p0, Lcom/android/server/AppOpsServiceStubImpl;->mActivityManagerInternal:Landroid/app/ActivityManagerInternal;

    if-nez v0, :cond_0

    .line 206
    const-class v0, Landroid/app/ActivityManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManagerInternal;

    iput-object v0, p0, Lcom/android/server/AppOpsServiceStubImpl;->mActivityManagerInternal:Landroid/app/ActivityManagerInternal;

    .line 208
    :cond_0
    iget-object v0, p0, Lcom/android/server/AppOpsServiceStubImpl;->mActivityManagerInternal:Landroid/app/ActivityManagerInternal;

    return-object v0
.end method

.method private getAppOpsService()Lcom/android/server/appop/AppOpsService;
    .locals 2

    .line 574
    iget-object v0, p0, Lcom/android/server/AppOpsServiceStubImpl;->mAppOpsService:Lcom/android/server/appop/AppOpsService;

    if-nez v0, :cond_0

    .line 575
    const-string v0, "appops"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 576
    .local v0, "b":Landroid/os/IBinder;
    invoke-static {v0}, Lcom/android/internal/app/IAppOpsService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/app/IAppOpsService;

    move-result-object v1

    check-cast v1, Lcom/android/server/appop/AppOpsService;

    iput-object v1, p0, Lcom/android/server/AppOpsServiceStubImpl;->mAppOpsService:Lcom/android/server/appop/AppOpsService;

    .line 578
    .end local v0    # "b":Landroid/os/IBinder;
    :cond_0
    iget-object v0, p0, Lcom/android/server/AppOpsServiceStubImpl;->mAppOpsService:Lcom/android/server/appop/AppOpsService;

    return-object v0
.end method

.method private getPackageManagerInternal()Landroid/content/pm/PackageManagerInternal;
    .locals 1

    .line 212
    iget-object v0, p0, Lcom/android/server/AppOpsServiceStubImpl;->mPackageManagerInternal:Landroid/content/pm/PackageManagerInternal;

    if-nez v0, :cond_0

    .line 213
    const-class v0, Landroid/content/pm/PackageManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageManagerInternal;

    iput-object v0, p0, Lcom/android/server/AppOpsServiceStubImpl;->mPackageManagerInternal:Landroid/content/pm/PackageManagerInternal;

    .line 215
    :cond_0
    iget-object v0, p0, Lcom/android/server/AppOpsServiceStubImpl;->mPackageManagerInternal:Landroid/content/pm/PackageManagerInternal;

    return-object v0
.end method

.method private getSecurityManager()Lmiui/security/SecurityManagerInternal;
    .locals 1

    .line 555
    iget-object v0, p0, Lcom/android/server/AppOpsServiceStubImpl;->mSecurityManagerInternal:Lmiui/security/SecurityManagerInternal;

    if-nez v0, :cond_0

    .line 556
    const-class v0, Lmiui/security/SecurityManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/security/SecurityManagerInternal;

    iput-object v0, p0, Lcom/android/server/AppOpsServiceStubImpl;->mSecurityManagerInternal:Lmiui/security/SecurityManagerInternal;

    .line 558
    :cond_0
    iget-object v0, p0, Lcom/android/server/AppOpsServiceStubImpl;->mSecurityManagerInternal:Lmiui/security/SecurityManagerInternal;

    return-object v0
.end method

.method private declared-synchronized getUidState(IZ)Lcom/android/server/AppOpsServiceStubImpl$UserState;
    .locals 3
    .param p1, "userHandle"    # I
    .param p2, "edit"    # Z

    monitor-enter p0

    .line 153
    :try_start_0
    iget-object v0, p0, Lcom/android/server/AppOpsServiceStubImpl;->mUidStates:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/AppOpsServiceStubImpl$UserState;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 154
    .local v0, "userState":Lcom/android/server/AppOpsServiceStubImpl$UserState;
    if-nez v0, :cond_1

    .line 155
    const/4 v1, 0x0

    if-nez p2, :cond_0

    .line 156
    monitor-exit p0

    return-object v1

    .line 158
    :cond_0
    :try_start_1
    new-instance v2, Lcom/android/server/AppOpsServiceStubImpl$UserState;

    invoke-direct {v2, v1}, Lcom/android/server/AppOpsServiceStubImpl$UserState;-><init>(Lcom/android/server/AppOpsServiceStubImpl$UserState-IA;)V

    move-object v0, v2

    .line 159
    iget-object v1, p0, Lcom/android/server/AppOpsServiceStubImpl;->mUidStates:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 161
    .end local p0    # "this":Lcom/android/server/AppOpsServiceStubImpl;
    :cond_1
    monitor-exit p0

    return-object v0

    .line 152
    .end local v0    # "userState":Lcom/android/server/AppOpsServiceStubImpl$UserState;
    .end local p1    # "userHandle":I
    .end local p2    # "edit":Z
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public static isCtsIgnore(Ljava/lang/String;)Z
    .locals 1
    .param p0, "packageName"    # Ljava/lang/String;

    .line 145
    sget-object v0, Lcom/android/server/AppOpsServiceStubImpl;->sCtsIgnore:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private synthetic lambda$startService$0(I)V
    .locals 3
    .param p1, "userId"    # I

    .line 375
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.miui.permission.Action.SecurityService"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 376
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.lbe.security.miui"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 377
    iget-object v1, p0, Lcom/android/server/AppOpsServiceStubImpl;->mContext:Landroid/content/Context;

    new-instance v2, Landroid/os/UserHandle;

    invoke-direct {v2, p1}, Landroid/os/UserHandle;-><init>(I)V

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 381
    nop

    .end local v0    # "intent":Landroid/content/Intent;
    goto :goto_0

    .line 378
    :catch_0
    move-exception v0

    .line 380
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "AppOpsServiceStubImpl"

    const-string v2, "Start Error"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 382
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private startService(I)V
    .locals 4
    .param p1, "userId"    # I

    .line 373
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/android/server/AppOpsServiceStubImpl$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p1}, Lcom/android/server/AppOpsServiceStubImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/AppOpsServiceStubImpl;I)V

    const-wide/16 v2, 0x514

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 383
    return-void
.end method

.method private updateRecordState()V
    .locals 3

    .line 181
    iget-object v0, p0, Lcom/android/server/AppOpsServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "key_app_behavior_record_enable"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    iput-boolean v2, p0, Lcom/android/server/AppOpsServiceStubImpl;->mRecordEnable:Z

    .line 182
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "updateRecordState: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/AppOpsServiceStubImpl;->mRecordEnable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AppOpsServiceStubImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    return-void
.end method


# virtual methods
.method public askOperationLocked(IILjava/lang/String;Ljava/lang/String;)I
    .locals 7
    .param p1, "code"    # I
    .param p2, "uid"    # I
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "message"    # Ljava/lang/String;

    .line 187
    const-string v0, "noteNoAsk"

    invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_3

    const-string v0, "NoAskNoRecord"

    invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 190
    :cond_0
    const/4 v0, 0x1

    .line 191
    .local v0, "result":I
    invoke-static {p2}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v2

    .line 192
    .local v2, "userId":I
    const/16 v3, 0x3e7

    if-ne v2, v3, :cond_1

    .line 193
    const/4 v2, 0x0

    .line 194
    invoke-static {p2}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v3

    invoke-static {v2, v3}, Landroid/os/UserHandle;->getUid(II)I

    move-result p2

    .line 196
    :cond_1
    invoke-static {p2}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v3

    const/4 v4, 0x0

    invoke-direct {p0, v3, v4}, Lcom/android/server/AppOpsServiceStubImpl;->getUidState(IZ)Lcom/android/server/AppOpsServiceStubImpl$UserState;

    move-result-object v3

    .line 197
    .local v3, "uidState":Lcom/android/server/AppOpsServiceStubImpl$UserState;
    if-eqz v3, :cond_2

    iget-object v4, v3, Lcom/android/server/AppOpsServiceStubImpl$UserState;->mCallbackBinder:Landroid/os/MiuiBinderProxy;

    if-eqz v4, :cond_2

    .line 198
    iget-object v4, v3, Lcom/android/server/AppOpsServiceStubImpl$UserState;->mCallbackBinder:Landroid/os/MiuiBinderProxy;

    .line 199
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    filled-new-array {v5, p3, v6}, [Ljava/lang/Object;

    move-result-object v5

    .line 198
    invoke-virtual {v4, v1, v5}, Landroid/os/MiuiBinderProxy;->callNonOneWayTransact(I[Ljava/lang/Object;)I

    move-result v0

    .line 201
    :cond_2
    return v0

    .line 188
    .end local v0    # "result":I
    .end local v2    # "userId":I
    .end local v3    # "uidState":Lcom/android/server/AppOpsServiceStubImpl$UserState;
    :cond_3
    :goto_0
    return v1
.end method

.method public assessVirtualEnable(III)Z
    .locals 8
    .param p1, "code"    # I
    .param p2, "uid"    # I
    .param p3, "pid"    # I

    .line 429
    const/4 v0, 0x1

    const/16 v1, 0x2710

    if-gt p1, v1, :cond_3

    const/16 v2, 0x1d

    if-eq p1, v2, :cond_3

    const/16 v2, 0x1e

    if-ne p1, v2, :cond_0

    goto :goto_1

    .line 434
    :cond_0
    invoke-static {p2}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v2

    if-ge v2, v1, :cond_1

    .line 435
    return v0

    .line 437
    :cond_1
    invoke-direct {p0}, Lcom/android/server/AppOpsServiceStubImpl;->getPackageManagerInternal()Landroid/content/pm/PackageManagerInternal;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/pm/PackageManagerInternal;->getPackage(I)Lcom/android/server/pm/pkg/AndroidPackage;

    move-result-object v1

    .line 438
    .local v1, "pkg":Lcom/android/server/pm/pkg/AndroidPackage;
    if-eqz v1, :cond_2

    invoke-direct {p0}, Lcom/android/server/AppOpsServiceStubImpl;->getPackageManagerInternal()Landroid/content/pm/PackageManagerInternal;

    move-result-object v2

    .line 439
    invoke-interface {v1}, Lcom/android/server/pm/pkg/AndroidPackage;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-wide/16 v4, 0x0

    const/16 v6, 0x3e8

    invoke-static {p2}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v7

    .line 438
    invoke-virtual/range {v2 .. v7}, Landroid/content/pm/PackageManagerInternal;->getApplicationInfo(Ljava/lang/String;JII)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    invoke-static {v2}, Lcom/android/server/am/ProcessUtils;->isSystem(Landroid/content/pm/ApplicationInfo;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 432
    .end local v1    # "pkg":Lcom/android/server/pm/pkg/AndroidPackage;
    :cond_3
    :goto_1
    return v0
.end method

.method public checkPermission(Landroid/content/pm/PackageManager;Ljava/lang/String;Ljava/lang/String;I)I
    .locals 1
    .param p1, "packageManager"    # Landroid/content/pm/PackageManager;
    .param p2, "permission"    # Ljava/lang/String;
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "uid"    # I

    .line 564
    nop

    .line 565
    invoke-static {p4}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v0

    .line 564
    invoke-static {p2, p3, v0}, Landroid/permission/PermissionManager;->checkPackageNamePermission(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public convertVirtualOp(I)I
    .locals 3
    .param p1, "code"    # I

    .line 464
    sget-object v0, Lcom/android/server/AppOpsServiceStubImpl;->supportVirtualGrant:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getNotificationRequestDelay(Landroid/content/pm/ActivityInfo;)I
    .locals 3
    .param p1, "activityInfo"    # Landroid/content/pm/ActivityInfo;

    .line 519
    const/4 v0, 0x0

    .line 520
    .local v0, "delay":I
    if-nez p1, :cond_0

    .line 521
    return v0

    .line 523
    :cond_0
    iget-object v1, p0, Lcom/android/server/AppOpsServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 524
    iget v1, p1, Landroid/content/pm/ActivityInfo;->screenOrientation:I

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    .line 529
    :sswitch_0
    const/16 v0, 0x3e8

    .line 530
    :goto_0
    goto :goto_1

    .line 533
    :cond_1
    iget v1, p1, Landroid/content/pm/ActivityInfo;->screenOrientation:I

    sparse-switch v1, :sswitch_data_1

    goto :goto_1

    .line 538
    :sswitch_1
    const/16 v0, 0x3e8

    .line 542
    :goto_1
    return v0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x6 -> :sswitch_0
        0x8 -> :sswitch_0
        0xb -> :sswitch_0
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x1 -> :sswitch_1
        0x7 -> :sswitch_1
        0x9 -> :sswitch_1
        0xc -> :sswitch_1
    .end sparse-switch
.end method

.method public init(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 140
    iput-object p1, p0, Lcom/android/server/AppOpsServiceStubImpl;->mContext:Landroid/content/Context;

    .line 141
    const-class v0, Lcom/miui/app/AppOpsServiceInternal;

    invoke-static {v0, p0}, Lcom/android/server/LocalServices;->addService(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 142
    return-void
.end method

.method public initSwitchedOp(Landroid/util/SparseArray;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray<",
            "[I>;)V"
        }
    .end annotation

    .line 547
    .local p1, "switchedOps":Landroid/util/SparseArray;, "Landroid/util/SparseArray<[I>;"
    const/16 v0, 0x2711

    .local v0, "miuiCode":I
    :goto_0
    const/16 v1, 0x2743

    if-ge v0, v1, :cond_0

    .line 548
    invoke-static {v0}, Landroid/app/AppOpsManager;->opToSwitch(I)I

    move-result v1

    .line 549
    .local v1, "switchCode":I
    nop

    .line 550
    invoke-virtual {p1, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [I

    invoke-static {v2, v0}, Lcom/android/internal/util/ArrayUtils;->appendInt([II)[I

    move-result-object v2

    .line 549
    invoke-virtual {p1, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 547
    .end local v1    # "switchCode":I
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 552
    .end local v0    # "miuiCode":I
    :cond_0
    return-void
.end method

.method public isMiuiOp(I)Z
    .locals 1
    .param p1, "op"    # I

    .line 570
    const/16 v0, 0x2710

    if-le p1, v0, :cond_0

    const/16 v0, 0x2743

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isRevokedCompatByMiui(Ljava/lang/String;I)Z
    .locals 3
    .param p1, "permission"    # Ljava/lang/String;
    .param p2, "mode"    # I

    .line 486
    invoke-virtual {p0, p1}, Lcom/android/server/AppOpsServiceStubImpl;->supportForegroundByMiui(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    .line 487
    if-eqz p2, :cond_0

    const/4 v0, 0x4

    if-eq p2, v0, :cond_0

    goto :goto_0

    :cond_0
    move v1, v2

    :goto_0
    return v1

    .line 490
    :cond_1
    if-eqz p2, :cond_2

    goto :goto_1

    :cond_2
    move v1, v2

    :goto_1
    return v1
.end method

.method public isSupportVirtualGrant(I)Z
    .locals 2
    .param p1, "code"    # I

    .line 456
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v0, :cond_0

    sget-object v0, Lcom/android/server/AppOpsServiceStubImpl;->supportVirtualGrant:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isTestSuitSpecialIgnore(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 149
    sget-object v0, Lcom/android/server/AppOpsServiceStubImpl;->sCtsIgnore:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public onAppApplyOperation(ILjava/lang/String;IIIIIZ)V
    .locals 15
    .param p1, "uid"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "op"    # I
    .param p4, "mode"    # I
    .param p5, "operationType"    # I
    .param p6, "processState"    # I
    .param p7, "capability"    # I
    .param p8, "appWidgetVisible"    # Z

    .line 232
    move-object v0, p0

    move/from16 v1, p1

    move-object/from16 v8, p2

    move/from16 v9, p3

    move/from16 v2, p4

    move/from16 v3, p6

    invoke-static/range {p1 .. p1}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v4

    const/16 v5, 0x7d0

    if-le v4, v5, :cond_c

    iget-boolean v4, v0, Lcom/android/server/AppOpsServiceStubImpl;->mRecordEnable:Z

    if-nez v4, :cond_0

    goto/16 :goto_3

    .line 236
    :cond_0
    const/16 v4, 0x2730

    if-ne v9, v4, :cond_1

    if-nez v2, :cond_1

    .line 237
    return-void

    .line 239
    :cond_1
    const/16 v5, 0x33

    if-eq v9, v5, :cond_2

    const/16 v5, 0x41

    if-ne v9, v5, :cond_3

    :cond_2
    if-nez v2, :cond_3

    .line 241
    invoke-direct {p0}, Lcom/android/server/AppOpsServiceStubImpl;->getAppOpsService()Lcom/android/server/appop/AppOpsService;

    move-result-object v5

    invoke-virtual {v5, v4, v1, v8}, Lcom/android/server/appop/AppOpsService;->checkOperation(IILjava/lang/String;)I

    move-result v4

    if-eqz v4, :cond_3

    .line 243
    return-void

    .line 246
    :cond_3
    invoke-static {}, Landroid/app/PrivacyTestModeStub;->get()Landroid/app/PrivacyTestModeStub;

    move-result-object v4

    iget-object v5, v0, Lcom/android/server/AppOpsServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v9, v1, v5, v8}, Landroid/app/PrivacyTestModeStub;->collectPrivacyTestModeInfo(IILandroid/content/Context;Ljava/lang/String;)V

    .line 247
    const/4 v10, 0x4

    if-eq v2, v10, :cond_4

    if-nez v2, :cond_9

    :cond_4
    const/16 v4, 0xc8

    if-le v3, v4, :cond_9

    .line 248
    if-nez p8, :cond_8

    .line 249
    invoke-direct {p0}, Lcom/android/server/AppOpsServiceStubImpl;->getActivityManagerInternal()Landroid/app/ActivityManagerInternal;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 250
    invoke-direct {p0}, Lcom/android/server/AppOpsServiceStubImpl;->getActivityManagerInternal()Landroid/app/ActivityManagerInternal;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/app/ActivityManagerInternal;->isPendingTopUid(I)Z

    move-result v4

    if-eqz v4, :cond_5

    goto :goto_1

    .line 253
    :cond_5
    invoke-static/range {p3 .. p3}, Landroid/app/AppOpsManager;->resolveFirstUnrestrictedUidState(I)I

    move-result v4

    if-gt v3, v4, :cond_9

    .line 254
    sparse-switch v9, :sswitch_data_0

    .line 277
    const/16 v3, 0xc8

    .line 278
    .end local p6    # "processState":I
    .local v3, "processState":I
    const/4 v2, 0x0

    .end local p4    # "mode":I
    .local v2, "mode":I
    goto :goto_0

    .line 271
    .end local v2    # "mode":I
    .end local v3    # "processState":I
    .restart local p4    # "mode":I
    .restart local p6    # "processState":I
    :sswitch_0
    and-int/lit8 v4, p7, 0x4

    if-eqz v4, :cond_6

    .line 272
    const/16 v3, 0xc8

    .line 273
    .end local p6    # "processState":I
    .restart local v3    # "processState":I
    const/4 v2, 0x0

    .end local p4    # "mode":I
    .restart local v2    # "mode":I
    goto :goto_0

    .line 265
    .end local v2    # "mode":I
    .end local v3    # "processState":I
    .restart local p4    # "mode":I
    .restart local p6    # "processState":I
    :sswitch_1
    and-int/lit8 v4, p7, 0x2

    if-eqz v4, :cond_6

    .line 266
    const/16 v3, 0xc8

    .line 267
    .end local p6    # "processState":I
    .restart local v3    # "processState":I
    const/4 v2, 0x0

    .end local p4    # "mode":I
    .restart local v2    # "mode":I
    goto :goto_0

    .line 259
    .end local v2    # "mode":I
    .end local v3    # "processState":I
    .restart local p4    # "mode":I
    .restart local p6    # "processState":I
    :sswitch_2
    and-int/lit8 v4, p7, 0x1

    if-eqz v4, :cond_6

    .line 260
    const/16 v3, 0xc8

    .line 261
    .end local p6    # "processState":I
    .restart local v3    # "processState":I
    const/4 v2, 0x0

    .line 281
    .end local p4    # "mode":I
    .restart local v2    # "mode":I
    :cond_6
    :goto_0
    if-eqz v2, :cond_7

    .line 282
    const/4 v2, 0x1

    move v11, v2

    move v12, v3

    goto :goto_2

    .line 281
    :cond_7
    move v11, v2

    move v12, v3

    goto :goto_2

    .line 251
    .end local v2    # "mode":I
    .end local v3    # "processState":I
    .restart local p4    # "mode":I
    .restart local p6    # "processState":I
    :cond_8
    :goto_1
    const/16 v3, 0xc8

    .line 252
    .end local p6    # "processState":I
    .restart local v3    # "processState":I
    const/4 v2, 0x0

    move v11, v2

    move v12, v3

    .end local p4    # "mode":I
    .restart local v2    # "mode":I
    goto :goto_2

    .line 286
    .end local v2    # "mode":I
    .end local v3    # "processState":I
    .restart local p4    # "mode":I
    .restart local p6    # "processState":I
    :cond_9
    move v11, v2

    move v12, v3

    .end local p4    # "mode":I
    .end local p6    # "processState":I
    .local v11, "mode":I
    .local v12, "processState":I
    :goto_2
    invoke-static/range {p1 .. p1}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v2

    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Lcom/android/server/AppOpsServiceStubImpl;->getUidState(IZ)Lcom/android/server/AppOpsServiceStubImpl$UserState;

    move-result-object v13

    .line 287
    .local v13, "uidState":Lcom/android/server/AppOpsServiceStubImpl$UserState;
    if-eqz v13, :cond_a

    iget-object v2, v13, Lcom/android/server/AppOpsServiceStubImpl$UserState;->mCallbackBinder:Landroid/os/MiuiBinderProxy;

    if-eqz v2, :cond_a

    .line 288
    iget-object v14, v13, Lcom/android/server/AppOpsServiceStubImpl$UserState;->mCallbackBinder:Landroid/os/MiuiBinderProxy;

    .line 290
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v3, p2

    filled-new-array/range {v2 .. v7}, [Ljava/lang/Object;

    move-result-object v2

    .line 288
    invoke-virtual {v14, v10, v2}, Landroid/os/MiuiBinderProxy;->callOneWayTransact(I[Ljava/lang/Object;)I

    .line 292
    :cond_a
    invoke-static/range {p3 .. p3}, Lmiui/security/AppBehavior;->switchOpToBehavior(I)I

    move-result v2

    if-eqz v2, :cond_b

    .line 293
    invoke-direct {p0}, Lcom/android/server/AppOpsServiceStubImpl;->getSecurityManager()Lmiui/security/SecurityManagerInternal;

    move-result-object v2

    if-eqz v2, :cond_b

    .line 294
    invoke-direct {p0}, Lcom/android/server/AppOpsServiceStubImpl;->getSecurityManager()Lmiui/security/SecurityManagerInternal;

    move-result-object v2

    invoke-static/range {p3 .. p3}, Lmiui/security/AppBehavior;->switchOpToBehavior(I)I

    move-result v3

    const-wide/16 v5, 0x1

    const/4 v7, 0x0

    move-object/from16 v4, p2

    invoke-virtual/range {v2 .. v7}, Lmiui/security/SecurityManagerInternal;->recordAppBehaviorAsync(ILjava/lang/String;JLjava/lang/String;)V

    .line 297
    :cond_b
    return-void

    .line 233
    .end local v11    # "mode":I
    .end local v12    # "processState":I
    .end local v13    # "uidState":Lcom/android/server/AppOpsServiceStubImpl$UserState;
    .restart local p4    # "mode":I
    .restart local p6    # "processState":I
    :cond_c
    :goto_3
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_2
        0x1 -> :sswitch_2
        0x1a -> :sswitch_1
        0x1b -> :sswitch_0
        0x29 -> :sswitch_2
        0x2a -> :sswitch_2
    .end sparse-switch
.end method

.method public onAppApplyOperation(ILjava/lang/String;IIIIIZILjava/lang/String;)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "op"    # I
    .param p4, "mode"    # I
    .param p5, "operationType"    # I
    .param p6, "processState"    # I
    .param p7, "capability"    # I
    .param p8, "appWidgetVisible"    # Z
    .param p9, "flags"    # I
    .param p10, "message"    # Ljava/lang/String;

    .line 220
    and-int/lit8 v0, p9, 0x20

    if-nez v0, :cond_2

    sget-object v0, Lcom/android/server/AppOpsServiceStubImpl;->MESSAGE_NOT_RECORD:Ljava/util/Set;

    .line 221
    invoke-interface {v0, p10}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 222
    invoke-static {p10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "WifiPermissionsUtil#noteAppOpAllowed"

    invoke-virtual {p10, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 223
    :cond_0
    const-string v0, "NoAskNoRecord"

    invoke-virtual {v0, p10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    .line 226
    :cond_1
    invoke-virtual/range {p0 .. p8}, Lcom/android/server/AppOpsServiceStubImpl;->onAppApplyOperation(ILjava/lang/String;IIIIIZ)V

    .line 227
    return-void

    .line 224
    :cond_2
    :goto_0
    return-void
.end method

.method public registerCallback(Landroid/os/IBinder;)I
    .locals 5
    .param p1, "callback"    # Landroid/os/IBinder;

    .line 337
    iget-object v0, p0, Lcom/android/server/AppOpsServiceStubImpl;->mContext:Landroid/content/Context;

    .line 338
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    .line 337
    const-string v3, "android.permission.UPDATE_APP_OPS_STATS"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v1, v2, v4}, Landroid/content/Context;->enforcePermission(Ljava/lang/String;IILjava/lang/String;)V

    .line 340
    if-nez p1, :cond_0

    .line 341
    const/4 v0, -0x1

    return v0

    .line 343
    :cond_0
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    move-result v0

    .line 344
    .local v0, "callingUserId":I
    if-nez v0, :cond_1

    .line 345
    const/16 v1, 0x3e7

    invoke-virtual {p0, p1, v1}, Lcom/android/server/AppOpsServiceStubImpl;->registerCallback(Landroid/os/IBinder;I)I

    .line 347
    :cond_1
    invoke-virtual {p0, p1, v0}, Lcom/android/server/AppOpsServiceStubImpl;->registerCallback(Landroid/os/IBinder;I)I

    move-result v1

    return v1
.end method

.method public registerCallback(Landroid/os/IBinder;I)I
    .locals 5
    .param p1, "callback"    # Landroid/os/IBinder;
    .param p2, "userId"    # I

    .line 352
    iget-object v0, p0, Lcom/android/server/AppOpsServiceStubImpl;->mContext:Landroid/content/Context;

    .line 353
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    .line 352
    const-string v3, "android.permission.UPDATE_APP_OPS_STATS"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v1, v2, v4}, Landroid/content/Context;->enforcePermission(Ljava/lang/String;IILjava/lang/String;)V

    .line 355
    const/4 v0, -0x1

    if-nez p1, :cond_0

    .line 356
    return v0

    .line 358
    :cond_0
    const/4 v1, 0x1

    invoke-direct {p0, p2, v1}, Lcom/android/server/AppOpsServiceStubImpl;->getUidState(IZ)Lcom/android/server/AppOpsServiceStubImpl$UserState;

    move-result-object v1

    .line 360
    .local v1, "uidState":Lcom/android/server/AppOpsServiceStubImpl$UserState;
    if-nez v1, :cond_1

    .line 361
    return v0

    .line 363
    :cond_1
    new-instance v0, Landroid/os/MiuiBinderProxy;

    const-string v2, "com.android.internal.app.IOpsCallback"

    invoke-direct {v0, p1, v2}, Landroid/os/MiuiBinderProxy;-><init>(Landroid/os/IBinder;Ljava/lang/String;)V

    iput-object v0, v1, Lcom/android/server/AppOpsServiceStubImpl$UserState;->mCallbackBinder:Landroid/os/MiuiBinderProxy;

    .line 365
    iget-object v0, v1, Lcom/android/server/AppOpsServiceStubImpl$UserState;->mCallback:Lcom/android/server/AppOpsServiceStubImpl$Callback;

    if-eqz v0, :cond_2

    .line 366
    iget-object v0, v1, Lcom/android/server/AppOpsServiceStubImpl$UserState;->mCallback:Lcom/android/server/AppOpsServiceStubImpl$Callback;

    invoke-virtual {v0}, Lcom/android/server/AppOpsServiceStubImpl$Callback;->unlinkToDeath()V

    .line 368
    :cond_2
    new-instance v0, Lcom/android/server/AppOpsServiceStubImpl$Callback;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/server/AppOpsServiceStubImpl$Callback;-><init>(Lcom/android/server/AppOpsServiceStubImpl;Landroid/os/IBinder;I)V

    iput-object v0, v1, Lcom/android/server/AppOpsServiceStubImpl$UserState;->mCallback:Lcom/android/server/AppOpsServiceStubImpl$Callback;

    .line 369
    const/4 v0, 0x0

    return v0
.end method

.method public declared-synchronized removeUser(I)V
    .locals 1
    .param p1, "userHandle"    # I

    monitor-enter p0

    .line 165
    :try_start_0
    iget-object v0, p0, Lcom/android/server/AppOpsServiceStubImpl;->mUidStates:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 166
    monitor-exit p0

    return-void

    .line 164
    .end local p0    # "this":Lcom/android/server/AppOpsServiceStubImpl;
    .end local p1    # "userHandle":I
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public skipNotificationRequest(Landroid/content/pm/ActivityInfo;)Z
    .locals 2
    .param p1, "activityInfo"    # Landroid/content/pm/ActivityInfo;

    .line 512
    if-eqz p1, :cond_0

    iget-object v0, p1, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 514
    const-string v1, "SplashActivity"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 512
    :goto_0
    return v0
.end method

.method public skipSyncAppOpsWithRuntime(III)Z
    .locals 3
    .param p1, "code"    # I
    .param p2, "mode"    # I
    .param p3, "oldMode"    # I

    .line 496
    invoke-static {}, Landroid/content/pm/PackageManagerStub;->get()Landroid/content/pm/PackageManagerStub;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManagerStub;->isOptimizationMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 497
    invoke-static {p1}, Landroid/app/AppOpsManager;->opToPermission(I)Ljava/lang/String;

    move-result-object v0

    .line 498
    .local v0, "permission":Ljava/lang/String;
    const/4 v1, 0x4

    if-ne p3, v1, :cond_0

    .line 499
    invoke-virtual {p0, v0}, Lcom/android/server/AppOpsServiceStubImpl;->supportForegroundByMiui(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 501
    const/4 v1, 0x1

    return v1

    .line 505
    :cond_0
    sget-object v1, Lcom/android/server/AppOpsServiceStubImpl;->PLATFORM_PERMISSIONS:Landroid/util/ArrayMap;

    invoke-virtual {v1, v0}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    const-string v2, "android.permission-group.SMS"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    return v1

    .line 507
    .end local v0    # "permission":Ljava/lang/String;
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public supportForegroundByMiui(Ljava/lang/String;)Z
    .locals 1
    .param p1, "permission"    # Ljava/lang/String;

    .line 473
    const-string v0, "android.permission.READ_EXTERNAL_STORAGE"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 474
    const-string v0, "android.permission.WRITE_EXTERNAL_STORAGE"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 475
    const-string v0, "android.permission.READ_CALL_LOG"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 476
    const-string v0, "android.permission.WRITE_CALL_LOG"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 477
    const-string v0, "android.permission.READ_CALENDAR"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 478
    const-string v0, "android.permission.WRITE_CALENDAR"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 479
    const-string v0, "android.permission.READ_CONTACTS"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 480
    const-string v0, "android.permission.WRITE_CONTACTS"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 481
    const-string v0, "android.permission.GET_ACCOUNTS"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 473
    :goto_1
    return v0
.end method

.method public systemReady()V
    .locals 4

    .line 169
    const-string v0, "key_app_behavior_record_enable"

    invoke-static {v0}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 170
    .local v0, "enableUri":Landroid/net/Uri;
    new-instance v1, Lcom/android/server/AppOpsServiceStubImpl$1;

    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->getHandler()Landroid/os/Handler;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/android/server/AppOpsServiceStubImpl$1;-><init>(Lcom/android/server/AppOpsServiceStubImpl;Landroid/os/Handler;)V

    .line 176
    .local v1, "observer":Landroid/database/ContentObserver;
    iget-object v2, p0, Lcom/android/server/AppOpsServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 177
    invoke-direct {p0}, Lcom/android/server/AppOpsServiceStubImpl;->updateRecordState()V

    .line 178
    return-void
.end method

.method public updateProcessState(II)V
    .locals 17
    .param p1, "uid"    # I
    .param p2, "procState"    # I

    .line 301
    move/from16 v0, p2

    invoke-static/range {p1 .. p1}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v1

    const/16 v2, 0x2710

    if-ge v1, v2, :cond_0

    .line 302
    return-void

    .line 304
    :cond_0
    invoke-static/range {p1 .. p1}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v1

    .line 305
    .local v1, "userId":I
    const/4 v2, 0x0

    move-object/from16 v9, p0

    invoke-direct {v9, v1, v2}, Lcom/android/server/AppOpsServiceStubImpl;->getUidState(IZ)Lcom/android/server/AppOpsServiceStubImpl$UserState;

    move-result-object v2

    .line 306
    .local v2, "uidState":Lcom/android/server/AppOpsServiceStubImpl$UserState;
    if-eqz v2, :cond_1

    iget-object v3, v2, Lcom/android/server/AppOpsServiceStubImpl$UserState;->mCallbackBinder:Landroid/os/MiuiBinderProxy;

    if-eqz v3, :cond_1

    .line 307
    iget-object v3, v2, Lcom/android/server/AppOpsServiceStubImpl$UserState;->mCallbackBinder:Landroid/os/MiuiBinderProxy;

    .line 308
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    filled-new-array {v4, v5}, [Ljava/lang/Object;

    move-result-object v4

    .line 307
    const/4 v5, 0x5

    invoke-virtual {v3, v5, v4}, Landroid/os/MiuiBinderProxy;->callOneWayTransact(I[Ljava/lang/Object;)I

    .line 310
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/android/server/AppOpsServiceStubImpl;->getSecurityManager()Lmiui/security/SecurityManagerInternal;

    move-result-object v3

    if-nez v3, :cond_2

    .line 311
    return-void

    .line 313
    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/android/server/AppOpsServiceStubImpl;->getPackageManagerInternal()Landroid/content/pm/PackageManagerInternal;

    move-result-object v3

    move/from16 v15, p1

    invoke-virtual {v3, v15}, Landroid/content/pm/PackageManagerInternal;->getPackage(I)Lcom/android/server/pm/pkg/AndroidPackage;

    move-result-object v16

    .line 314
    .local v16, "pkg":Lcom/android/server/pm/pkg/AndroidPackage;
    if-eqz v16, :cond_5

    invoke-direct/range {p0 .. p0}, Lcom/android/server/AppOpsServiceStubImpl;->getPackageManagerInternal()Landroid/content/pm/PackageManagerInternal;

    move-result-object v3

    .line 315
    invoke-interface/range {v16 .. v16}, Lcom/android/server/pm/pkg/AndroidPackage;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const-wide/16 v5, 0x0

    const/16 v7, 0x3e8

    .line 314
    move v8, v1

    invoke-virtual/range {v3 .. v8}, Landroid/content/pm/PackageManagerInternal;->getApplicationInfo(Ljava/lang/String;JII)Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    invoke-static {v3}, Lcom/android/server/am/ProcessUtils;->isSystem(Landroid/content/pm/ApplicationInfo;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 316
    const/4 v3, 0x0

    .local v3, "fgState":Z
    const/4 v4, 0x0

    .line 317
    .local v4, "bgState":Z
    const/16 v5, 0x14

    if-ge v0, v5, :cond_4

    .line 318
    const/4 v5, 0x7

    if-ge v0, v5, :cond_3

    .line 319
    const/4 v3, 0x1

    goto :goto_0

    .line 321
    :cond_3
    const/4 v4, 0x1

    goto :goto_0

    .line 324
    :cond_4
    invoke-direct/range {p0 .. p0}, Lcom/android/server/AppOpsServiceStubImpl;->getSecurityManager()Lmiui/security/SecurityManagerInternal;

    move-result-object v5

    invoke-interface/range {v16 .. v16}, Lcom/android/server/pm/pkg/AndroidPackage;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lmiui/security/SecurityManagerInternal;->removeCalleeRestrictChain(Ljava/lang/String;)V

    .line 326
    :goto_0
    invoke-direct/range {p0 .. p0}, Lcom/android/server/AppOpsServiceStubImpl;->getSecurityManager()Lmiui/security/SecurityManagerInternal;

    move-result-object v10

    const/16 v11, 0x16

    const/4 v12, 0x0

    .line 327
    invoke-interface/range {v16 .. v16}, Lcom/android/server/pm/pkg/AndroidPackage;->getPackageName()Ljava/lang/String;

    move-result-object v14

    .line 326
    move/from16 v13, p1

    move v15, v3

    invoke-virtual/range {v10 .. v15}, Lmiui/security/SecurityManagerInternal;->recordDurationInfo(ILandroid/os/IBinder;ILjava/lang/String;Z)V

    .line 329
    invoke-direct/range {p0 .. p0}, Lcom/android/server/AppOpsServiceStubImpl;->getSecurityManager()Lmiui/security/SecurityManagerInternal;

    move-result-object v10

    const/16 v11, 0x17

    .line 330
    invoke-interface/range {v16 .. v16}, Lcom/android/server/pm/pkg/AndroidPackage;->getPackageName()Ljava/lang/String;

    move-result-object v14

    .line 329
    move v15, v4

    invoke-virtual/range {v10 .. v15}, Lmiui/security/SecurityManagerInternal;->recordDurationInfo(ILandroid/os/IBinder;ILjava/lang/String;Z)V

    .line 333
    .end local v3    # "fgState":Z
    .end local v4    # "bgState":Z
    :cond_5
    return-void
.end method
