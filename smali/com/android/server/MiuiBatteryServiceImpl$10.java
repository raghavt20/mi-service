class com.android.server.MiuiBatteryServiceImpl$10 extends android.content.BroadcastReceiver {
	 /* .source "MiuiBatteryServiceImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/MiuiBatteryServiceImpl;->init(Landroid/content/Context;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.MiuiBatteryServiceImpl this$0; //synthetic
/* # direct methods */
 com.android.server.MiuiBatteryServiceImpl$10 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/MiuiBatteryServiceImpl; */
/* .line 373 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 376 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 377 */
/* .local v0, "action":Ljava/lang/String; */
v1 = this.this$0;
v1 = com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetDEBUG ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* new-instance v1, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v2 = "action = "; // const-string v2, "action = "
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v2 = "MiuiBatteryServiceImpl"; // const-string v2, "MiuiBatteryServiceImpl"
	 android.util.Slog .d ( v2,v1 );
	 /* .line 378 */
} // :cond_0
final String v1 = "android.intent.action.BOOT_COMPLETED"; // const-string v1, "android.intent.action.BOOT_COMPLETED"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* const-wide/16 v2, 0x0 */
/* if-nez v1, :cond_2 */
/* .line 379 */
final String v1 = "miui.intent.action.UPDATE_BATTERY_DATA"; // const-string v1, "miui.intent.action.UPDATE_BATTERY_DATA"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
	 /* .line 381 */
} // :cond_1
final String v1 = "miui.intent.action.ADJUST_VOLTAGE"; // const-string v1, "miui.intent.action.ADJUST_VOLTAGE"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_3
	 /* .line 382 */
	 v1 = this.this$0;
	 com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmHandler ( v1 );
	 /* const/16 v4, 0x14 */
	 (( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) v1 ).sendMessageDelayed ( v4, p2, v2, v3 ); // invoke-virtual {v1, v4, p2, v2, v3}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessageDelayed(ILjava/lang/Object;J)V
	 /* .line 380 */
} // :cond_2
} // :goto_0
v1 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmHandler ( v1 );
/* const/16 v4, 0xf */
(( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) v1 ).sendMessageDelayed ( v4, v2, v3 ); // invoke-virtual {v1, v4, v2, v3}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessageDelayed(IJ)V
/* .line 384 */
} // :cond_3
} // :goto_1
return;
} // .end method
