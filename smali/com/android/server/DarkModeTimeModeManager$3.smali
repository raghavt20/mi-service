.class Lcom/android/server/DarkModeTimeModeManager$3;
.super Ljava/lang/Object;
.source "DarkModeTimeModeManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/DarkModeTimeModeManager;->enterSettingsFromNotification(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/DarkModeTimeModeManager;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/android/server/DarkModeTimeModeManager;Landroid/content/Context;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/DarkModeTimeModeManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 335
    iput-object p1, p0, Lcom/android/server/DarkModeTimeModeManager$3;->this$0:Lcom/android/server/DarkModeTimeModeManager;

    iput-object p2, p0, Lcom/android/server/DarkModeTimeModeManager$3;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 338
    iget-object v0, p0, Lcom/android/server/DarkModeTimeModeManager$3;->val$context:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/DarkModeTimeModeHelper;->isDarkModeOpen(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 339
    new-instance v0, Lcom/android/server/DarkModeStauesEvent;

    invoke-direct {v0}, Lcom/android/server/DarkModeStauesEvent;-><init>()V

    .line 340
    const-string v1, "darkModeSuggest"

    invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setEventName(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;

    move-result-object v0

    .line 341
    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setTip(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;

    move-result-object v0

    .line 342
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setSuggestOpenInSetting(I)Lcom/android/server/DarkModeStauesEvent;

    move-result-object v0

    .line 343
    .local v0, "event":Lcom/android/server/DarkModeStauesEvent;
    iget-object v1, p0, Lcom/android/server/DarkModeTimeModeManager$3;->val$context:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/android/server/DarkModeOneTrackHelper;->uploadToOneTrack(Landroid/content/Context;Lcom/android/server/DarkModeEvent;)V

    .line 346
    .end local v0    # "event":Lcom/android/server/DarkModeStauesEvent;
    :cond_0
    iget-object v0, p0, Lcom/android/server/DarkModeTimeModeManager$3;->val$context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "enter_setting_by_notification"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 348
    return-void
.end method
