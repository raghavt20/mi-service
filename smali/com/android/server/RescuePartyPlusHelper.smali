.class public Lcom/android/server/RescuePartyPlusHelper;
.super Ljava/lang/Object;
.source "RescuePartyPlusHelper.java"


# static fields
.field private static final CLOUD_CONTROL_FILE:Ljava/lang/String; = "/data/mqsas/cloud/rescuePartyPlusCloudControl.json"

.field private static final CONFIG_RESET_PROCESS:Ljava/lang/String; = "sys.rescuepartyplus.config_reset_process"

.field private static final CORE_PACKAGE:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final DEVELOPMENT_PROP:Ljava/lang/String; = "ro.mi.development"

.field private static final PRODUCT_PROP:Ljava/lang/String; = "ro.build.product"

.field private static final PROP_BOOTCOMPLETE:Ljava/lang/String; = "sys.boot_completed"

.field private static final PROP_DISABLE_AUTORESTART_APP_PREFIX:Ljava/lang/String; = "sys.rescuepartyplus.disable_autorestart."

.field private static final PROP_READY_SHOW_RESET_CONFIG_UI:Ljava/lang/String; = "sys.rescuepartyplus.ready_show_reset_config_ui"

.field private static final RESCUEPARTY_DEBUG_PROC:Ljava/lang/String; = "persist.sys.rescuepartyplus.debug"

.field private static final RESCUEPARTY_LAST_RESET_CONFIG_PROP:Ljava/lang/String; = "persist.sys.rescuepartyplus.last_reset_config"

.field private static final RESCUEPARTY_PLUS_DISABLE_PROP:Ljava/lang/String; = "persist.sys.rescuepartyplus.disable"

.field private static final RESCUEPARTY_PLUS_ENABLE_PROP:Ljava/lang/String; = "persist.sys.rescuepartyplus.enable"

.field private static final RESCUEPARTY_TEMP_MITIGATION_COUNT_PROP:Ljava/lang/String; = "sys.rescuepartyplus.temp_mitigation_count"

.field private static final RESET_CONFIG_DEFAULT_SET:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field static final TAG:Ljava/lang/String; = "RescuePartyPlus"

.field private static final THEME_DATA:Ljava/lang/String; = "/data/system/theme"

.field private static final THEME_MAGIC_DATA:Ljava/lang/String; = "/data/system/theme_magic"

.field private static final TOP_UI_PACKAGE:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final UI_PACKAGE:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 50
    new-instance v0, Lcom/android/server/RescuePartyPlusHelper$1;

    invoke-direct {v0}, Lcom/android/server/RescuePartyPlusHelper$1;-><init>()V

    sput-object v0, Lcom/android/server/RescuePartyPlusHelper;->RESET_CONFIG_DEFAULT_SET:Ljava/util/Set;

    .line 60
    new-instance v0, Lcom/android/server/RescuePartyPlusHelper$2;

    invoke-direct {v0}, Lcom/android/server/RescuePartyPlusHelper$2;-><init>()V

    sput-object v0, Lcom/android/server/RescuePartyPlusHelper;->CORE_PACKAGE:Ljava/util/Set;

    .line 64
    new-instance v0, Lcom/android/server/RescuePartyPlusHelper$3;

    invoke-direct {v0}, Lcom/android/server/RescuePartyPlusHelper$3;-><init>()V

    sput-object v0, Lcom/android/server/RescuePartyPlusHelper;->UI_PACKAGE:Ljava/util/Set;

    .line 68
    new-instance v0, Lcom/android/server/RescuePartyPlusHelper$4;

    invoke-direct {v0}, Lcom/android/server/RescuePartyPlusHelper$4;-><init>()V

    sput-object v0, Lcom/android/server/RescuePartyPlusHelper;->TOP_UI_PACKAGE:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static checkBootCompletedStatus()Z
    .locals 2

    .line 111
    const-string/jumbo v0, "sys.boot_completed"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method static checkDisableRescuePartyPlus()Z
    .locals 3

    .line 146
    const-string v0, "persist.sys.rescuepartyplus.disable"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    const-string v2, "RescuePartyPlus"

    if-eqz v0, :cond_0

    .line 147
    const-string v0, "RescueParty Plus is disable!"

    invoke-static {v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    const/4 v0, 0x1

    return v0

    .line 149
    :cond_0
    const-string v0, "persist.sys.rescuepartyplus.enable"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 150
    const-string v0, "This device support and enable RescuePartyPlus! (Via cloud control)"

    invoke-static {v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    return v1

    .line 154
    :cond_1
    return v1
.end method

.method static checkPackageIsCore(Ljava/lang/String;)Z
    .locals 1
    .param p0, "packageName"    # Ljava/lang/String;

    .line 116
    if-nez p0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/android/server/RescuePartyPlusHelper;->CORE_PACKAGE:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0
.end method

.method static checkPackageIsTOPUI(Ljava/lang/String;)Z
    .locals 1
    .param p0, "packageName"    # Ljava/lang/String;

    .line 124
    if-nez p0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/android/server/RescuePartyPlusHelper;->TOP_UI_PACKAGE:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0
.end method

.method static checkPackageIsUI(Ljava/lang/String;)Z
    .locals 1
    .param p0, "packageName"    # Ljava/lang/String;

    .line 120
    if-nez p0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/android/server/RescuePartyPlusHelper;->UI_PACKAGE:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0
.end method

.method static delete(Ljava/io/File;Ljava/util/Set;)Z
    .locals 6
    .param p0, "deleteFile"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .line 171
    .local p1, "saveDir":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v0, 0x1

    .line 172
    .local v0, "result":Z
    invoke-virtual {p0}, Ljava/io/File;->isFile()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 173
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    move-result v1

    and-int/2addr v0, v1

    goto :goto_2

    .line 174
    :cond_0
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 175
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 176
    .local v1, "files":[Ljava/io/File;
    if-nez v1, :cond_1

    .line 177
    return v0

    .line 179
    :cond_1
    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_4

    aget-object v4, v1, v3

    .line 180
    .local v4, "deleteInFile":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->isFile()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 181
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    move-result v5

    and-int/2addr v0, v5

    goto :goto_1

    .line 182
    :cond_2
    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 183
    invoke-static {v4, p1}, Lcom/android/server/RescuePartyPlusHelper;->delete(Ljava/io/File;Ljava/util/Set;)Z

    move-result v5

    and-int/2addr v0, v5

    .line 179
    .end local v4    # "deleteInFile":Ljava/io/File;
    :cond_3
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 187
    :cond_4
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 188
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    .line 191
    .end local v1    # "files":[Ljava/io/File;
    :cond_5
    :goto_2
    return v0
.end method

.method static disableAppRestart(Ljava/lang/String;)V
    .locals 3
    .param p0, "packageName"    # Ljava/lang/String;

    .line 129
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const-string v1, "RescuePartyPlus"

    if-eqz v0, :cond_0

    .line 130
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Disable app auto restart failed, because package is empty: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    return-void

    .line 133
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Disable app auto restart: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "sys.rescuepartyplus.disable_autorestart."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Finished rescue level DISABLE_APP for package "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    invoke-static {v1, v0}, Lcom/android/server/pm/PackageManagerServiceUtils;->logCriticalInfo(ILjava/lang/String;)V

    .line 136
    return-void
.end method

.method static enableDebugStatus()Z
    .locals 2

    .line 140
    const-string v0, "persist.sys.rescuepartyplus.debug"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 141
    const-string v0, "ro.mi.development"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v1, 0x1

    .line 140
    :cond_1
    return v1
.end method

.method static getConfigResetProcessStatus()Z
    .locals 2

    .line 86
    const-string/jumbo v0, "sys.rescuepartyplus.config_reset_process"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method static getLastResetConfigStatus()Z
    .locals 2

    .line 94
    const-string v0, "persist.sys.rescuepartyplus.last_reset_config"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method static getLauncherPackageName(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .line 160
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 161
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "android.intent.category.HOME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 162
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    .line 163
    .local v1, "res":Landroid/content/pm/ResolveInfo;
    if-eqz v1, :cond_1

    iget-object v2, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v2, :cond_1

    iget-object v2, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    const-string v3, "android"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    .line 166
    :cond_0
    iget-object v2, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    return-object v2

    .line 164
    :cond_1
    :goto_0
    const/4 v2, 0x0

    return-object v2
.end method

.method static getMitigationTempCount()I
    .locals 2

    .line 78
    const-string/jumbo v0, "sys.rescuepartyplus.temp_mitigation_count"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private static getProductInfo()Ljava/lang/String;
    .locals 2

    .line 74
    const-string v0, "ro.build.product"

    const-string v1, "None"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static getShowResetConfigUIStatus()Z
    .locals 2

    .line 102
    const-string/jumbo v0, "sys.rescuepartyplus.ready_show_reset_config_ui"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method static resetTheme(Ljava/lang/String;)Z
    .locals 6
    .param p0, "failedPackage"    # Ljava/lang/String;

    .line 196
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Preparing to reset theme: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "RescuePartyPlus"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 198
    .local v0, "saveDir":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    const/4 v2, 0x1

    .line 199
    .local v2, "result":Z
    const-string v3, "/data/system/theme"

    invoke-virtual {v0, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 200
    const-string v4, "/data/system/theme_magic"

    invoke-virtual {v0, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 201
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v5, v0}, Lcom/android/server/RescuePartyPlusHelper;->delete(Ljava/io/File;Ljava/util/Set;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 202
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Delete theme files failed: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    const/4 v2, 0x0

    .line 205
    :cond_0
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3, v0}, Lcom/android/server/RescuePartyPlusHelper;->delete(Ljava/io/File;Ljava/util/Set;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 206
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Delete magic theme files failed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    const/4 v2, 0x0

    .line 209
    :cond_1
    return v2
.end method

.method static setConfigResetProcessStatus(Z)V
    .locals 2
    .param p0, "status"    # Z

    .line 90
    if-eqz p0, :cond_0

    const-string v0, "1"

    goto :goto_0

    :cond_0
    const-string v0, "0"

    :goto_0
    const-string/jumbo v1, "sys.rescuepartyplus.config_reset_process"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    return-void
.end method

.method static setLastResetConfigStatus(Z)V
    .locals 2
    .param p0, "status"    # Z

    .line 98
    if-eqz p0, :cond_0

    const-string v0, "1"

    goto :goto_0

    :cond_0
    const-string v0, "0"

    :goto_0
    const-string v1, "persist.sys.rescuepartyplus.last_reset_config"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    return-void
.end method

.method static setMitigationTempCount(I)V
    .locals 2
    .param p0, "mitigationTempCount"    # I

    .line 82
    const-string/jumbo v0, "sys.rescuepartyplus.temp_mitigation_count"

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    return-void
.end method

.method static setShowResetConfigUIStatus(Z)V
    .locals 2
    .param p0, "status"    # Z

    .line 106
    if-eqz p0, :cond_0

    const-string v0, "1"

    goto :goto_0

    :cond_0
    const-string v0, "0"

    :goto_0
    const-string/jumbo v1, "sys.rescuepartyplus.ready_show_reset_config_ui"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    return-void
.end method

.method static tryGetCloudControlOrDefaultData()Ljava/util/Set;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 214
    new-instance v0, Ljava/io/File;

    const-string v1, "/data/mqsas/cloud/rescuePartyPlusCloudControl.json"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 215
    .local v0, "cloudControlFile":Ljava/io/File;
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 216
    .local v1, "cloudControlData":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 217
    const-string v2, "Use rescueparty plus cloud control data!"

    const-string v3, "RescuePartyPlus"

    invoke-static {v3, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    :try_start_0
    new-instance v2, Ljava/io/FileReader;

    invoke-direct {v2, v0}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 219
    .local v2, "fileReader":Ljava/io/FileReader;
    :try_start_1
    new-instance v4, Ljava/io/BufferedReader;

    invoke-direct {v4, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 220
    .local v4, "bufferedReader":Ljava/io/BufferedReader;
    :try_start_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 221
    .local v5, "stringBuilder":Ljava/lang/StringBuilder;
    const-string v6, ""

    .line 222
    .local v6, "str":Ljava/lang/String;
    :goto_0
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v7

    move-object v6, v7

    if-eqz v7, :cond_0

    .line 223
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 226
    :cond_0
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 227
    .local v7, "originData":Ljava/lang/String;
    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_3

    .line 228
    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8, v7}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 229
    .local v8, "controlJSONObject":Lorg/json/JSONObject;
    const-string v9, "controlData"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 230
    .local v9, "controlData":Ljava/lang/String;
    const-string v10, ";"

    invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    .line 231
    .local v10, "deleteFiles":[Ljava/lang/String;
    array-length v11, v10

    const/4 v12, 0x0

    :goto_1
    if-ge v12, v11, :cond_2

    aget-object v13, v10, v12

    .line 232
    .local v13, "filePath":Ljava/lang/String;
    invoke-virtual {v13}, Ljava/lang/String;->isEmpty()Z

    move-result v14

    if-nez v14, :cond_1

    .line 233
    invoke-virtual {v1, v13}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 231
    .end local v13    # "filePath":Ljava/lang/String;
    :cond_1
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    .line 236
    .end local v8    # "controlJSONObject":Lorg/json/JSONObject;
    .end local v9    # "controlData":Ljava/lang/String;
    .end local v10    # "deleteFiles":[Ljava/lang/String;
    :cond_2
    goto :goto_2

    .line 237
    :cond_3
    const-string v8, "Cloud control data is empty!"

    invoke-static {v3, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 239
    .end local v5    # "stringBuilder":Ljava/lang/StringBuilder;
    .end local v6    # "str":Ljava/lang/String;
    .end local v7    # "originData":Ljava/lang/String;
    :goto_2
    :try_start_3
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .end local v4    # "bufferedReader":Ljava/io/BufferedReader;
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_5

    .line 218
    .restart local v4    # "bufferedReader":Ljava/io/BufferedReader;
    :catchall_0
    move-exception v5

    :try_start_5
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_3

    :catchall_1
    move-exception v6

    :try_start_6
    invoke-virtual {v5, v6}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "cloudControlFile":Ljava/io/File;
    .end local v1    # "cloudControlData":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    .end local v2    # "fileReader":Ljava/io/FileReader;
    :goto_3
    throw v5
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .end local v4    # "bufferedReader":Ljava/io/BufferedReader;
    .restart local v0    # "cloudControlFile":Ljava/io/File;
    .restart local v1    # "cloudControlData":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    .restart local v2    # "fileReader":Ljava/io/FileReader;
    :catchall_2
    move-exception v4

    :try_start_7
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    goto :goto_4

    :catchall_3
    move-exception v5

    :try_start_8
    invoke-virtual {v4, v5}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "cloudControlFile":Ljava/io/File;
    .end local v1    # "cloudControlData":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    :goto_4
    throw v4
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Lorg/json/JSONException; {:try_start_8 .. :try_end_8} :catch_0

    .line 241
    .end local v2    # "fileReader":Ljava/io/FileReader;
    .restart local v0    # "cloudControlFile":Ljava/io/File;
    .restart local v1    # "cloudControlData":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    :catch_0
    move-exception v2

    .line 242
    .local v2, "e":Lorg/json/JSONException;
    const-string v4, "Parse cloud control data failed!"

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .end local v2    # "e":Lorg/json/JSONException;
    goto :goto_5

    .line 239
    :catch_1
    move-exception v2

    .line 240
    .local v2, "e":Ljava/io/IOException;
    const-string v4, "Read cloud control data failed!"

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 243
    .end local v2    # "e":Ljava/io/IOException;
    :goto_5
    goto :goto_6

    .line 245
    :cond_4
    sget-object v2, Lcom/android/server/RescuePartyPlusHelper;->RESET_CONFIG_DEFAULT_SET:Ljava/util/Set;

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 248
    :goto_6
    return-object v1
.end method
