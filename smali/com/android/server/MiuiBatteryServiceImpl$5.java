class com.android.server.MiuiBatteryServiceImpl$5 extends android.database.ContentObserver {
	 /* .source "MiuiBatteryServiceImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/MiuiBatteryServiceImpl;->init(Landroid/content/Context;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.MiuiBatteryServiceImpl this$0; //synthetic
/* # direct methods */
 com.android.server.MiuiBatteryServiceImpl$5 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/MiuiBatteryServiceImpl; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 244 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0 ) {
/* .locals 6 */
/* .param p1, "selfChange" # Z */
/* .line 247 */
v0 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmMiCharge ( v0 );
(( miui.util.IMiCharge ) v0 ).getSocDecimal ( ); // invoke-virtual {v0}, Lmiui/util/IMiCharge;->getSocDecimal()Ljava/lang/String;
/* .line 248 */
/* .local v0, "socDecimal":Ljava/lang/String; */
v1 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmMiCharge ( v1 );
(( miui.util.IMiCharge ) v1 ).getSocDecimalRate ( ); // invoke-virtual {v1}, Lmiui/util/IMiCharge;->getSocDecimalRate()Ljava/lang/String;
/* .line 249 */
/* .local v1, "socDecimalRate":Ljava/lang/String; */
v2 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v2, :cond_0 */
/* .line 250 */
v2 = android.text.TextUtils .isEmpty ( v1 );
/* if-nez v2, :cond_0 */
/* .line 251 */
v2 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmHandler ( v2 );
v2 = (( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) v2 ).parseInt ( v0 ); // invoke-virtual {v2, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->parseInt(Ljava/lang/String;)I
/* .line 252 */
/* .local v2, "socDecimalValue":I */
v3 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmHandler ( v3 );
v3 = (( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) v3 ).parseInt ( v1 ); // invoke-virtual {v3, v1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->parseInt(Ljava/lang/String;)I
/* .line 253 */
/* .local v3, "socDecimalRateVaule":I */
v4 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmHandler ( v4 );
int v5 = 4; // const/4 v5, 0x4
(( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) v4 ).sendMessage ( v5, v2, v3 ); // invoke-virtual {v4, v5, v2, v3}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessage(III)V
/* .line 255 */
} // .end local v2 # "socDecimalValue":I
} // .end local v3 # "socDecimalRateVaule":I
} // :cond_0
return;
} // .end method
