.class public Lcom/android/server/wallpaper/WallpaperDataParserProxy;
.super Ljava/lang/Object;
.source "WallpaperDataParserProxy.java"


# static fields
.field public static makeJournaledFile:Lcom/xiaomi/reflect/RefMethod;
    .annotation runtime Lcom/xiaomi/reflect/annotation/MethodArguments;
        cls = {
            I
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/xiaomi/reflect/RefMethod<",
            "Lcom/android/internal/util/JournaledFile;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 10
    const-class v0, Lcom/android/server/wallpaper/WallpaperDataParserProxy;

    const-class v1, Lcom/android/server/wallpaper/WallpaperDataParser;

    invoke-static {v0, v1}, Lcom/xiaomi/reflect/RefClass;->attach(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Class;

    .line 11
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
