public class com.android.server.wallpaper.WallpaperManagerServiceImpl extends com.android.server.wallpaper.WallpaperManagerServiceStub {
	 /* .source "WallpaperManagerServiceImpl.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.wallpaper.WallpaperManagerServiceStub$$" */
} // .end annotation
/* # static fields */
private static final Integer FRAMEWORK_VERSION_TO_WALLPAPER;
private static final java.lang.String KEY_FRAMEWORK_VERSION_TO_WALLPAPER;
private static final java.util.List MIUI_WALLPAPER_COMPONENTS;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final Integer MSG_CREATE_BLURWALLPAPER;
private static final java.util.List SUPER_WALLPAPER_PACKAGE_NAMES;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.lang.String TAG;
/* # instance fields */
private android.os.Handler blurWallpaperHandler;
private android.os.HandlerThread blurWallpaperThread;
private android.content.Context mContext;
private com.android.server.wallpaper.WallpaperManagerService mService;
/* # direct methods */
static com.android.server.wallpaper.WallpaperManagerService -$$Nest$fgetmService ( com.android.server.wallpaper.WallpaperManagerServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mService;
} // .end method
static void -$$Nest$mnotifyBlurCallbacksLocked ( com.android.server.wallpaper.WallpaperManagerServiceImpl p0, com.android.server.wallpaper.WallpaperData p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->notifyBlurCallbacksLocked(Lcom/android/server/wallpaper/WallpaperData;)V */
return;
} // .end method
static com.android.server.wallpaper.WallpaperManagerServiceImpl ( ) {
/* .locals 3 */
/* .line 72 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 73 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 76 */
final String v2 = "ComponentInfo{com.miui.miwallpaper/com.miui.miwallpaper.wallpaperservice.ImageWallpaper}"; // const-string v2, "ComponentInfo{com.miui.miwallpaper/com.miui.miwallpaper.wallpaperservice.ImageWallpaper}"
/* .line 78 */
final String v0 = "com.miui.miwallpaper.snowmountain"; // const-string v0, "com.miui.miwallpaper.snowmountain"
/* .line 79 */
final String v0 = "com.miui.miwallpaper.geometry"; // const-string v0, "com.miui.miwallpaper.geometry"
/* .line 80 */
final String v0 = "com.miui.miwallpaper.saturn"; // const-string v0, "com.miui.miwallpaper.saturn"
/* .line 81 */
final String v0 = "com.miui.miwallpaper.earth"; // const-string v0, "com.miui.miwallpaper.earth"
/* .line 82 */
final String v0 = "com.miui.miwallpaper.mars"; // const-string v0, "com.miui.miwallpaper.mars"
/* .line 83 */
return;
} // .end method
public com.android.server.wallpaper.WallpaperManagerServiceImpl ( ) {
/* .locals 0 */
/* .line 58 */
/* invoke-direct {p0}, Lcom/android/server/wallpaper/WallpaperManagerServiceStub;-><init>()V */
return;
} // .end method
private android.graphics.Bitmap getCurrentWallpaperLocked ( android.graphics.Point p0 ) {
/* .locals 7 */
/* .param p1, "size" # Landroid/graphics/Point; */
/* .line 376 */
final String v0 = "Error getting wallpaper"; // const-string v0, "Error getting wallpaper"
final String v1 = "WallpaperManagerServiceImpl"; // const-string v1, "WallpaperManagerServiceImpl"
int v2 = 0; // const/4 v2, 0x0
/* .line 377 */
/* .local v2, "mBitmap":Landroid/graphics/Bitmap; */
/* invoke-direct {p0}, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->getWallpaper()Landroid/os/ParcelFileDescriptor; */
/* .line 378 */
/* .local v3, "fd":Landroid/os/ParcelFileDescriptor; */
int v4 = 0; // const/4 v4, 0x0
/* if-nez v3, :cond_0 */
/* .line 379 */
/* .line 383 */
} // :cond_0
try { // :try_start_0
/* new-instance v5, Landroid/graphics/BitmapFactory$Options; */
/* invoke-direct {v5}, Landroid/graphics/BitmapFactory$Options;-><init>()V */
/* .line 384 */
/* .local v5, "options":Landroid/graphics/BitmapFactory$Options; */
/* nop */
/* .line 385 */
(( android.os.ParcelFileDescriptor ) v3 ).getFileDescriptor ( ); // invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;
android.graphics.BitmapFactory .decodeFileDescriptor ( v6,v4,v5 );
/* .line 384 */
/* invoke-direct {p0, v4, p1}, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->scaleWallpaperBitmapToScreenSize(Landroid/graphics/Bitmap;Landroid/graphics/Point;)Landroid/graphics/Bitmap; */
/* :try_end_0 */
/* .catch Ljava/lang/OutOfMemoryError; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move-object v2, v4 */
/* .line 391 */
} // .end local v5 # "options":Landroid/graphics/BitmapFactory$Options;
try { // :try_start_1
(( android.os.ParcelFileDescriptor ) v3 ).close ( ); // invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor;->close()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 394 */
} // :goto_0
/* .line 392 */
/* :catch_0 */
/* move-exception v4 */
/* .line 393 */
/* .local v4, "e":Ljava/io/IOException; */
android.util.Slog .w ( v1,v0,v4 );
/* .line 395 */
} // .end local v4 # "e":Ljava/io/IOException;
/* .line 390 */
/* :catchall_0 */
/* move-exception v4 */
/* .line 387 */
/* :catch_1 */
/* move-exception v4 */
/* .line 388 */
/* .local v4, "e":Ljava/lang/OutOfMemoryError; */
try { // :try_start_2
final String v5 = "Can\'t decode file"; // const-string v5, "Can\'t decode file"
android.util.Slog .w ( v1,v5,v4 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 391 */
} // .end local v4 # "e":Ljava/lang/OutOfMemoryError;
try { // :try_start_3
(( android.os.ParcelFileDescriptor ) v3 ).close ( ); // invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_0 */
/* .line 396 */
} // :goto_1
/* .line 391 */
} // :goto_2
try { // :try_start_4
(( android.os.ParcelFileDescriptor ) v3 ).close ( ); // invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor;->close()V
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_2 */
/* .line 394 */
/* .line 392 */
/* :catch_2 */
/* move-exception v5 */
/* .line 393 */
/* .local v5, "e":Ljava/io/IOException; */
android.util.Slog .w ( v1,v0,v5 );
/* .line 395 */
} // .end local v5 # "e":Ljava/io/IOException;
} // :goto_3
/* throw v4 */
} // .end method
private android.graphics.Bitmap getDefaultWallpaperLocked ( android.graphics.Point p0 ) {
/* .locals 6 */
/* .param p1, "size" # Landroid/graphics/Point; */
/* .line 354 */
final String v0 = "Error getting wallpaper"; // const-string v0, "Error getting wallpaper"
final String v1 = "WallpaperManagerServiceImpl"; // const-string v1, "WallpaperManagerServiceImpl"
v2 = this.mContext;
int v3 = 0; // const/4 v3, 0x0
/* if-nez v2, :cond_0 */
/* .line 355 */
} // :cond_0
int v4 = 1; // const/4 v4, 0x1
android.app.WallpaperManager .openDefaultWallpaper ( v2,v4 );
/* .line 356 */
/* .local v2, "is":Ljava/io/InputStream; */
int v4 = 0; // const/4 v4, 0x0
/* .line 357 */
/* .local v4, "mBitmap":Landroid/graphics/Bitmap; */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 359 */
try { // :try_start_0
/* new-instance v5, Landroid/graphics/BitmapFactory$Options; */
/* invoke-direct {v5}, Landroid/graphics/BitmapFactory$Options;-><init>()V */
android.graphics.BitmapFactory .decodeStream ( v2,v3,v5 );
/* invoke-direct {p0, v3, p1}, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->scaleWallpaperBitmapToScreenSize(Landroid/graphics/Bitmap;Landroid/graphics/Point;)Landroid/graphics/Bitmap; */
/* :try_end_0 */
/* .catch Ljava/lang/OutOfMemoryError; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move-object v4, v3 */
/* .line 366 */
try { // :try_start_1
(( java.io.InputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/InputStream;->close()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 369 */
} // :goto_0
/* .line 367 */
/* :catch_0 */
/* move-exception v3 */
/* .line 368 */
/* .local v3, "e":Ljava/io/IOException; */
android.util.Slog .w ( v1,v0,v3 );
/* .line 370 */
} // .end local v3 # "e":Ljava/io/IOException;
/* .line 365 */
/* :catchall_0 */
/* move-exception v3 */
/* .line 361 */
/* :catch_1 */
/* move-exception v3 */
/* .line 362 */
/* .local v3, "e":Ljava/lang/OutOfMemoryError; */
try { // :try_start_2
final String v5 = "Can\'t decode stream"; // const-string v5, "Can\'t decode stream"
android.util.Slog .w ( v1,v5,v3 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 363 */
int v4 = 0; // const/4 v4, 0x0
/* .line 366 */
} // .end local v3 # "e":Ljava/lang/OutOfMemoryError;
try { // :try_start_3
(( java.io.InputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/InputStream;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_0 */
} // :goto_1
try { // :try_start_4
(( java.io.InputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/InputStream;->close()V
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_2 */
/* .line 369 */
/* .line 367 */
/* :catch_2 */
/* move-exception v5 */
/* .line 368 */
/* .local v5, "e":Ljava/io/IOException; */
android.util.Slog .w ( v1,v0,v5 );
/* .line 370 */
} // .end local v5 # "e":Ljava/io/IOException;
} // :goto_2
/* throw v3 */
/* .line 372 */
} // :cond_1
} // :goto_3
} // .end method
private android.os.ParcelFileDescriptor getWallpaper ( ) {
/* .locals 7 */
/* .line 400 */
v0 = this.mService;
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 403 */
} // :cond_0
(( com.android.server.wallpaper.WallpaperManagerService ) v0 ).getLock ( ); // invoke-virtual {v0}, Lcom/android/server/wallpaper/WallpaperManagerService;->getLock()Ljava/lang/Object;
/* monitor-enter v0 */
/* .line 404 */
try { // :try_start_0
v2 = (( com.android.server.wallpaper.WallpaperManagerServiceImpl ) p0 ).getWallpaperUserId ( ); // invoke-virtual {p0}, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->getWallpaperUserId()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 405 */
/* .local v2, "wallpaperUserId":I */
int v3 = 0; // const/4 v3, 0x0
/* .line 408 */
/* .local v3, "mParcelFile":Landroid/os/ParcelFileDescriptor; */
try { // :try_start_1
/* new-instance v4, Ljava/io/File; */
android.os.Environment .getUserSystemDirectory ( v2 );
/* const-string/jumbo v6, "wallpaper" */
/* invoke-direct {v4, v5, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* .line 409 */
/* .local v4, "wallpaperFile":Ljava/io/File; */
v5 = (( java.io.File ) v4 ).exists ( ); // invoke-virtual {v4}, Ljava/io/File;->exists()Z
/* :try_end_1 */
/* .catch Ljava/io/FileNotFoundException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* if-nez v5, :cond_1 */
/* .line 410 */
try { // :try_start_2
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 412 */
} // :cond_1
/* const/high16 v1, 0x10000000 */
try { // :try_start_3
android.os.ParcelFileDescriptor .open ( v4,v1 );
/* :try_end_3 */
/* .catch Ljava/io/FileNotFoundException; {:try_start_3 ..:try_end_3} :catch_0 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 416 */
} // .end local v3 # "mParcelFile":Landroid/os/ParcelFileDescriptor;
/* .local v1, "mParcelFile":Landroid/os/ParcelFileDescriptor; */
/* .line 413 */
} // .end local v1 # "mParcelFile":Landroid/os/ParcelFileDescriptor;
} // .end local v4 # "wallpaperFile":Ljava/io/File;
/* .restart local v3 # "mParcelFile":Landroid/os/ParcelFileDescriptor; */
/* :catch_0 */
/* move-exception v1 */
/* .line 414 */
/* .local v1, "e":Ljava/io/FileNotFoundException; */
try { // :try_start_4
final String v4 = "WallpaperManagerServiceImpl"; // const-string v4, "WallpaperManagerServiceImpl"
final String v5 = "Error getting wallpaper"; // const-string v5, "Error getting wallpaper"
android.util.Slog .w ( v4,v5,v1 );
/* .line 415 */
int v3 = 0; // const/4 v3, 0x0
/* move-object v1, v3 */
/* .line 417 */
} // .end local v3 # "mParcelFile":Landroid/os/ParcelFileDescriptor;
/* .local v1, "mParcelFile":Landroid/os/ParcelFileDescriptor; */
} // :goto_0
/* monitor-exit v0 */
/* .line 418 */
} // .end local v1 # "mParcelFile":Landroid/os/ParcelFileDescriptor;
} // .end local v2 # "wallpaperUserId":I
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* throw v1 */
} // .end method
private android.graphics.drawable.Drawable loadThumbnailWithoutTheme ( android.app.WallpaperInfo p0, android.content.pm.PackageManager p1 ) {
/* .locals 4 */
/* .param p1, "info" # Landroid/app/WallpaperInfo; */
/* .param p2, "pm" # Landroid/content/pm/PackageManager; */
/* .line 454 */
int v0 = 0; // const/4 v0, 0x0
/* .line 455 */
/* .local v0, "dr":Landroid/graphics/drawable/Drawable; */
int v1 = 0; // const/4 v1, 0x0
/* .line 456 */
/* .local v1, "thumbnailResource":I */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 457 */
v1 = (( android.app.WallpaperInfo ) p1 ).getThumbnailResource ( ); // invoke-virtual {p1}, Landroid/app/WallpaperInfo;->getThumbnailResource()I
/* .line 460 */
} // :cond_0
/* if-ltz v1, :cond_1 */
/* .line 461 */
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 462 */
(( android.app.WallpaperInfo ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Landroid/app/WallpaperInfo;->getPackageName()Ljava/lang/String;
/* .line 463 */
/* .local v2, "packageName":Ljava/lang/String; */
/* nop */
/* .line 464 */
(( android.app.WallpaperInfo ) p1 ).getServiceInfo ( ); // invoke-virtual {p1}, Landroid/app/WallpaperInfo;->getServiceInfo()Landroid/content/pm/ServiceInfo;
v3 = this.applicationInfo;
/* .line 463 */
(( android.content.pm.PackageManager ) p2 ).getDrawable ( v2, v1, v3 ); // invoke-virtual {p2, v2, v1, v3}, Landroid/content/pm/PackageManager;->getDrawable(Ljava/lang/String;ILandroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;
/* .line 467 */
} // .end local v2 # "packageName":Ljava/lang/String;
} // :cond_1
} // .end method
private void notifyBlurCallbacksLocked ( com.android.server.wallpaper.WallpaperData p0 ) {
/* .locals 4 */
/* .param p1, "wallpaper" # Lcom/android/server/wallpaper/WallpaperData; */
/* .line 514 */
(( com.android.server.wallpaper.WallpaperData ) p1 ).getCallbacks ( ); // invoke-virtual {p1}, Lcom/android/server/wallpaper/WallpaperData;->getCallbacks()Landroid/os/RemoteCallbackList;
/* .line 515 */
/* .local v0, "callbacks":Landroid/os/RemoteCallbackList;, "Landroid/os/RemoteCallbackList<Landroid/app/IWallpaperManagerCallback;>;" */
v1 = (( android.os.RemoteCallbackList ) v0 ).beginBroadcast ( ); // invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
/* .line 516 */
/* .local v1, "n":I */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
/* if-ge v2, v1, :cond_0 */
/* .line 518 */
try { // :try_start_0
(( android.os.RemoteCallbackList ) v0 ).getBroadcastItem ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;
/* check-cast v3, Landroid/app/IWallpaperManagerCallback; */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 522 */
/* .line 519 */
/* :catch_0 */
/* move-exception v3 */
/* .line 516 */
} // :goto_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 524 */
} // .end local v2 # "i":I
} // :cond_0
(( android.os.RemoteCallbackList ) v0 ).finishBroadcast ( ); // invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
/* .line 525 */
return;
} // .end method
private void saveBlurWallpaperBitmapLocked ( android.graphics.Bitmap p0, java.lang.String p1 ) {
/* .locals 11 */
/* .param p1, "blurImg" # Landroid/graphics/Bitmap; */
/* .param p2, "fileName" # Ljava/lang/String; */
/* .line 297 */
int v0 = 0; // const/4 v0, 0x0
/* .line 298 */
/* .local v0, "mWallpaperStream":Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream; */
final String v1 = "saveBlurWallpaperBitmapLocked begin"; // const-string v1, "saveBlurWallpaperBitmapLocked begin"
final String v2 = "WallpaperManagerServiceImpl"; // const-string v2, "WallpaperManagerServiceImpl"
android.util.Slog .d ( v2,v1 );
/* .line 299 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v3 */
/* .line 301 */
/* .local v3, "timeBegin":J */
try { // :try_start_0
v1 = (( com.android.server.wallpaper.WallpaperManagerServiceImpl ) p0 ).getWallpaperUserId ( ); // invoke-virtual {p0}, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->getWallpaperUserId()I
android.os.Environment .getUserSystemDirectory ( v1 );
/* .line 302 */
/* .local v1, "dir":Ljava/io/File; */
v5 = (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
/* if-nez v5, :cond_0 */
v5 = (( java.io.File ) v1 ).mkdir ( ); // invoke-virtual {v1}, Ljava/io/File;->mkdir()Z
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 303 */
(( java.io.File ) v1 ).getPath ( ); // invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;
/* const/16 v6, 0x1f9 */
int v7 = -1; // const/4 v7, -0x1
android.os.FileUtils .setPermissions ( v5,v6,v7,v7 );
/* .line 306 */
} // :cond_0
/* new-instance v5, Ljava/io/File; */
/* invoke-direct {v5, v1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* .line 307 */
/* .local v5, "file":Ljava/io/File; */
/* const/high16 v6, 0x38000000 */
android.os.ParcelFileDescriptor .open ( v5,v6 );
/* .line 309 */
/* .local v6, "fd":Landroid/os/ParcelFileDescriptor; */
v7 = android.os.SELinux .restorecon ( v5 );
/* :try_end_0 */
/* .catch Ljava/io/FileNotFoundException; {:try_start_0 ..:try_end_0} :catch_7 */
if ( v7 != null) { // if-eqz v7, :cond_4
/* .line 311 */
try { // :try_start_1
/* new-instance v7, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream; */
/* invoke-direct {v7, v6}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V */
/* move-object v0, v7 */
/* .line 312 */
v7 = android.graphics.Bitmap$CompressFormat.PNG;
/* const/16 v8, 0x5a */
(( android.graphics.Bitmap ) p1 ).compress ( v7, v8, v0 ); // invoke-virtual {p1, v7, v8, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 315 */
try { // :try_start_2
(( android.os.ParcelFileDescriptor$AutoCloseOutputStream ) v0 ).close ( ); // invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;->close()V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 317 */
/* .line 316 */
/* :catch_0 */
/* move-exception v7 */
/* .line 319 */
} // :goto_0
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 321 */
try { // :try_start_3
(( android.os.ParcelFileDescriptor ) v6 ).close ( ); // invoke-virtual {v6}, Landroid/os/ParcelFileDescriptor;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_1 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 323 */
/* .line 322 */
/* :catch_1 */
/* move-exception v7 */
/* .line 325 */
} // :cond_1
} // :goto_1
try { // :try_start_4
java.lang.System .currentTimeMillis ( );
/* move-result-wide v7 */
/* sub-long/2addr v7, v3 */
/* .line 326 */
/* .local v7, "takenTime":J */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "saveBlurWallpaperBitmapLocked takenTime = "; // const-string v10, "saveBlurWallpaperBitmapLocked takenTime = "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v7, v8 ); // invoke-virtual {v9, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v9 );
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* .line 328 */
/* nop */
/* .line 330 */
} // .end local v7 # "takenTime":J
try { // :try_start_5
(( android.os.ParcelFileDescriptor$AutoCloseOutputStream ) v0 ).close ( ); // invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;->close()V
/* :try_end_5 */
/* .catch Ljava/io/IOException; {:try_start_5 ..:try_end_5} :catch_2 */
/* .catch Ljava/io/FileNotFoundException; {:try_start_5 ..:try_end_5} :catch_7 */
/* .line 332 */
/* .line 331 */
/* :catch_2 */
/* move-exception v7 */
/* .line 334 */
} // :goto_2
if ( v6 != null) { // if-eqz v6, :cond_4
/* .line 336 */
try { // :try_start_6
(( android.os.ParcelFileDescriptor ) v6 ).close ( ); // invoke-virtual {v6}, Landroid/os/ParcelFileDescriptor;->close()V
/* :try_end_6 */
/* .catch Ljava/io/IOException; {:try_start_6 ..:try_end_6} :catch_3 */
/* .catch Ljava/io/FileNotFoundException; {:try_start_6 ..:try_end_6} :catch_7 */
/* .line 338 */
} // :goto_3
/* .line 337 */
/* :catch_3 */
/* move-exception v7 */
/* .line 328 */
/* :catchall_0 */
/* move-exception v7 */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 330 */
try { // :try_start_7
(( android.os.ParcelFileDescriptor$AutoCloseOutputStream ) v0 ).close ( ); // invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;->close()V
/* :try_end_7 */
/* .catch Ljava/io/IOException; {:try_start_7 ..:try_end_7} :catch_4 */
/* .catch Ljava/io/FileNotFoundException; {:try_start_7 ..:try_end_7} :catch_7 */
/* .line 332 */
/* .line 331 */
/* :catch_4 */
/* move-exception v8 */
/* .line 334 */
} // :cond_2
} // :goto_4
if ( v6 != null) { // if-eqz v6, :cond_3
/* .line 336 */
try { // :try_start_8
(( android.os.ParcelFileDescriptor ) v6 ).close ( ); // invoke-virtual {v6}, Landroid/os/ParcelFileDescriptor;->close()V
/* :try_end_8 */
/* .catch Ljava/io/IOException; {:try_start_8 ..:try_end_8} :catch_5 */
/* .catch Ljava/io/FileNotFoundException; {:try_start_8 ..:try_end_8} :catch_7 */
/* .line 338 */
/* .line 337 */
/* :catch_5 */
/* move-exception v8 */
/* .line 340 */
} // :cond_3
} // :goto_5
/* nop */
} // .end local v0 # "mWallpaperStream":Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;
} // .end local v3 # "timeBegin":J
} // .end local p0 # "this":Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;
} // .end local p1 # "blurImg":Landroid/graphics/Bitmap;
} // .end local p2 # "fileName":Ljava/lang/String;
try { // :try_start_9
/* throw v7 */
/* :try_end_9 */
/* .catch Ljava/io/FileNotFoundException; {:try_start_9 ..:try_end_9} :catch_7 */
/* .line 342 */
/* .restart local v0 # "mWallpaperStream":Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream; */
/* .restart local v3 # "timeBegin":J */
/* .restart local p0 # "this":Lcom/android/server/wallpaper/WallpaperManagerServiceImpl; */
/* .restart local p1 # "blurImg":Landroid/graphics/Bitmap; */
/* .restart local p2 # "fileName":Ljava/lang/String; */
} // :cond_4
} // :goto_6
if ( v6 != null) { // if-eqz v6, :cond_5
/* .line 344 */
try { // :try_start_a
(( android.os.ParcelFileDescriptor ) v6 ).close ( ); // invoke-virtual {v6}, Landroid/os/ParcelFileDescriptor;->close()V
/* :try_end_a */
/* .catch Ljava/io/IOException; {:try_start_a ..:try_end_a} :catch_6 */
/* .catch Ljava/io/FileNotFoundException; {:try_start_a ..:try_end_a} :catch_7 */
/* .line 346 */
/* .line 345 */
/* :catch_6 */
/* move-exception v2 */
/* .line 350 */
} // .end local v1 # "dir":Ljava/io/File;
} // .end local v5 # "file":Ljava/io/File;
} // :cond_5
} // :goto_7
/* .line 348 */
} // .end local v6 # "fd":Landroid/os/ParcelFileDescriptor;
/* :catch_7 */
/* move-exception v1 */
/* .line 349 */
/* .local v1, "e":Ljava/io/FileNotFoundException; */
final String v5 = "Error setting wallpaper"; // const-string v5, "Error setting wallpaper"
android.util.Slog .w ( v2,v5,v1 );
/* .line 351 */
} // .end local v1 # "e":Ljava/io/FileNotFoundException;
} // :goto_8
return;
} // .end method
private android.graphics.Bitmap scaleWallpaperBitmapToScreenSize ( android.graphics.Bitmap p0, android.graphics.Point p1 ) {
/* .locals 18 */
/* .param p1, "bitmap" # Landroid/graphics/Bitmap; */
/* .param p2, "size" # Landroid/graphics/Point; */
/* .line 423 */
/* move-object/from16 v7, p2 */
/* if-nez p1, :cond_0 */
/* .line 424 */
int v0 = 0; // const/4 v0, 0x0
/* .line 427 */
} // :cond_0
/* new-instance v0, Landroid/graphics/Matrix; */
/* invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V */
/* move-object v8, v0 */
/* .line 428 */
/* .local v8, "m":Landroid/graphics/Matrix; */
v9 = /* invoke-virtual/range {p1 ..p1}, Landroid/graphics/Bitmap;->getHeight()I */
/* .line 429 */
/* .local v9, "bitmap_height":I */
v10 = /* invoke-virtual/range {p1 ..p1}, Landroid/graphics/Bitmap;->getWidth()I */
/* .line 430 */
/* .local v10, "bitmap_width":I */
v11 = java.lang.Math .max ( v9,v10 );
/* .line 431 */
/* .local v11, "bitmapLongSideLength":I */
v12 = java.lang.Math .min ( v9,v10 );
/* .line 432 */
/* .local v12, "bitmapShortSideLength":I */
/* iget v0, v7, Landroid/graphics/Point;->x:I */
/* iget v1, v7, Landroid/graphics/Point;->y:I */
v13 = java.lang.Math .max ( v0,v1 );
/* .line 433 */
/* .local v13, "longSideLength":I */
/* iget v0, v7, Landroid/graphics/Point;->x:I */
/* iget v1, v7, Landroid/graphics/Point;->y:I */
v14 = java.lang.Math .min ( v0,v1 );
/* .line 435 */
/* .local v14, "shortSideLength":I */
if ( v13 != null) { // if-eqz v13, :cond_3
if ( v14 != null) { // if-eqz v14, :cond_3
/* if-eq v11, v13, :cond_3 */
/* .line 436 */
/* mul-int/lit8 v0, v14, 0x2 */
/* if-ne v11, v0, :cond_1 */
/* if-ne v12, v13, :cond_1 */
/* .line 437 */
/* .line 441 */
} // :cond_1
/* int-to-float v0, v11 */
/* int-to-float v1, v14 */
/* div-float/2addr v0, v1 */
/* int-to-float v1, v12 */
/* int-to-float v2, v13 */
/* div-float/2addr v1, v2 */
/* div-float/2addr v0, v1 */
/* const/high16 v1, 0x40000000 # 2.0f */
/* cmpl-float v0, v0, v1 */
/* if-nez v0, :cond_2 */
/* .line 442 */
/* mul-int/lit8 v0, v13, 0x2 */
} // :cond_2
/* move v0, v13 */
} // :goto_0
/* move v15, v0 */
/* .line 443 */
/* .local v15, "sidelength":I */
/* int-to-float v0, v15 */
/* int-to-float v1, v11 */
/* div-float v6, v0, v1 */
/* .line 444 */
/* .local v6, "scale":F */
(( android.graphics.Matrix ) v8 ).setScale ( v6, v6 ); // invoke-virtual {v8, v6, v6}, Landroid/graphics/Matrix;->setScale(FF)V
/* .line 445 */
int v1 = 0; // const/4 v1, 0x0
int v2 = 0; // const/4 v2, 0x0
/* const/16 v16, 0x1 */
/* move-object/from16 v0, p1 */
/* move v3, v10 */
/* move v4, v9 */
/* move-object v5, v8 */
/* move/from16 v17, v6 */
} // .end local v6 # "scale":F
/* .local v17, "scale":F */
/* move/from16 v6, v16 */
/* invoke-static/range {v0 ..v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap; */
/* .line 446 */
} // .end local v15 # "sidelength":I
} // .end local v17 # "scale":F
/* .local v0, "bmp":Landroid/graphics/Bitmap; */
/* .line 447 */
} // .end local v0 # "bmp":Landroid/graphics/Bitmap;
} // :cond_3
/* move-object/from16 v0, p1 */
/* .line 450 */
/* .restart local v0 # "bmp":Landroid/graphics/Bitmap; */
} // :goto_1
} // .end method
/* # virtual methods */
public void bindWallpaperComponentLocked ( com.android.server.wallpaper.WallpaperManagerService p0, com.android.server.wallpaper.WallpaperData p1, android.content.ComponentName p2, android.os.IRemoteCallback$Stub p3 ) {
/* .locals 10 */
/* .param p1, "service" # Lcom/android/server/wallpaper/WallpaperManagerService; */
/* .param p2, "wallpaper" # Lcom/android/server/wallpaper/WallpaperData; */
/* .param p3, "imageWallpaper" # Landroid/content/ComponentName; */
/* .param p4, "callback" # Landroid/os/IRemoteCallback$Stub; */
/* .line 112 */
if ( p1 != null) { // if-eqz p1, :cond_2
if ( p2 != null) { // if-eqz p2, :cond_2
v0 = this.connection;
/* if-nez v0, :cond_0 */
/* .line 117 */
} // :cond_0
v0 = (( com.android.server.wallpaper.WallpaperManagerServiceImpl ) p0 ).isMiuiWallpaperComponent ( p3 ); // invoke-virtual {p0, p3}, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->isMiuiWallpaperComponent(Landroid/content/ComponentName;)Z
/* .line 118 */
/* .local v0, "isMiuiWallpaper":Z */
v1 = com.android.server.wallpaper.WallpaperManagerServiceProxy.changingToSame;
/* filled-new-array {p3, p2}, [Ljava/lang/Object; */
/* .line 119 */
(( com.xiaomi.reflect.RefMethod ) v1 ).invoke ( p1, v2 ); // invoke-virtual {v1, p1, v2}, Lcom/xiaomi/reflect/RefMethod;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/Boolean; */
v1 = (( java.lang.Boolean ) v1 ).booleanValue ( ); // invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
/* .line 120 */
/* .local v1, "changeToSame":Z */
if ( v0 != null) { // if-eqz v0, :cond_1
if ( v1 != null) { // if-eqz v1, :cond_1
v2 = this.connection;
v2 = this.mService;
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 121 */
v2 = com.android.server.wallpaper.WallpaperManagerServiceProxy.notifyWallpaperChanged;
/* filled-new-array {p2}, [Ljava/lang/Object; */
(( com.xiaomi.reflect.RefMethod ) v2 ).invoke ( p1, v3 ); // invoke-virtual {v2, p1, v3}, Lcom/xiaomi/reflect/RefMethod;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* .line 124 */
} // :cond_1
int v6 = 1; // const/4 v6, 0x1
int v7 = 0; // const/4 v7, 0x0
/* move-object v4, p1 */
/* move-object v5, p3 */
/* move-object v8, p2 */
/* move-object v9, p4 */
/* invoke-virtual/range {v4 ..v9}, Lcom/android/server/wallpaper/WallpaperManagerService;->bindWallpaperComponentLocked(Landroid/content/ComponentName;ZZLcom/android/server/wallpaper/WallpaperData;Landroid/os/IRemoteCallback;)Z */
/* .line 127 */
} // :goto_0
return;
/* .line 113 */
} // .end local v0 # "isMiuiWallpaper":Z
} // .end local v1 # "changeToSame":Z
} // :cond_2
} // :goto_1
final String v0 = "WallpaperManagerServiceImpl"; // const-string v0, "WallpaperManagerServiceImpl"
final String v1 = "bindWallpaperComponentWhenFileChanged receive unexpected param"; // const-string v1, "bindWallpaperComponentWhenFileChanged receive unexpected param"
android.util.Slog .e ( v0,v1 );
/* .line 114 */
return;
} // .end method
public void checkAndConfigFrameworkVersionToWallpaper ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 97 */
/* if-nez p1, :cond_0 */
/* .line 98 */
final String v0 = "WallpaperManagerServiceImpl"; // const-string v0, "WallpaperManagerServiceImpl"
final String v1 = "checkAndConfigFrameworkVersionToWallpaper: context null"; // const-string v1, "checkAndConfigFrameworkVersionToWallpaper: context null"
android.util.Slog .e ( v0,v1 );
/* .line 99 */
return;
/* .line 101 */
} // :cond_0
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v1 = -1; // const/4 v1, -0x1
final String v2 = "framework_version_to_wallpaper"; // const-string v2, "framework_version_to_wallpaper"
v0 = android.provider.Settings$Global .getInt ( v0,v2,v1 );
/* .line 103 */
/* .local v0, "version":I */
int v1 = 1; // const/4 v1, 0x1
/* if-eq v0, v1, :cond_1 */
/* .line 104 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$Global .putInt ( v3,v2,v1 );
/* .line 107 */
} // :cond_1
return;
} // .end method
public void createBlurWallpaper ( Boolean p0 ) {
/* .locals 11 */
/* .param p1, "force" # Z */
/* .line 215 */
v0 = android.util.MiuiAppSizeCompatModeStub .get ( );
/* if-nez v0, :cond_0 */
v0 = com.android.server.wm.MiuiEmbeddingWindowServiceStubHead .isActivityEmbeddingEnable ( );
if ( v0 != null) { // if-eqz v0, :cond_1
} // :cond_0
v0 = this.mService;
/* if-nez v0, :cond_2 */
/* .line 217 */
} // :cond_1
final String v0 = "WallpaperManagerServiceImpl"; // const-string v0, "WallpaperManagerServiceImpl"
/* const-string/jumbo v1, "skip createBlurBitmap" */
android.util.Slog .d ( v0,v1 );
/* .line 218 */
return;
/* .line 221 */
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
/* .line 223 */
/* .local v0, "bitmap":Landroid/graphics/Bitmap; */
/* new-instance v1, Landroid/graphics/Point; */
/* invoke-direct {v1}, Landroid/graphics/Point;-><init>()V */
/* .line 225 */
/* .local v1, "size":Landroid/graphics/Point; */
try { // :try_start_0
/* const-string/jumbo v2, "window" */
/* .line 226 */
android.os.ServiceManager .checkService ( v2 );
/* .line 225 */
android.view.IWindowManager$Stub .asInterface ( v2 );
/* .line 226 */
int v3 = 0; // const/4 v3, 0x0
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 229 */
/* .line 227 */
/* :catch_0 */
/* move-exception v2 */
/* .line 228 */
/* .local v2, "e":Landroid/os/RemoteException; */
final String v3 = "WallpaperManagerServiceImpl"; // const-string v3, "WallpaperManagerServiceImpl"
final String v4 = "IWindowManager null"; // const-string v4, "IWindowManager null"
android.util.Slog .e ( v3,v4 );
/* .line 231 */
} // .end local v2 # "e":Landroid/os/RemoteException;
} // :goto_0
int v2 = 0; // const/4 v2, 0x0
/* .line 232 */
/* .local v2, "isColorBackgournd":Z */
v3 = this.mService;
(( com.android.server.wallpaper.WallpaperManagerService ) v3 ).getLock ( ); // invoke-virtual {v3}, Lcom/android/server/wallpaper/WallpaperManagerService;->getLock()Ljava/lang/Object;
/* monitor-enter v3 */
/* .line 233 */
try { // :try_start_1
v4 = (( com.android.server.wallpaper.WallpaperManagerServiceImpl ) p0 ).getWallpaperUserId ( ); // invoke-virtual {p0}, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->getWallpaperUserId()I
/* .line 234 */
/* .local v4, "wallpaperUserId":I */
/* if-nez p1, :cond_3 */
/* .line 235 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
android.os.Environment .getUserSystemDirectory ( v4 );
/* .line 236 */
(( java.io.File ) v6 ).getAbsolutePath ( ); // invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = "/blurwallpaper"; // const-string v6, "/blurwallpaper"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 237 */
/* .local v5, "blurWallpaperPathName":Ljava/lang/String; */
final String v6 = "WallpaperManagerServiceImpl"; // const-string v6, "WallpaperManagerServiceImpl"
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "createBlurWallpaper blurWallpaperPathName = "; // const-string v8, "createBlurWallpaper blurWallpaperPathName = "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v5 ); // invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v6,v7 );
/* .line 238 */
/* new-instance v6, Ljava/io/File; */
android.os.Environment .getUserSystemDirectory ( v4 );
final String v8 = "blurwallpaper"; // const-string v8, "blurwallpaper"
/* invoke-direct {v6, v7, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* .line 239 */
v6 = (( java.io.File ) v6 ).exists ( ); // invoke-virtual {v6}, Ljava/io/File;->exists()Z
if ( v6 != null) { // if-eqz v6, :cond_3
/* .line 240 */
/* monitor-exit v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
return;
/* .line 245 */
} // .end local v5 # "blurWallpaperPathName":Ljava/lang/String;
} // :cond_3
try { // :try_start_2
v5 = this.mService;
(( com.android.server.wallpaper.WallpaperManagerService ) v5 ).getWallpaperInfo ( v4 ); // invoke-virtual {v5, v4}, Lcom/android/server/wallpaper/WallpaperManagerService;->getWallpaperInfo(I)Landroid/app/WallpaperInfo;
/* .line 246 */
/* .local v5, "info":Landroid/app/WallpaperInfo; */
if ( v5 != null) { // if-eqz v5, :cond_5
v6 = this.mContext;
if ( v6 != null) { // if-eqz v6, :cond_5
/* .line 247 */
(( android.content.Context ) v6 ).getPackageManager ( ); // invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 248 */
/* .local v6, "pm":Landroid/content/pm/PackageManager; */
/* invoke-direct {p0, v5, v6}, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->loadThumbnailWithoutTheme(Landroid/app/WallpaperInfo;Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable; */
/* .line 249 */
/* .local v7, "dr":Landroid/graphics/drawable/Drawable; */
if ( v7 != null) { // if-eqz v7, :cond_4
/* instance-of v8, v7, Landroid/graphics/drawable/BitmapDrawable; */
if ( v8 != null) { // if-eqz v8, :cond_4
/* .line 251 */
v8 = android.graphics.Bitmap$Config.RGB_565;
/* .line 252 */
/* .local v8, "mBitmapConfig":Landroid/graphics/Bitmap$Config; */
/* nop */
/* .line 253 */
v9 = (( android.graphics.drawable.Drawable ) v7 ).getIntrinsicWidth ( ); // invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I
v10 = (( android.graphics.drawable.Drawable ) v7 ).getIntrinsicHeight ( ); // invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I
/* .line 252 */
android.graphics.Bitmap .createBitmap ( v9,v10,v8 );
/* invoke-direct {p0, v9, v1}, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->scaleWallpaperBitmapToScreenSize(Landroid/graphics/Bitmap;Landroid/graphics/Point;)Landroid/graphics/Bitmap; */
/* move-object v0, v9 */
/* .line 255 */
/* const/high16 v9, -0x1000000 */
(( android.graphics.Bitmap ) v0 ).eraseColor ( v9 ); // invoke-virtual {v0, v9}, Landroid/graphics/Bitmap;->eraseColor(I)V
/* .line 256 */
int v2 = 1; // const/4 v2, 0x1
/* .line 258 */
} // .end local v6 # "pm":Landroid/content/pm/PackageManager;
} // .end local v7 # "dr":Landroid/graphics/drawable/Drawable;
} // .end local v8 # "mBitmapConfig":Landroid/graphics/Bitmap$Config;
} // :cond_4
/* .line 259 */
} // :cond_5
/* invoke-direct {p0, v1}, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->getCurrentWallpaperLocked(Landroid/graphics/Point;)Landroid/graphics/Bitmap; */
/* :try_end_2 */
/* .catch Ljava/lang/RuntimeException; {:try_start_2 ..:try_end_2} :catch_2 */
/* .catch Ljava/lang/OutOfMemoryError; {:try_start_2 ..:try_end_2} :catch_1 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* move-object v0, v6 */
/* .line 267 */
} // :goto_1
/* .line 264 */
} // .end local v5 # "info":Landroid/app/WallpaperInfo;
/* :catch_1 */
/* move-exception v5 */
/* .line 265 */
/* .local v5, "e":Ljava/lang/OutOfMemoryError; */
try { // :try_start_3
final String v6 = "WallpaperManagerServiceImpl"; // const-string v6, "WallpaperManagerServiceImpl"
final String v7 = "No memory load current wallpaper"; // const-string v7, "No memory load current wallpaper"
android.util.Slog .w ( v6,v7,v5 );
/* .line 266 */
int v0 = 0; // const/4 v0, 0x0
/* .line 261 */
} // .end local v5 # "e":Ljava/lang/OutOfMemoryError;
/* :catch_2 */
/* move-exception v5 */
/* .line 262 */
/* .local v5, "e":Ljava/lang/RuntimeException; */
final String v6 = "WallpaperManagerServiceImpl"; // const-string v6, "WallpaperManagerServiceImpl"
final String v7 = "create blur wallpaper draw has some errors"; // const-string v7, "create blur wallpaper draw has some errors"
android.util.Slog .e ( v6,v7 );
/* .line 263 */
int v0 = 0; // const/4 v0, 0x0
/* .line 267 */
} // .end local v5 # "e":Ljava/lang/RuntimeException;
/* nop */
/* .line 268 */
} // .end local v4 # "wallpaperUserId":I
} // :goto_2
/* monitor-exit v3 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* .line 270 */
if ( v0 != null) { // if-eqz v0, :cond_6
try { // :try_start_4
v3 = (( android.graphics.Bitmap ) v0 ).isRecycled ( ); // invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z
if ( v3 != null) { // if-eqz v3, :cond_7
/* .line 271 */
} // :cond_6
final String v3 = "WallpaperManagerServiceImpl"; // const-string v3, "WallpaperManagerServiceImpl"
/* const-string/jumbo v4, "wallpaper is null or has been recycled" */
android.util.Slog .d ( v3,v4 );
/* .line 272 */
/* invoke-direct {p0, v1}, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->getDefaultWallpaperLocked(Landroid/graphics/Point;)Landroid/graphics/Bitmap; */
/* move-object v0, v3 */
/* .line 275 */
} // :cond_7
if ( v0 != null) { // if-eqz v0, :cond_9
/* .line 276 */
if ( v2 != null) { // if-eqz v2, :cond_8
/* .line 277 */
final String v3 = "blurwallpaper"; // const-string v3, "blurwallpaper"
/* invoke-direct {p0, v0, v3}, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->saveBlurWallpaperBitmapLocked(Landroid/graphics/Bitmap;Ljava/lang/String;)V */
/* .line 279 */
} // :cond_8
/* const/16 v3, 0xc8 */
com.android.server.BlurUtils .stackBlur ( v0,v3 );
/* .line 280 */
/* .local v3, "bmp":Landroid/graphics/Bitmap; */
if ( v3 != null) { // if-eqz v3, :cond_9
/* .line 281 */
(( android.graphics.Bitmap ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V
/* .line 282 */
/* const/high16 v4, 0x3f000000 # 0.5f */
com.android.server.BlurUtils .addBlackBoard ( v3,v4 );
/* .line 283 */
/* .local v4, "blurBitmap":Landroid/graphics/Bitmap; */
(( android.graphics.Bitmap ) v3 ).recycle ( ); // invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V
/* .line 284 */
final String v5 = "blurwallpaper"; // const-string v5, "blurwallpaper"
/* invoke-direct {p0, v4, v5}, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->saveBlurWallpaperBitmapLocked(Landroid/graphics/Bitmap;Ljava/lang/String;)V */
/* :try_end_4 */
/* .catch Ljava/lang/OutOfMemoryError; {:try_start_4 ..:try_end_4} :catch_3 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* .line 291 */
} // .end local v3 # "bmp":Landroid/graphics/Bitmap;
} // .end local v4 # "blurBitmap":Landroid/graphics/Bitmap;
} // :cond_9
} // :goto_3
if ( v0 != null) { // if-eqz v0, :cond_a
} // :goto_4
int v0 = 0; // const/4 v0, 0x0
/* :catchall_0 */
/* move-exception v3 */
/* .line 288 */
/* :catch_3 */
/* move-exception v3 */
/* .line 289 */
/* .local v3, "e":Ljava/lang/OutOfMemoryError; */
try { // :try_start_5
final String v4 = "WallpaperManagerServiceImpl"; // const-string v4, "WallpaperManagerServiceImpl"
final String v5 = "No memory load current wallpaper"; // const-string v5, "No memory load current wallpaper"
android.util.Slog .w ( v4,v5,v3 );
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_0 */
/* .line 291 */
/* nop */
} // .end local v3 # "e":Ljava/lang/OutOfMemoryError;
if ( v0 != null) { // if-eqz v0, :cond_a
/* .line 293 */
} // :cond_a
} // :goto_5
return;
/* .line 291 */
} // :goto_6
if ( v0 != null) { // if-eqz v0, :cond_b
int v0 = 0; // const/4 v0, 0x0
/* .line 292 */
} // :cond_b
/* throw v3 */
/* .line 268 */
/* :catchall_1 */
/* move-exception v4 */
try { // :try_start_6
/* monitor-exit v3 */
/* :try_end_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_1 */
/* throw v4 */
} // .end method
public android.os.ParcelFileDescriptor getBlurWallpaper ( android.app.IWallpaperManagerCallback p0 ) {
/* .locals 7 */
/* .param p1, "cb" # Landroid/app/IWallpaperManagerCallback; */
/* .line 471 */
v0 = this.mService;
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 473 */
} // :cond_0
(( com.android.server.wallpaper.WallpaperManagerService ) v0 ).getLock ( ); // invoke-virtual {v0}, Lcom/android/server/wallpaper/WallpaperManagerService;->getLock()Ljava/lang/Object;
/* monitor-enter v0 */
/* .line 474 */
try { // :try_start_0
v2 = (( com.android.server.wallpaper.WallpaperManagerServiceImpl ) p0 ).getWallpaperUserId ( ); // invoke-virtual {p0}, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->getWallpaperUserId()I
/* .line 475 */
/* .local v2, "wallpaperUserId":I */
v3 = this.mService;
/* .line 476 */
(( com.android.server.wallpaper.WallpaperManagerService ) v3 ).getWallpaperMap ( ); // invoke-virtual {v3}, Lcom/android/server/wallpaper/WallpaperManagerService;->getWallpaperMap()Landroid/util/SparseArray;
(( android.util.SparseArray ) v3 ).get ( v2 ); // invoke-virtual {v3, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v3, Lcom/android/server/wallpaper/WallpaperData; */
/* .line 477 */
/* .local v3, "wallpaper":Lcom/android/server/wallpaper/WallpaperData; */
if ( v3 != null) { // if-eqz v3, :cond_3
(( com.android.server.wallpaper.WallpaperData ) v3 ).getCallbacks ( ); // invoke-virtual {v3}, Lcom/android/server/wallpaper/WallpaperData;->getCallbacks()Landroid/os/RemoteCallbackList;
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 478 */
if ( p1 != null) { // if-eqz p1, :cond_1
(( com.android.server.wallpaper.WallpaperData ) v3 ).getCallbacks ( ); // invoke-virtual {v3}, Lcom/android/server/wallpaper/WallpaperData;->getCallbacks()Landroid/os/RemoteCallbackList;
v4 = (( android.os.RemoteCallbackList ) v4 ).isContainIBinder ( p1 ); // invoke-virtual {v4, p1}, Landroid/os/RemoteCallbackList;->isContainIBinder(Landroid/os/IInterface;)Z
/* if-nez v4, :cond_1 */
/* .line 479 */
(( com.android.server.wallpaper.WallpaperData ) v3 ).getCallbacks ( ); // invoke-virtual {v3}, Lcom/android/server/wallpaper/WallpaperData;->getCallbacks()Landroid/os/RemoteCallbackList;
(( android.os.RemoteCallbackList ) v4 ).register ( p1 ); // invoke-virtual {v4, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 482 */
} // :cond_1
try { // :try_start_1
/* new-instance v4, Ljava/io/File; */
android.os.Environment .getUserSystemDirectory ( v2 );
final String v6 = "blurwallpaper"; // const-string v6, "blurwallpaper"
/* invoke-direct {v4, v5, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* .line 484 */
/* .local v4, "f":Ljava/io/File; */
v5 = (( java.io.File ) v4 ).exists ( ); // invoke-virtual {v4}, Ljava/io/File;->exists()Z
/* :try_end_1 */
/* .catch Ljava/io/FileNotFoundException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* if-nez v5, :cond_2 */
/* .line 485 */
try { // :try_start_2
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 487 */
} // :cond_2
/* const/high16 v5, 0x10000000 */
try { // :try_start_3
android.os.ParcelFileDescriptor .open ( v4,v5 );
/* :try_end_3 */
/* .catch Ljava/io/FileNotFoundException; {:try_start_3 ..:try_end_3} :catch_0 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 488 */
/* .local v1, "mParcelFile":Landroid/os/ParcelFileDescriptor; */
try { // :try_start_4
/* monitor-exit v0 */
/* .line 489 */
} // .end local v1 # "mParcelFile":Landroid/os/ParcelFileDescriptor;
} // .end local v4 # "f":Ljava/io/File;
/* :catch_0 */
/* move-exception v4 */
/* .line 490 */
/* .local v4, "e":Ljava/io/FileNotFoundException; */
final String v5 = "WallpaperManagerServiceImpl"; // const-string v5, "WallpaperManagerServiceImpl"
final String v6 = "Error getting wallpaper"; // const-string v6, "Error getting wallpaper"
android.util.Slog .w ( v5,v6,v4 );
/* .line 491 */
/* monitor-exit v0 */
/* .line 494 */
} // .end local v4 # "e":Ljava/io/FileNotFoundException;
} // :cond_3
/* monitor-exit v0 */
/* .line 495 */
} // .end local v2 # "wallpaperUserId":I
} // .end local v3 # "wallpaper":Lcom/android/server/wallpaper/WallpaperData;
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* throw v1 */
} // .end method
public Integer getWallpaperUserId ( ) {
/* .locals 2 */
/* .line 504 */
v0 = android.os.Binder .getCallingUid ( );
/* .line 505 */
/* .local v0, "callingUid":I */
/* const/16 v1, 0x3e8 */
/* if-ne v0, v1, :cond_0 */
v1 = this.mService;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 506 */
v0 = (( com.android.server.wallpaper.WallpaperManagerService ) v1 ).getCurrentUserId ( ); // invoke-virtual {v1}, Lcom/android/server/wallpaper/WallpaperManagerService;->getCurrentUserId()I
/* .line 508 */
} // :cond_0
v0 = android.os.UserHandle .getUserId ( v0 );
/* .line 510 */
} // :goto_0
} // .end method
public void handleWallpaperObserverEvent ( com.android.server.wallpaper.WallpaperData p0 ) {
/* .locals 2 */
/* .param p1, "wallpaper" # Lcom/android/server/wallpaper/WallpaperData; */
/* .line 204 */
v0 = this.mService;
/* if-nez v0, :cond_0 */
return;
/* .line 206 */
} // :cond_0
final String v0 = "WallpaperManagerServiceImpl"; // const-string v0, "WallpaperManagerServiceImpl"
final String v1 = "handleWallpaperObserverEvent"; // const-string v1, "handleWallpaperObserverEvent"
android.util.Slog .v ( v0,v1 );
/* .line 207 */
v0 = this.blurWallpaperHandler;
int v1 = 1; // const/4 v1, 0x1
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 209 */
v0 = this.blurWallpaperHandler;
android.os.Message .obtain ( v0,v1,p1 );
/* .line 210 */
/* .local v0, "message":Landroid/os/Message; */
(( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
/* .line 211 */
return;
} // .end method
public void initBlurWallpaperThread ( ) {
/* .locals 2 */
/* .line 528 */
v0 = this.blurWallpaperThread;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 529 */
return;
/* .line 531 */
} // :cond_0
/* new-instance v0, Landroid/os/HandlerThread; */
final String v1 = "BlurWallpaperThread"; // const-string v1, "BlurWallpaperThread"
/* invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
this.blurWallpaperThread = v0;
/* .line 532 */
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 533 */
/* new-instance v0, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl$1; */
v1 = this.blurWallpaperThread;
(( android.os.HandlerThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl$1;-><init>(Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;Landroid/os/Looper;)V */
this.blurWallpaperHandler = v0;
/* .line 553 */
return;
} // .end method
public Boolean isMiuiWallpaperComponent ( android.content.ComponentName p0 ) {
/* .locals 2 */
/* .param p1, "componentName" # Landroid/content/ComponentName; */
/* .line 87 */
v0 = com.android.server.wallpaper.WallpaperManagerServiceImpl.MIUI_WALLPAPER_COMPONENTS;
v0 = (( android.content.ComponentName ) p1 ).toString ( ); // invoke-virtual {p1}, Landroid/content/ComponentName;->toString()Ljava/lang/String;
} // .end method
public Boolean isSuperWallpaper ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 92 */
v0 = v0 = com.android.server.wallpaper.WallpaperManagerServiceImpl.SUPER_WALLPAPER_PACKAGE_NAMES;
} // .end method
public com.android.server.wallpaper.WallpaperDataParser$WallpaperLoadingResult loadSettingLocked ( com.android.server.wallpaper.WallpaperDataParser p0, Integer p1, Boolean p2, com.android.server.wallpaper.WallpaperData p3, com.android.server.wallpaper.WallpaperData p4, Integer p5 ) {
/* .locals 11 */
/* .param p1, "parser" # Lcom/android/server/wallpaper/WallpaperDataParser; */
/* .param p2, "userId" # I */
/* .param p3, "keepDimensionHints" # Z */
/* .param p4, "wallpaperData" # Lcom/android/server/wallpaper/WallpaperData; */
/* .param p5, "lockWallpaperData" # Lcom/android/server/wallpaper/WallpaperData; */
/* .param p6, "which" # I */
/* .line 160 */
v0 = com.android.server.wallpaper.WallpaperDataParserProxy.makeJournaledFile;
java.lang.Integer .valueOf ( p2 );
/* filled-new-array {v1}, [Ljava/lang/Object; */
/* move-object v2, p1 */
(( com.xiaomi.reflect.RefMethod ) v0 ).invoke ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Lcom/xiaomi/reflect/RefMethod;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/android/internal/util/JournaledFile; */
/* .line 161 */
/* .local v0, "journal":Lcom/android/internal/util/JournaledFile; */
/* invoke-virtual/range {p1 ..p6}, Lcom/android/server/wallpaper/WallpaperDataParser;->loadSettingsLocked(IZLcom/android/server/wallpaper/WallpaperData;Lcom/android/server/wallpaper/WallpaperData;I)Lcom/android/server/wallpaper/WallpaperDataParser$WallpaperLoadingResult; */
/* .line 167 */
/* .local v1, "result":Lcom/android/server/wallpaper/WallpaperDataParser$WallpaperLoadingResult; */
(( com.android.server.wallpaper.WallpaperDataParser$WallpaperLoadingResult ) v1 ).getSystemWallpaperData ( ); // invoke-virtual {v1}, Lcom/android/server/wallpaper/WallpaperDataParser$WallpaperLoadingResult;->getSystemWallpaperData()Lcom/android/server/wallpaper/WallpaperData;
/* .line 168 */
/* .local v3, "resultSystemData":Lcom/android/server/wallpaper/WallpaperData; */
(( com.android.server.wallpaper.WallpaperDataParser$WallpaperLoadingResult ) v1 ).getLockWallpaperData ( ); // invoke-virtual {v1}, Lcom/android/server/wallpaper/WallpaperDataParser$WallpaperLoadingResult;->getLockWallpaperData()Lcom/android/server/wallpaper/WallpaperData;
/* .line 169 */
/* .local v4, "resultLockData":Lcom/android/server/wallpaper/WallpaperData; */
(( com.android.internal.util.JournaledFile ) v0 ).chooseForRead ( ); // invoke-virtual {v0}, Lcom/android/internal/util/JournaledFile;->chooseForRead()Ljava/io/File;
/* .line 170 */
/* .local v5, "journalFile":Ljava/io/File; */
v6 = (( com.android.server.wallpaper.WallpaperDataParser$WallpaperLoadingResult ) v1 ).success ( ); // invoke-virtual {v1}, Lcom/android/server/wallpaper/WallpaperDataParser$WallpaperLoadingResult;->success()Z
if ( v6 != null) { // if-eqz v6, :cond_1
if ( v3 != null) { // if-eqz v3, :cond_0
v6 = this.name;
if ( v6 != null) { // if-eqz v6, :cond_1
} // :cond_0
if ( v4 != null) { // if-eqz v4, :cond_6
v6 = this.name;
/* if-nez v6, :cond_6 */
/* .line 173 */
} // :cond_1
v6 = (( java.io.File ) v5 ).exists ( ); // invoke-virtual {v5}, Ljava/io/File;->exists()Z
final String v7 = "WallpaperManagerServiceImpl"; // const-string v7, "WallpaperManagerServiceImpl"
if ( v6 != null) { // if-eqz v6, :cond_4
/* .line 174 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v8, "unexpected situation when loadSetting, file may be irreversibly damaged, perform recovery.nullSystem?=" */
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
int v8 = 1; // const/4 v8, 0x1
int v9 = 0; // const/4 v9, 0x0
if ( v3 != null) { // if-eqz v3, :cond_2
v10 = this.name;
/* if-nez v10, :cond_2 */
/* move v10, v8 */
} // :cond_2
/* move v10, v9 */
} // :goto_0
(( java.lang.StringBuilder ) v6 ).append ( v10 ); // invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v10 = " nullLock?="; // const-string v10, " nullLock?="
(( java.lang.StringBuilder ) v6 ).append ( v10 ); // invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
if ( v4 != null) { // if-eqz v4, :cond_3
v10 = this.name;
/* if-nez v10, :cond_3 */
} // :cond_3
/* move v8, v9 */
} // :goto_1
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v8 = " exist=true"; // const-string v8, " exist=true"
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v7,v6 );
/* .line 180 */
(( java.io.File ) v5 ).delete ( ); // invoke-virtual {v5}, Ljava/io/File;->delete()Z
/* .line 182 */
} // :cond_4
final String v6 = "journalFile is not exist, first boot?"; // const-string v6, "journalFile is not exist, first boot?"
android.util.Slog .i ( v7,v6 );
/* .line 185 */
} // :goto_2
com.android.server.MiuiFallbackHelperStub .getInstance ( );
/* .line 186 */
(( com.android.internal.util.JournaledFile ) v0 ).chooseForRead ( ); // invoke-virtual {v0}, Lcom/android/internal/util/JournaledFile;->chooseForRead()Ljava/io/File;
(( java.io.File ) v8 ).getAbsolutePath ( ); // invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
v6 = (( com.android.server.MiuiFallbackHelperStub ) v6 ).restoreFile ( v8 ); // invoke-virtual {v6, v8}, Lcom/android/server/MiuiFallbackHelperStub;->restoreFile(Ljava/lang/String;)Z
if ( v6 != null) { // if-eqz v6, :cond_5
/* .line 187 */
/* invoke-virtual/range {p1 ..p6}, Lcom/android/server/wallpaper/WallpaperDataParser;->loadSettingsLocked(IZLcom/android/server/wallpaper/WallpaperData;Lcom/android/server/wallpaper/WallpaperData;I)Lcom/android/server/wallpaper/WallpaperDataParser$WallpaperLoadingResult; */
/* .line 190 */
} // :cond_5
final String v6 = "restore fail, first boot?"; // const-string v6, "restore fail, first boot?"
android.util.Slog .e ( v7,v6 );
/* .line 193 */
} // :cond_6
} // :goto_3
} // .end method
public void saveSettingLocked ( Integer p0, com.android.server.wallpaper.WallpaperDataParser p1, com.android.server.wallpaper.WallpaperData p2, com.android.server.wallpaper.WallpaperData p3 ) {
/* .locals 5 */
/* .param p1, "userId" # I */
/* .param p2, "parser" # Lcom/android/server/wallpaper/WallpaperDataParser; */
/* .param p3, "system" # Lcom/android/server/wallpaper/WallpaperData; */
/* .param p4, "lock" # Lcom/android/server/wallpaper/WallpaperData; */
/* .line 132 */
final String v0 = "WallpaperManagerServiceImpl"; // const-string v0, "WallpaperManagerServiceImpl"
/* if-nez p2, :cond_0 */
/* .line 133 */
final String v1 = "parser is null when perform saveSettingLocked"; // const-string v1, "parser is null when perform saveSettingLocked"
android.util.Slog .e ( v0,v1 );
/* .line 134 */
return;
/* .line 136 */
} // :cond_0
v1 = com.android.server.wallpaper.WallpaperDataParserProxy.makeJournaledFile;
java.lang.Integer .valueOf ( p1 );
/* filled-new-array {v2}, [Ljava/lang/Object; */
(( com.xiaomi.reflect.RefMethod ) v1 ).invoke ( p2, v2 ); // invoke-virtual {v1, p2, v2}, Lcom/xiaomi/reflect/RefMethod;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Lcom/android/internal/util/JournaledFile; */
/* .line 138 */
/* .local v1, "journal":Lcom/android/internal/util/JournaledFile; */
try { // :try_start_0
(( com.android.server.wallpaper.WallpaperDataParser ) p2 ).saveSettingsLocked ( p1, p3, p4 ); // invoke-virtual {p2, p1, p3, p4}, Lcom/android/server/wallpaper/WallpaperDataParser;->saveSettingsLocked(ILcom/android/server/wallpaper/WallpaperData;Lcom/android/server/wallpaper/WallpaperData;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 149 */
/* .line 139 */
/* :catch_0 */
/* move-exception v2 */
/* .line 140 */
/* .local v2, "e":Ljava/lang/Exception; */
/* const-string/jumbo v3, "unexpected situation when save settings, file may be irreversibly damaged, perform recovery" */
android.util.Slog .e ( v0,v3,v2 );
/* .line 142 */
(( com.android.internal.util.JournaledFile ) v1 ).chooseForRead ( ); // invoke-virtual {v1}, Lcom/android/internal/util/JournaledFile;->chooseForRead()Ljava/io/File;
/* .line 144 */
/* .local v0, "journalFile":Ljava/io/File; */
(( java.io.File ) v0 ).delete ( ); // invoke-virtual {v0}, Ljava/io/File;->delete()Z
/* .line 146 */
com.android.server.MiuiFallbackHelperStub .getInstance ( );
/* .line 147 */
(( java.io.File ) v0 ).getAbsolutePath ( ); // invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
(( com.android.server.MiuiFallbackHelperStub ) v3 ).restoreFile ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/MiuiFallbackHelperStub;->restoreFile(Ljava/lang/String;)Z
/* .line 148 */
(( com.android.server.wallpaper.WallpaperDataParser ) p2 ).saveSettingsLocked ( p1, p3, p4 ); // invoke-virtual {p2, p1, p3, p4}, Lcom/android/server/wallpaper/WallpaperDataParser;->saveSettingsLocked(ILcom/android/server/wallpaper/WallpaperData;Lcom/android/server/wallpaper/WallpaperData;)V
/* .line 152 */
} // .end local v0 # "journalFile":Ljava/io/File;
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_0
com.android.server.MiuiFallbackHelperStub .getInstance ( );
/* .line 153 */
(( com.android.internal.util.JournaledFile ) v1 ).chooseForRead ( ); // invoke-virtual {v1}, Lcom/android/internal/util/JournaledFile;->chooseForRead()Ljava/io/File;
(( java.io.File ) v2 ).getAbsolutePath ( ); // invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
(( com.android.server.MiuiFallbackHelperStub ) v0 ).snapshotFile ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/MiuiFallbackHelperStub;->snapshotFile(Ljava/lang/String;)V
/* .line 154 */
return;
} // .end method
public void setWallpaperManagerService ( com.android.server.wallpaper.WallpaperManagerService p0, android.content.Context p1 ) {
/* .locals 0 */
/* .param p1, "service" # Lcom/android/server/wallpaper/WallpaperManagerService; */
/* .param p2, "context" # Landroid/content/Context; */
/* .line 199 */
this.mService = p1;
/* .line 200 */
this.mContext = p2;
/* .line 201 */
return;
} // .end method
public void wallpaperManagerServiceReady ( ) {
/* .locals 2 */
/* .line 499 */
int v0 = 0; // const/4 v0, 0x0
(( com.android.server.wallpaper.WallpaperManagerServiceImpl ) p0 ).createBlurWallpaper ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->createBlurWallpaper(Z)V
/* .line 500 */
final String v0 = "WallpaperManagerServiceImpl"; // const-string v0, "WallpaperManagerServiceImpl"
final String v1 = "create blur wallpaper"; // const-string v1, "create blur wallpaper"
android.util.Slog .i ( v0,v1 );
/* .line 501 */
return;
} // .end method
