.class Lcom/android/server/wallpaper/WallpaperManagerServiceImpl$1;
.super Landroid/os/Handler;
.source "WallpaperManagerServiceImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->initBlurWallpaperThread()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;


# direct methods
.method constructor <init>(Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;Landroid/os/Looper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 533
    iput-object p1, p0, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl$1;->this$0:Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .line 536
    invoke-static {}, Landroid/util/MiuiAppSizeCompatModeStub;->get()Landroid/util/MiuiAppSizeCompatModeStub;

    move-result-object v0

    invoke-interface {v0}, Landroid/util/MiuiAppSizeCompatModeStub;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/android/server/wm/MiuiEmbeddingWindowServiceStubHead;->isActivityEmbeddingEnable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 537
    const-string v0, "WallpaperManagerServiceImpl"

    const-string/jumbo v1, "skip handle  create blur bitmap message, only support on pad and foldable"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 538
    return-void

    .line 540
    :cond_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/android/server/wallpaper/WallpaperData;

    .line 541
    .local v0, "wallpaper":Lcom/android/server/wallpaper/WallpaperData;
    if-nez v0, :cond_1

    return-void

    .line 543
    :cond_1
    const-string v1, "WallpaperManagerServiceImpl"

    const-string v2, "onEvent createBlurBitmap Blur wallpaper Begin"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 544
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 545
    .local v1, "timeBegin":J
    iget-object v3, p0, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl$1;->this$0:Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->createBlurWallpaper(Z)V

    .line 546
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    sub-long/2addr v3, v1

    .line 547
    .local v3, "takenTime":J
    const-string v5, "WallpaperManagerServiceImpl"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onEvent createBlurBitmap Blur takenTime = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 548
    iget-object v5, p0, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl$1;->this$0:Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;

    invoke-static {v5}, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->-$$Nest$fgetmService(Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;)Lcom/android/server/wallpaper/WallpaperManagerService;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/server/wallpaper/WallpaperManagerService;->getLock()Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    .line 549
    :try_start_0
    iget-object v6, p0, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl$1;->this$0:Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;

    invoke-static {v6, v0}, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->-$$Nest$mnotifyBlurCallbacksLocked(Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;Lcom/android/server/wallpaper/WallpaperData;)V

    .line 550
    monitor-exit v5

    .line 551
    return-void

    .line 550
    :catchall_0
    move-exception v6

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6
.end method
