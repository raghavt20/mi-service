class com.android.server.wallpaper.WallpaperManagerServiceImpl$1 extends android.os.Handler {
	 /* .source "WallpaperManagerServiceImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->initBlurWallpaperThread()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wallpaper.WallpaperManagerServiceImpl this$0; //synthetic
/* # direct methods */
 com.android.server.wallpaper.WallpaperManagerServiceImpl$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wallpaper/WallpaperManagerServiceImpl; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 533 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 8 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 536 */
v0 = android.util.MiuiAppSizeCompatModeStub .get ( );
/* if-nez v0, :cond_0 */
v0 = com.android.server.wm.MiuiEmbeddingWindowServiceStubHead .isActivityEmbeddingEnable ( );
/* if-nez v0, :cond_0 */
/* .line 537 */
final String v0 = "WallpaperManagerServiceImpl"; // const-string v0, "WallpaperManagerServiceImpl"
/* const-string/jumbo v1, "skip handle create blur bitmap message, only support on pad and foldable" */
android.util.Slog .d ( v0,v1 );
/* .line 538 */
return;
/* .line 540 */
} // :cond_0
v0 = this.obj;
/* check-cast v0, Lcom/android/server/wallpaper/WallpaperData; */
/* .line 541 */
/* .local v0, "wallpaper":Lcom/android/server/wallpaper/WallpaperData; */
/* if-nez v0, :cond_1 */
return;
/* .line 543 */
} // :cond_1
final String v1 = "WallpaperManagerServiceImpl"; // const-string v1, "WallpaperManagerServiceImpl"
final String v2 = "onEvent createBlurBitmap Blur wallpaper Begin"; // const-string v2, "onEvent createBlurBitmap Blur wallpaper Begin"
android.util.Slog .d ( v1,v2 );
/* .line 544 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v1 */
/* .line 545 */
/* .local v1, "timeBegin":J */
v3 = this.this$0;
int v4 = 1; // const/4 v4, 0x1
(( com.android.server.wallpaper.WallpaperManagerServiceImpl ) v3 ).createBlurWallpaper ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->createBlurWallpaper(Z)V
/* .line 546 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v3 */
/* sub-long/2addr v3, v1 */
/* .line 547 */
/* .local v3, "takenTime":J */
final String v5 = "WallpaperManagerServiceImpl"; // const-string v5, "WallpaperManagerServiceImpl"
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "onEvent createBlurBitmap Blur takenTime = "; // const-string v7, "onEvent createBlurBitmap Blur takenTime = "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v3, v4 ); // invoke-virtual {v6, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v5,v6 );
/* .line 548 */
v5 = this.this$0;
com.android.server.wallpaper.WallpaperManagerServiceImpl .-$$Nest$fgetmService ( v5 );
(( com.android.server.wallpaper.WallpaperManagerService ) v5 ).getLock ( ); // invoke-virtual {v5}, Lcom/android/server/wallpaper/WallpaperManagerService;->getLock()Ljava/lang/Object;
/* monitor-enter v5 */
/* .line 549 */
try { // :try_start_0
v6 = this.this$0;
com.android.server.wallpaper.WallpaperManagerServiceImpl .-$$Nest$mnotifyBlurCallbacksLocked ( v6,v0 );
/* .line 550 */
/* monitor-exit v5 */
/* .line 551 */
return;
/* .line 550 */
/* :catchall_0 */
/* move-exception v6 */
/* monitor-exit v5 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v6 */
} // .end method
