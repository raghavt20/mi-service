.class public Lcom/android/server/wallpaper/WallpaperManagerServiceProxy;
.super Ljava/lang/Object;
.source "WallpaperManagerServiceProxy.java"


# static fields
.field public static changingToSame:Lcom/xiaomi/reflect/RefMethod;
    .annotation runtime Lcom/xiaomi/reflect/annotation/MethodArguments;
        cls = {
            Landroid/content/ComponentName;,
            Lcom/android/server/wallpaper/WallpaperData;
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/xiaomi/reflect/RefMethod<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static notifyWallpaperChanged:Lcom/xiaomi/reflect/RefMethod;
    .annotation runtime Lcom/xiaomi/reflect/annotation/MethodArguments;
        cls = {
            Lcom/android/server/wallpaper/WallpaperData;
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/xiaomi/reflect/RefMethod<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 11
    const-class v0, Lcom/android/server/wallpaper/WallpaperManagerServiceProxy;

    const-class v1, Lcom/android/server/wallpaper/WallpaperManagerService;

    invoke-static {v0, v1}, Lcom/xiaomi/reflect/RefClass;->attach(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Class;

    .line 12
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
