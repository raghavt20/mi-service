.class public Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;
.super Lcom/android/server/wallpaper/WallpaperManagerServiceStub;
.source "WallpaperManagerServiceImpl.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.wallpaper.WallpaperManagerServiceStub$$"
.end annotation


# static fields
.field private static final FRAMEWORK_VERSION_TO_WALLPAPER:I = 0x1

.field private static final KEY_FRAMEWORK_VERSION_TO_WALLPAPER:Ljava/lang/String; = "framework_version_to_wallpaper"

.field private static final MIUI_WALLPAPER_COMPONENTS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final MSG_CREATE_BLURWALLPAPER:I = 0x1

.field private static final SUPER_WALLPAPER_PACKAGE_NAMES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "WallpaperManagerServiceImpl"


# instance fields
.field private blurWallpaperHandler:Landroid/os/Handler;

.field private blurWallpaperThread:Landroid/os/HandlerThread;

.field private mContext:Landroid/content/Context;

.field private mService:Lcom/android/server/wallpaper/WallpaperManagerService;


# direct methods
.method static bridge synthetic -$$Nest$fgetmService(Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;)Lcom/android/server/wallpaper/WallpaperManagerService;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->mService:Lcom/android/server/wallpaper/WallpaperManagerService;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mnotifyBlurCallbacksLocked(Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;Lcom/android/server/wallpaper/WallpaperData;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->notifyBlurCallbacksLocked(Lcom/android/server/wallpaper/WallpaperData;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 72
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->MIUI_WALLPAPER_COMPONENTS:Ljava/util/List;

    .line 73
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sput-object v1, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->SUPER_WALLPAPER_PACKAGE_NAMES:Ljava/util/List;

    .line 76
    const-string v2, "ComponentInfo{com.miui.miwallpaper/com.miui.miwallpaper.wallpaperservice.ImageWallpaper}"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    const-string v0, "com.miui.miwallpaper.snowmountain"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 79
    const-string v0, "com.miui.miwallpaper.geometry"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    const-string v0, "com.miui.miwallpaper.saturn"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    const-string v0, "com.miui.miwallpaper.earth"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 82
    const-string v0, "com.miui.miwallpaper.mars"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 83
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 58
    invoke-direct {p0}, Lcom/android/server/wallpaper/WallpaperManagerServiceStub;-><init>()V

    return-void
.end method

.method private getCurrentWallpaperLocked(Landroid/graphics/Point;)Landroid/graphics/Bitmap;
    .locals 7
    .param p1, "size"    # Landroid/graphics/Point;

    .line 376
    const-string v0, "Error getting wallpaper"

    const-string v1, "WallpaperManagerServiceImpl"

    const/4 v2, 0x0

    .line 377
    .local v2, "mBitmap":Landroid/graphics/Bitmap;
    invoke-direct {p0}, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->getWallpaper()Landroid/os/ParcelFileDescriptor;

    move-result-object v3

    .line 378
    .local v3, "fd":Landroid/os/ParcelFileDescriptor;
    const/4 v4, 0x0

    if-nez v3, :cond_0

    .line 379
    return-object v4

    .line 383
    :cond_0
    :try_start_0
    new-instance v5, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v5}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 384
    .local v5, "options":Landroid/graphics/BitmapFactory$Options;
    nop

    .line 385
    invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v6

    invoke-static {v6, v4, v5}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 384
    invoke-direct {p0, v4, p1}, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->scaleWallpaperBitmapToScreenSize(Landroid/graphics/Bitmap;Landroid/graphics/Point;)Landroid/graphics/Bitmap;

    move-result-object v4
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v2, v4

    .line 391
    .end local v5    # "options":Landroid/graphics/BitmapFactory$Options;
    :try_start_1
    invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 394
    :goto_0
    goto :goto_1

    .line 392
    :catch_0
    move-exception v4

    .line 393
    .local v4, "e":Ljava/io/IOException;
    invoke-static {v1, v0, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 395
    .end local v4    # "e":Ljava/io/IOException;
    goto :goto_1

    .line 390
    :catchall_0
    move-exception v4

    goto :goto_2

    .line 387
    :catch_1
    move-exception v4

    .line 388
    .local v4, "e":Ljava/lang/OutOfMemoryError;
    :try_start_2
    const-string v5, "Can\'t decode file"

    invoke-static {v1, v5, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 391
    .end local v4    # "e":Ljava/lang/OutOfMemoryError;
    :try_start_3
    invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 396
    :goto_1
    return-object v2

    .line 391
    :goto_2
    :try_start_4
    invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 394
    goto :goto_3

    .line 392
    :catch_2
    move-exception v5

    .line 393
    .local v5, "e":Ljava/io/IOException;
    invoke-static {v1, v0, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 395
    .end local v5    # "e":Ljava/io/IOException;
    :goto_3
    throw v4
.end method

.method private getDefaultWallpaperLocked(Landroid/graphics/Point;)Landroid/graphics/Bitmap;
    .locals 6
    .param p1, "size"    # Landroid/graphics/Point;

    .line 354
    const-string v0, "Error getting wallpaper"

    const-string v1, "WallpaperManagerServiceImpl"

    iget-object v2, p0, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    if-nez v2, :cond_0

    return-object v3

    .line 355
    :cond_0
    const/4 v4, 0x1

    invoke-static {v2, v4}, Landroid/app/WallpaperManager;->openDefaultWallpaper(Landroid/content/Context;I)Ljava/io/InputStream;

    move-result-object v2

    .line 356
    .local v2, "is":Ljava/io/InputStream;
    const/4 v4, 0x0

    .line 357
    .local v4, "mBitmap":Landroid/graphics/Bitmap;
    if-eqz v2, :cond_1

    .line 359
    :try_start_0
    new-instance v5, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v5}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    invoke-static {v2, v3, v5}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-direct {p0, v3, p1}, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->scaleWallpaperBitmapToScreenSize(Landroid/graphics/Bitmap;Landroid/graphics/Point;)Landroid/graphics/Bitmap;

    move-result-object v3
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v4, v3

    .line 366
    :try_start_1
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 369
    :goto_0
    goto :goto_3

    .line 367
    :catch_0
    move-exception v3

    .line 368
    .local v3, "e":Ljava/io/IOException;
    invoke-static {v1, v0, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 370
    .end local v3    # "e":Ljava/io/IOException;
    goto :goto_3

    .line 365
    :catchall_0
    move-exception v3

    goto :goto_1

    .line 361
    :catch_1
    move-exception v3

    .line 362
    .local v3, "e":Ljava/lang/OutOfMemoryError;
    :try_start_2
    const-string v5, "Can\'t decode stream"

    invoke-static {v1, v5, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 363
    const/4 v4, 0x0

    .line 366
    .end local v3    # "e":Ljava/lang/OutOfMemoryError;
    :try_start_3
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    :goto_1
    :try_start_4
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 369
    goto :goto_2

    .line 367
    :catch_2
    move-exception v5

    .line 368
    .local v5, "e":Ljava/io/IOException;
    invoke-static {v1, v0, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 370
    .end local v5    # "e":Ljava/io/IOException;
    :goto_2
    throw v3

    .line 372
    :cond_1
    :goto_3
    return-object v4
.end method

.method private getWallpaper()Landroid/os/ParcelFileDescriptor;
    .locals 7

    .line 400
    iget-object v0, p0, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->mService:Lcom/android/server/wallpaper/WallpaperManagerService;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 403
    :cond_0
    invoke-virtual {v0}, Lcom/android/server/wallpaper/WallpaperManagerService;->getLock()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 404
    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->getWallpaperUserId()I

    move-result v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 405
    .local v2, "wallpaperUserId":I
    const/4 v3, 0x0

    .line 408
    .local v3, "mParcelFile":Landroid/os/ParcelFileDescriptor;
    :try_start_1
    new-instance v4, Ljava/io/File;

    invoke-static {v2}, Landroid/os/Environment;->getUserSystemDirectory(I)Ljava/io/File;

    move-result-object v5

    const-string/jumbo v6, "wallpaper"

    invoke-direct {v4, v5, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 409
    .local v4, "wallpaperFile":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v5, :cond_1

    .line 410
    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    return-object v1

    .line 412
    :cond_1
    const/high16 v1, 0x10000000

    :try_start_3
    invoke-static {v4, v1}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v1
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 416
    .end local v3    # "mParcelFile":Landroid/os/ParcelFileDescriptor;
    .local v1, "mParcelFile":Landroid/os/ParcelFileDescriptor;
    goto :goto_0

    .line 413
    .end local v1    # "mParcelFile":Landroid/os/ParcelFileDescriptor;
    .end local v4    # "wallpaperFile":Ljava/io/File;
    .restart local v3    # "mParcelFile":Landroid/os/ParcelFileDescriptor;
    :catch_0
    move-exception v1

    .line 414
    .local v1, "e":Ljava/io/FileNotFoundException;
    :try_start_4
    const-string v4, "WallpaperManagerServiceImpl"

    const-string v5, "Error getting wallpaper"

    invoke-static {v4, v5, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 415
    const/4 v3, 0x0

    move-object v1, v3

    .line 417
    .end local v3    # "mParcelFile":Landroid/os/ParcelFileDescriptor;
    .local v1, "mParcelFile":Landroid/os/ParcelFileDescriptor;
    :goto_0
    monitor-exit v0

    return-object v1

    .line 418
    .end local v1    # "mParcelFile":Landroid/os/ParcelFileDescriptor;
    .end local v2    # "wallpaperUserId":I
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v1
.end method

.method private loadThumbnailWithoutTheme(Landroid/app/WallpaperInfo;Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;
    .locals 4
    .param p1, "info"    # Landroid/app/WallpaperInfo;
    .param p2, "pm"    # Landroid/content/pm/PackageManager;

    .line 454
    const/4 v0, 0x0

    .line 455
    .local v0, "dr":Landroid/graphics/drawable/Drawable;
    const/4 v1, 0x0

    .line 456
    .local v1, "thumbnailResource":I
    if-eqz p1, :cond_0

    .line 457
    invoke-virtual {p1}, Landroid/app/WallpaperInfo;->getThumbnailResource()I

    move-result v1

    .line 460
    :cond_0
    if-ltz v1, :cond_1

    .line 461
    if-eqz p1, :cond_1

    .line 462
    invoke-virtual {p1}, Landroid/app/WallpaperInfo;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 463
    .local v2, "packageName":Ljava/lang/String;
    nop

    .line 464
    invoke-virtual {p1}, Landroid/app/WallpaperInfo;->getServiceInfo()Landroid/content/pm/ServiceInfo;

    move-result-object v3

    iget-object v3, v3, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    .line 463
    invoke-virtual {p2, v2, v1, v3}, Landroid/content/pm/PackageManager;->getDrawable(Ljava/lang/String;ILandroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 467
    .end local v2    # "packageName":Ljava/lang/String;
    :cond_1
    return-object v0
.end method

.method private notifyBlurCallbacksLocked(Lcom/android/server/wallpaper/WallpaperData;)V
    .locals 4
    .param p1, "wallpaper"    # Lcom/android/server/wallpaper/WallpaperData;

    .line 514
    invoke-virtual {p1}, Lcom/android/server/wallpaper/WallpaperData;->getCallbacks()Landroid/os/RemoteCallbackList;

    move-result-object v0

    .line 515
    .local v0, "callbacks":Landroid/os/RemoteCallbackList;, "Landroid/os/RemoteCallbackList<Landroid/app/IWallpaperManagerCallback;>;"
    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v1

    .line 516
    .local v1, "n":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 518
    :try_start_0
    invoke-virtual {v0, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Landroid/app/IWallpaperManagerCallback;

    invoke-interface {v3}, Landroid/app/IWallpaperManagerCallback;->onWallpaperChanged()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 522
    goto :goto_1

    .line 519
    :catch_0
    move-exception v3

    .line 516
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 524
    .end local v2    # "i":I
    :cond_0
    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 525
    return-void
.end method

.method private saveBlurWallpaperBitmapLocked(Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 11
    .param p1, "blurImg"    # Landroid/graphics/Bitmap;
    .param p2, "fileName"    # Ljava/lang/String;

    .line 297
    const/4 v0, 0x0

    .line 298
    .local v0, "mWallpaperStream":Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;
    const-string v1, "saveBlurWallpaperBitmapLocked begin"

    const-string v2, "WallpaperManagerServiceImpl"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 301
    .local v3, "timeBegin":J
    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->getWallpaperUserId()I

    move-result v1

    invoke-static {v1}, Landroid/os/Environment;->getUserSystemDirectory(I)Ljava/io/File;

    move-result-object v1

    .line 302
    .local v1, "dir":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 303
    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x1f9

    const/4 v7, -0x1

    invoke-static {v5, v6, v7, v7}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    .line 306
    :cond_0
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 307
    .local v5, "file":Ljava/io/File;
    const/high16 v6, 0x38000000

    invoke-static {v5, v6}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v6

    .line 309
    .local v6, "fd":Landroid/os/ParcelFileDescriptor;
    invoke-static {v5}, Landroid/os/SELinux;->restorecon(Ljava/io/File;)Z

    move-result v7
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_7

    if-eqz v7, :cond_4

    .line 311
    :try_start_1
    new-instance v7, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;

    invoke-direct {v7, v6}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V

    move-object v0, v7

    .line 312
    sget-object v7, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v8, 0x5a

    invoke-virtual {p1, v7, v8, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 315
    :try_start_2
    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 317
    goto :goto_0

    .line 316
    :catch_0
    move-exception v7

    .line 319
    :goto_0
    if-eqz v6, :cond_1

    .line 321
    :try_start_3
    invoke-virtual {v6}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 323
    goto :goto_1

    .line 322
    :catch_1
    move-exception v7

    .line 325
    :cond_1
    :goto_1
    :try_start_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    sub-long/2addr v7, v3

    .line 326
    .local v7, "takenTime":J
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "saveBlurWallpaperBitmapLocked takenTime = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v2, v9}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 328
    nop

    .line 330
    .end local v7    # "takenTime":J
    :try_start_5
    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_7

    .line 332
    goto :goto_2

    .line 331
    :catch_2
    move-exception v7

    .line 334
    :goto_2
    if-eqz v6, :cond_4

    .line 336
    :try_start_6
    invoke-virtual {v6}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_7

    .line 338
    :goto_3
    goto :goto_6

    .line 337
    :catch_3
    move-exception v7

    goto :goto_3

    .line 328
    :catchall_0
    move-exception v7

    if-eqz v0, :cond_2

    .line 330
    :try_start_7
    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_7

    .line 332
    goto :goto_4

    .line 331
    :catch_4
    move-exception v8

    .line 334
    :cond_2
    :goto_4
    if-eqz v6, :cond_3

    .line 336
    :try_start_8
    invoke-virtual {v6}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5
    .catch Ljava/io/FileNotFoundException; {:try_start_8 .. :try_end_8} :catch_7

    .line 338
    goto :goto_5

    .line 337
    :catch_5
    move-exception v8

    .line 340
    :cond_3
    :goto_5
    nop

    .end local v0    # "mWallpaperStream":Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;
    .end local v3    # "timeBegin":J
    .end local p0    # "this":Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;
    .end local p1    # "blurImg":Landroid/graphics/Bitmap;
    .end local p2    # "fileName":Ljava/lang/String;
    :try_start_9
    throw v7
    :try_end_9
    .catch Ljava/io/FileNotFoundException; {:try_start_9 .. :try_end_9} :catch_7

    .line 342
    .restart local v0    # "mWallpaperStream":Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;
    .restart local v3    # "timeBegin":J
    .restart local p0    # "this":Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;
    .restart local p1    # "blurImg":Landroid/graphics/Bitmap;
    .restart local p2    # "fileName":Ljava/lang/String;
    :cond_4
    :goto_6
    if-eqz v6, :cond_5

    .line 344
    :try_start_a
    invoke-virtual {v6}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6
    .catch Ljava/io/FileNotFoundException; {:try_start_a .. :try_end_a} :catch_7

    .line 346
    goto :goto_7

    .line 345
    :catch_6
    move-exception v2

    .line 350
    .end local v1    # "dir":Ljava/io/File;
    .end local v5    # "file":Ljava/io/File;
    :cond_5
    :goto_7
    goto :goto_8

    .line 348
    .end local v6    # "fd":Landroid/os/ParcelFileDescriptor;
    :catch_7
    move-exception v1

    .line 349
    .local v1, "e":Ljava/io/FileNotFoundException;
    const-string v5, "Error setting wallpaper"

    invoke-static {v2, v5, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 351
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    :goto_8
    return-void
.end method

.method private scaleWallpaperBitmapToScreenSize(Landroid/graphics/Bitmap;Landroid/graphics/Point;)Landroid/graphics/Bitmap;
    .locals 18
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "size"    # Landroid/graphics/Point;

    .line 423
    move-object/from16 v7, p2

    if-nez p1, :cond_0

    .line 424
    const/4 v0, 0x0

    return-object v0

    .line 427
    :cond_0
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    move-object v8, v0

    .line 428
    .local v8, "m":Landroid/graphics/Matrix;
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    .line 429
    .local v9, "bitmap_height":I
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    .line 430
    .local v10, "bitmap_width":I
    invoke-static {v9, v10}, Ljava/lang/Math;->max(II)I

    move-result v11

    .line 431
    .local v11, "bitmapLongSideLength":I
    invoke-static {v9, v10}, Ljava/lang/Math;->min(II)I

    move-result v12

    .line 432
    .local v12, "bitmapShortSideLength":I
    iget v0, v7, Landroid/graphics/Point;->x:I

    iget v1, v7, Landroid/graphics/Point;->y:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v13

    .line 433
    .local v13, "longSideLength":I
    iget v0, v7, Landroid/graphics/Point;->x:I

    iget v1, v7, Landroid/graphics/Point;->y:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v14

    .line 435
    .local v14, "shortSideLength":I
    if-eqz v13, :cond_3

    if-eqz v14, :cond_3

    if-eq v11, v13, :cond_3

    .line 436
    mul-int/lit8 v0, v14, 0x2

    if-ne v11, v0, :cond_1

    if-ne v12, v13, :cond_1

    .line 437
    return-object p1

    .line 441
    :cond_1
    int-to-float v0, v11

    int-to-float v1, v14

    div-float/2addr v0, v1

    int-to-float v1, v12

    int-to-float v2, v13

    div-float/2addr v1, v2

    div-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    .line 442
    mul-int/lit8 v0, v13, 0x2

    goto :goto_0

    :cond_2
    move v0, v13

    :goto_0
    move v15, v0

    .line 443
    .local v15, "sidelength":I
    int-to-float v0, v15

    int-to-float v1, v11

    div-float v6, v0, v1

    .line 444
    .local v6, "scale":F
    invoke-virtual {v8, v6, v6}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 445
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/16 v16, 0x1

    move-object/from16 v0, p1

    move v3, v10

    move v4, v9

    move-object v5, v8

    move/from16 v17, v6

    .end local v6    # "scale":F
    .local v17, "scale":F
    move/from16 v6, v16

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 446
    .end local v15    # "sidelength":I
    .end local v17    # "scale":F
    .local v0, "bmp":Landroid/graphics/Bitmap;
    goto :goto_1

    .line 447
    .end local v0    # "bmp":Landroid/graphics/Bitmap;
    :cond_3
    move-object/from16 v0, p1

    .line 450
    .restart local v0    # "bmp":Landroid/graphics/Bitmap;
    :goto_1
    return-object v0
.end method


# virtual methods
.method public bindWallpaperComponentLocked(Lcom/android/server/wallpaper/WallpaperManagerService;Lcom/android/server/wallpaper/WallpaperData;Landroid/content/ComponentName;Landroid/os/IRemoteCallback$Stub;)V
    .locals 10
    .param p1, "service"    # Lcom/android/server/wallpaper/WallpaperManagerService;
    .param p2, "wallpaper"    # Lcom/android/server/wallpaper/WallpaperData;
    .param p3, "imageWallpaper"    # Landroid/content/ComponentName;
    .param p4, "callback"    # Landroid/os/IRemoteCallback$Stub;

    .line 112
    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    iget-object v0, p2, Lcom/android/server/wallpaper/WallpaperData;->connection:Lcom/android/server/wallpaper/WallpaperManagerService$WallpaperConnection;

    if-nez v0, :cond_0

    goto :goto_1

    .line 117
    :cond_0
    invoke-virtual {p0, p3}, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->isMiuiWallpaperComponent(Landroid/content/ComponentName;)Z

    move-result v0

    .line 118
    .local v0, "isMiuiWallpaper":Z
    sget-object v1, Lcom/android/server/wallpaper/WallpaperManagerServiceProxy;->changingToSame:Lcom/xiaomi/reflect/RefMethod;

    filled-new-array {p3, p2}, [Ljava/lang/Object;

    move-result-object v2

    .line 119
    invoke-virtual {v1, p1, v2}, Lcom/xiaomi/reflect/RefMethod;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 120
    .local v1, "changeToSame":Z
    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    iget-object v2, p2, Lcom/android/server/wallpaper/WallpaperData;->connection:Lcom/android/server/wallpaper/WallpaperManagerService$WallpaperConnection;

    iget-object v2, v2, Lcom/android/server/wallpaper/WallpaperManagerService$WallpaperConnection;->mService:Landroid/service/wallpaper/IWallpaperService;

    if-eqz v2, :cond_1

    .line 121
    sget-object v2, Lcom/android/server/wallpaper/WallpaperManagerServiceProxy;->notifyWallpaperChanged:Lcom/xiaomi/reflect/RefMethod;

    filled-new-array {p2}, [Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, p1, v3}, Lcom/xiaomi/reflect/RefMethod;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 124
    :cond_1
    const/4 v6, 0x1

    const/4 v7, 0x0

    move-object v4, p1

    move-object v5, p3

    move-object v8, p2

    move-object v9, p4

    invoke-virtual/range {v4 .. v9}, Lcom/android/server/wallpaper/WallpaperManagerService;->bindWallpaperComponentLocked(Landroid/content/ComponentName;ZZLcom/android/server/wallpaper/WallpaperData;Landroid/os/IRemoteCallback;)Z

    .line 127
    :goto_0
    return-void

    .line 113
    .end local v0    # "isMiuiWallpaper":Z
    .end local v1    # "changeToSame":Z
    :cond_2
    :goto_1
    const-string v0, "WallpaperManagerServiceImpl"

    const-string v1, "bindWallpaperComponentWhenFileChanged receive unexpected param"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    return-void
.end method

.method public checkAndConfigFrameworkVersionToWallpaper(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 97
    if-nez p1, :cond_0

    .line 98
    const-string v0, "WallpaperManagerServiceImpl"

    const-string v1, "checkAndConfigFrameworkVersionToWallpaper: context null"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    return-void

    .line 101
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, -0x1

    const-string v2, "framework_version_to_wallpaper"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 103
    .local v0, "version":I
    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    .line 104
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {v3, v2, v1}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 107
    :cond_1
    return-void
.end method

.method public createBlurWallpaper(Z)V
    .locals 11
    .param p1, "force"    # Z

    .line 215
    invoke-static {}, Landroid/util/MiuiAppSizeCompatModeStub;->get()Landroid/util/MiuiAppSizeCompatModeStub;

    move-result-object v0

    invoke-interface {v0}, Landroid/util/MiuiAppSizeCompatModeStub;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/android/server/wm/MiuiEmbeddingWindowServiceStubHead;->isActivityEmbeddingEnable()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->mService:Lcom/android/server/wallpaper/WallpaperManagerService;

    if-nez v0, :cond_2

    .line 217
    :cond_1
    const-string v0, "WallpaperManagerServiceImpl"

    const-string/jumbo v1, "skip createBlurBitmap"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    return-void

    .line 221
    :cond_2
    const/4 v0, 0x0

    .line 223
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 225
    .local v1, "size":Landroid/graphics/Point;
    :try_start_0
    const-string/jumbo v2, "window"

    .line 226
    invoke-static {v2}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    .line 225
    invoke-static {v2}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v2

    .line 226
    const/4 v3, 0x0

    invoke-interface {v2, v3, v1}, Landroid/view/IWindowManager;->getBaseDisplaySize(ILandroid/graphics/Point;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 229
    goto :goto_0

    .line 227
    :catch_0
    move-exception v2

    .line 228
    .local v2, "e":Landroid/os/RemoteException;
    const-string v3, "WallpaperManagerServiceImpl"

    const-string v4, "IWindowManager null"

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    .end local v2    # "e":Landroid/os/RemoteException;
    :goto_0
    const/4 v2, 0x0

    .line 232
    .local v2, "isColorBackgournd":Z
    iget-object v3, p0, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->mService:Lcom/android/server/wallpaper/WallpaperManagerService;

    invoke-virtual {v3}, Lcom/android/server/wallpaper/WallpaperManagerService;->getLock()Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 233
    :try_start_1
    invoke-virtual {p0}, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->getWallpaperUserId()I

    move-result v4

    .line 234
    .local v4, "wallpaperUserId":I
    if-nez p1, :cond_3

    .line 235
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v4}, Landroid/os/Environment;->getUserSystemDirectory(I)Ljava/io/File;

    move-result-object v6

    .line 236
    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/blurwallpaper"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 237
    .local v5, "blurWallpaperPathName":Ljava/lang/String;
    const-string v6, "WallpaperManagerServiceImpl"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "createBlurWallpaper blurWallpaperPathName = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    new-instance v6, Ljava/io/File;

    invoke-static {v4}, Landroid/os/Environment;->getUserSystemDirectory(I)Ljava/io/File;

    move-result-object v7

    const-string v8, "blurwallpaper"

    invoke-direct {v6, v7, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 239
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 240
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    return-void

    .line 245
    .end local v5    # "blurWallpaperPathName":Ljava/lang/String;
    :cond_3
    :try_start_2
    iget-object v5, p0, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->mService:Lcom/android/server/wallpaper/WallpaperManagerService;

    invoke-virtual {v5, v4}, Lcom/android/server/wallpaper/WallpaperManagerService;->getWallpaperInfo(I)Landroid/app/WallpaperInfo;

    move-result-object v5

    .line 246
    .local v5, "info":Landroid/app/WallpaperInfo;
    if-eqz v5, :cond_5

    iget-object v6, p0, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->mContext:Landroid/content/Context;

    if-eqz v6, :cond_5

    .line 247
    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    .line 248
    .local v6, "pm":Landroid/content/pm/PackageManager;
    invoke-direct {p0, v5, v6}, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->loadThumbnailWithoutTheme(Landroid/app/WallpaperInfo;Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .line 249
    .local v7, "dr":Landroid/graphics/drawable/Drawable;
    if-eqz v7, :cond_4

    instance-of v8, v7, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v8, :cond_4

    .line 251
    sget-object v8, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    .line 252
    .local v8, "mBitmapConfig":Landroid/graphics/Bitmap$Config;
    nop

    .line 253
    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v9

    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v10

    .line 252
    invoke-static {v9, v10, v8}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v9

    invoke-direct {p0, v9, v1}, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->scaleWallpaperBitmapToScreenSize(Landroid/graphics/Bitmap;Landroid/graphics/Point;)Landroid/graphics/Bitmap;

    move-result-object v9

    move-object v0, v9

    .line 255
    const/high16 v9, -0x1000000

    invoke-virtual {v0, v9}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 256
    const/4 v2, 0x1

    .line 258
    .end local v6    # "pm":Landroid/content/pm/PackageManager;
    .end local v7    # "dr":Landroid/graphics/drawable/Drawable;
    .end local v8    # "mBitmapConfig":Landroid/graphics/Bitmap$Config;
    :cond_4
    goto :goto_1

    .line 259
    :cond_5
    invoke-direct {p0, v1}, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->getCurrentWallpaperLocked(Landroid/graphics/Point;)Landroid/graphics/Bitmap;

    move-result-object v6
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-object v0, v6

    .line 267
    :goto_1
    goto :goto_2

    .line 264
    .end local v5    # "info":Landroid/app/WallpaperInfo;
    :catch_1
    move-exception v5

    .line 265
    .local v5, "e":Ljava/lang/OutOfMemoryError;
    :try_start_3
    const-string v6, "WallpaperManagerServiceImpl"

    const-string v7, "No memory load current wallpaper"

    invoke-static {v6, v7, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 266
    const/4 v0, 0x0

    goto :goto_2

    .line 261
    .end local v5    # "e":Ljava/lang/OutOfMemoryError;
    :catch_2
    move-exception v5

    .line 262
    .local v5, "e":Ljava/lang/RuntimeException;
    const-string v6, "WallpaperManagerServiceImpl"

    const-string v7, "create blur wallpaper draw has some errors"

    invoke-static {v6, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    const/4 v0, 0x0

    .line 267
    .end local v5    # "e":Ljava/lang/RuntimeException;
    nop

    .line 268
    .end local v4    # "wallpaperUserId":I
    :goto_2
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 270
    if-eqz v0, :cond_6

    :try_start_4
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 271
    :cond_6
    const-string v3, "WallpaperManagerServiceImpl"

    const-string/jumbo v4, "wallpaper is null or has been recycled"

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    invoke-direct {p0, v1}, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->getDefaultWallpaperLocked(Landroid/graphics/Point;)Landroid/graphics/Bitmap;

    move-result-object v3

    move-object v0, v3

    .line 275
    :cond_7
    if-eqz v0, :cond_9

    .line 276
    if-eqz v2, :cond_8

    .line 277
    const-string v3, "blurwallpaper"

    invoke-direct {p0, v0, v3}, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->saveBlurWallpaperBitmapLocked(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    goto :goto_3

    .line 279
    :cond_8
    const/16 v3, 0xc8

    invoke-static {v0, v3}, Lcom/android/server/BlurUtils;->stackBlur(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 280
    .local v3, "bmp":Landroid/graphics/Bitmap;
    if-eqz v3, :cond_9

    .line 281
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 282
    const/high16 v4, 0x3f000000    # 0.5f

    invoke-static {v3, v4}, Lcom/android/server/BlurUtils;->addBlackBoard(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 283
    .local v4, "blurBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 284
    const-string v5, "blurwallpaper"

    invoke-direct {p0, v4, v5}, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->saveBlurWallpaperBitmapLocked(Landroid/graphics/Bitmap;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 291
    .end local v3    # "bmp":Landroid/graphics/Bitmap;
    .end local v4    # "blurBitmap":Landroid/graphics/Bitmap;
    :cond_9
    :goto_3
    if-eqz v0, :cond_a

    :goto_4
    const/4 v0, 0x0

    goto :goto_5

    :catchall_0
    move-exception v3

    goto :goto_6

    .line 288
    :catch_3
    move-exception v3

    .line 289
    .local v3, "e":Ljava/lang/OutOfMemoryError;
    :try_start_5
    const-string v4, "WallpaperManagerServiceImpl"

    const-string v5, "No memory load current wallpaper"

    invoke-static {v4, v5, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 291
    nop

    .end local v3    # "e":Ljava/lang/OutOfMemoryError;
    if-eqz v0, :cond_a

    goto :goto_4

    .line 293
    :cond_a
    :goto_5
    return-void

    .line 291
    :goto_6
    if-eqz v0, :cond_b

    const/4 v0, 0x0

    .line 292
    :cond_b
    throw v3

    .line 268
    :catchall_1
    move-exception v4

    :try_start_6
    monitor-exit v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v4
.end method

.method public getBlurWallpaper(Landroid/app/IWallpaperManagerCallback;)Landroid/os/ParcelFileDescriptor;
    .locals 7
    .param p1, "cb"    # Landroid/app/IWallpaperManagerCallback;

    .line 471
    iget-object v0, p0, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->mService:Lcom/android/server/wallpaper/WallpaperManagerService;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 473
    :cond_0
    invoke-virtual {v0}, Lcom/android/server/wallpaper/WallpaperManagerService;->getLock()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 474
    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->getWallpaperUserId()I

    move-result v2

    .line 475
    .local v2, "wallpaperUserId":I
    iget-object v3, p0, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->mService:Lcom/android/server/wallpaper/WallpaperManagerService;

    .line 476
    invoke-virtual {v3}, Lcom/android/server/wallpaper/WallpaperManagerService;->getWallpaperMap()Landroid/util/SparseArray;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/wallpaper/WallpaperData;

    .line 477
    .local v3, "wallpaper":Lcom/android/server/wallpaper/WallpaperData;
    if-eqz v3, :cond_3

    invoke-virtual {v3}, Lcom/android/server/wallpaper/WallpaperData;->getCallbacks()Landroid/os/RemoteCallbackList;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 478
    if-eqz p1, :cond_1

    invoke-virtual {v3}, Lcom/android/server/wallpaper/WallpaperData;->getCallbacks()Landroid/os/RemoteCallbackList;

    move-result-object v4

    invoke-virtual {v4, p1}, Landroid/os/RemoteCallbackList;->isContainIBinder(Landroid/os/IInterface;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 479
    invoke-virtual {v3}, Lcom/android/server/wallpaper/WallpaperData;->getCallbacks()Landroid/os/RemoteCallbackList;

    move-result-object v4

    invoke-virtual {v4, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 482
    :cond_1
    :try_start_1
    new-instance v4, Ljava/io/File;

    invoke-static {v2}, Landroid/os/Environment;->getUserSystemDirectory(I)Ljava/io/File;

    move-result-object v5

    const-string v6, "blurwallpaper"

    invoke-direct {v4, v5, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 484
    .local v4, "f":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v5, :cond_2

    .line 485
    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    return-object v1

    .line 487
    :cond_2
    const/high16 v5, 0x10000000

    :try_start_3
    invoke-static {v4, v5}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v1
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 488
    .local v1, "mParcelFile":Landroid/os/ParcelFileDescriptor;
    :try_start_4
    monitor-exit v0

    return-object v1

    .line 489
    .end local v1    # "mParcelFile":Landroid/os/ParcelFileDescriptor;
    .end local v4    # "f":Ljava/io/File;
    :catch_0
    move-exception v4

    .line 490
    .local v4, "e":Ljava/io/FileNotFoundException;
    const-string v5, "WallpaperManagerServiceImpl"

    const-string v6, "Error getting wallpaper"

    invoke-static {v5, v6, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 491
    monitor-exit v0

    return-object v1

    .line 494
    .end local v4    # "e":Ljava/io/FileNotFoundException;
    :cond_3
    monitor-exit v0

    return-object v1

    .line 495
    .end local v2    # "wallpaperUserId":I
    .end local v3    # "wallpaper":Lcom/android/server/wallpaper/WallpaperData;
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v1
.end method

.method public getWallpaperUserId()I
    .locals 2

    .line 504
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 505
    .local v0, "callingUid":I
    const/16 v1, 0x3e8

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->mService:Lcom/android/server/wallpaper/WallpaperManagerService;

    if-eqz v1, :cond_0

    .line 506
    invoke-virtual {v1}, Lcom/android/server/wallpaper/WallpaperManagerService;->getCurrentUserId()I

    move-result v0

    goto :goto_0

    .line 508
    :cond_0
    invoke-static {v0}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v0

    .line 510
    :goto_0
    return v0
.end method

.method public handleWallpaperObserverEvent(Lcom/android/server/wallpaper/WallpaperData;)V
    .locals 2
    .param p1, "wallpaper"    # Lcom/android/server/wallpaper/WallpaperData;

    .line 204
    iget-object v0, p0, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->mService:Lcom/android/server/wallpaper/WallpaperManagerService;

    if-nez v0, :cond_0

    return-void

    .line 206
    :cond_0
    const-string v0, "WallpaperManagerServiceImpl"

    const-string v1, "handleWallpaperObserverEvent"

    invoke-static {v0, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    iget-object v0, p0, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->blurWallpaperHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 209
    iget-object v0, p0, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->blurWallpaperHandler:Landroid/os/Handler;

    invoke-static {v0, v1, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 210
    .local v0, "message":Landroid/os/Message;
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 211
    return-void
.end method

.method public initBlurWallpaperThread()V
    .locals 2

    .line 528
    iget-object v0, p0, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->blurWallpaperThread:Landroid/os/HandlerThread;

    if-eqz v0, :cond_0

    .line 529
    return-void

    .line 531
    :cond_0
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "BlurWallpaperThread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->blurWallpaperThread:Landroid/os/HandlerThread;

    .line 532
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 533
    new-instance v0, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl$1;

    iget-object v1, p0, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->blurWallpaperThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl$1;-><init>(Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->blurWallpaperHandler:Landroid/os/Handler;

    .line 553
    return-void
.end method

.method public isMiuiWallpaperComponent(Landroid/content/ComponentName;)Z
    .locals 2
    .param p1, "componentName"    # Landroid/content/ComponentName;

    .line 87
    sget-object v0, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->MIUI_WALLPAPER_COMPONENTS:Ljava/util/List;

    invoke-virtual {p1}, Landroid/content/ComponentName;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isSuperWallpaper(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 92
    sget-object v0, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->SUPER_WALLPAPER_PACKAGE_NAMES:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public loadSettingLocked(Lcom/android/server/wallpaper/WallpaperDataParser;IZLcom/android/server/wallpaper/WallpaperData;Lcom/android/server/wallpaper/WallpaperData;I)Lcom/android/server/wallpaper/WallpaperDataParser$WallpaperLoadingResult;
    .locals 11
    .param p1, "parser"    # Lcom/android/server/wallpaper/WallpaperDataParser;
    .param p2, "userId"    # I
    .param p3, "keepDimensionHints"    # Z
    .param p4, "wallpaperData"    # Lcom/android/server/wallpaper/WallpaperData;
    .param p5, "lockWallpaperData"    # Lcom/android/server/wallpaper/WallpaperData;
    .param p6, "which"    # I

    .line 160
    sget-object v0, Lcom/android/server/wallpaper/WallpaperDataParserProxy;->makeJournaledFile:Lcom/xiaomi/reflect/RefMethod;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    filled-new-array {v1}, [Ljava/lang/Object;

    move-result-object v1

    move-object v2, p1

    invoke-virtual {v0, p1, v1}, Lcom/xiaomi/reflect/RefMethod;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/internal/util/JournaledFile;

    .line 161
    .local v0, "journal":Lcom/android/internal/util/JournaledFile;
    invoke-virtual/range {p1 .. p6}, Lcom/android/server/wallpaper/WallpaperDataParser;->loadSettingsLocked(IZLcom/android/server/wallpaper/WallpaperData;Lcom/android/server/wallpaper/WallpaperData;I)Lcom/android/server/wallpaper/WallpaperDataParser$WallpaperLoadingResult;

    move-result-object v1

    .line 167
    .local v1, "result":Lcom/android/server/wallpaper/WallpaperDataParser$WallpaperLoadingResult;
    invoke-virtual {v1}, Lcom/android/server/wallpaper/WallpaperDataParser$WallpaperLoadingResult;->getSystemWallpaperData()Lcom/android/server/wallpaper/WallpaperData;

    move-result-object v3

    .line 168
    .local v3, "resultSystemData":Lcom/android/server/wallpaper/WallpaperData;
    invoke-virtual {v1}, Lcom/android/server/wallpaper/WallpaperDataParser$WallpaperLoadingResult;->getLockWallpaperData()Lcom/android/server/wallpaper/WallpaperData;

    move-result-object v4

    .line 169
    .local v4, "resultLockData":Lcom/android/server/wallpaper/WallpaperData;
    invoke-virtual {v0}, Lcom/android/internal/util/JournaledFile;->chooseForRead()Ljava/io/File;

    move-result-object v5

    .line 170
    .local v5, "journalFile":Ljava/io/File;
    invoke-virtual {v1}, Lcom/android/server/wallpaper/WallpaperDataParser$WallpaperLoadingResult;->success()Z

    move-result v6

    if-eqz v6, :cond_1

    if-eqz v3, :cond_0

    iget-object v6, v3, Lcom/android/server/wallpaper/WallpaperData;->name:Ljava/lang/String;

    if-eqz v6, :cond_1

    :cond_0
    if-eqz v4, :cond_6

    iget-object v6, v4, Lcom/android/server/wallpaper/WallpaperData;->name:Ljava/lang/String;

    if-nez v6, :cond_6

    .line 173
    :cond_1
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v6

    const-string v7, "WallpaperManagerServiceImpl"

    if-eqz v6, :cond_4

    .line 174
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "unexpected situation when loadSetting, file may be irreversibly damaged, perform recovery. nullSystem?="

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v8, 0x1

    const/4 v9, 0x0

    if-eqz v3, :cond_2

    iget-object v10, v3, Lcom/android/server/wallpaper/WallpaperData;->name:Ljava/lang/String;

    if-nez v10, :cond_2

    move v10, v8

    goto :goto_0

    :cond_2
    move v10, v9

    :goto_0
    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v10, " nullLock?="

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    if-eqz v4, :cond_3

    iget-object v10, v4, Lcom/android/server/wallpaper/WallpaperData;->name:Ljava/lang/String;

    if-nez v10, :cond_3

    goto :goto_1

    :cond_3
    move v8, v9

    :goto_1
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " exist=true"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v7, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    goto :goto_2

    .line 182
    :cond_4
    const-string v6, "journalFile is not exist, first boot?"

    invoke-static {v7, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    :goto_2
    invoke-static {}, Lcom/android/server/MiuiFallbackHelperStub;->getInstance()Lcom/android/server/MiuiFallbackHelperStub;

    move-result-object v6

    .line 186
    invoke-virtual {v0}, Lcom/android/internal/util/JournaledFile;->chooseForRead()Ljava/io/File;

    move-result-object v8

    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/android/server/MiuiFallbackHelperStub;->restoreFile(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 187
    invoke-virtual/range {p1 .. p6}, Lcom/android/server/wallpaper/WallpaperDataParser;->loadSettingsLocked(IZLcom/android/server/wallpaper/WallpaperData;Lcom/android/server/wallpaper/WallpaperData;I)Lcom/android/server/wallpaper/WallpaperDataParser$WallpaperLoadingResult;

    move-result-object v1

    goto :goto_3

    .line 190
    :cond_5
    const-string v6, "restore fail, first boot?"

    invoke-static {v7, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    :cond_6
    :goto_3
    return-object v1
.end method

.method public saveSettingLocked(ILcom/android/server/wallpaper/WallpaperDataParser;Lcom/android/server/wallpaper/WallpaperData;Lcom/android/server/wallpaper/WallpaperData;)V
    .locals 5
    .param p1, "userId"    # I
    .param p2, "parser"    # Lcom/android/server/wallpaper/WallpaperDataParser;
    .param p3, "system"    # Lcom/android/server/wallpaper/WallpaperData;
    .param p4, "lock"    # Lcom/android/server/wallpaper/WallpaperData;

    .line 132
    const-string v0, "WallpaperManagerServiceImpl"

    if-nez p2, :cond_0

    .line 133
    const-string v1, "parser is null when perform saveSettingLocked"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    return-void

    .line 136
    :cond_0
    sget-object v1, Lcom/android/server/wallpaper/WallpaperDataParserProxy;->makeJournaledFile:Lcom/xiaomi/reflect/RefMethod;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    filled-new-array {v2}, [Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, p2, v2}, Lcom/xiaomi/reflect/RefMethod;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/internal/util/JournaledFile;

    .line 138
    .local v1, "journal":Lcom/android/internal/util/JournaledFile;
    :try_start_0
    invoke-virtual {p2, p1, p3, p4}, Lcom/android/server/wallpaper/WallpaperDataParser;->saveSettingsLocked(ILcom/android/server/wallpaper/WallpaperData;Lcom/android/server/wallpaper/WallpaperData;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 149
    goto :goto_0

    .line 139
    :catch_0
    move-exception v2

    .line 140
    .local v2, "e":Ljava/lang/Exception;
    const-string/jumbo v3, "unexpected situation when save settings, file may be irreversibly damaged, perform recovery"

    invoke-static {v0, v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 142
    invoke-virtual {v1}, Lcom/android/internal/util/JournaledFile;->chooseForRead()Ljava/io/File;

    move-result-object v0

    .line 144
    .local v0, "journalFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 146
    invoke-static {}, Lcom/android/server/MiuiFallbackHelperStub;->getInstance()Lcom/android/server/MiuiFallbackHelperStub;

    move-result-object v3

    .line 147
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/server/MiuiFallbackHelperStub;->restoreFile(Ljava/lang/String;)Z

    .line 148
    invoke-virtual {p2, p1, p3, p4}, Lcom/android/server/wallpaper/WallpaperDataParser;->saveSettingsLocked(ILcom/android/server/wallpaper/WallpaperData;Lcom/android/server/wallpaper/WallpaperData;)V

    .line 152
    .end local v0    # "journalFile":Ljava/io/File;
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_0
    invoke-static {}, Lcom/android/server/MiuiFallbackHelperStub;->getInstance()Lcom/android/server/MiuiFallbackHelperStub;

    move-result-object v0

    .line 153
    invoke-virtual {v1}, Lcom/android/internal/util/JournaledFile;->chooseForRead()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/android/server/MiuiFallbackHelperStub;->snapshotFile(Ljava/lang/String;)V

    .line 154
    return-void
.end method

.method public setWallpaperManagerService(Lcom/android/server/wallpaper/WallpaperManagerService;Landroid/content/Context;)V
    .locals 0
    .param p1, "service"    # Lcom/android/server/wallpaper/WallpaperManagerService;
    .param p2, "context"    # Landroid/content/Context;

    .line 199
    iput-object p1, p0, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->mService:Lcom/android/server/wallpaper/WallpaperManagerService;

    .line 200
    iput-object p2, p0, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->mContext:Landroid/content/Context;

    .line 201
    return-void
.end method

.method public wallpaperManagerServiceReady()V
    .locals 2

    .line 499
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/server/wallpaper/WallpaperManagerServiceImpl;->createBlurWallpaper(Z)V

    .line 500
    const-string v0, "WallpaperManagerServiceImpl"

    const-string v1, "create blur wallpaper"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 501
    return-void
.end method
