public class com.android.server.DarkModeSuggestProvider {
	 /* .source "DarkModeSuggestProvider.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/DarkModeSuggestProvider$DarkModeSuggestObserver; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String DARK_MODE_DATA_DISABLE_REGION;
private static final java.lang.String DARK_MODE_SUGGEST_MODULE_NAME;
private static final java.lang.String TAG;
private static volatile com.android.server.DarkModeSuggestProvider sDarkModeSuggestProvider;
/* # instance fields */
private android.database.ContentObserver mDarkModeSuggestObserver;
/* # direct methods */
static com.android.server.DarkModeSuggestProvider ( ) {
	 /* .locals 1 */
	 /* .line 25 */
	 /* const-class v0, Lcom/android/server/DarkModeSuggestProvider; */
	 (( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
	 return;
} // .end method
public com.android.server.DarkModeSuggestProvider ( ) {
	 /* .locals 0 */
	 /* .line 24 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 return;
} // .end method
public static com.android.server.DarkModeSuggestProvider getInstance ( ) {
	 /* .locals 2 */
	 /* .line 38 */
	 v0 = com.android.server.DarkModeSuggestProvider.sDarkModeSuggestProvider;
	 /* if-nez v0, :cond_1 */
	 /* .line 39 */
	 /* const-class v0, Lcom/android/server/DarkModeSuggestProvider; */
	 /* monitor-enter v0 */
	 /* .line 40 */
	 try { // :try_start_0
		 v1 = com.android.server.DarkModeSuggestProvider.sDarkModeSuggestProvider;
		 /* if-nez v1, :cond_0 */
		 /* .line 41 */
		 /* new-instance v1, Lcom/android/server/DarkModeSuggestProvider; */
		 /* invoke-direct {v1}, Lcom/android/server/DarkModeSuggestProvider;-><init>()V */
		 /* .line 43 */
	 } // :cond_0
	 /* monitor-exit v0 */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 /* monitor-exit v0 */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* throw v1 */
	 /* .line 45 */
} // :cond_1
} // :goto_0
v0 = com.android.server.DarkModeSuggestProvider.sDarkModeSuggestProvider;
} // .end method
/* # virtual methods */
public void registerDataObserver ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 86 */
v0 = com.android.server.DarkModeSuggestProvider.TAG;
final String v1 = "registerDataObserver"; // const-string v1, "registerDataObserver"
android.util.Slog .i ( v0,v1 );
/* .line 87 */
/* new-instance v0, Lcom/android/server/DarkModeSuggestProvider$DarkModeSuggestObserver; */
com.android.server.MiuiBgThread .getHandler ( );
/* invoke-direct {v0, p0, v1, p1}, Lcom/android/server/DarkModeSuggestProvider$DarkModeSuggestObserver;-><init>(Lcom/android/server/DarkModeSuggestProvider;Landroid/os/Handler;Landroid/content/Context;)V */
this.mDarkModeSuggestObserver = v0;
/* .line 88 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.MiuiSettings$SettingsCloudData .getCloudDataNotifyUri ( );
int v2 = 1; // const/4 v2, 0x1
v3 = this.mDarkModeSuggestObserver;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 90 */
return;
} // .end method
public void unRegisterDataObserver ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 93 */
v0 = com.android.server.DarkModeSuggestProvider.TAG;
/* const-string/jumbo v1, "unRegisterDataObserver" */
android.util.Slog .i ( v0,v1 );
/* .line 94 */
v0 = this.mDarkModeSuggestObserver;
/* if-nez v0, :cond_0 */
/* .line 95 */
return;
/* .line 97 */
} // :cond_0
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v1 = this.mDarkModeSuggestObserver;
(( android.content.ContentResolver ) v0 ).unregisterContentObserver ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V
/* .line 98 */
return;
} // .end method
public void updateCloudDataForDarkModeSuggest ( android.content.Context p0 ) {
/* .locals 6 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 50 */
final String v0 = "dark_mode_suggest"; // const-string v0, "dark_mode_suggest"
/* .line 51 */
try { // :try_start_0
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v2 = 0; // const/4 v2, 0x0
int v3 = 0; // const/4 v3, 0x0
android.provider.MiuiSettings$SettingsCloudData .getCloudDataSingle ( v1,v0,v0,v2,v3 );
/* .line 53 */
/* .local v1, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
if ( v1 != null) { // if-eqz v1, :cond_0
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v1 ).json ( ); // invoke-virtual {v1}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
if ( v2 != null) { // if-eqz v2, :cond_0
	 /* .line 54 */
	 v2 = com.android.server.DarkModeSuggestProvider.TAG;
	 /* new-instance v4, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
	 /* const-string/jumbo v5, "suggestData: " */
	 (( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v1 ).json ( ); // invoke-virtual {v1}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
	 (( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .d ( v2,v4 );
	 /* .line 55 */
	 (( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v1 ).json ( ); // invoke-virtual {v1}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
	 (( org.json.JSONObject ) v4 ).getJSONArray ( v0 ); // invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
	 (( org.json.JSONArray ) v0 ).getJSONObject ( v3 ); // invoke-virtual {v0, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
	 /* .line 56 */
	 /* .local v0, "suggestArray":Lorg/json/JSONObject; */
	 /* const-string/jumbo v3, "showDarkModeSuggest" */
	 v3 = 	 (( org.json.JSONObject ) v0 ).getBoolean ( v3 ); // invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z
	 com.android.server.DarkModeTimeModeHelper .setDarkModeSuggestEnable ( p1,v3 );
	 /* .line 57 */
	 /* new-instance v3, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v4 = "sHasGetSuggestFromCloud: "; // const-string v4, "sHasGetSuggestFromCloud: "
	 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v4 = 	 com.android.server.DarkModeTimeModeHelper .isDarkModeSuggestEnable ( p1 );
	 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .d ( v2,v3 );
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 61 */
} // .end local v0 # "suggestArray":Lorg/json/JSONObject;
} // .end local v1 # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
} // :cond_0
/* .line 59 */
/* :catch_0 */
/* move-exception v0 */
/* .line 60 */
/* .local v0, "e":Ljava/lang/Exception; */
v1 = com.android.server.DarkModeSuggestProvider.TAG;
final String v2 = "exception when updateCloudDataForDarkModeSuggest"; // const-string v2, "exception when updateCloudDataForDarkModeSuggest"
android.util.Slog .e ( v1,v2,v0 );
/* .line 62 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public java.util.Set updateCloudDataForDisableRegion ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/content/Context;", */
/* ")", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 66 */
final String v0 = "dark_mode_data_disable_region"; // const-string v0, "dark_mode_data_disable_region"
/* new-instance v1, Ljava/util/HashSet; */
/* invoke-direct {v1}, Ljava/util/HashSet;-><init>()V */
/* .line 69 */
/* .local v1, "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
/* nop */
/* .line 70 */
try { // :try_start_0
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 69 */
int v3 = 0; // const/4 v3, 0x0
android.provider.MiuiSettings$SettingsCloudData .getCloudDataString ( v2,v0,v0,v3 );
/* .line 71 */
/* .local v0, "data":Ljava/lang/String; */
v2 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v2, :cond_0 */
/* .line 72 */
/* new-instance v2, Lorg/json/JSONArray; */
/* invoke-direct {v2, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V */
/* .line 73 */
/* .local v2, "apps":Lorg/json/JSONArray; */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
v4 = (( org.json.JSONArray ) v2 ).length ( ); // invoke-virtual {v2}, Lorg/json/JSONArray;->length()I
/* if-ge v3, v4, :cond_0 */
/* .line 74 */
(( org.json.JSONArray ) v2 ).getString ( v3 ); // invoke-virtual {v2, v3}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;
/* .line 73 */
/* add-int/lit8 v3, v3, 0x1 */
/* .line 77 */
} // .end local v2 # "apps":Lorg/json/JSONArray;
} // .end local v3 # "i":I
} // :cond_0
v2 = com.android.server.DarkModeSuggestProvider.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "uploadCloudDataForDisableRegion: CloudList: " */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v3 );
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 80 */
/* nop */
} // .end local v0 # "data":Ljava/lang/String;
/* .line 78 */
/* :catch_0 */
/* move-exception v0 */
/* .line 79 */
/* .local v0, "e":Lorg/json/JSONException; */
v2 = com.android.server.DarkModeSuggestProvider.TAG;
final String v3 = "exception when updateDisableRegionList: "; // const-string v3, "exception when updateDisableRegionList: "
android.util.Slog .e ( v2,v3,v0 );
/* .line 81 */
} // .end local v0 # "e":Lorg/json/JSONException;
} // :goto_1
} // .end method
