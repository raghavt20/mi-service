.class Lcom/android/server/MiuiAppUsageStats$AppUsageStats;
.super Ljava/lang/Object;
.source "MiuiAppUsageStats.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/MiuiAppUsageStats;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AppUsageStats"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lcom/android/server/MiuiAppUsageStats$AppUsageStats;",
        ">;"
    }
.end annotation


# instance fields
.field public firstForeGroundTime:J

.field private foregroundCount:I

.field public lastBackGroundTime:J

.field private lastUsageTime:J

.field private pkgName:Ljava/lang/String;

.field final synthetic this$0:Lcom/android/server/MiuiAppUsageStats;

.field private totalForegroundTime:J


# direct methods
.method public constructor <init>(Lcom/android/server/MiuiAppUsageStats;Ljava/lang/String;)V
    .locals 3
    .param p1, "this$0"    # Lcom/android/server/MiuiAppUsageStats;
    .param p2, "pkgName"    # Ljava/lang/String;

    .line 400
    iput-object p1, p0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->this$0:Lcom/android/server/MiuiAppUsageStats;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 401
    iput-object p2, p0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->pkgName:Ljava/lang/String;

    .line 402
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->totalForegroundTime:J

    .line 403
    iput-wide v0, p0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->lastUsageTime:J

    .line 404
    const/4 v2, 0x0

    iput v2, p0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->foregroundCount:I

    .line 405
    iput-wide v0, p0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->firstForeGroundTime:J

    .line 406
    iput-wide v0, p0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->lastBackGroundTime:J

    .line 407
    return-void
.end method


# virtual methods
.method public addForegroundTime(J)V
    .locals 2
    .param p1, "foregroundTime"    # J

    .line 434
    iget-wide v0, p0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->totalForegroundTime:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->totalForegroundTime:J

    .line 435
    return-void
.end method

.method public compareTo(Lcom/android/server/MiuiAppUsageStats$AppUsageStats;)I
    .locals 4
    .param p1, "data"    # Lcom/android/server/MiuiAppUsageStats$AppUsageStats;

    .line 465
    iget-wide v0, p1, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->totalForegroundTime:J

    iget-wide v2, p0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->totalForegroundTime:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Long;->compare(JJ)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 391
    check-cast p1, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;

    invoke-virtual {p0, p1}, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->compareTo(Lcom/android/server/MiuiAppUsageStats$AppUsageStats;)I

    move-result p1

    return p1
.end method

.method public getForegroundCount()I
    .locals 1

    .line 418
    iget v0, p0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->foregroundCount:I

    return v0
.end method

.method public getLastUsageTime()J
    .locals 2

    .line 414
    iget-wide v0, p0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->lastUsageTime:J

    return-wide v0
.end method

.method public getPkgName()Ljava/lang/String;
    .locals 1

    .line 446
    iget-object v0, p0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->pkgName:Ljava/lang/String;

    return-object v0
.end method

.method public getTotalForegroundTime()J
    .locals 2

    .line 410
    iget-wide v0, p0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->totalForegroundTime:J

    return-wide v0
.end method

.method public increaseForegroundCount()V
    .locals 1

    .line 442
    iget v0, p0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->foregroundCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->foregroundCount:I

    .line 443
    return-void
.end method

.method public isValid()Z
    .locals 4

    .line 460
    iget-wide v0, p0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->totalForegroundTime:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public minusForegroundTime(J)V
    .locals 2
    .param p1, "foregroundTime"    # J

    .line 438
    iget-wide v0, p0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->totalForegroundTime:J

    sub-long/2addr v0, p1

    iput-wide v0, p0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->totalForegroundTime:J

    .line 439
    return-void
.end method

.method public setForegroundCount(I)V
    .locals 0
    .param p1, "foregroundCount"    # I

    .line 430
    iput p1, p0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->foregroundCount:I

    .line 431
    return-void
.end method

.method public setLastUsageTime(J)V
    .locals 0
    .param p1, "lastUsageTime"    # J

    .line 426
    iput-wide p1, p0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->lastUsageTime:J

    .line 427
    return-void
.end method

.method public setPkgName(Ljava/lang/String;)V
    .locals 0
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 450
    iput-object p1, p0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->pkgName:Ljava/lang/String;

    .line 451
    return-void
.end method

.method public setTotalForegroundTime(J)V
    .locals 0
    .param p1, "totalForegroundTime"    # J

    .line 422
    iput-wide p1, p0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->totalForegroundTime:J

    .line 423
    return-void
.end method

.method public updateLastUsageTime(J)V
    .locals 2
    .param p1, "time"    # J

    .line 454
    iget-wide v0, p0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->lastUsageTime:J

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 455
    iput-wide p1, p0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->lastUsageTime:J

    .line 457
    :cond_0
    return-void
.end method
