.class final Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler$BatteryUEventObserver;
.super Landroid/os/UEventObserver;
.source "MiuiBatteryStatsService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "BatteryUEventObserver"
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;


# direct methods
.method private constructor <init>(Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;)V
    .locals 0

    .line 703
    iput-object p1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    invoke-direct {p0}, Landroid/os/UEventObserver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler$BatteryUEventObserver-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler$BatteryUEventObserver;-><init>(Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;)V

    return-void
.end method


# virtual methods
.method public onUEvent(Landroid/os/UEventObserver$UEvent;)V
    .locals 5
    .param p1, "event"    # Landroid/os/UEventObserver$UEvent;

    .line 706
    const-string v0, "POWER_SUPPLY_REVERSE_CHG_MODE"

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x0

    if-eqz v1, :cond_0

    .line 707
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 708
    .local v0, "openStatus":I
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->-$$Nest$fgetmLastOpenStatus(Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;)I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 709
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    invoke-static {v1, v0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->-$$Nest$fputmLastOpenStatus(Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;I)V

    .line 710
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    const/4 v4, 0x2

    invoke-virtual {v1, v4, v2, v3}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(IJ)V

    .line 711
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->-$$Nest$fgetmLastOpenStatus(Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;)I

    move-result v1

    if-lez v1, :cond_0

    .line 712
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    const/16 v4, 0x11

    invoke-virtual {v1, v4, v2, v3}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(IJ)V

    .line 716
    .end local v0    # "openStatus":I
    :cond_0
    const-string v0, "POWER_SUPPLY_VBUS_DISABLE"

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 717
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 718
    .local v0, "vbusDisable":I
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->-$$Nest$fgetmLastVbusDisable(Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;)I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 719
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    invoke-static {v1, v0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->-$$Nest$fputmLastVbusDisable(Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;I)V

    .line 720
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    const/16 v4, 0xb

    invoke-virtual {v1, v4, v0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessage(II)V

    .line 723
    .end local v0    # "vbusDisable":I
    :cond_1
    const-string v0, "POWER_SUPPLY_CC_SHORT_VBUS"

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 724
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 725
    .local v0, "shortStatus":I
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    iget v1, v1, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mLastShortStatus:I

    if-eq v0, v1, :cond_2

    .line 726
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    iput v0, v1, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mLastShortStatus:I

    .line 729
    .end local v0    # "shortStatus":I
    :cond_2
    const-string v0, "POWER_SUPPLY_LPD_INFOMATION"

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 730
    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 731
    .local v0, "lpdInfomation":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 732
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    const/16 v4, 0x12

    invoke-virtual {v1, v4, v0, v2, v3}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(ILjava/lang/Object;J)V

    .line 735
    .end local v0    # "lpdInfomation":Ljava/lang/String;
    :cond_3
    const-string v0, "POWER_SUPPLY_MOISTURE_DET_STS"

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 736
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 737
    .local v0, "lpdState":I
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    iget-object v1, v1, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmLastLpdState(Lcom/android/server/MiuiBatteryStatsService;)I

    move-result v1

    if-eq v0, v1, :cond_4

    .line 738
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    iget-object v1, v1, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1, v0}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fputmLastLpdState(Lcom/android/server/MiuiBatteryStatsService;I)V

    .line 739
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    iget-object v1, v1, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmLastLpdState(Lcom/android/server/MiuiBatteryStatsService;)I

    move-result v1

    if-lez v1, :cond_4

    .line 740
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    iget-object v1, v1, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmLpdCount(Lcom/android/server/MiuiBatteryStatsService;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v1, v2}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fputmLpdCount(Lcom/android/server/MiuiBatteryStatsService;I)V

    .line 744
    .end local v0    # "lpdState":I
    :cond_4
    return-void
.end method
