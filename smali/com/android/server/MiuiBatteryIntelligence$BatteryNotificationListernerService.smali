.class Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;
.super Landroid/service/notification/NotificationListenerService;
.source "MiuiBatteryIntelligence.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/MiuiBatteryIntelligence;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BatteryNotificationListernerService"
.end annotation


# static fields
.field private static final ACTION_FULL_CHARGER_NAVIGATION:Ljava/lang/String; = "miui.intent.action.ACTION_FULL_CHARGE_NAVIGATION"

.field private static final EXTRA_FULL_CHARGER_NAVIGATION:Ljava/lang/String; = "miui.intent.extra.FULL_CHARGER_NAVIGATION"


# instance fields
.field private mAlreadyStartApp:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mAlreadyStartAppXSpace:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mIsNavigation:Z

.field final synthetic this$0:Lcom/android/server/MiuiBatteryIntelligence;


# direct methods
.method private constructor <init>(Lcom/android/server/MiuiBatteryIntelligence;)V
    .locals 0

    .line 450
    iput-object p1, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->this$0:Lcom/android/server/MiuiBatteryIntelligence;

    invoke-direct {p0}, Landroid/service/notification/NotificationListenerService;-><init>()V

    .line 453
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->mAlreadyStartApp:Ljava/util/ArrayList;

    .line 454
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->mAlreadyStartAppXSpace:Ljava/util/ArrayList;

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/MiuiBatteryIntelligence;Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;-><init>(Lcom/android/server/MiuiBatteryIntelligence;)V

    return-void
.end method

.method private checkNavigationEnd(Landroid/service/notification/StatusBarNotification;)V
    .locals 4
    .param p1, "sbn"    # Landroid/service/notification/StatusBarNotification;

    .line 505
    invoke-direct {p0, p1}, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->isNavigationStatus(Landroid/service/notification/StatusBarNotification;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 506
    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 507
    .local v0, "packageName":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getUserId()I

    move-result v1

    .line 508
    .local v1, "appUserId":I
    if-nez v1, :cond_0

    iget-object v2, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->mAlreadyStartApp:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 509
    iget-object v2, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->mAlreadyStartApp:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 510
    :cond_0
    const/16 v2, 0x3e7

    if-ne v1, v2, :cond_1

    iget-object v2, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->mAlreadyStartAppXSpace:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 511
    iget-object v2, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->mAlreadyStartAppXSpace:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 513
    :cond_1
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Owner Space Start APP list = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->mAlreadyStartApp:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  XSpace Start APP list = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->mAlreadyStartAppXSpace:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MiuiBatteryIntelligence"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 514
    iget-object v2, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->mAlreadyStartApp:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->mAlreadyStartAppXSpace:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 515
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->mIsNavigation:Z

    .line 516
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->sendNavigationBroadcast()V

    .line 519
    .end local v0    # "packageName":Ljava/lang/String;
    .end local v1    # "appUserId":I
    :cond_2
    return-void
.end method

.method private checkNavigationEntry(Landroid/service/notification/StatusBarNotification;)V
    .locals 4
    .param p1, "sbn"    # Landroid/service/notification/StatusBarNotification;

    .line 491
    invoke-direct {p0, p1}, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->isNavigationStatus(Landroid/service/notification/StatusBarNotification;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 492
    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 493
    .local v0, "packageName":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getUserId()I

    move-result v1

    .line 494
    .local v1, "appUserId":I
    if-nez v1, :cond_0

    iget-object v2, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->mAlreadyStartApp:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 495
    iget-object v2, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->mAlreadyStartApp:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 496
    :cond_0
    const/16 v2, 0x3e7

    if-ne v1, v2, :cond_1

    iget-object v2, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->mAlreadyStartAppXSpace:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 497
    iget-object v2, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->mAlreadyStartAppXSpace:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 499
    :cond_1
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Owner Space Start APP list = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->mAlreadyStartApp:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  XSpace Start APP list = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->mAlreadyStartAppXSpace:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MiuiBatteryIntelligence"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 500
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->processNavigation()V

    .line 502
    .end local v0    # "packageName":Ljava/lang/String;
    .end local v1    # "appUserId":I
    :cond_2
    return-void
.end method

.method private isNavigationStatus(Landroid/service/notification/StatusBarNotification;)Z
    .locals 4
    .param p1, "sbn"    # Landroid/service/notification/StatusBarNotification;

    .line 522
    const/4 v0, 0x0

    if-eqz p1, :cond_2

    invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->isOnwerUser()Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 525
    :cond_0
    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 526
    .local v1, "packName":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->isClearable()Z

    move-result v2

    .line 527
    .local v2, "isClearable":Z
    if-nez v2, :cond_1

    iget-object v3, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->this$0:Lcom/android/server/MiuiBatteryIntelligence;

    invoke-static {v3}, Lcom/android/server/MiuiBatteryIntelligence;->-$$Nest$fgetmMapApplist(Lcom/android/server/MiuiBatteryIntelligence;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 528
    const/4 v0, 0x1

    return v0

    .line 530
    :cond_1
    return v0

    .line 523
    .end local v1    # "packName":Ljava/lang/String;
    .end local v2    # "isClearable":Z
    :cond_2
    :goto_0
    return v0
.end method

.method private isOnwerUser()Z
    .locals 1

    .line 552
    invoke-static {}, Lmiui/securityspace/CrossUserUtils;->getCurrentUserId()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isTargetEnvironment()Z
    .locals 2

    .line 546
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Plugged? "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->this$0:Lcom/android/server/MiuiBatteryIntelligence;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryIntelligence;->-$$Nest$fgetmPlugged(Lcom/android/server/MiuiBatteryIntelligence;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " isNavigation? "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->mIsNavigation:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiBatteryIntelligence"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 547
    iget-object v0, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->this$0:Lcom/android/server/MiuiBatteryIntelligence;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryIntelligence;->-$$Nest$fgetmPlugged(Lcom/android/server/MiuiBatteryIntelligence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->mIsNavigation:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private processNavigation()V
    .locals 1

    .line 534
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->mIsNavigation:Z

    .line 535
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->sendNavigationBroadcast()V

    .line 536
    return-void
.end method

.method private sendNavigationBroadcast()V
    .locals 4

    .line 539
    new-instance v0, Landroid/content/Intent;

    const-string v1, "miui.intent.action.ACTION_FULL_CHARGE_NAVIGATION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 540
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "miui.intent.extra.FULL_CHARGER_NAVIGATION"

    invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->isTargetEnvironment()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 541
    const-string v1, "com.miui.securitycenter"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 542
    iget-object v1, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->this$0:Lcom/android/server/MiuiBatteryIntelligence;

    iget-object v1, v1, Lcom/android/server/MiuiBatteryIntelligence;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    const-string v3, "com.xiaomi.permission.miuibatteryintelligence"

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;)V

    .line 543
    return-void
.end method


# virtual methods
.method public clearStartAppList()V
    .locals 1

    .line 556
    iget-object v0, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->mAlreadyStartApp:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 557
    iget-object v0, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->mAlreadyStartAppXSpace:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 558
    return-void
.end method

.method public onListenerConnected()V
    .locals 4

    .line 460
    const/4 v0, 0x0

    .line 462
    .local v0, "postedNotifications":[Landroid/service/notification/StatusBarNotification;
    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->getActiveNotifications()[Landroid/service/notification/StatusBarNotification;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 465
    goto :goto_0

    .line 463
    :catch_0
    move-exception v1

    .line 464
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "MiuiBatteryIntelligence"

    const-string v3, "Get Notification Faild"

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 466
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    if-eqz v0, :cond_0

    .line 467
    array-length v1, v0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    .line 468
    .local v3, "sbn":Landroid/service/notification/StatusBarNotification;
    invoke-direct {p0, v3}, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->checkNavigationEntry(Landroid/service/notification/StatusBarNotification;)V

    .line 467
    .end local v3    # "sbn":Landroid/service/notification/StatusBarNotification;
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 471
    :cond_0
    return-void
.end method

.method public onListenerDisconnected()V
    .locals 2

    .line 475
    const-string v0, "MiuiBatteryIntelligence"

    const-string v1, "destory?"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 476
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->mIsNavigation:Z

    .line 477
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->sendNavigationBroadcast()V

    .line 478
    return-void
.end method

.method public onNotificationPosted(Landroid/service/notification/StatusBarNotification;)V
    .locals 0
    .param p1, "sbn"    # Landroid/service/notification/StatusBarNotification;

    .line 482
    invoke-direct {p0, p1}, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->checkNavigationEntry(Landroid/service/notification/StatusBarNotification;)V

    .line 483
    return-void
.end method

.method public onNotificationRemoved(Landroid/service/notification/StatusBarNotification;)V
    .locals 0
    .param p1, "sbn"    # Landroid/service/notification/StatusBarNotification;

    .line 487
    invoke-direct {p0, p1}, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->checkNavigationEnd(Landroid/service/notification/StatusBarNotification;)V

    .line 488
    return-void
.end method
