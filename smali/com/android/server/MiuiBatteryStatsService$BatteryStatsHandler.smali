.class Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;
.super Landroid/os/Handler;
.source "MiuiBatteryStatsService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/MiuiBatteryStatsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "BatteryStatsHandler"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler$BatteryUEventObserver;
    }
.end annotation


# static fields
.field private static final BATTERY_LPD_EVENT:Ljava/lang/String; = "POWER_SUPPLY_MOISTURE_DET_STS"

.field private static final BATTERY_LPD_INFOMATION:Ljava/lang/String; = "POWER_SUPPLY_LPD_INFOMATION"

.field private static final CC_SHORT_VBUS_EVENT:Ljava/lang/String; = "POWER_SUPPLY_CC_SHORT_VBUS"

.field private static final LPD_DM_RES:Ljava/lang/String; = "LPD_DM_RES="

.field private static final LPD_DP_RES:Ljava/lang/String; = "LPD_DP_RES="

.field private static final LPD_SBU1_RES:Ljava/lang/String; = "LPD_SBU1_RES="

.field private static final LPD_SBU2_RES:Ljava/lang/String; = "LPD_SBU2_RES="

.field public static final MSG_HANDLE_LPD_INFOMATION:I = 0x12

.field public static final MSG_HANDLE_NOT_FULLY_CHARGED:I = 0x15

.field public static final MSG_HANDLE_SLOW_CHARGE:I = 0xd

.field public static final MSG_HANDLE_SREIES_DELTA_VOLTAGE:I = 0xf

.field public static final MSG_INTERMITTENT_CHARGE:I = 0xe

.field public static final MSG_NOT_FULLY_CHARGED_ORDER_TIMER:I = 0x14

.field public static final MSG_RESTORE_LAST_STATE:I = 0xa

.field public static final MSG_SAVE_BATT_INFO:I = 0x9

.field public static final MSG_SEND_BATTERY_EXCEPTION_TIME:I = 0x8

.field public static final MSG_SEND_BATTERY_TEMP:I = 0x7

.field public static final MSG_SLOW_CHARGE:I = 0xc

.field public static final MSG_TEMP_CONTROL_VOLTAGE:I = 0x10

.field public static final MSG_UPDATE_BATTERY_HEALTH:I = 0x0

.field public static final MSG_UPDATE_BATTERY_TEMP:I = 0x6

.field public static final MSG_UPDATE_CHARGE:I = 0x1

.field public static final MSG_UPDATE_CHARGE_ACTION:I = 0x4

.field public static final MSG_UPDATE_LPD_COUNT:I = 0x13

.field public static final MSG_UPDATE_POWER_OFF_DELAY:I = 0x5

.field public static final MSG_UPDATE_USB_FUNCTION:I = 0x3

.field public static final MSG_UPDATE_VBUS_DISALE:I = 0xb

.field public static final MSG_UPDATE_WIRELESS_REVERSE_CHARGE:I = 0x2

.field public static final MSG_WIRELESS_COMPOSITE:I = 0x11

.field private static final POEER_50:Ljava/lang/String; = "50"

.field private static final POWER_0:Ljava/lang/String; = "0"

.field private static final POWER_10:Ljava/lang/String; = "10"

.field private static final POWER_15:Ljava/lang/String; = "15"

.field private static final POWER_18:Ljava/lang/String; = "18"

.field private static final POWER_20:Ljava/lang/String; = "20"

.field private static final POWER_22_5:Ljava/lang/String; = "22.5"

.field private static final POWER_27:Ljava/lang/String; = "27"

.field private static final POWER_2_5:Ljava/lang/String; = "2.5"

.field private static final POWER_30:Ljava/lang/String; = "30"

.field private static final POWER_5:Ljava/lang/String; = "5"

.field private static final POWER_7_5:Ljava/lang/String; = "7.5"

.field private static final REVERSE_CHG_MODE_EVENT:Ljava/lang/String; = "POWER_SUPPLY_REVERSE_CHG_MODE"

.field private static final VBUS_DISABLE_EVENT:Ljava/lang/String; = "POWER_SUPPLY_VBUS_DISABLE"

.field public static final WIRELESS_OTG:I = 0x0

.field public static final WIRELESS_REVERSE_OTG:I = 0x1

.field public static final WIRELESS_REVERSE_WIRED:I = 0x2


# instance fields
.field private BATTER_HEALTH_PARAMS:[Ljava/lang/String;

.field private CHARGE_PARAMS:[Ljava/lang/String;

.field private SLOW_CHARGE_TYPE:[Ljava/lang/String;

.field private TEMP_COLLECTED_SCENE:[Ljava/lang/String;

.field private TX_ADAPT_POWER:[Ljava/lang/String;

.field private WIRELESS_COMPOSITE_TYPE:[Ljava/lang/String;

.field private WIRELESS_REVERSE_CHARGE_PARAMS:[Ljava/lang/String;

.field private mBatteryTemps:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mChargePowerHashMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mChargeType:Ljava/lang/String;

.field private mDeltaV:I

.field private mIsDeltaVAssigned:Z

.field private mLastOpenStatus:I

.field public mLastShortStatus:I

.field private mLastVbusDisable:I

.field private mNotUsbFunction:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mTxAdapter:Ljava/lang/String;

.field private final mUEventObserver:Landroid/os/UEventObserver;

.field final synthetic this$0:Lcom/android/server/MiuiBatteryStatsService;


# direct methods
.method static bridge synthetic -$$Nest$fgetmLastOpenStatus(Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;)I
    .locals 0

    iget p0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mLastOpenStatus:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLastVbusDisable(Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;)I
    .locals 0

    iget p0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mLastVbusDisable:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmLastOpenStatus(Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mLastOpenStatus:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLastVbusDisable(Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mLastVbusDisable:I

    return-void
.end method

.method static bridge synthetic -$$Nest$mformatTime(Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;J)Ljava/lang/String;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->formatTime(J)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mhandleNonlinearChangeOfCapacity(Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->handleNonlinearChangeOfCapacity(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msendAdjustVolBroadcast(Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendAdjustVolBroadcast()V

    return-void
.end method

.method public constructor <init>(Lcom/android/server/MiuiBatteryStatsService;Landroid/os/Looper;)V
    .locals 19
    .param p1, "this$0"    # Lcom/android/server/MiuiBatteryStatsService;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 491
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    iput-object v1, v0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    .line 492
    move-object/from16 v2, p2

    invoke-direct {v0, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 400
    const-string v3, "host_connected"

    const-string v4, "connected"

    const-string v5, "configured"

    const-string/jumbo v6, "unlocked"

    filled-new-array {v5, v6, v3, v4}, [Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    iput-object v3, v0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mNotUsbFunction:Ljava/util/List;

    .line 402
    const/4 v3, 0x0

    iput-object v3, v0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mChargeType:Ljava/lang/String;

    .line 403
    iput-object v3, v0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mTxAdapter:Ljava/lang/String;

    .line 405
    const/4 v4, 0x0

    iput v4, v0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mDeltaV:I

    .line 406
    iput-boolean v4, v0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mIsDeltaVAssigned:Z

    .line 414
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iput-object v4, v0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mChargePowerHashMap:Ljava/util/HashMap;

    .line 415
    new-instance v4, Landroid/util/ArrayMap;

    invoke-direct {v4}, Landroid/util/ArrayMap;-><init>()V

    iput-object v4, v0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mBatteryTemps:Landroid/util/ArrayMap;

    .line 418
    const-string v5, "2.5"

    const-string v6, "5"

    const-string v7, "5"

    const-string v8, "0"

    const-string v9, "5"

    const-string v10, "10"

    const-string v11, "10"

    const-string v12, "10"

    const-string v13, "20"

    const-string v14, "20"

    const-string v15, "20"

    const-string v16, "30"

    const-string v17, "30"

    const-string v18, "50"

    filled-new-array/range {v5 .. v18}, [Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->TX_ADAPT_POWER:[Ljava/lang/String;

    .line 435
    const-string v4, "charger_full"

    const-string v5, "battery_authentic"

    const-string v6, "cyclecount"

    const-string/jumbo v7, "soh"

    filled-new-array {v6, v7, v4, v5}, [Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->BATTER_HEALTH_PARAMS:[Ljava/lang/String;

    .line 442
    const-string v5, "charger_type"

    const-string/jumbo v6, "usb_voltage"

    const-string/jumbo v7, "usb_current"

    const-string v8, "charge_power"

    const-string/jumbo v9, "tx_adapter"

    const-string/jumbo v10, "tx_uuid"

    const-string v11, "pd_authentication"

    const-string v12, "pd_apdoMax"

    const-string/jumbo v13, "vbat"

    const-string v14, "capacity"

    const-string v15, "ibat"

    const-string v16, "Tbat"

    const-string v17, "resistance"

    const-string/jumbo v18, "thermal_level"

    filled-new-array/range {v5 .. v18}, [Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->CHARGE_PARAMS:[Ljava/lang/String;

    .line 459
    const-string/jumbo v4, "wireless_reverse_enable"

    filled-new-array {v4}, [Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->WIRELESS_REVERSE_CHARGE_PARAMS:[Ljava/lang/String;

    .line 463
    const-string/jumbo v5, "wiredOpenFFC"

    const-string/jumbo v6, "wirelessOpenFFC"

    const-string/jumbo v7, "wiredNotOpenFFC"

    const-string/jumbo v8, "wirelessNotOpenFFC"

    const-string v9, "releaseCharge"

    const-string v10, "reverseWirelessCharge"

    filled-new-array/range {v5 .. v10}, [Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->TEMP_COLLECTED_SCENE:[Ljava/lang/String;

    .line 472
    const-string/jumbo v4, "wiredNormalSlowCharge"

    const-string/jumbo v5, "wiredSlowCharge"

    const-string/jumbo v6, "wiredRecognizedUsbFloat"

    filled-new-array {v6, v4, v5}, [Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->SLOW_CHARGE_TYPE:[Ljava/lang/String;

    .line 478
    const-string/jumbo v4, "wireless_reverse_otg"

    const-string/jumbo v5, "wireless_reverse_wired"

    const-string/jumbo v6, "wireless_otg"

    filled-new-array {v6, v4, v5}, [Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->WIRELESS_COMPOSITE_TYPE:[Ljava/lang/String;

    .line 485
    const/4 v4, -0x1

    iput v4, v0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mLastVbusDisable:I

    .line 494
    invoke-direct/range {p0 .. p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->initWiredChargePower()V

    .line 495
    invoke-direct/range {p0 .. p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->initBatteryTemp()V

    .line 496
    new-instance v4, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler$BatteryUEventObserver;

    invoke-direct {v4, v0, v3}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler$BatteryUEventObserver;-><init>(Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler$BatteryUEventObserver-IA;)V

    iput-object v4, v0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mUEventObserver:Landroid/os/UEventObserver;

    .line 497
    const-string v3, "POWER_SUPPLY_REVERSE_CHG_MODE"

    invoke-virtual {v4, v3}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V

    .line 498
    const-string v3, "POWER_SUPPLY_VBUS_DISABLE"

    invoke-virtual {v4, v3}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V

    .line 499
    const-string v3, "POWER_SUPPLY_CC_SHORT_VBUS"

    invoke-virtual {v4, v3}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V

    .line 500
    const-string v3, "POWER_SUPPLY_LPD_INFOMATION"

    invoke-virtual {v4, v3}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V

    .line 501
    const-string v3, "POWER_SUPPLY_MOISTURE_DET_STS"

    invoke-virtual {v4, v3}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V

    .line 502
    invoke-static/range {p1 .. p1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v3

    const-string/jumbo v4, "smart_batt"

    invoke-virtual {v3, v4}, Lmiui/util/IMiCharge;->isFunctionSupported(Ljava/lang/String;)Z

    move-result v3

    invoke-static {v1, v3}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fputmSupportedSB(Lcom/android/server/MiuiBatteryStatsService;Z)V

    .line 503
    invoke-static/range {p1 .. p1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v3

    const-string v4, "cell1_volt"

    invoke-virtual {v3, v4}, Lmiui/util/IMiCharge;->isFunctionSupported(Ljava/lang/String;)Z

    move-result v3

    invoke-static {v1, v3}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fputmSupportedCellVolt(Lcom/android/server/MiuiBatteryStatsService;Z)V

    .line 504
    return-void
.end method

.method private formatTime(J)Ljava/lang/String;
    .locals 3
    .param p1, "timestamp"    # J

    .line 601
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "yyyy-MM-dd HH:mm:ss"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 602
    .local v0, "sdf":Ljava/text/SimpleDateFormat;
    const-wide/16 v1, 0x0

    cmp-long v1, p1, v1

    if-lez v1, :cond_0

    .line 603
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 605
    :cond_0
    const-string v1, "None"

    return-object v1
.end method

.method private getBatteryChargeType()Ljava/lang/String;
    .locals 1

    .line 540
    iget-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/util/IMiCharge;->getBatteryChargeType()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getChargingPowerMax()I
    .locals 2

    .line 524
    iget-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/util/IMiCharge;->getChargingPowerMax()Ljava/lang/String;

    move-result-object v0

    .line 525
    .local v0, "powerMax":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 526
    invoke-virtual {p0, v0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I

    move-result v1

    return v1

    .line 528
    :cond_0
    const/4 v1, -0x1

    return v1
.end method

.method private getPdAuthentication()I
    .locals 2

    .line 532
    iget-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/util/IMiCharge;->getPdAuthentication()Ljava/lang/String;

    move-result-object v0

    .line 533
    .local v0, "pdAuth":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 534
    invoke-virtual {p0, v0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I

    move-result v1

    return v1

    .line 536
    :cond_0
    const/4 v1, -0x1

    return v1
.end method

.method private getTimeHours(J)Ljava/lang/String;
    .locals 14
    .param p1, "totalMilliSeconds"    # J

    .line 609
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 610
    const-wide/16 v0, 0x3e8

    div-long v0, p1, v0

    .line 611
    .local v0, "totalSeconds":J
    const-wide/16 v2, 0x3c

    rem-long v4, v0, v2

    .line 612
    .local v4, "currentSecond":J
    div-long v6, v0, v2

    .line 613
    .local v6, "totalMinutes":J
    rem-long v8, v6, v2

    .line 614
    .local v8, "currentMinute":J
    div-long v2, v6, v2

    .line 615
    .local v2, "totalHour":J
    const-wide/16 v10, 0x18

    rem-long v10, v2, v10

    .line 616
    .local v10, "currentHour":J
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "h"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "min"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "s"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    return-object v12

    .line 618
    .end local v0    # "totalSeconds":J
    .end local v2    # "totalHour":J
    .end local v4    # "currentSecond":J
    .end local v6    # "totalMinutes":J
    .end local v8    # "currentMinute":J
    .end local v10    # "currentHour":J
    :cond_0
    const-string v0, "None"

    return-object v0
.end method

.method private handleBatteryHealth()V
    .locals 10

    .line 830
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 832
    .local v0, "params":Landroid/os/Bundle;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->BATTER_HEALTH_PARAMS:[Ljava/lang/String;

    array-length v3, v2

    const/4 v4, 0x0

    const-string v5, "MiuiBatteryStatsService"

    if-ge v1, v3, :cond_3

    .line 834
    :try_start_0
    aget-object v2, v2, v1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v6, "charger_full"

    const-string v7, "battery_authentic"

    const-string/jumbo v8, "soh"

    const-string v9, "cyclecount"

    sparse-switch v3, :sswitch_data_0

    :cond_0
    goto :goto_1

    :sswitch_0
    :try_start_1
    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v4, 0x2

    goto :goto_2

    :sswitch_1
    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v4, 0x3

    goto :goto_2

    :sswitch_2
    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v4, 0x1

    goto :goto_2

    :sswitch_3
    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_2

    :goto_1
    const/4 v4, -0x1

    :goto_2
    packed-switch v4, :pswitch_data_0

    .line 855
    goto :goto_3

    .line 851
    :pswitch_0
    iget-object v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v2

    invoke-virtual {v2}, Lmiui/util/IMiCharge;->getBatteryAuthentic()Ljava/lang/String;

    move-result-object v2

    .line 852
    .local v2, "value":Ljava/lang/String;
    invoke-virtual {v0, v7, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 853
    goto :goto_4

    .line 847
    .end local v2    # "value":Ljava/lang/String;
    :pswitch_1
    iget-object v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v2

    invoke-virtual {v2}, Lmiui/util/IMiCharge;->getBatteryChargeFull()Ljava/lang/String;

    move-result-object v2

    .line 848
    .restart local v2    # "value":Ljava/lang/String;
    invoke-virtual {v0, v6, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 849
    goto :goto_4

    .line 840
    .end local v2    # "value":Ljava/lang/String;
    :pswitch_2
    iget-object v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v2

    invoke-virtual {v2}, Lmiui/util/IMiCharge;->getBatterySoh()Ljava/lang/String;

    move-result-object v2

    .line 841
    .restart local v2    # "value":Ljava/lang/String;
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_2

    .line 842
    :cond_1
    const-string v3, "-1"

    move-object v2, v3

    .line 844
    :cond_2
    invoke-virtual {v0, v8, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 845
    goto :goto_4

    .line 836
    .end local v2    # "value":Ljava/lang/String;
    :pswitch_3
    iget-object v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v2

    invoke-virtual {v2}, Lmiui/util/IMiCharge;->getBatteryCycleCount()Ljava/lang/String;

    move-result-object v2

    .line 837
    .restart local v2    # "value":Ljava/lang/String;
    invoke-virtual {v0, v9, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 838
    goto :goto_4

    .line 855
    .end local v2    # "value":Ljava/lang/String;
    :goto_3
    const-string v2, "nothing to handle battery health"

    invoke-static {v5, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 859
    :goto_4
    goto :goto_5

    .line 857
    :catch_0
    move-exception v2

    .line 858
    .local v2, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "read file about battery health error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 832
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_5
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 861
    .end local v1    # "i":I
    :cond_3
    const-string v1, "cc_short_vbus"

    iget v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mLastShortStatus:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 862
    iput v4, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mLastShortStatus:I

    .line 863
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetDEBUG(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 864
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BATTER_HEALTH_PARAMS params = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 866
    :cond_4
    const-string v1, "31000000094"

    const-string v2, "battery_health"

    invoke-direct {p0, v0, v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendOneTrackInfo(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    .line 867
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v1

    const-string v2, "lpd_update_en"

    const-string v3, "1"

    invoke-virtual {v1, v2, v3}, Lmiui/util/IMiCharge;->setMiChargePath(Ljava/lang/String;Ljava/lang/String;)Z

    .line 868
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x440aa817 -> :sswitch_3
        0x1bd8c -> :sswitch_2
        0x4ec3aeeb -> :sswitch_1
        0x7f4a9df0 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private handleBatteryTemp(Landroid/content/Intent;)V
    .locals 12
    .param p1, "intent"    # Landroid/content/Intent;

    .line 1133
    const-string v0, "plugged"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 1134
    .local v0, "currentPlugType":I
    const-string/jumbo v1, "status"

    const/4 v2, 0x1

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 1135
    .local v1, "currentChargeStatus":I
    const-string/jumbo v3, "temperature"

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 1137
    .local v3, "currentTemp":I
    const/4 v5, 0x0

    .line 1138
    .local v5, "wirelessReverseValue":I
    const/4 v6, 0x0

    .line 1140
    .local v6, "fastChargeModeValue":I
    iget-object v7, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v7}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v7

    invoke-virtual {v7}, Lmiui/util/IMiCharge;->getWirelessReverseStatus()Ljava/lang/String;

    move-result-object v7

    .line 1141
    .local v7, "wirelessReverseStatus":Ljava/lang/String;
    if-eqz v7, :cond_0

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v8

    if-eqz v8, :cond_0

    .line 1142
    invoke-virtual {p0, v7}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 1144
    :cond_0
    iget-object v8, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v8}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v8

    invoke-virtual {v8}, Lmiui/util/IMiCharge;->getFastChargeModeStatus()Ljava/lang/String;

    move-result-object v8

    .line 1145
    .local v8, "fastChargeStatus":Ljava/lang/String;
    if-eqz v8, :cond_1

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v9

    if-eqz v9, :cond_1

    .line 1146
    invoke-virtual {p0, v8}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 1148
    :cond_1
    iget-object v9, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v9}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetDEBUG(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 1149
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "currentPlugType = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " currentChargeStatus = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " currentTemp = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " wirelessReverseStatus = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " fastChargeStatus = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "MiuiBatteryStatsService"

    invoke-static {v10, v9}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1154
    :cond_2
    if-lez v0, :cond_4

    .line 1155
    iget-object v9, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v9}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmChargeMaxTemp(Lcom/android/server/MiuiBatteryStatsService;)I

    move-result v9

    if-ge v9, v3, :cond_3

    .line 1156
    iget-object v9, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v9, v3}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fputmChargeMaxTemp(Lcom/android/server/MiuiBatteryStatsService;I)V

    .line 1158
    :cond_3
    iget-object v9, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v9}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmChargeMinTemp(Lcom/android/server/MiuiBatteryStatsService;)I

    move-result v9

    if-le v9, v3, :cond_4

    .line 1159
    iget-object v9, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v9, v3}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fputmChargeMinTemp(Lcom/android/server/MiuiBatteryStatsService;I)V

    .line 1163
    :cond_4
    const/4 v9, 0x2

    if-ne v1, v9, :cond_6

    if-eq v0, v2, :cond_5

    if-ne v0, v9, :cond_6

    :cond_5
    if-ne v6, v2, :cond_6

    .line 1166
    iget-object v10, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mBatteryTemps:Landroid/util/ArrayMap;

    iget-object v11, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->TEMP_COLLECTED_SCENE:[Ljava/lang/String;

    aget-object v4, v11, v4

    invoke-virtual {v10, v4}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;

    invoke-virtual {p0, v4, v3}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->calculateTemp(Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;I)V

    .line 1169
    :cond_6
    const/4 v4, 0x4

    if-ne v1, v9, :cond_7

    if-ne v0, v4, :cond_7

    if-ne v6, v2, :cond_7

    .line 1172
    iget-object v10, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mBatteryTemps:Landroid/util/ArrayMap;

    iget-object v11, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->TEMP_COLLECTED_SCENE:[Ljava/lang/String;

    aget-object v11, v11, v2

    invoke-virtual {v10, v11}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;

    invoke-virtual {p0, v10, v3}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->calculateTemp(Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;I)V

    .line 1175
    :cond_7
    if-ne v1, v9, :cond_9

    if-eq v0, v2, :cond_8

    if-ne v0, v9, :cond_9

    :cond_8
    if-nez v6, :cond_9

    .line 1178
    iget-object v10, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mBatteryTemps:Landroid/util/ArrayMap;

    iget-object v11, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->TEMP_COLLECTED_SCENE:[Ljava/lang/String;

    aget-object v11, v11, v9

    invoke-virtual {v10, v11}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;

    invoke-virtual {p0, v10, v3}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->calculateTemp(Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;I)V

    .line 1181
    :cond_9
    const/4 v10, 0x3

    if-ne v1, v9, :cond_a

    if-ne v0, v4, :cond_a

    if-nez v6, :cond_a

    .line 1184
    iget-object v9, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mBatteryTemps:Landroid/util/ArrayMap;

    iget-object v11, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->TEMP_COLLECTED_SCENE:[Ljava/lang/String;

    aget-object v11, v11, v10

    invoke-virtual {v9, v11}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;

    invoke-virtual {p0, v9, v3}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->calculateTemp(Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;I)V

    .line 1187
    :cond_a
    if-nez v5, :cond_b

    if-ne v1, v10, :cond_b

    .line 1188
    iget-object v9, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mBatteryTemps:Landroid/util/ArrayMap;

    iget-object v10, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->TEMP_COLLECTED_SCENE:[Ljava/lang/String;

    aget-object v4, v10, v4

    invoke-virtual {v9, v4}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;

    invoke-virtual {p0, v4, v3}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->calculateTemp(Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;I)V

    .line 1191
    :cond_b
    if-ne v5, v2, :cond_c

    .line 1192
    iget-object v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mBatteryTemps:Landroid/util/ArrayMap;

    iget-object v4, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->TEMP_COLLECTED_SCENE:[Ljava/lang/String;

    const/4 v9, 0x5

    aget-object v4, v4, v9

    invoke-virtual {v2, v4}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;

    invoke-virtual {p0, v2, v3}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->calculateTemp(Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;I)V

    .line 1194
    :cond_c
    return-void
.end method

.method private handleCharge()V
    .locals 25

    .line 871
    move-object/from16 v1, p0

    const-string v2, "WirelessCharging"

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    move-object v3, v0

    .line 873
    .local v3, "params":Landroid/os/Bundle;
    const-string v0, "00.00.00.00"

    .line 874
    .local v0, "txUuid":Ljava/lang/String;
    const/4 v4, 0x0

    .line 876
    .local v4, "txAdapterValue":I
    iget-object v5, v1, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v5

    invoke-virtual {v5}, Lmiui/util/IMiCharge;->getBatteryChargeType()Ljava/lang/String;

    move-result-object v5

    .line 877
    .local v5, "chargeType":Ljava/lang/String;
    iget-object v6, v1, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v6}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v6

    invoke-virtual {v6}, Lmiui/util/IMiCharge;->getTxAdapt()Ljava/lang/String;

    move-result-object v6

    .line 878
    .local v6, "txAdapter":Ljava/lang/String;
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_0

    .line 879
    invoke-virtual {v1, v6}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I

    move-result v4

    goto :goto_0

    .line 881
    :cond_0
    const-string v6, "-1"

    .line 883
    :goto_0
    iput-object v6, v1, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mTxAdapter:Ljava/lang/String;

    .line 884
    iget-object v7, v1, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v7}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v7

    const-string/jumbo v8, "wireless_tx_uuid"

    invoke-virtual {v7, v8}, Lmiui/util/IMiCharge;->isFunctionSupported(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 885
    iget-object v7, v1, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v7}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v7

    invoke-virtual {v7, v8}, Lmiui/util/IMiCharge;->getMiChargePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v7, v0

    goto :goto_1

    .line 884
    :cond_1
    move-object v7, v0

    .line 888
    .end local v0    # "txUuid":Ljava/lang/String;
    .local v7, "txUuid":Ljava/lang/String;
    :goto_1
    const/4 v0, 0x0

    move v8, v0

    .local v8, "i":I
    :goto_2
    iget-object v0, v1, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->CHARGE_PARAMS:[Ljava/lang/String;

    array-length v9, v0

    const-string v10, "MiuiBatteryStatsService"

    if-ge v8, v9, :cond_15

    .line 890
    :try_start_0
    aget-object v0, v0, v8

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v9
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_b

    const-string/jumbo v12, "tx_adapter"

    const-string v13, "resistance"

    const-string/jumbo v14, "usb_current"

    const-string/jumbo v15, "usb_voltage"

    const-string/jumbo v11, "thermal_level"

    move/from16 v16, v8

    .end local v8    # "i":I
    .local v16, "i":I
    const-string v8, "pd_apdoMax"

    move-object/from16 v17, v10

    const-string/jumbo v10, "vbat"

    move-object/from16 v18, v2

    const-string v2, "ibat"

    move-object/from16 v19, v5

    .end local v5    # "chargeType":Ljava/lang/String;
    .local v19, "chargeType":Ljava/lang/String;
    const-string v5, "Tbat"

    move/from16 v20, v4

    .end local v4    # "txAdapterValue":I
    .local v20, "txAdapterValue":I
    const-string v4, "capacity"

    move-object/from16 v21, v6

    .end local v6    # "txAdapter":Ljava/lang/String;
    .local v21, "txAdapter":Ljava/lang/String;
    const-string v6, "pd_authentication"

    move-object/from16 v22, v7

    .end local v7    # "txUuid":Ljava/lang/String;
    .local v22, "txUuid":Ljava/lang/String;
    const-string/jumbo v7, "tx_uuid"

    move-object/from16 v23, v3

    .end local v3    # "params":Landroid/os/Bundle;
    .local v23, "params":Landroid/os/Bundle;
    const-string v3, "charger_type"

    const-string v1, "charge_power"

    move-object/from16 v24, v7

    const/4 v7, 0x1

    sparse-switch v9, :sswitch_data_0

    move-object/from16 v9, v24

    goto/16 :goto_4

    :sswitch_0
    :try_start_1
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    goto/16 :goto_3

    :sswitch_1
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x4

    goto/16 :goto_3

    :sswitch_2
    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0xc

    goto :goto_3

    :sswitch_3
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    goto :goto_3

    :sswitch_4
    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move-object/from16 v9, v24

    const/4 v0, 0x2

    goto/16 :goto_5

    :sswitch_5
    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v7

    goto :goto_3

    :sswitch_6
    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0xd

    goto :goto_3

    :sswitch_7
    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x7

    goto :goto_3

    :sswitch_8
    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0x8

    goto :goto_3

    :sswitch_9
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0xa

    goto :goto_3

    :sswitch_a
    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0xb

    goto :goto_3

    :sswitch_b
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0x9

    goto :goto_3

    :sswitch_c
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x6

    :goto_3
    move-object/from16 v9, v24

    goto :goto_5

    :cond_2
    move-object/from16 v9, v24

    goto :goto_4

    :sswitch_d
    move-object/from16 v9, v24

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-eqz v0, :cond_3

    const/4 v0, 0x5

    goto :goto_5

    .line 986
    :catch_0
    move-exception v0

    move-object/from16 v4, p0

    move-object/from16 v3, v17

    move-object/from16 v5, v18

    move-object/from16 v9, v19

    move/from16 v8, v20

    move-object/from16 v6, v21

    move-object/from16 v2, v22

    move-object/from16 v1, v23

    goto/16 :goto_b

    .line 890
    :cond_3
    :goto_4
    const/4 v0, -0x1

    :goto_5
    const-string v24, "-1"

    packed-switch v0, :pswitch_data_0

    .line 984
    move-object/from16 v4, p0

    move-object/from16 v5, v18

    move-object/from16 v9, v19

    move/from16 v8, v20

    move-object/from16 v6, v21

    move-object/from16 v2, v22

    move-object/from16 v1, v23

    .end local v19    # "chargeType":Ljava/lang/String;
    .end local v20    # "txAdapterValue":I
    .end local v21    # "txAdapter":Ljava/lang/String;
    .end local v22    # "txUuid":Ljava/lang/String;
    .end local v23    # "params":Landroid/os/Bundle;
    .local v1, "params":Landroid/os/Bundle;
    .local v2, "txUuid":Ljava/lang/String;
    .restart local v6    # "txAdapter":Ljava/lang/String;
    .local v8, "txAdapterValue":I
    .local v9, "chargeType":Ljava/lang/String;
    goto/16 :goto_9

    .line 980
    .end local v1    # "params":Landroid/os/Bundle;
    .end local v2    # "txUuid":Ljava/lang/String;
    .end local v6    # "txAdapter":Ljava/lang/String;
    .end local v8    # "txAdapterValue":I
    .end local v9    # "chargeType":Ljava/lang/String;
    .restart local v19    # "chargeType":Ljava/lang/String;
    .restart local v20    # "txAdapterValue":I
    .restart local v21    # "txAdapter":Ljava/lang/String;
    .restart local v22    # "txUuid":Ljava/lang/String;
    .restart local v23    # "params":Landroid/os/Bundle;
    :pswitch_0
    move-object/from16 v3, p0

    :try_start_2
    iget-object v0, v3, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/util/IMiCharge;->getBatteryThermaLevel()Ljava/lang/String;

    move-result-object v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 981
    .local v0, "value":Ljava/lang/String;
    move-object/from16 v15, v23

    .end local v23    # "params":Landroid/os/Bundle;
    .local v15, "params":Landroid/os/Bundle;
    :try_start_3
    invoke-virtual {v15, v11, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 982
    move-object v4, v3

    move-object v1, v15

    move-object/from16 v5, v18

    move-object/from16 v9, v19

    move/from16 v8, v20

    move-object/from16 v6, v21

    move-object/from16 v2, v22

    goto/16 :goto_a

    .line 986
    .end local v0    # "value":Ljava/lang/String;
    .end local v15    # "params":Landroid/os/Bundle;
    .restart local v23    # "params":Landroid/os/Bundle;
    :catch_1
    move-exception v0

    move-object v4, v3

    move-object/from16 v3, v17

    move-object/from16 v5, v18

    move-object/from16 v9, v19

    move/from16 v8, v20

    move-object/from16 v6, v21

    move-object/from16 v2, v22

    move-object/from16 v1, v23

    .end local v23    # "params":Landroid/os/Bundle;
    .restart local v15    # "params":Landroid/os/Bundle;
    goto/16 :goto_b

    .line 973
    .end local v15    # "params":Landroid/os/Bundle;
    .restart local v23    # "params":Landroid/os/Bundle;
    :pswitch_1
    move-object/from16 v3, p0

    move-object/from16 v15, v23

    .end local v23    # "params":Landroid/os/Bundle;
    .restart local v15    # "params":Landroid/os/Bundle;
    iget-object v0, v3, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/util/IMiCharge;->getBatteryResistance()Ljava/lang/String;

    move-result-object v0

    .line 974
    .restart local v0    # "value":Ljava/lang/String;
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_5

    .line 975
    :cond_4
    move-object/from16 v0, v24

    .line 977
    :cond_5
    invoke-virtual {v15, v13, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 978
    move-object v4, v3

    move-object v1, v15

    move-object/from16 v5, v18

    move-object/from16 v9, v19

    move/from16 v8, v20

    move-object/from16 v6, v21

    move-object/from16 v2, v22

    goto/16 :goto_a

    .line 969
    .end local v0    # "value":Ljava/lang/String;
    .end local v15    # "params":Landroid/os/Bundle;
    .restart local v23    # "params":Landroid/os/Bundle;
    :pswitch_2
    move-object/from16 v3, p0

    move-object/from16 v15, v23

    .end local v23    # "params":Landroid/os/Bundle;
    .restart local v15    # "params":Landroid/os/Bundle;
    iget-object v0, v3, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/util/IMiCharge;->getBatteryTbat()Ljava/lang/String;

    move-result-object v0

    .line 970
    .restart local v0    # "value":Ljava/lang/String;
    invoke-virtual {v15, v5, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 971
    move-object v4, v3

    move-object v1, v15

    move-object/from16 v5, v18

    move-object/from16 v9, v19

    move/from16 v8, v20

    move-object/from16 v6, v21

    move-object/from16 v2, v22

    goto/16 :goto_a

    .line 965
    .end local v0    # "value":Ljava/lang/String;
    .end local v15    # "params":Landroid/os/Bundle;
    .restart local v23    # "params":Landroid/os/Bundle;
    :pswitch_3
    move-object/from16 v3, p0

    move-object/from16 v15, v23

    .end local v23    # "params":Landroid/os/Bundle;
    .restart local v15    # "params":Landroid/os/Bundle;
    iget-object v0, v3, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/util/IMiCharge;->getBatteryIbat()Ljava/lang/String;

    move-result-object v0

    .line 966
    .restart local v0    # "value":Ljava/lang/String;
    invoke-virtual {v15, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 967
    move-object v4, v3

    move-object v1, v15

    move-object/from16 v5, v18

    move-object/from16 v9, v19

    move/from16 v8, v20

    move-object/from16 v6, v21

    move-object/from16 v2, v22

    goto/16 :goto_a

    .line 961
    .end local v0    # "value":Ljava/lang/String;
    .end local v15    # "params":Landroid/os/Bundle;
    .restart local v23    # "params":Landroid/os/Bundle;
    :pswitch_4
    move-object/from16 v3, p0

    move-object/from16 v15, v23

    .end local v23    # "params":Landroid/os/Bundle;
    .restart local v15    # "params":Landroid/os/Bundle;
    iget-object v0, v3, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/util/IMiCharge;->getBatteryCapacity()Ljava/lang/String;

    move-result-object v0

    .line 962
    .restart local v0    # "value":Ljava/lang/String;
    invoke-virtual {v15, v4, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 963
    move-object v4, v3

    move-object v1, v15

    move-object/from16 v5, v18

    move-object/from16 v9, v19

    move/from16 v8, v20

    move-object/from16 v6, v21

    move-object/from16 v2, v22

    goto/16 :goto_a

    .line 957
    .end local v0    # "value":Ljava/lang/String;
    .end local v15    # "params":Landroid/os/Bundle;
    .restart local v23    # "params":Landroid/os/Bundle;
    :pswitch_5
    move-object/from16 v3, p0

    move-object/from16 v15, v23

    .end local v23    # "params":Landroid/os/Bundle;
    .restart local v15    # "params":Landroid/os/Bundle;
    iget-object v0, v3, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/util/IMiCharge;->getBatteryVbat()Ljava/lang/String;

    move-result-object v0

    .line 958
    .restart local v0    # "value":Ljava/lang/String;
    invoke-virtual {v15, v10, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 959
    move-object v4, v3

    move-object v1, v15

    move-object/from16 v5, v18

    move-object/from16 v9, v19

    move/from16 v8, v20

    move-object/from16 v6, v21

    move-object/from16 v2, v22

    goto/16 :goto_a

    .line 950
    .end local v0    # "value":Ljava/lang/String;
    .end local v15    # "params":Landroid/os/Bundle;
    .restart local v23    # "params":Landroid/os/Bundle;
    :pswitch_6
    move-object/from16 v3, p0

    move-object/from16 v15, v23

    .end local v23    # "params":Landroid/os/Bundle;
    .restart local v15    # "params":Landroid/os/Bundle;
    iget-object v0, v3, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/util/IMiCharge;->getPdApdoMax()Ljava/lang/String;

    move-result-object v0

    .line 951
    .restart local v0    # "value":Ljava/lang/String;
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_7

    .line 952
    :cond_6
    move-object/from16 v0, v24

    .line 954
    :cond_7
    invoke-virtual {v15, v8, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 955
    move-object v4, v3

    move-object v1, v15

    move-object/from16 v5, v18

    move-object/from16 v9, v19

    move/from16 v8, v20

    move-object/from16 v6, v21

    move-object/from16 v2, v22

    goto/16 :goto_a

    .line 943
    .end local v0    # "value":Ljava/lang/String;
    .end local v15    # "params":Landroid/os/Bundle;
    .restart local v23    # "params":Landroid/os/Bundle;
    :pswitch_7
    move-object/from16 v3, p0

    move-object/from16 v15, v23

    .end local v23    # "params":Landroid/os/Bundle;
    .restart local v15    # "params":Landroid/os/Bundle;
    iget-object v0, v3, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/util/IMiCharge;->getPdAuthentication()Ljava/lang/String;

    move-result-object v0

    .line 944
    .restart local v0    # "value":Ljava/lang/String;
    if-eqz v0, :cond_8

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_9

    .line 945
    :cond_8
    move-object/from16 v0, v24

    .line 947
    :cond_9
    invoke-virtual {v15, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 948
    move-object v4, v3

    move-object v1, v15

    move-object/from16 v5, v18

    move-object/from16 v9, v19

    move/from16 v8, v20

    move-object/from16 v6, v21

    move-object/from16 v2, v22

    goto/16 :goto_a

    .line 986
    .end local v0    # "value":Ljava/lang/String;
    :catch_2
    move-exception v0

    move-object v4, v3

    move-object v1, v15

    move-object/from16 v3, v17

    move-object/from16 v5, v18

    move-object/from16 v9, v19

    move/from16 v8, v20

    move-object/from16 v6, v21

    move-object/from16 v2, v22

    goto/16 :goto_b

    .line 940
    .end local v15    # "params":Landroid/os/Bundle;
    .restart local v23    # "params":Landroid/os/Bundle;
    :pswitch_8
    move-object/from16 v3, p0

    move-object/from16 v15, v23

    .end local v23    # "params":Landroid/os/Bundle;
    .restart local v15    # "params":Landroid/os/Bundle;
    move-object/from16 v2, v22

    .end local v22    # "txUuid":Ljava/lang/String;
    .restart local v2    # "txUuid":Ljava/lang/String;
    :try_start_4
    invoke-virtual {v15, v9, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 941
    move-object v4, v3

    move-object v1, v15

    move-object/from16 v5, v18

    move-object/from16 v9, v19

    move/from16 v8, v20

    move-object/from16 v6, v21

    goto/16 :goto_a

    .line 986
    :catch_3
    move-exception v0

    move-object v4, v3

    move-object v1, v15

    move-object/from16 v3, v17

    move-object/from16 v5, v18

    move-object/from16 v9, v19

    move/from16 v8, v20

    move-object/from16 v6, v21

    goto/16 :goto_b

    .line 937
    .end local v2    # "txUuid":Ljava/lang/String;
    .end local v15    # "params":Landroid/os/Bundle;
    .restart local v22    # "txUuid":Ljava/lang/String;
    .restart local v23    # "params":Landroid/os/Bundle;
    :pswitch_9
    move-object/from16 v3, p0

    move-object/from16 v2, v22

    move-object/from16 v15, v23

    .end local v22    # "txUuid":Ljava/lang/String;
    .end local v23    # "params":Landroid/os/Bundle;
    .restart local v2    # "txUuid":Ljava/lang/String;
    .restart local v15    # "params":Landroid/os/Bundle;
    move-object/from16 v6, v21

    .end local v21    # "txAdapter":Ljava/lang/String;
    .restart local v6    # "txAdapter":Ljava/lang/String;
    :try_start_5
    invoke-virtual {v15, v12, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    .line 938
    move-object v4, v3

    move-object v1, v15

    move-object/from16 v5, v18

    move-object/from16 v9, v19

    move/from16 v8, v20

    goto/16 :goto_a

    .line 986
    :catch_4
    move-exception v0

    move-object v4, v3

    move-object v1, v15

    move-object/from16 v3, v17

    move-object/from16 v5, v18

    move-object/from16 v9, v19

    move/from16 v8, v20

    goto/16 :goto_b

    .line 909
    .end local v2    # "txUuid":Ljava/lang/String;
    .end local v6    # "txAdapter":Ljava/lang/String;
    .end local v15    # "params":Landroid/os/Bundle;
    .restart local v21    # "txAdapter":Ljava/lang/String;
    .restart local v22    # "txUuid":Ljava/lang/String;
    .restart local v23    # "params":Landroid/os/Bundle;
    :pswitch_a
    move-object/from16 v3, p0

    move-object/from16 v6, v21

    move-object/from16 v2, v22

    move-object/from16 v15, v23

    .end local v21    # "txAdapter":Ljava/lang/String;
    .end local v22    # "txUuid":Ljava/lang/String;
    .end local v23    # "params":Landroid/os/Bundle;
    .restart local v2    # "txUuid":Ljava/lang/String;
    .restart local v6    # "txAdapter":Ljava/lang/String;
    .restart local v15    # "params":Landroid/os/Bundle;
    const-string v0, "0"

    move-object v4, v0

    .line 910
    .local v4, "chargePower":Ljava/lang/String;
    if-lez v20, :cond_a

    const/16 v5, 0xf

    move/from16 v8, v20

    .end local v20    # "txAdapterValue":I
    .restart local v8    # "txAdapterValue":I
    if-ge v8, v5, :cond_b

    .line 911
    :try_start_6
    iget-object v0, v3, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->TX_ADAPT_POWER:[Ljava/lang/String;

    add-int/lit8 v5, v8, -0x1

    aget-object v0, v0, v5

    .line 912
    .end local v4    # "chargePower":Ljava/lang/String;
    .local v0, "chargePower":Ljava/lang/String;
    invoke-virtual {v15, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_5

    .line 913
    move-object v4, v3

    move-object v1, v15

    move-object/from16 v5, v18

    move-object/from16 v9, v19

    goto/16 :goto_a

    .line 986
    .end local v0    # "chargePower":Ljava/lang/String;
    :catch_5
    move-exception v0

    move-object v4, v3

    move-object v1, v15

    move-object/from16 v3, v17

    move-object/from16 v5, v18

    move-object/from16 v9, v19

    goto/16 :goto_b

    .line 910
    .end local v8    # "txAdapterValue":I
    .restart local v4    # "chargePower":Ljava/lang/String;
    .restart local v20    # "txAdapterValue":I
    :cond_a
    move/from16 v8, v20

    .line 916
    .end local v20    # "txAdapterValue":I
    .restart local v8    # "txAdapterValue":I
    :cond_b
    :try_start_7
    const-string v5, "Unknown"
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_6

    move-object/from16 v9, v19

    .end local v19    # "chargeType":Ljava/lang/String;
    .restart local v9    # "chargeType":Ljava/lang/String;
    :try_start_8
    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 917
    nop

    .end local v4    # "chargePower":Ljava/lang/String;
    .restart local v0    # "chargePower":Ljava/lang/String;
    goto :goto_8

    .line 918
    .end local v0    # "chargePower":Ljava/lang/String;
    .restart local v4    # "chargePower":Ljava/lang/String;
    :cond_c
    const-string v0, "USB_HVDCP_3"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 919
    iget-object v0, v3, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/util/IMiCharge;->getQuickChargeType()Ljava/lang/String;

    move-result-object v0

    .line 920
    .local v0, "value":Ljava/lang/String;
    invoke-virtual {v3, v0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 921
    .local v5, "valueInteger":I
    const/4 v10, 0x2

    if-ne v5, v10, :cond_d

    .line 922
    const-string v7, "27"

    move-object v4, v7

    goto :goto_6

    .line 923
    :cond_d
    if-ne v5, v7, :cond_e

    .line 924
    const-string v7, "18"

    move-object v4, v7

    .line 926
    .end local v5    # "valueInteger":I
    :cond_e
    :goto_6
    move-object v0, v4

    goto :goto_8

    .end local v0    # "value":Ljava/lang/String;
    :cond_f
    const-string v0, "USB_PD"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_11

    const-string v0, "PD_PPS"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    goto :goto_7

    .line 929
    :cond_10
    iget-object v0, v3, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mChargePowerHashMap:Ljava/util/HashMap;

    invoke-virtual {v0, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .end local v4    # "chargePower":Ljava/lang/String;
    .local v0, "chargePower":Ljava/lang/String;
    goto :goto_8

    .line 927
    .end local v0    # "chargePower":Ljava/lang/String;
    .restart local v4    # "chargePower":Ljava/lang/String;
    :cond_11
    :goto_7
    iget-object v0, v3, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/util/IMiCharge;->getPdApdoMax()Ljava/lang/String;

    move-result-object v0

    .line 931
    .end local v4    # "chargePower":Ljava/lang/String;
    .restart local v0    # "chargePower":Ljava/lang/String;
    :goto_8
    if-eqz v0, :cond_12

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_13

    .line 932
    :cond_12
    move-object/from16 v0, v24

    .line 934
    :cond_13
    invoke-virtual {v15, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 935
    move-object v4, v3

    move-object v1, v15

    move-object/from16 v5, v18

    goto/16 :goto_a

    .line 986
    .end local v0    # "chargePower":Ljava/lang/String;
    .end local v9    # "chargeType":Ljava/lang/String;
    .restart local v19    # "chargeType":Ljava/lang/String;
    :catch_6
    move-exception v0

    move-object/from16 v9, v19

    move-object v4, v3

    move-object v1, v15

    move-object/from16 v3, v17

    move-object/from16 v5, v18

    .end local v19    # "chargeType":Ljava/lang/String;
    .restart local v9    # "chargeType":Ljava/lang/String;
    goto/16 :goto_b

    .line 905
    .end local v2    # "txUuid":Ljava/lang/String;
    .end local v6    # "txAdapter":Ljava/lang/String;
    .end local v8    # "txAdapterValue":I
    .end local v9    # "chargeType":Ljava/lang/String;
    .end local v15    # "params":Landroid/os/Bundle;
    .restart local v19    # "chargeType":Ljava/lang/String;
    .restart local v20    # "txAdapterValue":I
    .restart local v21    # "txAdapter":Ljava/lang/String;
    .restart local v22    # "txUuid":Ljava/lang/String;
    .restart local v23    # "params":Landroid/os/Bundle;
    :pswitch_b
    move-object/from16 v3, p0

    move-object/from16 v9, v19

    move/from16 v8, v20

    move-object/from16 v6, v21

    move-object/from16 v2, v22

    move-object/from16 v15, v23

    .end local v19    # "chargeType":Ljava/lang/String;
    .end local v20    # "txAdapterValue":I
    .end local v21    # "txAdapter":Ljava/lang/String;
    .end local v22    # "txUuid":Ljava/lang/String;
    .end local v23    # "params":Landroid/os/Bundle;
    .restart local v2    # "txUuid":Ljava/lang/String;
    .restart local v6    # "txAdapter":Ljava/lang/String;
    .restart local v8    # "txAdapterValue":I
    .restart local v9    # "chargeType":Ljava/lang/String;
    .restart local v15    # "params":Landroid/os/Bundle;
    iget-object v0, v3, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/util/IMiCharge;->getUsbCurrent()Ljava/lang/String;

    move-result-object v0

    .line 906
    .local v0, "value":Ljava/lang/String;
    invoke-virtual {v15, v14, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_7

    .line 907
    move-object v4, v3

    move-object v1, v15

    move-object/from16 v5, v18

    goto/16 :goto_a

    .line 986
    .end local v0    # "value":Ljava/lang/String;
    :catch_7
    move-exception v0

    move-object v4, v3

    move-object v1, v15

    move-object/from16 v3, v17

    move-object/from16 v5, v18

    goto/16 :goto_b

    .line 901
    .end local v2    # "txUuid":Ljava/lang/String;
    .end local v6    # "txAdapter":Ljava/lang/String;
    .end local v8    # "txAdapterValue":I
    .end local v9    # "chargeType":Ljava/lang/String;
    .end local v15    # "params":Landroid/os/Bundle;
    .restart local v19    # "chargeType":Ljava/lang/String;
    .restart local v20    # "txAdapterValue":I
    .restart local v21    # "txAdapter":Ljava/lang/String;
    .restart local v22    # "txUuid":Ljava/lang/String;
    .restart local v23    # "params":Landroid/os/Bundle;
    :pswitch_c
    move-object/from16 v3, p0

    move-object/from16 v9, v19

    move/from16 v8, v20

    move-object/from16 v6, v21

    move-object/from16 v2, v22

    move-object/from16 v1, v23

    .end local v19    # "chargeType":Ljava/lang/String;
    .end local v20    # "txAdapterValue":I
    .end local v21    # "txAdapter":Ljava/lang/String;
    .end local v22    # "txUuid":Ljava/lang/String;
    .end local v23    # "params":Landroid/os/Bundle;
    .restart local v1    # "params":Landroid/os/Bundle;
    .restart local v2    # "txUuid":Ljava/lang/String;
    .restart local v6    # "txAdapter":Ljava/lang/String;
    .restart local v8    # "txAdapterValue":I
    .restart local v9    # "chargeType":Ljava/lang/String;
    :try_start_9
    iget-object v0, v3, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/util/IMiCharge;->getUsbVoltage()Ljava/lang/String;

    move-result-object v0

    .line 902
    .restart local v0    # "value":Ljava/lang/String;
    invoke-virtual {v1, v15, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_8

    .line 903
    move-object v4, v3

    move-object/from16 v5, v18

    goto :goto_a

    .line 986
    .end local v0    # "value":Ljava/lang/String;
    :catch_8
    move-exception v0

    move-object v4, v3

    move-object/from16 v3, v17

    move-object/from16 v5, v18

    goto :goto_b

    .line 892
    .end local v1    # "params":Landroid/os/Bundle;
    .end local v2    # "txUuid":Ljava/lang/String;
    .end local v6    # "txAdapter":Ljava/lang/String;
    .end local v8    # "txAdapterValue":I
    .end local v9    # "chargeType":Ljava/lang/String;
    .restart local v19    # "chargeType":Ljava/lang/String;
    .restart local v20    # "txAdapterValue":I
    .restart local v21    # "txAdapter":Ljava/lang/String;
    .restart local v22    # "txUuid":Ljava/lang/String;
    .restart local v23    # "params":Landroid/os/Bundle;
    :pswitch_d
    move-object/from16 v4, p0

    move-object/from16 v9, v19

    move/from16 v8, v20

    move-object/from16 v6, v21

    move-object/from16 v2, v22

    move-object/from16 v1, v23

    .end local v19    # "chargeType":Ljava/lang/String;
    .end local v20    # "txAdapterValue":I
    .end local v21    # "txAdapter":Ljava/lang/String;
    .end local v22    # "txUuid":Ljava/lang/String;
    .end local v23    # "params":Landroid/os/Bundle;
    .restart local v1    # "params":Landroid/os/Bundle;
    .restart local v2    # "txUuid":Ljava/lang/String;
    .restart local v6    # "txAdapter":Ljava/lang/String;
    .restart local v8    # "txAdapterValue":I
    .restart local v9    # "chargeType":Ljava/lang/String;
    if-lez v8, :cond_14

    .line 893
    move-object/from16 v5, v18

    :try_start_a
    iput-object v5, v4, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mChargeType:Ljava/lang/String;

    .line 894
    invoke-virtual {v1, v3, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_a

    .line 896
    :cond_14
    move-object/from16 v5, v18

    iput-object v9, v4, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mChargeType:Ljava/lang/String;

    .line 897
    invoke-virtual {v1, v3, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 899
    goto :goto_a

    .line 986
    :catch_9
    move-exception v0

    move-object/from16 v3, v17

    goto :goto_b

    .line 984
    :goto_9
    const-string v0, "no chagre params to handle"
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_9

    move-object/from16 v3, v17

    :try_start_b
    invoke-static {v3, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_a

    .line 988
    :goto_a
    goto :goto_c

    .line 986
    :catch_a
    move-exception v0

    goto :goto_b

    .end local v1    # "params":Landroid/os/Bundle;
    .end local v2    # "txUuid":Ljava/lang/String;
    .end local v9    # "chargeType":Ljava/lang/String;
    .end local v16    # "i":I
    .restart local v3    # "params":Landroid/os/Bundle;
    .local v4, "txAdapterValue":I
    .local v5, "chargeType":Ljava/lang/String;
    .restart local v7    # "txUuid":Ljava/lang/String;
    .local v8, "i":I
    :catch_b
    move-exception v0

    move-object v9, v5

    move/from16 v16, v8

    move-object v5, v2

    move v8, v4

    move-object v2, v7

    move-object v4, v1

    move-object v1, v3

    move-object v3, v10

    .line 987
    .end local v3    # "params":Landroid/os/Bundle;
    .end local v4    # "txAdapterValue":I
    .end local v5    # "chargeType":Ljava/lang/String;
    .end local v7    # "txUuid":Ljava/lang/String;
    .local v0, "e":Ljava/lang/Exception;
    .restart local v1    # "params":Landroid/os/Bundle;
    .restart local v2    # "txUuid":Ljava/lang/String;
    .local v8, "txAdapterValue":I
    .restart local v9    # "chargeType":Ljava/lang/String;
    .restart local v16    # "i":I
    :goto_b
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "read charge file error "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 888
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_c
    add-int/lit8 v0, v16, 0x1

    move-object v3, v1

    move-object v7, v2

    move-object v1, v4

    move-object v2, v5

    move v4, v8

    move-object v5, v9

    move v8, v0

    .end local v16    # "i":I
    .local v0, "i":I
    goto/16 :goto_2

    .end local v0    # "i":I
    .end local v1    # "params":Landroid/os/Bundle;
    .end local v2    # "txUuid":Ljava/lang/String;
    .end local v9    # "chargeType":Ljava/lang/String;
    .restart local v3    # "params":Landroid/os/Bundle;
    .restart local v4    # "txAdapterValue":I
    .restart local v5    # "chargeType":Ljava/lang/String;
    .restart local v7    # "txUuid":Ljava/lang/String;
    .local v8, "i":I
    :cond_15
    move-object v9, v5

    move-object v2, v7

    move/from16 v16, v8

    move v8, v4

    move-object v4, v1

    move-object v1, v3

    move-object v3, v10

    .line 990
    .end local v3    # "params":Landroid/os/Bundle;
    .end local v4    # "txAdapterValue":I
    .end local v5    # "chargeType":Ljava/lang/String;
    .end local v7    # "txUuid":Ljava/lang/String;
    .restart local v1    # "params":Landroid/os/Bundle;
    .restart local v2    # "txUuid":Ljava/lang/String;
    .local v8, "txAdapterValue":I
    .restart local v9    # "chargeType":Ljava/lang/String;
    iget-object v0, v4, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetDEBUG(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 991
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Charge params =  "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 993
    :cond_16
    const-string v0, "31000000094"

    const-string v3, "charge"

    invoke-direct {v4, v1, v0, v3}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendOneTrackInfo(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    .line 994
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x3575ef6a -> :sswitch_d
        -0x269fe3dd -> :sswitch_c
        -0x40aeb46 -> :sswitch_b
        0x27ab41 -> :sswitch_a
        0x31370c -> :sswitch_9
        0x371fdf -> :sswitch_8
        0x277c8abf -> :sswitch_7
        0x314d48dc -> :sswitch_6
        0x3df703a3 -> :sswitch_5
        0x5b7165be -> :sswitch_4
        0x5fd237ba -> :sswitch_3
        0x6f175839 -> :sswitch_2
        0x79579494 -> :sswitch_1
        0x7f510a9b -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private handleChargeAction()V
    .locals 8

    .line 1020
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1021
    .local v0, "params":Landroid/os/Bundle;
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mChargeType:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1022
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmChargeStartTime(Lcom/android/server/MiuiBatteryStatsService;)J

    move-result-wide v1

    invoke-direct {p0, v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->formatTime(J)Ljava/lang/String;

    move-result-object v1

    const-string v2, "charge_start_time"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1023
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmChargeEndTime(Lcom/android/server/MiuiBatteryStatsService;)J

    move-result-wide v1

    invoke-direct {p0, v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->formatTime(J)Ljava/lang/String;

    move-result-object v1

    const-string v2, "charge_end_time"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1024
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmFullChargeStartTime(Lcom/android/server/MiuiBatteryStatsService;)J

    move-result-wide v1

    invoke-direct {p0, v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->formatTime(J)Ljava/lang/String;

    move-result-object v1

    const-string v2, "full_charge_start_time"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1025
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmFullChargeEndTime(Lcom/android/server/MiuiBatteryStatsService;)J

    move-result-wide v1

    invoke-direct {p0, v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->formatTime(J)Ljava/lang/String;

    move-result-object v1

    const-string v2, "full_charge_end_time"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1026
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmChargeStartCapacity(Lcom/android/server/MiuiBatteryStatsService;)I

    move-result v1

    const-string v2, "charge_start_capacity"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1027
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmChargeEndCapacity(Lcom/android/server/MiuiBatteryStatsService;)I

    move-result v1

    const-string v2, "charge_end_capacity"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1028
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmChargeEndTime(Lcom/android/server/MiuiBatteryStatsService;)J

    move-result-wide v1

    iget-object v3, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v3}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmChargeStartTime(Lcom/android/server/MiuiBatteryStatsService;)J

    move-result-wide v3

    sub-long/2addr v1, v3

    .line 1029
    invoke-direct {p0, v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->getTimeHours(J)Ljava/lang/String;

    move-result-object v1

    .line 1028
    const-string v2, "charge_total_time"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1030
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmFullChargeEndTime(Lcom/android/server/MiuiBatteryStatsService;)J

    move-result-wide v1

    iget-object v3, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v3}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmFullChargeStartTime(Lcom/android/server/MiuiBatteryStatsService;)J

    move-result-wide v3

    sub-long/2addr v1, v3

    .line 1031
    invoke-direct {p0, v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->getTimeHours(J)Ljava/lang/String;

    move-result-object v1

    .line 1030
    const-string v2, "full_charge_total_time"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1032
    const-string v1, "charger_type"

    iget-object v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mChargeType:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1033
    const-string/jumbo v1, "tx_adapter"

    iget-object v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mTxAdapter:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1034
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmAppUsageStats(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiAppUsageStats;

    move-result-object v2

    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmContext(Lcom/android/server/MiuiBatteryStatsService;)Landroid/content/Context;

    move-result-object v3

    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmChargeStartTime(Lcom/android/server/MiuiBatteryStatsService;)J

    move-result-wide v4

    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmChargeEndTime(Lcom/android/server/MiuiBatteryStatsService;)J

    move-result-wide v6

    .line 1035
    invoke-virtual/range {v2 .. v7}, Lcom/android/server/MiuiAppUsageStats;->getTop3Apps(Landroid/content/Context;JJ)Ljava/util/ArrayList;

    move-result-object v1

    .line 1034
    const-string/jumbo v2, "top3_app"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1036
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmChargeMaxTemp(Lcom/android/server/MiuiBatteryStatsService;)I

    move-result v1

    const-string v2, "charge_battery_max_temp"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1037
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmChargeMinTemp(Lcom/android/server/MiuiBatteryStatsService;)I

    move-result v1

    const-string v2, "charge_battery_min_temp"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1038
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmScreenOnTime(Lcom/android/server/MiuiBatteryStatsService;)J

    move-result-wide v1

    invoke-direct {p0, v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->getTimeHours(J)Ljava/lang/String;

    move-result-object v1

    const-string v2, "screen_on_time"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1039
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmChargeEndTime(Lcom/android/server/MiuiBatteryStatsService;)J

    move-result-wide v1

    iget-object v3, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v3}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmChargeStartTime(Lcom/android/server/MiuiBatteryStatsService;)J

    move-result-wide v3

    sub-long/2addr v1, v3

    iget-object v3, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v3}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmScreenOnTime(Lcom/android/server/MiuiBatteryStatsService;)J

    move-result-wide v3

    sub-long/2addr v1, v3

    .line 1040
    invoke-direct {p0, v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->getTimeHours(J)Ljava/lang/String;

    move-result-object v1

    .line 1039
    const-string v2, "screen_off_time"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1042
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetDEBUG(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1043
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Charge Action params =  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiBatteryStatsService"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1045
    :cond_0
    const-string v1, "31000000094"

    const-string v2, "charge_action"

    invoke-direct {p0, v0, v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendOneTrackInfo(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    .line 1046
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mChargeType:Ljava/lang/String;

    .line 1047
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    const-wide/16 v2, 0x0

    invoke-static {v1, v2, v3}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fputmFullChargeStartTime(Lcom/android/server/MiuiBatteryStatsService;J)V

    .line 1048
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1, v2, v3}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fputmFullChargeEndTime(Lcom/android/server/MiuiBatteryStatsService;J)V

    .line 1049
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    const/4 v4, 0x0

    invoke-static {v1, v4}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fputmChargeMaxTemp(Lcom/android/server/MiuiBatteryStatsService;I)V

    .line 1050
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1, v4}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fputmChargeMinTemp(Lcom/android/server/MiuiBatteryStatsService;I)V

    .line 1051
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1, v2, v3}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fputmScreenOnTime(Lcom/android/server/MiuiBatteryStatsService;J)V

    .line 1053
    :cond_1
    return-void
.end method

.method private handleCheckSoc()V
    .locals 6

    .line 1385
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1386
    .local v0, "params":Landroid/os/Bundle;
    const/4 v1, 0x0

    .line 1387
    .local v1, "socValue":I
    iget-object v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v2

    invoke-virtual {v2}, Lmiui/util/IMiCharge;->getBatteryCapacity()Ljava/lang/String;

    move-result-object v2

    .line 1388
    .local v2, "soc":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1389
    invoke-virtual {p0, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1391
    :cond_0
    const/16 v3, 0x64

    const-string v4, "not_fully_charged"

    if-eq v1, v3, :cond_1

    .line 1392
    const-string v3, "notFullyCharged"

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1394
    :cond_1
    iget-object v3, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v3}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetDEBUG(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1395
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "NOT_FULLY_CHARGED_PARAMS params = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v5, "MiuiBatteryStatsService"

    invoke-static {v5, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1397
    :cond_2
    const-string v3, "31000000094"

    invoke-direct {p0, v0, v3, v4}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendOneTrackInfo(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    .line 1398
    return-void
.end method

.method private handleIntermittentCharge()V
    .locals 5

    .line 1281
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1282
    .local v0, "params":Landroid/os/Bundle;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mDischargingCount = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmDischargingCount(Lcom/android/server/MiuiBatteryStatsService;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiBatteryStatsService"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1283
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmDischargingCount(Lcom/android/server/MiuiBatteryStatsService;)I

    move-result v1

    const/4 v3, 0x3

    if-lt v1, v3, :cond_1

    .line 1284
    const-string v1, "intermittentCharge"

    const-string v3, "intermittent_charge"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1285
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetDEBUG(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1286
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Intermittent Charge params = "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1288
    :cond_0
    const-string v1, "31000000094"

    invoke-direct {p0, v0, v1, v3}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendOneTrackInfo(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    .line 1290
    :cond_1
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fputmDischargingCount(Lcom/android/server/MiuiBatteryStatsService;I)V

    .line 1291
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1, v2}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fputmIsHandleIntermittentCharge(Lcom/android/server/MiuiBatteryStatsService;Z)V

    .line 1292
    return-void
.end method

.method private handleLPDInfomation(Ljava/lang/String;)V
    .locals 13
    .param p1, "lpdInfomation"    # Ljava/lang/String;

    .line 1333
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1334
    .local v0, "params":Landroid/os/Bundle;
    const/4 v1, 0x0

    .line 1335
    .local v1, "dp":I
    const/4 v2, 0x0

    .line 1336
    .local v2, "dm":I
    const/4 v3, 0x0

    .line 1337
    .local v3, "sbu1":I
    const/4 v4, 0x0

    .line 1338
    .local v4, "sbu2":I
    const/4 v5, 0x0

    .line 1339
    .local v5, "lines":[Ljava/lang/String;
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1340
    const-string v6, "\n"

    invoke-virtual {p1, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 1342
    :cond_0
    const-string v6, "MiuiBatteryStatsService"

    if-eqz v5, :cond_9

    array-length v7, v5

    if-nez v7, :cond_1

    goto/16 :goto_2

    .line 1346
    :cond_1
    invoke-static {v5}, Lcom/android/internal/util/ArrayUtils;->size([Ljava/lang/Object;)I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    .local v7, "i":I
    :goto_0
    const-string v8, "LPD_SBU2_RES="

    const-string v9, "LPD_SBU1_RES="

    const-string v10, "LPD_DM_RES="

    const-string v11, "LPD_DP_RES="

    if-ltz v7, :cond_7

    .line 1347
    aget-object v12, v5, v7

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 1348
    goto :goto_1

    .line 1350
    :cond_2
    aget-object v12, v5, v7

    invoke-virtual {v12, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 1351
    aget-object v8, v5, v7

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1352
    const-string v8, "lpd_dp_res"

    invoke-virtual {v0, v8, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1353
    goto :goto_1

    .line 1355
    :cond_3
    aget-object v11, v5, v7

    invoke-virtual {v11, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 1356
    aget-object v8, v5, v7

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 1357
    const-string v8, "lpd_dm_res"

    invoke-virtual {v0, v8, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1358
    goto :goto_1

    .line 1360
    :cond_4
    aget-object v10, v5, v7

    invoke-virtual {v10, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 1361
    aget-object v8, v5, v7

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 1362
    const-string v8, "lpd_sbu1_res"

    invoke-virtual {v0, v8, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1363
    goto :goto_1

    .line 1365
    :cond_5
    aget-object v9, v5, v7

    invoke-virtual {v9, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 1366
    aget-object v9, v5, v7

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v9, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 1367
    const-string v8, "lpd_sbu2_res"

    invoke-virtual {v0, v8, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1346
    :cond_6
    :goto_1
    add-int/lit8 v7, v7, -0x1

    goto/16 :goto_0

    .line 1371
    .end local v7    # "i":I
    :cond_7
    iget-object v7, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v7}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetDEBUG(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 1372
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1374
    :cond_8
    const-string v6, "31000000094"

    const-string v7, "battery_lpd_infomation"

    invoke-direct {p0, v0, v6, v7}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendOneTrackInfo(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    .line 1375
    return-void

    .line 1343
    :cond_9
    :goto_2
    const-string v7, "LpdInfomation has no data"

    invoke-static {v6, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1344
    return-void
.end method

.method private handleLpdCountReport()V
    .locals 3

    .line 1378
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1379
    .local v0, "params":Landroid/os/Bundle;
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmLpdCount(Lcom/android/server/MiuiBatteryStatsService;)I

    move-result v1

    const-string v2, "lpd_count"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1380
    const-string v1, "31000000094"

    invoke-direct {p0, v0, v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendOneTrackInfo(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    .line 1381
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fputmLpdCount(Lcom/android/server/MiuiBatteryStatsService;I)V

    .line 1382
    return-void
.end method

.method private handleNonlinearChangeOfCapacity(I)V
    .locals 5
    .param p1, "currentSoc"    # I

    .line 1295
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1296
    .local v0, "params":Landroid/os/Bundle;
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmLastSoc(Lcom/android/server/MiuiBatteryStatsService;)I

    move-result v1

    sub-int v1, p1, v1

    .line 1297
    .local v1, "capacityChangeValue":I
    iget-object v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetDEBUG(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v2

    const-string v3, "MiuiBatteryStatsService"

    if-eqz v2, :cond_0

    .line 1298
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "currentSoc = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " mLastSoc = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v4}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmLastSoc(Lcom/android/server/MiuiBatteryStatsService;)I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " capacityChangeValue = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1299
    :cond_0
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v2

    const/4 v4, 0x1

    if-le v2, v4, :cond_2

    .line 1300
    const-string v2, "capacity_change_value"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1301
    iget-object v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetDEBUG(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1302
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Nonlinear Change Of Capacity params = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1304
    :cond_1
    const-string v2, "31000000094"

    const-string v3, "nonlinear_change_of_capacity"

    invoke-direct {p0, v0, v2, v3}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendOneTrackInfo(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    .line 1306
    :cond_2
    return-void
.end method

.method private handlePowerOff(Z)V
    .locals 6
    .param p1, "shutdown_start"    # Z

    .line 1056
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1057
    .local v0, "params":Landroid/os/Bundle;
    const-string/jumbo v1, "shutdown_delay"

    if-nez p1, :cond_0

    .line 1058
    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1059
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v1

    invoke-virtual {v1}, Lmiui/util/IMiCharge;->getBatteryVbat()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v3, "vbat"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1060
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v1

    invoke-virtual {v1}, Lmiui/util/IMiCharge;->getBatteryTbat()Ljava/lang/String;

    move-result-object v1

    const-string v3, "Tbat"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1061
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v1

    invoke-virtual {v1}, Lmiui/util/IMiCharge;->getBatteryIbat()Ljava/lang/String;

    move-result-object v1

    const-string v3, "ibat"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1062
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    move-result-object v1

    const/4 v3, 0x5

    const-wide/32 v4, 0x88b8

    invoke-virtual {v1, v3, v2, v4, v5}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(IZJ)V

    goto :goto_0

    .line 1064
    :cond_0
    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1066
    :goto_0
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetDEBUG(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1067
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Power Off Action params =  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiBatteryStatsService"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1069
    :cond_1
    const-string v1, "31000000094"

    const-string v2, "power_off"

    invoke-direct {p0, v0, v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendOneTrackInfo(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    .line 1070
    return-void
.end method

.method private handleSlowCharge()V
    .locals 15

    .line 1233
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1234
    .local v0, "params":Landroid/os/Bundle;
    const/4 v1, 0x0

    .line 1235
    .local v1, "battTempValue":I
    const/4 v2, 0x0

    .line 1236
    .local v2, "thermalLevelValue":I
    const/4 v3, 0x0

    .line 1237
    .local v3, "quickChargeTypeValue":I
    const/4 v4, 0x0

    .line 1238
    .local v4, "chargeCurrentSocValue":I
    iget-object v5, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v5

    invoke-virtual {v5}, Lmiui/util/IMiCharge;->getBatteryCapacity()Ljava/lang/String;

    move-result-object v5

    .line 1239
    .local v5, "chargeCurrentSoc":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1240
    invoke-virtual {p0, v5}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 1242
    :cond_0
    iget-object v6, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v6}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetDEBUG(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v6

    const-string v7, "MiuiBatteryStatsService"

    if-eqz v6, :cond_1

    .line 1243
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "chargeCurrentSoc = "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", chargeStartSoc = "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v8, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v8}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmChargeStartCapacity(Lcom/android/server/MiuiBatteryStatsService;)I

    move-result v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v7, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1245
    :cond_1
    const-string v6, "USB_FLOAT"

    invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->getBatteryChargeType()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    const-string/jumbo v8, "slow_charge_type"

    if-eqz v6, :cond_2

    .line 1246
    iget-object v6, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->SLOW_CHARGE_TYPE:[Ljava/lang/String;

    const/4 v9, 0x0

    aget-object v6, v6, v9

    invoke-virtual {v0, v8, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1247
    :cond_2
    iget-object v6, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v6}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmChargeStartCapacity(Lcom/android/server/MiuiBatteryStatsService;)I

    move-result v6

    sub-int v6, v4, v6

    const/4 v9, 0x3

    if-ge v6, v9, :cond_9

    .line 1248
    iget-object v6, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v6}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v6

    invoke-virtual {v6}, Lmiui/util/IMiCharge;->getPdAuthentication()Ljava/lang/String;

    move-result-object v6

    .line 1249
    .local v6, "pdVerified":Ljava/lang/String;
    iget-object v10, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v10}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v10

    invoke-virtual {v10}, Lmiui/util/IMiCharge;->getQuickChargeType()Ljava/lang/String;

    move-result-object v10

    .line 1250
    .local v10, "quickChargeType":Ljava/lang/String;
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_3

    .line 1251
    invoke-virtual {p0, v10}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 1253
    :cond_3
    iget-object v11, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v11}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v11

    invoke-virtual {v11}, Lmiui/util/IMiCharge;->getBatteryThermaLevel()Ljava/lang/String;

    move-result-object v11

    .line 1254
    .local v11, "thermalLevel":Ljava/lang/String;
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_4

    .line 1255
    invoke-virtual {p0, v11}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 1257
    :cond_4
    iget-object v12, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v12}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v12

    invoke-virtual {v12}, Lmiui/util/IMiCharge;->getBatteryTbat()Ljava/lang/String;

    move-result-object v12

    .line 1258
    .local v12, "battTemp":Ljava/lang/String;
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_5

    .line 1259
    invoke-virtual {p0, v12}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1261
    :cond_5
    iget-object v13, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v13}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetDEBUG(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 1262
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "pdVerified = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", quickChargeTypeValue = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", thermalLevelValue = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", battTempValue = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v7, v13}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1265
    :cond_6
    const-string v13, "1"

    invoke-virtual {v13, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    const/16 v14, 0x1d6

    if-eqz v13, :cond_8

    if-gt v1, v14, :cond_7

    const/16 v13, 0x9

    if-le v2, v13, :cond_8

    .line 1266
    :cond_7
    iget-object v9, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->SLOW_CHARGE_TYPE:[Ljava/lang/String;

    const/4 v13, 0x1

    aget-object v9, v9, v13

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1267
    :cond_8
    if-lt v3, v9, :cond_9

    const/16 v9, 0x50

    if-ge v4, v9, :cond_9

    const/16 v9, 0x8

    if-ge v2, v9, :cond_9

    if-ge v1, v14, :cond_9

    .line 1268
    iget-object v9, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->SLOW_CHARGE_TYPE:[Ljava/lang/String;

    const/4 v13, 0x2

    aget-object v9, v9, v13

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1272
    .end local v6    # "pdVerified":Ljava/lang/String;
    .end local v10    # "quickChargeType":Ljava/lang/String;
    .end local v11    # "thermalLevel":Ljava/lang/String;
    .end local v12    # "battTemp":Ljava/lang/String;
    :cond_9
    :goto_0
    invoke-virtual {v0}, Landroid/os/Bundle;->size()I

    move-result v6

    if-eqz v6, :cond_b

    .line 1273
    iget-object v6, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v6}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetDEBUG(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 1274
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Slow Charge params = "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v7, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1276
    :cond_a
    const-string v6, "31000000094"

    const-string/jumbo v7, "slow_charge"

    invoke-direct {p0, v0, v6, v7}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendOneTrackInfo(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    .line 1278
    :cond_b
    return-void
.end method

.method private handleSreiesDeltaVoltage(Landroid/content/Intent;)V
    .locals 10
    .param p1, "intent"    # Landroid/content/Intent;

    .line 1082
    const-string/jumbo v0, "temperature"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 1083
    .local v0, "temp":I
    const-string v2, "level"

    const/4 v3, -0x1

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 1084
    .local v2, "soc":I
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1085
    .local v3, "params":Landroid/os/Bundle;
    const/4 v4, 0x0

    .line 1086
    .local v4, "currentV":I
    const/16 v5, 0xc8

    const-string v6, "MiuiBatteryStatsService"

    if-lt v0, v5, :cond_3

    const/16 v5, 0x15e

    if-gt v0, v5, :cond_3

    const/16 v5, 0x23

    if-lt v2, v5, :cond_3

    const/16 v5, 0x50

    if-gt v2, v5, :cond_3

    .line 1087
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v1

    invoke-virtual {v1}, Lmiui/util/IMiCharge;->getBatteryIbat()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1088
    .local v1, "ibat":I
    iget-object v5, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v5

    const-string v7, "cell1_volt"

    invoke-virtual {v5, v7}, Lmiui/util/IMiCharge;->getMiChargePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 1089
    .local v5, "vCell1":I
    iget-object v7, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v7}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v7

    const-string v8, "cell2_volt"

    invoke-virtual {v7, v8}, Lmiui/util/IMiCharge;->getMiChargePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 1090
    .local v7, "vCell2":I
    const v8, 0x186a0

    if-gt v1, v8, :cond_2

    const/16 v8, 0xf0a

    if-lt v5, v8, :cond_2

    const/16 v9, 0x1004

    if-gt v5, v9, :cond_2

    if-lt v7, v8, :cond_2

    if-gt v7, v9, :cond_2

    .line 1091
    sub-int v8, v5, v7

    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    move-result v4

    .line 1092
    iget v8, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mDeltaV:I

    if-le v4, v8, :cond_0

    .line 1093
    iput v4, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mDeltaV:I

    .line 1094
    :cond_0
    iget-object v8, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v8}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetDEBUG(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1095
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "cell1_volt =  "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", cell2_volt = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", mDeltaV = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mDeltaV:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1097
    :cond_1
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mIsDeltaVAssigned:Z

    .line 1099
    .end local v1    # "ibat":I
    .end local v5    # "vCell1":I
    .end local v7    # "vCell2":I
    :cond_2
    goto :goto_0

    :cond_3
    iget-boolean v5, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mIsDeltaVAssigned:Z

    if-eqz v5, :cond_2

    .line 1100
    iget v5, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mDeltaV:I

    const-string/jumbo v7, "series_delta_voltage"

    invoke-virtual {v3, v7, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1101
    iget-object v5, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetDEBUG(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1102
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "series delta voltage params =  "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1104
    :cond_4
    const-string v5, "31000000094"

    invoke-direct {p0, v3, v5, v7}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendOneTrackInfo(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    .line 1105
    iput v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mDeltaV:I

    .line 1106
    iput-boolean v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mIsDeltaVAssigned:Z

    .line 1108
    :goto_0
    return-void
.end method

.method private handleUsbFunction(Landroid/content/Intent;)V
    .locals 8
    .param p1, "intent"    # Landroid/content/Intent;

    .line 1111
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1112
    .local v0, "params":Landroid/os/Bundle;
    const-string/jumbo v1, "unlocked"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 1113
    .local v1, "dataUnlock":Z
    const-string v2, "data_unlock"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1115
    iget-object v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v2

    invoke-virtual {v2}, Lmiui/util/IMiCharge;->isUSB32()Z

    move-result v2

    const-string v3, "USB32"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1117
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v2

    .line 1118
    .local v2, "keySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v3, ""

    .line 1119
    .local v3, "usb_function":Ljava/lang/String;
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 1120
    .local v5, "key":Ljava/lang/String;
    iget-object v6, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mNotUsbFunction:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    goto :goto_0

    .line 1121
    :cond_0
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1122
    :cond_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1123
    .end local v5    # "key":Ljava/lang/String;
    goto :goto_0

    .line 1124
    :cond_2
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "none"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1125
    :cond_3
    const-string/jumbo v4, "usb_function"

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1126
    iget-object v4, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v4}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetDEBUG(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1127
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Usb function params =  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "MiuiBatteryStatsService"

    invoke-static {v5, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1129
    :cond_4
    const-string v4, "31000000092"

    const-string/jumbo v5, "usb_deivce"

    invoke-direct {p0, v0, v4, v5}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendOneTrackInfo(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    .line 1130
    return-void
.end method

.method private handleVbusDisable(I)V
    .locals 3
    .param p1, "vbusDisable"    # I

    .line 1073
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1074
    .local v0, "params":Landroid/os/Bundle;
    const-string/jumbo v1, "vbus_disable"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1075
    const-string v2, "31000000094"

    invoke-direct {p0, v0, v2, v1}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendOneTrackInfo(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    .line 1076
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetDEBUG(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1077
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "VBUS params = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiBatteryStatsService"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1079
    :cond_0
    return-void
.end method

.method private handleWirelessComposite()V
    .locals 5

    .line 1309
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1310
    .local v0, "params":Landroid/os/Bundle;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mPlugType = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmPlugType(Lcom/android/server/MiuiBatteryStatsService;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mLastOpenStatus = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mLastOpenStatus:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mOtgConnected = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmOtgConnected(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiBatteryStatsService"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1312
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmPlugType(Lcom/android/server/MiuiBatteryStatsService;)I

    move-result v1

    const-string/jumbo v3, "wireless_composite"

    const/4 v4, 0x4

    if-ne v1, v4, :cond_0

    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmOtgConnected(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1313
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->WIRELESS_COMPOSITE_TYPE:[Ljava/lang/String;

    const/4 v4, 0x0

    aget-object v1, v1, v4

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1316
    :cond_0
    iget v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mLastOpenStatus:I

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmOtgConnected(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1317
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->WIRELESS_COMPOSITE_TYPE:[Ljava/lang/String;

    const/4 v4, 0x1

    aget-object v1, v1, v4

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1320
    :cond_1
    iget v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mLastOpenStatus:I

    if-lez v1, :cond_2

    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmPlugType(Lcom/android/server/MiuiBatteryStatsService;)I

    move-result v1

    if-lez v1, :cond_2

    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmPlugType(Lcom/android/server/MiuiBatteryStatsService;)I

    move-result v1

    if-eq v1, v4, :cond_2

    .line 1321
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->WIRELESS_COMPOSITE_TYPE:[Ljava/lang/String;

    const/4 v4, 0x2

    aget-object v1, v1, v4

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1324
    :cond_2
    :goto_0
    invoke-virtual {v0}, Landroid/os/Bundle;->size()I

    move-result v1

    if-lez v1, :cond_4

    .line 1325
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetDEBUG(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1326
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "wireless composite = "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1328
    :cond_3
    const-string v1, "31000000094"

    invoke-direct {p0, v0, v1, v3}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendOneTrackInfo(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    .line 1330
    :cond_4
    return-void
.end method

.method private handleWirelessReverseCharge()V
    .locals 6

    .line 997
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 999
    .local v0, "params":Landroid/os/Bundle;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->WIRELESS_REVERSE_CHARGE_PARAMS:[Ljava/lang/String;

    array-length v3, v2

    const-string v4, "MiuiBatteryStatsService"

    if-ge v1, v3, :cond_1

    .line 1001
    :try_start_0
    aget-object v2, v2, v1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string/jumbo v5, "wireless_reverse_enable"

    packed-switch v3, :pswitch_data_0

    :cond_0
    goto :goto_1

    :pswitch_0
    :try_start_1
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    goto :goto_2

    :goto_1
    const/4 v2, -0x1

    :goto_2
    packed-switch v2, :pswitch_data_1

    .line 1007
    goto :goto_3

    .line 1003
    :pswitch_1
    iget-object v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v2

    invoke-virtual {v2}, Lmiui/util/IMiCharge;->getWirelessReverseStatus()Ljava/lang/String;

    move-result-object v2

    .line 1004
    .local v2, "value":Ljava/lang/String;
    invoke-virtual {v0, v5, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1005
    goto :goto_4

    .line 1007
    .end local v2    # "value":Ljava/lang/String;
    :goto_3
    const-string v2, "no wireless reverse charge params to handle"

    invoke-static {v4, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 1011
    :goto_4
    goto :goto_5

    .line 1009
    :catch_0
    move-exception v2

    .line 1010
    .local v2, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "read wireless reverse charge file error "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 999
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1013
    .end local v1    # "i":I
    :cond_1
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetDEBUG(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1014
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Wireless Reverse Charge params =  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1016
    :cond_2
    const-string v1, "31000000094"

    const-string/jumbo v2, "wireless_reverse_charge"

    invoke-direct {p0, v0, v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendOneTrackInfo(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    .line 1017
    return-void

    :pswitch_data_0
    .packed-switch -0x708fdbbf
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method

.method private initBatteryTemp()V
    .locals 4

    .line 517
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->TEMP_COLLECTED_SCENE:[Ljava/lang/String;

    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 518
    new-instance v2, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;

    iget-object v3, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    aget-object v1, v1, v0

    invoke-direct {v2, v3, v1}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;-><init>(Lcom/android/server/MiuiBatteryStatsService;Ljava/lang/String;)V

    move-object v1, v2

    .line 519
    .local v1, "batteryTempInfo":Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;
    iget-object v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mBatteryTemps:Landroid/util/ArrayMap;

    iget-object v3, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->TEMP_COLLECTED_SCENE:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v2, v3, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 517
    .end local v1    # "batteryTempInfo":Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 521
    .end local v0    # "i":I
    :cond_0
    return-void
.end method

.method private initWiredChargePower()V
    .locals 3

    .line 507
    iget-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mChargePowerHashMap:Ljava/util/HashMap;

    const-string v1, "USB"

    const-string v2, "2.5"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 508
    iget-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mChargePowerHashMap:Ljava/util/HashMap;

    const-string v1, "OCP"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 509
    iget-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mChargePowerHashMap:Ljava/util/HashMap;

    const-string v1, "USB_DCP"

    const-string v2, "7.5"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 510
    iget-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mChargePowerHashMap:Ljava/util/HashMap;

    const-string v1, "USB_CDP"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 511
    iget-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mChargePowerHashMap:Ljava/util/HashMap;

    const-string v1, "USB_FLOAT"

    const-string v2, "5"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 512
    iget-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mChargePowerHashMap:Ljava/util/HashMap;

    const-string v1, "USB_HVDCP"

    const-string v2, "15"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 513
    iget-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mChargePowerHashMap:Ljava/util/HashMap;

    const-string v1, "USB_HVDCP_3P5"

    const-string v2, "22.5"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 514
    return-void
.end method

.method private orderCheckSocTimer()V
    .locals 7

    .line 584
    iget-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmAlarmManager(Lcom/android/server/MiuiBatteryStatsService;)Landroid/app/AlarmManager;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmPendingIntentCheckSoc(Lcom/android/server/MiuiBatteryStatsService;)Landroid/app/PendingIntent;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 587
    :cond_0
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->getPdAuthentication()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmIsOrderedCheckSocTimer(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 588
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->getChargingPowerMax()I

    move-result v0

    const/4 v2, 0x2

    const/16 v3, 0x21

    if-ne v0, v3, :cond_1

    .line 589
    iget-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v0, v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fputmIsOrderedCheckSocTimer(Lcom/android/server/MiuiBatteryStatsService;Z)V

    .line 590
    iget-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmAlarmManager(Lcom/android/server/MiuiBatteryStatsService;)Landroid/app/AlarmManager;

    move-result-object v0

    .line 591
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    const-wide/32 v5, 0x6ddd00

    add-long/2addr v3, v5

    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmPendingIntentCheckSoc(Lcom/android/server/MiuiBatteryStatsService;)Landroid/app/PendingIntent;

    move-result-object v1

    .line 590
    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/app/AlarmManager;->setExactAndAllowWhileIdle(IJLandroid/app/PendingIntent;)V

    goto :goto_0

    .line 592
    :cond_1
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->getChargingPowerMax()I

    move-result v0

    if-le v0, v3, :cond_2

    .line 593
    iget-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v0, v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fputmIsOrderedCheckSocTimer(Lcom/android/server/MiuiBatteryStatsService;Z)V

    .line 594
    iget-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmAlarmManager(Lcom/android/server/MiuiBatteryStatsService;)Landroid/app/AlarmManager;

    move-result-object v0

    .line 595
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    const-wide/32 v5, 0x36ee80

    add-long/2addr v3, v5

    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmPendingIntentCheckSoc(Lcom/android/server/MiuiBatteryStatsService;)Landroid/app/PendingIntent;

    move-result-object v1

    .line 594
    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/app/AlarmManager;->setExactAndAllowWhileIdle(IJLandroid/app/PendingIntent;)V

    .line 598
    :cond_2
    :goto_0
    return-void

    .line 585
    :cond_3
    :goto_1
    return-void
.end method

.method private sendAdjustVolBroadcast()V
    .locals 3

    .line 693
    new-instance v0, Landroid/content/Intent;

    const-string v1, "miui.intent.action.ADJUST_VOLTAGE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 694
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 695
    const-string v1, "miui.intent.extra.ADJUST_VOLTAGE_TS"

    invoke-static {}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$sfgetmIsSatisfyTempSocCondition()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 696
    const-string v1, "miui.intent.extra.ADJUST_VOLTAGE_TL"

    invoke-static {}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$sfgetmIsSatisfyTempLevelCondition()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 697
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "send broadcast adjust , mIsSatisfyTempSocCondition = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$sfgetmIsSatisfyTempSocCondition()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , mIsSatisfyTempLevelCondition = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$sfgetmIsSatisfyTempLevelCondition()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiBatteryStatsService"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 700
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmContext(Lcom/android/server/MiuiBatteryStatsService;)Landroid/content/Context;

    move-result-object v1

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 701
    return-void
.end method

.method private sendBatteryTempData()V
    .locals 5

    .line 1197
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->TEMP_COLLECTED_SCENE:[Ljava/lang/String;

    array-length v2, v1

    if-ge v0, v2, :cond_3

    .line 1198
    iget-object v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mBatteryTemps:Landroid/util/ArrayMap;

    aget-object v1, v1, v0

    invoke-virtual {v2, v1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;

    .line 1199
    .local v1, "batteryTempInfo":Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;
    iget-object v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->TEMP_COLLECTED_SCENE:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->getCondition()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    return-void

    .line 1200
    :cond_0
    invoke-virtual {v1}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->getDataCount()I

    move-result v2

    if-nez v2, :cond_1

    goto :goto_1

    .line 1201
    :cond_1
    invoke-virtual {v1}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->setAverageTemp()V

    .line 1202
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1203
    .local v2, "params":Landroid/os/Bundle;
    const-string v3, "battery_charge_discharge_state"

    invoke-virtual {v1}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->getCondition()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1204
    const-string v3, "max_temp"

    invoke-virtual {v1}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->getMaxTemp()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1205
    const-string v3, "min_temp"

    invoke-virtual {v1}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->getMinTemp()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1206
    const-string v3, "ave_temp"

    invoke-virtual {v1}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->getAverageTemp()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1207
    const-string v3, "31000000094"

    const-string v4, "battery_temp"

    invoke-direct {p0, v2, v3, v4}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendOneTrackInfo(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    .line 1208
    invoke-virtual {v1}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->reset()V

    .line 1209
    iget-object v3, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v3}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetDEBUG(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1210
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->TEMP_COLLECTED_SCENE:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " params =  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "MiuiBatteryStatsService"

    invoke-static {v4, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1197
    .end local v1    # "batteryTempInfo":Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;
    .end local v2    # "params":Landroid/os/Bundle;
    :cond_2
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 1213
    .end local v0    # "i":I
    :cond_3
    return-void
.end method

.method private sendBatteryTempVoltageTimeData(Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;)V
    .locals 3
    .param p1, "info"    # Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;

    .line 1216
    invoke-static {p1}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->-$$Nest$mstopTime(Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;)V

    .line 1217
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1218
    .local v0, "params":Landroid/os/Bundle;
    invoke-virtual {p1}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->getHighTempTotalTime()J

    move-result-wide v1

    invoke-direct {p0, v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->getTimeHours(J)Ljava/lang/String;

    move-result-object v1

    const-string v2, "high_temp_time"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1219
    invoke-virtual {p1}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->getHighVoltageTotalTime()J

    move-result-wide v1

    invoke-direct {p0, v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->getTimeHours(J)Ljava/lang/String;

    move-result-object v1

    const-string v2, "high_voltage_time"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1220
    invoke-virtual {p1}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->getHighTempVoltageTotalTime()J

    move-result-wide v1

    invoke-direct {p0, v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->getTimeHours(J)Ljava/lang/String;

    move-result-object v1

    const-string v2, "high_temp_voltage_time"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1221
    invoke-virtual {p1}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->getFullChargeTotalTime()J

    move-result-wide v1

    invoke-direct {p0, v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->getTimeHours(J)Ljava/lang/String;

    move-result-object v1

    const-string v2, "full_charge_total_time"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1222
    const-string v1, "charge_status"

    invoke-virtual {p1}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->getCondition()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1224
    const-string v1, "31000000094"

    const-string v2, "battery_high_temp_voltage"

    invoke-direct {p0, v0, v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendOneTrackInfo(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    .line 1226
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetDEBUG(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1227
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->getCondition()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " params =  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiBatteryStatsService"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1229
    :cond_0
    invoke-static {p1}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->-$$Nest$mclearTime(Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;)V

    .line 1230
    return-void
.end method

.method private sendOneTrackInfo(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "params"    # Landroid/os/Bundle;
    .param p2, "id"    # Ljava/lang/String;
    .param p3, "event"    # Ljava/lang/String;

    .line 675
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_0

    return-void

    .line 676
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "onetrack.action.TRACK_EVENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 677
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.miui.analytics"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 678
    const-string v1, "APP_ID"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 679
    const-string v1, "EVENT_NAME"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 680
    const-string v1, "PACKAGE"

    const-string v2, "Android"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 681
    invoke-virtual {v0, p1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 682
    sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v1, :cond_1

    .line 683
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 686
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmContext(Lcom/android/server/MiuiBatteryStatsService;)Landroid/content/Context;

    move-result-object v1

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 689
    goto :goto_0

    .line 687
    :catch_0
    move-exception v1

    .line 688
    .local v1, "e":Ljava/lang/IllegalStateException;
    const-string v2, "MiuiBatteryStatsService"

    const-string v3, "Start one-Track service failed"

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 690
    .end local v1    # "e":Ljava/lang/IllegalStateException;
    :goto_0
    return-void
.end method


# virtual methods
.method public calculateTemp(Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;I)V
    .locals 1
    .param p1, "t"    # Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;
    .param p2, "temp"    # I

    .line 622
    invoke-virtual {p1}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->getMaxTemp()I

    move-result v0

    if-ge v0, p2, :cond_0

    .line 623
    invoke-virtual {p1, p2}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->setMaxTemp(I)V

    .line 626
    :cond_0
    invoke-virtual {p1}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->getMinTemp()I

    move-result v0

    if-le v0, p2, :cond_1

    .line 627
    invoke-virtual {p1, p2}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->setMinTemp(I)V

    .line 630
    :cond_1
    invoke-virtual {p1, p2}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->setTotalTemp(I)V

    .line 631
    invoke-virtual {p1}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->setDataCount()V

    .line 632
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .line 749
    iget v0, p1, Landroid/os/Message;->what:I

    const-string v1, "lpd_update_en"

    packed-switch v0, :pswitch_data_0

    .line 825
    const-string v0, "MiuiBatteryStatsService"

    const-string v1, "no message to handle"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 822
    :pswitch_0
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->handleCheckSoc()V

    .line 823
    goto/16 :goto_1

    .line 817
    :pswitch_1
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->handleLpdCountReport()V

    .line 819
    :pswitch_2
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->orderCheckSocTimer()V

    .line 820
    goto/16 :goto_1

    .line 812
    :pswitch_3
    iget-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v0

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Lmiui/util/IMiCharge;->setMiChargePath(Ljava/lang/String;Ljava/lang/String;)Z

    .line 813
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 814
    .local v0, "lpdInfomation":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->handleLPDInfomation(Ljava/lang/String;)V

    .line 815
    goto/16 :goto_1

    .line 809
    .end local v0    # "lpdInfomation":Ljava/lang/String;
    :pswitch_4
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->handleWirelessComposite()V

    .line 810
    goto/16 :goto_1

    .line 806
    :pswitch_5
    iget-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmBatteryTempLevel(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->-$$Nest$mcycleCheck(Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;)V

    .line 807
    goto/16 :goto_1

    .line 802
    :pswitch_6
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/content/Intent;

    .line 803
    .local v0, "k":Landroid/content/Intent;
    invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->handleSreiesDeltaVoltage(Landroid/content/Intent;)V

    .line 804
    goto/16 :goto_1

    .line 799
    .end local v0    # "k":Landroid/content/Intent;
    :pswitch_7
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->handleIntermittentCharge()V

    .line 800
    goto/16 :goto_1

    .line 796
    :pswitch_8
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->handleSlowCharge()V

    .line 797
    goto/16 :goto_1

    .line 793
    :pswitch_9
    invoke-virtual {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->limitTimeForSlowCharge()V

    .line 794
    goto/16 :goto_1

    .line 790
    :pswitch_a
    iget v0, p1, Landroid/os/Message;->arg1:I

    invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->handleVbusDisable(I)V

    .line 791
    goto/16 :goto_1

    .line 785
    :pswitch_b
    iget-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v0

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Lmiui/util/IMiCharge;->setMiChargePath(Ljava/lang/String;Ljava/lang/String;)Z

    .line 786
    iget-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmBatteryTempSocTime(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->-$$Nest$mreadDataFromFile(Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;)V

    .line 787
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendAdjustVolBroadcast()V

    .line 788
    goto :goto_1

    .line 782
    :pswitch_c
    iget-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmBatteryTempSocTime(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;->-$$Nest$mwriteDataToFile(Lcom/android/server/MiuiBatteryStatsService$BatteryTempSocTimeInfo;)V

    .line 783
    goto :goto_1

    .line 778
    :pswitch_d
    iget-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmBatteryInfoNormal(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendBatteryTempVoltageTimeData(Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;)V

    .line 779
    iget-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmBatteryInfoFull(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendBatteryTempVoltageTimeData(Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;)V

    .line 780
    goto :goto_1

    .line 775
    :pswitch_e
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendBatteryTempData()V

    .line 776
    goto :goto_1

    .line 771
    :pswitch_f
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/content/Intent;

    .line 772
    .local v0, "j":Landroid/content/Intent;
    invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->handleBatteryTemp(Landroid/content/Intent;)V

    .line 773
    goto :goto_1

    .line 767
    .end local v0    # "j":Landroid/content/Intent;
    :pswitch_10
    iget v0, p1, Landroid/os/Message;->arg1:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    move v0, v1

    .line 768
    .local v0, "value":Z
    invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->handlePowerOff(Z)V

    .line 769
    goto :goto_1

    .line 764
    .end local v0    # "value":Z
    :pswitch_11
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->handleChargeAction()V

    .line 765
    goto :goto_1

    .line 760
    :pswitch_12
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/content/Intent;

    .line 761
    .local v0, "i":Landroid/content/Intent;
    invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->handleUsbFunction(Landroid/content/Intent;)V

    .line 762
    goto :goto_1

    .line 757
    .end local v0    # "i":Landroid/content/Intent;
    :pswitch_13
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->handleWirelessReverseCharge()V

    .line 758
    goto :goto_1

    .line 754
    :pswitch_14
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->handleCharge()V

    .line 755
    goto :goto_1

    .line 751
    :pswitch_15
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->handleBatteryHealth()V

    .line 752
    nop

    .line 827
    :goto_1
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public limitTimeForSlowCharge()V
    .locals 7

    .line 570
    const-string v0, "USB_FLOAT"

    invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->getBatteryChargeType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 571
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->handleSlowCharge()V

    goto :goto_0

    .line 573
    :cond_0
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->getChargingPowerMax()I

    move-result v0

    const/16 v1, 0x60

    const/4 v2, 0x2

    if-ge v0, v1, :cond_1

    .line 574
    iget-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmAlarmManager(Lcom/android/server/MiuiBatteryStatsService;)Landroid/app/AlarmManager;

    move-result-object v0

    .line 575
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    const-wide/32 v5, 0x927c0

    add-long/2addr v3, v5

    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmPendingIntentLimitTime(Lcom/android/server/MiuiBatteryStatsService;)Landroid/app/PendingIntent;

    move-result-object v1

    .line 574
    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/app/AlarmManager;->setExactAndAllowWhileIdle(IJLandroid/app/PendingIntent;)V

    goto :goto_0

    .line 577
    :cond_1
    iget-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmAlarmManager(Lcom/android/server/MiuiBatteryStatsService;)Landroid/app/AlarmManager;

    move-result-object v0

    .line 578
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    const-wide/32 v5, 0x493e0

    add-long/2addr v3, v5

    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmPendingIntentLimitTime(Lcom/android/server/MiuiBatteryStatsService;)Landroid/app/PendingIntent;

    move-result-object v1

    .line 577
    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/app/AlarmManager;->setExactAndAllowWhileIdle(IJLandroid/app/PendingIntent;)V

    .line 581
    :goto_0
    return-void
.end method

.method public parseInt(Ljava/lang/String;)I
    .locals 5
    .param p1, "argument"    # Ljava/lang/String;

    .line 663
    const-string v0, "MiuiBatteryStatsService"

    const/4 v1, -0x1

    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 664
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    return v0

    .line 666
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "argument = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 667
    return v1

    .line 668
    :catch_0
    move-exception v2

    .line 669
    .local v2, "e":Ljava/lang/NumberFormatException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid integer argument "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 670
    return v1
.end method

.method public sendMessage(II)V
    .locals 1
    .param p1, "what"    # I
    .param p2, "arg1"    # I

    .line 635
    invoke-virtual {p0, p1}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->removeMessages(I)V

    .line 636
    invoke-static {p0, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 637
    .local v0, "m":Landroid/os/Message;
    iput p2, v0, Landroid/os/Message;->arg1:I

    .line 638
    invoke-virtual {p0, v0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessage(Landroid/os/Message;)Z

    .line 639
    return-void
.end method

.method public sendMessageDelayed(IJ)V
    .locals 1
    .param p1, "what"    # I
    .param p2, "delayMillis"    # J

    .line 656
    invoke-virtual {p0, p1}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->removeMessages(I)V

    .line 657
    invoke-static {p0, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 658
    .local v0, "m":Landroid/os/Message;
    invoke-virtual {p0, v0, p2, p3}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 659
    return-void
.end method

.method public sendMessageDelayed(ILjava/lang/Object;J)V
    .locals 1
    .param p1, "what"    # I
    .param p2, "arg"    # Ljava/lang/Object;
    .param p3, "delayMillis"    # J

    .line 642
    invoke-virtual {p0, p1}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->removeMessages(I)V

    .line 643
    invoke-static {p0, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 644
    .local v0, "m":Landroid/os/Message;
    iput-object p2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 645
    invoke-virtual {p0, v0, p3, p4}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 646
    return-void
.end method

.method public sendMessageDelayed(IZJ)V
    .locals 1
    .param p1, "what"    # I
    .param p2, "arg"    # Z
    .param p3, "delayMillis"    # J

    .line 649
    invoke-virtual {p0, p1}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->removeMessages(I)V

    .line 650
    invoke-static {p0, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 651
    .local v0, "m":Landroid/os/Message;
    iput p2, v0, Landroid/os/Message;->arg1:I

    .line 652
    invoke-virtual {p0, v0, p3, p4}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 653
    return-void
.end method

.method public switchTimer(ZI)V
    .locals 5
    .param p1, "plugged"    # Z
    .param p2, "soc"    # I

    .line 553
    iget-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmAlarmManager(Lcom/android/server/MiuiBatteryStatsService;)Landroid/app/AlarmManager;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmPendingIntentCycleCheck(Lcom/android/server/MiuiBatteryStatsService;)Landroid/app/PendingIntent;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 556
    :cond_0
    if-eqz p1, :cond_1

    .line 557
    const/16 v0, 0x5a

    if-ge p2, v0, :cond_2

    .line 559
    iget-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmAlarmManager(Lcom/android/server/MiuiBatteryStatsService;)Landroid/app/AlarmManager;

    move-result-object v0

    .line 560
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    const-wide/16 v3, 0x7530

    add-long/2addr v1, v3

    iget-object v3, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v3}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmPendingIntentCycleCheck(Lcom/android/server/MiuiBatteryStatsService;)Landroid/app/PendingIntent;

    move-result-object v3

    .line 559
    const/4 v4, 0x2

    invoke-virtual {v0, v4, v1, v2, v3}, Landroid/app/AlarmManager;->setExactAndAllowWhileIdle(IJLandroid/app/PendingIntent;)V

    goto :goto_0

    .line 564
    :cond_1
    iget-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmAlarmManager(Lcom/android/server/MiuiBatteryStatsService;)Landroid/app/AlarmManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmPendingIntentCycleCheck(Lcom/android/server/MiuiBatteryStatsService;)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 565
    iget-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmBatteryTempLevel(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->-$$Nest$mdataReset(Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;)V

    .line 567
    :cond_2
    :goto_0
    return-void

    .line 554
    :cond_3
    :goto_1
    return-void
.end method

.method public updateChargeInfo(Z)V
    .locals 3
    .param p1, "plugged"    # Z

    .line 544
    const/4 v0, 0x1

    if-eqz p1, :cond_0

    .line 545
    const-wide/32 v1, 0x493e0

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(IJ)V

    goto :goto_0

    .line 547
    :cond_0
    invoke-virtual {p0, v0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->removeMessages(I)V

    .line 548
    const/4 v0, 0x4

    const-wide/16 v1, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(IJ)V

    .line 550
    :goto_0
    return-void
.end method
