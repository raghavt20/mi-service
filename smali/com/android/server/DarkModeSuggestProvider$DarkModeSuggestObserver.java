class com.android.server.DarkModeSuggestProvider$DarkModeSuggestObserver extends android.database.ContentObserver {
	 /* .source "DarkModeSuggestProvider.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/DarkModeSuggestProvider; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "DarkModeSuggestObserver" */
} // .end annotation
/* # instance fields */
private final java.lang.String TAG;
private android.content.Context mContext;
final com.android.server.DarkModeSuggestProvider this$0; //synthetic
/* # direct methods */
public com.android.server.DarkModeSuggestProvider$DarkModeSuggestObserver ( ) {
/* .locals 0 */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .param p3, "context" # Landroid/content/Context; */
/* .line 105 */
this.this$0 = p1;
/* .line 106 */
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
/* .line 101 */
/* const-class p1, Lcom/android/server/DarkModeSuggestProvider; */
(( java.lang.Class ) p1 ).getSimpleName ( ); // invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
this.TAG = p1;
/* .line 107 */
this.mContext = p3;
/* .line 108 */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "selfChange" # Z */
/* .line 112 */
v0 = this.TAG;
final String v1 = "onChange"; // const-string v1, "onChange"
android.util.Slog .i ( v0,v1 );
/* .line 113 */
v0 = this.this$0;
v1 = this.mContext;
(( com.android.server.DarkModeSuggestProvider ) v0 ).updateCloudDataForDarkModeSuggest ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/DarkModeSuggestProvider;->updateCloudDataForDarkModeSuggest(Landroid/content/Context;)V
/* .line 114 */
return;
} // .end method
