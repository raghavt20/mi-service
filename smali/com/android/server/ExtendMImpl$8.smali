.class Lcom/android/server/ExtendMImpl$8;
.super Landroid/os/IVoldTaskListener$Stub;
.source "ExtendMImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/ExtendMImpl;->stopFlush()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/ExtendMImpl;


# direct methods
.method constructor <init>(Lcom/android/server/ExtendMImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/ExtendMImpl;

    .line 1272
    iput-object p1, p0, Lcom/android/server/ExtendMImpl$8;->this$0:Lcom/android/server/ExtendMImpl;

    invoke-direct {p0}, Landroid/os/IVoldTaskListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onFinished(ILandroid/os/PersistableBundle;)V
    .locals 2
    .param p1, "status"    # I
    .param p2, "extras"    # Landroid/os/PersistableBundle;

    .line 1280
    const-string v0, "ExtM"

    if-nez p1, :cond_0

    .line 1281
    const-string/jumbo v1, "send SIGUSR1 to flush thread success"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1282
    iget-object v0, p0, Lcom/android/server/ExtendMImpl$8;->this$0:Lcom/android/server/ExtendMImpl;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/android/server/ExtendMImpl;->-$$Nest$mupdateState(Lcom/android/server/ExtendMImpl;I)V

    goto :goto_0

    .line 1283
    :cond_0
    const/4 v1, -0x1

    if-ne p1, v1, :cond_1

    .line 1284
    const-string v1, "The flush thread is not running"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1286
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/android/server/ExtendMImpl$8;->this$0:Lcom/android/server/ExtendMImpl;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/server/ExtendMImpl;->-$$Nest$fputisFlushFinished(Lcom/android/server/ExtendMImpl;Z)V

    .line 1287
    return-void
.end method

.method public onStatus(ILandroid/os/PersistableBundle;)V
    .locals 0
    .param p1, "status"    # I
    .param p2, "extras"    # Landroid/os/PersistableBundle;

    .line 1276
    return-void
.end method
