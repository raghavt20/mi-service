.class public Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;
.super Ljava/lang/Object;
.source "ScoutSystemMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/ScoutSystemMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ScoutSystemInfo"
.end annotation


# instance fields
.field private mBinderTransInfo:Ljava/lang/String;

.field private mDescribeInfo:Ljava/lang/String;

.field private mDetails:Ljava/lang/String;

.field private mEvent:I

.field private mHandlerChecks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;",
            ">;"
        }
    .end annotation
.end field

.field private mIsHalf:Z

.field private mPreScoutLevel:I

.field private mScoutLevel:I

.field private mTimeStamp:J

.field private mUuid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;IILjava/util/ArrayList;ZLjava/lang/String;)V
    .locals 1
    .param p1, "mDescribeInfo"    # Ljava/lang/String;
    .param p2, "mDetails"    # Ljava/lang/String;
    .param p3, "mScoutLevel"    # I
    .param p4, "mPreScoutLevel"    # I
    .param p6, "mIsHalf"    # Z
    .param p7, "mUuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "II",
            "Ljava/util/ArrayList<",
            "Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;",
            ">;Z",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 325
    .local p5, "mHandlerChecks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 326
    iput-object p1, p0, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->mDescribeInfo:Ljava/lang/String;

    .line 327
    iput-object p2, p0, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->mDetails:Ljava/lang/String;

    .line 328
    iput p3, p0, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->mScoutLevel:I

    .line 329
    iput p4, p0, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->mPreScoutLevel:I

    .line 330
    iput-boolean p6, p0, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->mIsHalf:Z

    .line 331
    iput-object p5, p0, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->mHandlerChecks:Ljava/util/ArrayList;

    .line 332
    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->mBinderTransInfo:Ljava/lang/String;

    .line 333
    iput-object p7, p0, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->mUuid:Ljava/lang/String;

    .line 334
    return-void
.end method

.method private getFormatDateTime(J)Ljava/lang/String;
    .locals 4
    .param p1, "timeMillis"    # J

    .line 402
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gtz v0, :cond_0

    const-string/jumbo v0, "unknow time"

    return-object v0

    .line 403
    :cond_0
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, p1, p2}, Ljava/util/Date;-><init>(J)V

    .line 404
    .local v0, "date":Ljava/util/Date;
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string/jumbo v2, "yyyy-MM-dd-HH-mm-ss"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 405
    .local v1, "dateFormat":Ljava/text/SimpleDateFormat;
    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method


# virtual methods
.method public getBinderTransInfo()Ljava/lang/String;
    .locals 1

    .line 361
    iget-object v0, p0, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->mBinderTransInfo:Ljava/lang/String;

    return-object v0
.end method

.method public getDescribeInfo()Ljava/lang/String;
    .locals 1

    .line 341
    iget-object v0, p0, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->mDescribeInfo:Ljava/lang/String;

    return-object v0
.end method

.method public getDetails()Ljava/lang/String;
    .locals 1

    .line 373
    iget-object v0, p0, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->mDetails:Ljava/lang/String;

    return-object v0
.end method

.method public getEvent()I
    .locals 1

    .line 381
    iget v0, p0, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->mEvent:I

    return v0
.end method

.method public getEventString(I)Ljava/lang/String;
    .locals 1
    .param p1, "eventId"    # I

    .line 409
    sparse-switch p1, :sswitch_data_0

    .line 425
    const-string v0, "UNKNOW"

    return-object v0

    .line 417
    :sswitch_0
    const-string v0, "FW_SCOUT_SLOW"

    return-object v0

    .line 415
    :sswitch_1
    const-string v0, "FW_SCOUT_NORMALLY"

    return-object v0

    .line 413
    :sswitch_2
    const-string v0, "FW_SCOUT_BINDER_FULL"

    return-object v0

    .line 411
    :sswitch_3
    const-string v0, "FW_SCOUT_HANG"

    return-object v0

    .line 423
    :sswitch_4
    const-string v0, "WATCHDOG_DUMP_ERROR"

    return-object v0

    .line 419
    :sswitch_5
    const-string v0, "HALF_WATCHDOG"

    return-object v0

    .line 421
    :sswitch_6
    const-string v0, "WATCHDOG"

    return-object v0

    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_6
        0x180 -> :sswitch_5
        0x181 -> :sswitch_4
        0x190 -> :sswitch_3
        0x191 -> :sswitch_2
        0x192 -> :sswitch_1
        0x193 -> :sswitch_0
    .end sparse-switch
.end method

.method public getHalfState()Z
    .locals 1

    .line 337
    iget-boolean v0, p0, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->mIsHalf:Z

    return v0
.end method

.method public getHandlerCheckers()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;",
            ">;"
        }
    .end annotation

    .line 353
    iget-object v0, p0, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->mHandlerChecks:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getPreScoutLevel()I
    .locals 1

    .line 349
    iget v0, p0, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->mPreScoutLevel:I

    return v0
.end method

.method public getScoutLevel()I
    .locals 1

    .line 345
    iget v0, p0, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->mScoutLevel:I

    return v0
.end method

.method public getTimeStamp()J
    .locals 2

    .line 369
    iget-wide v0, p0, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->mTimeStamp:J

    return-wide v0
.end method

.method public getUuid()Ljava/lang/String;
    .locals 1

    .line 384
    iget-object v0, p0, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->mUuid:Ljava/lang/String;

    return-object v0
.end method

.method public setBinderTransInfo(Ljava/lang/String;)V
    .locals 0
    .param p1, "mBinderTransInfo"    # Ljava/lang/String;

    .line 357
    iput-object p1, p0, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->mBinderTransInfo:Ljava/lang/String;

    .line 358
    return-void
.end method

.method public setEvent(I)V
    .locals 0
    .param p1, "mEvent"    # I

    .line 377
    iput p1, p0, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->mEvent:I

    .line 378
    return-void
.end method

.method public setTimeStamp(J)V
    .locals 0
    .param p1, "mTimeStamp"    # J

    .line 365
    iput-wide p1, p0, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->mTimeStamp:J

    .line 366
    return-void
.end method

.method public setUuid(Ljava/lang/String;)V
    .locals 0
    .param p1, "mUuid"    # Ljava/lang/String;

    .line 387
    iput-object p1, p0, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->mUuid:Ljava/lang/String;

    .line 388
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 391
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 392
    .local v0, "sb":Ljava/lang/StringBuilder;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\nException \uff1a "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->mEvent:I

    invoke-virtual {p0, v2}, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->getEventString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nTimeStamp : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->mTimeStamp:J

    .line 393
    invoke-direct {p0, v2, v3}, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->getFormatDateTime(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nProcessName : system_server\nPid : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 395
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nSummary : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->mDescribeInfo:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->mBinderTransInfo:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 392
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 398
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
