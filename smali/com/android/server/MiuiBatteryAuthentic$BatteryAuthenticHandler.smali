.class Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;
.super Landroid/os/Handler;
.source "MiuiBatteryAuthentic.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/MiuiBatteryAuthentic;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "BatteryAuthenticHandler"
.end annotation


# static fields
.field static final CONFIRM:I = 0x3

.field private static final CONTENT_TYPE:Ljava/lang/String; = "Content-Type"

.field private static final DEFAULT_CONNECT_TIME_OUT:I = 0x7d0

.field private static final DEFAULT_READ_TIME_OUT:I = 0x7d0

.field private static final DELAY_SET_TIME:I = 0x1388

.field static final INIT:I = 0x1

.field static final MSG_BATTERY_AUTHENTIC:I = 0x0

.field static final MSG_SIGN_RESULT:I = 0x3

.field static final MSG_VERITY_SIGN:I = 0x1

.field static final MSG_VERITY_SIGN_CONFIRM:I = 0x2

.field private static final TYPE:Ljava/lang/String; = "application/json; charset=UTF-8"

.field static final VERIFY:I = 0x2


# instance fields
.field private mAndroidId:Ljava/lang/String;

.field private mBatteryAuthentic:Ljava/lang/String;

.field private mChallenge:Ljava/lang/String;

.field private mConFirmChallenge:Ljava/lang/String;

.field private mConfirmResultCount:I

.field private mGetFromServerCount:I

.field private mIsConfirm:Z

.field private mIsGetServerInfo:Z

.field private mProjectName:Ljava/lang/String;

.field private mTryGetBatteryCount:I

.field final synthetic this$0:Lcom/android/server/MiuiBatteryAuthentic;


# direct methods
.method public constructor <init>(Lcom/android/server/MiuiBatteryAuthentic;Landroid/os/Looper;)V
    .locals 1
    .param p1, "this$0"    # Lcom/android/server/MiuiBatteryAuthentic;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 155
    iput-object p1, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->this$0:Lcom/android/server/MiuiBatteryAuthentic;

    .line 156
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 144
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mAndroidId:Ljava/lang/String;

    .line 145
    iput-object v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mProjectName:Ljava/lang/String;

    .line 146
    iput-object v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mChallenge:Ljava/lang/String;

    .line 147
    iput-object v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mConFirmChallenge:Ljava/lang/String;

    .line 148
    iput-object v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mBatteryAuthentic:Ljava/lang/String;

    .line 149
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mGetFromServerCount:I

    .line 150
    iput v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mConfirmResultCount:I

    .line 151
    iput v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mTryGetBatteryCount:I

    .line 152
    iput-boolean v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mIsConfirm:Z

    .line 153
    iput-boolean v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mIsGetServerInfo:Z

    .line 157
    return-void
.end method

.method private SHA256(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "strText"    # Ljava/lang/String;

    .line 389
    const-string v0, ""

    .line 390
    .local v0, "strResult":Ljava/lang/String;
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 392
    :try_start_0
    const-string v1, "SHA-256"

    invoke-static {v1}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    .line 393
    .local v1, "messageDigest":Ljava/security/MessageDigest;
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/security/MessageDigest;->update([B)V

    .line 394
    invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v2

    .line 395
    .local v2, "byteBuffer":[B
    invoke-virtual {p0, v2}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->bytesToHexString([B)Ljava/lang/String;

    move-result-object v3
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v3

    .line 400
    .end local v1    # "messageDigest":Ljava/security/MessageDigest;
    .end local v2    # "byteBuffer":[B
    :goto_0
    goto :goto_1

    .line 398
    :catch_0
    move-exception v1

    .line 399
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 396
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v1

    .line 397
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v1}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    .end local v1    # "e":Ljava/security/NoSuchAlgorithmException;
    goto :goto_0

    .line 402
    :cond_0
    :goto_1
    return-object v0
.end method

.method private closeBufferedReader(Ljava/io/BufferedReader;)V
    .locals 1
    .param p1, "br"    # Ljava/io/BufferedReader;

    .line 644
    if-eqz p1, :cond_0

    .line 646
    :try_start_0
    invoke-virtual {p1}, Ljava/io/BufferedReader;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 649
    goto :goto_0

    .line 647
    :catch_0
    move-exception v0

    .line 648
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 651
    .end local v0    # "e":Ljava/io/IOException;
    :cond_0
    :goto_0
    return-void
.end method

.method private getAndroidId()Ljava/lang/String;
    .locals 2

    .line 288
    const-string v0, "ro.serialno"

    const-string/jumbo v1, "unknown"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 289
    .local v0, "androidID":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 290
    return-object v0

    .line 292
    :cond_0
    const-string v1, ""

    return-object v1
.end method

.method private getImei()Ljava/lang/String;
    .locals 3

    .line 358
    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/telephony/TelephonyManager;->getImeiList()Ljava/util/List;

    move-result-object v0

    .line 359
    .local v0, "imeis":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 360
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 361
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 362
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    return-object v2

    .line 360
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 366
    .end local v1    # "i":I
    :cond_1
    const-string v1, "1234567890"

    return-object v1
.end method

.method private getProjectName()Ljava/lang/String;
    .locals 2

    .line 296
    const-string v0, "ro.product.device"

    const-string/jumbo v1, "unknown"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 297
    .local v0, "device":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 298
    return-object v0

    .line 300
    :cond_0
    const-string v1, ""

    return-object v1
.end method

.method private toByte(C)B
    .locals 1
    .param p1, "c"    # C

    .line 353
    const-string v0, "0123456789abcdef"

    invoke-virtual {v0, p1}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    int-to-byte v0, v0

    .line 354
    .local v0, "b":B
    return v0
.end method

.method private verifyECDSA()Z
    .locals 9

    .line 244
    const/4 v0, 0x0

    .line 247
    .local v0, "result":Z
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->this$0:Lcom/android/server/MiuiBatteryAuthentic;

    iget-object v2, v2, Lcom/android/server/MiuiBatteryAuthentic;->mBatterySn:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->this$0:Lcom/android/server/MiuiBatteryAuthentic;

    iget-object v2, v2, Lcom/android/server/MiuiBatteryAuthentic;->mImei:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 248
    .local v1, "str":Ljava/lang/String;
    iget-object v2, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->this$0:Lcom/android/server/MiuiBatteryAuthentic;

    iget-object v2, v2, Lcom/android/server/MiuiBatteryAuthentic;->mCloudSign:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 249
    iget-object v2, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->this$0:Lcom/android/server/MiuiBatteryAuthentic;

    iget-object v2, v2, Lcom/android/server/MiuiBatteryAuthentic;->mCloudSign:Ljava/lang/String;

    .line 253
    .local v2, "signString":Ljava/lang/String;
    invoke-virtual {p0, v2}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->hexStringToByte(Ljava/lang/String;)[B

    move-result-object v3

    .line 255
    .local v3, "sign":[B
    new-instance v4, Ljava/security/spec/X509EncodedKeySpec;

    const-string v5, "3059301306072a8648ce3d020106082a8648ce3d030107034200045846fce7eaab1053c62f76cd7c61ae09a8411a5c106cad7a95c11c26dd25e507e963e2ae8f2c9672db92fe9834584dc41996454c8c929fc26e9d512e4096f450"

    invoke-virtual {p0, v5}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->hexStringToByte(Ljava/lang/String;)[B

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/security/spec/X509EncodedKeySpec;-><init>([B)V

    .line 256
    .local v4, "x509EncodedKeySpec":Ljava/security/spec/X509EncodedKeySpec;
    const-string v5, "EC"

    invoke-static {v5}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;)Ljava/security/KeyFactory;

    move-result-object v5

    .line 257
    .local v5, "keyFactory":Ljava/security/KeyFactory;
    invoke-virtual {v5, v4}, Ljava/security/KeyFactory;->generatePublic(Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;

    move-result-object v6

    .line 258
    .local v6, "publicKey":Ljava/security/PublicKey;
    const-string v7, "SHA256withECDSA"

    invoke-static {v7}, Ljava/security/Signature;->getInstance(Ljava/lang/String;)Ljava/security/Signature;

    move-result-object v7

    .line 259
    .local v7, "signature":Ljava/security/Signature;
    invoke-virtual {v7, v6}, Ljava/security/Signature;->initVerify(Ljava/security/PublicKey;)V

    .line 260
    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/security/Signature;->update([B)V

    .line 261
    invoke-virtual {v7, v3}, Ljava/security/Signature;->verify([B)Z

    move-result v8
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v8

    .line 265
    .end local v1    # "str":Ljava/lang/String;
    .end local v3    # "sign":[B
    .end local v4    # "x509EncodedKeySpec":Ljava/security/spec/X509EncodedKeySpec;
    .end local v5    # "keyFactory":Ljava/security/KeyFactory;
    .end local v6    # "publicKey":Ljava/security/PublicKey;
    .end local v7    # "signature":Ljava/security/Signature;
    goto :goto_0

    .line 251
    .end local v2    # "signString":Ljava/lang/String;
    .restart local v1    # "str":Ljava/lang/String;
    :cond_0
    return v0

    .line 263
    .end local v1    # "str":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 264
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 266
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return v0
.end method

.method private verifyFromServer()Z
    .locals 3

    .line 270
    invoke-virtual {p0}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->batteryVerifyInit()V

    .line 271
    iget-object v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mChallenge:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const-string v1, "MiuiBatteryAuthentic"

    if-nez v0, :cond_1

    .line 272
    invoke-virtual {p0}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->batteryVerify()V

    .line 273
    iget-object v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->this$0:Lcom/android/server/MiuiBatteryAuthentic;

    iget-object v0, v0, Lcom/android/server/MiuiBatteryAuthentic;->mCloudSign:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 274
    const/4 v0, 0x2

    const-wide/16 v1, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->sendMessageDelayed(IJ)V

    .line 275
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->sendMessageDelayed(IJ)V

    .line 276
    return v0

    .line 278
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mChallenge:Ljava/lang/String;

    .line 279
    const-string/jumbo v0, "verify error failed, get sign failed"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 282
    :cond_1
    const-string/jumbo v0, "verify error failed, get challenge failed"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 284
    :goto_0
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public batteryVerify()V
    .locals 14

    .line 460
    const-string v0, "MiuiBatteryAuthentic"

    const-string v1, "https://battery.mioffice.cn/api/bat/verify"

    .line 462
    .local v1, "url":Ljava/lang/String;
    const/4 v2, 0x0

    .line 463
    .local v2, "conn":Ljavax/net/ssl/HttpsURLConnection;
    const/4 v3, 0x0

    .line 464
    .local v3, "writer":Ljava/io/OutputStreamWriter;
    const/4 v4, 0x0

    .line 466
    .local v4, "br":Ljava/io/BufferedReader;
    const/4 v5, 0x2

    invoke-virtual {p0, v5}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->collectData(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 469
    .local v5, "jsonObject":Lorg/json/JSONObject;
    :try_start_0
    new-instance v6, Ljava/net/URL;

    invoke-direct {v6, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v6

    check-cast v6, Ljavax/net/ssl/HttpsURLConnection;

    move-object v2, v6

    .line 470
    const/16 v6, 0x7d0

    invoke-virtual {v2, v6}, Ljavax/net/ssl/HttpsURLConnection;->setConnectTimeout(I)V

    .line 471
    invoke-virtual {v2, v6}, Ljavax/net/ssl/HttpsURLConnection;->setReadTimeout(I)V

    .line 472
    const/4 v6, 0x1

    invoke-virtual {v2, v6}, Ljavax/net/ssl/HttpsURLConnection;->setDoOutput(Z)V

    .line 473
    invoke-virtual {v2, v6}, Ljavax/net/ssl/HttpsURLConnection;->setDoInput(Z)V

    .line 474
    const/4 v7, 0x0

    invoke-virtual {v2, v7}, Ljavax/net/ssl/HttpsURLConnection;->setUseCaches(Z)V

    .line 475
    const-string v7, "Content-Type"

    const-string v8, "application/json; charset=UTF-8"

    invoke-virtual {v2, v7, v8}, Ljavax/net/ssl/HttpsURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 476
    const-string v7, "POST"

    invoke-virtual {v2, v7}, Ljavax/net/ssl/HttpsURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 477
    invoke-virtual {v2}, Ljavax/net/ssl/HttpsURLConnection;->connect()V

    .line 479
    if-eqz v5, :cond_0

    .line 480
    new-instance v7, Ljava/io/OutputStreamWriter;

    invoke-virtual {v2}, Ljavax/net/ssl/HttpsURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    move-object v3, v7

    .line 481
    invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/io/OutputStreamWriter;->write(Ljava/lang/String;)V

    .line 482
    invoke-virtual {v3}, Ljava/io/OutputStreamWriter;->flush()V

    .line 483
    invoke-virtual {v3}, Ljava/io/OutputStreamWriter;->close()V

    .line 486
    :cond_0
    invoke-virtual {v2}, Ljavax/net/ssl/HttpsURLConnection;->getResponseCode()I

    move-result v7

    const/16 v8, 0xc8

    if-ne v7, v8, :cond_4

    .line 487
    new-instance v7, Ljava/io/BufferedReader;

    new-instance v8, Ljava/io/InputStreamReader;

    invoke-virtual {v2}, Ljavax/net/ssl/HttpsURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    const/16 v9, 0x400

    invoke-direct {v7, v8, v9}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V

    move-object v4, v7

    .line 488
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 490
    .local v7, "sb":Ljava/lang/StringBuilder;
    :goto_0
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v8

    move-object v9, v8

    .local v9, "line":Ljava/lang/String;
    if-eqz v8, :cond_1

    .line 491
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 495
    :cond_1
    new-instance v8, Lorg/json/JSONObject;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v8, v10}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 497
    .local v8, "result":Lorg/json/JSONObject;
    const-string v10, "code"

    invoke-virtual {v8, v10}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v10

    .line 498
    .local v10, "code":I
    if-nez v10, :cond_2

    .line 499
    const-string v11, "data"

    invoke-virtual {v8, v11}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v11

    .line 500
    .local v11, "data":Lorg/json/JSONObject;
    iget-object v12, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->this$0:Lcom/android/server/MiuiBatteryAuthentic;

    const-string/jumbo v13, "sign"

    invoke-virtual {v11, v13}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    iput-object v13, v12, Lcom/android/server/MiuiBatteryAuthentic;->mCloudSign:Ljava/lang/String;

    .line 501
    const-string v12, "rk"

    invoke-virtual {v11, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mConFirmChallenge:Ljava/lang/String;

    .line 502
    iput-boolean v6, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mIsGetServerInfo:Z

    .line 503
    .end local v11    # "data":Lorg/json/JSONObject;
    goto :goto_1

    :cond_2
    const/16 v11, 0x2715

    if-ne v10, v11, :cond_3

    .line 504
    iput-boolean v6, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mIsGetServerInfo:Z

    goto :goto_1

    .line 506
    :cond_3
    const-string v6, "request result is failed"

    invoke-static {v0, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/net/ProtocolException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 516
    .end local v7    # "sb":Ljava/lang/StringBuilder;
    .end local v8    # "result":Lorg/json/JSONObject;
    .end local v9    # "line":Ljava/lang/String;
    .end local v10    # "code":I
    :cond_4
    :goto_1
    if-eqz v2, :cond_5

    .line 517
    goto :goto_2

    .line 516
    :catchall_0
    move-exception v0

    goto :goto_3

    .line 513
    :catch_0
    move-exception v6

    .line 514
    .local v6, "e":Ljava/lang/Exception;
    :try_start_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "batteryVerify "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 516
    nop

    .end local v6    # "e":Ljava/lang/Exception;
    if-eqz v2, :cond_5

    .line 517
    goto :goto_2

    .line 511
    :catch_1
    move-exception v6

    .line 512
    .local v6, "e":Ljava/io/IOException;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "batteryVerify IOException "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 516
    nop

    .end local v6    # "e":Ljava/io/IOException;
    if-eqz v2, :cond_5

    .line 517
    goto :goto_2

    .line 509
    :catch_2
    move-exception v6

    .line 510
    .local v6, "e":Ljava/net/ProtocolException;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "batteryVerify ProtocolException "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 516
    nop

    .end local v6    # "e":Ljava/net/ProtocolException;
    if-eqz v2, :cond_5

    .line 517
    :goto_2
    invoke-virtual {v2}, Ljavax/net/ssl/HttpsURLConnection;->disconnect()V

    .line 519
    :cond_5
    invoke-direct {p0, v4}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->closeBufferedReader(Ljava/io/BufferedReader;)V

    .line 520
    nop

    .line 521
    return-void

    .line 516
    :goto_3
    if-eqz v2, :cond_6

    .line 517
    invoke-virtual {v2}, Ljavax/net/ssl/HttpsURLConnection;->disconnect()V

    .line 519
    :cond_6
    invoke-direct {p0, v4}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->closeBufferedReader(Ljava/io/BufferedReader;)V

    .line 520
    throw v0
.end method

.method public batteryVerifyConfirm()V
    .locals 12

    .line 584
    const-string v0, "MiuiBatteryAuthentic"

    const-string v1, "https://battery.mioffice.cn/api/bat/confirm"

    .line 586
    .local v1, "url":Ljava/lang/String;
    const/4 v2, 0x0

    .line 587
    .local v2, "conn":Ljavax/net/ssl/HttpsURLConnection;
    const/4 v3, 0x0

    .line 588
    .local v3, "writer":Ljava/io/OutputStreamWriter;
    const/4 v4, 0x0

    .line 590
    .local v4, "br":Ljava/io/BufferedReader;
    const/4 v5, 0x3

    invoke-virtual {p0, v5}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->collectData(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 593
    .local v5, "jsonObject":Lorg/json/JSONObject;
    :try_start_0
    new-instance v6, Ljava/net/URL;

    invoke-direct {v6, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v6

    check-cast v6, Ljavax/net/ssl/HttpsURLConnection;

    move-object v2, v6

    .line 594
    const/16 v6, 0x7d0

    invoke-virtual {v2, v6}, Ljavax/net/ssl/HttpsURLConnection;->setConnectTimeout(I)V

    .line 595
    invoke-virtual {v2, v6}, Ljavax/net/ssl/HttpsURLConnection;->setReadTimeout(I)V

    .line 596
    const/4 v6, 0x1

    invoke-virtual {v2, v6}, Ljavax/net/ssl/HttpsURLConnection;->setDoOutput(Z)V

    .line 597
    invoke-virtual {v2, v6}, Ljavax/net/ssl/HttpsURLConnection;->setDoInput(Z)V

    .line 598
    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Ljavax/net/ssl/HttpsURLConnection;->setUseCaches(Z)V

    .line 599
    const-string v6, "Content-Type"

    const-string v7, "application/json; charset=UTF-8"

    invoke-virtual {v2, v6, v7}, Ljavax/net/ssl/HttpsURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 600
    const-string v6, "POST"

    invoke-virtual {v2, v6}, Ljavax/net/ssl/HttpsURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 601
    invoke-virtual {v2}, Ljavax/net/ssl/HttpsURLConnection;->connect()V

    .line 603
    if-eqz v5, :cond_0

    .line 604
    new-instance v6, Ljava/io/OutputStreamWriter;

    invoke-virtual {v2}, Ljavax/net/ssl/HttpsURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    move-object v3, v6

    .line 605
    invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/io/OutputStreamWriter;->write(Ljava/lang/String;)V

    .line 606
    invoke-virtual {v3}, Ljava/io/OutputStreamWriter;->flush()V

    .line 607
    invoke-virtual {v3}, Ljava/io/OutputStreamWriter;->close()V

    .line 610
    :cond_0
    invoke-virtual {v2}, Ljavax/net/ssl/HttpsURLConnection;->getResponseCode()I

    move-result v6

    const/16 v7, 0xc8

    if-ne v6, v7, :cond_3

    .line 611
    new-instance v6, Ljava/io/BufferedReader;

    new-instance v7, Ljava/io/InputStreamReader;

    invoke-virtual {v2}, Ljavax/net/ssl/HttpsURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    const/16 v8, 0x400

    invoke-direct {v6, v7, v8}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V

    move-object v4, v6

    .line 612
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 614
    .local v6, "sb":Ljava/lang/StringBuilder;
    :goto_0
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    .local v8, "line":Ljava/lang/String;
    if-eqz v7, :cond_1

    .line 615
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 619
    :cond_1
    new-instance v7, Lorg/json/JSONObject;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v9}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 621
    .local v7, "result":Lorg/json/JSONObject;
    const-string v9, "code"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v9

    .line 622
    .local v9, "code":I
    if-nez v9, :cond_2

    .line 623
    const-string v10, "data"

    invoke-virtual {v7, v10}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v10

    .line 624
    .local v10, "data":Lorg/json/JSONObject;
    const-string v11, "ret"

    invoke-virtual {v10, v11}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v11

    iput-boolean v11, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mIsConfirm:Z

    .line 625
    .end local v10    # "data":Lorg/json/JSONObject;
    goto :goto_1

    .line 626
    :cond_2
    const-string v10, "confirm request is failed"

    invoke-static {v0, v10}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/net/ProtocolException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 636
    .end local v6    # "sb":Ljava/lang/StringBuilder;
    .end local v7    # "result":Lorg/json/JSONObject;
    .end local v8    # "line":Ljava/lang/String;
    .end local v9    # "code":I
    :cond_3
    :goto_1
    if-eqz v2, :cond_4

    .line 637
    goto :goto_2

    .line 636
    :catchall_0
    move-exception v0

    goto :goto_3

    .line 633
    :catch_0
    move-exception v6

    .line 634
    .local v6, "e":Ljava/lang/Exception;
    :try_start_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "batteryVerifyConfirm "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 636
    nop

    .end local v6    # "e":Ljava/lang/Exception;
    if-eqz v2, :cond_4

    .line 637
    goto :goto_2

    .line 631
    :catch_1
    move-exception v6

    .line 632
    .local v6, "e":Ljava/io/IOException;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "batteryVerifyConfirm IOException "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 636
    nop

    .end local v6    # "e":Ljava/io/IOException;
    if-eqz v2, :cond_4

    .line 637
    goto :goto_2

    .line 629
    :catch_2
    move-exception v6

    .line 630
    .local v6, "e":Ljava/net/ProtocolException;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "batteryVerifyConfirm ProtocolException "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 636
    nop

    .end local v6    # "e":Ljava/net/ProtocolException;
    if-eqz v2, :cond_4

    .line 637
    :goto_2
    invoke-virtual {v2}, Ljavax/net/ssl/HttpsURLConnection;->disconnect()V

    .line 639
    :cond_4
    invoke-direct {p0, v4}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->closeBufferedReader(Ljava/io/BufferedReader;)V

    .line 640
    nop

    .line 641
    return-void

    .line 636
    :goto_3
    if-eqz v2, :cond_5

    .line 637
    invoke-virtual {v2}, Ljavax/net/ssl/HttpsURLConnection;->disconnect()V

    .line 639
    :cond_5
    invoke-direct {p0, v4}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->closeBufferedReader(Ljava/io/BufferedReader;)V

    .line 640
    throw v0
.end method

.method public batteryVerifyInit()V
    .locals 12

    .line 524
    const-string v0, "MiuiBatteryAuthentic"

    const-string v1, "https://battery.mioffice.cn/api/bat/init"

    .line 526
    .local v1, "url":Ljava/lang/String;
    const/4 v2, 0x0

    .line 527
    .local v2, "conn":Ljavax/net/ssl/HttpsURLConnection;
    const/4 v3, 0x0

    .line 528
    .local v3, "writer":Ljava/io/OutputStreamWriter;
    const/4 v4, 0x0

    .line 530
    .local v4, "br":Ljava/io/BufferedReader;
    const/4 v5, 0x1

    invoke-virtual {p0, v5}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->collectData(I)Lorg/json/JSONObject;

    move-result-object v6

    .line 533
    .local v6, "jsonObject":Lorg/json/JSONObject;
    :try_start_0
    new-instance v7, Ljava/net/URL;

    invoke-direct {v7, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v7

    check-cast v7, Ljavax/net/ssl/HttpsURLConnection;

    move-object v2, v7

    .line 534
    const/16 v7, 0x7d0

    invoke-virtual {v2, v7}, Ljavax/net/ssl/HttpsURLConnection;->setConnectTimeout(I)V

    .line 535
    invoke-virtual {v2, v7}, Ljavax/net/ssl/HttpsURLConnection;->setReadTimeout(I)V

    .line 536
    invoke-virtual {v2, v5}, Ljavax/net/ssl/HttpsURLConnection;->setDoOutput(Z)V

    .line 537
    invoke-virtual {v2, v5}, Ljavax/net/ssl/HttpsURLConnection;->setDoInput(Z)V

    .line 538
    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Ljavax/net/ssl/HttpsURLConnection;->setUseCaches(Z)V

    .line 539
    const-string v5, "Content-Type"

    const-string v7, "application/json; charset=UTF-8"

    invoke-virtual {v2, v5, v7}, Ljavax/net/ssl/HttpsURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 540
    const-string v5, "POST"

    invoke-virtual {v2, v5}, Ljavax/net/ssl/HttpsURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 541
    invoke-virtual {v2}, Ljavax/net/ssl/HttpsURLConnection;->connect()V

    .line 543
    if-eqz v6, :cond_0

    .line 544
    new-instance v5, Ljava/io/OutputStreamWriter;

    invoke-virtual {v2}, Ljavax/net/ssl/HttpsURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v7

    invoke-direct {v5, v7}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    move-object v3, v5

    .line 545
    invoke-virtual {v6}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/io/OutputStreamWriter;->write(Ljava/lang/String;)V

    .line 546
    invoke-virtual {v3}, Ljava/io/OutputStreamWriter;->flush()V

    .line 547
    invoke-virtual {v3}, Ljava/io/OutputStreamWriter;->close()V

    .line 550
    :cond_0
    invoke-virtual {v2}, Ljavax/net/ssl/HttpsURLConnection;->getResponseCode()I

    move-result v5

    const/16 v7, 0xc8

    if-ne v5, v7, :cond_3

    .line 551
    new-instance v5, Ljava/io/BufferedReader;

    new-instance v7, Ljava/io/InputStreamReader;

    invoke-virtual {v2}, Ljavax/net/ssl/HttpsURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    const/16 v8, 0x400

    invoke-direct {v5, v7, v8}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V

    move-object v4, v5

    .line 552
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 554
    .local v5, "sb":Ljava/lang/StringBuilder;
    :goto_0
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    .local v8, "line":Ljava/lang/String;
    if-eqz v7, :cond_1

    .line 555
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 559
    :cond_1
    new-instance v7, Lorg/json/JSONObject;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v9}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 561
    .local v7, "result":Lorg/json/JSONObject;
    const-string v9, "code"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v9

    .line 562
    .local v9, "code":I
    if-nez v9, :cond_2

    .line 563
    const-string v10, "data"

    invoke-virtual {v7, v10}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v10

    .line 564
    .local v10, "data":Lorg/json/JSONObject;
    const-string v11, "rk"

    invoke-virtual {v10, v11}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mChallenge:Ljava/lang/String;

    .line 565
    .end local v10    # "data":Lorg/json/JSONObject;
    goto :goto_1

    .line 566
    :cond_2
    const-string v10, "init request is failed"

    invoke-static {v0, v10}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/net/ProtocolException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 576
    .end local v5    # "sb":Ljava/lang/StringBuilder;
    .end local v7    # "result":Lorg/json/JSONObject;
    .end local v8    # "line":Ljava/lang/String;
    .end local v9    # "code":I
    :cond_3
    :goto_1
    if-eqz v2, :cond_4

    .line 577
    goto :goto_2

    .line 576
    :catchall_0
    move-exception v0

    goto :goto_3

    .line 573
    :catch_0
    move-exception v5

    .line 574
    .local v5, "e":Ljava/lang/Exception;
    :try_start_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "batteryVerifyInit "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 576
    nop

    .end local v5    # "e":Ljava/lang/Exception;
    if-eqz v2, :cond_4

    .line 577
    goto :goto_2

    .line 571
    :catch_1
    move-exception v5

    .line 572
    .local v5, "e":Ljava/io/IOException;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "batteryVerifyInit IOException "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 576
    nop

    .end local v5    # "e":Ljava/io/IOException;
    if-eqz v2, :cond_4

    .line 577
    goto :goto_2

    .line 569
    :catch_2
    move-exception v5

    .line 570
    .local v5, "e":Ljava/net/ProtocolException;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "batteryVerifyInit ProtocolException "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 576
    nop

    .end local v5    # "e":Ljava/net/ProtocolException;
    if-eqz v2, :cond_4

    .line 577
    :goto_2
    invoke-virtual {v2}, Ljavax/net/ssl/HttpsURLConnection;->disconnect()V

    .line 579
    :cond_4
    invoke-direct {p0, v4}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->closeBufferedReader(Ljava/io/BufferedReader;)V

    .line 580
    nop

    .line 581
    return-void

    .line 576
    :goto_3
    if-eqz v2, :cond_5

    .line 577
    invoke-virtual {v2}, Ljavax/net/ssl/HttpsURLConnection;->disconnect()V

    .line 579
    :cond_5
    invoke-direct {p0, v4}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->closeBufferedReader(Ljava/io/BufferedReader;)V

    .line 580
    throw v0
.end method

.method public bytesToHexString([B)Ljava/lang/String;
    .locals 6
    .param p1, "src"    # [B

    .line 326
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, ""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 327
    .local v0, "stringBuilder":Ljava/lang/StringBuilder;
    if-eqz p1, :cond_3

    array-length v1, p1

    if-gtz v1, :cond_0

    goto :goto_1

    .line 330
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_2

    .line 331
    aget-byte v2, p1, v1

    and-int/lit16 v2, v2, 0xff

    .line 332
    .local v2, "v":I
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    .line 333
    .local v3, "hv":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x2

    if-ge v4, v5, :cond_1

    .line 334
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 336
    :cond_1
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 330
    .end local v2    # "v":I
    .end local v3    # "hv":Ljava/lang/String;
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 338
    .end local v1    # "i":I
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 328
    :cond_3
    :goto_1
    const/4 v1, 0x0

    return-object v1
.end method

.method public collectData(I)Lorg/json/JSONObject;
    .locals 7
    .param p1, "pushMethod"    # I

    .line 406
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 407
    .local v0, "jsonObject":Lorg/json/JSONObject;
    const/4 v1, 0x0

    .line 409
    .local v1, "BEREncoding":Ljava/lang/String;
    const/4 v2, 0x1

    if-ne p1, v2, :cond_3

    .line 410
    iget-object v3, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->this$0:Lcom/android/server/MiuiBatteryAuthentic;

    iget-object v3, v3, Lcom/android/server/MiuiBatteryAuthentic;->mImei:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->this$0:Lcom/android/server/MiuiBatteryAuthentic;

    iget-object v3, v3, Lcom/android/server/MiuiBatteryAuthentic;->mImei:Ljava/lang/String;

    const-string v4, "1234567890"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 411
    :cond_0
    iget-object v3, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->this$0:Lcom/android/server/MiuiBatteryAuthentic;

    invoke-direct {p0}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->getImei()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/android/server/MiuiBatteryAuthentic;->mImei:Ljava/lang/String;

    .line 413
    :cond_1
    iget-object v3, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->this$0:Lcom/android/server/MiuiBatteryAuthentic;

    iget-object v3, v3, Lcom/android/server/MiuiBatteryAuthentic;->mBatterySn:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->this$0:Lcom/android/server/MiuiBatteryAuthentic;

    iget-object v3, v3, Lcom/android/server/MiuiBatteryAuthentic;->mBatterySn:Ljava/lang/String;

    const-string v4, "111111111111111111111111"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 414
    :cond_2
    iget-object v3, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->this$0:Lcom/android/server/MiuiBatteryAuthentic;

    invoke-virtual {p0}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->getBatterySn()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/android/server/MiuiBatteryAuthentic;->mBatterySn:Ljava/lang/String;

    .line 417
    :cond_3
    iget-object v3, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mAndroidId:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 418
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->getAndroidId()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mAndroidId:Ljava/lang/String;

    .line 420
    :cond_4
    iget-object v3, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mProjectName:Ljava/lang/String;

    if-nez v3, :cond_5

    .line 421
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->getProjectName()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mProjectName:Ljava/lang/String;

    .line 423
    :cond_5
    iget-object v3, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mBatteryAuthentic:Ljava/lang/String;

    if-nez v3, :cond_6

    .line 424
    iget-object v3, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->this$0:Lcom/android/server/MiuiBatteryAuthentic;

    invoke-static {v3}, Lcom/android/server/MiuiBatteryAuthentic;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryAuthentic;)Lmiui/util/IMiCharge;

    move-result-object v3

    const-string v4, "authentic"

    invoke-virtual {v3, v4}, Lmiui/util/IMiCharge;->getMiChargePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mBatteryAuthentic:Ljava/lang/String;

    .line 427
    :cond_6
    if-ne p1, v2, :cond_8

    .line 428
    :try_start_0
    iget-object v3, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->this$0:Lcom/android/server/MiuiBatteryAuthentic;

    iget-object v3, v3, Lcom/android/server/MiuiBatteryAuthentic;->mBatterySn:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->bytesToHexString([B)Ljava/lang/String;

    move-result-object v3

    .line 429
    .local v3, "data":Ljava/lang/String;
    iget-object v4, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->this$0:Lcom/android/server/MiuiBatteryAuthentic;

    iget-object v4, v4, Lcom/android/server/MiuiBatteryAuthentic;->mIMTService:Lcom/android/server/MiuiBatteryAuthentic$IMTService;

    iget-object v5, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->this$0:Lcom/android/server/MiuiBatteryAuthentic;

    iget-object v5, v5, Lcom/android/server/MiuiBatteryAuthentic;->mBatterySn:Ljava/lang/String;

    invoke-direct {p0, v5}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->SHA256(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v2, v5}, Lcom/android/server/MiuiBatteryAuthentic$IMTService;->eccSign(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 430
    .local v2, "sign":Ljava/lang/String;
    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    const/16 v5, 0x40

    if-le v4, v5, :cond_7

    .line 431
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "30440220"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v6, 0x0

    invoke-virtual {v2, v6, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "0220"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 432
    invoke-virtual {v2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v1, v4

    .line 434
    :cond_7
    iget-object v4, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->this$0:Lcom/android/server/MiuiBatteryAuthentic;

    iget-object v4, v4, Lcom/android/server/MiuiBatteryAuthentic;->mIMTService:Lcom/android/server/MiuiBatteryAuthentic$IMTService;

    invoke-virtual {v4}, Lcom/android/server/MiuiBatteryAuthentic$IMTService;->getFid()Ljava/lang/String;

    move-result-object v4

    .line 436
    .local v4, "fid":Ljava/lang/String;
    const-string v5, "fid"

    invoke-virtual {v0, v5, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 437
    const-string/jumbo v5, "sign"

    invoke-virtual {v0, v5, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 438
    const-string v5, "content"

    invoke-virtual {v0, v5, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 439
    nop

    .end local v2    # "sign":Ljava/lang/String;
    .end local v3    # "data":Ljava/lang/String;
    .end local v4    # "fid":Ljava/lang/String;
    goto :goto_0

    .line 453
    :catch_0
    move-exception v2

    goto :goto_1

    .line 451
    :catch_1
    move-exception v2

    goto :goto_2

    .line 440
    :cond_8
    const/4 v2, 0x2

    const-string v3, "rk"

    if-ne p1, v2, :cond_9

    .line 441
    :try_start_1
    iget-object v2, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mChallenge:Ljava/lang/String;

    invoke-virtual {v0, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_0

    .line 442
    :cond_9
    const/4 v2, 0x3

    if-ne p1, v2, :cond_a

    .line 443
    iget-object v2, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mConFirmChallenge:Ljava/lang/String;

    invoke-virtual {v0, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 444
    const-string v2, "hostAuthentication"

    iget-object v3, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mBatteryAuthentic:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 447
    :cond_a
    :goto_0
    const-string v2, "imei"

    iget-object v3, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->this$0:Lcom/android/server/MiuiBatteryAuthentic;

    iget-object v3, v3, Lcom/android/server/MiuiBatteryAuthentic;->mImei:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 448
    const-string/jumbo v2, "sn"

    iget-object v3, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->this$0:Lcom/android/server/MiuiBatteryAuthentic;

    iget-object v3, v3, Lcom/android/server/MiuiBatteryAuthentic;->mBatterySn:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 449
    const-string v2, "androidId"

    iget-object v3, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mAndroidId:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 450
    const-string v2, "project"

    iget-object v3, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mProjectName:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    .line 454
    .local v2, "e":Ljava/lang/Exception;
    :goto_1
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_4

    .line 452
    .local v2, "e":Lorg/json/JSONException;
    :goto_2
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    .line 455
    .end local v2    # "e":Lorg/json/JSONException;
    :goto_3
    nop

    .line 456
    :goto_4
    return-object v0
.end method

.method public getBatterySn()Ljava/lang/String;
    .locals 9

    .line 304
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, ""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 305
    .local v0, "stringBuilder":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->this$0:Lcom/android/server/MiuiBatteryAuthentic;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryAuthentic;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryAuthentic;)Lmiui/util/IMiCharge;

    move-result-object v1

    const-string/jumbo v2, "server_sn"

    invoke-virtual {v1, v2}, Lmiui/util/IMiCharge;->getMiChargePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 307
    .local v1, "str":Ljava/lang/String;
    const/4 v2, 0x0

    :try_start_0
    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 308
    .local v3, "splitString":[Ljava/lang/String;
    array-length v4, v3

    move v5, v2

    :goto_0
    if-ge v5, v4, :cond_0

    aget-object v6, v3, v5

    .line 309
    .local v6, "s":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    const/4 v8, 0x2

    invoke-virtual {v6, v8, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    const/16 v8, 0x10

    invoke-static {v7, v8}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 310
    .local v7, "number":I
    int-to-char v8, v7

    .line 311
    .local v8, "c":C
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 308
    nop

    .end local v6    # "s":Ljava/lang/String;
    .end local v7    # "number":I
    .end local v8    # "c":C
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 315
    .end local v3    # "splitString":[Ljava/lang/String;
    :cond_0
    goto :goto_1

    .line 313
    :catch_0
    move-exception v3

    .line 314
    .local v3, "e":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getBatterySn failed "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "MiuiBatteryAuthentic"

    invoke-static {v5, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    .end local v3    # "e":Ljava/lang/Exception;
    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 317
    .local v3, "result":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_1

    .line 318
    const-string v3, "111111111111111111111111"

    goto :goto_2

    .line 319
    :cond_1
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    const/16 v5, 0x18

    if-lt v4, v5, :cond_2

    .line 320
    invoke-virtual {v3, v2, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 322
    :cond_2
    :goto_2
    return-object v3
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 9
    .param p1, "msg"    # Landroid/os/Message;

    .line 174
    iget v0, p1, Landroid/os/Message;->what:I

    const-wide/16 v1, 0x1388

    const/4 v3, 0x3

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    packed-switch v0, :pswitch_data_0

    .line 239
    const-string v0, "MiuiBatteryAuthentic"

    const-string v1, "No Message"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 228
    :pswitch_0
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-ne v0, v7, :cond_0

    move v6, v7

    :cond_0
    move v0, v6

    .line 229
    .local v0, "success":Z
    const/4 v4, 0x0

    .line 230
    .local v4, "callResult":Z
    const-string/jumbo v5, "server_result"

    if-eqz v0, :cond_1

    .line 231
    iget-object v6, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->this$0:Lcom/android/server/MiuiBatteryAuthentic;

    invoke-static {v6}, Lcom/android/server/MiuiBatteryAuthentic;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryAuthentic;)Lmiui/util/IMiCharge;

    move-result-object v6

    const-string v7, "1"

    invoke-virtual {v6, v5, v7}, Lmiui/util/IMiCharge;->setMiChargePath(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    goto :goto_0

    .line 233
    :cond_1
    iget-object v6, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->this$0:Lcom/android/server/MiuiBatteryAuthentic;

    invoke-static {v6}, Lcom/android/server/MiuiBatteryAuthentic;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryAuthentic;)Lmiui/util/IMiCharge;

    move-result-object v6

    const-string v7, "0"

    invoke-virtual {v6, v5, v7}, Lmiui/util/IMiCharge;->setMiChargePath(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    .line 235
    :goto_0
    if-nez v4, :cond_b

    .line 236
    invoke-virtual {p0, v3, v0, v1, v2}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->sendMessageDelayed(IZJ)V

    goto/16 :goto_1

    .line 214
    .end local v0    # "success":Z
    .end local v4    # "callResult":Z
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->this$0:Lcom/android/server/MiuiBatteryAuthentic;

    iget-object v0, v0, Lcom/android/server/MiuiBatteryAuthentic;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->isNetworkConnected(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->isDeviceProvisioned()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 215
    invoke-virtual {p0}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->batteryVerifyConfirm()V

    .line 217
    :cond_2
    iget-boolean v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mIsConfirm:Z

    if-nez v0, :cond_3

    iget v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mConfirmResultCount:I

    const/16 v1, 0x3c

    if-ge v0, v1, :cond_3

    .line 218
    add-int/2addr v0, v7

    iput v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mConfirmResultCount:I

    .line 219
    const/4 v0, 0x2

    const-wide/32 v1, 0xea60

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->sendMessageDelayed(IJ)V

    goto/16 :goto_1

    .line 221
    :cond_3
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->verifyECDSA()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 222
    iget-object v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->this$0:Lcom/android/server/MiuiBatteryAuthentic;

    iget-object v0, v0, Lcom/android/server/MiuiBatteryAuthentic;->mContentResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->this$0:Lcom/android/server/MiuiBatteryAuthentic;

    iget-object v1, v1, Lcom/android/server/MiuiBatteryAuthentic;->mCloudSign:Ljava/lang/String;

    const-string v2, "battery_authentic_certificate"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 224
    :cond_4
    iput v6, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mConfirmResultCount:I

    .line 226
    goto/16 :goto_1

    .line 196
    :pswitch_2
    iget-object v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->this$0:Lcom/android/server/MiuiBatteryAuthentic;

    iget-object v0, v0, Lcom/android/server/MiuiBatteryAuthentic;->mImei:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 197
    iget-object v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->this$0:Lcom/android/server/MiuiBatteryAuthentic;

    invoke-direct {p0}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->getImei()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/server/MiuiBatteryAuthentic;->mImei:Ljava/lang/String;

    .line 199
    :cond_5
    iget-object v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->this$0:Lcom/android/server/MiuiBatteryAuthentic;

    iget-object v0, v0, Lcom/android/server/MiuiBatteryAuthentic;->mBatterySn:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 200
    iget-object v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->this$0:Lcom/android/server/MiuiBatteryAuthentic;

    invoke-virtual {p0}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->getBatterySn()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/server/MiuiBatteryAuthentic;->mBatterySn:Ljava/lang/String;

    .line 202
    :cond_6
    iget-object v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mAndroidId:Ljava/lang/String;

    if-nez v0, :cond_7

    .line 203
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->getAndroidId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mAndroidId:Ljava/lang/String;

    .line 205
    :cond_7
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->verifyECDSA()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 206
    iget-object v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->this$0:Lcom/android/server/MiuiBatteryAuthentic;

    iput-boolean v7, v0, Lcom/android/server/MiuiBatteryAuthentic;->mIsVerified:Z

    .line 207
    invoke-virtual {p0, v3, v7, v4, v5}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->sendMessageDelayed(IZJ)V

    goto :goto_1

    .line 208
    :cond_8
    iget-object v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->this$0:Lcom/android/server/MiuiBatteryAuthentic;

    iget-boolean v0, v0, Lcom/android/server/MiuiBatteryAuthentic;->mIsVerified:Z

    if-nez v0, :cond_b

    .line 209
    iget-object v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->this$0:Lcom/android/server/MiuiBatteryAuthentic;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/android/server/MiuiBatteryAuthentic;->mCloudSign:Ljava/lang/String;

    .line 210
    invoke-virtual {p0, v6, v4, v5}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->sendMessageDelayed(IJ)V

    goto :goto_1

    .line 176
    :pswitch_3
    iget-object v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->this$0:Lcom/android/server/MiuiBatteryAuthentic;

    iget-object v0, v0, Lcom/android/server/MiuiBatteryAuthentic;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->isNetworkConnected(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-virtual {p0}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->isDeviceProvisioned()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 177
    iget-object v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->this$0:Lcom/android/server/MiuiBatteryAuthentic;

    iget-object v0, v0, Lcom/android/server/MiuiBatteryAuthentic;->mBatterySn:Ljava/lang/String;

    const-string v8, "111111111111111111111111"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mTryGetBatteryCount:I

    const/16 v8, 0xa

    if-ge v0, v8, :cond_9

    .line 178
    invoke-virtual {p0, v6, v1, v2}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->sendMessageDelayed(IJ)V

    .line 179
    iget-object v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->this$0:Lcom/android/server/MiuiBatteryAuthentic;

    invoke-virtual {p0}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->getBatterySn()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/server/MiuiBatteryAuthentic;->mBatterySn:Ljava/lang/String;

    .line 180
    iget v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mTryGetBatteryCount:I

    add-int/2addr v0, v7

    iput v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mTryGetBatteryCount:I

    .line 181
    return-void

    .line 183
    :cond_9
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->verifyFromServer()Z

    move-result v0

    if-nez v0, :cond_a

    iget v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mGetFromServerCount:I

    const/4 v1, 0x4

    if-ge v0, v1, :cond_a

    .line 184
    invoke-virtual {p0, v6, v4, v5}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->sendMessageDelayed(IJ)V

    .line 185
    iget v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mGetFromServerCount:I

    add-int/2addr v0, v7

    iput v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mGetFromServerCount:I

    goto :goto_1

    .line 188
    :cond_a
    iget-object v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->this$0:Lcom/android/server/MiuiBatteryAuthentic;

    iput-boolean v7, v0, Lcom/android/server/MiuiBatteryAuthentic;->mIsVerified:Z

    .line 189
    iget-boolean v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->mIsGetServerInfo:Z

    if-eqz v0, :cond_b

    .line 190
    invoke-virtual {p0, v3, v6, v4, v5}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->sendMessageDelayed(IZJ)V

    .line 241
    :cond_b
    :goto_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public hexStringToByte(Ljava/lang/String;)[B
    .locals 7
    .param p1, "hex"    # Ljava/lang/String;

    .line 342
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    .line 343
    .local v0, "len":I
    new-array v1, v0, [B

    .line 344
    .local v1, "result":[B
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    .line 345
    .local v2, "achar":[C
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v0, :cond_0

    .line 346
    mul-int/lit8 v4, v3, 0x2

    .line 347
    .local v4, "pos":I
    aget-char v5, v2, v4

    invoke-direct {p0, v5}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->toByte(C)B

    move-result v5

    shl-int/lit8 v5, v5, 0x4

    add-int/lit8 v6, v4, 0x1

    aget-char v6, v2, v6

    invoke-direct {p0, v6}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->toByte(C)B

    move-result v6

    or-int/2addr v5, v6

    int-to-byte v5, v5

    aput-byte v5, v1, v3

    .line 345
    .end local v4    # "pos":I
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 349
    .end local v3    # "i":I
    :cond_0
    return-object v1
.end method

.method public isDeviceProvisioned()Z
    .locals 3

    .line 370
    iget-object v0, p0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->this$0:Lcom/android/server/MiuiBatteryAuthentic;

    iget-object v0, v0, Lcom/android/server/MiuiBatteryAuthentic;->mContentResolver:Landroid/content/ContentResolver;

    const-string v1, "device_provisioned"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    :cond_0
    return v2
.end method

.method public isNetworkConnected(Landroid/content/Context;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 374
    const/4 v0, 0x0

    .line 376
    .local v0, "isAvailable":Z
    :try_start_0
    const-string v1, "connectivity"

    .line 377
    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 378
    .local v1, "connectivityManager":Landroid/net/ConnectivityManager;
    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    .line 379
    .local v2, "networkInfo":Landroid/net/NetworkInfo;
    if-eqz v2, :cond_0

    .line 380
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v3

    .line 384
    .end local v1    # "connectivityManager":Landroid/net/ConnectivityManager;
    .end local v2    # "networkInfo":Landroid/net/NetworkInfo;
    :cond_0
    goto :goto_0

    .line 382
    :catch_0
    move-exception v1

    .line 383
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 385
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return v0
.end method

.method public sendMessageDelayed(IJ)V
    .locals 1
    .param p1, "what"    # I
    .param p2, "delayMillis"    # J

    .line 160
    invoke-virtual {p0, p1}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->removeMessages(I)V

    .line 161
    invoke-static {p0, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 162
    .local v0, "m":Landroid/os/Message;
    invoke-virtual {p0, v0, p2, p3}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 163
    return-void
.end method

.method public sendMessageDelayed(IZJ)V
    .locals 1
    .param p1, "what"    # I
    .param p2, "arg"    # Z
    .param p3, "delayMillis"    # J

    .line 166
    invoke-virtual {p0, p1}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->removeMessages(I)V

    .line 167
    invoke-static {p0, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 168
    .local v0, "m":Landroid/os/Message;
    iput p2, v0, Landroid/os/Message;->arg1:I

    .line 169
    invoke-virtual {p0, v0, p3, p4}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 170
    return-void
.end method
