public class com.android.server.usage.UsageStatsServiceImpl implements com.android.server.usage.UsageStatsServiceStub {
	 /* .source "UsageStatsServiceImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final Integer LIMIT_COUNT;
	 private static final com.android.server.utils.quota.Category QUOTA_CATEGORIES;
	 private static final Integer QUOTA_CATEGORY_NUM;
	 private static final java.lang.String QUOTA_CATEGORY_TAGS;
	 private static final java.util.HashMap QUOTA_CATEGORY_TAGS_MAP;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/HashMap<", */
	 /* "Ljava/lang/String;", */
	 /* "Ljava/lang/Integer;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private static final Integer SHRINK_THRESHOLD;
private static final java.lang.String TAG;
private static final Integer WINDOW_MS;
/* # instance fields */
private Boolean hasInit;
private com.android.server.utils.quota.CountQuotaTracker mQuotaTracker;
/* # direct methods */
static com.android.server.usage.UsageStatsServiceImpl ( ) {
/* .locals 2 */
/* .line 23 */
/* const-class v0, Lcom/android/server/usage/UsageStatsServiceImpl; */
(( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
/* .line 40 */
/* const/16 v0, 0x26 */
/* new-array v1, v0, [Ljava/lang/String; */
/* .line 41 */
/* new-array v1, v0, [Lcom/android/server/utils/quota/Category; */
/* .line 42 */
/* new-instance v1, Ljava/util/HashMap; */
/* invoke-direct {v1, v0}, Ljava/util/HashMap;-><init>(I)V */
return;
} // .end method
public com.android.server.usage.UsageStatsServiceImpl ( ) {
/* .locals 1 */
/* .line 21 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 46 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/usage/UsageStatsServiceImpl;->hasInit:Z */
return;
} // .end method
static com.android.server.utils.quota.Category lambda$init$0 ( Integer p0, java.lang.String p1, java.lang.String p2 ) { //synthethic
/* .locals 2 */
/* .param p0, "userId" # I */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "tag" # Ljava/lang/String; */
/* .line 57 */
v0 = com.android.server.usage.UsageStatsServiceImpl.QUOTA_CATEGORY_TAGS_MAP;
(( java.util.HashMap ) v0 ).get ( p2 ); // invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/Integer; */
v0 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
/* .line 58 */
/* .local v0, "catIdx":I */
v1 = com.android.server.usage.UsageStatsServiceImpl.QUOTA_CATEGORIES;
/* aget-object v1, v1, v0 */
} // .end method
/* # virtual methods */
public void dumpCountQuotaTracker ( com.android.internal.util.IndentingPrintWriter p0 ) {
/* .locals 2 */
/* .param p1, "idpw" # Lcom/android/internal/util/IndentingPrintWriter; */
/* .line 82 */
/* iget-boolean v0, p0, Lcom/android/server/usage/UsageStatsServiceImpl;->hasInit:Z */
/* if-nez v0, :cond_0 */
/* .line 83 */
v0 = com.android.server.usage.UsageStatsServiceImpl.TAG;
final String v1 = "UsageStatsServiceImpl not init, so can\'t dumpCountQuotaTracker."; // const-string v1, "UsageStatsServiceImpl not init, so can\'t dumpCountQuotaTracker."
android.util.Slog .w ( v0,v1 );
/* .line 84 */
return;
/* .line 87 */
} // :cond_0
(( com.android.internal.util.IndentingPrintWriter ) p1 ).println ( ); // invoke-virtual {p1}, Lcom/android/internal/util/IndentingPrintWriter;->println()V
/* .line 88 */
v0 = this.mQuotaTracker;
(( com.android.server.utils.quota.CountQuotaTracker ) v0 ).dump ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/utils/quota/CountQuotaTracker;->dump(Lcom/android/internal/util/IndentingPrintWriter;)V
/* .line 89 */
return;
} // .end method
public void init ( android.content.Context p0 ) {
/* .locals 8 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 51 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
/* const/16 v1, 0x26 */
/* if-ge v0, v1, :cond_0 */
/* .line 52 */
v1 = com.android.server.usage.UsageStatsServiceImpl.QUOTA_CATEGORY_TAGS;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "UsageEvent_Quota_Category_"; // const-string v3, "UsageEvent_Quota_Category_"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* aput-object v2, v1, v0 */
/* .line 53 */
v2 = com.android.server.usage.UsageStatsServiceImpl.QUOTA_CATEGORIES;
/* new-instance v3, Lcom/android/server/utils/quota/Category; */
/* aget-object v4, v1, v0 */
/* invoke-direct {v3, v4}, Lcom/android/server/utils/quota/Category;-><init>(Ljava/lang/String;)V */
/* aput-object v3, v2, v0 */
/* .line 54 */
v2 = com.android.server.usage.UsageStatsServiceImpl.QUOTA_CATEGORY_TAGS_MAP;
/* aget-object v1, v1, v0 */
java.lang.Integer .valueOf ( v0 );
(( java.util.HashMap ) v2 ).put ( v1, v3 ); // invoke-virtual {v2, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 51 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 56 */
} // .end local v0 # "i":I
} // :cond_0
/* new-instance v0, Lcom/android/server/usage/UsageStatsServiceImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v0}, Lcom/android/server/usage/UsageStatsServiceImpl$$ExternalSyntheticLambda0;-><init>()V */
/* .line 60 */
/* .local v0, "quotaCategorizer":Lcom/android/server/utils/quota/Categorizer; */
/* new-instance v2, Lcom/android/server/utils/quota/CountQuotaTracker; */
/* invoke-direct {v2, p1, v0}, Lcom/android/server/utils/quota/CountQuotaTracker;-><init>(Landroid/content/Context;Lcom/android/server/utils/quota/Categorizer;)V */
this.mQuotaTracker = v2;
/* .line 61 */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_1
/* if-ge v2, v1, :cond_1 */
/* .line 62 */
v3 = this.mQuotaTracker;
v4 = com.android.server.usage.UsageStatsServiceImpl.QUOTA_CATEGORIES;
/* aget-object v4, v4, v2 */
/* const/16 v5, 0x2710 */
/* const-wide/32 v6, 0x5265c00 */
(( com.android.server.utils.quota.CountQuotaTracker ) v3 ).setCountLimit ( v4, v5, v6, v7 ); // invoke-virtual {v3, v4, v5, v6, v7}, Lcom/android/server/utils/quota/CountQuotaTracker;->setCountLimit(Lcom/android/server/utils/quota/Category;IJ)V
/* .line 61 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 64 */
} // .end local v2 # "i":I
} // :cond_1
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/android/server/usage/UsageStatsServiceImpl;->hasInit:Z */
/* .line 65 */
return;
} // .end method
public Boolean isEventInQuota ( android.app.usage.UsageEvents$Event p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "event" # Landroid/app/usage/UsageEvents$Event; */
/* .param p2, "userId" # I */
/* .line 69 */
/* iget-boolean v0, p0, Lcom/android/server/usage/UsageStatsServiceImpl;->hasInit:Z */
/* if-nez v0, :cond_0 */
/* .line 70 */
v0 = com.android.server.usage.UsageStatsServiceImpl.TAG;
/* new-instance v1, Ljava/lang/Throwable; */
/* invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V */
final String v2 = "UsageStatsServiceImpl not init, so can\'t do isEventInQuota."; // const-string v2, "UsageStatsServiceImpl not init, so can\'t do isEventInQuota."
android.util.Slog .w ( v0,v2,v1 );
/* .line 71 */
int v0 = 1; // const/4 v0, 0x1
/* .line 74 */
} // :cond_0
/* iget v0, p1, Landroid/app/usage/UsageEvents$Event;->mEventType:I */
/* .line 75 */
/* .local v0, "et":I */
v1 = this.mPackage;
/* .line 76 */
/* .local v1, "pkg":Ljava/lang/String; */
v2 = com.android.server.usage.UsageStatsServiceImpl.QUOTA_CATEGORY_TAGS;
/* aget-object v2, v2, v0 */
/* .line 77 */
/* .local v2, "tag":Ljava/lang/String; */
v3 = this.mQuotaTracker;
v3 = (( com.android.server.utils.quota.CountQuotaTracker ) v3 ).noteEvent ( p2, v1, v2 ); // invoke-virtual {v3, p2, v1, v2}, Lcom/android/server/utils/quota/CountQuotaTracker;->noteEvent(ILjava/lang/String;Ljava/lang/String;)Z
} // .end method
public Boolean isLimitExceed ( java.util.List p0 ) {
/* .locals 6 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Landroid/app/usage/UsageEvents$Event;", */
/* ">;)Z" */
/* } */
} // .end annotation
/* .line 93 */
v0 = /* .local p1, "events":Ljava/util/List;, "Ljava/util/List<Landroid/app/usage/UsageEvents$Event;>;" */
/* const v1, 0x186a0 */
/* if-ge v0, v1, :cond_0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 95 */
} // :cond_0
v0 = com.android.server.usage.UsageStatsServiceImpl.TAG;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Events exceed 100000! Reserved event end at timeStamp: "; // const-string v2, "Events exceed 100000! Reserved event end at timeStamp: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = /* .line 96 */
int v3 = 1; // const/4 v3, 0x1
/* sub-int/2addr v2, v3 */
/* check-cast v2, Landroid/app/usage/UsageEvents$Event; */
(( android.app.usage.UsageEvents$Event ) v2 ).getTimeStamp ( ); // invoke-virtual {v2}, Landroid/app/usage/UsageEvents$Event;->getTimeStamp()J
/* move-result-wide v4 */
(( java.lang.StringBuilder ) v1 ).append ( v4, v5 ); // invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v2 = " to avoid OOM."; // const-string v2, " to avoid OOM."
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 95 */
android.util.Slog .w ( v0,v1 );
/* .line 97 */
} // .end method
