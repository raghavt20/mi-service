.class public Lcom/android/server/usage/UsageStatsServiceImpl;
.super Ljava/lang/Object;
.source "UsageStatsServiceImpl.java"

# interfaces
.implements Lcom/android/server/usage/UsageStatsServiceStub;


# static fields
.field private static final LIMIT_COUNT:I = 0x2710

.field private static final QUOTA_CATEGORIES:[Lcom/android/server/utils/quota/Category;

.field private static final QUOTA_CATEGORY_NUM:I = 0x26

.field private static final QUOTA_CATEGORY_TAGS:[Ljava/lang/String;

.field private static final QUOTA_CATEGORY_TAGS_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final SHRINK_THRESHOLD:I = 0x186a0

.field private static final TAG:Ljava/lang/String;

.field private static final WINDOW_MS:I = 0x5265c00


# instance fields
.field private hasInit:Z

.field private mQuotaTracker:Lcom/android/server/utils/quota/CountQuotaTracker;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 23
    const-class v0, Lcom/android/server/usage/UsageStatsServiceImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/usage/UsageStatsServiceImpl;->TAG:Ljava/lang/String;

    .line 40
    const/16 v0, 0x26

    new-array v1, v0, [Ljava/lang/String;

    sput-object v1, Lcom/android/server/usage/UsageStatsServiceImpl;->QUOTA_CATEGORY_TAGS:[Ljava/lang/String;

    .line 41
    new-array v1, v0, [Lcom/android/server/utils/quota/Category;

    sput-object v1, Lcom/android/server/usage/UsageStatsServiceImpl;->QUOTA_CATEGORIES:[Lcom/android/server/utils/quota/Category;

    .line 42
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1, v0}, Ljava/util/HashMap;-><init>(I)V

    sput-object v1, Lcom/android/server/usage/UsageStatsServiceImpl;->QUOTA_CATEGORY_TAGS_MAP:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/usage/UsageStatsServiceImpl;->hasInit:Z

    return-void
.end method

.method static synthetic lambda$init$0(ILjava/lang/String;Ljava/lang/String;)Lcom/android/server/utils/quota/Category;
    .locals 2
    .param p0, "userId"    # I
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "tag"    # Ljava/lang/String;

    .line 57
    sget-object v0, Lcom/android/server/usage/UsageStatsServiceImpl;->QUOTA_CATEGORY_TAGS_MAP:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 58
    .local v0, "catIdx":I
    sget-object v1, Lcom/android/server/usage/UsageStatsServiceImpl;->QUOTA_CATEGORIES:[Lcom/android/server/utils/quota/Category;

    aget-object v1, v1, v0

    return-object v1
.end method


# virtual methods
.method public dumpCountQuotaTracker(Lcom/android/internal/util/IndentingPrintWriter;)V
    .locals 2
    .param p1, "idpw"    # Lcom/android/internal/util/IndentingPrintWriter;

    .line 82
    iget-boolean v0, p0, Lcom/android/server/usage/UsageStatsServiceImpl;->hasInit:Z

    if-nez v0, :cond_0

    .line 83
    sget-object v0, Lcom/android/server/usage/UsageStatsServiceImpl;->TAG:Ljava/lang/String;

    const-string v1, "UsageStatsServiceImpl not init, so can\'t dumpCountQuotaTracker."

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    return-void

    .line 87
    :cond_0
    invoke-virtual {p1}, Lcom/android/internal/util/IndentingPrintWriter;->println()V

    .line 88
    iget-object v0, p0, Lcom/android/server/usage/UsageStatsServiceImpl;->mQuotaTracker:Lcom/android/server/utils/quota/CountQuotaTracker;

    invoke-virtual {v0, p1}, Lcom/android/server/utils/quota/CountQuotaTracker;->dump(Lcom/android/internal/util/IndentingPrintWriter;)V

    .line 89
    return-void
.end method

.method public init(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .line 51
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0x26

    if-ge v0, v1, :cond_0

    .line 52
    sget-object v1, Lcom/android/server/usage/UsageStatsServiceImpl;->QUOTA_CATEGORY_TAGS:[Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "UsageEvent_Quota_Category_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 53
    sget-object v2, Lcom/android/server/usage/UsageStatsServiceImpl;->QUOTA_CATEGORIES:[Lcom/android/server/utils/quota/Category;

    new-instance v3, Lcom/android/server/utils/quota/Category;

    aget-object v4, v1, v0

    invoke-direct {v3, v4}, Lcom/android/server/utils/quota/Category;-><init>(Ljava/lang/String;)V

    aput-object v3, v2, v0

    .line 54
    sget-object v2, Lcom/android/server/usage/UsageStatsServiceImpl;->QUOTA_CATEGORY_TAGS_MAP:Ljava/util/HashMap;

    aget-object v1, v1, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 56
    .end local v0    # "i":I
    :cond_0
    new-instance v0, Lcom/android/server/usage/UsageStatsServiceImpl$$ExternalSyntheticLambda0;

    invoke-direct {v0}, Lcom/android/server/usage/UsageStatsServiceImpl$$ExternalSyntheticLambda0;-><init>()V

    .line 60
    .local v0, "quotaCategorizer":Lcom/android/server/utils/quota/Categorizer;
    new-instance v2, Lcom/android/server/utils/quota/CountQuotaTracker;

    invoke-direct {v2, p1, v0}, Lcom/android/server/utils/quota/CountQuotaTracker;-><init>(Landroid/content/Context;Lcom/android/server/utils/quota/Categorizer;)V

    iput-object v2, p0, Lcom/android/server/usage/UsageStatsServiceImpl;->mQuotaTracker:Lcom/android/server/utils/quota/CountQuotaTracker;

    .line 61
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v1, :cond_1

    .line 62
    iget-object v3, p0, Lcom/android/server/usage/UsageStatsServiceImpl;->mQuotaTracker:Lcom/android/server/utils/quota/CountQuotaTracker;

    sget-object v4, Lcom/android/server/usage/UsageStatsServiceImpl;->QUOTA_CATEGORIES:[Lcom/android/server/utils/quota/Category;

    aget-object v4, v4, v2

    const/16 v5, 0x2710

    const-wide/32 v6, 0x5265c00

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/android/server/utils/quota/CountQuotaTracker;->setCountLimit(Lcom/android/server/utils/quota/Category;IJ)V

    .line 61
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 64
    .end local v2    # "i":I
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/server/usage/UsageStatsServiceImpl;->hasInit:Z

    .line 65
    return-void
.end method

.method public isEventInQuota(Landroid/app/usage/UsageEvents$Event;I)Z
    .locals 4
    .param p1, "event"    # Landroid/app/usage/UsageEvents$Event;
    .param p2, "userId"    # I

    .line 69
    iget-boolean v0, p0, Lcom/android/server/usage/UsageStatsServiceImpl;->hasInit:Z

    if-nez v0, :cond_0

    .line 70
    sget-object v0, Lcom/android/server/usage/UsageStatsServiceImpl;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/Throwable;

    invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V

    const-string v2, "UsageStatsServiceImpl not init, so can\'t do isEventInQuota."

    invoke-static {v0, v2, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 71
    const/4 v0, 0x1

    return v0

    .line 74
    :cond_0
    iget v0, p1, Landroid/app/usage/UsageEvents$Event;->mEventType:I

    .line 75
    .local v0, "et":I
    iget-object v1, p1, Landroid/app/usage/UsageEvents$Event;->mPackage:Ljava/lang/String;

    .line 76
    .local v1, "pkg":Ljava/lang/String;
    sget-object v2, Lcom/android/server/usage/UsageStatsServiceImpl;->QUOTA_CATEGORY_TAGS:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 77
    .local v2, "tag":Ljava/lang/String;
    iget-object v3, p0, Lcom/android/server/usage/UsageStatsServiceImpl;->mQuotaTracker:Lcom/android/server/utils/quota/CountQuotaTracker;

    invoke-virtual {v3, p2, v1, v2}, Lcom/android/server/utils/quota/CountQuotaTracker;->noteEvent(ILjava/lang/String;Ljava/lang/String;)Z

    move-result v3

    return v3
.end method

.method public isLimitExceed(Ljava/util/List;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/app/usage/UsageEvents$Event;",
            ">;)Z"
        }
    .end annotation

    .line 93
    .local p1, "events":Ljava/util/List;, "Ljava/util/List<Landroid/app/usage/UsageEvents$Event;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const v1, 0x186a0

    if-ge v0, v1, :cond_0

    const/4 v0, 0x0

    return v0

    .line 95
    :cond_0
    sget-object v0, Lcom/android/server/usage/UsageStatsServiceImpl;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Events exceed 100000! Reserved event end at timeStamp: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 96
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    sub-int/2addr v2, v3

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/usage/UsageEvents$Event;

    invoke-virtual {v2}, Landroid/app/usage/UsageEvents$Event;->getTimeStamp()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to avoid OOM."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 95
    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    return v3
.end method
