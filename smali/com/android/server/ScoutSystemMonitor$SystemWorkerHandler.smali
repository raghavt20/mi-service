.class final Lcom/android/server/ScoutSystemMonitor$SystemWorkerHandler;
.super Landroid/os/Handler;
.source "ScoutSystemMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/ScoutSystemMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SystemWorkerHandler"
.end annotation


# static fields
.field static final FW_SCOUT_BINDER_FULL:I = 0x1

.field static final FW_SCOUT_HANG:I = 0x0

.field static final FW_SCOUT_MEM_CHECK:I = 0xa

.field static final FW_SCOUT_MEM_CRITICAL:I = 0xc

.field static final FW_SCOUT_MEM_DUMP:I = 0xb

.field static final FW_SCOUT_NORMALLY:I = 0x3

.field static final FW_SCOUT_SLOW:I = 0x2


# instance fields
.field final synthetic this$0:Lcom/android/server/ScoutSystemMonitor;


# direct methods
.method public constructor <init>(Lcom/android/server/ScoutSystemMonitor;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 440
    iput-object p1, p0, Lcom/android/server/ScoutSystemMonitor$SystemWorkerHandler;->this$0:Lcom/android/server/ScoutSystemMonitor;

    .line 441
    const/4 p1, 0x0

    invoke-direct {p0, p2, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    .line 442
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 20
    .param p1, "msg"    # Landroid/os/Message;

    .line 446
    move-object/from16 v0, p1

    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x5

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 447
    .local v1, "pids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    move-object v2, v3

    .line 448
    .local v2, "nativePids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v3, Lcom/android/server/ScoutHelper$ScoutBinderInfo;

    .line 449
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v4

    const-string v5, "MIUIScout System"

    const/4 v6, 0x0

    invoke-direct {v3, v4, v6, v5}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;-><init>(IILjava/lang/String;)V

    move-object v12, v3

    .line 451
    .local v12, "scoutBinderInfo":Lcom/android/server/ScoutHelper$ScoutBinderInfo;
    const/4 v13, 0x0

    .line 454
    .local v13, "waitedHalf":Z
    const/4 v14, 0x0

    .line 457
    .local v14, "mScoutStack":Ljava/io/File;
    iget v3, v0, Landroid/os/Message;->what:I

    const/4 v4, 0x2

    const-string v15, "ScoutSystemMonitor"

    const-string v5, "\n"

    sparse-switch v3, :sswitch_data_0

    .line 501
    move/from16 v17, v13

    .end local v13    # "waitedHalf":Z
    .local v17, "waitedHalf":Z
    const-string v3, "    // wrong message received of WorkerHandler"

    invoke-static {v15, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 498
    .end local v17    # "waitedHalf":Z
    .restart local v13    # "waitedHalf":Z
    :sswitch_0
    invoke-static {}, Lcom/android/server/am/AppProfilerStub;->getInstance()Lcom/android/server/am/AppProfilerStub;

    move-result-object v3

    invoke-interface {v3, v6}, Lcom/android/server/am/AppProfilerStub;->checkMemoryPsi(Z)V

    .line 499
    move/from16 v17, v13

    goto/16 :goto_2

    .line 495
    :sswitch_1
    invoke-static {}, Lcom/android/server/am/AppProfilerStub;->getInstance()Lcom/android/server/am/AppProfilerStub;

    move-result-object v3

    invoke-interface {v3}, Lcom/android/server/am/AppProfilerStub;->dumpProcsMemInfo()V

    .line 496
    move/from16 v17, v13

    goto/16 :goto_2

    .line 492
    :sswitch_2
    invoke-static {}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->getInstance()Lcom/miui/server/stability/ScoutDisplayMemoryManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->checkScoutLowMemory()V

    .line 493
    move/from16 v17, v13

    goto/16 :goto_2

    .line 476
    :sswitch_3
    iget-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object v11, v3

    check-cast v11, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;

    .line 477
    .local v11, "mInfo":Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;
    invoke-virtual {v11}, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->getHalfState()Z

    move-result v16

    .line 478
    .local v16, "mIsHalf":Z
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 479
    new-instance v3, Ljava/util/TreeMap;

    invoke-direct {v3}, Ljava/util/TreeMap;-><init>()V

    move-object v10, v3

    .line 480
    .local v10, "inPidMap":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    invoke-static {v3, v12, v1, v2}, Lcom/android/server/ScoutHelper;->checkBinderCallPidList(ILcom/android/server/ScoutHelper$ScoutBinderInfo;Ljava/util/ArrayList;Ljava/util/ArrayList;)Z

    .line 481
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    invoke-static {v3, v12, v10, v1, v2}, Lcom/android/server/ScoutHelper;->checkBinderThreadFull(ILcom/android/server/ScoutHelper$ScoutBinderInfo;Ljava/util/TreeMap;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 482
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->getBinderTransInfo()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 483
    invoke-virtual {v12}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->getProcInfo()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 482
    invoke-virtual {v11, v3}, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->setBinderTransInfo(Ljava/lang/String;)V

    .line 484
    if-eqz v16, :cond_1

    invoke-virtual {v11}, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->getScoutLevel()I

    move-result v3

    if-gt v3, v4, :cond_0

    goto :goto_0

    :cond_0
    move-object/from16 v19, v10

    move/from16 v17, v13

    move-object v13, v11

    goto :goto_1

    .line 485
    :cond_1
    :goto_0
    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 486
    invoke-static {v2}, Ljava/util/concurrent/CompletableFuture;->completedFuture(Ljava/lang/Object;)Ljava/util/concurrent/CompletableFuture;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v11}, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    .line 485
    move-object v3, v1

    move-object/from16 v19, v10

    .end local v10    # "inPidMap":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    .local v19, "inPidMap":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    move-object/from16 v10, v17

    move/from16 v17, v13

    move-object v13, v11

    .end local v11    # "mInfo":Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;
    .local v13, "mInfo":Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;
    .restart local v17    # "waitedHalf":Z
    move-object/from16 v11, v18

    invoke-static/range {v3 .. v11}, Lcom/android/server/am/StackTracesDumpHelper;->dumpStackTraces(Ljava/util/ArrayList;Lcom/android/internal/os/ProcessCpuTracker;Landroid/util/SparseBooleanArray;Ljava/util/concurrent/Future;Ljava/io/StringWriter;Ljava/lang/String;Ljava/lang/String;Ljava/util/concurrent/Executor;Lcom/android/internal/os/anr/AnrLatencyTracker;)Ljava/io/File;

    move-result-object v14

    .line 488
    :goto_1
    move-object/from16 v3, v19

    .end local v19    # "inPidMap":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    .local v3, "inPidMap":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    invoke-static {v15, v3}, Lcom/android/server/ScoutHelper;->resumeBinderThreadFull(Ljava/lang/String;Ljava/util/TreeMap;)Ljava/lang/String;

    move-result-object v4

    .line 489
    .local v4, "mOtherMsg":Ljava/lang/String;
    const/16 v5, 0x191

    invoke-static {v5, v14, v13, v4}, Lcom/android/server/ScoutSystemMonitor;->onFwScout(ILjava/io/File;Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;Ljava/lang/String;)V

    .line 490
    goto :goto_2

    .line 459
    .end local v3    # "inPidMap":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    .end local v4    # "mOtherMsg":Ljava/lang/String;
    .end local v16    # "mIsHalf":Z
    .end local v17    # "waitedHalf":Z
    .local v13, "waitedHalf":Z
    :sswitch_4
    move/from16 v17, v13

    .end local v13    # "waitedHalf":Z
    .restart local v17    # "waitedHalf":Z
    iget-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object v13, v3

    check-cast v13, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;

    .line 460
    .local v13, "mInfo":Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;
    invoke-virtual {v13}, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->getHalfState()Z

    move-result v15

    .line 461
    .local v15, "mIsHalf":Z
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 462
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    invoke-static {v3, v12, v1, v2}, Lcom/android/server/ScoutHelper;->checkBinderCallPidList(ILcom/android/server/ScoutHelper$ScoutBinderInfo;Ljava/util/ArrayList;Ljava/util/ArrayList;)Z

    .line 463
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->getBinderTransInfo()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 464
    invoke-virtual {v12}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->getProcInfo()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 463
    invoke-virtual {v13, v3}, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->setBinderTransInfo(Ljava/lang/String;)V

    .line 465
    if-eqz v15, :cond_2

    invoke-virtual {v13}, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->getScoutLevel()I

    move-result v3

    if-gt v3, v4, :cond_3

    .line 466
    :cond_2
    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 467
    invoke-static {v2}, Ljava/util/concurrent/CompletableFuture;->completedFuture(Ljava/lang/Object;)Ljava/util/concurrent/CompletableFuture;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v13}, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    .line 466
    move-object v3, v1

    invoke-static/range {v3 .. v11}, Lcom/android/server/am/StackTracesDumpHelper;->dumpStackTraces(Ljava/util/ArrayList;Lcom/android/internal/os/ProcessCpuTracker;Landroid/util/SparseBooleanArray;Ljava/util/concurrent/Future;Ljava/io/StringWriter;Ljava/lang/String;Ljava/lang/String;Ljava/util/concurrent/Executor;Lcom/android/internal/os/anr/AnrLatencyTracker;)Ljava/io/File;

    move-result-object v14

    .line 469
    :cond_3
    const/16 v3, 0x190

    const-string v4, ""

    invoke-static {v3, v14, v13, v4}, Lcom/android/server/ScoutSystemMonitor;->onFwScout(ILjava/io/File;Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;Ljava/lang/String;)V

    .line 470
    invoke-virtual {v12}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->getMonkeyPid()I

    move-result v3

    .line 471
    .local v3, "monkeyPid":I
    if-eqz v3, :cond_4

    .line 472
    invoke-static {v3}, Landroid/os/Process;->killProcess(I)V

    .line 504
    .end local v3    # "monkeyPid":I
    .end local v13    # "mInfo":Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;
    .end local v15    # "mIsHalf":Z
    :cond_4
    :goto_2
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_4
        0x1 -> :sswitch_3
        0xa -> :sswitch_2
        0xb -> :sswitch_1
        0xc -> :sswitch_0
    .end sparse-switch
.end method
