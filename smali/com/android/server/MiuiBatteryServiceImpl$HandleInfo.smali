.class Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;
.super Ljava/lang/Object;
.source "MiuiBatteryServiceImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/MiuiBatteryServiceImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "HandleInfo"
.end annotation


# instance fields
.field mBatteryLevel:I

.field mBatteryStats:Ljava/lang/String;

.field mColorNumber:Ljava/lang/String;

.field mConnectState:I

.field mFwVersion:Ljava/lang/String;

.field mHidName:Ljava/lang/String;

.field mPid:I

.field mVid:I

.field final synthetic this$0:Lcom/android/server/MiuiBatteryServiceImpl;


# direct methods
.method public constructor <init>(Lcom/android/server/MiuiBatteryServiceImpl;Landroid/hardware/usb/UsbDevice;)V
    .locals 2
    .param p1, "this$0"    # Lcom/android/server/MiuiBatteryServiceImpl;
    .param p2, "usbDevice"    # Landroid/hardware/usb/UsbDevice;

    .line 1635
    iput-object p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1636
    invoke-virtual {p2}, Landroid/hardware/usb/UsbDevice;->getProductId()I

    move-result v0

    iput v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->mPid:I

    .line 1637
    invoke-virtual {p2}, Landroid/hardware/usb/UsbDevice;->getVendorId()I

    move-result v0

    iput v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->mVid:I

    .line 1638
    invoke-virtual {p2}, Landroid/hardware/usb/UsbDevice;->getVersion()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->mFwVersion:Ljava/lang/String;

    .line 1639
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "hid-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Landroid/hardware/usb/UsbDevice;->getSerialNumber()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-battery"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->mHidName:Ljava/lang/String;

    .line 1640
    return-void
.end method


# virtual methods
.method setBatteryLevel(I)V
    .locals 0
    .param p1, "level"    # I

    .line 1647
    iput p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->mBatteryLevel:I

    .line 1648
    return-void
.end method

.method setBatteryStats(Ljava/lang/String;)V
    .locals 0
    .param p1, "batteryStats"    # Ljava/lang/String;

    .line 1651
    iput-object p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->mBatteryStats:Ljava/lang/String;

    .line 1652
    return-void
.end method

.method setColorNumber(Ljava/lang/String;)V
    .locals 0
    .param p1, "color"    # Ljava/lang/String;

    .line 1655
    iput-object p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->mColorNumber:Ljava/lang/String;

    .line 1656
    return-void
.end method

.method setConnectState(I)V
    .locals 0
    .param p1, "connectState"    # I

    .line 1643
    iput p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->mConnectState:I

    .line 1644
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1660
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "HandleInfo{mConnectState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->mConnectState:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mPid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->mPid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mVid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->mVid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mBatteryLevel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->mBatteryLevel:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mHidName=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->mHidName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", mBatteryStats=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->mBatteryStats:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", mFwVersion=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->mFwVersion:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", mColorNumber=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->mColorNumber:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
