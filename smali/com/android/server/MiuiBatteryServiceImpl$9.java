class com.android.server.MiuiBatteryServiceImpl$9 extends android.content.BroadcastReceiver {
	 /* .source "MiuiBatteryServiceImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/MiuiBatteryServiceImpl;->init(Landroid/content/Context;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.MiuiBatteryServiceImpl this$0; //synthetic
/* # direct methods */
 com.android.server.MiuiBatteryServiceImpl$9 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/MiuiBatteryServiceImpl; */
/* .line 336 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 8 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 339 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 340 */
/* .local v0, "action":Ljava/lang/String; */
final String v1 = "android.bluetooth.adapter.action.STATE_CHANGED"; // const-string v1, "android.bluetooth.adapter.action.STATE_CHANGED"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* const/16 v2, 0xa */
int v3 = 0; // const/4 v3, 0x0
if ( v1 != null) { // if-eqz v1, :cond_2
	 /* .line 341 */
	 final String v1 = "android.bluetooth.adapter.extra.STATE"; // const-string v1, "android.bluetooth.adapter.extra.STATE"
	 v1 = 	 (( android.content.Intent ) p2 ).getIntExtra ( v1, v3 ); // invoke-virtual {p2, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
	 /* .line 342 */
	 /* .local v1, "value":I */
	 /* const/16 v4, 0xf */
	 /* if-eq v1, v4, :cond_0 */
	 /* if-ne v1, v2, :cond_1 */
	 /* .line 343 */
} // :cond_0
v4 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fputBtConnectedCount ( v4,v3 );
/* .line 344 */
v4 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmHandler ( v4 );
(( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) v4 ).sendMessage ( v2, v3 ); // invoke-virtual {v4, v2, v3}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessage(IZ)V
/* .line 346 */
} // .end local v1 # "value":I
} // :cond_1
} // :cond_2
final String v1 = "android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED"; // const-string v1, "android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_3 */
/* .line 347 */
final String v1 = "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"; // const-string v1, "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 348 */
} // :cond_3
final String v1 = "android.bluetooth.profile.extra.STATE"; // const-string v1, "android.bluetooth.profile.extra.STATE"
int v4 = -1; // const/4 v4, -0x1
v1 = (( android.content.Intent ) p2 ).getIntExtra ( v1, v4 ); // invoke-virtual {p2, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
/* .line 349 */
/* .local v1, "state":I */
final String v5 = "android.bluetooth.profile.extra.PREVIOUS_STATE"; // const-string v5, "android.bluetooth.profile.extra.PREVIOUS_STATE"
v4 = (( android.content.Intent ) p2 ).getIntExtra ( v5, v4 ); // invoke-virtual {p2, v5, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
/* .line 350 */
/* .local v4, "preState":I */
int v5 = 2; // const/4 v5, 0x2
int v6 = 1; // const/4 v6, 0x1
/* if-ne v1, v5, :cond_4 */
/* .line 351 */
v3 = this.this$0;
v5 = com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetBtConnectedCount ( v3 );
/* add-int/2addr v5, v6 */
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fputBtConnectedCount ( v3,v5 );
/* .line 352 */
v3 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmHandler ( v3 );
(( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) v3 ).sendMessage ( v2, v6 ); // invoke-virtual {v3, v2, v6}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessage(IZ)V
/* .line 353 */
} // :cond_4
/* if-nez v1, :cond_5 */
/* if-eq v4, v6, :cond_5 */
/* .line 355 */
v5 = this.this$0;
v7 = com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetBtConnectedCount ( v5 );
/* sub-int/2addr v7, v6 */
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fputBtConnectedCount ( v5,v7 );
/* .line 356 */
v5 = this.this$0;
v5 = com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetBtConnectedCount ( v5 );
/* if-gtz v5, :cond_5 */
/* .line 357 */
v5 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fputBtConnectedCount ( v5,v3 );
/* .line 358 */
v5 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmHandler ( v5 );
(( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) v5 ).sendMessage ( v2, v3 ); // invoke-virtual {v5, v2, v3}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessage(IZ)V
/* .line 362 */
} // .end local v1 # "state":I
} // .end local v4 # "preState":I
} // :cond_5
} // :goto_0
return;
} // .end method
