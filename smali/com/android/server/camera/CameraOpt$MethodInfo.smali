.class Lcom/android/server/camera/CameraOpt$MethodInfo;
.super Ljava/lang/Object;
.source "CameraOpt.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/camera/CameraOpt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MethodInfo"
.end annotation


# instance fields
.field mClazz:Ljava/lang/Class;

.field mMethodName:Ljava/lang/String;

.field mParamsTypes:[Ljava/lang/Class;


# direct methods
.method constructor <init>()V
    .locals 0

    .line 168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 169
    return-void
.end method

.method constructor <init>(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V
    .locals 0
    .param p1, "clazz"    # Ljava/lang/Class;
    .param p2, "methodName"    # Ljava/lang/String;
    .param p3, "paramsTypes"    # [Ljava/lang/Class;

    .line 171
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 172
    iput-object p1, p0, Lcom/android/server/camera/CameraOpt$MethodInfo;->mClazz:Ljava/lang/Class;

    .line 173
    iput-object p2, p0, Lcom/android/server/camera/CameraOpt$MethodInfo;->mMethodName:Ljava/lang/String;

    .line 174
    iput-object p3, p0, Lcom/android/server/camera/CameraOpt$MethodInfo;->mParamsTypes:[Ljava/lang/Class;

    .line 175
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .line 186
    invoke-static {p1}, Ljava/util/Objects;->isNull(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 187
    return v1

    .line 189
    :cond_0
    const/4 v0, 0x1

    if-ne p0, p1, :cond_1

    .line 190
    return v0

    .line 192
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_2

    .line 193
    return v1

    .line 196
    :cond_2
    move-object v2, p1

    check-cast v2, Lcom/android/server/camera/CameraOpt$MethodInfo;

    .line 197
    .local v2, "methodInfo":Lcom/android/server/camera/CameraOpt$MethodInfo;
    iget-object v3, p0, Lcom/android/server/camera/CameraOpt$MethodInfo;->mClazz:Ljava/lang/Class;

    iget-object v4, v2, Lcom/android/server/camera/CameraOpt$MethodInfo;->mClazz:Ljava/lang/Class;

    if-eq v3, v4, :cond_3

    .line 198
    return v1

    .line 200
    :cond_3
    iget-object v3, p0, Lcom/android/server/camera/CameraOpt$MethodInfo;->mMethodName:Ljava/lang/String;

    iget-object v4, v2, Lcom/android/server/camera/CameraOpt$MethodInfo;->mMethodName:Ljava/lang/String;

    if-eq v3, v4, :cond_4

    .line 201
    return v1

    .line 203
    :cond_4
    iget-object v3, p0, Lcom/android/server/camera/CameraOpt$MethodInfo;->mParamsTypes:[Ljava/lang/Class;

    iget-object v4, v2, Lcom/android/server/camera/CameraOpt$MethodInfo;->mParamsTypes:[Ljava/lang/Class;

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 204
    return v1

    .line 206
    :cond_5
    return v0
.end method

.method public hashCode()I
    .locals 4

    .line 179
    iget-object v0, p0, Lcom/android/server/camera/CameraOpt$MethodInfo;->mClazz:Ljava/lang/Class;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    move v0, v1

    .line 180
    .local v0, "result":I
    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v3, p0, Lcom/android/server/camera/CameraOpt$MethodInfo;->mMethodName:Ljava/lang/String;

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v2, v1

    iget-object v1, p0, Lcom/android/server/camera/CameraOpt$MethodInfo;->mParamsTypes:[Ljava/lang/Class;

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v2, v1

    .line 181
    .end local v0    # "result":I
    .local v2, "result":I
    return v2
.end method

.method public setClass(Ljava/lang/Class;)Lcom/android/server/camera/CameraOpt$MethodInfo;
    .locals 0
    .param p1, "clazz"    # Ljava/lang/Class;

    .line 210
    iput-object p1, p0, Lcom/android/server/camera/CameraOpt$MethodInfo;->mClazz:Ljava/lang/Class;

    .line 211
    return-object p0
.end method

.method public setMethodName(Ljava/lang/String;)Lcom/android/server/camera/CameraOpt$MethodInfo;
    .locals 0
    .param p1, "methodName"    # Ljava/lang/String;

    .line 215
    iput-object p1, p0, Lcom/android/server/camera/CameraOpt$MethodInfo;->mMethodName:Ljava/lang/String;

    .line 216
    return-object p0
.end method

.method public setParmasClass([Ljava/lang/Class;)Lcom/android/server/camera/CameraOpt$MethodInfo;
    .locals 0
    .param p1, "paramsTypes"    # [Ljava/lang/Class;

    .line 220
    iput-object p1, p0, Lcom/android/server/camera/CameraOpt$MethodInfo;->mParamsTypes:[Ljava/lang/Class;

    .line 221
    return-object p0
.end method
