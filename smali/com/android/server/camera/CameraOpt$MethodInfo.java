class com.android.server.camera.CameraOpt$MethodInfo {
	 /* .source "CameraOpt.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/camera/CameraOpt; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "MethodInfo" */
} // .end annotation
/* # instance fields */
java.lang.Class mClazz;
java.lang.String mMethodName;
java.lang.Class mParamsTypes;
/* # direct methods */
 com.android.server.camera.CameraOpt$MethodInfo ( ) {
/* .locals 0 */
/* .line 168 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 169 */
return;
} // .end method
 com.android.server.camera.CameraOpt$MethodInfo ( ) {
/* .locals 0 */
/* .param p1, "clazz" # Ljava/lang/Class; */
/* .param p2, "methodName" # Ljava/lang/String; */
/* .param p3, "paramsTypes" # [Ljava/lang/Class; */
/* .line 171 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 172 */
this.mClazz = p1;
/* .line 173 */
this.mMethodName = p2;
/* .line 174 */
this.mParamsTypes = p3;
/* .line 175 */
return;
} // .end method
/* # virtual methods */
public Boolean equals ( java.lang.Object p0 ) {
/* .locals 5 */
/* .param p1, "o" # Ljava/lang/Object; */
/* .line 186 */
v0 = java.util.Objects .isNull ( p1 );
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 187 */
	 /* .line 189 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* if-ne p0, p1, :cond_1 */
/* .line 190 */
/* .line 192 */
} // :cond_1
(( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
(( java.lang.Object ) p1 ).getClass ( ); // invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
/* if-eq v2, v3, :cond_2 */
/* .line 193 */
/* .line 196 */
} // :cond_2
/* move-object v2, p1 */
/* check-cast v2, Lcom/android/server/camera/CameraOpt$MethodInfo; */
/* .line 197 */
/* .local v2, "methodInfo":Lcom/android/server/camera/CameraOpt$MethodInfo; */
v3 = this.mClazz;
v4 = this.mClazz;
/* if-eq v3, v4, :cond_3 */
/* .line 198 */
/* .line 200 */
} // :cond_3
v3 = this.mMethodName;
v4 = this.mMethodName;
/* if-eq v3, v4, :cond_4 */
/* .line 201 */
/* .line 203 */
} // :cond_4
v3 = this.mParamsTypes;
v4 = this.mParamsTypes;
v3 = java.util.Arrays .equals ( v3,v4 );
/* if-nez v3, :cond_5 */
/* .line 204 */
/* .line 206 */
} // :cond_5
} // .end method
public Integer hashCode ( ) {
/* .locals 4 */
/* .line 179 */
v0 = this.mClazz;
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = (( java.lang.Object ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I
} // :cond_0
/* move v0, v1 */
/* .line 180 */
/* .local v0, "result":I */
} // :goto_0
/* mul-int/lit8 v2, v0, 0x1f */
v3 = this.mMethodName;
if ( v3 != null) { // if-eqz v3, :cond_1
v1 = (( java.lang.String ) v3 ).hashCode ( ); // invoke-virtual {v3}, Ljava/lang/String;->hashCode()I
} // :cond_1
/* add-int/2addr v2, v1 */
v1 = this.mParamsTypes;
v1 = java.util.Arrays .hashCode ( v1 );
/* add-int/2addr v2, v1 */
/* .line 181 */
} // .end local v0 # "result":I
/* .local v2, "result":I */
} // .end method
public com.android.server.camera.CameraOpt$MethodInfo setClass ( java.lang.Class p0 ) {
/* .locals 0 */
/* .param p1, "clazz" # Ljava/lang/Class; */
/* .line 210 */
this.mClazz = p1;
/* .line 211 */
} // .end method
public com.android.server.camera.CameraOpt$MethodInfo setMethodName ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "methodName" # Ljava/lang/String; */
/* .line 215 */
this.mMethodName = p1;
/* .line 216 */
} // .end method
public com.android.server.camera.CameraOpt$MethodInfo setParmasClass ( java.lang.Class[] p0 ) {
/* .locals 0 */
/* .param p1, "paramsTypes" # [Ljava/lang/Class; */
/* .line 220 */
this.mParamsTypes = p1;
/* .line 221 */
} // .end method
