.class Lcom/android/server/camera/CameraActivitySceneMode$1;
.super Lmiui/process/IActivityChangeListener$Stub;
.source "CameraActivitySceneMode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/camera/CameraActivitySceneMode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/camera/CameraActivitySceneMode;


# direct methods
.method constructor <init>(Lcom/android/server/camera/CameraActivitySceneMode;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/camera/CameraActivitySceneMode;

    .line 81
    iput-object p1, p0, Lcom/android/server/camera/CameraActivitySceneMode$1;->this$0:Lcom/android/server/camera/CameraActivitySceneMode;

    invoke-direct {p0}, Lmiui/process/IActivityChangeListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivityChanged(Landroid/content/ComponentName;Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "preName"    # Landroid/content/ComponentName;
    .param p2, "curName"    # Landroid/content/ComponentName;

    .line 84
    if-nez p2, :cond_0

    .line 85
    return-void

    .line 86
    :cond_0
    invoke-virtual {p2}, Landroid/content/ComponentName;->toString()Ljava/lang/String;

    move-result-object v0

    .line 87
    .local v0, "curActivityName":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/camera/CameraActivitySceneMode$1;->this$0:Lcom/android/server/camera/CameraActivitySceneMode;

    invoke-static {v1, v0}, Lcom/android/server/camera/CameraActivitySceneMode;->-$$Nest$mdecideActivitySceneMode(Lcom/android/server/camera/CameraActivitySceneMode;Ljava/lang/String;)V

    .line 88
    return-void
.end method
