public class com.android.server.camera.CameraActivitySceneMode extends com.android.server.camera.CameraActivitySceneStub {
	 /* .source "CameraActivitySceneMode.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.camera.CameraActivitySceneStub$$" */
} // .end annotation
/* # static fields */
private static final java.lang.String ACTIVITY_ALIPAY_COMMON_SCAN;
private static final java.lang.String ACTIVITY_ALIPAY_HEALTH_SCAN;
private static final java.lang.String ACTIVITY_MIFACE_REGISTRATION;
private static final java.lang.String ACTIVITY_WECHAT_MULTITALK_VIDEO;
private static final java.lang.String ACTIVITY_WECHAT_SCAN;
private static final java.lang.String ACTIVITY_WECHAT_SNAPSHOT;
private static final java.lang.String ACTIVITY_WECHAT_VIDEO;
private static final java.lang.String CAMERA_HIGHRESOLUTIONBLOB_LIST;
private static final java.lang.String CAMERA_LIVE_LIST;
private static final java.lang.String CAMERA_VIDEOCALL_LIST;
private static final java.lang.String CONFIG_JSON_DEF_PATH;
private static final java.lang.String CONFIG_JSON_PATH;
private static final java.lang.String PERSIST_ACTIVITY;
private static final java.lang.String PERSIST_HIGHBLOB;
private static final java.lang.String PERSIST_LIVE;
private static final java.lang.String PERSIST_MIFACE;
private static final java.lang.String PERSIST_SNAPSHOT;
private static final java.lang.String PERSIST_VIDEO;
private static final java.lang.String PERSIST_VIDEOCALL;
private static final java.lang.String TAG;
private static final java.lang.String TAG_CONFIG;
private static final java.lang.String TAG_CONFIGS;
private static final java.lang.String TAG_DEFAULT;
private static final java.lang.String TAG_NAME;
private static final java.lang.String mTargetActivityList;
private static final java.lang.String mTargetPackageList;
private static volatile com.android.server.camera.CameraActivitySceneMode sIntance;
/* # instance fields */
private miui.process.IActivityChangeListener$Stub mActivityChangeListener;
private java.util.List mCameraHighBlobList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List mCameraLiveList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List mCameraVideocallList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.Map mListConfig;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static void -$$Nest$mdecideActivitySceneMode ( com.android.server.camera.CameraActivitySceneMode p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/camera/CameraActivitySceneMode;->decideActivitySceneMode(Ljava/lang/String;)V */
return;
} // .end method
static com.android.server.camera.CameraActivitySceneMode ( ) {
/* .locals 8 */
/* .line 38 */
/* const-class v0, Lcom/android/server/camera/CameraActivitySceneMode; */
(( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
/* .line 69 */
final String v1 = "com.tencent.mm.plugin.voip.ui.VideoActivity"; // const-string v1, "com.tencent.mm.plugin.voip.ui.VideoActivity"
final String v2 = "com.tencent.mm.plugin.multitalk.ui.MultiTalkMainUI"; // const-string v2, "com.tencent.mm.plugin.multitalk.ui.MultiTalkMainUI"
final String v3 = "com.tencent.mm.plugin.scanner.ui.BaseScanUI"; // const-string v3, "com.tencent.mm.plugin.scanner.ui.BaseScanUI"
final String v4 = "com.tencent.mm.plugin.recordvideo.activity.MMRecordUI"; // const-string v4, "com.tencent.mm.plugin.recordvideo.activity.MMRecordUI"
final String v5 = "com.alipay.mobile.scan.as.main.MainCaptureActivity"; // const-string v5, "com.alipay.mobile.scan.as.main.MainCaptureActivity"
final String v6 = "com.alipay.mobile.scan.as.tool.ToolsCaptureActivity"; // const-string v6, "com.alipay.mobile.scan.as.tool.ToolsCaptureActivity"
final String v7 = "com.android.settings.faceunlock.MiuiNormalCameraMultiFaceInput"; // const-string v7, "com.android.settings.faceunlock.MiuiNormalCameraMultiFaceInput"
/* filled-new-array/range {v1 ..v7}, [Ljava/lang/String; */
/* .line 79 */
int v0 = 0; // const/4 v0, 0x0
/* new-array v0, v0, [Ljava/lang/String; */
return;
} // .end method
public com.android.server.camera.CameraActivitySceneMode ( ) {
/* .locals 1 */
/* .line 37 */
/* invoke-direct {p0}, Lcom/android/server/camera/CameraActivitySceneStub;-><init>()V */
/* .line 44 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.mListConfig = v0;
/* .line 45 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mCameraVideocallList = v0;
/* .line 46 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mCameraHighBlobList = v0;
/* .line 47 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mCameraLiveList = v0;
/* .line 80 */
/* new-instance v0, Lcom/android/server/camera/CameraActivitySceneMode$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/camera/CameraActivitySceneMode$1;-><init>(Lcom/android/server/camera/CameraActivitySceneMode;)V */
this.mActivityChangeListener = v0;
return;
} // .end method
private void beginConfig ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "content" # Ljava/lang/String; */
/* .line 210 */
final String v0 = "default"; // const-string v0, "default"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
v0 = android.text.TextUtils .isEmpty ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 214 */
} // :cond_0
/* invoke-direct {p0, p1}, Lcom/android/server/camera/CameraActivitySceneMode;->parseConfigListLocked(Ljava/lang/String;)V */
/* .line 215 */
return;
/* .line 211 */
} // :cond_1
} // :goto_0
v0 = com.android.server.camera.CameraActivitySceneMode.TAG;
final String v1 = "json file not found or read fail!"; // const-string v1, "json file not found or read fail!"
android.util.Slog .w ( v0,v1 );
/* .line 212 */
return;
} // .end method
private Boolean checkFile ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "path" # Ljava/lang/String; */
/* .line 228 */
v0 = android.text.TextUtils .isEmpty ( p1 );
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 229 */
/* .line 231 */
} // :cond_0
/* new-instance v0, Ljava/io/File; */
/* invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 232 */
/* .local v0, "file":Ljava/io/File; */
v2 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
if ( v2 != null) { // if-eqz v2, :cond_1
v2 = (( java.io.File ) v0 ).isFile ( ); // invoke-virtual {v0}, Ljava/io/File;->isFile()Z
if ( v2 != null) { // if-eqz v2, :cond_1
v2 = (( java.io.File ) v0 ).canRead ( ); // invoke-virtual {v0}, Ljava/io/File;->canRead()Z
if ( v2 != null) { // if-eqz v2, :cond_1
int v1 = 1; // const/4 v1, 0x1
} // :cond_1
} // .end method
private void decideActivitySceneMode ( java.lang.String p0 ) {
/* .locals 8 */
/* .param p1, "activityName" # Ljava/lang/String; */
/* .line 118 */
final String v0 = "persist.vendor.vcb.video"; // const-string v0, "persist.vendor.vcb.video"
final String v1 = "false"; // const-string v1, "false"
android.os.SystemProperties .set ( v0,v1 );
/* .line 119 */
final String v2 = "persist.vendor.vcb.snapshot"; // const-string v2, "persist.vendor.vcb.snapshot"
android.os.SystemProperties .set ( v2,v1 );
/* .line 120 */
final String v3 = "persist.vendor.camera.3rdhighResolutionBlob.scenes"; // const-string v3, "persist.vendor.camera.3rdhighResolutionBlob.scenes"
android.os.SystemProperties .set ( v3,v1 );
/* .line 121 */
final String v4 = "persist.vendor.camera.3rdvideocall.scenes"; // const-string v4, "persist.vendor.camera.3rdvideocall.scenes"
android.os.SystemProperties .set ( v4,v1 );
/* .line 122 */
final String v5 = "persist.vendor.miface.registration.scenes"; // const-string v5, "persist.vendor.miface.registration.scenes"
android.os.SystemProperties .set ( v5,v1 );
/* .line 123 */
final String v6 = "persist.vendor.camera.3rdlive.scenes"; // const-string v6, "persist.vendor.camera.3rdlive.scenes"
android.os.SystemProperties .set ( v6,v1 );
/* .line 124 */
final String v1 = "com.tencent.mm.plugin.voip.ui.VideoActivity"; // const-string v1, "com.tencent.mm.plugin.voip.ui.VideoActivity"
v1 = (( java.lang.String ) p1 ).contains ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* const-string/jumbo v7, "true" */
/* if-nez v1, :cond_0 */
final String v1 = "com.tencent.mm.plugin.multitalk.ui.MultiTalkMainUI"; // const-string v1, "com.tencent.mm.plugin.multitalk.ui.MultiTalkMainUI"
v1 = (( java.lang.String ) p1 ).contains ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 125 */
} // :cond_0
android.os.SystemProperties .set ( v0,v7 );
/* .line 127 */
} // :cond_1
final String v0 = "com.tencent.mm.plugin.recordvideo.activity.MMRecordUI"; // const-string v0, "com.tencent.mm.plugin.recordvideo.activity.MMRecordUI"
v0 = (( java.lang.String ) p1 ).contains ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 128 */
android.os.SystemProperties .set ( v2,v7 );
/* .line 130 */
} // :cond_2
final String v0 = "com.android.settings.faceunlock.MiuiNormalCameraMultiFaceInput"; // const-string v0, "com.android.settings.faceunlock.MiuiNormalCameraMultiFaceInput"
v0 = (( java.lang.String ) p1 ).contains ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 131 */
android.os.SystemProperties .set ( v5,v7 );
/* .line 133 */
} // :cond_3
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v1 = v1 = this.mCameraVideocallList;
/* if-ge v0, v1, :cond_5 */
/* .line 134 */
v1 = this.mCameraVideocallList;
/* check-cast v1, Ljava/lang/CharSequence; */
v1 = (( java.lang.String ) p1 ).contains ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 135 */
android.os.SystemProperties .set ( v4,v7 );
/* .line 136 */
return;
/* .line 133 */
} // :cond_4
/* add-int/lit8 v0, v0, 0x1 */
/* .line 139 */
} // .end local v0 # "i":I
} // :cond_5
int v0 = 0; // const/4 v0, 0x0
/* .restart local v0 # "i":I */
} // :goto_1
v1 = v1 = this.mCameraHighBlobList;
/* if-ge v0, v1, :cond_7 */
/* .line 140 */
v1 = this.mCameraHighBlobList;
/* check-cast v1, Ljava/lang/CharSequence; */
v1 = (( java.lang.String ) p1 ).contains ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 141 */
android.os.SystemProperties .set ( v3,v7 );
/* .line 142 */
return;
/* .line 139 */
} // :cond_6
/* add-int/lit8 v0, v0, 0x1 */
/* .line 145 */
} // .end local v0 # "i":I
} // :cond_7
int v0 = 0; // const/4 v0, 0x0
/* .restart local v0 # "i":I */
} // :goto_2
v1 = v1 = this.mCameraLiveList;
/* if-ge v0, v1, :cond_9 */
/* .line 146 */
v1 = this.mCameraLiveList;
/* check-cast v1, Ljava/lang/CharSequence; */
v1 = (( java.lang.String ) p1 ).contains ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v1 != null) { // if-eqz v1, :cond_8
/* .line 147 */
android.os.SystemProperties .set ( v6,v7 );
/* .line 148 */
return;
/* .line 145 */
} // :cond_8
/* add-int/lit8 v0, v0, 0x1 */
/* .line 151 */
} // .end local v0 # "i":I
} // :cond_9
return;
} // .end method
public static com.android.server.camera.CameraActivitySceneMode getInstance ( ) {
/* .locals 2 */
/* .line 173 */
v0 = com.android.server.camera.CameraActivitySceneMode.sIntance;
/* if-nez v0, :cond_1 */
/* .line 174 */
/* const-class v0, Lcom/android/server/camera/CameraActivitySceneMode; */
/* monitor-enter v0 */
/* .line 175 */
try { // :try_start_0
v1 = com.android.server.camera.CameraActivitySceneMode.sIntance;
/* if-nez v1, :cond_0 */
/* .line 176 */
/* new-instance v1, Lcom/android/server/camera/CameraActivitySceneMode; */
/* invoke-direct {v1}, Lcom/android/server/camera/CameraActivitySceneMode;-><init>()V */
/* .line 178 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 180 */
} // :cond_1
} // :goto_0
v0 = com.android.server.camera.CameraActivitySceneMode.sIntance;
} // .end method
static void lambda$readJSONFileToString$0 ( java.lang.StringBuilder p0, java.lang.String p1 ) { //synthethic
/* .locals 2 */
/* .param p0, "builder" # Ljava/lang/StringBuilder; */
/* .param p1, "s" # Ljava/lang/String; */
/* .line 220 */
(( java.lang.StringBuilder ) p0 ).append ( p1 ); // invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "\n"; // const-string v1, "\n"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
return;
} // .end method
private void parseConfigListLocked ( java.lang.String p0 ) {
/* .locals 11 */
/* .param p1, "content" # Ljava/lang/String; */
/* .line 185 */
final String v0 = "config"; // const-string v0, "config"
try { // :try_start_0
final String v1 = "configs"; // const-string v1, "configs"
/* .line 186 */
/* .local v1, "tag":Ljava/lang/String; */
/* new-instance v2, Lorg/json/JSONObject; */
/* invoke-direct {v2, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V */
/* .line 187 */
/* .local v2, "jsonObject":Lorg/json/JSONObject; */
v3 = (( org.json.JSONObject ) v2 ).has ( v1 ); // invoke-virtual {v2, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 188 */
(( org.json.JSONObject ) v2 ).optJSONArray ( v1 ); // invoke-virtual {v2, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 189 */
/* .local v3, "arrays":Lorg/json/JSONArray; */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
} // :goto_0
v5 = (( org.json.JSONArray ) v3 ).length ( ); // invoke-virtual {v3}, Lorg/json/JSONArray;->length()I
/* if-ge v4, v5, :cond_3 */
/* .line 190 */
(( org.json.JSONArray ) v3 ).optJSONObject ( v4 ); // invoke-virtual {v3, v4}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;
/* .line 191 */
/* .local v5, "obj":Lorg/json/JSONObject; */
/* if-nez v5, :cond_0 */
/* .line 192 */
} // :cond_0
final String v6 = "name"; // const-string v6, "name"
(( org.json.JSONObject ) v5 ).optString ( v6 ); // invoke-virtual {v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 193 */
/* .local v6, "name":Ljava/lang/String; */
v7 = (( org.json.JSONObject ) v5 ).has ( v0 ); // invoke-virtual {v5, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v7 != null) { // if-eqz v7, :cond_2
/* .line 194 */
(( org.json.JSONObject ) v5 ).optJSONArray ( v0 ); // invoke-virtual {v5, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 195 */
/* .local v7, "values":Lorg/json/JSONArray; */
/* new-instance v8, Ljava/util/ArrayList; */
/* invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V */
/* .line 196 */
/* .local v8, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
int v9 = 0; // const/4 v9, 0x0
/* .local v9, "j":I */
} // :goto_1
v10 = (( org.json.JSONArray ) v7 ).length ( ); // invoke-virtual {v7}, Lorg/json/JSONArray;->length()I
/* if-ge v9, v10, :cond_1 */
/* .line 197 */
(( org.json.JSONArray ) v7 ).getString ( v9 ); // invoke-virtual {v7, v9}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;
/* .line 198 */
/* .local v10, "pkg":Ljava/lang/String; */
/* .line 196 */
/* nop */
} // .end local v10 # "pkg":Ljava/lang/String;
/* add-int/lit8 v9, v9, 0x1 */
/* .line 200 */
} // .end local v9 # "j":I
} // :cond_1
v9 = this.mListConfig;
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 189 */
} // .end local v5 # "obj":Lorg/json/JSONObject;
} // .end local v6 # "name":Ljava/lang/String;
} // .end local v7 # "values":Lorg/json/JSONArray;
} // .end local v8 # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // :cond_2
} // :goto_2
/* add-int/lit8 v4, v4, 0x1 */
/* .line 206 */
} // .end local v1 # "tag":Ljava/lang/String;
} // .end local v2 # "jsonObject":Lorg/json/JSONObject;
} // .end local v3 # "arrays":Lorg/json/JSONArray;
} // .end local v4 # "i":I
} // :cond_3
/* .line 204 */
/* :catch_0 */
/* move-exception v0 */
/* .line 205 */
/* .local v0, "e":Lorg/json/JSONException; */
(( org.json.JSONException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V
/* .line 207 */
} // .end local v0 # "e":Lorg/json/JSONException;
} // :goto_3
return;
} // .end method
private void parseJson ( ) {
/* .locals 4 */
/* .line 236 */
final String v0 = "default"; // const-string v0, "default"
/* .line 237 */
/* .local v0, "content":Ljava/lang/String; */
final String v1 = "/system_ext/etc/camerascene.json"; // const-string v1, "/system_ext/etc/camerascene.json"
v2 = /* invoke-direct {p0, v1}, Lcom/android/server/camera/CameraActivitySceneMode;->checkFile(Ljava/lang/String;)Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 238 */
v2 = com.android.server.camera.CameraActivitySceneMode.TAG;
/* const-string/jumbo v3, "the default json file path is : /system_ext/etc/camerascene.json" */
android.util.Slog .i ( v2,v3 );
/* .line 239 */
/* invoke-direct {p0, v1}, Lcom/android/server/camera/CameraActivitySceneMode;->readJSONFileToString(Ljava/lang/String;)Ljava/lang/String; */
/* .line 240 */
/* invoke-direct {p0, v0}, Lcom/android/server/camera/CameraActivitySceneMode;->beginConfig(Ljava/lang/String;)V */
/* .line 242 */
} // :cond_0
final String v1 = "/odm/etc/camera/camerascene.json"; // const-string v1, "/odm/etc/camera/camerascene.json"
v2 = /* invoke-direct {p0, v1}, Lcom/android/server/camera/CameraActivitySceneMode;->checkFile(Ljava/lang/String;)Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 243 */
v2 = com.android.server.camera.CameraActivitySceneMode.TAG;
/* const-string/jumbo v3, "the odm json file path is : /odm/etc/camera/camerascene.json" */
android.util.Slog .i ( v2,v3 );
/* .line 244 */
/* invoke-direct {p0, v1}, Lcom/android/server/camera/CameraActivitySceneMode;->readJSONFileToString(Ljava/lang/String;)Ljava/lang/String; */
/* .line 245 */
/* invoke-direct {p0, v0}, Lcom/android/server/camera/CameraActivitySceneMode;->beginConfig(Ljava/lang/String;)V */
/* .line 247 */
} // :cond_1
return;
} // .end method
private java.lang.String readJSONFileToString ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "name" # Ljava/lang/String; */
/* .line 218 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 219 */
/* .local v0, "builder":Ljava/lang/StringBuilder; */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
/* new-array v1, v1, [Ljava/lang/String; */
java.nio.file.Paths .get ( p1,v1 );
v2 = java.nio.charset.StandardCharsets.UTF_8;
java.nio.file.Files .lines ( v1,v2 );
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 220 */
/* .local v1, "stream":Ljava/util/stream/Stream;, "Ljava/util/stream/Stream<Ljava/lang/String;>;" */
try { // :try_start_1
/* new-instance v2, Lcom/android/server/camera/CameraActivitySceneMode$$ExternalSyntheticLambda0; */
/* invoke-direct {v2, v0}, Lcom/android/server/camera/CameraActivitySceneMode$$ExternalSyntheticLambda0;-><init>(Ljava/lang/StringBuilder;)V */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 221 */
if ( v1 != null) { // if-eqz v1, :cond_0
try { // :try_start_2
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 223 */
} // .end local v1 # "stream":Ljava/util/stream/Stream;, "Ljava/util/stream/Stream<Ljava/lang/String;>;"
} // :cond_0
/* .line 219 */
/* .restart local v1 # "stream":Ljava/util/stream/Stream;, "Ljava/util/stream/Stream<Ljava/lang/String;>;" */
/* :catchall_0 */
/* move-exception v2 */
if ( v1 != null) { // if-eqz v1, :cond_1
try { // :try_start_3
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v3 */
try { // :try_start_4
(( java.lang.Throwable ) v2 ).addSuppressed ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "builder":Ljava/lang/StringBuilder;
} // .end local p0 # "this":Lcom/android/server/camera/CameraActivitySceneMode;
} // .end local p1 # "name":Ljava/lang/String;
} // :cond_1
} // :goto_0
/* throw v2 */
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 221 */
} // .end local v1 # "stream":Ljava/util/stream/Stream;, "Ljava/util/stream/Stream<Ljava/lang/String;>;"
/* .restart local v0 # "builder":Ljava/lang/StringBuilder; */
/* .restart local p0 # "this":Lcom/android/server/camera/CameraActivitySceneMode; */
/* .restart local p1 # "name":Ljava/lang/String; */
/* :catch_0 */
/* move-exception v1 */
/* .line 222 */
/* .local v1, "e":Ljava/io/IOException; */
v2 = com.android.server.camera.CameraActivitySceneMode.TAG;
final String v3 = "IOException"; // const-string v3, "IOException"
android.util.Slog .e ( v2,v3 );
/* .line 224 */
} // .end local v1 # "e":Ljava/io/IOException;
} // :goto_1
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
private void registerActivityChangeListener ( ) {
/* .locals 5 */
/* .line 92 */
v0 = this.mActivityChangeListener;
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 93 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 94 */
/* .local v0, "targetActivities":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 96 */
/* .local v1, "targetPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
v3 = com.android.server.camera.CameraActivitySceneMode.mTargetActivityList;
/* array-length v4, v3 */
/* if-ge v2, v4, :cond_0 */
/* .line 97 */
/* aget-object v3, v3, v2 */
/* .line 96 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 99 */
} // .end local v2 # "i":I
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
/* .restart local v2 # "i":I */
} // :goto_1
v3 = v3 = this.mCameraVideocallList;
/* if-ge v2, v3, :cond_1 */
/* .line 100 */
v3 = this.mCameraVideocallList;
/* check-cast v3, Ljava/lang/String; */
/* .line 99 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 102 */
} // .end local v2 # "i":I
} // :cond_1
int v2 = 0; // const/4 v2, 0x0
/* .restart local v2 # "i":I */
} // :goto_2
v3 = v3 = this.mCameraHighBlobList;
/* if-ge v2, v3, :cond_2 */
/* .line 103 */
v3 = this.mCameraHighBlobList;
/* check-cast v3, Ljava/lang/String; */
/* .line 102 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 105 */
} // .end local v2 # "i":I
} // :cond_2
int v2 = 0; // const/4 v2, 0x0
/* .restart local v2 # "i":I */
} // :goto_3
v3 = v3 = this.mCameraLiveList;
/* if-ge v2, v3, :cond_3 */
/* .line 106 */
v3 = this.mCameraLiveList;
/* check-cast v3, Ljava/lang/String; */
/* .line 105 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 109 */
} // .end local v2 # "i":I
} // :cond_3
int v2 = 0; // const/4 v2, 0x0
/* .restart local v2 # "i":I */
} // :goto_4
v3 = com.android.server.camera.CameraActivitySceneMode.mTargetPackageList;
/* array-length v4, v3 */
/* if-ge v2, v4, :cond_4 */
/* .line 110 */
/* aget-object v3, v3, v2 */
/* .line 109 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 112 */
} // .end local v2 # "i":I
} // :cond_4
v2 = this.mActivityChangeListener;
miui.process.ProcessManager .registerActivityChangeListener ( v1,v0,v2 );
/* .line 115 */
} // .end local v0 # "targetActivities":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // .end local v1 # "targetPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // :cond_5
return;
} // .end method
private void updateActivityList ( ) {
/* .locals 2 */
/* .line 167 */
v0 = this.mListConfig;
/* const-string/jumbo v1, "videocall_list" */
/* check-cast v0, Ljava/util/List; */
this.mCameraVideocallList = v0;
/* .line 168 */
v0 = this.mListConfig;
final String v1 = "highresolutionblob_list"; // const-string v1, "highresolutionblob_list"
/* check-cast v0, Ljava/util/List; */
this.mCameraHighBlobList = v0;
/* .line 169 */
v0 = this.mListConfig;
/* const-string/jumbo v1, "tplive_list" */
/* check-cast v0, Ljava/util/List; */
this.mCameraLiveList = v0;
/* .line 170 */
return;
} // .end method
/* # virtual methods */
public void initSystemBooted ( ) {
/* .locals 2 */
/* .line 154 */
final String v0 = "persist.vendor.vcb.video"; // const-string v0, "persist.vendor.vcb.video"
final String v1 = "false"; // const-string v1, "false"
android.os.SystemProperties .set ( v0,v1 );
/* .line 155 */
final String v0 = "persist.vendor.vcb.snapshot"; // const-string v0, "persist.vendor.vcb.snapshot"
android.os.SystemProperties .set ( v0,v1 );
/* .line 156 */
final String v0 = "persist.vendor.camera.3rdhighResolutionBlob.scenes"; // const-string v0, "persist.vendor.camera.3rdhighResolutionBlob.scenes"
android.os.SystemProperties .set ( v0,v1 );
/* .line 157 */
final String v0 = "persist.vendor.camera.3rdvideocall.scenes"; // const-string v0, "persist.vendor.camera.3rdvideocall.scenes"
android.os.SystemProperties .set ( v0,v1 );
/* .line 158 */
final String v0 = "persist.vendor.miface.registration.scenes"; // const-string v0, "persist.vendor.miface.registration.scenes"
android.os.SystemProperties .set ( v0,v1 );
/* .line 159 */
final String v0 = "persist.vendor.camera.3rdlive.scenes"; // const-string v0, "persist.vendor.camera.3rdlive.scenes"
android.os.SystemProperties .set ( v0,v1 );
/* .line 160 */
int v0 = 0; // const/4 v0, 0x0
java.lang.Integer .toString ( v0 );
final String v1 = "persist.vendor.vcb.activity"; // const-string v1, "persist.vendor.vcb.activity"
android.os.SystemProperties .set ( v1,v0 );
/* .line 161 */
/* invoke-direct {p0}, Lcom/android/server/camera/CameraActivitySceneMode;->parseJson()V */
/* .line 162 */
/* invoke-direct {p0}, Lcom/android/server/camera/CameraActivitySceneMode;->updateActivityList()V */
/* .line 163 */
/* invoke-direct {p0}, Lcom/android/server/camera/CameraActivitySceneMode;->registerActivityChangeListener()V */
/* .line 164 */
return;
} // .end method
