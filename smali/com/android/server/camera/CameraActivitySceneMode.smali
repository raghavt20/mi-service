.class public Lcom/android/server/camera/CameraActivitySceneMode;
.super Lcom/android/server/camera/CameraActivitySceneStub;
.source "CameraActivitySceneMode.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.camera.CameraActivitySceneStub$$"
.end annotation


# static fields
.field private static final ACTIVITY_ALIPAY_COMMON_SCAN:Ljava/lang/String; = "com.alipay.mobile.scan.as.main.MainCaptureActivity"

.field private static final ACTIVITY_ALIPAY_HEALTH_SCAN:Ljava/lang/String; = "com.alipay.mobile.scan.as.tool.ToolsCaptureActivity"

.field private static final ACTIVITY_MIFACE_REGISTRATION:Ljava/lang/String; = "com.android.settings.faceunlock.MiuiNormalCameraMultiFaceInput"

.field private static final ACTIVITY_WECHAT_MULTITALK_VIDEO:Ljava/lang/String; = "com.tencent.mm.plugin.multitalk.ui.MultiTalkMainUI"

.field private static final ACTIVITY_WECHAT_SCAN:Ljava/lang/String; = "com.tencent.mm.plugin.scanner.ui.BaseScanUI"

.field private static final ACTIVITY_WECHAT_SNAPSHOT:Ljava/lang/String; = "com.tencent.mm.plugin.recordvideo.activity.MMRecordUI"

.field private static final ACTIVITY_WECHAT_VIDEO:Ljava/lang/String; = "com.tencent.mm.plugin.voip.ui.VideoActivity"

.field private static final CAMERA_HIGHRESOLUTIONBLOB_LIST:Ljava/lang/String; = "highresolutionblob_list"

.field private static final CAMERA_LIVE_LIST:Ljava/lang/String; = "tplive_list"

.field private static final CAMERA_VIDEOCALL_LIST:Ljava/lang/String; = "videocall_list"

.field private static final CONFIG_JSON_DEF_PATH:Ljava/lang/String; = "/system_ext/etc/camerascene.json"

.field private static final CONFIG_JSON_PATH:Ljava/lang/String; = "/odm/etc/camera/camerascene.json"

.field private static final PERSIST_ACTIVITY:Ljava/lang/String; = "persist.vendor.vcb.activity"

.field private static final PERSIST_HIGHBLOB:Ljava/lang/String; = "persist.vendor.camera.3rdhighResolutionBlob.scenes"

.field private static final PERSIST_LIVE:Ljava/lang/String; = "persist.vendor.camera.3rdlive.scenes"

.field private static final PERSIST_MIFACE:Ljava/lang/String; = "persist.vendor.miface.registration.scenes"

.field private static final PERSIST_SNAPSHOT:Ljava/lang/String; = "persist.vendor.vcb.snapshot"

.field private static final PERSIST_VIDEO:Ljava/lang/String; = "persist.vendor.vcb.video"

.field private static final PERSIST_VIDEOCALL:Ljava/lang/String; = "persist.vendor.camera.3rdvideocall.scenes"

.field private static final TAG:Ljava/lang/String;

.field private static final TAG_CONFIG:Ljava/lang/String; = "config"

.field private static final TAG_CONFIGS:Ljava/lang/String; = "configs"

.field private static final TAG_DEFAULT:Ljava/lang/String; = "default"

.field private static final TAG_NAME:Ljava/lang/String; = "name"

.field private static final mTargetActivityList:[Ljava/lang/String;

.field private static final mTargetPackageList:[Ljava/lang/String;

.field private static volatile sIntance:Lcom/android/server/camera/CameraActivitySceneMode;


# instance fields
.field private mActivityChangeListener:Lmiui/process/IActivityChangeListener$Stub;

.field private mCameraHighBlobList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mCameraLiveList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mCameraVideocallList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mListConfig:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$mdecideActivitySceneMode(Lcom/android/server/camera/CameraActivitySceneMode;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/camera/CameraActivitySceneMode;->decideActivitySceneMode(Ljava/lang/String;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 8

    .line 38
    const-class v0, Lcom/android/server/camera/CameraActivitySceneMode;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/camera/CameraActivitySceneMode;->TAG:Ljava/lang/String;

    .line 69
    const-string v1, "com.tencent.mm.plugin.voip.ui.VideoActivity"

    const-string v2, "com.tencent.mm.plugin.multitalk.ui.MultiTalkMainUI"

    const-string v3, "com.tencent.mm.plugin.scanner.ui.BaseScanUI"

    const-string v4, "com.tencent.mm.plugin.recordvideo.activity.MMRecordUI"

    const-string v5, "com.alipay.mobile.scan.as.main.MainCaptureActivity"

    const-string v6, "com.alipay.mobile.scan.as.tool.ToolsCaptureActivity"

    const-string v7, "com.android.settings.faceunlock.MiuiNormalCameraMultiFaceInput"

    filled-new-array/range {v1 .. v7}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/camera/CameraActivitySceneMode;->mTargetActivityList:[Ljava/lang/String;

    .line 79
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/android/server/camera/CameraActivitySceneMode;->mTargetPackageList:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 37
    invoke-direct {p0}, Lcom/android/server/camera/CameraActivitySceneStub;-><init>()V

    .line 44
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/camera/CameraActivitySceneMode;->mListConfig:Ljava/util/Map;

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/camera/CameraActivitySceneMode;->mCameraVideocallList:Ljava/util/List;

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/camera/CameraActivitySceneMode;->mCameraHighBlobList:Ljava/util/List;

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/camera/CameraActivitySceneMode;->mCameraLiveList:Ljava/util/List;

    .line 80
    new-instance v0, Lcom/android/server/camera/CameraActivitySceneMode$1;

    invoke-direct {v0, p0}, Lcom/android/server/camera/CameraActivitySceneMode$1;-><init>(Lcom/android/server/camera/CameraActivitySceneMode;)V

    iput-object v0, p0, Lcom/android/server/camera/CameraActivitySceneMode;->mActivityChangeListener:Lmiui/process/IActivityChangeListener$Stub;

    return-void
.end method

.method private beginConfig(Ljava/lang/String;)V
    .locals 2
    .param p1, "content"    # Ljava/lang/String;

    .line 210
    const-string v0, "default"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 214
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/server/camera/CameraActivitySceneMode;->parseConfigListLocked(Ljava/lang/String;)V

    .line 215
    return-void

    .line 211
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/camera/CameraActivitySceneMode;->TAG:Ljava/lang/String;

    const-string v1, "json file not found or read fail!"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 212
    return-void
.end method

.method private checkFile(Ljava/lang/String;)Z
    .locals 3
    .param p1, "path"    # Ljava/lang/String;

    .line 228
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 229
    return v1

    .line 231
    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 232
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->canRead()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method private decideActivitySceneMode(Ljava/lang/String;)V
    .locals 8
    .param p1, "activityName"    # Ljava/lang/String;

    .line 118
    const-string v0, "persist.vendor.vcb.video"

    const-string v1, "false"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    const-string v2, "persist.vendor.vcb.snapshot"

    invoke-static {v2, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    const-string v3, "persist.vendor.camera.3rdhighResolutionBlob.scenes"

    invoke-static {v3, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    const-string v4, "persist.vendor.camera.3rdvideocall.scenes"

    invoke-static {v4, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    const-string v5, "persist.vendor.miface.registration.scenes"

    invoke-static {v5, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    const-string v6, "persist.vendor.camera.3rdlive.scenes"

    invoke-static {v6, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    const-string v1, "com.tencent.mm.plugin.voip.ui.VideoActivity"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    const-string/jumbo v7, "true"

    if-nez v1, :cond_0

    const-string v1, "com.tencent.mm.plugin.multitalk.ui.MultiTalkMainUI"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 125
    :cond_0
    invoke-static {v0, v7}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    :cond_1
    const-string v0, "com.tencent.mm.plugin.recordvideo.activity.MMRecordUI"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 128
    invoke-static {v2, v7}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    :cond_2
    const-string v0, "com.android.settings.faceunlock.MiuiNormalCameraMultiFaceInput"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 131
    invoke-static {v5, v7}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    :cond_3
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/android/server/camera/CameraActivitySceneMode;->mCameraVideocallList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_5

    .line 134
    iget-object v1, p0, Lcom/android/server/camera/CameraActivitySceneMode;->mCameraVideocallList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 135
    invoke-static {v4, v7}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    return-void

    .line 133
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 139
    .end local v0    # "i":I
    :cond_5
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_1
    iget-object v1, p0, Lcom/android/server/camera/CameraActivitySceneMode;->mCameraHighBlobList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_7

    .line 140
    iget-object v1, p0, Lcom/android/server/camera/CameraActivitySceneMode;->mCameraHighBlobList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 141
    invoke-static {v3, v7}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    return-void

    .line 139
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 145
    .end local v0    # "i":I
    :cond_7
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_2
    iget-object v1, p0, Lcom/android/server/camera/CameraActivitySceneMode;->mCameraLiveList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_9

    .line 146
    iget-object v1, p0, Lcom/android/server/camera/CameraActivitySceneMode;->mCameraLiveList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 147
    invoke-static {v6, v7}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    return-void

    .line 145
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 151
    .end local v0    # "i":I
    :cond_9
    return-void
.end method

.method public static getInstance()Lcom/android/server/camera/CameraActivitySceneMode;
    .locals 2

    .line 173
    sget-object v0, Lcom/android/server/camera/CameraActivitySceneMode;->sIntance:Lcom/android/server/camera/CameraActivitySceneMode;

    if-nez v0, :cond_1

    .line 174
    const-class v0, Lcom/android/server/camera/CameraActivitySceneMode;

    monitor-enter v0

    .line 175
    :try_start_0
    sget-object v1, Lcom/android/server/camera/CameraActivitySceneMode;->sIntance:Lcom/android/server/camera/CameraActivitySceneMode;

    if-nez v1, :cond_0

    .line 176
    new-instance v1, Lcom/android/server/camera/CameraActivitySceneMode;

    invoke-direct {v1}, Lcom/android/server/camera/CameraActivitySceneMode;-><init>()V

    sput-object v1, Lcom/android/server/camera/CameraActivitySceneMode;->sIntance:Lcom/android/server/camera/CameraActivitySceneMode;

    .line 178
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 180
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/camera/CameraActivitySceneMode;->sIntance:Lcom/android/server/camera/CameraActivitySceneMode;

    return-object v0
.end method

.method static synthetic lambda$readJSONFileToString$0(Ljava/lang/StringBuilder;Ljava/lang/String;)V
    .locals 2
    .param p0, "builder"    # Ljava/lang/StringBuilder;
    .param p1, "s"    # Ljava/lang/String;

    .line 220
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method

.method private parseConfigListLocked(Ljava/lang/String;)V
    .locals 11
    .param p1, "content"    # Ljava/lang/String;

    .line 185
    const-string v0, "config"

    :try_start_0
    const-string v1, "configs"

    .line 186
    .local v1, "tag":Ljava/lang/String;
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 187
    .local v2, "jsonObject":Lorg/json/JSONObject;
    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 188
    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 189
    .local v3, "arrays":Lorg/json/JSONArray;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v4, v5, :cond_3

    .line 190
    invoke-virtual {v3, v4}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 191
    .local v5, "obj":Lorg/json/JSONObject;
    if-nez v5, :cond_0

    goto :goto_2

    .line 192
    :cond_0
    const-string v6, "name"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 193
    .local v6, "name":Ljava/lang/String;
    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 194
    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v7

    .line 195
    .local v7, "values":Lorg/json/JSONArray;
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 196
    .local v8, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v9, 0x0

    .local v9, "j":I
    :goto_1
    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v10

    if-ge v9, v10, :cond_1

    .line 197
    invoke-virtual {v7, v9}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 198
    .local v10, "pkg":Ljava/lang/String;
    invoke-interface {v8, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 196
    nop

    .end local v10    # "pkg":Ljava/lang/String;
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 200
    .end local v9    # "j":I
    :cond_1
    iget-object v9, p0, Lcom/android/server/camera/CameraActivitySceneMode;->mListConfig:Ljava/util/Map;

    invoke-interface {v9, v6, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 189
    .end local v5    # "obj":Lorg/json/JSONObject;
    .end local v6    # "name":Ljava/lang/String;
    .end local v7    # "values":Lorg/json/JSONArray;
    .end local v8    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 206
    .end local v1    # "tag":Ljava/lang/String;
    .end local v2    # "jsonObject":Lorg/json/JSONObject;
    .end local v3    # "arrays":Lorg/json/JSONArray;
    .end local v4    # "i":I
    :cond_3
    goto :goto_3

    .line 204
    :catch_0
    move-exception v0

    .line 205
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 207
    .end local v0    # "e":Lorg/json/JSONException;
    :goto_3
    return-void
.end method

.method private parseJson()V
    .locals 4

    .line 236
    const-string v0, "default"

    .line 237
    .local v0, "content":Ljava/lang/String;
    const-string v1, "/system_ext/etc/camerascene.json"

    invoke-direct {p0, v1}, Lcom/android/server/camera/CameraActivitySceneMode;->checkFile(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 238
    sget-object v2, Lcom/android/server/camera/CameraActivitySceneMode;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "the default json file path is : /system_ext/etc/camerascene.json"

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    invoke-direct {p0, v1}, Lcom/android/server/camera/CameraActivitySceneMode;->readJSONFileToString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 240
    invoke-direct {p0, v0}, Lcom/android/server/camera/CameraActivitySceneMode;->beginConfig(Ljava/lang/String;)V

    .line 242
    :cond_0
    const-string v1, "/odm/etc/camera/camerascene.json"

    invoke-direct {p0, v1}, Lcom/android/server/camera/CameraActivitySceneMode;->checkFile(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 243
    sget-object v2, Lcom/android/server/camera/CameraActivitySceneMode;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "the odm json file path is : /odm/etc/camera/camerascene.json"

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    invoke-direct {p0, v1}, Lcom/android/server/camera/CameraActivitySceneMode;->readJSONFileToString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 245
    invoke-direct {p0, v0}, Lcom/android/server/camera/CameraActivitySceneMode;->beginConfig(Ljava/lang/String;)V

    .line 247
    :cond_1
    return-void
.end method

.method private readJSONFileToString(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .line 218
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 219
    .local v0, "builder":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    :try_start_0
    new-array v1, v1, [Ljava/lang/String;

    invoke-static {p1, v1}, Ljava/nio/file/Paths;->get(Ljava/lang/String;[Ljava/lang/String;)Ljava/nio/file/Path;

    move-result-object v1

    sget-object v2, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-static {v1, v2}, Ljava/nio/file/Files;->lines(Ljava/nio/file/Path;Ljava/nio/charset/Charset;)Ljava/util/stream/Stream;

    move-result-object v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 220
    .local v1, "stream":Ljava/util/stream/Stream;, "Ljava/util/stream/Stream<Ljava/lang/String;>;"
    :try_start_1
    new-instance v2, Lcom/android/server/camera/CameraActivitySceneMode$$ExternalSyntheticLambda0;

    invoke-direct {v2, v0}, Lcom/android/server/camera/CameraActivitySceneMode$$ExternalSyntheticLambda0;-><init>(Ljava/lang/StringBuilder;)V

    invoke-interface {v1, v2}, Ljava/util/stream/Stream;->forEach(Ljava/util/function/Consumer;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 221
    if-eqz v1, :cond_0

    :try_start_2
    invoke-interface {v1}, Ljava/util/stream/Stream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 223
    .end local v1    # "stream":Ljava/util/stream/Stream;, "Ljava/util/stream/Stream<Ljava/lang/String;>;"
    :cond_0
    goto :goto_1

    .line 219
    .restart local v1    # "stream":Ljava/util/stream/Stream;, "Ljava/util/stream/Stream<Ljava/lang/String;>;"
    :catchall_0
    move-exception v2

    if-eqz v1, :cond_1

    :try_start_3
    invoke-interface {v1}, Ljava/util/stream/Stream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v3

    :try_start_4
    invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "builder":Ljava/lang/StringBuilder;
    .end local p0    # "this":Lcom/android/server/camera/CameraActivitySceneMode;
    .end local p1    # "name":Ljava/lang/String;
    :cond_1
    :goto_0
    throw v2
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 221
    .end local v1    # "stream":Ljava/util/stream/Stream;, "Ljava/util/stream/Stream<Ljava/lang/String;>;"
    .restart local v0    # "builder":Ljava/lang/StringBuilder;
    .restart local p0    # "this":Lcom/android/server/camera/CameraActivitySceneMode;
    .restart local p1    # "name":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 222
    .local v1, "e":Ljava/io/IOException;
    sget-object v2, Lcom/android/server/camera/CameraActivitySceneMode;->TAG:Ljava/lang/String;

    const-string v3, "IOException"

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    .end local v1    # "e":Ljava/io/IOException;
    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private registerActivityChangeListener()V
    .locals 5

    .line 92
    iget-object v0, p0, Lcom/android/server/camera/CameraActivitySceneMode;->mActivityChangeListener:Lmiui/process/IActivityChangeListener$Stub;

    if-eqz v0, :cond_5

    .line 93
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 94
    .local v0, "targetActivities":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 96
    .local v1, "targetPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    sget-object v3, Lcom/android/server/camera/CameraActivitySceneMode;->mTargetActivityList:[Ljava/lang/String;

    array-length v4, v3

    if-ge v2, v4, :cond_0

    .line 97
    aget-object v3, v3, v2

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 96
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 99
    .end local v2    # "i":I
    :cond_0
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_1
    iget-object v3, p0, Lcom/android/server/camera/CameraActivitySceneMode;->mCameraVideocallList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 100
    iget-object v3, p0, Lcom/android/server/camera/CameraActivitySceneMode;->mCameraVideocallList:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 99
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 102
    .end local v2    # "i":I
    :cond_1
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_2
    iget-object v3, p0, Lcom/android/server/camera/CameraActivitySceneMode;->mCameraHighBlobList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 103
    iget-object v3, p0, Lcom/android/server/camera/CameraActivitySceneMode;->mCameraHighBlobList:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 102
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 105
    .end local v2    # "i":I
    :cond_2
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_3
    iget-object v3, p0, Lcom/android/server/camera/CameraActivitySceneMode;->mCameraLiveList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_3

    .line 106
    iget-object v3, p0, Lcom/android/server/camera/CameraActivitySceneMode;->mCameraLiveList:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 105
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 109
    .end local v2    # "i":I
    :cond_3
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_4
    sget-object v3, Lcom/android/server/camera/CameraActivitySceneMode;->mTargetPackageList:[Ljava/lang/String;

    array-length v4, v3

    if-ge v2, v4, :cond_4

    .line 110
    aget-object v3, v3, v2

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 109
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 112
    .end local v2    # "i":I
    :cond_4
    iget-object v2, p0, Lcom/android/server/camera/CameraActivitySceneMode;->mActivityChangeListener:Lmiui/process/IActivityChangeListener$Stub;

    invoke-static {v1, v0, v2}, Lmiui/process/ProcessManager;->registerActivityChangeListener(Ljava/util/List;Ljava/util/List;Lmiui/process/IActivityChangeListener;)V

    .line 115
    .end local v0    # "targetActivities":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v1    # "targetPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_5
    return-void
.end method

.method private updateActivityList()V
    .locals 2

    .line 167
    iget-object v0, p0, Lcom/android/server/camera/CameraActivitySceneMode;->mListConfig:Ljava/util/Map;

    const-string/jumbo v1, "videocall_list"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/android/server/camera/CameraActivitySceneMode;->mCameraVideocallList:Ljava/util/List;

    .line 168
    iget-object v0, p0, Lcom/android/server/camera/CameraActivitySceneMode;->mListConfig:Ljava/util/Map;

    const-string v1, "highresolutionblob_list"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/android/server/camera/CameraActivitySceneMode;->mCameraHighBlobList:Ljava/util/List;

    .line 169
    iget-object v0, p0, Lcom/android/server/camera/CameraActivitySceneMode;->mListConfig:Ljava/util/Map;

    const-string/jumbo v1, "tplive_list"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/android/server/camera/CameraActivitySceneMode;->mCameraLiveList:Ljava/util/List;

    .line 170
    return-void
.end method


# virtual methods
.method public initSystemBooted()V
    .locals 2

    .line 154
    const-string v0, "persist.vendor.vcb.video"

    const-string v1, "false"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    const-string v0, "persist.vendor.vcb.snapshot"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    const-string v0, "persist.vendor.camera.3rdhighResolutionBlob.scenes"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    const-string v0, "persist.vendor.camera.3rdvideocall.scenes"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    const-string v0, "persist.vendor.miface.registration.scenes"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    const-string v0, "persist.vendor.camera.3rdlive.scenes"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "persist.vendor.vcb.activity"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    invoke-direct {p0}, Lcom/android/server/camera/CameraActivitySceneMode;->parseJson()V

    .line 162
    invoke-direct {p0}, Lcom/android/server/camera/CameraActivitySceneMode;->updateActivityList()V

    .line 163
    invoke-direct {p0}, Lcom/android/server/camera/CameraActivitySceneMode;->registerActivityChangeListener()V

    .line 164
    return-void
.end method
