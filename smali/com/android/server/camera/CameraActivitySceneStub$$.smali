.class public final Lcom/android/server/camera/CameraActivitySceneStub$$;
.super Ljava/lang/Object;
.source "CameraActivitySceneStub$$.java"

# interfaces
.implements Lcom/miui/base/MiuiStubRegistry$ImplProviderManifest;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final collectImplProviders(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/miui/base/MiuiStubRegistry$ImplProvider<",
            "*>;>;)V"
        }
    .end annotation

    .line 19
    .local p1, "outProviders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/miui/base/MiuiStubRegistry$ImplProvider<*>;>;"
    new-instance v0, Lcom/android/server/camera/CameraOpt$Provider;

    invoke-direct {v0}, Lcom/android/server/camera/CameraOpt$Provider;-><init>()V

    const-string v1, "com.android.server.camera.CameraOptStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    new-instance v0, Lcom/android/server/camera/CameraActivitySceneMode$Provider;

    invoke-direct {v0}, Lcom/android/server/camera/CameraActivitySceneMode$Provider;-><init>()V

    const-string v1, "com.android.server.camera.CameraActivitySceneStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    new-instance v0, Lcom/android/server/wm/DisplayRotationStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/DisplayRotationStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.wm.DisplayRotationStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    return-void
.end method
