public class com.android.server.camera.CameraTrackInjector implements com.android.server.camera.CameraTrackExt {
	 /* .source "CameraTrackInjector.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String APP_ID;
private static final java.lang.String CAMERAUSAGE_API;
private static final java.lang.String CAMERAUSAGE_CLIENT_NAME;
private static final java.lang.String CAMERAUSAGE_DURATION;
private static final java.lang.String CAMERAUSAGE_EVENT_NAME;
private static final java.lang.String CAMERAUSAGE_ID;
private static final Integer CAMERA_STATE_CLOSED;
private static final Integer CAMERA_STATE_OPEN;
private static final java.lang.String PACKAGE;
private static final java.lang.String TAG;
/* # instance fields */
private android.util.ArrayMap mActiveCameraUsage;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArrayMap<", */
/* "Ljava/lang/String;", */
/* "Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.content.Context mContext;
/* # direct methods */
static com.android.server.camera.CameraTrackInjector ( ) {
/* .locals 1 */
/* .line 18 */
/* const-class v0, Lcom/android/server/camera/CameraTrackInjector; */
(( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
return;
} // .end method
public com.android.server.camera.CameraTrackInjector ( ) {
/* .locals 1 */
/* .line 17 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 20 */
/* new-instance v0, Landroid/util/ArrayMap; */
/* invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V */
this.mActiveCameraUsage = v0;
return;
} // .end method
private void trackCamera ( com.android.server.camera.CameraTrackInjector$CameraUsageBean p0 ) {
/* .locals 7 */
/* .param p1, "bean" # Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean; */
/* .line 107 */
v0 = this.mContext;
/* if-nez v0, :cond_0 */
/* .line 108 */
return;
/* .line 110 */
} // :cond_0
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 112 */
/* .local v0, "ident":J */
try { // :try_start_0
/* new-instance v2, Landroid/content/Intent; */
final String v3 = "onetrack.action.TRACK_EVENT"; // const-string v3, "onetrack.action.TRACK_EVENT"
/* invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 113 */
/* .local v2, "intent":Landroid/content/Intent; */
final String v3 = "com.miui.analytics"; // const-string v3, "com.miui.analytics"
(( android.content.Intent ) v2 ).setPackage ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 114 */
final String v3 = "APP_ID"; // const-string v3, "APP_ID"
final String v4 = "31000000285"; // const-string v4, "31000000285"
(( android.content.Intent ) v2 ).putExtra ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 115 */
final String v3 = "PACKAGE"; // const-string v3, "PACKAGE"
final String v4 = "com.android.camera"; // const-string v4, "com.android.camera"
(( android.content.Intent ) v2 ).putExtra ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 116 */
final String v3 = "EVENT_NAME"; // const-string v3, "EVENT_NAME"
final String v4 = "key_camerausage"; // const-string v4, "key_camerausage"
(( android.content.Intent ) v2 ).putExtra ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 117 */
/* new-instance v3, Landroid/os/Bundle; */
/* invoke-direct {v3}, Landroid/os/Bundle;-><init>()V */
/* .line 118 */
/* .local v3, "params":Landroid/os/Bundle; */
final String v4 = "attr_camerausage_client_name"; // const-string v4, "attr_camerausage_client_name"
v5 = this.mClientName;
(( android.os.Bundle ) v3 ).putString ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 119 */
final String v4 = "attr_camerausage_id"; // const-string v4, "attr_camerausage_id"
v5 = this.mCameraId;
(( android.os.Bundle ) v3 ).putString ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 120 */
final String v4 = "attr_camerausage_duration"; // const-string v4, "attr_camerausage_duration"
(( com.android.server.camera.CameraTrackInjector$CameraUsageBean ) p1 ).getDuration ( ); // invoke-virtual {p1}, Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;->getDuration()J
/* move-result-wide v5 */
(( android.os.Bundle ) v3 ).putLong ( v4, v5, v6 ); // invoke-virtual {v3, v4, v5, v6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V
/* .line 121 */
final String v4 = "attr_camerausage_api"; // const-string v4, "attr_camerausage_api"
/* iget v5, p1, Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;->mCameraAPI:I */
(( android.os.Bundle ) v3 ).putInt ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 122 */
(( android.content.Intent ) v2 ).putExtras ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;
/* .line 124 */
v4 = this.mContext;
v5 = android.os.UserHandle.CURRENT;
(( android.content.Context ) v4 ).startServiceAsUser ( v2, v5 ); // invoke-virtual {v4, v2, v5}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 128 */
/* nop */
} // .end local v2 # "intent":Landroid/content/Intent;
} // .end local v3 # "params":Landroid/os/Bundle;
/* :catchall_0 */
/* move-exception v2 */
/* .line 125 */
/* :catch_0 */
/* move-exception v2 */
/* .line 126 */
/* .local v2, "e":Ljava/lang/Exception; */
try { // :try_start_1
v3 = com.android.server.camera.CameraTrackInjector.TAG;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "start onetrack service : " */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v3,v4 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 128 */
/* nop */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_0
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 129 */
/* nop */
/* .line 130 */
return;
/* .line 128 */
} // :goto_1
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 129 */
/* throw v2 */
} // .end method
/* # virtual methods */
public void trackCameraState ( android.content.Context p0, java.lang.String p1, Integer p2, java.lang.String p3, Integer p4 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "cameraId" # Ljava/lang/String; */
/* .param p3, "newCameraState" # I */
/* .param p4, "clientName" # Ljava/lang/String; */
/* .param p5, "apiLevel" # I */
/* .line 76 */
if ( p4 != null) { // if-eqz p4, :cond_0
final String v0 = "com.android.camera"; // const-string v0, "com.android.camera"
v0 = (( java.lang.String ) p4 ).equals ( v0 ); // invoke-virtual {p4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 77 */
return;
/* .line 79 */
} // :cond_0
this.mContext = p1;
/* .line 81 */
final String v0 = "_"; // const-string v0, "_"
/* packed-switch p3, :pswitch_data_0 */
/* .line 94 */
/* :pswitch_0 */
v1 = this.mActiveCameraUsage;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( p4 ); // invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( android.util.ArrayMap ) v1 ).remove ( v0 ); // invoke-virtual {v1, v0}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean; */
/* .line 95 */
/* .local v0, "cameraUsageBean":Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean; */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 96 */
(( com.android.server.camera.CameraTrackInjector$CameraUsageBean ) v0 ).markCompleted ( ); // invoke-virtual {v0}, Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;->markCompleted()V
/* .line 97 */
/* invoke-direct {p0, v0}, Lcom/android/server/camera/CameraTrackInjector;->trackCamera(Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;)V */
/* .line 83 */
} // .end local v0 # "cameraUsageBean":Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;
/* :pswitch_1 */
v1 = this.mActiveCameraUsage;
v1 = (( android.util.ArrayMap ) v1 ).size ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->size()I
/* const/16 v2, 0x64 */
/* if-le v1, v2, :cond_1 */
/* .line 84 */
v1 = this.mActiveCameraUsage;
(( android.util.ArrayMap ) v1 ).clear ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->clear()V
/* .line 86 */
} // :cond_1
/* new-instance v1, Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean; */
/* invoke-direct {v1, p4, p2, p5}, Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;-><init>(Ljava/lang/String;Ljava/lang/String;I)V */
/* .line 87 */
/* .local v1, "cameraUsageBean":Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean; */
v2 = this.mActiveCameraUsage;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( p4 ); // invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( android.util.ArrayMap ) v2 ).put ( v0, v1 ); // invoke-virtual {v2, v0, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean; */
/* .line 88 */
/* .local v0, "oldBean":Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean; */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 89 */
(( com.android.server.camera.CameraTrackInjector$CameraUsageBean ) v0 ).markCompleted ( ); // invoke-virtual {v0}, Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;->markCompleted()V
/* .line 90 */
/* invoke-direct {p0, v0}, Lcom/android/server/camera/CameraTrackInjector;->trackCamera(Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;)V */
/* .line 104 */
} // .end local v0 # "oldBean":Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;
} // .end local v1 # "cameraUsageBean":Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;
} // :cond_2
} // :goto_0
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
