.class public Lcom/android/server/camera/CameraTrackInjector;
.super Ljava/lang/Object;
.source "CameraTrackInjector.java"

# interfaces
.implements Lcom/android/server/camera/CameraTrackExt;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;
    }
.end annotation


# static fields
.field private static final APP_ID:Ljava/lang/String; = "31000000285"

.field private static final CAMERAUSAGE_API:Ljava/lang/String; = "attr_camerausage_api"

.field private static final CAMERAUSAGE_CLIENT_NAME:Ljava/lang/String; = "attr_camerausage_client_name"

.field private static final CAMERAUSAGE_DURATION:Ljava/lang/String; = "attr_camerausage_duration"

.field private static final CAMERAUSAGE_EVENT_NAME:Ljava/lang/String; = "key_camerausage"

.field private static final CAMERAUSAGE_ID:Ljava/lang/String; = "attr_camerausage_id"

.field private static final CAMERA_STATE_CLOSED:I = 0x1

.field private static final CAMERA_STATE_OPEN:I = 0x0

.field private static final PACKAGE:Ljava/lang/String; = "com.android.camera"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mActiveCameraUsage:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 18
    const-class v0, Lcom/android/server/camera/CameraTrackInjector;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/camera/CameraTrackInjector;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/camera/CameraTrackInjector;->mActiveCameraUsage:Landroid/util/ArrayMap;

    return-void
.end method

.method private trackCamera(Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;)V
    .locals 7
    .param p1, "bean"    # Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;

    .line 107
    iget-object v0, p0, Lcom/android/server/camera/CameraTrackInjector;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 108
    return-void

    .line 110
    :cond_0
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 112
    .local v0, "ident":J
    :try_start_0
    new-instance v2, Landroid/content/Intent;

    const-string v3, "onetrack.action.TRACK_EVENT"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 113
    .local v2, "intent":Landroid/content/Intent;
    const-string v3, "com.miui.analytics"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 114
    const-string v3, "APP_ID"

    const-string v4, "31000000285"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 115
    const-string v3, "PACKAGE"

    const-string v4, "com.android.camera"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 116
    const-string v3, "EVENT_NAME"

    const-string v4, "key_camerausage"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 117
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 118
    .local v3, "params":Landroid/os/Bundle;
    const-string v4, "attr_camerausage_client_name"

    iget-object v5, p1, Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;->mClientName:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    const-string v4, "attr_camerausage_id"

    iget-object v5, p1, Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;->mCameraId:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    const-string v4, "attr_camerausage_duration"

    invoke-virtual {p1}, Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;->getDuration()J

    move-result-wide v5

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 121
    const-string v4, "attr_camerausage_api"

    iget v5, p1, Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;->mCameraAPI:I

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 122
    invoke-virtual {v2, v3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 124
    iget-object v4, p0, Lcom/android/server/camera/CameraTrackInjector;->mContext:Landroid/content/Context;

    sget-object v5, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v4, v2, v5}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 128
    nop

    .end local v2    # "intent":Landroid/content/Intent;
    .end local v3    # "params":Landroid/os/Bundle;
    goto :goto_0

    :catchall_0
    move-exception v2

    goto :goto_1

    .line 125
    :catch_0
    move-exception v2

    .line 126
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v3, Lcom/android/server/camera/CameraTrackInjector;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "start onetrack service : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 128
    nop

    .end local v2    # "e":Ljava/lang/Exception;
    :goto_0
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 129
    nop

    .line 130
    return-void

    .line 128
    :goto_1
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 129
    throw v2
.end method


# virtual methods
.method public trackCameraState(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cameraId"    # Ljava/lang/String;
    .param p3, "newCameraState"    # I
    .param p4, "clientName"    # Ljava/lang/String;
    .param p5, "apiLevel"    # I

    .line 76
    if-eqz p4, :cond_0

    const-string v0, "com.android.camera"

    invoke-virtual {p4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    return-void

    .line 79
    :cond_0
    iput-object p1, p0, Lcom/android/server/camera/CameraTrackInjector;->mContext:Landroid/content/Context;

    .line 81
    const-string v0, "_"

    packed-switch p3, :pswitch_data_0

    goto :goto_0

    .line 94
    :pswitch_0
    iget-object v1, p0, Lcom/android/server/camera/CameraTrackInjector;->mActiveCameraUsage:Landroid/util/ArrayMap;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;

    .line 95
    .local v0, "cameraUsageBean":Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;
    if-eqz v0, :cond_2

    .line 96
    invoke-virtual {v0}, Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;->markCompleted()V

    .line 97
    invoke-direct {p0, v0}, Lcom/android/server/camera/CameraTrackInjector;->trackCamera(Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;)V

    goto :goto_0

    .line 83
    .end local v0    # "cameraUsageBean":Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;
    :pswitch_1
    iget-object v1, p0, Lcom/android/server/camera/CameraTrackInjector;->mActiveCameraUsage:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->size()I

    move-result v1

    const/16 v2, 0x64

    if-le v1, v2, :cond_1

    .line 84
    iget-object v1, p0, Lcom/android/server/camera/CameraTrackInjector;->mActiveCameraUsage:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->clear()V

    .line 86
    :cond_1
    new-instance v1, Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;

    invoke-direct {v1, p4, p2, p5}, Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    .line 87
    .local v1, "cameraUsageBean":Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;
    iget-object v2, p0, Lcom/android/server/camera/CameraTrackInjector;->mActiveCameraUsage:Landroid/util/ArrayMap;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;

    .line 88
    .local v0, "oldBean":Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;
    if-eqz v0, :cond_2

    .line 89
    invoke-virtual {v0}, Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;->markCompleted()V

    .line 90
    invoke-direct {p0, v0}, Lcom/android/server/camera/CameraTrackInjector;->trackCamera(Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;)V

    .line 104
    .end local v0    # "oldBean":Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;
    .end local v1    # "cameraUsageBean":Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;
    :cond_2
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
