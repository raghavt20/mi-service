.class public Lcom/android/server/camera/CameraOpt;
.super Ljava/lang/Object;
.source "CameraOpt.java"

# interfaces
.implements Lcom/android/server/camera/CameraOptStub;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/camera/CameraOpt$MethodInfo;
    }
.end annotation


# static fields
.field private static IS_SUPPORT_ABIS:Z = false

.field private static final TAG:Ljava/lang/String; = "CameraOpt"

.field private static mCameraOptManager:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field

.field private static volatile mMethodInfoMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/android/server/camera/CameraOpt$MethodInfo;",
            "Ljava/lang/reflect/Method;",
            ">;"
        }
    .end annotation
.end field

.field private static sMethodInfo:Lcom/android/server/camera/CameraOpt$MethodInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 24
    new-instance v0, Lcom/android/server/camera/CameraOpt$MethodInfo;

    invoke-direct {v0}, Lcom/android/server/camera/CameraOpt$MethodInfo;-><init>()V

    sput-object v0, Lcom/android/server/camera/CameraOpt;->sMethodInfo:Lcom/android/server/camera/CameraOpt$MethodInfo;

    .line 25
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/server/camera/CameraOpt;->mMethodInfoMap:Ljava/util/Map;

    .line 30
    const-string v0, "/system_ext/framework/miui-cameraopt.jar"

    .line 31
    .local v0, "jarPath":Ljava/lang/String;
    const-string v1, "com.miui.cameraopt.CameraOptManager"

    .line 33
    .local v1, "cameraOptManagerClazzName":Ljava/lang/String;
    :try_start_0
    sget-object v2, Landroid/os/Build;->SUPPORTED_ABIS:[Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    if-eqz v2, :cond_0

    sget-object v2, Landroid/os/Build;->SUPPORTED_ABIS:[Ljava/lang/String;

    aget-object v2, v2, v3

    const-string v3, "arm"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    :cond_0
    sput-boolean v3, Lcom/android/server/camera/CameraOpt;->IS_SUPPORT_ABIS:Z

    .line 34
    const-class v2, Lcom/android/server/camera/CameraOpt;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    check-cast v2, Ldalvik/system/PathClassLoader;

    .line 35
    .local v2, "pathClassLoader":Ldalvik/system/PathClassLoader;
    invoke-virtual {v2, v0}, Ldalvik/system/PathClassLoader;->addDexPath(Ljava/lang/String;)V

    .line 36
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    sput-object v3, Lcom/android/server/camera/CameraOpt;->mCameraOptManager:Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 39
    .end local v2    # "pathClassLoader":Ldalvik/system/PathClassLoader;
    goto :goto_0

    .line 37
    :catch_0
    move-exception v2

    .line 38
    .local v2, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ClassNotFound from "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "CameraOpt"

    invoke-static {v4, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    .end local v0    # "jarPath":Ljava/lang/String;
    .end local v1    # "cameraOptManagerClazzName":Ljava/lang/String;
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static varargs callMethod(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p0, "funcName"    # Ljava/lang/String;
    .param p1, "values"    # [Ljava/lang/Object;

    .line 80
    sget-boolean v0, Lcom/android/server/camera/CameraOpt;->IS_SUPPORT_ABIS:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 81
    return-object v1

    .line 83
    :cond_0
    sget-object v0, Lcom/android/server/camera/CameraOpt;->mCameraOptManager:Ljava/lang/Class;

    if-nez v0, :cond_1

    .line 84
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "call methods("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") failed, because cameraopt was null."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "CameraOpt"

    invoke-static {v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    return-object v1

    .line 87
    :cond_1
    invoke-static {v0, p0, p1}, Lcom/android/server/camera/CameraOpt;->callStaticMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static varargs callStaticMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p0, "clazz"    # Ljava/lang/Class;
    .param p1, "funcName"    # Ljava/lang/String;
    .param p2, "values"    # [Ljava/lang/Object;

    .line 92
    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    .line 93
    :cond_0
    :try_start_0
    invoke-static {p2}, Lcom/android/server/camera/CameraOpt;->getParameterTypes([Ljava/lang/Object;)[Ljava/lang/Class;

    move-result-object v1

    invoke-static {p0, p1, v1}, Lcom/android/server/camera/CameraOpt;->getFuncMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 94
    .local v1, "declaredMethod":Ljava/lang/reflect/Method;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 95
    if-eqz p2, :cond_2

    array-length v2, p2

    if-gtz v2, :cond_1

    goto :goto_0

    .line 98
    :cond_1
    invoke-virtual {v1, v0, p2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 96
    :cond_2
    :goto_0
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 99
    .end local v1    # "declaredMethod":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v1

    .line 100
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "call methods("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") failed :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "CameraOpt"

    invoke-static {v3, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    return-object v0
.end method

.method private static getDeclaredMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    .locals 4
    .param p0, "clazz"    # Ljava/lang/Class;
    .param p1, "methodName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/Class<",
            "*>;)",
            "Ljava/lang/reflect/Method;"
        }
    .end annotation

    .line 43
    .local p2, "paramsTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    const/4 v0, 0x0

    .line 45
    .local v0, "declaredMethod":Ljava/lang/reflect/Method;
    if-eqz p2, :cond_1

    :try_start_0
    array-length v1, p2

    if-gtz v1, :cond_0

    goto :goto_0

    .line 48
    :cond_0
    invoke-virtual {p0, p1, p2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    move-object v0, v1

    goto :goto_1

    .line 46
    :cond_1
    :goto_0
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Class;

    invoke-virtual {p0, p1, v1}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 52
    :goto_1
    goto :goto_2

    .line 50
    :catch_0
    move-exception v1

    .line 51
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getDeclaredMethod:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " failed"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "CameraOpt"

    invoke-static {v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_2
    return-object v0
.end method

.method private static getFuncMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    .locals 5
    .param p0, "clazz"    # Ljava/lang/Class;
    .param p1, "methodName"    # Ljava/lang/String;
    .param p2, "paramsTypes"    # [Ljava/lang/Class;

    .line 57
    sget-object v0, Lcom/android/server/camera/CameraOpt;->sMethodInfo:Lcom/android/server/camera/CameraOpt$MethodInfo;

    monitor-enter v0

    .line 58
    :try_start_0
    sget-object v1, Lcom/android/server/camera/CameraOpt;->sMethodInfo:Lcom/android/server/camera/CameraOpt$MethodInfo;

    invoke-virtual {v1, p0}, Lcom/android/server/camera/CameraOpt$MethodInfo;->setClass(Ljava/lang/Class;)Lcom/android/server/camera/CameraOpt$MethodInfo;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/android/server/camera/CameraOpt$MethodInfo;->setMethodName(Ljava/lang/String;)Lcom/android/server/camera/CameraOpt$MethodInfo;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/android/server/camera/CameraOpt$MethodInfo;->setParmasClass([Ljava/lang/Class;)Lcom/android/server/camera/CameraOpt$MethodInfo;

    .line 59
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 61
    const/4 v0, 0x0

    :try_start_1
    sget-object v1, Lcom/android/server/camera/CameraOpt;->mMethodInfoMap:Ljava/util/Map;

    sget-object v2, Lcom/android/server/camera/CameraOpt;->sMethodInfo:Lcom/android/server/camera/CameraOpt$MethodInfo;

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 62
    sget-object v1, Lcom/android/server/camera/CameraOpt;->mMethodInfoMap:Ljava/util/Map;

    sget-object v2, Lcom/android/server/camera/CameraOpt;->sMethodInfo:Lcom/android/server/camera/CameraOpt$MethodInfo;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/reflect/Method;

    return-object v1

    .line 64
    :cond_0
    invoke-static {p0, p1, p2}, Lcom/android/server/camera/CameraOpt;->getDeclaredMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 65
    .local v1, "method":Ljava/lang/reflect/Method;
    if-nez v1, :cond_1

    .line 66
    return-object v0

    .line 68
    :cond_1
    sget-object v2, Lcom/android/server/camera/CameraOpt;->mMethodInfoMap:Ljava/util/Map;

    monitor-enter v2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 69
    :try_start_2
    sget-object v3, Lcom/android/server/camera/CameraOpt;->mMethodInfoMap:Ljava/util/Map;

    new-instance v4, Lcom/android/server/camera/CameraOpt$MethodInfo;

    invoke-direct {v4, p0, p1, p2}, Lcom/android/server/camera/CameraOpt$MethodInfo;-><init>(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    invoke-interface {v3, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    monitor-exit v2

    .line 71
    return-object v1

    .line 70
    :catchall_0
    move-exception v3

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .end local p0    # "clazz":Ljava/lang/Class;
    .end local p1    # "methodName":Ljava/lang/String;
    .end local p2    # "paramsTypes":[Ljava/lang/Class;
    :try_start_3
    throw v3
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    .line 73
    .end local v1    # "method":Ljava/lang/reflect/Method;
    .restart local p0    # "clazz":Ljava/lang/Class;
    .restart local p1    # "methodName":Ljava/lang/String;
    .restart local p2    # "paramsTypes":[Ljava/lang/Class;
    :catch_0
    move-exception v1

    .line 74
    .local v1, "ex":Ljava/lang/Exception;
    const-string v2, "CameraOpt"

    const-string v3, "getFuncMethod Fail!"

    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    return-object v0

    .line 59
    .end local v1    # "ex":Ljava/lang/Exception;
    :catchall_1
    move-exception v1

    :try_start_4
    monitor-exit v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v1
.end method

.method private static varargs getParameterTypes([Ljava/lang/Object;)[Ljava/lang/Class;
    .locals 6
    .param p0, "args"    # [Ljava/lang/Object;

    .line 106
    const/4 v0, 0x0

    if-nez p0, :cond_0

    .line 107
    return-object v0

    .line 109
    :cond_0
    array-length v1, p0

    new-array v1, v1, [Ljava/lang/Class;

    .line 110
    .local v1, "parameterTypes":[Ljava/lang/Class;
    const/4 v2, 0x0

    .local v2, "i":I
    array-length v3, p0

    .local v3, "j":I
    :goto_0
    if-ge v2, v3, :cond_7

    .line 111
    aget-object v4, p0, v2

    if-nez v4, :cond_1

    .line 112
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "the params of method is null, so return. index : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "CameraOpt"

    invoke-static {v5, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    return-object v0

    .line 115
    :cond_1
    aget-object v4, p0, v2

    instance-of v4, v4, Ljava/lang/Integer;

    if-eqz v4, :cond_2

    .line 116
    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v1, v2

    goto :goto_1

    .line 117
    :cond_2
    aget-object v4, p0, v2

    instance-of v4, v4, Ljava/lang/Double;

    if-eqz v4, :cond_3

    .line 118
    sget-object v4, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    aput-object v4, v1, v2

    goto :goto_1

    .line 119
    :cond_3
    aget-object v4, p0, v2

    instance-of v4, v4, Ljava/lang/Long;

    if-eqz v4, :cond_4

    .line 120
    sget-object v4, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v4, v1, v2

    goto :goto_1

    .line 121
    :cond_4
    aget-object v4, p0, v2

    instance-of v4, v4, Ljava/lang/Boolean;

    if-eqz v4, :cond_5

    .line 122
    sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v4, v1, v2

    goto :goto_1

    .line 123
    :cond_5
    aget-object v4, p0, v2

    instance-of v4, v4, Landroid/content/Context;

    if-eqz v4, :cond_6

    .line 124
    const-class v4, Landroid/content/Context;

    aput-object v4, v1, v2

    goto :goto_1

    .line 126
    :cond_6
    aget-object v4, p0, v2

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    aput-object v4, v1, v2

    .line 110
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 129
    .end local v2    # "i":I
    .end local v3    # "j":I
    :cond_7
    return-object v1
.end method

.method private isNeedBoostCamera(Landroid/content/Intent;)Z
    .locals 5
    .param p1, "in"    # Landroid/content/Intent;

    .line 151
    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 152
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    .line 153
    .local v1, "comp":Landroid/content/ComponentName;
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 154
    .local v2, "action":Ljava/lang/String;
    if-eqz v1, :cond_4

    if-nez v2, :cond_1

    goto :goto_1

    .line 155
    :cond_1
    invoke-virtual {v1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.android.camera/.Camera"

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 156
    invoke-virtual {v1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.android.camera/.VoiceCamera"

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    .line 159
    :cond_2
    return v0

    .line 157
    :cond_3
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 154
    :cond_4
    :goto_1
    return v0
.end method


# virtual methods
.method public boostCameraByThreshold(Landroid/content/Intent;)V
    .locals 2
    .param p1, "in"    # Landroid/content/Intent;

    .line 145
    invoke-direct {p0, p1}, Lcom/android/server/camera/CameraOpt;->isNeedBoostCamera(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    filled-new-array {v0}, [Ljava/lang/Object;

    move-result-object v0

    const-string v1, "boostCameraByThreshold"

    invoke-static {v1, v0}, Lcom/android/server/camera/CameraOpt;->callMethod(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    :cond_0
    return-void
.end method

.method public interceptAppRestartIfNeeded(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1, "processName"    # Ljava/lang/String;
    .param p2, "type"    # Ljava/lang/String;

    .line 139
    const-string v0, "interceptAppRestartIfNeeded"

    filled-new-array {p1, p2}, [Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/camera/CameraOpt;->callMethod(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 140
    .local v0, "result":Ljava/lang/Object;
    if-eqz v0, :cond_0

    move-object v1, v0

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public onTransitionAnimateStateChanged(Z)V
    .locals 2
    .param p1, "isAnimationStart"    # Z

    .line 134
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    filled-new-array {v0}, [Ljava/lang/Object;

    move-result-object v0

    const-string v1, "onTransitionAnimateStateChanged"

    invoke-static {v1, v0}, Lcom/android/server/camera/CameraOpt;->callMethod(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    return-void
.end method
