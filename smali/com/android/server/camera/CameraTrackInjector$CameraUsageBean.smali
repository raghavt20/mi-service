.class Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;
.super Ljava/lang/Object;
.source "CameraTrackInjector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/camera/CameraTrackInjector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CameraUsageBean"
.end annotation


# instance fields
.field public mCameraAPI:I

.field public mCameraId:Ljava/lang/String;

.field public mClientName:Ljava/lang/String;

.field private mCompleted:Z

.field private mDurationOrStartTimeMs:J


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2
    .param p1, "clientName"    # Ljava/lang/String;
    .param p2, "cameraId"    # Ljava/lang/String;
    .param p3, "apiLevel"    # I

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p2, p0, Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;->mCameraId:Ljava/lang/String;

    .line 43
    iput-object p1, p0, Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;->mClientName:Ljava/lang/String;

    .line 44
    iput p3, p0, Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;->mCameraAPI:I

    .line 45
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;->mDurationOrStartTimeMs:J

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;->mCompleted:Z

    .line 47
    return-void
.end method


# virtual methods
.method public getDuration()J
    .locals 4

    .line 61
    iget-boolean v0, p0, Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;->mCompleted:Z

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;->mDurationOrStartTimeMs:J

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0
.end method

.method public markCompleted()V
    .locals 4

    .line 50
    iget-boolean v0, p0, Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;->mCompleted:Z

    if-eqz v0, :cond_0

    .line 51
    return-void

    .line 53
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;->mCompleted:Z

    .line 54
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;->mDurationOrStartTimeMs:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;->mDurationOrStartTimeMs:J

    .line 55
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 65
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CameraUsageBean(clientName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;->mClientName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",CameraAPI="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;->mCameraAPI:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",cameraId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;->mCameraId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",durationOrStartTimeS="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 69
    invoke-virtual {p0}, Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;->getDuration()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 65
    return-object v0
.end method
