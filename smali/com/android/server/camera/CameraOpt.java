public class com.android.server.camera.CameraOpt implements com.android.server.camera.CameraOptStub {
	 /* .source "CameraOpt.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/camera/CameraOpt$MethodInfo; */
	 /* } */
} // .end annotation
/* # static fields */
private static Boolean IS_SUPPORT_ABIS;
private static final java.lang.String TAG;
private static java.lang.Class mCameraOptManager;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/Class<", */
/* "*>;" */
/* } */
} // .end annotation
} // .end field
private static volatile java.util.Map mMethodInfoMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Lcom/android/server/camera/CameraOpt$MethodInfo;", */
/* "Ljava/lang/reflect/Method;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static com.android.server.camera.CameraOpt$MethodInfo sMethodInfo;
/* # direct methods */
static com.android.server.camera.CameraOpt ( ) {
/* .locals 5 */
/* .line 24 */
/* new-instance v0, Lcom/android/server/camera/CameraOpt$MethodInfo; */
/* invoke-direct {v0}, Lcom/android/server/camera/CameraOpt$MethodInfo;-><init>()V */
/* .line 25 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* .line 30 */
final String v0 = "/system_ext/framework/miui-cameraopt.jar"; // const-string v0, "/system_ext/framework/miui-cameraopt.jar"
/* .line 31 */
/* .local v0, "jarPath":Ljava/lang/String; */
final String v1 = "com.miui.cameraopt.CameraOptManager"; // const-string v1, "com.miui.cameraopt.CameraOptManager"
/* .line 33 */
/* .local v1, "cameraOptManagerClazzName":Ljava/lang/String; */
try { // :try_start_0
v2 = android.os.Build.SUPPORTED_ABIS;
int v3 = 0; // const/4 v3, 0x0
/* aget-object v2, v2, v3 */
if ( v2 != null) { // if-eqz v2, :cond_0
v2 = android.os.Build.SUPPORTED_ABIS;
/* aget-object v2, v2, v3 */
final String v3 = "arm"; // const-string v3, "arm"
v3 = (( java.lang.String ) v2 ).startsWith ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
} // :cond_0
com.android.server.camera.CameraOpt.IS_SUPPORT_ABIS = (v3!= 0);
/* .line 34 */
/* const-class v2, Lcom/android/server/camera/CameraOpt; */
(( java.lang.Class ) v2 ).getClassLoader ( ); // invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;
/* check-cast v2, Ldalvik/system/PathClassLoader; */
/* .line 35 */
/* .local v2, "pathClassLoader":Ldalvik/system/PathClassLoader; */
(( dalvik.system.PathClassLoader ) v2 ).addDexPath ( v0 ); // invoke-virtual {v2, v0}, Ldalvik/system/PathClassLoader;->addDexPath(Ljava/lang/String;)V
/* .line 36 */
java.lang.Class .forName ( v1 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 39 */
} // .end local v2 # "pathClassLoader":Ldalvik/system/PathClassLoader;
/* .line 37 */
/* :catch_0 */
/* move-exception v2 */
/* .line 38 */
/* .local v2, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "ClassNotFound from "; // const-string v4, "ClassNotFound from "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ", "; // const-string v4, ", "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "CameraOpt"; // const-string v4, "CameraOpt"
android.util.Slog .e ( v4,v3 );
/* .line 40 */
} // .end local v0 # "jarPath":Ljava/lang/String;
} // .end local v1 # "cameraOptManagerClazzName":Ljava/lang/String;
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public com.android.server.camera.CameraOpt ( ) {
/* .locals 0 */
/* .line 22 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
public static java.lang.Object callMethod ( java.lang.String p0, java.lang.Object...p1 ) {
/* .locals 3 */
/* .param p0, "funcName" # Ljava/lang/String; */
/* .param p1, "values" # [Ljava/lang/Object; */
/* .line 80 */
/* sget-boolean v0, Lcom/android/server/camera/CameraOpt;->IS_SUPPORT_ABIS:Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 81 */
/* .line 83 */
} // :cond_0
v0 = com.android.server.camera.CameraOpt.mCameraOptManager;
/* if-nez v0, :cond_1 */
/* .line 84 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "call methods("; // const-string v2, "call methods("
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ") failed, because cameraopt was null."; // const-string v2, ") failed, because cameraopt was null."
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "CameraOpt"; // const-string v2, "CameraOpt"
android.util.Slog .w ( v2,v0 );
/* .line 85 */
/* .line 87 */
} // :cond_1
com.android.server.camera.CameraOpt .callStaticMethod ( v0,p0,p1 );
} // .end method
public static java.lang.Object callStaticMethod ( java.lang.Class p0, java.lang.String p1, java.lang.Object...p2 ) {
/* .locals 4 */
/* .param p0, "clazz" # Ljava/lang/Class; */
/* .param p1, "funcName" # Ljava/lang/String; */
/* .param p2, "values" # [Ljava/lang/Object; */
/* .line 92 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p0, :cond_0 */
/* .line 93 */
} // :cond_0
try { // :try_start_0
com.android.server.camera.CameraOpt .getParameterTypes ( p2 );
com.android.server.camera.CameraOpt .getFuncMethod ( p0,p1,v1 );
/* .line 94 */
/* .local v1, "declaredMethod":Ljava/lang/reflect/Method; */
int v2 = 1; // const/4 v2, 0x1
(( java.lang.reflect.Method ) v1 ).setAccessible ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V
/* .line 95 */
if ( p2 != null) { // if-eqz p2, :cond_2
/* array-length v2, p2 */
/* if-gtz v2, :cond_1 */
/* .line 98 */
} // :cond_1
(( java.lang.reflect.Method ) v1 ).invoke ( v0, p2 ); // invoke-virtual {v1, v0, p2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* .line 96 */
} // :cond_2
} // :goto_0
int v2 = 0; // const/4 v2, 0x0
/* new-array v2, v2, [Ljava/lang/Object; */
(( java.lang.reflect.Method ) v1 ).invoke ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 99 */
} // .end local v1 # "declaredMethod":Ljava/lang/reflect/Method;
/* :catch_0 */
/* move-exception v1 */
/* .line 100 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "call methods("; // const-string v3, "call methods("
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = ") failed :"; // const-string v3, ") failed :"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "CameraOpt"; // const-string v3, "CameraOpt"
android.util.Slog .w ( v3,v2 );
/* .line 101 */
} // .end method
private static java.lang.reflect.Method getDeclaredMethod ( java.lang.Class p0, java.lang.String p1, java.lang.Class[] p2 ) {
/* .locals 4 */
/* .param p0, "clazz" # Ljava/lang/Class; */
/* .param p1, "methodName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/Class;", */
/* "Ljava/lang/String;", */
/* "[", */
/* "Ljava/lang/Class<", */
/* "*>;)", */
/* "Ljava/lang/reflect/Method;" */
/* } */
} // .end annotation
/* .line 43 */
/* .local p2, "paramsTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;" */
int v0 = 0; // const/4 v0, 0x0
/* .line 45 */
/* .local v0, "declaredMethod":Ljava/lang/reflect/Method; */
if ( p2 != null) { // if-eqz p2, :cond_1
try { // :try_start_0
/* array-length v1, p2 */
/* if-gtz v1, :cond_0 */
/* .line 48 */
} // :cond_0
(( java.lang.Class ) p0 ).getDeclaredMethod ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* move-object v0, v1 */
/* .line 46 */
} // :cond_1
} // :goto_0
int v1 = 0; // const/4 v1, 0x0
/* new-array v1, v1, [Ljava/lang/Class; */
(( java.lang.Class ) p0 ).getDeclaredMethod ( p1, v1 ); // invoke-virtual {p0, p1, v1}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object v0, v1 */
/* .line 52 */
} // :goto_1
/* .line 50 */
/* :catch_0 */
/* move-exception v1 */
/* .line 51 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "getDeclaredMethod:"; // const-string v3, "getDeclaredMethod:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " failed"; // const-string v3, " failed"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "CameraOpt"; // const-string v3, "CameraOpt"
android.util.Slog .e ( v3,v2 );
/* .line 53 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_2
} // .end method
private static java.lang.reflect.Method getFuncMethod ( java.lang.Class p0, java.lang.String p1, java.lang.Class[] p2 ) {
/* .locals 5 */
/* .param p0, "clazz" # Ljava/lang/Class; */
/* .param p1, "methodName" # Ljava/lang/String; */
/* .param p2, "paramsTypes" # [Ljava/lang/Class; */
/* .line 57 */
v0 = com.android.server.camera.CameraOpt.sMethodInfo;
/* monitor-enter v0 */
/* .line 58 */
try { // :try_start_0
v1 = com.android.server.camera.CameraOpt.sMethodInfo;
(( com.android.server.camera.CameraOpt$MethodInfo ) v1 ).setClass ( p0 ); // invoke-virtual {v1, p0}, Lcom/android/server/camera/CameraOpt$MethodInfo;->setClass(Ljava/lang/Class;)Lcom/android/server/camera/CameraOpt$MethodInfo;
(( com.android.server.camera.CameraOpt$MethodInfo ) v1 ).setMethodName ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/camera/CameraOpt$MethodInfo;->setMethodName(Ljava/lang/String;)Lcom/android/server/camera/CameraOpt$MethodInfo;
(( com.android.server.camera.CameraOpt$MethodInfo ) v1 ).setParmasClass ( p2 ); // invoke-virtual {v1, p2}, Lcom/android/server/camera/CameraOpt$MethodInfo;->setParmasClass([Ljava/lang/Class;)Lcom/android/server/camera/CameraOpt$MethodInfo;
/* .line 59 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 61 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_1
v1 = com.android.server.camera.CameraOpt.mMethodInfoMap;
v1 = v2 = com.android.server.camera.CameraOpt.sMethodInfo;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 62 */
v1 = com.android.server.camera.CameraOpt.mMethodInfoMap;
v2 = com.android.server.camera.CameraOpt.sMethodInfo;
/* check-cast v1, Ljava/lang/reflect/Method; */
/* .line 64 */
} // :cond_0
com.android.server.camera.CameraOpt .getDeclaredMethod ( p0,p1,p2 );
/* .line 65 */
/* .local v1, "method":Ljava/lang/reflect/Method; */
/* if-nez v1, :cond_1 */
/* .line 66 */
/* .line 68 */
} // :cond_1
v2 = com.android.server.camera.CameraOpt.mMethodInfoMap;
/* monitor-enter v2 */
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 69 */
try { // :try_start_2
v3 = com.android.server.camera.CameraOpt.mMethodInfoMap;
/* new-instance v4, Lcom/android/server/camera/CameraOpt$MethodInfo; */
/* invoke-direct {v4, p0, p1, p2}, Lcom/android/server/camera/CameraOpt$MethodInfo;-><init>(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V */
/* .line 70 */
/* monitor-exit v2 */
/* .line 71 */
/* .line 70 */
/* :catchall_0 */
/* move-exception v3 */
/* monitor-exit v2 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
} // .end local p0 # "clazz":Ljava/lang/Class;
} // .end local p1 # "methodName":Ljava/lang/String;
} // .end local p2 # "paramsTypes":[Ljava/lang/Class;
try { // :try_start_3
/* throw v3 */
/* :try_end_3 */
/* .catch Ljava/lang/Exception; {:try_start_3 ..:try_end_3} :catch_0 */
/* .line 73 */
} // .end local v1 # "method":Ljava/lang/reflect/Method;
/* .restart local p0 # "clazz":Ljava/lang/Class; */
/* .restart local p1 # "methodName":Ljava/lang/String; */
/* .restart local p2 # "paramsTypes":[Ljava/lang/Class; */
/* :catch_0 */
/* move-exception v1 */
/* .line 74 */
/* .local v1, "ex":Ljava/lang/Exception; */
final String v2 = "CameraOpt"; // const-string v2, "CameraOpt"
final String v3 = "getFuncMethod Fail!"; // const-string v3, "getFuncMethod Fail!"
android.util.Slog .w ( v2,v3 );
/* .line 75 */
/* .line 59 */
} // .end local v1 # "ex":Ljava/lang/Exception;
/* :catchall_1 */
/* move-exception v1 */
try { // :try_start_4
/* monitor-exit v0 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* throw v1 */
} // .end method
private static java.lang.Class getParameterTypes ( java.lang.Object...p0 ) {
/* .locals 6 */
/* .param p0, "args" # [Ljava/lang/Object; */
/* .line 106 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p0, :cond_0 */
/* .line 107 */
/* .line 109 */
} // :cond_0
/* array-length v1, p0 */
/* new-array v1, v1, [Ljava/lang/Class; */
/* .line 110 */
/* .local v1, "parameterTypes":[Ljava/lang/Class; */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
/* array-length v3, p0 */
/* .local v3, "j":I */
} // :goto_0
/* if-ge v2, v3, :cond_7 */
/* .line 111 */
/* aget-object v4, p0, v2 */
/* if-nez v4, :cond_1 */
/* .line 112 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "the params of method is null, so return.index : " */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "CameraOpt"; // const-string v5, "CameraOpt"
android.util.Slog .w ( v5,v4 );
/* .line 113 */
/* .line 115 */
} // :cond_1
/* aget-object v4, p0, v2 */
/* instance-of v4, v4, Ljava/lang/Integer; */
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 116 */
v4 = java.lang.Integer.TYPE;
/* aput-object v4, v1, v2 */
/* .line 117 */
} // :cond_2
/* aget-object v4, p0, v2 */
/* instance-of v4, v4, Ljava/lang/Double; */
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 118 */
v4 = java.lang.Double.TYPE;
/* aput-object v4, v1, v2 */
/* .line 119 */
} // :cond_3
/* aget-object v4, p0, v2 */
/* instance-of v4, v4, Ljava/lang/Long; */
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 120 */
v4 = java.lang.Long.TYPE;
/* aput-object v4, v1, v2 */
/* .line 121 */
} // :cond_4
/* aget-object v4, p0, v2 */
/* instance-of v4, v4, Ljava/lang/Boolean; */
if ( v4 != null) { // if-eqz v4, :cond_5
/* .line 122 */
v4 = java.lang.Boolean.TYPE;
/* aput-object v4, v1, v2 */
/* .line 123 */
} // :cond_5
/* aget-object v4, p0, v2 */
/* instance-of v4, v4, Landroid/content/Context; */
if ( v4 != null) { // if-eqz v4, :cond_6
/* .line 124 */
/* const-class v4, Landroid/content/Context; */
/* aput-object v4, v1, v2 */
/* .line 126 */
} // :cond_6
/* aget-object v4, p0, v2 */
(( java.lang.Object ) v4 ).getClass ( ); // invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
/* aput-object v4, v1, v2 */
/* .line 110 */
} // :goto_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 129 */
} // .end local v2 # "i":I
} // .end local v3 # "j":I
} // :cond_7
} // .end method
private Boolean isNeedBoostCamera ( android.content.Intent p0 ) {
/* .locals 5 */
/* .param p1, "in" # Landroid/content/Intent; */
/* .line 151 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_0 */
/* .line 152 */
} // :cond_0
(( android.content.Intent ) p1 ).getComponent ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
/* .line 153 */
/* .local v1, "comp":Landroid/content/ComponentName; */
(( android.content.Intent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 154 */
/* .local v2, "action":Ljava/lang/String; */
if ( v1 != null) { // if-eqz v1, :cond_4
/* if-nez v2, :cond_1 */
/* .line 155 */
} // :cond_1
(( android.content.ComponentName ) v1 ).flattenToShortString ( ); // invoke-virtual {v1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
final String v4 = "com.android.camera/.Camera"; // const-string v4, "com.android.camera/.Camera"
v3 = android.text.TextUtils .equals ( v3,v4 );
/* if-nez v3, :cond_3 */
/* .line 156 */
(( android.content.ComponentName ) v1 ).flattenToShortString ( ); // invoke-virtual {v1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
final String v4 = "com.android.camera/.VoiceCamera"; // const-string v4, "com.android.camera/.VoiceCamera"
v3 = android.text.TextUtils .equals ( v3,v4 );
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 159 */
} // :cond_2
/* .line 157 */
} // :cond_3
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
/* .line 154 */
} // :cond_4
} // :goto_1
} // .end method
/* # virtual methods */
public void boostCameraByThreshold ( android.content.Intent p0 ) {
/* .locals 2 */
/* .param p1, "in" # Landroid/content/Intent; */
/* .line 145 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/camera/CameraOpt;->isNeedBoostCamera(Landroid/content/Intent;)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 146 */
/* const-wide/16 v0, 0x0 */
java.lang.Long .valueOf ( v0,v1 );
/* filled-new-array {v0}, [Ljava/lang/Object; */
final String v1 = "boostCameraByThreshold"; // const-string v1, "boostCameraByThreshold"
com.android.server.camera.CameraOpt .callMethod ( v1,v0 );
/* .line 148 */
} // :cond_0
return;
} // .end method
public Boolean interceptAppRestartIfNeeded ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .param p2, "type" # Ljava/lang/String; */
/* .line 139 */
final String v0 = "interceptAppRestartIfNeeded"; // const-string v0, "interceptAppRestartIfNeeded"
/* filled-new-array {p1, p2}, [Ljava/lang/Object; */
com.android.server.camera.CameraOpt .callMethod ( v0,v1 );
/* .line 140 */
/* .local v0, "result":Ljava/lang/Object; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* move-object v1, v0 */
/* check-cast v1, Ljava/lang/Boolean; */
v1 = (( java.lang.Boolean ) v1 ).booleanValue ( ); // invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
if ( v1 != null) { // if-eqz v1, :cond_0
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
} // .end method
public void onTransitionAnimateStateChanged ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "isAnimationStart" # Z */
/* .line 134 */
java.lang.Boolean .valueOf ( p1 );
/* filled-new-array {v0}, [Ljava/lang/Object; */
final String v1 = "onTransitionAnimateStateChanged"; // const-string v1, "onTransitionAnimateStateChanged"
com.android.server.camera.CameraOpt .callMethod ( v1,v0 );
/* .line 135 */
return;
} // .end method
