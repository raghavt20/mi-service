class com.android.server.camera.CameraTrackInjector$CameraUsageBean {
	 /* .source "CameraTrackInjector.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/camera/CameraTrackInjector; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "CameraUsageBean" */
} // .end annotation
/* # instance fields */
public Integer mCameraAPI;
public java.lang.String mCameraId;
public java.lang.String mClientName;
private Boolean mCompleted;
private Long mDurationOrStartTimeMs;
/* # direct methods */
public com.android.server.camera.CameraTrackInjector$CameraUsageBean ( ) {
/* .locals 2 */
/* .param p1, "clientName" # Ljava/lang/String; */
/* .param p2, "cameraId" # Ljava/lang/String; */
/* .param p3, "apiLevel" # I */
/* .line 41 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 42 */
this.mCameraId = p2;
/* .line 43 */
this.mClientName = p1;
/* .line 44 */
/* iput p3, p0, Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;->mCameraAPI:I */
/* .line 45 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;->mDurationOrStartTimeMs:J */
/* .line 46 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;->mCompleted:Z */
/* .line 47 */
return;
} // .end method
/* # virtual methods */
public Long getDuration ( ) {
/* .locals 4 */
/* .line 61 */
/* iget-boolean v0, p0, Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;->mCompleted:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* iget-wide v0, p0, Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;->mDurationOrStartTimeMs:J */
	 /* const-wide/16 v2, 0x3e8 */
	 /* div-long/2addr v0, v2 */
} // :cond_0
/* const-wide/16 v0, 0x0 */
} // :goto_0
/* return-wide v0 */
} // .end method
public void markCompleted ( ) {
/* .locals 4 */
/* .line 50 */
/* iget-boolean v0, p0, Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;->mCompleted:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 51 */
return;
/* .line 53 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;->mCompleted:Z */
/* .line 54 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v0 */
/* iget-wide v2, p0, Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;->mDurationOrStartTimeMs:J */
/* sub-long/2addr v0, v2 */
/* iput-wide v0, p0, Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;->mDurationOrStartTimeMs:J */
/* .line 55 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 3 */
/* .line 65 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "CameraUsageBean(clientName="; // const-string v1, "CameraUsageBean(clientName="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mClientName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ",CameraAPI="; // const-string v1, ",CameraAPI="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;->mCameraAPI:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ",cameraId="; // const-string v1, ",cameraId="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mCameraId;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ",durationOrStartTimeS="; // const-string v1, ",durationOrStartTimeS="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 69 */
(( com.android.server.camera.CameraTrackInjector$CameraUsageBean ) p0 ).getDuration ( ); // invoke-virtual {p0}, Lcom/android/server/camera/CameraTrackInjector$CameraUsageBean;->getDuration()J
/* move-result-wide v1 */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = ")"; // const-string v1, ")"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 65 */
} // .end method
