.class Lcom/android/server/ExtendMImpl$2;
.super Ljava/lang/Object;
.source "ExtendMImpl.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/ExtendMImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/ExtendMImpl;


# direct methods
.method constructor <init>(Lcom/android/server/ExtendMImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/ExtendMImpl;

    .line 1084
    iput-object p1, p0, Lcom/android/server/ExtendMImpl$2;->this$0:Lcom/android/server/ExtendMImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public binderDied()V
    .locals 2

    .line 1087
    const-string v0, "ExtM"

    const-string/jumbo v1, "vold died; reconnecting"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1088
    iget-object v0, p0, Lcom/android/server/ExtendMImpl$2;->this$0:Lcom/android/server/ExtendMImpl;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/server/ExtendMImpl;->-$$Nest$fputmVold(Lcom/android/server/ExtendMImpl;Landroid/os/IVold;)V

    .line 1089
    iget-object v0, p0, Lcom/android/server/ExtendMImpl$2;->this$0:Lcom/android/server/ExtendMImpl;

    invoke-static {v0}, Lcom/android/server/ExtendMImpl;->-$$Nest$mconnect(Lcom/android/server/ExtendMImpl;)V

    .line 1090
    return-void
.end method
