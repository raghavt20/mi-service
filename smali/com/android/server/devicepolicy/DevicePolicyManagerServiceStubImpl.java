public class com.android.server.devicepolicy.DevicePolicyManagerServiceStubImpl extends com.android.server.devicepolicy.DevicePolicyManagerServiceStub {
	 /* .source "DevicePolicyManagerServiceStubImpl.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.devicepolicy.DevicePolicyManagerServiceStub$$" */
} // .end annotation
/* # static fields */
private static final java.lang.String TAG;
/* # direct methods */
public com.android.server.devicepolicy.DevicePolicyManagerServiceStubImpl ( ) {
	 /* .locals 0 */
	 /* .line 18 */
	 /* invoke-direct {p0}, Lcom/android/server/devicepolicy/DevicePolicyManagerServiceStub;-><init>()V */
	 return;
} // .end method
/* # virtual methods */
public void checkFactoryResetRecoveryCondition ( java.lang.String p0, Boolean p1, android.content.Context p2 ) {
	 /* .locals 8 */
	 /* .param p1, "key" # Ljava/lang/String; */
	 /* .param p2, "enabledFromThisOwner" # Z */
	 /* .param p3, "mContext" # Landroid/content/Context; */
	 /* .line 24 */
	 final String v0 = "DevicePolicyManagerServiceStubImpl"; // const-string v0, "DevicePolicyManagerServiceStubImpl"
	 try { // :try_start_0
		 final String v1 = "android.os.RecoverySystem"; // const-string v1, "android.os.RecoverySystem"
		 java.lang.Class .forName ( v1 );
		 /* .line 25 */
		 /* .local v1, "recoverySystem":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
		 final String v2 = "disableFactoryReset"; // const-string v2, "disableFactoryReset"
		 int v3 = 2; // const/4 v3, 0x2
		 /* new-array v4, v3, [Ljava/lang/Class; */
		 /* const-class v5, Landroid/content/Context; */
		 int v6 = 0; // const/4 v6, 0x0
		 /* aput-object v5, v4, v6 */
		 v5 = java.lang.Boolean.TYPE;
		 int v7 = 1; // const/4 v7, 0x1
		 /* aput-object v5, v4, v7 */
		 (( java.lang.Class ) v1 ).getDeclaredMethod ( v2, v4 ); // invoke-virtual {v1, v2, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
		 /* .line 27 */
		 /* .local v2, "method_disableFactoryReset":Ljava/lang/reflect/Method; */
		 final String v4 = "no_factory_reset"; // const-string v4, "no_factory_reset"
		 v4 = 		 (( java.lang.String ) p1 ).equals ( v4 ); // invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v4 != null) { // if-eqz v4, :cond_1
			 /* .line 28 */
			 if ( p2 != null) { // if-eqz p2, :cond_0
				 /* .line 29 */
				 (( java.lang.reflect.Method ) v2 ).setAccessible ( v7 ); // invoke-virtual {v2, v7}, Ljava/lang/reflect/Method;->setAccessible(Z)V
				 /* .line 30 */
				 /* new-array v3, v3, [Ljava/lang/Object; */
				 /* aput-object p3, v3, v6 */
				 /* .line 31 */
				 java.lang.Boolean .valueOf ( v7 );
				 /* aput-object v4, v3, v7 */
				 /* .line 30 */
				 (( java.lang.reflect.Method ) v2 ).invoke ( v1, v3 ); // invoke-virtual {v2, v1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
				 /* .line 32 */
				 final String v3 = "Restrict factory reset by recovery system"; // const-string v3, "Restrict factory reset by recovery system"
				 android.util.Slog .w ( v0,v3 );
				 /* .line 34 */
			 } // :cond_0
			 (( java.lang.reflect.Method ) v2 ).setAccessible ( v7 ); // invoke-virtual {v2, v7}, Ljava/lang/reflect/Method;->setAccessible(Z)V
			 /* .line 35 */
			 /* new-array v3, v3, [Ljava/lang/Object; */
			 /* aput-object p3, v3, v6 */
			 /* .line 36 */
			 java.lang.Boolean .valueOf ( v6 );
			 /* aput-object v4, v3, v7 */
			 /* .line 35 */
			 (( java.lang.reflect.Method ) v2 ).invoke ( v1, v3 ); // invoke-virtual {v2, v1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
			 /* .line 37 */
			 final String v3 = "Launch factory reset by recovery system"; // const-string v3, "Launch factory reset by recovery system"
			 android.util.Slog .w ( v0,v3 );
			 /* :try_end_0 */
			 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* .line 42 */
		 } // .end local v1 # "recoverySystem":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
	 } // .end local v2 # "method_disableFactoryReset":Ljava/lang/reflect/Method;
} // :cond_1
} // :goto_0
/* .line 40 */
/* :catch_0 */
/* move-exception v1 */
/* .line 41 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v2 = "Failed to get recoverySystem"; // const-string v2, "Failed to get recoverySystem"
android.util.Slog .w ( v0,v2,v1 );
/* .line 43 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_1
return;
} // .end method
