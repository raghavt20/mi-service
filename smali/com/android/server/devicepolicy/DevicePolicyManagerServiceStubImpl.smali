.class public Lcom/android/server/devicepolicy/DevicePolicyManagerServiceStubImpl;
.super Lcom/android/server/devicepolicy/DevicePolicyManagerServiceStub;
.source "DevicePolicyManagerServiceStubImpl.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.devicepolicy.DevicePolicyManagerServiceStub$$"
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "DevicePolicyManagerServiceStubImpl"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Lcom/android/server/devicepolicy/DevicePolicyManagerServiceStub;-><init>()V

    return-void
.end method


# virtual methods
.method public checkFactoryResetRecoveryCondition(Ljava/lang/String;ZLandroid/content/Context;)V
    .locals 8
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "enabledFromThisOwner"    # Z
    .param p3, "mContext"    # Landroid/content/Context;

    .line 24
    const-string v0, "DevicePolicyManagerServiceStubImpl"

    :try_start_0
    const-string v1, "android.os.RecoverySystem"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 25
    .local v1, "recoverySystem":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v2, "disableFactoryReset"

    const/4 v3, 0x2

    new-array v4, v3, [Ljava/lang/Class;

    const-class v5, Landroid/content/Context;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    sget-object v5, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v7, 0x1

    aput-object v5, v4, v7

    invoke-virtual {v1, v2, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 27
    .local v2, "method_disableFactoryReset":Ljava/lang/reflect/Method;
    const-string v4, "no_factory_reset"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 28
    if-eqz p2, :cond_0

    .line 29
    invoke-virtual {v2, v7}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 30
    new-array v3, v3, [Ljava/lang/Object;

    aput-object p3, v3, v6

    .line 31
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v7

    .line 30
    invoke-virtual {v2, v1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    const-string v3, "Restrict factory reset by recovery system"

    invoke-static {v0, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 34
    :cond_0
    invoke-virtual {v2, v7}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 35
    new-array v3, v3, [Ljava/lang/Object;

    aput-object p3, v3, v6

    .line 36
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v7

    .line 35
    invoke-virtual {v2, v1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    const-string v3, "Launch factory reset by recovery system"

    invoke-static {v0, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 42
    .end local v1    # "recoverySystem":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v2    # "method_disableFactoryReset":Ljava/lang/reflect/Method;
    :cond_1
    :goto_0
    goto :goto_1

    .line 40
    :catch_0
    move-exception v1

    .line 41
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "Failed to get recoverySystem"

    invoke-static {v0, v2, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 43
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_1
    return-void
.end method
