public class com.android.server.DarkModeOneTrackHelper {
	 /* .source "DarkModeOneTrackHelper.java" */
	 /* # static fields */
	 private static final java.lang.String APP_ID;
	 private static final java.lang.String DEVICE_REGION;
	 private static final java.lang.String EVENT_NAME;
	 public static final java.lang.String EVENT_NAME_AUTO_SWITCH;
	 public static final java.lang.String EVENT_NAME_SETTING;
	 public static final java.lang.String EVENT_NAME_STATUS;
	 public static final java.lang.String EVENT_NAME_SUGGEST;
	 private static final java.lang.String ONETRACK_PACKAGE_NAME;
	 private static final java.lang.String ONE_TRACK_ACTION;
	 private static final java.lang.String PARAM_KEY_APP_LIST;
	 private static final java.lang.String PARAM_KEY_APP_NAME;
	 private static final java.lang.String PARAM_KEY_APP_PKG;
	 private static final java.lang.String PARAM_KEY_BEGIN_TIME;
	 private static final java.lang.String PARAM_KEY_CONTRAST;
	 private static final java.lang.String PARAM_KEY_DARK_MODE_STATUS;
	 private static final java.lang.String PARAM_KEY_END_TIME;
	 private static final java.lang.String PARAM_KEY_SETTING_CHANNEL;
	 private static final java.lang.String PARAM_KEY_STATUS_AFTER_CLICK;
	 private static final java.lang.String PARAM_KEY_SUGGEST;
	 private static final java.lang.String PARAM_KEY_SUGGEST_CLICK;
	 private static final java.lang.String PARAM_KEY_SUGGEST_ENABLE;
	 private static final java.lang.String PARAM_KEY_SUGGEST_OPEN_IN_SETTING;
	 private static final java.lang.String PARAM_KEY_SWITCH_WAY;
	 private static final java.lang.String PARAM_KEY_TIME_MODE_PATTERN;
	 private static final java.lang.String PARAM_KEY_TIME_MODE_STATUS;
	 private static final java.lang.String PARAM_KEY_WALL_PAPER;
	 public static final java.lang.String PARAM_VALUE_APP_NAME;
	 public static final java.lang.String PARAM_VALUE_APP_PKG;
	 public static final java.lang.String PARAM_VALUE_APP_SWITCH_STATUS;
	 public static final java.lang.String PARAM_VALUE_CHANNEL_CENTER;
	 public static final java.lang.String PARAM_VALUE_CHANNEL_NOTIFY;
	 public static final java.lang.String PARAM_VALUE_CHANNEL_SETTING;
	 public static final java.lang.String PARAM_VALUE_CLOSE;
	 public static final java.lang.String PARAM_VALUE_CUSTOM;
	 public static final java.lang.String PARAM_VALUE_OPEN;
	 public static final java.lang.String PARAM_VALUE_SWITCH_CLOSE;
	 public static final java.lang.String PARAM_VALUE_SWITCH_OPEN;
	 public static final java.lang.String PARAM_VALUE_TWILIGHT;
	 private static final java.lang.String TAG;
	 private static final java.lang.String TIP;
	 public static final java.lang.String TIP_APP_SETTING;
	 public static final java.lang.String TIP_APP_STATUS;
	 public static final java.lang.String TIP_AUTO_SWITCH;
	 public static final java.lang.String TIP_CONTRAST_SETTING;
	 public static final java.lang.String TIP_DARK_MODE_SETTING;
	 public static final java.lang.String TIP_DARK_MODE_STATUS;
	 public static final java.lang.String TIP_SUGGEST;
	 public static final java.lang.String TIP_TIME_MODE_SETTING;
	 public static final java.lang.String TIP_WALL_PAPER_SETTING;
	 private static java.util.Set sRegions;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/Set<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
/* # direct methods */
static void -$$Nest$smupdateDarkModeIntent ( android.content.Intent p0, com.android.server.DarkModeEvent p1 ) { //bridge//synthethic
/* .locals 0 */
com.android.server.DarkModeOneTrackHelper .updateDarkModeIntent ( p0,p1 );
return;
} // .end method
static com.android.server.DarkModeOneTrackHelper ( ) {
/* .locals 2 */
/* .line 29 */
final String v0 = "ro.miui.region"; // const-string v0, "ro.miui.region"
final String v1 = "CN"; // const-string v1, "CN"
android.os.SystemProperties .get ( v0,v1 );
/* .line 82 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
return;
} // .end method
public com.android.server.DarkModeOneTrackHelper ( ) {
/* .locals 0 */
/* .line 21 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
public static void setDataDisableRegion ( java.util.Set p0 ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 112 */
/* .local p0, "regions":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
v0 = com.android.server.DarkModeOneTrackHelper.sRegions;
/* .line 113 */
v0 = com.android.server.DarkModeOneTrackHelper.sRegions;
/* .line 114 */
return;
} // .end method
private static void updateAppSettingIntent ( android.content.Intent p0, com.android.server.DarkModeStauesEvent p1 ) {
/* .locals 3 */
/* .param p0, "intent" # Landroid/content/Intent; */
/* .param p1, "event" # Lcom/android/server/DarkModeStauesEvent; */
/* .line 193 */
final String v0 = "EVENT_NAME"; // const-string v0, "EVENT_NAME"
/* const-string/jumbo v1, "setting" */
(( android.content.Intent ) p0 ).putExtra ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 194 */
(( com.android.server.DarkModeStauesEvent ) p1 ).getTip ( ); // invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getTip()Ljava/lang/String;
/* const-string/jumbo v2, "tip" */
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 195 */
(( com.android.server.DarkModeStauesEvent ) p1 ).getAppPkg ( ); // invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getAppPkg()Ljava/lang/String;
final String v2 = "app_package_name"; // const-string v2, "app_package_name"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 196 */
(( com.android.server.DarkModeStauesEvent ) p1 ).getAppName ( ); // invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getAppName()Ljava/lang/String;
final String v2 = "app_name"; // const-string v2, "app_name"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 197 */
(( com.android.server.DarkModeStauesEvent ) p1 ).getAppEnable ( ); // invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getAppEnable()Ljava/lang/String;
final String v2 = "after_click_status"; // const-string v2, "after_click_status"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 198 */
return;
} // .end method
private static void updateAppStatusIntent ( android.content.Intent p0, com.android.server.DarkModeStauesEvent p1 ) {
/* .locals 3 */
/* .param p0, "intent" # Landroid/content/Intent; */
/* .param p1, "event" # Lcom/android/server/DarkModeStauesEvent; */
/* .line 163 */
final String v0 = "EVENT_NAME"; // const-string v0, "EVENT_NAME"
/* const-string/jumbo v1, "status" */
(( android.content.Intent ) p0 ).putExtra ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 164 */
(( com.android.server.DarkModeStauesEvent ) p1 ).getTip ( ); // invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getTip()Ljava/lang/String;
/* const-string/jumbo v2, "tip" */
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 165 */
(( com.android.server.DarkModeStauesEvent ) p1 ).getAppList ( ); // invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getAppList()Ljava/util/List;
/* check-cast v1, Ljava/util/ArrayList; */
final String v2 = "app_list"; // const-string v2, "app_list"
(( android.content.Intent ) v0 ).putStringArrayListExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;
/* .line 166 */
return;
} // .end method
private static void updateAutoSwitchIntent ( android.content.Intent p0, com.android.server.DarkModeStauesEvent p1 ) {
/* .locals 3 */
/* .param p0, "intent" # Landroid/content/Intent; */
/* .param p1, "event" # Lcom/android/server/DarkModeStauesEvent; */
/* .line 208 */
final String v0 = "EVENT_NAME"; // const-string v0, "EVENT_NAME"
final String v1 = "auto_switch"; // const-string v1, "auto_switch"
(( android.content.Intent ) p0 ).putExtra ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 209 */
(( com.android.server.DarkModeStauesEvent ) p1 ).getTip ( ); // invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getTip()Ljava/lang/String;
/* const-string/jumbo v2, "tip" */
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 210 */
(( com.android.server.DarkModeStauesEvent ) p1 ).getAutoSwitch ( ); // invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getAutoSwitch()Ljava/lang/String;
/* const-string/jumbo v2, "switch_way" */
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 211 */
return;
} // .end method
private static void updateContrastSettingIntent ( android.content.Intent p0, com.android.server.DarkModeStauesEvent p1 ) {
/* .locals 3 */
/* .param p0, "intent" # Landroid/content/Intent; */
/* .param p1, "event" # Lcom/android/server/DarkModeStauesEvent; */
/* .line 187 */
final String v0 = "EVENT_NAME"; // const-string v0, "EVENT_NAME"
/* const-string/jumbo v1, "setting" */
(( android.content.Intent ) p0 ).putExtra ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 188 */
(( com.android.server.DarkModeStauesEvent ) p1 ).getTip ( ); // invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getTip()Ljava/lang/String;
/* const-string/jumbo v2, "tip" */
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 189 */
(( com.android.server.DarkModeStauesEvent ) p1 ).getContrastStatus ( ); // invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getContrastStatus()Ljava/lang/String;
final String v2 = "after_click_status"; // const-string v2, "after_click_status"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 190 */
return;
} // .end method
private static void updateDarkModeIntent ( android.content.Intent p0, com.android.server.DarkModeEvent p1 ) {
/* .locals 2 */
/* .param p0, "intent" # Landroid/content/Intent; */
/* .param p1, "event" # Lcom/android/server/DarkModeEvent; */
/* .line 117 */
v1 = (( java.lang.String ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/String;->hashCode()I
/* sparse-switch v1, :sswitch_data_0 */
} // :cond_0
/* :sswitch_0 */
final String v1 = "577.5.0.1.23096"; // const-string v1, "577.5.0.1.23096"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 7; // const/4 v0, 0x7
/* :sswitch_1 */
final String v1 = "577.3.2.1.23093"; // const-string v1, "577.3.2.1.23093"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 4; // const/4 v0, 0x4
/* :sswitch_2 */
final String v1 = "577.1.2.1.23090"; // const-string v1, "577.1.2.1.23090"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
	 int v0 = 1; // const/4 v0, 0x1
	 /* :sswitch_3 */
	 final String v1 = ""; // const-string v1, ""
	 v0 = 	 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* const/16 v0, 0x8 */
		 /* :sswitch_4 */
		 final String v1 = "577.3.3.1.23094"; // const-string v1, "577.3.3.1.23094"
		 v0 = 		 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 int v0 = 5; // const/4 v0, 0x5
			 /* :sswitch_5 */
			 final String v1 = "577.4.0.1.23106"; // const-string v1, "577.4.0.1.23106"
			 v0 = 			 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
			 if ( v0 != null) { // if-eqz v0, :cond_0
				 int v0 = 6; // const/4 v0, 0x6
				 /* :sswitch_6 */
				 final String v1 = "577.3.1.1.23092"; // const-string v1, "577.3.1.1.23092"
				 v0 = 				 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
				 if ( v0 != null) { // if-eqz v0, :cond_0
					 int v0 = 3; // const/4 v0, 0x3
					 /* :sswitch_7 */
					 final String v1 = "577.2.0.1.23091"; // const-string v1, "577.2.0.1.23091"
					 v0 = 					 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
					 if ( v0 != null) { // if-eqz v0, :cond_0
						 int v0 = 2; // const/4 v0, 0x2
						 /* :sswitch_8 */
						 final String v1 = "577.1.1.1.23089"; // const-string v1, "577.1.1.1.23089"
						 v0 = 						 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
						 if ( v0 != null) { // if-eqz v0, :cond_0
							 int v0 = 0; // const/4 v0, 0x0
						 } // :goto_0
						 int v0 = -1; // const/4 v0, -0x1
					 } // :goto_1
					 /* packed-switch v0, :pswitch_data_0 */
					 /* .line 143 */
					 /* :pswitch_0 */
					 /* move-object v0, p1 */
					 /* check-cast v0, Lcom/android/server/DarkModeStauesEvent; */
					 com.android.server.DarkModeOneTrackHelper .updateSuggestEventIntent ( p0,v0 );
					 /* .line 144 */
					 /* .line 140 */
					 /* :pswitch_1 */
					 /* move-object v0, p1 */
					 /* check-cast v0, Lcom/android/server/DarkModeStauesEvent; */
					 com.android.server.DarkModeOneTrackHelper .updateAutoSwitchIntent ( p0,v0 );
					 /* .line 141 */
					 /* .line 137 */
					 /* :pswitch_2 */
					 /* move-object v0, p1 */
					 /* check-cast v0, Lcom/android/server/DarkModeStauesEvent; */
					 com.android.server.DarkModeOneTrackHelper .updateDarkModeSettingIntent ( p0,v0 );
					 /* .line 138 */
					 /* .line 134 */
					 /* :pswitch_3 */
					 /* move-object v0, p1 */
					 /* check-cast v0, Lcom/android/server/DarkModeStauesEvent; */
					 com.android.server.DarkModeOneTrackHelper .updateAppSettingIntent ( p0,v0 );
					 /* .line 135 */
					 /* .line 131 */
					 /* :pswitch_4 */
					 /* move-object v0, p1 */
					 /* check-cast v0, Lcom/android/server/DarkModeStauesEvent; */
					 com.android.server.DarkModeOneTrackHelper .updateContrastSettingIntent ( p0,v0 );
					 /* .line 132 */
					 /* .line 128 */
					 /* :pswitch_5 */
					 /* move-object v0, p1 */
					 /* check-cast v0, Lcom/android/server/DarkModeStauesEvent; */
					 com.android.server.DarkModeOneTrackHelper .updateWallPaperSettingIntent ( p0,v0 );
					 /* .line 129 */
					 /* .line 125 */
					 /* :pswitch_6 */
					 /* move-object v0, p1 */
					 /* check-cast v0, Lcom/android/server/DarkModeStauesEvent; */
					 com.android.server.DarkModeOneTrackHelper .updateTimeModeSettingIntent ( p0,v0 );
					 /* .line 126 */
					 /* .line 122 */
					 /* :pswitch_7 */
					 /* move-object v0, p1 */
					 /* check-cast v0, Lcom/android/server/DarkModeStauesEvent; */
					 com.android.server.DarkModeOneTrackHelper .updateAppStatusIntent ( p0,v0 );
					 /* .line 123 */
					 /* .line 119 */
					 /* :pswitch_8 */
					 /* move-object v0, p1 */
					 /* check-cast v0, Lcom/android/server/DarkModeStauesEvent; */
					 com.android.server.DarkModeOneTrackHelper .updateDarkModeStatusIntent ( p0,v0 );
					 /* .line 120 */
					 /* nop */
					 /* .line 148 */
				 } // :goto_2
				 return;
				 /* :sswitch_data_0 */
				 /* .sparse-switch */
				 /* -0x5ce9f380 -> :sswitch_8 */
				 /* -0x5c49afa9 -> :sswitch_7 */
				 /* -0x33208de6 -> :sswitch_6 */
				 /* -0x32804778 -> :sswitch_5 */
				 /* -0xa97afe2 -> :sswitch_4 */
				 /* 0x0 -> :sswitch_3 */
				 /* 0x375a7b97 -> :sswitch_2 */
				 /* 0x6123e11c -> :sswitch_1 */
				 /* 0x6264689f -> :sswitch_0 */
			 } // .end sparse-switch
			 /* :pswitch_data_0 */
			 /* .packed-switch 0x0 */
			 /* :pswitch_8 */
			 /* :pswitch_7 */
			 /* :pswitch_6 */
			 /* :pswitch_5 */
			 /* :pswitch_4 */
			 /* :pswitch_3 */
			 /* :pswitch_2 */
			 /* :pswitch_1 */
			 /* :pswitch_0 */
		 } // .end packed-switch
	 } // .end method
	 private static void updateDarkModeSettingIntent ( android.content.Intent p0, com.android.server.DarkModeStauesEvent p1 ) {
		 /* .locals 3 */
		 /* .param p0, "intent" # Landroid/content/Intent; */
		 /* .param p1, "event" # Lcom/android/server/DarkModeStauesEvent; */
		 /* .line 201 */
		 final String v0 = "EVENT_NAME"; // const-string v0, "EVENT_NAME"
		 /* const-string/jumbo v1, "setting" */
		 (( android.content.Intent ) p0 ).putExtra ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 202 */
		 (( com.android.server.DarkModeStauesEvent ) p1 ).getTip ( ); // invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getTip()Ljava/lang/String;
		 /* const-string/jumbo v2, "tip" */
		 (( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 203 */
		 (( com.android.server.DarkModeStauesEvent ) p1 ).getDarkModeStatus ( ); // invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getDarkModeStatus()Ljava/lang/String;
		 final String v2 = "dark_status"; // const-string v2, "dark_status"
		 (( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 204 */
		 (( com.android.server.DarkModeStauesEvent ) p1 ).getSettingChannel ( ); // invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getSettingChannel()Ljava/lang/String;
		 /* const-string/jumbo v2, "setting_channel" */
		 (( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 205 */
		 return;
	 } // .end method
	 private static void updateDarkModeStatusIntent ( android.content.Intent p0, com.android.server.DarkModeStauesEvent p1 ) {
		 /* .locals 3 */
		 /* .param p0, "intent" # Landroid/content/Intent; */
		 /* .param p1, "event" # Lcom/android/server/DarkModeStauesEvent; */
		 /* .line 151 */
		 final String v0 = "EVENT_NAME"; // const-string v0, "EVENT_NAME"
		 /* const-string/jumbo v1, "status" */
		 (( android.content.Intent ) p0 ).putExtra ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 152 */
		 (( com.android.server.DarkModeStauesEvent ) p1 ).getTip ( ); // invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getTip()Ljava/lang/String;
		 /* const-string/jumbo v2, "tip" */
		 (( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 153 */
		 (( com.android.server.DarkModeStauesEvent ) p1 ).getDarkModeStatus ( ); // invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getDarkModeStatus()Ljava/lang/String;
		 final String v2 = "dark_status"; // const-string v2, "dark_status"
		 (( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 154 */
		 (( com.android.server.DarkModeStauesEvent ) p1 ).getTimeModeStatus ( ); // invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getTimeModeStatus()Ljava/lang/String;
		 final String v2 = "dark_mode_timing_status"; // const-string v2, "dark_mode_timing_status"
		 (( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 155 */
		 (( com.android.server.DarkModeStauesEvent ) p1 ).getTimeModePattern ( ); // invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getTimeModePattern()Ljava/lang/String;
		 final String v2 = "dark_mode_timing_pattern"; // const-string v2, "dark_mode_timing_pattern"
		 (( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 156 */
		 (( com.android.server.DarkModeStauesEvent ) p1 ).getBeginTime ( ); // invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getBeginTime()Ljava/lang/String;
		 final String v2 = "begin_time"; // const-string v2, "begin_time"
		 (( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 157 */
		 (( com.android.server.DarkModeStauesEvent ) p1 ).getEndTime ( ); // invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getEndTime()Ljava/lang/String;
		 final String v2 = "end_time"; // const-string v2, "end_time"
		 (( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 158 */
		 (( com.android.server.DarkModeStauesEvent ) p1 ).getWallPaperStatus ( ); // invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getWallPaperStatus()Ljava/lang/String;
		 /* const-string/jumbo v2, "wallpaper_status" */
		 (( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 159 */
		 (( com.android.server.DarkModeStauesEvent ) p1 ).getContrastStatus ( ); // invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getContrastStatus()Ljava/lang/String;
		 final String v2 = "font_bgcolor_status"; // const-string v2, "font_bgcolor_status"
		 (( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 160 */
		 return;
	 } // .end method
	 private static void updateSuggestEventIntent ( android.content.Intent p0, com.android.server.DarkModeStauesEvent p1 ) {
		 /* .locals 2 */
		 /* .param p0, "intent" # Landroid/content/Intent; */
		 /* .param p1, "event" # Lcom/android/server/DarkModeStauesEvent; */
		 /* .line 214 */
		 final String v0 = "EVENT_NAME"; // const-string v0, "EVENT_NAME"
		 final String v1 = "darkModeSuggest"; // const-string v1, "darkModeSuggest"
		 (( android.content.Intent ) p0 ).putExtra ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 215 */
		 v0 = 		 (( com.android.server.DarkModeStauesEvent ) p1 ).getSuggest ( ); // invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getSuggest()I
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 216 */
			 final String v0 = "dark_mode_mode_suggest"; // const-string v0, "dark_mode_mode_suggest"
			 v1 = 			 (( com.android.server.DarkModeStauesEvent ) p1 ).getSuggest ( ); // invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getSuggest()I
			 (( android.content.Intent ) p0 ).putExtra ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
			 /* .line 218 */
		 } // :cond_0
		 v0 = 		 (( com.android.server.DarkModeStauesEvent ) p1 ).getSuggestEnable ( ); // invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getSuggestEnable()I
		 if ( v0 != null) { // if-eqz v0, :cond_1
			 /* .line 219 */
			 final String v0 = "dark_mode_suggest_enable"; // const-string v0, "dark_mode_suggest_enable"
			 v1 = 			 (( com.android.server.DarkModeStauesEvent ) p1 ).getSuggestEnable ( ); // invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getSuggestEnable()I
			 (( android.content.Intent ) p0 ).putExtra ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
			 /* .line 221 */
		 } // :cond_1
		 v0 = 		 (( com.android.server.DarkModeStauesEvent ) p1 ).getSuggestClick ( ); // invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getSuggestClick()I
		 if ( v0 != null) { // if-eqz v0, :cond_2
			 /* .line 222 */
			 final String v0 = "dark_mode_suggest_enter_settings"; // const-string v0, "dark_mode_suggest_enter_settings"
			 v1 = 			 (( com.android.server.DarkModeStauesEvent ) p1 ).getSuggestClick ( ); // invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getSuggestClick()I
			 (( android.content.Intent ) p0 ).putExtra ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
			 /* .line 224 */
		 } // :cond_2
		 v0 = 		 (( com.android.server.DarkModeStauesEvent ) p1 ).getSuggestOpenInSetting ( ); // invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getSuggestOpenInSetting()I
		 if ( v0 != null) { // if-eqz v0, :cond_3
			 /* .line 225 */
			 final String v0 = "open_dark_mode_from_settings"; // const-string v0, "open_dark_mode_from_settings"
			 v1 = 			 (( com.android.server.DarkModeStauesEvent ) p1 ).getSuggestOpenInSetting ( ); // invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getSuggestOpenInSetting()I
			 (( android.content.Intent ) p0 ).putExtra ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
			 /* .line 227 */
		 } // :cond_3
		 return;
	 } // .end method
	 private static void updateTimeModeSettingIntent ( android.content.Intent p0, com.android.server.DarkModeStauesEvent p1 ) {
		 /* .locals 3 */
		 /* .param p0, "intent" # Landroid/content/Intent; */
		 /* .param p1, "event" # Lcom/android/server/DarkModeStauesEvent; */
		 /* .line 169 */
		 final String v0 = "EVENT_NAME"; // const-string v0, "EVENT_NAME"
		 /* const-string/jumbo v1, "setting" */
		 (( android.content.Intent ) p0 ).putExtra ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 170 */
		 (( com.android.server.DarkModeStauesEvent ) p1 ).getTip ( ); // invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getTip()Ljava/lang/String;
		 /* const-string/jumbo v2, "tip" */
		 (( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 171 */
		 (( com.android.server.DarkModeStauesEvent ) p1 ).getTimeModeStatus ( ); // invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getTimeModeStatus()Ljava/lang/String;
		 final String v2 = "dark_mode_timing_status"; // const-string v2, "dark_mode_timing_status"
		 (( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 172 */
		 (( com.android.server.DarkModeStauesEvent ) p1 ).getTimeModePattern ( ); // invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getTimeModePattern()Ljava/lang/String;
		 final String v2 = "dark_mode_timing_pattern"; // const-string v2, "dark_mode_timing_pattern"
		 (( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 173 */
		 (( com.android.server.DarkModeStauesEvent ) p1 ).getBeginTime ( ); // invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getBeginTime()Ljava/lang/String;
		 final String v2 = "begin_time"; // const-string v2, "begin_time"
		 (( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 174 */
		 (( com.android.server.DarkModeStauesEvent ) p1 ).getEndTime ( ); // invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getEndTime()Ljava/lang/String;
		 final String v2 = "end_time"; // const-string v2, "end_time"
		 (( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 175 */
		 /* const-string/jumbo v0, "\u65e5\u51fa\u65e5\u843d\u6a21\u5f0f" */
		 (( com.android.server.DarkModeStauesEvent ) p1 ).getTimeModePattern ( ); // invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getTimeModePattern()Ljava/lang/String;
		 v0 = 		 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 176 */
			 /* const-string/jumbo v0, "setting_channel" */
			 (( com.android.server.DarkModeStauesEvent ) p1 ).getSettingChannel ( ); // invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getSettingChannel()Ljava/lang/String;
			 (( android.content.Intent ) p0 ).putExtra ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
			 /* .line 178 */
		 } // :cond_0
		 return;
	 } // .end method
	 private static void updateWallPaperSettingIntent ( android.content.Intent p0, com.android.server.DarkModeStauesEvent p1 ) {
		 /* .locals 3 */
		 /* .param p0, "intent" # Landroid/content/Intent; */
		 /* .param p1, "event" # Lcom/android/server/DarkModeStauesEvent; */
		 /* .line 181 */
		 final String v0 = "EVENT_NAME"; // const-string v0, "EVENT_NAME"
		 /* const-string/jumbo v1, "setting" */
		 (( android.content.Intent ) p0 ).putExtra ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 182 */
		 (( com.android.server.DarkModeStauesEvent ) p1 ).getTip ( ); // invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getTip()Ljava/lang/String;
		 /* const-string/jumbo v2, "tip" */
		 (( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 183 */
		 (( com.android.server.DarkModeStauesEvent ) p1 ).getWallPaperStatus ( ); // invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getWallPaperStatus()Ljava/lang/String;
		 final String v2 = "after_click_status"; // const-string v2, "after_click_status"
		 (( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 184 */
		 return;
	 } // .end method
	 public static void uploadToOneTrack ( android.content.Context p0, com.android.server.DarkModeEvent p1 ) {
		 /* .locals 3 */
		 /* .param p0, "context" # Landroid/content/Context; */
		 /* .param p1, "event" # Lcom/android/server/DarkModeEvent; */
		 /* .line 85 */
		 v0 = 		 v0 = com.android.server.DarkModeOneTrackHelper.sRegions;
		 /* if-nez v0, :cond_0 */
		 v0 = com.android.server.DarkModeOneTrackHelper.sRegions;
		 v0 = 		 v1 = com.android.server.DarkModeOneTrackHelper.DEVICE_REGION;
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 86 */
			 /* new-instance v0, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
			 final String v2 = "do not upload data in "; // const-string v2, "do not upload data in "
			 (( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 final String v1 = "DarkModeOneTrackHelper"; // const-string v1, "DarkModeOneTrackHelper"
			 android.util.Slog .i ( v1,v0 );
			 /* .line 87 */
			 return;
			 /* .line 89 */
		 } // :cond_0
		 if ( p0 != null) { // if-eqz p0, :cond_2
			 if ( p1 != null) { // if-eqz p1, :cond_2
				 if ( v0 != null) { // if-eqz v0, :cond_2
					 /* if-nez v0, :cond_1 */
					 /* .line 92 */
				 } // :cond_1
				 /* .line 93 */
				 /* .local v0, "newEvent":Lcom/android/server/DarkModeEvent; */
				 com.android.server.MiuiBgThread .getHandler ( );
				 /* new-instance v2, Lcom/android/server/DarkModeOneTrackHelper$1; */
				 /* invoke-direct {v2, p0, v0}, Lcom/android/server/DarkModeOneTrackHelper$1;-><init>(Landroid/content/Context;Lcom/android/server/DarkModeEvent;)V */
				 (( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
				 /* .line 109 */
				 return;
				 /* .line 90 */
			 } // .end local v0 # "newEvent":Lcom/android/server/DarkModeEvent;
		 } // :cond_2
	 } // :goto_0
	 return;
} // .end method
