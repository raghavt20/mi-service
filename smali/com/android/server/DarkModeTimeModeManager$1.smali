.class Lcom/android/server/DarkModeTimeModeManager$1;
.super Landroid/content/BroadcastReceiver;
.source "DarkModeTimeModeManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/DarkModeTimeModeManager;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/DarkModeTimeModeManager;


# direct methods
.method constructor <init>(Lcom/android/server/DarkModeTimeModeManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/DarkModeTimeModeManager;

    .line 73
    iput-object p1, p0, Lcom/android/server/DarkModeTimeModeManager$1;->this$0:Lcom/android/server/DarkModeTimeModeManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 76
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 77
    .local v0, "action":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " onReceive: action = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "DarkModeTimeModeManager"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    goto :goto_0

    :sswitch_0
    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_1

    :sswitch_1
    const-string v1, "miui.action.intent.DARK_MODE_SUGGEST_MESSAGE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    goto :goto_1

    :sswitch_2
    const-string v1, "miui.action.intent.DARK_MODE_SUGGEST_ENABLE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_1

    :goto_0
    const/4 v1, -0x1

    :goto_1
    packed-switch v1, :pswitch_data_0

    goto :goto_2

    .line 88
    :pswitch_0
    iget-object v1, p0, Lcom/android/server/DarkModeTimeModeManager$1;->this$0:Lcom/android/server/DarkModeTimeModeManager;

    invoke-virtual {v1, p1}, Lcom/android/server/DarkModeTimeModeManager;->enterSettingsFromNotification(Landroid/content/Context;)V

    .line 89
    goto :goto_2

    .line 85
    :pswitch_1
    iget-object v1, p0, Lcom/android/server/DarkModeTimeModeManager$1;->this$0:Lcom/android/server/DarkModeTimeModeManager;

    invoke-virtual {v1, p1}, Lcom/android/server/DarkModeTimeModeManager;->showDarkModeSuggestToast(Landroid/content/Context;)V

    .line 86
    goto :goto_2

    .line 80
    :pswitch_2
    iget-object v1, p0, Lcom/android/server/DarkModeTimeModeManager$1;->this$0:Lcom/android/server/DarkModeTimeModeManager;

    invoke-virtual {v1, p1}, Lcom/android/server/DarkModeTimeModeManager;->canShowDarkModeSuggestNotifocation(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 81
    iget-object v1, p0, Lcom/android/server/DarkModeTimeModeManager$1;->this$0:Lcom/android/server/DarkModeTimeModeManager;

    invoke-virtual {v1, p1}, Lcom/android/server/DarkModeTimeModeManager;->showDarkModeSuggestNotification(Landroid/content/Context;)V

    .line 93
    :cond_1
    :goto_2
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7957c397 -> :sswitch_2
        -0x18c53a3f -> :sswitch_1
        0x311a1d6c -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
