.class final Lcom/android/server/tof/ContactlessGestureService$BinderService;
.super Landroid/os/Binder;
.source "ContactlessGestureService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/tof/ContactlessGestureService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "BinderService"
.end annotation


# instance fields
.field mTofServiceShellCommand:Lcom/android/server/tof/ContactlessGestureService$TofServiceShellCommand;

.field final synthetic this$0:Lcom/android/server/tof/ContactlessGestureService;


# direct methods
.method private constructor <init>(Lcom/android/server/tof/ContactlessGestureService;)V
    .locals 2

    .line 638
    iput-object p1, p0, Lcom/android/server/tof/ContactlessGestureService$BinderService;->this$0:Lcom/android/server/tof/ContactlessGestureService;

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 639
    new-instance v0, Lcom/android/server/tof/ContactlessGestureService$TofServiceShellCommand;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/android/server/tof/ContactlessGestureService$TofServiceShellCommand;-><init>(Lcom/android/server/tof/ContactlessGestureService;Lcom/android/server/tof/ContactlessGestureService$TofServiceShellCommand-IA;)V

    iput-object v0, p0, Lcom/android/server/tof/ContactlessGestureService$BinderService;->mTofServiceShellCommand:Lcom/android/server/tof/ContactlessGestureService$TofServiceShellCommand;

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/tof/ContactlessGestureService;Lcom/android/server/tof/ContactlessGestureService$BinderService-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/tof/ContactlessGestureService$BinderService;-><init>(Lcom/android/server/tof/ContactlessGestureService;)V

    return-void
.end method

.method private getTofGestureAppData(Landroid/os/Parcel;Landroid/os/Parcel;)V
    .locals 4
    .param p1, "data"    # Landroid/os/Parcel;
    .param p2, "reply"    # Landroid/os/Parcel;

    .line 675
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 677
    .local v0, "token":J
    :try_start_0
    const-string v2, "com.miui.tof.TofManager"

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 678
    iget-object v2, p0, Lcom/android/server/tof/ContactlessGestureService$BinderService;->this$0:Lcom/android/server/tof/ContactlessGestureService;

    invoke-static {v2}, Lcom/android/server/tof/ContactlessGestureService;->-$$Nest$fgetmContactlessGestureController(Lcom/android/server/tof/ContactlessGestureService;)Lcom/android/server/tof/ContactlessGestureController;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/server/tof/ContactlessGestureController;->getTofGestureAppData()Lcom/miui/tof/gesture/TofGestureAppData;

    move-result-object v2

    .line 679
    .local v2, "appData":Lcom/miui/tof/gesture/TofGestureAppData;
    invoke-virtual {p2}, Landroid/os/Parcel;->writeNoException()V

    .line 680
    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 682
    .end local v2    # "appData":Lcom/miui/tof/gesture/TofGestureAppData;
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 683
    nop

    .line 684
    return-void

    .line 682
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 683
    throw v2
.end method

.method private setTofGestureConfig(Landroid/os/Parcel;)V
    .locals 6
    .param p1, "data"    # Landroid/os/Parcel;

    .line 662
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 664
    .local v0, "token":J
    :try_start_0
    invoke-static {}, Landroid/os/Binder;->getCallingUserHandle()Landroid/os/UserHandle;

    move-result-object v2

    .line 665
    .local v2, "userHandle":Landroid/os/UserHandle;
    const-string v3, "com.miui.tof.TofManager"

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 666
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 667
    .local v3, "packageName":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readBoolean()Z

    move-result v4

    .line 668
    .local v4, "enable":Z
    iget-object v5, p0, Lcom/android/server/tof/ContactlessGestureService$BinderService;->this$0:Lcom/android/server/tof/ContactlessGestureService;

    invoke-static {v5}, Lcom/android/server/tof/ContactlessGestureService;->-$$Nest$fgetmContactlessGestureController(Lcom/android/server/tof/ContactlessGestureService;)Lcom/android/server/tof/ContactlessGestureController;

    move-result-object v5

    invoke-virtual {v5, v3, v4}, Lcom/android/server/tof/ContactlessGestureController;->changeAppConfigWithPackageName(Ljava/lang/String;Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 670
    nop

    .end local v2    # "userHandle":Landroid/os/UserHandle;
    .end local v3    # "packageName":Ljava/lang/String;
    .end local v4    # "enable":Z
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 671
    nop

    .line 672
    return-void

    .line 670
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 671
    throw v2
.end method


# virtual methods
.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 3
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "pw"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;

    .line 697
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService$BinderService;->this$0:Lcom/android/server/tof/ContactlessGestureService;

    invoke-static {v0}, Lcom/android/server/tof/ContactlessGestureService;->-$$Nest$fgetmContext(Lcom/android/server/tof/ContactlessGestureService;)Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/android/server/tof/ContactlessGestureService;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2}, Lcom/android/internal/util/DumpUtils;->checkDumpPermission(Landroid/content/Context;Ljava/lang/String;Ljava/io/PrintWriter;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 698
    return-void

    .line 700
    :cond_0
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 702
    .local v0, "ident":J
    :try_start_0
    iget-object v2, p0, Lcom/android/server/tof/ContactlessGestureService$BinderService;->this$0:Lcom/android/server/tof/ContactlessGestureService;

    invoke-static {v2, p2}, Lcom/android/server/tof/ContactlessGestureService;->-$$Nest$mdumpInternal(Lcom/android/server/tof/ContactlessGestureService;Ljava/io/PrintWriter;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 704
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 705
    nop

    .line 706
    return-void

    .line 704
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 705
    throw v2
.end method

.method public onShellCommand(Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;[Ljava/lang/String;Landroid/os/ShellCallback;Landroid/os/ResultReceiver;)V
    .locals 8
    .param p1, "in"    # Ljava/io/FileDescriptor;
    .param p2, "out"    # Ljava/io/FileDescriptor;
    .param p3, "err"    # Ljava/io/FileDescriptor;
    .param p4, "args"    # [Ljava/lang/String;
    .param p5, "callback"    # Landroid/os/ShellCallback;
    .param p6, "resultReceiver"    # Landroid/os/ResultReceiver;

    .line 691
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService$BinderService;->mTofServiceShellCommand:Lcom/android/server/tof/ContactlessGestureService$TofServiceShellCommand;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-virtual/range {v0 .. v7}, Lcom/android/server/tof/ContactlessGestureService$TofServiceShellCommand;->exec(Landroid/os/Binder;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;[Ljava/lang/String;Landroid/os/ShellCallback;Landroid/os/ResultReceiver;)I

    .line 693
    return-void
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 2
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 644
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService$BinderService;->this$0:Lcom/android/server/tof/ContactlessGestureService;

    invoke-static {v0}, Lcom/android/server/tof/ContactlessGestureService;->-$$Nest$fgetmTofGestureSupport(Lcom/android/server/tof/ContactlessGestureService;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService$BinderService;->this$0:Lcom/android/server/tof/ContactlessGestureService;

    invoke-static {v0}, Lcom/android/server/tof/ContactlessGestureService;->-$$Nest$fgetmAonGestureSupport(Lcom/android/server/tof/ContactlessGestureService;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 645
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    return v0

    .line 648
    :cond_0
    const/4 v0, 0x1

    packed-switch p1, :pswitch_data_0

    .line 657
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    return v0

    .line 650
    :pswitch_0
    invoke-direct {p0, p2}, Lcom/android/server/tof/ContactlessGestureService$BinderService;->setTofGestureConfig(Landroid/os/Parcel;)V

    .line 651
    invoke-static {}, Landroid/os/Binder;->getCallingUserHandle()Landroid/os/UserHandle;

    move-result-object v1

    .line 652
    .local v1, "userHandle":Landroid/os/UserHandle;
    return v0

    .line 654
    .end local v1    # "userHandle":Landroid/os/UserHandle;
    :pswitch_1
    invoke-direct {p0, p2, p3}, Lcom/android/server/tof/ContactlessGestureService$BinderService;->getTofGestureAppData(Landroid/os/Parcel;Landroid/os/Parcel;)V

    .line 655
    return v0

    :pswitch_data_0
    .packed-switch 0xfffffd
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
