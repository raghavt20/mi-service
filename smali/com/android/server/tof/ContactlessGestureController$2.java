class com.android.server.tof.ContactlessGestureController$2 implements android.hardware.display.DisplayManager$DisplayListener {
	 /* .source "ContactlessGestureController.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/tof/ContactlessGestureController;->registerDisplayListener()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.tof.ContactlessGestureController this$0; //synthetic
/* # direct methods */
public static void $r8$lambda$bFtpEDHLlXHJlGnmeXaIv1SREMc ( com.android.server.tof.ContactlessGestureController$2 p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureController$2;->lambda$onDisplayChanged$0()V */
return;
} // .end method
 com.android.server.tof.ContactlessGestureController$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/tof/ContactlessGestureController; */
/* .line 886 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
private void lambda$onDisplayChanged$0 ( ) { //synthethic
/* .locals 2 */
/* .line 901 */
v0 = this.this$0;
int v1 = 3; // const/4 v1, 0x3
(( com.android.server.tof.ContactlessGestureController ) v0 ).onSceneChanged ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/tof/ContactlessGestureController;->onSceneChanged(I)V
return;
} // .end method
/* # virtual methods */
public void onDisplayAdded ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "displayId" # I */
/* .line 890 */
return;
} // .end method
public void onDisplayChanged ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "displayId" # I */
/* .line 899 */
v0 = this.this$0;
com.android.server.tof.ContactlessGestureController .-$$Nest$mupdateDefaultDisplayRotation ( v0 );
/* .line 900 */
v0 = this.this$0;
com.android.server.tof.ContactlessGestureController .-$$Nest$mcheckTopActivities ( v0 );
/* .line 901 */
v0 = this.this$0;
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/tof/ContactlessGestureController$2$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0}, Lcom/android/server/tof/ContactlessGestureController$2$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/tof/ContactlessGestureController$2;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 902 */
return;
} // .end method
public void onDisplayRemoved ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "displayId" # I */
/* .line 895 */
return;
} // .end method
