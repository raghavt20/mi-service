class com.android.server.tof.ContactlessGestureController$1 implements java.lang.Runnable {
	 /* .source "ContactlessGestureController.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/tof/ContactlessGestureController; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.tof.ContactlessGestureController this$0; //synthetic
/* # direct methods */
 com.android.server.tof.ContactlessGestureController$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/tof/ContactlessGestureController; */
/* .line 792 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 12 */
/* .line 795 */
final String v0 = "ContactlessGestureController"; // const-string v0, "ContactlessGestureController"
int v1 = 0; // const/4 v1, 0x0
/* .line 796 */
/* .local v1, "splitScreenModeChange":Z */
int v2 = 0; // const/4 v2, 0x0
/* .line 797 */
/* .local v2, "topActivity":Landroid/content/ComponentName; */
int v3 = 1; // const/4 v3, 0x1
/* .line 799 */
/* .local v3, "topActivityFullScreenOrNotOccluded":Z */
int v4 = 1; // const/4 v4, 0x1
try { // :try_start_0
	 v5 = 	 android.app.ActivityTaskManager .getService ( );
	 /* .line 800 */
	 /* .local v5, "splitScreenMode":Z */
	 v6 = this.this$0;
	 v6 = 	 com.android.server.tof.ContactlessGestureController .-$$Nest$fgetmIsSplitScreenMode ( v6 );
	 /* if-eq v5, v6, :cond_0 */
	 /* .line 801 */
	 v6 = this.this$0;
	 com.android.server.tof.ContactlessGestureController .-$$Nest$fputmIsSplitScreenMode ( v6,v5 );
	 /* .line 802 */
	 int v1 = 1; // const/4 v1, 0x1
	 /* .line 805 */
} // :cond_0
v6 = this.this$0;
com.android.server.tof.ContactlessGestureController .-$$Nest$fgetmActivityManager ( v6 );
/* .line 806 */
/* .local v6, "infos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityTaskManager$RootTaskInfo;>;" */
} // :cond_1
v8 = } // :goto_0
if ( v8 != null) { // if-eqz v8, :cond_8
/* check-cast v8, Landroid/app/ActivityTaskManager$RootTaskInfo; */
/* .line 808 */
/* .local v8, "info":Landroid/app/ActivityTaskManager$RootTaskInfo; */
/* iget v9, v8, Landroid/app/ActivityTaskManager$RootTaskInfo;->displayId:I */
/* if-nez v9, :cond_1 */
/* iget-boolean v9, v8, Landroid/app/ActivityTaskManager$RootTaskInfo;->visible:Z */
if ( v9 != null) { // if-eqz v9, :cond_1
v9 = this.topActivity;
/* .line 809 */
v9 = java.util.Objects .isNull ( v9 );
if ( v9 != null) { // if-eqz v9, :cond_2
	 /* .line 810 */
	 /* .line 814 */
} // :cond_2
v9 = (( android.app.ActivityTaskManager$RootTaskInfo ) v8 ).getWindowingMode ( ); // invoke-virtual {v8}, Landroid/app/ActivityTaskManager$RootTaskInfo;->getWindowingMode()I
/* .line 815 */
/* .local v9, "windoingMode":I */
v10 = this.this$0;
v10 = com.android.server.tof.ContactlessGestureController .-$$Nest$fgetmIsDeskTopMode ( v10 );
int v11 = 0; // const/4 v11, 0x0
/* if-nez v10, :cond_4 */
/* .line 816 */
v7 = this.topActivity;
/* move-object v2, v7 */
/* .line 817 */
/* if-ne v9, v4, :cond_3 */
/* move v11, v4 */
} // :cond_3
/* move v3, v11 */
/* .line 818 */
/* .line 821 */
} // :cond_4
/* if-ne v9, v4, :cond_5 */
/* .line 822 */
v7 = this.topActivity;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object v2, v7 */
/* .line 823 */
/* .line 825 */
} // :cond_5
if ( v3 != null) { // if-eqz v3, :cond_7
/* .line 827 */
int v10 = 5; // const/4 v10, 0x5
/* if-ne v9, v10, :cond_6 */
/* move v11, v4 */
} // :cond_6
/* move v3, v11 */
/* .line 830 */
} // .end local v8 # "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
} // .end local v9 # "windoingMode":I
} // :cond_7
/* .line 833 */
} // .end local v5 # "splitScreenMode":Z
} // .end local v6 # "infos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityTaskManager$RootTaskInfo;>;"
} // :cond_8
} // :goto_1
/* .line 831 */
/* :catch_0 */
/* move-exception v5 */
/* .line 832 */
/* .local v5, "e":Landroid/os/RemoteException; */
final String v6 = "cannot getTasks"; // const-string v6, "cannot getTasks"
android.util.Slog .e ( v0,v6,v5 );
/* .line 835 */
} // .end local v5 # "e":Landroid/os/RemoteException;
} // :goto_2
v5 = this.this$0;
v5 = com.android.server.tof.ContactlessGestureController .-$$Nest$fgetmTopActivityFullScreenOrNotOccluded ( v5 );
/* if-ne v3, v5, :cond_9 */
/* if-nez v1, :cond_9 */
if ( v2 != null) { // if-eqz v2, :cond_b
v5 = this.this$0;
com.android.server.tof.ContactlessGestureController .-$$Nest$fgetmTopActivity ( v5 );
/* .line 837 */
v5 = (( android.content.ComponentName ) v2 ).equals ( v5 ); // invoke-virtual {v2, v5}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z
/* if-nez v5, :cond_b */
/* .line 838 */
} // :cond_9
/* sget-boolean v5, Lcom/android/server/tof/ContactlessGestureService;->mDebug:Z */
if ( v5 != null) { // if-eqz v5, :cond_a
/* .line 839 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v6, "top activity status changed, mTopActivityFullScreenOrNotOccluded:" */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.this$0;
v6 = com.android.server.tof.ContactlessGestureController .-$$Nest$fgetmTopActivityFullScreenOrNotOccluded ( v6 );
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v6 = ", mIsDeskTopMode:"; // const-string v6, ", mIsDeskTopMode:"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.this$0;
v6 = com.android.server.tof.ContactlessGestureController .-$$Nest$fgetmIsDeskTopMode ( v6 );
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v6 = ", mTopActivity:"; // const-string v6, ", mTopActivity:"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v6 = ", mIsSplitScreenMode:"; // const-string v6, ", mIsSplitScreenMode:"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.this$0;
v6 = com.android.server.tof.ContactlessGestureController .-$$Nest$fgetmIsSplitScreenMode ( v6 );
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v5 );
/* .line 845 */
} // :cond_a
v0 = this.this$0;
com.android.server.tof.ContactlessGestureController .-$$Nest$fputmTopActivity ( v0,v2 );
/* .line 846 */
v0 = this.this$0;
com.android.server.tof.ContactlessGestureController .-$$Nest$fputmTopActivityFullScreenOrNotOccluded ( v0,v3 );
/* .line 847 */
v0 = this.this$0;
(( com.android.server.tof.ContactlessGestureController ) v0 ).onSceneChanged ( v4 ); // invoke-virtual {v0, v4}, Lcom/android/server/tof/ContactlessGestureController;->onSceneChanged(I)V
/* .line 849 */
} // :cond_b
return;
} // .end method
