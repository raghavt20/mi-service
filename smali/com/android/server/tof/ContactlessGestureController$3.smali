.class Lcom/android/server/tof/ContactlessGestureController$3;
.super Landroid/app/IMiuiActivityObserver$Stub;
.source "ContactlessGestureController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/tof/ContactlessGestureController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/tof/ContactlessGestureController;


# direct methods
.method constructor <init>(Lcom/android/server/tof/ContactlessGestureController;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/tof/ContactlessGestureController;

    .line 1090
    iput-object p1, p0, Lcom/android/server/tof/ContactlessGestureController$3;->this$0:Lcom/android/server/tof/ContactlessGestureController;

    invoke-direct {p0}, Landroid/app/IMiuiActivityObserver$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public activityDestroyed(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 1113
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController$3;->this$0:Lcom/android/server/tof/ContactlessGestureController;

    invoke-static {v0}, Lcom/android/server/tof/ContactlessGestureController;->-$$Nest$mcheckTopActivities(Lcom/android/server/tof/ContactlessGestureController;)V

    .line 1114
    return-void
.end method

.method public activityIdle(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;

    .line 1094
    return-void
.end method

.method public activityPaused(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 1104
    return-void
.end method

.method public activityResumed(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 1098
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController$3;->this$0:Lcom/android/server/tof/ContactlessGestureController;

    invoke-static {v0}, Lcom/android/server/tof/ContactlessGestureController;->-$$Nest$mcheckTopActivities(Lcom/android/server/tof/ContactlessGestureController;)V

    .line 1099
    return-void
.end method

.method public activityStopped(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 1108
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController$3;->this$0:Lcom/android/server/tof/ContactlessGestureController;

    invoke-static {v0}, Lcom/android/server/tof/ContactlessGestureController;->-$$Nest$mcheckTopActivities(Lcom/android/server/tof/ContactlessGestureController;)V

    .line 1109
    return-void
.end method

.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .line 1118
    return-object p0
.end method
