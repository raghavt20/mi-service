public class com.android.server.tof.TofGestureComponent {
	 /* .source "TofGestureComponent.java" */
	 /* # static fields */
	 public static final Integer CATEGORY_LONG_VIDEO;
	 public static final Integer CATEGORY_MUSIC;
	 public static final Integer CATEGORY_OFFICE;
	 public static final Integer CATEGORY_PHONE;
	 public static final Integer CATEGORY_READER;
	 public static final Integer CATEGORY_SHORT_VIDEO;
	 public static final Integer CATEGORY_UNKNOWN;
	 public static final Integer FEATURE_INVALID;
	 public static final Integer FLAG_EVENT_SWIPE_DOWN;
	 public static final Integer FLAG_EVENT_SWIPE_LEFT;
	 public static final Integer FLAG_EVENT_SWIPE_RIGHT;
	 public static final Integer FLAG_EVENT_SWIPE_UP;
	 public static final Integer FLAG_KEYCODE_CALL;
	 public static final Integer FLAG_KEYCODE_DPAD_LEFT;
	 public static final Integer FLAG_KEYCODE_DPAD_RIGHT;
	 public static final Integer FLAG_KEYCODE_ENDCALL;
	 public static final Integer FLAG_KEYCODE_MEDIA_FAST_FORWARD;
	 public static final Integer FLAG_KEYCODE_MEDIA_NEXT;
	 public static final Integer FLAG_KEYCODE_MEDIA_PLAY_PAUSE;
	 public static final Integer FLAG_KEYCODE_MEDIA_PREVIOUS;
	 public static final Integer FLAG_KEYCODE_MEDIA_REWIND;
	 public static final Integer FLAG_KEYCODE_PAGE_DOWN;
	 public static final Integer FLAG_KEYCODE_PAGE_UP;
	 public static final Integer FLAG_KEYCODE_SPACE_MEDIA_PLAY_PAUSE;
	 public static final Integer FLAG_KEYCODE_VOLUME_DOWN;
	 public static final Integer FLAG_KEYCODE_VOLUME_UP;
	 /* # instance fields */
	 private java.lang.String mActivityName;
	 private Integer mCategory;
	 private Boolean mEnable;
	 private Integer mLandScapeFeature;
	 private Integer mMaxVersionCode;
	 private Integer mMinVersionCode;
	 private java.lang.String mPackageName;
	 private Integer mPortraitFeature;
	 /* # direct methods */
	 public com.android.server.tof.TofGestureComponent ( ) {
		 /* .locals 0 */
		 /* .line 43 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 45 */
		 return;
	 } // .end method
	 public com.android.server.tof.TofGestureComponent ( ) {
		 /* .locals 0 */
		 /* .param p1, "pkgName" # Ljava/lang/String; */
		 /* .param p2, "activityName" # Ljava/lang/String; */
		 /* .param p3, "category" # I */
		 /* .param p4, "feature" # I */
		 /* .param p5, "featureVertical" # I */
		 /* .param p6, "minVersionCode" # I */
		 /* .param p7, "mMaxVersionCode" # I */
		 /* .line 49 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 50 */
		 this.mPackageName = p1;
		 /* .line 51 */
		 this.mActivityName = p2;
		 /* .line 52 */
		 /* iput p3, p0, Lcom/android/server/tof/TofGestureComponent;->mCategory:I */
		 /* .line 53 */
		 /* iput p4, p0, Lcom/android/server/tof/TofGestureComponent;->mLandScapeFeature:I */
		 /* .line 54 */
		 /* iput p5, p0, Lcom/android/server/tof/TofGestureComponent;->mPortraitFeature:I */
		 /* .line 55 */
		 /* iput p6, p0, Lcom/android/server/tof/TofGestureComponent;->mMinVersionCode:I */
		 /* .line 56 */
		 /* iput p7, p0, Lcom/android/server/tof/TofGestureComponent;->mMaxVersionCode:I */
		 /* .line 57 */
		 return;
	 } // .end method
	 public com.android.server.tof.TofGestureComponent ( ) {
		 /* .locals 0 */
		 /* .param p1, "pkgName" # Ljava/lang/String; */
		 /* .param p2, "activityName" # Ljava/lang/String; */
		 /* .param p3, "category" # I */
		 /* .param p4, "feature" # I */
		 /* .param p5, "featureVertical" # I */
		 /* .param p6, "minVersionCode" # I */
		 /* .param p7, "maxVersionCode" # I */
		 /* .param p8, "enable" # Z */
		 /* .line 61 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 62 */
		 this.mPackageName = p1;
		 /* .line 63 */
		 this.mActivityName = p2;
		 /* .line 64 */
		 /* iput p3, p0, Lcom/android/server/tof/TofGestureComponent;->mCategory:I */
		 /* .line 65 */
		 /* iput p4, p0, Lcom/android/server/tof/TofGestureComponent;->mLandScapeFeature:I */
		 /* .line 66 */
		 /* iput p5, p0, Lcom/android/server/tof/TofGestureComponent;->mPortraitFeature:I */
		 /* .line 67 */
		 /* iput p6, p0, Lcom/android/server/tof/TofGestureComponent;->mMinVersionCode:I */
		 /* .line 68 */
		 /* iput p7, p0, Lcom/android/server/tof/TofGestureComponent;->mMaxVersionCode:I */
		 /* .line 69 */
		 /* iput-boolean p8, p0, Lcom/android/server/tof/TofGestureComponent;->mEnable:Z */
		 /* .line 70 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public java.lang.String getActivityName ( ) {
		 /* .locals 1 */
		 /* .line 95 */
		 v0 = this.mActivityName;
	 } // .end method
	 public Integer getCategory ( ) {
		 /* .locals 1 */
		 /* .line 103 */
		 /* iget v0, p0, Lcom/android/server/tof/TofGestureComponent;->mCategory:I */
	 } // .end method
	 public Integer getLandscapeFeature ( ) {
		 /* .locals 1 */
		 /* .line 111 */
		 /* iget v0, p0, Lcom/android/server/tof/TofGestureComponent;->mLandScapeFeature:I */
	 } // .end method
	 public Integer getMaxVersionCode ( ) {
		 /* .locals 1 */
		 /* .line 135 */
		 /* iget v0, p0, Lcom/android/server/tof/TofGestureComponent;->mMaxVersionCode:I */
	 } // .end method
	 public Integer getMinVersionCode ( ) {
		 /* .locals 1 */
		 /* .line 127 */
		 /* iget v0, p0, Lcom/android/server/tof/TofGestureComponent;->mMinVersionCode:I */
	 } // .end method
	 public java.lang.String getPackageName ( ) {
		 /* .locals 1 */
		 /* .line 87 */
		 v0 = this.mPackageName;
	 } // .end method
	 public Integer getPortraitFeature ( ) {
		 /* .locals 1 */
		 /* .line 119 */
		 /* iget v0, p0, Lcom/android/server/tof/TofGestureComponent;->mPortraitFeature:I */
	 } // .end method
	 public Boolean isEnable ( ) {
		 /* .locals 1 */
		 /* .line 143 */
		 /* iget-boolean v0, p0, Lcom/android/server/tof/TofGestureComponent;->mEnable:Z */
	 } // .end method
	 public Boolean isVersionSupported ( Integer p0 ) {
		 /* .locals 6 */
		 /* .param p1, "versionCode" # I */
		 /* .line 151 */
		 /* iget v0, p0, Lcom/android/server/tof/TofGestureComponent;->mMinVersionCode:I */
		 int v1 = 1; // const/4 v1, 0x1
		 int v2 = 0; // const/4 v2, 0x0
		 /* if-lez v0, :cond_0 */
		 /* move v3, v1 */
	 } // :cond_0
	 /* move v3, v2 */
	 /* .line 152 */
	 /* .local v3, "isMinVersionValid":Z */
} // :goto_0
/* iget v4, p0, Lcom/android/server/tof/TofGestureComponent;->mMaxVersionCode:I */
/* if-lez v4, :cond_1 */
/* move v5, v1 */
} // :cond_1
/* move v5, v2 */
/* .line 154 */
/* .local v5, "isMaxVersionValid":Z */
} // :goto_1
if ( v3 != null) { // if-eqz v3, :cond_3
/* if-nez v5, :cond_3 */
/* .line 155 */
/* if-lt p1, v0, :cond_2 */
} // :cond_2
/* move v1, v2 */
} // :goto_2
/* .line 159 */
} // :cond_3
if ( v3 != null) { // if-eqz v3, :cond_5
if ( v5 != null) { // if-eqz v5, :cond_5
/* .line 160 */
/* if-lt p1, v0, :cond_4 */
/* if-gt p1, v4, :cond_4 */
} // :cond_4
/* move v1, v2 */
} // :goto_3
/* .line 164 */
} // :cond_5
if ( v5 != null) { // if-eqz v5, :cond_7
/* if-nez v3, :cond_7 */
/* .line 165 */
/* if-gt p1, v4, :cond_6 */
} // :cond_6
/* move v1, v2 */
} // :goto_4
/* .line 169 */
} // :cond_7
/* if-nez v3, :cond_8 */
/* if-nez v5, :cond_8 */
/* .line 170 */
/* .line 172 */
} // :cond_8
} // .end method
public void setActivityName ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "mActivityName" # Ljava/lang/String; */
/* .line 99 */
this.mActivityName = p1;
/* .line 100 */
return;
} // .end method
public void setCategory ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "mCategory" # I */
/* .line 107 */
/* iput p1, p0, Lcom/android/server/tof/TofGestureComponent;->mCategory:I */
/* .line 108 */
return;
} // .end method
public void setEnable ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "mEnable" # Z */
/* .line 147 */
/* iput-boolean p1, p0, Lcom/android/server/tof/TofGestureComponent;->mEnable:Z */
/* .line 148 */
return;
} // .end method
public void setLandScapeFeature ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "mFeature" # I */
/* .line 115 */
/* iput p1, p0, Lcom/android/server/tof/TofGestureComponent;->mLandScapeFeature:I */
/* .line 116 */
return;
} // .end method
public void setMaxVersionCode ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "maxVersionCode" # I */
/* .line 139 */
/* iput p1, p0, Lcom/android/server/tof/TofGestureComponent;->mMaxVersionCode:I */
/* .line 140 */
return;
} // .end method
public void setMinVersionCode ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "minVersionCode" # I */
/* .line 131 */
/* iput p1, p0, Lcom/android/server/tof/TofGestureComponent;->mMinVersionCode:I */
/* .line 132 */
return;
} // .end method
public void setPackageName ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "mPackageName" # Ljava/lang/String; */
/* .line 91 */
this.mPackageName = p1;
/* .line 92 */
return;
} // .end method
public void setPortraitFeature ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "mFeatureVertical" # I */
/* .line 123 */
/* iput p1, p0, Lcom/android/server/tof/TofGestureComponent;->mPortraitFeature:I */
/* .line 124 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 3 */
/* .line 74 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "TofGestureComponent{mPackageName=\'"; // const-string v1, "TofGestureComponent{mPackageName=\'"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mPackageName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v1, 0x27 */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
final String v2 = ", mActivityName=\'"; // const-string v2, ", mActivityName=\'"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mActivityName;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
final String v1 = ", mCategory="; // const-string v1, ", mCategory="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/tof/TofGestureComponent;->mCategory:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", mLandScapeFeature="; // const-string v1, ", mLandScapeFeature="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/tof/TofGestureComponent;->mLandScapeFeature:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", mPortraitFeature="; // const-string v1, ", mPortraitFeature="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/tof/TofGestureComponent;->mPortraitFeature:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", mMinVersionCode="; // const-string v1, ", mMinVersionCode="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/tof/TofGestureComponent;->mMinVersionCode:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", mMaxVersionCode="; // const-string v1, ", mMaxVersionCode="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/tof/TofGestureComponent;->mMaxVersionCode:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", mEnable="; // const-string v1, ", mEnable="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/tof/TofGestureComponent;->mEnable:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
/* const/16 v1, 0x7d */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
