.class public Lcom/android/server/tof/AppStatusManager;
.super Ljava/lang/Object;
.source "AppStatusManager.java"


# static fields
.field private static final GESTURE_DISABLED_APPS_KEY:Ljava/lang/String; = "gesture_disabled_apps_"

.field private static final GESTURE_ENABLED_APPS_KEY:Ljava/lang/String; = "gesture_enabled_apps_"

.field private static instances:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/android/server/tof/AppStatusManager;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mCachedDisabledApps:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mCachedEnabledApps:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mUserId:Ljava/lang/String;


# direct methods
.method public static synthetic $r8$lambda$7hnc7QMZIQEz3YuOOGpUzGa5I88(Lcom/android/server/tof/AppStatusManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/tof/AppStatusManager;->lambda$saveDisabledAppsAsync$0()V

    return-void
.end method

.method public static synthetic $r8$lambda$nScmJ-B-RuACglFtwT8jCjE7gK0(Lcom/android/server/tof/AppStatusManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/tof/AppStatusManager;->lambda$saveEnabledAppsAsync$1()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/server/tof/AppStatusManager;->instances:Ljava/util/HashMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Landroid/content/Context;)V
    .locals 0
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/android/server/tof/AppStatusManager;->mUserId:Ljava/lang/String;

    .line 41
    iput-object p2, p0, Lcom/android/server/tof/AppStatusManager;->mContext:Landroid/content/Context;

    .line 42
    invoke-direct {p0}, Lcom/android/server/tof/AppStatusManager;->init()V

    .line 43
    return-void
.end method

.method private static getOrCreateInstance(Landroid/content/Context;)Lcom/android/server/tof/AppStatusManager;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .line 235
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 236
    .local v0, "userId":Ljava/lang/String;
    sget-object v1, Lcom/android/server/tof/AppStatusManager;->instances:Ljava/util/HashMap;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 237
    sget-object v1, Lcom/android/server/tof/AppStatusManager;->instances:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/tof/AppStatusManager;

    return-object v1

    .line 240
    :cond_0
    new-instance v1, Lcom/android/server/tof/AppStatusManager;

    invoke-direct {v1, v0, p0}, Lcom/android/server/tof/AppStatusManager;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 241
    .local v1, "appManager":Lcom/android/server/tof/AppStatusManager;
    sget-object v2, Lcom/android/server/tof/AppStatusManager;->instances:Ljava/util/HashMap;

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 242
    return-object v1
.end method

.method private init()V
    .locals 5

    .line 131
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/server/tof/AppStatusManager;->mCachedDisabledApps:Ljava/util/Set;

    .line 132
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/server/tof/AppStatusManager;->mCachedEnabledApps:Ljava/util/Set;

    .line 133
    iget-object v0, p0, Lcom/android/server/tof/AppStatusManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "gesture_disabled_apps_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tof/AppStatusManager;->mUserId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 135
    .local v0, "disableApps":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const-string v2, ","

    if-nez v1, :cond_0

    .line 136
    iget-object v1, p0, Lcom/android/server/tof/AppStatusManager;->mCachedDisabledApps:Ljava/util/Set;

    invoke-static {v0, v2}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 138
    :cond_0
    iget-object v1, p0, Lcom/android/server/tof/AppStatusManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "gesture_enabled_apps_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/tof/AppStatusManager;->mUserId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 140
    .local v1, "enableApps":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 141
    iget-object v3, p0, Lcom/android/server/tof/AppStatusManager;->mCachedEnabledApps:Ljava/util/Set;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 143
    :cond_1
    return-void
.end method

.method public static isAppDisabled(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "pkg"    # Ljava/lang/String;

    .line 177
    if-nez p0, :cond_0

    .line 178
    const/4 v0, 0x0

    return v0

    .line 180
    :cond_0
    invoke-static {p0}, Lcom/android/server/tof/AppStatusManager;->getOrCreateInstance(Landroid/content/Context;)Lcom/android/server/tof/AppStatusManager;

    move-result-object v0

    .line 181
    .local v0, "appStatusManager":Lcom/android/server/tof/AppStatusManager;
    invoke-virtual {v0, p1}, Lcom/android/server/tof/AppStatusManager;->isDisabled(Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method public static isAppEnabled(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "pkg"    # Ljava/lang/String;

    .line 191
    if-nez p0, :cond_0

    .line 192
    const/4 v0, 0x0

    return v0

    .line 194
    :cond_0
    invoke-static {p0}, Lcom/android/server/tof/AppStatusManager;->getOrCreateInstance(Landroid/content/Context;)Lcom/android/server/tof/AppStatusManager;

    move-result-object v0

    .line 195
    .local v0, "appStatusManager":Lcom/android/server/tof/AppStatusManager;
    invoke-virtual {v0, p1}, Lcom/android/server/tof/AppStatusManager;->isEnabled(Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method private synthetic lambda$saveDisabledAppsAsync$0()V
    .locals 4

    .line 151
    const-string v0, ","

    iget-object v1, p0, Lcom/android/server/tof/AppStatusManager;->mCachedDisabledApps:Ljava/util/Set;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    .line 152
    .local v0, "appsString":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/tof/AppStatusManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "gesture_disabled_apps_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/tof/AppStatusManager;->mUserId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 154
    return-void
.end method

.method private synthetic lambda$saveEnabledAppsAsync$1()V
    .locals 4

    .line 163
    const-string v0, ","

    iget-object v1, p0, Lcom/android/server/tof/AppStatusManager;->mCachedEnabledApps:Ljava/util/Set;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    .line 164
    .local v0, "appsString":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/tof/AppStatusManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "gesture_enabled_apps_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/tof/AppStatusManager;->mUserId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 166
    return-void
.end method

.method private saveDisabledAppsAsync()V
    .locals 2

    .line 149
    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->get()Lcom/android/internal/os/BackgroundThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/internal/os/BackgroundThread;->getThreadHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/server/tof/AppStatusManager$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0}, Lcom/android/server/tof/AppStatusManager$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/tof/AppStatusManager;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 155
    return-void
.end method

.method private saveEnabledAppsAsync()V
    .locals 2

    .line 161
    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->get()Lcom/android/internal/os/BackgroundThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/internal/os/BackgroundThread;->getThreadHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/server/tof/AppStatusManager$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/android/server/tof/AppStatusManager$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/tof/AppStatusManager;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 167
    return-void
.end method

.method public static setAppEnable(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "enable"    # Z

    .line 219
    if-eqz p0, :cond_1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 222
    :cond_0
    invoke-static {p0}, Lcom/android/server/tof/AppStatusManager;->getOrCreateInstance(Landroid/content/Context;)Lcom/android/server/tof/AppStatusManager;

    move-result-object v0

    .line 223
    .local v0, "appStatusManager":Lcom/android/server/tof/AppStatusManager;
    invoke-virtual {v0, p1, p2}, Lcom/android/server/tof/AppStatusManager;->setEnable(Ljava/lang/String;Z)V

    .line 224
    return-void

    .line 220
    .end local v0    # "appStatusManager":Lcom/android/server/tof/AppStatusManager;
    :cond_1
    :goto_0
    return-void
.end method

.method public static statusHasChanged(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "pkg"    # Ljava/lang/String;

    .line 205
    const/4 v0, 0x0

    if-nez p0, :cond_0

    .line 206
    return v0

    .line 208
    :cond_0
    invoke-static {p0, p1}, Lcom/android/server/tof/AppStatusManager;->isAppDisabled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {p0, p1}, Lcom/android/server/tof/AppStatusManager;->isAppEnabled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :cond_2
    return v0
.end method


# virtual methods
.method public declared-synchronized addDisabledApp(Ljava/lang/String;)V
    .locals 1
    .param p1, "appPackageName"    # Ljava/lang/String;

    monitor-enter p0

    .line 52
    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/tof/AppStatusManager;->mCachedDisabledApps:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    invoke-direct {p0}, Lcom/android/server/tof/AppStatusManager;->saveDisabledAppsAsync()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 55
    .end local p0    # "this":Lcom/android/server/tof/AppStatusManager;
    :cond_0
    monitor-exit p0

    return-void

    .line 51
    .end local p1    # "appPackageName":Ljava/lang/String;
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized addEnabledApp(Ljava/lang/String;)V
    .locals 1
    .param p1, "appPackageName"    # Ljava/lang/String;

    monitor-enter p0

    .line 76
    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/tof/AppStatusManager;->mCachedEnabledApps:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    invoke-direct {p0}, Lcom/android/server/tof/AppStatusManager;->saveEnabledAppsAsync()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 79
    .end local p0    # "this":Lcom/android/server/tof/AppStatusManager;
    :cond_0
    monitor-exit p0

    return-void

    .line 75
    .end local p1    # "appPackageName":Ljava/lang/String;
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public isDisabled(Ljava/lang/String;)Z
    .locals 1
    .param p1, "pkg"    # Ljava/lang/String;

    .line 119
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/tof/AppStatusManager;->mCachedDisabledApps:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isEnabled(Ljava/lang/String;)Z
    .locals 1
    .param p1, "pkg"    # Ljava/lang/String;

    .line 123
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/tof/AppStatusManager;->mCachedEnabledApps:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public declared-synchronized removeDisabledApp(Ljava/lang/String;)V
    .locals 1
    .param p1, "appPackageName"    # Ljava/lang/String;

    monitor-enter p0

    .line 64
    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/tof/AppStatusManager;->mCachedDisabledApps:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    invoke-direct {p0}, Lcom/android/server/tof/AppStatusManager;->saveDisabledAppsAsync()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67
    .end local p0    # "this":Lcom/android/server/tof/AppStatusManager;
    :cond_0
    monitor-exit p0

    return-void

    .line 63
    .end local p1    # "appPackageName":Ljava/lang/String;
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized removeEnabledApp(Ljava/lang/String;)V
    .locals 1
    .param p1, "appPackageName"    # Ljava/lang/String;

    monitor-enter p0

    .line 88
    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/tof/AppStatusManager;->mCachedEnabledApps:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    invoke-direct {p0}, Lcom/android/server/tof/AppStatusManager;->saveEnabledAppsAsync()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 91
    .end local p0    # "this":Lcom/android/server/tof/AppStatusManager;
    :cond_0
    monitor-exit p0

    return-void

    .line 87
    .end local p1    # "appPackageName":Ljava/lang/String;
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public setEnable(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "enable"    # Z

    .line 99
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    return-void

    .line 103
    :cond_0
    if-eqz p2, :cond_1

    .line 104
    invoke-virtual {p0, p1}, Lcom/android/server/tof/AppStatusManager;->removeDisabledApp(Ljava/lang/String;)V

    .line 105
    invoke-virtual {p0, p1}, Lcom/android/server/tof/AppStatusManager;->addEnabledApp(Ljava/lang/String;)V

    goto :goto_0

    .line 107
    :cond_1
    invoke-virtual {p0, p1}, Lcom/android/server/tof/AppStatusManager;->addDisabledApp(Ljava/lang/String;)V

    .line 108
    invoke-virtual {p0, p1}, Lcom/android/server/tof/AppStatusManager;->removeEnabledApp(Ljava/lang/String;)V

    .line 110
    :goto_0
    return-void
.end method
