class com.android.server.tof.ToFGestureController extends com.android.server.tof.ContactlessGestureController {
	 /* .source "ToFGestureController.java" */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 private static final java.lang.String TOF_CLIENT_ACTION;
	 /* # instance fields */
	 private miui.tof.ITofClientService mService;
	 /* # direct methods */
	 public static void $r8$lambda$Et94CJNRsFJ-qKgC60Z8b6tDUU0 ( com.android.server.tof.ToFGestureController p0, android.os.IBinder p1 ) { //synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0, p1}, Lcom/android/server/tof/ToFGestureController;->lambda$onGestureServiceConnected$0(Landroid/os/IBinder;)V */
		 return;
	 } // .end method
	 public com.android.server.tof.ToFGestureController ( ) {
		 /* .locals 0 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .param p2, "handler" # Landroid/os/Handler; */
		 /* .param p3, "contactlessGestureService" # Lcom/android/server/tof/ContactlessGestureService; */
		 /* .line 25 */
		 /* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/tof/ContactlessGestureController;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/tof/ContactlessGestureService;)V */
		 /* .line 26 */
		 return;
	 } // .end method
	 private void lambda$onGestureServiceConnected$0 ( android.os.IBinder p0 ) { //synthethic
		 /* .locals 2 */
		 /* .param p1, "service" # Landroid/os/IBinder; */
		 /* .line 98 */
		 final String v0 = "TofGestureController"; // const-string v0, "TofGestureController"
		 final String v1 = "onServiceConnected"; // const-string v1, "onServiceConnected"
		 android.util.Slog .i ( v0,v1 );
		 /* .line 99 */
		 v0 = this.mService;
		 /* if-nez v0, :cond_0 */
		 /* .line 100 */
		 miui.tof.ITofClientService$Stub .asInterface ( p1 );
		 this.mService = v0;
		 /* .line 102 */
	 } // :cond_0
	 v0 = this.mServiceBindingLatch;
	 (( java.util.concurrent.CountDownLatch ) v0 ).countDown ( ); // invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V
	 /* .line 103 */
	 return;
} // .end method
/* # virtual methods */
public java.lang.String getAction ( ) {
	 /* .locals 1 */
	 /* .line 39 */
	 final String v0 = "miui.intent.action.TOF_SERVICE"; // const-string v0, "miui.intent.action.TOF_SERVICE"
} // .end method
public android.content.ComponentName getTofClientComponent ( ) {
	 /* .locals 2 */
	 /* .line 30 */
	 v0 = this.mContext;
	 (( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
	 /* const v1, 0x110f00aa */
	 (( android.content.res.Resources ) v0 ).getString ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
	 /* .line 31 */
	 /* .local v0, "value":Ljava/lang/String; */
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 32 */
		 android.content.ComponentName .unflattenFromString ( v0 );
		 /* .line 34 */
	 } // :cond_0
	 int v1 = 0; // const/4 v1, 0x0
} // .end method
public Boolean isServiceInit ( ) {
	 /* .locals 1 */
	 /* .line 86 */
	 v0 = this.mService;
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 int v0 = 1; // const/4 v0, 0x1
	 } // :cond_0
	 int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public void notifyRotationChanged ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "rotation" # I */
/* .line 61 */
return;
} // .end method
public void onGestureServiceConnected ( android.content.ComponentName p0, android.os.IBinder p1 ) {
/* .locals 2 */
/* .param p1, "name" # Landroid/content/ComponentName; */
/* .param p2, "service" # Landroid/os/IBinder; */
/* .line 97 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/tof/ToFGestureController$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0, p2}, Lcom/android/server/tof/ToFGestureController$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/tof/ToFGestureController;Landroid/os/IBinder;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 104 */
return;
} // .end method
public void registerGestureListenerIfNeeded ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "register" # Z */
/* .line 56 */
return;
} // .end method
public void restContactlessGestureService ( ) {
/* .locals 2 */
/* .line 91 */
int v0 = 0; // const/4 v0, 0x0
this.mService = v0;
/* .line 92 */
/* new-instance v0, Ljava/util/concurrent/CountDownLatch; */
int v1 = 1; // const/4 v1, 0x1
/* invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V */
this.mServiceBindingLatch = v0;
/* .line 93 */
return;
} // .end method
public void showGestureNotification ( ) {
/* .locals 3 */
/* .line 44 */
final String v0 = "TofGestureController"; // const-string v0, "TofGestureController"
v1 = this.mService;
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 46 */
	 try { // :try_start_0
		 /* .line 47 */
		 /* const-string/jumbo v1, "show Tof gesture notification" */
		 android.util.Slog .i ( v0,v1 );
		 /* :try_end_0 */
		 /* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 50 */
		 /* .line 48 */
		 /* :catch_0 */
		 /* move-exception v1 */
		 /* .line 49 */
		 /* .local v1, "e":Landroid/os/RemoteException; */
		 (( android.os.RemoteException ) v1 ).toString ( ); // invoke-virtual {v1}, Landroid/os/RemoteException;->toString()Ljava/lang/String;
		 android.util.Slog .e ( v0,v2 );
		 /* .line 52 */
	 } // .end local v1 # "e":Landroid/os/RemoteException;
} // :cond_0
} // :goto_0
return;
} // .end method
public Boolean updateGestureHint ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "label" # I */
/* .line 70 */
final String v0 = "TofGestureController"; // const-string v0, "TofGestureController"
v1 = this.mService;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 72 */
try { // :try_start_0
	 v2 = 	 (( com.android.server.tof.ToFGestureController ) p0 ).getFeatureFromLabel ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/tof/ToFGestureController;->getFeatureFromLabel(I)I
	 /* .line 73 */
	 v3 = 	 (( com.android.server.tof.ToFGestureController ) p0 ).getCurrentSupportFeature ( ); // invoke-virtual {p0}, Lcom/android/server/tof/ToFGestureController;->getCurrentSupportFeature()I
	 (( com.android.server.tof.ToFGestureController ) p0 ).getCurrentPkg ( ); // invoke-virtual {p0}, Lcom/android/server/tof/ToFGestureController;->getCurrentPkg()Ljava/lang/String;
	 /* .line 72 */
	 /* .line 74 */
	 /* sget-boolean v1, Lcom/android/server/tof/ContactlessGestureService;->mDebug:Z */
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 /* .line 75 */
		 /* new-instance v1, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v2 = "TMS update view label:"; // const-string v2, "TMS update view label:"
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( com.android.server.tof.ToFGestureController ) p0 ).gestureLabelToString ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/tof/ToFGestureController;->gestureLabelToString(I)Ljava/lang/String;
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Slog .i ( v0,v1 );
		 /* :try_end_0 */
		 /* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 79 */
	 } // :cond_0
	 /* .line 77 */
	 /* :catch_0 */
	 /* move-exception v1 */
	 /* .line 78 */
	 /* .local v1, "e":Landroid/os/RemoteException; */
	 (( android.os.RemoteException ) v1 ).toString ( ); // invoke-virtual {v1}, Landroid/os/RemoteException;->toString()Ljava/lang/String;
	 android.util.Slog .e ( v0,v2 );
	 /* .line 81 */
} // .end local v1 # "e":Landroid/os/RemoteException;
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
