.class final Lcom/android/server/tof/ContactlessGestureService$LocalService;
.super Lcom/android/server/tof/TofManagerInternal;
.source "ContactlessGestureService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/tof/ContactlessGestureService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "LocalService"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/tof/ContactlessGestureService;


# direct methods
.method public static synthetic $r8$lambda$0Jp5qHT5hRfmDo6cks81M352PV4(Lcom/android/server/tof/ContactlessGestureService$LocalService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService$LocalService;->lambda$onEarlyInteractivityChange$0()V

    return-void
.end method

.method public static synthetic $r8$lambda$VWW8xWsJoAluAb8jyTbr7PGptH8(Lcom/android/server/tof/ContactlessGestureService$LocalService;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/tof/ContactlessGestureService$LocalService;->lambda$updateGestureAppConfig$2(Ljava/util/List;)V

    return-void
.end method

.method public static synthetic $r8$lambda$j4E8pXQovqfT_9Ywblf_JWY5tPk(Lcom/android/server/tof/ContactlessGestureService$LocalService;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/tof/ContactlessGestureService$LocalService;->lambda$onDefaultDisplayFocusChanged$1(Ljava/lang/String;)V

    return-void
.end method

.method private constructor <init>(Lcom/android/server/tof/ContactlessGestureService;)V
    .locals 0

    .line 599
    iput-object p1, p0, Lcom/android/server/tof/ContactlessGestureService$LocalService;->this$0:Lcom/android/server/tof/ContactlessGestureService;

    invoke-direct {p0}, Lcom/android/server/tof/TofManagerInternal;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/tof/ContactlessGestureService;Lcom/android/server/tof/ContactlessGestureService$LocalService-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/tof/ContactlessGestureService$LocalService;-><init>(Lcom/android/server/tof/ContactlessGestureService;)V

    return-void
.end method

.method private synthetic lambda$onDefaultDisplayFocusChanged$1(Ljava/lang/String;)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 611
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService$LocalService;->this$0:Lcom/android/server/tof/ContactlessGestureService;

    invoke-static {v0}, Lcom/android/server/tof/ContactlessGestureService;->-$$Nest$fgetmTofGestureEnabled(Lcom/android/server/tof/ContactlessGestureService;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService$LocalService;->this$0:Lcom/android/server/tof/ContactlessGestureService;

    iget-boolean v0, v0, Lcom/android/server/tof/ContactlessGestureService;->mAonGestureEnabled:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService$LocalService;->this$0:Lcom/android/server/tof/ContactlessGestureService;

    invoke-static {v0}, Lcom/android/server/tof/ContactlessGestureService;->-$$Nest$fgetmContactlessGestureController(Lcom/android/server/tof/ContactlessGestureService;)Lcom/android/server/tof/ContactlessGestureController;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 613
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService$LocalService;->this$0:Lcom/android/server/tof/ContactlessGestureService;

    invoke-static {v0}, Lcom/android/server/tof/ContactlessGestureService;->-$$Nest$fgetmContactlessGestureController(Lcom/android/server/tof/ContactlessGestureService;)Lcom/android/server/tof/ContactlessGestureController;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/tof/ContactlessGestureController;->updateFocusedPackage(Ljava/lang/String;)V

    .line 615
    :cond_1
    return-void
.end method

.method private synthetic lambda$onEarlyInteractivityChange$0()V
    .locals 1

    .line 604
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService$LocalService;->this$0:Lcom/android/server/tof/ContactlessGestureService;

    invoke-static {v0}, Lcom/android/server/tof/ContactlessGestureService;->-$$Nest$mregisterTofProximityListenerIfNeeded(Lcom/android/server/tof/ContactlessGestureService;)V

    return-void
.end method

.method private synthetic lambda$updateGestureAppConfig$2(Ljava/util/List;)V
    .locals 2
    .param p1, "componentFeatureList"    # Ljava/util/List;

    .line 621
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService$LocalService;->this$0:Lcom/android/server/tof/ContactlessGestureService;

    invoke-static {v0}, Lcom/android/server/tof/ContactlessGestureService;->-$$Nest$misAonGestureSupport(Lcom/android/server/tof/ContactlessGestureService;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 622
    return-void

    .line 625
    :cond_0
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService$LocalService;->this$0:Lcom/android/server/tof/ContactlessGestureService;

    invoke-static {v0}, Lcom/android/server/tof/ContactlessGestureService;->-$$Nest$fgetmContactlessGestureController(Lcom/android/server/tof/ContactlessGestureService;)Lcom/android/server/tof/ContactlessGestureController;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 626
    invoke-static {}, Lcom/android/server/tof/ContactlessGestureService;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "updateGestureAppConfig"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 627
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService$LocalService;->this$0:Lcom/android/server/tof/ContactlessGestureService;

    invoke-static {v0}, Lcom/android/server/tof/ContactlessGestureService;->-$$Nest$fgetmContactlessGestureController(Lcom/android/server/tof/ContactlessGestureService;)Lcom/android/server/tof/ContactlessGestureController;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/tof/ContactlessGestureController;->parseGestureComponentFromCloud(Ljava/util/List;)V

    .line 628
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService$LocalService;->this$0:Lcom/android/server/tof/ContactlessGestureService;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/server/tof/ContactlessGestureService;->-$$Nest$fputmComponentFeatureList(Lcom/android/server/tof/ContactlessGestureService;Ljava/util/List;)V

    goto :goto_0

    .line 630
    :cond_1
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService$LocalService;->this$0:Lcom/android/server/tof/ContactlessGestureService;

    invoke-static {v0, p1}, Lcom/android/server/tof/ContactlessGestureService;->-$$Nest$fputmComponentFeatureList(Lcom/android/server/tof/ContactlessGestureService;Ljava/util/List;)V

    .line 632
    :goto_0
    return-void
.end method


# virtual methods
.method public onDefaultDisplayFocusChanged(Ljava/lang/String;)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .line 610
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService$LocalService;->this$0:Lcom/android/server/tof/ContactlessGestureService;

    invoke-static {v0}, Lcom/android/server/tof/ContactlessGestureService;->-$$Nest$fgetmHandler(Lcom/android/server/tof/ContactlessGestureService;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/server/tof/ContactlessGestureService$LocalService$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0, p1}, Lcom/android/server/tof/ContactlessGestureService$LocalService$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/tof/ContactlessGestureService$LocalService;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 616
    return-void
.end method

.method public onEarlyInteractivityChange(Z)V
    .locals 2
    .param p1, "interactive"    # Z

    .line 602
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService$LocalService;->this$0:Lcom/android/server/tof/ContactlessGestureService;

    invoke-static {v0}, Lcom/android/server/tof/ContactlessGestureService;->-$$Nest$fgetmTofProximitySupport(Lcom/android/server/tof/ContactlessGestureService;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 603
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService$LocalService;->this$0:Lcom/android/server/tof/ContactlessGestureService;

    invoke-static {v0, p1}, Lcom/android/server/tof/ContactlessGestureService;->-$$Nest$fputmIsInteractive(Lcom/android/server/tof/ContactlessGestureService;Z)V

    .line 604
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService$LocalService;->this$0:Lcom/android/server/tof/ContactlessGestureService;

    invoke-static {v0}, Lcom/android/server/tof/ContactlessGestureService;->-$$Nest$fgetmHandler(Lcom/android/server/tof/ContactlessGestureService;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/server/tof/ContactlessGestureService$LocalService$$ExternalSyntheticLambda2;

    invoke-direct {v1, p0}, Lcom/android/server/tof/ContactlessGestureService$LocalService$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/tof/ContactlessGestureService$LocalService;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 606
    :cond_0
    return-void
.end method

.method public updateGestureAppConfig(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 620
    .local p1, "componentFeatureList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService$LocalService;->this$0:Lcom/android/server/tof/ContactlessGestureService;

    invoke-static {v0}, Lcom/android/server/tof/ContactlessGestureService;->-$$Nest$fgetmHandler(Lcom/android/server/tof/ContactlessGestureService;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/server/tof/ContactlessGestureService$LocalService$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p1}, Lcom/android/server/tof/ContactlessGestureService$LocalService$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/tof/ContactlessGestureService$LocalService;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 634
    return-void
.end method
