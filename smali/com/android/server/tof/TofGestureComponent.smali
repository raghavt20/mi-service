.class public Lcom/android/server/tof/TofGestureComponent;
.super Ljava/lang/Object;
.source "TofGestureComponent.java"


# static fields
.field public static final CATEGORY_LONG_VIDEO:I = 0x1

.field public static final CATEGORY_MUSIC:I = 0x3

.field public static final CATEGORY_OFFICE:I = 0x4

.field public static final CATEGORY_PHONE:I = 0x5

.field public static final CATEGORY_READER:I = 0x6

.field public static final CATEGORY_SHORT_VIDEO:I = 0x2

.field public static final CATEGORY_UNKNOWN:I = 0x0

.field public static final FEATURE_INVALID:I = 0x0

.field public static final FLAG_EVENT_SWIPE_DOWN:I = 0x1000

.field public static final FLAG_EVENT_SWIPE_LEFT:I = 0x10000

.field public static final FLAG_EVENT_SWIPE_RIGHT:I = 0x20000

.field public static final FLAG_EVENT_SWIPE_UP:I = 0x800

.field public static final FLAG_KEYCODE_CALL:I = 0x80

.field public static final FLAG_KEYCODE_DPAD_LEFT:I = 0x4

.field public static final FLAG_KEYCODE_DPAD_RIGHT:I = 0x2

.field public static final FLAG_KEYCODE_ENDCALL:I = 0x100

.field public static final FLAG_KEYCODE_MEDIA_FAST_FORWARD:I = 0x2000

.field public static final FLAG_KEYCODE_MEDIA_NEXT:I = 0x8

.field public static final FLAG_KEYCODE_MEDIA_PLAY_PAUSE:I = 0x1

.field public static final FLAG_KEYCODE_MEDIA_PREVIOUS:I = 0x10

.field public static final FLAG_KEYCODE_MEDIA_REWIND:I = 0x4000

.field public static final FLAG_KEYCODE_PAGE_DOWN:I = 0x20

.field public static final FLAG_KEYCODE_PAGE_UP:I = 0x40

.field public static final FLAG_KEYCODE_SPACE_MEDIA_PLAY_PAUSE:I = 0x8000

.field public static final FLAG_KEYCODE_VOLUME_DOWN:I = 0x400

.field public static final FLAG_KEYCODE_VOLUME_UP:I = 0x200


# instance fields
.field private mActivityName:Ljava/lang/String;

.field private mCategory:I

.field private mEnable:Z

.field private mLandScapeFeature:I

.field private mMaxVersionCode:I

.field private mMinVersionCode:I

.field private mPackageName:Ljava/lang/String;

.field private mPortraitFeature:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;IIIII)V
    .locals 0
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "activityName"    # Ljava/lang/String;
    .param p3, "category"    # I
    .param p4, "feature"    # I
    .param p5, "featureVertical"    # I
    .param p6, "minVersionCode"    # I
    .param p7, "mMaxVersionCode"    # I

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/android/server/tof/TofGestureComponent;->mPackageName:Ljava/lang/String;

    .line 51
    iput-object p2, p0, Lcom/android/server/tof/TofGestureComponent;->mActivityName:Ljava/lang/String;

    .line 52
    iput p3, p0, Lcom/android/server/tof/TofGestureComponent;->mCategory:I

    .line 53
    iput p4, p0, Lcom/android/server/tof/TofGestureComponent;->mLandScapeFeature:I

    .line 54
    iput p5, p0, Lcom/android/server/tof/TofGestureComponent;->mPortraitFeature:I

    .line 55
    iput p6, p0, Lcom/android/server/tof/TofGestureComponent;->mMinVersionCode:I

    .line 56
    iput p7, p0, Lcom/android/server/tof/TofGestureComponent;->mMaxVersionCode:I

    .line 57
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;IIIIIZ)V
    .locals 0
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "activityName"    # Ljava/lang/String;
    .param p3, "category"    # I
    .param p4, "feature"    # I
    .param p5, "featureVertical"    # I
    .param p6, "minVersionCode"    # I
    .param p7, "maxVersionCode"    # I
    .param p8, "enable"    # Z

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, Lcom/android/server/tof/TofGestureComponent;->mPackageName:Ljava/lang/String;

    .line 63
    iput-object p2, p0, Lcom/android/server/tof/TofGestureComponent;->mActivityName:Ljava/lang/String;

    .line 64
    iput p3, p0, Lcom/android/server/tof/TofGestureComponent;->mCategory:I

    .line 65
    iput p4, p0, Lcom/android/server/tof/TofGestureComponent;->mLandScapeFeature:I

    .line 66
    iput p5, p0, Lcom/android/server/tof/TofGestureComponent;->mPortraitFeature:I

    .line 67
    iput p6, p0, Lcom/android/server/tof/TofGestureComponent;->mMinVersionCode:I

    .line 68
    iput p7, p0, Lcom/android/server/tof/TofGestureComponent;->mMaxVersionCode:I

    .line 69
    iput-boolean p8, p0, Lcom/android/server/tof/TofGestureComponent;->mEnable:Z

    .line 70
    return-void
.end method


# virtual methods
.method public getActivityName()Ljava/lang/String;
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/android/server/tof/TofGestureComponent;->mActivityName:Ljava/lang/String;

    return-object v0
.end method

.method public getCategory()I
    .locals 1

    .line 103
    iget v0, p0, Lcom/android/server/tof/TofGestureComponent;->mCategory:I

    return v0
.end method

.method public getLandscapeFeature()I
    .locals 1

    .line 111
    iget v0, p0, Lcom/android/server/tof/TofGestureComponent;->mLandScapeFeature:I

    return v0
.end method

.method public getMaxVersionCode()I
    .locals 1

    .line 135
    iget v0, p0, Lcom/android/server/tof/TofGestureComponent;->mMaxVersionCode:I

    return v0
.end method

.method public getMinVersionCode()I
    .locals 1

    .line 127
    iget v0, p0, Lcom/android/server/tof/TofGestureComponent;->mMinVersionCode:I

    return v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .line 87
    iget-object v0, p0, Lcom/android/server/tof/TofGestureComponent;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public getPortraitFeature()I
    .locals 1

    .line 119
    iget v0, p0, Lcom/android/server/tof/TofGestureComponent;->mPortraitFeature:I

    return v0
.end method

.method public isEnable()Z
    .locals 1

    .line 143
    iget-boolean v0, p0, Lcom/android/server/tof/TofGestureComponent;->mEnable:Z

    return v0
.end method

.method public isVersionSupported(I)Z
    .locals 6
    .param p1, "versionCode"    # I

    .line 151
    iget v0, p0, Lcom/android/server/tof/TofGestureComponent;->mMinVersionCode:I

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-lez v0, :cond_0

    move v3, v1

    goto :goto_0

    :cond_0
    move v3, v2

    .line 152
    .local v3, "isMinVersionValid":Z
    :goto_0
    iget v4, p0, Lcom/android/server/tof/TofGestureComponent;->mMaxVersionCode:I

    if-lez v4, :cond_1

    move v5, v1

    goto :goto_1

    :cond_1
    move v5, v2

    .line 154
    .local v5, "isMaxVersionValid":Z
    :goto_1
    if-eqz v3, :cond_3

    if-nez v5, :cond_3

    .line 155
    if-lt p1, v0, :cond_2

    goto :goto_2

    :cond_2
    move v1, v2

    :goto_2
    return v1

    .line 159
    :cond_3
    if-eqz v3, :cond_5

    if-eqz v5, :cond_5

    .line 160
    if-lt p1, v0, :cond_4

    if-gt p1, v4, :cond_4

    goto :goto_3

    :cond_4
    move v1, v2

    :goto_3
    return v1

    .line 164
    :cond_5
    if-eqz v5, :cond_7

    if-nez v3, :cond_7

    .line 165
    if-gt p1, v4, :cond_6

    goto :goto_4

    :cond_6
    move v1, v2

    :goto_4
    return v1

    .line 169
    :cond_7
    if-nez v3, :cond_8

    if-nez v5, :cond_8

    .line 170
    return v1

    .line 172
    :cond_8
    return v2
.end method

.method public setActivityName(Ljava/lang/String;)V
    .locals 0
    .param p1, "mActivityName"    # Ljava/lang/String;

    .line 99
    iput-object p1, p0, Lcom/android/server/tof/TofGestureComponent;->mActivityName:Ljava/lang/String;

    .line 100
    return-void
.end method

.method public setCategory(I)V
    .locals 0
    .param p1, "mCategory"    # I

    .line 107
    iput p1, p0, Lcom/android/server/tof/TofGestureComponent;->mCategory:I

    .line 108
    return-void
.end method

.method public setEnable(Z)V
    .locals 0
    .param p1, "mEnable"    # Z

    .line 147
    iput-boolean p1, p0, Lcom/android/server/tof/TofGestureComponent;->mEnable:Z

    .line 148
    return-void
.end method

.method public setLandScapeFeature(I)V
    .locals 0
    .param p1, "mFeature"    # I

    .line 115
    iput p1, p0, Lcom/android/server/tof/TofGestureComponent;->mLandScapeFeature:I

    .line 116
    return-void
.end method

.method public setMaxVersionCode(I)V
    .locals 0
    .param p1, "maxVersionCode"    # I

    .line 139
    iput p1, p0, Lcom/android/server/tof/TofGestureComponent;->mMaxVersionCode:I

    .line 140
    return-void
.end method

.method public setMinVersionCode(I)V
    .locals 0
    .param p1, "minVersionCode"    # I

    .line 131
    iput p1, p0, Lcom/android/server/tof/TofGestureComponent;->mMinVersionCode:I

    .line 132
    return-void
.end method

.method public setPackageName(Ljava/lang/String;)V
    .locals 0
    .param p1, "mPackageName"    # Ljava/lang/String;

    .line 91
    iput-object p1, p0, Lcom/android/server/tof/TofGestureComponent;->mPackageName:Ljava/lang/String;

    .line 92
    return-void
.end method

.method public setPortraitFeature(I)V
    .locals 0
    .param p1, "mFeatureVertical"    # I

    .line 123
    iput p1, p0, Lcom/android/server/tof/TofGestureComponent;->mPortraitFeature:I

    .line 124
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 74
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TofGestureComponent{mPackageName=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/tof/TofGestureComponent;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", mActivityName=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/tof/TofGestureComponent;->mActivityName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mCategory="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/tof/TofGestureComponent;->mCategory:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mLandScapeFeature="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/tof/TofGestureComponent;->mLandScapeFeature:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mPortraitFeature="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/tof/TofGestureComponent;->mPortraitFeature:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mMinVersionCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/tof/TofGestureComponent;->mMinVersionCode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mMaxVersionCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/tof/TofGestureComponent;->mMaxVersionCode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mEnable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/tof/TofGestureComponent;->mEnable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
