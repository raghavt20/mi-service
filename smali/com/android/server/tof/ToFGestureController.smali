.class Lcom/android/server/tof/ToFGestureController;
.super Lcom/android/server/tof/ContactlessGestureController;
.source "ToFGestureController.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "TofGestureController"

.field private static final TOF_CLIENT_ACTION:Ljava/lang/String; = "miui.intent.action.TOF_SERVICE"


# instance fields
.field private mService:Lmiui/tof/ITofClientService;


# direct methods
.method public static synthetic $r8$lambda$Et94CJNRsFJ-qKgC60Z8b6tDUU0(Lcom/android/server/tof/ToFGestureController;Landroid/os/IBinder;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/tof/ToFGestureController;->lambda$onGestureServiceConnected$0(Landroid/os/IBinder;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/tof/ContactlessGestureService;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "contactlessGestureService"    # Lcom/android/server/tof/ContactlessGestureService;

    .line 25
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/tof/ContactlessGestureController;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/tof/ContactlessGestureService;)V

    .line 26
    return-void
.end method

.method private synthetic lambda$onGestureServiceConnected$0(Landroid/os/IBinder;)V
    .locals 2
    .param p1, "service"    # Landroid/os/IBinder;

    .line 98
    const-string v0, "TofGestureController"

    const-string v1, "onServiceConnected"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    iget-object v0, p0, Lcom/android/server/tof/ToFGestureController;->mService:Lmiui/tof/ITofClientService;

    if-nez v0, :cond_0

    .line 100
    invoke-static {p1}, Lmiui/tof/ITofClientService$Stub;->asInterface(Landroid/os/IBinder;)Lmiui/tof/ITofClientService;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tof/ToFGestureController;->mService:Lmiui/tof/ITofClientService;

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/android/server/tof/ToFGestureController;->mServiceBindingLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 103
    return-void
.end method


# virtual methods
.method public getAction()Ljava/lang/String;
    .locals 1

    .line 39
    const-string v0, "miui.intent.action.TOF_SERVICE"

    return-object v0
.end method

.method public getTofClientComponent()Landroid/content/ComponentName;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/android/server/tof/ToFGestureController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x110f00aa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 31
    .local v0, "value":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 32
    invoke-static {v0}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v1

    return-object v1

    .line 34
    :cond_0
    const/4 v1, 0x0

    return-object v1
.end method

.method public isServiceInit()Z
    .locals 1

    .line 86
    iget-object v0, p0, Lcom/android/server/tof/ToFGestureController;->mService:Lmiui/tof/ITofClientService;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public notifyRotationChanged(I)V
    .locals 0
    .param p1, "rotation"    # I

    .line 61
    return-void
.end method

.method public onGestureServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .line 97
    iget-object v0, p0, Lcom/android/server/tof/ToFGestureController;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/tof/ToFGestureController$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p2}, Lcom/android/server/tof/ToFGestureController$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/tof/ToFGestureController;Landroid/os/IBinder;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 104
    return-void
.end method

.method public registerGestureListenerIfNeeded(Z)V
    .locals 0
    .param p1, "register"    # Z

    .line 56
    return-void
.end method

.method public restContactlessGestureService()V
    .locals 2

    .line 91
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/tof/ToFGestureController;->mService:Lmiui/tof/ITofClientService;

    .line 92
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/android/server/tof/ToFGestureController;->mServiceBindingLatch:Ljava/util/concurrent/CountDownLatch;

    .line 93
    return-void
.end method

.method public showGestureNotification()V
    .locals 3

    .line 44
    const-string v0, "TofGestureController"

    iget-object v1, p0, Lcom/android/server/tof/ToFGestureController;->mService:Lmiui/tof/ITofClientService;

    if-eqz v1, :cond_0

    .line 46
    :try_start_0
    invoke-interface {v1}, Lmiui/tof/ITofClientService;->showTofGestureNotification()V

    .line 47
    const-string/jumbo v1, "show Tof gesture notification"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 50
    goto :goto_0

    .line 48
    :catch_0
    move-exception v1

    .line 49
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_0
    :goto_0
    return-void
.end method

.method public updateGestureHint(I)Z
    .locals 5
    .param p1, "label"    # I

    .line 70
    const-string v0, "TofGestureController"

    iget-object v1, p0, Lcom/android/server/tof/ToFGestureController;->mService:Lmiui/tof/ITofClientService;

    if-eqz v1, :cond_1

    .line 72
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/android/server/tof/ToFGestureController;->getFeatureFromLabel(I)I

    move-result v2

    .line 73
    invoke-virtual {p0}, Lcom/android/server/tof/ToFGestureController;->getCurrentSupportFeature()I

    move-result v3

    invoke-virtual {p0}, Lcom/android/server/tof/ToFGestureController;->getCurrentPkg()Ljava/lang/String;

    move-result-object v4

    .line 72
    invoke-interface {v1, v2, v3, v4}, Lmiui/tof/ITofClientService;->showGestureHint(IILjava/lang/String;)V

    .line 74
    sget-boolean v1, Lcom/android/server/tof/ContactlessGestureService;->mDebug:Z

    if-eqz v1, :cond_0

    .line 75
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TMS update view label:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0, p1}, Lcom/android/server/tof/ToFGestureController;->gestureLabelToString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 79
    :cond_0
    goto :goto_0

    .line 77
    :catch_0
    move-exception v1

    .line 78
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0
.end method
