.class Lcom/android/server/tof/ContactlessGestureController$1;
.super Ljava/lang/Object;
.source "ContactlessGestureController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/tof/ContactlessGestureController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/tof/ContactlessGestureController;


# direct methods
.method constructor <init>(Lcom/android/server/tof/ContactlessGestureController;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/tof/ContactlessGestureController;

    .line 792
    iput-object p1, p0, Lcom/android/server/tof/ContactlessGestureController$1;->this$0:Lcom/android/server/tof/ContactlessGestureController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .line 795
    const-string v0, "ContactlessGestureController"

    const/4 v1, 0x0

    .line 796
    .local v1, "splitScreenModeChange":Z
    const/4 v2, 0x0

    .line 797
    .local v2, "topActivity":Landroid/content/ComponentName;
    const/4 v3, 0x1

    .line 799
    .local v3, "topActivityFullScreenOrNotOccluded":Z
    const/4 v4, 0x1

    :try_start_0
    invoke-static {}, Landroid/app/ActivityTaskManager;->getService()Landroid/app/IActivityTaskManager;

    move-result-object v5

    invoke-interface {v5}, Landroid/app/IActivityTaskManager;->isInSplitScreenWindowingMode()Z

    move-result v5

    .line 800
    .local v5, "splitScreenMode":Z
    iget-object v6, p0, Lcom/android/server/tof/ContactlessGestureController$1;->this$0:Lcom/android/server/tof/ContactlessGestureController;

    invoke-static {v6}, Lcom/android/server/tof/ContactlessGestureController;->-$$Nest$fgetmIsSplitScreenMode(Lcom/android/server/tof/ContactlessGestureController;)Z

    move-result v6

    if-eq v5, v6, :cond_0

    .line 801
    iget-object v6, p0, Lcom/android/server/tof/ContactlessGestureController$1;->this$0:Lcom/android/server/tof/ContactlessGestureController;

    invoke-static {v6, v5}, Lcom/android/server/tof/ContactlessGestureController;->-$$Nest$fputmIsSplitScreenMode(Lcom/android/server/tof/ContactlessGestureController;Z)V

    .line 802
    const/4 v1, 0x1

    .line 805
    :cond_0
    iget-object v6, p0, Lcom/android/server/tof/ContactlessGestureController$1;->this$0:Lcom/android/server/tof/ContactlessGestureController;

    invoke-static {v6}, Lcom/android/server/tof/ContactlessGestureController;->-$$Nest$fgetmActivityManager(Lcom/android/server/tof/ContactlessGestureController;)Landroid/app/IActivityManager;

    move-result-object v6

    invoke-interface {v6}, Landroid/app/IActivityManager;->getAllRootTaskInfos()Ljava/util/List;

    move-result-object v6

    .line 806
    .local v6, "infos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityTaskManager$RootTaskInfo;>;"
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_8

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/app/ActivityTaskManager$RootTaskInfo;

    .line 808
    .local v8, "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
    iget v9, v8, Landroid/app/ActivityTaskManager$RootTaskInfo;->displayId:I

    if-nez v9, :cond_1

    iget-boolean v9, v8, Landroid/app/ActivityTaskManager$RootTaskInfo;->visible:Z

    if-eqz v9, :cond_1

    iget-object v9, v8, Landroid/app/ActivityTaskManager$RootTaskInfo;->topActivity:Landroid/content/ComponentName;

    .line 809
    invoke-static {v9}, Ljava/util/Objects;->isNull(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 810
    goto :goto_0

    .line 814
    :cond_2
    invoke-virtual {v8}, Landroid/app/ActivityTaskManager$RootTaskInfo;->getWindowingMode()I

    move-result v9

    .line 815
    .local v9, "windoingMode":I
    iget-object v10, p0, Lcom/android/server/tof/ContactlessGestureController$1;->this$0:Lcom/android/server/tof/ContactlessGestureController;

    invoke-static {v10}, Lcom/android/server/tof/ContactlessGestureController;->-$$Nest$fgetmIsDeskTopMode(Lcom/android/server/tof/ContactlessGestureController;)Z

    move-result v10

    const/4 v11, 0x0

    if-nez v10, :cond_4

    .line 816
    iget-object v7, v8, Landroid/app/ActivityTaskManager$RootTaskInfo;->topActivity:Landroid/content/ComponentName;

    move-object v2, v7

    .line 817
    if-ne v9, v4, :cond_3

    move v11, v4

    :cond_3
    move v3, v11

    .line 818
    goto :goto_1

    .line 821
    :cond_4
    if-ne v9, v4, :cond_5

    .line 822
    iget-object v7, v8, Landroid/app/ActivityTaskManager$RootTaskInfo;->topActivity:Landroid/content/ComponentName;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v2, v7

    .line 823
    goto :goto_1

    .line 825
    :cond_5
    if-eqz v3, :cond_7

    .line 827
    const/4 v10, 0x5

    if-ne v9, v10, :cond_6

    move v11, v4

    :cond_6
    move v3, v11

    .line 830
    .end local v8    # "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
    .end local v9    # "windoingMode":I
    :cond_7
    goto :goto_0

    .line 833
    .end local v5    # "splitScreenMode":Z
    .end local v6    # "infos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityTaskManager$RootTaskInfo;>;"
    :cond_8
    :goto_1
    goto :goto_2

    .line 831
    :catch_0
    move-exception v5

    .line 832
    .local v5, "e":Landroid/os/RemoteException;
    const-string v6, "cannot getTasks"

    invoke-static {v0, v6, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 835
    .end local v5    # "e":Landroid/os/RemoteException;
    :goto_2
    iget-object v5, p0, Lcom/android/server/tof/ContactlessGestureController$1;->this$0:Lcom/android/server/tof/ContactlessGestureController;

    invoke-static {v5}, Lcom/android/server/tof/ContactlessGestureController;->-$$Nest$fgetmTopActivityFullScreenOrNotOccluded(Lcom/android/server/tof/ContactlessGestureController;)Z

    move-result v5

    if-ne v3, v5, :cond_9

    if-nez v1, :cond_9

    if-eqz v2, :cond_b

    iget-object v5, p0, Lcom/android/server/tof/ContactlessGestureController$1;->this$0:Lcom/android/server/tof/ContactlessGestureController;

    invoke-static {v5}, Lcom/android/server/tof/ContactlessGestureController;->-$$Nest$fgetmTopActivity(Lcom/android/server/tof/ContactlessGestureController;)Landroid/content/ComponentName;

    move-result-object v5

    .line 837
    invoke-virtual {v2, v5}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_b

    .line 838
    :cond_9
    sget-boolean v5, Lcom/android/server/tof/ContactlessGestureService;->mDebug:Z

    if-eqz v5, :cond_a

    .line 839
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "top activity status changed, mTopActivityFullScreenOrNotOccluded:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/server/tof/ContactlessGestureController$1;->this$0:Lcom/android/server/tof/ContactlessGestureController;

    invoke-static {v6}, Lcom/android/server/tof/ContactlessGestureController;->-$$Nest$fgetmTopActivityFullScreenOrNotOccluded(Lcom/android/server/tof/ContactlessGestureController;)Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", mIsDeskTopMode:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/server/tof/ContactlessGestureController$1;->this$0:Lcom/android/server/tof/ContactlessGestureController;

    invoke-static {v6}, Lcom/android/server/tof/ContactlessGestureController;->-$$Nest$fgetmIsDeskTopMode(Lcom/android/server/tof/ContactlessGestureController;)Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", mTopActivity:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", mIsSplitScreenMode:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/server/tof/ContactlessGestureController$1;->this$0:Lcom/android/server/tof/ContactlessGestureController;

    invoke-static {v6}, Lcom/android/server/tof/ContactlessGestureController;->-$$Nest$fgetmIsSplitScreenMode(Lcom/android/server/tof/ContactlessGestureController;)Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 845
    :cond_a
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController$1;->this$0:Lcom/android/server/tof/ContactlessGestureController;

    invoke-static {v0, v2}, Lcom/android/server/tof/ContactlessGestureController;->-$$Nest$fputmTopActivity(Lcom/android/server/tof/ContactlessGestureController;Landroid/content/ComponentName;)V

    .line 846
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController$1;->this$0:Lcom/android/server/tof/ContactlessGestureController;

    invoke-static {v0, v3}, Lcom/android/server/tof/ContactlessGestureController;->-$$Nest$fputmTopActivityFullScreenOrNotOccluded(Lcom/android/server/tof/ContactlessGestureController;Z)V

    .line 847
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController$1;->this$0:Lcom/android/server/tof/ContactlessGestureController;

    invoke-virtual {v0, v4}, Lcom/android/server/tof/ContactlessGestureController;->onSceneChanged(I)V

    .line 849
    :cond_b
    return-void
.end method
