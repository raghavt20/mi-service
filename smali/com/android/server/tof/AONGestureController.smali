.class Lcom/android/server/tof/AONGestureController;
.super Lcom/android/server/tof/ContactlessGestureController;
.source "AONGestureController.java"


# static fields
.field private static final ALWAYS_CHECK_GESTURE_TIMEOUT:I = 0x927c0

.field public static final AON_SERVICE_CLASS:Ljava/lang/String; = "com.xiaomi.aon.AONService"

.field public static final AON_SERVICE_PACKAGE:Ljava/lang/String; = "com.xiaomi.aon"

.field private static final CLIENT_ACTION:Ljava/lang/String; = "miui.intent.action.GESTURE_SERVICE"

.field private static final FPS_CHECK_GESTURE:I = 0x6

.field private static final TAG:Ljava/lang/String; = "AONGestureController"

.field private static final TYPE_HAND_ROIACTION:I = 0x102


# instance fields
.field private mAonComponentName:Landroid/content/ComponentName;

.field private mAonService:Lcom/xiaomi/aon/IMiAON;

.field private final mConnection:Landroid/content/ServiceConnection;

.field private final mGestureListener:Lcom/xiaomi/aon/IMiAONListener;

.field private mLastDisplayRotation:I

.field private mLastSupportGesture:I

.field private mListeningState:Z

.field public mService:Lmiui/aon/IContactlessGestureService;


# direct methods
.method public static synthetic $r8$lambda$Hd1xd4kPDn5EA1PWkJ9wetcLdTM(Lcom/android/server/tof/AONGestureController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/tof/AONGestureController;->lambda$notifyAonEventChangeIfNeeded$1()V

    return-void
.end method

.method public static synthetic $r8$lambda$hqs1dt98pyAV_bz6XTty91HW74M(Lcom/android/server/tof/AONGestureController;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/tof/AONGestureController;->lambda$registerGestureListenerIfNeeded$0(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmAonService(Lcom/android/server/tof/AONGestureController;Lcom/xiaomi/aon/IMiAON;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/tof/AONGestureController;->mAonService:Lcom/xiaomi/aon/IMiAON;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmListeningState(Lcom/android/server/tof/AONGestureController;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/tof/AONGestureController;->mListeningState:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mregisterListener(Lcom/android/server/tof/AONGestureController;III)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/tof/AONGestureController;->registerListener(III)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/tof/ContactlessGestureService;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "contactlessGestureService"    # Lcom/android/server/tof/ContactlessGestureService;

    .line 44
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/tof/ContactlessGestureController;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/tof/ContactlessGestureService;)V

    .line 37
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/tof/AONGestureController;->mLastSupportGesture:I

    .line 38
    iput v0, p0, Lcom/android/server/tof/AONGestureController;->mLastDisplayRotation:I

    .line 168
    new-instance v0, Lcom/android/server/tof/AONGestureController$1;

    invoke-direct {v0, p0}, Lcom/android/server/tof/AONGestureController$1;-><init>(Lcom/android/server/tof/AONGestureController;)V

    iput-object v0, p0, Lcom/android/server/tof/AONGestureController;->mGestureListener:Lcom/xiaomi/aon/IMiAONListener;

    .line 228
    new-instance v0, Lcom/android/server/tof/AONGestureController$2;

    invoke-direct {v0, p0}, Lcom/android/server/tof/AONGestureController$2;-><init>(Lcom/android/server/tof/AONGestureController;)V

    iput-object v0, p0, Lcom/android/server/tof/AONGestureController;->mConnection:Landroid/content/ServiceConnection;

    .line 45
    return-void
.end method

.method private bindAonService()V
    .locals 5

    .line 216
    iget-object v0, p0, Lcom/android/server/tof/AONGestureController;->mAonService:Lcom/xiaomi/aon/IMiAON;

    if-eqz v0, :cond_0

    .line 217
    return-void

    .line 219
    :cond_0
    iget-object v0, p0, Lcom/android/server/tof/AONGestureController;->mAonComponentName:Landroid/content/ComponentName;

    const-string v1, "com.xiaomi.aon.AONService"

    if-nez v0, :cond_1

    .line 220
    new-instance v0, Landroid/content/ComponentName;

    const-string v2, "com.xiaomi.aon"

    invoke-direct {v0, v2, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/tof/AONGestureController;->mAonComponentName:Landroid/content/ComponentName;

    .line 222
    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/server/tof/AONGestureController;->mAonComponentName:Landroid/content/ComponentName;

    .line 223
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 224
    .local v0, "serviceIntent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/android/server/tof/AONGestureController;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/tof/AONGestureController;->mConnection:Landroid/content/ServiceConnection;

    const v3, 0x4000001

    sget-object v4, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/content/Context;->bindServiceAsUser(Landroid/content/Intent;Landroid/content/ServiceConnection;ILandroid/os/UserHandle;)Z

    .line 226
    return-void
.end method

.method private synthetic lambda$notifyAonEventChangeIfNeeded$1()V
    .locals 5

    .line 105
    const-string v0, "AONGestureController"

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tof/AONGestureController;->getCurrentSupportGesture()I

    move-result v1

    .line 107
    .local v1, "currentSupportGesture":I
    iget v2, p0, Lcom/android/server/tof/AONGestureController;->mLastDisplayRotation:I

    iget v3, p0, Lcom/android/server/tof/AONGestureController;->mDisplayRotation:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/android/server/tof/AONGestureController;->mLastSupportGesture:I

    if-ne v2, v1, :cond_0

    .line 109
    return-void

    .line 111
    :cond_0
    iget-object v2, p0, Lcom/android/server/tof/AONGestureController;->mAonService:Lcom/xiaomi/aon/IMiAON;

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/android/server/tof/AONGestureController;->mListeningState:Z

    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    .line 112
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "notify aon event changed,mDisplayRotation:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/server/tof/AONGestureController;->mDisplayRotation:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " currentSupportGesture:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 114
    invoke-static {v1}, Ljava/lang/Integer;->toBinaryString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 112
    invoke-static {v0, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    iget-object v2, p0, Lcom/android/server/tof/AONGestureController;->mAonService:Lcom/xiaomi/aon/IMiAON;

    iget v3, p0, Lcom/android/server/tof/AONGestureController;->mDisplayRotation:I

    add-int/lit8 v3, v3, 0x10

    const/4 v4, 0x1

    shl-int v3, v4, v3

    const/16 v4, 0x102

    invoke-interface {v2, v4, v3, v1}, Lcom/xiaomi/aon/IMiAON;->aonEventUpdate(III)V

    .line 117
    iput v1, p0, Lcom/android/server/tof/AONGestureController;->mLastSupportGesture:I

    .line 118
    iget v2, p0, Lcom/android/server/tof/AONGestureController;->mDisplayRotation:I

    iput v2, p0, Lcom/android/server/tof/AONGestureController;->mLastDisplayRotation:I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 122
    .end local v1    # "currentSupportGesture":I
    :cond_1
    goto :goto_0

    .line 120
    :catch_0
    move-exception v1

    .line 121
    .local v1, "e":Landroid/os/RemoteException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "notifyRotationChanged: error "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    .end local v1    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method

.method private synthetic lambda$registerGestureListenerIfNeeded$0(Z)V
    .locals 3
    .param p1, "register"    # Z

    .line 76
    if-eqz p1, :cond_0

    .line 77
    const/4 v0, 0x6

    const v1, 0x927c0

    const/16 v2, 0x102

    invoke-direct {p0, v2, v0, v1}, Lcom/android/server/tof/AONGestureController;->registerListener(III)V

    goto :goto_0

    .line 79
    :cond_0
    invoke-direct {p0}, Lcom/android/server/tof/AONGestureController;->unRegisterListener()V

    .line 81
    :goto_0
    return-void
.end method

.method private notifyAonEventChangeIfNeeded()V
    .locals 2

    .line 103
    iget-object v0, p0, Lcom/android/server/tof/AONGestureController;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/tof/AONGestureController$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0}, Lcom/android/server/tof/AONGestureController$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/tof/AONGestureController;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 124
    return-void
.end method

.method private registerListener(III)V
    .locals 4
    .param p1, "type"    # I
    .param p2, "fps"    # I
    .param p3, "timeout"    # I

    .line 186
    invoke-direct {p0}, Lcom/android/server/tof/AONGestureController;->bindAonService()V

    .line 187
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "register listener, aonService is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/tof/AONGestureController;->mAonService:Lcom/xiaomi/aon/IMiAON;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mListeningState:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/tof/AONGestureController;->mListeningState:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mDisplayRotation:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/tof/AONGestureController;->mDisplayRotation:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AONGestureController"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    :try_start_0
    iget-object v0, p0, Lcom/android/server/tof/AONGestureController;->mAonService:Lcom/xiaomi/aon/IMiAON;

    if-eqz v0, :cond_0

    iget-boolean v2, p0, Lcom/android/server/tof/AONGestureController;->mListeningState:Z

    if-nez v2, :cond_0

    .line 191
    int-to-float v2, p2

    iget-object v3, p0, Lcom/android/server/tof/AONGestureController;->mGestureListener:Lcom/xiaomi/aon/IMiAONListener;

    invoke-interface {v0, p1, v2, p3, v3}, Lcom/xiaomi/aon/IMiAON;->registerListener(IFILcom/xiaomi/aon/IMiAONListener;)I

    .line 192
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/tof/AONGestureController;->mListeningState:Z

    .line 194
    :cond_0
    invoke-direct {p0}, Lcom/android/server/tof/AONGestureController;->notifyAonEventChangeIfNeeded()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 197
    goto :goto_0

    .line 195
    :catch_0
    move-exception v0

    .line 196
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "registerListener: error "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method

.method private unRegisterListener()V
    .locals 4

    .line 202
    const-string v0, "AONGestureController"

    :try_start_0
    iget-boolean v1, p0, Lcom/android/server/tof/AONGestureController;->mListeningState:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/server/tof/AONGestureController;->mAonService:Lcom/xiaomi/aon/IMiAON;

    if-eqz v1, :cond_0

    .line 203
    const-string/jumbo v1, "unregister listener!"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    iget-object v1, p0, Lcom/android/server/tof/AONGestureController;->mAonService:Lcom/xiaomi/aon/IMiAON;

    iget-object v2, p0, Lcom/android/server/tof/AONGestureController;->mGestureListener:Lcom/xiaomi/aon/IMiAONListener;

    const/16 v3, 0x102

    invoke-interface {v1, v3, v2}, Lcom/xiaomi/aon/IMiAON;->unregisterListener(ILcom/xiaomi/aon/IMiAONListener;)I

    .line 205
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/server/tof/AONGestureController;->mListeningState:Z

    .line 206
    const/4 v2, -0x1

    iput v2, p0, Lcom/android/server/tof/AONGestureController;->mLastSupportGesture:I

    .line 207
    iput v2, p0, Lcom/android/server/tof/AONGestureController;->mLastDisplayRotation:I

    .line 208
    iput-boolean v1, p0, Lcom/android/server/tof/AONGestureController;->mIsTrigger:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 212
    :cond_0
    goto :goto_0

    .line 210
    :catch_0
    move-exception v1

    .line 211
    .local v1, "e":Landroid/os/RemoteException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "unRegisterListener: error "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    .end local v1    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method


# virtual methods
.method public getAction()Ljava/lang/String;
    .locals 1

    .line 58
    const-string v0, "miui.intent.action.GESTURE_SERVICE"

    return-object v0
.end method

.method public getTofClientComponent()Landroid/content/ComponentName;
    .locals 2

    .line 49
    iget-object v0, p0, Lcom/android/server/tof/AONGestureController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x110f00a1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 50
    .local v0, "value":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 51
    invoke-static {v0}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v1

    return-object v1

    .line 53
    :cond_0
    const/4 v1, 0x0

    return-object v1
.end method

.method public isServiceInit()Z
    .locals 1

    .line 150
    iget-object v0, p0, Lcom/android/server/tof/AONGestureController;->mService:Lmiui/aon/IContactlessGestureService;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public notifyRotationChanged(I)V
    .locals 0
    .param p1, "rotation"    # I

    .line 97
    return-void
.end method

.method public onGestureServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .line 161
    const-string v0, "AONGestureController"

    const-string v1, "onServiceConnected"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    iget-object v0, p0, Lcom/android/server/tof/AONGestureController;->mService:Lmiui/aon/IContactlessGestureService;

    if-nez v0, :cond_0

    .line 163
    invoke-static {p2}, Lmiui/aon/IContactlessGestureService$Stub;->asInterface(Landroid/os/IBinder;)Lmiui/aon/IContactlessGestureService;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tof/AONGestureController;->mService:Lmiui/aon/IContactlessGestureService;

    .line 165
    :cond_0
    iget-object v0, p0, Lcom/android/server/tof/AONGestureController;->mServiceBindingLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 166
    return-void
.end method

.method public registerGestureListenerIfNeeded(Z)V
    .locals 2
    .param p1, "register"    # Z

    .line 75
    iget-object v0, p0, Lcom/android/server/tof/AONGestureController;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/tof/AONGestureController$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p1}, Lcom/android/server/tof/AONGestureController$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/tof/AONGestureController;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 82
    return-void
.end method

.method public restContactlessGestureService()V
    .locals 2

    .line 155
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/tof/AONGestureController;->mService:Lmiui/aon/IContactlessGestureService;

    .line 156
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/android/server/tof/AONGestureController;->mServiceBindingLatch:Ljava/util/concurrent/CountDownLatch;

    .line 157
    return-void
.end method

.method public showGestureNotification()V
    .locals 3

    .line 63
    const-string v0, "AONGestureController"

    iget-object v1, p0, Lcom/android/server/tof/AONGestureController;->mService:Lmiui/aon/IContactlessGestureService;

    if-eqz v1, :cond_0

    .line 65
    :try_start_0
    invoke-interface {v1}, Lmiui/aon/IContactlessGestureService;->showContactlessGestureNotification()V

    .line 66
    const-string/jumbo v1, "show aon gesture notification, if needed"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 69
    goto :goto_0

    .line 67
    :catch_0
    move-exception v1

    .line 68
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_0
    :goto_0
    return-void
.end method

.method public updateGestureHint(I)Z
    .locals 8
    .param p1, "label"    # I

    .line 133
    const-string v0, "AONGestureController"

    const/4 v1, 0x0

    .line 134
    .local v1, "success":Z
    iget-object v2, p0, Lcom/android/server/tof/AONGestureController;->mService:Lmiui/aon/IContactlessGestureService;

    if-eqz v2, :cond_1

    .line 136
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/android/server/tof/AONGestureController;->getFeatureFromLabel(I)I

    move-result v3

    .line 137
    invoke-virtual {p0}, Lcom/android/server/tof/AONGestureController;->getCurrentSupportFeature()I

    move-result v4

    invoke-virtual {p0}, Lcom/android/server/tof/AONGestureController;->getCurrentAppType()I

    move-result v5

    invoke-virtual {p0}, Lcom/android/server/tof/AONGestureController;->getCurrentPkg()Ljava/lang/String;

    move-result-object v6

    iget v7, p0, Lcom/android/server/tof/AONGestureController;->mDisplayRotation:I

    .line 136
    invoke-interface/range {v2 .. v7}, Lmiui/aon/IContactlessGestureService;->showGestureHint(IIILjava/lang/String;I)Z

    move-result v2

    move v1, v2

    .line 138
    sget-boolean v2, Lcom/android/server/tof/ContactlessGestureService;->mDebug:Z

    if-eqz v2, :cond_0

    .line 139
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AON update view label:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, p1}, Lcom/android/server/tof/AONGestureController;->gestureLabelToString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 143
    :cond_0
    goto :goto_0

    .line 141
    :catch_0
    move-exception v2

    .line 142
    .local v2, "e":Landroid/os/RemoteException;
    invoke-virtual {v2}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    .end local v2    # "e":Landroid/os/RemoteException;
    :cond_1
    :goto_0
    return v1
.end method
