.class final Lcom/android/server/tof/ContactlessGestureService$TofServiceShellCommand;
.super Landroid/os/ShellCommand;
.source "ContactlessGestureService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/tof/ContactlessGestureService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "TofServiceShellCommand"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/tof/ContactlessGestureService;


# direct methods
.method private constructor <init>(Lcom/android/server/tof/ContactlessGestureService;)V
    .locals 0

    .line 709
    iput-object p1, p0, Lcom/android/server/tof/ContactlessGestureService$TofServiceShellCommand;->this$0:Lcom/android/server/tof/ContactlessGestureService;

    invoke-direct {p0}, Landroid/os/ShellCommand;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/tof/ContactlessGestureService;Lcom/android/server/tof/ContactlessGestureService$TofServiceShellCommand-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/tof/ContactlessGestureService$TofServiceShellCommand;-><init>(Lcom/android/server/tof/ContactlessGestureService;)V

    return-void
.end method

.method private dumpHelp()V
    .locals 2

    .line 759
    invoke-virtual {p0}, Lcom/android/server/tof/ContactlessGestureService$TofServiceShellCommand;->getOutPrintWriter()Ljava/io/PrintWriter;

    move-result-object v0

    .line 760
    .local v0, "pw":Ljava/io/PrintWriter;
    const-string v1, "Contactless gesture commands: "

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 761
    const-string v1, "  --help|-h"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 762
    const-string v1, "    Print this help text."

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 763
    const-string v1, "  logging-enable"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 764
    const-string v1, "    Enable logging."

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 765
    const-string v1, "  logging-disable"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 766
    const-string v1, "    Disable logging."

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 767
    const-string v1, "  tof-start-gesture-app"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 768
    const-string v1, "    Launch the Tof gesture app."

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 769
    const-string v1, "  tof-pro-sensor"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 770
    const-string v1, "    Simulating the proximity sensor state."

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 771
    const-string v1, "  gesture"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 772
    const-string v1, "    Simulating the gesture sensor events."

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 773
    return-void
.end method


# virtual methods
.method public onCommand(Ljava/lang/String;)I
    .locals 6
    .param p1, "cmd"    # Ljava/lang/String;

    .line 712
    if-nez p1, :cond_0

    .line 713
    invoke-virtual {p0, p1}, Lcom/android/server/tof/ContactlessGestureService$TofServiceShellCommand;->handleDefaultCommands(Ljava/lang/String;)I

    move-result v0

    return v0

    .line 715
    :cond_0
    invoke-virtual {p0}, Lcom/android/server/tof/ContactlessGestureService$TofServiceShellCommand;->getErrPrintWriter()Ljava/io/PrintWriter;

    move-result-object v0

    .line 717
    .local v0, "err":Ljava/io/PrintWriter;
    const/4 v1, -0x1

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    sparse-switch v2, :sswitch_data_0

    :cond_1
    goto :goto_0

    :sswitch_0
    const-string v2, "logging-enable"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v4

    goto :goto_1

    :sswitch_1
    const-string v2, "logging-disable"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v3

    goto :goto_1

    :sswitch_2
    const-string/jumbo v2, "tof-start-gesture-app"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    goto :goto_1

    :sswitch_3
    const-string/jumbo v2, "tof-pro-sensor"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x3

    goto :goto_1

    :sswitch_4
    const-string v2, "gesture"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v2, :cond_1

    const/4 v2, 0x4

    goto :goto_1

    :goto_0
    move v2, v1

    :goto_1
    const-string/jumbo v5, "user"

    packed-switch v2, :pswitch_data_0

    .line 744
    :try_start_1
    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService$TofServiceShellCommand;->dumpHelp()V

    goto :goto_2

    .line 736
    :pswitch_0
    sget-boolean v2, Lcom/android/server/tof/ContactlessGestureService;->mDebug:Z

    if-nez v2, :cond_2

    sget-object v2, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 737
    :cond_2
    invoke-virtual {p0}, Lcom/android/server/tof/ContactlessGestureService$TofServiceShellCommand;->getNextArgRequired()Ljava/lang/String;

    move-result-object v2

    .line 738
    .local v2, "label":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 739
    iget-object v3, p0, Lcom/android/server/tof/ContactlessGestureService$TofServiceShellCommand;->this$0:Lcom/android/server/tof/ContactlessGestureService;

    invoke-static {v3}, Lcom/android/server/tof/ContactlessGestureService;->-$$Nest$fgetmContactlessGestureController(Lcom/android/server/tof/ContactlessGestureService;)Lcom/android/server/tof/ContactlessGestureController;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/android/server/tof/ContactlessGestureController;->handleGestureEvent(I)V

    .line 742
    .end local v2    # "label":Ljava/lang/String;
    :cond_3
    return v4

    .line 732
    :pswitch_1
    invoke-virtual {p0}, Lcom/android/server/tof/ContactlessGestureService$TofServiceShellCommand;->getNextArgRequired()Ljava/lang/String;

    move-result-object v2

    .line 733
    .local v2, "powerOn":Ljava/lang/String;
    iget-object v3, p0, Lcom/android/server/tof/ContactlessGestureService$TofServiceShellCommand;->this$0:Lcom/android/server/tof/ContactlessGestureService;

    const-string/jumbo v5, "true"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    invoke-static {v3, v5}, Lcom/android/server/tof/ContactlessGestureService;->-$$Nest$mhandleTofProximitySensorChanged(Lcom/android/server/tof/ContactlessGestureService;Z)V

    .line 734
    return v4

    .line 723
    .end local v2    # "powerOn":Ljava/lang/String;
    :pswitch_2
    sget-object v2, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 724
    invoke-virtual {p0}, Lcom/android/server/tof/ContactlessGestureService$TofServiceShellCommand;->getNextArgRequired()Ljava/lang/String;

    move-result-object v2

    .line 725
    .local v2, "component":Ljava/lang/String;
    if-eqz v2, :cond_4

    .line 726
    iget-object v3, p0, Lcom/android/server/tof/ContactlessGestureService$TofServiceShellCommand;->this$0:Lcom/android/server/tof/ContactlessGestureService;

    invoke-static {v3}, Lcom/android/server/tof/ContactlessGestureService;->-$$Nest$fgetmContactlessGestureController(Lcom/android/server/tof/ContactlessGestureService;)Lcom/android/server/tof/ContactlessGestureController;

    move-result-object v3

    .line 727
    invoke-static {v2}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v5

    .line 726
    invoke-virtual {v3, v5}, Lcom/android/server/tof/ContactlessGestureController;->bindService(Landroid/content/ComponentName;)V

    .line 730
    .end local v2    # "component":Ljava/lang/String;
    :cond_4
    return v4

    .line 721
    :pswitch_3
    iget-object v2, p0, Lcom/android/server/tof/ContactlessGestureService$TofServiceShellCommand;->this$0:Lcom/android/server/tof/ContactlessGestureService;

    invoke-static {v2, v4}, Lcom/android/server/tof/ContactlessGestureService;->-$$Nest$msetDebug(Lcom/android/server/tof/ContactlessGestureService;Z)I

    move-result v1

    return v1

    .line 719
    :pswitch_4
    iget-object v2, p0, Lcom/android/server/tof/ContactlessGestureService$TofServiceShellCommand;->this$0:Lcom/android/server/tof/ContactlessGestureService;

    invoke-static {v2, v3}, Lcom/android/server/tof/ContactlessGestureService;->-$$Nest$msetDebug(Lcom/android/server/tof/ContactlessGestureService;Z)I

    move-result v1
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    return v1

    .line 745
    :goto_2
    return v4

    .line 747
    :catch_0
    move-exception v2

    .line 748
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 750
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    return v1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x479a2b7 -> :sswitch_4
        0x8009c9c -> :sswitch_3
        0x1014b6b0 -> :sswitch_2
        0x3e04027a -> :sswitch_1
        0x670c0bb1 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onHelp()V
    .locals 0

    .line 755
    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService$TofServiceShellCommand;->dumpHelp()V

    .line 756
    return-void
.end method
