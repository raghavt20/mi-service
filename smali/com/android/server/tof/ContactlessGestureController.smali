.class public abstract Lcom/android/server/tof/ContactlessGestureController;
.super Ljava/lang/Object;
.source "ContactlessGestureController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/tof/ContactlessGestureController$TofClientConnecton;,
        Lcom/android/server/tof/ContactlessGestureController$AccessibilityTelephonyCallback;,
        Lcom/android/server/tof/ContactlessGestureController$ScreenStateReceiver;
    }
.end annotation


# static fields
.field public static final AON_GESTURE_DOWN_STATE:I = 0x19

.field public static final AON_GESTURE_LEFT_STATE:I = 0x1a

.field public static final AON_GESTURE_RIGHT_STATE:I = 0x1b

.field public static final AON_GESTURE_TRIGGER_DOWN:I = 0x15

.field public static final AON_GESTURE_TRIGGER_LEFT:I = 0x16

.field public static final AON_GESTURE_TRIGGER_RIGHT:I = 0x17

.field public static final AON_GESTURE_TRIGGER_UP:I = 0x14

.field public static final AON_GESTURE_UP_STATE:I = 0x18

.field private static final CHECK_TOP_ACTIVITY_DELAY:I = 0x384

.field private static final DELAY:I = 0x1f4

.field private static final FLAG_GESTURE_DOWN_STATE_FEATURE:I = -0xa

.field private static final FLAG_GESTURE_INVALIDATE_SCENARIO_FEATURE:I = -0x3

.field private static final FLAG_GESTURE_LEFT_STATE_FEATURE:I = -0xc

.field private static final FLAG_GESTURE_RIGHT_STATE_FEATURE:I = -0xb

.field private static final FLAG_GESTURE_TRIGGER_DOWN_FEATURE:I = -0x6

.field private static final FLAG_GESTURE_TRIGGER_LEFT_FEATURE:I = -0x8

.field private static final FLAG_GESTURE_TRIGGER_RIGHT_FEATURE:I = -0x7

.field private static final FLAG_GESTURE_TRIGGER_UP_FEATURE:I = -0x5

.field private static final FLAG_GESTURE_UN_TRIGGER_FEATURE:I = -0x2

.field private static final FLAG_GESTURE_UP_STATE_FEATURE:I = -0x9

.field private static final FLAG_TOF_GESTURE_TRIGGER_FEATURE:I = -0x1

.field private static final FLAG_TOF_GESTURE_WORKING_FEATURE:I = -0x4

.field public static final GESTURE_GESTURE_WORKING:I = 0xe

.field public static final GESTURE_INVALIDATE_SCENARIO:I = 0xd

.field private static final SCENE_ACTIVITY_FOCUS_CHANGE:I = 0x2

.field private static final SCENE_CALL_STATE_CHANGE:I = 0x4

.field private static final SCENE_DISPLAY_ROTATION_CHANGE:I = 0x3

.field private static final SCENE_DISPLAY_STATE_CHANGE:I = 0x5

.field public static final SCENE_OTHER_CHANGE:I = 0x6

.field private static final SCENE_TOP_ACTIVITY_CHANGE:I = 0x1

.field private static final SERVICE_BIND_AWAIT_MILLIS:J = 0x3e8L

.field private static final TAG:Ljava/lang/String; = "ContactlessGestureController"

.field public static final TOF_GESTURE_DOUBLE_PRESS:I = 0x7

.field public static final TOF_GESTURE_DOWN:I = 0x3

.field public static final TOF_GESTURE_DRAW_CIRCLE:I = 0x8

.field public static final TOF_GESTURE_LEFT:I = 0x1

.field public static final TOF_GESTURE_RIGHT:I = 0x2

.field public static final TOF_GESTURE_TRIGGER:I = 0xa

.field public static final TOF_GESTURE_UN_TRIGGER:I = 0xc

.field public static final TOF_GESTURE_UP:I = 0x4


# instance fields
.field private mActivityManager:Landroid/app/IActivityManager;

.field mActivityStateObserver:Landroid/app/IMiuiActivityObserver;

.field private mAppFeatureHelper:Lcom/android/server/tof/AppFeatureHelper;

.field protected mComponentName:Landroid/content/ComponentName;

.field private mContactlessGestureService:Lcom/android/server/tof/ContactlessGestureService;

.field protected mContext:Landroid/content/Context;

.field private mDisplayManager:Landroid/hardware/display/DisplayManager;

.field protected mDisplayRotation:I

.field private mFocusedPackage:Ljava/lang/String;

.field protected mHandler:Landroid/os/Handler;

.field private mInputHelper:Lcom/android/server/tof/InputHelper;

.field private mInteractive:Z

.field private mIsDeskTopMode:Z

.field private mIsSplitScreenMode:Z

.field protected mIsTrigger:Z

.field private mLastTriggerLabel:I

.field private mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

.field private mPowerManagerServiceImpl:Lcom/android/server/power/PowerManagerServiceStub;

.field private mRegisterGesture:Z

.field private mRegisterGestureRunnable:Ljava/lang/Runnable;

.field private mScreenReceiver:Landroid/content/BroadcastReceiver;

.field protected mServiceBindingLatch:Ljava/util/concurrent/CountDownLatch;

.field private volatile mServiceIsBinding:Z

.field mShouldUpdateTriggerView:Z

.field private final mTelephonyCallback:Landroid/telephony/TelephonyCallback;

.field private mTelephonyManager:Landroid/telephony/TelephonyManager;

.field private mTofClientConnection:Lcom/android/server/tof/ContactlessGestureController$TofClientConnecton;

.field private mTofGestureDoublePressSupportFeature:I

.field private mTofGestureDownSupportFeature:I

.field private mTofGestureDrawCircleSupportFeature:I

.field private mTofGestureLeftSupportFeature:I

.field private mTofGestureRightSupportFeature:I

.field private mTofGestureUpSupportFeature:I

.field private mTopActivity:Landroid/content/ComponentName;

.field private mTopActivityFullScreenOrNotOccluded:Z

.field private mTriggerUiSuccess:Z

.field private mUpdateTopActivityIfNeededRunnable:Ljava/lang/Runnable;

.field private mWindowManagerService:Lcom/android/server/wm/WindowManagerService;


# direct methods
.method public static synthetic $r8$lambda$-gNbufUTUtH5sJHi_HrsvQHoPME(Lcom/android/server/tof/ContactlessGestureController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureController;->lambda$handleGestureEvent$4()V

    return-void
.end method

.method public static synthetic $r8$lambda$Qirqa5DXyu7XBB3wf9nUln8QeZE(Lcom/android/server/tof/ContactlessGestureController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureController;->lambda$updateFocusedPackage$2()V

    return-void
.end method

.method public static synthetic $r8$lambda$QkWLFcRB17zPNO-yrb_ugDE4u2E(Lcom/android/server/tof/ContactlessGestureController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureController;->lambda$updateFocusedPackage$1()V

    return-void
.end method

.method public static synthetic $r8$lambda$oYK4Q2xpEv2AxTXWRspQRdw2oPE(Lcom/android/server/tof/ContactlessGestureController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureController;->lambda$new$3()V

    return-void
.end method

.method public static synthetic $r8$lambda$teGRhQEnVNiv4RR3mLliuYjsoP0(Lcom/android/server/tof/ContactlessGestureController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureController;->lambda$unBindService$0()V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmActivityManager(Lcom/android/server/tof/ContactlessGestureController;)Landroid/app/IActivityManager;
    .locals 0

    iget-object p0, p0, Lcom/android/server/tof/ContactlessGestureController;->mActivityManager:Landroid/app/IActivityManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsDeskTopMode(Lcom/android/server/tof/ContactlessGestureController;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/tof/ContactlessGestureController;->mIsDeskTopMode:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsSplitScreenMode(Lcom/android/server/tof/ContactlessGestureController;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/tof/ContactlessGestureController;->mIsSplitScreenMode:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmTofClientConnection(Lcom/android/server/tof/ContactlessGestureController;)Lcom/android/server/tof/ContactlessGestureController$TofClientConnecton;
    .locals 0

    iget-object p0, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofClientConnection:Lcom/android/server/tof/ContactlessGestureController$TofClientConnecton;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmTopActivity(Lcom/android/server/tof/ContactlessGestureController;)Landroid/content/ComponentName;
    .locals 0

    iget-object p0, p0, Lcom/android/server/tof/ContactlessGestureController;->mTopActivity:Landroid/content/ComponentName;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmTopActivityFullScreenOrNotOccluded(Lcom/android/server/tof/ContactlessGestureController;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/tof/ContactlessGestureController;->mTopActivityFullScreenOrNotOccluded:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmInteractive(Lcom/android/server/tof/ContactlessGestureController;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/tof/ContactlessGestureController;->mInteractive:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsSplitScreenMode(Lcom/android/server/tof/ContactlessGestureController;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/tof/ContactlessGestureController;->mIsSplitScreenMode:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmServiceIsBinding(Lcom/android/server/tof/ContactlessGestureController;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/tof/ContactlessGestureController;->mServiceIsBinding:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmTopActivity(Lcom/android/server/tof/ContactlessGestureController;Landroid/content/ComponentName;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/tof/ContactlessGestureController;->mTopActivity:Landroid/content/ComponentName;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmTopActivityFullScreenOrNotOccluded(Lcom/android/server/tof/ContactlessGestureController;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/tof/ContactlessGestureController;->mTopActivityFullScreenOrNotOccluded:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mcheckTopActivities(Lcom/android/server/tof/ContactlessGestureController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureController;->checkTopActivities()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateDefaultDisplayRotation(Lcom/android/server/tof/ContactlessGestureController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureController;->updateDefaultDisplayRotation()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/tof/ContactlessGestureService;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "contactlessGestureService"    # Lcom/android/server/tof/ContactlessGestureService;

    .line 277
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 145
    const v0, 0x14014

    iput v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureLeftSupportFeature:I

    .line 150
    const v0, 0x2200a

    iput v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureRightSupportFeature:I

    .line 157
    const/16 v0, 0x1440

    iput v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureDownSupportFeature:I

    .line 163
    const/16 v0, 0xa20

    iput v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureUpSupportFeature:I

    .line 168
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureDoublePressSupportFeature:I

    .line 173
    const/16 v1, 0x180

    iput v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureDrawCircleSupportFeature:I

    .line 257
    iput-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mInteractive:Z

    .line 259
    iput-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mTopActivityFullScreenOrNotOccluded:Z

    .line 475
    new-instance v1, Lcom/android/server/tof/ContactlessGestureController$$ExternalSyntheticLambda3;

    invoke-direct {v1, p0}, Lcom/android/server/tof/ContactlessGestureController$$ExternalSyntheticLambda3;-><init>(Lcom/android/server/tof/ContactlessGestureController;)V

    iput-object v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mRegisterGestureRunnable:Ljava/lang/Runnable;

    .line 792
    new-instance v1, Lcom/android/server/tof/ContactlessGestureController$1;

    invoke-direct {v1, p0}, Lcom/android/server/tof/ContactlessGestureController$1;-><init>(Lcom/android/server/tof/ContactlessGestureController;)V

    iput-object v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mUpdateTopActivityIfNeededRunnable:Ljava/lang/Runnable;

    .line 1090
    new-instance v1, Lcom/android/server/tof/ContactlessGestureController$3;

    invoke-direct {v1, p0}, Lcom/android/server/tof/ContactlessGestureController$3;-><init>(Lcom/android/server/tof/ContactlessGestureController;)V

    iput-object v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mActivityStateObserver:Landroid/app/IMiuiActivityObserver;

    .line 278
    iput-object p1, p0, Lcom/android/server/tof/ContactlessGestureController;->mContext:Landroid/content/Context;

    .line 279
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {p2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mHandler:Landroid/os/Handler;

    .line 280
    new-instance v1, Lcom/android/server/tof/ContactlessGestureController$TofClientConnecton;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/android/server/tof/ContactlessGestureController$TofClientConnecton;-><init>(Lcom/android/server/tof/ContactlessGestureController;Lcom/android/server/tof/ContactlessGestureController$TofClientConnecton-IA;)V

    iput-object v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofClientConnection:Lcom/android/server/tof/ContactlessGestureController$TofClientConnecton;

    .line 281
    new-instance v1, Lcom/android/server/tof/InputHelper;

    invoke-direct {v1}, Lcom/android/server/tof/InputHelper;-><init>()V

    iput-object v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mInputHelper:Lcom/android/server/tof/InputHelper;

    .line 282
    invoke-static {}, Landroid/app/ActivityManager;->getService()Landroid/app/IActivityManager;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mActivityManager:Landroid/app/IActivityManager;

    .line 283
    iput-object p3, p0, Lcom/android/server/tof/ContactlessGestureController;->mContactlessGestureService:Lcom/android/server/tof/ContactlessGestureService;

    .line 284
    iget-object v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mContext:Landroid/content/Context;

    const-string v3, "phone"

    invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    iput-object v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 285
    const-string/jumbo v1, "window"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    check-cast v1, Lcom/android/server/wm/WindowManagerService;

    iput-object v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mWindowManagerService:Lcom/android/server/wm/WindowManagerService;

    .line 286
    iget-object v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mContext:Landroid/content/Context;

    const-string v3, "display"

    invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/display/DisplayManager;

    iput-object v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    .line 287
    const-class v1, Lcom/android/server/policy/WindowManagerPolicy;

    invoke-static {v1}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/policy/WindowManagerPolicy;

    iput-object v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    .line 288
    new-instance v1, Lcom/android/server/tof/ContactlessGestureController$AccessibilityTelephonyCallback;

    invoke-direct {v1, p0, v2}, Lcom/android/server/tof/ContactlessGestureController$AccessibilityTelephonyCallback;-><init>(Lcom/android/server/tof/ContactlessGestureController;Lcom/android/server/tof/ContactlessGestureController$AccessibilityTelephonyCallback-IA;)V

    iput-object v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mTelephonyCallback:Landroid/telephony/TelephonyCallback;

    .line 289
    new-instance v1, Lcom/android/server/tof/ContactlessGestureController$ScreenStateReceiver;

    invoke-direct {v1, p0, v2}, Lcom/android/server/tof/ContactlessGestureController$ScreenStateReceiver;-><init>(Lcom/android/server/tof/ContactlessGestureController;Lcom/android/server/tof/ContactlessGestureController$ScreenStateReceiver-IA;)V

    iput-object v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mScreenReceiver:Landroid/content/BroadcastReceiver;

    .line 290
    invoke-static {}, Lcom/android/server/power/PowerManagerServiceStub;->get()Lcom/android/server/power/PowerManagerServiceStub;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mPowerManagerServiceImpl:Lcom/android/server/power/PowerManagerServiceStub;

    .line 291
    new-instance v1, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v1, v0}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mServiceBindingLatch:Ljava/util/concurrent/CountDownLatch;

    .line 292
    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureController;->initTofGestureConfig()V

    .line 293
    invoke-static {}, Lcom/android/server/tof/AppFeatureHelper;->getInstance()Lcom/android/server/tof/AppFeatureHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mAppFeatureHelper:Lcom/android/server/tof/AppFeatureHelper;

    .line 294
    iget-object v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/android/server/tof/AppFeatureHelper;->initTofComponentConfig(Landroid/content/Context;)V

    .line 295
    invoke-virtual {p0}, Lcom/android/server/tof/ContactlessGestureController;->getTofClientComponent()Landroid/content/ComponentName;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mComponentName:Landroid/content/ComponentName;

    .line 296
    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureController;->registerListenerIfNeeded()V

    .line 297
    return-void
.end method

.method private awaitServiceBinding()V
    .locals 4

    .line 1063
    :try_start_0
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mServiceBindingLatch:Ljava/util/concurrent/CountDownLatch;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1066
    goto :goto_0

    .line 1064
    :catch_0
    move-exception v0

    .line 1065
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v1, "ContactlessGestureController"

    const-string v2, "Interrupted while waiting to bind contactless gesture client Service."

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1067
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mServiceIsBinding:Z

    .line 1068
    return-void
.end method

.method private checkTopActivities()V
    .locals 4

    .line 853
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mUpdateTopActivityIfNeededRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 854
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mUpdateTopActivityIfNeededRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x384

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 855
    return-void
.end method

.method private dismissTriggerViewIfNeeded(I)V
    .locals 1
    .param p1, "label"    # I

    .line 787
    const/16 v0, 0xc

    if-ne p1, v0, :cond_0

    .line 788
    invoke-virtual {p0, p1}, Lcom/android/server/tof/ContactlessGestureController;->updateGestureHint(I)Z

    .line 790
    :cond_0
    return-void
.end method

.method private initTofGestureConfig()V
    .locals 2

    .line 300
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 301
    const v1, 0x110b002f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureUpSupportFeature:I

    .line 302
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 303
    const v1, 0x110b002b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureDownSupportFeature:I

    .line 304
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 305
    const v1, 0x110b002d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureLeftSupportFeature:I

    .line 306
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 307
    const v1, 0x110b002e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureRightSupportFeature:I

    .line 308
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 309
    const v1, 0x110b002a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureDoublePressSupportFeature:I

    .line 310
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 311
    const v1, 0x110b002c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureDrawCircleSupportFeature:I

    .line 312
    return-void
.end method

.method private isGestureStateLabel(I)Z
    .locals 1
    .param p1, "label"    # I

    .line 682
    const/16 v0, 0x18

    if-eq p1, v0, :cond_1

    const/16 v0, 0x19

    if-eq p1, v0, :cond_1

    const/16 v0, 0x1a

    if-eq p1, v0, :cond_1

    const/16 v0, 0x1b

    if-ne p1, v0, :cond_0

    goto :goto_0

    .line 688
    :cond_0
    const/4 v0, 0x0

    return v0

    .line 686
    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method private isKeyguardShowing()Z
    .locals 1

    .line 465
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    invoke-interface {v0}, Lcom/android/server/policy/WindowManagerPolicy;->isKeyguardShowingAndNotOccluded()Z

    move-result v0

    return v0
.end method

.method private isStateOrTriggerLabelSupport(II)Z
    .locals 2
    .param p1, "label"    # I
    .param p2, "currentSupportFeature"    # I

    .line 602
    const/4 v0, 0x0

    .line 603
    .local v0, "feature":I
    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 610
    :sswitch_0
    iget v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureLeftSupportFeature:I

    and-int v0, p2, v1

    .line 611
    goto :goto_0

    .line 606
    :sswitch_1
    iget v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureRightSupportFeature:I

    and-int v0, p2, v1

    .line 607
    goto :goto_0

    .line 614
    :sswitch_2
    iget v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureUpSupportFeature:I

    and-int v0, p2, v1

    .line 615
    goto :goto_0

    .line 619
    :sswitch_3
    iget v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureDownSupportFeature:I

    and-int v0, p2, v1

    .line 620
    const/16 v1, 0x80

    if-eq p2, v1, :cond_0

    const/16 v1, 0x100

    if-ne p2, v1, :cond_1

    .line 622
    :cond_0
    move v0, p2

    .line 628
    :cond_1
    :goto_0
    if-lez v0, :cond_2

    const/4 v1, 0x1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    return v1

    nop

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_3
        0x14 -> :sswitch_3
        0x15 -> :sswitch_2
        0x16 -> :sswitch_1
        0x17 -> :sswitch_0
        0x18 -> :sswitch_3
        0x19 -> :sswitch_2
        0x1a -> :sswitch_1
        0x1b -> :sswitch_0
    .end sparse-switch
.end method

.method private isTopActivityFocused()Z
    .locals 3

    .line 456
    const/4 v0, 0x1

    .line 457
    .local v0, "topActivityFocused":Z
    iget-object v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mFocusedPackage:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/android/server/tof/ContactlessGestureController;->mTopActivity:Landroid/content/ComponentName;

    if-eqz v2, :cond_0

    .line 458
    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 460
    :cond_0
    return v0
.end method

.method private isTriggerLabel(I)Z
    .locals 1
    .param p1, "label"    # I

    .line 692
    const/16 v0, 0xa

    if-eq p1, v0, :cond_1

    const/16 v0, 0x14

    if-eq p1, v0, :cond_1

    const/16 v0, 0x15

    if-eq p1, v0, :cond_1

    const/16 v0, 0x16

    if-eq p1, v0, :cond_1

    const/16 v0, 0x17

    if-ne p1, v0, :cond_0

    goto :goto_0

    .line 699
    :cond_0
    const/4 v0, 0x0

    return v0

    .line 697
    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method private synthetic lambda$handleGestureEvent$4()V
    .locals 1

    .line 527
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/server/tof/ContactlessGestureController;->registerGestureListenerIfNeeded(Z)V

    return-void
.end method

.method private synthetic lambda$new$3()V
    .locals 2

    .line 476
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mContactlessGestureService:Lcom/android/server/tof/ContactlessGestureService;

    iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mRegisterGesture:Z

    invoke-virtual {v0, v1}, Lcom/android/server/tof/ContactlessGestureService;->registerContactlessGestureListenerIfNeeded(Z)V

    return-void
.end method

.method private synthetic lambda$unBindService$0()V
    .locals 2

    .line 325
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofClientConnection:Lcom/android/server/tof/ContactlessGestureController$TofClientConnecton;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 326
    return-void
.end method

.method private synthetic lambda$updateFocusedPackage$1()V
    .locals 1

    .line 443
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/android/server/tof/ContactlessGestureController;->onSceneChanged(I)V

    return-void
.end method

.method private synthetic lambda$updateFocusedPackage$2()V
    .locals 1

    .line 446
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mUpdateTopActivityIfNeededRunnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 447
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/android/server/tof/ContactlessGestureController;->onSceneChanged(I)V

    .line 448
    return-void
.end method

.method private monitorActivityChanges()V
    .locals 3

    .line 862
    :try_start_0
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mActivityManager:Landroid/app/IActivityManager;

    iget-object v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mActivityStateObserver:Landroid/app/IMiuiActivityObserver;

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-interface {v0, v1, v2}, Landroid/app/IActivityManager;->registerActivityObserver(Landroid/app/IMiuiActivityObserver;Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 865
    goto :goto_0

    .line 863
    :catch_0
    move-exception v0

    .line 864
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "ContactlessGestureController"

    const-string v2, "cannot register activity monitoring"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 866
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureController;->checkTopActivities()V

    .line 867
    return-void
.end method

.method private registerCallStateChange()V
    .locals 3

    .line 873
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    new-instance v1, Landroid/os/HandlerExecutor;

    iget-object v2, p0, Lcom/android/server/tof/ContactlessGestureController;->mHandler:Landroid/os/Handler;

    invoke-direct {v1, v2}, Landroid/os/HandlerExecutor;-><init>(Landroid/os/Handler;)V

    iget-object v2, p0, Lcom/android/server/tof/ContactlessGestureController;->mTelephonyCallback:Landroid/telephony/TelephonyCallback;

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->registerTelephonyCallback(Ljava/util/concurrent/Executor;Landroid/telephony/TelephonyCallback;)V

    .line 875
    return-void
.end method

.method private registerDisplayListener()V
    .locals 3

    .line 886
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    new-instance v1, Lcom/android/server/tof/ContactlessGestureController$2;

    invoke-direct {v1, p0}, Lcom/android/server/tof/ContactlessGestureController$2;-><init>(Lcom/android/server/tof/ContactlessGestureController;)V

    iget-object v2, p0, Lcom/android/server/tof/ContactlessGestureController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1, v2}, Landroid/hardware/display/DisplayManager;->registerDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;Landroid/os/Handler;)V

    .line 904
    return-void
.end method

.method private registerListenerDelayIfNeeded(Z)V
    .locals 1
    .param p1, "register"    # Z

    .line 485
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/server/tof/ContactlessGestureController;->registerListenerDelayIfNeeded(ZZ)V

    .line 486
    return-void
.end method

.method private registerListenerDelayIfNeeded(ZZ)V
    .locals 4
    .param p1, "register"    # Z
    .param p2, "immediate"    # Z

    .line 496
    iput-boolean p1, p0, Lcom/android/server/tof/ContactlessGestureController;->mRegisterGesture:Z

    .line 498
    if-nez p1, :cond_0

    .line 499
    const/16 v0, 0xd

    invoke-virtual {p0, v0}, Lcom/android/server/tof/ContactlessGestureController;->updateGestureHint(I)Z

    .line 501
    :cond_0
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mRegisterGestureRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 502
    if-eqz p2, :cond_1

    .line 503
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mRegisterGestureRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 505
    :cond_1
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mRegisterGestureRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 507
    :goto_0
    return-void
.end method

.method private registerListenerIfNeeded()V
    .locals 0

    .line 358
    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureController;->monitorActivityChanges()V

    .line 359
    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureController;->registerCallStateChange()V

    .line 360
    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureController;->registerDisplayListener()V

    .line 361
    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureController;->registerScreenReceiver()V

    .line 362
    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureController;->updateDefaultDisplayRotation()V

    .line 363
    return-void
.end method

.method private registerScreenReceiver()V
    .locals 3

    .line 878
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 879
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 880
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 881
    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->setPriority(I)V

    .line 882
    iget-object v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/tof/ContactlessGestureController;->mScreenReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 883
    return-void
.end method

.method private showFakeTrigger(II)Z
    .locals 2
    .param p1, "label"    # I
    .param p2, "currentSupportFeature"    # I

    .line 640
    const/4 v0, -0x1

    .line 641
    .local v0, "fakeLabel":I
    iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mIsTrigger:Z

    if-nez v1, :cond_0

    .line 642
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 653
    :pswitch_0
    const/16 v0, 0x17

    .line 654
    goto :goto_0

    .line 650
    :pswitch_1
    const/16 v0, 0x16

    .line 651
    goto :goto_0

    .line 647
    :pswitch_2
    const/16 v0, 0x15

    .line 648
    goto :goto_0

    .line 644
    :pswitch_3
    const/16 v0, 0x14

    .line 645
    nop

    .line 658
    :goto_0
    if-lez v0, :cond_0

    .line 659
    invoke-direct {p0, v0, p2}, Lcom/android/server/tof/ContactlessGestureController;->showTrigger(II)Z

    move-result v1

    return v1

    .line 662
    :cond_0
    const/4 v1, 0x0

    return v1

    :pswitch_data_0
    .packed-switch 0x18
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private showGestureState(II)V
    .locals 1
    .param p1, "label"    # I
    .param p2, "currentSupportFeature"    # I

    .line 595
    invoke-direct {p0, p1}, Lcom/android/server/tof/ContactlessGestureController;->isGestureStateLabel(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 596
    invoke-direct {p0, p1, p2}, Lcom/android/server/tof/ContactlessGestureController;->isStateOrTriggerLabelSupport(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 597
    invoke-virtual {p0, p1}, Lcom/android/server/tof/ContactlessGestureController;->updateGestureHint(I)Z

    .line 599
    :cond_0
    return-void
.end method

.method private showGestureViewIfNeeded(II)V
    .locals 1
    .param p1, "label"    # I
    .param p2, "currentSupportFeature"    # I

    .line 588
    invoke-direct {p0, p1, p2}, Lcom/android/server/tof/ContactlessGestureController;->showTrigger(II)Z

    move-result v0

    if-nez v0, :cond_0

    .line 589
    invoke-direct {p0, p1, p2}, Lcom/android/server/tof/ContactlessGestureController;->showFakeTrigger(II)Z

    move-result v0

    if-nez v0, :cond_0

    .line 590
    invoke-direct {p0, p1, p2}, Lcom/android/server/tof/ContactlessGestureController;->showGestureState(II)V

    .line 592
    :cond_0
    return-void
.end method

.method private showLastTriggerViewIfNeeded()V
    .locals 2

    .line 423
    iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mIsTrigger:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mShouldUpdateTriggerView:Z

    if-eqz v0, :cond_0

    .line 424
    iget v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mLastTriggerLabel:I

    invoke-virtual {p0}, Lcom/android/server/tof/ContactlessGestureController;->getCurrentSupportFeature()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/android/server/tof/ContactlessGestureController;->isStateOrTriggerLabelSupport(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 425
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "show last trigger view, mLastTriggerLabel:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mLastTriggerLabel:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ContactlessGestureController"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 426
    iget v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mLastTriggerLabel:I

    invoke-virtual {p0, v0}, Lcom/android/server/tof/ContactlessGestureController;->updateGestureHint(I)Z

    .line 429
    :cond_0
    return-void
.end method

.method private showTrigger(II)Z
    .locals 1
    .param p1, "label"    # I
    .param p2, "currentSupportFeature"    # I

    .line 673
    invoke-direct {p0, p1}, Lcom/android/server/tof/ContactlessGestureController;->isTriggerLabel(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 674
    invoke-direct {p0, p1, p2}, Lcom/android/server/tof/ContactlessGestureController;->isStateOrTriggerLabelSupport(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 675
    invoke-virtual {p0, p1}, Lcom/android/server/tof/ContactlessGestureController;->updateGestureHint(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mIsTrigger:Z

    .line 676
    const/4 v0, 0x1

    return v0

    .line 678
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private updateDefaultDisplayRotation()V
    .locals 2

    .line 469
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mWindowManagerService:Lcom/android/server/wm/WindowManagerService;

    invoke-virtual {v0}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayRotation()I

    move-result v0

    .line 470
    .local v0, "currentRotation":I
    iget v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mDisplayRotation:I

    if-eq v0, v1, :cond_0

    .line 471
    iput v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mDisplayRotation:I

    .line 473
    :cond_0
    return-void
.end method


# virtual methods
.method public bindService()V
    .locals 1

    .line 315
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mComponentName:Landroid/content/ComponentName;

    invoke-virtual {p0, v0}, Lcom/android/server/tof/ContactlessGestureController;->bindService(Landroid/content/ComponentName;)V

    .line 316
    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureController;->awaitServiceBinding()V

    .line 317
    return-void
.end method

.method public bindService(Landroid/content/ComponentName;)V
    .locals 7
    .param p1, "name"    # Landroid/content/ComponentName;

    .line 332
    const-string/jumbo v0, "unable to bind tof service: "

    invoke-virtual {p0}, Lcom/android/server/tof/ContactlessGestureController;->isServiceInit()Z

    move-result v1

    if-nez v1, :cond_1

    if-eqz p1, :cond_1

    iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mServiceIsBinding:Z

    if-nez v1, :cond_1

    .line 333
    const-string v1, "bindService"

    const-string v2, "ContactlessGestureController"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mServiceIsBinding:Z

    .line 335
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/server/tof/ContactlessGestureController;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 336
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {v1, p1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 337
    const/high16 v3, 0x800000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 339
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tof/ContactlessGestureController;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofClientConnection:Lcom/android/server/tof/ContactlessGestureController$TofClientConnecton;

    sget-object v5, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    const v6, 0x4000001

    invoke-virtual {v3, v1, v4, v6, v5}, Landroid/content/Context;->bindServiceAsUser(Landroid/content/Intent;Landroid/content/ServiceConnection;ILandroid/os/UserHandle;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 342
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 346
    :cond_0
    goto :goto_0

    .line 344
    :catch_0
    move-exception v3

    .line 345
    .local v3, "ex":Ljava/lang/SecurityException;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 348
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v3    # "ex":Ljava/lang/SecurityException;
    :cond_1
    :goto_0
    return-void
.end method

.method public changeAppConfigWithPackageName(Ljava/lang/String;Z)Z
    .locals 1
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "enable"    # Z

    .line 916
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mAppFeatureHelper:Lcom/android/server/tof/AppFeatureHelper;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/tof/AppFeatureHelper;->changeTofGestureAppConfig(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public dump(Ljava/io/PrintWriter;)V
    .locals 3
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 1073
    const-string v0, "Contactless Gesture Controller:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1074
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "    gesture client: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/server/tof/ContactlessGestureController;->getTofClientComponent()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1075
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "    gesture client connected: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/server/tof/ContactlessGestureController;->isServiceInit()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "true"

    goto :goto_0

    :cond_0
    const-string v1, "false"

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1076
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "    register: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mRegisterGesture:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1077
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "    interactive: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mInteractive:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1078
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "    isTrigger: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mIsTrigger:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1079
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "    mLastTriggerLabel: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mLastTriggerLabel:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1080
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "    focused package: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mFocusedPackage:Ljava/lang/String;

    const-string v2, ""

    if-eqz v1, :cond_1

    goto :goto_1

    :cond_1
    move-object v1, v2

    :goto_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1081
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "    display rotation: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mDisplayRotation:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1082
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "    desk top mode enable: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mIsDeskTopMode:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1083
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "    split screen mode: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mIsSplitScreenMode:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1084
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "    isScreenCastMode: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mContactlessGestureService:Lcom/android/server/tof/ContactlessGestureService;

    invoke-virtual {v1}, Lcom/android/server/tof/ContactlessGestureService;->isScreenCastMode()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1085
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "    top activity: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mTopActivity:Landroid/content/ComponentName;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/content/ComponentName;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_2
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1086
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "    resConfig: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mAppFeatureHelper:Lcom/android/server/tof/AppFeatureHelper;

    invoke-virtual {v1}, Lcom/android/server/tof/AppFeatureHelper;->getResGestureConfig()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1087
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "    cloudConfig: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mAppFeatureHelper:Lcom/android/server/tof/AppFeatureHelper;

    invoke-virtual {v1}, Lcom/android/server/tof/AppFeatureHelper;->getCloudGestureConfig()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1088
    return-void
.end method

.method public gestureLabelToString(I)Ljava/lang/String;
    .locals 2
    .param p1, "label"    # I

    .line 1000
    packed-switch p1, :pswitch_data_0

    .line 1056
    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UNKNOWN label:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .local v0, "gesture":Ljava/lang/String;
    goto :goto_0

    .line 1047
    .end local v0    # "gesture":Ljava/lang/String;
    :pswitch_1
    const-string v0, "GESTURE_RIGHT_STATE"

    .line 1048
    .restart local v0    # "gesture":Ljava/lang/String;
    goto :goto_0

    .line 1044
    .end local v0    # "gesture":Ljava/lang/String;
    :pswitch_2
    const-string v0, "GESTURE_LEFT_STATE"

    .line 1045
    .restart local v0    # "gesture":Ljava/lang/String;
    goto :goto_0

    .line 1041
    .end local v0    # "gesture":Ljava/lang/String;
    :pswitch_3
    const-string v0, "GESTURE_DOWN_STATE"

    .line 1042
    .restart local v0    # "gesture":Ljava/lang/String;
    goto :goto_0

    .line 1038
    .end local v0    # "gesture":Ljava/lang/String;
    :pswitch_4
    const-string v0, "GESTURE_UP_STATE"

    .line 1039
    .restart local v0    # "gesture":Ljava/lang/String;
    goto :goto_0

    .line 1032
    .end local v0    # "gesture":Ljava/lang/String;
    :pswitch_5
    const-string v0, "GESTURE_TRIGGER_RIGHT"

    .line 1033
    .restart local v0    # "gesture":Ljava/lang/String;
    goto :goto_0

    .line 1029
    .end local v0    # "gesture":Ljava/lang/String;
    :pswitch_6
    const-string v0, "GESTURE_TRIGGER_LEFT"

    .line 1030
    .restart local v0    # "gesture":Ljava/lang/String;
    goto :goto_0

    .line 1026
    .end local v0    # "gesture":Ljava/lang/String;
    :pswitch_7
    const-string v0, "GESTURE_TRIGGER_DOWN"

    .line 1027
    .restart local v0    # "gesture":Ljava/lang/String;
    goto :goto_0

    .line 1023
    .end local v0    # "gesture":Ljava/lang/String;
    :pswitch_8
    const-string v0, "GESTURE_TRIGGER_UP"

    .line 1024
    .restart local v0    # "gesture":Ljava/lang/String;
    goto :goto_0

    .line 1053
    .end local v0    # "gesture":Ljava/lang/String;
    :pswitch_9
    const-string v0, "GESTURE_GESTURE_WORKING"

    .line 1054
    .restart local v0    # "gesture":Ljava/lang/String;
    goto :goto_0

    .line 1050
    .end local v0    # "gesture":Ljava/lang/String;
    :pswitch_a
    const-string v0, "INVALIDATE_SCENARIO"

    .line 1051
    .restart local v0    # "gesture":Ljava/lang/String;
    goto :goto_0

    .line 1035
    .end local v0    # "gesture":Ljava/lang/String;
    :pswitch_b
    const-string v0, "GESTURE_UN_TRIGGER"

    .line 1036
    .restart local v0    # "gesture":Ljava/lang/String;
    goto :goto_0

    .line 1020
    .end local v0    # "gesture":Ljava/lang/String;
    :pswitch_c
    const-string v0, "GESTURE_TRIGGER"

    .line 1021
    .restart local v0    # "gesture":Ljava/lang/String;
    goto :goto_0

    .line 1017
    .end local v0    # "gesture":Ljava/lang/String;
    :pswitch_d
    const-string v0, "GESTURE_DRAW_CIRCLE"

    .line 1018
    .restart local v0    # "gesture":Ljava/lang/String;
    goto :goto_0

    .line 1014
    .end local v0    # "gesture":Ljava/lang/String;
    :pswitch_e
    const-string v0, "GESTURE_DOUBLE_PRESS"

    .line 1015
    .restart local v0    # "gesture":Ljava/lang/String;
    goto :goto_0

    .line 1011
    .end local v0    # "gesture":Ljava/lang/String;
    :pswitch_f
    const-string v0, "GESTURE_UP"

    .line 1012
    .restart local v0    # "gesture":Ljava/lang/String;
    goto :goto_0

    .line 1008
    .end local v0    # "gesture":Ljava/lang/String;
    :pswitch_10
    const-string v0, "GESTURE_DOWN"

    .line 1009
    .restart local v0    # "gesture":Ljava/lang/String;
    goto :goto_0

    .line 1005
    .end local v0    # "gesture":Ljava/lang/String;
    :pswitch_11
    const-string v0, "GESTURE_RIGHT"

    .line 1006
    .restart local v0    # "gesture":Ljava/lang/String;
    goto :goto_0

    .line 1002
    .end local v0    # "gesture":Ljava/lang/String;
    :pswitch_12
    const-string v0, "GESTURE_LEFT"

    .line 1003
    .restart local v0    # "gesture":Ljava/lang/String;
    nop

    .line 1058
    :goto_0
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_0
        :pswitch_0
        :pswitch_e
        :pswitch_d
        :pswitch_0
        :pswitch_c
        :pswitch_0
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public abstract getAction()Ljava/lang/String;
.end method

.method public getCurrentAppType()I
    .locals 2

    .line 771
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mAppFeatureHelper:Lcom/android/server/tof/AppFeatureHelper;

    iget-object v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mTopActivity:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Lcom/android/server/tof/AppFeatureHelper;->getSupportComponentFromConfig(Landroid/content/ComponentName;)Lcom/android/server/tof/TofGestureComponent;

    move-result-object v0

    .line 773
    .local v0, "component":Lcom/android/server/tof/TofGestureComponent;
    if-nez v0, :cond_0

    .line 774
    const/4 v1, 0x0

    return v1

    .line 776
    :cond_0
    invoke-virtual {v0}, Lcom/android/server/tof/TofGestureComponent;->getCategory()I

    move-result v1

    return v1
.end method

.method public getCurrentPkg()Ljava/lang/String;
    .locals 1

    .line 767
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mTopActivity:Landroid/content/ComponentName;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method getCurrentSupportFeature()I
    .locals 3

    .line 708
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 716
    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureController;->isKeyguardShowing()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureController;->isTopActivityFocused()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_2

    .line 713
    :pswitch_0
    const/16 v0, 0x100

    .line 714
    .local v0, "appSupportFeature":I
    goto :goto_3

    .line 710
    .end local v0    # "appSupportFeature":I
    :pswitch_1
    const/16 v0, 0x80

    .line 711
    .restart local v0    # "appSupportFeature":I
    goto :goto_3

    .line 719
    .end local v0    # "appSupportFeature":I
    :cond_0
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mAppFeatureHelper:Lcom/android/server/tof/AppFeatureHelper;

    iget-object v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mTopActivity:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Lcom/android/server/tof/AppFeatureHelper;->getSupportComponentFromConfig(Landroid/content/ComponentName;)Lcom/android/server/tof/TofGestureComponent;

    move-result-object v0

    .line 721
    .local v0, "component":Lcom/android/server/tof/TofGestureComponent;
    if-nez v0, :cond_1

    .line 722
    const/4 v1, 0x0

    return v1

    .line 724
    :cond_1
    iget v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mDisplayRotation:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_3

    const/4 v2, 0x3

    if-ne v1, v2, :cond_2

    goto :goto_0

    .line 726
    :cond_2
    invoke-virtual {v0}, Lcom/android/server/tof/TofGestureComponent;->getPortraitFeature()I

    move-result v1

    goto :goto_1

    :cond_3
    :goto_0
    invoke-virtual {v0}, Lcom/android/server/tof/TofGestureComponent;->getLandscapeFeature()I

    move-result v1

    :goto_1
    move v0, v1

    .local v1, "appSupportFeature":I
    goto :goto_3

    .line 717
    .end local v0    # "component":Lcom/android/server/tof/TofGestureComponent;
    .end local v1    # "appSupportFeature":I
    :cond_4
    :goto_2
    const/4 v0, 0x0

    .line 731
    .local v0, "appSupportFeature":I
    :goto_3
    return v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method getCurrentSupportGesture()I
    .locals 3

    .line 740
    const/4 v0, 0x0

    .line 741
    .local v0, "gestureSupport":I
    invoke-virtual {p0}, Lcom/android/server/tof/ContactlessGestureController;->getCurrentSupportFeature()I

    move-result v1

    .line 742
    .local v1, "appSupportFeature":I
    iget v2, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureUpSupportFeature:I

    and-int/2addr v2, v1

    if-nez v2, :cond_0

    iget v2, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureDownSupportFeature:I

    and-int/2addr v2, v1

    if-eqz v2, :cond_1

    .line 744
    :cond_0
    or-int/lit8 v0, v0, 0x1

    .line 746
    :cond_1
    iget v2, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureLeftSupportFeature:I

    and-int/2addr v2, v1

    if-nez v2, :cond_2

    iget v2, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureRightSupportFeature:I

    and-int/2addr v2, v1

    if-eqz v2, :cond_3

    .line 748
    :cond_2
    or-int/lit8 v0, v0, 0x2

    .line 750
    :cond_3
    iget v2, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureDoublePressSupportFeature:I

    and-int/2addr v2, v1

    if-eqz v2, :cond_4

    .line 751
    or-int/lit8 v0, v0, 0x4

    .line 753
    :cond_4
    iget v2, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureDrawCircleSupportFeature:I

    and-int/2addr v2, v1

    if-eqz v2, :cond_5

    .line 754
    or-int/lit8 v0, v0, 0x8

    .line 756
    :cond_5
    return v0
.end method

.method getFeatureFromLabel(I)I
    .locals 1
    .param p1, "label"    # I

    .line 936
    const/4 v0, -0x3

    .line 937
    .local v0, "labelFeature":I
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 984
    :pswitch_1
    const/16 v0, -0xb

    .line 985
    goto :goto_0

    .line 981
    :pswitch_2
    const/16 v0, -0xc

    .line 982
    goto :goto_0

    .line 978
    :pswitch_3
    const/16 v0, -0xa

    .line 979
    goto :goto_0

    .line 975
    :pswitch_4
    const/16 v0, -0x9

    .line 976
    goto :goto_0

    .line 969
    :pswitch_5
    const/4 v0, -0x7

    .line 970
    goto :goto_0

    .line 966
    :pswitch_6
    const/4 v0, -0x8

    .line 967
    goto :goto_0

    .line 963
    :pswitch_7
    const/4 v0, -0x6

    .line 964
    goto :goto_0

    .line 960
    :pswitch_8
    const/4 v0, -0x5

    .line 961
    goto :goto_0

    .line 990
    :pswitch_9
    const/4 v0, -0x4

    .line 991
    goto :goto_0

    .line 987
    :pswitch_a
    const/4 v0, -0x3

    .line 988
    goto :goto_0

    .line 972
    :pswitch_b
    const/4 v0, -0x2

    .line 973
    goto :goto_0

    .line 957
    :pswitch_c
    const/4 v0, -0x1

    .line 958
    goto :goto_0

    .line 954
    :pswitch_d
    iget v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureDrawCircleSupportFeature:I

    .line 955
    goto :goto_0

    .line 951
    :pswitch_e
    iget v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureDoublePressSupportFeature:I

    .line 952
    goto :goto_0

    .line 948
    :pswitch_f
    iget v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureUpSupportFeature:I

    .line 949
    goto :goto_0

    .line 945
    :pswitch_10
    iget v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureDownSupportFeature:I

    .line 946
    goto :goto_0

    .line 942
    :pswitch_11
    iget v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureRightSupportFeature:I

    .line 943
    goto :goto_0

    .line 939
    :pswitch_12
    iget v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureLeftSupportFeature:I

    .line 940
    nop

    .line 995
    :goto_0
    return v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_0
        :pswitch_0
        :pswitch_e
        :pswitch_d
        :pswitch_0
        :pswitch_c
        :pswitch_0
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public abstract getTofClientComponent()Landroid/content/ComponentName;
.end method

.method public getTofGestureAppData()Lcom/miui/tof/gesture/TofGestureAppData;
    .locals 1

    .line 923
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mAppFeatureHelper:Lcom/android/server/tof/AppFeatureHelper;

    invoke-virtual {v0}, Lcom/android/server/tof/AppFeatureHelper;->getTofGestureAppData()Lcom/miui/tof/gesture/TofGestureAppData;

    move-result-object v0

    return-object v0
.end method

.method public handleGestureEvent(I)V
    .locals 6
    .param p1, "label"    # I

    .line 510
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "sensorEvent:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/android/server/tof/ContactlessGestureController;->gestureLabelToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mIsTrigger:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mIsTrigger:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ContactlessGestureController"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 511
    invoke-direct {p0, p1}, Lcom/android/server/tof/ContactlessGestureController;->dismissTriggerViewIfNeeded(I)V

    .line 512
    iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mRegisterGesture:Z

    if-nez v0, :cond_0

    .line 513
    return-void

    .line 516
    :cond_0
    invoke-virtual {p0}, Lcom/android/server/tof/ContactlessGestureController;->getCurrentSupportFeature()I

    move-result v0

    .line 520
    .local v0, "appSupportFeature":I
    const/4 v2, 0x0

    const/16 v3, 0xc

    if-ne p1, v3, :cond_1

    .line 521
    iput-boolean v2, p0, Lcom/android/server/tof/ContactlessGestureController;->mIsTrigger:Z

    goto :goto_0

    .line 522
    :cond_1
    invoke-direct {p0, p1}, Lcom/android/server/tof/ContactlessGestureController;->isGestureStateLabel(I)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-direct {p0, p1}, Lcom/android/server/tof/ContactlessGestureController;->isTriggerLabel(I)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 523
    :cond_2
    iput p1, p0, Lcom/android/server/tof/ContactlessGestureController;->mLastTriggerLabel:I

    .line 526
    if-nez v0, :cond_3

    invoke-direct {p0, p1}, Lcom/android/server/tof/ContactlessGestureController;->isTriggerLabel(I)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 527
    iget-object v4, p0, Lcom/android/server/tof/ContactlessGestureController;->mHandler:Landroid/os/Handler;

    new-instance v5, Lcom/android/server/tof/ContactlessGestureController$$ExternalSyntheticLambda0;

    invoke-direct {v5, p0}, Lcom/android/server/tof/ContactlessGestureController$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/tof/ContactlessGestureController;)V

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 531
    :cond_3
    :goto_0
    if-eqz v0, :cond_7

    .line 532
    const/4 v4, 0x0

    .line 533
    .local v4, "feature":I
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto :goto_1

    .line 553
    :pswitch_1
    invoke-virtual {p0, p1}, Lcom/android/server/tof/ContactlessGestureController;->updateGestureHint(I)Z

    .line 554
    goto :goto_1

    .line 564
    :pswitch_2
    invoke-direct {p0, p1, v0}, Lcom/android/server/tof/ContactlessGestureController;->showGestureViewIfNeeded(II)V

    .line 565
    goto :goto_1

    .line 550
    :pswitch_3
    iget v5, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureDrawCircleSupportFeature:I

    and-int v4, v0, v5

    .line 551
    goto :goto_1

    .line 547
    :pswitch_4
    iget v5, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureDoublePressSupportFeature:I

    and-int v4, v0, v5

    .line 548
    goto :goto_1

    .line 544
    :pswitch_5
    iget v5, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureUpSupportFeature:I

    and-int v4, v0, v5

    .line 545
    goto :goto_1

    .line 541
    :pswitch_6
    iget v5, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureDownSupportFeature:I

    and-int v4, v0, v5

    .line 542
    goto :goto_1

    .line 538
    :pswitch_7
    iget v5, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureRightSupportFeature:I

    and-int v4, v0, v5

    .line 539
    goto :goto_1

    .line 535
    :pswitch_8
    iget v5, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureLeftSupportFeature:I

    and-int v4, v0, v5

    .line 536
    nop

    .line 570
    :goto_1
    if-eq p1, v3, :cond_6

    .line 571
    iget-object v3, p0, Lcom/android/server/tof/ContactlessGestureController;->mTopActivity:Landroid/content/ComponentName;

    if-nez v3, :cond_4

    const/4 v3, 0x0

    goto :goto_2

    :cond_4
    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 572
    .local v3, "pkg":Ljava/lang/String;
    :goto_2
    if-lez v4, :cond_5

    const/4 v2, 0x1

    .line 573
    .local v2, "success":Z
    :cond_5
    iget-object v5, p0, Lcom/android/server/tof/ContactlessGestureController;->mPowerManagerServiceImpl:Lcom/android/server/power/PowerManagerServiceStub;

    invoke-virtual {v5, v3, v2, p1}, Lcom/android/server/power/PowerManagerServiceStub;->notifyGestureEvent(Ljava/lang/String;ZI)V

    .line 578
    .end local v2    # "success":Z
    .end local v3    # "pkg":Ljava/lang/String;
    :cond_6
    iget-boolean v2, p0, Lcom/android/server/tof/ContactlessGestureController;->mIsTrigger:Z

    if-eqz v2, :cond_7

    if-lez v4, :cond_7

    .line 579
    invoke-virtual {p0, p1}, Lcom/android/server/tof/ContactlessGestureController;->updateGestureHint(I)Z

    .line 580
    iget-object v2, p0, Lcom/android/server/tof/ContactlessGestureController;->mInputHelper:Lcom/android/server/tof/InputHelper;

    invoke-virtual {v2, v4}, Lcom/android/server/tof/InputHelper;->injectSupportInputEvent(I)V

    .line 581
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleTofGesture feature:0x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",mIsTrigger:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/server/tof/ContactlessGestureController;->mIsTrigger:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 585
    .end local v4    # "feature":I
    :cond_7
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public abstract isServiceInit()Z
.end method

.method public abstract notifyRotationChanged(I)V
.end method

.method public abstract onGestureServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
.end method

.method public onSceneChanged(I)V
    .locals 7
    .param p1, "scene"    # I

    .line 371
    iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mInteractive:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mContactlessGestureService:Lcom/android/server/tof/ContactlessGestureService;

    invoke-virtual {v0}, Lcom/android/server/tof/ContactlessGestureService;->isSupportTofGestureFeature()Z

    move-result v0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mDisplayRotation:I

    if-ne v0, v2, :cond_d

    :cond_0
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mContactlessGestureService:Lcom/android/server/tof/ContactlessGestureService;

    .line 373
    invoke-virtual {v0}, Lcom/android/server/tof/ContactlessGestureService;->isScreenCastMode()Z

    move-result v0

    if-nez v0, :cond_d

    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mContactlessGestureService:Lcom/android/server/tof/ContactlessGestureService;

    .line 374
    invoke-virtual {v0}, Lcom/android/server/tof/ContactlessGestureService;->isTalkBackEnabled()Z

    move-result v0

    if-nez v0, :cond_d

    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mContactlessGestureService:Lcom/android/server/tof/ContactlessGestureService;

    .line 375
    invoke-virtual {v0}, Lcom/android/server/tof/ContactlessGestureService;->isOneHandedModeEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    goto/16 :goto_7

    .line 380
    :cond_1
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v0

    .line 381
    .local v0, "callState":I
    if-eq v0, v2, :cond_c

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    goto/16 :goto_6

    .line 389
    :cond_2
    iget-boolean v4, p0, Lcom/android/server/tof/ContactlessGestureController;->mIsSplitScreenMode:Z

    if-nez v4, :cond_b

    iget-boolean v4, p0, Lcom/android/server/tof/ContactlessGestureController;->mTopActivityFullScreenOrNotOccluded:Z

    if-nez v4, :cond_3

    goto/16 :goto_5

    .line 394
    :cond_3
    iget-object v4, p0, Lcom/android/server/tof/ContactlessGestureController;->mAppFeatureHelper:Lcom/android/server/tof/AppFeatureHelper;

    iget-object v5, p0, Lcom/android/server/tof/ContactlessGestureController;->mTopActivity:Landroid/content/ComponentName;

    iget v6, p0, Lcom/android/server/tof/ContactlessGestureController;->mDisplayRotation:I

    if-eqz v6, :cond_5

    if-ne v6, v3, :cond_4

    goto :goto_0

    :cond_4
    move v3, v1

    goto :goto_1

    :cond_5
    :goto_0
    move v3, v2

    :goto_1
    invoke-virtual {v4, v5, v3}, Lcom/android/server/tof/AppFeatureHelper;->getSupportFeature(Landroid/content/ComponentName;Z)I

    move-result v3

    if-lez v3, :cond_6

    move v3, v2

    goto :goto_2

    :cond_6
    move v3, v1

    .line 397
    .local v3, "isSupported":Z
    :goto_2
    sget-boolean v4, Lcom/android/server/tof/ContactlessGestureService;->mDebug:Z

    if-eqz v4, :cond_8

    .line 398
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isSupported:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 399
    iget-object v5, p0, Lcom/android/server/tof/ContactlessGestureController;->mTopActivity:Landroid/content/ComponentName;

    if-eqz v5, :cond_7

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ", component: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/server/tof/ContactlessGestureController;->mTopActivity:Landroid/content/ComponentName;

    invoke-virtual {v6}, Landroid/content/ComponentName;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_3

    :cond_7
    const-string v5, ""

    :goto_3
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 398
    const-string v5, "ContactlessGestureController"

    invoke-static {v5, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 401
    :cond_8
    invoke-direct {p0, v3}, Lcom/android/server/tof/ContactlessGestureController;->registerListenerDelayIfNeeded(Z)V

    .line 403
    if-eqz v3, :cond_9

    invoke-virtual {p0}, Lcom/android/server/tof/ContactlessGestureController;->getCurrentSupportFeature()I

    move-result v4

    if-eqz v4, :cond_9

    .line 404
    iget-object v4, p0, Lcom/android/server/tof/ContactlessGestureController;->mContactlessGestureService:Lcom/android/server/tof/ContactlessGestureService;

    iget-boolean v4, v4, Lcom/android/server/tof/ContactlessGestureService;->mAonGestureEnabled:Z

    if-eqz v4, :cond_a

    .line 406
    iget-object v4, p0, Lcom/android/server/tof/ContactlessGestureController;->mContactlessGestureService:Lcom/android/server/tof/ContactlessGestureService;

    invoke-virtual {v4}, Lcom/android/server/tof/ContactlessGestureService;->startGestureClientIfNeeded()V

    .line 407
    const/16 v4, 0xe

    invoke-virtual {p0, v4}, Lcom/android/server/tof/ContactlessGestureController;->updateGestureHint(I)Z

    .line 410
    if-eq p1, v2, :cond_a

    const/4 v2, 0x3

    if-eq p1, v2, :cond_a

    .line 411
    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureController;->showLastTriggerViewIfNeeded()V

    .line 412
    iput-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mShouldUpdateTriggerView:Z

    goto :goto_4

    .line 417
    :cond_9
    const/16 v1, 0xd

    invoke-virtual {p0, v1}, Lcom/android/server/tof/ContactlessGestureController;->updateGestureHint(I)Z

    .line 418
    iput-boolean v2, p0, Lcom/android/server/tof/ContactlessGestureController;->mShouldUpdateTriggerView:Z

    .line 420
    :cond_a
    :goto_4
    return-void

    .line 390
    .end local v3    # "isSupported":Z
    :cond_b
    :goto_5
    invoke-direct {p0, v1}, Lcom/android/server/tof/ContactlessGestureController;->registerListenerDelayIfNeeded(Z)V

    .line 391
    return-void

    .line 383
    :cond_c
    :goto_6
    invoke-direct {p0, v2, v2}, Lcom/android/server/tof/ContactlessGestureController;->registerListenerDelayIfNeeded(ZZ)V

    .line 384
    return-void

    .line 376
    .end local v0    # "callState":I
    :cond_d
    :goto_7
    invoke-direct {p0, v1}, Lcom/android/server/tof/ContactlessGestureController;->registerListenerDelayIfNeeded(Z)V

    .line 377
    return-void
.end method

.method public parseGestureComponentFromCloud(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 907
    .local p1, "componentFeatureList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mAppFeatureHelper:Lcom/android/server/tof/AppFeatureHelper;

    invoke-virtual {v0, p1}, Lcom/android/server/tof/AppFeatureHelper;->parseContactlessGestureComponentFromCloud(Ljava/util/List;)V

    .line 908
    return-void
.end method

.method public abstract registerGestureListenerIfNeeded(Z)V
.end method

.method public abstract restContactlessGestureService()V
.end method

.method public abstract showGestureNotification()V
.end method

.method public unBindService()V
    .locals 2

    .line 324
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/tof/ContactlessGestureController$$ExternalSyntheticLambda4;

    invoke-direct {v1, p0}, Lcom/android/server/tof/ContactlessGestureController$$ExternalSyntheticLambda4;-><init>(Lcom/android/server/tof/ContactlessGestureController;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 327
    return-void
.end method

.method public updateDeskTopMode(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .line 432
    iput-boolean p1, p0, Lcom/android/server/tof/ContactlessGestureController;->mIsDeskTopMode:Z

    .line 433
    return-void
.end method

.method public updateFocusedPackage(Ljava/lang/String;)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .line 441
    iput-object p1, p0, Lcom/android/server/tof/ContactlessGestureController;->mFocusedPackage:Ljava/lang/String;

    .line 442
    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureController;->isTopActivityFocused()Z

    move-result v0

    if-nez v0, :cond_0

    .line 443
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/tof/ContactlessGestureController$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0}, Lcom/android/server/tof/ContactlessGestureController$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/tof/ContactlessGestureController;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 445
    :cond_0
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/tof/ContactlessGestureController$$ExternalSyntheticLambda2;

    invoke-direct {v1, p0}, Lcom/android/server/tof/ContactlessGestureController$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/tof/ContactlessGestureController;)V

    const-wide/16 v2, 0x708

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 450
    :goto_0
    return-void
.end method

.method public abstract updateGestureHint(I)Z
.end method
