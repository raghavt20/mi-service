.class Lcom/android/server/tof/AONGestureController$1;
.super Lcom/xiaomi/aon/IMiAONListener$Stub;
.source "AONGestureController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/tof/AONGestureController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/tof/AONGestureController;


# direct methods
.method constructor <init>(Lcom/android/server/tof/AONGestureController;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/tof/AONGestureController;

    .line 168
    iput-object p1, p0, Lcom/android/server/tof/AONGestureController$1;->this$0:Lcom/android/server/tof/AONGestureController;

    invoke-direct {p0}, Lcom/xiaomi/aon/IMiAONListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onCallbackListener(I[I)V
    .locals 2
    .param p1, "type"    # I
    .param p2, "data"    # [I

    .line 171
    if-eqz p2, :cond_1

    array-length v0, p2

    const/4 v1, 0x2

    if-lt v0, v1, :cond_1

    const/4 v0, 0x1

    aget v1, p2, v0

    if-gtz v1, :cond_0

    goto :goto_0

    .line 177
    :cond_0
    iget-object v1, p0, Lcom/android/server/tof/AONGestureController$1;->this$0:Lcom/android/server/tof/AONGestureController;

    aget v0, p2, v0

    invoke-virtual {v1, v0}, Lcom/android/server/tof/AONGestureController;->handleGestureEvent(I)V

    .line 178
    return-void

    .line 172
    :cond_1
    :goto_0
    sget-boolean v0, Lcom/android/server/tof/ContactlessGestureService;->mDebug:Z

    if-eqz v0, :cond_2

    .line 173
    const-string v0, "AONGestureController"

    const-string v1, "The aon data is valid!"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    :cond_2
    return-void
.end method

.method public onImageAvailiable(I)V
    .locals 0
    .param p1, "frame"    # I

    .line 182
    return-void
.end method
