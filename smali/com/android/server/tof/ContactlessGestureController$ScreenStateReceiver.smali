.class final Lcom/android/server/tof/ContactlessGestureController$ScreenStateReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ContactlessGestureController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/tof/ContactlessGestureController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ScreenStateReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/tof/ContactlessGestureController;


# direct methods
.method public static synthetic $r8$lambda$5gt7B1sX6XXFEdKWasrfxKVXj3c(Lcom/android/server/tof/ContactlessGestureController$ScreenStateReceiver;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureController$ScreenStateReceiver;->lambda$onReceive$0()V

    return-void
.end method

.method private constructor <init>(Lcom/android/server/tof/ContactlessGestureController;)V
    .locals 0

    .line 1122
    iput-object p1, p0, Lcom/android/server/tof/ContactlessGestureController$ScreenStateReceiver;->this$0:Lcom/android/server/tof/ContactlessGestureController;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/tof/ContactlessGestureController;Lcom/android/server/tof/ContactlessGestureController$ScreenStateReceiver-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/tof/ContactlessGestureController$ScreenStateReceiver;-><init>(Lcom/android/server/tof/ContactlessGestureController;)V

    return-void
.end method

.method private synthetic lambda$onReceive$0()V
    .locals 2

    .line 1130
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController$ScreenStateReceiver;->this$0:Lcom/android/server/tof/ContactlessGestureController;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/android/server/tof/ContactlessGestureController;->onSceneChanged(I)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 1125
    const-string v0, "android.intent.action.SCREEN_OFF"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1126
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController$ScreenStateReceiver;->this$0:Lcom/android/server/tof/ContactlessGestureController;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/server/tof/ContactlessGestureController;->-$$Nest$fputmInteractive(Lcom/android/server/tof/ContactlessGestureController;Z)V

    goto :goto_0

    .line 1127
    :cond_0
    const-string v0, "android.intent.action.SCREEN_ON"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1128
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController$ScreenStateReceiver;->this$0:Lcom/android/server/tof/ContactlessGestureController;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/server/tof/ContactlessGestureController;->-$$Nest$fputmInteractive(Lcom/android/server/tof/ContactlessGestureController;Z)V

    .line 1130
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController$ScreenStateReceiver;->this$0:Lcom/android/server/tof/ContactlessGestureController;

    iget-object v0, v0, Lcom/android/server/tof/ContactlessGestureController;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/tof/ContactlessGestureController$ScreenStateReceiver$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/android/server/tof/ContactlessGestureController$ScreenStateReceiver$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/tof/ContactlessGestureController$ScreenStateReceiver;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1131
    return-void
.end method
