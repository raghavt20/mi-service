class com.android.server.tof.AONGestureController$2 implements android.content.ServiceConnection {
	 /* .source "AONGestureController.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/tof/AONGestureController; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.tof.AONGestureController this$0; //synthetic
/* # direct methods */
public static void $r8$lambda$Y24CoqKwY8xgMfkrj-C_HmZrmSI ( com.android.server.tof.AONGestureController$2 p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/tof/AONGestureController$2;->lambda$cleanupService$1()V */
return;
} // .end method
public static void $r8$lambda$yHBlKupqIPrPXosvfhlF8Ffj-Gg ( com.android.server.tof.AONGestureController$2 p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/tof/AONGestureController$2;->lambda$onServiceConnected$0()V */
return;
} // .end method
 com.android.server.tof.AONGestureController$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/tof/AONGestureController; */
/* .line 228 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
private void lambda$cleanupService$1 ( ) { //synthethic
/* .locals 2 */
/* .line 255 */
v0 = this.this$0;
int v1 = 0; // const/4 v1, 0x0
com.android.server.tof.AONGestureController .-$$Nest$fputmAonService ( v0,v1 );
/* .line 256 */
v0 = this.this$0;
int v1 = 0; // const/4 v1, 0x0
com.android.server.tof.AONGestureController .-$$Nest$fputmListeningState ( v0,v1 );
/* .line 257 */
return;
} // .end method
private void lambda$onServiceConnected$0 ( ) { //synthethic
/* .locals 4 */
/* .line 233 */
v0 = this.this$0;
int v1 = 6; // const/4 v1, 0x6
/* const v2, 0x927c0 */
/* const/16 v3, 0x102 */
com.android.server.tof.AONGestureController .-$$Nest$mregisterListener ( v0,v3,v1,v2 );
return;
} // .end method
/* # virtual methods */
public void cleanupService ( ) {
/* .locals 2 */
/* .line 254 */
v0 = this.this$0;
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/tof/AONGestureController$2$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0}, Lcom/android/server/tof/AONGestureController$2$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/tof/AONGestureController$2;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 258 */
return;
} // .end method
public void onBindingDied ( android.content.ComponentName p0 ) {
/* .locals 0 */
/* .param p1, "name" # Landroid/content/ComponentName; */
/* .line 245 */
(( com.android.server.tof.AONGestureController$2 ) p0 ).cleanupService ( ); // invoke-virtual {p0}, Lcom/android/server/tof/AONGestureController$2;->cleanupService()V
/* .line 246 */
return;
} // .end method
public void onNullBinding ( android.content.ComponentName p0 ) {
/* .locals 0 */
/* .param p1, "name" # Landroid/content/ComponentName; */
/* .line 250 */
(( com.android.server.tof.AONGestureController$2 ) p0 ).cleanupService ( ); // invoke-virtual {p0}, Lcom/android/server/tof/AONGestureController$2;->cleanupService()V
/* .line 251 */
return;
} // .end method
public void onServiceConnected ( android.content.ComponentName p0, android.os.IBinder p1 ) {
/* .locals 2 */
/* .param p1, "className" # Landroid/content/ComponentName; */
/* .param p2, "service" # Landroid/os/IBinder; */
/* .line 231 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Aon service connected success, service:"; // const-string v1, "Aon service connected success, service:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "AONGestureController"; // const-string v1, "AONGestureController"
android.util.Slog .i ( v1,v0 );
/* .line 232 */
v0 = this.this$0;
android.os.Binder .allowBlocking ( p2 );
com.xiaomi.aon.IMiAON$Stub .asInterface ( v1 );
com.android.server.tof.AONGestureController .-$$Nest$fputmAonService ( v0,v1 );
/* .line 233 */
v0 = this.this$0;
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/tof/AONGestureController$2$$ExternalSyntheticLambda1; */
/* invoke-direct {v1, p0}, Lcom/android/server/tof/AONGestureController$2$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/tof/AONGestureController$2;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 235 */
return;
} // .end method
public void onServiceDisconnected ( android.content.ComponentName p0 ) {
/* .locals 2 */
/* .param p1, "className" # Landroid/content/ComponentName; */
/* .line 239 */
final String v0 = "AONGestureController"; // const-string v0, "AONGestureController"
final String v1 = "Aon service connected failure"; // const-string v1, "Aon service connected failure"
android.util.Slog .i ( v0,v1 );
/* .line 240 */
(( com.android.server.tof.AONGestureController$2 ) p0 ).cleanupService ( ); // invoke-virtual {p0}, Lcom/android/server/tof/AONGestureController$2;->cleanupService()V
/* .line 241 */
return;
} // .end method
