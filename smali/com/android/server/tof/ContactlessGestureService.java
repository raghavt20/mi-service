public class com.android.server.tof.ContactlessGestureService extends com.android.server.SystemService {
	 /* .source "ContactlessGestureService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/tof/ContactlessGestureService$SettingsObserver;, */
	 /* Lcom/android/server/tof/ContactlessGestureService$BinderService;, */
	 /* Lcom/android/server/tof/ContactlessGestureService$LocalService;, */
	 /* Lcom/android/server/tof/ContactlessGestureService$TofServiceShellCommand; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer DEBOUNCE_TIME;
private static final Integer EXTERNAL_DISPLAY_CONNECTED;
public static final java.lang.String FEATURE_AON_GESTURE_SUPPORT;
public static final java.lang.String FEATURE_TOF_GESTURE_SUPPORT;
public static final java.lang.String FEATURE_TOF_PROXIMITY_SUPPORT;
private static final java.lang.String GESTURE_GUIDE_DISMISS;
private static final java.lang.String GESTURE_THREAD_NAME;
private static final java.lang.String MIUI_DESK_TOP_MODE;
private static final java.lang.String SERVICE_NAME;
private static final Integer SYNERGY_MODE_ON;
private static final java.lang.String TAG;
private static final Integer TOF_GESTURE_SENSOR_TYPE;
private static final Integer TOF_PERSON_LEAVE_SENSOR_TYPE;
private static final Integer TOF_PERSON_NEAR_SENSOR_TYPE;
public static Boolean mDebug;
/* # instance fields */
private android.view.accessibility.AccessibilityManager mAccessibilityManager;
public Boolean mAonGestureEnabled;
private Boolean mAonGestureSupport;
private java.util.List mComponentFeatureList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.android.server.tof.ContactlessGestureController mContactlessGestureController;
private final android.content.Context mContext;
private Boolean mDeskTopModeEnabled;
private Boolean mExternalDisplayConnected;
private android.os.Handler mHandler;
private Boolean mIsInteractive;
private Boolean mOneHandedModeEnable;
private android.os.PowerManager mPowerManager;
private com.android.server.power.PowerManagerServiceStub mPowerManagerServiceImpl;
private Boolean mScreenProjectInScreen;
private android.hardware.SensorManager mSensorManager;
private com.android.server.tof.ContactlessGestureService$SettingsObserver mSettingsObserver;
private Boolean mSynergyModeEnable;
private Boolean mTofGestureEnabled;
private Boolean mTofGestureRegistered;
private android.hardware.Sensor mTofGestureSensor;
private Boolean mTofGestureSupport;
private Boolean mTofPersonLeaveRegistered;
private android.hardware.Sensor mTofPersonLeaveSensor;
private Boolean mTofPersonNearRegistered;
private android.hardware.Sensor mTofPersonNearSensor;
private Boolean mTofProximity;
private Boolean mTofProximitySupport;
private Boolean mTofScreenOffEnabled;
private Boolean mTofScreenOnEnabled;
private final android.hardware.SensorEventListener mTofSensorListener;
/* # direct methods */
static Boolean -$$Nest$fgetmAonGestureSupport ( com.android.server.tof.ContactlessGestureService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/tof/ContactlessGestureService;->mAonGestureSupport:Z */
} // .end method
static com.android.server.tof.ContactlessGestureController -$$Nest$fgetmContactlessGestureController ( com.android.server.tof.ContactlessGestureService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContactlessGestureController;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.android.server.tof.ContactlessGestureService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static android.os.Handler -$$Nest$fgetmHandler ( com.android.server.tof.ContactlessGestureService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHandler;
} // .end method
static Boolean -$$Nest$fgetmTofGestureEnabled ( com.android.server.tof.ContactlessGestureService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureEnabled:Z */
} // .end method
static Boolean -$$Nest$fgetmTofGestureSupport ( com.android.server.tof.ContactlessGestureService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureSupport:Z */
} // .end method
static Boolean -$$Nest$fgetmTofProximitySupport ( com.android.server.tof.ContactlessGestureService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofProximitySupport:Z */
} // .end method
static void -$$Nest$fputmComponentFeatureList ( com.android.server.tof.ContactlessGestureService p0, java.util.List p1 ) { //bridge//synthethic
/* .locals 0 */
this.mComponentFeatureList = p1;
return;
} // .end method
static void -$$Nest$fputmIsInteractive ( com.android.server.tof.ContactlessGestureService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/tof/ContactlessGestureService;->mIsInteractive:Z */
return;
} // .end method
static void -$$Nest$mdumpInternal ( com.android.server.tof.ContactlessGestureService p0, java.io.PrintWriter p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/tof/ContactlessGestureService;->dumpInternal(Ljava/io/PrintWriter;)V */
return;
} // .end method
static void -$$Nest$mhandleGestureSensorChanged ( com.android.server.tof.ContactlessGestureService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/tof/ContactlessGestureService;->handleGestureSensorChanged(I)V */
return;
} // .end method
static void -$$Nest$mhandleSettingsChanged ( com.android.server.tof.ContactlessGestureService p0, android.net.Uri p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/tof/ContactlessGestureService;->handleSettingsChanged(Landroid/net/Uri;)V */
return;
} // .end method
static void -$$Nest$mhandleTofProximitySensorChanged ( com.android.server.tof.ContactlessGestureService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/tof/ContactlessGestureService;->handleTofProximitySensorChanged(Z)V */
return;
} // .end method
static Boolean -$$Nest$misAonGestureSupport ( com.android.server.tof.ContactlessGestureService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->isAonGestureSupport()Z */
} // .end method
static void -$$Nest$mregisterTofProximityListenerIfNeeded ( com.android.server.tof.ContactlessGestureService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->registerTofProximityListenerIfNeeded()V */
return;
} // .end method
static Integer -$$Nest$msetDebug ( com.android.server.tof.ContactlessGestureService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1}, Lcom/android/server/tof/ContactlessGestureService;->setDebug(Z)I */
} // .end method
static java.lang.String -$$Nest$sfgetTAG ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.android.server.tof.ContactlessGestureService.TAG;
} // .end method
static com.android.server.tof.ContactlessGestureService ( ) {
/* .locals 1 */
/* .line 66 */
/* const-class v0, Lcom/android/server/tof/ContactlessGestureService; */
(( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
/* .line 67 */
int v0 = 0; // const/4 v0, 0x0
com.android.server.tof.ContactlessGestureService.mDebug = (v0!= 0);
return;
} // .end method
public com.android.server.tof.ContactlessGestureService ( ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 119 */
/* invoke-direct {p0, p1}, Lcom/android/server/SystemService;-><init>(Landroid/content/Context;)V */
/* .line 108 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mIsInteractive:Z */
/* .line 307 */
/* new-instance v0, Lcom/android/server/tof/ContactlessGestureService$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/tof/ContactlessGestureService$1;-><init>(Lcom/android/server/tof/ContactlessGestureService;)V */
this.mTofSensorListener = v0;
/* .line 120 */
this.mContext = p1;
/* .line 121 */
/* new-instance v0, Lcom/android/server/ServiceThread; */
int v1 = -4; // const/4 v1, -0x4
int v2 = 0; // const/4 v2, 0x0
final String v3 = "miui.gesture"; // const-string v3, "miui.gesture"
/* invoke-direct {v0, v3, v1, v2}, Lcom/android/server/ServiceThread;-><init>(Ljava/lang/String;IZ)V */
/* .line 123 */
/* .local v0, "handlerThread":Lcom/android/server/ServiceThread; */
(( com.android.server.ServiceThread ) v0 ).start ( ); // invoke-virtual {v0}, Lcom/android/server/ServiceThread;->start()V
/* .line 124 */
/* new-instance v1, Landroid/os/Handler; */
(( com.android.server.ServiceThread ) v0 ).getLooper ( ); // invoke-virtual {v0}, Lcom/android/server/ServiceThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
this.mHandler = v1;
/* .line 125 */
/* new-instance v1, Lcom/android/server/tof/ContactlessGestureService$SettingsObserver; */
v2 = this.mHandler;
/* invoke-direct {v1, p0, v2}, Lcom/android/server/tof/ContactlessGestureService$SettingsObserver;-><init>(Lcom/android/server/tof/ContactlessGestureService;Landroid/os/Handler;)V */
this.mSettingsObserver = v1;
/* .line 126 */
/* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->checkFeatureSupport()V */
/* .line 127 */
return;
} // .end method
private void checkFeatureSupport ( ) {
/* .locals 3 */
/* .line 402 */
final String v0 = "config_tof_proximity_available"; // const-string v0, "config_tof_proximity_available"
int v1 = 0; // const/4 v1, 0x0
v0 = miui.util.FeatureParser .getBoolean ( v0,v1 );
/* iput-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofProximitySupport:Z */
/* .line 403 */
final String v0 = "config_tof_gesture_available"; // const-string v0, "config_tof_gesture_available"
v0 = miui.util.FeatureParser .getBoolean ( v0,v1 );
/* iput-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureSupport:Z */
/* .line 404 */
final String v0 = "config_aon_gesture_available"; // const-string v0, "config_aon_gesture_available"
v0 = miui.util.FeatureParser .getBoolean ( v0,v1 );
/* iput-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mAonGestureSupport:Z */
/* .line 405 */
v0 = com.android.server.tof.ContactlessGestureService.TAG;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "check feature support mTofProximitySupport:"; // const-string v2, "check feature support mTofProximitySupport:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofProximitySupport:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = ", mTofGestureSupport:"; // const-string v2, ", mTofGestureSupport:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureSupport:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = ", mAonGestureSupport:"; // const-string v2, ", mAonGestureSupport:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/tof/ContactlessGestureService;->mAonGestureSupport:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v1 );
/* .line 408 */
return;
} // .end method
private void dumpInternal ( java.io.PrintWriter p0 ) {
/* .locals 2 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 564 */
final String v0 = "ContactlessGesture service status "; // const-string v0, "ContactlessGesture service status "
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 565 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mTofProximitySupport:"; // const-string v1, " mTofProximitySupport:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofProximitySupport:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 566 */
/* iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofProximitySupport:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 567 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mTofProximity:"; // const-string v1, " mTofProximity:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofProximity:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 568 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mTofScreenOnEnabled:"; // const-string v1, " mTofScreenOnEnabled:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofScreenOnEnabled:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 569 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mTofScreenOffEnabled:"; // const-string v1, " mTofScreenOffEnabled:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofScreenOffEnabled:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 570 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mTofPersonNearRegistered:"; // const-string v1, " mTofPersonNearRegistered:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofPersonNearRegistered:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 571 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mTofPersonLeaveRegistered:"; // const-string v1, " mTofPersonLeaveRegistered:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofPersonLeaveRegistered:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 573 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mTofGestureSupport:"; // const-string v1, " mTofGestureSupport:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureSupport:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 574 */
/* iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureSupport:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 575 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mTofGestureEnabled:"; // const-string v1, " mTofGestureEnabled:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureEnabled:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 576 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mTofGestureRegistered:"; // const-string v1, " mTofGestureRegistered:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureRegistered:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 578 */
} // :cond_1
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mAonGestureSupport:"; // const-string v1, " mAonGestureSupport:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mAonGestureSupport:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 579 */
/* iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureSupport:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 580 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mAonGestureEnabled:"; // const-string v1, " mAonGestureEnabled:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mAonGestureEnabled:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 583 */
} // :cond_2
v0 = this.mContactlessGestureController;
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 584 */
(( com.android.server.tof.ContactlessGestureController ) v0 ).dump ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/tof/ContactlessGestureController;->dump(Ljava/io/PrintWriter;)V
/* .line 586 */
} // :cond_3
return;
} // .end method
private void handleDeskTopModeChanged ( ) {
/* .locals 2 */
/* .line 388 */
v0 = this.mContactlessGestureController;
/* iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mDeskTopModeEnabled:Z */
(( com.android.server.tof.ContactlessGestureController ) v0 ).updateDeskTopMode ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/tof/ContactlessGestureController;->updateDeskTopMode(Z)V
/* .line 389 */
return;
} // .end method
private void handleGestureGuideDismiss ( ) {
/* .locals 5 */
/* .line 482 */
v0 = this.mContext;
/* .line 483 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 482 */
final String v1 = "gesture_guide_dismiss"; // const-string v1, "gesture_guide_dismiss"
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.Settings$Global .getInt ( v0,v1,v2 );
int v3 = 1; // const/4 v3, 0x1
/* if-ne v0, v3, :cond_0 */
} // :cond_0
/* move v3, v2 */
} // :goto_0
/* move v0, v3 */
/* .line 484 */
/* .local v0, "guideDismiss":Z */
/* iget-boolean v3, p0, Lcom/android/server/tof/ContactlessGestureService;->mAonGestureSupport:Z */
if ( v3 != null) { // if-eqz v3, :cond_1
/* iget-boolean v3, p0, Lcom/android/server/tof/ContactlessGestureService;->mAonGestureEnabled:Z */
if ( v3 != null) { // if-eqz v3, :cond_1
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 485 */
v3 = com.android.server.tof.ContactlessGestureService.TAG;
/* const-string/jumbo v4, "the guide dismiss,register aon listener anew." */
android.util.Slog .i ( v3,v4 );
/* .line 486 */
v3 = this.mContactlessGestureController;
(( com.android.server.tof.ContactlessGestureController ) v3 ).registerGestureListenerIfNeeded ( v2 ); // invoke-virtual {v3, v2}, Lcom/android/server/tof/ContactlessGestureController;->registerGestureListenerIfNeeded(Z)V
/* .line 487 */
v3 = this.mContactlessGestureController;
int v4 = 6; // const/4 v4, 0x6
(( com.android.server.tof.ContactlessGestureController ) v3 ).onSceneChanged ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/tof/ContactlessGestureController;->onSceneChanged(I)V
/* .line 488 */
v3 = this.mContext;
(( android.content.Context ) v3 ).getContentResolver ( ); // invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$Global .putInt ( v3,v1,v2 );
/* .line 490 */
} // :cond_1
return;
} // .end method
private void handleGestureSensorChanged ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "label" # I */
/* .line 342 */
v0 = this.mContactlessGestureController;
v0 = (( com.android.server.tof.ContactlessGestureController ) v0 ).isServiceInit ( ); // invoke-virtual {v0}, Lcom/android/server/tof/ContactlessGestureController;->isServiceInit()Z
/* if-nez v0, :cond_0 */
/* .line 343 */
v0 = this.mContactlessGestureController;
(( com.android.server.tof.ContactlessGestureController ) v0 ).bindService ( ); // invoke-virtual {v0}, Lcom/android/server/tof/ContactlessGestureController;->bindService()V
/* .line 345 */
} // :cond_0
v0 = this.mContactlessGestureController;
(( com.android.server.tof.ContactlessGestureController ) v0 ).handleGestureEvent ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/tof/ContactlessGestureController;->handleGestureEvent(I)V
/* .line 346 */
return;
} // .end method
private void handleOneHandedModeEnableChanged ( ) {
/* .locals 4 */
/* .line 492 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v1 = -2; // const/4 v1, -0x2
final String v2 = "one_handed_mode_activated"; // const-string v2, "one_handed_mode_activated"
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v2,v3,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
int v3 = 1; // const/4 v3, 0x1
} // :cond_0
/* iput-boolean v3, p0, Lcom/android/server/tof/ContactlessGestureService;->mOneHandedModeEnable:Z */
/* .line 494 */
/* iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mAonGestureSupport:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mAonGestureEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 495 */
v0 = this.mContactlessGestureController;
int v1 = 6; // const/4 v1, 0x6
(( com.android.server.tof.ContactlessGestureController ) v0 ).onSceneChanged ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/tof/ContactlessGestureController;->onSceneChanged(I)V
/* .line 497 */
} // :cond_1
return;
} // .end method
private void handleScreenCastModeChanged ( ) {
/* .locals 2 */
/* .line 392 */
/* iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureSupport:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 393 */
	 v0 = this.mContactlessGestureController;
	 int v1 = 6; // const/4 v1, 0x6
	 (( com.android.server.tof.ContactlessGestureController ) v0 ).onSceneChanged ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/tof/ContactlessGestureController;->onSceneChanged(I)V
	 /* .line 395 */
} // :cond_0
return;
} // .end method
private void handleSettingsChanged ( android.net.Uri p0 ) {
/* .locals 2 */
/* .param p1, "uri" # Landroid/net/Uri; */
/* .line 440 */
(( android.net.Uri ) p1 ).getLastPathSegment ( ); // invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;
v1 = (( java.lang.String ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/String;->hashCode()I
/* sparse-switch v1, :sswitch_data_0 */
} // :cond_0
/* goto/16 :goto_0 */
/* :sswitch_0 */
/* const-string/jumbo v1, "screen_project_in_screening" */
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 4; // const/4 v0, 0x4
/* :sswitch_1 */
final String v1 = "miui_dkt_mode"; // const-string v1, "miui_dkt_mode"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* const/16 v0, 0x8 */
	 /* :sswitch_2 */
	 final String v1 = "miui_aon_gesture"; // const-string v1, "miui_aon_gesture"
	 v0 = 	 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 int v0 = 3; // const/4 v0, 0x3
		 /* :sswitch_3 */
		 final String v1 = "one_handed_mode_activated"; // const-string v1, "one_handed_mode_activated"
		 v0 = 		 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 int v0 = 7; // const/4 v0, 0x7
			 /* :sswitch_4 */
			 final String v1 = "miui_tof_screen_on"; // const-string v1, "miui_tof_screen_on"
			 v0 = 			 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
			 if ( v0 != null) { // if-eqz v0, :cond_0
				 int v0 = 0; // const/4 v0, 0x0
				 /* :sswitch_5 */
				 final String v1 = "miui_tof_screen_off"; // const-string v1, "miui_tof_screen_off"
				 v0 = 				 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
				 if ( v0 != null) { // if-eqz v0, :cond_0
					 int v0 = 1; // const/4 v0, 0x1
					 /* :sswitch_6 */
					 final String v1 = "gesture_guide_dismiss"; // const-string v1, "gesture_guide_dismiss"
					 v0 = 					 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
					 if ( v0 != null) { // if-eqz v0, :cond_0
						 /* const/16 v0, 0x9 */
						 /* :sswitch_7 */
						 final String v1 = "miui_tof_gesture"; // const-string v1, "miui_tof_gesture"
						 v0 = 						 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
						 if ( v0 != null) { // if-eqz v0, :cond_0
							 int v0 = 2; // const/4 v0, 0x2
							 /* :sswitch_8 */
							 /* const-string/jumbo v1, "synergy_mode" */
							 v0 = 							 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
							 if ( v0 != null) { // if-eqz v0, :cond_0
								 int v0 = 5; // const/4 v0, 0x5
								 /* :sswitch_9 */
								 final String v1 = "external_display_connected"; // const-string v1, "external_display_connected"
								 v0 = 								 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
								 if ( v0 != null) { // if-eqz v0, :cond_0
									 int v0 = 6; // const/4 v0, 0x6
								 } // :goto_0
								 int v0 = -1; // const/4 v0, -0x1
							 } // :goto_1
							 /* packed-switch v0, :pswitch_data_0 */
							 /* .line 470 */
							 /* :pswitch_0 */
							 /* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->handleGestureGuideDismiss()V */
							 /* .line 471 */
							 /* .line 467 */
							 /* :pswitch_1 */
							 /* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->updateDeskTopModeConfig()V */
							 /* .line 468 */
							 /* .line 464 */
							 /* :pswitch_2 */
							 /* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->handleOneHandedModeEnableChanged()V */
							 /* .line 465 */
							 /* .line 458 */
							 /* :pswitch_3 */
							 /* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->updateScreenProjectConfig()V */
							 /* .line 459 */
							 /* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->updateSynergyModeConfig()V */
							 /* .line 460 */
							 /* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->updateExternalDisplayConnectConfig()V */
							 /* .line 461 */
							 /* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->handleScreenCastModeChanged()V */
							 /* .line 462 */
							 /* .line 452 */
							 /* :pswitch_4 */
							 /* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->updateAonGestureConfig()V */
							 /* .line 453 */
							 (( com.android.server.tof.ContactlessGestureService ) p0 ).startGestureClientIfNeeded ( ); // invoke-virtual {p0}, Lcom/android/server/tof/ContactlessGestureService;->startGestureClientIfNeeded()V
							 /* .line 454 */
							 /* .line 448 */
							 /* :pswitch_5 */
							 /* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->updateTofGestureConfig()V */
							 /* .line 449 */
							 (( com.android.server.tof.ContactlessGestureService ) p0 ).startGestureClientIfNeeded ( ); // invoke-virtual {p0}, Lcom/android/server/tof/ContactlessGestureService;->startGestureClientIfNeeded()V
							 /* .line 450 */
							 /* .line 443 */
							 /* :pswitch_6 */
							 /* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->updateTofScreenOnConfig()V */
							 /* .line 444 */
							 /* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->updateTofScreenOffConfig()V */
							 /* .line 445 */
							 /* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->registerTofProximityListenerIfNeeded()V */
							 /* .line 446 */
							 /* nop */
							 /* .line 476 */
						 } // :goto_2
						 return;
						 /* :sswitch_data_0 */
						 /* .sparse-switch */
						 /* -0x688973a8 -> :sswitch_9 */
						 /* -0x55bbaa45 -> :sswitch_8 */
						 /* -0x320b7a3a -> :sswitch_7 */
						 /* -0x17e86c2f -> :sswitch_6 */
						 /* -0x17a1ce81 -> :sswitch_5 */
						 /* -0x9053831 -> :sswitch_4 */
						 /* 0x2730082d -> :sswitch_3 */
						 /* 0x631eb97b -> :sswitch_2 */
						 /* 0x75414804 -> :sswitch_1 */
						 /* 0x790152b5 -> :sswitch_0 */
					 } // .end sparse-switch
					 /* :pswitch_data_0 */
					 /* .packed-switch 0x0 */
					 /* :pswitch_6 */
					 /* :pswitch_6 */
					 /* :pswitch_5 */
					 /* :pswitch_4 */
					 /* :pswitch_3 */
					 /* :pswitch_3 */
					 /* :pswitch_3 */
					 /* :pswitch_2 */
					 /* :pswitch_1 */
					 /* :pswitch_0 */
				 } // .end packed-switch
			 } // .end method
			 private void handleTofProximitySensorChanged ( Boolean p0 ) {
				 /* .locals 12 */
				 /* .param p1, "proximity" # Z */
				 /* .line 359 */
				 /* iput-boolean p1, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofProximity:Z */
				 /* .line 360 */
				 v0 = 				 (( com.android.server.tof.ContactlessGestureService ) p0 ).isScreenCastMode ( ); // invoke-virtual {p0}, Lcom/android/server/tof/ContactlessGestureService;->isScreenCastMode()Z
				 /* .line 361 */
				 /* .local v0, "hangupMode":Z */
				 int v1 = 0; // const/4 v1, 0x0
				 /* .line 362 */
				 /* .local v1, "screenWakeLock":Z */
				 /* const-wide/16 v2, 0x0 */
				 /* .line 363 */
				 /* .local v2, "lastUserActivityTime":J */
				 if ( v0 != null) { // if-eqz v0, :cond_0
					 /* .line 364 */
					 v4 = com.android.server.tof.ContactlessGestureService.TAG;
					 final String v5 = "ignore Tof sensor in hangup mode"; // const-string v5, "ignore Tof sensor in hangup mode"
					 android.util.Slog .i ( v4,v5 );
					 /* .line 365 */
					 return;
					 /* .line 368 */
				 } // :cond_0
				 /* iget-boolean v4, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofProximity:Z */
				 int v5 = 0; // const/4 v5, 0x0
				 if ( v4 != null) { // if-eqz v4, :cond_1
					 /* .line 369 */
					 v4 = this.mPowerManager;
					 android.os.SystemClock .uptimeMillis ( );
					 /* move-result-wide v6 */
					 /* const-string/jumbo v8, "tof" */
					 (( android.os.PowerManager ) v4 ).wakeUp ( v6, v7, v5, v8 ); // invoke-virtual {v4, v6, v7, v5, v8}, Landroid/os/PowerManager;->wakeUp(JILjava/lang/String;)V
					 /* .line 371 */
					 v4 = this.mPowerManagerServiceImpl;
					 int v5 = 1; // const/4 v5, 0x1
					 (( com.android.server.power.PowerManagerServiceStub ) v4 ).notifyTofPowerState ( v5 ); // invoke-virtual {v4, v5}, Lcom/android/server/power/PowerManagerServiceStub;->notifyTofPowerState(Z)V
					 /* .line 373 */
				 } // :cond_1
				 v4 = this.mPowerManagerServiceImpl;
				 v1 = 				 (( com.android.server.power.PowerManagerServiceStub ) v4 ).isAcquiredScreenBrightWakeLock ( v5 ); // invoke-virtual {v4, v5}, Lcom/android/server/power/PowerManagerServiceStub;->isAcquiredScreenBrightWakeLock(I)Z
				 /* .line 374 */
				 v4 = this.mPowerManagerServiceImpl;
				 (( com.android.server.power.PowerManagerServiceStub ) v4 ).getDefaultDisplayLastUserActivity ( ); // invoke-virtual {v4}, Lcom/android/server/power/PowerManagerServiceStub;->getDefaultDisplayLastUserActivity()J
				 /* move-result-wide v2 */
				 /* .line 375 */
				 android.os.SystemClock .uptimeMillis ( );
				 /* move-result-wide v6 */
				 /* .line 376 */
				 /* .local v6, "time":J */
				 /* if-nez v1, :cond_2 */
				 /* sub-long v8, v6, v2 */
				 /* const-wide/16 v10, 0x1388 */
				 /* cmp-long v4, v8, v10 */
				 /* if-lez v4, :cond_2 */
				 /* .line 378 */
				 v4 = this.mPowerManager;
				 (( android.os.PowerManager ) v4 ).goToSleep ( v6, v7, v5, v5 ); // invoke-virtual {v4, v6, v7, v5, v5}, Landroid/os/PowerManager;->goToSleep(JII)V
				 /* .line 379 */
				 v4 = this.mPowerManagerServiceImpl;
				 (( com.android.server.power.PowerManagerServiceStub ) v4 ).notifyTofPowerState ( v5 ); // invoke-virtual {v4, v5}, Lcom/android/server/power/PowerManagerServiceStub;->notifyTofPowerState(Z)V
				 /* .line 382 */
			 } // .end local v6 # "time":J
		 } // :cond_2
	 } // :goto_0
	 v4 = com.android.server.tof.ContactlessGestureService.TAG;
	 /* iget-boolean v5, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofProximity:Z */
	 if ( v5 != null) { // if-eqz v5, :cond_3
		 /* const-string/jumbo v5, "wake up" */
		 /* .line 384 */
	 } // :cond_3
	 /* new-instance v5, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v6 = "go to sleep, screenWakeLock: "; // const-string v6, "go to sleep, screenWakeLock: "
	 (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v5 ).append ( v1 ); // invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
	 final String v6 = ", lastUserActivity: "; // const-string v6, ", lastUserActivity: "
	 (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 android.util.TimeUtils .formatUptime ( v2,v3 );
	 (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 /* .line 382 */
} // :goto_1
android.util.Slog .i ( v4,v5 );
/* .line 385 */
return;
} // .end method
private Boolean isAonGestureSupport ( ) {
/* .locals 1 */
/* .line 398 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
private void registerContentObserver ( ) {
/* .locals 5 */
/* .line 187 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 188 */
/* .local v0, "cr":Landroid/content/ContentResolver; */
final String v1 = "miui_tof_screen_on"; // const-string v1, "miui_tof_screen_on"
android.provider.Settings$Secure .getUriFor ( v1 );
v2 = this.mSettingsObserver;
int v3 = 0; // const/4 v3, 0x0
int v4 = -1; // const/4 v4, -0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 191 */
final String v1 = "miui_tof_screen_off"; // const-string v1, "miui_tof_screen_off"
android.provider.Settings$Secure .getUriFor ( v1 );
v2 = this.mSettingsObserver;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 194 */
final String v1 = "miui_dkt_mode"; // const-string v1, "miui_dkt_mode"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mSettingsObserver;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 196 */
final String v1 = "miui_tof_gesture"; // const-string v1, "miui_tof_gesture"
android.provider.Settings$Secure .getUriFor ( v1 );
v2 = this.mSettingsObserver;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 199 */
final String v1 = "miui_aon_gesture"; // const-string v1, "miui_aon_gesture"
android.provider.Settings$Secure .getUriFor ( v1 );
v2 = this.mSettingsObserver;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 202 */
/* const-string/jumbo v1, "screen_project_in_screening" */
android.provider.Settings$Secure .getUriFor ( v1 );
v2 = this.mSettingsObserver;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 205 */
final String v1 = "one_handed_mode_activated"; // const-string v1, "one_handed_mode_activated"
android.provider.Settings$Secure .getUriFor ( v1 );
v2 = this.mSettingsObserver;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 208 */
/* const-string/jumbo v1, "synergy_mode" */
android.provider.Settings$Secure .getUriFor ( v1 );
v2 = this.mSettingsObserver;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 212 */
final String v1 = "external_display_connected"; // const-string v1, "external_display_connected"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mSettingsObserver;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 215 */
final String v1 = "gesture_guide_dismiss"; // const-string v1, "gesture_guide_dismiss"
android.provider.Settings$Global .getUriFor ( v1 );
v2 = this.mSettingsObserver;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 217 */
return;
} // .end method
private void registerPersonLeaveListenerIfNeeded ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "register" # Z */
/* .line 236 */
v0 = this.mSensorManager;
/* if-nez v0, :cond_0 */
/* .line 237 */
return;
/* .line 240 */
} // :cond_0
if ( p1 != null) { // if-eqz p1, :cond_1
/* iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofPersonLeaveRegistered:Z */
/* if-nez v1, :cond_1 */
/* .line 241 */
v1 = this.mTofSensorListener;
v2 = this.mTofPersonLeaveSensor;
int v3 = 3; // const/4 v3, 0x3
v0 = (( android.hardware.SensorManager ) v0 ).registerListener ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z
/* iput-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofPersonLeaveRegistered:Z */
/* .line 243 */
v0 = com.android.server.tof.ContactlessGestureService.TAG;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "registerPersonLeaveListener: "; // const-string v2, "registerPersonLeaveListener: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofPersonLeaveRegistered:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v1 );
/* .line 244 */
} // :cond_1
/* if-nez p1, :cond_2 */
/* iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofPersonLeaveRegistered:Z */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 245 */
v1 = this.mTofSensorListener;
v2 = this.mTofPersonLeaveSensor;
(( android.hardware.SensorManager ) v0 ).unregisterListener ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V
/* .line 246 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofPersonLeaveRegistered:Z */
/* .line 248 */
} // :cond_2
} // :goto_0
return;
} // .end method
private void registerPersonNearListenerIfNeeded ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "register" # Z */
/* .line 257 */
v0 = this.mSensorManager;
/* if-nez v0, :cond_0 */
/* .line 258 */
return;
/* .line 261 */
} // :cond_0
if ( p1 != null) { // if-eqz p1, :cond_1
/* iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofPersonNearRegistered:Z */
/* if-nez v1, :cond_1 */
/* .line 262 */
v1 = this.mTofSensorListener;
v2 = this.mTofPersonNearSensor;
int v3 = 3; // const/4 v3, 0x3
v0 = (( android.hardware.SensorManager ) v0 ).registerListener ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z
/* iput-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofPersonNearRegistered:Z */
/* .line 264 */
v0 = com.android.server.tof.ContactlessGestureService.TAG;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "registerPersonNearListener: "; // const-string v2, "registerPersonNearListener: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofPersonNearRegistered:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v1 );
/* .line 265 */
} // :cond_1
/* if-nez p1, :cond_2 */
/* iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofPersonNearRegistered:Z */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 266 */
v1 = this.mTofSensorListener;
v2 = this.mTofPersonNearSensor;
(( android.hardware.SensorManager ) v0 ).unregisterListener ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V
/* .line 267 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofPersonNearRegistered:Z */
/* .line 269 */
} // :cond_2
} // :goto_0
return;
} // .end method
private void registerTofGestureListenerIfNeeded ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "register" # Z */
/* .line 291 */
v0 = this.mSensorManager;
/* if-nez v0, :cond_0 */
/* .line 292 */
v0 = com.android.server.tof.ContactlessGestureService.TAG;
final String v1 = "SensorManager is not ready, reject register tof sensor."; // const-string v1, "SensorManager is not ready, reject register tof sensor."
android.util.Slog .e ( v0,v1 );
/* .line 293 */
return;
/* .line 296 */
} // :cond_0
if ( p1 != null) { // if-eqz p1, :cond_1
/* iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureRegistered:Z */
/* if-nez v1, :cond_1 */
/* .line 297 */
v1 = this.mTofSensorListener;
v2 = this.mTofGestureSensor;
int v3 = 3; // const/4 v3, 0x3
v0 = (( android.hardware.SensorManager ) v0 ).registerListener ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z
/* iput-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureRegistered:Z */
/* .line 299 */
} // :cond_1
/* if-nez p1, :cond_2 */
/* iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureRegistered:Z */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 300 */
v1 = this.mTofSensorListener;
v2 = this.mTofGestureSensor;
(( android.hardware.SensorManager ) v0 ).unregisterListener ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V
/* .line 301 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureRegistered:Z */
/* .line 303 */
} // :cond_2
} // :goto_0
v0 = com.android.server.tof.ContactlessGestureService.TAG;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "registerTofGestureListener: "; // const-string v2, "registerTofGestureListener: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = ",mTofGestureRegistered:"; // const-string v2, ",mTofGestureRegistered:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureRegistered:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v1 );
/* .line 305 */
return;
} // .end method
private void registerTofProximityListenerIfNeeded ( ) {
/* .locals 3 */
/* .line 223 */
/* iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofProximitySupport:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 224 */
/* iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mIsInteractive:Z */
int v1 = 1; // const/4 v1, 0x1
int v2 = 0; // const/4 v2, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofScreenOffEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* move v0, v1 */
} // :cond_0
/* move v0, v2 */
} // :goto_0
/* invoke-direct {p0, v0}, Lcom/android/server/tof/ContactlessGestureService;->registerPersonLeaveListenerIfNeeded(Z)V */
/* .line 225 */
/* iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mIsInteractive:Z */
/* if-nez v0, :cond_1 */
/* iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofScreenOnEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
} // :cond_1
/* move v1, v2 */
} // :goto_1
/* invoke-direct {p0, v1}, Lcom/android/server/tof/ContactlessGestureService;->registerPersonNearListenerIfNeeded(Z)V */
/* .line 227 */
} // :cond_2
return;
} // .end method
private Integer setDebug ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "enable" # Z */
/* .line 435 */
com.android.server.tof.ContactlessGestureService.mDebug = (p1!= 0);
/* .line 436 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
private void systemReady ( ) {
/* .locals 3 */
/* .line 152 */
v0 = this.mContext;
final String v1 = "power"; // const-string v1, "power"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/os/PowerManager; */
this.mPowerManager = v0;
/* .line 153 */
com.android.server.power.PowerManagerServiceStub .get ( );
this.mPowerManagerServiceImpl = v0;
/* .line 154 */
v0 = this.mContext;
/* const-string/jumbo v1, "sensor" */
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/hardware/SensorManager; */
this.mSensorManager = v0;
/* .line 155 */
/* const v1, 0x1fa2703 */
int v2 = 1; // const/4 v2, 0x1
(( android.hardware.SensorManager ) v0 ).getDefaultSensor ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(IZ)Landroid/hardware/Sensor;
this.mTofPersonNearSensor = v0;
/* .line 156 */
v0 = this.mSensorManager;
/* const v1, 0x1fa2704 */
(( android.hardware.SensorManager ) v0 ).getDefaultSensor ( v1 ); // invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;
this.mTofPersonLeaveSensor = v0;
/* .line 157 */
v0 = this.mSensorManager;
/* const v1, 0x1fa2700 */
(( android.hardware.SensorManager ) v0 ).getDefaultSensor ( v1 ); // invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;
this.mTofGestureSensor = v0;
/* .line 158 */
v0 = this.mContext;
/* .line 159 */
final String v1 = "accessibility"; // const-string v1, "accessibility"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/view/accessibility/AccessibilityManager; */
this.mAccessibilityManager = v0;
/* .line 160 */
/* iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mAonGestureSupport:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 161 */
/* new-instance v0, Lcom/android/server/tof/AONGestureController; */
v1 = this.mContext;
v2 = this.mHandler;
/* invoke-direct {v0, v1, v2, p0}, Lcom/android/server/tof/AONGestureController;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/tof/ContactlessGestureService;)V */
this.mContactlessGestureController = v0;
/* .line 163 */
} // :cond_0
/* new-instance v0, Lcom/android/server/tof/ToFGestureController; */
v1 = this.mContext;
v2 = this.mHandler;
/* invoke-direct {v0, v1, v2, p0}, Lcom/android/server/tof/ToFGestureController;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/tof/ContactlessGestureService;)V */
this.mContactlessGestureController = v0;
/* .line 165 */
} // :goto_0
v0 = this.mContactlessGestureController;
v1 = this.mComponentFeatureList;
(( com.android.server.tof.ContactlessGestureController ) v0 ).parseGestureComponentFromCloud ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/tof/ContactlessGestureController;->parseGestureComponentFromCloud(Ljava/util/List;)V
/* .line 166 */
/* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->updateConfigState()V */
/* .line 167 */
/* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->registerContentObserver()V */
/* .line 168 */
return;
} // .end method
private void updateAonGestureConfig ( ) {
/* .locals 4 */
/* .line 537 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v1 = -2; // const/4 v1, -0x2
final String v2 = "miui_aon_gesture"; // const-string v2, "miui_aon_gesture"
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v2,v3,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
int v3 = 1; // const/4 v3, 0x1
} // :cond_0
/* iput-boolean v3, p0, Lcom/android/server/tof/ContactlessGestureService;->mAonGestureEnabled:Z */
/* .line 539 */
return;
} // .end method
private void updateConfigState ( ) {
/* .locals 0 */
/* .line 171 */
/* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->updateTofScreenOnConfig()V */
/* .line 172 */
/* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->updateTofScreenOffConfig()V */
/* .line 173 */
/* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->updateTofGestureConfig()V */
/* .line 174 */
/* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->updateAonGestureConfig()V */
/* .line 175 */
/* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->updateDeskTopModeConfig()V */
/* .line 176 */
/* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->updateScreenProjectConfig()V */
/* .line 177 */
/* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->updateSynergyModeConfig()V */
/* .line 178 */
/* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->updateExternalDisplayConnectConfig()V */
/* .line 179 */
/* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->handleOneHandedModeEnableChanged()V */
/* .line 180 */
/* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->registerTofProximityListenerIfNeeded()V */
/* .line 181 */
return;
} // .end method
private void updateDeskTopModeConfig ( ) {
/* .locals 4 */
/* .line 516 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v1 = -2; // const/4 v1, -0x2
final String v2 = "miui_dkt_mode"; // const-string v2, "miui_dkt_mode"
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$System .getIntForUser ( v0,v2,v3,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
int v3 = 1; // const/4 v3, 0x1
} // :cond_0
/* iput-boolean v3, p0, Lcom/android/server/tof/ContactlessGestureService;->mDeskTopModeEnabled:Z */
/* .line 518 */
/* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->handleDeskTopModeChanged()V */
/* .line 519 */
return;
} // .end method
private void updateExternalDisplayConnectConfig ( ) {
/* .locals 4 */
/* .line 510 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v1 = -2; // const/4 v1, -0x2
final String v2 = "external_display_connected"; // const-string v2, "external_display_connected"
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$System .getIntForUser ( v0,v2,v3,v1 );
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* move v3, v1 */
} // :cond_0
/* iput-boolean v3, p0, Lcom/android/server/tof/ContactlessGestureService;->mExternalDisplayConnected:Z */
/* .line 513 */
return;
} // .end method
private void updateScreenProjectConfig ( ) {
/* .locals 4 */
/* .line 500 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v1 = -2; // const/4 v1, -0x2
/* const-string/jumbo v2, "screen_project_in_screening" */
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v2,v3,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
int v3 = 1; // const/4 v3, 0x1
} // :cond_0
/* iput-boolean v3, p0, Lcom/android/server/tof/ContactlessGestureService;->mScreenProjectInScreen:Z */
/* .line 502 */
return;
} // .end method
private void updateSynergyModeConfig ( ) {
/* .locals 4 */
/* .line 505 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v1 = -2; // const/4 v1, -0x2
/* const-string/jumbo v2, "synergy_mode" */
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v2,v3,v1 );
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* move v3, v1 */
} // :cond_0
/* iput-boolean v3, p0, Lcom/android/server/tof/ContactlessGestureService;->mSynergyModeEnable:Z */
/* .line 507 */
return;
} // .end method
private void updateTofGestureConfig ( ) {
/* .locals 4 */
/* .line 532 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v1 = -2; // const/4 v1, -0x2
final String v2 = "miui_tof_gesture"; // const-string v2, "miui_tof_gesture"
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v2,v3,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
int v3 = 1; // const/4 v3, 0x1
} // :cond_0
/* iput-boolean v3, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureEnabled:Z */
/* .line 534 */
return;
} // .end method
private void updateTofScreenOffConfig ( ) {
/* .locals 4 */
/* .line 527 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v1 = -2; // const/4 v1, -0x2
final String v2 = "miui_tof_screen_off"; // const-string v2, "miui_tof_screen_off"
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v2,v3,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
int v3 = 1; // const/4 v3, 0x1
} // :cond_0
/* iput-boolean v3, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofScreenOffEnabled:Z */
/* .line 529 */
return;
} // .end method
private void updateTofScreenOnConfig ( ) {
/* .locals 4 */
/* .line 522 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v1 = -2; // const/4 v1, -0x2
final String v2 = "miui_tof_screen_on"; // const-string v2, "miui_tof_screen_on"
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v2,v3,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
int v3 = 1; // const/4 v3, 0x1
} // :cond_0
/* iput-boolean v3, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofScreenOnEnabled:Z */
/* .line 524 */
return;
} // .end method
private void updateTofServiceIfNeeded ( ) {
/* .locals 1 */
/* .line 549 */
/* iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureSupport:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mContactlessGestureController;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 550 */
v0 = (( com.android.server.tof.ContactlessGestureController ) v0 ).isServiceInit ( ); // invoke-virtual {v0}, Lcom/android/server/tof/ContactlessGestureController;->isServiceInit()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 551 */
v0 = this.mContactlessGestureController;
(( com.android.server.tof.ContactlessGestureController ) v0 ).unBindService ( ); // invoke-virtual {v0}, Lcom/android/server/tof/ContactlessGestureController;->unBindService()V
/* .line 553 */
} // :cond_0
return;
} // .end method
/* # virtual methods */
public Boolean isOneHandedModeEnabled ( ) {
/* .locals 1 */
/* .line 431 */
/* iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mOneHandedModeEnable:Z */
} // .end method
public Boolean isScreenCastMode ( ) {
/* .locals 1 */
/* .line 419 */
/* iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mScreenProjectInScreen:Z */
/* if-nez v0, :cond_1 */
/* iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mSynergyModeEnable:Z */
/* if-nez v0, :cond_1 */
/* iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mExternalDisplayConnected:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
public Boolean isSupportTofGestureFeature ( ) {
/* .locals 1 */
/* .line 415 */
/* iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureSupport:Z */
} // .end method
public Boolean isSupportTofPersonProximityFeature ( ) {
/* .locals 1 */
/* .line 411 */
/* iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofProximitySupport:Z */
} // .end method
public Boolean isTalkBackEnabled ( ) {
/* .locals 2 */
/* .line 423 */
v0 = this.mAccessibilityManager;
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 424 */
/* .line 426 */
} // :cond_0
v0 = (( android.view.accessibility.AccessibilityManager ) v0 ).isEnabled ( ); // invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.mAccessibilityManager;
/* .line 427 */
v0 = (( android.view.accessibility.AccessibilityManager ) v0 ).isTouchExplorationEnabled ( ); // invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z
if ( v0 != null) { // if-eqz v0, :cond_1
int v1 = 1; // const/4 v1, 0x1
} // :cond_1
/* nop */
/* .line 426 */
} // :goto_0
} // .end method
public void onBootPhase ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "phase" # I */
/* .line 138 */
/* const/16 v0, 0x3e8 */
/* if-ne p1, v0, :cond_0 */
/* .line 139 */
/* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->systemReady()V */
/* .line 141 */
} // :cond_0
return;
} // .end method
public void onStart ( ) {
/* .locals 3 */
/* .line 131 */
/* new-instance v0, Lcom/android/server/tof/ContactlessGestureService$BinderService; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, p0, v1}, Lcom/android/server/tof/ContactlessGestureService$BinderService;-><init>(Lcom/android/server/tof/ContactlessGestureService;Lcom/android/server/tof/ContactlessGestureService$BinderService-IA;)V */
/* const-string/jumbo v2, "tof" */
(( com.android.server.tof.ContactlessGestureService ) p0 ).publishBinderService ( v2, v0 ); // invoke-virtual {p0, v2, v0}, Lcom/android/server/tof/ContactlessGestureService;->publishBinderService(Ljava/lang/String;Landroid/os/IBinder;)V
/* .line 132 */
v0 = com.android.server.tof.ContactlessGestureService.TAG;
final String v2 = "publish contactless gesture Service"; // const-string v2, "publish contactless gesture Service"
android.util.Slog .i ( v0,v2 );
/* .line 133 */
/* const-class v0, Lcom/android/server/tof/TofManagerInternal; */
/* new-instance v2, Lcom/android/server/tof/ContactlessGestureService$LocalService; */
/* invoke-direct {v2, p0, v1}, Lcom/android/server/tof/ContactlessGestureService$LocalService;-><init>(Lcom/android/server/tof/ContactlessGestureService;Lcom/android/server/tof/ContactlessGestureService$LocalService-IA;)V */
(( com.android.server.tof.ContactlessGestureService ) p0 ).publishLocalService ( v0, v2 ); // invoke-virtual {p0, v0, v2}, Lcom/android/server/tof/ContactlessGestureService;->publishLocalService(Ljava/lang/Class;Ljava/lang/Object;)V
/* .line 134 */
return;
} // .end method
public void onUserSwitching ( com.android.server.SystemService$TargetUser p0, com.android.server.SystemService$TargetUser p1 ) {
/* .locals 4 */
/* .param p1, "from" # Lcom/android/server/SystemService$TargetUser; */
/* .param p2, "to" # Lcom/android/server/SystemService$TargetUser; */
/* .line 145 */
v0 = com.android.server.tof.ContactlessGestureService.TAG;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "from user:"; // const-string v2, "from user:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.SystemService$TargetUser ) p1 ).getUserHandle ( ); // invoke-virtual {p1}, Lcom/android/server/SystemService$TargetUser;->getUserHandle()Landroid/os/UserHandle;
(( android.os.UserHandle ) v2 ).toString ( ); // invoke-virtual {v2}, Landroid/os/UserHandle;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " getUserIdentifier:"; // const-string v2, " getUserIdentifier:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = (( com.android.server.SystemService$TargetUser ) p1 ).getUserIdentifier ( ); // invoke-virtual {p1}, Lcom/android/server/SystemService$TargetUser;->getUserIdentifier()I
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ", to:"; // const-string v3, ", to:"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 146 */
(( com.android.server.SystemService$TargetUser ) p2 ).getUserHandle ( ); // invoke-virtual {p2}, Lcom/android/server/SystemService$TargetUser;->getUserHandle()Landroid/os/UserHandle;
(( android.os.UserHandle ) v3 ).toString ( ); // invoke-virtual {v3}, Landroid/os/UserHandle;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = (( com.android.server.SystemService$TargetUser ) p2 ).getUserIdentifier ( ); // invoke-virtual {p2}, Lcom/android/server/SystemService$TargetUser;->getUserIdentifier()I
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 145 */
android.util.Slog .i ( v0,v1 );
/* .line 147 */
/* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->updateConfigState()V */
/* .line 148 */
/* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->updateTofServiceIfNeeded()V */
/* .line 149 */
return;
} // .end method
public void registerContactlessGestureListenerIfNeeded ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "register" # Z */
/* .line 278 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 279 */
(( com.android.server.tof.ContactlessGestureService ) p0 ).startGestureClientIfNeeded ( ); // invoke-virtual {p0}, Lcom/android/server/tof/ContactlessGestureService;->startGestureClientIfNeeded()V
/* .line 280 */
(( com.android.server.tof.ContactlessGestureService ) p0 ).showTofGestureNotificationIfNeeded ( ); // invoke-virtual {p0}, Lcom/android/server/tof/ContactlessGestureService;->showTofGestureNotificationIfNeeded()V
/* .line 282 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureSupport:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 283 */
/* invoke-direct {p0, p1}, Lcom/android/server/tof/ContactlessGestureService;->registerTofGestureListenerIfNeeded(Z)V */
/* .line 284 */
} // :cond_1
/* iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mAonGestureSupport:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mAonGestureEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 285 */
v0 = this.mContactlessGestureController;
(( com.android.server.tof.ContactlessGestureController ) v0 ).registerGestureListenerIfNeeded ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/tof/ContactlessGestureController;->registerGestureListenerIfNeeded(Z)V
/* .line 286 */
v0 = com.android.server.tof.ContactlessGestureService.TAG;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "registerGestureListener:"; // const-string v2, "registerGestureListener:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v1 );
/* .line 288 */
} // :cond_2
} // :goto_0
return;
} // .end method
public void showTofGestureNotificationIfNeeded ( ) {
/* .locals 1 */
/* .line 542 */
/* iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureSupport:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mAonGestureSupport:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mAonGestureEnabled:Z */
/* if-nez v0, :cond_2 */
/* .line 544 */
} // :cond_1
v0 = this.mContactlessGestureController;
(( com.android.server.tof.ContactlessGestureController ) v0 ).showGestureNotification ( ); // invoke-virtual {v0}, Lcom/android/server/tof/ContactlessGestureController;->showGestureNotification()V
/* .line 546 */
} // :cond_2
return;
} // .end method
public void startGestureClientIfNeeded ( ) {
/* .locals 2 */
/* .line 556 */
/* iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureSupport:Z */
/* if-nez v0, :cond_0 */
/* iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mAonGestureSupport:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
} // :cond_0
v0 = this.mContactlessGestureController;
/* .line 557 */
v0 = (( com.android.server.tof.ContactlessGestureController ) v0 ).isServiceInit ( ); // invoke-virtual {v0}, Lcom/android/server/tof/ContactlessGestureController;->isServiceInit()Z
/* if-nez v0, :cond_1 */
/* .line 558 */
v0 = com.android.server.tof.ContactlessGestureService.TAG;
/* const-string/jumbo v1, "start bind contactless gesture client." */
android.util.Slog .i ( v0,v1 );
/* .line 559 */
v0 = this.mContactlessGestureController;
(( com.android.server.tof.ContactlessGestureController ) v0 ).bindService ( ); // invoke-virtual {v0}, Lcom/android/server/tof/ContactlessGestureController;->bindService()V
/* .line 561 */
} // :cond_1
return;
} // .end method
