.class Lcom/android/server/tof/ContactlessGestureController$2;
.super Ljava/lang/Object;
.source "ContactlessGestureController.java"

# interfaces
.implements Landroid/hardware/display/DisplayManager$DisplayListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/tof/ContactlessGestureController;->registerDisplayListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/tof/ContactlessGestureController;


# direct methods
.method public static synthetic $r8$lambda$bFtpEDHLlXHJlGnmeXaIv1SREMc(Lcom/android/server/tof/ContactlessGestureController$2;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureController$2;->lambda$onDisplayChanged$0()V

    return-void
.end method

.method constructor <init>(Lcom/android/server/tof/ContactlessGestureController;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/tof/ContactlessGestureController;

    .line 886
    iput-object p1, p0, Lcom/android/server/tof/ContactlessGestureController$2;->this$0:Lcom/android/server/tof/ContactlessGestureController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private synthetic lambda$onDisplayChanged$0()V
    .locals 2

    .line 901
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController$2;->this$0:Lcom/android/server/tof/ContactlessGestureController;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/android/server/tof/ContactlessGestureController;->onSceneChanged(I)V

    return-void
.end method


# virtual methods
.method public onDisplayAdded(I)V
    .locals 0
    .param p1, "displayId"    # I

    .line 890
    return-void
.end method

.method public onDisplayChanged(I)V
    .locals 2
    .param p1, "displayId"    # I

    .line 899
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController$2;->this$0:Lcom/android/server/tof/ContactlessGestureController;

    invoke-static {v0}, Lcom/android/server/tof/ContactlessGestureController;->-$$Nest$mupdateDefaultDisplayRotation(Lcom/android/server/tof/ContactlessGestureController;)V

    .line 900
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController$2;->this$0:Lcom/android/server/tof/ContactlessGestureController;

    invoke-static {v0}, Lcom/android/server/tof/ContactlessGestureController;->-$$Nest$mcheckTopActivities(Lcom/android/server/tof/ContactlessGestureController;)V

    .line 901
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController$2;->this$0:Lcom/android/server/tof/ContactlessGestureController;

    iget-object v0, v0, Lcom/android/server/tof/ContactlessGestureController;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/tof/ContactlessGestureController$2$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/android/server/tof/ContactlessGestureController$2$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/tof/ContactlessGestureController$2;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 902
    return-void
.end method

.method public onDisplayRemoved(I)V
    .locals 0
    .param p1, "displayId"    # I

    .line 895
    return-void
.end method
