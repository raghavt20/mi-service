.class final Lcom/android/server/tof/ContactlessGestureController$AccessibilityTelephonyCallback;
.super Landroid/telephony/TelephonyCallback;
.source "ContactlessGestureController.java"

# interfaces
.implements Landroid/telephony/TelephonyCallback$CallStateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/tof/ContactlessGestureController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "AccessibilityTelephonyCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/tof/ContactlessGestureController;


# direct methods
.method private constructor <init>(Lcom/android/server/tof/ContactlessGestureController;)V
    .locals 0

    .line 926
    iput-object p1, p0, Lcom/android/server/tof/ContactlessGestureController$AccessibilityTelephonyCallback;->this$0:Lcom/android/server/tof/ContactlessGestureController;

    invoke-direct {p0}, Landroid/telephony/TelephonyCallback;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/tof/ContactlessGestureController;Lcom/android/server/tof/ContactlessGestureController$AccessibilityTelephonyCallback-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/tof/ContactlessGestureController$AccessibilityTelephonyCallback;-><init>(Lcom/android/server/tof/ContactlessGestureController;)V

    return-void
.end method


# virtual methods
.method public onCallStateChanged(I)V
    .locals 2
    .param p1, "state"    # I

    .line 930
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " call state changed, state:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ContactlessGestureController"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 931
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController$AccessibilityTelephonyCallback;->this$0:Lcom/android/server/tof/ContactlessGestureController;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/android/server/tof/ContactlessGestureController;->onSceneChanged(I)V

    .line 932
    return-void
.end method
