public class com.android.server.tof.TofGestureAppInfo {
	 /* .source "TofGestureAppInfo.java" */
	 /* # instance fields */
	 private Integer mCategory;
	 private java.lang.String mComponentName;
	 private Boolean mEnable;
	 private Integer mFeature;
	 private Boolean mShowInSettings;
	 /* # direct methods */
	 public com.android.server.tof.TofGestureAppInfo ( ) {
		 /* .locals 0 */
		 /* .line 19 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 21 */
		 return;
	 } // .end method
	 public com.android.server.tof.TofGestureAppInfo ( ) {
		 /* .locals 0 */
		 /* .param p1, "mComponentName" # Ljava/lang/String; */
		 /* .param p2, "mCategory" # I */
		 /* .param p3, "mFeature" # I */
		 /* .param p4, "mShowInSettings" # Z */
		 /* .param p5, "mEnable" # Z */
		 /* .line 24 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 25 */
		 this.mComponentName = p1;
		 /* .line 26 */
		 /* iput p2, p0, Lcom/android/server/tof/TofGestureAppInfo;->mCategory:I */
		 /* .line 27 */
		 /* iput p3, p0, Lcom/android/server/tof/TofGestureAppInfo;->mFeature:I */
		 /* .line 28 */
		 /* iput-boolean p4, p0, Lcom/android/server/tof/TofGestureAppInfo;->mShowInSettings:Z */
		 /* .line 29 */
		 /* iput-boolean p5, p0, Lcom/android/server/tof/TofGestureAppInfo;->mEnable:Z */
		 /* .line 30 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Integer getCategory ( ) {
		 /* .locals 1 */
		 /* .line 41 */
		 /* iget v0, p0, Lcom/android/server/tof/TofGestureAppInfo;->mCategory:I */
	 } // .end method
	 public java.lang.String getComponentName ( ) {
		 /* .locals 1 */
		 /* .line 33 */
		 v0 = this.mComponentName;
	 } // .end method
	 public Integer getFeature ( ) {
		 /* .locals 1 */
		 /* .line 49 */
		 /* iget v0, p0, Lcom/android/server/tof/TofGestureAppInfo;->mFeature:I */
	 } // .end method
	 public Boolean isEnable ( ) {
		 /* .locals 1 */
		 /* .line 65 */
		 /* iget-boolean v0, p0, Lcom/android/server/tof/TofGestureAppInfo;->mEnable:Z */
	 } // .end method
	 public Boolean isShowInSettings ( ) {
		 /* .locals 1 */
		 /* .line 57 */
		 /* iget-boolean v0, p0, Lcom/android/server/tof/TofGestureAppInfo;->mShowInSettings:Z */
	 } // .end method
	 public void setCategory ( Integer p0 ) {
		 /* .locals 0 */
		 /* .param p1, "mCategory" # I */
		 /* .line 45 */
		 /* iput p1, p0, Lcom/android/server/tof/TofGestureAppInfo;->mCategory:I */
		 /* .line 46 */
		 return;
	 } // .end method
	 public void setComponentName ( java.lang.String p0 ) {
		 /* .locals 0 */
		 /* .param p1, "mComponentName" # Ljava/lang/String; */
		 /* .line 37 */
		 this.mComponentName = p1;
		 /* .line 38 */
		 return;
	 } // .end method
	 public void setEnable ( Boolean p0 ) {
		 /* .locals 0 */
		 /* .param p1, "mEnable" # Z */
		 /* .line 69 */
		 /* iput-boolean p1, p0, Lcom/android/server/tof/TofGestureAppInfo;->mEnable:Z */
		 /* .line 70 */
		 return;
	 } // .end method
	 public void setFeature ( Integer p0 ) {
		 /* .locals 0 */
		 /* .param p1, "mFeature" # I */
		 /* .line 53 */
		 /* iput p1, p0, Lcom/android/server/tof/TofGestureAppInfo;->mFeature:I */
		 /* .line 54 */
		 return;
	 } // .end method
	 public void setShowInSettings ( Boolean p0 ) {
		 /* .locals 0 */
		 /* .param p1, "mShowInSettings" # Z */
		 /* .line 61 */
		 /* iput-boolean p1, p0, Lcom/android/server/tof/TofGestureAppInfo;->mShowInSettings:Z */
		 /* .line 62 */
		 return;
	 } // .end method
	 public java.lang.String toString ( ) {
		 /* .locals 2 */
		 /* .line 74 */
		 /* new-instance v0, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v1 = "TofGestureAppInfo{mComponentName=\'"; // const-string v1, "TofGestureAppInfo{mComponentName=\'"
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 v1 = this.mComponentName;
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* const/16 v1, 0x27 */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
		 final String v1 = ", mAllPageSupport="; // const-string v1, ", mAllPageSupport="
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget v1, p0, Lcom/android/server/tof/TofGestureAppInfo;->mCategory:I */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 final String v1 = ", mFeature="; // const-string v1, ", mFeature="
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget v1, p0, Lcom/android/server/tof/TofGestureAppInfo;->mFeature:I */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 final String v1 = ", mShowInSettings="; // const-string v1, ", mShowInSettings="
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget-boolean v1, p0, Lcom/android/server/tof/TofGestureAppInfo;->mShowInSettings:Z */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
		 final String v1 = ", mEnable="; // const-string v1, ", mEnable="
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget-boolean v1, p0, Lcom/android/server/tof/TofGestureAppInfo;->mEnable:Z */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
		 /* const/16 v1, 0x7d */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 } // .end method
