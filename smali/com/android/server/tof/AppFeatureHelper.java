public class com.android.server.tof.AppFeatureHelper {
	 /* .source "AppFeatureHelper.java" */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 private static volatile com.android.server.tof.AppFeatureHelper mInstance;
	 /* # instance fields */
	 private java.lang.String mCloudGestureConfig;
	 private android.content.Context mContext;
	 private android.content.pm.PackageManager mPackageManager;
	 private java.lang.String mResGestureConfig;
	 private final java.util.HashMap mTofGestureAppConfigs;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/HashMap<", */
	 /* "Ljava/lang/String;", */
	 /* "Ljava/util/List<", */
	 /* "Lcom/android/server/tof/TofGestureComponent;", */
	 /* ">;>;" */
	 /* } */
} // .end annotation
} // .end field
/* # direct methods */
private com.android.server.tof.AppFeatureHelper ( ) {
/* .locals 1 */
/* .line 35 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 33 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mTofGestureAppConfigs = v0;
/* .line 37 */
return;
} // .end method
private void addTofGestureAppConfig ( com.android.server.tof.TofGestureComponent p0 ) {
/* .locals 3 */
/* .param p1, "gestureApp" # Lcom/android/server/tof/TofGestureComponent; */
/* .line 220 */
if ( p1 != null) { // if-eqz p1, :cond_1
	 /* .line 221 */
	 (( com.android.server.tof.TofGestureComponent ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Lcom/android/server/tof/TofGestureComponent;->getPackageName()Ljava/lang/String;
	 /* .line 222 */
	 /* .local v0, "pkgName":Ljava/lang/String; */
	 v1 = this.mTofGestureAppConfigs;
	 (( java.util.HashMap ) v1 ).get ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
	 /* check-cast v1, Ljava/util/List; */
	 /* .line 223 */
	 /* .local v1, "tofGestureComponents":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/tof/TofGestureComponent;>;" */
	 /* if-nez v1, :cond_0 */
	 /* .line 224 */
	 /* new-instance v2, Ljava/util/ArrayList; */
	 /* invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V */
	 /* move-object v1, v2 */
	 /* .line 226 */
} // :cond_0
/* .line 227 */
v2 = this.mTofGestureAppConfigs;
(( java.util.HashMap ) v2 ).put ( v0, v1 ); // invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 229 */
} // .end local v0 # "pkgName":Ljava/lang/String;
} // .end local v1 # "tofGestureComponents":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/tof/TofGestureComponent;>;"
} // :cond_1
return;
} // .end method
private Integer getAppCategory ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "activity" # Ljava/lang/String; */
/* .line 94 */
int v0 = 0; // const/4 v0, 0x0
(( com.android.server.tof.AppFeatureHelper ) p0 ).getSupportComponentFromConfig ( p1, p2, v0 ); // invoke-virtual {p0, p1, p2, v0}, Lcom/android/server/tof/AppFeatureHelper;->getSupportComponentFromConfig(Ljava/lang/String;Ljava/lang/String;Z)Lcom/android/server/tof/TofGestureComponent;
/* .line 96 */
/* .local v1, "tofGestureComponent":Lcom/android/server/tof/TofGestureComponent; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 97 */
v0 = (( com.android.server.tof.TofGestureComponent ) v1 ).getCategory ( ); // invoke-virtual {v1}, Lcom/android/server/tof/TofGestureComponent;->getCategory()I
/* .line 99 */
} // :cond_0
} // .end method
private java.lang.String getAppName ( android.content.pm.PackageInfo p0 ) {
/* .locals 3 */
/* .param p1, "info" # Landroid/content/pm/PackageInfo; */
/* .line 329 */
final String v0 = ""; // const-string v0, ""
/* .line 330 */
/* .local v0, "appName":Ljava/lang/String; */
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 331 */
v1 = this.applicationInfo;
/* .line 332 */
/* .local v1, "appInfo":Landroid/content/pm/ApplicationInfo; */
/* if-nez v1, :cond_0 */
final String v2 = ""; // const-string v2, ""
} // :cond_0
v2 = this.mPackageManager;
(( android.content.pm.ApplicationInfo ) v1 ).loadLabel ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
} // :goto_0
/* move-object v0, v2 */
/* .line 334 */
} // .end local v1 # "appInfo":Landroid/content/pm/ApplicationInfo;
} // :cond_1
} // .end method
public static com.android.server.tof.AppFeatureHelper getInstance ( ) {
/* .locals 2 */
/* .line 40 */
v0 = com.android.server.tof.AppFeatureHelper.mInstance;
/* if-nez v0, :cond_1 */
/* .line 41 */
/* const-class v0, Lcom/android/server/tof/AppFeatureHelper; */
/* monitor-enter v0 */
/* .line 42 */
try { // :try_start_0
v1 = com.android.server.tof.AppFeatureHelper.mInstance;
/* if-nez v1, :cond_0 */
/* .line 43 */
/* new-instance v1, Lcom/android/server/tof/AppFeatureHelper; */
/* invoke-direct {v1}, Lcom/android/server/tof/AppFeatureHelper;-><init>()V */
/* .line 45 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 47 */
} // :cond_1
} // :goto_0
v0 = com.android.server.tof.AppFeatureHelper.mInstance;
} // .end method
private android.content.pm.PackageInfo getPackageInfo ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .line 250 */
try { // :try_start_0
v0 = this.mPackageManager;
/* .line 251 */
v1 = android.app.ActivityManager .getCurrentUser ( );
/* .line 250 */
/* const/16 v2, 0x40 */
(( android.content.pm.PackageManager ) v0 ).getPackageInfoAsUser ( p1, v2, v1 ); // invoke-virtual {v0, p1, v2, v1}, Landroid/content/pm/PackageManager;->getPackageInfoAsUser(Ljava/lang/String;II)Landroid/content/pm/PackageInfo;
/* :try_end_0 */
/* .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 252 */
/* :catch_0 */
/* move-exception v0 */
/* .line 253 */
/* .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException; */
final String v1 = "AppFeatureHelper"; // const-string v1, "AppFeatureHelper"
(( android.content.pm.PackageManager$NameNotFoundException ) v0 ).toString ( ); // invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v2 );
/* .line 255 */
} // .end local v0 # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Integer getSupportFeature ( java.lang.String p0, java.lang.String p1, Boolean p2 ) {
/* .locals 2 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "activity" # Ljava/lang/String; */
/* .param p3, "vertical" # Z */
/* .line 85 */
int v0 = 0; // const/4 v0, 0x0
(( com.android.server.tof.AppFeatureHelper ) p0 ).getSupportComponentFromConfig ( p1, p2, v0 ); // invoke-virtual {p0, p1, p2, v0}, Lcom/android/server/tof/AppFeatureHelper;->getSupportComponentFromConfig(Ljava/lang/String;Ljava/lang/String;Z)Lcom/android/server/tof/TofGestureComponent;
/* .line 87 */
/* .local v1, "tofGestureComponent":Lcom/android/server/tof/TofGestureComponent; */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 88 */
if ( p3 != null) { // if-eqz p3, :cond_0
v0 = (( com.android.server.tof.TofGestureComponent ) v1 ).getPortraitFeature ( ); // invoke-virtual {v1}, Lcom/android/server/tof/TofGestureComponent;->getPortraitFeature()I
} // :cond_0
v0 = (( com.android.server.tof.TofGestureComponent ) v1 ).getLandscapeFeature ( ); // invoke-virtual {v1}, Lcom/android/server/tof/TofGestureComponent;->getLandscapeFeature()I
} // :goto_0
/* .line 90 */
} // :cond_1
} // .end method
private java.lang.String getSupportSceneDes ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "type" # I */
/* .param p2, "portraitFeature" # I */
/* .line 302 */
/* packed-switch p1, :pswitch_data_0 */
/* .line 323 */
/* :pswitch_0 */
final String v0 = ""; // const-string v0, ""
/* .local v0, "des":Ljava/lang/String; */
/* .line 320 */
} // .end local v0 # "des":Ljava/lang/String;
/* :pswitch_1 */
v0 = this.mContext;
/* const v1, 0x110f0046 */
(( android.content.Context ) v0 ).getString ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;
/* .line 321 */
/* .restart local v0 # "des":Ljava/lang/String; */
/* .line 317 */
} // .end local v0 # "des":Ljava/lang/String;
/* :pswitch_2 */
v0 = this.mContext;
/* const v1, 0x110f03c8 */
(( android.content.Context ) v0 ).getString ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;
/* .line 318 */
/* .restart local v0 # "des":Ljava/lang/String; */
/* .line 314 */
} // .end local v0 # "des":Ljava/lang/String;
/* :pswitch_3 */
v0 = this.mContext;
/* const v1, 0x110f03c7 */
(( android.content.Context ) v0 ).getString ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;
/* .line 315 */
/* .restart local v0 # "des":Ljava/lang/String; */
/* .line 311 */
} // .end local v0 # "des":Ljava/lang/String;
/* :pswitch_4 */
v0 = this.mContext;
/* const v1, 0x110f03c9 */
(( android.content.Context ) v0 ).getString ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;
/* .line 312 */
/* .restart local v0 # "des":Ljava/lang/String; */
/* .line 304 */
} // .end local v0 # "des":Ljava/lang/String;
/* :pswitch_5 */
/* if-lez p2, :cond_0 */
/* .line 305 */
v0 = this.mContext;
/* const v1, 0x110f0047 */
(( android.content.Context ) v0 ).getString ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;
/* .restart local v0 # "des":Ljava/lang/String; */
/* .line 307 */
} // .end local v0 # "des":Ljava/lang/String;
} // :cond_0
v0 = this.mContext;
/* const v1, 0x110f03ca */
(( android.content.Context ) v0 ).getString ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;
/* .line 309 */
/* .restart local v0 # "des":Ljava/lang/String; */
/* nop */
/* .line 325 */
} // :goto_0
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_0 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
private com.miui.tof.gesture.TofGestureAppDetailInfo getSupportTofGestureAppDetailInfo ( android.content.pm.PackageInfo p0 ) {
/* .locals 8 */
/* .param p1, "packageInfo" # Landroid/content/pm/PackageInfo; */
/* .line 274 */
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 275 */
v0 = this.packageName;
/* .line 276 */
/* .local v0, "pkgName":Ljava/lang/String; */
/* invoke-direct {p0, p1}, Lcom/android/server/tof/AppFeatureHelper;->getAppName(Landroid/content/pm/PackageInfo;)Ljava/lang/String; */
/* .line 277 */
/* .local v1, "appName":Ljava/lang/String; */
/* iget v2, p1, Landroid/content/pm/PackageInfo;->versionCode:I */
/* .line 278 */
/* .local v2, "versionCode":I */
v3 = this.mTofGestureAppConfigs;
(( java.util.HashMap ) v3 ).get ( v0 ); // invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v3, Ljava/util/List; */
/* .line 279 */
/* .local v3, "tofGestureComponents":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/tof/TofGestureComponent;>;" */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 280 */
int v4 = 0; // const/4 v4, 0x0
/* check-cast v4, Lcom/android/server/tof/TofGestureComponent; */
/* .line 281 */
/* .local v4, "tofGestureComponent":Lcom/android/server/tof/TofGestureComponent; */
v5 = (( com.android.server.tof.TofGestureComponent ) v4 ).isVersionSupported ( v2 ); // invoke-virtual {v4, v2}, Lcom/android/server/tof/TofGestureComponent;->isVersionSupported(I)Z
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 282 */
v5 = (( com.android.server.tof.TofGestureComponent ) v4 ).getCategory ( ); // invoke-virtual {v4}, Lcom/android/server/tof/TofGestureComponent;->getCategory()I
/* .line 283 */
v6 = (( com.android.server.tof.TofGestureComponent ) v4 ).getPortraitFeature ( ); // invoke-virtual {v4}, Lcom/android/server/tof/TofGestureComponent;->getPortraitFeature()I
/* .line 282 */
/* invoke-direct {p0, v5, v6}, Lcom/android/server/tof/AppFeatureHelper;->getSupportSceneDes(II)Ljava/lang/String; */
/* .line 285 */
/* .local v5, "supportSceneDes":Ljava/lang/String; */
v6 = this.mContext;
v6 = com.android.server.tof.AppStatusManager .statusHasChanged ( v6,v0 );
if ( v6 != null) { // if-eqz v6, :cond_0
/* .line 286 */
v6 = this.mContext;
v6 = com.android.server.tof.AppStatusManager .isAppEnabled ( v6,v0 );
/* .local v6, "isEnable":Z */
/* .line 288 */
} // .end local v6 # "isEnable":Z
} // :cond_0
v6 = (( com.android.server.tof.TofGestureComponent ) v4 ).isEnable ( ); // invoke-virtual {v4}, Lcom/android/server/tof/TofGestureComponent;->isEnable()Z
/* .line 290 */
/* .restart local v6 # "isEnable":Z */
} // :goto_0
/* new-instance v7, Lcom/miui/tof/gesture/TofGestureAppDetailInfo; */
/* invoke-direct {v7, v0, v1, v6, v5}, Lcom/miui/tof/gesture/TofGestureAppDetailInfo;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V */
/* .line 292 */
/* .local v7, "appDetailInfo":Lcom/miui/tof/gesture/TofGestureAppDetailInfo; */
/* .line 297 */
} // .end local v0 # "pkgName":Ljava/lang/String;
} // .end local v1 # "appName":Ljava/lang/String;
} // .end local v2 # "versionCode":I
} // .end local v3 # "tofGestureComponents":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/tof/TofGestureComponent;>;"
} // .end local v4 # "tofGestureComponent":Lcom/android/server/tof/TofGestureComponent;
} // .end local v5 # "supportSceneDes":Ljava/lang/String;
} // .end local v6 # "isEnable":Z
} // .end local v7 # "appDetailInfo":Lcom/miui/tof/gesture/TofGestureAppDetailInfo;
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Integer hexStringToInt ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "config" # Ljava/lang/String; */
/* .line 201 */
int v0 = 0; // const/4 v0, 0x0
if ( p1 != null) { // if-eqz p1, :cond_3
v1 = (( java.lang.String ) p1 ).isEmpty ( ); // invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 205 */
} // :cond_0
final String v1 = "\\d+"; // const-string v1, "\\d+"
v1 = (( java.lang.String ) p1 ).matches ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 206 */
v0 = java.lang.Integer .parseInt ( p1 );
/* .line 209 */
} // :cond_1
(( java.lang.String ) p1 ).toLowerCase ( ); // invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;
final String v2 = "0x"; // const-string v2, "0x"
v1 = (( java.lang.String ) v1 ).startsWith ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 211 */
int v1 = 2; // const/4 v1, 0x2
try { // :try_start_0
(( java.lang.String ) p1 ).substring ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;
/* const/16 v2, 0x10 */
v0 = java.lang.Integer .parseInt ( v1,v2 );
/* :try_end_0 */
/* .catch Ljava/lang/NumberFormatException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 212 */
/* :catch_0 */
/* move-exception v1 */
/* .line 213 */
/* .local v1, "e":Ljava/lang/NumberFormatException; */
/* .line 216 */
} // .end local v1 # "e":Ljava/lang/NumberFormatException;
} // :cond_2
/* .line 202 */
} // :cond_3
} // :goto_0
} // .end method
private com.android.server.tof.TofGestureComponent parseTofGestureComponent ( java.lang.String p0 ) {
/* .locals 18 */
/* .param p1, "item" # Ljava/lang/String; */
/* .line 172 */
/* move-object/from16 v1, p0 */
/* move-object/from16 v2, p1 */
final String v0 = ","; // const-string v0, ","
(( java.lang.String ) v2 ).split ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 173 */
/* .local v3, "appConfig":[Ljava/lang/String; */
/* array-length v0, v3 */
/* const/16 v4, 0x8 */
final String v5 = "gesture component ["; // const-string v5, "gesture component ["
int v6 = 0; // const/4 v6, 0x0
final String v7 = "AppFeatureHelper"; // const-string v7, "AppFeatureHelper"
/* if-ge v0, v4, :cond_0 */
/* .line 174 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = "] length error!"; // const-string v4, "] length error!"
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v7,v0 );
/* .line 175 */
/* .line 178 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* aget-object v4, v3, v0 */
/* .line 179 */
/* .local v4, "pkgName":Ljava/lang/String; */
int v8 = 1; // const/4 v8, 0x1
/* aget-object v17, v3, v8 */
/* .line 181 */
/* .local v17, "activity":Ljava/lang/String; */
int v9 = 2; // const/4 v9, 0x2
try { // :try_start_0
/* aget-object v9, v3, v9 */
v11 = java.lang.Integer .parseInt ( v9 );
/* .line 182 */
/* .local v11, "category":I */
int v9 = 3; // const/4 v9, 0x3
/* aget-object v10, v3, v9 */
v10 = (( java.lang.String ) v10 ).isEmpty ( ); // invoke-virtual {v10}, Ljava/lang/String;->isEmpty()Z
if ( v10 != null) { // if-eqz v10, :cond_1
/* .line 183 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "] feature config error!"; // const-string v5, "] feature config error!"
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v7,v0 );
/* .line 184 */
/* .line 186 */
} // :cond_1
/* aget-object v5, v3, v9 */
v12 = /* invoke-direct {v1, v5}, Lcom/android/server/tof/AppFeatureHelper;->hexStringToInt(Ljava/lang/String;)I */
/* .line 187 */
/* .local v12, "feature":I */
int v5 = 4; // const/4 v5, 0x4
/* aget-object v9, v3, v5 */
v9 = (( java.lang.String ) v9 ).isEmpty ( ); // invoke-virtual {v9}, Ljava/lang/String;->isEmpty()Z
if ( v9 != null) { // if-eqz v9, :cond_2
/* move v13, v12 */
} // :cond_2
/* aget-object v5, v3, v5 */
v5 = /* invoke-direct {v1, v5}, Lcom/android/server/tof/AppFeatureHelper;->hexStringToInt(Ljava/lang/String;)I */
/* move v13, v5 */
/* .line 188 */
/* .local v13, "featureVertical":I */
} // :goto_0
int v5 = 5; // const/4 v5, 0x5
/* aget-object v5, v3, v5 */
v14 = java.lang.Integer .parseInt ( v5 );
/* .line 189 */
/* .local v14, "minVersionCode":I */
int v5 = 6; // const/4 v5, 0x6
/* aget-object v5, v3, v5 */
v15 = java.lang.Integer .parseInt ( v5 );
/* .line 190 */
/* .local v15, "maxVersionCode":I */
int v5 = 7; // const/4 v5, 0x7
/* aget-object v5, v3, v5 */
v5 = java.lang.Integer .parseInt ( v5 );
/* if-ne v5, v8, :cond_3 */
/* move/from16 v16, v8 */
} // :cond_3
/* move/from16 v16, v0 */
/* .line 192 */
/* .local v16, "enabled":Z */
} // :goto_1
/* new-instance v0, Lcom/android/server/tof/TofGestureComponent; */
/* move-object v8, v0 */
/* move-object v9, v4 */
/* move-object/from16 v10, v17 */
/* invoke-direct/range {v8 ..v16}, Lcom/android/server/tof/TofGestureComponent;-><init>(Ljava/lang/String;Ljava/lang/String;IIIIIZ)V */
/* :try_end_0 */
/* .catch Ljava/lang/NumberFormatException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 194 */
} // .end local v11 # "category":I
} // .end local v12 # "feature":I
} // .end local v13 # "featureVertical":I
} // .end local v14 # "minVersionCode":I
} // .end local v15 # "maxVersionCode":I
} // .end local v16 # "enabled":Z
/* :catch_0 */
/* move-exception v0 */
/* .line 195 */
/* .local v0, "e":Ljava/lang/RuntimeException; */
(( java.lang.RuntimeException ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;
android.util.Slog .e ( v7,v5 );
/* .line 196 */
} // .end method
private void parseTofGestureComponentFromRes ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 158 */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x11030057 */
(( android.content.res.Resources ) v0 ).getStringArray ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 159 */
/* .local v0, "array":[Ljava/lang/String; */
/* if-nez v0, :cond_0 */
/* .line 160 */
final String v1 = "AppFeatureHelper"; // const-string v1, "AppFeatureHelper"
final String v2 = "get config_tofComponentSupport resource error."; // const-string v2, "get config_tofComponentSupport resource error."
android.util.Slog .e ( v1,v2 );
/* .line 161 */
return;
/* .line 164 */
} // :cond_0
java.util.Arrays .toString ( v0 );
(( com.android.server.tof.AppFeatureHelper ) p0 ).setResGestureConfig ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/tof/AppFeatureHelper;->setResGestureConfig(Ljava/lang/String;)V
/* .line 165 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "index":I */
} // :goto_0
/* array-length v2, v0 */
/* if-ge v1, v2, :cond_1 */
/* .line 166 */
/* aget-object v2, v0, v1 */
/* invoke-direct {p0, v2}, Lcom/android/server/tof/AppFeatureHelper;->parseTofGestureComponent(Ljava/lang/String;)Lcom/android/server/tof/TofGestureComponent; */
/* .line 167 */
/* .local v2, "gestureApp":Lcom/android/server/tof/TofGestureComponent; */
/* invoke-direct {p0, v2}, Lcom/android/server/tof/AppFeatureHelper;->addTofGestureAppConfig(Lcom/android/server/tof/TofGestureComponent;)V */
/* .line 165 */
} // .end local v2 # "gestureApp":Lcom/android/server/tof/TofGestureComponent;
/* add-int/lit8 v1, v1, 0x1 */
/* .line 169 */
} // .end local v1 # "index":I
} // :cond_1
return;
} // .end method
/* # virtual methods */
public Boolean changeTofGestureAppConfig ( java.lang.String p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "enable" # Z */
/* .line 259 */
v0 = this.mTofGestureAppConfigs;
(( java.util.HashMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/util/List; */
/* .line 260 */
/* .local v0, "tofGestureComponents":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/tof/TofGestureComponent;>;" */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 261 */
v1 = this.mContext;
com.android.server.tof.AppStatusManager .setAppEnable ( v1,p1,p2 );
/* .line 262 */
int v1 = 1; // const/4 v1, 0x1
/* .line 264 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Integer getAppCategory ( android.content.ComponentName p0 ) {
/* .locals 2 */
/* .param p1, "componentName" # Landroid/content/ComponentName; */
/* .line 78 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 79 */
(( android.content.ComponentName ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
(( android.content.ComponentName ) p1 ).getClassName ( ); // invoke-virtual {p1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
v0 = /* invoke-direct {p0, v0, v1}, Lcom/android/server/tof/AppFeatureHelper;->getAppCategory(Ljava/lang/String;Ljava/lang/String;)I */
/* .line 81 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public java.lang.String getCloudGestureConfig ( ) {
/* .locals 1 */
/* .line 59 */
v0 = this.mCloudGestureConfig;
} // .end method
public java.lang.String getResGestureConfig ( ) {
/* .locals 1 */
/* .line 51 */
v0 = this.mResGestureConfig;
} // .end method
public com.android.server.tof.TofGestureComponent getSupportComponentFromConfig ( android.content.ComponentName p0 ) {
/* .locals 3 */
/* .param p1, "componentName" # Landroid/content/ComponentName; */
/* .line 103 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 104 */
(( android.content.ComponentName ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 105 */
(( android.content.ComponentName ) p1 ).getClassName ( ); // invoke-virtual {p1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
/* .line 104 */
int v2 = 0; // const/4 v2, 0x0
(( com.android.server.tof.AppFeatureHelper ) p0 ).getSupportComponentFromConfig ( v0, v1, v2 ); // invoke-virtual {p0, v0, v1, v2}, Lcom/android/server/tof/AppFeatureHelper;->getSupportComponentFromConfig(Ljava/lang/String;Ljava/lang/String;Z)Lcom/android/server/tof/TofGestureComponent;
/* .line 107 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public com.android.server.tof.TofGestureComponent getSupportComponentFromConfig ( java.lang.String p0, java.lang.String p1, Boolean p2 ) {
/* .locals 9 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "activity" # Ljava/lang/String; */
/* .param p3, "includeDisable" # Z */
/* .line 112 */
v0 = this.mTofGestureAppConfigs;
/* monitor-enter v0 */
/* .line 113 */
try { // :try_start_0
v1 = this.mTofGestureAppConfigs;
(( java.util.HashMap ) v1 ).get ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/util/List; */
/* .line 114 */
/* .local v1, "tofGestureComponents":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/tof/TofGestureComponent;>;" */
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 116 */
v2 = this.mContext;
v2 = com.android.server.tof.AppStatusManager .statusHasChanged ( v2,p1 );
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 117 */
v2 = this.mContext;
v2 = com.android.server.tof.AppStatusManager .isAppEnabled ( v2,p1 );
/* .local v2, "enabled":Z */
/* .line 119 */
} // .end local v2 # "enabled":Z
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
/* check-cast v2, Lcom/android/server/tof/TofGestureComponent; */
v2 = (( com.android.server.tof.TofGestureComponent ) v2 ).isEnable ( ); // invoke-virtual {v2}, Lcom/android/server/tof/TofGestureComponent;->isEnable()Z
/* .line 121 */
/* .restart local v2 # "enabled":Z */
} // :goto_0
v4 = } // :goto_1
if ( v4 != null) { // if-eqz v4, :cond_4
/* check-cast v4, Lcom/android/server/tof/TofGestureComponent; */
/* .line 122 */
/* .local v4, "tofGestureComponent":Lcom/android/server/tof/TofGestureComponent; */
/* invoke-direct {p0, p1}, Lcom/android/server/tof/AppFeatureHelper;->getPackageInfo(Ljava/lang/String;)Landroid/content/pm/PackageInfo; */
/* .line 123 */
/* .local v5, "info":Landroid/content/pm/PackageInfo; */
/* iget v6, v5, Landroid/content/pm/PackageInfo;->versionCode:I */
v6 = (( com.android.server.tof.TofGestureComponent ) v4 ).isVersionSupported ( v6 ); // invoke-virtual {v4, v6}, Lcom/android/server/tof/TofGestureComponent;->isVersionSupported(I)Z
if ( v6 != null) { // if-eqz v6, :cond_3
/* if-nez v2, :cond_1 */
if ( p3 != null) { // if-eqz p3, :cond_3
/* .line 125 */
} // :cond_1
(( com.android.server.tof.TofGestureComponent ) v4 ).getActivityName ( ); // invoke-virtual {v4}, Lcom/android/server/tof/TofGestureComponent;->getActivityName()Ljava/lang/String;
/* .line 126 */
/* .local v6, "configActivity":Ljava/lang/String; */
(( com.android.server.tof.TofGestureComponent ) v4 ).getPackageName ( ); // invoke-virtual {v4}, Lcom/android/server/tof/TofGestureComponent;->getPackageName()Ljava/lang/String;
/* .line 128 */
/* .local v7, "configPackageName":Ljava/lang/String; */
v8 = (( java.lang.String ) p2 ).equals ( v6 ); // invoke-virtual {p2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v8, :cond_2 */
/* .line 129 */
v8 = (( java.lang.String ) v6 ).isEmpty ( ); // invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z
if ( v8 != null) { // if-eqz v8, :cond_3
v8 = (( java.lang.String ) v7 ).equals ( p1 ); // invoke-virtual {v7, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v8 != null) { // if-eqz v8, :cond_3
/* .line 130 */
} // :cond_2
/* monitor-exit v0 */
/* .line 133 */
} // .end local v4 # "tofGestureComponent":Lcom/android/server/tof/TofGestureComponent;
} // .end local v5 # "info":Landroid/content/pm/PackageInfo;
} // .end local v6 # "configActivity":Ljava/lang/String;
} // .end local v7 # "configPackageName":Ljava/lang/String;
} // :cond_3
/* .line 135 */
} // .end local v2 # "enabled":Z
} // :cond_4
/* monitor-exit v0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 136 */
} // .end local v1 # "tofGestureComponents":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/tof/TofGestureComponent;>;"
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Integer getSupportFeature ( android.content.ComponentName p0 ) {
/* .locals 1 */
/* .param p1, "componentName" # Landroid/content/ComponentName; */
/* .line 74 */
int v0 = 0; // const/4 v0, 0x0
v0 = (( com.android.server.tof.AppFeatureHelper ) p0 ).getSupportFeature ( p1, v0 ); // invoke-virtual {p0, p1, v0}, Lcom/android/server/tof/AppFeatureHelper;->getSupportFeature(Landroid/content/ComponentName;Z)I
} // .end method
public Integer getSupportFeature ( android.content.ComponentName p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "componentName" # Landroid/content/ComponentName; */
/* .param p2, "vertical" # Z */
/* .line 67 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 68 */
(( android.content.ComponentName ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
(( android.content.ComponentName ) p1 ).getClassName ( ); // invoke-virtual {p1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
v0 = /* invoke-direct {p0, v0, v1, p2}, Lcom/android/server/tof/AppFeatureHelper;->getSupportFeature(Ljava/lang/String;Ljava/lang/String;Z)I */
/* .line 70 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public com.miui.tof.gesture.TofGestureAppData getTofGestureAppData ( ) {
/* .locals 6 */
/* .line 232 */
/* new-instance v0, Lcom/miui/tof/gesture/TofGestureAppData; */
/* invoke-direct {v0}, Lcom/miui/tof/gesture/TofGestureAppData;-><init>()V */
/* .line 233 */
/* .local v0, "gestureAppData":Lcom/miui/tof/gesture/TofGestureAppData; */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 234 */
/* .local v1, "tofGestureAppDetailInfos":Ljava/util/List;, "Ljava/util/List<Lcom/miui/tof/gesture/TofGestureAppDetailInfo;>;" */
v2 = this.mTofGestureAppConfigs;
(( java.util.HashMap ) v2 ).keySet ( ); // invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_2
/* check-cast v3, Ljava/lang/String; */
/* .line 235 */
/* .local v3, "pkgName":Ljava/lang/String; */
/* invoke-direct {p0, v3}, Lcom/android/server/tof/AppFeatureHelper;->getPackageInfo(Ljava/lang/String;)Landroid/content/pm/PackageInfo; */
/* .line 236 */
/* .local v4, "info":Landroid/content/pm/PackageInfo; */
/* if-nez v4, :cond_0 */
/* .line 237 */
/* .line 239 */
} // :cond_0
/* invoke-direct {p0, v4}, Lcom/android/server/tof/AppFeatureHelper;->getSupportTofGestureAppDetailInfo(Landroid/content/pm/PackageInfo;)Lcom/miui/tof/gesture/TofGestureAppDetailInfo; */
/* .line 240 */
/* .local v5, "appDetailInfo":Lcom/miui/tof/gesture/TofGestureAppDetailInfo; */
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 241 */
/* .line 243 */
} // .end local v3 # "pkgName":Ljava/lang/String;
} // .end local v4 # "info":Landroid/content/pm/PackageInfo;
} // .end local v5 # "appDetailInfo":Lcom/miui/tof/gesture/TofGestureAppDetailInfo;
} // :cond_1
/* .line 244 */
} // :cond_2
(( com.miui.tof.gesture.TofGestureAppData ) v0 ).setTofGestureAppDetailInfoList ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/tof/gesture/TofGestureAppData;->setTofGestureAppDetailInfoList(Ljava/util/List;)V
/* .line 245 */
} // .end method
public void initTofComponentConfig ( android.content.Context p0 ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 140 */
this.mContext = p1;
/* .line 141 */
(( android.content.Context ) p1 ).getPackageManager ( ); // invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
this.mPackageManager = v0;
/* .line 142 */
/* invoke-direct {p0, p1}, Lcom/android/server/tof/AppFeatureHelper;->parseTofGestureComponentFromRes(Landroid/content/Context;)V */
/* .line 143 */
return;
} // .end method
public void parseContactlessGestureComponentFromCloud ( java.util.List p0 ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 146 */
/* .local p1, "componentFeatureList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = if ( p1 != null) { // if-eqz p1, :cond_2
/* if-nez v0, :cond_0 */
/* .line 149 */
} // :cond_0
(( java.lang.Object ) p1 ).toString ( ); // invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( com.android.server.tof.AppFeatureHelper ) p0 ).setCloudGestureConfig ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/tof/AppFeatureHelper;->setCloudGestureConfig(Ljava/lang/String;)V
/* .line 150 */
v0 = this.mTofGestureAppConfigs;
(( java.util.HashMap ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->clear()V
/* .line 151 */
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Ljava/lang/String; */
/* .line 152 */
/* .local v1, "item":Ljava/lang/String; */
/* invoke-direct {p0, v1}, Lcom/android/server/tof/AppFeatureHelper;->parseTofGestureComponent(Ljava/lang/String;)Lcom/android/server/tof/TofGestureComponent; */
/* .line 153 */
/* .local v2, "gestureApp":Lcom/android/server/tof/TofGestureComponent; */
/* invoke-direct {p0, v2}, Lcom/android/server/tof/AppFeatureHelper;->addTofGestureAppConfig(Lcom/android/server/tof/TofGestureComponent;)V */
/* .line 154 */
} // .end local v1 # "item":Ljava/lang/String;
} // .end local v2 # "gestureApp":Lcom/android/server/tof/TofGestureComponent;
/* .line 155 */
} // :cond_1
return;
/* .line 147 */
} // :cond_2
} // :goto_1
return;
} // .end method
public void setCloudGestureConfig ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "mCloudGestureConfig" # Ljava/lang/String; */
/* .line 63 */
this.mCloudGestureConfig = p1;
/* .line 64 */
return;
} // .end method
public void setResGestureConfig ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "mResGestureConfig" # Ljava/lang/String; */
/* .line 55 */
this.mResGestureConfig = p1;
/* .line 56 */
return;
} // .end method
