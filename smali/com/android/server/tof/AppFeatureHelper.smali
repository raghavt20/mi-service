.class public Lcom/android/server/tof/AppFeatureHelper;
.super Ljava/lang/Object;
.source "AppFeatureHelper.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "AppFeatureHelper"

.field private static volatile mInstance:Lcom/android/server/tof/AppFeatureHelper;


# instance fields
.field private mCloudGestureConfig:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mPackageManager:Landroid/content/pm/PackageManager;

.field private mResGestureConfig:Ljava/lang/String;

.field private final mTofGestureAppConfigs:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/android/server/tof/TofGestureComponent;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/tof/AppFeatureHelper;->mTofGestureAppConfigs:Ljava/util/HashMap;

    .line 37
    return-void
.end method

.method private addTofGestureAppConfig(Lcom/android/server/tof/TofGestureComponent;)V
    .locals 3
    .param p1, "gestureApp"    # Lcom/android/server/tof/TofGestureComponent;

    .line 220
    if-eqz p1, :cond_1

    .line 221
    invoke-virtual {p1}, Lcom/android/server/tof/TofGestureComponent;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 222
    .local v0, "pkgName":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/tof/AppFeatureHelper;->mTofGestureAppConfigs:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 223
    .local v1, "tofGestureComponents":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/tof/TofGestureComponent;>;"
    if-nez v1, :cond_0

    .line 224
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object v1, v2

    .line 226
    :cond_0
    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 227
    iget-object v2, p0, Lcom/android/server/tof/AppFeatureHelper;->mTofGestureAppConfigs:Ljava/util/HashMap;

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 229
    .end local v0    # "pkgName":Ljava/lang/String;
    .end local v1    # "tofGestureComponents":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/tof/TofGestureComponent;>;"
    :cond_1
    return-void
.end method

.method private getAppCategory(Ljava/lang/String;Ljava/lang/String;)I
    .locals 2
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "activity"    # Ljava/lang/String;

    .line 94
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/android/server/tof/AppFeatureHelper;->getSupportComponentFromConfig(Ljava/lang/String;Ljava/lang/String;Z)Lcom/android/server/tof/TofGestureComponent;

    move-result-object v1

    .line 96
    .local v1, "tofGestureComponent":Lcom/android/server/tof/TofGestureComponent;
    if-eqz v1, :cond_0

    .line 97
    invoke-virtual {v1}, Lcom/android/server/tof/TofGestureComponent;->getCategory()I

    move-result v0

    return v0

    .line 99
    :cond_0
    return v0
.end method

.method private getAppName(Landroid/content/pm/PackageInfo;)Ljava/lang/String;
    .locals 3
    .param p1, "info"    # Landroid/content/pm/PackageInfo;

    .line 329
    const-string v0, ""

    .line 330
    .local v0, "appName":Ljava/lang/String;
    if-eqz p1, :cond_1

    .line 331
    iget-object v1, p1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    .line 332
    .local v1, "appInfo":Landroid/content/pm/ApplicationInfo;
    if-nez v1, :cond_0

    const-string v2, ""

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/android/server/tof/AppFeatureHelper;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v1, v2}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_0
    move-object v0, v2

    .line 334
    .end local v1    # "appInfo":Landroid/content/pm/ApplicationInfo;
    :cond_1
    return-object v0
.end method

.method public static getInstance()Lcom/android/server/tof/AppFeatureHelper;
    .locals 2

    .line 40
    sget-object v0, Lcom/android/server/tof/AppFeatureHelper;->mInstance:Lcom/android/server/tof/AppFeatureHelper;

    if-nez v0, :cond_1

    .line 41
    const-class v0, Lcom/android/server/tof/AppFeatureHelper;

    monitor-enter v0

    .line 42
    :try_start_0
    sget-object v1, Lcom/android/server/tof/AppFeatureHelper;->mInstance:Lcom/android/server/tof/AppFeatureHelper;

    if-nez v1, :cond_0

    .line 43
    new-instance v1, Lcom/android/server/tof/AppFeatureHelper;

    invoke-direct {v1}, Lcom/android/server/tof/AppFeatureHelper;-><init>()V

    sput-object v1, Lcom/android/server/tof/AppFeatureHelper;->mInstance:Lcom/android/server/tof/AppFeatureHelper;

    .line 45
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 47
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/tof/AppFeatureHelper;->mInstance:Lcom/android/server/tof/AppFeatureHelper;

    return-object v0
.end method

.method private getPackageInfo(Ljava/lang/String;)Landroid/content/pm/PackageInfo;
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 250
    :try_start_0
    iget-object v0, p0, Lcom/android/server/tof/AppFeatureHelper;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 251
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v1

    .line 250
    const/16 v2, 0x40

    invoke-virtual {v0, p1, v2, v1}, Landroid/content/pm/PackageManager;->getPackageInfoAsUser(Ljava/lang/String;II)Landroid/content/pm/PackageInfo;

    move-result-object v0
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 252
    :catch_0
    move-exception v0

    .line 253
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v1, "AppFeatureHelper"

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v0, 0x0

    return-object v0
.end method

.method private getSupportFeature(Ljava/lang/String;Ljava/lang/String;Z)I
    .locals 2
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "activity"    # Ljava/lang/String;
    .param p3, "vertical"    # Z

    .line 85
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/android/server/tof/AppFeatureHelper;->getSupportComponentFromConfig(Ljava/lang/String;Ljava/lang/String;Z)Lcom/android/server/tof/TofGestureComponent;

    move-result-object v1

    .line 87
    .local v1, "tofGestureComponent":Lcom/android/server/tof/TofGestureComponent;
    if-eqz v1, :cond_1

    .line 88
    if-eqz p3, :cond_0

    invoke-virtual {v1}, Lcom/android/server/tof/TofGestureComponent;->getPortraitFeature()I

    move-result v0

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Lcom/android/server/tof/TofGestureComponent;->getLandscapeFeature()I

    move-result v0

    :goto_0
    return v0

    .line 90
    :cond_1
    return v0
.end method

.method private getSupportSceneDes(II)Ljava/lang/String;
    .locals 2
    .param p1, "type"    # I
    .param p2, "portraitFeature"    # I

    .line 302
    packed-switch p1, :pswitch_data_0

    .line 323
    :pswitch_0
    const-string v0, ""

    .local v0, "des":Ljava/lang/String;
    goto :goto_0

    .line 320
    .end local v0    # "des":Ljava/lang/String;
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/tof/AppFeatureHelper;->mContext:Landroid/content/Context;

    const v1, 0x110f0046

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 321
    .restart local v0    # "des":Ljava/lang/String;
    goto :goto_0

    .line 317
    .end local v0    # "des":Ljava/lang/String;
    :pswitch_2
    iget-object v0, p0, Lcom/android/server/tof/AppFeatureHelper;->mContext:Landroid/content/Context;

    const v1, 0x110f03c8

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 318
    .restart local v0    # "des":Ljava/lang/String;
    goto :goto_0

    .line 314
    .end local v0    # "des":Ljava/lang/String;
    :pswitch_3
    iget-object v0, p0, Lcom/android/server/tof/AppFeatureHelper;->mContext:Landroid/content/Context;

    const v1, 0x110f03c7

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 315
    .restart local v0    # "des":Ljava/lang/String;
    goto :goto_0

    .line 311
    .end local v0    # "des":Ljava/lang/String;
    :pswitch_4
    iget-object v0, p0, Lcom/android/server/tof/AppFeatureHelper;->mContext:Landroid/content/Context;

    const v1, 0x110f03c9

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 312
    .restart local v0    # "des":Ljava/lang/String;
    goto :goto_0

    .line 304
    .end local v0    # "des":Ljava/lang/String;
    :pswitch_5
    if-lez p2, :cond_0

    .line 305
    iget-object v0, p0, Lcom/android/server/tof/AppFeatureHelper;->mContext:Landroid/content/Context;

    const v1, 0x110f0047

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "des":Ljava/lang/String;
    goto :goto_0

    .line 307
    .end local v0    # "des":Ljava/lang/String;
    :cond_0
    iget-object v0, p0, Lcom/android/server/tof/AppFeatureHelper;->mContext:Landroid/content/Context;

    const v1, 0x110f03ca

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 309
    .restart local v0    # "des":Ljava/lang/String;
    nop

    .line 325
    :goto_0
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getSupportTofGestureAppDetailInfo(Landroid/content/pm/PackageInfo;)Lcom/miui/tof/gesture/TofGestureAppDetailInfo;
    .locals 8
    .param p1, "packageInfo"    # Landroid/content/pm/PackageInfo;

    .line 274
    if-eqz p1, :cond_1

    .line 275
    iget-object v0, p1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    .line 276
    .local v0, "pkgName":Ljava/lang/String;
    invoke-direct {p0, p1}, Lcom/android/server/tof/AppFeatureHelper;->getAppName(Landroid/content/pm/PackageInfo;)Ljava/lang/String;

    move-result-object v1

    .line 277
    .local v1, "appName":Ljava/lang/String;
    iget v2, p1, Landroid/content/pm/PackageInfo;->versionCode:I

    .line 278
    .local v2, "versionCode":I
    iget-object v3, p0, Lcom/android/server/tof/AppFeatureHelper;->mTofGestureAppConfigs:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 279
    .local v3, "tofGestureComponents":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/tof/TofGestureComponent;>;"
    if-eqz v3, :cond_1

    .line 280
    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/tof/TofGestureComponent;

    .line 281
    .local v4, "tofGestureComponent":Lcom/android/server/tof/TofGestureComponent;
    invoke-virtual {v4, v2}, Lcom/android/server/tof/TofGestureComponent;->isVersionSupported(I)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 282
    invoke-virtual {v4}, Lcom/android/server/tof/TofGestureComponent;->getCategory()I

    move-result v5

    .line 283
    invoke-virtual {v4}, Lcom/android/server/tof/TofGestureComponent;->getPortraitFeature()I

    move-result v6

    .line 282
    invoke-direct {p0, v5, v6}, Lcom/android/server/tof/AppFeatureHelper;->getSupportSceneDes(II)Ljava/lang/String;

    move-result-object v5

    .line 285
    .local v5, "supportSceneDes":Ljava/lang/String;
    iget-object v6, p0, Lcom/android/server/tof/AppFeatureHelper;->mContext:Landroid/content/Context;

    invoke-static {v6, v0}, Lcom/android/server/tof/AppStatusManager;->statusHasChanged(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 286
    iget-object v6, p0, Lcom/android/server/tof/AppFeatureHelper;->mContext:Landroid/content/Context;

    invoke-static {v6, v0}, Lcom/android/server/tof/AppStatusManager;->isAppEnabled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v6

    .local v6, "isEnable":Z
    goto :goto_0

    .line 288
    .end local v6    # "isEnable":Z
    :cond_0
    invoke-virtual {v4}, Lcom/android/server/tof/TofGestureComponent;->isEnable()Z

    move-result v6

    .line 290
    .restart local v6    # "isEnable":Z
    :goto_0
    new-instance v7, Lcom/miui/tof/gesture/TofGestureAppDetailInfo;

    invoke-direct {v7, v0, v1, v6, v5}, Lcom/miui/tof/gesture/TofGestureAppDetailInfo;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 292
    .local v7, "appDetailInfo":Lcom/miui/tof/gesture/TofGestureAppDetailInfo;
    return-object v7

    .line 297
    .end local v0    # "pkgName":Ljava/lang/String;
    .end local v1    # "appName":Ljava/lang/String;
    .end local v2    # "versionCode":I
    .end local v3    # "tofGestureComponents":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/tof/TofGestureComponent;>;"
    .end local v4    # "tofGestureComponent":Lcom/android/server/tof/TofGestureComponent;
    .end local v5    # "supportSceneDes":Ljava/lang/String;
    .end local v6    # "isEnable":Z
    .end local v7    # "appDetailInfo":Lcom/miui/tof/gesture/TofGestureAppDetailInfo;
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method private hexStringToInt(Ljava/lang/String;)I
    .locals 3
    .param p1, "config"    # Ljava/lang/String;

    .line 201
    const/4 v0, 0x0

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 205
    :cond_0
    const-string v1, "\\d+"

    invoke-virtual {p1, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 206
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    return v0

    .line 209
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "0x"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 211
    const/4 v1, 0x2

    :try_start_0
    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x10

    invoke-static {v1, v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 212
    :catch_0
    move-exception v1

    .line 213
    .local v1, "e":Ljava/lang/NumberFormatException;
    return v0

    .line 216
    .end local v1    # "e":Ljava/lang/NumberFormatException;
    :cond_2
    return v0

    .line 202
    :cond_3
    :goto_0
    return v0
.end method

.method private parseTofGestureComponent(Ljava/lang/String;)Lcom/android/server/tof/TofGestureComponent;
    .locals 18
    .param p1, "item"    # Ljava/lang/String;

    .line 172
    move-object/from16 v1, p0

    move-object/from16 v2, p1

    const-string v0, ","

    invoke-virtual {v2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 173
    .local v3, "appConfig":[Ljava/lang/String;
    array-length v0, v3

    const/16 v4, 0x8

    const-string v5, "gesture component ["

    const/4 v6, 0x0

    const-string v7, "AppFeatureHelper"

    if-ge v0, v4, :cond_0

    .line 174
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "] length error!"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    return-object v6

    .line 178
    :cond_0
    const/4 v0, 0x0

    aget-object v4, v3, v0

    .line 179
    .local v4, "pkgName":Ljava/lang/String;
    const/4 v8, 0x1

    aget-object v17, v3, v8

    .line 181
    .local v17, "activity":Ljava/lang/String;
    const/4 v9, 0x2

    :try_start_0
    aget-object v9, v3, v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    .line 182
    .local v11, "category":I
    const/4 v9, 0x3

    aget-object v10, v3, v9

    invoke-virtual {v10}, Ljava/lang/String;->isEmpty()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 183
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "] feature config error!"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    return-object v6

    .line 186
    :cond_1
    aget-object v5, v3, v9

    invoke-direct {v1, v5}, Lcom/android/server/tof/AppFeatureHelper;->hexStringToInt(Ljava/lang/String;)I

    move-result v12

    .line 187
    .local v12, "feature":I
    const/4 v5, 0x4

    aget-object v9, v3, v5

    invoke-virtual {v9}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_2

    move v13, v12

    goto :goto_0

    :cond_2
    aget-object v5, v3, v5

    invoke-direct {v1, v5}, Lcom/android/server/tof/AppFeatureHelper;->hexStringToInt(Ljava/lang/String;)I

    move-result v5

    move v13, v5

    .line 188
    .local v13, "featureVertical":I
    :goto_0
    const/4 v5, 0x5

    aget-object v5, v3, v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v14

    .line 189
    .local v14, "minVersionCode":I
    const/4 v5, 0x6

    aget-object v5, v3, v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v15

    .line 190
    .local v15, "maxVersionCode":I
    const/4 v5, 0x7

    aget-object v5, v3, v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    if-ne v5, v8, :cond_3

    move/from16 v16, v8

    goto :goto_1

    :cond_3
    move/from16 v16, v0

    .line 192
    .local v16, "enabled":Z
    :goto_1
    new-instance v0, Lcom/android/server/tof/TofGestureComponent;

    move-object v8, v0

    move-object v9, v4

    move-object/from16 v10, v17

    invoke-direct/range {v8 .. v16}, Lcom/android/server/tof/TofGestureComponent;-><init>(Ljava/lang/String;Ljava/lang/String;IIIIIZ)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 194
    .end local v11    # "category":I
    .end local v12    # "feature":I
    .end local v13    # "featureVertical":I
    .end local v14    # "minVersionCode":I
    .end local v15    # "maxVersionCode":I
    .end local v16    # "enabled":Z
    :catch_0
    move-exception v0

    .line 195
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v7, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    return-object v6
.end method

.method private parseTofGestureComponentFromRes(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 158
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x11030057

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 159
    .local v0, "array":[Ljava/lang/String;
    if-nez v0, :cond_0

    .line 160
    const-string v1, "AppFeatureHelper"

    const-string v2, "get config_tofComponentSupport resource error."

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    return-void

    .line 164
    :cond_0
    invoke-static {v0}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/server/tof/AppFeatureHelper;->setResGestureConfig(Ljava/lang/String;)V

    .line 165
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_1

    .line 166
    aget-object v2, v0, v1

    invoke-direct {p0, v2}, Lcom/android/server/tof/AppFeatureHelper;->parseTofGestureComponent(Ljava/lang/String;)Lcom/android/server/tof/TofGestureComponent;

    move-result-object v2

    .line 167
    .local v2, "gestureApp":Lcom/android/server/tof/TofGestureComponent;
    invoke-direct {p0, v2}, Lcom/android/server/tof/AppFeatureHelper;->addTofGestureAppConfig(Lcom/android/server/tof/TofGestureComponent;)V

    .line 165
    .end local v2    # "gestureApp":Lcom/android/server/tof/TofGestureComponent;
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 169
    .end local v1    # "index":I
    :cond_1
    return-void
.end method


# virtual methods
.method public changeTofGestureAppConfig(Ljava/lang/String;Z)Z
    .locals 2
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "enable"    # Z

    .line 259
    iget-object v0, p0, Lcom/android/server/tof/AppFeatureHelper;->mTofGestureAppConfigs:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 260
    .local v0, "tofGestureComponents":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/tof/TofGestureComponent;>;"
    if-eqz v0, :cond_0

    .line 261
    iget-object v1, p0, Lcom/android/server/tof/AppFeatureHelper;->mContext:Landroid/content/Context;

    invoke-static {v1, p1, p2}, Lcom/android/server/tof/AppStatusManager;->setAppEnable(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 262
    const/4 v1, 0x1

    return v1

    .line 264
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method public getAppCategory(Landroid/content/ComponentName;)I
    .locals 2
    .param p1, "componentName"    # Landroid/content/ComponentName;

    .line 78
    if-eqz p1, :cond_0

    .line 79
    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/android/server/tof/AppFeatureHelper;->getAppCategory(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0

    .line 81
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public getCloudGestureConfig()Ljava/lang/String;
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/android/server/tof/AppFeatureHelper;->mCloudGestureConfig:Ljava/lang/String;

    return-object v0
.end method

.method public getResGestureConfig()Ljava/lang/String;
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/android/server/tof/AppFeatureHelper;->mResGestureConfig:Ljava/lang/String;

    return-object v0
.end method

.method public getSupportComponentFromConfig(Landroid/content/ComponentName;)Lcom/android/server/tof/TofGestureComponent;
    .locals 3
    .param p1, "componentName"    # Landroid/content/ComponentName;

    .line 103
    if-eqz p1, :cond_0

    .line 104
    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 105
    invoke-virtual {p1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    .line 104
    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/server/tof/AppFeatureHelper;->getSupportComponentFromConfig(Ljava/lang/String;Ljava/lang/String;Z)Lcom/android/server/tof/TofGestureComponent;

    move-result-object v0

    return-object v0

    .line 107
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSupportComponentFromConfig(Ljava/lang/String;Ljava/lang/String;Z)Lcom/android/server/tof/TofGestureComponent;
    .locals 9
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "activity"    # Ljava/lang/String;
    .param p3, "includeDisable"    # Z

    .line 112
    iget-object v0, p0, Lcom/android/server/tof/AppFeatureHelper;->mTofGestureAppConfigs:Ljava/util/HashMap;

    monitor-enter v0

    .line 113
    :try_start_0
    iget-object v1, p0, Lcom/android/server/tof/AppFeatureHelper;->mTofGestureAppConfigs:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 114
    .local v1, "tofGestureComponents":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/tof/TofGestureComponent;>;"
    if-eqz v1, :cond_4

    .line 116
    iget-object v2, p0, Lcom/android/server/tof/AppFeatureHelper;->mContext:Landroid/content/Context;

    invoke-static {v2, p1}, Lcom/android/server/tof/AppStatusManager;->statusHasChanged(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 117
    iget-object v2, p0, Lcom/android/server/tof/AppFeatureHelper;->mContext:Landroid/content/Context;

    invoke-static {v2, p1}, Lcom/android/server/tof/AppStatusManager;->isAppEnabled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    .local v2, "enabled":Z
    goto :goto_0

    .line 119
    .end local v2    # "enabled":Z
    :cond_0
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/tof/TofGestureComponent;

    invoke-virtual {v2}, Lcom/android/server/tof/TofGestureComponent;->isEnable()Z

    move-result v2

    .line 121
    .restart local v2    # "enabled":Z
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/tof/TofGestureComponent;

    .line 122
    .local v4, "tofGestureComponent":Lcom/android/server/tof/TofGestureComponent;
    invoke-direct {p0, p1}, Lcom/android/server/tof/AppFeatureHelper;->getPackageInfo(Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v5

    .line 123
    .local v5, "info":Landroid/content/pm/PackageInfo;
    iget v6, v5, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-virtual {v4, v6}, Lcom/android/server/tof/TofGestureComponent;->isVersionSupported(I)Z

    move-result v6

    if-eqz v6, :cond_3

    if-nez v2, :cond_1

    if-eqz p3, :cond_3

    .line 125
    :cond_1
    invoke-virtual {v4}, Lcom/android/server/tof/TofGestureComponent;->getActivityName()Ljava/lang/String;

    move-result-object v6

    .line 126
    .local v6, "configActivity":Ljava/lang/String;
    invoke-virtual {v4}, Lcom/android/server/tof/TofGestureComponent;->getPackageName()Ljava/lang/String;

    move-result-object v7

    .line 128
    .local v7, "configPackageName":Ljava/lang/String;
    invoke-virtual {p2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 129
    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-virtual {v7, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 130
    :cond_2
    monitor-exit v0

    return-object v4

    .line 133
    .end local v4    # "tofGestureComponent":Lcom/android/server/tof/TofGestureComponent;
    .end local v5    # "info":Landroid/content/pm/PackageInfo;
    .end local v6    # "configActivity":Ljava/lang/String;
    .end local v7    # "configPackageName":Ljava/lang/String;
    :cond_3
    goto :goto_1

    .line 135
    .end local v2    # "enabled":Z
    :cond_4
    monitor-exit v0

    const/4 v0, 0x0

    return-object v0

    .line 136
    .end local v1    # "tofGestureComponents":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/tof/TofGestureComponent;>;"
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getSupportFeature(Landroid/content/ComponentName;)I
    .locals 1
    .param p1, "componentName"    # Landroid/content/ComponentName;

    .line 74
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/server/tof/AppFeatureHelper;->getSupportFeature(Landroid/content/ComponentName;Z)I

    move-result v0

    return v0
.end method

.method public getSupportFeature(Landroid/content/ComponentName;Z)I
    .locals 2
    .param p1, "componentName"    # Landroid/content/ComponentName;
    .param p2, "vertical"    # Z

    .line 67
    if-eqz p1, :cond_0

    .line 68
    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1, p2}, Lcom/android/server/tof/AppFeatureHelper;->getSupportFeature(Ljava/lang/String;Ljava/lang/String;Z)I

    move-result v0

    return v0

    .line 70
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public getTofGestureAppData()Lcom/miui/tof/gesture/TofGestureAppData;
    .locals 6

    .line 232
    new-instance v0, Lcom/miui/tof/gesture/TofGestureAppData;

    invoke-direct {v0}, Lcom/miui/tof/gesture/TofGestureAppData;-><init>()V

    .line 233
    .local v0, "gestureAppData":Lcom/miui/tof/gesture/TofGestureAppData;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 234
    .local v1, "tofGestureAppDetailInfos":Ljava/util/List;, "Ljava/util/List<Lcom/miui/tof/gesture/TofGestureAppDetailInfo;>;"
    iget-object v2, p0, Lcom/android/server/tof/AppFeatureHelper;->mTofGestureAppConfigs:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 235
    .local v3, "pkgName":Ljava/lang/String;
    invoke-direct {p0, v3}, Lcom/android/server/tof/AppFeatureHelper;->getPackageInfo(Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v4

    .line 236
    .local v4, "info":Landroid/content/pm/PackageInfo;
    if-nez v4, :cond_0

    .line 237
    goto :goto_0

    .line 239
    :cond_0
    invoke-direct {p0, v4}, Lcom/android/server/tof/AppFeatureHelper;->getSupportTofGestureAppDetailInfo(Landroid/content/pm/PackageInfo;)Lcom/miui/tof/gesture/TofGestureAppDetailInfo;

    move-result-object v5

    .line 240
    .local v5, "appDetailInfo":Lcom/miui/tof/gesture/TofGestureAppDetailInfo;
    if-eqz v5, :cond_1

    .line 241
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 243
    .end local v3    # "pkgName":Ljava/lang/String;
    .end local v4    # "info":Landroid/content/pm/PackageInfo;
    .end local v5    # "appDetailInfo":Lcom/miui/tof/gesture/TofGestureAppDetailInfo;
    :cond_1
    goto :goto_0

    .line 244
    :cond_2
    invoke-virtual {v0, v1}, Lcom/miui/tof/gesture/TofGestureAppData;->setTofGestureAppDetailInfoList(Ljava/util/List;)V

    .line 245
    return-object v0
.end method

.method public initTofComponentConfig(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 140
    iput-object p1, p0, Lcom/android/server/tof/AppFeatureHelper;->mContext:Landroid/content/Context;

    .line 141
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tof/AppFeatureHelper;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 142
    invoke-direct {p0, p1}, Lcom/android/server/tof/AppFeatureHelper;->parseTofGestureComponentFromRes(Landroid/content/Context;)V

    .line 143
    return-void
.end method

.method public parseContactlessGestureComponentFromCloud(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 146
    .local p1, "componentFeatureList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz p1, :cond_2

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 149
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/tof/AppFeatureHelper;->setCloudGestureConfig(Ljava/lang/String;)V

    .line 150
    iget-object v0, p0, Lcom/android/server/tof/AppFeatureHelper;->mTofGestureAppConfigs:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 151
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 152
    .local v1, "item":Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/android/server/tof/AppFeatureHelper;->parseTofGestureComponent(Ljava/lang/String;)Lcom/android/server/tof/TofGestureComponent;

    move-result-object v2

    .line 153
    .local v2, "gestureApp":Lcom/android/server/tof/TofGestureComponent;
    invoke-direct {p0, v2}, Lcom/android/server/tof/AppFeatureHelper;->addTofGestureAppConfig(Lcom/android/server/tof/TofGestureComponent;)V

    .line 154
    .end local v1    # "item":Ljava/lang/String;
    .end local v2    # "gestureApp":Lcom/android/server/tof/TofGestureComponent;
    goto :goto_0

    .line 155
    :cond_1
    return-void

    .line 147
    :cond_2
    :goto_1
    return-void
.end method

.method public setCloudGestureConfig(Ljava/lang/String;)V
    .locals 0
    .param p1, "mCloudGestureConfig"    # Ljava/lang/String;

    .line 63
    iput-object p1, p0, Lcom/android/server/tof/AppFeatureHelper;->mCloudGestureConfig:Ljava/lang/String;

    .line 64
    return-void
.end method

.method public setResGestureConfig(Ljava/lang/String;)V
    .locals 0
    .param p1, "mResGestureConfig"    # Ljava/lang/String;

    .line 55
    iput-object p1, p0, Lcom/android/server/tof/AppFeatureHelper;->mResGestureConfig:Ljava/lang/String;

    .line 56
    return-void
.end method
