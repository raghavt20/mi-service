public class com.android.server.tof.AppStatusManager {
	 /* .source "AppStatusManager.java" */
	 /* # static fields */
	 private static final java.lang.String GESTURE_DISABLED_APPS_KEY;
	 private static final java.lang.String GESTURE_ENABLED_APPS_KEY;
	 private static java.util.HashMap instances;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/HashMap<", */
	 /* "Ljava/lang/String;", */
	 /* "Lcom/android/server/tof/AppStatusManager;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
/* # instance fields */
private java.util.Set mCachedDisabledApps;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.Set mCachedEnabledApps;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.content.Context mContext;
private java.lang.String mUserId;
/* # direct methods */
public static void $r8$lambda$7hnc7QMZIQEz3YuOOGpUzGa5I88 ( com.android.server.tof.AppStatusManager p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/tof/AppStatusManager;->lambda$saveDisabledAppsAsync$0()V */
return;
} // .end method
public static void $r8$lambda$nScmJ-B-RuACglFtwT8jCjE7gK0 ( com.android.server.tof.AppStatusManager p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/tof/AppStatusManager;->lambda$saveEnabledAppsAsync$1()V */
return;
} // .end method
static com.android.server.tof.AppStatusManager ( ) {
/* .locals 1 */
/* .line 25 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
return;
} // .end method
private com.android.server.tof.AppStatusManager ( ) {
/* .locals 0 */
/* .param p1, "userId" # Ljava/lang/String; */
/* .param p2, "context" # Landroid/content/Context; */
/* .line 39 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 40 */
this.mUserId = p1;
/* .line 41 */
this.mContext = p2;
/* .line 42 */
/* invoke-direct {p0}, Lcom/android/server/tof/AppStatusManager;->init()V */
/* .line 43 */
return;
} // .end method
private static com.android.server.tof.AppStatusManager getOrCreateInstance ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 235 */
v0 = android.app.ActivityManager .getCurrentUser ( );
java.lang.String .valueOf ( v0 );
/* .line 236 */
/* .local v0, "userId":Ljava/lang/String; */
v1 = com.android.server.tof.AppStatusManager.instances;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = ""; // const-string v3, ""
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
v1 = (( java.util.HashMap ) v1 ).containsKey ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 237 */
v1 = com.android.server.tof.AppStatusManager.instances;
(( java.util.HashMap ) v1 ).get ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Lcom/android/server/tof/AppStatusManager; */
/* .line 240 */
} // :cond_0
/* new-instance v1, Lcom/android/server/tof/AppStatusManager; */
/* invoke-direct {v1, v0, p0}, Lcom/android/server/tof/AppStatusManager;-><init>(Ljava/lang/String;Landroid/content/Context;)V */
/* .line 241 */
/* .local v1, "appManager":Lcom/android/server/tof/AppStatusManager; */
v2 = com.android.server.tof.AppStatusManager.instances;
(( java.util.HashMap ) v2 ).put ( v0, v1 ); // invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 242 */
} // .end method
private void init ( ) {
/* .locals 5 */
/* .line 131 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.mCachedDisabledApps = v0;
/* .line 132 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.mCachedEnabledApps = v0;
/* .line 133 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "gesture_disabled_apps_"; // const-string v2, "gesture_disabled_apps_"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mUserId;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.provider.Settings$Secure .getString ( v0,v1 );
/* .line 135 */
/* .local v0, "disableApps":Ljava/lang/String; */
v1 = android.text.TextUtils .isEmpty ( v0 );
final String v2 = ","; // const-string v2, ","
/* if-nez v1, :cond_0 */
/* .line 136 */
v1 = this.mCachedDisabledApps;
android.text.TextUtils .split ( v0,v2 );
java.util.Arrays .asList ( v3 );
/* .line 138 */
} // :cond_0
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "gesture_enabled_apps_"; // const-string v4, "gesture_enabled_apps_"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.mUserId;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.provider.Settings$Secure .getString ( v1,v3 );
/* .line 140 */
/* .local v1, "enableApps":Ljava/lang/String; */
v3 = android.text.TextUtils .isEmpty ( v1 );
/* if-nez v3, :cond_1 */
/* .line 141 */
v3 = this.mCachedEnabledApps;
android.text.TextUtils .split ( v1,v2 );
java.util.Arrays .asList ( v2 );
/* .line 143 */
} // :cond_1
return;
} // .end method
public static Boolean isAppDisabled ( android.content.Context p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .line 177 */
/* if-nez p0, :cond_0 */
/* .line 178 */
int v0 = 0; // const/4 v0, 0x0
/* .line 180 */
} // :cond_0
com.android.server.tof.AppStatusManager .getOrCreateInstance ( p0 );
/* .line 181 */
/* .local v0, "appStatusManager":Lcom/android/server/tof/AppStatusManager; */
v1 = (( com.android.server.tof.AppStatusManager ) v0 ).isDisabled ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/tof/AppStatusManager;->isDisabled(Ljava/lang/String;)Z
} // .end method
public static Boolean isAppEnabled ( android.content.Context p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .line 191 */
/* if-nez p0, :cond_0 */
/* .line 192 */
int v0 = 0; // const/4 v0, 0x0
/* .line 194 */
} // :cond_0
com.android.server.tof.AppStatusManager .getOrCreateInstance ( p0 );
/* .line 195 */
/* .local v0, "appStatusManager":Lcom/android/server/tof/AppStatusManager; */
v1 = (( com.android.server.tof.AppStatusManager ) v0 ).isEnabled ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/tof/AppStatusManager;->isEnabled(Ljava/lang/String;)Z
} // .end method
private void lambda$saveDisabledAppsAsync$0 ( ) { //synthethic
/* .locals 4 */
/* .line 151 */
final String v0 = ","; // const-string v0, ","
v1 = this.mCachedDisabledApps;
android.text.TextUtils .join ( v0,v1 );
/* .line 152 */
/* .local v0, "appsString":Ljava/lang/String; */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "gesture_disabled_apps_"; // const-string v3, "gesture_disabled_apps_"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mUserId;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.provider.Settings$Secure .putString ( v1,v2,v0 );
/* .line 154 */
return;
} // .end method
private void lambda$saveEnabledAppsAsync$1 ( ) { //synthethic
/* .locals 4 */
/* .line 163 */
final String v0 = ","; // const-string v0, ","
v1 = this.mCachedEnabledApps;
android.text.TextUtils .join ( v0,v1 );
/* .line 164 */
/* .local v0, "appsString":Ljava/lang/String; */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "gesture_enabled_apps_"; // const-string v3, "gesture_enabled_apps_"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mUserId;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.provider.Settings$Secure .putString ( v1,v2,v0 );
/* .line 166 */
return;
} // .end method
private void saveDisabledAppsAsync ( ) {
/* .locals 2 */
/* .line 149 */
com.android.internal.os.BackgroundThread .get ( );
(( com.android.internal.os.BackgroundThread ) v0 ).getThreadHandler ( ); // invoke-virtual {v0}, Lcom/android/internal/os/BackgroundThread;->getThreadHandler()Landroid/os/Handler;
/* new-instance v1, Lcom/android/server/tof/AppStatusManager$$ExternalSyntheticLambda1; */
/* invoke-direct {v1, p0}, Lcom/android/server/tof/AppStatusManager$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/tof/AppStatusManager;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 155 */
return;
} // .end method
private void saveEnabledAppsAsync ( ) {
/* .locals 2 */
/* .line 161 */
com.android.internal.os.BackgroundThread .get ( );
(( com.android.internal.os.BackgroundThread ) v0 ).getThreadHandler ( ); // invoke-virtual {v0}, Lcom/android/internal/os/BackgroundThread;->getThreadHandler()Landroid/os/Handler;
/* new-instance v1, Lcom/android/server/tof/AppStatusManager$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0}, Lcom/android/server/tof/AppStatusManager$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/tof/AppStatusManager;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 167 */
return;
} // .end method
public static void setAppEnable ( android.content.Context p0, java.lang.String p1, Boolean p2 ) {
/* .locals 1 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "enable" # Z */
/* .line 219 */
if ( p0 != null) { // if-eqz p0, :cond_1
v0 = android.text.TextUtils .isEmpty ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 222 */
} // :cond_0
com.android.server.tof.AppStatusManager .getOrCreateInstance ( p0 );
/* .line 223 */
/* .local v0, "appStatusManager":Lcom/android/server/tof/AppStatusManager; */
(( com.android.server.tof.AppStatusManager ) v0 ).setEnable ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/tof/AppStatusManager;->setEnable(Ljava/lang/String;Z)V
/* .line 224 */
return;
/* .line 220 */
} // .end local v0 # "appStatusManager":Lcom/android/server/tof/AppStatusManager;
} // :cond_1
} // :goto_0
return;
} // .end method
public static Boolean statusHasChanged ( android.content.Context p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .line 205 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p0, :cond_0 */
/* .line 206 */
/* .line 208 */
} // :cond_0
v1 = com.android.server.tof.AppStatusManager .isAppDisabled ( p0,p1 );
/* if-nez v1, :cond_1 */
v1 = com.android.server.tof.AppStatusManager .isAppEnabled ( p0,p1 );
if ( v1 != null) { // if-eqz v1, :cond_2
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
} // :cond_2
} // .end method
/* # virtual methods */
public synchronized void addDisabledApp ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "appPackageName" # Ljava/lang/String; */
/* monitor-enter p0 */
/* .line 52 */
try { // :try_start_0
v0 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v0, :cond_0 */
v0 = v0 = this.mCachedDisabledApps;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 53 */
/* invoke-direct {p0}, Lcom/android/server/tof/AppStatusManager;->saveDisabledAppsAsync()V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 55 */
} // .end local p0 # "this":Lcom/android/server/tof/AppStatusManager;
} // :cond_0
/* monitor-exit p0 */
return;
/* .line 51 */
} // .end local p1 # "appPackageName":Ljava/lang/String;
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
public synchronized void addEnabledApp ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "appPackageName" # Ljava/lang/String; */
/* monitor-enter p0 */
/* .line 76 */
try { // :try_start_0
v0 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v0, :cond_0 */
v0 = v0 = this.mCachedEnabledApps;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 77 */
/* invoke-direct {p0}, Lcom/android/server/tof/AppStatusManager;->saveEnabledAppsAsync()V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 79 */
} // .end local p0 # "this":Lcom/android/server/tof/AppStatusManager;
} // :cond_0
/* monitor-exit p0 */
return;
/* .line 75 */
} // .end local p1 # "appPackageName":Ljava/lang/String;
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
public Boolean isDisabled ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .line 119 */
v0 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v0, :cond_0 */
v0 = v0 = this.mCachedDisabledApps;
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean isEnabled ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .line 123 */
v0 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v0, :cond_0 */
v0 = v0 = this.mCachedEnabledApps;
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public synchronized void removeDisabledApp ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "appPackageName" # Ljava/lang/String; */
/* monitor-enter p0 */
/* .line 64 */
try { // :try_start_0
v0 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v0, :cond_0 */
v0 = v0 = this.mCachedDisabledApps;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 65 */
/* invoke-direct {p0}, Lcom/android/server/tof/AppStatusManager;->saveDisabledAppsAsync()V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 67 */
} // .end local p0 # "this":Lcom/android/server/tof/AppStatusManager;
} // :cond_0
/* monitor-exit p0 */
return;
/* .line 63 */
} // .end local p1 # "appPackageName":Ljava/lang/String;
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
public synchronized void removeEnabledApp ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "appPackageName" # Ljava/lang/String; */
/* monitor-enter p0 */
/* .line 88 */
try { // :try_start_0
v0 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v0, :cond_0 */
v0 = v0 = this.mCachedEnabledApps;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 89 */
/* invoke-direct {p0}, Lcom/android/server/tof/AppStatusManager;->saveEnabledAppsAsync()V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 91 */
} // .end local p0 # "this":Lcom/android/server/tof/AppStatusManager;
} // :cond_0
/* monitor-exit p0 */
return;
/* .line 87 */
} // .end local p1 # "appPackageName":Ljava/lang/String;
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
public void setEnable ( java.lang.String p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "enable" # Z */
/* .line 99 */
v0 = android.text.TextUtils .isEmpty ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 100 */
return;
/* .line 103 */
} // :cond_0
if ( p2 != null) { // if-eqz p2, :cond_1
/* .line 104 */
(( com.android.server.tof.AppStatusManager ) p0 ).removeDisabledApp ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/tof/AppStatusManager;->removeDisabledApp(Ljava/lang/String;)V
/* .line 105 */
(( com.android.server.tof.AppStatusManager ) p0 ).addEnabledApp ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/tof/AppStatusManager;->addEnabledApp(Ljava/lang/String;)V
/* .line 107 */
} // :cond_1
(( com.android.server.tof.AppStatusManager ) p0 ).addDisabledApp ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/tof/AppStatusManager;->addDisabledApp(Ljava/lang/String;)V
/* .line 108 */
(( com.android.server.tof.AppStatusManager ) p0 ).removeEnabledApp ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/tof/AppStatusManager;->removeEnabledApp(Ljava/lang/String;)V
/* .line 110 */
} // :goto_0
return;
} // .end method
