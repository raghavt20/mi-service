public abstract class com.android.server.tof.ContactlessGestureController {
	 /* .source "ContactlessGestureController.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/tof/ContactlessGestureController$TofClientConnecton;, */
	 /* Lcom/android/server/tof/ContactlessGestureController$AccessibilityTelephonyCallback;, */
	 /* Lcom/android/server/tof/ContactlessGestureController$ScreenStateReceiver; */
	 /* } */
} // .end annotation
/* # static fields */
public static final Integer AON_GESTURE_DOWN_STATE;
public static final Integer AON_GESTURE_LEFT_STATE;
public static final Integer AON_GESTURE_RIGHT_STATE;
public static final Integer AON_GESTURE_TRIGGER_DOWN;
public static final Integer AON_GESTURE_TRIGGER_LEFT;
public static final Integer AON_GESTURE_TRIGGER_RIGHT;
public static final Integer AON_GESTURE_TRIGGER_UP;
public static final Integer AON_GESTURE_UP_STATE;
private static final Integer CHECK_TOP_ACTIVITY_DELAY;
private static final Integer DELAY;
private static final Integer FLAG_GESTURE_DOWN_STATE_FEATURE;
private static final Integer FLAG_GESTURE_INVALIDATE_SCENARIO_FEATURE;
private static final Integer FLAG_GESTURE_LEFT_STATE_FEATURE;
private static final Integer FLAG_GESTURE_RIGHT_STATE_FEATURE;
private static final Integer FLAG_GESTURE_TRIGGER_DOWN_FEATURE;
private static final Integer FLAG_GESTURE_TRIGGER_LEFT_FEATURE;
private static final Integer FLAG_GESTURE_TRIGGER_RIGHT_FEATURE;
private static final Integer FLAG_GESTURE_TRIGGER_UP_FEATURE;
private static final Integer FLAG_GESTURE_UN_TRIGGER_FEATURE;
private static final Integer FLAG_GESTURE_UP_STATE_FEATURE;
private static final Integer FLAG_TOF_GESTURE_TRIGGER_FEATURE;
private static final Integer FLAG_TOF_GESTURE_WORKING_FEATURE;
public static final Integer GESTURE_GESTURE_WORKING;
public static final Integer GESTURE_INVALIDATE_SCENARIO;
private static final Integer SCENE_ACTIVITY_FOCUS_CHANGE;
private static final Integer SCENE_CALL_STATE_CHANGE;
private static final Integer SCENE_DISPLAY_ROTATION_CHANGE;
private static final Integer SCENE_DISPLAY_STATE_CHANGE;
public static final Integer SCENE_OTHER_CHANGE;
private static final Integer SCENE_TOP_ACTIVITY_CHANGE;
private static final Long SERVICE_BIND_AWAIT_MILLIS;
private static final java.lang.String TAG;
public static final Integer TOF_GESTURE_DOUBLE_PRESS;
public static final Integer TOF_GESTURE_DOWN;
public static final Integer TOF_GESTURE_DRAW_CIRCLE;
public static final Integer TOF_GESTURE_LEFT;
public static final Integer TOF_GESTURE_RIGHT;
public static final Integer TOF_GESTURE_TRIGGER;
public static final Integer TOF_GESTURE_UN_TRIGGER;
public static final Integer TOF_GESTURE_UP;
/* # instance fields */
private android.app.IActivityManager mActivityManager;
android.app.IMiuiActivityObserver mActivityStateObserver;
private com.android.server.tof.AppFeatureHelper mAppFeatureHelper;
protected android.content.ComponentName mComponentName;
private com.android.server.tof.ContactlessGestureService mContactlessGestureService;
protected android.content.Context mContext;
private android.hardware.display.DisplayManager mDisplayManager;
protected Integer mDisplayRotation;
private java.lang.String mFocusedPackage;
protected android.os.Handler mHandler;
private com.android.server.tof.InputHelper mInputHelper;
private Boolean mInteractive;
private Boolean mIsDeskTopMode;
private Boolean mIsSplitScreenMode;
protected Boolean mIsTrigger;
private Integer mLastTriggerLabel;
private com.android.server.policy.WindowManagerPolicy mPolicy;
private com.android.server.power.PowerManagerServiceStub mPowerManagerServiceImpl;
private Boolean mRegisterGesture;
private java.lang.Runnable mRegisterGestureRunnable;
private android.content.BroadcastReceiver mScreenReceiver;
protected java.util.concurrent.CountDownLatch mServiceBindingLatch;
private volatile Boolean mServiceIsBinding;
Boolean mShouldUpdateTriggerView;
private final android.telephony.TelephonyCallback mTelephonyCallback;
private android.telephony.TelephonyManager mTelephonyManager;
private com.android.server.tof.ContactlessGestureController$TofClientConnecton mTofClientConnection;
private Integer mTofGestureDoublePressSupportFeature;
private Integer mTofGestureDownSupportFeature;
private Integer mTofGestureDrawCircleSupportFeature;
private Integer mTofGestureLeftSupportFeature;
private Integer mTofGestureRightSupportFeature;
private Integer mTofGestureUpSupportFeature;
private android.content.ComponentName mTopActivity;
private Boolean mTopActivityFullScreenOrNotOccluded;
private Boolean mTriggerUiSuccess;
private java.lang.Runnable mUpdateTopActivityIfNeededRunnable;
private com.android.server.wm.WindowManagerService mWindowManagerService;
/* # direct methods */
public static void $r8$lambda$-gNbufUTUtH5sJHi_HrsvQHoPME ( com.android.server.tof.ContactlessGestureController p0 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureController;->lambda$handleGestureEvent$4()V */
	 return;
} // .end method
public static void $r8$lambda$Qirqa5DXyu7XBB3wf9nUln8QeZE ( com.android.server.tof.ContactlessGestureController p0 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureController;->lambda$updateFocusedPackage$2()V */
	 return;
} // .end method
public static void $r8$lambda$QkWLFcRB17zPNO-yrb_ugDE4u2E ( com.android.server.tof.ContactlessGestureController p0 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureController;->lambda$updateFocusedPackage$1()V */
	 return;
} // .end method
public static void $r8$lambda$oYK4Q2xpEv2AxTXWRspQRdw2oPE ( com.android.server.tof.ContactlessGestureController p0 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureController;->lambda$new$3()V */
	 return;
} // .end method
public static void $r8$lambda$teGRhQEnVNiv4RR3mLliuYjsoP0 ( com.android.server.tof.ContactlessGestureController p0 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureController;->lambda$unBindService$0()V */
	 return;
} // .end method
static android.app.IActivityManager -$$Nest$fgetmActivityManager ( com.android.server.tof.ContactlessGestureController p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mActivityManager;
} // .end method
static Boolean -$$Nest$fgetmIsDeskTopMode ( com.android.server.tof.ContactlessGestureController p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/android/server/tof/ContactlessGestureController;->mIsDeskTopMode:Z */
} // .end method
static Boolean -$$Nest$fgetmIsSplitScreenMode ( com.android.server.tof.ContactlessGestureController p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/android/server/tof/ContactlessGestureController;->mIsSplitScreenMode:Z */
} // .end method
static com.android.server.tof.ContactlessGestureController$TofClientConnecton -$$Nest$fgetmTofClientConnection ( com.android.server.tof.ContactlessGestureController p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mTofClientConnection;
} // .end method
static android.content.ComponentName -$$Nest$fgetmTopActivity ( com.android.server.tof.ContactlessGestureController p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mTopActivity;
} // .end method
static Boolean -$$Nest$fgetmTopActivityFullScreenOrNotOccluded ( com.android.server.tof.ContactlessGestureController p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/android/server/tof/ContactlessGestureController;->mTopActivityFullScreenOrNotOccluded:Z */
} // .end method
static void -$$Nest$fputmInteractive ( com.android.server.tof.ContactlessGestureController p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/tof/ContactlessGestureController;->mInteractive:Z */
	 return;
} // .end method
static void -$$Nest$fputmIsSplitScreenMode ( com.android.server.tof.ContactlessGestureController p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/tof/ContactlessGestureController;->mIsSplitScreenMode:Z */
	 return;
} // .end method
static void -$$Nest$fputmServiceIsBinding ( com.android.server.tof.ContactlessGestureController p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/tof/ContactlessGestureController;->mServiceIsBinding:Z */
	 return;
} // .end method
static void -$$Nest$fputmTopActivity ( com.android.server.tof.ContactlessGestureController p0, android.content.ComponentName p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 this.mTopActivity = p1;
	 return;
} // .end method
static void -$$Nest$fputmTopActivityFullScreenOrNotOccluded ( com.android.server.tof.ContactlessGestureController p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/tof/ContactlessGestureController;->mTopActivityFullScreenOrNotOccluded:Z */
	 return;
} // .end method
static void -$$Nest$mcheckTopActivities ( com.android.server.tof.ContactlessGestureController p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureController;->checkTopActivities()V */
	 return;
} // .end method
static void -$$Nest$mupdateDefaultDisplayRotation ( com.android.server.tof.ContactlessGestureController p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureController;->updateDefaultDisplayRotation()V */
	 return;
} // .end method
public com.android.server.tof.ContactlessGestureController ( ) {
	 /* .locals 4 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .param p2, "handler" # Landroid/os/Handler; */
	 /* .param p3, "contactlessGestureService" # Lcom/android/server/tof/ContactlessGestureService; */
	 /* .line 277 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 145 */
	 /* const v0, 0x14014 */
	 /* iput v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureLeftSupportFeature:I */
	 /* .line 150 */
	 /* const v0, 0x2200a */
	 /* iput v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureRightSupportFeature:I */
	 /* .line 157 */
	 /* const/16 v0, 0x1440 */
	 /* iput v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureDownSupportFeature:I */
	 /* .line 163 */
	 /* const/16 v0, 0xa20 */
	 /* iput v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureUpSupportFeature:I */
	 /* .line 168 */
	 int v0 = 1; // const/4 v0, 0x1
	 /* iput v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureDoublePressSupportFeature:I */
	 /* .line 173 */
	 /* const/16 v1, 0x180 */
	 /* iput v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureDrawCircleSupportFeature:I */
	 /* .line 257 */
	 /* iput-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mInteractive:Z */
	 /* .line 259 */
	 /* iput-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mTopActivityFullScreenOrNotOccluded:Z */
	 /* .line 475 */
	 /* new-instance v1, Lcom/android/server/tof/ContactlessGestureController$$ExternalSyntheticLambda3; */
	 /* invoke-direct {v1, p0}, Lcom/android/server/tof/ContactlessGestureController$$ExternalSyntheticLambda3;-><init>(Lcom/android/server/tof/ContactlessGestureController;)V */
	 this.mRegisterGestureRunnable = v1;
	 /* .line 792 */
	 /* new-instance v1, Lcom/android/server/tof/ContactlessGestureController$1; */
	 /* invoke-direct {v1, p0}, Lcom/android/server/tof/ContactlessGestureController$1;-><init>(Lcom/android/server/tof/ContactlessGestureController;)V */
	 this.mUpdateTopActivityIfNeededRunnable = v1;
	 /* .line 1090 */
	 /* new-instance v1, Lcom/android/server/tof/ContactlessGestureController$3; */
	 /* invoke-direct {v1, p0}, Lcom/android/server/tof/ContactlessGestureController$3;-><init>(Lcom/android/server/tof/ContactlessGestureController;)V */
	 this.mActivityStateObserver = v1;
	 /* .line 278 */
	 this.mContext = p1;
	 /* .line 279 */
	 /* new-instance v1, Landroid/os/Handler; */
	 (( android.os.Handler ) p2 ).getLooper ( ); // invoke-virtual {p2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;
	 /* invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
	 this.mHandler = v1;
	 /* .line 280 */
	 /* new-instance v1, Lcom/android/server/tof/ContactlessGestureController$TofClientConnecton; */
	 int v2 = 0; // const/4 v2, 0x0
	 /* invoke-direct {v1, p0, v2}, Lcom/android/server/tof/ContactlessGestureController$TofClientConnecton;-><init>(Lcom/android/server/tof/ContactlessGestureController;Lcom/android/server/tof/ContactlessGestureController$TofClientConnecton-IA;)V */
	 this.mTofClientConnection = v1;
	 /* .line 281 */
	 /* new-instance v1, Lcom/android/server/tof/InputHelper; */
	 /* invoke-direct {v1}, Lcom/android/server/tof/InputHelper;-><init>()V */
	 this.mInputHelper = v1;
	 /* .line 282 */
	 android.app.ActivityManager .getService ( );
	 this.mActivityManager = v1;
	 /* .line 283 */
	 this.mContactlessGestureService = p3;
	 /* .line 284 */
	 v1 = this.mContext;
	 final String v3 = "phone"; // const-string v3, "phone"
	 (( android.content.Context ) v1 ).getSystemService ( v3 ); // invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
	 /* check-cast v1, Landroid/telephony/TelephonyManager; */
	 this.mTelephonyManager = v1;
	 /* .line 285 */
	 /* const-string/jumbo v1, "window" */
	 android.os.ServiceManager .getService ( v1 );
	 /* check-cast v1, Lcom/android/server/wm/WindowManagerService; */
	 this.mWindowManagerService = v1;
	 /* .line 286 */
	 v1 = this.mContext;
	 final String v3 = "display"; // const-string v3, "display"
	 (( android.content.Context ) v1 ).getSystemService ( v3 ); // invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
	 /* check-cast v1, Landroid/hardware/display/DisplayManager; */
	 this.mDisplayManager = v1;
	 /* .line 287 */
	 /* const-class v1, Lcom/android/server/policy/WindowManagerPolicy; */
	 com.android.server.LocalServices .getService ( v1 );
	 /* check-cast v1, Lcom/android/server/policy/WindowManagerPolicy; */
	 this.mPolicy = v1;
	 /* .line 288 */
	 /* new-instance v1, Lcom/android/server/tof/ContactlessGestureController$AccessibilityTelephonyCallback; */
	 /* invoke-direct {v1, p0, v2}, Lcom/android/server/tof/ContactlessGestureController$AccessibilityTelephonyCallback;-><init>(Lcom/android/server/tof/ContactlessGestureController;Lcom/android/server/tof/ContactlessGestureController$AccessibilityTelephonyCallback-IA;)V */
	 this.mTelephonyCallback = v1;
	 /* .line 289 */
	 /* new-instance v1, Lcom/android/server/tof/ContactlessGestureController$ScreenStateReceiver; */
	 /* invoke-direct {v1, p0, v2}, Lcom/android/server/tof/ContactlessGestureController$ScreenStateReceiver;-><init>(Lcom/android/server/tof/ContactlessGestureController;Lcom/android/server/tof/ContactlessGestureController$ScreenStateReceiver-IA;)V */
	 this.mScreenReceiver = v1;
	 /* .line 290 */
	 com.android.server.power.PowerManagerServiceStub .get ( );
	 this.mPowerManagerServiceImpl = v1;
	 /* .line 291 */
	 /* new-instance v1, Ljava/util/concurrent/CountDownLatch; */
	 /* invoke-direct {v1, v0}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V */
	 this.mServiceBindingLatch = v1;
	 /* .line 292 */
	 /* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureController;->initTofGestureConfig()V */
	 /* .line 293 */
	 com.android.server.tof.AppFeatureHelper .getInstance ( );
	 this.mAppFeatureHelper = v0;
	 /* .line 294 */
	 v1 = this.mContext;
	 (( com.android.server.tof.AppFeatureHelper ) v0 ).initTofComponentConfig ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/tof/AppFeatureHelper;->initTofComponentConfig(Landroid/content/Context;)V
	 /* .line 295 */
	 (( com.android.server.tof.ContactlessGestureController ) p0 ).getTofClientComponent ( ); // invoke-virtual {p0}, Lcom/android/server/tof/ContactlessGestureController;->getTofClientComponent()Landroid/content/ComponentName;
	 this.mComponentName = v0;
	 /* .line 296 */
	 /* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureController;->registerListenerIfNeeded()V */
	 /* .line 297 */
	 return;
} // .end method
private void awaitServiceBinding ( ) {
	 /* .locals 4 */
	 /* .line 1063 */
	 try { // :try_start_0
		 v0 = this.mServiceBindingLatch;
		 v1 = java.util.concurrent.TimeUnit.MILLISECONDS;
		 /* const-wide/16 v2, 0x3e8 */
		 (( java.util.concurrent.CountDownLatch ) v0 ).await ( v2, v3, v1 ); // invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
		 /* :try_end_0 */
		 /* .catch Ljava/lang/InterruptedException; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 1066 */
		 /* .line 1064 */
		 /* :catch_0 */
		 /* move-exception v0 */
		 /* .line 1065 */
		 /* .local v0, "e":Ljava/lang/InterruptedException; */
		 final String v1 = "ContactlessGestureController"; // const-string v1, "ContactlessGestureController"
		 final String v2 = "Interrupted while waiting to bind contactless gesture client Service."; // const-string v2, "Interrupted while waiting to bind contactless gesture client Service."
		 android.util.Slog .e ( v1,v2,v0 );
		 /* .line 1067 */
	 } // .end local v0 # "e":Ljava/lang/InterruptedException;
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mServiceIsBinding:Z */
/* .line 1068 */
return;
} // .end method
private void checkTopActivities ( ) {
/* .locals 4 */
/* .line 853 */
v0 = this.mHandler;
v1 = this.mUpdateTopActivityIfNeededRunnable;
(( android.os.Handler ) v0 ).removeCallbacks ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 854 */
v0 = this.mHandler;
v1 = this.mUpdateTopActivityIfNeededRunnable;
/* const-wide/16 v2, 0x384 */
(( android.os.Handler ) v0 ).postDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 855 */
return;
} // .end method
private void dismissTriggerViewIfNeeded ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "label" # I */
/* .line 787 */
/* const/16 v0, 0xc */
/* if-ne p1, v0, :cond_0 */
/* .line 788 */
(( com.android.server.tof.ContactlessGestureController ) p0 ).updateGestureHint ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/tof/ContactlessGestureController;->updateGestureHint(I)Z
/* .line 790 */
} // :cond_0
return;
} // .end method
private void initTofGestureConfig ( ) {
/* .locals 2 */
/* .line 300 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 301 */
/* const v1, 0x110b002f */
v0 = (( android.content.res.Resources ) v0 ).getInteger ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I
/* iput v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureUpSupportFeature:I */
/* .line 302 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 303 */
/* const v1, 0x110b002b */
v0 = (( android.content.res.Resources ) v0 ).getInteger ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I
/* iput v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureDownSupportFeature:I */
/* .line 304 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 305 */
/* const v1, 0x110b002d */
v0 = (( android.content.res.Resources ) v0 ).getInteger ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I
/* iput v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureLeftSupportFeature:I */
/* .line 306 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 307 */
/* const v1, 0x110b002e */
v0 = (( android.content.res.Resources ) v0 ).getInteger ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I
/* iput v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureRightSupportFeature:I */
/* .line 308 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 309 */
/* const v1, 0x110b002a */
v0 = (( android.content.res.Resources ) v0 ).getInteger ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I
/* iput v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureDoublePressSupportFeature:I */
/* .line 310 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 311 */
/* const v1, 0x110b002c */
v0 = (( android.content.res.Resources ) v0 ).getInteger ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I
/* iput v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureDrawCircleSupportFeature:I */
/* .line 312 */
return;
} // .end method
private Boolean isGestureStateLabel ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "label" # I */
/* .line 682 */
/* const/16 v0, 0x18 */
/* if-eq p1, v0, :cond_1 */
/* const/16 v0, 0x19 */
/* if-eq p1, v0, :cond_1 */
/* const/16 v0, 0x1a */
/* if-eq p1, v0, :cond_1 */
/* const/16 v0, 0x1b */
/* if-ne p1, v0, :cond_0 */
/* .line 688 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 686 */
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
private Boolean isKeyguardShowing ( ) {
/* .locals 1 */
/* .line 465 */
v0 = v0 = this.mPolicy;
} // .end method
private Boolean isStateOrTriggerLabelSupport ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "label" # I */
/* .param p2, "currentSupportFeature" # I */
/* .line 602 */
int v0 = 0; // const/4 v0, 0x0
/* .line 603 */
/* .local v0, "feature":I */
/* sparse-switch p1, :sswitch_data_0 */
/* .line 610 */
/* :sswitch_0 */
/* iget v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureLeftSupportFeature:I */
/* and-int v0, p2, v1 */
/* .line 611 */
/* .line 606 */
/* :sswitch_1 */
/* iget v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureRightSupportFeature:I */
/* and-int v0, p2, v1 */
/* .line 607 */
/* .line 614 */
/* :sswitch_2 */
/* iget v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureUpSupportFeature:I */
/* and-int v0, p2, v1 */
/* .line 615 */
/* .line 619 */
/* :sswitch_3 */
/* iget v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureDownSupportFeature:I */
/* and-int v0, p2, v1 */
/* .line 620 */
/* const/16 v1, 0x80 */
/* if-eq p2, v1, :cond_0 */
/* const/16 v1, 0x100 */
/* if-ne p2, v1, :cond_1 */
/* .line 622 */
} // :cond_0
/* move v0, p2 */
/* .line 628 */
} // :cond_1
} // :goto_0
/* if-lez v0, :cond_2 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_2
int v1 = 0; // const/4 v1, 0x0
} // :goto_1
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0xa -> :sswitch_3 */
/* 0x14 -> :sswitch_3 */
/* 0x15 -> :sswitch_2 */
/* 0x16 -> :sswitch_1 */
/* 0x17 -> :sswitch_0 */
/* 0x18 -> :sswitch_3 */
/* 0x19 -> :sswitch_2 */
/* 0x1a -> :sswitch_1 */
/* 0x1b -> :sswitch_0 */
} // .end sparse-switch
} // .end method
private Boolean isTopActivityFocused ( ) {
/* .locals 3 */
/* .line 456 */
int v0 = 1; // const/4 v0, 0x1
/* .line 457 */
/* .local v0, "topActivityFocused":Z */
v1 = this.mFocusedPackage;
if ( v1 != null) { // if-eqz v1, :cond_0
v2 = this.mTopActivity;
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 458 */
(( android.content.ComponentName ) v2 ).getPackageName ( ); // invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
v0 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* .line 460 */
} // :cond_0
} // .end method
private Boolean isTriggerLabel ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "label" # I */
/* .line 692 */
/* const/16 v0, 0xa */
/* if-eq p1, v0, :cond_1 */
/* const/16 v0, 0x14 */
/* if-eq p1, v0, :cond_1 */
/* const/16 v0, 0x15 */
/* if-eq p1, v0, :cond_1 */
/* const/16 v0, 0x16 */
/* if-eq p1, v0, :cond_1 */
/* const/16 v0, 0x17 */
/* if-ne p1, v0, :cond_0 */
/* .line 699 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 697 */
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
private void lambda$handleGestureEvent$4 ( ) { //synthethic
/* .locals 1 */
/* .line 527 */
int v0 = 0; // const/4 v0, 0x0
(( com.android.server.tof.ContactlessGestureController ) p0 ).registerGestureListenerIfNeeded ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/tof/ContactlessGestureController;->registerGestureListenerIfNeeded(Z)V
return;
} // .end method
private void lambda$new$3 ( ) { //synthethic
/* .locals 2 */
/* .line 476 */
v0 = this.mContactlessGestureService;
/* iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mRegisterGesture:Z */
(( com.android.server.tof.ContactlessGestureService ) v0 ).registerContactlessGestureListenerIfNeeded ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/tof/ContactlessGestureService;->registerContactlessGestureListenerIfNeeded(Z)V
return;
} // .end method
private void lambda$unBindService$0 ( ) { //synthethic
/* .locals 2 */
/* .line 325 */
v0 = this.mContext;
v1 = this.mTofClientConnection;
(( android.content.Context ) v0 ).unbindService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
/* .line 326 */
return;
} // .end method
private void lambda$updateFocusedPackage$1 ( ) { //synthethic
/* .locals 1 */
/* .line 443 */
int v0 = 2; // const/4 v0, 0x2
(( com.android.server.tof.ContactlessGestureController ) p0 ).onSceneChanged ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/tof/ContactlessGestureController;->onSceneChanged(I)V
return;
} // .end method
private void lambda$updateFocusedPackage$2 ( ) { //synthethic
/* .locals 1 */
/* .line 446 */
v0 = this.mUpdateTopActivityIfNeededRunnable;
/* .line 447 */
int v0 = 2; // const/4 v0, 0x2
(( com.android.server.tof.ContactlessGestureController ) p0 ).onSceneChanged ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/tof/ContactlessGestureController;->onSceneChanged(I)V
/* .line 448 */
return;
} // .end method
private void monitorActivityChanges ( ) {
/* .locals 3 */
/* .line 862 */
try { // :try_start_0
v0 = this.mActivityManager;
v1 = this.mActivityStateObserver;
/* new-instance v2, Landroid/content/Intent; */
/* invoke-direct {v2}, Landroid/content/Intent;-><init>()V */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 865 */
/* .line 863 */
/* :catch_0 */
/* move-exception v0 */
/* .line 864 */
/* .local v0, "e":Landroid/os/RemoteException; */
final String v1 = "ContactlessGestureController"; // const-string v1, "ContactlessGestureController"
final String v2 = "cannot register activity monitoring"; // const-string v2, "cannot register activity monitoring"
android.util.Slog .e ( v1,v2,v0 );
/* .line 866 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :goto_0
/* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureController;->checkTopActivities()V */
/* .line 867 */
return;
} // .end method
private void registerCallStateChange ( ) {
/* .locals 3 */
/* .line 873 */
v0 = this.mTelephonyManager;
/* new-instance v1, Landroid/os/HandlerExecutor; */
v2 = this.mHandler;
/* invoke-direct {v1, v2}, Landroid/os/HandlerExecutor;-><init>(Landroid/os/Handler;)V */
v2 = this.mTelephonyCallback;
(( android.telephony.TelephonyManager ) v0 ).registerTelephonyCallback ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->registerTelephonyCallback(Ljava/util/concurrent/Executor;Landroid/telephony/TelephonyCallback;)V
/* .line 875 */
return;
} // .end method
private void registerDisplayListener ( ) {
/* .locals 3 */
/* .line 886 */
v0 = this.mDisplayManager;
/* new-instance v1, Lcom/android/server/tof/ContactlessGestureController$2; */
/* invoke-direct {v1, p0}, Lcom/android/server/tof/ContactlessGestureController$2;-><init>(Lcom/android/server/tof/ContactlessGestureController;)V */
v2 = this.mHandler;
(( android.hardware.display.DisplayManager ) v0 ).registerDisplayListener ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/hardware/display/DisplayManager;->registerDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;Landroid/os/Handler;)V
/* .line 904 */
return;
} // .end method
private void registerListenerDelayIfNeeded ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "register" # Z */
/* .line 485 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, p1, v0}, Lcom/android/server/tof/ContactlessGestureController;->registerListenerDelayIfNeeded(ZZ)V */
/* .line 486 */
return;
} // .end method
private void registerListenerDelayIfNeeded ( Boolean p0, Boolean p1 ) {
/* .locals 4 */
/* .param p1, "register" # Z */
/* .param p2, "immediate" # Z */
/* .line 496 */
/* iput-boolean p1, p0, Lcom/android/server/tof/ContactlessGestureController;->mRegisterGesture:Z */
/* .line 498 */
/* if-nez p1, :cond_0 */
/* .line 499 */
/* const/16 v0, 0xd */
(( com.android.server.tof.ContactlessGestureController ) p0 ).updateGestureHint ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/tof/ContactlessGestureController;->updateGestureHint(I)Z
/* .line 501 */
} // :cond_0
v0 = this.mHandler;
v1 = this.mRegisterGestureRunnable;
(( android.os.Handler ) v0 ).removeCallbacks ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 502 */
if ( p2 != null) { // if-eqz p2, :cond_1
/* .line 503 */
v0 = this.mHandler;
v1 = this.mRegisterGestureRunnable;
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 505 */
} // :cond_1
v0 = this.mHandler;
v1 = this.mRegisterGestureRunnable;
/* const-wide/16 v2, 0x1f4 */
(( android.os.Handler ) v0 ).postDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 507 */
} // :goto_0
return;
} // .end method
private void registerListenerIfNeeded ( ) {
/* .locals 0 */
/* .line 358 */
/* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureController;->monitorActivityChanges()V */
/* .line 359 */
/* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureController;->registerCallStateChange()V */
/* .line 360 */
/* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureController;->registerDisplayListener()V */
/* .line 361 */
/* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureController;->registerScreenReceiver()V */
/* .line 362 */
/* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureController;->updateDefaultDisplayRotation()V */
/* .line 363 */
return;
} // .end method
private void registerScreenReceiver ( ) {
/* .locals 3 */
/* .line 878 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 879 */
/* .local v0, "intentFilter":Landroid/content/IntentFilter; */
final String v1 = "android.intent.action.SCREEN_OFF"; // const-string v1, "android.intent.action.SCREEN_OFF"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 880 */
final String v1 = "android.intent.action.SCREEN_ON"; // const-string v1, "android.intent.action.SCREEN_ON"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 881 */
/* const/16 v1, 0x3e8 */
(( android.content.IntentFilter ) v0 ).setPriority ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->setPriority(I)V
/* .line 882 */
v1 = this.mContext;
v2 = this.mScreenReceiver;
(( android.content.Context ) v1 ).registerReceiver ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 883 */
return;
} // .end method
private Boolean showFakeTrigger ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "label" # I */
/* .param p2, "currentSupportFeature" # I */
/* .line 640 */
int v0 = -1; // const/4 v0, -0x1
/* .line 641 */
/* .local v0, "fakeLabel":I */
/* iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mIsTrigger:Z */
/* if-nez v1, :cond_0 */
/* .line 642 */
/* packed-switch p1, :pswitch_data_0 */
/* .line 653 */
/* :pswitch_0 */
/* const/16 v0, 0x17 */
/* .line 654 */
/* .line 650 */
/* :pswitch_1 */
/* const/16 v0, 0x16 */
/* .line 651 */
/* .line 647 */
/* :pswitch_2 */
/* const/16 v0, 0x15 */
/* .line 648 */
/* .line 644 */
/* :pswitch_3 */
/* const/16 v0, 0x14 */
/* .line 645 */
/* nop */
/* .line 658 */
} // :goto_0
/* if-lez v0, :cond_0 */
/* .line 659 */
v1 = /* invoke-direct {p0, v0, p2}, Lcom/android/server/tof/ContactlessGestureController;->showTrigger(II)Z */
/* .line 662 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* :pswitch_data_0 */
/* .packed-switch 0x18 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private void showGestureState ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "label" # I */
/* .param p2, "currentSupportFeature" # I */
/* .line 595 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/tof/ContactlessGestureController;->isGestureStateLabel(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 596 */
v0 = /* invoke-direct {p0, p1, p2}, Lcom/android/server/tof/ContactlessGestureController;->isStateOrTriggerLabelSupport(II)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 597 */
(( com.android.server.tof.ContactlessGestureController ) p0 ).updateGestureHint ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/tof/ContactlessGestureController;->updateGestureHint(I)Z
/* .line 599 */
} // :cond_0
return;
} // .end method
private void showGestureViewIfNeeded ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "label" # I */
/* .param p2, "currentSupportFeature" # I */
/* .line 588 */
v0 = /* invoke-direct {p0, p1, p2}, Lcom/android/server/tof/ContactlessGestureController;->showTrigger(II)Z */
/* if-nez v0, :cond_0 */
/* .line 589 */
v0 = /* invoke-direct {p0, p1, p2}, Lcom/android/server/tof/ContactlessGestureController;->showFakeTrigger(II)Z */
/* if-nez v0, :cond_0 */
/* .line 590 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/tof/ContactlessGestureController;->showGestureState(II)V */
/* .line 592 */
} // :cond_0
return;
} // .end method
private void showLastTriggerViewIfNeeded ( ) {
/* .locals 2 */
/* .line 423 */
/* iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mIsTrigger:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mShouldUpdateTriggerView:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 424 */
/* iget v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mLastTriggerLabel:I */
v1 = (( com.android.server.tof.ContactlessGestureController ) p0 ).getCurrentSupportFeature ( ); // invoke-virtual {p0}, Lcom/android/server/tof/ContactlessGestureController;->getCurrentSupportFeature()I
v0 = /* invoke-direct {p0, v0, v1}, Lcom/android/server/tof/ContactlessGestureController;->isStateOrTriggerLabelSupport(II)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 425 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "show last trigger view, mLastTriggerLabel:" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mLastTriggerLabel:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "ContactlessGestureController"; // const-string v1, "ContactlessGestureController"
android.util.Slog .i ( v1,v0 );
/* .line 426 */
/* iget v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mLastTriggerLabel:I */
(( com.android.server.tof.ContactlessGestureController ) p0 ).updateGestureHint ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/tof/ContactlessGestureController;->updateGestureHint(I)Z
/* .line 429 */
} // :cond_0
return;
} // .end method
private Boolean showTrigger ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "label" # I */
/* .param p2, "currentSupportFeature" # I */
/* .line 673 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/tof/ContactlessGestureController;->isTriggerLabel(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 674 */
v0 = /* invoke-direct {p0, p1, p2}, Lcom/android/server/tof/ContactlessGestureController;->isStateOrTriggerLabelSupport(II)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 675 */
v0 = (( com.android.server.tof.ContactlessGestureController ) p0 ).updateGestureHint ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/tof/ContactlessGestureController;->updateGestureHint(I)Z
/* iput-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mIsTrigger:Z */
/* .line 676 */
int v0 = 1; // const/4 v0, 0x1
/* .line 678 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private void updateDefaultDisplayRotation ( ) {
/* .locals 2 */
/* .line 469 */
v0 = this.mWindowManagerService;
v0 = (( com.android.server.wm.WindowManagerService ) v0 ).getDefaultDisplayRotation ( ); // invoke-virtual {v0}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayRotation()I
/* .line 470 */
/* .local v0, "currentRotation":I */
/* iget v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mDisplayRotation:I */
/* if-eq v0, v1, :cond_0 */
/* .line 471 */
/* iput v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mDisplayRotation:I */
/* .line 473 */
} // :cond_0
return;
} // .end method
/* # virtual methods */
public void bindService ( ) {
/* .locals 1 */
/* .line 315 */
v0 = this.mComponentName;
(( com.android.server.tof.ContactlessGestureController ) p0 ).bindService ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/tof/ContactlessGestureController;->bindService(Landroid/content/ComponentName;)V
/* .line 316 */
/* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureController;->awaitServiceBinding()V */
/* .line 317 */
return;
} // .end method
public void bindService ( android.content.ComponentName p0 ) {
/* .locals 7 */
/* .param p1, "name" # Landroid/content/ComponentName; */
/* .line 332 */
/* const-string/jumbo v0, "unable to bind tof service: " */
v1 = (( com.android.server.tof.ContactlessGestureController ) p0 ).isServiceInit ( ); // invoke-virtual {p0}, Lcom/android/server/tof/ContactlessGestureController;->isServiceInit()Z
/* if-nez v1, :cond_1 */
if ( p1 != null) { // if-eqz p1, :cond_1
/* iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mServiceIsBinding:Z */
/* if-nez v1, :cond_1 */
/* .line 333 */
final String v1 = "bindService"; // const-string v1, "bindService"
final String v2 = "ContactlessGestureController"; // const-string v2, "ContactlessGestureController"
android.util.Slog .i ( v2,v1 );
/* .line 334 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mServiceIsBinding:Z */
/* .line 335 */
/* new-instance v1, Landroid/content/Intent; */
(( com.android.server.tof.ContactlessGestureController ) p0 ).getAction ( ); // invoke-virtual {p0}, Lcom/android/server/tof/ContactlessGestureController;->getAction()Ljava/lang/String;
/* invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 336 */
/* .local v1, "intent":Landroid/content/Intent; */
(( android.content.Intent ) v1 ).setComponent ( p1 ); // invoke-virtual {v1, p1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 337 */
/* const/high16 v3, 0x800000 */
(( android.content.Intent ) v1 ).addFlags ( v3 ); // invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 339 */
try { // :try_start_0
v3 = this.mContext;
v4 = this.mTofClientConnection;
v5 = android.os.UserHandle.CURRENT;
/* const v6, 0x4000001 */
v3 = (( android.content.Context ) v3 ).bindServiceAsUser ( v1, v4, v6, v5 ); // invoke-virtual {v3, v1, v4, v6, v5}, Landroid/content/Context;->bindServiceAsUser(Landroid/content/Intent;Landroid/content/ServiceConnection;ILandroid/os/UserHandle;)Z
/* if-nez v3, :cond_0 */
/* .line 342 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v3 );
/* :try_end_0 */
/* .catch Ljava/lang/SecurityException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 346 */
} // :cond_0
/* .line 344 */
/* :catch_0 */
/* move-exception v3 */
/* .line 345 */
/* .local v3, "ex":Ljava/lang/SecurityException; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v0,v3 );
/* .line 348 */
} // .end local v1 # "intent":Landroid/content/Intent;
} // .end local v3 # "ex":Ljava/lang/SecurityException;
} // :cond_1
} // :goto_0
return;
} // .end method
public Boolean changeAppConfigWithPackageName ( java.lang.String p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "enable" # Z */
/* .line 916 */
v0 = this.mAppFeatureHelper;
v0 = (( com.android.server.tof.AppFeatureHelper ) v0 ).changeTofGestureAppConfig ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/tof/AppFeatureHelper;->changeTofGestureAppConfig(Ljava/lang/String;Z)Z
} // .end method
public void dump ( java.io.PrintWriter p0 ) {
/* .locals 3 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 1073 */
final String v0 = "Contactless Gesture Controller:"; // const-string v0, "Contactless Gesture Controller:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1074 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " gesture client: "; // const-string v1, " gesture client: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.tof.ContactlessGestureController ) p0 ).getTofClientComponent ( ); // invoke-virtual {p0}, Lcom/android/server/tof/ContactlessGestureController;->getTofClientComponent()Landroid/content/ComponentName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1075 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " gesture client connected: "; // const-string v1, " gesture client connected: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = (( com.android.server.tof.ContactlessGestureController ) p0 ).isServiceInit ( ); // invoke-virtual {p0}, Lcom/android/server/tof/ContactlessGestureController;->isServiceInit()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* const-string/jumbo v1, "true" */
} // :cond_0
final String v1 = "false"; // const-string v1, "false"
} // :goto_0
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1076 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " register: "; // const-string v1, " register: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mRegisterGesture:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1077 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " interactive: "; // const-string v1, " interactive: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mInteractive:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1078 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " isTrigger: "; // const-string v1, " isTrigger: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mIsTrigger:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1079 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mLastTriggerLabel: "; // const-string v1, " mLastTriggerLabel: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mLastTriggerLabel:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1080 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " focused package: "; // const-string v1, " focused package: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mFocusedPackage;
final String v2 = ""; // const-string v2, ""
if ( v1 != null) { // if-eqz v1, :cond_1
} // :cond_1
/* move-object v1, v2 */
} // :goto_1
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1081 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " display rotation: "; // const-string v1, " display rotation: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mDisplayRotation:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1082 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " desk top mode enable: "; // const-string v1, " desk top mode enable: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mIsDeskTopMode:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1083 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " split screen mode: "; // const-string v1, " split screen mode: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mIsSplitScreenMode:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1084 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " isScreenCastMode: "; // const-string v1, " isScreenCastMode: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mContactlessGestureService;
v1 = (( com.android.server.tof.ContactlessGestureService ) v1 ).isScreenCastMode ( ); // invoke-virtual {v1}, Lcom/android/server/tof/ContactlessGestureService;->isScreenCastMode()Z
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1085 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " top activity: "; // const-string v1, " top activity: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mTopActivity;
if ( v1 != null) { // if-eqz v1, :cond_2
(( android.content.ComponentName ) v1 ).toString ( ); // invoke-virtual {v1}, Landroid/content/ComponentName;->toString()Ljava/lang/String;
} // :cond_2
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1086 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " resConfig: "; // const-string v1, " resConfig: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mAppFeatureHelper;
(( com.android.server.tof.AppFeatureHelper ) v1 ).getResGestureConfig ( ); // invoke-virtual {v1}, Lcom/android/server/tof/AppFeatureHelper;->getResGestureConfig()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1087 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " cloudConfig: "; // const-string v1, " cloudConfig: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mAppFeatureHelper;
(( com.android.server.tof.AppFeatureHelper ) v1 ).getCloudGestureConfig ( ); // invoke-virtual {v1}, Lcom/android/server/tof/AppFeatureHelper;->getCloudGestureConfig()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1088 */
return;
} // .end method
public java.lang.String gestureLabelToString ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "label" # I */
/* .line 1000 */
/* packed-switch p1, :pswitch_data_0 */
/* .line 1056 */
/* :pswitch_0 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "UNKNOWN label:"; // const-string v1, "UNKNOWN label:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .local v0, "gesture":Ljava/lang/String; */
/* .line 1047 */
} // .end local v0 # "gesture":Ljava/lang/String;
/* :pswitch_1 */
final String v0 = "GESTURE_RIGHT_STATE"; // const-string v0, "GESTURE_RIGHT_STATE"
/* .line 1048 */
/* .restart local v0 # "gesture":Ljava/lang/String; */
/* .line 1044 */
} // .end local v0 # "gesture":Ljava/lang/String;
/* :pswitch_2 */
final String v0 = "GESTURE_LEFT_STATE"; // const-string v0, "GESTURE_LEFT_STATE"
/* .line 1045 */
/* .restart local v0 # "gesture":Ljava/lang/String; */
/* .line 1041 */
} // .end local v0 # "gesture":Ljava/lang/String;
/* :pswitch_3 */
final String v0 = "GESTURE_DOWN_STATE"; // const-string v0, "GESTURE_DOWN_STATE"
/* .line 1042 */
/* .restart local v0 # "gesture":Ljava/lang/String; */
/* .line 1038 */
} // .end local v0 # "gesture":Ljava/lang/String;
/* :pswitch_4 */
final String v0 = "GESTURE_UP_STATE"; // const-string v0, "GESTURE_UP_STATE"
/* .line 1039 */
/* .restart local v0 # "gesture":Ljava/lang/String; */
/* .line 1032 */
} // .end local v0 # "gesture":Ljava/lang/String;
/* :pswitch_5 */
final String v0 = "GESTURE_TRIGGER_RIGHT"; // const-string v0, "GESTURE_TRIGGER_RIGHT"
/* .line 1033 */
/* .restart local v0 # "gesture":Ljava/lang/String; */
/* .line 1029 */
} // .end local v0 # "gesture":Ljava/lang/String;
/* :pswitch_6 */
final String v0 = "GESTURE_TRIGGER_LEFT"; // const-string v0, "GESTURE_TRIGGER_LEFT"
/* .line 1030 */
/* .restart local v0 # "gesture":Ljava/lang/String; */
/* .line 1026 */
} // .end local v0 # "gesture":Ljava/lang/String;
/* :pswitch_7 */
final String v0 = "GESTURE_TRIGGER_DOWN"; // const-string v0, "GESTURE_TRIGGER_DOWN"
/* .line 1027 */
/* .restart local v0 # "gesture":Ljava/lang/String; */
/* .line 1023 */
} // .end local v0 # "gesture":Ljava/lang/String;
/* :pswitch_8 */
final String v0 = "GESTURE_TRIGGER_UP"; // const-string v0, "GESTURE_TRIGGER_UP"
/* .line 1024 */
/* .restart local v0 # "gesture":Ljava/lang/String; */
/* .line 1053 */
} // .end local v0 # "gesture":Ljava/lang/String;
/* :pswitch_9 */
final String v0 = "GESTURE_GESTURE_WORKING"; // const-string v0, "GESTURE_GESTURE_WORKING"
/* .line 1054 */
/* .restart local v0 # "gesture":Ljava/lang/String; */
/* .line 1050 */
} // .end local v0 # "gesture":Ljava/lang/String;
/* :pswitch_a */
final String v0 = "INVALIDATE_SCENARIO"; // const-string v0, "INVALIDATE_SCENARIO"
/* .line 1051 */
/* .restart local v0 # "gesture":Ljava/lang/String; */
/* .line 1035 */
} // .end local v0 # "gesture":Ljava/lang/String;
/* :pswitch_b */
final String v0 = "GESTURE_UN_TRIGGER"; // const-string v0, "GESTURE_UN_TRIGGER"
/* .line 1036 */
/* .restart local v0 # "gesture":Ljava/lang/String; */
/* .line 1020 */
} // .end local v0 # "gesture":Ljava/lang/String;
/* :pswitch_c */
final String v0 = "GESTURE_TRIGGER"; // const-string v0, "GESTURE_TRIGGER"
/* .line 1021 */
/* .restart local v0 # "gesture":Ljava/lang/String; */
/* .line 1017 */
} // .end local v0 # "gesture":Ljava/lang/String;
/* :pswitch_d */
final String v0 = "GESTURE_DRAW_CIRCLE"; // const-string v0, "GESTURE_DRAW_CIRCLE"
/* .line 1018 */
/* .restart local v0 # "gesture":Ljava/lang/String; */
/* .line 1014 */
} // .end local v0 # "gesture":Ljava/lang/String;
/* :pswitch_e */
final String v0 = "GESTURE_DOUBLE_PRESS"; // const-string v0, "GESTURE_DOUBLE_PRESS"
/* .line 1015 */
/* .restart local v0 # "gesture":Ljava/lang/String; */
/* .line 1011 */
} // .end local v0 # "gesture":Ljava/lang/String;
/* :pswitch_f */
final String v0 = "GESTURE_UP"; // const-string v0, "GESTURE_UP"
/* .line 1012 */
/* .restart local v0 # "gesture":Ljava/lang/String; */
/* .line 1008 */
} // .end local v0 # "gesture":Ljava/lang/String;
/* :pswitch_10 */
final String v0 = "GESTURE_DOWN"; // const-string v0, "GESTURE_DOWN"
/* .line 1009 */
/* .restart local v0 # "gesture":Ljava/lang/String; */
/* .line 1005 */
} // .end local v0 # "gesture":Ljava/lang/String;
/* :pswitch_11 */
final String v0 = "GESTURE_RIGHT"; // const-string v0, "GESTURE_RIGHT"
/* .line 1006 */
/* .restart local v0 # "gesture":Ljava/lang/String; */
/* .line 1002 */
} // .end local v0 # "gesture":Ljava/lang/String;
/* :pswitch_12 */
final String v0 = "GESTURE_LEFT"; // const-string v0, "GESTURE_LEFT"
/* .line 1003 */
/* .restart local v0 # "gesture":Ljava/lang/String; */
/* nop */
/* .line 1058 */
} // :goto_0
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_12 */
/* :pswitch_11 */
/* :pswitch_10 */
/* :pswitch_f */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_e */
/* :pswitch_d */
/* :pswitch_0 */
/* :pswitch_c */
/* :pswitch_0 */
/* :pswitch_b */
/* :pswitch_a */
/* :pswitch_9 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
public abstract java.lang.String getAction ( ) {
} // .end method
public Integer getCurrentAppType ( ) {
/* .locals 2 */
/* .line 771 */
v0 = this.mAppFeatureHelper;
v1 = this.mTopActivity;
(( com.android.server.tof.AppFeatureHelper ) v0 ).getSupportComponentFromConfig ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/tof/AppFeatureHelper;->getSupportComponentFromConfig(Landroid/content/ComponentName;)Lcom/android/server/tof/TofGestureComponent;
/* .line 773 */
/* .local v0, "component":Lcom/android/server/tof/TofGestureComponent; */
/* if-nez v0, :cond_0 */
/* .line 774 */
int v1 = 0; // const/4 v1, 0x0
/* .line 776 */
} // :cond_0
v1 = (( com.android.server.tof.TofGestureComponent ) v0 ).getCategory ( ); // invoke-virtual {v0}, Lcom/android/server/tof/TofGestureComponent;->getCategory()I
} // .end method
public java.lang.String getCurrentPkg ( ) {
/* .locals 1 */
/* .line 767 */
v0 = this.mTopActivity;
if ( v0 != null) { // if-eqz v0, :cond_0
(( android.content.ComponentName ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
Integer getCurrentSupportFeature ( ) {
/* .locals 3 */
/* .line 708 */
v0 = this.mTelephonyManager;
v0 = (( android.telephony.TelephonyManager ) v0 ).getCallState ( ); // invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I
/* packed-switch v0, :pswitch_data_0 */
/* .line 716 */
v0 = /* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureController;->isKeyguardShowing()Z */
/* if-nez v0, :cond_4 */
v0 = /* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureController;->isTopActivityFocused()Z */
/* if-nez v0, :cond_0 */
/* .line 713 */
/* :pswitch_0 */
/* const/16 v0, 0x100 */
/* .line 714 */
/* .local v0, "appSupportFeature":I */
/* .line 710 */
} // .end local v0 # "appSupportFeature":I
/* :pswitch_1 */
/* const/16 v0, 0x80 */
/* .line 711 */
/* .restart local v0 # "appSupportFeature":I */
/* .line 719 */
} // .end local v0 # "appSupportFeature":I
} // :cond_0
v0 = this.mAppFeatureHelper;
v1 = this.mTopActivity;
(( com.android.server.tof.AppFeatureHelper ) v0 ).getSupportComponentFromConfig ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/tof/AppFeatureHelper;->getSupportComponentFromConfig(Landroid/content/ComponentName;)Lcom/android/server/tof/TofGestureComponent;
/* .line 721 */
/* .local v0, "component":Lcom/android/server/tof/TofGestureComponent; */
/* if-nez v0, :cond_1 */
/* .line 722 */
int v1 = 0; // const/4 v1, 0x0
/* .line 724 */
} // :cond_1
/* iget v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mDisplayRotation:I */
int v2 = 1; // const/4 v2, 0x1
/* if-eq v1, v2, :cond_3 */
int v2 = 3; // const/4 v2, 0x3
/* if-ne v1, v2, :cond_2 */
/* .line 726 */
} // :cond_2
v1 = (( com.android.server.tof.TofGestureComponent ) v0 ).getPortraitFeature ( ); // invoke-virtual {v0}, Lcom/android/server/tof/TofGestureComponent;->getPortraitFeature()I
} // :cond_3
} // :goto_0
v1 = (( com.android.server.tof.TofGestureComponent ) v0 ).getLandscapeFeature ( ); // invoke-virtual {v0}, Lcom/android/server/tof/TofGestureComponent;->getLandscapeFeature()I
} // :goto_1
/* move v0, v1 */
/* .local v1, "appSupportFeature":I */
/* .line 717 */
} // .end local v0 # "component":Lcom/android/server/tof/TofGestureComponent;
} // .end local v1 # "appSupportFeature":I
} // :cond_4
} // :goto_2
int v0 = 0; // const/4 v0, 0x0
/* .line 731 */
/* .local v0, "appSupportFeature":I */
} // :goto_3
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
Integer getCurrentSupportGesture ( ) {
/* .locals 3 */
/* .line 740 */
int v0 = 0; // const/4 v0, 0x0
/* .line 741 */
/* .local v0, "gestureSupport":I */
v1 = (( com.android.server.tof.ContactlessGestureController ) p0 ).getCurrentSupportFeature ( ); // invoke-virtual {p0}, Lcom/android/server/tof/ContactlessGestureController;->getCurrentSupportFeature()I
/* .line 742 */
/* .local v1, "appSupportFeature":I */
/* iget v2, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureUpSupportFeature:I */
/* and-int/2addr v2, v1 */
/* if-nez v2, :cond_0 */
/* iget v2, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureDownSupportFeature:I */
/* and-int/2addr v2, v1 */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 744 */
} // :cond_0
/* or-int/lit8 v0, v0, 0x1 */
/* .line 746 */
} // :cond_1
/* iget v2, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureLeftSupportFeature:I */
/* and-int/2addr v2, v1 */
/* if-nez v2, :cond_2 */
/* iget v2, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureRightSupportFeature:I */
/* and-int/2addr v2, v1 */
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 748 */
} // :cond_2
/* or-int/lit8 v0, v0, 0x2 */
/* .line 750 */
} // :cond_3
/* iget v2, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureDoublePressSupportFeature:I */
/* and-int/2addr v2, v1 */
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 751 */
/* or-int/lit8 v0, v0, 0x4 */
/* .line 753 */
} // :cond_4
/* iget v2, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureDrawCircleSupportFeature:I */
/* and-int/2addr v2, v1 */
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 754 */
/* or-int/lit8 v0, v0, 0x8 */
/* .line 756 */
} // :cond_5
} // .end method
Integer getFeatureFromLabel ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "label" # I */
/* .line 936 */
int v0 = -3; // const/4 v0, -0x3
/* .line 937 */
/* .local v0, "labelFeature":I */
/* packed-switch p1, :pswitch_data_0 */
/* :pswitch_0 */
/* .line 984 */
/* :pswitch_1 */
/* const/16 v0, -0xb */
/* .line 985 */
/* .line 981 */
/* :pswitch_2 */
/* const/16 v0, -0xc */
/* .line 982 */
/* .line 978 */
/* :pswitch_3 */
/* const/16 v0, -0xa */
/* .line 979 */
/* .line 975 */
/* :pswitch_4 */
/* const/16 v0, -0x9 */
/* .line 976 */
/* .line 969 */
/* :pswitch_5 */
int v0 = -7; // const/4 v0, -0x7
/* .line 970 */
/* .line 966 */
/* :pswitch_6 */
int v0 = -8; // const/4 v0, -0x8
/* .line 967 */
/* .line 963 */
/* :pswitch_7 */
int v0 = -6; // const/4 v0, -0x6
/* .line 964 */
/* .line 960 */
/* :pswitch_8 */
int v0 = -5; // const/4 v0, -0x5
/* .line 961 */
/* .line 990 */
/* :pswitch_9 */
int v0 = -4; // const/4 v0, -0x4
/* .line 991 */
/* .line 987 */
/* :pswitch_a */
int v0 = -3; // const/4 v0, -0x3
/* .line 988 */
/* .line 972 */
/* :pswitch_b */
int v0 = -2; // const/4 v0, -0x2
/* .line 973 */
/* .line 957 */
/* :pswitch_c */
int v0 = -1; // const/4 v0, -0x1
/* .line 958 */
/* .line 954 */
/* :pswitch_d */
/* iget v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureDrawCircleSupportFeature:I */
/* .line 955 */
/* .line 951 */
/* :pswitch_e */
/* iget v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureDoublePressSupportFeature:I */
/* .line 952 */
/* .line 948 */
/* :pswitch_f */
/* iget v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureUpSupportFeature:I */
/* .line 949 */
/* .line 945 */
/* :pswitch_10 */
/* iget v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureDownSupportFeature:I */
/* .line 946 */
/* .line 942 */
/* :pswitch_11 */
/* iget v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureRightSupportFeature:I */
/* .line 943 */
/* .line 939 */
/* :pswitch_12 */
/* iget v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureLeftSupportFeature:I */
/* .line 940 */
/* nop */
/* .line 995 */
} // :goto_0
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_12 */
/* :pswitch_11 */
/* :pswitch_10 */
/* :pswitch_f */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_e */
/* :pswitch_d */
/* :pswitch_0 */
/* :pswitch_c */
/* :pswitch_0 */
/* :pswitch_b */
/* :pswitch_a */
/* :pswitch_9 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
public abstract android.content.ComponentName getTofClientComponent ( ) {
} // .end method
public com.miui.tof.gesture.TofGestureAppData getTofGestureAppData ( ) {
/* .locals 1 */
/* .line 923 */
v0 = this.mAppFeatureHelper;
(( com.android.server.tof.AppFeatureHelper ) v0 ).getTofGestureAppData ( ); // invoke-virtual {v0}, Lcom/android/server/tof/AppFeatureHelper;->getTofGestureAppData()Lcom/miui/tof/gesture/TofGestureAppData;
} // .end method
public void handleGestureEvent ( Integer p0 ) {
/* .locals 6 */
/* .param p1, "label" # I */
/* .line 510 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "sensorEvent:" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.tof.ContactlessGestureController ) p0 ).gestureLabelToString ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/tof/ContactlessGestureController;->gestureLabelToString(I)Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " mIsTrigger:"; // const-string v1, " mIsTrigger:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mIsTrigger:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "ContactlessGestureController"; // const-string v1, "ContactlessGestureController"
android.util.Slog .i ( v1,v0 );
/* .line 511 */
/* invoke-direct {p0, p1}, Lcom/android/server/tof/ContactlessGestureController;->dismissTriggerViewIfNeeded(I)V */
/* .line 512 */
/* iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mRegisterGesture:Z */
/* if-nez v0, :cond_0 */
/* .line 513 */
return;
/* .line 516 */
} // :cond_0
v0 = (( com.android.server.tof.ContactlessGestureController ) p0 ).getCurrentSupportFeature ( ); // invoke-virtual {p0}, Lcom/android/server/tof/ContactlessGestureController;->getCurrentSupportFeature()I
/* .line 520 */
/* .local v0, "appSupportFeature":I */
int v2 = 0; // const/4 v2, 0x0
/* const/16 v3, 0xc */
/* if-ne p1, v3, :cond_1 */
/* .line 521 */
/* iput-boolean v2, p0, Lcom/android/server/tof/ContactlessGestureController;->mIsTrigger:Z */
/* .line 522 */
} // :cond_1
v4 = /* invoke-direct {p0, p1}, Lcom/android/server/tof/ContactlessGestureController;->isGestureStateLabel(I)Z */
/* if-nez v4, :cond_2 */
v4 = /* invoke-direct {p0, p1}, Lcom/android/server/tof/ContactlessGestureController;->isTriggerLabel(I)Z */
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 523 */
} // :cond_2
/* iput p1, p0, Lcom/android/server/tof/ContactlessGestureController;->mLastTriggerLabel:I */
/* .line 526 */
/* if-nez v0, :cond_3 */
v4 = /* invoke-direct {p0, p1}, Lcom/android/server/tof/ContactlessGestureController;->isTriggerLabel(I)Z */
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 527 */
v4 = this.mHandler;
/* new-instance v5, Lcom/android/server/tof/ContactlessGestureController$$ExternalSyntheticLambda0; */
/* invoke-direct {v5, p0}, Lcom/android/server/tof/ContactlessGestureController$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/tof/ContactlessGestureController;)V */
(( android.os.Handler ) v4 ).post ( v5 ); // invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 531 */
} // :cond_3
} // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_7
/* .line 532 */
int v4 = 0; // const/4 v4, 0x0
/* .line 533 */
/* .local v4, "feature":I */
/* packed-switch p1, :pswitch_data_0 */
/* :pswitch_0 */
/* .line 553 */
/* :pswitch_1 */
(( com.android.server.tof.ContactlessGestureController ) p0 ).updateGestureHint ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/tof/ContactlessGestureController;->updateGestureHint(I)Z
/* .line 554 */
/* .line 564 */
/* :pswitch_2 */
/* invoke-direct {p0, p1, v0}, Lcom/android/server/tof/ContactlessGestureController;->showGestureViewIfNeeded(II)V */
/* .line 565 */
/* .line 550 */
/* :pswitch_3 */
/* iget v5, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureDrawCircleSupportFeature:I */
/* and-int v4, v0, v5 */
/* .line 551 */
/* .line 547 */
/* :pswitch_4 */
/* iget v5, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureDoublePressSupportFeature:I */
/* and-int v4, v0, v5 */
/* .line 548 */
/* .line 544 */
/* :pswitch_5 */
/* iget v5, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureUpSupportFeature:I */
/* and-int v4, v0, v5 */
/* .line 545 */
/* .line 541 */
/* :pswitch_6 */
/* iget v5, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureDownSupportFeature:I */
/* and-int v4, v0, v5 */
/* .line 542 */
/* .line 538 */
/* :pswitch_7 */
/* iget v5, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureRightSupportFeature:I */
/* and-int v4, v0, v5 */
/* .line 539 */
/* .line 535 */
/* :pswitch_8 */
/* iget v5, p0, Lcom/android/server/tof/ContactlessGestureController;->mTofGestureLeftSupportFeature:I */
/* and-int v4, v0, v5 */
/* .line 536 */
/* nop */
/* .line 570 */
} // :goto_1
/* if-eq p1, v3, :cond_6 */
/* .line 571 */
v3 = this.mTopActivity;
/* if-nez v3, :cond_4 */
int v3 = 0; // const/4 v3, 0x0
} // :cond_4
(( android.content.ComponentName ) v3 ).getPackageName ( ); // invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 572 */
/* .local v3, "pkg":Ljava/lang/String; */
} // :goto_2
/* if-lez v4, :cond_5 */
int v2 = 1; // const/4 v2, 0x1
/* .line 573 */
/* .local v2, "success":Z */
} // :cond_5
v5 = this.mPowerManagerServiceImpl;
(( com.android.server.power.PowerManagerServiceStub ) v5 ).notifyGestureEvent ( v3, v2, p1 ); // invoke-virtual {v5, v3, v2, p1}, Lcom/android/server/power/PowerManagerServiceStub;->notifyGestureEvent(Ljava/lang/String;ZI)V
/* .line 578 */
} // .end local v2 # "success":Z
} // .end local v3 # "pkg":Ljava/lang/String;
} // :cond_6
/* iget-boolean v2, p0, Lcom/android/server/tof/ContactlessGestureController;->mIsTrigger:Z */
if ( v2 != null) { // if-eqz v2, :cond_7
/* if-lez v4, :cond_7 */
/* .line 579 */
(( com.android.server.tof.ContactlessGestureController ) p0 ).updateGestureHint ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/tof/ContactlessGestureController;->updateGestureHint(I)Z
/* .line 580 */
v2 = this.mInputHelper;
(( com.android.server.tof.InputHelper ) v2 ).injectSupportInputEvent ( v4 ); // invoke-virtual {v2, v4}, Lcom/android/server/tof/InputHelper;->injectSupportInputEvent(I)V
/* .line 581 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "handleTofGesture feature:0x"; // const-string v3, "handleTofGesture feature:0x"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.Integer .toHexString ( v4 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = ",mIsTrigger:"; // const-string v3, ",mIsTrigger:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, p0, Lcom/android/server/tof/ContactlessGestureController;->mIsTrigger:Z */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v2 );
/* .line 585 */
} // .end local v4 # "feature":I
} // :cond_7
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_0 */
/* :pswitch_2 */
/* :pswitch_0 */
/* :pswitch_1 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_2 */
/* :pswitch_2 */
/* :pswitch_2 */
/* :pswitch_2 */
/* :pswitch_2 */
/* :pswitch_2 */
/* :pswitch_2 */
/* :pswitch_2 */
} // .end packed-switch
} // .end method
public abstract Boolean isServiceInit ( ) {
} // .end method
public abstract void notifyRotationChanged ( Integer p0 ) {
} // .end method
public abstract void onGestureServiceConnected ( android.content.ComponentName p0, android.os.IBinder p1 ) {
} // .end method
public void onSceneChanged ( Integer p0 ) {
/* .locals 7 */
/* .param p1, "scene" # I */
/* .line 371 */
/* iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mInteractive:Z */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_d
v0 = this.mContactlessGestureService;
v0 = (( com.android.server.tof.ContactlessGestureService ) v0 ).isSupportTofGestureFeature ( ); // invoke-virtual {v0}, Lcom/android/server/tof/ContactlessGestureService;->isSupportTofGestureFeature()Z
int v2 = 1; // const/4 v2, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget v0, p0, Lcom/android/server/tof/ContactlessGestureController;->mDisplayRotation:I */
/* if-ne v0, v2, :cond_d */
} // :cond_0
v0 = this.mContactlessGestureService;
/* .line 373 */
v0 = (( com.android.server.tof.ContactlessGestureService ) v0 ).isScreenCastMode ( ); // invoke-virtual {v0}, Lcom/android/server/tof/ContactlessGestureService;->isScreenCastMode()Z
/* if-nez v0, :cond_d */
v0 = this.mContactlessGestureService;
/* .line 374 */
v0 = (( com.android.server.tof.ContactlessGestureService ) v0 ).isTalkBackEnabled ( ); // invoke-virtual {v0}, Lcom/android/server/tof/ContactlessGestureService;->isTalkBackEnabled()Z
/* if-nez v0, :cond_d */
v0 = this.mContactlessGestureService;
/* .line 375 */
v0 = (( com.android.server.tof.ContactlessGestureService ) v0 ).isOneHandedModeEnabled ( ); // invoke-virtual {v0}, Lcom/android/server/tof/ContactlessGestureService;->isOneHandedModeEnabled()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* goto/16 :goto_7 */
/* .line 380 */
} // :cond_1
v0 = this.mTelephonyManager;
v0 = (( android.telephony.TelephonyManager ) v0 ).getCallState ( ); // invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I
/* .line 381 */
/* .local v0, "callState":I */
/* if-eq v0, v2, :cond_c */
int v3 = 2; // const/4 v3, 0x2
/* if-ne v0, v3, :cond_2 */
/* goto/16 :goto_6 */
/* .line 389 */
} // :cond_2
/* iget-boolean v4, p0, Lcom/android/server/tof/ContactlessGestureController;->mIsSplitScreenMode:Z */
/* if-nez v4, :cond_b */
/* iget-boolean v4, p0, Lcom/android/server/tof/ContactlessGestureController;->mTopActivityFullScreenOrNotOccluded:Z */
/* if-nez v4, :cond_3 */
/* goto/16 :goto_5 */
/* .line 394 */
} // :cond_3
v4 = this.mAppFeatureHelper;
v5 = this.mTopActivity;
/* iget v6, p0, Lcom/android/server/tof/ContactlessGestureController;->mDisplayRotation:I */
if ( v6 != null) { // if-eqz v6, :cond_5
/* if-ne v6, v3, :cond_4 */
} // :cond_4
/* move v3, v1 */
} // :cond_5
} // :goto_0
/* move v3, v2 */
} // :goto_1
v3 = (( com.android.server.tof.AppFeatureHelper ) v4 ).getSupportFeature ( v5, v3 ); // invoke-virtual {v4, v5, v3}, Lcom/android/server/tof/AppFeatureHelper;->getSupportFeature(Landroid/content/ComponentName;Z)I
/* if-lez v3, :cond_6 */
/* move v3, v2 */
} // :cond_6
/* move v3, v1 */
/* .line 397 */
/* .local v3, "isSupported":Z */
} // :goto_2
/* sget-boolean v4, Lcom/android/server/tof/ContactlessGestureService;->mDebug:Z */
if ( v4 != null) { // if-eqz v4, :cond_8
/* .line 398 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "isSupported:"; // const-string v5, "isSupported:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
/* .line 399 */
v5 = this.mTopActivity;
if ( v5 != null) { // if-eqz v5, :cond_7
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = ", component: "; // const-string v6, ", component: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.mTopActivity;
(( android.content.ComponentName ) v6 ).toString ( ); // invoke-virtual {v6}, Landroid/content/ComponentName;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // :cond_7
final String v5 = ""; // const-string v5, ""
} // :goto_3
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 398 */
final String v5 = "ContactlessGestureController"; // const-string v5, "ContactlessGestureController"
android.util.Slog .i ( v5,v4 );
/* .line 401 */
} // :cond_8
/* invoke-direct {p0, v3}, Lcom/android/server/tof/ContactlessGestureController;->registerListenerDelayIfNeeded(Z)V */
/* .line 403 */
if ( v3 != null) { // if-eqz v3, :cond_9
v4 = (( com.android.server.tof.ContactlessGestureController ) p0 ).getCurrentSupportFeature ( ); // invoke-virtual {p0}, Lcom/android/server/tof/ContactlessGestureController;->getCurrentSupportFeature()I
if ( v4 != null) { // if-eqz v4, :cond_9
/* .line 404 */
v4 = this.mContactlessGestureService;
/* iget-boolean v4, v4, Lcom/android/server/tof/ContactlessGestureService;->mAonGestureEnabled:Z */
if ( v4 != null) { // if-eqz v4, :cond_a
/* .line 406 */
v4 = this.mContactlessGestureService;
(( com.android.server.tof.ContactlessGestureService ) v4 ).startGestureClientIfNeeded ( ); // invoke-virtual {v4}, Lcom/android/server/tof/ContactlessGestureService;->startGestureClientIfNeeded()V
/* .line 407 */
/* const/16 v4, 0xe */
(( com.android.server.tof.ContactlessGestureController ) p0 ).updateGestureHint ( v4 ); // invoke-virtual {p0, v4}, Lcom/android/server/tof/ContactlessGestureController;->updateGestureHint(I)Z
/* .line 410 */
/* if-eq p1, v2, :cond_a */
int v2 = 3; // const/4 v2, 0x3
/* if-eq p1, v2, :cond_a */
/* .line 411 */
/* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureController;->showLastTriggerViewIfNeeded()V */
/* .line 412 */
/* iput-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureController;->mShouldUpdateTriggerView:Z */
/* .line 417 */
} // :cond_9
/* const/16 v1, 0xd */
(( com.android.server.tof.ContactlessGestureController ) p0 ).updateGestureHint ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/tof/ContactlessGestureController;->updateGestureHint(I)Z
/* .line 418 */
/* iput-boolean v2, p0, Lcom/android/server/tof/ContactlessGestureController;->mShouldUpdateTriggerView:Z */
/* .line 420 */
} // :cond_a
} // :goto_4
return;
/* .line 390 */
} // .end local v3 # "isSupported":Z
} // :cond_b
} // :goto_5
/* invoke-direct {p0, v1}, Lcom/android/server/tof/ContactlessGestureController;->registerListenerDelayIfNeeded(Z)V */
/* .line 391 */
return;
/* .line 383 */
} // :cond_c
} // :goto_6
/* invoke-direct {p0, v2, v2}, Lcom/android/server/tof/ContactlessGestureController;->registerListenerDelayIfNeeded(ZZ)V */
/* .line 384 */
return;
/* .line 376 */
} // .end local v0 # "callState":I
} // :cond_d
} // :goto_7
/* invoke-direct {p0, v1}, Lcom/android/server/tof/ContactlessGestureController;->registerListenerDelayIfNeeded(Z)V */
/* .line 377 */
return;
} // .end method
public void parseGestureComponentFromCloud ( java.util.List p0 ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 907 */
/* .local p1, "componentFeatureList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = this.mAppFeatureHelper;
(( com.android.server.tof.AppFeatureHelper ) v0 ).parseContactlessGestureComponentFromCloud ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/tof/AppFeatureHelper;->parseContactlessGestureComponentFromCloud(Ljava/util/List;)V
/* .line 908 */
return;
} // .end method
public abstract void registerGestureListenerIfNeeded ( Boolean p0 ) {
} // .end method
public abstract void restContactlessGestureService ( ) {
} // .end method
public abstract void showGestureNotification ( ) {
} // .end method
public void unBindService ( ) {
/* .locals 2 */
/* .line 324 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/tof/ContactlessGestureController$$ExternalSyntheticLambda4; */
/* invoke-direct {v1, p0}, Lcom/android/server/tof/ContactlessGestureController$$ExternalSyntheticLambda4;-><init>(Lcom/android/server/tof/ContactlessGestureController;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 327 */
return;
} // .end method
public void updateDeskTopMode ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "enable" # Z */
/* .line 432 */
/* iput-boolean p1, p0, Lcom/android/server/tof/ContactlessGestureController;->mIsDeskTopMode:Z */
/* .line 433 */
return;
} // .end method
public void updateFocusedPackage ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 441 */
this.mFocusedPackage = p1;
/* .line 442 */
v0 = /* invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureController;->isTopActivityFocused()Z */
/* if-nez v0, :cond_0 */
/* .line 443 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/tof/ContactlessGestureController$$ExternalSyntheticLambda1; */
/* invoke-direct {v1, p0}, Lcom/android/server/tof/ContactlessGestureController$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/tof/ContactlessGestureController;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 445 */
} // :cond_0
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/tof/ContactlessGestureController$$ExternalSyntheticLambda2; */
/* invoke-direct {v1, p0}, Lcom/android/server/tof/ContactlessGestureController$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/tof/ContactlessGestureController;)V */
/* const-wide/16 v2, 0x708 */
(( android.os.Handler ) v0 ).postDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 450 */
} // :goto_0
return;
} // .end method
public abstract Boolean updateGestureHint ( Integer p0 ) {
} // .end method
