.class Lcom/android/server/tof/ContactlessGestureService$1;
.super Ljava/lang/Object;
.source "ContactlessGestureService.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/tof/ContactlessGestureService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/tof/ContactlessGestureService;


# direct methods
.method constructor <init>(Lcom/android/server/tof/ContactlessGestureService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/tof/ContactlessGestureService;

    .line 307
    iput-object p1, p0, Lcom/android/server/tof/ContactlessGestureService$1;->this$0:Lcom/android/server/tof/ContactlessGestureService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "i"    # I

    .line 333
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 6
    .param p1, "sensorEvent"    # Landroid/hardware/SensorEvent;

    .line 310
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    .line 311
    .local v0, "value":F
    iget-object v2, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v2}, Landroid/hardware/Sensor;->getType()I

    move-result v2

    .line 312
    .local v2, "sensorType":I
    invoke-static {}, Lcom/android/server/tof/ContactlessGestureService;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onSensorChanged, sensor:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v5}, Landroid/hardware/Sensor;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", value:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    const/4 v3, 0x1

    const/high16 v4, 0x3f800000    # 1.0f

    packed-switch v2, :pswitch_data_0

    .line 325
    :pswitch_0
    invoke-static {}, Lcom/android/server/tof/ContactlessGestureService;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v3, "unknown sensor"

    invoke-static {v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 319
    :pswitch_1
    iget-object v5, p0, Lcom/android/server/tof/ContactlessGestureService$1;->this$0:Lcom/android/server/tof/ContactlessGestureService;

    cmpl-float v4, v0, v4

    if-eqz v4, :cond_0

    move v1, v3

    :cond_0
    invoke-static {v5, v1}, Lcom/android/server/tof/ContactlessGestureService;->-$$Nest$mhandleTofProximitySensorChanged(Lcom/android/server/tof/ContactlessGestureService;Z)V

    .line 320
    goto :goto_0

    .line 316
    :pswitch_2
    iget-object v5, p0, Lcom/android/server/tof/ContactlessGestureService$1;->this$0:Lcom/android/server/tof/ContactlessGestureService;

    cmpl-float v4, v0, v4

    if-nez v4, :cond_1

    move v1, v3

    :cond_1
    invoke-static {v5, v1}, Lcom/android/server/tof/ContactlessGestureService;->-$$Nest$mhandleTofProximitySensorChanged(Lcom/android/server/tof/ContactlessGestureService;Z)V

    .line 317
    goto :goto_0

    .line 322
    :pswitch_3
    iget-object v1, p0, Lcom/android/server/tof/ContactlessGestureService$1;->this$0:Lcom/android/server/tof/ContactlessGestureService;

    float-to-int v3, v0

    invoke-static {v1, v3}, Lcom/android/server/tof/ContactlessGestureService;->-$$Nest$mhandleGestureSensorChanged(Lcom/android/server/tof/ContactlessGestureService;I)V

    .line 323
    nop

    .line 328
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1fa2700
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
