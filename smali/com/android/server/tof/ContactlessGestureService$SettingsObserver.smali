.class final Lcom/android/server/tof/ContactlessGestureService$SettingsObserver;
.super Landroid/database/ContentObserver;
.source "ContactlessGestureService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/tof/ContactlessGestureService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SettingsObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/tof/ContactlessGestureService;


# direct methods
.method public constructor <init>(Lcom/android/server/tof/ContactlessGestureService;Landroid/os/Handler;)V
    .locals 0
    .param p2, "handler"    # Landroid/os/Handler;

    .line 589
    iput-object p1, p0, Lcom/android/server/tof/ContactlessGestureService$SettingsObserver;->this$0:Lcom/android/server/tof/ContactlessGestureService;

    .line 590
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 591
    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 1
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 595
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService$SettingsObserver;->this$0:Lcom/android/server/tof/ContactlessGestureService;

    invoke-static {v0, p2}, Lcom/android/server/tof/ContactlessGestureService;->-$$Nest$mhandleSettingsChanged(Lcom/android/server/tof/ContactlessGestureService;Landroid/net/Uri;)V

    .line 596
    return-void
.end method
