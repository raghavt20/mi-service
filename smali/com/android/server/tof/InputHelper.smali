.class public Lcom/android/server/tof/InputHelper;
.super Ljava/lang/Object;
.source "InputHelper.java"


# static fields
.field private static final SWIPE_DURATION:I = 0x96

.field private static final TAG:Ljava/lang/String; = "TofInputHelper"

.field private static final THREAD_NAME:Ljava/lang/String; = "input.tof"


# instance fields
.field private mDownPoint:Landroid/graphics/Point;

.field private mHandler:Landroid/os/Handler;

.field private mLeftPoint:Landroid/graphics/Point;

.field private mMiuiInputManagerInternal:Lcom/android/server/input/MiuiInputManagerInternal;

.field private mRightPoint:Landroid/graphics/Point;

.field private mUpPoint:Landroid/graphics/Point;

.field private mWindowManagerService:Lcom/android/server/wm/WindowManagerService;


# direct methods
.method public static synthetic $r8$lambda$TWdQiUm3MS3PB9-Bp97CbGQXce4(Lcom/android/server/tof/InputHelper;Landroid/graphics/Point;Landroid/graphics/Point;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/tof/InputHelper;->lambda$simulatedSwipe$1(Landroid/graphics/Point;Landroid/graphics/Point;)V

    return-void
.end method

.method public static synthetic $r8$lambda$uSr0UK20JnfPF-W4avpjkeonkG8(Lcom/android/server/tof/InputHelper;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/tof/InputHelper;->lambda$injectKeyEvent$0(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateSwipePoint(Lcom/android/server/tof/InputHelper;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/tof/InputHelper;->updateSwipePoint()V

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/android/server/tof/InputHelper;->mUpPoint:Landroid/graphics/Point;

    .line 35
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/android/server/tof/InputHelper;->mDownPoint:Landroid/graphics/Point;

    .line 36
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/android/server/tof/InputHelper;->mLeftPoint:Landroid/graphics/Point;

    .line 37
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/android/server/tof/InputHelper;->mRightPoint:Landroid/graphics/Point;

    .line 40
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "input.tof"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 41
    .local v0, "thread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 42
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/android/server/tof/InputHelper;->mHandler:Landroid/os/Handler;

    .line 43
    const-class v1, Lcom/android/server/input/MiuiInputManagerInternal;

    invoke-static {v1}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/input/MiuiInputManagerInternal;

    iput-object v1, p0, Lcom/android/server/tof/InputHelper;->mMiuiInputManagerInternal:Lcom/android/server/input/MiuiInputManagerInternal;

    .line 44
    const-string/jumbo v1, "window"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    check-cast v1, Lcom/android/server/wm/WindowManagerService;

    iput-object v1, p0, Lcom/android/server/tof/InputHelper;->mWindowManagerService:Lcom/android/server/wm/WindowManagerService;

    .line 45
    invoke-direct {p0}, Lcom/android/server/tof/InputHelper;->updateSwipePoint()V

    .line 46
    invoke-direct {p0}, Lcom/android/server/tof/InputHelper;->registerDisplayChangeListener()V

    .line 47
    return-void
.end method

.method private injectKeyEvent(I)V
    .locals 2
    .param p1, "keyCode"    # I

    .line 190
    iget-object v0, p0, Lcom/android/server/tof/InputHelper;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/tof/InputHelper$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0, p1}, Lcom/android/server/tof/InputHelper$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/tof/InputHelper;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 205
    return-void
.end method

.method private injectKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/KeyEvent;

    .line 208
    invoke-static {}, Landroid/hardware/input/InputManager;->getInstance()Landroid/hardware/input/InputManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/hardware/input/InputManager;->injectInputEvent(Landroid/view/InputEvent;I)Z

    move-result v0

    return v0
.end method

.method private synthetic lambda$injectKeyEvent$0(I)V
    .locals 19
    .param p1, "keyCode"    # I

    .line 191
    move-object/from16 v1, p0

    const-string v2, "TofInputHelper"

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v17

    .line 192
    .local v17, "now":J
    const/4 v7, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, -0x1

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-wide/from16 v3, v17

    move-wide/from16 v5, v17

    move/from16 v8, p1

    invoke-static/range {v3 .. v16}, Landroid/view/KeyEvent;->obtain(JJIIIIIIIIILjava/lang/String;)Landroid/view/KeyEvent;

    move-result-object v3

    .line 197
    .local v3, "event":Landroid/view/KeyEvent;
    :try_start_0
    invoke-direct {v1, v3}, Lcom/android/server/tof/InputHelper;->injectKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    .line 198
    .local v0, "result":Z
    const/4 v4, 0x1

    invoke-static {v3, v4}, Landroid/view/KeyEvent;->changeAction(Landroid/view/KeyEvent;I)Landroid/view/KeyEvent;

    move-result-object v4

    invoke-direct {v1, v4}, Lcom/android/server/tof/InputHelper;->injectKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v4

    and-int/2addr v0, v4

    .line 199
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "injectKeyEvent result:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 200
    invoke-static/range {p1 .. p1}, Landroid/view/KeyEvent;->keyCodeToString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 199
    invoke-static {v2, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 203
    nop

    .end local v0    # "result":Z
    goto :goto_0

    .line 201
    :catch_0
    move-exception v0

    .line 202
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :goto_0
    return-void
.end method

.method private synthetic lambda$simulatedSwipe$1(Landroid/graphics/Point;Landroid/graphics/Point;)V
    .locals 6
    .param p1, "start"    # Landroid/graphics/Point;
    .param p2, "end"    # Landroid/graphics/Point;

    .line 216
    iget-object v0, p0, Lcom/android/server/tof/InputHelper;->mMiuiInputManagerInternal:Lcom/android/server/input/MiuiInputManagerInternal;

    iget v1, p1, Landroid/graphics/Point;->x:I

    iget v2, p1, Landroid/graphics/Point;->y:I

    iget v3, p2, Landroid/graphics/Point;->x:I

    iget v4, p2, Landroid/graphics/Point;->y:I

    const/16 v5, 0x96

    invoke-virtual/range {v0 .. v5}, Lcom/android/server/input/MiuiInputManagerInternal;->swipe(IIIII)Z

    .line 218
    return-void
.end method

.method private registerDisplayChangeListener()V
    .locals 5

    .line 222
    invoke-static {}, Landroid/hardware/display/DisplayManagerGlobal;->getInstance()Landroid/hardware/display/DisplayManagerGlobal;

    move-result-object v0

    .line 223
    .local v0, "mGlobal":Landroid/hardware/display/DisplayManagerGlobal;
    new-instance v1, Lcom/android/server/tof/InputHelper$1;

    invoke-direct {v1, p0}, Lcom/android/server/tof/InputHelper$1;-><init>(Lcom/android/server/tof/InputHelper;)V

    iget-object v2, p0, Lcom/android/server/tof/InputHelper;->mHandler:Landroid/os/Handler;

    const-wide/16 v3, 0x4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/hardware/display/DisplayManagerGlobal;->registerDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;Landroid/os/Handler;J)V

    .line 239
    return-void
.end method

.method private updateSwipePoint()V
    .locals 8

    .line 242
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 245
    .local v0, "point":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/android/server/tof/InputHelper;->mWindowManagerService:Lcom/android/server/wm/WindowManagerService;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Lcom/android/server/wm/WindowManagerService;->getBaseDisplaySize(ILandroid/graphics/Point;)V

    .line 246
    iget-object v1, p0, Lcom/android/server/tof/InputHelper;->mWindowManagerService:Lcom/android/server/wm/WindowManagerService;

    invoke-virtual {v1}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayRotation()I

    move-result v1

    .line 248
    .local v1, "rotation":I
    const/4 v2, 0x1

    const/4 v3, 0x3

    if-eq v1, v2, :cond_1

    if-ne v1, v3, :cond_0

    goto :goto_0

    .line 252
    :cond_0
    iget v2, v0, Landroid/graphics/Point;->x:I

    .line 253
    .local v2, "displayWidth":I
    iget v4, v0, Landroid/graphics/Point;->y:I

    .local v4, "displayHeight":I
    goto :goto_1

    .line 249
    .end local v2    # "displayWidth":I
    .end local v4    # "displayHeight":I
    :cond_1
    :goto_0
    iget v2, v0, Landroid/graphics/Point;->y:I

    .line 250
    .restart local v2    # "displayWidth":I
    iget v4, v0, Landroid/graphics/Point;->x:I

    .line 255
    .restart local v4    # "displayHeight":I
    :goto_1
    iget-object v5, p0, Lcom/android/server/tof/InputHelper;->mUpPoint:Landroid/graphics/Point;

    div-int/lit8 v6, v2, 0x3

    div-int/lit8 v7, v4, 0x3

    invoke-virtual {v5, v6, v7}, Landroid/graphics/Point;->set(II)V

    .line 256
    iget-object v5, p0, Lcom/android/server/tof/InputHelper;->mDownPoint:Landroid/graphics/Point;

    div-int/lit8 v6, v2, 0x3

    add-int/lit8 v6, v6, 0x32

    mul-int/lit8 v7, v4, 0x2

    div-int/2addr v7, v3

    invoke-virtual {v5, v6, v7}, Landroid/graphics/Point;->set(II)V

    .line 257
    iget-object v3, p0, Lcom/android/server/tof/InputHelper;->mLeftPoint:Landroid/graphics/Point;

    div-int/lit8 v5, v2, 0x4

    div-int/lit8 v6, v4, 0x5

    invoke-virtual {v3, v5, v6}, Landroid/graphics/Point;->set(II)V

    .line 258
    iget-object v3, p0, Lcom/android/server/tof/InputHelper;->mRightPoint:Landroid/graphics/Point;

    mul-int/lit8 v5, v2, 0x3

    div-int/lit8 v5, v5, 0x4

    div-int/lit8 v6, v4, 0x5

    add-int/lit8 v6, v6, 0x32

    invoke-virtual {v3, v5, v6}, Landroid/graphics/Point;->set(II)V

    .line 259
    return-void
.end method


# virtual methods
.method public injectSupportInputEvent(I)V
    .locals 2
    .param p1, "feature"    # I

    .line 50
    sparse-switch p1, :sswitch_data_0

    .line 106
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "unsupported feature:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TofInputHelper"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 94
    :sswitch_0
    invoke-virtual {p0}, Lcom/android/server/tof/InputHelper;->sendSwipeRightEvent()V

    .line 95
    goto :goto_0

    .line 91
    :sswitch_1
    invoke-virtual {p0}, Lcom/android/server/tof/InputHelper;->sendSwipeLeftEvent()V

    .line 92
    goto :goto_0

    .line 103
    :sswitch_2
    invoke-virtual {p0}, Lcom/android/server/tof/InputHelper;->sendMediaPlayPauseSpaceEvent()V

    .line 104
    goto :goto_0

    .line 100
    :sswitch_3
    invoke-virtual {p0}, Lcom/android/server/tof/InputHelper;->sendMediaRewind()V

    .line 101
    goto :goto_0

    .line 97
    :sswitch_4
    invoke-virtual {p0}, Lcom/android/server/tof/InputHelper;->sendMediaFastForward()V

    .line 98
    goto :goto_0

    .line 88
    :sswitch_5
    invoke-virtual {p0}, Lcom/android/server/tof/InputHelper;->sendSwipeDownEvent()V

    .line 89
    goto :goto_0

    .line 85
    :sswitch_6
    invoke-virtual {p0}, Lcom/android/server/tof/InputHelper;->sendSwipeUpEvent()V

    .line 86
    goto :goto_0

    .line 82
    :sswitch_7
    invoke-virtual {p0}, Lcom/android/server/tof/InputHelper;->sendVolumeDownEvent()V

    .line 83
    goto :goto_0

    .line 79
    :sswitch_8
    invoke-virtual {p0}, Lcom/android/server/tof/InputHelper;->sendVolumeUpEvent()V

    .line 80
    goto :goto_0

    .line 76
    :sswitch_9
    invoke-virtual {p0}, Lcom/android/server/tof/InputHelper;->sendEndCallEvent()V

    .line 77
    goto :goto_0

    .line 73
    :sswitch_a
    invoke-virtual {p0}, Lcom/android/server/tof/InputHelper;->sendCallEvent()V

    .line 74
    goto :goto_0

    .line 70
    :sswitch_b
    invoke-virtual {p0}, Lcom/android/server/tof/InputHelper;->sendPageUpEvent()V

    .line 71
    goto :goto_0

    .line 67
    :sswitch_c
    invoke-virtual {p0}, Lcom/android/server/tof/InputHelper;->sendPageDownEvent()V

    .line 68
    goto :goto_0

    .line 61
    :sswitch_d
    invoke-virtual {p0}, Lcom/android/server/tof/InputHelper;->sendMediaPreviousEvent()V

    .line 62
    goto :goto_0

    .line 64
    :sswitch_e
    invoke-virtual {p0}, Lcom/android/server/tof/InputHelper;->sendMediaNextEvent()V

    .line 65
    goto :goto_0

    .line 58
    :sswitch_f
    invoke-virtual {p0}, Lcom/android/server/tof/InputHelper;->sendDpadLeftEvent()V

    .line 59
    goto :goto_0

    .line 55
    :sswitch_10
    invoke-virtual {p0}, Lcom/android/server/tof/InputHelper;->sendDpadRightEvent()V

    .line 56
    goto :goto_0

    .line 52
    :sswitch_11
    invoke-virtual {p0}, Lcom/android/server/tof/InputHelper;->sendMediaPlayPauseEvent()V

    .line 53
    nop

    .line 108
    :goto_0
    return-void

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_11
        0x2 -> :sswitch_10
        0x4 -> :sswitch_f
        0x8 -> :sswitch_e
        0x10 -> :sswitch_d
        0x20 -> :sswitch_c
        0x40 -> :sswitch_b
        0x80 -> :sswitch_a
        0x100 -> :sswitch_9
        0x200 -> :sswitch_8
        0x400 -> :sswitch_7
        0x800 -> :sswitch_6
        0x1000 -> :sswitch_5
        0x2000 -> :sswitch_4
        0x4000 -> :sswitch_3
        0x8000 -> :sswitch_2
        0x10000 -> :sswitch_1
        0x20000 -> :sswitch_0
    .end sparse-switch
.end method

.method public sendCallEvent()V
    .locals 1

    .line 115
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/android/server/tof/InputHelper;->injectKeyEvent(I)V

    .line 116
    return-void
.end method

.method public sendDpadLeftEvent()V
    .locals 1

    .line 138
    const/16 v0, 0x15

    invoke-direct {p0, v0}, Lcom/android/server/tof/InputHelper;->injectKeyEvent(I)V

    .line 139
    return-void
.end method

.method public sendDpadRightEvent()V
    .locals 1

    .line 142
    const/16 v0, 0x16

    invoke-direct {p0, v0}, Lcom/android/server/tof/InputHelper;->injectKeyEvent(I)V

    .line 143
    return-void
.end method

.method public sendEndCallEvent()V
    .locals 1

    .line 111
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/android/server/tof/InputHelper;->injectKeyEvent(I)V

    .line 112
    return-void
.end method

.method public sendMediaFastForward()V
    .locals 1

    .line 152
    const/16 v0, 0x5a

    invoke-direct {p0, v0}, Lcom/android/server/tof/InputHelper;->injectKeyEvent(I)V

    .line 153
    return-void
.end method

.method public sendMediaNextEvent()V
    .locals 1

    .line 130
    const/16 v0, 0x57

    invoke-direct {p0, v0}, Lcom/android/server/tof/InputHelper;->injectKeyEvent(I)V

    .line 131
    return-void
.end method

.method public sendMediaPlayPauseEvent()V
    .locals 1

    .line 156
    const/16 v0, 0x55

    invoke-direct {p0, v0}, Lcom/android/server/tof/InputHelper;->injectKeyEvent(I)V

    .line 157
    return-void
.end method

.method public sendMediaPlayPauseSpaceEvent()V
    .locals 1

    .line 160
    const/16 v0, 0x3e

    invoke-direct {p0, v0}, Lcom/android/server/tof/InputHelper;->injectKeyEvent(I)V

    .line 161
    return-void
.end method

.method public sendMediaPreviousEvent()V
    .locals 1

    .line 134
    const/16 v0, 0x58

    invoke-direct {p0, v0}, Lcom/android/server/tof/InputHelper;->injectKeyEvent(I)V

    .line 135
    return-void
.end method

.method public sendMediaRewind()V
    .locals 1

    .line 147
    const/16 v0, 0x59

    invoke-direct {p0, v0}, Lcom/android/server/tof/InputHelper;->injectKeyEvent(I)V

    .line 148
    return-void
.end method

.method public sendPageDownEvent()V
    .locals 1

    .line 123
    const/16 v0, 0x5d

    invoke-direct {p0, v0}, Lcom/android/server/tof/InputHelper;->injectKeyEvent(I)V

    .line 124
    return-void
.end method

.method public sendPageUpEvent()V
    .locals 1

    .line 119
    const/16 v0, 0x5c

    invoke-direct {p0, v0}, Lcom/android/server/tof/InputHelper;->injectKeyEvent(I)V

    .line 120
    return-void
.end method

.method public sendSwipeDownEvent()V
    .locals 2

    .line 178
    iget-object v0, p0, Lcom/android/server/tof/InputHelper;->mUpPoint:Landroid/graphics/Point;

    iget-object v1, p0, Lcom/android/server/tof/InputHelper;->mDownPoint:Landroid/graphics/Point;

    invoke-virtual {p0, v0, v1}, Lcom/android/server/tof/InputHelper;->simulatedSwipe(Landroid/graphics/Point;Landroid/graphics/Point;)V

    .line 179
    return-void
.end method

.method public sendSwipeLeftEvent()V
    .locals 2

    .line 182
    iget-object v0, p0, Lcom/android/server/tof/InputHelper;->mRightPoint:Landroid/graphics/Point;

    iget-object v1, p0, Lcom/android/server/tof/InputHelper;->mLeftPoint:Landroid/graphics/Point;

    invoke-virtual {p0, v0, v1}, Lcom/android/server/tof/InputHelper;->simulatedSwipe(Landroid/graphics/Point;Landroid/graphics/Point;)V

    .line 183
    return-void
.end method

.method public sendSwipeRightEvent()V
    .locals 2

    .line 186
    iget-object v0, p0, Lcom/android/server/tof/InputHelper;->mLeftPoint:Landroid/graphics/Point;

    iget-object v1, p0, Lcom/android/server/tof/InputHelper;->mRightPoint:Landroid/graphics/Point;

    invoke-virtual {p0, v0, v1}, Lcom/android/server/tof/InputHelper;->simulatedSwipe(Landroid/graphics/Point;Landroid/graphics/Point;)V

    .line 187
    return-void
.end method

.method public sendSwipeUpEvent()V
    .locals 2

    .line 174
    iget-object v0, p0, Lcom/android/server/tof/InputHelper;->mDownPoint:Landroid/graphics/Point;

    iget-object v1, p0, Lcom/android/server/tof/InputHelper;->mUpPoint:Landroid/graphics/Point;

    invoke-virtual {p0, v0, v1}, Lcom/android/server/tof/InputHelper;->simulatedSwipe(Landroid/graphics/Point;Landroid/graphics/Point;)V

    .line 175
    return-void
.end method

.method public sendVolumeDownEvent()V
    .locals 1

    .line 170
    const/16 v0, 0x19

    invoke-direct {p0, v0}, Lcom/android/server/tof/InputHelper;->injectKeyEvent(I)V

    .line 171
    return-void
.end method

.method public sendVolumeUpEvent()V
    .locals 1

    .line 165
    const/16 v0, 0x18

    invoke-direct {p0, v0}, Lcom/android/server/tof/InputHelper;->injectKeyEvent(I)V

    .line 166
    return-void
.end method

.method public simulatedSwipe(Landroid/graphics/Point;Landroid/graphics/Point;)V
    .locals 2
    .param p1, "start"    # Landroid/graphics/Point;
    .param p2, "end"    # Landroid/graphics/Point;

    .line 215
    iget-object v0, p0, Lcom/android/server/tof/InputHelper;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/tof/InputHelper$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/server/tof/InputHelper$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/tof/InputHelper;Landroid/graphics/Point;Landroid/graphics/Point;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 219
    return-void
.end method
