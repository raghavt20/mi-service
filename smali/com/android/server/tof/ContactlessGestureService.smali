.class public Lcom/android/server/tof/ContactlessGestureService;
.super Lcom/android/server/SystemService;
.source "ContactlessGestureService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/tof/ContactlessGestureService$SettingsObserver;,
        Lcom/android/server/tof/ContactlessGestureService$BinderService;,
        Lcom/android/server/tof/ContactlessGestureService$LocalService;,
        Lcom/android/server/tof/ContactlessGestureService$TofServiceShellCommand;
    }
.end annotation


# static fields
.field private static final DEBOUNCE_TIME:I = 0x1388

.field private static final EXTERNAL_DISPLAY_CONNECTED:I = 0x1

.field public static final FEATURE_AON_GESTURE_SUPPORT:Ljava/lang/String; = "config_aon_gesture_available"

.field public static final FEATURE_TOF_GESTURE_SUPPORT:Ljava/lang/String; = "config_tof_gesture_available"

.field public static final FEATURE_TOF_PROXIMITY_SUPPORT:Ljava/lang/String; = "config_tof_proximity_available"

.field private static final GESTURE_GUIDE_DISMISS:Ljava/lang/String; = "gesture_guide_dismiss"

.field private static final GESTURE_THREAD_NAME:Ljava/lang/String; = "miui.gesture"

.field private static final MIUI_DESK_TOP_MODE:Ljava/lang/String; = "miui_dkt_mode"

.field private static final SERVICE_NAME:Ljava/lang/String; = "tof"

.field private static final SYNERGY_MODE_ON:I = 0x1

.field private static final TAG:Ljava/lang/String;

.field private static final TOF_GESTURE_SENSOR_TYPE:I = 0x1fa2700

.field private static final TOF_PERSON_LEAVE_SENSOR_TYPE:I = 0x1fa2704

.field private static final TOF_PERSON_NEAR_SENSOR_TYPE:I = 0x1fa2703

.field public static mDebug:Z


# instance fields
.field private mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

.field public mAonGestureEnabled:Z

.field private mAonGestureSupport:Z

.field private mComponentFeatureList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mContactlessGestureController:Lcom/android/server/tof/ContactlessGestureController;

.field private final mContext:Landroid/content/Context;

.field private mDeskTopModeEnabled:Z

.field private mExternalDisplayConnected:Z

.field private mHandler:Landroid/os/Handler;

.field private mIsInteractive:Z

.field private mOneHandedModeEnable:Z

.field private mPowerManager:Landroid/os/PowerManager;

.field private mPowerManagerServiceImpl:Lcom/android/server/power/PowerManagerServiceStub;

.field private mScreenProjectInScreen:Z

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mSettingsObserver:Lcom/android/server/tof/ContactlessGestureService$SettingsObserver;

.field private mSynergyModeEnable:Z

.field private mTofGestureEnabled:Z

.field private mTofGestureRegistered:Z

.field private mTofGestureSensor:Landroid/hardware/Sensor;

.field private mTofGestureSupport:Z

.field private mTofPersonLeaveRegistered:Z

.field private mTofPersonLeaveSensor:Landroid/hardware/Sensor;

.field private mTofPersonNearRegistered:Z

.field private mTofPersonNearSensor:Landroid/hardware/Sensor;

.field private mTofProximity:Z

.field private mTofProximitySupport:Z

.field private mTofScreenOffEnabled:Z

.field private mTofScreenOnEnabled:Z

.field private final mTofSensorListener:Landroid/hardware/SensorEventListener;


# direct methods
.method static bridge synthetic -$$Nest$fgetmAonGestureSupport(Lcom/android/server/tof/ContactlessGestureService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/tof/ContactlessGestureService;->mAonGestureSupport:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmContactlessGestureController(Lcom/android/server/tof/ContactlessGestureService;)Lcom/android/server/tof/ContactlessGestureController;
    .locals 0

    iget-object p0, p0, Lcom/android/server/tof/ContactlessGestureService;->mContactlessGestureController:Lcom/android/server/tof/ContactlessGestureController;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/tof/ContactlessGestureService;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/tof/ContactlessGestureService;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/server/tof/ContactlessGestureService;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/tof/ContactlessGestureService;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmTofGestureEnabled(Lcom/android/server/tof/ContactlessGestureService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureEnabled:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmTofGestureSupport(Lcom/android/server/tof/ContactlessGestureService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureSupport:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmTofProximitySupport(Lcom/android/server/tof/ContactlessGestureService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofProximitySupport:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmComponentFeatureList(Lcom/android/server/tof/ContactlessGestureService;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/tof/ContactlessGestureService;->mComponentFeatureList:Ljava/util/List;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsInteractive(Lcom/android/server/tof/ContactlessGestureService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/tof/ContactlessGestureService;->mIsInteractive:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mdumpInternal(Lcom/android/server/tof/ContactlessGestureService;Ljava/io/PrintWriter;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/tof/ContactlessGestureService;->dumpInternal(Ljava/io/PrintWriter;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleGestureSensorChanged(Lcom/android/server/tof/ContactlessGestureService;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/tof/ContactlessGestureService;->handleGestureSensorChanged(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleSettingsChanged(Lcom/android/server/tof/ContactlessGestureService;Landroid/net/Uri;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/tof/ContactlessGestureService;->handleSettingsChanged(Landroid/net/Uri;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleTofProximitySensorChanged(Lcom/android/server/tof/ContactlessGestureService;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/tof/ContactlessGestureService;->handleTofProximitySensorChanged(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$misAonGestureSupport(Lcom/android/server/tof/ContactlessGestureService;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->isAonGestureSupport()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mregisterTofProximityListenerIfNeeded(Lcom/android/server/tof/ContactlessGestureService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->registerTofProximityListenerIfNeeded()V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetDebug(Lcom/android/server/tof/ContactlessGestureService;Z)I
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/tof/ContactlessGestureService;->setDebug(Z)I

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$sfgetTAG()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/server/tof/ContactlessGestureService;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 66
    const-class v0, Lcom/android/server/tof/ContactlessGestureService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/tof/ContactlessGestureService;->TAG:Ljava/lang/String;

    .line 67
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/server/tof/ContactlessGestureService;->mDebug:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 119
    invoke-direct {p0, p1}, Lcom/android/server/SystemService;-><init>(Landroid/content/Context;)V

    .line 108
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mIsInteractive:Z

    .line 307
    new-instance v0, Lcom/android/server/tof/ContactlessGestureService$1;

    invoke-direct {v0, p0}, Lcom/android/server/tof/ContactlessGestureService$1;-><init>(Lcom/android/server/tof/ContactlessGestureService;)V

    iput-object v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofSensorListener:Landroid/hardware/SensorEventListener;

    .line 120
    iput-object p1, p0, Lcom/android/server/tof/ContactlessGestureService;->mContext:Landroid/content/Context;

    .line 121
    new-instance v0, Lcom/android/server/ServiceThread;

    const/4 v1, -0x4

    const/4 v2, 0x0

    const-string v3, "miui.gesture"

    invoke-direct {v0, v3, v1, v2}, Lcom/android/server/ServiceThread;-><init>(Ljava/lang/String;IZ)V

    .line 123
    .local v0, "handlerThread":Lcom/android/server/ServiceThread;
    invoke-virtual {v0}, Lcom/android/server/ServiceThread;->start()V

    .line 124
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Lcom/android/server/ServiceThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mHandler:Landroid/os/Handler;

    .line 125
    new-instance v1, Lcom/android/server/tof/ContactlessGestureService$SettingsObserver;

    iget-object v2, p0, Lcom/android/server/tof/ContactlessGestureService;->mHandler:Landroid/os/Handler;

    invoke-direct {v1, p0, v2}, Lcom/android/server/tof/ContactlessGestureService$SettingsObserver;-><init>(Lcom/android/server/tof/ContactlessGestureService;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mSettingsObserver:Lcom/android/server/tof/ContactlessGestureService$SettingsObserver;

    .line 126
    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->checkFeatureSupport()V

    .line 127
    return-void
.end method

.method private checkFeatureSupport()V
    .locals 3

    .line 402
    const-string v0, "config_tof_proximity_available"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofProximitySupport:Z

    .line 403
    const-string v0, "config_tof_gesture_available"

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureSupport:Z

    .line 404
    const-string v0, "config_aon_gesture_available"

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mAonGestureSupport:Z

    .line 405
    sget-object v0, Lcom/android/server/tof/ContactlessGestureService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "check feature support mTofProximitySupport:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofProximitySupport:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mTofGestureSupport:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureSupport:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mAonGestureSupport:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/tof/ContactlessGestureService;->mAonGestureSupport:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 408
    return-void
.end method

.method private dumpInternal(Ljava/io/PrintWriter;)V
    .locals 2
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 564
    const-string v0, "ContactlessGesture service status "

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 565
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "   mTofProximitySupport:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofProximitySupport:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 566
    iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofProximitySupport:Z

    if-eqz v0, :cond_0

    .line 567
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "       mTofProximity:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofProximity:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 568
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "       mTofScreenOnEnabled:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofScreenOnEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 569
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "       mTofScreenOffEnabled:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofScreenOffEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 570
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "       mTofPersonNearRegistered:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofPersonNearRegistered:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 571
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "       mTofPersonLeaveRegistered:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofPersonLeaveRegistered:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 573
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "   mTofGestureSupport:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureSupport:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 574
    iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureSupport:Z

    if-eqz v0, :cond_1

    .line 575
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "       mTofGestureEnabled:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 576
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "       mTofGestureRegistered:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureRegistered:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 578
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "   mAonGestureSupport:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mAonGestureSupport:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 579
    iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureSupport:Z

    if-eqz v0, :cond_2

    .line 580
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "       mAonGestureEnabled:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mAonGestureEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 583
    :cond_2
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mContactlessGestureController:Lcom/android/server/tof/ContactlessGestureController;

    if-eqz v0, :cond_3

    .line 584
    invoke-virtual {v0, p1}, Lcom/android/server/tof/ContactlessGestureController;->dump(Ljava/io/PrintWriter;)V

    .line 586
    :cond_3
    return-void
.end method

.method private handleDeskTopModeChanged()V
    .locals 2

    .line 388
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mContactlessGestureController:Lcom/android/server/tof/ContactlessGestureController;

    iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mDeskTopModeEnabled:Z

    invoke-virtual {v0, v1}, Lcom/android/server/tof/ContactlessGestureController;->updateDeskTopMode(Z)V

    .line 389
    return-void
.end method

.method private handleGestureGuideDismiss()V
    .locals 5

    .line 482
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mContext:Landroid/content/Context;

    .line 483
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 482
    const-string v1, "gesture_guide_dismiss"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v3, 0x1

    if-ne v0, v3, :cond_0

    goto :goto_0

    :cond_0
    move v3, v2

    :goto_0
    move v0, v3

    .line 484
    .local v0, "guideDismiss":Z
    iget-boolean v3, p0, Lcom/android/server/tof/ContactlessGestureService;->mAonGestureSupport:Z

    if-eqz v3, :cond_1

    iget-boolean v3, p0, Lcom/android/server/tof/ContactlessGestureService;->mAonGestureEnabled:Z

    if-eqz v3, :cond_1

    if-eqz v0, :cond_1

    .line 485
    sget-object v3, Lcom/android/server/tof/ContactlessGestureService;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "the guide dismiss,register aon listener anew."

    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 486
    iget-object v3, p0, Lcom/android/server/tof/ContactlessGestureService;->mContactlessGestureController:Lcom/android/server/tof/ContactlessGestureController;

    invoke-virtual {v3, v2}, Lcom/android/server/tof/ContactlessGestureController;->registerGestureListenerIfNeeded(Z)V

    .line 487
    iget-object v3, p0, Lcom/android/server/tof/ContactlessGestureService;->mContactlessGestureController:Lcom/android/server/tof/ContactlessGestureController;

    const/4 v4, 0x6

    invoke-virtual {v3, v4}, Lcom/android/server/tof/ContactlessGestureController;->onSceneChanged(I)V

    .line 488
    iget-object v3, p0, Lcom/android/server/tof/ContactlessGestureService;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {v3, v1, v2}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 490
    :cond_1
    return-void
.end method

.method private handleGestureSensorChanged(I)V
    .locals 1
    .param p1, "label"    # I

    .line 342
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mContactlessGestureController:Lcom/android/server/tof/ContactlessGestureController;

    invoke-virtual {v0}, Lcom/android/server/tof/ContactlessGestureController;->isServiceInit()Z

    move-result v0

    if-nez v0, :cond_0

    .line 343
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mContactlessGestureController:Lcom/android/server/tof/ContactlessGestureController;

    invoke-virtual {v0}, Lcom/android/server/tof/ContactlessGestureController;->bindService()V

    .line 345
    :cond_0
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mContactlessGestureController:Lcom/android/server/tof/ContactlessGestureController;

    invoke-virtual {v0, p1}, Lcom/android/server/tof/ContactlessGestureController;->handleGestureEvent(I)V

    .line 346
    return-void
.end method

.method private handleOneHandedModeEnableChanged()V
    .locals 4

    .line 492
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, -0x2

    const-string v2, "one_handed_mode_activated"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v3, 0x1

    :cond_0
    iput-boolean v3, p0, Lcom/android/server/tof/ContactlessGestureService;->mOneHandedModeEnable:Z

    .line 494
    iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mAonGestureSupport:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mAonGestureEnabled:Z

    if-eqz v0, :cond_1

    .line 495
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mContactlessGestureController:Lcom/android/server/tof/ContactlessGestureController;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/android/server/tof/ContactlessGestureController;->onSceneChanged(I)V

    .line 497
    :cond_1
    return-void
.end method

.method private handleScreenCastModeChanged()V
    .locals 2

    .line 392
    iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureSupport:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureEnabled:Z

    if-eqz v0, :cond_0

    .line 393
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mContactlessGestureController:Lcom/android/server/tof/ContactlessGestureController;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/android/server/tof/ContactlessGestureController;->onSceneChanged(I)V

    .line 395
    :cond_0
    return-void
.end method

.method private handleSettingsChanged(Landroid/net/Uri;)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .line 440
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    goto/16 :goto_0

    :sswitch_0
    const-string/jumbo v1, "screen_project_in_screening"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    goto :goto_1

    :sswitch_1
    const-string v1, "miui_dkt_mode"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    goto :goto_1

    :sswitch_2
    const-string v1, "miui_aon_gesture"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    goto :goto_1

    :sswitch_3
    const-string v1, "one_handed_mode_activated"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x7

    goto :goto_1

    :sswitch_4
    const-string v1, "miui_tof_screen_on"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_5
    const-string v1, "miui_tof_screen_off"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_6
    const-string v1, "gesture_guide_dismiss"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x9

    goto :goto_1

    :sswitch_7
    const-string v1, "miui_tof_gesture"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    goto :goto_1

    :sswitch_8
    const-string/jumbo v1, "synergy_mode"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x5

    goto :goto_1

    :sswitch_9
    const-string v1, "external_display_connected"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    goto :goto_1

    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_0

    goto :goto_2

    .line 470
    :pswitch_0
    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->handleGestureGuideDismiss()V

    .line 471
    goto :goto_2

    .line 467
    :pswitch_1
    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->updateDeskTopModeConfig()V

    .line 468
    goto :goto_2

    .line 464
    :pswitch_2
    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->handleOneHandedModeEnableChanged()V

    .line 465
    goto :goto_2

    .line 458
    :pswitch_3
    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->updateScreenProjectConfig()V

    .line 459
    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->updateSynergyModeConfig()V

    .line 460
    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->updateExternalDisplayConnectConfig()V

    .line 461
    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->handleScreenCastModeChanged()V

    .line 462
    goto :goto_2

    .line 452
    :pswitch_4
    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->updateAonGestureConfig()V

    .line 453
    invoke-virtual {p0}, Lcom/android/server/tof/ContactlessGestureService;->startGestureClientIfNeeded()V

    .line 454
    goto :goto_2

    .line 448
    :pswitch_5
    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->updateTofGestureConfig()V

    .line 449
    invoke-virtual {p0}, Lcom/android/server/tof/ContactlessGestureService;->startGestureClientIfNeeded()V

    .line 450
    goto :goto_2

    .line 443
    :pswitch_6
    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->updateTofScreenOnConfig()V

    .line 444
    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->updateTofScreenOffConfig()V

    .line 445
    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->registerTofProximityListenerIfNeeded()V

    .line 446
    nop

    .line 476
    :goto_2
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x688973a8 -> :sswitch_9
        -0x55bbaa45 -> :sswitch_8
        -0x320b7a3a -> :sswitch_7
        -0x17e86c2f -> :sswitch_6
        -0x17a1ce81 -> :sswitch_5
        -0x9053831 -> :sswitch_4
        0x2730082d -> :sswitch_3
        0x631eb97b -> :sswitch_2
        0x75414804 -> :sswitch_1
        0x790152b5 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private handleTofProximitySensorChanged(Z)V
    .locals 12
    .param p1, "proximity"    # Z

    .line 359
    iput-boolean p1, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofProximity:Z

    .line 360
    invoke-virtual {p0}, Lcom/android/server/tof/ContactlessGestureService;->isScreenCastMode()Z

    move-result v0

    .line 361
    .local v0, "hangupMode":Z
    const/4 v1, 0x0

    .line 362
    .local v1, "screenWakeLock":Z
    const-wide/16 v2, 0x0

    .line 363
    .local v2, "lastUserActivityTime":J
    if-eqz v0, :cond_0

    .line 364
    sget-object v4, Lcom/android/server/tof/ContactlessGestureService;->TAG:Ljava/lang/String;

    const-string v5, "ignore Tof sensor in hangup mode"

    invoke-static {v4, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    return-void

    .line 368
    :cond_0
    iget-boolean v4, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofProximity:Z

    const/4 v5, 0x0

    if-eqz v4, :cond_1

    .line 369
    iget-object v4, p0, Lcom/android/server/tof/ContactlessGestureService;->mPowerManager:Landroid/os/PowerManager;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    const-string/jumbo v8, "tof"

    invoke-virtual {v4, v6, v7, v5, v8}, Landroid/os/PowerManager;->wakeUp(JILjava/lang/String;)V

    .line 371
    iget-object v4, p0, Lcom/android/server/tof/ContactlessGestureService;->mPowerManagerServiceImpl:Lcom/android/server/power/PowerManagerServiceStub;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/android/server/power/PowerManagerServiceStub;->notifyTofPowerState(Z)V

    goto :goto_0

    .line 373
    :cond_1
    iget-object v4, p0, Lcom/android/server/tof/ContactlessGestureService;->mPowerManagerServiceImpl:Lcom/android/server/power/PowerManagerServiceStub;

    invoke-virtual {v4, v5}, Lcom/android/server/power/PowerManagerServiceStub;->isAcquiredScreenBrightWakeLock(I)Z

    move-result v1

    .line 374
    iget-object v4, p0, Lcom/android/server/tof/ContactlessGestureService;->mPowerManagerServiceImpl:Lcom/android/server/power/PowerManagerServiceStub;

    invoke-virtual {v4}, Lcom/android/server/power/PowerManagerServiceStub;->getDefaultDisplayLastUserActivity()J

    move-result-wide v2

    .line 375
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    .line 376
    .local v6, "time":J
    if-nez v1, :cond_2

    sub-long v8, v6, v2

    const-wide/16 v10, 0x1388

    cmp-long v4, v8, v10

    if-lez v4, :cond_2

    .line 378
    iget-object v4, p0, Lcom/android/server/tof/ContactlessGestureService;->mPowerManager:Landroid/os/PowerManager;

    invoke-virtual {v4, v6, v7, v5, v5}, Landroid/os/PowerManager;->goToSleep(JII)V

    .line 379
    iget-object v4, p0, Lcom/android/server/tof/ContactlessGestureService;->mPowerManagerServiceImpl:Lcom/android/server/power/PowerManagerServiceStub;

    invoke-virtual {v4, v5}, Lcom/android/server/power/PowerManagerServiceStub;->notifyTofPowerState(Z)V

    .line 382
    .end local v6    # "time":J
    :cond_2
    :goto_0
    sget-object v4, Lcom/android/server/tof/ContactlessGestureService;->TAG:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofProximity:Z

    if-eqz v5, :cond_3

    const-string/jumbo v5, "wake up"

    goto :goto_1

    .line 384
    :cond_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "go to sleep, screenWakeLock: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", lastUserActivity: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v2, v3}, Landroid/util/TimeUtils;->formatUptime(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 382
    :goto_1
    invoke-static {v4, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 385
    return-void
.end method

.method private isAonGestureSupport()Z
    .locals 1

    .line 398
    const/4 v0, 0x1

    return v0
.end method

.method private registerContentObserver()V
    .locals 5

    .line 187
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 188
    .local v0, "cr":Landroid/content/ContentResolver;
    const-string v1, "miui_tof_screen_on"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tof/ContactlessGestureService;->mSettingsObserver:Lcom/android/server/tof/ContactlessGestureService$SettingsObserver;

    const/4 v3, 0x0

    const/4 v4, -0x1

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 191
    const-string v1, "miui_tof_screen_off"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tof/ContactlessGestureService;->mSettingsObserver:Lcom/android/server/tof/ContactlessGestureService$SettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 194
    const-string v1, "miui_dkt_mode"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tof/ContactlessGestureService;->mSettingsObserver:Lcom/android/server/tof/ContactlessGestureService$SettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 196
    const-string v1, "miui_tof_gesture"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tof/ContactlessGestureService;->mSettingsObserver:Lcom/android/server/tof/ContactlessGestureService$SettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 199
    const-string v1, "miui_aon_gesture"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tof/ContactlessGestureService;->mSettingsObserver:Lcom/android/server/tof/ContactlessGestureService$SettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 202
    const-string/jumbo v1, "screen_project_in_screening"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tof/ContactlessGestureService;->mSettingsObserver:Lcom/android/server/tof/ContactlessGestureService$SettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 205
    const-string v1, "one_handed_mode_activated"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tof/ContactlessGestureService;->mSettingsObserver:Lcom/android/server/tof/ContactlessGestureService$SettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 208
    const-string/jumbo v1, "synergy_mode"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tof/ContactlessGestureService;->mSettingsObserver:Lcom/android/server/tof/ContactlessGestureService$SettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 212
    const-string v1, "external_display_connected"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tof/ContactlessGestureService;->mSettingsObserver:Lcom/android/server/tof/ContactlessGestureService$SettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 215
    const-string v1, "gesture_guide_dismiss"

    invoke-static {v1}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tof/ContactlessGestureService;->mSettingsObserver:Lcom/android/server/tof/ContactlessGestureService$SettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 217
    return-void
.end method

.method private registerPersonLeaveListenerIfNeeded(Z)V
    .locals 4
    .param p1, "register"    # Z

    .line 236
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mSensorManager:Landroid/hardware/SensorManager;

    if-nez v0, :cond_0

    .line 237
    return-void

    .line 240
    :cond_0
    if-eqz p1, :cond_1

    iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofPersonLeaveRegistered:Z

    if-nez v1, :cond_1

    .line 241
    iget-object v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofSensorListener:Landroid/hardware/SensorEventListener;

    iget-object v2, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofPersonLeaveSensor:Landroid/hardware/Sensor;

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofPersonLeaveRegistered:Z

    .line 243
    sget-object v0, Lcom/android/server/tof/ContactlessGestureService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "registerPersonLeaveListener: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofPersonLeaveRegistered:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 244
    :cond_1
    if-nez p1, :cond_2

    iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofPersonLeaveRegistered:Z

    if-eqz v1, :cond_2

    .line 245
    iget-object v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofSensorListener:Landroid/hardware/SensorEventListener;

    iget-object v2, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofPersonLeaveSensor:Landroid/hardware/Sensor;

    invoke-virtual {v0, v1, v2}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V

    .line 246
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofPersonLeaveRegistered:Z

    .line 248
    :cond_2
    :goto_0
    return-void
.end method

.method private registerPersonNearListenerIfNeeded(Z)V
    .locals 4
    .param p1, "register"    # Z

    .line 257
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mSensorManager:Landroid/hardware/SensorManager;

    if-nez v0, :cond_0

    .line 258
    return-void

    .line 261
    :cond_0
    if-eqz p1, :cond_1

    iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofPersonNearRegistered:Z

    if-nez v1, :cond_1

    .line 262
    iget-object v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofSensorListener:Landroid/hardware/SensorEventListener;

    iget-object v2, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofPersonNearSensor:Landroid/hardware/Sensor;

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofPersonNearRegistered:Z

    .line 264
    sget-object v0, Lcom/android/server/tof/ContactlessGestureService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "registerPersonNearListener: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofPersonNearRegistered:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 265
    :cond_1
    if-nez p1, :cond_2

    iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofPersonNearRegistered:Z

    if-eqz v1, :cond_2

    .line 266
    iget-object v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofSensorListener:Landroid/hardware/SensorEventListener;

    iget-object v2, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofPersonNearSensor:Landroid/hardware/Sensor;

    invoke-virtual {v0, v1, v2}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V

    .line 267
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofPersonNearRegistered:Z

    .line 269
    :cond_2
    :goto_0
    return-void
.end method

.method private registerTofGestureListenerIfNeeded(Z)V
    .locals 4
    .param p1, "register"    # Z

    .line 291
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mSensorManager:Landroid/hardware/SensorManager;

    if-nez v0, :cond_0

    .line 292
    sget-object v0, Lcom/android/server/tof/ContactlessGestureService;->TAG:Ljava/lang/String;

    const-string v1, "SensorManager is not ready, reject register tof sensor."

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    return-void

    .line 296
    :cond_0
    if-eqz p1, :cond_1

    iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureRegistered:Z

    if-nez v1, :cond_1

    .line 297
    iget-object v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofSensorListener:Landroid/hardware/SensorEventListener;

    iget-object v2, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureSensor:Landroid/hardware/Sensor;

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureRegistered:Z

    goto :goto_0

    .line 299
    :cond_1
    if-nez p1, :cond_2

    iget-boolean v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureRegistered:Z

    if-eqz v1, :cond_2

    .line 300
    iget-object v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofSensorListener:Landroid/hardware/SensorEventListener;

    iget-object v2, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureSensor:Landroid/hardware/Sensor;

    invoke-virtual {v0, v1, v2}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V

    .line 301
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureRegistered:Z

    .line 303
    :cond_2
    :goto_0
    sget-object v0, Lcom/android/server/tof/ContactlessGestureService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "registerTofGestureListener: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",mTofGestureRegistered:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureRegistered:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    return-void
.end method

.method private registerTofProximityListenerIfNeeded()V
    .locals 3

    .line 223
    iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofProximitySupport:Z

    if-eqz v0, :cond_2

    .line 224
    iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mIsInteractive:Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofScreenOffEnabled:Z

    if-eqz v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v2

    :goto_0
    invoke-direct {p0, v0}, Lcom/android/server/tof/ContactlessGestureService;->registerPersonLeaveListenerIfNeeded(Z)V

    .line 225
    iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mIsInteractive:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofScreenOnEnabled:Z

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    move v1, v2

    :goto_1
    invoke-direct {p0, v1}, Lcom/android/server/tof/ContactlessGestureService;->registerPersonNearListenerIfNeeded(Z)V

    .line 227
    :cond_2
    return-void
.end method

.method private setDebug(Z)I
    .locals 1
    .param p1, "enable"    # Z

    .line 435
    sput-boolean p1, Lcom/android/server/tof/ContactlessGestureService;->mDebug:Z

    .line 436
    const/4 v0, 0x0

    return v0
.end method

.method private systemReady()V
    .locals 3

    .line 152
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mContext:Landroid/content/Context;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mPowerManager:Landroid/os/PowerManager;

    .line 153
    invoke-static {}, Lcom/android/server/power/PowerManagerServiceStub;->get()Lcom/android/server/power/PowerManagerServiceStub;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mPowerManagerServiceImpl:Lcom/android/server/power/PowerManagerServiceStub;

    .line 154
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "sensor"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mSensorManager:Landroid/hardware/SensorManager;

    .line 155
    const v1, 0x1fa2703

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(IZ)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofPersonNearSensor:Landroid/hardware/Sensor;

    .line 156
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mSensorManager:Landroid/hardware/SensorManager;

    const v1, 0x1fa2704

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofPersonLeaveSensor:Landroid/hardware/Sensor;

    .line 157
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mSensorManager:Landroid/hardware/SensorManager;

    const v1, 0x1fa2700

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureSensor:Landroid/hardware/Sensor;

    .line 158
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mContext:Landroid/content/Context;

    .line 159
    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    .line 160
    iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mAonGestureSupport:Z

    if-eqz v0, :cond_0

    .line 161
    new-instance v0, Lcom/android/server/tof/AONGestureController;

    iget-object v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/tof/ContactlessGestureService;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, v1, v2, p0}, Lcom/android/server/tof/AONGestureController;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/tof/ContactlessGestureService;)V

    iput-object v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mContactlessGestureController:Lcom/android/server/tof/ContactlessGestureController;

    goto :goto_0

    .line 163
    :cond_0
    new-instance v0, Lcom/android/server/tof/ToFGestureController;

    iget-object v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/tof/ContactlessGestureService;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, v1, v2, p0}, Lcom/android/server/tof/ToFGestureController;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/tof/ContactlessGestureService;)V

    iput-object v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mContactlessGestureController:Lcom/android/server/tof/ContactlessGestureController;

    .line 165
    :goto_0
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mContactlessGestureController:Lcom/android/server/tof/ContactlessGestureController;

    iget-object v1, p0, Lcom/android/server/tof/ContactlessGestureService;->mComponentFeatureList:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/android/server/tof/ContactlessGestureController;->parseGestureComponentFromCloud(Ljava/util/List;)V

    .line 166
    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->updateConfigState()V

    .line 167
    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->registerContentObserver()V

    .line 168
    return-void
.end method

.method private updateAonGestureConfig()V
    .locals 4

    .line 537
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, -0x2

    const-string v2, "miui_aon_gesture"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v3, 0x1

    :cond_0
    iput-boolean v3, p0, Lcom/android/server/tof/ContactlessGestureService;->mAonGestureEnabled:Z

    .line 539
    return-void
.end method

.method private updateConfigState()V
    .locals 0

    .line 171
    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->updateTofScreenOnConfig()V

    .line 172
    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->updateTofScreenOffConfig()V

    .line 173
    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->updateTofGestureConfig()V

    .line 174
    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->updateAonGestureConfig()V

    .line 175
    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->updateDeskTopModeConfig()V

    .line 176
    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->updateScreenProjectConfig()V

    .line 177
    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->updateSynergyModeConfig()V

    .line 178
    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->updateExternalDisplayConnectConfig()V

    .line 179
    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->handleOneHandedModeEnableChanged()V

    .line 180
    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->registerTofProximityListenerIfNeeded()V

    .line 181
    return-void
.end method

.method private updateDeskTopModeConfig()V
    .locals 4

    .line 516
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, -0x2

    const-string v2, "miui_dkt_mode"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v3, 0x1

    :cond_0
    iput-boolean v3, p0, Lcom/android/server/tof/ContactlessGestureService;->mDeskTopModeEnabled:Z

    .line 518
    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->handleDeskTopModeChanged()V

    .line 519
    return-void
.end method

.method private updateExternalDisplayConnectConfig()V
    .locals 4

    .line 510
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, -0x2

    const-string v2, "external_display_connected"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    move v3, v1

    :cond_0
    iput-boolean v3, p0, Lcom/android/server/tof/ContactlessGestureService;->mExternalDisplayConnected:Z

    .line 513
    return-void
.end method

.method private updateScreenProjectConfig()V
    .locals 4

    .line 500
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, -0x2

    const-string/jumbo v2, "screen_project_in_screening"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v3, 0x1

    :cond_0
    iput-boolean v3, p0, Lcom/android/server/tof/ContactlessGestureService;->mScreenProjectInScreen:Z

    .line 502
    return-void
.end method

.method private updateSynergyModeConfig()V
    .locals 4

    .line 505
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, -0x2

    const-string/jumbo v2, "synergy_mode"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    move v3, v1

    :cond_0
    iput-boolean v3, p0, Lcom/android/server/tof/ContactlessGestureService;->mSynergyModeEnable:Z

    .line 507
    return-void
.end method

.method private updateTofGestureConfig()V
    .locals 4

    .line 532
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, -0x2

    const-string v2, "miui_tof_gesture"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v3, 0x1

    :cond_0
    iput-boolean v3, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureEnabled:Z

    .line 534
    return-void
.end method

.method private updateTofScreenOffConfig()V
    .locals 4

    .line 527
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, -0x2

    const-string v2, "miui_tof_screen_off"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v3, 0x1

    :cond_0
    iput-boolean v3, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofScreenOffEnabled:Z

    .line 529
    return-void
.end method

.method private updateTofScreenOnConfig()V
    .locals 4

    .line 522
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, -0x2

    const-string v2, "miui_tof_screen_on"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v3, 0x1

    :cond_0
    iput-boolean v3, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofScreenOnEnabled:Z

    .line 524
    return-void
.end method

.method private updateTofServiceIfNeeded()V
    .locals 1

    .line 549
    iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureSupport:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mContactlessGestureController:Lcom/android/server/tof/ContactlessGestureController;

    if-eqz v0, :cond_0

    .line 550
    invoke-virtual {v0}, Lcom/android/server/tof/ContactlessGestureController;->isServiceInit()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 551
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mContactlessGestureController:Lcom/android/server/tof/ContactlessGestureController;

    invoke-virtual {v0}, Lcom/android/server/tof/ContactlessGestureController;->unBindService()V

    .line 553
    :cond_0
    return-void
.end method


# virtual methods
.method public isOneHandedModeEnabled()Z
    .locals 1

    .line 431
    iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mOneHandedModeEnable:Z

    return v0
.end method

.method public isScreenCastMode()Z
    .locals 1

    .line 419
    iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mScreenProjectInScreen:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mSynergyModeEnable:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mExternalDisplayConnected:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public isSupportTofGestureFeature()Z
    .locals 1

    .line 415
    iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureSupport:Z

    return v0
.end method

.method public isSupportTofPersonProximityFeature()Z
    .locals 1

    .line 411
    iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofProximitySupport:Z

    return v0
.end method

.method public isTalkBackEnabled()Z
    .locals 2

    .line 423
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 424
    return v1

    .line 426
    :cond_0
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    .line 427
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    nop

    .line 426
    :goto_0
    return v1
.end method

.method public onBootPhase(I)V
    .locals 1
    .param p1, "phase"    # I

    .line 138
    const/16 v0, 0x3e8

    if-ne p1, v0, :cond_0

    .line 139
    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->systemReady()V

    .line 141
    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 3

    .line 131
    new-instance v0, Lcom/android/server/tof/ContactlessGestureService$BinderService;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/server/tof/ContactlessGestureService$BinderService;-><init>(Lcom/android/server/tof/ContactlessGestureService;Lcom/android/server/tof/ContactlessGestureService$BinderService-IA;)V

    const-string/jumbo v2, "tof"

    invoke-virtual {p0, v2, v0}, Lcom/android/server/tof/ContactlessGestureService;->publishBinderService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 132
    sget-object v0, Lcom/android/server/tof/ContactlessGestureService;->TAG:Ljava/lang/String;

    const-string v2, "publish contactless gesture Service"

    invoke-static {v0, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    const-class v0, Lcom/android/server/tof/TofManagerInternal;

    new-instance v2, Lcom/android/server/tof/ContactlessGestureService$LocalService;

    invoke-direct {v2, p0, v1}, Lcom/android/server/tof/ContactlessGestureService$LocalService;-><init>(Lcom/android/server/tof/ContactlessGestureService;Lcom/android/server/tof/ContactlessGestureService$LocalService-IA;)V

    invoke-virtual {p0, v0, v2}, Lcom/android/server/tof/ContactlessGestureService;->publishLocalService(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 134
    return-void
.end method

.method public onUserSwitching(Lcom/android/server/SystemService$TargetUser;Lcom/android/server/SystemService$TargetUser;)V
    .locals 4
    .param p1, "from"    # Lcom/android/server/SystemService$TargetUser;
    .param p2, "to"    # Lcom/android/server/SystemService$TargetUser;

    .line 145
    sget-object v0, Lcom/android/server/tof/ContactlessGestureService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "from user:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/android/server/SystemService$TargetUser;->getUserHandle()Landroid/os/UserHandle;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/UserHandle;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " getUserIdentifier:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/android/server/SystemService$TargetUser;->getUserIdentifier()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", to:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 146
    invoke-virtual {p2}, Lcom/android/server/SystemService$TargetUser;->getUserHandle()Landroid/os/UserHandle;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/UserHandle;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/android/server/SystemService$TargetUser;->getUserIdentifier()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 145
    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->updateConfigState()V

    .line 148
    invoke-direct {p0}, Lcom/android/server/tof/ContactlessGestureService;->updateTofServiceIfNeeded()V

    .line 149
    return-void
.end method

.method public registerContactlessGestureListenerIfNeeded(Z)V
    .locals 3
    .param p1, "register"    # Z

    .line 278
    if-eqz p1, :cond_0

    .line 279
    invoke-virtual {p0}, Lcom/android/server/tof/ContactlessGestureService;->startGestureClientIfNeeded()V

    .line 280
    invoke-virtual {p0}, Lcom/android/server/tof/ContactlessGestureService;->showTofGestureNotificationIfNeeded()V

    .line 282
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureSupport:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureEnabled:Z

    if-eqz v0, :cond_1

    .line 283
    invoke-direct {p0, p1}, Lcom/android/server/tof/ContactlessGestureService;->registerTofGestureListenerIfNeeded(Z)V

    goto :goto_0

    .line 284
    :cond_1
    iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mAonGestureSupport:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mAonGestureEnabled:Z

    if-eqz v0, :cond_2

    .line 285
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mContactlessGestureController:Lcom/android/server/tof/ContactlessGestureController;

    invoke-virtual {v0, p1}, Lcom/android/server/tof/ContactlessGestureController;->registerGestureListenerIfNeeded(Z)V

    .line 286
    sget-object v0, Lcom/android/server/tof/ContactlessGestureService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "registerGestureListener:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    :cond_2
    :goto_0
    return-void
.end method

.method public showTofGestureNotificationIfNeeded()V
    .locals 1

    .line 542
    iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureSupport:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureEnabled:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mAonGestureSupport:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mAonGestureEnabled:Z

    if-nez v0, :cond_2

    .line 544
    :cond_1
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mContactlessGestureController:Lcom/android/server/tof/ContactlessGestureController;

    invoke-virtual {v0}, Lcom/android/server/tof/ContactlessGestureController;->showGestureNotification()V

    .line 546
    :cond_2
    return-void
.end method

.method public startGestureClientIfNeeded()V
    .locals 2

    .line 556
    iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mTofGestureSupport:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mAonGestureSupport:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mContactlessGestureController:Lcom/android/server/tof/ContactlessGestureController;

    .line 557
    invoke-virtual {v0}, Lcom/android/server/tof/ContactlessGestureController;->isServiceInit()Z

    move-result v0

    if-nez v0, :cond_1

    .line 558
    sget-object v0, Lcom/android/server/tof/ContactlessGestureService;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "start bind contactless gesture client."

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 559
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureService;->mContactlessGestureController:Lcom/android/server/tof/ContactlessGestureController;

    invoke-virtual {v0}, Lcom/android/server/tof/ContactlessGestureController;->bindService()V

    .line 561
    :cond_1
    return-void
.end method
