class com.android.server.tof.AONGestureController extends com.android.server.tof.ContactlessGestureController {
	 /* .source "AONGestureController.java" */
	 /* # static fields */
	 private static final Integer ALWAYS_CHECK_GESTURE_TIMEOUT;
	 public static final java.lang.String AON_SERVICE_CLASS;
	 public static final java.lang.String AON_SERVICE_PACKAGE;
	 private static final java.lang.String CLIENT_ACTION;
	 private static final Integer FPS_CHECK_GESTURE;
	 private static final java.lang.String TAG;
	 private static final Integer TYPE_HAND_ROIACTION;
	 /* # instance fields */
	 private android.content.ComponentName mAonComponentName;
	 private com.xiaomi.aon.IMiAON mAonService;
	 private final android.content.ServiceConnection mConnection;
	 private final com.xiaomi.aon.IMiAONListener mGestureListener;
	 private Integer mLastDisplayRotation;
	 private Integer mLastSupportGesture;
	 private Boolean mListeningState;
	 public miui.aon.IContactlessGestureService mService;
	 /* # direct methods */
	 public static void $r8$lambda$Hd1xd4kPDn5EA1PWkJ9wetcLdTM ( com.android.server.tof.AONGestureController p0 ) { //synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0}, Lcom/android/server/tof/AONGestureController;->lambda$notifyAonEventChangeIfNeeded$1()V */
		 return;
	 } // .end method
	 public static void $r8$lambda$hqs1dt98pyAV_bz6XTty91HW74M ( com.android.server.tof.AONGestureController p0, Boolean p1 ) { //synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0, p1}, Lcom/android/server/tof/AONGestureController;->lambda$registerGestureListenerIfNeeded$0(Z)V */
		 return;
	 } // .end method
	 static void -$$Nest$fputmAonService ( com.android.server.tof.AONGestureController p0, com.xiaomi.aon.IMiAON p1 ) { //bridge//synthethic
		 /* .locals 0 */
		 this.mAonService = p1;
		 return;
	 } // .end method
	 static void -$$Nest$fputmListeningState ( com.android.server.tof.AONGestureController p0, Boolean p1 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* iput-boolean p1, p0, Lcom/android/server/tof/AONGestureController;->mListeningState:Z */
		 return;
	 } // .end method
	 static void -$$Nest$mregisterListener ( com.android.server.tof.AONGestureController p0, Integer p1, Integer p2, Integer p3 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/tof/AONGestureController;->registerListener(III)V */
		 return;
	 } // .end method
	 public com.android.server.tof.AONGestureController ( ) {
		 /* .locals 1 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .param p2, "handler" # Landroid/os/Handler; */
		 /* .param p3, "contactlessGestureService" # Lcom/android/server/tof/ContactlessGestureService; */
		 /* .line 44 */
		 /* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/tof/ContactlessGestureController;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/tof/ContactlessGestureService;)V */
		 /* .line 37 */
		 int v0 = -1; // const/4 v0, -0x1
		 /* iput v0, p0, Lcom/android/server/tof/AONGestureController;->mLastSupportGesture:I */
		 /* .line 38 */
		 /* iput v0, p0, Lcom/android/server/tof/AONGestureController;->mLastDisplayRotation:I */
		 /* .line 168 */
		 /* new-instance v0, Lcom/android/server/tof/AONGestureController$1; */
		 /* invoke-direct {v0, p0}, Lcom/android/server/tof/AONGestureController$1;-><init>(Lcom/android/server/tof/AONGestureController;)V */
		 this.mGestureListener = v0;
		 /* .line 228 */
		 /* new-instance v0, Lcom/android/server/tof/AONGestureController$2; */
		 /* invoke-direct {v0, p0}, Lcom/android/server/tof/AONGestureController$2;-><init>(Lcom/android/server/tof/AONGestureController;)V */
		 this.mConnection = v0;
		 /* .line 45 */
		 return;
	 } // .end method
	 private void bindAonService ( ) {
		 /* .locals 5 */
		 /* .line 216 */
		 v0 = this.mAonService;
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 217 */
			 return;
			 /* .line 219 */
		 } // :cond_0
		 v0 = this.mAonComponentName;
		 final String v1 = "com.xiaomi.aon.AONService"; // const-string v1, "com.xiaomi.aon.AONService"
		 /* if-nez v0, :cond_1 */
		 /* .line 220 */
		 /* new-instance v0, Landroid/content/ComponentName; */
		 final String v2 = "com.xiaomi.aon"; // const-string v2, "com.xiaomi.aon"
		 /* invoke-direct {v0, v2, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
		 this.mAonComponentName = v0;
		 /* .line 222 */
	 } // :cond_1
	 /* new-instance v0, Landroid/content/Intent; */
	 /* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
	 v1 = this.mAonComponentName;
	 /* .line 223 */
	 (( android.content.Intent ) v0 ).setComponent ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
	 /* .line 224 */
	 /* .local v0, "serviceIntent":Landroid/content/Intent; */
	 v1 = this.mContext;
	 v2 = this.mConnection;
	 /* const v3, 0x4000001 */
	 v4 = android.os.UserHandle.CURRENT;
	 (( android.content.Context ) v1 ).bindServiceAsUser ( v0, v2, v3, v4 ); // invoke-virtual {v1, v0, v2, v3, v4}, Landroid/content/Context;->bindServiceAsUser(Landroid/content/Intent;Landroid/content/ServiceConnection;ILandroid/os/UserHandle;)Z
	 /* .line 226 */
	 return;
} // .end method
private void lambda$notifyAonEventChangeIfNeeded$1 ( ) { //synthethic
	 /* .locals 5 */
	 /* .line 105 */
	 final String v0 = "AONGestureController"; // const-string v0, "AONGestureController"
	 try { // :try_start_0
		 v1 = 		 (( com.android.server.tof.AONGestureController ) p0 ).getCurrentSupportGesture ( ); // invoke-virtual {p0}, Lcom/android/server/tof/AONGestureController;->getCurrentSupportGesture()I
		 /* .line 107 */
		 /* .local v1, "currentSupportGesture":I */
		 /* iget v2, p0, Lcom/android/server/tof/AONGestureController;->mLastDisplayRotation:I */
		 /* iget v3, p0, Lcom/android/server/tof/AONGestureController;->mDisplayRotation:I */
		 /* if-ne v2, v3, :cond_0 */
		 /* iget v2, p0, Lcom/android/server/tof/AONGestureController;->mLastSupportGesture:I */
		 /* if-ne v2, v1, :cond_0 */
		 /* .line 109 */
		 return;
		 /* .line 111 */
	 } // :cond_0
	 v2 = this.mAonService;
	 if ( v2 != null) { // if-eqz v2, :cond_1
		 /* iget-boolean v2, p0, Lcom/android/server/tof/AONGestureController;->mListeningState:Z */
		 if ( v2 != null) { // if-eqz v2, :cond_1
			 if ( v1 != null) { // if-eqz v1, :cond_1
				 /* .line 112 */
				 /* new-instance v2, Ljava/lang/StringBuilder; */
				 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
				 final String v3 = "notify aon event changed,mDisplayRotation:"; // const-string v3, "notify aon event changed,mDisplayRotation:"
				 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
				 /* iget v3, p0, Lcom/android/server/tof/AONGestureController;->mDisplayRotation:I */
				 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
				 final String v3 = " currentSupportGesture:"; // const-string v3, " currentSupportGesture:"
				 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
				 /* .line 114 */
				 java.lang.Integer .toBinaryString ( v1 );
				 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
				 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
				 /* .line 112 */
				 android.util.Slog .i ( v0,v2 );
				 /* .line 115 */
				 v2 = this.mAonService;
				 /* iget v3, p0, Lcom/android/server/tof/AONGestureController;->mDisplayRotation:I */
				 /* add-int/lit8 v3, v3, 0x10 */
				 int v4 = 1; // const/4 v4, 0x1
				 /* shl-int v3, v4, v3 */
				 /* const/16 v4, 0x102 */
				 /* .line 117 */
				 /* iput v1, p0, Lcom/android/server/tof/AONGestureController;->mLastSupportGesture:I */
				 /* .line 118 */
				 /* iget v2, p0, Lcom/android/server/tof/AONGestureController;->mDisplayRotation:I */
				 /* iput v2, p0, Lcom/android/server/tof/AONGestureController;->mLastDisplayRotation:I */
				 /* :try_end_0 */
				 /* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
				 /* .line 122 */
			 } // .end local v1 # "currentSupportGesture":I
		 } // :cond_1
		 /* .line 120 */
		 /* :catch_0 */
		 /* move-exception v1 */
		 /* .line 121 */
		 /* .local v1, "e":Landroid/os/RemoteException; */
		 /* new-instance v2, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v3 = "notifyRotationChanged: error "; // const-string v3, "notifyRotationChanged: error "
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Slog .e ( v0,v2 );
		 /* .line 123 */
	 } // .end local v1 # "e":Landroid/os/RemoteException;
} // :goto_0
return;
} // .end method
private void lambda$registerGestureListenerIfNeeded$0 ( Boolean p0 ) { //synthethic
/* .locals 3 */
/* .param p1, "register" # Z */
/* .line 76 */
if ( p1 != null) { // if-eqz p1, :cond_0
	 /* .line 77 */
	 int v0 = 6; // const/4 v0, 0x6
	 /* const v1, 0x927c0 */
	 /* const/16 v2, 0x102 */
	 /* invoke-direct {p0, v2, v0, v1}, Lcom/android/server/tof/AONGestureController;->registerListener(III)V */
	 /* .line 79 */
} // :cond_0
/* invoke-direct {p0}, Lcom/android/server/tof/AONGestureController;->unRegisterListener()V */
/* .line 81 */
} // :goto_0
return;
} // .end method
private void notifyAonEventChangeIfNeeded ( ) {
/* .locals 2 */
/* .line 103 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/tof/AONGestureController$$ExternalSyntheticLambda1; */
/* invoke-direct {v1, p0}, Lcom/android/server/tof/AONGestureController$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/tof/AONGestureController;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 124 */
return;
} // .end method
private void registerListener ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 4 */
/* .param p1, "type" # I */
/* .param p2, "fps" # I */
/* .param p3, "timeout" # I */
/* .line 186 */
/* invoke-direct {p0}, Lcom/android/server/tof/AONGestureController;->bindAonService()V */
/* .line 187 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "register listener, aonService is "; // const-string v1, "register listener, aonService is "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mAonService;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = " mListeningState:"; // const-string v1, " mListeningState:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/tof/AONGestureController;->mListeningState:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " mDisplayRotation:"; // const-string v1, " mDisplayRotation:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/tof/AONGestureController;->mDisplayRotation:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "AONGestureController"; // const-string v1, "AONGestureController"
android.util.Slog .i ( v1,v0 );
/* .line 190 */
try { // :try_start_0
v0 = this.mAonService;
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* iget-boolean v2, p0, Lcom/android/server/tof/AONGestureController;->mListeningState:Z */
	 /* if-nez v2, :cond_0 */
	 /* .line 191 */
	 /* int-to-float v2, p2 */
	 v3 = this.mGestureListener;
	 /* .line 192 */
	 int v0 = 1; // const/4 v0, 0x1
	 /* iput-boolean v0, p0, Lcom/android/server/tof/AONGestureController;->mListeningState:Z */
	 /* .line 194 */
} // :cond_0
/* invoke-direct {p0}, Lcom/android/server/tof/AONGestureController;->notifyAonEventChangeIfNeeded()V */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 197 */
/* .line 195 */
/* :catch_0 */
/* move-exception v0 */
/* .line 196 */
/* .local v0, "e":Landroid/os/RemoteException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "registerListener: error "; // const-string v3, "registerListener: error "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v2 );
/* .line 198 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :goto_0
return;
} // .end method
private void unRegisterListener ( ) {
/* .locals 4 */
/* .line 202 */
final String v0 = "AONGestureController"; // const-string v0, "AONGestureController"
try { // :try_start_0
/* iget-boolean v1, p0, Lcom/android/server/tof/AONGestureController;->mListeningState:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = this.mAonService;
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 203 */
	 /* const-string/jumbo v1, "unregister listener!" */
	 android.util.Slog .i ( v0,v1 );
	 /* .line 204 */
	 v1 = this.mAonService;
	 v2 = this.mGestureListener;
	 /* const/16 v3, 0x102 */
	 /* .line 205 */
	 int v1 = 0; // const/4 v1, 0x0
	 /* iput-boolean v1, p0, Lcom/android/server/tof/AONGestureController;->mListeningState:Z */
	 /* .line 206 */
	 int v2 = -1; // const/4 v2, -0x1
	 /* iput v2, p0, Lcom/android/server/tof/AONGestureController;->mLastSupportGesture:I */
	 /* .line 207 */
	 /* iput v2, p0, Lcom/android/server/tof/AONGestureController;->mLastDisplayRotation:I */
	 /* .line 208 */
	 /* iput-boolean v1, p0, Lcom/android/server/tof/AONGestureController;->mIsTrigger:Z */
	 /* :try_end_0 */
	 /* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 212 */
} // :cond_0
/* .line 210 */
/* :catch_0 */
/* move-exception v1 */
/* .line 211 */
/* .local v1, "e":Landroid/os/RemoteException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "unRegisterListener: error " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v2 );
/* .line 213 */
} // .end local v1 # "e":Landroid/os/RemoteException;
} // :goto_0
return;
} // .end method
/* # virtual methods */
public java.lang.String getAction ( ) {
/* .locals 1 */
/* .line 58 */
final String v0 = "miui.intent.action.GESTURE_SERVICE"; // const-string v0, "miui.intent.action.GESTURE_SERVICE"
} // .end method
public android.content.ComponentName getTofClientComponent ( ) {
/* .locals 2 */
/* .line 49 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x110f00a1 */
(( android.content.res.Resources ) v0 ).getString ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* .line 50 */
/* .local v0, "value":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 51 */
android.content.ComponentName .unflattenFromString ( v0 );
/* .line 53 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Boolean isServiceInit ( ) {
/* .locals 1 */
/* .line 150 */
v0 = this.mService;
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public void notifyRotationChanged ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "rotation" # I */
/* .line 97 */
return;
} // .end method
public void onGestureServiceConnected ( android.content.ComponentName p0, android.os.IBinder p1 ) {
/* .locals 2 */
/* .param p1, "name" # Landroid/content/ComponentName; */
/* .param p2, "service" # Landroid/os/IBinder; */
/* .line 161 */
final String v0 = "AONGestureController"; // const-string v0, "AONGestureController"
final String v1 = "onServiceConnected"; // const-string v1, "onServiceConnected"
android.util.Slog .i ( v0,v1 );
/* .line 162 */
v0 = this.mService;
/* if-nez v0, :cond_0 */
/* .line 163 */
miui.aon.IContactlessGestureService$Stub .asInterface ( p2 );
this.mService = v0;
/* .line 165 */
} // :cond_0
v0 = this.mServiceBindingLatch;
(( java.util.concurrent.CountDownLatch ) v0 ).countDown ( ); // invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V
/* .line 166 */
return;
} // .end method
public void registerGestureListenerIfNeeded ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "register" # Z */
/* .line 75 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/tof/AONGestureController$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/tof/AONGestureController$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/tof/AONGestureController;Z)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 82 */
return;
} // .end method
public void restContactlessGestureService ( ) {
/* .locals 2 */
/* .line 155 */
int v0 = 0; // const/4 v0, 0x0
this.mService = v0;
/* .line 156 */
/* new-instance v0, Ljava/util/concurrent/CountDownLatch; */
int v1 = 1; // const/4 v1, 0x1
/* invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V */
this.mServiceBindingLatch = v0;
/* .line 157 */
return;
} // .end method
public void showGestureNotification ( ) {
/* .locals 3 */
/* .line 63 */
final String v0 = "AONGestureController"; // const-string v0, "AONGestureController"
v1 = this.mService;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 65 */
try { // :try_start_0
/* .line 66 */
/* const-string/jumbo v1, "show aon gesture notification, if needed" */
android.util.Slog .i ( v0,v1 );
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 69 */
/* .line 67 */
/* :catch_0 */
/* move-exception v1 */
/* .line 68 */
/* .local v1, "e":Landroid/os/RemoteException; */
(( android.os.RemoteException ) v1 ).toString ( ); // invoke-virtual {v1}, Landroid/os/RemoteException;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v2 );
/* .line 71 */
} // .end local v1 # "e":Landroid/os/RemoteException;
} // :cond_0
} // :goto_0
return;
} // .end method
public Boolean updateGestureHint ( Integer p0 ) {
/* .locals 8 */
/* .param p1, "label" # I */
/* .line 133 */
final String v0 = "AONGestureController"; // const-string v0, "AONGestureController"
int v1 = 0; // const/4 v1, 0x0
/* .line 134 */
/* .local v1, "success":Z */
v2 = this.mService;
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 136 */
try { // :try_start_0
v3 = (( com.android.server.tof.AONGestureController ) p0 ).getFeatureFromLabel ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/tof/AONGestureController;->getFeatureFromLabel(I)I
/* .line 137 */
v4 = (( com.android.server.tof.AONGestureController ) p0 ).getCurrentSupportFeature ( ); // invoke-virtual {p0}, Lcom/android/server/tof/AONGestureController;->getCurrentSupportFeature()I
v5 = (( com.android.server.tof.AONGestureController ) p0 ).getCurrentAppType ( ); // invoke-virtual {p0}, Lcom/android/server/tof/AONGestureController;->getCurrentAppType()I
(( com.android.server.tof.AONGestureController ) p0 ).getCurrentPkg ( ); // invoke-virtual {p0}, Lcom/android/server/tof/AONGestureController;->getCurrentPkg()Ljava/lang/String;
/* iget v7, p0, Lcom/android/server/tof/AONGestureController;->mDisplayRotation:I */
/* .line 136 */
v2 = /* invoke-interface/range {v2 ..v7}, Lmiui/aon/IContactlessGestureService;->showGestureHint(IIILjava/lang/String;I)Z */
/* move v1, v2 */
/* .line 138 */
/* sget-boolean v2, Lcom/android/server/tof/ContactlessGestureService;->mDebug:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 139 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "AON update view label:"; // const-string v3, "AON update view label:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.tof.AONGestureController ) p0 ).gestureLabelToString ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/tof/AONGestureController;->gestureLabelToString(I)Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v2 );
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 143 */
} // :cond_0
/* .line 141 */
/* :catch_0 */
/* move-exception v2 */
/* .line 142 */
/* .local v2, "e":Landroid/os/RemoteException; */
(( android.os.RemoteException ) v2 ).toString ( ); // invoke-virtual {v2}, Landroid/os/RemoteException;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v3 );
/* .line 145 */
} // .end local v2 # "e":Landroid/os/RemoteException;
} // :cond_1
} // :goto_0
} // .end method
