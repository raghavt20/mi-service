.class public Lcom/android/server/tof/TofGestureAppInfo;
.super Ljava/lang/Object;
.source "TofGestureAppInfo.java"


# instance fields
.field private mCategory:I

.field private mComponentName:Ljava/lang/String;

.field private mEnable:Z

.field private mFeature:I

.field private mShowInSettings:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IIZZ)V
    .locals 0
    .param p1, "mComponentName"    # Ljava/lang/String;
    .param p2, "mCategory"    # I
    .param p3, "mFeature"    # I
    .param p4, "mShowInSettings"    # Z
    .param p5, "mEnable"    # Z

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/android/server/tof/TofGestureAppInfo;->mComponentName:Ljava/lang/String;

    .line 26
    iput p2, p0, Lcom/android/server/tof/TofGestureAppInfo;->mCategory:I

    .line 27
    iput p3, p0, Lcom/android/server/tof/TofGestureAppInfo;->mFeature:I

    .line 28
    iput-boolean p4, p0, Lcom/android/server/tof/TofGestureAppInfo;->mShowInSettings:Z

    .line 29
    iput-boolean p5, p0, Lcom/android/server/tof/TofGestureAppInfo;->mEnable:Z

    .line 30
    return-void
.end method


# virtual methods
.method public getCategory()I
    .locals 1

    .line 41
    iget v0, p0, Lcom/android/server/tof/TofGestureAppInfo;->mCategory:I

    return v0
.end method

.method public getComponentName()Ljava/lang/String;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/android/server/tof/TofGestureAppInfo;->mComponentName:Ljava/lang/String;

    return-object v0
.end method

.method public getFeature()I
    .locals 1

    .line 49
    iget v0, p0, Lcom/android/server/tof/TofGestureAppInfo;->mFeature:I

    return v0
.end method

.method public isEnable()Z
    .locals 1

    .line 65
    iget-boolean v0, p0, Lcom/android/server/tof/TofGestureAppInfo;->mEnable:Z

    return v0
.end method

.method public isShowInSettings()Z
    .locals 1

    .line 57
    iget-boolean v0, p0, Lcom/android/server/tof/TofGestureAppInfo;->mShowInSettings:Z

    return v0
.end method

.method public setCategory(I)V
    .locals 0
    .param p1, "mCategory"    # I

    .line 45
    iput p1, p0, Lcom/android/server/tof/TofGestureAppInfo;->mCategory:I

    .line 46
    return-void
.end method

.method public setComponentName(Ljava/lang/String;)V
    .locals 0
    .param p1, "mComponentName"    # Ljava/lang/String;

    .line 37
    iput-object p1, p0, Lcom/android/server/tof/TofGestureAppInfo;->mComponentName:Ljava/lang/String;

    .line 38
    return-void
.end method

.method public setEnable(Z)V
    .locals 0
    .param p1, "mEnable"    # Z

    .line 69
    iput-boolean p1, p0, Lcom/android/server/tof/TofGestureAppInfo;->mEnable:Z

    .line 70
    return-void
.end method

.method public setFeature(I)V
    .locals 0
    .param p1, "mFeature"    # I

    .line 53
    iput p1, p0, Lcom/android/server/tof/TofGestureAppInfo;->mFeature:I

    .line 54
    return-void
.end method

.method public setShowInSettings(Z)V
    .locals 0
    .param p1, "mShowInSettings"    # Z

    .line 61
    iput-boolean p1, p0, Lcom/android/server/tof/TofGestureAppInfo;->mShowInSettings:Z

    .line 62
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 74
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TofGestureAppInfo{mComponentName=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/tof/TofGestureAppInfo;->mComponentName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mAllPageSupport="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/tof/TofGestureAppInfo;->mCategory:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mFeature="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/tof/TofGestureAppInfo;->mFeature:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mShowInSettings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/tof/TofGestureAppInfo;->mShowInSettings:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mEnable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/tof/TofGestureAppInfo;->mEnable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
