.class Lcom/android/server/tof/AONGestureController$2;
.super Ljava/lang/Object;
.source "AONGestureController.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/tof/AONGestureController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/tof/AONGestureController;


# direct methods
.method public static synthetic $r8$lambda$Y24CoqKwY8xgMfkrj-C_HmZrmSI(Lcom/android/server/tof/AONGestureController$2;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/tof/AONGestureController$2;->lambda$cleanupService$1()V

    return-void
.end method

.method public static synthetic $r8$lambda$yHBlKupqIPrPXosvfhlF8Ffj-Gg(Lcom/android/server/tof/AONGestureController$2;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/tof/AONGestureController$2;->lambda$onServiceConnected$0()V

    return-void
.end method

.method constructor <init>(Lcom/android/server/tof/AONGestureController;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/tof/AONGestureController;

    .line 228
    iput-object p1, p0, Lcom/android/server/tof/AONGestureController$2;->this$0:Lcom/android/server/tof/AONGestureController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private synthetic lambda$cleanupService$1()V
    .locals 2

    .line 255
    iget-object v0, p0, Lcom/android/server/tof/AONGestureController$2;->this$0:Lcom/android/server/tof/AONGestureController;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/server/tof/AONGestureController;->-$$Nest$fputmAonService(Lcom/android/server/tof/AONGestureController;Lcom/xiaomi/aon/IMiAON;)V

    .line 256
    iget-object v0, p0, Lcom/android/server/tof/AONGestureController$2;->this$0:Lcom/android/server/tof/AONGestureController;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/server/tof/AONGestureController;->-$$Nest$fputmListeningState(Lcom/android/server/tof/AONGestureController;Z)V

    .line 257
    return-void
.end method

.method private synthetic lambda$onServiceConnected$0()V
    .locals 4

    .line 233
    iget-object v0, p0, Lcom/android/server/tof/AONGestureController$2;->this$0:Lcom/android/server/tof/AONGestureController;

    const/4 v1, 0x6

    const v2, 0x927c0

    const/16 v3, 0x102

    invoke-static {v0, v3, v1, v2}, Lcom/android/server/tof/AONGestureController;->-$$Nest$mregisterListener(Lcom/android/server/tof/AONGestureController;III)V

    return-void
.end method


# virtual methods
.method public cleanupService()V
    .locals 2

    .line 254
    iget-object v0, p0, Lcom/android/server/tof/AONGestureController$2;->this$0:Lcom/android/server/tof/AONGestureController;

    iget-object v0, v0, Lcom/android/server/tof/AONGestureController;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/tof/AONGestureController$2$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/android/server/tof/AONGestureController$2$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/tof/AONGestureController$2;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 258
    return-void
.end method

.method public onBindingDied(Landroid/content/ComponentName;)V
    .locals 0
    .param p1, "name"    # Landroid/content/ComponentName;

    .line 245
    invoke-virtual {p0}, Lcom/android/server/tof/AONGestureController$2;->cleanupService()V

    .line 246
    return-void
.end method

.method public onNullBinding(Landroid/content/ComponentName;)V
    .locals 0
    .param p1, "name"    # Landroid/content/ComponentName;

    .line 250
    invoke-virtual {p0}, Lcom/android/server/tof/AONGestureController$2;->cleanupService()V

    .line 251
    return-void
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .line 231
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Aon service connected success, service:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AONGestureController"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    iget-object v0, p0, Lcom/android/server/tof/AONGestureController$2;->this$0:Lcom/android/server/tof/AONGestureController;

    invoke-static {p2}, Landroid/os/Binder;->allowBlocking(Landroid/os/IBinder;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/aon/IMiAON$Stub;->asInterface(Landroid/os/IBinder;)Lcom/xiaomi/aon/IMiAON;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/tof/AONGestureController;->-$$Nest$fputmAonService(Lcom/android/server/tof/AONGestureController;Lcom/xiaomi/aon/IMiAON;)V

    .line 233
    iget-object v0, p0, Lcom/android/server/tof/AONGestureController$2;->this$0:Lcom/android/server/tof/AONGestureController;

    iget-object v0, v0, Lcom/android/server/tof/AONGestureController;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/tof/AONGestureController$2$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0}, Lcom/android/server/tof/AONGestureController$2$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/tof/AONGestureController$2;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 235
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;

    .line 239
    const-string v0, "AONGestureController"

    const-string v1, "Aon service connected failure"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    invoke-virtual {p0}, Lcom/android/server/tof/AONGestureController$2;->cleanupService()V

    .line 241
    return-void
.end method
