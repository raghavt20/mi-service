.class public abstract Lcom/android/server/tof/TofManagerInternal;
.super Ljava/lang/Object;
.source "TofManagerInternal.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract onDefaultDisplayFocusChanged(Ljava/lang/String;)V
.end method

.method public abstract onEarlyInteractivityChange(Z)V
.end method

.method public abstract updateGestureAppConfig(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method
