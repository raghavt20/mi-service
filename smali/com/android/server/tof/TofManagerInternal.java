public abstract class com.android.server.tof.TofManagerInternal {
	 /* .source "TofManagerInternal.java" */
	 /* # direct methods */
	 public com.android.server.tof.TofManagerInternal ( ) {
		 /* .locals 0 */
		 /* .line 8 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public abstract void onDefaultDisplayFocusChanged ( java.lang.String p0 ) {
	 } // .end method
	 public abstract void onEarlyInteractivityChange ( Boolean p0 ) {
	 } // .end method
	 public abstract void updateGestureAppConfig ( java.util.List p0 ) {
		 /* .annotation system Ldalvik/annotation/Signature; */
		 /* value = { */
		 /* "(", */
		 /* "Ljava/util/List<", */
		 /* "Ljava/lang/String;", */
		 /* ">;)V" */
		 /* } */
	 } // .end annotation
} // .end method
