class com.android.server.tof.ContactlessGestureService$1 implements android.hardware.SensorEventListener {
	 /* .source "ContactlessGestureService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/tof/ContactlessGestureService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.tof.ContactlessGestureService this$0; //synthetic
/* # direct methods */
 com.android.server.tof.ContactlessGestureService$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/tof/ContactlessGestureService; */
/* .line 307 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onAccuracyChanged ( android.hardware.Sensor p0, Integer p1 ) {
/* .locals 0 */
/* .param p1, "sensor" # Landroid/hardware/Sensor; */
/* .param p2, "i" # I */
/* .line 333 */
return;
} // .end method
public void onSensorChanged ( android.hardware.SensorEvent p0 ) {
/* .locals 6 */
/* .param p1, "sensorEvent" # Landroid/hardware/SensorEvent; */
/* .line 310 */
v0 = this.values;
int v1 = 0; // const/4 v1, 0x0
/* aget v0, v0, v1 */
/* .line 311 */
/* .local v0, "value":F */
v2 = this.sensor;
v2 = (( android.hardware.Sensor ) v2 ).getType ( ); // invoke-virtual {v2}, Landroid/hardware/Sensor;->getType()I
/* .line 312 */
/* .local v2, "sensorType":I */
com.android.server.tof.ContactlessGestureService .-$$Nest$sfgetTAG ( );
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "onSensorChanged, sensor:"; // const-string v5, "onSensorChanged, sensor:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.sensor;
(( android.hardware.Sensor ) v5 ).getName ( ); // invoke-virtual {v5}, Landroid/hardware/Sensor;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = ", value:"; // const-string v5, ", value:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v4 );
/* .line 314 */
int v3 = 1; // const/4 v3, 0x1
/* const/high16 v4, 0x3f800000 # 1.0f */
/* packed-switch v2, :pswitch_data_0 */
/* .line 325 */
/* :pswitch_0 */
com.android.server.tof.ContactlessGestureService .-$$Nest$sfgetTAG ( );
/* const-string/jumbo v3, "unknown sensor" */
android.util.Slog .e ( v1,v3 );
/* .line 319 */
/* :pswitch_1 */
v5 = this.this$0;
/* cmpl-float v4, v0, v4 */
if ( v4 != null) { // if-eqz v4, :cond_0
	 /* move v1, v3 */
} // :cond_0
com.android.server.tof.ContactlessGestureService .-$$Nest$mhandleTofProximitySensorChanged ( v5,v1 );
/* .line 320 */
/* .line 316 */
/* :pswitch_2 */
v5 = this.this$0;
/* cmpl-float v4, v0, v4 */
/* if-nez v4, :cond_1 */
/* move v1, v3 */
} // :cond_1
com.android.server.tof.ContactlessGestureService .-$$Nest$mhandleTofProximitySensorChanged ( v5,v1 );
/* .line 317 */
/* .line 322 */
/* :pswitch_3 */
v1 = this.this$0;
/* float-to-int v3, v0 */
com.android.server.tof.ContactlessGestureService .-$$Nest$mhandleGestureSensorChanged ( v1,v3 );
/* .line 323 */
/* nop */
/* .line 328 */
} // :goto_0
return;
/* :pswitch_data_0 */
/* .packed-switch 0x1fa2700 */
/* :pswitch_3 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_2 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
