public class com.android.server.tof.InputHelper {
	 /* .source "InputHelper.java" */
	 /* # static fields */
	 private static final Integer SWIPE_DURATION;
	 private static final java.lang.String TAG;
	 private static final java.lang.String THREAD_NAME;
	 /* # instance fields */
	 private android.graphics.Point mDownPoint;
	 private android.os.Handler mHandler;
	 private android.graphics.Point mLeftPoint;
	 private com.android.server.input.MiuiInputManagerInternal mMiuiInputManagerInternal;
	 private android.graphics.Point mRightPoint;
	 private android.graphics.Point mUpPoint;
	 private com.android.server.wm.WindowManagerService mWindowManagerService;
	 /* # direct methods */
	 public static void $r8$lambda$TWdQiUm3MS3PB9-Bp97CbGQXce4 ( com.android.server.tof.InputHelper p0, android.graphics.Point p1, android.graphics.Point p2 ) { //synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0, p1, p2}, Lcom/android/server/tof/InputHelper;->lambda$simulatedSwipe$1(Landroid/graphics/Point;Landroid/graphics/Point;)V */
		 return;
	 } // .end method
	 public static void $r8$lambda$uSr0UK20JnfPF-W4avpjkeonkG8 ( com.android.server.tof.InputHelper p0, Integer p1 ) { //synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0, p1}, Lcom/android/server/tof/InputHelper;->lambda$injectKeyEvent$0(I)V */
		 return;
	 } // .end method
	 static void -$$Nest$mupdateSwipePoint ( com.android.server.tof.InputHelper p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0}, Lcom/android/server/tof/InputHelper;->updateSwipePoint()V */
		 return;
	 } // .end method
	 public com.android.server.tof.InputHelper ( ) {
		 /* .locals 3 */
		 /* .line 39 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 34 */
		 /* new-instance v0, Landroid/graphics/Point; */
		 /* invoke-direct {v0}, Landroid/graphics/Point;-><init>()V */
		 this.mUpPoint = v0;
		 /* .line 35 */
		 /* new-instance v0, Landroid/graphics/Point; */
		 /* invoke-direct {v0}, Landroid/graphics/Point;-><init>()V */
		 this.mDownPoint = v0;
		 /* .line 36 */
		 /* new-instance v0, Landroid/graphics/Point; */
		 /* invoke-direct {v0}, Landroid/graphics/Point;-><init>()V */
		 this.mLeftPoint = v0;
		 /* .line 37 */
		 /* new-instance v0, Landroid/graphics/Point; */
		 /* invoke-direct {v0}, Landroid/graphics/Point;-><init>()V */
		 this.mRightPoint = v0;
		 /* .line 40 */
		 /* new-instance v0, Landroid/os/HandlerThread; */
		 final String v1 = "input.tof"; // const-string v1, "input.tof"
		 /* invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
		 /* .line 41 */
		 /* .local v0, "thread":Landroid/os/HandlerThread; */
		 (( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
		 /* .line 42 */
		 /* new-instance v1, Landroid/os/Handler; */
		 (( android.os.HandlerThread ) v0 ).getLooper ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
		 /* invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
		 this.mHandler = v1;
		 /* .line 43 */
		 /* const-class v1, Lcom/android/server/input/MiuiInputManagerInternal; */
		 com.android.server.LocalServices .getService ( v1 );
		 /* check-cast v1, Lcom/android/server/input/MiuiInputManagerInternal; */
		 this.mMiuiInputManagerInternal = v1;
		 /* .line 44 */
		 /* const-string/jumbo v1, "window" */
		 android.os.ServiceManager .getService ( v1 );
		 /* check-cast v1, Lcom/android/server/wm/WindowManagerService; */
		 this.mWindowManagerService = v1;
		 /* .line 45 */
		 /* invoke-direct {p0}, Lcom/android/server/tof/InputHelper;->updateSwipePoint()V */
		 /* .line 46 */
		 /* invoke-direct {p0}, Lcom/android/server/tof/InputHelper;->registerDisplayChangeListener()V */
		 /* .line 47 */
		 return;
	 } // .end method
	 private void injectKeyEvent ( Integer p0 ) {
		 /* .locals 2 */
		 /* .param p1, "keyCode" # I */
		 /* .line 190 */
		 v0 = this.mHandler;
		 /* new-instance v1, Lcom/android/server/tof/InputHelper$$ExternalSyntheticLambda1; */
		 /* invoke-direct {v1, p0, p1}, Lcom/android/server/tof/InputHelper$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/tof/InputHelper;I)V */
		 (( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
		 /* .line 205 */
		 return;
	 } // .end method
	 private Boolean injectKeyEvent ( android.view.KeyEvent p0 ) {
		 /* .locals 2 */
		 /* .param p1, "event" # Landroid/view/KeyEvent; */
		 /* .line 208 */
		 android.hardware.input.InputManager .getInstance ( );
		 int v1 = 1; // const/4 v1, 0x1
		 v0 = 		 (( android.hardware.input.InputManager ) v0 ).injectInputEvent ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Landroid/hardware/input/InputManager;->injectInputEvent(Landroid/view/InputEvent;I)Z
	 } // .end method
	 private void lambda$injectKeyEvent$0 ( Integer p0 ) { //synthethic
		 /* .locals 19 */
		 /* .param p1, "keyCode" # I */
		 /* .line 191 */
		 /* move-object/from16 v1, p0 */
		 final String v2 = "TofInputHelper"; // const-string v2, "TofInputHelper"
		 android.os.SystemClock .uptimeMillis ( );
		 /* move-result-wide v17 */
		 /* .line 192 */
		 /* .local v17, "now":J */
		 int v7 = 0; // const/4 v7, 0x0
		 int v9 = 0; // const/4 v9, 0x0
		 int v10 = 0; // const/4 v10, 0x0
		 int v11 = -1; // const/4 v11, -0x1
		 int v12 = 0; // const/4 v12, 0x0
		 int v13 = 0; // const/4 v13, 0x0
		 int v14 = 0; // const/4 v14, 0x0
		 int v15 = 0; // const/4 v15, 0x0
		 /* const/16 v16, 0x0 */
		 /* move-wide/from16 v3, v17 */
		 /* move-wide/from16 v5, v17 */
		 /* move/from16 v8, p1 */
		 /* invoke-static/range {v3 ..v16}, Landroid/view/KeyEvent;->obtain(JJIIIIIIIIILjava/lang/String;)Landroid/view/KeyEvent; */
		 /* .line 197 */
		 /* .local v3, "event":Landroid/view/KeyEvent; */
		 try { // :try_start_0
			 v0 = 			 /* invoke-direct {v1, v3}, Lcom/android/server/tof/InputHelper;->injectKeyEvent(Landroid/view/KeyEvent;)Z */
			 /* .line 198 */
			 /* .local v0, "result":Z */
			 int v4 = 1; // const/4 v4, 0x1
			 android.view.KeyEvent .changeAction ( v3,v4 );
			 v4 = 			 /* invoke-direct {v1, v4}, Lcom/android/server/tof/InputHelper;->injectKeyEvent(Landroid/view/KeyEvent;)Z */
			 /* and-int/2addr v0, v4 */
			 /* .line 199 */
			 /* new-instance v4, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
			 final String v5 = "injectKeyEvent result:"; // const-string v5, "injectKeyEvent result:"
			 (( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
			 final String v5 = ","; // const-string v5, ","
			 (( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 /* .line 200 */
			 /* invoke-static/range {p1 ..p1}, Landroid/view/KeyEvent;->keyCodeToString(I)Ljava/lang/String; */
			 (( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 /* .line 199 */
			 android.util.Slog .i ( v2,v4 );
			 /* :try_end_0 */
			 /* .catch Ljava/lang/IllegalArgumentException; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* .line 203 */
			 /* nop */
		 } // .end local v0 # "result":Z
		 /* .line 201 */
		 /* :catch_0 */
		 /* move-exception v0 */
		 /* .line 202 */
		 /* .local v0, "e":Ljava/lang/IllegalArgumentException; */
		 (( java.lang.IllegalArgumentException ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;
		 android.util.Slog .e ( v2,v4 );
		 /* .line 204 */
	 } // .end local v0 # "e":Ljava/lang/IllegalArgumentException;
} // :goto_0
return;
} // .end method
private void lambda$simulatedSwipe$1 ( android.graphics.Point p0, android.graphics.Point p1 ) { //synthethic
/* .locals 6 */
/* .param p1, "start" # Landroid/graphics/Point; */
/* .param p2, "end" # Landroid/graphics/Point; */
/* .line 216 */
v0 = this.mMiuiInputManagerInternal;
/* iget v1, p1, Landroid/graphics/Point;->x:I */
/* iget v2, p1, Landroid/graphics/Point;->y:I */
/* iget v3, p2, Landroid/graphics/Point;->x:I */
/* iget v4, p2, Landroid/graphics/Point;->y:I */
/* const/16 v5, 0x96 */
/* invoke-virtual/range {v0 ..v5}, Lcom/android/server/input/MiuiInputManagerInternal;->swipe(IIIII)Z */
/* .line 218 */
return;
} // .end method
private void registerDisplayChangeListener ( ) {
/* .locals 5 */
/* .line 222 */
android.hardware.display.DisplayManagerGlobal .getInstance ( );
/* .line 223 */
/* .local v0, "mGlobal":Landroid/hardware/display/DisplayManagerGlobal; */
/* new-instance v1, Lcom/android/server/tof/InputHelper$1; */
/* invoke-direct {v1, p0}, Lcom/android/server/tof/InputHelper$1;-><init>(Lcom/android/server/tof/InputHelper;)V */
v2 = this.mHandler;
/* const-wide/16 v3, 0x4 */
(( android.hardware.display.DisplayManagerGlobal ) v0 ).registerDisplayListener ( v1, v2, v3, v4 ); // invoke-virtual {v0, v1, v2, v3, v4}, Landroid/hardware/display/DisplayManagerGlobal;->registerDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;Landroid/os/Handler;J)V
/* .line 239 */
return;
} // .end method
private void updateSwipePoint ( ) {
/* .locals 8 */
/* .line 242 */
/* new-instance v0, Landroid/graphics/Point; */
/* invoke-direct {v0}, Landroid/graphics/Point;-><init>()V */
/* .line 245 */
/* .local v0, "point":Landroid/graphics/Point; */
v1 = this.mWindowManagerService;
int v2 = 0; // const/4 v2, 0x0
(( com.android.server.wm.WindowManagerService ) v1 ).getBaseDisplaySize ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Lcom/android/server/wm/WindowManagerService;->getBaseDisplaySize(ILandroid/graphics/Point;)V
/* .line 246 */
v1 = this.mWindowManagerService;
v1 = (( com.android.server.wm.WindowManagerService ) v1 ).getDefaultDisplayRotation ( ); // invoke-virtual {v1}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayRotation()I
/* .line 248 */
/* .local v1, "rotation":I */
int v2 = 1; // const/4 v2, 0x1
int v3 = 3; // const/4 v3, 0x3
/* if-eq v1, v2, :cond_1 */
/* if-ne v1, v3, :cond_0 */
/* .line 252 */
} // :cond_0
/* iget v2, v0, Landroid/graphics/Point;->x:I */
/* .line 253 */
/* .local v2, "displayWidth":I */
/* iget v4, v0, Landroid/graphics/Point;->y:I */
/* .local v4, "displayHeight":I */
/* .line 249 */
} // .end local v2 # "displayWidth":I
} // .end local v4 # "displayHeight":I
} // :cond_1
} // :goto_0
/* iget v2, v0, Landroid/graphics/Point;->y:I */
/* .line 250 */
/* .restart local v2 # "displayWidth":I */
/* iget v4, v0, Landroid/graphics/Point;->x:I */
/* .line 255 */
/* .restart local v4 # "displayHeight":I */
} // :goto_1
v5 = this.mUpPoint;
/* div-int/lit8 v6, v2, 0x3 */
/* div-int/lit8 v7, v4, 0x3 */
(( android.graphics.Point ) v5 ).set ( v6, v7 ); // invoke-virtual {v5, v6, v7}, Landroid/graphics/Point;->set(II)V
/* .line 256 */
v5 = this.mDownPoint;
/* div-int/lit8 v6, v2, 0x3 */
/* add-int/lit8 v6, v6, 0x32 */
/* mul-int/lit8 v7, v4, 0x2 */
/* div-int/2addr v7, v3 */
(( android.graphics.Point ) v5 ).set ( v6, v7 ); // invoke-virtual {v5, v6, v7}, Landroid/graphics/Point;->set(II)V
/* .line 257 */
v3 = this.mLeftPoint;
/* div-int/lit8 v5, v2, 0x4 */
/* div-int/lit8 v6, v4, 0x5 */
(( android.graphics.Point ) v3 ).set ( v5, v6 ); // invoke-virtual {v3, v5, v6}, Landroid/graphics/Point;->set(II)V
/* .line 258 */
v3 = this.mRightPoint;
/* mul-int/lit8 v5, v2, 0x3 */
/* div-int/lit8 v5, v5, 0x4 */
/* div-int/lit8 v6, v4, 0x5 */
/* add-int/lit8 v6, v6, 0x32 */
(( android.graphics.Point ) v3 ).set ( v5, v6 ); // invoke-virtual {v3, v5, v6}, Landroid/graphics/Point;->set(II)V
/* .line 259 */
return;
} // .end method
/* # virtual methods */
public void injectSupportInputEvent ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "feature" # I */
/* .line 50 */
/* sparse-switch p1, :sswitch_data_0 */
/* .line 106 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "unsupported feature:" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "TofInputHelper"; // const-string v1, "TofInputHelper"
android.util.Slog .e ( v1,v0 );
/* .line 94 */
/* :sswitch_0 */
(( com.android.server.tof.InputHelper ) p0 ).sendSwipeRightEvent ( ); // invoke-virtual {p0}, Lcom/android/server/tof/InputHelper;->sendSwipeRightEvent()V
/* .line 95 */
/* .line 91 */
/* :sswitch_1 */
(( com.android.server.tof.InputHelper ) p0 ).sendSwipeLeftEvent ( ); // invoke-virtual {p0}, Lcom/android/server/tof/InputHelper;->sendSwipeLeftEvent()V
/* .line 92 */
/* .line 103 */
/* :sswitch_2 */
(( com.android.server.tof.InputHelper ) p0 ).sendMediaPlayPauseSpaceEvent ( ); // invoke-virtual {p0}, Lcom/android/server/tof/InputHelper;->sendMediaPlayPauseSpaceEvent()V
/* .line 104 */
/* .line 100 */
/* :sswitch_3 */
(( com.android.server.tof.InputHelper ) p0 ).sendMediaRewind ( ); // invoke-virtual {p0}, Lcom/android/server/tof/InputHelper;->sendMediaRewind()V
/* .line 101 */
/* .line 97 */
/* :sswitch_4 */
(( com.android.server.tof.InputHelper ) p0 ).sendMediaFastForward ( ); // invoke-virtual {p0}, Lcom/android/server/tof/InputHelper;->sendMediaFastForward()V
/* .line 98 */
/* .line 88 */
/* :sswitch_5 */
(( com.android.server.tof.InputHelper ) p0 ).sendSwipeDownEvent ( ); // invoke-virtual {p0}, Lcom/android/server/tof/InputHelper;->sendSwipeDownEvent()V
/* .line 89 */
/* .line 85 */
/* :sswitch_6 */
(( com.android.server.tof.InputHelper ) p0 ).sendSwipeUpEvent ( ); // invoke-virtual {p0}, Lcom/android/server/tof/InputHelper;->sendSwipeUpEvent()V
/* .line 86 */
/* .line 82 */
/* :sswitch_7 */
(( com.android.server.tof.InputHelper ) p0 ).sendVolumeDownEvent ( ); // invoke-virtual {p0}, Lcom/android/server/tof/InputHelper;->sendVolumeDownEvent()V
/* .line 83 */
/* .line 79 */
/* :sswitch_8 */
(( com.android.server.tof.InputHelper ) p0 ).sendVolumeUpEvent ( ); // invoke-virtual {p0}, Lcom/android/server/tof/InputHelper;->sendVolumeUpEvent()V
/* .line 80 */
/* .line 76 */
/* :sswitch_9 */
(( com.android.server.tof.InputHelper ) p0 ).sendEndCallEvent ( ); // invoke-virtual {p0}, Lcom/android/server/tof/InputHelper;->sendEndCallEvent()V
/* .line 77 */
/* .line 73 */
/* :sswitch_a */
(( com.android.server.tof.InputHelper ) p0 ).sendCallEvent ( ); // invoke-virtual {p0}, Lcom/android/server/tof/InputHelper;->sendCallEvent()V
/* .line 74 */
/* .line 70 */
/* :sswitch_b */
(( com.android.server.tof.InputHelper ) p0 ).sendPageUpEvent ( ); // invoke-virtual {p0}, Lcom/android/server/tof/InputHelper;->sendPageUpEvent()V
/* .line 71 */
/* .line 67 */
/* :sswitch_c */
(( com.android.server.tof.InputHelper ) p0 ).sendPageDownEvent ( ); // invoke-virtual {p0}, Lcom/android/server/tof/InputHelper;->sendPageDownEvent()V
/* .line 68 */
/* .line 61 */
/* :sswitch_d */
(( com.android.server.tof.InputHelper ) p0 ).sendMediaPreviousEvent ( ); // invoke-virtual {p0}, Lcom/android/server/tof/InputHelper;->sendMediaPreviousEvent()V
/* .line 62 */
/* .line 64 */
/* :sswitch_e */
(( com.android.server.tof.InputHelper ) p0 ).sendMediaNextEvent ( ); // invoke-virtual {p0}, Lcom/android/server/tof/InputHelper;->sendMediaNextEvent()V
/* .line 65 */
/* .line 58 */
/* :sswitch_f */
(( com.android.server.tof.InputHelper ) p0 ).sendDpadLeftEvent ( ); // invoke-virtual {p0}, Lcom/android/server/tof/InputHelper;->sendDpadLeftEvent()V
/* .line 59 */
/* .line 55 */
/* :sswitch_10 */
(( com.android.server.tof.InputHelper ) p0 ).sendDpadRightEvent ( ); // invoke-virtual {p0}, Lcom/android/server/tof/InputHelper;->sendDpadRightEvent()V
/* .line 56 */
/* .line 52 */
/* :sswitch_11 */
(( com.android.server.tof.InputHelper ) p0 ).sendMediaPlayPauseEvent ( ); // invoke-virtual {p0}, Lcom/android/server/tof/InputHelper;->sendMediaPlayPauseEvent()V
/* .line 53 */
/* nop */
/* .line 108 */
} // :goto_0
return;
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x1 -> :sswitch_11 */
/* 0x2 -> :sswitch_10 */
/* 0x4 -> :sswitch_f */
/* 0x8 -> :sswitch_e */
/* 0x10 -> :sswitch_d */
/* 0x20 -> :sswitch_c */
/* 0x40 -> :sswitch_b */
/* 0x80 -> :sswitch_a */
/* 0x100 -> :sswitch_9 */
/* 0x200 -> :sswitch_8 */
/* 0x400 -> :sswitch_7 */
/* 0x800 -> :sswitch_6 */
/* 0x1000 -> :sswitch_5 */
/* 0x2000 -> :sswitch_4 */
/* 0x4000 -> :sswitch_3 */
/* 0x8000 -> :sswitch_2 */
/* 0x10000 -> :sswitch_1 */
/* 0x20000 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
public void sendCallEvent ( ) {
/* .locals 1 */
/* .line 115 */
int v0 = 5; // const/4 v0, 0x5
/* invoke-direct {p0, v0}, Lcom/android/server/tof/InputHelper;->injectKeyEvent(I)V */
/* .line 116 */
return;
} // .end method
public void sendDpadLeftEvent ( ) {
/* .locals 1 */
/* .line 138 */
/* const/16 v0, 0x15 */
/* invoke-direct {p0, v0}, Lcom/android/server/tof/InputHelper;->injectKeyEvent(I)V */
/* .line 139 */
return;
} // .end method
public void sendDpadRightEvent ( ) {
/* .locals 1 */
/* .line 142 */
/* const/16 v0, 0x16 */
/* invoke-direct {p0, v0}, Lcom/android/server/tof/InputHelper;->injectKeyEvent(I)V */
/* .line 143 */
return;
} // .end method
public void sendEndCallEvent ( ) {
/* .locals 1 */
/* .line 111 */
int v0 = 6; // const/4 v0, 0x6
/* invoke-direct {p0, v0}, Lcom/android/server/tof/InputHelper;->injectKeyEvent(I)V */
/* .line 112 */
return;
} // .end method
public void sendMediaFastForward ( ) {
/* .locals 1 */
/* .line 152 */
/* const/16 v0, 0x5a */
/* invoke-direct {p0, v0}, Lcom/android/server/tof/InputHelper;->injectKeyEvent(I)V */
/* .line 153 */
return;
} // .end method
public void sendMediaNextEvent ( ) {
/* .locals 1 */
/* .line 130 */
/* const/16 v0, 0x57 */
/* invoke-direct {p0, v0}, Lcom/android/server/tof/InputHelper;->injectKeyEvent(I)V */
/* .line 131 */
return;
} // .end method
public void sendMediaPlayPauseEvent ( ) {
/* .locals 1 */
/* .line 156 */
/* const/16 v0, 0x55 */
/* invoke-direct {p0, v0}, Lcom/android/server/tof/InputHelper;->injectKeyEvent(I)V */
/* .line 157 */
return;
} // .end method
public void sendMediaPlayPauseSpaceEvent ( ) {
/* .locals 1 */
/* .line 160 */
/* const/16 v0, 0x3e */
/* invoke-direct {p0, v0}, Lcom/android/server/tof/InputHelper;->injectKeyEvent(I)V */
/* .line 161 */
return;
} // .end method
public void sendMediaPreviousEvent ( ) {
/* .locals 1 */
/* .line 134 */
/* const/16 v0, 0x58 */
/* invoke-direct {p0, v0}, Lcom/android/server/tof/InputHelper;->injectKeyEvent(I)V */
/* .line 135 */
return;
} // .end method
public void sendMediaRewind ( ) {
/* .locals 1 */
/* .line 147 */
/* const/16 v0, 0x59 */
/* invoke-direct {p0, v0}, Lcom/android/server/tof/InputHelper;->injectKeyEvent(I)V */
/* .line 148 */
return;
} // .end method
public void sendPageDownEvent ( ) {
/* .locals 1 */
/* .line 123 */
/* const/16 v0, 0x5d */
/* invoke-direct {p0, v0}, Lcom/android/server/tof/InputHelper;->injectKeyEvent(I)V */
/* .line 124 */
return;
} // .end method
public void sendPageUpEvent ( ) {
/* .locals 1 */
/* .line 119 */
/* const/16 v0, 0x5c */
/* invoke-direct {p0, v0}, Lcom/android/server/tof/InputHelper;->injectKeyEvent(I)V */
/* .line 120 */
return;
} // .end method
public void sendSwipeDownEvent ( ) {
/* .locals 2 */
/* .line 178 */
v0 = this.mUpPoint;
v1 = this.mDownPoint;
(( com.android.server.tof.InputHelper ) p0 ).simulatedSwipe ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/tof/InputHelper;->simulatedSwipe(Landroid/graphics/Point;Landroid/graphics/Point;)V
/* .line 179 */
return;
} // .end method
public void sendSwipeLeftEvent ( ) {
/* .locals 2 */
/* .line 182 */
v0 = this.mRightPoint;
v1 = this.mLeftPoint;
(( com.android.server.tof.InputHelper ) p0 ).simulatedSwipe ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/tof/InputHelper;->simulatedSwipe(Landroid/graphics/Point;Landroid/graphics/Point;)V
/* .line 183 */
return;
} // .end method
public void sendSwipeRightEvent ( ) {
/* .locals 2 */
/* .line 186 */
v0 = this.mLeftPoint;
v1 = this.mRightPoint;
(( com.android.server.tof.InputHelper ) p0 ).simulatedSwipe ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/tof/InputHelper;->simulatedSwipe(Landroid/graphics/Point;Landroid/graphics/Point;)V
/* .line 187 */
return;
} // .end method
public void sendSwipeUpEvent ( ) {
/* .locals 2 */
/* .line 174 */
v0 = this.mDownPoint;
v1 = this.mUpPoint;
(( com.android.server.tof.InputHelper ) p0 ).simulatedSwipe ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/tof/InputHelper;->simulatedSwipe(Landroid/graphics/Point;Landroid/graphics/Point;)V
/* .line 175 */
return;
} // .end method
public void sendVolumeDownEvent ( ) {
/* .locals 1 */
/* .line 170 */
/* const/16 v0, 0x19 */
/* invoke-direct {p0, v0}, Lcom/android/server/tof/InputHelper;->injectKeyEvent(I)V */
/* .line 171 */
return;
} // .end method
public void sendVolumeUpEvent ( ) {
/* .locals 1 */
/* .line 165 */
/* const/16 v0, 0x18 */
/* invoke-direct {p0, v0}, Lcom/android/server/tof/InputHelper;->injectKeyEvent(I)V */
/* .line 166 */
return;
} // .end method
public void simulatedSwipe ( android.graphics.Point p0, android.graphics.Point p1 ) {
/* .locals 2 */
/* .param p1, "start" # Landroid/graphics/Point; */
/* .param p2, "end" # Landroid/graphics/Point; */
/* .line 215 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/tof/InputHelper$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0, p1, p2}, Lcom/android/server/tof/InputHelper$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/tof/InputHelper;Landroid/graphics/Point;Landroid/graphics/Point;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 219 */
return;
} // .end method
