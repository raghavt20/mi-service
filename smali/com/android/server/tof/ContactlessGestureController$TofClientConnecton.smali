.class final Lcom/android/server/tof/ContactlessGestureController$TofClientConnecton;
.super Ljava/lang/Object;
.source "ContactlessGestureController.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/tof/ContactlessGestureController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "TofClientConnecton"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/tof/ContactlessGestureController;


# direct methods
.method private constructor <init>(Lcom/android/server/tof/ContactlessGestureController;)V
    .locals 0

    .line 1134
    iput-object p1, p0, Lcom/android/server/tof/ContactlessGestureController$TofClientConnecton;->this$0:Lcom/android/server/tof/ContactlessGestureController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/tof/ContactlessGestureController;Lcom/android/server/tof/ContactlessGestureController$TofClientConnecton-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/tof/ContactlessGestureController$TofClientConnecton;-><init>(Lcom/android/server/tof/ContactlessGestureController;)V

    return-void
.end method


# virtual methods
.method public binderDied()V
    .locals 2

    .line 1152
    const-string v0, "ContactlessGestureController"

    const-string v1, "binder died"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1153
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController$TofClientConnecton;->this$0:Lcom/android/server/tof/ContactlessGestureController;

    invoke-virtual {v0}, Lcom/android/server/tof/ContactlessGestureController;->restContactlessGestureService()V

    .line 1154
    return-void
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .line 1138
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController$TofClientConnecton;->this$0:Lcom/android/server/tof/ContactlessGestureController;

    invoke-static {v0}, Lcom/android/server/tof/ContactlessGestureController;->-$$Nest$fgetmTofClientConnection(Lcom/android/server/tof/ContactlessGestureController;)Lcom/android/server/tof/ContactlessGestureController$TofClientConnecton;

    move-result-object v0

    if-ne v0, p0, :cond_0

    .line 1139
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController$TofClientConnecton;->this$0:Lcom/android/server/tof/ContactlessGestureController;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/tof/ContactlessGestureController;->onGestureServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V

    .line 1140
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController$TofClientConnecton;->this$0:Lcom/android/server/tof/ContactlessGestureController;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/server/tof/ContactlessGestureController;->-$$Nest$fputmServiceIsBinding(Lcom/android/server/tof/ContactlessGestureController;Z)V

    .line 1142
    :cond_0
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .line 1146
    const-string v0, "ContactlessGestureController"

    const-string v1, "onServiceDisconnected"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1147
    iget-object v0, p0, Lcom/android/server/tof/ContactlessGestureController$TofClientConnecton;->this$0:Lcom/android/server/tof/ContactlessGestureController;

    invoke-virtual {v0}, Lcom/android/server/tof/ContactlessGestureController;->restContactlessGestureService()V

    .line 1148
    return-void
.end method
