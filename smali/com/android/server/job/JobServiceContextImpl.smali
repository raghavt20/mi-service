.class public Lcom/android/server/job/JobServiceContextImpl;
.super Lcom/android/server/job/JobServiceContextStub;
.source "JobServiceContextImpl.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.job.JobServiceContextStub$$"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Lcom/android/server/job/JobServiceContextStub;-><init>()V

    return-void
.end method


# virtual methods
.method public checkIfCancelJob(Lcom/android/server/job/JobServiceContext;Landroid/content/Context;Landroid/content/Intent;Landroid/content/Context$BindServiceFlags;Lcom/android/server/job/controllers/JobStatus;)Z
    .locals 4
    .param p1, "jobContext"    # Lcom/android/server/job/JobServiceContext;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "service"    # Landroid/content/Intent;
    .param p4, "bindFlags"    # Landroid/content/Context$BindServiceFlags;
    .param p5, "job"    # Lcom/android/server/job/controllers/JobStatus;

    .line 20
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v0, :cond_2

    if-eqz p5, :cond_2

    invoke-static {}, Lcom/android/server/am/AutoStartManagerServiceStub;->getInstance()Lcom/android/server/am/AutoStartManagerServiceStub;

    move-result-object v0

    .line 21
    invoke-virtual {p5}, Lcom/android/server/job/controllers/JobStatus;->getUserId()I

    move-result v1

    invoke-virtual {p5}, Lcom/android/server/job/controllers/JobStatus;->getUid()I

    move-result v2

    invoke-interface {v0, p2, p3, v1, v2}, Lcom/android/server/am/AutoStartManagerServiceStub;->isAllowStartService(Landroid/content/Context;Landroid/content/Intent;II)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 24
    :cond_0
    invoke-virtual {p5}, Lcom/android/server/job/controllers/JobStatus;->getJobId()I

    move-result v0

    .line 25
    .local v0, "jobId":I
    invoke-virtual {p5}, Lcom/android/server/job/controllers/JobStatus;->getUid()I

    move-result v1

    .line 26
    .local v1, "uid":I
    const-class v2, Lcom/android/server/job/JobSchedulerInternal;

    invoke-static {v2}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/job/JobSchedulerInternal;

    .line 27
    .local v2, "internal":Lcom/android/server/job/JobSchedulerInternal;
    if-eqz v2, :cond_1

    .line 28
    invoke-virtual {p5}, Lcom/android/server/job/controllers/JobStatus;->getNamespace()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v1, v3, v0}, Lcom/android/server/job/JobSchedulerInternal;->cancelJob(ILjava/lang/String;I)V

    .line 30
    :cond_1
    const/4 v3, 0x1

    return v3

    .line 22
    .end local v0    # "jobId":I
    .end local v1    # "uid":I
    .end local v2    # "internal":Lcom/android/server/job/JobSchedulerInternal;
    :cond_2
    :goto_0
    const/4 v0, 0x0

    return v0
.end method
