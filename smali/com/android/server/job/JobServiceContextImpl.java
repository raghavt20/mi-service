public class com.android.server.job.JobServiceContextImpl extends com.android.server.job.JobServiceContextStub {
	 /* .source "JobServiceContextImpl.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.job.JobServiceContextStub$$" */
} // .end annotation
/* # direct methods */
public com.android.server.job.JobServiceContextImpl ( ) {
	 /* .locals 0 */
	 /* .line 16 */
	 /* invoke-direct {p0}, Lcom/android/server/job/JobServiceContextStub;-><init>()V */
	 return;
} // .end method
/* # virtual methods */
public Boolean checkIfCancelJob ( com.android.server.job.JobServiceContext p0, android.content.Context p1, android.content.Intent p2, android.content.Context$BindServiceFlags p3, com.android.server.job.controllers.JobStatus p4 ) {
	 /* .locals 4 */
	 /* .param p1, "jobContext" # Lcom/android/server/job/JobServiceContext; */
	 /* .param p2, "context" # Landroid/content/Context; */
	 /* .param p3, "service" # Landroid/content/Intent; */
	 /* .param p4, "bindFlags" # Landroid/content/Context$BindServiceFlags; */
	 /* .param p5, "job" # Lcom/android/server/job/controllers/JobStatus; */
	 /* .line 20 */
	 /* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
	 /* if-nez v0, :cond_2 */
	 if ( p5 != null) { // if-eqz p5, :cond_2
		 com.android.server.am.AutoStartManagerServiceStub .getInstance ( );
		 /* .line 21 */
		 v1 = 		 (( com.android.server.job.controllers.JobStatus ) p5 ).getUserId ( ); // invoke-virtual {p5}, Lcom/android/server/job/controllers/JobStatus;->getUserId()I
		 v0 = 		 v2 = 		 (( com.android.server.job.controllers.JobStatus ) p5 ).getUid ( ); // invoke-virtual {p5}, Lcom/android/server/job/controllers/JobStatus;->getUid()I
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 24 */
		 } // :cond_0
		 v0 = 		 (( com.android.server.job.controllers.JobStatus ) p5 ).getJobId ( ); // invoke-virtual {p5}, Lcom/android/server/job/controllers/JobStatus;->getJobId()I
		 /* .line 25 */
		 /* .local v0, "jobId":I */
		 v1 = 		 (( com.android.server.job.controllers.JobStatus ) p5 ).getUid ( ); // invoke-virtual {p5}, Lcom/android/server/job/controllers/JobStatus;->getUid()I
		 /* .line 26 */
		 /* .local v1, "uid":I */
		 /* const-class v2, Lcom/android/server/job/JobSchedulerInternal; */
		 com.android.server.LocalServices .getService ( v2 );
		 /* check-cast v2, Lcom/android/server/job/JobSchedulerInternal; */
		 /* .line 27 */
		 /* .local v2, "internal":Lcom/android/server/job/JobSchedulerInternal; */
		 if ( v2 != null) { // if-eqz v2, :cond_1
			 /* .line 28 */
			 (( com.android.server.job.controllers.JobStatus ) p5 ).getNamespace ( ); // invoke-virtual {p5}, Lcom/android/server/job/controllers/JobStatus;->getNamespace()Ljava/lang/String;
			 /* .line 30 */
		 } // :cond_1
		 int v3 = 1; // const/4 v3, 0x1
		 /* .line 22 */
	 } // .end local v0 # "jobId":I
} // .end local v1 # "uid":I
} // .end local v2 # "internal":Lcom/android/server/job/JobSchedulerInternal;
} // :cond_2
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
