.class public Lcom/android/server/logcat/LogcatManagerServiceImpl;
.super Lcom/android/server/logcat/LogcatManagerServiceStub;
.source "LogcatManagerServiceImpl.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.logcat.LogcatManagerServiceStub$$"
.end annotation


# static fields
.field private static final sAutoApprovedList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mPmi:Lcom/android/server/pm/permission/PermissionManagerServiceInternal;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/logcat/LogcatManagerServiceImpl;->sAutoApprovedList:Ljava/util/List;

    .line 24
    const-string v1, "com.xiaomi.vipaccount"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 25
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Lcom/android/server/logcat/LogcatManagerServiceStub;-><init>()V

    return-void
.end method


# virtual methods
.method public isAutoApproved(IILjava/lang/String;)Z
    .locals 3
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "packageName"    # Ljava/lang/String;

    .line 29
    sget-object v0, Lcom/android/server/logcat/LogcatManagerServiceImpl;->sAutoApprovedList:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 30
    return v1

    .line 32
    :cond_0
    iget-object v0, p0, Lcom/android/server/logcat/LogcatManagerServiceImpl;->mPmi:Lcom/android/server/pm/permission/PermissionManagerServiceInternal;

    if-nez v0, :cond_1

    .line 33
    const-class v0, Lcom/android/server/pm/permission/PermissionManagerServiceInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/pm/permission/PermissionManagerServiceInternal;

    iput-object v0, p0, Lcom/android/server/logcat/LogcatManagerServiceImpl;->mPmi:Lcom/android/server/pm/permission/PermissionManagerServiceInternal;

    .line 35
    :cond_1
    iget-object v0, p0, Lcom/android/server/logcat/LogcatManagerServiceImpl;->mPmi:Lcom/android/server/pm/permission/PermissionManagerServiceInternal;

    const-string v2, "android.permission.READ_LOGS"

    invoke-interface {v0, p1, v2}, Lcom/android/server/pm/permission/PermissionManagerServiceInternal;->checkUidPermission(ILjava/lang/String;)I

    move-result v0

    if-nez v0, :cond_2

    const/4 v1, 0x1

    :cond_2
    return v1
.end method
