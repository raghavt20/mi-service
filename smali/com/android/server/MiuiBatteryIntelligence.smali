.class public Lcom/android/server/MiuiBatteryIntelligence;
.super Ljava/lang/Object;
.source "MiuiBatteryIntelligence.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;,
        Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;
    }
.end annotation


# static fields
.field public static final ACTION_NEED_SCAN_WIFI:Ljava/lang/String; = "miui.intent.action.NEED_SCAN_WIFI"

.field private static final CHARGE_CLOUD_MODULE_NAME:Ljava/lang/String; = "ChargeFwCloudControl"

.field private static final DEBUG:Z

.field public static final FUNCTION_LOW_BATTERY_FAST_CHARGE:I = 0x8

.field public static final FUNCTION_NAVIGATION_CHARGE:I = 0x2

.field public static final FUNCTION_OUT_DOOR_CHARGE:I = 0x4

.field private static volatile INSTANCE:Lcom/android/server/MiuiBatteryIntelligence; = null

.field private static final INTERVAL:J = 0x493e0L

.field private static final MIUI_SECURITYCENTER_APP:Ljava/lang/String; = "com.miui.securitycenter"

.field private static final NAVIGATION_APP_WHILTE_LIST:Ljava/lang/String; = "NavigationWhiteList"

.field private static final PERMISSON_MIUIBATTERY_BROADCAST:Ljava/lang/String; = "com.xiaomi.permission.miuibatteryintelligence"

.field private static final URI_CLOUD_ALL_DATA_NOTIFY:Landroid/net/Uri;


# instance fields
.field private MAP_APP:[Ljava/lang/String;

.field private final TAG:Ljava/lang/String;

.field public mContext:Landroid/content/Context;

.field private mHandler:Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;

.field private mMapApplist:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mMiCharge:Lmiui/util/IMiCharge;

.field private mPlugged:Z

.field private mStateChangedReceiver:Landroid/content/BroadcastReceiver;

.field private mWifiState:I

.field public mfunctions:I


# direct methods
.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryIntelligence;)Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/MiuiBatteryIntelligence;->mHandler:Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmMapApplist(Lcom/android/server/MiuiBatteryIntelligence;)Ljava/util/ArrayList;
    .locals 0

    iget-object p0, p0, Lcom/android/server/MiuiBatteryIntelligence;->mMapApplist:Ljava/util/ArrayList;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryIntelligence;)Lmiui/util/IMiCharge;
    .locals 0

    iget-object p0, p0, Lcom/android/server/MiuiBatteryIntelligence;->mMiCharge:Lmiui/util/IMiCharge;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPlugged(Lcom/android/server/MiuiBatteryIntelligence;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/MiuiBatteryIntelligence;->mPlugged:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmWifiState(Lcom/android/server/MiuiBatteryIntelligence;)I
    .locals 0

    iget p0, p0, Lcom/android/server/MiuiBatteryIntelligence;->mWifiState:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmPlugged(Lcom/android/server/MiuiBatteryIntelligence;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/MiuiBatteryIntelligence;->mPlugged:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmWifiState(Lcom/android/server/MiuiBatteryIntelligence;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/MiuiBatteryIntelligence;->mWifiState:I

    return-void
.end method

.method static bridge synthetic -$$Nest$mreadLocalCloudControlData(Lcom/android/server/MiuiBatteryIntelligence;Landroid/content/ContentResolver;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/MiuiBatteryIntelligence;->readLocalCloudControlData(Landroid/content/ContentResolver;Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetDEBUG()Z
    .locals 1

    sget-boolean v0, Lcom/android/server/MiuiBatteryIntelligence;->DEBUG:Z

    return v0
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 54
    const-string v0, "persist.sys.debug_stats"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/MiuiBatteryIntelligence;->DEBUG:Z

    .line 60
    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/MiuiBatteryIntelligence;->INSTANCE:Lcom/android/server/MiuiBatteryIntelligence;

    .line 76
    nop

    .line 77
    const-string v0, "content://com.android.settings.cloud.CloudSettings/cloud_all_data/notify"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/server/MiuiBatteryIntelligence;->URI_CLOUD_ALL_DATA_NOTIFY:Landroid/net/Uri;

    .line 76
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const-string v0, "MiuiBatteryIntelligence"

    iput-object v0, p0, Lcom/android/server/MiuiBatteryIntelligence;->TAG:Ljava/lang/String;

    .line 73
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/MiuiBatteryIntelligence;->mMapApplist:Ljava/util/ArrayList;

    .line 79
    invoke-static {}, Lmiui/util/IMiCharge;->getInstance()Lmiui/util/IMiCharge;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/MiuiBatteryIntelligence;->mMiCharge:Lmiui/util/IMiCharge;

    .line 81
    const-string v0, "com.tencent.map"

    const-string v1, "com.baidu.BaiduMap"

    const-string v2, "com.autonavi.minimap"

    filled-new-array {v2, v0, v1}, [Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/MiuiBatteryIntelligence;->MAP_APP:[Ljava/lang/String;

    .line 87
    new-instance v0, Lcom/android/server/MiuiBatteryIntelligence$1;

    invoke-direct {v0, p0}, Lcom/android/server/MiuiBatteryIntelligence$1;-><init>(Lcom/android/server/MiuiBatteryIntelligence;)V

    iput-object v0, p0, Lcom/android/server/MiuiBatteryIntelligence;->mStateChangedReceiver:Landroid/content/BroadcastReceiver;

    .line 126
    invoke-direct {p0, p1}, Lcom/android/server/MiuiBatteryIntelligence;->init(Landroid/content/Context;)V

    .line 127
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/android/server/MiuiBatteryIntelligence;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .line 115
    sget-object v0, Lcom/android/server/MiuiBatteryIntelligence;->INSTANCE:Lcom/android/server/MiuiBatteryIntelligence;

    if-nez v0, :cond_1

    .line 116
    const-class v0, Lcom/android/server/MiuiBatteryIntelligence;

    monitor-enter v0

    .line 117
    :try_start_0
    sget-object v1, Lcom/android/server/MiuiBatteryIntelligence;->INSTANCE:Lcom/android/server/MiuiBatteryIntelligence;

    if-nez v1, :cond_0

    .line 118
    new-instance v1, Lcom/android/server/MiuiBatteryIntelligence;

    invoke-direct {v1, p0}, Lcom/android/server/MiuiBatteryIntelligence;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/android/server/MiuiBatteryIntelligence;->INSTANCE:Lcom/android/server/MiuiBatteryIntelligence;

    .line 120
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 122
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/MiuiBatteryIntelligence;->INSTANCE:Lcom/android/server/MiuiBatteryIntelligence;

    return-object v0
.end method

.method private init(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 130
    iput-object p1, p0, Lcom/android/server/MiuiBatteryIntelligence;->mContext:Landroid/content/Context;

    .line 131
    const-string v0, "persist.vendor.smartchg"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/server/MiuiBatteryIntelligence;->mfunctions:I

    .line 132
    new-instance v0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;

    invoke-static {}, Lcom/android/server/MiuiBgThread;->get()Lcom/android/server/MiuiBgThread;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/MiuiBgThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;-><init>(Lcom/android/server/MiuiBatteryIntelligence;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/MiuiBatteryIntelligence;->mHandler:Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;

    .line 133
    iget-object v0, p0, Lcom/android/server/MiuiBatteryIntelligence;->MAP_APP:[Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryIntelligence;->initMapList([Ljava/lang/String;)V

    .line 134
    iget-object v0, p0, Lcom/android/server/MiuiBatteryIntelligence;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "NavigationWhiteList"

    invoke-direct {p0, v0, v1}, Lcom/android/server/MiuiBatteryIntelligence;->registerCloudControlObserver(Landroid/content/ContentResolver;Ljava/lang/String;)V

    .line 135
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence;->registerChangeStateReceiver()V

    .line 136
    return-void
.end method

.method private initMapList([Ljava/lang/String;)V
    .locals 3
    .param p1, "maplist"    # [Ljava/lang/String;

    .line 151
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 152
    iget-object v1, p0, Lcom/android/server/MiuiBatteryIntelligence;->mMapApplist:Ljava/util/ArrayList;

    aget-object v2, p1, v0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 151
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 154
    .end local v0    # "i":I
    :cond_0
    return-void
.end method

.method private readLocalCloudControlData(Landroid/content/ContentResolver;Ljava/lang/String;)V
    .locals 6
    .param p1, "contentResolver"    # Landroid/content/ContentResolver;
    .param p2, "moduleName"    # Ljava/lang/String;

    .line 158
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const-string v1, "MiuiBatteryIntelligence"

    if-eqz v0, :cond_0

    .line 159
    const-string v0, "moduleName can not be null"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    return-void

    .line 163
    :cond_0
    :try_start_0
    const-string v0, "NavigationWhiteList"

    const/4 v2, 0x0

    invoke-static {p1, p2, v0, v2}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 164
    .local v0, "data":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "on cloud read and data = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 166
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 167
    .local v2, "apps":Lorg/json/JSONArray;
    iget-object v3, p0, Lcom/android/server/MiuiBatteryIntelligence;->mMapApplist:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 168
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v3, v4, :cond_1

    .line 169
    iget-object v4, p0, Lcom/android/server/MiuiBatteryIntelligence;->mMapApplist:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 168
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 171
    .end local v3    # "i":I
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cloud data for listNavigation "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/MiuiBatteryIntelligence;->mMapApplist:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 175
    .end local v0    # "data":Ljava/lang/String;
    .end local v2    # "apps":Lorg/json/JSONArray;
    :cond_2
    goto :goto_1

    .line 173
    :catch_0
    move-exception v0

    .line 174
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "Exception when readLocalCloudControlData  :"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 177
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    return-void
.end method

.method private registerChangeStateReceiver()V
    .locals 3

    .line 139
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 140
    .local v0, "i":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 141
    const-string v1, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 142
    invoke-virtual {p0}, Lcom/android/server/MiuiBatteryIntelligence;->isSupportOutDoorCharge()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 143
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 144
    const-string v1, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 145
    const-string v1, "miui.intent.action.NEED_SCAN_WIFI"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 147
    :cond_0
    iget-object v1, p0, Lcom/android/server/MiuiBatteryIntelligence;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/MiuiBatteryIntelligence;->mStateChangedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 148
    return-void
.end method

.method private registerCloudControlObserver(Landroid/content/ContentResolver;Ljava/lang/String;)V
    .locals 3
    .param p1, "contentResolver"    # Landroid/content/ContentResolver;
    .param p2, "moduleName"    # Ljava/lang/String;

    .line 180
    sget-object v0, Lcom/android/server/MiuiBatteryIntelligence;->URI_CLOUD_ALL_DATA_NOTIFY:Landroid/net/Uri;

    new-instance v1, Lcom/android/server/MiuiBatteryIntelligence$2;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2, p1, p2}, Lcom/android/server/MiuiBatteryIntelligence$2;-><init>(Lcom/android/server/MiuiBatteryIntelligence;Landroid/os/Handler;Landroid/content/ContentResolver;Ljava/lang/String;)V

    const/4 v2, 0x1

    invoke-virtual {p1, v0, v2, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 188
    return-void
.end method


# virtual methods
.method public isSupportLowBatteryFastCharge()Z
    .locals 1

    .line 199
    iget v0, p0, Lcom/android/server/MiuiBatteryIntelligence;->mfunctions:I

    and-int/lit8 v0, v0, 0x8

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSupportNavigationCharge()Z
    .locals 1

    .line 191
    iget v0, p0, Lcom/android/server/MiuiBatteryIntelligence;->mfunctions:I

    and-int/lit8 v0, v0, 0x2

    if-lez v0, :cond_0

    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSupportOutDoorCharge()Z
    .locals 1

    .line 195
    iget v0, p0, Lcom/android/server/MiuiBatteryIntelligence;->mfunctions:I

    and-int/lit8 v0, v0, 0x4

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
