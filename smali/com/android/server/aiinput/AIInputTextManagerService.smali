.class public Lcom/android/server/aiinput/AIInputTextManagerService;
.super Lcom/xiaomi/aiinput/IAIInputTextManager$Stub;
.source "AIInputTextManagerService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/aiinput/AIInputTextManagerService$ManagerClient;,
        Lcom/android/server/aiinput/AIInputTextManagerService$Lifecycle;
    }
.end annotation


# static fields
.field private static final BLACK_LIST:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final MSG_PERMISSION_DENIAL:Ljava/lang/String; = "Permission Denial: AI_INPUT_INFORMATION"

.field private static final PERMISSION_AI_INPUT_INFORMATION:Ljava/lang/String; = "com.miui.aiinput.permission.AI_INPUT_INFORMATION"

.field public static final SERVICE_NAME:Ljava/lang/String; = "aiinputtext"

.field public static final TAG:Ljava/lang/String; = "AIInputTextManagerService"

.field private static instance:Lcom/android/server/aiinput/AIInputTextManagerService;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mDeathRecipient:Landroid/os/IBinder$DeathRecipient;

.field private mManagerClient:Lcom/android/server/aiinput/AIInputTextManagerService$ManagerClient;

.field private final mPackageManager:Landroid/content/pm/PackageManager;


# direct methods
.method static bridge synthetic -$$Nest$fputmManagerClient(Lcom/android/server/aiinput/AIInputTextManagerService;Lcom/android/server/aiinput/AIInputTextManagerService$ManagerClient;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/aiinput/AIInputTextManagerService;->mManagerClient:Lcom/android/server/aiinput/AIInputTextManagerService$ManagerClient;

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 43
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/android/server/aiinput/AIInputTextManagerService;->BLACK_LIST:Ljava/util/HashSet;

    .line 46
    const-string v1, "com.miui.voiceassist"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 47
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 81
    invoke-direct {p0}, Lcom/xiaomi/aiinput/IAIInputTextManager$Stub;-><init>()V

    .line 49
    new-instance v0, Lcom/android/server/aiinput/AIInputTextManagerService$1;

    invoke-direct {v0, p0}, Lcom/android/server/aiinput/AIInputTextManagerService$1;-><init>(Lcom/android/server/aiinput/AIInputTextManagerService;)V

    iput-object v0, p0, Lcom/android/server/aiinput/AIInputTextManagerService;->mDeathRecipient:Landroid/os/IBinder$DeathRecipient;

    .line 82
    iput-object p1, p0, Lcom/android/server/aiinput/AIInputTextManagerService;->mContext:Landroid/content/Context;

    .line 83
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/aiinput/AIInputTextManagerService;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 84
    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/android/server/aiinput/AIInputTextManagerService;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    const-class v0, Lcom/android/server/aiinput/AIInputTextManagerService;

    monitor-enter v0

    .line 87
    :try_start_0
    sget-object v1, Lcom/android/server/aiinput/AIInputTextManagerService;->instance:Lcom/android/server/aiinput/AIInputTextManagerService;

    if-nez v1, :cond_0

    .line 88
    new-instance v1, Lcom/android/server/aiinput/AIInputTextManagerService;

    invoke-direct {v1, p0}, Lcom/android/server/aiinput/AIInputTextManagerService;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/android/server/aiinput/AIInputTextManagerService;->instance:Lcom/android/server/aiinput/AIInputTextManagerService;

    .line 90
    :cond_0
    sget-object v1, Lcom/android/server/aiinput/AIInputTextManagerService;->instance:Lcom/android/server/aiinput/AIInputTextManagerService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    .line 86
    .end local p0    # "context":Landroid/content/Context;
    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method private isDenialFromUid(I)Z
    .locals 4
    .param p1, "uid"    # I

    .line 132
    iget-object v0, p0, Lcom/android/server/aiinput/AIInputTextManagerService;->mContext:Landroid/content/Context;

    const/4 v1, 0x1

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/server/aiinput/AIInputTextManagerService;->mPackageManager:Landroid/content/pm/PackageManager;

    if-nez v0, :cond_0

    goto :goto_0

    .line 135
    :cond_0
    invoke-virtual {v0, p1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v0

    .line 136
    .local v0, "packages":[Ljava/lang/String;
    if-eqz v0, :cond_1

    array-length v2, v0

    if-lez v2, :cond_1

    .line 137
    const/4 v2, 0x0

    aget-object v2, v0, v2

    const-string v3, "com.miui.voiceassist"

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    xor-int/2addr v1, v2

    return v1

    .line 139
    :cond_1
    return v1

    .line 133
    .end local v0    # "packages":[Ljava/lang/String;
    :cond_2
    :goto_0
    return v1
.end method


# virtual methods
.method public addClient(Lcom/xiaomi/aiinput/IAIInputTextManagerClient;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "client"    # Lcom/xiaomi/aiinput/IAIInputTextManagerClient;
    .param p2, "ctx"    # Ljava/lang/String;
    .param p3, "pkg"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 96
    if-nez p1, :cond_0

    .line 97
    return-void

    .line 100
    :cond_0
    sget-object v0, Lcom/android/server/aiinput/AIInputTextManagerService;->BLACK_LIST:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 101
    .local v1, "blackCtx":Ljava/lang/String;
    invoke-virtual {p2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {p3, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_1

    .line 104
    .end local v1    # "blackCtx":Ljava/lang/String;
    :cond_1
    goto :goto_0

    .line 102
    .restart local v1    # "blackCtx":Ljava/lang/String;
    :cond_2
    :goto_1
    return-void

    .line 106
    .end local v1    # "blackCtx":Ljava/lang/String;
    :cond_3
    iget-object v0, p0, Lcom/android/server/aiinput/AIInputTextManagerService;->mManagerClient:Lcom/android/server/aiinput/AIInputTextManagerService$ManagerClient;

    const/4 v1, 0x0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/android/server/aiinput/AIInputTextManagerService$ManagerClient;->getClient()Lcom/xiaomi/aiinput/IAIInputTextManagerClient;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 108
    :try_start_0
    iget-object v0, p0, Lcom/android/server/aiinput/AIInputTextManagerService;->mManagerClient:Lcom/android/server/aiinput/AIInputTextManagerService$ManagerClient;

    invoke-virtual {v0}, Lcom/android/server/aiinput/AIInputTextManagerService$ManagerClient;->getClient()Lcom/xiaomi/aiinput/IAIInputTextManagerClient;

    move-result-object v0

    invoke-interface {v0}, Lcom/xiaomi/aiinput/IAIInputTextManagerClient;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/aiinput/AIInputTextManagerService;->mDeathRecipient:Landroid/os/IBinder$DeathRecipient;

    invoke-interface {v0, v2, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0

    .line 110
    goto :goto_2

    .line 109
    :catch_0
    move-exception v0

    .line 112
    :cond_4
    :goto_2
    invoke-interface {p1}, Lcom/xiaomi/aiinput/IAIInputTextManagerClient;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/aiinput/AIInputTextManagerService;->mDeathRecipient:Landroid/os/IBinder$DeathRecipient;

    invoke-interface {v0, v2, v1}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V

    .line 113
    new-instance v0, Lcom/android/server/aiinput/AIInputTextManagerService$ManagerClient;

    invoke-direct {v0, p1, p2}, Lcom/android/server/aiinput/AIInputTextManagerService$ManagerClient;-><init>(Lcom/xiaomi/aiinput/IAIInputTextManagerClient;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/aiinput/AIInputTextManagerService;->mManagerClient:Lcom/android/server/aiinput/AIInputTextManagerService$ManagerClient;

    .line 114
    return-void
.end method

.method public getAIInputTextClient()Lcom/xiaomi/aiinput/IAIInputTextManagerClient;
    .locals 4

    .line 118
    iget-object v0, p0, Lcom/android/server/aiinput/AIInputTextManagerService;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 119
    return-object v1

    .line 121
    :cond_0
    const-string v2, "com.miui.aiinput.permission.AI_INPUT_INFORMATION"

    const-string v3, "Permission Denial: AI_INPUT_INFORMATION"

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/server/aiinput/AIInputTextManagerService;->isDenialFromUid(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 123
    return-object v1

    .line 125
    :cond_1
    iget-object v0, p0, Lcom/android/server/aiinput/AIInputTextManagerService;->mManagerClient:Lcom/android/server/aiinput/AIInputTextManagerService$ManagerClient;

    if-nez v0, :cond_2

    .line 126
    return-object v1

    .line 128
    :cond_2
    invoke-virtual {v0}, Lcom/android/server/aiinput/AIInputTextManagerService$ManagerClient;->getClient()Lcom/xiaomi/aiinput/IAIInputTextManagerClient;

    move-result-object v0

    return-object v0
.end method
