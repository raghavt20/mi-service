public class com.android.server.aiinput.AIInputTextManagerService extends com.xiaomi.aiinput.IAIInputTextManager$Stub {
	 /* .source "AIInputTextManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/aiinput/AIInputTextManagerService$ManagerClient;, */
	 /* Lcom/android/server/aiinput/AIInputTextManagerService$Lifecycle; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.util.HashSet BLACK_LIST;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashSet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.lang.String MSG_PERMISSION_DENIAL;
private static final java.lang.String PERMISSION_AI_INPUT_INFORMATION;
public static final java.lang.String SERVICE_NAME;
public static final java.lang.String TAG;
private static com.android.server.aiinput.AIInputTextManagerService instance;
/* # instance fields */
private final android.content.Context mContext;
private android.os.IBinder$DeathRecipient mDeathRecipient;
private com.android.server.aiinput.AIInputTextManagerService$ManagerClient mManagerClient;
private final android.content.pm.PackageManager mPackageManager;
/* # direct methods */
static void -$$Nest$fputmManagerClient ( com.android.server.aiinput.AIInputTextManagerService p0, com.android.server.aiinput.AIInputTextManagerService$ManagerClient p1 ) { //bridge//synthethic
/* .locals 0 */
this.mManagerClient = p1;
return;
} // .end method
static com.android.server.aiinput.AIInputTextManagerService ( ) {
/* .locals 2 */
/* .line 43 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 46 */
final String v1 = "com.miui.voiceassist"; // const-string v1, "com.miui.voiceassist"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 47 */
return;
} // .end method
private com.android.server.aiinput.AIInputTextManagerService ( ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 81 */
/* invoke-direct {p0}, Lcom/xiaomi/aiinput/IAIInputTextManager$Stub;-><init>()V */
/* .line 49 */
/* new-instance v0, Lcom/android/server/aiinput/AIInputTextManagerService$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/aiinput/AIInputTextManagerService$1;-><init>(Lcom/android/server/aiinput/AIInputTextManagerService;)V */
this.mDeathRecipient = v0;
/* .line 82 */
this.mContext = p1;
/* .line 83 */
(( android.content.Context ) p1 ).getPackageManager ( ); // invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
this.mPackageManager = v0;
/* .line 84 */
return;
} // .end method
public static synchronized com.android.server.aiinput.AIInputTextManagerService getInstance ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p0, "context" # Landroid/content/Context; */
/* const-class v0, Lcom/android/server/aiinput/AIInputTextManagerService; */
/* monitor-enter v0 */
/* .line 87 */
try { // :try_start_0
v1 = com.android.server.aiinput.AIInputTextManagerService.instance;
/* if-nez v1, :cond_0 */
/* .line 88 */
/* new-instance v1, Lcom/android/server/aiinput/AIInputTextManagerService; */
/* invoke-direct {v1, p0}, Lcom/android/server/aiinput/AIInputTextManagerService;-><init>(Landroid/content/Context;)V */
/* .line 90 */
} // :cond_0
v1 = com.android.server.aiinput.AIInputTextManagerService.instance;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* monitor-exit v0 */
/* .line 86 */
} // .end local p0 # "context":Landroid/content/Context;
/* :catchall_0 */
/* move-exception p0 */
/* monitor-exit v0 */
/* throw p0 */
} // .end method
private Boolean isDenialFromUid ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .line 132 */
v0 = this.mContext;
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = this.mPackageManager;
/* if-nez v0, :cond_0 */
/* .line 135 */
} // :cond_0
(( android.content.pm.PackageManager ) v0 ).getPackagesForUid ( p1 ); // invoke-virtual {v0, p1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;
/* .line 136 */
/* .local v0, "packages":[Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* array-length v2, v0 */
/* if-lez v2, :cond_1 */
/* .line 137 */
int v2 = 0; // const/4 v2, 0x0
/* aget-object v2, v0, v2 */
final String v3 = "com.miui.voiceassist"; // const-string v3, "com.miui.voiceassist"
v2 = android.text.TextUtils .equals ( v2,v3 );
/* xor-int/2addr v1, v2 */
/* .line 139 */
} // :cond_1
/* .line 133 */
} // .end local v0 # "packages":[Ljava/lang/String;
} // :cond_2
} // :goto_0
} // .end method
/* # virtual methods */
public void addClient ( com.xiaomi.aiinput.IAIInputTextManagerClient p0, java.lang.String p1, java.lang.String p2 ) {
/* .locals 3 */
/* .param p1, "client" # Lcom/xiaomi/aiinput/IAIInputTextManagerClient; */
/* .param p2, "ctx" # Ljava/lang/String; */
/* .param p3, "pkg" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 96 */
/* if-nez p1, :cond_0 */
/* .line 97 */
return;
/* .line 100 */
} // :cond_0
v0 = com.android.server.aiinput.AIInputTextManagerService.BLACK_LIST;
(( java.util.HashSet ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_3
/* check-cast v1, Ljava/lang/String; */
/* .line 101 */
/* .local v1, "blackCtx":Ljava/lang/String; */
v2 = (( java.lang.String ) p2 ).contains ( v1 ); // invoke-virtual {p2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v2, :cond_2 */
v2 = android.text.TextUtils .equals ( p3,v1 );
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 104 */
} // .end local v1 # "blackCtx":Ljava/lang/String;
} // :cond_1
/* .line 102 */
/* .restart local v1 # "blackCtx":Ljava/lang/String; */
} // :cond_2
} // :goto_1
return;
/* .line 106 */
} // .end local v1 # "blackCtx":Ljava/lang/String;
} // :cond_3
v0 = this.mManagerClient;
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_4
(( com.android.server.aiinput.AIInputTextManagerService$ManagerClient ) v0 ).getClient ( ); // invoke-virtual {v0}, Lcom/android/server/aiinput/AIInputTextManagerService$ManagerClient;->getClient()Lcom/xiaomi/aiinput/IAIInputTextManagerClient;
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 108 */
try { // :try_start_0
v0 = this.mManagerClient;
(( com.android.server.aiinput.AIInputTextManagerService$ManagerClient ) v0 ).getClient ( ); // invoke-virtual {v0}, Lcom/android/server/aiinput/AIInputTextManagerService$ManagerClient;->getClient()Lcom/xiaomi/aiinput/IAIInputTextManagerClient;
v2 = this.mDeathRecipient;
/* :try_end_0 */
/* .catch Ljava/util/NoSuchElementException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 110 */
/* .line 109 */
/* :catch_0 */
/* move-exception v0 */
/* .line 112 */
} // :cond_4
} // :goto_2
v2 = this.mDeathRecipient;
/* .line 113 */
/* new-instance v0, Lcom/android/server/aiinput/AIInputTextManagerService$ManagerClient; */
/* invoke-direct {v0, p1, p2}, Lcom/android/server/aiinput/AIInputTextManagerService$ManagerClient;-><init>(Lcom/xiaomi/aiinput/IAIInputTextManagerClient;Ljava/lang/String;)V */
this.mManagerClient = v0;
/* .line 114 */
return;
} // .end method
public com.xiaomi.aiinput.IAIInputTextManagerClient getAIInputTextClient ( ) {
/* .locals 4 */
/* .line 118 */
v0 = this.mContext;
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 119 */
/* .line 121 */
} // :cond_0
final String v2 = "com.miui.aiinput.permission.AI_INPUT_INFORMATION"; // const-string v2, "com.miui.aiinput.permission.AI_INPUT_INFORMATION"
final String v3 = "Permission Denial: AI_INPUT_INFORMATION"; // const-string v3, "Permission Denial: AI_INPUT_INFORMATION"
(( android.content.Context ) v0 ).enforceCallingPermission ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 122 */
v0 = android.os.Binder .getCallingUid ( );
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/aiinput/AIInputTextManagerService;->isDenialFromUid(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 123 */
/* .line 125 */
} // :cond_1
v0 = this.mManagerClient;
/* if-nez v0, :cond_2 */
/* .line 126 */
/* .line 128 */
} // :cond_2
(( com.android.server.aiinput.AIInputTextManagerService$ManagerClient ) v0 ).getClient ( ); // invoke-virtual {v0}, Lcom/android/server/aiinput/AIInputTextManagerService$ManagerClient;->getClient()Lcom/xiaomi/aiinput/IAIInputTextManagerClient;
} // .end method
