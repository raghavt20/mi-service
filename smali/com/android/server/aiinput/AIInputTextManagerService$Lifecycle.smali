.class public final Lcom/android/server/aiinput/AIInputTextManagerService$Lifecycle;
.super Lcom/android/server/SystemService;
.source "AIInputTextManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/aiinput/AIInputTextManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Lifecycle"
.end annotation


# instance fields
.field private final mService:Lcom/android/server/aiinput/AIInputTextManagerService;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 29
    invoke-direct {p0, p1}, Lcom/android/server/SystemService;-><init>(Landroid/content/Context;)V

    .line 30
    invoke-static {p1}, Lcom/android/server/aiinput/AIInputTextManagerService;->getInstance(Landroid/content/Context;)Lcom/android/server/aiinput/AIInputTextManagerService;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/aiinput/AIInputTextManagerService$Lifecycle;->mService:Lcom/android/server/aiinput/AIInputTextManagerService;

    .line 31
    return-void
.end method


# virtual methods
.method public onStart()V
    .locals 2

    .line 35
    const-string v0, "aiinputtext"

    iget-object v1, p0, Lcom/android/server/aiinput/AIInputTextManagerService$Lifecycle;->mService:Lcom/android/server/aiinput/AIInputTextManagerService;

    invoke-virtual {p0, v0, v1}, Lcom/android/server/aiinput/AIInputTextManagerService$Lifecycle;->publishBinderService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 36
    return-void
.end method
