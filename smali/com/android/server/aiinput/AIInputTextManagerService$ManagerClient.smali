.class Lcom/android/server/aiinput/AIInputTextManagerService$ManagerClient;
.super Ljava/lang/Object;
.source "AIInputTextManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/aiinput/AIInputTextManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ManagerClient"
.end annotation


# instance fields
.field private mClient:Lcom/xiaomi/aiinput/IAIInputTextManagerClient;

.field private mContext:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/xiaomi/aiinput/IAIInputTextManagerClient;Ljava/lang/String;)V
    .locals 0
    .param p1, "client"    # Lcom/xiaomi/aiinput/IAIInputTextManagerClient;
    .param p2, "ctx"    # Ljava/lang/String;

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-object p1, p0, Lcom/android/server/aiinput/AIInputTextManagerService$ManagerClient;->mClient:Lcom/xiaomi/aiinput/IAIInputTextManagerClient;

    .line 67
    iput-object p2, p0, Lcom/android/server/aiinput/AIInputTextManagerService$ManagerClient;->mContext:Ljava/lang/String;

    .line 68
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "obj"    # Ljava/lang/Object;

    .line 72
    iget-object v0, p0, Lcom/android/server/aiinput/AIInputTextManagerService$ManagerClient;->mClient:Lcom/xiaomi/aiinput/IAIInputTextManagerClient;

    move-object v1, p1

    check-cast v1, Lcom/android/server/aiinput/AIInputTextManagerService$ManagerClient;

    iget-object v1, v1, Lcom/android/server/aiinput/AIInputTextManagerService$ManagerClient;->mClient:Lcom/xiaomi/aiinput/IAIInputTextManagerClient;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/aiinput/AIInputTextManagerService$ManagerClient;->mContext:Ljava/lang/String;

    move-object v1, p1

    check-cast v1, Lcom/android/server/aiinput/AIInputTextManagerService$ManagerClient;

    iget-object v1, v1, Lcom/android/server/aiinput/AIInputTextManagerService$ManagerClient;->mContext:Ljava/lang/String;

    .line 73
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 72
    :goto_0
    return v0
.end method

.method public getClient()Lcom/xiaomi/aiinput/IAIInputTextManagerClient;
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/android/server/aiinput/AIInputTextManagerService$ManagerClient;->mClient:Lcom/xiaomi/aiinput/IAIInputTextManagerClient;

    return-object v0
.end method
