class com.android.server.ForceDarkAppListProvider$1 extends android.database.ContentObserver {
	 /* .source "ForceDarkAppListProvider.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/ForceDarkAppListProvider;->registerDataObserver(Landroid/content/Context;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.ForceDarkAppListProvider this$0; //synthetic
final android.content.Context val$context; //synthetic
/* # direct methods */
 com.android.server.ForceDarkAppListProvider$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/ForceDarkAppListProvider; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 202 */
this.this$0 = p1;
this.val$context = p3;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "selfChange" # Z */
/* .line 205 */
/* const-class v0, Lcom/android/server/ForceDarkAppListProvider; */
(( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
final String v1 = "forceDarkAppListCouldData onChange--"; // const-string v1, "forceDarkAppListCouldData onChange--"
android.util.Slog .w ( v0,v1 );
/* .line 206 */
v0 = this.this$0;
v1 = this.val$context;
(( com.android.server.ForceDarkAppListProvider ) v0 ).updateCloudForceDarkAppList ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/ForceDarkAppListProvider;->updateCloudForceDarkAppList(Landroid/content/Context;)V
/* .line 207 */
return;
} // .end method
