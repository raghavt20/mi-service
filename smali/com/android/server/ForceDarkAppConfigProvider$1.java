class com.android.server.ForceDarkAppConfigProvider$1 extends android.database.ContentObserver {
	 /* .source "ForceDarkAppConfigProvider.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/ForceDarkAppConfigProvider;->registerDataObserver(Landroid/content/Context;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.ForceDarkAppConfigProvider this$0; //synthetic
final android.content.Context val$context; //synthetic
/* # direct methods */
 com.android.server.ForceDarkAppConfigProvider$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/ForceDarkAppConfigProvider; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 143 */
this.this$0 = p1;
this.val$context = p3;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "selfChange" # Z */
/* .line 146 */
com.android.server.ForceDarkAppConfigProvider .-$$Nest$sfgetTAG ( );
final String v1 = "forceDarkCouldData onChange--"; // const-string v1, "forceDarkCouldData onChange--"
android.util.Slog .i ( v0,v1 );
/* .line 147 */
v0 = this.this$0;
v1 = this.val$context;
(( com.android.server.ForceDarkAppConfigProvider ) v0 ).updateLocalForceDarkAppConfig ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/ForceDarkAppConfigProvider;->updateLocalForceDarkAppConfig(Landroid/content/Context;)V
/* .line 148 */
return;
} // .end method
