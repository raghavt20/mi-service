.class Lcom/android/server/power/PowerManagerServiceImpl$5;
.super Ljava/lang/Object;
.source "PowerManagerServiceImpl.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/power/PowerManagerServiceImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/power/PowerManagerServiceImpl;


# direct methods
.method constructor <init>(Lcom/android/server/power/PowerManagerServiceImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/power/PowerManagerServiceImpl;

    .line 1339
    iput-object p1, p0, Lcom/android/server/power/PowerManagerServiceImpl$5;->this$0:Lcom/android/server/power/PowerManagerServiceImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "i"    # I

    .line 1350
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 2
    .param p1, "sensorEvent"    # Landroid/hardware/SensorEvent;

    .line 1342
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 1343
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl$5;->this$0:Lcom/android/server/power/PowerManagerServiceImpl;

    invoke-virtual {v0}, Lcom/android/server/power/PowerManagerServiceImpl;->updateUserActivityLocked()V

    .line 1345
    :cond_0
    return-void
.end method
