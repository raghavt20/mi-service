.class public Lcom/android/server/power/UidStateHelper;
.super Ljava/lang/Object;
.source "UidStateHelper.java"


# static fields
.field private static DEBUG:Z = false

.field private static final MSG_DISPATCH_UID_STATE_CHANGE:I = 0x1

.field private static TAG:Ljava/lang/String;

.field private static sInstance:Lcom/android/server/power/UidStateHelper;


# instance fields
.field private final mActivityManager:Landroid/app/IActivityManager;

.field private final mHandler:Landroid/os/Handler;

.field private mHandlerCallback:Landroid/os/Handler$Callback;

.field private mObserverInstalled:Z

.field private mProcessObserver:Landroid/app/IProcessObserver;

.field private mStateLock:Ljava/lang/Object;

.field private final mUidForeground:Landroid/util/SparseBooleanArray;

.field private final mUidPidForeground:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Landroid/util/SparseBooleanArray;",
            ">;"
        }
    .end annotation
.end field

.field private mUidStateObervers:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList<",
            "Lcom/android/internal/app/IUidStateChangeCallback;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$fgetmStateLock(Lcom/android/server/power/UidStateHelper;)Ljava/lang/Object;
    .locals 0

    iget-object p0, p0, Lcom/android/server/power/UidStateHelper;->mStateLock:Ljava/lang/Object;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmUidPidForeground(Lcom/android/server/power/UidStateHelper;)Landroid/util/SparseArray;
    .locals 0

    iget-object p0, p0, Lcom/android/server/power/UidStateHelper;->mUidPidForeground:Landroid/util/SparseArray;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mcomputeUidForegroundLocked(Lcom/android/server/power/UidStateHelper;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/power/UidStateHelper;->computeUidForegroundLocked(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mdispatchUidStateChange(Lcom/android/server/power/UidStateHelper;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/power/UidStateHelper;->dispatchUidStateChange(II)V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetDEBUG()Z
    .locals 1

    sget-boolean v0, Lcom/android/server/power/UidStateHelper;->DEBUG:Z

    return v0
.end method

.method static bridge synthetic -$$Nest$sfgetTAG()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/server/power/UidStateHelper;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 21
    const-string v0, "UidProcStateHelper"

    sput-object v0, Lcom/android/server/power/UidStateHelper;->TAG:Ljava/lang/String;

    .line 22
    sget-boolean v0, Landroid/os/Build;->IS_DEBUGGABLE:Z

    sput-boolean v0, Lcom/android/server/power/UidStateHelper;->DEBUG:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/server/power/UidStateHelper;->mStateLock:Ljava/lang/Object;

    .line 32
    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v0, p0, Lcom/android/server/power/UidStateHelper;->mUidForeground:Landroid/util/SparseBooleanArray;

    .line 33
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/server/power/UidStateHelper;->mUidPidForeground:Landroid/util/SparseArray;

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/power/UidStateHelper;->mObserverInstalled:Z

    .line 36
    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    iput-object v0, p0, Lcom/android/server/power/UidStateHelper;->mUidStateObervers:Landroid/os/RemoteCallbackList;

    .line 149
    new-instance v0, Lcom/android/server/power/UidStateHelper$1;

    invoke-direct {v0, p0}, Lcom/android/server/power/UidStateHelper$1;-><init>(Lcom/android/server/power/UidStateHelper;)V

    iput-object v0, p0, Lcom/android/server/power/UidStateHelper;->mProcessObserver:Landroid/app/IProcessObserver;

    .line 195
    new-instance v0, Lcom/android/server/power/UidStateHelper$2;

    invoke-direct {v0, p0}, Lcom/android/server/power/UidStateHelper$2;-><init>(Lcom/android/server/power/UidStateHelper;)V

    iput-object v0, p0, Lcom/android/server/power/UidStateHelper;->mHandlerCallback:Landroid/os/Handler$Callback;

    .line 48
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/power/UidStateHelper;->mActivityManager:Landroid/app/IActivityManager;

    .line 49
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->get()Lcom/android/internal/os/BackgroundThread;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/internal/os/BackgroundThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/power/UidStateHelper;->mHandlerCallback:Landroid/os/Handler$Callback;

    invoke-direct {v1, v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v1, p0, Lcom/android/server/power/UidStateHelper;->mHandler:Landroid/os/Handler;

    .line 53
    :try_start_0
    iget-object v1, p0, Lcom/android/server/power/UidStateHelper;->mProcessObserver:Landroid/app/IProcessObserver;

    invoke-interface {v0, v1}, Landroid/app/IActivityManager;->registerProcessObserver(Landroid/app/IProcessObserver;)V

    .line 54
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/power/UidStateHelper;->mObserverInstalled:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 56
    goto :goto_0

    .line 55
    :catch_0
    move-exception v0

    .line 57
    :goto_0
    return-void
.end method

.method private computeUidForegroundLocked(I)V
    .locals 6
    .param p1, "uid"    # I

    .line 114
    iget-object v0, p0, Lcom/android/server/power/UidStateHelper;->mUidPidForeground:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/SparseBooleanArray;

    .line 117
    .local v0, "pidForeground":Landroid/util/SparseBooleanArray;
    const/4 v1, 0x0

    .line 118
    .local v1, "uidForeground":Z
    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result v2

    .line 119
    .local v2, "size":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v2, :cond_1

    .line 120
    invoke-virtual {v0, v3}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 121
    const/4 v1, 0x1

    .line 122
    goto :goto_1

    .line 119
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 126
    .end local v3    # "i":I
    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/android/server/power/UidStateHelper;->mUidForeground:Landroid/util/SparseBooleanArray;

    const/4 v4, 0x0

    invoke-virtual {v3, p1, v4}, Landroid/util/SparseBooleanArray;->get(IZ)Z

    move-result v3

    .line 127
    .local v3, "oldUidForeground":Z
    if-eq v3, v1, :cond_2

    .line 129
    iget-object v4, p0, Lcom/android/server/power/UidStateHelper;->mUidForeground:Landroid/util/SparseBooleanArray;

    invoke-virtual {v4, p1, v1}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 130
    iget-object v4, p0, Lcom/android/server/power/UidStateHelper;->mHandler:Landroid/os/Handler;

    const/4 v5, 0x1

    invoke-virtual {v4, v5, p1, v1}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v4

    .line 131
    invoke-virtual {v4}, Landroid/os/Message;->sendToTarget()V

    .line 133
    :cond_2
    return-void
.end method

.method private dispatchUidStateChange(II)V
    .locals 4
    .param p1, "uid"    # I
    .param p2, "state"    # I

    .line 136
    iget-object v0, p0, Lcom/android/server/power/UidStateHelper;->mUidStateObervers:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v0

    .line 137
    .local v0, "length":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 138
    iget-object v2, p0, Lcom/android/server/power/UidStateHelper;->mUidStateObervers:Landroid/os/RemoteCallbackList;

    invoke-virtual {v2, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v2

    check-cast v2, Lcom/android/internal/app/IUidStateChangeCallback;

    .line 139
    .local v2, "callback":Lcom/android/internal/app/IUidStateChangeCallback;
    if-eqz v2, :cond_0

    .line 141
    :try_start_0
    invoke-interface {v2, p1, p2}, Lcom/android/internal/app/IUidStateChangeCallback;->onUidStateChange(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 143
    goto :goto_1

    .line 142
    :catch_0
    move-exception v3

    .line 137
    .end local v2    # "callback":Lcom/android/internal/app/IUidStateChangeCallback;
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 146
    .end local v1    # "i":I
    :cond_1
    iget-object v1, p0, Lcom/android/server/power/UidStateHelper;->mUidStateObervers:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 147
    return-void
.end method

.method public static get()Lcom/android/server/power/UidStateHelper;
    .locals 1

    .line 41
    sget-object v0, Lcom/android/server/power/UidStateHelper;->sInstance:Lcom/android/server/power/UidStateHelper;

    if-nez v0, :cond_0

    .line 42
    new-instance v0, Lcom/android/server/power/UidStateHelper;

    invoke-direct {v0}, Lcom/android/server/power/UidStateHelper;-><init>()V

    sput-object v0, Lcom/android/server/power/UidStateHelper;->sInstance:Lcom/android/server/power/UidStateHelper;

    .line 44
    :cond_0
    sget-object v0, Lcom/android/server/power/UidStateHelper;->sInstance:Lcom/android/server/power/UidStateHelper;

    return-object v0
.end method


# virtual methods
.method public isUidForeground(I)Z
    .locals 3
    .param p1, "uid"    # I

    .line 79
    invoke-static {p1}, Landroid/os/UserHandle;->isApp(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 80
    const/4 v0, 0x1

    return v0

    .line 82
    :cond_0
    iget-object v0, p0, Lcom/android/server/power/UidStateHelper;->mStateLock:Ljava/lang/Object;

    monitor-enter v0

    .line 83
    :try_start_0
    iget-object v1, p0, Lcom/android/server/power/UidStateHelper;->mUidForeground:Landroid/util/SparseBooleanArray;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/util/SparseBooleanArray;->get(IZ)Z

    move-result v1

    monitor-exit v0

    return v1

    .line 84
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public isUidForeground(IZ)Z
    .locals 9
    .param p1, "uid"    # I
    .param p2, "doubleCheck"    # Z

    .line 88
    invoke-static {p1}, Landroid/os/UserHandle;->isApp(I)Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    .line 89
    return v1

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/android/server/power/UidStateHelper;->mStateLock:Ljava/lang/Object;

    monitor-enter v0

    .line 93
    :try_start_0
    iget-object v2, p0, Lcom/android/server/power/UidStateHelper;->mUidForeground:Landroid/util/SparseBooleanArray;

    const/4 v3, 0x0

    invoke-virtual {v2, p1, v3}, Landroid/util/SparseBooleanArray;->get(IZ)Z

    move-result v2

    .line 94
    .local v2, "isUidFg":Z
    if-eqz p2, :cond_5

    .line 95
    const/4 v4, 0x0

    .line 96
    .local v4, "isFgByPids":Z
    iget-object v5, p0, Lcom/android/server/power/UidStateHelper;->mUidPidForeground:Landroid/util/SparseArray;

    invoke-virtual {v5, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/util/SparseBooleanArray;

    .line 97
    .local v5, "pidForeground":Landroid/util/SparseBooleanArray;
    if-eqz v5, :cond_1

    .line 98
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    invoke-virtual {v5}, Landroid/util/SparseBooleanArray;->size()I

    move-result v7

    if-ge v6, v7, :cond_1

    .line 99
    invoke-virtual {v5, v6}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    .line 98
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 103
    .end local v6    # "i":I
    :cond_1
    if-eq v2, v4, :cond_2

    .line 104
    sget-object v6, Lcom/android/server/power/UidStateHelper;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ProcessObserver may miss callback, isUidFg="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " isFgByPids="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Slog;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    :cond_2
    if-nez v2, :cond_4

    if-eqz v4, :cond_3

    goto :goto_1

    :cond_3
    move v1, v3

    :cond_4
    :goto_1
    monitor-exit v0

    return v1

    .line 108
    .end local v4    # "isFgByPids":Z
    .end local v5    # "pidForeground":Landroid/util/SparseBooleanArray;
    :cond_5
    monitor-exit v0

    .line 109
    return v2

    .line 108
    .end local v2    # "isUidFg":Z
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public registerUidStateObserver(Lcom/android/internal/app/IUidStateChangeCallback;)V
    .locals 2
    .param p1, "callback"    # Lcom/android/internal/app/IUidStateChangeCallback;

    .line 60
    iget-boolean v0, p0, Lcom/android/server/power/UidStateHelper;->mObserverInstalled:Z

    if-eqz v0, :cond_0

    .line 63
    monitor-enter p0

    .line 64
    :try_start_0
    iget-object v0, p0, Lcom/android/server/power/UidStateHelper;->mUidStateObervers:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    .line 65
    monitor-exit p0

    .line 66
    return-void

    .line 65
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 61
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ProcessObserver not installed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public unregisterUidStateObserver(Lcom/android/internal/app/IUidStateChangeCallback;)V
    .locals 2
    .param p1, "callback"    # Lcom/android/internal/app/IUidStateChangeCallback;

    .line 69
    iget-boolean v0, p0, Lcom/android/server/power/UidStateHelper;->mObserverInstalled:Z

    if-eqz v0, :cond_0

    .line 72
    monitor-enter p0

    .line 73
    :try_start_0
    iget-object v0, p0, Lcom/android/server/power/UidStateHelper;->mUidStateObervers:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    .line 74
    monitor-exit p0

    .line 75
    return-void

    .line 74
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 70
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ProcessObserver not installed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
