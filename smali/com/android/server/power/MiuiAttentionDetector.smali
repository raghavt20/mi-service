.class public Lcom/android/server/power/MiuiAttentionDetector;
.super Ljava/lang/Object;
.source "MiuiAttentionDetector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;,
        Lcom/android/server/power/MiuiAttentionDetector$SettingsObserver;,
        Lcom/android/server/power/MiuiAttentionDetector$UserSwitchObserver;
    }
.end annotation


# static fields
.field private static final ALWAYS_CHECK_ATTENTION_TIMEOUT:I = 0x927c0

.field private static final AON_CHECK_INCORRECT_LUX:F

.field public static final AON_SCREEN_OFF_SUPPORTED:Z

.field public static final AON_SCREEN_ON_SUPPORTED:Z

.field public static final AON_SERVICE_CLASS:Ljava/lang/String; = "com.xiaomi.aon.AONService"

.field public static final AON_SERVICE_PACKAGE:Ljava/lang/String; = "com.xiaomi.aon"

.field private static final CONNECTION_TTL_MILLIS:J = 0xea60L

.field private static final DIM_DURATION_CHECK_ATTENTION_TIMEOUT:I = 0x4e20

.field private static final EXTERNAL_DISPLAY_CONNECTED:I = 0x1

.field private static final FPS_CHECK_ATTENTION_SCREEN_OFF:I

.field private static final FPS_CHECK_ATTENTION_SCREEN_ON:I

.field private static final NO_ACTIVITE_CHECK_ATTENTION_MILLIS:J = 0x1f40L

.field private static final NO_ACTIVITY_CHECK_ATTENTION_TIMEOUT:I = 0x7d0

.field public static final REASON_DIM_DURATION_CHECK_ATTENTION:Ljava/lang/String; = "dim duration"

.field public static final REASON_INTERACTIVE_CHANGE_CHECK_ATTENTION:Ljava/lang/String; = "interactive change"

.field public static final REASON_NO_USER_ACTIVITY_CHECK_ATTENTION:Ljava/lang/String; = "no user activity"

.field private static final SERVICE_BIND_AWAIT_MILLIS:J = 0x3e8L

.field private static final SYNERGY_MODE_ON:I = 0x1

.field private static final TAG:Ljava/lang/String;

.field private static final TYPE_CHECK_ATTENTION:I

.field private static final TYPE_CHECK_ATTENTION_EYE:I = 0x4

.field private static final TYPE_CHECK_ATTENTION_FACE:I = 0x1


# instance fields
.field private mAonScreenOffEnabled:Z

.field private mAonScreenOnEnabled:Z

.field private mAonService:Lcom/xiaomi/aon/IMiAON;

.field private final mAttentionHandler:Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;

.field private final mAttentionListener:Lcom/xiaomi/aon/IMiAONListener;

.field private mCheckTimeout:I

.field private mComponentName:Landroid/content/ComponentName;

.field private final mConnection:Landroid/content/ServiceConnection;

.field private final mContext:Landroid/content/Context;

.field private mExternalDisplayConnected:Z

.field private mFps:I

.field private mInteractive:Z

.field private mIsChecked:Z

.field private mLightSensor:Landroid/hardware/Sensor;

.field private mLightSensorEnabled:Z

.field private final mLightSensorListener:Landroid/hardware/SensorEventListener;

.field private mLux:F

.field private mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

.field private mPowerManager:Landroid/os/PowerManager;

.field private final mPowerManagerServiceImpl:Lcom/android/server/power/PowerManagerServiceImpl;

.field private mReason:Ljava/lang/String;

.field private mScreenProjectInScreen:Z

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mServiceBindingLatch:Ljava/util/concurrent/CountDownLatch;

.field private mSettingsObserver:Lcom/android/server/power/MiuiAttentionDetector$SettingsObserver;

.field private mStartTimeStamp:J

.field private mStayOn:Z

.field private mSynergyModeEnable:Z

.field private mType:I


# direct methods
.method public static synthetic $r8$lambda$dNnNsyuIYa1on_Ynz2xhkdT6lpI(Lcom/android/server/power/MiuiAttentionDetector;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector;->lambda$onUserActivityChanged$2()V

    return-void
.end method

.method public static synthetic $r8$lambda$f_REzlYFnWzsw3f6ZxB3O4BhEhs(Lcom/android/server/power/MiuiAttentionDetector;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector;->lambda$handleAttentionResult$0()V

    return-void
.end method

.method public static synthetic $r8$lambda$lX-hb_8T4lQgeJF_5KXXQgKTFbc(Lcom/android/server/power/MiuiAttentionDetector;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/power/MiuiAttentionDetector;->lambda$notifyInteractiveChange$1(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmAttentionHandler(Lcom/android/server/power/MiuiAttentionDetector;)Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mAttentionHandler:Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmServiceBindingLatch(Lcom/android/server/power/MiuiAttentionDetector;)Ljava/util/concurrent/CountDownLatch;
    .locals 0

    iget-object p0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mServiceBindingLatch:Ljava/util/concurrent/CountDownLatch;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmAonService(Lcom/android/server/power/MiuiAttentionDetector;Lcom/xiaomi/aon/IMiAON;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/power/MiuiAttentionDetector;->mAonService:Lcom/xiaomi/aon/IMiAON;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsChecked(Lcom/android/server/power/MiuiAttentionDetector;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/power/MiuiAttentionDetector;->mIsChecked:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLux(Lcom/android/server/power/MiuiAttentionDetector;F)V
    .locals 0

    iput p1, p0, Lcom/android/server/power/MiuiAttentionDetector;->mLux:F

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmServiceBindingLatch(Lcom/android/server/power/MiuiAttentionDetector;Ljava/util/concurrent/CountDownLatch;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/power/MiuiAttentionDetector;->mServiceBindingLatch:Ljava/util/concurrent/CountDownLatch;

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleAttentionResult(Lcom/android/server/power/MiuiAttentionDetector;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/power/MiuiAttentionDetector;->handleAttentionResult(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandlePendingCallback(Lcom/android/server/power/MiuiAttentionDetector;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector;->handlePendingCallback()V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetLightSensorEnable(Lcom/android/server/power/MiuiAttentionDetector;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/power/MiuiAttentionDetector;->setLightSensorEnable(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateAonScreenOffConfig(Lcom/android/server/power/MiuiAttentionDetector;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector;->updateAonScreenOffConfig()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateAonScreenOnConfig(Lcom/android/server/power/MiuiAttentionDetector;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector;->updateAonScreenOnConfig()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateConfigState(Lcom/android/server/power/MiuiAttentionDetector;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector;->updateConfigState()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateExternalDisplayConnectConfig(Lcom/android/server/power/MiuiAttentionDetector;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector;->updateExternalDisplayConnectConfig()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateScreenProjectConfig(Lcom/android/server/power/MiuiAttentionDetector;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector;->updateScreenProjectConfig()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateSynergyModeConfig(Lcom/android/server/power/MiuiAttentionDetector;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector;->updateSynergyModeConfig()V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetFPS_CHECK_ATTENTION_SCREEN_OFF()I
    .locals 1

    sget v0, Lcom/android/server/power/MiuiAttentionDetector;->FPS_CHECK_ATTENTION_SCREEN_OFF:I

    return v0
.end method

.method static bridge synthetic -$$Nest$sfgetTAG()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/server/power/MiuiAttentionDetector;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$sfgetTYPE_CHECK_ATTENTION()I
    .locals 1

    sget v0, Lcom/android/server/power/MiuiAttentionDetector;->TYPE_CHECK_ATTENTION:I

    return v0
.end method

.method static constructor <clinit>()V
    .locals 4

    .line 56
    const-class v0, Lcom/android/server/power/MiuiAttentionDetector;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/power/MiuiAttentionDetector;->TAG:Ljava/lang/String;

    .line 107
    nop

    .line 108
    const-string v0, "aon_screen_off_fps"

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getInteger(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/server/power/MiuiAttentionDetector;->FPS_CHECK_ATTENTION_SCREEN_OFF:I

    .line 109
    nop

    .line 110
    const-string v0, "aon_screen_on_fps"

    const/16 v1, 0xf

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getInteger(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/server/power/MiuiAttentionDetector;->FPS_CHECK_ATTENTION_SCREEN_ON:I

    .line 123
    nop

    .line 124
    const-string v0, "config_aon_check_incorrect_lux"

    const/high16 v1, 0x40c00000    # 6.0f

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getFloat(Ljava/lang/String;F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    sput v0, Lcom/android/server/power/MiuiAttentionDetector;->AON_CHECK_INCORRECT_LUX:F

    .line 126
    nop

    .line 127
    const-string v0, "check_attention_type"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getInteger(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/server/power/MiuiAttentionDetector;->TYPE_CHECK_ATTENTION:I

    .line 129
    nop

    .line 130
    const-string v0, "config_aon_proximity_available"

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_1

    .line 131
    const-string v3, "config_aon_screen_on_available"

    invoke-static {v3, v2}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_0
    move v3, v2

    goto :goto_1

    :cond_1
    :goto_0
    move v3, v1

    :goto_1
    sput-boolean v3, Lcom/android/server/power/MiuiAttentionDetector;->AON_SCREEN_ON_SUPPORTED:Z

    .line 133
    nop

    .line 134
    invoke-static {v0, v2}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_3

    .line 135
    const-string v0, "config_aon_screen_off_available"

    invoke-static {v0, v2}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_2

    :cond_2
    move v1, v2

    goto :goto_3

    :cond_3
    :goto_2
    nop

    :goto_3
    sput-boolean v1, Lcom/android/server/power/MiuiAttentionDetector;->AON_SCREEN_OFF_SUPPORTED:Z

    .line 133
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/PowerManager;Lcom/android/server/power/PowerManagerServiceImpl;Lcom/android/server/policy/WindowManagerPolicy;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "powerManager"    # Landroid/os/PowerManager;
    .param p3, "impl"    # Lcom/android/server/power/PowerManagerServiceImpl;
    .param p4, "policy"    # Lcom/android/server/policy/WindowManagerPolicy;

    .line 137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mInteractive:Z

    .line 160
    new-instance v1, Lcom/android/server/power/MiuiAttentionDetector$1;

    invoke-direct {v1, p0}, Lcom/android/server/power/MiuiAttentionDetector$1;-><init>(Lcom/android/server/power/MiuiAttentionDetector;)V

    iput-object v1, p0, Lcom/android/server/power/MiuiAttentionDetector;->mLightSensorListener:Landroid/hardware/SensorEventListener;

    .line 172
    new-instance v1, Lcom/android/server/power/MiuiAttentionDetector$2;

    invoke-direct {v1, p0}, Lcom/android/server/power/MiuiAttentionDetector$2;-><init>(Lcom/android/server/power/MiuiAttentionDetector;)V

    iput-object v1, p0, Lcom/android/server/power/MiuiAttentionDetector;->mAttentionListener:Lcom/xiaomi/aon/IMiAONListener;

    .line 250
    new-instance v1, Lcom/android/server/power/MiuiAttentionDetector$3;

    invoke-direct {v1, p0}, Lcom/android/server/power/MiuiAttentionDetector$3;-><init>(Lcom/android/server/power/MiuiAttentionDetector;)V

    iput-object v1, p0, Lcom/android/server/power/MiuiAttentionDetector;->mConnection:Landroid/content/ServiceConnection;

    .line 138
    iput-object p1, p0, Lcom/android/server/power/MiuiAttentionDetector;->mContext:Landroid/content/Context;

    .line 139
    iput-object p3, p0, Lcom/android/server/power/MiuiAttentionDetector;->mPowerManagerServiceImpl:Lcom/android/server/power/PowerManagerServiceImpl;

    .line 140
    iput-object p4, p0, Lcom/android/server/power/MiuiAttentionDetector;->mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    .line 141
    new-instance v1, Lcom/android/server/ServiceThread;

    const/4 v2, -0x4

    const/4 v3, 0x0

    const-string v4, "MiuiAttentionDetector"

    invoke-direct {v1, v4, v2, v3}, Lcom/android/server/ServiceThread;-><init>(Ljava/lang/String;IZ)V

    .line 143
    .local v1, "handlerThread":Lcom/android/server/ServiceThread;
    invoke-virtual {v1}, Lcom/android/server/ServiceThread;->start()V

    .line 144
    new-instance v2, Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;

    invoke-virtual {v1}, Lcom/android/server/ServiceThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;-><init>(Lcom/android/server/power/MiuiAttentionDetector;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/android/server/power/MiuiAttentionDetector;->mAttentionHandler:Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;

    .line 145
    iput-object p2, p0, Lcom/android/server/power/MiuiAttentionDetector;->mPowerManager:Landroid/os/PowerManager;

    .line 146
    new-instance v3, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v3, v0}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v3, p0, Lcom/android/server/power/MiuiAttentionDetector;->mServiceBindingLatch:Ljava/util/concurrent/CountDownLatch;

    .line 147
    new-instance v0, Lcom/android/server/power/MiuiAttentionDetector$SettingsObserver;

    invoke-direct {v0, p0, v2}, Lcom/android/server/power/MiuiAttentionDetector$SettingsObserver;-><init>(Lcom/android/server/power/MiuiAttentionDetector;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mSettingsObserver:Lcom/android/server/power/MiuiAttentionDetector$SettingsObserver;

    .line 148
    const-string/jumbo v0, "sensor"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mSensorManager:Landroid/hardware/SensorManager;

    .line 149
    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mLightSensor:Landroid/hardware/Sensor;

    .line 150
    invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector;->updateConfigState()V

    .line 151
    invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector;->registerContentObserver()V

    .line 153
    :try_start_0
    new-instance v0, Lcom/android/server/power/MiuiAttentionDetector$UserSwitchObserver;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Lcom/android/server/power/MiuiAttentionDetector$UserSwitchObserver;-><init>(Lcom/android/server/power/MiuiAttentionDetector;Lcom/android/server/power/MiuiAttentionDetector$UserSwitchObserver-IA;)V

    .line 154
    .local v0, "observer":Lcom/android/server/power/MiuiAttentionDetector$UserSwitchObserver;
    invoke-static {}, Landroid/app/ActivityManager;->getService()Landroid/app/IActivityManager;

    move-result-object v2

    sget-object v3, Lcom/android/server/power/MiuiAttentionDetector;->TAG:Ljava/lang/String;

    invoke-interface {v2, v0, v3}, Landroid/app/IActivityManager;->registerUserSwitchObserver(Landroid/app/IUserSwitchObserver;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 157
    .end local v0    # "observer":Lcom/android/server/power/MiuiAttentionDetector$UserSwitchObserver;
    goto :goto_0

    .line 155
    :catch_0
    move-exception v0

    .line 158
    :goto_0
    return-void
.end method

.method private awaitServiceBinding()V
    .locals 4

    .line 347
    :try_start_0
    iget-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mServiceBindingLatch:Ljava/util/concurrent/CountDownLatch;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 350
    goto :goto_0

    .line 348
    :catch_0
    move-exception v0

    .line 349
    .local v0, "e":Ljava/lang/InterruptedException;
    sget-object v1, Lcom/android/server/power/MiuiAttentionDetector;->TAG:Ljava/lang/String;

    const-string v2, "Interrupted while waiting to bind Attention Service."

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 351
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :goto_0
    return-void
.end method

.method private bindAonService()V
    .locals 5

    .line 323
    iget-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mAonService:Lcom/xiaomi/aon/IMiAON;

    if-eqz v0, :cond_0

    .line 324
    return-void

    .line 326
    :cond_0
    iget-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mComponentName:Landroid/content/ComponentName;

    const-string v1, "com.xiaomi.aon.AONService"

    if-nez v0, :cond_1

    .line 327
    new-instance v0, Landroid/content/ComponentName;

    const-string v2, "com.xiaomi.aon"

    invoke-direct {v0, v2, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mComponentName:Landroid/content/ComponentName;

    .line 329
    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/server/power/MiuiAttentionDetector;->mComponentName:Landroid/content/ComponentName;

    .line 330
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 331
    .local v0, "serviceIntent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/android/server/power/MiuiAttentionDetector;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/power/MiuiAttentionDetector;->mConnection:Landroid/content/ServiceConnection;

    const v3, 0x4000001

    sget-object v4, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/content/Context;->bindServiceAsUser(Landroid/content/Intent;Landroid/content/ServiceConnection;ILandroid/os/UserHandle;)Z

    .line 333
    return-void
.end method

.method private freeIfInactive()V
    .locals 4

    .line 408
    iget-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mAttentionHandler:Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;->removeMessages(I)V

    .line 410
    iget-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mAttentionHandler:Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;

    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 412
    return-void
.end method

.method private handleAttentionResult(I)V
    .locals 10
    .param p1, "result"    # I

    .line 206
    invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector;->isScreenCastMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 207
    sget-object v0, Lcom/android/server/power/MiuiAttentionDetector;->TAG:Ljava/lang/String;

    const-string v1, "ignore aon event in hangup mode"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    return-void

    .line 210
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 211
    .local v0, "time":J
    const-string v2, ""

    .line 212
    .local v2, "action":Ljava/lang/String;
    const/4 v3, 0x0

    .line 213
    .local v3, "screenWakeLock":Z
    iget-boolean v4, p0, Lcom/android/server/power/MiuiAttentionDetector;->mInteractive:Z

    const/4 v5, 0x1

    const/4 v6, 0x0

    if-eqz v4, :cond_3

    .line 214
    iget-object v4, p0, Lcom/android/server/power/MiuiAttentionDetector;->mPowerManagerServiceImpl:Lcom/android/server/power/PowerManagerServiceImpl;

    .line 215
    invoke-virtual {v4, v6}, Lcom/android/server/power/PowerManagerServiceImpl;->isAcquiredScreenBrightWakeLock(I)Z

    move-result v3

    .line 218
    if-nez p1, :cond_1

    iget-boolean v4, p0, Lcom/android/server/power/MiuiAttentionDetector;->mLightSensorEnabled:Z

    if-eqz v4, :cond_1

    iget v4, p0, Lcom/android/server/power/MiuiAttentionDetector;->mLux:F

    sget v7, Lcom/android/server/power/MiuiAttentionDetector;->AON_CHECK_INCORRECT_LUX:F

    cmpg-float v4, v4, v7

    if-gez v4, :cond_1

    .line 220
    const-string v2, "low lux,ignore"

    .line 221
    invoke-virtual {p0}, Lcom/android/server/power/MiuiAttentionDetector;->onUserActivityChanged()V

    goto :goto_0

    .line 222
    :cond_1
    if-nez v3, :cond_2

    iget-boolean v4, p0, Lcom/android/server/power/MiuiAttentionDetector;->mStayOn:Z

    if-nez v4, :cond_2

    if-nez p1, :cond_2

    .line 223
    const-string v2, "dim"

    .line 224
    iget-object v4, p0, Lcom/android/server/power/MiuiAttentionDetector;->mPowerManagerServiceImpl:Lcom/android/server/power/PowerManagerServiceImpl;

    invoke-virtual {v4}, Lcom/android/server/power/PowerManagerServiceImpl;->requestDimmingRightNowDueToAttention()V

    .line 225
    iget-object v4, p0, Lcom/android/server/power/MiuiAttentionDetector;->mAttentionHandler:Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;

    new-instance v7, Lcom/android/server/power/MiuiAttentionDetector$$ExternalSyntheticLambda1;

    invoke-direct {v7, p0}, Lcom/android/server/power/MiuiAttentionDetector$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/power/MiuiAttentionDetector;)V

    invoke-virtual {v4, v7}, Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 234
    :cond_2
    const-string/jumbo v2, "userActivity"

    .line 235
    iget-object v4, p0, Lcom/android/server/power/MiuiAttentionDetector;->mPowerManager:Landroid/os/PowerManager;

    invoke-virtual {v4, v0, v1, v6, v6}, Landroid/os/PowerManager;->userActivity(JII)V

    goto :goto_0

    .line 237
    :cond_3
    if-ne p1, v5, :cond_4

    .line 238
    const-string/jumbo v2, "wakeUp"

    .line 239
    iget-object v4, p0, Lcom/android/server/power/MiuiAttentionDetector;->mPowerManager:Landroid/os/PowerManager;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v7

    const-string v9, "aon"

    invoke-virtual {v4, v7, v8, v6, v9}, Landroid/os/PowerManager;->wakeUp(JILjava/lang/String;)V

    .line 242
    :cond_4
    :goto_0
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 243
    iget-object v4, p0, Lcom/android/server/power/MiuiAttentionDetector;->mPowerManagerServiceImpl:Lcom/android/server/power/PowerManagerServiceImpl;

    iget-boolean v7, p0, Lcom/android/server/power/MiuiAttentionDetector;->mInteractive:Z

    if-ne p1, v5, :cond_5

    goto :goto_1

    :cond_5
    move v5, v6

    :goto_1
    iget-wide v8, p0, Lcom/android/server/power/MiuiAttentionDetector;->mStartTimeStamp:J

    invoke-virtual {v4, v7, v5, v8, v9}, Lcom/android/server/power/PowerManagerServiceImpl;->notifyAonScreenOnOffEvent(ZZJ)V

    .line 246
    :cond_6
    sget-object v4, Lcom/android/server/power/MiuiAttentionDetector;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onSuccess: mInteractive:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v6, p0, Lcom/android/server/power/MiuiAttentionDetector;->mInteractive:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " screenWakelock:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " result:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " action:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " lux:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/android/server/power/MiuiAttentionDetector;->mLux:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    return-void
.end method

.method private handlePendingCallback()V
    .locals 4

    .line 292
    iget-boolean v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mIsChecked:Z

    if-nez v0, :cond_0

    .line 293
    iget v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mType:I

    iget v1, p0, Lcom/android/server/power/MiuiAttentionDetector;->mFps:I

    iget-object v2, p0, Lcom/android/server/power/MiuiAttentionDetector;->mReason:Ljava/lang/String;

    iget v3, p0, Lcom/android/server/power/MiuiAttentionDetector;->mCheckTimeout:I

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/android/server/power/MiuiAttentionDetector;->checkAttention(IILjava/lang/String;I)V

    .line 295
    :cond_0
    return-void
.end method

.method private isScreenCastMode()Z
    .locals 1

    .line 484
    iget-boolean v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mScreenProjectInScreen:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mSynergyModeEnable:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mExternalDisplayConnected:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private synthetic lambda$handleAttentionResult$0()V
    .locals 4

    .line 227
    invoke-virtual {p0}, Lcom/android/server/power/MiuiAttentionDetector;->cancelAttentionCheck()V

    .line 228
    sget v0, Lcom/android/server/power/MiuiAttentionDetector;->TYPE_CHECK_ATTENTION:I

    sget v1, Lcom/android/server/power/MiuiAttentionDetector;->FPS_CHECK_ATTENTION_SCREEN_OFF:I

    const-string v2, "dim duration"

    const/16 v3, 0x4e20

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/android/server/power/MiuiAttentionDetector;->checkAttention(IILjava/lang/String;I)V

    .line 232
    return-void
.end method

.method private synthetic lambda$notifyInteractiveChange$1(Z)V
    .locals 4
    .param p1, "interactive"    # Z

    .line 419
    iget-boolean v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mInteractive:Z

    if-eq v0, p1, :cond_2

    .line 420
    iput-boolean p1, p0, Lcom/android/server/power/MiuiAttentionDetector;->mInteractive:Z

    .line 421
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/server/power/MiuiAttentionDetector;->setLightSensorEnable(Z)V

    .line 423
    iget-boolean v1, p0, Lcom/android/server/power/MiuiAttentionDetector;->mInteractive:Z

    const/4 v2, 0x1

    if-nez v1, :cond_0

    .line 424
    iget-object v1, p0, Lcom/android/server/power/MiuiAttentionDetector;->mAttentionHandler:Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;

    invoke-virtual {v1, v2}, Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;->removeMessages(I)V

    .line 425
    iget-object v1, p0, Lcom/android/server/power/MiuiAttentionDetector;->mAttentionHandler:Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;

    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;->removeMessages(I)V

    .line 427
    :cond_0
    if-nez p1, :cond_1

    iget-boolean v1, p0, Lcom/android/server/power/MiuiAttentionDetector;->mAonScreenOnEnabled:Z

    if-eqz v1, :cond_1

    move v0, v2

    :cond_1
    invoke-direct {p0, v0}, Lcom/android/server/power/MiuiAttentionDetector;->registerAttentionListenerIfNeeded(Z)V

    .line 429
    :cond_2
    return-void
.end method

.method private synthetic lambda$onUserActivityChanged$2()V
    .locals 4

    .line 470
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/server/power/MiuiAttentionDetector;->setLightSensorEnable(Z)V

    .line 471
    invoke-virtual {p0}, Lcom/android/server/power/MiuiAttentionDetector;->cancelAttentionCheck()V

    .line 472
    iget-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mAttentionHandler:Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;->removeMessages(I)V

    .line 473
    iget-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mAttentionHandler:Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;

    const-wide/16 v2, 0x1f40

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 476
    return-void
.end method

.method private registerAttentionListenerIfNeeded(Z)V
    .locals 4
    .param p1, "register"    # Z

    .line 446
    if-eqz p1, :cond_1

    .line 448
    iget-boolean v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mIsChecked:Z

    if-eqz v0, :cond_0

    .line 449
    invoke-virtual {p0}, Lcom/android/server/power/MiuiAttentionDetector;->cancelAttentionCheck()V

    .line 451
    :cond_0
    sget v0, Lcom/android/server/power/MiuiAttentionDetector;->TYPE_CHECK_ATTENTION:I

    sget v1, Lcom/android/server/power/MiuiAttentionDetector;->FPS_CHECK_ATTENTION_SCREEN_ON:I

    const-string v2, "interactive change"

    const v3, 0x927c0

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/android/server/power/MiuiAttentionDetector;->checkAttention(IILjava/lang/String;I)V

    goto :goto_0

    .line 456
    :cond_1
    invoke-virtual {p0}, Lcom/android/server/power/MiuiAttentionDetector;->cancelAndUnbind()V

    .line 458
    :goto_0
    return-void
.end method

.method private registerContentObserver()V
    .locals 5

    .line 306
    iget-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 307
    .local v0, "cr":Landroid/content/ContentResolver;
    const-string v1, "miui_people_near_screen_on"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/power/MiuiAttentionDetector;->mSettingsObserver:Lcom/android/server/power/MiuiAttentionDetector$SettingsObserver;

    const/4 v3, 0x0

    const/4 v4, -0x1

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 309
    const-string v1, "gaze_lock_screen_setting"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/power/MiuiAttentionDetector;->mSettingsObserver:Lcom/android/server/power/MiuiAttentionDetector$SettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 311
    const-string/jumbo v1, "screen_project_in_screening"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/power/MiuiAttentionDetector;->mSettingsObserver:Lcom/android/server/power/MiuiAttentionDetector$SettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 314
    const-string/jumbo v1, "synergy_mode"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/power/MiuiAttentionDetector;->mSettingsObserver:Lcom/android/server/power/MiuiAttentionDetector$SettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 317
    const-string v1, "external_display_connected"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/power/MiuiAttentionDetector;->mSettingsObserver:Lcom/android/server/power/MiuiAttentionDetector$SettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 320
    return-void
.end method

.method private setLightSensorEnable(Z)V
    .locals 4
    .param p1, "enabled"    # Z

    .line 433
    iget-boolean v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mLightSensorEnabled:Z

    if-ne p1, v0, :cond_0

    .line 434
    return-void

    .line 436
    :cond_0
    if-eqz p1, :cond_1

    .line 437
    iget-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/android/server/power/MiuiAttentionDetector;->mLightSensorListener:Landroid/hardware/SensorEventListener;

    iget-object v2, p0, Lcom/android/server/power/MiuiAttentionDetector;->mLightSensor:Landroid/hardware/Sensor;

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    goto :goto_0

    .line 440
    :cond_1
    iget-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/android/server/power/MiuiAttentionDetector;->mLightSensorListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 442
    :goto_0
    iput-boolean p1, p0, Lcom/android/server/power/MiuiAttentionDetector;->mLightSensorEnabled:Z

    .line 443
    return-void
.end method

.method private updateAonScreenOffConfig()V
    .locals 4

    .line 510
    sget-boolean v0, Lcom/android/server/power/MiuiAttentionDetector;->AON_SCREEN_OFF_SUPPORTED:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mContext:Landroid/content/Context;

    .line 511
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "gaze_lock_screen_setting"

    const/4 v3, -0x2

    invoke-static {v0, v2, v1, v3}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    nop

    :goto_0
    iput-boolean v1, p0, Lcom/android/server/power/MiuiAttentionDetector;->mAonScreenOffEnabled:Z

    .line 515
    if-nez v1, :cond_1

    .line 516
    invoke-virtual {p0}, Lcom/android/server/power/MiuiAttentionDetector;->cancelAttentionCheck()V

    .line 517
    iget-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mAttentionHandler:Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;->removeMessages(I)V

    goto :goto_1

    .line 519
    :cond_1
    invoke-virtual {p0}, Lcom/android/server/power/MiuiAttentionDetector;->onUserActivityChanged()V

    .line 521
    :goto_1
    return-void
.end method

.method private updateAonScreenOnConfig()V
    .locals 4

    .line 488
    sget-boolean v0, Lcom/android/server/power/MiuiAttentionDetector;->AON_SCREEN_ON_SUPPORTED:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mContext:Landroid/content/Context;

    .line 489
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "miui_people_near_screen_on"

    const/4 v3, -0x2

    invoke-static {v0, v2, v1, v3}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    nop

    :goto_0
    iput-boolean v1, p0, Lcom/android/server/power/MiuiAttentionDetector;->mAonScreenOnEnabled:Z

    .line 491
    return-void
.end method

.method private updateConfigState()V
    .locals 0

    .line 298
    invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector;->updateAonScreenOnConfig()V

    .line 299
    invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector;->updateAonScreenOffConfig()V

    .line 300
    invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector;->updateScreenProjectConfig()V

    .line 301
    invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector;->updateSynergyModeConfig()V

    .line 302
    invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector;->updateExternalDisplayConnectConfig()V

    .line 303
    return-void
.end method

.method private updateExternalDisplayConnectConfig()V
    .locals 4

    .line 504
    iget-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, -0x2

    const-string v2, "external_display_connected"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    move v3, v1

    :cond_0
    iput-boolean v3, p0, Lcom/android/server/power/MiuiAttentionDetector;->mExternalDisplayConnected:Z

    .line 507
    return-void
.end method

.method private updateScreenProjectConfig()V
    .locals 4

    .line 494
    iget-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, -0x2

    const-string/jumbo v2, "screen_project_in_screening"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v3, 0x1

    :cond_0
    iput-boolean v3, p0, Lcom/android/server/power/MiuiAttentionDetector;->mScreenProjectInScreen:Z

    .line 496
    return-void
.end method

.method private updateSynergyModeConfig()V
    .locals 4

    .line 499
    iget-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, -0x2

    const-string/jumbo v2, "synergy_mode"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    move v3, v1

    :cond_0
    iput-boolean v3, p0, Lcom/android/server/power/MiuiAttentionDetector;->mSynergyModeEnable:Z

    .line 501
    return-void
.end method


# virtual methods
.method public cancelAndUnbind()V
    .locals 2

    .line 336
    iget-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mAonService:Lcom/xiaomi/aon/IMiAON;

    if-nez v0, :cond_0

    .line 337
    return-void

    .line 339
    :cond_0
    invoke-virtual {p0}, Lcom/android/server/power/MiuiAttentionDetector;->cancelAttentionCheck()V

    .line 340
    iget-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/server/power/MiuiAttentionDetector;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 341
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mAonService:Lcom/xiaomi/aon/IMiAON;

    .line 342
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mServiceBindingLatch:Ljava/util/concurrent/CountDownLatch;

    .line 343
    return-void
.end method

.method public cancelAttentionCheck()V
    .locals 4

    .line 396
    :try_start_0
    iget-boolean v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mIsChecked:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mAonService:Lcom/xiaomi/aon/IMiAON;

    if-eqz v0, :cond_0

    .line 397
    sget-object v0, Lcom/android/server/power/MiuiAttentionDetector;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Attention check cancel, callers: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x3

    invoke-static {v2}, Landroid/os/Debug;->getCallers(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    iget-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mAonService:Lcom/xiaomi/aon/IMiAON;

    iget v1, p0, Lcom/android/server/power/MiuiAttentionDetector;->mType:I

    iget-object v2, p0, Lcom/android/server/power/MiuiAttentionDetector;->mAttentionListener:Lcom/xiaomi/aon/IMiAONListener;

    invoke-interface {v0, v1, v2}, Lcom/xiaomi/aon/IMiAON;->unregisterListener(ILcom/xiaomi/aon/IMiAONListener;)I

    .line 399
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mIsChecked:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 403
    :cond_0
    goto :goto_0

    .line 401
    :catch_0
    move-exception v0

    .line 402
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/android/server/power/MiuiAttentionDetector;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cancelAttentionCheck: error "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 404
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method

.method public checkAttention(IILjava/lang/String;I)V
    .locals 4
    .param p1, "type"    # I
    .param p2, "fps"    # I
    .param p3, "reason"    # Ljava/lang/String;
    .param p4, "timeout"    # I

    .line 362
    iget-boolean v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mInteractive:Z

    if-eqz v0, :cond_1

    .line 364
    iget-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    invoke-interface {v0}, Lcom/android/server/policy/WindowManagerPolicy;->isKeyguardShowingAndNotOccluded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 365
    return-void

    .line 369
    :cond_0
    invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector;->freeIfInactive()V

    .line 371
    :cond_1
    invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector;->bindAonService()V

    .line 373
    invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector;->awaitServiceBinding()V

    .line 374
    iput p1, p0, Lcom/android/server/power/MiuiAttentionDetector;->mType:I

    .line 375
    iput p2, p0, Lcom/android/server/power/MiuiAttentionDetector;->mFps:I

    .line 376
    iput-object p3, p0, Lcom/android/server/power/MiuiAttentionDetector;->mReason:Ljava/lang/String;

    .line 377
    iput p4, p0, Lcom/android/server/power/MiuiAttentionDetector;->mCheckTimeout:I

    .line 378
    sget-object v0, Lcom/android/server/power/MiuiAttentionDetector;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "check attention, the reason is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", attentionService is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/power/MiuiAttentionDetector;->mAonService:Lcom/xiaomi/aon/IMiAON;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mIsChecked:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/power/MiuiAttentionDetector;->mIsChecked:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 381
    :try_start_0
    iget-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mAonService:Lcom/xiaomi/aon/IMiAON;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mIsChecked:Z

    if-nez v0, :cond_2

    .line 382
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mStartTimeStamp:J

    .line 383
    iget-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mAonService:Lcom/xiaomi/aon/IMiAON;

    int-to-float v1, p2

    iget-object v2, p0, Lcom/android/server/power/MiuiAttentionDetector;->mAttentionListener:Lcom/xiaomi/aon/IMiAONListener;

    invoke-interface {v0, p1, v1, p4, v2}, Lcom/xiaomi/aon/IMiAON;->registerListener(IFILcom/xiaomi/aon/IMiAONListener;)I

    .line 384
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mIsChecked:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 388
    :cond_2
    goto :goto_0

    .line 386
    :catch_0
    move-exception v0

    .line 387
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/android/server/power/MiuiAttentionDetector;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "checkAttention: error "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 389
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method

.method public dump(Ljava/io/PrintWriter;)V
    .locals 2
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 579
    const-string v0, "MiuiAttentionDetector:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 580
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " mComponentName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/power/MiuiAttentionDetector;->mComponentName:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 581
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " mAonScreenOnEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/power/MiuiAttentionDetector;->mAonScreenOnEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 582
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " mAonScreenOffEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/power/MiuiAttentionDetector;->mAonScreenOffEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 583
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " mIsChecked="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/power/MiuiAttentionDetector;->mIsChecked:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 584
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " isScreenCastMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector;->isScreenCastMode()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 585
    return-void
.end method

.method public notifyInteractiveChange(Z)V
    .locals 2
    .param p1, "interactive"    # Z

    .line 415
    iget-boolean v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mAonScreenOnEnabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mAonScreenOffEnabled:Z

    if-eqz v0, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector;->isScreenCastMode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 416
    :cond_1
    return-void

    .line 418
    :cond_2
    iget-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mAttentionHandler:Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;

    new-instance v1, Lcom/android/server/power/MiuiAttentionDetector$$ExternalSyntheticLambda2;

    invoke-direct {v1, p0, p1}, Lcom/android/server/power/MiuiAttentionDetector$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/power/MiuiAttentionDetector;Z)V

    invoke-virtual {v0, v1}, Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;->post(Ljava/lang/Runnable;)Z

    .line 430
    return-void
.end method

.method public notifyStayOnChanged(Z)V
    .locals 0
    .param p1, "stayOn"    # Z

    .line 480
    iput-boolean p1, p0, Lcom/android/server/power/MiuiAttentionDetector;->mStayOn:Z

    .line 481
    return-void
.end method

.method public onUserActivityChanged()V
    .locals 2

    .line 466
    iget-boolean v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mAonScreenOffEnabled:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector;->isScreenCastMode()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 469
    :cond_0
    iget-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mAttentionHandler:Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;

    new-instance v1, Lcom/android/server/power/MiuiAttentionDetector$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/android/server/power/MiuiAttentionDetector$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/power/MiuiAttentionDetector;)V

    invoke-virtual {v0, v1}, Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;->post(Ljava/lang/Runnable;)Z

    .line 477
    return-void

    .line 467
    :cond_1
    :goto_0
    return-void
.end method
