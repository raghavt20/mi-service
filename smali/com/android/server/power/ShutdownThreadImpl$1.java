class com.android.server.power.ShutdownThreadImpl$1 implements android.media.MediaPlayer$OnCompletionListener {
	 /* .source "ShutdownThreadImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/power/ShutdownThreadImpl;->playShutdownMusicImpl(Ljava/lang/String;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final java.lang.Object val$actionDoneSync; //synthetic
/* # direct methods */
 com.android.server.power.ShutdownThreadImpl$1 ( ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 189 */
this.val$actionDoneSync = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onCompletion ( android.media.MediaPlayer p0 ) {
/* .locals 2 */
/* .param p1, "mp" # Landroid/media/MediaPlayer; */
/* .line 192 */
v0 = this.val$actionDoneSync;
/* monitor-enter v0 */
/* .line 193 */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
com.android.server.power.ShutdownThreadImpl .-$$Nest$sfputsIsShutdownMusicPlaying ( v1 );
/* .line 194 */
v1 = this.val$actionDoneSync;
(( java.lang.Object ) v1 ).notifyAll ( ); // invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V
/* .line 195 */
/* monitor-exit v0 */
/* .line 196 */
return;
/* .line 195 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
