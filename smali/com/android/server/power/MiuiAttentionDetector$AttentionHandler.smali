.class Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;
.super Landroid/os/Handler;
.source "MiuiAttentionDetector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/power/MiuiAttentionDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AttentionHandler"
.end annotation


# static fields
.field private static final CHECK_ATTENTION_NO_USER_ACTIVITY:I = 0x2

.field private static final CHECK_CONNECTION_EXPIRATION:I = 0x1


# instance fields
.field final synthetic this$0:Lcom/android/server/power/MiuiAttentionDetector;


# direct methods
.method constructor <init>(Lcom/android/server/power/MiuiAttentionDetector;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 527
    iput-object p1, p0, Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;->this$0:Lcom/android/server/power/MiuiAttentionDetector;

    .line 528
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 529
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .line 533
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 539
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;->this$0:Lcom/android/server/power/MiuiAttentionDetector;

    invoke-static {}, Lcom/android/server/power/MiuiAttentionDetector;->-$$Nest$sfgetTYPE_CHECK_ATTENTION()I

    move-result v1

    invoke-static {}, Lcom/android/server/power/MiuiAttentionDetector;->-$$Nest$sfgetFPS_CHECK_ATTENTION_SCREEN_OFF()I

    move-result v2

    const-string v3, "no user activity"

    const/16 v4, 0x7d0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/server/power/MiuiAttentionDetector;->checkAttention(IILjava/lang/String;I)V

    .line 543
    iget-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;->this$0:Lcom/android/server/power/MiuiAttentionDetector;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/server/power/MiuiAttentionDetector;->-$$Nest$msetLightSensorEnable(Lcom/android/server/power/MiuiAttentionDetector;Z)V

    .line 544
    goto :goto_0

    .line 536
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;->this$0:Lcom/android/server/power/MiuiAttentionDetector;

    invoke-virtual {v0}, Lcom/android/server/power/MiuiAttentionDetector;->cancelAndUnbind()V

    .line 537
    nop

    .line 548
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
