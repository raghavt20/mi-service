class com.android.server.power.UidStateHelper$1 extends android.app.IProcessObserver$Stub {
	 /* .source "UidStateHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/power/UidStateHelper; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.power.UidStateHelper this$0; //synthetic
/* # direct methods */
 com.android.server.power.UidStateHelper$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/power/UidStateHelper; */
/* .line 149 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/app/IProcessObserver$Stub;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onForegroundActivitiesChanged ( Integer p0, Integer p1, Boolean p2 ) {
/* .locals 4 */
/* .param p1, "pid" # I */
/* .param p2, "uid" # I */
/* .param p3, "foregroundActivities" # Z */
/* .line 152 */
v0 = com.android.server.power.UidStateHelper .-$$Nest$sfgetDEBUG ( );
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 153 */
	 com.android.server.power.UidStateHelper .-$$Nest$sfgetTAG ( );
	 /* new-instance v1, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v2 = "foreground changed:["; // const-string v2, "foreground changed:["
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 final String v2 = ","; // const-string v2, ","
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 final String v2 = ","; // const-string v2, ","
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).append ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
	 final String v2 = "]"; // const-string v2, "]"
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .v ( v0,v1 );
	 /* .line 155 */
} // :cond_0
v0 = this.this$0;
com.android.server.power.UidStateHelper .-$$Nest$fgetmStateLock ( v0 );
/* monitor-enter v0 */
/* .line 160 */
try { // :try_start_0
	 v1 = this.this$0;
	 com.android.server.power.UidStateHelper .-$$Nest$fgetmUidPidForeground ( v1 );
	 (( android.util.SparseArray ) v1 ).get ( p2 ); // invoke-virtual {v1, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
	 /* check-cast v1, Landroid/util/SparseBooleanArray; */
	 /* .line 161 */
	 /* .local v1, "pidForeground":Landroid/util/SparseBooleanArray; */
	 /* if-nez v1, :cond_1 */
	 /* .line 162 */
	 /* new-instance v2, Landroid/util/SparseBooleanArray; */
	 int v3 = 2; // const/4 v3, 0x2
	 /* invoke-direct {v2, v3}, Landroid/util/SparseBooleanArray;-><init>(I)V */
	 /* move-object v1, v2 */
	 /* .line 163 */
	 v2 = this.this$0;
	 com.android.server.power.UidStateHelper .-$$Nest$fgetmUidPidForeground ( v2 );
	 (( android.util.SparseArray ) v2 ).put ( p2, v1 ); // invoke-virtual {v2, p2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
	 /* .line 165 */
} // :cond_1
(( android.util.SparseBooleanArray ) v1 ).put ( p1, p3 ); // invoke-virtual {v1, p1, p3}, Landroid/util/SparseBooleanArray;->put(IZ)V
/* .line 166 */
v2 = this.this$0;
com.android.server.power.UidStateHelper .-$$Nest$mcomputeUidForegroundLocked ( v2,p2 );
/* .line 167 */
} // .end local v1 # "pidForeground":Landroid/util/SparseBooleanArray;
/* monitor-exit v0 */
/* .line 168 */
return;
/* .line 167 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void onForegroundServicesChanged ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 3 */
/* .param p1, "pid" # I */
/* .param p2, "uid" # I */
/* .param p3, "serviceTypes" # I */
/* .line 172 */
v0 = com.android.server.power.UidStateHelper .-$$Nest$sfgetDEBUG ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 173 */
com.android.server.power.UidStateHelper .-$$Nest$sfgetTAG ( );
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "foreground changed:["; // const-string v2, "foreground changed:["
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ","; // const-string v2, ","
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = "]"; // const-string v2, "]"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .v ( v0,v1 );
/* .line 177 */
} // :cond_0
return;
} // .end method
public void onProcessDied ( Integer p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "pid" # I */
/* .param p2, "uid" # I */
/* .line 181 */
v0 = com.android.server.power.UidStateHelper .-$$Nest$sfgetDEBUG ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 182 */
com.android.server.power.UidStateHelper .-$$Nest$sfgetTAG ( );
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "process died:["; // const-string v2, "process died:["
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ","; // const-string v2, ","
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = "]"; // const-string v2, "]"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .v ( v0,v1 );
/* .line 184 */
} // :cond_0
v0 = this.this$0;
com.android.server.power.UidStateHelper .-$$Nest$fgetmStateLock ( v0 );
/* monitor-enter v0 */
/* .line 186 */
try { // :try_start_0
v1 = this.this$0;
com.android.server.power.UidStateHelper .-$$Nest$fgetmUidPidForeground ( v1 );
(( android.util.SparseArray ) v1 ).get ( p2 ); // invoke-virtual {v1, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v1, Landroid/util/SparseBooleanArray; */
/* .line 187 */
/* .local v1, "pidForeground":Landroid/util/SparseBooleanArray; */
if ( v1 != null) { // if-eqz v1, :cond_1
	 /* .line 188 */
	 (( android.util.SparseBooleanArray ) v1 ).delete ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/SparseBooleanArray;->delete(I)V
	 /* .line 189 */
	 v2 = this.this$0;
	 com.android.server.power.UidStateHelper .-$$Nest$mcomputeUidForegroundLocked ( v2,p2 );
	 /* .line 191 */
} // .end local v1 # "pidForeground":Landroid/util/SparseBooleanArray;
} // :cond_1
/* monitor-exit v0 */
/* .line 192 */
return;
/* .line 191 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
