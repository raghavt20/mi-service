.class Lcom/android/server/power/NotifierInjector$1;
.super Ljava/lang/Object;
.source "NotifierInjector.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/power/NotifierInjector;->onWakefulnessInHangUp(ZI)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/power/NotifierInjector;

.field final synthetic val$hangUp:Z


# direct methods
.method constructor <init>(Lcom/android/server/power/NotifierInjector;Z)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/power/NotifierInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 83
    iput-object p1, p0, Lcom/android/server/power/NotifierInjector$1;->this$0:Lcom/android/server/power/NotifierInjector;

    iput-boolean p2, p0, Lcom/android/server/power/NotifierInjector$1;->val$hangUp:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .line 87
    iget-object v0, p0, Lcom/android/server/power/NotifierInjector$1;->this$0:Lcom/android/server/power/NotifierInjector;

    iget-boolean v1, p0, Lcom/android/server/power/NotifierInjector$1;->val$hangUp:Z

    invoke-virtual {v0, v1}, Lcom/android/server/power/NotifierInjector;->updateRotationOffState(Z)V

    .line 88
    iget-object v0, p0, Lcom/android/server/power/NotifierInjector$1;->this$0:Lcom/android/server/power/NotifierInjector;

    iget-boolean v1, p0, Lcom/android/server/power/NotifierInjector$1;->val$hangUp:Z

    invoke-virtual {v0, v1}, Lcom/android/server/power/NotifierInjector;->sendHangupBroadcast(Z)V

    .line 89
    return-void
.end method
