.class public Lcom/android/server/power/NotifierInjector;
.super Ljava/lang/Object;
.source "NotifierInjector.java"


# static fields
.field private static final RESTORE_BRIGHTNESS_BROADCAST_ACTION:Ljava/lang/String; = "miui.intent.action.RESTORE_BRIGHTNESS"

.field private static final RESTORE_BRIGHTNESS_BROADCAST_PERMISSION:Ljava/lang/String; = "miui.permission.restore_brightness"

.field private static final TAG:Ljava/lang/String; = "PowerManagerNotifierInjector"


# instance fields
.field private mAccelerometerRotationEnabled:Z

.field private mBeingHangUp:Z

.field private final mContext:Landroid/content/Context;

.field private mDisableRotationDueToHangUp:Z

.field private final mHandler:Landroid/os/Handler;

.field private final mInputManagerInternal:Lcom/android/server/input/InputManagerInternal;

.field private mWindowManagerService:Landroid/view/IWindowManager;


# direct methods
.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/power/NotifierInjector;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/power/NotifierInjector;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/android/server/power/NotifierInjector;->mContext:Landroid/content/Context;

    .line 38
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/power/NotifierInjector;->mHandler:Landroid/os/Handler;

    .line 39
    const-class v0, Lcom/android/server/input/InputManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/input/InputManagerInternal;

    iput-object v0, p0, Lcom/android/server/power/NotifierInjector;->mInputManagerInternal:Lcom/android/server/input/InputManagerInternal;

    .line 40
    nop

    .line 41
    const-string/jumbo v0, "window"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 40
    invoke-static {v0}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/power/NotifierInjector;->mWindowManagerService:Landroid/view/IWindowManager;

    .line 43
    invoke-virtual {p0}, Lcom/android/server/power/NotifierInjector;->updateAccelerometerRotationLocked()V

    .line 44
    return-void
.end method


# virtual methods
.method protected dump(Ljava/io/PrintWriter;)V
    .locals 2
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 115
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mBeingHangUp: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/power/NotifierInjector;->mBeingHangUp:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 116
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mDisableRotationDueToHangUp: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/power/NotifierInjector;->mDisableRotationDueToHangUp:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 117
    return-void
.end method

.method protected onWakefulnessInHangUp(ZI)V
    .locals 3
    .param p1, "hangUp"    # Z
    .param p2, "wakefulness"    # I

    .line 77
    iget-boolean v0, p0, Lcom/android/server/power/NotifierInjector;->mBeingHangUp:Z

    if-eq v0, p1, :cond_2

    .line 78
    iput-boolean p1, p0, Lcom/android/server/power/NotifierInjector;->mBeingHangUp:Z

    .line 79
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onWakefulnessInHangUp: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 80
    iget-boolean v1, p0, Lcom/android/server/power/NotifierInjector;->mBeingHangUp:Z

    if-eqz v1, :cond_0

    const-string v1, "disable "

    goto :goto_0

    :cond_0
    const-string v1, "enable "

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "input event."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 79
    const-string v1, "PowerManagerNotifierInjector"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    invoke-static {p2}, Landroid/os/PowerManagerInternal;->isInteractive(I)Z

    move-result v0

    .line 82
    .local v0, "interactive":Z
    iget-object v1, p0, Lcom/android/server/power/NotifierInjector;->mInputManagerInternal:Lcom/android/server/input/InputManagerInternal;

    iget-boolean v2, p0, Lcom/android/server/power/NotifierInjector;->mBeingHangUp:Z

    if-nez v2, :cond_1

    if-eqz v0, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v1, v2}, Lcom/android/server/input/InputManagerInternal;->setInteractive(Z)V

    .line 83
    iget-object v1, p0, Lcom/android/server/power/NotifierInjector;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/android/server/power/NotifierInjector$1;

    invoke-direct {v2, p0, p1}, Lcom/android/server/power/NotifierInjector$1;-><init>(Lcom/android/server/power/NotifierInjector;Z)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 92
    .end local v0    # "interactive":Z
    :cond_2
    return-void
.end method

.method protected sendBroadcastRestoreBrightness()V
    .locals 2

    .line 103
    iget-object v0, p0, Lcom/android/server/power/NotifierInjector;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/power/NotifierInjector$2;

    invoke-direct {v1, p0}, Lcom/android/server/power/NotifierInjector$2;-><init>(Lcom/android/server/power/NotifierInjector;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 111
    return-void
.end method

.method protected sendHangupBroadcast(Z)V
    .locals 5
    .param p1, "hangup"    # Z

    .line 95
    new-instance v0, Landroid/content/Intent;

    const-string v1, "miui.intent.action.HANG_UP_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 96
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x50000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 97
    const-string v1, "hang_up_enable"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 98
    iget-object v1, p0, Lcom/android/server/power/NotifierInjector;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const-string v3, "com.miui.permission.HANG_UP_CHANGED"

    const/4 v4, 0x0

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 100
    return-void
.end method

.method protected updateAccelerometerRotationLocked()V
    .locals 4

    .line 66
    iget-object v0, p0, Lcom/android/server/power/NotifierInjector;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, -0x2

    const-string v2, "accelerometer_rotation"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    move v3, v1

    :cond_0
    iput-boolean v3, p0, Lcom/android/server/power/NotifierInjector;->mAccelerometerRotationEnabled:Z

    .line 69
    return-void
.end method

.method protected updateRotationOffState(Z)V
    .locals 4
    .param p1, "hangUp"    # Z

    .line 49
    const-string v0, "PowerManagerNotifierInjector"

    if-eqz p1, :cond_0

    :try_start_0
    iget-boolean v1, p0, Lcom/android/server/power/NotifierInjector;->mAccelerometerRotationEnabled:Z

    if-eqz v1, :cond_0

    .line 51
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/server/power/NotifierInjector;->mDisableRotationDueToHangUp:Z

    .line 52
    iget-object v1, p0, Lcom/android/server/power/NotifierInjector;->mWindowManagerService:Landroid/view/IWindowManager;

    const/4 v2, -0x1

    invoke-interface {v1, v2}, Landroid/view/IWindowManager;->freezeRotation(I)V

    .line 53
    const-string/jumbo v1, "updateRotationOffState: disable accelerometer rotation."

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 60
    :catch_0
    move-exception v1

    goto :goto_0

    .line 54
    :cond_0
    if-nez p1, :cond_1

    iget-boolean v1, p0, Lcom/android/server/power/NotifierInjector;->mDisableRotationDueToHangUp:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/android/server/power/NotifierInjector;->mAccelerometerRotationEnabled:Z

    if-nez v1, :cond_1

    .line 56
    const-string/jumbo v1, "updateRotationOffState: enable accelerometer rotation."

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/server/power/NotifierInjector;->mDisableRotationDueToHangUp:Z

    .line 58
    iget-object v1, p0, Lcom/android/server/power/NotifierInjector;->mWindowManagerService:Landroid/view/IWindowManager;

    invoke-interface {v1}, Landroid/view/IWindowManager;->thawRotation()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 61
    .local v1, "e":Landroid/os/RemoteException;
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "updateRotationOffState: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 62
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_1
    :goto_1
    nop

    .line 63
    :goto_2
    return-void
.end method
