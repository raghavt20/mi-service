.class Lcom/android/server/power/ShutdownThreadImpl$1;
.super Ljava/lang/Object;
.source "ShutdownThreadImpl.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/power/ShutdownThreadImpl;->playShutdownMusicImpl(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic val$actionDoneSync:Ljava/lang/Object;


# direct methods
.method constructor <init>(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 189
    iput-object p1, p0, Lcom/android/server/power/ShutdownThreadImpl$1;->val$actionDoneSync:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 2
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .line 192
    iget-object v0, p0, Lcom/android/server/power/ShutdownThreadImpl$1;->val$actionDoneSync:Ljava/lang/Object;

    monitor-enter v0

    .line 193
    const/4 v1, 0x0

    :try_start_0
    invoke-static {v1}, Lcom/android/server/power/ShutdownThreadImpl;->-$$Nest$sfputsIsShutdownMusicPlaying(Z)V

    .line 194
    iget-object v1, p0, Lcom/android/server/power/ShutdownThreadImpl$1;->val$actionDoneSync:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 195
    monitor-exit v0

    .line 196
    return-void

    .line 195
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
