public class com.android.server.power.MiuiAttentionDetector {
	 /* .source "MiuiAttentionDetector.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;, */
	 /* Lcom/android/server/power/MiuiAttentionDetector$SettingsObserver;, */
	 /* Lcom/android/server/power/MiuiAttentionDetector$UserSwitchObserver; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer ALWAYS_CHECK_ATTENTION_TIMEOUT;
private static final Float AON_CHECK_INCORRECT_LUX;
public static final Boolean AON_SCREEN_OFF_SUPPORTED;
public static final Boolean AON_SCREEN_ON_SUPPORTED;
public static final java.lang.String AON_SERVICE_CLASS;
public static final java.lang.String AON_SERVICE_PACKAGE;
private static final Long CONNECTION_TTL_MILLIS;
private static final Integer DIM_DURATION_CHECK_ATTENTION_TIMEOUT;
private static final Integer EXTERNAL_DISPLAY_CONNECTED;
private static final Integer FPS_CHECK_ATTENTION_SCREEN_OFF;
private static final Integer FPS_CHECK_ATTENTION_SCREEN_ON;
private static final Long NO_ACTIVITE_CHECK_ATTENTION_MILLIS;
private static final Integer NO_ACTIVITY_CHECK_ATTENTION_TIMEOUT;
public static final java.lang.String REASON_DIM_DURATION_CHECK_ATTENTION;
public static final java.lang.String REASON_INTERACTIVE_CHANGE_CHECK_ATTENTION;
public static final java.lang.String REASON_NO_USER_ACTIVITY_CHECK_ATTENTION;
private static final Long SERVICE_BIND_AWAIT_MILLIS;
private static final Integer SYNERGY_MODE_ON;
private static final java.lang.String TAG;
private static final Integer TYPE_CHECK_ATTENTION;
private static final Integer TYPE_CHECK_ATTENTION_EYE;
private static final Integer TYPE_CHECK_ATTENTION_FACE;
/* # instance fields */
private Boolean mAonScreenOffEnabled;
private Boolean mAonScreenOnEnabled;
private com.xiaomi.aon.IMiAON mAonService;
private final com.android.server.power.MiuiAttentionDetector$AttentionHandler mAttentionHandler;
private final com.xiaomi.aon.IMiAONListener mAttentionListener;
private Integer mCheckTimeout;
private android.content.ComponentName mComponentName;
private final android.content.ServiceConnection mConnection;
private final android.content.Context mContext;
private Boolean mExternalDisplayConnected;
private Integer mFps;
private Boolean mInteractive;
private Boolean mIsChecked;
private android.hardware.Sensor mLightSensor;
private Boolean mLightSensorEnabled;
private final android.hardware.SensorEventListener mLightSensorListener;
private Float mLux;
private com.android.server.policy.WindowManagerPolicy mPolicy;
private android.os.PowerManager mPowerManager;
private final com.android.server.power.PowerManagerServiceImpl mPowerManagerServiceImpl;
private java.lang.String mReason;
private Boolean mScreenProjectInScreen;
private android.hardware.SensorManager mSensorManager;
private java.util.concurrent.CountDownLatch mServiceBindingLatch;
private com.android.server.power.MiuiAttentionDetector$SettingsObserver mSettingsObserver;
private Long mStartTimeStamp;
private Boolean mStayOn;
private Boolean mSynergyModeEnable;
private Integer mType;
/* # direct methods */
public static void $r8$lambda$dNnNsyuIYa1on_Ynz2xhkdT6lpI ( com.android.server.power.MiuiAttentionDetector p0 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector;->lambda$onUserActivityChanged$2()V */
	 return;
} // .end method
public static void $r8$lambda$f_REzlYFnWzsw3f6ZxB3O4BhEhs ( com.android.server.power.MiuiAttentionDetector p0 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector;->lambda$handleAttentionResult$0()V */
	 return;
} // .end method
public static void $r8$lambda$lX-hb_8T4lQgeJF_5KXXQgKTFbc ( com.android.server.power.MiuiAttentionDetector p0, Boolean p1 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/power/MiuiAttentionDetector;->lambda$notifyInteractiveChange$1(Z)V */
	 return;
} // .end method
static com.android.server.power.MiuiAttentionDetector$AttentionHandler -$$Nest$fgetmAttentionHandler ( com.android.server.power.MiuiAttentionDetector p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mAttentionHandler;
} // .end method
static java.util.concurrent.CountDownLatch -$$Nest$fgetmServiceBindingLatch ( com.android.server.power.MiuiAttentionDetector p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mServiceBindingLatch;
} // .end method
static void -$$Nest$fputmAonService ( com.android.server.power.MiuiAttentionDetector p0, com.xiaomi.aon.IMiAON p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 this.mAonService = p1;
	 return;
} // .end method
static void -$$Nest$fputmIsChecked ( com.android.server.power.MiuiAttentionDetector p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/power/MiuiAttentionDetector;->mIsChecked:Z */
	 return;
} // .end method
static void -$$Nest$fputmLux ( com.android.server.power.MiuiAttentionDetector p0, Float p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput p1, p0, Lcom/android/server/power/MiuiAttentionDetector;->mLux:F */
	 return;
} // .end method
static void -$$Nest$fputmServiceBindingLatch ( com.android.server.power.MiuiAttentionDetector p0, java.util.concurrent.CountDownLatch p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 this.mServiceBindingLatch = p1;
	 return;
} // .end method
static void -$$Nest$mhandleAttentionResult ( com.android.server.power.MiuiAttentionDetector p0, Integer p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/power/MiuiAttentionDetector;->handleAttentionResult(I)V */
	 return;
} // .end method
static void -$$Nest$mhandlePendingCallback ( com.android.server.power.MiuiAttentionDetector p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector;->handlePendingCallback()V */
	 return;
} // .end method
static void -$$Nest$msetLightSensorEnable ( com.android.server.power.MiuiAttentionDetector p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/power/MiuiAttentionDetector;->setLightSensorEnable(Z)V */
	 return;
} // .end method
static void -$$Nest$mupdateAonScreenOffConfig ( com.android.server.power.MiuiAttentionDetector p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector;->updateAonScreenOffConfig()V */
	 return;
} // .end method
static void -$$Nest$mupdateAonScreenOnConfig ( com.android.server.power.MiuiAttentionDetector p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector;->updateAonScreenOnConfig()V */
	 return;
} // .end method
static void -$$Nest$mupdateConfigState ( com.android.server.power.MiuiAttentionDetector p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector;->updateConfigState()V */
	 return;
} // .end method
static void -$$Nest$mupdateExternalDisplayConnectConfig ( com.android.server.power.MiuiAttentionDetector p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector;->updateExternalDisplayConnectConfig()V */
	 return;
} // .end method
static void -$$Nest$mupdateScreenProjectConfig ( com.android.server.power.MiuiAttentionDetector p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector;->updateScreenProjectConfig()V */
	 return;
} // .end method
static void -$$Nest$mupdateSynergyModeConfig ( com.android.server.power.MiuiAttentionDetector p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector;->updateSynergyModeConfig()V */
	 return;
} // .end method
static Integer -$$Nest$sfgetFPS_CHECK_ATTENTION_SCREEN_OFF ( ) { //bridge//synthethic
	 /* .locals 1 */
} // .end method
static java.lang.String -$$Nest$sfgetTAG ( ) { //bridge//synthethic
	 /* .locals 1 */
	 v0 = com.android.server.power.MiuiAttentionDetector.TAG;
} // .end method
static Integer -$$Nest$sfgetTYPE_CHECK_ATTENTION ( ) { //bridge//synthethic
	 /* .locals 1 */
} // .end method
static com.android.server.power.MiuiAttentionDetector ( ) {
	 /* .locals 4 */
	 /* .line 56 */
	 /* const-class v0, Lcom/android/server/power/MiuiAttentionDetector; */
	 (( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
	 /* .line 107 */
	 /* nop */
	 /* .line 108 */
	 final String v0 = "aon_screen_off_fps"; // const-string v0, "aon_screen_off_fps"
	 /* const/16 v1, 0xa */
	 v0 = 	 miui.util.FeatureParser .getInteger ( v0,v1 );
	 /* .line 109 */
	 /* nop */
	 /* .line 110 */
	 final String v0 = "aon_screen_on_fps"; // const-string v0, "aon_screen_on_fps"
	 /* const/16 v1, 0xf */
	 v0 = 	 miui.util.FeatureParser .getInteger ( v0,v1 );
	 /* .line 123 */
	 /* nop */
	 /* .line 124 */
	 final String v0 = "config_aon_check_incorrect_lux"; // const-string v0, "config_aon_check_incorrect_lux"
	 /* const/high16 v1, 0x40c00000 # 6.0f */
	 miui.util.FeatureParser .getFloat ( v0,v1 );
	 v0 = 	 (( java.lang.Float ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
	 /* .line 126 */
	 /* nop */
	 /* .line 127 */
	 final String v0 = "check_attention_type"; // const-string v0, "check_attention_type"
	 int v1 = 1; // const/4 v1, 0x1
	 v0 = 	 miui.util.FeatureParser .getInteger ( v0,v1 );
	 /* .line 129 */
	 /* nop */
	 /* .line 130 */
	 final String v0 = "config_aon_proximity_available"; // const-string v0, "config_aon_proximity_available"
	 int v2 = 0; // const/4 v2, 0x0
	 v3 = 	 miui.util.FeatureParser .getBoolean ( v0,v2 );
	 /* if-nez v3, :cond_1 */
	 /* .line 131 */
	 final String v3 = "config_aon_screen_on_available"; // const-string v3, "config_aon_screen_on_available"
	 v3 = 	 miui.util.FeatureParser .getBoolean ( v3,v2 );
	 if ( v3 != null) { // if-eqz v3, :cond_0
	 } // :cond_0
	 /* move v3, v2 */
} // :cond_1
} // :goto_0
/* move v3, v1 */
} // :goto_1
com.android.server.power.MiuiAttentionDetector.AON_SCREEN_ON_SUPPORTED = (v3!= 0);
/* .line 133 */
/* nop */
/* .line 134 */
v0 = miui.util.FeatureParser .getBoolean ( v0,v2 );
/* if-nez v0, :cond_3 */
/* .line 135 */
final String v0 = "config_aon_screen_off_available"; // const-string v0, "config_aon_screen_off_available"
v0 = miui.util.FeatureParser .getBoolean ( v0,v2 );
if ( v0 != null) { // if-eqz v0, :cond_2
} // :cond_2
/* move v1, v2 */
} // :cond_3
} // :goto_2
/* nop */
} // :goto_3
com.android.server.power.MiuiAttentionDetector.AON_SCREEN_OFF_SUPPORTED = (v1!= 0);
/* .line 133 */
return;
} // .end method
public com.android.server.power.MiuiAttentionDetector ( ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "powerManager" # Landroid/os/PowerManager; */
/* .param p3, "impl" # Lcom/android/server/power/PowerManagerServiceImpl; */
/* .param p4, "policy" # Lcom/android/server/policy/WindowManagerPolicy; */
/* .line 137 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 69 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mInteractive:Z */
/* .line 160 */
/* new-instance v1, Lcom/android/server/power/MiuiAttentionDetector$1; */
/* invoke-direct {v1, p0}, Lcom/android/server/power/MiuiAttentionDetector$1;-><init>(Lcom/android/server/power/MiuiAttentionDetector;)V */
this.mLightSensorListener = v1;
/* .line 172 */
/* new-instance v1, Lcom/android/server/power/MiuiAttentionDetector$2; */
/* invoke-direct {v1, p0}, Lcom/android/server/power/MiuiAttentionDetector$2;-><init>(Lcom/android/server/power/MiuiAttentionDetector;)V */
this.mAttentionListener = v1;
/* .line 250 */
/* new-instance v1, Lcom/android/server/power/MiuiAttentionDetector$3; */
/* invoke-direct {v1, p0}, Lcom/android/server/power/MiuiAttentionDetector$3;-><init>(Lcom/android/server/power/MiuiAttentionDetector;)V */
this.mConnection = v1;
/* .line 138 */
this.mContext = p1;
/* .line 139 */
this.mPowerManagerServiceImpl = p3;
/* .line 140 */
this.mPolicy = p4;
/* .line 141 */
/* new-instance v1, Lcom/android/server/ServiceThread; */
int v2 = -4; // const/4 v2, -0x4
int v3 = 0; // const/4 v3, 0x0
final String v4 = "MiuiAttentionDetector"; // const-string v4, "MiuiAttentionDetector"
/* invoke-direct {v1, v4, v2, v3}, Lcom/android/server/ServiceThread;-><init>(Ljava/lang/String;IZ)V */
/* .line 143 */
/* .local v1, "handlerThread":Lcom/android/server/ServiceThread; */
(( com.android.server.ServiceThread ) v1 ).start ( ); // invoke-virtual {v1}, Lcom/android/server/ServiceThread;->start()V
/* .line 144 */
/* new-instance v2, Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler; */
(( com.android.server.ServiceThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Lcom/android/server/ServiceThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v2, p0, v3}, Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;-><init>(Lcom/android/server/power/MiuiAttentionDetector;Landroid/os/Looper;)V */
this.mAttentionHandler = v2;
/* .line 145 */
this.mPowerManager = p2;
/* .line 146 */
/* new-instance v3, Ljava/util/concurrent/CountDownLatch; */
/* invoke-direct {v3, v0}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V */
this.mServiceBindingLatch = v3;
/* .line 147 */
/* new-instance v0, Lcom/android/server/power/MiuiAttentionDetector$SettingsObserver; */
/* invoke-direct {v0, p0, v2}, Lcom/android/server/power/MiuiAttentionDetector$SettingsObserver;-><init>(Lcom/android/server/power/MiuiAttentionDetector;Landroid/os/Handler;)V */
this.mSettingsObserver = v0;
/* .line 148 */
/* const-string/jumbo v0, "sensor" */
(( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/hardware/SensorManager; */
this.mSensorManager = v0;
/* .line 149 */
int v2 = 5; // const/4 v2, 0x5
(( android.hardware.SensorManager ) v0 ).getDefaultSensor ( v2 ); // invoke-virtual {v0, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;
this.mLightSensor = v0;
/* .line 150 */
/* invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector;->updateConfigState()V */
/* .line 151 */
/* invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector;->registerContentObserver()V */
/* .line 153 */
try { // :try_start_0
/* new-instance v0, Lcom/android/server/power/MiuiAttentionDetector$UserSwitchObserver; */
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {v0, p0, v2}, Lcom/android/server/power/MiuiAttentionDetector$UserSwitchObserver;-><init>(Lcom/android/server/power/MiuiAttentionDetector;Lcom/android/server/power/MiuiAttentionDetector$UserSwitchObserver-IA;)V */
/* .line 154 */
/* .local v0, "observer":Lcom/android/server/power/MiuiAttentionDetector$UserSwitchObserver; */
android.app.ActivityManager .getService ( );
v3 = com.android.server.power.MiuiAttentionDetector.TAG;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 157 */
} // .end local v0 # "observer":Lcom/android/server/power/MiuiAttentionDetector$UserSwitchObserver;
/* .line 155 */
/* :catch_0 */
/* move-exception v0 */
/* .line 158 */
} // :goto_0
return;
} // .end method
private void awaitServiceBinding ( ) {
/* .locals 4 */
/* .line 347 */
try { // :try_start_0
v0 = this.mServiceBindingLatch;
v1 = java.util.concurrent.TimeUnit.MILLISECONDS;
/* const-wide/16 v2, 0x3e8 */
(( java.util.concurrent.CountDownLatch ) v0 ).await ( v2, v3, v1 ); // invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
/* :try_end_0 */
/* .catch Ljava/lang/InterruptedException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 350 */
/* .line 348 */
/* :catch_0 */
/* move-exception v0 */
/* .line 349 */
/* .local v0, "e":Ljava/lang/InterruptedException; */
v1 = com.android.server.power.MiuiAttentionDetector.TAG;
final String v2 = "Interrupted while waiting to bind Attention Service."; // const-string v2, "Interrupted while waiting to bind Attention Service."
android.util.Slog .e ( v1,v2,v0 );
/* .line 351 */
} // .end local v0 # "e":Ljava/lang/InterruptedException;
} // :goto_0
return;
} // .end method
private void bindAonService ( ) {
/* .locals 5 */
/* .line 323 */
v0 = this.mAonService;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 324 */
return;
/* .line 326 */
} // :cond_0
v0 = this.mComponentName;
final String v1 = "com.xiaomi.aon.AONService"; // const-string v1, "com.xiaomi.aon.AONService"
/* if-nez v0, :cond_1 */
/* .line 327 */
/* new-instance v0, Landroid/content/ComponentName; */
final String v2 = "com.xiaomi.aon"; // const-string v2, "com.xiaomi.aon"
/* invoke-direct {v0, v2, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
this.mComponentName = v0;
/* .line 329 */
} // :cond_1
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
v1 = this.mComponentName;
/* .line 330 */
(( android.content.Intent ) v0 ).setComponent ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 331 */
/* .local v0, "serviceIntent":Landroid/content/Intent; */
v1 = this.mContext;
v2 = this.mConnection;
/* const v3, 0x4000001 */
v4 = android.os.UserHandle.CURRENT;
(( android.content.Context ) v1 ).bindServiceAsUser ( v0, v2, v3, v4 ); // invoke-virtual {v1, v0, v2, v3, v4}, Landroid/content/Context;->bindServiceAsUser(Landroid/content/Intent;Landroid/content/ServiceConnection;ILandroid/os/UserHandle;)Z
/* .line 333 */
return;
} // .end method
private void freeIfInactive ( ) {
/* .locals 4 */
/* .line 408 */
v0 = this.mAttentionHandler;
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.power.MiuiAttentionDetector$AttentionHandler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;->removeMessages(I)V
/* .line 410 */
v0 = this.mAttentionHandler;
/* const-wide/32 v2, 0xea60 */
(( com.android.server.power.MiuiAttentionDetector$AttentionHandler ) v0 ).sendEmptyMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;->sendEmptyMessageDelayed(IJ)Z
/* .line 412 */
return;
} // .end method
private void handleAttentionResult ( Integer p0 ) {
/* .locals 10 */
/* .param p1, "result" # I */
/* .line 206 */
v0 = /* invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector;->isScreenCastMode()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 207 */
v0 = com.android.server.power.MiuiAttentionDetector.TAG;
final String v1 = "ignore aon event in hangup mode"; // const-string v1, "ignore aon event in hangup mode"
android.util.Slog .i ( v0,v1 );
/* .line 208 */
return;
/* .line 210 */
} // :cond_0
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 211 */
/* .local v0, "time":J */
final String v2 = ""; // const-string v2, ""
/* .line 212 */
/* .local v2, "action":Ljava/lang/String; */
int v3 = 0; // const/4 v3, 0x0
/* .line 213 */
/* .local v3, "screenWakeLock":Z */
/* iget-boolean v4, p0, Lcom/android/server/power/MiuiAttentionDetector;->mInteractive:Z */
int v5 = 1; // const/4 v5, 0x1
int v6 = 0; // const/4 v6, 0x0
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 214 */
v4 = this.mPowerManagerServiceImpl;
/* .line 215 */
v3 = (( com.android.server.power.PowerManagerServiceImpl ) v4 ).isAcquiredScreenBrightWakeLock ( v6 ); // invoke-virtual {v4, v6}, Lcom/android/server/power/PowerManagerServiceImpl;->isAcquiredScreenBrightWakeLock(I)Z
/* .line 218 */
/* if-nez p1, :cond_1 */
/* iget-boolean v4, p0, Lcom/android/server/power/MiuiAttentionDetector;->mLightSensorEnabled:Z */
if ( v4 != null) { // if-eqz v4, :cond_1
/* iget v4, p0, Lcom/android/server/power/MiuiAttentionDetector;->mLux:F */
/* cmpg-float v4, v4, v7 */
/* if-gez v4, :cond_1 */
/* .line 220 */
final String v2 = "low lux,ignore"; // const-string v2, "low lux,ignore"
/* .line 221 */
(( com.android.server.power.MiuiAttentionDetector ) p0 ).onUserActivityChanged ( ); // invoke-virtual {p0}, Lcom/android/server/power/MiuiAttentionDetector;->onUserActivityChanged()V
/* .line 222 */
} // :cond_1
/* if-nez v3, :cond_2 */
/* iget-boolean v4, p0, Lcom/android/server/power/MiuiAttentionDetector;->mStayOn:Z */
/* if-nez v4, :cond_2 */
/* if-nez p1, :cond_2 */
/* .line 223 */
final String v2 = "dim"; // const-string v2, "dim"
/* .line 224 */
v4 = this.mPowerManagerServiceImpl;
(( com.android.server.power.PowerManagerServiceImpl ) v4 ).requestDimmingRightNowDueToAttention ( ); // invoke-virtual {v4}, Lcom/android/server/power/PowerManagerServiceImpl;->requestDimmingRightNowDueToAttention()V
/* .line 225 */
v4 = this.mAttentionHandler;
/* new-instance v7, Lcom/android/server/power/MiuiAttentionDetector$$ExternalSyntheticLambda1; */
/* invoke-direct {v7, p0}, Lcom/android/server/power/MiuiAttentionDetector$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/power/MiuiAttentionDetector;)V */
(( com.android.server.power.MiuiAttentionDetector$AttentionHandler ) v4 ).post ( v7 ); // invoke-virtual {v4, v7}, Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;->post(Ljava/lang/Runnable;)Z
/* .line 234 */
} // :cond_2
/* const-string/jumbo v2, "userActivity" */
/* .line 235 */
v4 = this.mPowerManager;
(( android.os.PowerManager ) v4 ).userActivity ( v0, v1, v6, v6 ); // invoke-virtual {v4, v0, v1, v6, v6}, Landroid/os/PowerManager;->userActivity(JII)V
/* .line 237 */
} // :cond_3
/* if-ne p1, v5, :cond_4 */
/* .line 238 */
/* const-string/jumbo v2, "wakeUp" */
/* .line 239 */
v4 = this.mPowerManager;
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v7 */
final String v9 = "aon"; // const-string v9, "aon"
(( android.os.PowerManager ) v4 ).wakeUp ( v7, v8, v6, v9 ); // invoke-virtual {v4, v7, v8, v6, v9}, Landroid/os/PowerManager;->wakeUp(JILjava/lang/String;)V
/* .line 242 */
} // :cond_4
} // :goto_0
v4 = android.text.TextUtils .isEmpty ( v2 );
/* if-nez v4, :cond_6 */
/* .line 243 */
v4 = this.mPowerManagerServiceImpl;
/* iget-boolean v7, p0, Lcom/android/server/power/MiuiAttentionDetector;->mInteractive:Z */
/* if-ne p1, v5, :cond_5 */
} // :cond_5
/* move v5, v6 */
} // :goto_1
/* iget-wide v8, p0, Lcom/android/server/power/MiuiAttentionDetector;->mStartTimeStamp:J */
(( com.android.server.power.PowerManagerServiceImpl ) v4 ).notifyAonScreenOnOffEvent ( v7, v5, v8, v9 ); // invoke-virtual {v4, v7, v5, v8, v9}, Lcom/android/server/power/PowerManagerServiceImpl;->notifyAonScreenOnOffEvent(ZZJ)V
/* .line 246 */
} // :cond_6
v4 = com.android.server.power.MiuiAttentionDetector.TAG;
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "onSuccess: mInteractive:"; // const-string v6, "onSuccess: mInteractive:"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v6, p0, Lcom/android/server/power/MiuiAttentionDetector;->mInteractive:Z */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v6 = " screenWakelock:"; // const-string v6, " screenWakelock:"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v6 = " result:"; // const-string v6, " result:"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( p1 ); // invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = " action:"; // const-string v6, " action:"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = " lux:"; // const-string v6, " lux:"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v6, p0, Lcom/android/server/power/MiuiAttentionDetector;->mLux:F */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v4,v5 );
/* .line 248 */
return;
} // .end method
private void handlePendingCallback ( ) {
/* .locals 4 */
/* .line 292 */
/* iget-boolean v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mIsChecked:Z */
/* if-nez v0, :cond_0 */
/* .line 293 */
/* iget v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mType:I */
/* iget v1, p0, Lcom/android/server/power/MiuiAttentionDetector;->mFps:I */
v2 = this.mReason;
/* iget v3, p0, Lcom/android/server/power/MiuiAttentionDetector;->mCheckTimeout:I */
(( com.android.server.power.MiuiAttentionDetector ) p0 ).checkAttention ( v0, v1, v2, v3 ); // invoke-virtual {p0, v0, v1, v2, v3}, Lcom/android/server/power/MiuiAttentionDetector;->checkAttention(IILjava/lang/String;I)V
/* .line 295 */
} // :cond_0
return;
} // .end method
private Boolean isScreenCastMode ( ) {
/* .locals 1 */
/* .line 484 */
/* iget-boolean v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mScreenProjectInScreen:Z */
/* if-nez v0, :cond_1 */
/* iget-boolean v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mSynergyModeEnable:Z */
/* if-nez v0, :cond_1 */
/* iget-boolean v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mExternalDisplayConnected:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
private void lambda$handleAttentionResult$0 ( ) { //synthethic
/* .locals 4 */
/* .line 227 */
(( com.android.server.power.MiuiAttentionDetector ) p0 ).cancelAttentionCheck ( ); // invoke-virtual {p0}, Lcom/android/server/power/MiuiAttentionDetector;->cancelAttentionCheck()V
/* .line 228 */
final String v2 = "dim duration"; // const-string v2, "dim duration"
/* const/16 v3, 0x4e20 */
(( com.android.server.power.MiuiAttentionDetector ) p0 ).checkAttention ( v0, v1, v2, v3 ); // invoke-virtual {p0, v0, v1, v2, v3}, Lcom/android/server/power/MiuiAttentionDetector;->checkAttention(IILjava/lang/String;I)V
/* .line 232 */
return;
} // .end method
private void lambda$notifyInteractiveChange$1 ( Boolean p0 ) { //synthethic
/* .locals 4 */
/* .param p1, "interactive" # Z */
/* .line 419 */
/* iget-boolean v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mInteractive:Z */
/* if-eq v0, p1, :cond_2 */
/* .line 420 */
/* iput-boolean p1, p0, Lcom/android/server/power/MiuiAttentionDetector;->mInteractive:Z */
/* .line 421 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, v0}, Lcom/android/server/power/MiuiAttentionDetector;->setLightSensorEnable(Z)V */
/* .line 423 */
/* iget-boolean v1, p0, Lcom/android/server/power/MiuiAttentionDetector;->mInteractive:Z */
int v2 = 1; // const/4 v2, 0x1
/* if-nez v1, :cond_0 */
/* .line 424 */
v1 = this.mAttentionHandler;
(( com.android.server.power.MiuiAttentionDetector$AttentionHandler ) v1 ).removeMessages ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;->removeMessages(I)V
/* .line 425 */
v1 = this.mAttentionHandler;
int v3 = 2; // const/4 v3, 0x2
(( com.android.server.power.MiuiAttentionDetector$AttentionHandler ) v1 ).removeMessages ( v3 ); // invoke-virtual {v1, v3}, Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;->removeMessages(I)V
/* .line 427 */
} // :cond_0
/* if-nez p1, :cond_1 */
/* iget-boolean v1, p0, Lcom/android/server/power/MiuiAttentionDetector;->mAonScreenOnEnabled:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* move v0, v2 */
} // :cond_1
/* invoke-direct {p0, v0}, Lcom/android/server/power/MiuiAttentionDetector;->registerAttentionListenerIfNeeded(Z)V */
/* .line 429 */
} // :cond_2
return;
} // .end method
private void lambda$onUserActivityChanged$2 ( ) { //synthethic
/* .locals 4 */
/* .line 470 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, v0}, Lcom/android/server/power/MiuiAttentionDetector;->setLightSensorEnable(Z)V */
/* .line 471 */
(( com.android.server.power.MiuiAttentionDetector ) p0 ).cancelAttentionCheck ( ); // invoke-virtual {p0}, Lcom/android/server/power/MiuiAttentionDetector;->cancelAttentionCheck()V
/* .line 472 */
v0 = this.mAttentionHandler;
int v1 = 2; // const/4 v1, 0x2
(( com.android.server.power.MiuiAttentionDetector$AttentionHandler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;->removeMessages(I)V
/* .line 473 */
v0 = this.mAttentionHandler;
/* const-wide/16 v2, 0x1f40 */
(( com.android.server.power.MiuiAttentionDetector$AttentionHandler ) v0 ).sendEmptyMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;->sendEmptyMessageDelayed(IJ)Z
/* .line 476 */
return;
} // .end method
private void registerAttentionListenerIfNeeded ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "register" # Z */
/* .line 446 */
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 448 */
/* iget-boolean v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mIsChecked:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 449 */
(( com.android.server.power.MiuiAttentionDetector ) p0 ).cancelAttentionCheck ( ); // invoke-virtual {p0}, Lcom/android/server/power/MiuiAttentionDetector;->cancelAttentionCheck()V
/* .line 451 */
} // :cond_0
final String v2 = "interactive change"; // const-string v2, "interactive change"
/* const v3, 0x927c0 */
(( com.android.server.power.MiuiAttentionDetector ) p0 ).checkAttention ( v0, v1, v2, v3 ); // invoke-virtual {p0, v0, v1, v2, v3}, Lcom/android/server/power/MiuiAttentionDetector;->checkAttention(IILjava/lang/String;I)V
/* .line 456 */
} // :cond_1
(( com.android.server.power.MiuiAttentionDetector ) p0 ).cancelAndUnbind ( ); // invoke-virtual {p0}, Lcom/android/server/power/MiuiAttentionDetector;->cancelAndUnbind()V
/* .line 458 */
} // :goto_0
return;
} // .end method
private void registerContentObserver ( ) {
/* .locals 5 */
/* .line 306 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 307 */
/* .local v0, "cr":Landroid/content/ContentResolver; */
final String v1 = "miui_people_near_screen_on"; // const-string v1, "miui_people_near_screen_on"
android.provider.Settings$Secure .getUriFor ( v1 );
v2 = this.mSettingsObserver;
int v3 = 0; // const/4 v3, 0x0
int v4 = -1; // const/4 v4, -0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 309 */
final String v1 = "gaze_lock_screen_setting"; // const-string v1, "gaze_lock_screen_setting"
android.provider.Settings$Secure .getUriFor ( v1 );
v2 = this.mSettingsObserver;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 311 */
/* const-string/jumbo v1, "screen_project_in_screening" */
android.provider.Settings$Secure .getUriFor ( v1 );
v2 = this.mSettingsObserver;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 314 */
/* const-string/jumbo v1, "synergy_mode" */
android.provider.Settings$Secure .getUriFor ( v1 );
v2 = this.mSettingsObserver;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 317 */
final String v1 = "external_display_connected"; // const-string v1, "external_display_connected"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mSettingsObserver;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 320 */
return;
} // .end method
private void setLightSensorEnable ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "enabled" # Z */
/* .line 433 */
/* iget-boolean v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mLightSensorEnabled:Z */
/* if-ne p1, v0, :cond_0 */
/* .line 434 */
return;
/* .line 436 */
} // :cond_0
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 437 */
v0 = this.mSensorManager;
v1 = this.mLightSensorListener;
v2 = this.mLightSensor;
int v3 = 3; // const/4 v3, 0x3
(( android.hardware.SensorManager ) v0 ).registerListener ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z
/* .line 440 */
} // :cond_1
v0 = this.mSensorManager;
v1 = this.mLightSensorListener;
(( android.hardware.SensorManager ) v0 ).unregisterListener ( v1 ); // invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V
/* .line 442 */
} // :goto_0
/* iput-boolean p1, p0, Lcom/android/server/power/MiuiAttentionDetector;->mLightSensorEnabled:Z */
/* .line 443 */
return;
} // .end method
private void updateAonScreenOffConfig ( ) {
/* .locals 4 */
/* .line 510 */
/* sget-boolean v0, Lcom/android/server/power/MiuiAttentionDetector;->AON_SCREEN_OFF_SUPPORTED:Z */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mContext;
/* .line 511 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v2 = "gaze_lock_screen_setting"; // const-string v2, "gaze_lock_screen_setting"
int v3 = -2; // const/4 v3, -0x2
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v2,v1,v3 );
if ( v0 != null) { // if-eqz v0, :cond_0
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
/* nop */
} // :goto_0
/* iput-boolean v1, p0, Lcom/android/server/power/MiuiAttentionDetector;->mAonScreenOffEnabled:Z */
/* .line 515 */
/* if-nez v1, :cond_1 */
/* .line 516 */
(( com.android.server.power.MiuiAttentionDetector ) p0 ).cancelAttentionCheck ( ); // invoke-virtual {p0}, Lcom/android/server/power/MiuiAttentionDetector;->cancelAttentionCheck()V
/* .line 517 */
v0 = this.mAttentionHandler;
int v1 = 2; // const/4 v1, 0x2
(( com.android.server.power.MiuiAttentionDetector$AttentionHandler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;->removeMessages(I)V
/* .line 519 */
} // :cond_1
(( com.android.server.power.MiuiAttentionDetector ) p0 ).onUserActivityChanged ( ); // invoke-virtual {p0}, Lcom/android/server/power/MiuiAttentionDetector;->onUserActivityChanged()V
/* .line 521 */
} // :goto_1
return;
} // .end method
private void updateAonScreenOnConfig ( ) {
/* .locals 4 */
/* .line 488 */
/* sget-boolean v0, Lcom/android/server/power/MiuiAttentionDetector;->AON_SCREEN_ON_SUPPORTED:Z */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mContext;
/* .line 489 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v2 = "miui_people_near_screen_on"; // const-string v2, "miui_people_near_screen_on"
int v3 = -2; // const/4 v3, -0x2
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v2,v1,v3 );
if ( v0 != null) { // if-eqz v0, :cond_0
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
/* nop */
} // :goto_0
/* iput-boolean v1, p0, Lcom/android/server/power/MiuiAttentionDetector;->mAonScreenOnEnabled:Z */
/* .line 491 */
return;
} // .end method
private void updateConfigState ( ) {
/* .locals 0 */
/* .line 298 */
/* invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector;->updateAonScreenOnConfig()V */
/* .line 299 */
/* invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector;->updateAonScreenOffConfig()V */
/* .line 300 */
/* invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector;->updateScreenProjectConfig()V */
/* .line 301 */
/* invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector;->updateSynergyModeConfig()V */
/* .line 302 */
/* invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector;->updateExternalDisplayConnectConfig()V */
/* .line 303 */
return;
} // .end method
private void updateExternalDisplayConnectConfig ( ) {
/* .locals 4 */
/* .line 504 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v1 = -2; // const/4 v1, -0x2
final String v2 = "external_display_connected"; // const-string v2, "external_display_connected"
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$System .getIntForUser ( v0,v2,v3,v1 );
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* move v3, v1 */
} // :cond_0
/* iput-boolean v3, p0, Lcom/android/server/power/MiuiAttentionDetector;->mExternalDisplayConnected:Z */
/* .line 507 */
return;
} // .end method
private void updateScreenProjectConfig ( ) {
/* .locals 4 */
/* .line 494 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v1 = -2; // const/4 v1, -0x2
/* const-string/jumbo v2, "screen_project_in_screening" */
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v2,v3,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
int v3 = 1; // const/4 v3, 0x1
} // :cond_0
/* iput-boolean v3, p0, Lcom/android/server/power/MiuiAttentionDetector;->mScreenProjectInScreen:Z */
/* .line 496 */
return;
} // .end method
private void updateSynergyModeConfig ( ) {
/* .locals 4 */
/* .line 499 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v1 = -2; // const/4 v1, -0x2
/* const-string/jumbo v2, "synergy_mode" */
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v2,v3,v1 );
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* move v3, v1 */
} // :cond_0
/* iput-boolean v3, p0, Lcom/android/server/power/MiuiAttentionDetector;->mSynergyModeEnable:Z */
/* .line 501 */
return;
} // .end method
/* # virtual methods */
public void cancelAndUnbind ( ) {
/* .locals 2 */
/* .line 336 */
v0 = this.mAonService;
/* if-nez v0, :cond_0 */
/* .line 337 */
return;
/* .line 339 */
} // :cond_0
(( com.android.server.power.MiuiAttentionDetector ) p0 ).cancelAttentionCheck ( ); // invoke-virtual {p0}, Lcom/android/server/power/MiuiAttentionDetector;->cancelAttentionCheck()V
/* .line 340 */
v0 = this.mContext;
v1 = this.mConnection;
(( android.content.Context ) v0 ).unbindService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
/* .line 341 */
int v0 = 0; // const/4 v0, 0x0
this.mAonService = v0;
/* .line 342 */
/* new-instance v0, Ljava/util/concurrent/CountDownLatch; */
int v1 = 1; // const/4 v1, 0x1
/* invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V */
this.mServiceBindingLatch = v0;
/* .line 343 */
return;
} // .end method
public void cancelAttentionCheck ( ) {
/* .locals 4 */
/* .line 396 */
try { // :try_start_0
/* iget-boolean v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mIsChecked:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mAonService;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 397 */
v0 = com.android.server.power.MiuiAttentionDetector.TAG;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Attention check cancel, callers: "; // const-string v2, "Attention check cancel, callers: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
int v2 = 3; // const/4 v2, 0x3
android.os.Debug .getCallers ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v1 );
/* .line 398 */
v0 = this.mAonService;
/* iget v1, p0, Lcom/android/server/power/MiuiAttentionDetector;->mType:I */
v2 = this.mAttentionListener;
/* .line 399 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mIsChecked:Z */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 403 */
} // :cond_0
/* .line 401 */
/* :catch_0 */
/* move-exception v0 */
/* .line 402 */
/* .local v0, "e":Landroid/os/RemoteException; */
v1 = com.android.server.power.MiuiAttentionDetector.TAG;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "cancelAttentionCheck: error "; // const-string v3, "cancelAttentionCheck: error "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v2 );
/* .line 404 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :goto_0
return;
} // .end method
public void checkAttention ( Integer p0, Integer p1, java.lang.String p2, Integer p3 ) {
/* .locals 4 */
/* .param p1, "type" # I */
/* .param p2, "fps" # I */
/* .param p3, "reason" # Ljava/lang/String; */
/* .param p4, "timeout" # I */
/* .line 362 */
/* iget-boolean v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mInteractive:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 364 */
v0 = v0 = this.mPolicy;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 365 */
return;
/* .line 369 */
} // :cond_0
/* invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector;->freeIfInactive()V */
/* .line 371 */
} // :cond_1
/* invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector;->bindAonService()V */
/* .line 373 */
/* invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector;->awaitServiceBinding()V */
/* .line 374 */
/* iput p1, p0, Lcom/android/server/power/MiuiAttentionDetector;->mType:I */
/* .line 375 */
/* iput p2, p0, Lcom/android/server/power/MiuiAttentionDetector;->mFps:I */
/* .line 376 */
this.mReason = p3;
/* .line 377 */
/* iput p4, p0, Lcom/android/server/power/MiuiAttentionDetector;->mCheckTimeout:I */
/* .line 378 */
v0 = com.android.server.power.MiuiAttentionDetector.TAG;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "check attention, the reason is "; // const-string v2, "check attention, the reason is "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ", attentionService is "; // const-string v2, ", attentionService is "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mAonService;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v2 = " mIsChecked:"; // const-string v2, " mIsChecked:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/power/MiuiAttentionDetector;->mIsChecked:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v1 );
/* .line 381 */
try { // :try_start_0
v0 = this.mAonService;
if ( v0 != null) { // if-eqz v0, :cond_2
/* iget-boolean v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mIsChecked:Z */
/* if-nez v0, :cond_2 */
/* .line 382 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mStartTimeStamp:J */
/* .line 383 */
v0 = this.mAonService;
/* int-to-float v1, p2 */
v2 = this.mAttentionListener;
/* .line 384 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mIsChecked:Z */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 388 */
} // :cond_2
/* .line 386 */
/* :catch_0 */
/* move-exception v0 */
/* .line 387 */
/* .local v0, "e":Landroid/os/RemoteException; */
v1 = com.android.server.power.MiuiAttentionDetector.TAG;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "checkAttention: error "; // const-string v3, "checkAttention: error "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v2 );
/* .line 389 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :goto_0
return;
} // .end method
public void dump ( java.io.PrintWriter p0 ) {
/* .locals 2 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 579 */
final String v0 = "MiuiAttentionDetector:"; // const-string v0, "MiuiAttentionDetector:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 580 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mComponentName="; // const-string v1, " mComponentName="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mComponentName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 581 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mAonScreenOnEnabled="; // const-string v1, " mAonScreenOnEnabled="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/power/MiuiAttentionDetector;->mAonScreenOnEnabled:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 582 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mAonScreenOffEnabled="; // const-string v1, " mAonScreenOffEnabled="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/power/MiuiAttentionDetector;->mAonScreenOffEnabled:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 583 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mIsChecked="; // const-string v1, " mIsChecked="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/power/MiuiAttentionDetector;->mIsChecked:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 584 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " isScreenCastMode="; // const-string v1, " isScreenCastMode="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = /* invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector;->isScreenCastMode()Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 585 */
return;
} // .end method
public void notifyInteractiveChange ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "interactive" # Z */
/* .line 415 */
/* iget-boolean v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mAonScreenOnEnabled:Z */
/* if-nez v0, :cond_0 */
/* iget-boolean v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mAonScreenOffEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
} // :cond_0
v0 = /* invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector;->isScreenCastMode()Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 416 */
} // :cond_1
return;
/* .line 418 */
} // :cond_2
v0 = this.mAttentionHandler;
/* new-instance v1, Lcom/android/server/power/MiuiAttentionDetector$$ExternalSyntheticLambda2; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/power/MiuiAttentionDetector$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/power/MiuiAttentionDetector;Z)V */
(( com.android.server.power.MiuiAttentionDetector$AttentionHandler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;->post(Ljava/lang/Runnable;)Z
/* .line 430 */
return;
} // .end method
public void notifyStayOnChanged ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "stayOn" # Z */
/* .line 480 */
/* iput-boolean p1, p0, Lcom/android/server/power/MiuiAttentionDetector;->mStayOn:Z */
/* .line 481 */
return;
} // .end method
public void onUserActivityChanged ( ) {
/* .locals 2 */
/* .line 466 */
/* iget-boolean v0, p0, Lcom/android/server/power/MiuiAttentionDetector;->mAonScreenOffEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = /* invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector;->isScreenCastMode()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 469 */
} // :cond_0
v0 = this.mAttentionHandler;
/* new-instance v1, Lcom/android/server/power/MiuiAttentionDetector$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0}, Lcom/android/server/power/MiuiAttentionDetector$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/power/MiuiAttentionDetector;)V */
(( com.android.server.power.MiuiAttentionDetector$AttentionHandler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;->post(Ljava/lang/Runnable;)Z
/* .line 477 */
return;
/* .line 467 */
} // :cond_1
} // :goto_0
return;
} // .end method
