.class Lcom/android/server/power/MiuiAttentionDetector$3;
.super Ljava/lang/Object;
.source "MiuiAttentionDetector.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/power/MiuiAttentionDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/power/MiuiAttentionDetector;


# direct methods
.method public static synthetic $r8$lambda$fqfwK1K1IlVKb8K7ccSUIkLUq-8(Lcom/android/server/power/MiuiAttentionDetector$3;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector$3;->lambda$onServiceConnected$0()V

    return-void
.end method

.method public static synthetic $r8$lambda$x6Ewf7hL_Bz2YQCjpr7u4TlUBQA(Lcom/android/server/power/MiuiAttentionDetector$3;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector$3;->lambda$cleanupService$1()V

    return-void
.end method

.method constructor <init>(Lcom/android/server/power/MiuiAttentionDetector;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/power/MiuiAttentionDetector;

    .line 250
    iput-object p1, p0, Lcom/android/server/power/MiuiAttentionDetector$3;->this$0:Lcom/android/server/power/MiuiAttentionDetector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private synthetic lambda$cleanupService$1()V
    .locals 3

    .line 279
    iget-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector$3;->this$0:Lcom/android/server/power/MiuiAttentionDetector;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/server/power/MiuiAttentionDetector;->-$$Nest$fputmAonService(Lcom/android/server/power/MiuiAttentionDetector;Lcom/xiaomi/aon/IMiAON;)V

    .line 280
    iget-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector$3;->this$0:Lcom/android/server/power/MiuiAttentionDetector;

    new-instance v1, Ljava/util/concurrent/CountDownLatch;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    invoke-static {v0, v1}, Lcom/android/server/power/MiuiAttentionDetector;->-$$Nest$fputmServiceBindingLatch(Lcom/android/server/power/MiuiAttentionDetector;Ljava/util/concurrent/CountDownLatch;)V

    .line 281
    iget-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector$3;->this$0:Lcom/android/server/power/MiuiAttentionDetector;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/server/power/MiuiAttentionDetector;->-$$Nest$fputmIsChecked(Lcom/android/server/power/MiuiAttentionDetector;Z)V

    .line 282
    return-void
.end method

.method private synthetic lambda$onServiceConnected$0()V
    .locals 1

    .line 257
    iget-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector$3;->this$0:Lcom/android/server/power/MiuiAttentionDetector;

    invoke-static {v0}, Lcom/android/server/power/MiuiAttentionDetector;->-$$Nest$mhandlePendingCallback(Lcom/android/server/power/MiuiAttentionDetector;)V

    .line 258
    return-void
.end method


# virtual methods
.method public cleanupService()V
    .locals 2

    .line 278
    iget-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector$3;->this$0:Lcom/android/server/power/MiuiAttentionDetector;

    invoke-static {v0}, Lcom/android/server/power/MiuiAttentionDetector;->-$$Nest$fgetmAttentionHandler(Lcom/android/server/power/MiuiAttentionDetector;)Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;

    move-result-object v0

    new-instance v1, Lcom/android/server/power/MiuiAttentionDetector$3$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/android/server/power/MiuiAttentionDetector$3$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/power/MiuiAttentionDetector$3;)V

    invoke-virtual {v0, v1}, Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;->post(Ljava/lang/Runnable;)Z

    .line 283
    return-void
.end method

.method public onBindingDied(Landroid/content/ComponentName;)V
    .locals 0
    .param p1, "name"    # Landroid/content/ComponentName;

    .line 269
    invoke-virtual {p0}, Lcom/android/server/power/MiuiAttentionDetector$3;->cleanupService()V

    .line 270
    return-void
.end method

.method public onNullBinding(Landroid/content/ComponentName;)V
    .locals 0
    .param p1, "name"    # Landroid/content/ComponentName;

    .line 274
    invoke-virtual {p0}, Lcom/android/server/power/MiuiAttentionDetector$3;->cleanupService()V

    .line 275
    return-void
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .line 253
    invoke-static {}, Lcom/android/server/power/MiuiAttentionDetector;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Attention service connected success, service:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    iget-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector$3;->this$0:Lcom/android/server/power/MiuiAttentionDetector;

    invoke-static {p2}, Landroid/os/Binder;->allowBlocking(Landroid/os/IBinder;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/aon/IMiAON$Stub;->asInterface(Landroid/os/IBinder;)Lcom/xiaomi/aon/IMiAON;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/power/MiuiAttentionDetector;->-$$Nest$fputmAonService(Lcom/android/server/power/MiuiAttentionDetector;Lcom/xiaomi/aon/IMiAON;)V

    .line 255
    iget-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector$3;->this$0:Lcom/android/server/power/MiuiAttentionDetector;

    invoke-static {v0}, Lcom/android/server/power/MiuiAttentionDetector;->-$$Nest$fgetmServiceBindingLatch(Lcom/android/server/power/MiuiAttentionDetector;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 256
    iget-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector$3;->this$0:Lcom/android/server/power/MiuiAttentionDetector;

    invoke-static {v0}, Lcom/android/server/power/MiuiAttentionDetector;->-$$Nest$fgetmAttentionHandler(Lcom/android/server/power/MiuiAttentionDetector;)Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;

    move-result-object v0

    new-instance v1, Lcom/android/server/power/MiuiAttentionDetector$3$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0}, Lcom/android/server/power/MiuiAttentionDetector$3$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/power/MiuiAttentionDetector$3;)V

    invoke-virtual {v0, v1}, Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;->post(Ljava/lang/Runnable;)Z

    .line 259
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;

    .line 263
    invoke-static {}, Lcom/android/server/power/MiuiAttentionDetector;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Attention service connected failure"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    invoke-virtual {p0}, Lcom/android/server/power/MiuiAttentionDetector$3;->cleanupService()V

    .line 265
    return-void
.end method
