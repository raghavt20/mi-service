public class com.android.server.power.UidStateHelper {
	 /* .source "UidStateHelper.java" */
	 /* # static fields */
	 private static Boolean DEBUG;
	 private static final Integer MSG_DISPATCH_UID_STATE_CHANGE;
	 private static java.lang.String TAG;
	 private static com.android.server.power.UidStateHelper sInstance;
	 /* # instance fields */
	 private final android.app.IActivityManager mActivityManager;
	 private final android.os.Handler mHandler;
	 private android.os.Handler$Callback mHandlerCallback;
	 private Boolean mObserverInstalled;
	 private android.app.IProcessObserver mProcessObserver;
	 private java.lang.Object mStateLock;
	 private final android.util.SparseBooleanArray mUidForeground;
	 private final android.util.SparseArray mUidPidForeground;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Landroid/util/SparseArray<", */
	 /* "Landroid/util/SparseBooleanArray;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private android.os.RemoteCallbackList mUidStateObervers;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/os/RemoteCallbackList<", */
/* "Lcom/android/internal/app/IUidStateChangeCallback;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static java.lang.Object -$$Nest$fgetmStateLock ( com.android.server.power.UidStateHelper p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mStateLock;
} // .end method
static android.util.SparseArray -$$Nest$fgetmUidPidForeground ( com.android.server.power.UidStateHelper p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mUidPidForeground;
} // .end method
static void -$$Nest$mcomputeUidForegroundLocked ( com.android.server.power.UidStateHelper p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/power/UidStateHelper;->computeUidForegroundLocked(I)V */
return;
} // .end method
static void -$$Nest$mdispatchUidStateChange ( com.android.server.power.UidStateHelper p0, Integer p1, Integer p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/power/UidStateHelper;->dispatchUidStateChange(II)V */
return;
} // .end method
static Boolean -$$Nest$sfgetDEBUG ( ) { //bridge//synthethic
/* .locals 1 */
/* sget-boolean v0, Lcom/android/server/power/UidStateHelper;->DEBUG:Z */
} // .end method
static java.lang.String -$$Nest$sfgetTAG ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.android.server.power.UidStateHelper.TAG;
} // .end method
static com.android.server.power.UidStateHelper ( ) {
/* .locals 1 */
/* .line 21 */
final String v0 = "UidProcStateHelper"; // const-string v0, "UidProcStateHelper"
/* .line 22 */
/* sget-boolean v0, Landroid/os/Build;->IS_DEBUGGABLE:Z */
com.android.server.power.UidStateHelper.DEBUG = (v0!= 0);
return;
} // .end method
private com.android.server.power.UidStateHelper ( ) {
/* .locals 4 */
/* .line 47 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 30 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
this.mStateLock = v0;
/* .line 32 */
/* new-instance v0, Landroid/util/SparseBooleanArray; */
/* invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V */
this.mUidForeground = v0;
/* .line 33 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
this.mUidPidForeground = v0;
/* .line 35 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/power/UidStateHelper;->mObserverInstalled:Z */
/* .line 36 */
/* new-instance v0, Landroid/os/RemoteCallbackList; */
/* invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V */
this.mUidStateObervers = v0;
/* .line 149 */
/* new-instance v0, Lcom/android/server/power/UidStateHelper$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/power/UidStateHelper$1;-><init>(Lcom/android/server/power/UidStateHelper;)V */
this.mProcessObserver = v0;
/* .line 195 */
/* new-instance v0, Lcom/android/server/power/UidStateHelper$2; */
/* invoke-direct {v0, p0}, Lcom/android/server/power/UidStateHelper$2;-><init>(Lcom/android/server/power/UidStateHelper;)V */
this.mHandlerCallback = v0;
/* .line 48 */
android.app.ActivityManagerNative .getDefault ( );
this.mActivityManager = v0;
/* .line 49 */
/* new-instance v1, Landroid/os/Handler; */
com.android.internal.os.BackgroundThread .get ( );
(( com.android.internal.os.BackgroundThread ) v2 ).getLooper ( ); // invoke-virtual {v2}, Lcom/android/internal/os/BackgroundThread;->getLooper()Landroid/os/Looper;
v3 = this.mHandlerCallback;
/* invoke-direct {v1, v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V */
this.mHandler = v1;
/* .line 53 */
try { // :try_start_0
v1 = this.mProcessObserver;
/* .line 54 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/power/UidStateHelper;->mObserverInstalled:Z */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 56 */
/* .line 55 */
/* :catch_0 */
/* move-exception v0 */
/* .line 57 */
} // :goto_0
return;
} // .end method
private void computeUidForegroundLocked ( Integer p0 ) {
/* .locals 6 */
/* .param p1, "uid" # I */
/* .line 114 */
v0 = this.mUidPidForeground;
(( android.util.SparseArray ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v0, Landroid/util/SparseBooleanArray; */
/* .line 117 */
/* .local v0, "pidForeground":Landroid/util/SparseBooleanArray; */
int v1 = 0; // const/4 v1, 0x0
/* .line 118 */
/* .local v1, "uidForeground":Z */
v2 = (( android.util.SparseBooleanArray ) v0 ).size ( ); // invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I
/* .line 119 */
/* .local v2, "size":I */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
/* if-ge v3, v2, :cond_1 */
/* .line 120 */
v4 = (( android.util.SparseBooleanArray ) v0 ).valueAt ( v3 ); // invoke-virtual {v0, v3}, Landroid/util/SparseBooleanArray;->valueAt(I)Z
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 121 */
int v1 = 1; // const/4 v1, 0x1
/* .line 122 */
/* .line 119 */
} // :cond_0
/* add-int/lit8 v3, v3, 0x1 */
/* .line 126 */
} // .end local v3 # "i":I
} // :cond_1
} // :goto_1
v3 = this.mUidForeground;
int v4 = 0; // const/4 v4, 0x0
v3 = (( android.util.SparseBooleanArray ) v3 ).get ( p1, v4 ); // invoke-virtual {v3, p1, v4}, Landroid/util/SparseBooleanArray;->get(IZ)Z
/* .line 127 */
/* .local v3, "oldUidForeground":Z */
/* if-eq v3, v1, :cond_2 */
/* .line 129 */
v4 = this.mUidForeground;
(( android.util.SparseBooleanArray ) v4 ).put ( p1, v1 ); // invoke-virtual {v4, p1, v1}, Landroid/util/SparseBooleanArray;->put(IZ)V
/* .line 130 */
v4 = this.mHandler;
int v5 = 1; // const/4 v5, 0x1
(( android.os.Handler ) v4 ).obtainMessage ( v5, p1, v1 ); // invoke-virtual {v4, v5, p1, v1}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;
/* .line 131 */
(( android.os.Message ) v4 ).sendToTarget ( ); // invoke-virtual {v4}, Landroid/os/Message;->sendToTarget()V
/* .line 133 */
} // :cond_2
return;
} // .end method
private void dispatchUidStateChange ( Integer p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .param p2, "state" # I */
/* .line 136 */
v0 = this.mUidStateObervers;
v0 = (( android.os.RemoteCallbackList ) v0 ).beginBroadcast ( ); // invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
/* .line 137 */
/* .local v0, "length":I */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* if-ge v1, v0, :cond_1 */
/* .line 138 */
v2 = this.mUidStateObervers;
(( android.os.RemoteCallbackList ) v2 ).getBroadcastItem ( v1 ); // invoke-virtual {v2, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;
/* check-cast v2, Lcom/android/internal/app/IUidStateChangeCallback; */
/* .line 139 */
/* .local v2, "callback":Lcom/android/internal/app/IUidStateChangeCallback; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 141 */
try { // :try_start_0
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 143 */
/* .line 142 */
/* :catch_0 */
/* move-exception v3 */
/* .line 137 */
} // .end local v2 # "callback":Lcom/android/internal/app/IUidStateChangeCallback;
} // :cond_0
} // :goto_1
/* add-int/lit8 v1, v1, 0x1 */
/* .line 146 */
} // .end local v1 # "i":I
} // :cond_1
v1 = this.mUidStateObervers;
(( android.os.RemoteCallbackList ) v1 ).finishBroadcast ( ); // invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
/* .line 147 */
return;
} // .end method
public static com.android.server.power.UidStateHelper get ( ) {
/* .locals 1 */
/* .line 41 */
v0 = com.android.server.power.UidStateHelper.sInstance;
/* if-nez v0, :cond_0 */
/* .line 42 */
/* new-instance v0, Lcom/android/server/power/UidStateHelper; */
/* invoke-direct {v0}, Lcom/android/server/power/UidStateHelper;-><init>()V */
/* .line 44 */
} // :cond_0
v0 = com.android.server.power.UidStateHelper.sInstance;
} // .end method
/* # virtual methods */
public Boolean isUidForeground ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .line 79 */
v0 = android.os.UserHandle .isApp ( p1 );
/* if-nez v0, :cond_0 */
/* .line 80 */
int v0 = 1; // const/4 v0, 0x1
/* .line 82 */
} // :cond_0
v0 = this.mStateLock;
/* monitor-enter v0 */
/* .line 83 */
try { // :try_start_0
v1 = this.mUidForeground;
int v2 = 0; // const/4 v2, 0x0
v1 = (( android.util.SparseBooleanArray ) v1 ).get ( p1, v2 ); // invoke-virtual {v1, p1, v2}, Landroid/util/SparseBooleanArray;->get(IZ)Z
/* monitor-exit v0 */
/* .line 84 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean isUidForeground ( Integer p0, Boolean p1 ) {
/* .locals 9 */
/* .param p1, "uid" # I */
/* .param p2, "doubleCheck" # Z */
/* .line 88 */
v0 = android.os.UserHandle .isApp ( p1 );
int v1 = 1; // const/4 v1, 0x1
/* if-nez v0, :cond_0 */
/* .line 89 */
/* .line 92 */
} // :cond_0
v0 = this.mStateLock;
/* monitor-enter v0 */
/* .line 93 */
try { // :try_start_0
v2 = this.mUidForeground;
int v3 = 0; // const/4 v3, 0x0
v2 = (( android.util.SparseBooleanArray ) v2 ).get ( p1, v3 ); // invoke-virtual {v2, p1, v3}, Landroid/util/SparseBooleanArray;->get(IZ)Z
/* .line 94 */
/* .local v2, "isUidFg":Z */
if ( p2 != null) { // if-eqz p2, :cond_5
/* .line 95 */
int v4 = 0; // const/4 v4, 0x0
/* .line 96 */
/* .local v4, "isFgByPids":Z */
v5 = this.mUidPidForeground;
(( android.util.SparseArray ) v5 ).get ( p1 ); // invoke-virtual {v5, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v5, Landroid/util/SparseBooleanArray; */
/* .line 97 */
/* .local v5, "pidForeground":Landroid/util/SparseBooleanArray; */
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 98 */
int v6 = 0; // const/4 v6, 0x0
/* .local v6, "i":I */
} // :goto_0
v7 = (( android.util.SparseBooleanArray ) v5 ).size ( ); // invoke-virtual {v5}, Landroid/util/SparseBooleanArray;->size()I
/* if-ge v6, v7, :cond_1 */
/* .line 99 */
(( android.util.SparseBooleanArray ) v5 ).keyAt ( v6 ); // invoke-virtual {v5, v6}, Landroid/util/SparseBooleanArray;->keyAt(I)I
/* .line 98 */
/* add-int/lit8 v6, v6, 0x1 */
/* .line 103 */
} // .end local v6 # "i":I
} // :cond_1
/* if-eq v2, v4, :cond_2 */
/* .line 104 */
v6 = com.android.server.power.UidStateHelper.TAG;
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "ProcessObserver may miss callback, isUidFg="; // const-string v8, "ProcessObserver may miss callback, isUidFg="
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v2 ); // invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v8 = " isFgByPids="; // const-string v8, " isFgByPids="
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .wtf ( v6,v7 );
/* .line 106 */
} // :cond_2
/* if-nez v2, :cond_4 */
if ( v4 != null) { // if-eqz v4, :cond_3
} // :cond_3
/* move v1, v3 */
} // :cond_4
} // :goto_1
/* monitor-exit v0 */
/* .line 108 */
} // .end local v4 # "isFgByPids":Z
} // .end local v5 # "pidForeground":Landroid/util/SparseBooleanArray;
} // :cond_5
/* monitor-exit v0 */
/* .line 109 */
/* .line 108 */
} // .end local v2 # "isUidFg":Z
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void registerUidStateObserver ( com.android.internal.app.IUidStateChangeCallback p0 ) {
/* .locals 2 */
/* .param p1, "callback" # Lcom/android/internal/app/IUidStateChangeCallback; */
/* .line 60 */
/* iget-boolean v0, p0, Lcom/android/server/power/UidStateHelper;->mObserverInstalled:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 63 */
/* monitor-enter p0 */
/* .line 64 */
try { // :try_start_0
v0 = this.mUidStateObervers;
(( android.os.RemoteCallbackList ) v0 ).register ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z
/* .line 65 */
/* monitor-exit p0 */
/* .line 66 */
return;
/* .line 65 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v0 */
/* .line 61 */
} // :cond_0
/* new-instance v0, Ljava/lang/IllegalStateException; */
final String v1 = "ProcessObserver not installed"; // const-string v1, "ProcessObserver not installed"
/* invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
public void unregisterUidStateObserver ( com.android.internal.app.IUidStateChangeCallback p0 ) {
/* .locals 2 */
/* .param p1, "callback" # Lcom/android/internal/app/IUidStateChangeCallback; */
/* .line 69 */
/* iget-boolean v0, p0, Lcom/android/server/power/UidStateHelper;->mObserverInstalled:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 72 */
/* monitor-enter p0 */
/* .line 73 */
try { // :try_start_0
v0 = this.mUidStateObervers;
(( android.os.RemoteCallbackList ) v0 ).unregister ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z
/* .line 74 */
/* monitor-exit p0 */
/* .line 75 */
return;
/* .line 74 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v0 */
/* .line 70 */
} // :cond_0
/* new-instance v0, Ljava/lang/IllegalStateException; */
final String v1 = "ProcessObserver not installed"; // const-string v1, "ProcessObserver not installed"
/* invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
