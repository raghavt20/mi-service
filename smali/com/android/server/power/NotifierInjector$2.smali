.class Lcom/android/server/power/NotifierInjector$2;
.super Ljava/lang/Object;
.source "NotifierInjector.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/power/NotifierInjector;->sendBroadcastRestoreBrightness()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/power/NotifierInjector;


# direct methods
.method constructor <init>(Lcom/android/server/power/NotifierInjector;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/power/NotifierInjector;

    .line 103
    iput-object p1, p0, Lcom/android/server/power/NotifierInjector$2;->this$0:Lcom/android/server/power/NotifierInjector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 106
    new-instance v0, Landroid/content/Intent;

    const-string v1, "miui.intent.action.RESTORE_BRIGHTNESS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 107
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/android/server/power/NotifierInjector$2;->this$0:Lcom/android/server/power/NotifierInjector;

    invoke-static {v1}, Lcom/android/server/power/NotifierInjector;->-$$Nest$fgetmContext(Lcom/android/server/power/NotifierInjector;)Landroid/content/Context;

    move-result-object v1

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const-string v3, "miui.permission.restore_brightness"

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;)V

    .line 109
    return-void
.end method
