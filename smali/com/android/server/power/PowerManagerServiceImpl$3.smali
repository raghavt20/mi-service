.class Lcom/android/server/power/PowerManagerServiceImpl$3;
.super Lmiui/app/IFreeformCallback$Stub;
.source "PowerManagerServiceImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/power/PowerManagerServiceImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/power/PowerManagerServiceImpl;


# direct methods
.method constructor <init>(Lcom/android/server/power/PowerManagerServiceImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/power/PowerManagerServiceImpl;

    .line 404
    iput-object p1, p0, Lcom/android/server/power/PowerManagerServiceImpl$3;->this$0:Lcom/android/server/power/PowerManagerServiceImpl;

    invoke-direct {p0}, Lmiui/app/IFreeformCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public dispatchFreeFormStackModeChanged(ILmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;)V
    .locals 2
    .param p1, "action"    # I
    .param p2, "stackInfo"    # Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;

    .line 408
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 411
    :pswitch_1
    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl$3;->this$0:Lcom/android/server/power/PowerManagerServiceImpl;

    invoke-static {v1}, Lcom/android/server/power/PowerManagerServiceImpl;->-$$Nest$fgetmUpdateForegroundAppRunnable(Lcom/android/server/power/PowerManagerServiceImpl;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 412
    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl$3;->this$0:Lcom/android/server/power/PowerManagerServiceImpl;

    invoke-static {v1}, Lcom/android/server/power/PowerManagerServiceImpl;->-$$Nest$fgetmUpdateForegroundAppRunnable(Lcom/android/server/power/PowerManagerServiceImpl;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 413
    nop

    .line 417
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
