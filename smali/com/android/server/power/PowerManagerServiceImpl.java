public class com.android.server.power.PowerManagerServiceImpl extends com.android.server.power.PowerManagerServiceStub {
	 /* .source "PowerManagerServiceImpl.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.power.PowerManagerServiceStub$$" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lcom/android/server/power/PowerManagerServiceImpl$PowerManagerStubHandler;, */
/* Lcom/android/server/power/PowerManagerServiceImpl$SettingsObserver;, */
/* Lcom/android/server/power/PowerManagerServiceImpl$ClientDeathCallback; */
/* } */
} // .end annotation
/* # static fields */
private static Boolean DEBUG;
private static final Integer DEFAULT_SCREEN_OFF_TIMEOUT_SECONDARY_DISPLAY;
private static final Integer DEFAULT_SUBSCREEN_SUPER_POWER_SAVE_MODE;
private static final Integer DIRTY_SCREEN_BRIGHTNESS_BOOST;
private static final Integer DIRTY_SETTINGS;
private static final Integer FLAG_BOOST_BRIGHTNESS;
private static final Integer FLAG_KEEP_SECONDARY_ALWAYS_ON;
private static final Integer FLAG_SCREEN_PROJECTION;
private static final Boolean IS_FOLDABLE_DEVICE;
private static final java.lang.String KEY_DEVICE_LOCK;
private static final java.lang.String KEY_SECONDARY_SCREEN_ENABLE;
private static final Integer MSG_UPDATE_FOREGROUND_APP;
private static final Boolean OPTIMIZE_WAKELOCK_ENABLED;
private static final Integer PICK_UP_SENSOR_TYPE;
private static final java.lang.String POWER_SAVE_MODE_OPEN;
private static final java.lang.String REASON_CHANGE_SECONDARY_STATE_CAMERA_CALL;
private static final java.lang.String SCREEN_OFF_TIMEOUT_SECONDARY_DISPLAY;
private static final java.util.List SKIP_TRANSITION_WAKE_UP_DETAILS;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.lang.String SUBSCREEN_SUPER_POWER_SAVE_MODE;
private static final Integer SUB_DISPLAY_GROUP_ID;
private static final Boolean SUPPORT_FOD;
private static final java.lang.String TAG;
private static final Integer WAKE_LOCK_SCREEN_BRIGHT;
private static final Integer WAKE_LOCK_SCREEN_DIM;
private static final Integer WAKE_LOCK_STAY_AWAKE;
public static final java.lang.String WAKING_UP_DETAILS_BIOMETRIC;
public static final java.lang.String WAKING_UP_DETAILS_FINGERPRINT;
public static final java.lang.String WAKING_UP_DETAILS_GOTO_UNLOCK;
private static final Float mBrightnessDecreaseRatio;
/* # instance fields */
private Boolean isDeviceLock;
private android.app.IActivityTaskManager mActivityTaskManager;
private Boolean mAdaptiveSleepEnabled;
private Boolean mAlwaysWakeUp;
private Boolean mAonScreenOffEnabled;
private com.android.server.power.MiuiAttentionDetector mAttentionDetector;
private Boolean mBootCompleted;
private Long mBrightWaitTime;
private Boolean mBrightnessBoostForSoftLightInProgress;
private java.util.HashMap mClientDeathCallbacks;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Landroid/os/IBinder;", */
/* "Lcom/android/server/power/PowerManagerServiceImpl$ClientDeathCallback;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.content.Context mContext;
private Boolean mDimEnable;
private Long mDisplayStateChangeStateTime;
private Integer mDriveMode;
private java.lang.Boolean mFolded;
private java.lang.String mForegroundAppPackageName;
private final miui.app.IFreeformCallback mFreeformCallBack;
private miui.greeze.IGreezeManager mGreezeManager;
private android.os.Handler mHandler;
private Boolean mHangUpEnabled;
private Boolean mIsBrightWakeLock;
private volatile Boolean mIsFreeFormOrSplitWindowMode;
private Boolean mIsKeyguardHasShownWhenExitHangup;
private Boolean mIsPowerSaveModeEnabled;
private Boolean mIsUserSetupComplete;
private Long mLastRequestDimmingTime;
private java.lang.String mLastSecondaryDisplayGoToSleepReason;
private Long mLastSecondaryDisplayGoToSleepTime;
private Long mLastSecondaryDisplayUserActivityTime;
private java.lang.String mLastSecondaryDisplayWakeUpReason;
private Long mLastSecondaryDisplayWakeUpTime;
private java.lang.Object mLock;
private com.android.server.input.MiuiInputManagerInternal mMiuiInputManagerInternal;
private com.android.server.power.statistic.MiuiPowerStatisticTracker mMiuiPowerStatisticTracker;
private Boolean mNoSupprotInnerScreenProximitySensor;
private com.android.server.power.NotifierInjector mNotifierInjector;
private java.lang.String mPendingForegroundAppPackageName;
private Boolean mPendingSetDirtyDueToRequestDimming;
private Boolean mPendingStopBrightnessBoost;
private Boolean mPickUpGestureWakeUpEnabled;
private android.hardware.Sensor mPickUpSensor;
private Boolean mPickUpSensorEnabled;
private final android.hardware.SensorEventListener mPickUpSensorListener;
private com.android.server.policy.WindowManagerPolicy mPolicy;
private android.util.SparseArray mPowerGroups;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Lcom/android/server/power/PowerGroup;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.miui.whetstone.PowerKeeperPolicy mPowerKeeperPolicy;
private android.os.PowerManager mPowerManager;
private com.android.server.power.PowerManagerService mPowerManagerService;
private Integer mPreWakefulness;
private android.content.ContentResolver mResolver;
private Boolean mScreenProjectionEnabled;
private final java.util.List mScreenWakeLockWhitelists;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Boolean mSecondaryDisplayEnabled;
private Long mSecondaryDisplayScreenOffTimeout;
private android.hardware.SensorManager mSensorManager;
private com.android.server.power.PowerManagerServiceImpl$SettingsObserver mSettingsObserver;
private Boolean mSituatedDimmingDueToAttention;
private Boolean mSituatedDimmingDueToSynergy;
private Integer mSubScreenSuperPowerSaveMode;
private Boolean mSupportAdaptiveSleep;
private Boolean mSynergyModeEnable;
private Boolean mSystemReady;
private final android.app.TaskStackListener mTaskStackListener;
private Boolean mTouchInteractiveUnavailable;
private final java.lang.Runnable mUpdateForegroundAppRunnable;
private Integer mUserActivitySecondaryDisplaySummary;
private Long mUserActivityTimeoutOverrideFromMirrorManager;
private final java.util.List mVisibleWindowUids;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.ArrayList mWakeLocks;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Lcom/android/server/power/PowerManagerService$WakeLock;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Integer mWakefulness;
private final miui.process.IForegroundWindowListener mWindowListener;
/* # direct methods */
public static void $r8$lambda$1rkCId4MrOI2k03MGNvReTtQiEM ( com.android.server.power.PowerManagerServiceImpl p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updateForegroundApp()V */
return;
} // .end method
public static void $r8$lambda$pzfv8pcWLm9HcghS2Yx3ij0_avs ( com.android.server.power.PowerManagerServiceImpl p0, Boolean p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/power/PowerManagerServiceImpl;->onFoldStateChanged(Z)V */
return;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.android.server.power.PowerManagerServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static java.lang.Object -$$Nest$fgetmLock ( com.android.server.power.PowerManagerServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mLock;
} // .end method
static java.lang.Runnable -$$Nest$fgetmUpdateForegroundAppRunnable ( com.android.server.power.PowerManagerServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mUpdateForegroundAppRunnable;
} // .end method
static void -$$Nest$mdoDieLocked ( com.android.server.power.PowerManagerServiceImpl p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/power/PowerManagerServiceImpl;->doDieLocked(I)V */
return;
} // .end method
static void -$$Nest$mhandleSettingsChangedLocked ( com.android.server.power.PowerManagerServiceImpl p0, android.net.Uri p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/power/PowerManagerServiceImpl;->handleSettingsChangedLocked(Landroid/net/Uri;)V */
return;
} // .end method
static void -$$Nest$mupdateForegroundAppPackageName ( com.android.server.power.PowerManagerServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updateForegroundAppPackageName()V */
return;
} // .end method
static com.android.server.power.PowerManagerServiceImpl ( ) {
/* .locals 4 */
/* .line 89 */
/* nop */
/* .line 90 */
final String v0 = "ro.hardware.fp.fod"; // const-string v0, "ro.hardware.fp.fod"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.power.PowerManagerServiceImpl.SUPPORT_FOD = (v0!= 0);
/* .line 94 */
final String v0 = "com.android.systemui:GOTO_UNLOCK"; // const-string v0, "com.android.systemui:GOTO_UNLOCK"
final String v2 = "android.policy:BIOMETRIC"; // const-string v2, "android.policy:BIOMETRIC"
final String v3 = "android.policy:FINGERPRINT"; // const-string v3, "android.policy:FINGERPRINT"
/* filled-new-array {v3, v0, v2}, [Ljava/lang/String; */
java.util.Arrays .asList ( v0 );
/* .line 99 */
com.android.server.power.PowerManagerServiceImpl.DEBUG = (v1!= 0);
/* .line 138 */
/* nop */
/* .line 139 */
final String v0 = "optimize_wakelock_enabled"; // const-string v0, "optimize_wakelock_enabled"
int v2 = 1; // const/4 v2, 0x1
v0 = miui.util.FeatureParser .getBoolean ( v0,v2 );
com.android.server.power.PowerManagerServiceImpl.OPTIMIZE_WAKELOCK_ENABLED = (v0!= 0);
/* .line 144 */
/* nop */
/* .line 145 */
final String v0 = "persist.sys.muiltdisplay_type"; // const-string v0, "persist.sys.muiltdisplay_type"
v0 = android.os.SystemProperties .getInt ( v0,v1 );
int v3 = 2; // const/4 v3, 0x2
/* if-ne v0, v3, :cond_0 */
/* move v1, v2 */
} // :cond_0
com.android.server.power.PowerManagerServiceImpl.IS_FOLDABLE_DEVICE = (v1!= 0);
/* .line 197 */
final String v0 = "brightness_decrease_ratio"; // const-string v0, "brightness_decrease_ratio"
/* const v1, 0x3f666666 # 0.9f */
miui.util.FeatureParser .getFloat ( v0,v1 );
v0 = (( java.lang.Float ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
return;
} // .end method
public com.android.server.power.PowerManagerServiceImpl ( ) {
/* .locals 2 */
/* .line 85 */
/* invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceStub;-><init>()V */
/* .line 146 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
java.util.Collections .synchronizedList ( v0 );
this.mVisibleWindowUids = v0;
/* .line 157 */
/* const-wide/16 v0, -0x1 */
/* iput-wide v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mUserActivityTimeoutOverrideFromMirrorManager:J */
/* .line 165 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mIsKeyguardHasShownWhenExitHangup:Z */
/* .line 167 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mClientDeathCallbacks = v0;
/* .line 201 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mBrightWaitTime:J */
/* .line 205 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSubScreenSuperPowerSaveMode:I */
/* .line 231 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mScreenWakeLockWhitelists = v0;
/* .line 382 */
/* new-instance v0, Lcom/android/server/power/PowerManagerServiceImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v0, p0}, Lcom/android/server/power/PowerManagerServiceImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/power/PowerManagerServiceImpl;)V */
this.mUpdateForegroundAppRunnable = v0;
/* .line 384 */
/* new-instance v0, Lcom/android/server/power/PowerManagerServiceImpl$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/power/PowerManagerServiceImpl$1;-><init>(Lcom/android/server/power/PowerManagerServiceImpl;)V */
this.mTaskStackListener = v0;
/* .line 392 */
/* new-instance v0, Lcom/android/server/power/PowerManagerServiceImpl$2; */
/* invoke-direct {v0, p0}, Lcom/android/server/power/PowerManagerServiceImpl$2;-><init>(Lcom/android/server/power/PowerManagerServiceImpl;)V */
this.mWindowListener = v0;
/* .line 404 */
/* new-instance v0, Lcom/android/server/power/PowerManagerServiceImpl$3; */
/* invoke-direct {v0, p0}, Lcom/android/server/power/PowerManagerServiceImpl$3;-><init>(Lcom/android/server/power/PowerManagerServiceImpl;)V */
this.mFreeformCallBack = v0;
/* .line 1339 */
/* new-instance v0, Lcom/android/server/power/PowerManagerServiceImpl$5; */
/* invoke-direct {v0, p0}, Lcom/android/server/power/PowerManagerServiceImpl$5;-><init>(Lcom/android/server/power/PowerManagerServiceImpl;)V */
this.mPickUpSensorListener = v0;
return;
} // .end method
private void boostScreenBrightness ( android.os.IBinder p0, Boolean p1 ) {
/* .locals 4 */
/* .param p1, "token" # Landroid/os/IBinder; */
/* .param p2, "enabled" # Z */
/* .line 1472 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 1476 */
v0 = android.os.Binder .getCallingUid ( );
/* .line 1477 */
/* .local v0, "uid":I */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v1 */
/* .line 1479 */
/* .local v1, "ident":J */
try { // :try_start_0
/* invoke-direct {p0, p1, p2, v0}, Lcom/android/server/power/PowerManagerServiceImpl;->boostScreenBrightnessInternal(Landroid/os/IBinder;ZI)V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1481 */
android.os.Binder .restoreCallingIdentity ( v1,v2 );
/* .line 1482 */
/* nop */
/* .line 1483 */
return;
/* .line 1481 */
/* :catchall_0 */
/* move-exception v3 */
android.os.Binder .restoreCallingIdentity ( v1,v2 );
/* .line 1482 */
/* throw v3 */
/* .line 1473 */
} // .end local v0 # "uid":I
} // .end local v1 # "ident":J
} // :cond_0
/* new-instance v0, Ljava/lang/IllegalArgumentException; */
/* const-string/jumbo v1, "token must not be null" */
/* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
private void boostScreenBrightnessInternal ( android.os.IBinder p0, Boolean p1, Integer p2 ) {
/* .locals 4 */
/* .param p1, "token" # Landroid/os/IBinder; */
/* .param p2, "enabled" # Z */
/* .param p3, "uid" # I */
/* .line 1315 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 1316 */
try { // :try_start_0
/* iget-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mBrightnessBoostForSoftLightInProgress:Z */
/* if-eq v1, p2, :cond_1 */
/* .line 1317 */
int v1 = 2; // const/4 v1, 0x2
/* invoke-direct {p0, p1, v1, p2}, Lcom/android/server/power/PowerManagerServiceImpl;->setDeathCallbackLocked(Landroid/os/IBinder;IZ)V */
/* .line 1318 */
/* iput-boolean p2, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mBrightnessBoostForSoftLightInProgress:Z */
/* .line 1319 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 1320 */
final String v1 = "PowerManagerServiceImpl"; // const-string v1, "PowerManagerServiceImpl"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Start boosting screen brightness: uid = "; // const-string v3, "Start boosting screen brightness: uid = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p3 ); // invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 1321 */
v1 = this.mPowerManager;
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v2 */
(( android.os.PowerManager ) v1 ).boostScreenBrightness ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/os/PowerManager;->boostScreenBrightness(J)V
/* .line 1323 */
} // :cond_0
/* invoke-direct {p0, p3}, Lcom/android/server/power/PowerManagerServiceImpl;->stopBoostingBrightnessLocked(I)V */
/* .line 1326 */
} // :cond_1
} // :goto_0
/* monitor-exit v0 */
/* .line 1327 */
return;
/* .line 1326 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void clearWakeUpFlags ( android.os.IBinder p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "token" # Landroid/os/IBinder; */
/* .param p2, "flag" # I */
/* .line 1581 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 1583 */
/* .local v0, "ident":J */
try { // :try_start_0
/* invoke-direct {p0, p1, p2}, Lcom/android/server/power/PowerManagerServiceImpl;->clearWakeUpFlagsInternal(Landroid/os/IBinder;I)V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1585 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 1586 */
/* nop */
/* .line 1587 */
return;
/* .line 1585 */
/* :catchall_0 */
/* move-exception v2 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 1586 */
/* throw v2 */
} // .end method
private void clearWakeUpFlagsInternal ( android.os.IBinder p0, Integer p1 ) {
/* .locals 9 */
/* .param p1, "token" # Landroid/os/IBinder; */
/* .param p2, "flag" # I */
/* .line 1632 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 1633 */
try { // :try_start_0
/* sget-boolean v1, Lmiui/os/DeviceFeature;->IS_SUBSCREEN_DEVICE:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* and-int/lit8 v1, p2, 0x1 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1634 */
int v1 = 0; // const/4 v1, 0x0
/* iput-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mAlwaysWakeUp:Z */
/* .line 1635 */
(( com.android.server.power.PowerManagerServiceImpl ) p0 ).unregisterDeathCallbackLocked ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/power/PowerManagerServiceImpl;->unregisterDeathCallbackLocked(Landroid/os/IBinder;)V
/* .line 1636 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v4 */
/* .line 1637 */
/* .local v4, "now":J */
v2 = this.mPowerManagerService;
int v3 = 2; // const/4 v3, 0x2
int v6 = 0; // const/4 v6, 0x0
int v7 = 0; // const/4 v7, 0x0
/* const/16 v8, 0x3e8 */
/* invoke-virtual/range {v2 ..v8}, Lcom/android/server/power/PowerManagerService;->userActivitySecondaryDisplay(IJIII)V */
/* .line 1640 */
} // .end local v4 # "now":J
} // :cond_0
/* monitor-exit v0 */
/* .line 1641 */
return;
/* .line 1640 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void doDieLocked ( ) {
/* .locals 0 */
/* .line 1266 */
(( com.android.server.power.PowerManagerServiceImpl ) p0 ).clearRequestDimmingParamsLocked ( ); // invoke-virtual {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->clearRequestDimmingParamsLocked()V
/* .line 1267 */
/* invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->resetScreenProjectionSettingsLocked()V */
/* .line 1268 */
return;
} // .end method
private void doDieLocked ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "flag" # I */
/* .line 1271 */
/* and-int/lit8 v0, p1, 0x1 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mAlwaysWakeUp:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1272 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mAlwaysWakeUp:Z */
/* .line 1273 */
v0 = this.mPowerManagerService;
(( com.android.server.power.PowerManagerService ) v0 ).updatePowerStateLocked ( ); // invoke-virtual {v0}, Lcom/android/server/power/PowerManagerService;->updatePowerStateLocked()V
/* .line 1274 */
} // :cond_0
/* if-nez p1, :cond_1 */
/* .line 1275 */
(( com.android.server.power.PowerManagerServiceImpl ) p0 ).clearRequestDimmingParamsLocked ( ); // invoke-virtual {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->clearRequestDimmingParamsLocked()V
/* .line 1276 */
/* invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->resetScreenProjectionSettingsLocked()V */
/* .line 1277 */
} // :cond_1
int v0 = 2; // const/4 v0, 0x2
/* if-ne p1, v0, :cond_2 */
/* .line 1278 */
/* const/16 v0, 0x3e8 */
/* invoke-direct {p0, v0}, Lcom/android/server/power/PowerManagerServiceImpl;->stopBoostingBrightnessLocked(I)V */
/* .line 1280 */
} // :cond_2
} // :goto_0
return;
} // .end method
private getRealOwners ( com.android.server.power.PowerManagerService$WakeLock p0 ) {
/* .locals 4 */
/* .param p1, "wakeLock" # Lcom/android/server/power/PowerManagerService$WakeLock; */
/* .line 617 */
int v0 = 0; // const/4 v0, 0x0
/* new-array v1, v0, [I */
/* .line 618 */
/* .local v1, "realOwners":[I */
v2 = this.mWorkSource;
/* if-nez v2, :cond_0 */
/* .line 619 */
int v2 = 1; // const/4 v2, 0x1
/* new-array v1, v2, [I */
/* .line 620 */
/* iget v2, p1, Lcom/android/server/power/PowerManagerService$WakeLock;->mOwnerUid:I */
/* aput v2, v1, v0 */
/* .line 622 */
} // :cond_0
v0 = this.mWorkSource;
v0 = (( android.os.WorkSource ) v0 ).size ( ); // invoke-virtual {v0}, Landroid/os/WorkSource;->size()I
/* .line 623 */
/* .local v0, "N":I */
/* new-array v1, v0, [I */
/* .line 624 */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
/* if-ge v2, v0, :cond_1 */
/* .line 625 */
v3 = this.mWorkSource;
v3 = (( android.os.WorkSource ) v3 ).get ( v2 ); // invoke-virtual {v3, v2}, Landroid/os/WorkSource;->get(I)I
/* aput v3, v1, v2 */
/* .line 624 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 628 */
} // .end local v0 # "N":I
} // .end local v2 # "i":I
} // :cond_1
} // :goto_1
} // .end method
private void goToSleepSecondaryDisplay ( Long p0, java.lang.String p1 ) {
/* .locals 10 */
/* .param p1, "eventTime" # J */
/* .param p3, "reason" # Ljava/lang/String; */
/* .line 1540 */
/* sget-boolean v0, Lmiui/os/DeviceFeature;->IS_SUBSCREEN_DEVICE:Z */
/* if-nez v0, :cond_0 */
/* .line 1541 */
return;
/* .line 1543 */
} // :cond_0
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* cmp-long v0, p1, v0 */
/* if-gtz v0, :cond_4 */
/* .line 1547 */
v0 = android.os.Binder .getCallingUid ( );
/* .line 1548 */
/* .local v0, "uid":I */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v7 */
/* .line 1551 */
/* .local v7, "ident":J */
try { // :try_start_0
v9 = this.mLock;
/* monitor-enter v9 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 1552 */
try { // :try_start_1
/* iget-wide v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLastSecondaryDisplayWakeUpTime:J */
/* cmp-long v1, p1, v1 */
/* if-ltz v1, :cond_3 */
/* .line 1553 */
v1 = (( com.android.server.power.PowerManagerServiceImpl ) p0 ).getSecondaryDisplayWakefulnessLocked ( ); // invoke-virtual {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->getSecondaryDisplayWakefulnessLocked()I
v1 = android.os.PowerManagerInternal .isInteractive ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_3
/* iget-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSystemReady:Z */
if ( v1 != null) { // if-eqz v1, :cond_3
/* iget-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mBootCompleted:Z */
/* if-nez v1, :cond_1 */
/* .line 1559 */
} // :cond_1
v1 = /* invoke-direct {p0, p3}, Lcom/android/server/power/PowerManagerServiceImpl;->shouldGoToSleepWhileAlwaysOn(Ljava/lang/String;)Z */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 1560 */
final String v1 = "PowerManagerServiceImpl"; // const-string v1, "PowerManagerServiceImpl"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Ignore "; // const-string v3, "Ignore "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p3 ); // invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " to apply secondary display sleep while mAlwaysWakeUp"; // const-string v3, " to apply secondary display sleep while mAlwaysWakeUp"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 1561 */
/* monitor-exit v9 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1572 */
android.os.Binder .restoreCallingIdentity ( v7,v8 );
/* .line 1561 */
return;
/* .line 1563 */
} // :cond_2
try { // :try_start_2
/* iput-wide p1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLastSecondaryDisplayGoToSleepTime:J */
/* .line 1564 */
this.mLastSecondaryDisplayGoToSleepReason = p3;
/* .line 1565 */
int v1 = 0; // const/4 v1, 0x0
/* iput-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mAlwaysWakeUp:Z */
/* .line 1567 */
final String v1 = "PowerManagerServiceImpl"; // const-string v1, "PowerManagerServiceImpl"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Going to sleep secondary display from: "; // const-string v3, "Going to sleep secondary display from: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ", reason: "; // const-string v3, ", reason: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p3 ); // invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 1568 */
v1 = this.mPowerManagerService;
v2 = this.mPowerGroups;
int v3 = 1; // const/4 v3, 0x1
(( android.util.SparseArray ) v2 ).get ( v3 ); // invoke-virtual {v2, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v2, Lcom/android/server/power/PowerGroup; */
int v5 = 0; // const/4 v5, 0x0
/* move-wide v3, p1 */
/* move v6, v0 */
/* invoke-virtual/range {v1 ..v6}, Lcom/android/server/power/PowerManagerService;->goToSleepSecondaryDisplay(Lcom/android/server/power/PowerGroup;JII)V */
/* .line 1570 */
/* monitor-exit v9 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 1572 */
android.os.Binder .restoreCallingIdentity ( v7,v8 );
/* .line 1573 */
/* nop */
/* .line 1574 */
return;
/* .line 1556 */
} // :cond_3
} // :goto_0
try { // :try_start_3
final String v1 = "PowerManagerServiceImpl"; // const-string v1, "PowerManagerServiceImpl"
final String v2 = "goToSleepSecondaryDisplay: return"; // const-string v2, "goToSleepSecondaryDisplay: return"
android.util.Slog .d ( v1,v2 );
/* .line 1557 */
/* monitor-exit v9 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 1572 */
android.os.Binder .restoreCallingIdentity ( v7,v8 );
/* .line 1557 */
return;
/* .line 1570 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_4
/* monitor-exit v9 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
} // .end local v0 # "uid":I
} // .end local v7 # "ident":J
} // .end local p0 # "this":Lcom/android/server/power/PowerManagerServiceImpl;
} // .end local p1 # "eventTime":J
} // .end local p3 # "reason":Ljava/lang/String;
try { // :try_start_5
/* throw v1 */
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_1 */
/* .line 1572 */
/* .restart local v0 # "uid":I */
/* .restart local v7 # "ident":J */
/* .restart local p0 # "this":Lcom/android/server/power/PowerManagerServiceImpl; */
/* .restart local p1 # "eventTime":J */
/* .restart local p3 # "reason":Ljava/lang/String; */
/* :catchall_1 */
/* move-exception v1 */
android.os.Binder .restoreCallingIdentity ( v7,v8 );
/* .line 1573 */
/* throw v1 */
/* .line 1544 */
} // .end local v0 # "uid":I
} // .end local v7 # "ident":J
} // :cond_4
/* new-instance v0, Ljava/lang/IllegalArgumentException; */
final String v1 = "event time must not be in the future"; // const-string v1, "event time must not be in the future"
/* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
private void handleSettingsChangedLocked ( android.net.Uri p0 ) {
/* .locals 2 */
/* .param p1, "uri" # Landroid/net/Uri; */
/* .line 1152 */
(( android.net.Uri ) p1 ).getLastPathSegment ( ); // invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;
v1 = (( java.lang.String ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/String;->hashCode()I
/* sparse-switch v1, :sswitch_data_0 */
} // :cond_0
/* goto/16 :goto_0 */
/* :sswitch_0 */
/* const-string/jumbo v1, "screen_project_in_screening" */
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 0; // const/4 v0, 0x0
/* goto/16 :goto_1 */
/* :sswitch_1 */
final String v1 = "drive_mode_drive_mode"; // const-string v1, "drive_mode_drive_mode"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 4; // const/4 v0, 0x4
/* goto/16 :goto_1 */
/* :sswitch_2 */
final String v1 = "POWER_SAVE_MODE_OPEN"; // const-string v1, "POWER_SAVE_MODE_OPEN"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0xc */
/* goto/16 :goto_1 */
/* :sswitch_3 */
final String v1 = "gaze_lock_screen_setting"; // const-string v1, "gaze_lock_screen_setting"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0xb */
/* :sswitch_4 */
final String v1 = "accelerometer_rotation"; // const-string v1, "accelerometer_rotation"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 3; // const/4 v0, 0x3
/* :sswitch_5 */
/* const-string/jumbo v1, "subscreen_super_power_save_mode" */
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x8 */
/* :sswitch_6 */
final String v1 = "pick_up_gesture_wakeup_mode"; // const-string v1, "pick_up_gesture_wakeup_mode"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x9 */
/* :sswitch_7 */
/* const-string/jumbo v1, "screen_project_hang_up_on" */
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
/* :sswitch_8 */
/* const-string/jumbo v1, "user_setup_complete" */
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 7; // const/4 v0, 0x7
/* :sswitch_9 */
final String v1 = "adaptive_sleep"; // const-string v1, "adaptive_sleep"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0xa */
/* :sswitch_a */
/* const-string/jumbo v1, "synergy_mode" */
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 2; // const/4 v0, 0x2
/* :sswitch_b */
final String v1 = "com.xiaomi.system.devicelock.locked"; // const-string v1, "com.xiaomi.system.devicelock.locked"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 5; // const/4 v0, 0x5
/* :sswitch_c */
/* const-string/jumbo v1, "subscreen_display_time" */
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 6; // const/4 v0, 0x6
} // :goto_0
int v0 = -1; // const/4 v0, -0x1
} // :goto_1
/* packed-switch v0, :pswitch_data_0 */
/* .line 1189 */
/* :pswitch_0 */
/* invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updatePowerSaveMode()V */
/* .line 1190 */
v0 = this.mPowerManagerService;
(( com.android.server.power.PowerManagerService ) v0 ).updatePowerStateLocked ( ); // invoke-virtual {v0}, Lcom/android/server/power/PowerManagerService;->updatePowerStateLocked()V
/* .line 1191 */
/* .line 1186 */
/* :pswitch_1 */
/* invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updateAonScreenOffConfig()V */
/* .line 1187 */
/* .line 1183 */
/* :pswitch_2 */
/* invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updateAdaptiveSleepLocked()V */
/* .line 1184 */
/* .line 1180 */
/* :pswitch_3 */
/* invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updatePickUpGestureWakeUpModeLocked()V */
/* .line 1181 */
/* .line 1177 */
/* :pswitch_4 */
/* invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updateSubScreenSuperPowerSaveMode()V */
/* .line 1178 */
/* .line 1174 */
/* :pswitch_5 */
/* invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updateUserSetupCompleteLocked()V */
/* .line 1175 */
/* .line 1171 */
/* :pswitch_6 */
/* invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updateSecondaryDisplayScreenOffTimeoutLocked()V */
/* .line 1172 */
/* .line 1168 */
/* :pswitch_7 */
/* invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updateDeviceLockState()V */
/* .line 1169 */
/* .line 1165 */
/* :pswitch_8 */
/* invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updateDriveModeLocked()V */
/* .line 1166 */
/* .line 1162 */
/* :pswitch_9 */
v0 = this.mNotifierInjector;
(( com.android.server.power.NotifierInjector ) v0 ).updateAccelerometerRotationLocked ( ); // invoke-virtual {v0}, Lcom/android/server/power/NotifierInjector;->updateAccelerometerRotationLocked()V
/* .line 1163 */
/* .line 1156 */
/* :pswitch_a */
v0 = /* invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updateScreenProjectionLocked()Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1157 */
v0 = this.mPowerManagerService;
(( com.android.server.power.PowerManagerService ) v0 ).updatePowerStateLocked ( ); // invoke-virtual {v0}, Lcom/android/server/power/PowerManagerService;->updatePowerStateLocked()V
/* .line 1158 */
return;
/* .line 1195 */
} // :cond_1
} // :goto_2
return;
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x6e271123 -> :sswitch_c */
/* -0x57f0fb82 -> :sswitch_b */
/* -0x55bbaa45 -> :sswitch_a */
/* -0x46cdc832 -> :sswitch_9 */
/* -0x3a0f4851 -> :sswitch_8 */
/* -0x35ba1871 -> :sswitch_7 */
/* -0x350b0519 -> :sswitch_6 */
/* -0x1a88b1ac -> :sswitch_5 */
/* 0x4d5796e -> :sswitch_4 */
/* 0x53b87d57 -> :sswitch_3 */
/* 0x56c145be -> :sswitch_2 */
/* 0x593d229f -> :sswitch_1 */
/* 0x790152b5 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_a */
/* :pswitch_a */
/* :pswitch_a */
/* :pswitch_9 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
static void lambda$setBootPhase$0 ( ) { //synthethic
/* .locals 1 */
/* .line 926 */
com.android.server.power.statistic.MiuiPowerStatisticTracker .getInstance ( );
(( com.android.server.power.statistic.MiuiPowerStatisticTracker ) v0 ).bootCompleted ( ); // invoke-virtual {v0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->bootCompleted()V
return;
} // .end method
private void loadSettingsLocked ( ) {
/* .locals 0 */
/* .line 325 */
/* invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updateDriveModeLocked()V */
/* .line 326 */
/* invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updateSecondaryDisplayScreenOffTimeoutLocked()V */
/* .line 327 */
/* invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updateUserSetupCompleteLocked()V */
/* .line 328 */
/* invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updatePickUpGestureWakeUpModeLocked()V */
/* .line 329 */
/* invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updateAdaptiveSleepLocked()V */
/* .line 330 */
/* invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updateAonScreenOffConfig()V */
/* .line 331 */
/* invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updatePowerSaveMode()V */
/* .line 332 */
return;
} // .end method
private void onFoldStateChanged ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "folded" # Z */
/* .line 498 */
v0 = this.mFolded;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = (( java.lang.Boolean ) v0 ).booleanValue ( ); // invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
/* if-ne v0, p1, :cond_0 */
/* .line 499 */
return;
/* .line 501 */
} // :cond_0
java.lang.Boolean .valueOf ( p1 );
this.mFolded = v0;
/* .line 502 */
/* invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updateProximityWakeLockDisabledState()V */
/* .line 503 */
return;
} // .end method
private void printProximityWakeLockStatusChange ( com.android.server.power.PowerManagerService$WakeLock p0, Boolean p1 ) {
/* .locals 3 */
/* .param p1, "wakeLock" # Lcom/android/server/power/PowerManagerService$WakeLock; */
/* .param p2, "disabled" # Z */
/* .line 570 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 571 */
/* .local v0, "sb":Ljava/lang/StringBuilder; */
final String v1 = "Proximity wakelock:["; // const-string v1, "Proximity wakelock:["
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 572 */
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
/* .line 573 */
final String v2 = "]"; // const-string v2, "]"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 574 */
final String v2 = ", disabled: "; // const-string v2, ", disabled: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 575 */
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
/* .line 576 */
final String v2 = ", reason: "; // const-string v2, ", reason: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 577 */
if ( p2 != null) { // if-eqz p2, :cond_2
/* .line 578 */
/* sget-boolean v1, Lcom/android/server/power/PowerManagerServiceImpl;->IS_FOLDABLE_DEVICE:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = this.mFolded;
v1 = (( java.lang.Boolean ) v1 ).booleanValue ( ); // invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
/* if-nez v1, :cond_0 */
/* iget-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mNoSupprotInnerScreenProximitySensor:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 579 */
final String v1 = "in unfold state"; // const-string v1, "in unfold state"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 581 */
} // :cond_0
/* iget-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mIsFreeFormOrSplitWindowMode:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
final String v1 = "in freeform or split window mode"; // const-string v1, "in freeform or split window mode"
/* .line 582 */
} // :cond_1
final String v1 = "in background"; // const-string v1, "in background"
/* .line 581 */
} // :goto_0
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 585 */
} // :cond_2
/* sget-boolean v1, Lcom/android/server/power/PowerManagerServiceImpl;->IS_FOLDABLE_DEVICE:Z */
if ( v1 != null) { // if-eqz v1, :cond_3
v1 = this.mFolded;
v1 = (( java.lang.Boolean ) v1 ).booleanValue ( ); // invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
/* if-nez v1, :cond_3 */
/* iget-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mNoSupprotInnerScreenProximitySensor:Z */
/* if-nez v1, :cond_3 */
/* .line 586 */
final String v1 = "making a call from the internal screen"; // const-string v1, "making a call from the internal screen"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 588 */
} // :cond_3
final String v1 = "in foreground"; // const-string v1, "in foreground"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 591 */
} // :goto_1
final String v1 = "PowerManagerServiceImpl"; // const-string v1, "PowerManagerServiceImpl"
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v2 );
/* .line 592 */
return;
} // .end method
private void registerForegroundAppObserver ( ) {
/* .locals 2 */
/* .line 371 */
try { // :try_start_0
v0 = this.mActivityTaskManager;
v1 = this.mTaskStackListener;
/* .line 372 */
v0 = this.mFreeformCallBack;
miui.app.MiuiFreeFormManager .registerFreeformCallback ( v0 );
/* .line 373 */
v0 = this.mWindowListener;
miui.process.ProcessManager .registerForegroundWindowListener ( v0 );
/* .line 376 */
/* invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updateForegroundApp()V */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 379 */
/* .line 377 */
/* :catch_0 */
/* move-exception v0 */
/* .line 380 */
} // :goto_0
return;
} // .end method
private void requestHangUpWhenScreenProject ( android.os.IBinder p0, Boolean p1 ) {
/* .locals 3 */
/* .param p1, "token" # Landroid/os/IBinder; */
/* .param p2, "hangup" # Z */
/* .line 1302 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 1304 */
/* .local v0, "ident":J */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 1305 */
try { // :try_start_0
/* invoke-direct {p0, p1, p2}, Lcom/android/server/power/PowerManagerServiceImpl;->requestHangUpWhenScreenProjectInternal(Landroid/os/IBinder;Z)V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1308 */
/* :catchall_0 */
/* move-exception v2 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 1309 */
/* throw v2 */
/* .line 1308 */
} // :cond_0
} // :goto_0
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 1309 */
/* nop */
/* .line 1310 */
return;
} // .end method
private void requestHangUpWhenScreenProjectInternal ( android.os.IBinder p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "token" # Landroid/os/IBinder; */
/* .param p2, "hangup" # Z */
/* .line 1223 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 1224 */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
/* invoke-direct {p0, p1, v1, p2}, Lcom/android/server/power/PowerManagerServiceImpl;->setDeathCallbackLocked(Landroid/os/IBinder;IZ)V */
/* .line 1225 */
/* invoke-direct {p0, p2}, Lcom/android/server/power/PowerManagerServiceImpl;->setHangUpModeLocked(Z)V */
/* .line 1226 */
/* monitor-exit v0 */
/* .line 1227 */
return;
/* .line 1226 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void resetScreenProjectionSettingsLocked ( ) {
/* .locals 3 */
/* .line 886 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v1, "screen_project_in_screening" */
int v2 = 0; // const/4 v2, 0x0
android.provider.Settings$Secure .putInt ( v0,v1,v2 );
/* .line 888 */
/* invoke-direct {p0, v2}, Lcom/android/server/power/PowerManagerServiceImpl;->setHangUpModeLocked(Z)V */
/* .line 889 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v1, "synergy_mode" */
android.provider.Settings$Secure .putInt ( v0,v1,v2 );
/* .line 891 */
return;
} // .end method
private void setDeathCallbackLocked ( android.os.IBinder p0, Integer p1, Boolean p2 ) {
/* .locals 2 */
/* .param p1, "token" # Landroid/os/IBinder; */
/* .param p2, "flag" # I */
/* .param p3, "register" # Z */
/* .line 1230 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 1231 */
if ( p3 != null) { // if-eqz p3, :cond_0
/* .line 1232 */
try { // :try_start_0
(( com.android.server.power.PowerManagerServiceImpl ) p0 ).registerDeathCallbackLocked ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/android/server/power/PowerManagerServiceImpl;->registerDeathCallbackLocked(Landroid/os/IBinder;I)V
/* .line 1234 */
} // :cond_0
(( com.android.server.power.PowerManagerServiceImpl ) p0 ).unregisterDeathCallbackLocked ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/power/PowerManagerServiceImpl;->unregisterDeathCallbackLocked(Landroid/os/IBinder;)V
/* .line 1236 */
} // :goto_0
/* monitor-exit v0 */
/* .line 1237 */
return;
/* .line 1236 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void setHangUpModeLocked ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "enable" # Z */
/* .line 894 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 895 */
/* nop */
/* .line 894 */
/* const-string/jumbo v1, "screen_project_hang_up_on" */
android.provider.Settings$Secure .putInt ( v0,v1,p1 );
/* .line 896 */
return;
} // .end method
private Boolean setWakeLockDisabledStateLocked ( com.android.server.power.PowerManagerService$WakeLock p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "wakeLock" # Lcom/android/server/power/PowerManagerService$WakeLock; */
/* .param p2, "disabled" # Z */
/* .line 719 */
int v0 = 0; // const/4 v0, 0x0
/* .line 720 */
/* .local v0, "changed":Z */
/* iget-boolean v1, p1, Lcom/android/server/power/PowerManagerService$WakeLock;->mDisabled:Z */
/* if-eq v1, p2, :cond_0 */
/* .line 721 */
int v0 = 1; // const/4 v0, 0x1
/* .line 722 */
/* iput-boolean p2, p1, Lcom/android/server/power/PowerManagerService$WakeLock;->mDisabled:Z */
/* .line 724 */
} // :cond_0
} // .end method
private Boolean shouldGoToSleepWhileAlwaysOn ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "reason" # Ljava/lang/String; */
/* .line 1628 */
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mAlwaysWakeUp:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
final String v0 = "CAMERA_CALL"; // const-string v0, "CAMERA_CALL"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private void stopBoostingBrightnessLocked ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .line 1330 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "stop boosting screen brightness: uid = " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "PowerManagerServiceImpl"; // const-string v1, "PowerManagerServiceImpl"
android.util.Slog .d ( v1,v0 );
/* .line 1331 */
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mBrightnessBoostForSoftLightInProgress:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1332 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mBrightnessBoostForSoftLightInProgress:Z */
/* .line 1334 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPendingStopBrightnessBoost:Z */
/* .line 1335 */
v0 = this.mPowerManagerService;
(( com.android.server.power.PowerManagerService ) v0 ).updatePowerStateLocked ( ); // invoke-virtual {v0}, Lcom/android/server/power/PowerManagerService;->updatePowerStateLocked()V
/* .line 1336 */
return;
} // .end method
private void systemReady ( ) {
/* .locals 6 */
/* .line 271 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x1110013 */
v0 = (( android.content.res.Resources ) v0 ).getBoolean ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z
/* iput-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSupportAdaptiveSleep:Z */
/* .line 273 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x1103004f */
(( android.content.res.Resources ) v0 ).getStringArray ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 275 */
/* .local v0, "whitelist":[Ljava/lang/String; */
v1 = this.mScreenWakeLockWhitelists;
java.util.Arrays .asList ( v0 );
/* .line 276 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v2, 0x1105006b */
v1 = (( android.content.res.Resources ) v1 ).getBoolean ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z
/* iput-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mNoSupprotInnerScreenProximitySensor:Z */
/* .line 279 */
/* invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->registerForegroundAppObserver()V */
/* .line 280 */
/* invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->resetScreenProjectionSettingsLocked()V */
/* .line 281 */
/* invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->loadSettingsLocked()V */
/* .line 283 */
v1 = this.mResolver;
/* const-string/jumbo v2, "screen_project_in_screening" */
android.provider.Settings$Secure .getUriFor ( v2 );
v3 = this.mSettingsObserver;
int v4 = 0; // const/4 v4, 0x0
int v5 = -1; // const/4 v5, -0x1
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v4, v3, v5 ); // invoke-virtual {v1, v2, v4, v3, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 286 */
v1 = this.mResolver;
/* const-string/jumbo v2, "screen_project_hang_up_on" */
android.provider.Settings$Secure .getUriFor ( v2 );
v3 = this.mSettingsObserver;
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v4, v3, v5 ); // invoke-virtual {v1, v2, v4, v3, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 289 */
v1 = this.mResolver;
/* const-string/jumbo v2, "synergy_mode" */
android.provider.Settings$Secure .getUriFor ( v2 );
v3 = this.mSettingsObserver;
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v4, v3, v5 ); // invoke-virtual {v1, v2, v4, v3, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 292 */
v1 = this.mResolver;
final String v2 = "accelerometer_rotation"; // const-string v2, "accelerometer_rotation"
android.provider.Settings$System .getUriFor ( v2 );
v3 = this.mSettingsObserver;
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v4, v3, v5 ); // invoke-virtual {v1, v2, v4, v3, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 295 */
v1 = this.mResolver;
final String v2 = "com.xiaomi.system.devicelock.locked"; // const-string v2, "com.xiaomi.system.devicelock.locked"
android.provider.Settings$Global .getUriFor ( v2 );
v3 = this.mSettingsObserver;
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v4, v3, v5 ); // invoke-virtual {v1, v2, v4, v3, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 298 */
v1 = this.mResolver;
/* const-string/jumbo v2, "subscreen_display_time" */
android.provider.Settings$System .getUriFor ( v2 );
v3 = this.mSettingsObserver;
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v4, v3, v5 ); // invoke-virtual {v1, v2, v4, v3, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 301 */
v1 = this.mResolver;
/* const-string/jumbo v2, "subscreen_super_power_save_mode" */
android.provider.Settings$System .getUriFor ( v2 );
v3 = this.mSettingsObserver;
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v4, v3, v5 ); // invoke-virtual {v1, v2, v4, v3, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 304 */
v1 = this.mResolver;
final String v2 = "drive_mode_drive_mode"; // const-string v2, "drive_mode_drive_mode"
android.provider.Settings$System .getUriFor ( v2 );
v3 = this.mSettingsObserver;
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v4, v3, v5 ); // invoke-virtual {v1, v2, v4, v3, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 307 */
v1 = this.mResolver;
final String v2 = "pick_up_gesture_wakeup_mode"; // const-string v2, "pick_up_gesture_wakeup_mode"
android.provider.Settings$System .getUriFor ( v2 );
v3 = this.mSettingsObserver;
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v4, v3, v5 ); // invoke-virtual {v1, v2, v4, v3, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 310 */
v1 = this.mResolver;
final String v2 = "adaptive_sleep"; // const-string v2, "adaptive_sleep"
android.provider.Settings$Secure .getUriFor ( v2 );
v3 = this.mSettingsObserver;
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v4, v3, v5 ); // invoke-virtual {v1, v2, v4, v3, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 312 */
v1 = this.mResolver;
final String v2 = "gaze_lock_screen_setting"; // const-string v2, "gaze_lock_screen_setting"
android.provider.Settings$Secure .getUriFor ( v2 );
v3 = this.mSettingsObserver;
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v4, v3, v5 ); // invoke-virtual {v1, v2, v4, v3, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 314 */
v1 = this.mResolver;
final String v2 = "POWER_SAVE_MODE_OPEN"; // const-string v2, "POWER_SAVE_MODE_OPEN"
android.provider.Settings$System .getUriFor ( v2 );
v3 = this.mSettingsObserver;
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v4, v3, v5 ); // invoke-virtual {v1, v2, v4, v3, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 316 */
/* sget-boolean v1, Lcom/android/server/power/PowerManagerServiceImpl;->IS_FOLDABLE_DEVICE:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 317 */
v1 = this.mContext;
/* const-class v2, Landroid/hardware/devicestate/DeviceStateManager; */
(( android.content.Context ) v1 ).getSystemService ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
/* check-cast v1, Landroid/hardware/devicestate/DeviceStateManager; */
/* .line 318 */
/* .local v1, "deviceStateManager":Landroid/hardware/devicestate/DeviceStateManager; */
/* new-instance v2, Landroid/os/HandlerExecutor; */
v3 = this.mHandler;
/* invoke-direct {v2, v3}, Landroid/os/HandlerExecutor;-><init>(Landroid/os/Handler;)V */
/* new-instance v3, Landroid/hardware/devicestate/DeviceStateManager$FoldStateListener; */
v4 = this.mContext;
/* new-instance v5, Lcom/android/server/power/PowerManagerServiceImpl$$ExternalSyntheticLambda1; */
/* invoke-direct {v5, p0}, Lcom/android/server/power/PowerManagerServiceImpl$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/power/PowerManagerServiceImpl;)V */
/* invoke-direct {v3, v4, v5}, Landroid/hardware/devicestate/DeviceStateManager$FoldStateListener;-><init>(Landroid/content/Context;Ljava/util/function/Consumer;)V */
(( android.hardware.devicestate.DeviceStateManager ) v1 ).registerCallback ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/hardware/devicestate/DeviceStateManager;->registerCallback(Ljava/util/concurrent/Executor;Landroid/hardware/devicestate/DeviceStateManager$DeviceStateCallback;)V
/* .line 321 */
} // .end local v1 # "deviceStateManager":Landroid/hardware/devicestate/DeviceStateManager;
} // :cond_0
return;
} // .end method
private void updateAdaptiveSleepLocked ( ) {
/* .locals 4 */
/* .line 345 */
v0 = this.mResolver;
int v1 = -2; // const/4 v1, -0x2
final String v2 = "adaptive_sleep"; // const-string v2, "adaptive_sleep"
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v2,v3,v1 );
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* move v3, v1 */
} // :cond_0
/* iput-boolean v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mAdaptiveSleepEnabled:Z */
/* .line 347 */
return;
} // .end method
private void updateAlwaysWakeUpIfNeededLocked ( android.os.IBinder p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "token" # Landroid/os/IBinder; */
/* .param p2, "flag" # I */
/* .line 1590 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* and-int/lit8 v0, p2, 0x1 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1591 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mAlwaysWakeUp:Z */
/* .line 1592 */
/* invoke-direct {p0, p1, p2, v0}, Lcom/android/server/power/PowerManagerServiceImpl;->setDeathCallbackLocked(Landroid/os/IBinder;IZ)V */
/* .line 1594 */
} // :cond_0
return;
} // .end method
private void updateAonScreenOffConfig ( ) {
/* .locals 4 */
/* .line 350 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v1 = -2; // const/4 v1, -0x2
final String v2 = "gaze_lock_screen_setting"; // const-string v2, "gaze_lock_screen_setting"
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v2,v3,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
int v3 = 1; // const/4 v3, 0x1
} // :cond_0
/* iput-boolean v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mAonScreenOffEnabled:Z */
/* .line 352 */
return;
} // .end method
private void updateDeviceLockState ( ) {
/* .locals 3 */
/* .line 1219 */
v0 = this.mResolver;
final String v1 = "com.xiaomi.system.devicelock.locked"; // const-string v1, "com.xiaomi.system.devicelock.locked"
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.Settings$Global .getInt ( v0,v1,v2 );
if ( v0 != null) { // if-eqz v0, :cond_0
int v2 = 1; // const/4 v2, 0x1
} // :cond_0
/* iput-boolean v2, p0, Lcom/android/server/power/PowerManagerServiceImpl;->isDeviceLock:Z */
/* .line 1220 */
return;
} // .end method
private void updateDriveModeLocked ( ) {
/* .locals 4 */
/* .line 335 */
v0 = this.mResolver;
int v1 = 0; // const/4 v1, 0x0
int v2 = -2; // const/4 v2, -0x2
final String v3 = "drive_mode_drive_mode"; // const-string v3, "drive_mode_drive_mode"
v0 = android.provider.Settings$System .getIntForUser ( v0,v3,v1,v2 );
/* iput v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mDriveMode:I */
/* .line 337 */
return;
} // .end method
private void updateForegroundApp ( ) {
/* .locals 4 */
/* .line 422 */
try { // :try_start_0
v0 = this.mActivityTaskManager;
/* .line 423 */
/* .local v0, "info":Landroid/app/ActivityTaskManager$RootTaskInfo; */
if ( v0 != null) { // if-eqz v0, :cond_3
v1 = this.topActivity;
/* if-nez v1, :cond_0 */
/* .line 426 */
} // :cond_0
v1 = (( android.app.ActivityTaskManager$RootTaskInfo ) v0 ).getWindowingMode ( ); // invoke-virtual {v0}, Landroid/app/ActivityTaskManager$RootTaskInfo;->getWindowingMode()I
int v2 = 5; // const/4 v2, 0x5
int v3 = 1; // const/4 v3, 0x1
/* if-eq v1, v2, :cond_2 */
v1 = this.mActivityTaskManager;
v1 = /* .line 427 */
if ( v1 != null) { // if-eqz v1, :cond_1
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
} // :cond_2
} // :goto_0
/* move v1, v3 */
} // :goto_1
/* iput-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mIsFreeFormOrSplitWindowMode:Z */
/* .line 428 */
v1 = this.topActivity;
(( android.content.ComponentName ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 430 */
/* .local v1, "packageName":Ljava/lang/String; */
this.mPendingForegroundAppPackageName = v1;
/* .line 431 */
v2 = this.mHandler;
(( android.os.Handler ) v2 ).removeMessages ( v3 ); // invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V
/* .line 432 */
v2 = this.mHandler;
(( android.os.Handler ) v2 ).sendEmptyMessage ( v3 ); // invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 435 */
/* nop */
} // .end local v0 # "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
} // .end local v1 # "packageName":Ljava/lang/String;
/* .line 424 */
/* .restart local v0 # "info":Landroid/app/ActivityTaskManager$RootTaskInfo; */
} // :cond_3
} // :goto_2
return;
/* .line 433 */
} // .end local v0 # "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
/* :catch_0 */
/* move-exception v0 */
/* .line 436 */
} // :goto_3
return;
} // .end method
private void updateForegroundAppPackageName ( ) {
/* .locals 2 */
/* .line 439 */
v0 = this.mPendingForegroundAppPackageName;
this.mForegroundAppPackageName = v0;
/* .line 440 */
int v0 = 0; // const/4 v0, 0x0
this.mPendingForegroundAppPackageName = v0;
/* .line 441 */
/* invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updateProximityWakeLockDisabledState()V */
/* .line 443 */
v0 = this.mMiuiPowerStatisticTracker;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 444 */
v1 = this.mForegroundAppPackageName;
(( com.android.server.power.statistic.MiuiPowerStatisticTracker ) v0 ).notifyForegroundAppChanged ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->notifyForegroundAppChanged(Ljava/lang/String;)V
/* .line 446 */
} // :cond_0
return;
} // .end method
private void updatePickUpGestureWakeUpModeLocked ( ) {
/* .locals 4 */
/* .line 340 */
v0 = this.mResolver;
int v1 = -2; // const/4 v1, -0x2
final String v2 = "pick_up_gesture_wakeup_mode"; // const-string v2, "pick_up_gesture_wakeup_mode"
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$System .getIntForUser ( v0,v2,v3,v1 );
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* move v3, v1 */
} // :cond_0
/* iput-boolean v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPickUpGestureWakeUpEnabled:Z */
/* .line 342 */
return;
} // .end method
private void updatePowerSaveMode ( ) {
/* .locals 4 */
/* .line 1645 */
v0 = this.mResolver;
int v1 = -2; // const/4 v1, -0x2
final String v2 = "POWER_SAVE_MODE_OPEN"; // const-string v2, "POWER_SAVE_MODE_OPEN"
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$System .getIntForUser ( v0,v2,v3,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
int v3 = 1; // const/4 v3, 0x1
} // :cond_0
/* move v0, v3 */
/* .line 1647 */
/* .local v0, "isPowerSaveModeEnabled":Z */
/* iput-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mIsPowerSaveModeEnabled:Z */
/* .line 1648 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "updatePowerSaveMode: mIsPowerSaveModeEnabled: " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mIsPowerSaveModeEnabled:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "PowerManagerServiceImpl"; // const-string v2, "PowerManagerServiceImpl"
android.util.Slog .i ( v2,v1 );
/* .line 1649 */
return;
} // .end method
private void updateProximityWakeLockDisabledState ( ) {
/* .locals 4 */
/* .line 449 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 450 */
try { // :try_start_0
v1 = this.mWakeLocks;
(( java.util.ArrayList ) v1 ).iterator ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Lcom/android/server/power/PowerManagerService$WakeLock; */
/* .line 451 */
/* .local v2, "wakeLock":Lcom/android/server/power/PowerManagerService$WakeLock; */
int v3 = 1; // const/4 v3, 0x1
(( com.android.server.power.PowerManagerServiceImpl ) p0 ).setProximityWakeLockDisabledStateLocked ( v2, v3 ); // invoke-virtual {p0, v2, v3}, Lcom/android/server/power/PowerManagerServiceImpl;->setProximityWakeLockDisabledStateLocked(Lcom/android/server/power/PowerManagerService$WakeLock;Z)V
/* .line 452 */
} // .end local v2 # "wakeLock":Lcom/android/server/power/PowerManagerService$WakeLock;
/* .line 453 */
} // :cond_0
/* monitor-exit v0 */
/* .line 454 */
return;
/* .line 453 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private Boolean updateScreenProjectionLocked ( ) {
/* .locals 6 */
/* .line 994 */
v0 = this.mResolver;
/* const-string/jumbo v1, "screen_project_in_screening" */
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.Settings$Secure .getInt ( v0,v1,v2 );
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* move v0, v1 */
} // :cond_0
/* move v0, v2 */
/* .line 996 */
/* .local v0, "screenProjectingEnabled":Z */
} // :goto_0
v3 = this.mResolver;
/* const-string/jumbo v4, "screen_project_hang_up_on" */
v3 = android.provider.Settings$Secure .getInt ( v3,v4,v2 );
/* if-ne v3, v1, :cond_1 */
/* move v3, v1 */
} // :cond_1
/* move v3, v2 */
/* .line 998 */
/* .local v3, "hangUpEnabled":Z */
} // :goto_1
v4 = this.mResolver;
/* const-string/jumbo v5, "synergy_mode" */
v4 = android.provider.Settings$Secure .getInt ( v4,v5,v2 );
/* if-ne v4, v1, :cond_2 */
/* move v4, v1 */
} // :cond_2
/* move v4, v2 */
/* .line 1000 */
/* .local v4, "synergyModeEnable":Z */
} // :goto_2
/* iget-boolean v5, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mScreenProjectionEnabled:Z */
/* if-ne v0, v5, :cond_4 */
/* iget-boolean v5, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mHangUpEnabled:Z */
/* if-ne v3, v5, :cond_4 */
/* iget-boolean v5, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSynergyModeEnable:Z */
/* if-eq v4, v5, :cond_3 */
/* .line 1021 */
} // :cond_3
/* .line 1003 */
} // :cond_4
} // :goto_3
/* if-nez v3, :cond_5 */
/* iget-boolean v5, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mHangUpEnabled:Z */
if ( v5 != null) { // if-eqz v5, :cond_5
/* .line 1004 */
/* iput-boolean v2, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mHangUpEnabled:Z */
/* .line 1005 */
/* .line 1007 */
} // :cond_5
/* iput-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mScreenProjectionEnabled:Z */
/* .line 1008 */
/* iput-boolean v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mHangUpEnabled:Z */
/* .line 1009 */
/* iput-boolean v4, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSynergyModeEnable:Z */
/* .line 1010 */
/* if-nez v0, :cond_6 */
if ( v4 != null) { // if-eqz v4, :cond_7
} // :cond_6
if ( v3 != null) { // if-eqz v3, :cond_7
/* .line 1011 */
(( com.android.server.power.PowerManagerServiceImpl ) p0 ).hangUpNoUpdateLocked ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/power/PowerManagerServiceImpl;->hangUpNoUpdateLocked(Z)Z
/* .line 1012 */
} // :cond_7
/* if-nez v0, :cond_8 */
/* if-nez v4, :cond_8 */
/* .line 1013 */
(( com.android.server.power.PowerManagerServiceImpl ) p0 ).hangUpNoUpdateLocked ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/power/PowerManagerServiceImpl;->hangUpNoUpdateLocked(Z)Z
/* .line 1016 */
} // :cond_8
} // :goto_4
/* iget-boolean v5, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mHangUpEnabled:Z */
if ( v5 != null) { // if-eqz v5, :cond_9
/* .line 1017 */
/* invoke-direct {p0, v2}, Lcom/android/server/power/PowerManagerServiceImpl;->setHangUpModeLocked(Z)V */
/* .line 1019 */
} // :cond_9
} // .end method
private void updateSecondaryDisplayScreenOffTimeoutLocked ( ) {
/* .locals 3 */
/* .line 1612 */
v0 = this.mResolver;
/* const-string/jumbo v1, "subscreen_display_time" */
/* const/16 v2, 0x3a98 */
v0 = android.provider.Settings$System .getInt ( v0,v1,v2 );
/* int-to-long v0, v0 */
/* iput-wide v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSecondaryDisplayScreenOffTimeout:J */
/* .line 1615 */
return;
} // .end method
private void updateSubScreenSuperPowerSaveMode ( ) {
/* .locals 4 */
/* .line 1606 */
v0 = this.mResolver;
int v1 = 0; // const/4 v1, 0x0
int v2 = -2; // const/4 v2, -0x2
/* const-string/jumbo v3, "subscreen_super_power_save_mode" */
v0 = android.provider.Settings$System .getIntForUser ( v0,v3,v1,v2 );
/* iput v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSubScreenSuperPowerSaveMode:I */
/* .line 1609 */
return;
} // .end method
private void updateUserSetupCompleteLocked ( ) {
/* .locals 4 */
/* .line 1618 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v1 = -2; // const/4 v1, -0x2
/* const-string/jumbo v2, "user_setup_complete" */
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v2,v3,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
int v3 = 1; // const/4 v3, 0x1
} // :cond_0
/* iput-boolean v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mIsUserSetupComplete:Z */
/* .line 1621 */
return;
} // .end method
private void wakeUpSecondaryDisplay ( Long p0, java.lang.String p1 ) {
/* .locals 6 */
/* .param p1, "eventTime" # J */
/* .param p3, "reason" # Ljava/lang/String; */
/* .line 1491 */
int v1 = 0; // const/4 v1, 0x0
int v4 = 0; // const/4 v4, 0x0
/* move-object v0, p0 */
/* move-wide v2, p1 */
/* move-object v5, p3 */
/* invoke-direct/range {v0 ..v5}, Lcom/android/server/power/PowerManagerServiceImpl;->wakeUpSecondaryDisplay(Landroid/os/IBinder;JILjava/lang/String;)V */
/* .line 1492 */
return;
} // .end method
private void wakeUpSecondaryDisplay ( android.os.IBinder p0, Long p1, Integer p2, java.lang.String p3 ) {
/* .locals 17 */
/* .param p1, "token" # Landroid/os/IBinder; */
/* .param p2, "eventTime" # J */
/* .param p4, "flag" # I */
/* .param p5, "reason" # Ljava/lang/String; */
/* .line 1500 */
/* move-object/from16 v1, p0 */
/* move-wide/from16 v9, p2 */
/* move-object/from16 v11, p5 */
/* sget-boolean v0, Lmiui/os/DeviceFeature;->IS_SUBSCREEN_DEVICE:Z */
/* if-nez v0, :cond_0 */
/* .line 1501 */
return;
/* .line 1503 */
} // :cond_0
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v2 */
/* cmp-long v0, v9, v2 */
/* if-gtz v0, :cond_5 */
/* .line 1507 */
v12 = android.os.Binder .getCallingUid ( );
/* .line 1508 */
/* .local v12, "uid":I */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v13 */
/* .line 1511 */
/* .local v13, "ident":J */
try { // :try_start_0
v15 = this.mLock;
/* monitor-enter v15 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 1512 */
try { // :try_start_1
/* iget-wide v2, v1, Lcom/android/server/power/PowerManagerServiceImpl;->mLastSecondaryDisplayWakeUpTime:J */
/* cmp-long v0, v9, v2 */
/* if-ltz v0, :cond_4 */
/* .line 1513 */
v0 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/power/PowerManagerServiceImpl;->getSecondaryDisplayWakefulnessLocked()I */
int v2 = 1; // const/4 v2, 0x1
/* if-ne v0, v2, :cond_1 */
final String v0 = "CAMERA_CALL"; // const-string v0, "CAMERA_CALL"
/* .line 1516 */
v0 = (( java.lang.String ) v0 ).equals ( v11 ); // invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
} // :cond_1
/* iget-boolean v0, v1, Lcom/android/server/power/PowerManagerServiceImpl;->mSystemReady:Z */
/* if-nez v0, :cond_3 */
} // :cond_2
/* .line 1522 */
} // :cond_3
/* iput-wide v9, v1, Lcom/android/server/power/PowerManagerServiceImpl;->mLastSecondaryDisplayWakeUpTime:J */
/* iput-wide v9, v1, Lcom/android/server/power/PowerManagerServiceImpl;->mLastSecondaryDisplayUserActivityTime:J */
/* .line 1523 */
this.mLastSecondaryDisplayWakeUpReason = v11;
/* .line 1524 */
/* move-object/from16 v8, p1 */
/* move/from16 v7, p4 */
/* invoke-direct {v1, v8, v7}, Lcom/android/server/power/PowerManagerServiceImpl;->updateAlwaysWakeUpIfNeededLocked(Landroid/os/IBinder;I)V */
/* .line 1526 */
final String v0 = "PowerManagerServiceImpl"; // const-string v0, "PowerManagerServiceImpl"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Waking up secondary display from: "; // const-string v4, "Waking up secondary display from: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v12 ); // invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = ", reason: "; // const-string v4, ", reason: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v11 ); // invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v3 );
/* .line 1527 */
v0 = this.mPowerManagerService;
v3 = this.mPowerGroups;
(( android.util.SparseArray ) v3 ).get ( v2 ); // invoke-virtual {v3, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* move-object v3, v2 */
/* check-cast v3, Lcom/android/server/power/PowerGroup; */
int v6 = 0; // const/4 v6, 0x0
/* const-string/jumbo v16, "sub-display" */
/* move-object v2, v0 */
/* move-wide/from16 v4, p2 */
/* move-object/from16 v7, v16 */
/* move v8, v12 */
/* invoke-virtual/range {v2 ..v8}, Lcom/android/server/power/PowerManagerService;->wakeUpSecondaryDisplay(Lcom/android/server/power/PowerGroup;JILjava/lang/String;I)V */
/* .line 1529 */
/* monitor-exit v15 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1531 */
android.os.Binder .restoreCallingIdentity ( v13,v14 );
/* .line 1532 */
/* nop */
/* .line 1533 */
return;
/* .line 1518 */
} // :cond_4
} // :goto_0
try { // :try_start_2
final String v0 = "PowerManagerServiceImpl"; // const-string v0, "PowerManagerServiceImpl"
/* const-string/jumbo v2, "wakeUpSecondaryDisplay: return" */
android.util.Slog .d ( v0,v2 );
/* .line 1519 */
/* monitor-exit v15 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 1531 */
android.os.Binder .restoreCallingIdentity ( v13,v14 );
/* .line 1519 */
return;
/* .line 1529 */
/* :catchall_0 */
/* move-exception v0 */
try { // :try_start_3
/* monitor-exit v15 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
} // .end local v12 # "uid":I
} // .end local v13 # "ident":J
} // .end local p0 # "this":Lcom/android/server/power/PowerManagerServiceImpl;
} // .end local p1 # "token":Landroid/os/IBinder;
} // .end local p2 # "eventTime":J
} // .end local p4 # "flag":I
} // .end local p5 # "reason":Ljava/lang/String;
try { // :try_start_4
/* throw v0 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* .line 1531 */
/* .restart local v12 # "uid":I */
/* .restart local v13 # "ident":J */
/* .restart local p0 # "this":Lcom/android/server/power/PowerManagerServiceImpl; */
/* .restart local p1 # "token":Landroid/os/IBinder; */
/* .restart local p2 # "eventTime":J */
/* .restart local p4 # "flag":I */
/* .restart local p5 # "reason":Ljava/lang/String; */
/* :catchall_1 */
/* move-exception v0 */
android.os.Binder .restoreCallingIdentity ( v13,v14 );
/* .line 1532 */
/* throw v0 */
/* .line 1504 */
} // .end local v12 # "uid":I
} // .end local v13 # "ident":J
} // :cond_5
/* new-instance v0, Ljava/lang/IllegalArgumentException; */
final String v2 = "event time must not be in the future"; // const-string v2, "event time must not be in the future"
/* invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
/* # virtual methods */
public void addVisibleWindowUids ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .line 507 */
v0 = this.mVisibleWindowUids;
v0 = java.lang.Integer .valueOf ( p1 );
/* if-nez v0, :cond_0 */
/* .line 508 */
v0 = this.mVisibleWindowUids;
java.lang.Integer .valueOf ( p1 );
/* .line 510 */
} // :cond_0
return;
} // .end method
protected Integer adjustDirtyIfNeededLocked ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "mDirty" # I */
/* .line 1136 */
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSynergyModeEnable:Z */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSituatedDimmingDueToSynergy:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPendingSetDirtyDueToRequestDimming:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1138 */
/* or-int/lit8 p1, p1, 0x20 */
/* .line 1139 */
/* iput-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPendingSetDirtyDueToRequestDimming:Z */
/* .line 1141 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPendingStopBrightnessBoost:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1142 */
/* or-int/lit16 p1, p1, 0x800 */
/* .line 1144 */
} // :cond_1
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSituatedDimmingDueToAttention:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPendingSetDirtyDueToRequestDimming:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1145 */
/* or-int/lit8 p1, p1, 0x20 */
/* .line 1146 */
/* iput-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPendingSetDirtyDueToRequestDimming:Z */
/* .line 1148 */
} // :cond_2
} // .end method
public Long adjustScreenOffTimeoutIfNeededLocked ( com.android.server.power.PowerGroup p0, Long p1 ) {
/* .locals 2 */
/* .param p1, "powerGroup" # Lcom/android/server/power/PowerGroup; */
/* .param p2, "timeout" # J */
/* .line 1661 */
/* sget-boolean v0, Lmiui/os/DeviceFeature;->IS_SUBSCREEN_DEVICE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = (( com.android.server.power.PowerGroup ) p1 ).getGroupId ( ); // invoke-virtual {p1}, Lcom/android/server/power/PowerGroup;->getGroupId()I
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* .line 1662 */
/* iget-wide v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSecondaryDisplayScreenOffTimeout:J */
/* return-wide v0 */
/* .line 1664 */
} // :cond_0
/* return-wide p2 */
} // .end method
protected Integer adjustWakeLockDueToHangUpLocked ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "wakeLockSummary" # I */
/* .line 1033 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mIsBrightWakeLock:Z */
/* .line 1034 */
/* and-int/lit8 v0, p1, 0x26 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mScreenProjectionEnabled:Z */
/* if-nez v0, :cond_0 */
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSynergyModeEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1037 */
} // :cond_0
/* and-int/lit8 p1, p1, -0x27 */
/* .line 1039 */
} // :cond_1
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mDimEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* and-int/lit8 v0, p1, 0x2 */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1041 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mIsBrightWakeLock:Z */
/* .line 1042 */
/* and-int/lit8 p1, p1, -0x3 */
/* .line 1043 */
/* or-int/lit8 p1, p1, 0x4 */
/* .line 1045 */
} // :cond_2
} // :goto_0
} // .end method
public void checkScreenWakeLockDisabledStateLocked ( ) {
/* .locals 6 */
/* .line 521 */
int v0 = 0; // const/4 v0, 0x0
/* .line 522 */
/* .local v0, "changed":Z */
v1 = this.mWakeLocks;
(( java.util.ArrayList ) v1 ).iterator ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_3
/* check-cast v2, Lcom/android/server/power/PowerManagerService$WakeLock; */
/* .line 523 */
/* .local v2, "wakeLock":Lcom/android/server/power/PowerManagerService$WakeLock; */
v3 = com.android.server.power.PowerManagerService .isScreenLock ( v2 );
if ( v3 != null) { // if-eqz v3, :cond_2
/* iget v3, v2, Lcom/android/server/power/PowerManagerService$WakeLock;->mOwnerUid:I */
/* .line 524 */
v3 = android.os.UserHandle .isApp ( v3 );
if ( v3 != null) { // if-eqz v3, :cond_2
v3 = this.mScreenWakeLockWhitelists;
v4 = this.mPackageName;
v3 = /* .line 525 */
/* if-nez v3, :cond_2 */
/* .line 526 */
v3 = this.mVisibleWindowUids;
/* iget v4, v2, Lcom/android/server/power/PowerManagerService$WakeLock;->mOwnerUid:I */
v3 = java.lang.Integer .valueOf ( v4 );
/* .line 527 */
/* .local v3, "state":Z */
/* iget-boolean v4, v2, Lcom/android/server/power/PowerManagerService$WakeLock;->mDisabled:Z */
/* if-ne v3, v4, :cond_2 */
/* .line 528 */
int v0 = 1; // const/4 v0, 0x1
/* .line 529 */
/* xor-int/lit8 v4, v3, 0x1 */
/* iput-boolean v4, v2, Lcom/android/server/power/PowerManagerService$WakeLock;->mDisabled:Z */
/* .line 530 */
/* iget-boolean v4, v2, Lcom/android/server/power/PowerManagerService$WakeLock;->mDisabled:Z */
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 531 */
v4 = this.mPowerManagerService;
(( com.android.server.power.PowerManagerService ) v4 ).notifyWakeLockReleasedLocked ( v2 ); // invoke-virtual {v4, v2}, Lcom/android/server/power/PowerManagerService;->notifyWakeLockReleasedLocked(Lcom/android/server/power/PowerManagerService$WakeLock;)V
/* .line 533 */
} // :cond_0
v4 = this.mPowerManagerService;
(( com.android.server.power.PowerManagerService ) v4 ).notifyWakeLockAcquiredLocked ( v2 ); // invoke-virtual {v4, v2}, Lcom/android/server/power/PowerManagerService;->notifyWakeLockAcquiredLocked(Lcom/android/server/power/PowerManagerService$WakeLock;)V
/* .line 535 */
} // :goto_1
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "screen wakeLock:["; // const-string v5, "screen wakeLock:["
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.power.PowerManagerService$WakeLock ) v2 ).toString ( ); // invoke-virtual {v2}, Lcom/android/server/power/PowerManagerService$WakeLock;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "]"; // const-string v5, "]"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 536 */
/* iget-boolean v5, v2, Lcom/android/server/power/PowerManagerService$WakeLock;->mDisabled:Z */
if ( v5 != null) { // if-eqz v5, :cond_1
final String v5 = "disabled"; // const-string v5, "disabled"
} // :cond_1
final String v5 = "enabled"; // const-string v5, "enabled"
} // :goto_2
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 535 */
final String v5 = "PowerManagerServiceImpl"; // const-string v5, "PowerManagerServiceImpl"
android.util.Slog .d ( v5,v4 );
/* .line 539 */
} // .end local v2 # "wakeLock":Lcom/android/server/power/PowerManagerService$WakeLock;
} // .end local v3 # "state":Z
} // :cond_2
/* .line 540 */
} // :cond_3
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 541 */
v1 = this.mPowerManagerService;
(( com.android.server.power.PowerManagerService ) v1 ).setWakeLockDirtyLocked ( ); // invoke-virtual {v1}, Lcom/android/server/power/PowerManagerService;->setWakeLockDirtyLocked()V
/* .line 543 */
} // :cond_4
return;
} // .end method
public void clearRequestDimmingParamsLocked ( ) {
/* .locals 4 */
/* .line 1095 */
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSituatedDimmingDueToSynergy:Z */
/* const-wide/16 v1, -0x1 */
int v3 = 0; // const/4 v3, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1096 */
/* iput-boolean v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSituatedDimmingDueToSynergy:Z */
/* .line 1097 */
/* iput-wide v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mUserActivityTimeoutOverrideFromMirrorManager:J */
/* .line 1098 */
/* iput-wide v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLastRequestDimmingTime:J */
/* .line 1099 */
/* iput-boolean v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPendingSetDirtyDueToRequestDimming:Z */
/* .line 1101 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSituatedDimmingDueToAttention:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1102 */
/* iput-boolean v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSituatedDimmingDueToAttention:Z */
/* .line 1103 */
/* iput-wide v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLastRequestDimmingTime:J */
/* .line 1104 */
/* iput-boolean v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPendingSetDirtyDueToRequestDimming:Z */
/* .line 1106 */
} // :cond_1
return;
} // .end method
public void clearVisibleWindowUids ( ) {
/* .locals 1 */
/* .line 514 */
v0 = v0 = this.mVisibleWindowUids;
/* if-nez v0, :cond_0 */
/* .line 515 */
v0 = this.mVisibleWindowUids;
/* .line 517 */
} // :cond_0
return;
} // .end method
public void dumpLocal ( java.io.PrintWriter p0 ) {
/* .locals 11 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 1930 */
/* sget-boolean v0, Lcom/android/server/power/PowerDebugConfig;->DEBUG_PMS:Z */
com.android.server.power.PowerManagerServiceImpl.DEBUG = (v0!= 0);
/* .line 1931 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "mScreenProjectionEnabled: "; // const-string v1, "mScreenProjectionEnabled: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mScreenProjectionEnabled:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1932 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "mHangUpEnabled: "; // const-string v1, "mHangUpEnabled: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mHangUpEnabled:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1933 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "mSynergyModeEnable: "; // const-string v1, "mSynergyModeEnable: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSynergyModeEnable:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1934 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "mIsKeyguardHasShownWhenExitHangup: "; // const-string v1, "mIsKeyguardHasShownWhenExitHangup: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mIsKeyguardHasShownWhenExitHangup:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1935 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "mSituatedDimmingDueToSynergy: "; // const-string v1, "mSituatedDimmingDueToSynergy: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSituatedDimmingDueToSynergy:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1936 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "mLastRequestDimmingTime: "; // const-string v1, "mLastRequestDimmingTime: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLastRequestDimmingTime:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1937 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "mPendingSetDirtyDueToRequestDimming: "; // const-string v1, "mPendingSetDirtyDueToRequestDimming: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPendingSetDirtyDueToRequestDimming:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1938 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "mUserActivityTimeoutOverrideFromMirrorManager: "; // const-string v1, "mUserActivityTimeoutOverrideFromMirrorManager: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mUserActivityTimeoutOverrideFromMirrorManager:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1940 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "mSituatedDimmingDueToAttention: "; // const-string v1, "mSituatedDimmingDueToAttention: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSituatedDimmingDueToAttention:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1941 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "mIsPowerSaveModeEnabled: "; // const-string v1, "mIsPowerSaveModeEnabled: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mIsPowerSaveModeEnabled:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1942 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "mTouchInteractiveUnavailable: "; // const-string v1, "mTouchInteractiveUnavailable: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mTouchInteractiveUnavailable:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1943 */
v0 = this.mNotifierInjector;
(( com.android.server.power.NotifierInjector ) v0 ).dump ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/power/NotifierInjector;->dump(Ljava/io/PrintWriter;)V
/* .line 1945 */
/* sget-boolean v0, Lmiui/os/DeviceFeature;->IS_SUBSCREEN_DEVICE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1946 */
(( java.io.PrintWriter ) p1 ).println ( ); // invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V
/* .line 1947 */
final String v0 = "Secondary display power state: "; // const-string v0, "Secondary display power state: "
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1948 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 1949 */
/* .local v0, "now":J */
/* iget-wide v2, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLastSecondaryDisplayUserActivityTime:J */
/* sub-long v2, v0, v2 */
/* .line 1950 */
/* .local v2, "lastUserActivityDuration":J */
/* iget-wide v4, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLastSecondaryDisplayGoToSleepTime:J */
/* sub-long v4, v0, v4 */
/* .line 1951 */
/* .local v4, "lastGoToSleepDuration":J */
/* iget-wide v6, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLastSecondaryDisplayWakeUpTime:J */
/* sub-long v6, v0, v6 */
/* .line 1953 */
/* .local v6, "lastWakeUpDuration":J */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = " mLastSecondaryDisplayUserActivityTime: "; // const-string v9, " mLastSecondaryDisplayUserActivityTime: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1954 */
android.util.TimeUtils .formatDuration ( v2,v3 );
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v9 = " ago."; // const-string v9, " ago."
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1953 */
(( java.io.PrintWriter ) p1 ).println ( v8 ); // invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1955 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = " mLastSecondaryDisplayWakeUpTime: "; // const-string v10, " mLastSecondaryDisplayWakeUpTime: "
(( java.lang.StringBuilder ) v8 ).append ( v10 ); // invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1956 */
android.util.TimeUtils .formatDuration ( v6,v7 );
(( java.lang.StringBuilder ) v8 ).append ( v10 ); // invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1955 */
(( java.io.PrintWriter ) p1 ).println ( v8 ); // invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1957 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = " mLastSecondaryDisplayGoToSleepTime: "; // const-string v10, " mLastSecondaryDisplayGoToSleepTime: "
(( java.lang.StringBuilder ) v8 ).append ( v10 ); // invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1958 */
android.util.TimeUtils .formatDuration ( v4,v5 );
(( java.lang.StringBuilder ) v8 ).append ( v10 ); // invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1957 */
(( java.io.PrintWriter ) p1 ).println ( v8 ); // invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1959 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = " mUserActivitySecondaryDisplaySummary: 0x"; // const-string v9, " mUserActivitySecondaryDisplaySummary: 0x"
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v9, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mUserActivitySecondaryDisplaySummary:I */
/* .line 1960 */
java.lang.Integer .toHexString ( v9 );
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1959 */
(( java.io.PrintWriter ) p1 ).println ( v8 ); // invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1961 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = " mLastSecondaryDisplayWakeUpReason: "; // const-string v9, " mLastSecondaryDisplayWakeUpReason: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v9 = this.mLastSecondaryDisplayWakeUpReason;
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v8 ); // invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1962 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = " mLastSecondaryDisplayGoToSleepReason: "; // const-string v9, " mLastSecondaryDisplayGoToSleepReason: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v9 = this.mLastSecondaryDisplayGoToSleepReason;
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v8 ); // invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1963 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = " mSecondaryDisplayScreenOffTimeout: "; // const-string v9, " mSecondaryDisplayScreenOffTimeout: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v9, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSecondaryDisplayScreenOffTimeout:J */
(( java.lang.StringBuilder ) v8 ).append ( v9, v10 ); // invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v9 = " ms."; // const-string v9, " ms."
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v8 ); // invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1964 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = " mAlwaysWakeUp: "; // const-string v9, " mAlwaysWakeUp: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v9, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mAlwaysWakeUp:Z */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v8 ); // invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1966 */
} // .end local v0 # "now":J
} // .end local v2 # "lastUserActivityDuration":J
} // .end local v4 # "lastGoToSleepDuration":J
} // .end local v6 # "lastWakeUpDuration":J
} // :cond_0
/* sget-boolean v0, Lcom/android/server/power/PowerManagerServiceImpl;->OPTIMIZE_WAKELOCK_ENABLED:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1967 */
final String v0 = ""; // const-string v0, ""
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1968 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "mIsFreeFormOrSplitWindowMode: "; // const-string v1, "mIsFreeFormOrSplitWindowMode: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mIsFreeFormOrSplitWindowMode:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1970 */
} // :cond_1
v0 = this.mAttentionDetector;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1971 */
(( com.android.server.power.MiuiAttentionDetector ) v0 ).dump ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/power/MiuiAttentionDetector;->dump(Ljava/io/PrintWriter;)V
/* .line 1973 */
} // :cond_2
return;
} // .end method
public Boolean exitHangupWakefulnessLocked ( ) {
/* .locals 2 */
/* .line 963 */
/* iget v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPreWakefulness:I */
int v1 = 4; // const/4 v1, 0x4
/* if-ne v0, v1, :cond_0 */
/* iget v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mWakefulness:I */
/* if-eq v0, v1, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
protected void finishWakefulnessChangeIfNeededLocked ( ) {
/* .locals 3 */
/* .line 968 */
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mScreenProjectionEnabled:Z */
/* if-nez v0, :cond_0 */
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mHangUpEnabled:Z */
/* if-nez v0, :cond_0 */
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSynergyModeEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
} // :cond_0
/* iget v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mWakefulness:I */
int v1 = 4; // const/4 v1, 0x4
/* if-eq v0, v1, :cond_2 */
/* .line 970 */
} // :cond_1
v0 = this.mNotifierInjector;
int v1 = 0; // const/4 v1, 0x0
/* iget v2, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mWakefulness:I */
(( com.android.server.power.NotifierInjector ) v0 ).onWakefulnessInHangUp ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/power/NotifierInjector;->onWakefulnessInHangUp(ZI)V
/* .line 972 */
} // :cond_2
return;
} // .end method
public Long getDefaultDisplayLastUserActivity ( ) {
/* .locals 3 */
/* .line 1754 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 1755 */
try { // :try_start_0
v1 = this.mPowerGroups;
int v2 = 0; // const/4 v2, 0x0
(( android.util.SparseArray ) v1 ).get ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v1, Lcom/android/server/power/PowerGroup; */
(( com.android.server.power.PowerGroup ) v1 ).getLastUserActivityTimeLocked ( ); // invoke-virtual {v1}, Lcom/android/server/power/PowerGroup;->getLastUserActivityTimeLocked()J
/* move-result-wide v1 */
/* monitor-exit v0 */
/* return-wide v1 */
/* .line 1756 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Long getDimDurationExtraTime ( Long p0 ) {
/* .locals 4 */
/* .param p1, "extraTimeMillis" # J */
/* .line 1675 */
/* iget v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mDriveMode:I */
int v1 = 1; // const/4 v1, 0x1
/* const-wide/16 v2, 0x0 */
/* if-ne v0, v1, :cond_1 */
/* cmp-long v0, p1, v2 */
/* if-gtz v0, :cond_0 */
/* .line 1678 */
} // :cond_0
/* return-wide p1 */
/* .line 1676 */
} // :cond_1
} // :goto_0
/* return-wide v2 */
} // .end method
public Integer getPartialWakeLockHoldByUid ( Integer p0 ) {
/* .locals 7 */
/* .param p1, "uid" # I */
/* .line 633 */
int v0 = 0; // const/4 v0, 0x0
/* .line 634 */
/* .local v0, "wakeLockNum":I */
v1 = this.mLock;
/* monitor-enter v1 */
/* .line 635 */
try { // :try_start_0
v2 = this.mWakeLocks;
(( java.util.ArrayList ) v2 ).iterator ( ); // invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
} // :cond_0
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_4
/* check-cast v3, Lcom/android/server/power/PowerManagerService$WakeLock; */
/* .line 636 */
/* .local v3, "wakeLock":Lcom/android/server/power/PowerManagerService$WakeLock; */
v4 = this.mWorkSource;
/* .line 637 */
/* .local v4, "ws":Landroid/os/WorkSource; */
/* if-nez v4, :cond_1 */
/* iget v5, v3, Lcom/android/server/power/PowerManagerService$WakeLock;->mOwnerUid:I */
/* if-ne v5, p1, :cond_0 */
} // :cond_1
if ( v4 != null) { // if-eqz v4, :cond_2
int v5 = 0; // const/4 v5, 0x0
v5 = (( android.os.WorkSource ) v4 ).get ( v5 ); // invoke-virtual {v4, v5}, Landroid/os/WorkSource;->get(I)I
/* if-eq v5, p1, :cond_2 */
/* .line 638 */
/* .line 640 */
} // :cond_2
/* iget v5, v3, Lcom/android/server/power/PowerManagerService$WakeLock;->mFlags:I */
/* const v6, 0xffff */
/* and-int/2addr v5, v6 */
/* .line 641 */
/* .local v5, "wakeLockType":I */
int v6 = 1; // const/4 v6, 0x1
/* if-ne v5, v6, :cond_3 */
/* .line 642 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 644 */
} // .end local v3 # "wakeLock":Lcom/android/server/power/PowerManagerService$WakeLock;
} // .end local v4 # "ws":Landroid/os/WorkSource;
} // .end local v5 # "wakeLockType":I
} // :cond_3
/* .line 645 */
} // :cond_4
/* monitor-exit v1 */
/* .line 646 */
/* .line 645 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
public Integer getScreenWakeLockHoldByUid ( Integer p0 ) {
/* .locals 7 */
/* .param p1, "uid" # I */
/* .line 651 */
int v0 = 0; // const/4 v0, 0x0
/* .line 652 */
/* .local v0, "wakeLockNum":I */
v1 = this.mLock;
/* monitor-enter v1 */
/* .line 653 */
try { // :try_start_0
v2 = this.mWakeLocks;
(( java.util.ArrayList ) v2 ).iterator ( ); // invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
} // :cond_0
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_3
/* check-cast v3, Lcom/android/server/power/PowerManagerService$WakeLock; */
/* .line 654 */
/* .local v3, "wakeLock":Lcom/android/server/power/PowerManagerService$WakeLock; */
v4 = this.mWorkSource;
/* .line 655 */
/* .local v4, "ws":Landroid/os/WorkSource; */
/* if-nez v4, :cond_1 */
/* iget v5, v3, Lcom/android/server/power/PowerManagerService$WakeLock;->mOwnerUid:I */
/* if-ne v5, p1, :cond_0 */
} // :cond_1
if ( v4 != null) { // if-eqz v4, :cond_2
int v5 = 0; // const/4 v5, 0x0
v5 = (( android.os.WorkSource ) v4 ).get ( v5 ); // invoke-virtual {v4, v5}, Landroid/os/WorkSource;->get(I)I
/* if-eq v5, p1, :cond_2 */
/* .line 656 */
/* .line 658 */
} // :cond_2
/* iget v5, v3, Lcom/android/server/power/PowerManagerService$WakeLock;->mFlags:I */
/* const v6, 0xffff */
/* and-int/2addr v5, v6 */
/* .line 659 */
/* .local v5, "wakeLockType":I */
/* sparse-switch v5, :sswitch_data_0 */
/* .line 663 */
/* :sswitch_0 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 664 */
/* nop */
/* .line 668 */
} // .end local v3 # "wakeLock":Lcom/android/server/power/PowerManagerService$WakeLock;
} // .end local v4 # "ws":Landroid/os/WorkSource;
} // .end local v5 # "wakeLockType":I
} // :goto_1
/* .line 669 */
} // :cond_3
/* monitor-exit v1 */
/* .line 670 */
/* .line 669 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x6 -> :sswitch_0 */
/* 0xa -> :sswitch_0 */
/* 0x1a -> :sswitch_0 */
} // .end sparse-switch
} // .end method
protected Integer getSecondaryDisplayWakefulnessLocked ( ) {
/* .locals 2 */
/* .line 1624 */
v0 = this.mPowerGroups;
int v1 = 1; // const/4 v1, 0x1
(( android.util.SparseArray ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/power/PowerGroup; */
v0 = (( com.android.server.power.PowerGroup ) v0 ).getWakefulnessLocked ( ); // invoke-virtual {v0}, Lcom/android/server/power/PowerGroup;->getWakefulnessLocked()I
} // .end method
public Boolean hangUpNoUpdateLocked ( Boolean p0 ) {
/* .locals 14 */
/* .param p1, "hangUp" # Z */
/* .line 900 */
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mBootCompleted:Z */
if ( v0 != null) { // if-eqz v0, :cond_5
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_5
int v0 = 4; // const/4 v0, 0x4
if ( p1 != null) { // if-eqz p1, :cond_0
/* iget v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mWakefulness:I */
/* if-eq v1, v0, :cond_5 */
} // :cond_0
/* if-nez p1, :cond_1 */
/* iget v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mWakefulness:I */
/* if-eq v1, v0, :cond_1 */
/* .line 905 */
} // :cond_1
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "hangUpNoUpdateLocked: "; // const-string v2, "hangUpNoUpdateLocked: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 906 */
if ( p1 != null) { // if-eqz p1, :cond_2
final String v2 = "enter"; // const-string v2, "enter"
} // :cond_2
final String v2 = "exit"; // const-string v2, "exit"
} // :goto_0
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " hang up mode..."; // const-string v2, " hang up mode..."
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 905 */
final String v2 = "PowerManagerServiceImpl"; // const-string v2, "PowerManagerServiceImpl"
android.util.Slog .d ( v2,v1 );
/* .line 907 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
/* .line 908 */
/* .local v1, "eventTime":J */
/* if-nez p1, :cond_3 */
/* .line 909 */
(( com.android.server.power.PowerManagerServiceImpl ) p0 ).updateUserActivityLocked ( ); // invoke-virtual {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updateUserActivityLocked()V
/* .line 911 */
} // :cond_3
v3 = this.mPowerManagerService;
int v4 = 0; // const/4 v4, 0x0
/* .line 913 */
int v13 = 1; // const/4 v13, 0x1
if ( p1 != null) { // if-eqz p1, :cond_4
/* move v5, v0 */
} // :cond_4
/* move v5, v13 */
} // :goto_1
/* const/16 v8, 0x3e8 */
int v9 = 0; // const/4 v9, 0x0
int v10 = 0; // const/4 v10, 0x0
int v11 = 0; // const/4 v11, 0x0
final String v12 = "hangup"; // const-string v12, "hangup"
/* .line 911 */
/* move-wide v6, v1 */
/* invoke-virtual/range {v3 ..v12}, Lcom/android/server/power/PowerManagerService;->setWakefulnessLocked(IIJIIILjava/lang/String;Ljava/lang/String;)V */
/* .line 916 */
v0 = this.mNotifierInjector;
/* iget v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mWakefulness:I */
(( com.android.server.power.NotifierInjector ) v0 ).onWakefulnessInHangUp ( p1, v3 ); // invoke-virtual {v0, p1, v3}, Lcom/android/server/power/NotifierInjector;->onWakefulnessInHangUp(ZI)V
/* .line 917 */
/* .line 903 */
} // .end local v1 # "eventTime":J
} // :cond_5
} // :goto_2
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void init ( com.android.server.power.PowerManagerService p0, java.util.ArrayList p1, java.lang.Object p2, android.content.Context p3, android.os.Looper p4, android.util.SparseArray p5 ) {
/* .locals 3 */
/* .param p1, "powerManagerService" # Lcom/android/server/power/PowerManagerService; */
/* .param p3, "lock" # Ljava/lang/Object; */
/* .param p4, "context" # Landroid/content/Context; */
/* .param p5, "looper" # Landroid/os/Looper; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lcom/android/server/power/PowerManagerService;", */
/* "Ljava/util/ArrayList<", */
/* "Lcom/android/server/power/PowerManagerService$WakeLock;", */
/* ">;", */
/* "Ljava/lang/Object;", */
/* "Landroid/content/Context;", */
/* "Landroid/os/Looper;", */
/* "Landroid/util/SparseArray<", */
/* "Lcom/android/server/power/PowerGroup;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 241 */
/* .local p2, "allWakeLocks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/power/PowerManagerService$WakeLock;>;" */
/* .local p6, "powerGroup":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/android/server/power/PowerGroup;>;" */
this.mContext = p4;
/* .line 242 */
this.mPowerManagerService = p1;
/* .line 243 */
this.mWakeLocks = p2;
/* .line 244 */
this.mLock = p3;
/* .line 246 */
/* new-instance v0, Lcom/android/server/power/PowerManagerServiceImpl$PowerManagerStubHandler; */
/* invoke-direct {v0, p0, p5}, Lcom/android/server/power/PowerManagerServiceImpl$PowerManagerStubHandler;-><init>(Lcom/android/server/power/PowerManagerServiceImpl;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 247 */
com.miui.whetstone.PowerKeeperPolicy .getInstance ( );
this.mPowerKeeperPolicy = v0;
/* .line 248 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
this.mResolver = v0;
/* .line 249 */
/* new-instance v0, Lcom/android/server/power/PowerManagerServiceImpl$SettingsObserver; */
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/power/PowerManagerServiceImpl$SettingsObserver;-><init>(Lcom/android/server/power/PowerManagerServiceImpl;Landroid/os/Handler;)V */
this.mSettingsObserver = v0;
/* .line 250 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSystemReady:Z */
/* .line 251 */
v1 = this.mContext;
final String v2 = "power"; // const-string v2, "power"
(( android.content.Context ) v1 ).getSystemService ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v1, Landroid/os/PowerManager; */
this.mPowerManager = v1;
/* .line 252 */
/* new-instance v1, Lcom/android/server/power/NotifierInjector; */
v2 = this.mContext;
/* invoke-direct {v1, v2}, Lcom/android/server/power/NotifierInjector;-><init>(Landroid/content/Context;)V */
this.mNotifierInjector = v1;
/* .line 253 */
this.mPowerGroups = p6;
/* .line 254 */
/* const-class v1, Lcom/android/server/policy/WindowManagerPolicy; */
com.android.server.LocalServices .getService ( v1 );
/* check-cast v1, Lcom/android/server/policy/WindowManagerPolicy; */
this.mPolicy = v1;
/* .line 255 */
/* const-class v1, Lcom/android/server/input/MiuiInputManagerInternal; */
com.android.server.LocalServices .getService ( v1 );
/* check-cast v1, Lcom/android/server/input/MiuiInputManagerInternal; */
this.mMiuiInputManagerInternal = v1;
/* .line 256 */
v1 = this.mContext;
/* const-string/jumbo v2, "sensor" */
(( android.content.Context ) v1 ).getSystemService ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v1, Landroid/hardware/SensorManager; */
this.mSensorManager = v1;
/* .line 257 */
/* const v2, 0x1fa265c */
(( android.hardware.SensorManager ) v1 ).getDefaultSensor ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Landroid/hardware/SensorManager;->getDefaultSensor(IZ)Landroid/hardware/Sensor;
this.mPickUpSensor = v0;
/* .line 258 */
android.app.ActivityTaskManager .getService ( );
this.mActivityTaskManager = v0;
/* .line 259 */
/* nop */
/* .line 260 */
final String v0 = "greezer"; // const-string v0, "greezer"
android.os.ServiceManager .getService ( v0 );
/* .line 259 */
miui.greeze.IGreezeManager$Stub .asInterface ( v0 );
this.mGreezeManager = v0;
/* .line 261 */
/* invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->systemReady()V */
/* .line 263 */
com.android.server.power.statistic.MiuiPowerStatisticTracker .getInstance ( );
this.mMiuiPowerStatisticTracker = v0;
/* .line 264 */
v1 = this.mContext;
(( com.android.server.power.statistic.MiuiPowerStatisticTracker ) v0 ).init ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->init(Landroid/content/Context;)V
/* .line 265 */
/* sget-boolean v0, Lcom/android/server/power/MiuiAttentionDetector;->AON_SCREEN_OFF_SUPPORTED:Z */
/* if-nez v0, :cond_0 */
/* sget-boolean v0, Lcom/android/server/power/MiuiAttentionDetector;->AON_SCREEN_ON_SUPPORTED:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 266 */
} // :cond_0
/* new-instance v0, Lcom/android/server/power/MiuiAttentionDetector; */
v1 = this.mPowerManager;
v2 = this.mPolicy;
/* invoke-direct {v0, p4, v1, p0, v2}, Lcom/android/server/power/MiuiAttentionDetector;-><init>(Landroid/content/Context;Landroid/os/PowerManager;Lcom/android/server/power/PowerManagerServiceImpl;Lcom/android/server/policy/WindowManagerPolicy;)V */
this.mAttentionDetector = v0;
/* .line 268 */
} // :cond_1
return;
} // .end method
public Boolean isAcquiredScreenBrightWakeLock ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "groupId" # I */
/* .line 1741 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 1742 */
try { // :try_start_0
v1 = this.mPowerGroups;
(( android.util.SparseArray ) v1 ).get ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v1, Lcom/android/server/power/PowerGroup; */
v1 = (( com.android.server.power.PowerGroup ) v1 ).getWakeLockSummaryLocked ( ); // invoke-virtual {v1}, Lcom/android/server/power/PowerGroup;->getWakeLockSummaryLocked()I
/* .line 1743 */
/* .local v1, "wakeLockSummary":I */
/* and-int/lit8 v2, v1, 0x6 */
if ( v2 != null) { // if-eqz v2, :cond_0
int v2 = 1; // const/4 v2, 0x1
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
/* move v1, v2 */
/* .line 1745 */
/* .local v1, "screenBrightWakeLock":Z */
/* monitor-exit v0 */
/* .line 1746 */
/* .line 1745 */
} // .end local v1 # "screenBrightWakeLock":Z
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean isAllowWakeUpList ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .line 675 */
v0 = this.mGreezeManager;
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 676 */
/* .line 680 */
} // :cond_0
v0 = try { // :try_start_0
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 681 */
/* :catch_0 */
/* move-exception v0 */
/* .line 682 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v2 = "PowerManagerServiceImpl"; // const-string v2, "PowerManagerServiceImpl"
final String v3 = "isAllowWakeUpList error "; // const-string v3, "isAllowWakeUpList error "
android.util.Slog .e ( v2,v3,v0 );
/* .line 684 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // .end method
public Boolean isBrightnessBoostForSoftLightInProgress ( ) {
/* .locals 1 */
/* .line 1683 */
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mBrightnessBoostForSoftLightInProgress:Z */
} // .end method
public Boolean isInHangUpState ( ) {
/* .locals 2 */
/* .line 948 */
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mScreenProjectionEnabled:Z */
/* if-nez v0, :cond_0 */
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSynergyModeEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
} // :cond_0
/* iget v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mWakefulness:I */
int v1 = 4; // const/4 v1, 0x4
/* if-ne v0, v1, :cond_1 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean isPendingStopBrightnessBoost ( ) {
/* .locals 1 */
/* .line 1688 */
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPendingStopBrightnessBoost:Z */
} // .end method
public Boolean isShutdownOrRebootPermitted ( Boolean p0, Boolean p1, java.lang.String p2, Boolean p3 ) {
/* .locals 2 */
/* .param p1, "shutdown" # Z */
/* .param p2, "confirm" # Z */
/* .param p3, "reason" # Ljava/lang/String; */
/* .param p4, "wait" # Z */
/* .line 825 */
if ( p1 != null) { // if-eqz p1, :cond_1
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->isDeviceLock:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 826 */
com.android.server.UiThread .getHandler ( );
/* .line 827 */
/* .local v0, "h":Landroid/os/Handler; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 828 */
/* new-instance v1, Lcom/android/server/power/PowerManagerServiceImpl$4; */
/* invoke-direct {v1, p0}, Lcom/android/server/power/PowerManagerServiceImpl$4;-><init>(Lcom/android/server/power/PowerManagerServiceImpl;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 837 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 839 */
} // .end local v0 # "h":Landroid/os/Handler;
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
} // .end method
public Boolean isSituatedDimmingDueToSynergy ( ) {
/* .locals 1 */
/* .line 954 */
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mScreenProjectionEnabled:Z */
/* if-nez v0, :cond_0 */
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSynergyModeEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSituatedDimmingDueToSynergy:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean isSubScreenSuperPowerSaveModeOpen ( ) {
/* .locals 2 */
/* .line 1597 */
/* sget-boolean v0, Lmiui/os/DeviceFeature;->IS_SUBSCREEN_DEVICE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSubScreenSuperPowerSaveMode:I */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1599 */
final String v0 = "PowerManagerServiceImpl"; // const-string v0, "PowerManagerServiceImpl"
/* const-string/jumbo v1, "wilderness subscreen_super_power_save_mode open" */
android.util.Slog .i ( v0,v1 );
/* .line 1600 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1602 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean isWakelockDisabledByPolicy ( com.android.server.power.PowerManagerService$WakeLock p0 ) {
/* .locals 7 */
/* .param p1, "wakeLock" # Lcom/android/server/power/PowerManagerService$WakeLock; */
/* .line 695 */
int v0 = 0; // const/4 v0, 0x0
/* .line 696 */
/* .local v0, "disabled":Z */
/* invoke-direct {p0, p1}, Lcom/android/server/power/PowerManagerServiceImpl;->getRealOwners(Lcom/android/server/power/PowerManagerService$WakeLock;)[I */
/* .line 697 */
/* .local v1, "realOwners":[I */
v2 = this.mPowerKeeperPolicy;
int v3 = 0; // const/4 v3, 0x0
if ( v2 != null) { // if-eqz v2, :cond_4
v2 = this.mGreezeManager;
/* if-nez v2, :cond_0 */
/* .line 701 */
} // :cond_0
try { // :try_start_0
/* array-length v2, v1 */
} // :goto_0
/* if-ge v3, v2, :cond_3 */
/* aget v4, v1, v3 */
/* .line 702 */
/* .local v4, "realOwner":I */
v5 = this.mPowerKeeperPolicy;
v6 = this.mTag;
v5 = (( com.miui.whetstone.PowerKeeperPolicy ) v5 ).isWakelockDisabledByPolicy ( v6, v4 ); // invoke-virtual {v5, v6, v4}, Lcom/miui/whetstone/PowerKeeperPolicy;->isWakelockDisabledByPolicy(Ljava/lang/String;I)Z
int v6 = 1; // const/4 v6, 0x1
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 703 */
int v0 = 1; // const/4 v0, 0x1
/* .line 704 */
final String v2 = "PowerKeeper"; // const-string v2, "PowerKeeper"
(( com.android.server.power.PowerManagerServiceImpl ) p0 ).printPartialWakeLockDisabledIfNeeded ( p1, v6, v2 ); // invoke-virtual {p0, p1, v6, v2}, Lcom/android/server/power/PowerManagerServiceImpl;->printPartialWakeLockDisabledIfNeeded(Lcom/android/server/power/PowerManagerService$WakeLock;ZLjava/lang/String;)V
/* .line 705 */
/* .line 706 */
} // :cond_1
v5 = android.os.UserHandle .isApp ( v4 );
if ( v5 != null) { // if-eqz v5, :cond_2
v5 = v5 = this.mGreezeManager;
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 707 */
int v0 = 1; // const/4 v0, 0x1
/* .line 708 */
final String v2 = "UidFrozen"; // const-string v2, "UidFrozen"
(( com.android.server.power.PowerManagerServiceImpl ) p0 ).printPartialWakeLockDisabledIfNeeded ( p1, v6, v2 ); // invoke-virtual {p0, p1, v6, v2}, Lcom/android/server/power/PowerManagerServiceImpl;->printPartialWakeLockDisabledIfNeeded(Lcom/android/server/power/PowerManagerService$WakeLock;ZLjava/lang/String;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 709 */
/* .line 701 */
} // .end local v4 # "realOwner":I
} // :cond_2
/* add-int/lit8 v3, v3, 0x1 */
/* .line 714 */
} // :cond_3
} // :goto_1
/* .line 712 */
/* :catch_0 */
/* move-exception v2 */
/* .line 713 */
/* .local v2, "e":Ljava/lang/Exception; */
final String v3 = "PowerManagerServiceImpl"; // const-string v3, "PowerManagerServiceImpl"
final String v4 = "isWakelockDisabledByPolicy error "; // const-string v4, "isWakelockDisabledByPolicy error "
android.util.Slog .e ( v3,v4,v2 );
/* .line 715 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_2
/* .line 698 */
} // :cond_4
} // :goto_3
} // .end method
public void lockNowIfNeededLocked ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "reason" # I */
/* .line 1724 */
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSynergyModeEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
/* if-ne p1, v0, :cond_0 */
/* iget v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mWakefulness:I */
int v1 = 4; // const/4 v1, 0x4
/* if-ne v0, v1, :cond_0 */
/* .line 1727 */
v0 = com.android.server.wm.WindowManagerServiceStub .get ( );
/* if-nez v0, :cond_0 */
/* .line 1728 */
v0 = this.mPolicy;
int v1 = 0; // const/4 v1, 0x0
/* .line 1729 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mIsKeyguardHasShownWhenExitHangup:Z */
/* .line 1731 */
} // :cond_0
return;
} // .end method
public void notifyAonScreenOnOffEvent ( Boolean p0, Boolean p1, Long p2 ) {
/* .locals 5 */
/* .param p1, "isScreenOn" # Z */
/* .param p2, "hasFace" # Z */
/* .param p3, "startTime" # J */
/* .line 1846 */
v0 = this.mMiuiPowerStatisticTracker;
/* if-nez v0, :cond_0 */
/* .line 1847 */
return;
/* .line 1849 */
} // :cond_0
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 1850 */
/* .local v0, "now":J */
/* sub-long v2, v0, p3 */
/* .line 1851 */
/* .local v2, "latencyMs":J */
v4 = this.mMiuiPowerStatisticTracker;
(( com.android.server.power.statistic.MiuiPowerStatisticTracker ) v4 ).notifyAonScreenOnOffEvent ( p1, p2, v2, v3 ); // invoke-virtual {v4, p1, p2, v2, v3}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->notifyAonScreenOnOffEvent(ZZJ)V
/* .line 1852 */
return;
} // .end method
public void notifyDimStateChanged ( Integer p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "pendingRequestPolicy" # I */
/* .param p2, "requestPolicy" # I */
/* .line 1900 */
v0 = this.mMiuiPowerStatisticTracker;
/* if-nez v0, :cond_0 */
/* .line 1901 */
return;
/* .line 1904 */
} // :cond_0
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 1905 */
/* .local v0, "now":J */
int v2 = 2; // const/4 v2, 0x2
/* if-eq v2, p2, :cond_1 */
/* if-ne v2, p1, :cond_3 */
/* .line 1907 */
} // :cond_1
/* if-ne v2, p2, :cond_2 */
int v2 = 1; // const/4 v2, 0x1
} // :cond_2
int v2 = 0; // const/4 v2, 0x0
/* .line 1908 */
/* .local v2, "isEnterDim":Z */
} // :goto_0
v3 = this.mMiuiPowerStatisticTracker;
(( com.android.server.power.statistic.MiuiPowerStatisticTracker ) v3 ).notifyDimStateChanged ( v0, v1, v2 ); // invoke-virtual {v3, v0, v1, v2}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->notifyDimStateChanged(JZ)V
/* .line 1910 */
} // .end local v2 # "isEnterDim":Z
} // :cond_3
return;
} // .end method
public void notifyDisplayPortConnectStateChanged ( Long p0, Boolean p1, java.lang.String p2, Integer p3, java.lang.String p4 ) {
/* .locals 7 */
/* .param p1, "physicalDisplayId" # J */
/* .param p3, "isConnected" # Z */
/* .param p4, "productName" # Ljava/lang/String; */
/* .param p5, "frameRate" # I */
/* .param p6, "resolution" # Ljava/lang/String; */
/* .line 1858 */
v0 = this.mMiuiPowerStatisticTracker;
/* if-nez v0, :cond_0 */
/* .line 1859 */
return;
/* .line 1862 */
} // :cond_0
/* move-wide v1, p1 */
/* move v3, p3 */
/* move-object v4, p4 */
/* move v5, p5 */
/* move-object v6, p6 */
/* invoke-virtual/range {v0 ..v6}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->notifyDisplayPortConnectStateChanged(JZLjava/lang/String;ILjava/lang/String;)V */
/* .line 1864 */
return;
} // .end method
public void notifyDisplayStateChangeEndLocked ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "isFirstDisplay" # Z */
/* .line 1805 */
v0 = this.mMiuiPowerStatisticTracker;
if ( v0 != null) { // if-eqz v0, :cond_1
/* if-nez p1, :cond_0 */
/* .line 1809 */
} // :cond_0
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* iget-wide v2, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mDisplayStateChangeStateTime:J */
/* sub-long/2addr v0, v2 */
/* long-to-int v0, v0 */
/* .line 1810 */
/* .local v0, "latencyMs":I */
v1 = this.mMiuiPowerStatisticTracker;
(( com.android.server.power.statistic.MiuiPowerStatisticTracker ) v1 ).notifyDisplayStateChangedLatencyLocked ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->notifyDisplayStateChangedLatencyLocked(I)V
/* .line 1811 */
return;
/* .line 1806 */
} // .end local v0 # "latencyMs":I
} // :cond_1
} // :goto_0
return;
} // .end method
public void notifyDisplayStateChangeStartLocked ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "isFirstDisplay" # Z */
/* .line 1797 */
/* if-nez p1, :cond_0 */
/* .line 1798 */
return;
/* .line 1800 */
} // :cond_0
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mDisplayStateChangeStateTime:J */
/* .line 1801 */
return;
} // .end method
public void notifyGestureEvent ( java.lang.String p0, Boolean p1, Integer p2 ) {
/* .locals 1 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "success" # Z */
/* .param p3, "label" # I */
/* .line 1835 */
v0 = this.mMiuiPowerStatisticTracker;
/* if-nez v0, :cond_0 */
/* .line 1836 */
return;
/* .line 1839 */
} // :cond_0
(( com.android.server.power.statistic.MiuiPowerStatisticTracker ) v0 ).notifyGestureEvent ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->notifyGestureEvent(Ljava/lang/String;ZI)V
/* .line 1840 */
return;
} // .end method
public void notifyScreenOnUnBlocker ( Integer p0, Long p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "displayId" # I */
/* .param p2, "screenOnBlockStartRealTime" # J */
/* .param p4, "delayMiles" # I */
/* .line 1783 */
v0 = this.mMiuiPowerStatisticTracker;
if ( v0 != null) { // if-eqz v0, :cond_1
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 1787 */
} // :cond_0
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v0 */
/* sub-long/2addr v0, p2 */
/* long-to-int v0, v0 */
/* .line 1788 */
/* .local v0, "delay":I */
v1 = this.mMiuiPowerStatisticTracker;
(( com.android.server.power.statistic.MiuiPowerStatisticTracker ) v1 ).notifyScreenOnUnBlocker ( v0, p4 ); // invoke-virtual {v1, v0, p4}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->notifyScreenOnUnBlocker(II)V
/* .line 1789 */
return;
/* .line 1784 */
} // .end local v0 # "delay":I
} // :cond_1
} // :goto_0
return;
} // .end method
public void notifyStayOnChanged ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "stayOn" # Z */
/* .line 1876 */
v0 = this.mAttentionDetector;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1877 */
(( com.android.server.power.MiuiAttentionDetector ) v0 ).notifyStayOnChanged ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/power/MiuiAttentionDetector;->notifyStayOnChanged(Z)V
/* .line 1879 */
} // :cond_0
return;
} // .end method
public void notifyTofPowerState ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "wakeup" # Z */
/* .line 1826 */
v0 = this.mMiuiPowerStatisticTracker;
/* if-nez v0, :cond_0 */
/* .line 1827 */
return;
/* .line 1830 */
} // :cond_0
(( com.android.server.power.statistic.MiuiPowerStatisticTracker ) v0 ).notifyTofPowerState ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->notifyTofPowerState(Z)V
/* .line 1831 */
return;
} // .end method
public void notifyUserActivityTimeChanged ( ) {
/* .locals 1 */
/* .line 1869 */
v0 = this.mAttentionDetector;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1870 */
(( com.android.server.power.MiuiAttentionDetector ) v0 ).onUserActivityChanged ( ); // invoke-virtual {v0}, Lcom/android/server/power/MiuiAttentionDetector;->onUserActivityChanged()V
/* .line 1872 */
} // :cond_0
return;
} // .end method
public void notifyUserAttentionChanged ( Long p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "startTimeStamp" # J */
/* .param p3, "result" # I */
/* .line 1815 */
v0 = this.mMiuiPowerStatisticTracker;
/* if-nez v0, :cond_0 */
/* .line 1816 */
return;
/* .line 1819 */
} // :cond_0
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 1820 */
/* .local v0, "now":J */
/* sub-long v2, v0, p1 */
/* long-to-int v2, v2 */
/* .line 1821 */
/* .local v2, "latencyMs":I */
v3 = this.mMiuiPowerStatisticTracker;
(( com.android.server.power.statistic.MiuiPowerStatisticTracker ) v3 ).notifyUserAttentionChanged ( v2, p3 ); // invoke-virtual {v3, v2, p3}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->notifyUserAttentionChanged(II)V
/* .line 1822 */
return;
} // .end method
public void notifyWakefulnessChangedLocked ( Integer p0, Integer p1, Long p2, Integer p3, java.lang.String p4 ) {
/* .locals 1 */
/* .param p1, "groupId" # I */
/* .param p2, "wakefulness" # I */
/* .param p3, "eventTime" # J */
/* .param p5, "reason" # I */
/* .param p6, "details" # Ljava/lang/String; */
/* .line 1762 */
v0 = this.mMiuiPowerStatisticTracker;
if ( v0 != null) { // if-eqz v0, :cond_1
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 1766 */
} // :cond_0
(( com.android.server.power.statistic.MiuiPowerStatisticTracker ) v0 ).notifyWakefulnessChangedLocked ( p2, p3, p4, p5 ); // invoke-virtual {v0, p2, p3, p4, p5}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->notifyWakefulnessChangedLocked(IJI)V
/* .line 1767 */
com.android.server.wm.WindowManagerServiceStub .get ( );
/* .line 1768 */
return;
/* .line 1763 */
} // :cond_1
} // :goto_0
return;
} // .end method
public void notifyWakefulnessCompletedLocked ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "groupId" # I */
/* .line 1772 */
v0 = this.mMiuiPowerStatisticTracker;
if ( v0 != null) { // if-eqz v0, :cond_1
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 1776 */
} // :cond_0
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 1777 */
/* .local v0, "wakefulnessCompletedTime":J */
v2 = this.mMiuiPowerStatisticTracker;
(( com.android.server.power.statistic.MiuiPowerStatisticTracker ) v2 ).notifyWakefulnessCompletedLocked ( v0, v1 ); // invoke-virtual {v2, v0, v1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->notifyWakefulnessCompletedLocked(J)V
/* .line 1778 */
return;
/* .line 1773 */
} // .end local v0 # "wakefulnessCompletedTime":J
} // :cond_1
} // :goto_0
return;
} // .end method
public Boolean onTransact ( Integer p0, android.os.Parcel p1, android.os.Parcel p2, Integer p3 ) {
/* .locals 7 */
/* .param p1, "code" # I */
/* .param p2, "data" # Landroid/os/Parcel; */
/* .param p3, "reply" # Landroid/os/Parcel; */
/* .param p4, "flags" # I */
/* .line 1410 */
int v0 = 1; // const/4 v0, 0x1
final String v1 = "android.os.IPowerManager"; // const-string v1, "android.os.IPowerManager"
/* packed-switch p1, :pswitch_data_0 */
/* .line 1459 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1412 */
/* :pswitch_0 */
try { // :try_start_0
(( android.os.Parcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1413 */
(( android.os.Parcel ) p2 ).readLong ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J
/* move-result-wide v1 */
/* .line 1414 */
/* .local v1, "duration":J */
(( com.android.server.power.PowerManagerServiceImpl ) p0 ).requestDimmingRightNow ( v1, v2 ); // invoke-virtual {p0, v1, v2}, Lcom/android/server/power/PowerManagerServiceImpl;->requestDimmingRightNow(J)V
/* .line 1415 */
/* .line 1417 */
} // .end local v1 # "duration":J
/* :pswitch_1 */
(( android.os.Parcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1418 */
(( android.os.Parcel ) p2 ).readStrongBinder ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;
/* .line 1419 */
/* .local v1, "token":Landroid/os/IBinder; */
v2 = (( android.os.Parcel ) p2 ).readBoolean ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readBoolean()Z
/* .line 1420 */
/* .local v2, "hangup":Z */
/* invoke-direct {p0, v1, v2}, Lcom/android/server/power/PowerManagerServiceImpl;->requestHangUpWhenScreenProject(Landroid/os/IBinder;Z)V */
/* .line 1421 */
/* .line 1423 */
} // .end local v1 # "token":Landroid/os/IBinder;
} // .end local v2 # "hangup":Z
/* :pswitch_2 */
(( android.os.Parcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1424 */
(( android.os.Parcel ) p2 ).readLong ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J
/* move-result-wide v1 */
/* .line 1425 */
/* .local v1, "wakeUpTime":J */
(( android.os.Parcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;
/* .line 1426 */
/* .local v3, "wakeUpReason":Ljava/lang/String; */
/* invoke-direct {p0, v1, v2, v3}, Lcom/android/server/power/PowerManagerServiceImpl;->wakeUpSecondaryDisplay(JLjava/lang/String;)V */
/* .line 1427 */
/* .line 1429 */
} // .end local v1 # "wakeUpTime":J
} // .end local v3 # "wakeUpReason":Ljava/lang/String;
/* :pswitch_3 */
(( android.os.Parcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1430 */
(( android.os.Parcel ) p2 ).readLong ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J
/* move-result-wide v1 */
/* .line 1431 */
/* .local v1, "goToSleepTime":J */
(( android.os.Parcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;
/* .line 1432 */
/* .local v3, "goToSleepReason":Ljava/lang/String; */
/* invoke-direct {p0, v1, v2, v3}, Lcom/android/server/power/PowerManagerServiceImpl;->goToSleepSecondaryDisplay(JLjava/lang/String;)V */
/* .line 1433 */
/* .line 1435 */
} // .end local v1 # "goToSleepTime":J
} // .end local v3 # "goToSleepReason":Ljava/lang/String;
/* :pswitch_4 */
(( android.os.Parcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1436 */
(( android.os.Parcel ) p2 ).readStrongBinder ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;
/* .line 1437 */
/* .local v2, "token1":Landroid/os/IBinder; */
(( android.os.Parcel ) p2 ).readLong ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J
/* move-result-wide v3 */
/* .line 1438 */
/* .local v3, "wakeUpTime1":J */
v5 = (( android.os.Parcel ) p2 ).readInt ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
/* .line 1439 */
/* .local v5, "flag":I */
(( android.os.Parcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;
/* .line 1440 */
/* .local v6, "wakeUpReason1":Ljava/lang/String; */
/* move-object v1, p0 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/power/PowerManagerServiceImpl;->wakeUpSecondaryDisplay(Landroid/os/IBinder;JILjava/lang/String;)V */
/* .line 1441 */
/* .line 1443 */
} // .end local v2 # "token1":Landroid/os/IBinder;
} // .end local v3 # "wakeUpTime1":J
} // .end local v5 # "flag":I
} // .end local v6 # "wakeUpReason1":Ljava/lang/String;
/* :pswitch_5 */
(( android.os.Parcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1444 */
v1 = (( com.android.server.power.PowerManagerServiceImpl ) p0 ).getSecondaryDisplayWakefulnessLocked ( ); // invoke-virtual {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->getSecondaryDisplayWakefulnessLocked()I
(( android.os.Parcel ) p3 ).writeInt ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 1445 */
/* .line 1447 */
/* :pswitch_6 */
(( android.os.Parcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1448 */
(( android.os.Parcel ) p2 ).readStrongBinder ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;
/* .line 1449 */
/* .local v1, "token2":Landroid/os/IBinder; */
v2 = (( android.os.Parcel ) p2 ).readInt ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
/* .line 1450 */
/* .local v2, "flag2":I */
/* invoke-direct {p0, v1, v2}, Lcom/android/server/power/PowerManagerServiceImpl;->clearWakeUpFlags(Landroid/os/IBinder;I)V */
/* .line 1451 */
/* .line 1453 */
} // .end local v1 # "token2":Landroid/os/IBinder;
} // .end local v2 # "flag2":I
/* :pswitch_7 */
(( android.os.Parcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1454 */
(( android.os.Parcel ) p2 ).readStrongBinder ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;
/* .line 1455 */
/* .local v1, "token3":Landroid/os/IBinder; */
v2 = (( android.os.Parcel ) p2 ).readBoolean ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readBoolean()Z
/* .line 1456 */
/* .local v2, "enabled":Z */
/* invoke-direct {p0, v1, v2}, Lcom/android/server/power/PowerManagerServiceImpl;->boostScreenBrightness(Landroid/os/IBinder;Z)V */
/* :try_end_0 */
/* .catch Ljava/lang/RuntimeException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1457 */
/* .line 1461 */
} // .end local v1 # "token3":Landroid/os/IBinder;
} // .end local v2 # "enabled":Z
/* :catch_0 */
/* move-exception v0 */
/* .line 1462 */
/* .local v0, "e":Ljava/lang/RuntimeException; */
/* throw v0 */
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0xfffff7 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public void onUserSwitching ( ) {
/* .locals 0 */
/* .line 1199 */
/* invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updateAonScreenOffConfig()V */
/* .line 1200 */
return;
} // .end method
public void printPartialWakeLockDisabledIfNeeded ( com.android.server.power.PowerManagerService$WakeLock p0, Boolean p1, java.lang.String p2 ) {
/* .locals 2 */
/* .param p1, "wakeLock" # Lcom/android/server/power/PowerManagerService$WakeLock; */
/* .param p2, "disabled" # Z */
/* .param p3, "reason" # Ljava/lang/String; */
/* .line 596 */
/* iget-boolean v0, p1, Lcom/android/server/power/PowerManagerService$WakeLock;->mDisabled:Z */
/* if-eq v0, p2, :cond_0 */
/* .line 597 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Partial wakeLock:["; // const-string v1, "Partial wakeLock:["
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = "], disabled: "; // const-string v1, "], disabled: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = ", procState: "; // const-string v1, ", procState: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mUidState;
/* iget v1, v1, Lcom/android/server/power/PowerManagerService$UidState;->mProcState:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", reason: "; // const-string v1, ", reason: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "PowerManagerServiceImpl"; // const-string v1, "PowerManagerServiceImpl"
android.util.Slog .i ( v1,v0 );
/* .line 602 */
} // :cond_0
return;
} // .end method
protected void printWakeLockDetails ( com.android.server.power.PowerManagerService$WakeLock p0, Boolean p1 ) {
/* .locals 3 */
/* .param p1, "wakeLock" # Lcom/android/server/power/PowerManagerService$WakeLock; */
/* .param p2, "isAcquired" # Z */
/* .line 551 */
/* iget v0, p1, Lcom/android/server/power/PowerManagerService$WakeLock;->mFlags:I */
/* const v1, 0xffff */
/* and-int/2addr v0, v1 */
/* sparse-switch v0, :sswitch_data_0 */
/* .line 556 */
/* :sswitch_0 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 557 */
/* .local v0, "sb":Ljava/lang/StringBuilder; */
if ( p2 != null) { // if-eqz p2, :cond_0
final String v1 = "Acquire wakelock: "; // const-string v1, "Acquire wakelock: "
} // :cond_0
final String v1 = "Release wakelock: "; // const-string v1, "Release wakelock: "
} // :goto_0
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 558 */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
/* .line 559 */
final String v1 = "PowerManagerServiceImpl"; // const-string v1, "PowerManagerServiceImpl"
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v2 );
/* .line 560 */
/* nop */
/* .line 564 */
} // .end local v0 # "sb":Ljava/lang/StringBuilder;
} // :goto_1
return;
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x6 -> :sswitch_0 */
/* 0xa -> :sswitch_0 */
/* 0x1a -> :sswitch_0 */
/* 0x20 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
public java.lang.String reassignRebootReason ( Integer p0, Integer p1 ) {
/* .locals 8 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .line 1699 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1700 */
/* .local v0, "reason":Ljava/lang/String; */
final String v1 = "Unexpected reboot or shutdown reason is null, the reason will be reassign by uid and processname"; // const-string v1, "Unexpected reboot or shutdown reason is null, the reason will be reassign by uid and processname"
final String v2 = "PowerManagerServiceImpl"; // const-string v2, "PowerManagerServiceImpl"
android.util.Slog .w ( v2,v1 );
/* .line 1701 */
v1 = this.mContext;
final String v3 = "activity"; // const-string v3, "activity"
(( android.content.Context ) v1 ).getSystemService ( v3 ); // invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v1, Landroid/app/ActivityManager; */
/* .line 1702 */
/* .local v1, "am":Landroid/app/ActivityManager; */
int v3 = 0; // const/4 v3, 0x0
/* .line 1704 */
/* .local v3, "processName":Ljava/lang/String; */
try { // :try_start_0
(( android.app.ActivityManager ) v1 ).getRunningAppProcesses ( ); // invoke-virtual {v1}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;
/* .line 1705 */
/* .local v4, "runningProcesses":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;" */
v6 = } // :goto_0
if ( v6 != null) { // if-eqz v6, :cond_1
/* check-cast v6, Landroid/app/ActivityManager$RunningAppProcessInfo; */
/* .line 1706 */
/* .local v6, "processInfo":Landroid/app/ActivityManager$RunningAppProcessInfo; */
/* iget v7, v6, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I */
/* if-ne v7, p2, :cond_0 */
/* .line 1707 */
v2 = this.processName;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1708 */
} // .end local v3 # "processName":Ljava/lang/String;
/* .local v2, "processName":Ljava/lang/String; */
/* move-object v3, v2 */
/* .line 1710 */
} // .end local v2 # "processName":Ljava/lang/String;
} // .end local v6 # "processInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
/* .restart local v3 # "processName":Ljava/lang/String; */
} // :cond_0
/* .line 1713 */
} // .end local v4 # "runningProcesses":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
} // :cond_1
} // :goto_1
/* .line 1711 */
/* :catch_0 */
/* move-exception v4 */
/* .line 1712 */
/* .local v4, "e":Ljava/lang/Exception; */
final String v5 = "Failed to get running app processes from ActivityManager"; // const-string v5, "Failed to get running app processes from ActivityManager"
android.util.Slog .e ( v2,v5,v4 );
/* .line 1714 */
} // .end local v4 # "e":Ljava/lang/Exception;
} // :goto_2
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = ","; // const-string v4, ","
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* if-nez v3, :cond_2 */
java.lang.Integer .valueOf ( p2 );
} // :cond_2
/* move-object v4, v3 */
} // :goto_3
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1715 */
} // .end method
public Boolean recalculateGlobalWakefulnessSkipPowerGroup ( com.android.server.power.PowerGroup p0 ) {
/* .locals 1 */
/* .param p1, "powerGroup" # Lcom/android/server/power/PowerGroup; */
/* .line 1887 */
v0 = (( com.android.server.power.PowerGroup ) p1 ).getGroupId ( ); // invoke-virtual {p1}, Lcom/android/server/power/PowerGroup;->getGroupId()I
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public void recordShutDownTime ( ) {
/* .locals 11 */
/* .line 847 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/cache/recovery/last_utime"; // const-string v1, "/cache/recovery/last_utime"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 848 */
/* .local v0, "last_utime":Ljava/io/File; */
v1 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
final String v2 = "PowerManagerServiceImpl"; // const-string v2, "PowerManagerServiceImpl"
/* if-nez v1, :cond_0 */
/* .line 849 */
final String v1 = "last_utime doesn\'t exist"; // const-string v1, "last_utime doesn\'t exist"
android.util.Slog .e ( v2,v1 );
/* .line 850 */
return;
/* .line 854 */
} // :cond_0
try { // :try_start_0
/* new-instance v1, Ljava/io/BufferedReader; */
/* new-instance v3, Ljava/io/FileReader; */
/* invoke-direct {v3, v0}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V */
/* invoke-direct {v1, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* .line 855 */
/* .local v1, "reader":Ljava/io/BufferedReader; */
(( java.io.BufferedReader ) v1 ).readLine ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v4, v3 */
/* .local v4, "str":Ljava/lang/String; */
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 856 */
java.lang.Long .parseLong ( v4 );
/* move-result-wide v5 */
/* .line 857 */
/* .local v5, "usrConfirmTime":J */
/* const-wide/16 v7, 0x0 */
/* cmp-long v3, v5, v7 */
/* if-gtz v3, :cond_1 */
/* .line 858 */
final String v3 = "last_utime has invalid content"; // const-string v3, "last_utime has invalid content"
android.util.Slog .e ( v2,v3 );
/* .line 859 */
(( java.io.BufferedReader ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
/* .line 860 */
return;
/* .line 867 */
} // :cond_1
(( java.io.BufferedReader ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
/* .line 869 */
/* new-instance v3, Ljava/io/File; */
final String v7 = "/cache/recovery/last_shutdowntime"; // const-string v7, "/cache/recovery/last_shutdowntime"
/* invoke-direct {v3, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 870 */
/* .local v3, "last_ShutdownTime":Ljava/io/File; */
/* new-instance v7, Ljava/io/BufferedWriter; */
/* new-instance v8, Ljava/io/FileWriter; */
/* invoke-direct {v8, v3}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V */
/* invoke-direct {v7, v8}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V */
/* .line 871 */
/* .local v7, "writer":Ljava/io/BufferedWriter; */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v8 */
java.lang.Long .toString ( v8,v9 );
/* .line 872 */
/* .local v8, "buf":Ljava/lang/String; */
(( java.io.BufferedWriter ) v7 ).write ( v8 ); // invoke-virtual {v7, v8}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
/* .line 873 */
(( java.io.BufferedWriter ) v7 ).flush ( ); // invoke-virtual {v7}, Ljava/io/BufferedWriter;->flush()V
/* .line 874 */
(( java.io.BufferedWriter ) v7 ).close ( ); // invoke-virtual {v7}, Ljava/io/BufferedWriter;->close()V
/* .line 875 */
int v9 = 1; // const/4 v9, 0x1
int v10 = 0; // const/4 v10, 0x0
v9 = (( java.io.File ) v3 ).setReadable ( v9, v10 ); // invoke-virtual {v3, v9, v10}, Ljava/io/File;->setReadable(ZZ)Z
/* if-nez v9, :cond_2 */
/* .line 876 */
(( java.io.File ) v3 ).delete ( ); // invoke-virtual {v3}, Ljava/io/File;->delete()Z
/* .line 877 */
/* const-string/jumbo v9, "set last_shutdowntime readable failed" */
android.util.Slog .e ( v2,v9 );
/* .line 878 */
return;
/* .line 882 */
} // .end local v1 # "reader":Ljava/io/BufferedReader;
} // .end local v3 # "last_ShutdownTime":Ljava/io/File;
} // .end local v7 # "writer":Ljava/io/BufferedWriter;
} // :cond_2
/* .line 863 */
} // .end local v5 # "usrConfirmTime":J
} // .end local v8 # "buf":Ljava/lang/String;
/* .restart local v1 # "reader":Ljava/io/BufferedReader; */
} // :cond_3
final String v3 = "last_utime is blank"; // const-string v3, "last_utime is blank"
android.util.Slog .e ( v2,v3 );
/* .line 864 */
(( java.io.BufferedReader ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 865 */
return;
/* .line 880 */
} // .end local v1 # "reader":Ljava/io/BufferedReader;
} // .end local v4 # "str":Ljava/lang/String;
/* :catch_0 */
/* move-exception v1 */
/* .line 881 */
/* .local v1, "ex":Ljava/io/IOException; */
(( java.io.IOException ) v1 ).getMessage ( ); // invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;
android.util.Slog .e ( v2,v3 );
/* .line 883 */
} // .end local v1 # "ex":Ljava/io/IOException;
} // :goto_0
return;
} // .end method
protected void registerDeathCallbackLocked ( android.os.IBinder p0 ) {
/* .locals 2 */
/* .param p1, "token" # Landroid/os/IBinder; */
/* .line 1248 */
v0 = this.mClientDeathCallbacks;
v0 = (( java.util.HashMap ) v0 ).containsKey ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1249 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Client token "; // const-string v1, "Client token "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = " has already registered."; // const-string v1, " has already registered."
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "PowerManagerServiceImpl"; // const-string v1, "PowerManagerServiceImpl"
android.util.Slog .d ( v1,v0 );
/* .line 1250 */
return;
/* .line 1252 */
} // :cond_0
v0 = this.mClientDeathCallbacks;
/* new-instance v1, Lcom/android/server/power/PowerManagerServiceImpl$ClientDeathCallback; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/power/PowerManagerServiceImpl$ClientDeathCallback;-><init>(Lcom/android/server/power/PowerManagerServiceImpl;Landroid/os/IBinder;)V */
(( java.util.HashMap ) v0 ).put ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 1253 */
return;
} // .end method
protected void registerDeathCallbackLocked ( android.os.IBinder p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "token" # Landroid/os/IBinder; */
/* .param p2, "flag" # I */
/* .line 1240 */
v0 = this.mClientDeathCallbacks;
v0 = (( java.util.HashMap ) v0 ).containsKey ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1241 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Client token "; // const-string v1, "Client token "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = " has already registered."; // const-string v1, " has already registered."
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "PowerManagerServiceImpl"; // const-string v1, "PowerManagerServiceImpl"
android.util.Slog .d ( v1,v0 );
/* .line 1242 */
return;
/* .line 1244 */
} // :cond_0
v0 = this.mClientDeathCallbacks;
/* new-instance v1, Lcom/android/server/power/PowerManagerServiceImpl$ClientDeathCallback; */
/* invoke-direct {v1, p0, p1, p2}, Lcom/android/server/power/PowerManagerServiceImpl$ClientDeathCallback;-><init>(Lcom/android/server/power/PowerManagerServiceImpl;Landroid/os/IBinder;I)V */
(( java.util.HashMap ) v0 ).put ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 1245 */
return;
} // .end method
protected void requestDimmingRightNow ( Long p0 ) {
/* .locals 4 */
/* .param p1, "timeMillis" # J */
/* .line 1288 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 1290 */
/* .local v0, "ident":J */
/* const-wide/16 v2, 0x0 */
/* cmp-long v2, p1, v2 */
/* if-lez v2, :cond_0 */
/* .line 1291 */
try { // :try_start_0
(( com.android.server.power.PowerManagerServiceImpl ) p0 ).requestDimmingRightNowInternal ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/android/server/power/PowerManagerServiceImpl;->requestDimmingRightNowInternal(J)V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1294 */
/* :catchall_0 */
/* move-exception v2 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 1295 */
/* throw v2 */
/* .line 1294 */
} // :cond_0
} // :goto_0
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 1295 */
/* nop */
/* .line 1296 */
return;
} // .end method
public void requestDimmingRightNowDueToAttention ( ) {
/* .locals 6 */
/* .line 1060 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 1061 */
try { // :try_start_0
v1 = this.mPowerGroups;
int v2 = 0; // const/4 v2, 0x0
(( android.util.SparseArray ) v1 ).get ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v1, Lcom/android/server/power/PowerGroup; */
/* .line 1062 */
v1 = (( com.android.server.power.PowerGroup ) v1 ).getUserActivitySummaryLocked ( ); // invoke-virtual {v1}, Lcom/android/server/power/PowerGroup;->getUserActivitySummaryLocked()I
/* and-int/lit8 v1, v1, 0x2 */
int v3 = 1; // const/4 v3, 0x1
if ( v1 != null) { // if-eqz v1, :cond_0
/* move v2, v3 */
} // :cond_0
/* move v1, v2 */
/* .line 1064 */
/* .local v1, "isDimming":Z */
/* iget-boolean v2, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSituatedDimmingDueToAttention:Z */
/* if-nez v2, :cond_1 */
/* if-nez v1, :cond_1 */
/* .line 1066 */
(( com.android.server.power.PowerManagerServiceImpl ) p0 ).updateUserActivityLocked ( ); // invoke-virtual {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updateUserActivityLocked()V
/* .line 1068 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v4 */
/* iput-wide v4, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLastRequestDimmingTime:J */
/* .line 1069 */
/* iput-boolean v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSituatedDimmingDueToAttention:Z */
/* .line 1070 */
/* iput-boolean v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPendingSetDirtyDueToRequestDimming:Z */
/* .line 1071 */
v2 = this.mPowerManagerService;
(( com.android.server.power.PowerManagerService ) v2 ).updatePowerStateLocked ( ); // invoke-virtual {v2}, Lcom/android/server/power/PowerManagerService;->updatePowerStateLocked()V
/* .line 1073 */
} // .end local v1 # "isDimming":Z
} // :cond_1
/* monitor-exit v0 */
/* .line 1074 */
return;
/* .line 1073 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
protected void requestDimmingRightNowInternal ( Long p0 ) {
/* .locals 4 */
/* .param p1, "timeMillis" # J */
/* .line 1076 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 1077 */
try { // :try_start_0
/* iget-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSituatedDimmingDueToSynergy:Z */
/* if-nez v1, :cond_1 */
/* .line 1078 */
/* sget-boolean v1, Lcom/android/server/power/PowerManagerServiceImpl;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1079 */
final String v1 = "PowerManagerServiceImpl"; // const-string v1, "PowerManagerServiceImpl"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "requestDimmingRightNowInternal: timeout: "; // const-string v3, "requestDimmingRightNowInternal: timeout: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1, p2 ); // invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 1082 */
} // :cond_0
(( com.android.server.power.PowerManagerServiceImpl ) p0 ).updateUserActivityLocked ( ); // invoke-virtual {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updateUserActivityLocked()V
/* .line 1084 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
/* iput-wide v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLastRequestDimmingTime:J */
/* .line 1085 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSituatedDimmingDueToSynergy:Z */
/* .line 1086 */
/* iput-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPendingSetDirtyDueToRequestDimming:Z */
/* .line 1087 */
/* iput-wide p1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mUserActivityTimeoutOverrideFromMirrorManager:J */
/* .line 1088 */
v1 = this.mPowerManagerService;
(( com.android.server.power.PowerManagerService ) v1 ).updatePowerStateLocked ( ); // invoke-virtual {v1}, Lcom/android/server/power/PowerManagerService;->updatePowerStateLocked()V
/* .line 1090 */
} // :cond_1
/* monitor-exit v0 */
/* .line 1091 */
return;
/* .line 1090 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void resetBoostBrightnessIfNeededLocked ( ) {
/* .locals 1 */
/* .line 1692 */
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPendingStopBrightnessBoost:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1693 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPendingStopBrightnessBoost:Z */
/* .line 1695 */
} // :cond_0
return;
} // .end method
protected void sendBroadcastRestoreBrightnessIfNeededLocked ( ) {
/* .locals 1 */
/* .line 1213 */
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mDimEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mIsBrightWakeLock:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1214 */
v0 = this.mNotifierInjector;
(( com.android.server.power.NotifierInjector ) v0 ).sendBroadcastRestoreBrightness ( ); // invoke-virtual {v0}, Lcom/android/server/power/NotifierInjector;->sendBroadcastRestoreBrightness()V
/* .line 1216 */
} // :cond_0
return;
} // .end method
public void setBootPhase ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "phase" # I */
/* .line 922 */
/* const/16 v0, 0x3e8 */
/* if-ne p1, v0, :cond_0 */
/* .line 923 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mBootCompleted:Z */
/* .line 926 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/power/PowerManagerServiceImpl$$ExternalSyntheticLambda2; */
/* invoke-direct {v1}, Lcom/android/server/power/PowerManagerServiceImpl$$ExternalSyntheticLambda2;-><init>()V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 928 */
} // :cond_0
return;
} // .end method
public void setPickUpSensorEnable ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "isDimming" # Z */
/* .line 354 */
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSupportAdaptiveSleep:Z */
/* if-nez v0, :cond_0 */
/* sget-boolean v0, Lcom/android/server/power/MiuiAttentionDetector;->AON_SCREEN_OFF_SUPPORTED:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mAdaptiveSleepEnabled:Z */
/* if-nez v0, :cond_1 */
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mAonScreenOffEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
} // :cond_1
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPickUpGestureWakeUpEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = this.mPolicy;
v0 = /* .line 357 */
/* if-nez v0, :cond_2 */
if ( p1 != null) { // if-eqz p1, :cond_2
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPickUpSensorEnabled:Z */
/* if-nez v0, :cond_2 */
/* .line 360 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPickUpSensorEnabled:Z */
/* .line 361 */
v0 = this.mSensorManager;
v1 = this.mPickUpSensorListener;
v2 = this.mPickUpSensor;
int v3 = 3; // const/4 v3, 0x3
(( android.hardware.SensorManager ) v0 ).registerListener ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z
/* .line 363 */
} // :cond_2
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPickUpSensorEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* if-nez p1, :cond_3 */
/* .line 364 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPickUpSensorEnabled:Z */
/* .line 365 */
v0 = this.mSensorManager;
v1 = this.mPickUpSensorListener;
(( android.hardware.SensorManager ) v0 ).unregisterListener ( v1 ); // invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V
/* .line 367 */
} // :cond_3
} // :goto_0
return;
} // .end method
protected void setProximityWakeLockDisabledStateLocked ( com.android.server.power.PowerManagerService$WakeLock p0, Boolean p1 ) {
/* .locals 6 */
/* .param p1, "wakeLock" # Lcom/android/server/power/PowerManagerService$WakeLock; */
/* .param p2, "notAcquired" # Z */
/* .line 464 */
/* sget-boolean v0, Lcom/android/server/power/PowerManagerServiceImpl;->OPTIMIZE_WAKELOCK_ENABLED:Z */
if ( v0 != null) { // if-eqz v0, :cond_9
/* iget v0, p1, Lcom/android/server/power/PowerManagerService$WakeLock;->mOwnerUid:I */
v0 = android.os.UserHandle .isApp ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_9
/* iget v0, p1, Lcom/android/server/power/PowerManagerService$WakeLock;->mFlags:I */
/* const v1, 0xffff */
/* and-int/2addr v0, v1 */
/* const/16 v1, 0x20 */
/* if-ne v0, v1, :cond_9 */
/* .line 467 */
v0 = this.mPackageName;
v1 = this.mForegroundAppPackageName;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* .line 468 */
/* .local v0, "isInForeground":Z */
/* sget-boolean v1, Lcom/android/server/power/PowerManagerServiceImpl;->IS_FOLDABLE_DEVICE:Z */
int v2 = 0; // const/4 v2, 0x0
int v3 = 1; // const/4 v3, 0x1
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = this.mFolded;
v1 = (( java.lang.Boolean ) v1 ).booleanValue ( ); // invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
/* if-nez v1, :cond_0 */
/* move v1, v3 */
} // :cond_0
/* move v1, v2 */
/* .line 469 */
/* .local v1, "isUnfolded":Z */
} // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* iget-boolean v4, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mNoSupprotInnerScreenProximitySensor:Z */
if ( v4 != null) { // if-eqz v4, :cond_1
/* move v4, v3 */
} // :cond_1
/* move v4, v2 */
/* .line 470 */
/* .local v4, "isDisabledProximitySensor":Z */
} // :goto_1
if ( p2 != null) { // if-eqz p2, :cond_6
/* .line 471 */
if ( v0 != null) { // if-eqz v0, :cond_2
/* iget-boolean v5, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mIsFreeFormOrSplitWindowMode:Z */
/* if-nez v5, :cond_2 */
if ( v4 != null) { // if-eqz v4, :cond_3
} // :cond_2
/* move v2, v3 */
/* .line 473 */
/* .local v2, "shouldDisabled":Z */
} // :cond_3
/* iget-boolean v3, p1, Lcom/android/server/power/PowerManagerService$WakeLock;->mDisabled:Z */
/* if-eq v2, v3, :cond_5 */
/* .line 474 */
(( com.android.server.power.PowerManagerService$WakeLock ) p1 ).setDisabled ( v2 ); // invoke-virtual {p1, v2}, Lcom/android/server/power/PowerManagerService$WakeLock;->setDisabled(Z)Z
/* .line 475 */
/* invoke-direct {p0, p1, v2}, Lcom/android/server/power/PowerManagerServiceImpl;->printProximityWakeLockStatusChange(Lcom/android/server/power/PowerManagerService$WakeLock;Z)V */
/* .line 477 */
/* iget-boolean v3, p1, Lcom/android/server/power/PowerManagerService$WakeLock;->mDisabled:Z */
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 478 */
v3 = this.mPowerManagerService;
(( com.android.server.power.PowerManagerService ) v3 ).notifyWakeLockReleasedLocked ( p1 ); // invoke-virtual {v3, p1}, Lcom/android/server/power/PowerManagerService;->notifyWakeLockReleasedLocked(Lcom/android/server/power/PowerManagerService$WakeLock;)V
/* .line 480 */
} // :cond_4
v3 = this.mPowerManagerService;
(( com.android.server.power.PowerManagerService ) v3 ).notifyWakeLockAcquiredLocked ( p1 ); // invoke-virtual {v3, p1}, Lcom/android/server/power/PowerManagerService;->notifyWakeLockAcquiredLocked(Lcom/android/server/power/PowerManagerService$WakeLock;)V
/* .line 482 */
} // :goto_2
v3 = this.mPowerManagerService;
(( com.android.server.power.PowerManagerService ) v3 ).setWakeLockDirtyLocked ( ); // invoke-virtual {v3}, Lcom/android/server/power/PowerManagerService;->setWakeLockDirtyLocked()V
/* .line 483 */
v3 = this.mPowerManagerService;
(( com.android.server.power.PowerManagerService ) v3 ).updatePowerStateLocked ( ); // invoke-virtual {v3}, Lcom/android/server/power/PowerManagerService;->updatePowerStateLocked()V
/* .line 485 */
} // .end local v2 # "shouldDisabled":Z
} // :cond_5
/* .line 487 */
} // :cond_6
v2 = v2 = this.mPolicy;
/* if-nez v2, :cond_7 */
if ( v0 != null) { // if-eqz v0, :cond_8
} // :cond_7
/* iget-boolean v2, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mIsFreeFormOrSplitWindowMode:Z */
/* if-nez v2, :cond_8 */
if ( v4 != null) { // if-eqz v4, :cond_9
/* .line 489 */
} // :cond_8
(( com.android.server.power.PowerManagerService$WakeLock ) p1 ).setDisabled ( v3 ); // invoke-virtual {p1, v3}, Lcom/android/server/power/PowerManagerService$WakeLock;->setDisabled(Z)Z
/* .line 491 */
/* invoke-direct {p0, p1, v3}, Lcom/android/server/power/PowerManagerServiceImpl;->printProximityWakeLockStatusChange(Lcom/android/server/power/PowerManagerService$WakeLock;Z)V */
/* .line 495 */
} // .end local v0 # "isInForeground":Z
} // .end local v1 # "isUnfolded":Z
} // .end local v4 # "isDisabledProximitySensor":Z
} // :cond_9
} // :goto_3
return;
} // .end method
public void setTouchInteractive ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "isDimming" # Z */
/* .line 607 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mTouchInteractiveUnavailable:Z */
/* if-nez v0, :cond_0 */
/* .line 608 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mTouchInteractiveUnavailable:Z */
/* .line 609 */
v1 = this.mMiuiInputManagerInternal;
(( com.android.server.input.MiuiInputManagerInternal ) v1 ).setDimState ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/input/MiuiInputManagerInternal;->setDimState(Z)V
/* .line 610 */
} // :cond_0
/* if-nez p1, :cond_1 */
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mTouchInteractiveUnavailable:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 611 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mTouchInteractiveUnavailable:Z */
/* .line 612 */
v1 = this.mMiuiInputManagerInternal;
(( com.android.server.input.MiuiInputManagerInternal ) v1 ).setDimState ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/input/MiuiInputManagerInternal;->setDimState(Z)V
/* .line 614 */
} // :cond_1
} // :goto_0
return;
} // .end method
public void setUidPartialWakeLockDisabledState ( Integer p0, java.lang.String p1, Boolean p2 ) {
/* .locals 9 */
/* .param p1, "uid" # I */
/* .param p2, "tag" # Ljava/lang/String; */
/* .param p3, "disabled" # Z */
/* .line 788 */
/* if-nez p2, :cond_1 */
v0 = android.os.UserHandle .isApp ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 789 */
} // :cond_0
/* new-instance v0, Ljava/lang/IllegalArgumentException; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "can not disable all wakelock for uid "; // const-string v2, "can not disable all wakelock for uid "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
/* .line 792 */
} // :cond_1
} // :goto_0
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 793 */
try { // :try_start_0
v1 = this.mWakeLocks;
(( java.util.ArrayList ) v1 ).iterator ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v2 = } // :goto_1
if ( v2 != null) { // if-eqz v2, :cond_7
/* check-cast v2, Lcom/android/server/power/PowerManagerService$WakeLock; */
/* .line 794 */
/* .local v2, "wakeLock":Lcom/android/server/power/PowerManagerService$WakeLock; */
int v3 = 0; // const/4 v3, 0x0
/* .line 795 */
/* .local v3, "changed":Z */
/* iget v4, v2, Lcom/android/server/power/PowerManagerService$WakeLock;->mFlags:I */
/* const v5, 0xffff */
/* and-int/2addr v4, v5 */
/* packed-switch v4, :pswitch_data_0 */
/* .line 797 */
/* :pswitch_0 */
/* invoke-direct {p0, v2}, Lcom/android/server/power/PowerManagerServiceImpl;->getRealOwners(Lcom/android/server/power/PowerManagerService$WakeLock;)[I */
/* .line 798 */
/* .local v4, "realOwners":[I */
/* array-length v5, v4 */
int v6 = 0; // const/4 v6, 0x0
} // :goto_2
/* if-ge v6, v5, :cond_4 */
/* aget v7, v4, v6 */
/* .line 799 */
/* .local v7, "realOwner":I */
/* if-ne v7, p1, :cond_3 */
if ( p2 != null) { // if-eqz p2, :cond_2
v8 = this.mTag;
v8 = (( java.lang.String ) p2 ).equals ( v8 ); // invoke-virtual {p2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v8 != null) { // if-eqz v8, :cond_3
/* .line 800 */
} // :cond_2
v5 = /* invoke-direct {p0, v2, p3}, Lcom/android/server/power/PowerManagerServiceImpl;->setWakeLockDisabledStateLocked(Lcom/android/server/power/PowerManagerService$WakeLock;Z)Z */
/* move v3, v5 */
/* .line 801 */
/* .line 798 */
} // .end local v7 # "realOwner":I
} // :cond_3
/* add-int/lit8 v6, v6, 0x1 */
/* .line 804 */
} // :cond_4
} // :goto_3
/* nop */
/* .line 808 */
} // .end local v4 # "realOwners":[I
} // :goto_4
if ( v3 != null) { // if-eqz v3, :cond_6
/* .line 809 */
/* iget-boolean v4, v2, Lcom/android/server/power/PowerManagerService$WakeLock;->mDisabled:Z */
if ( v4 != null) { // if-eqz v4, :cond_5
/* .line 810 */
v4 = this.mPowerManagerService;
(( com.android.server.power.PowerManagerService ) v4 ).notifyWakeLockReleasedLocked ( v2 ); // invoke-virtual {v4, v2}, Lcom/android/server/power/PowerManagerService;->notifyWakeLockReleasedLocked(Lcom/android/server/power/PowerManagerService$WakeLock;)V
/* .line 812 */
} // :cond_5
v4 = this.mPowerManagerService;
(( com.android.server.power.PowerManagerService ) v4 ).notifyWakeLockAcquiredLocked ( v2 ); // invoke-virtual {v4, v2}, Lcom/android/server/power/PowerManagerService;->notifyWakeLockAcquiredLocked(Lcom/android/server/power/PowerManagerService$WakeLock;)V
/* .line 814 */
} // :goto_5
/* const-string/jumbo v4, "unknown" */
(( com.android.server.power.PowerManagerServiceImpl ) p0 ).printPartialWakeLockDisabledIfNeeded ( v2, p3, v4 ); // invoke-virtual {p0, v2, p3, v4}, Lcom/android/server/power/PowerManagerServiceImpl;->printPartialWakeLockDisabledIfNeeded(Lcom/android/server/power/PowerManagerService$WakeLock;ZLjava/lang/String;)V
/* .line 815 */
v4 = this.mPowerManagerService;
(( com.android.server.power.PowerManagerService ) v4 ).setWakeLockDirtyLocked ( ); // invoke-virtual {v4}, Lcom/android/server/power/PowerManagerService;->setWakeLockDirtyLocked()V
/* .line 816 */
v4 = this.mPowerManagerService;
(( com.android.server.power.PowerManagerService ) v4 ).updatePowerStateLocked ( ); // invoke-virtual {v4}, Lcom/android/server/power/PowerManagerService;->updatePowerStateLocked()V
/* .line 818 */
} // .end local v2 # "wakeLock":Lcom/android/server/power/PowerManagerService$WakeLock;
} // .end local v3 # "changed":Z
} // :cond_6
/* .line 819 */
} // :cond_7
/* monitor-exit v0 */
/* .line 820 */
return;
/* .line 819 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public void setUidPartialWakeLockDisabledState ( Integer p0, java.lang.String p1, Boolean p2, java.lang.String p3 ) {
/* .locals 10 */
/* .param p1, "uid" # I */
/* .param p2, "tag" # Ljava/lang/String; */
/* .param p3, "disabled" # Z */
/* .param p4, "reason" # Ljava/lang/String; */
/* .line 750 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 751 */
int v1 = 0; // const/4 v1, 0x0
/* .line 752 */
/* .local v1, "ret":Z */
try { // :try_start_0
v2 = this.mWakeLocks;
(( java.util.ArrayList ) v2 ).iterator ( ); // invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_5
/* check-cast v3, Lcom/android/server/power/PowerManagerService$WakeLock; */
/* .line 753 */
/* .local v3, "wakeLock":Lcom/android/server/power/PowerManagerService$WakeLock; */
int v4 = 0; // const/4 v4, 0x0
/* .line 754 */
/* .local v4, "changed":Z */
/* iget v5, v3, Lcom/android/server/power/PowerManagerService$WakeLock;->mFlags:I */
/* const v6, 0xffff */
/* and-int/2addr v5, v6 */
/* packed-switch v5, :pswitch_data_0 */
/* .line 756 */
/* :pswitch_0 */
/* invoke-direct {p0, v3}, Lcom/android/server/power/PowerManagerServiceImpl;->getRealOwners(Lcom/android/server/power/PowerManagerService$WakeLock;)[I */
/* .line 757 */
/* .local v5, "realOwners":[I */
/* array-length v6, v5 */
int v7 = 0; // const/4 v7, 0x0
} // :goto_1
/* if-ge v7, v6, :cond_2 */
/* aget v8, v5, v7 */
/* .line 758 */
/* .local v8, "realOwner":I */
/* if-ne v8, p1, :cond_1 */
if ( p2 != null) { // if-eqz p2, :cond_0
v9 = this.mTag;
v9 = (( java.lang.String ) p2 ).equals ( v9 ); // invoke-virtual {p2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v9 != null) { // if-eqz v9, :cond_1
/* .line 759 */
} // :cond_0
/* iget-boolean v9, v3, Lcom/android/server/power/PowerManagerService$WakeLock;->mDisabled:Z */
/* if-eq v9, p3, :cond_1 */
/* .line 760 */
(( com.android.server.power.PowerManagerServiceImpl ) p0 ).printPartialWakeLockDisabledIfNeeded ( v3, p3, p4 ); // invoke-virtual {p0, v3, p3, p4}, Lcom/android/server/power/PowerManagerServiceImpl;->printPartialWakeLockDisabledIfNeeded(Lcom/android/server/power/PowerManagerService$WakeLock;ZLjava/lang/String;)V
/* .line 761 */
/* iput-boolean p3, v3, Lcom/android/server/power/PowerManagerService$WakeLock;->mDisabled:Z */
/* .line 762 */
int v4 = 1; // const/4 v4, 0x1
/* .line 757 */
} // .end local v8 # "realOwner":I
} // :cond_1
/* add-int/lit8 v7, v7, 0x1 */
/* .line 766 */
} // :cond_2
/* nop */
/* .line 770 */
} // .end local v5 # "realOwners":[I
} // :goto_2
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 771 */
/* iget-boolean v5, v3, Lcom/android/server/power/PowerManagerService$WakeLock;->mDisabled:Z */
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 772 */
v5 = this.mPowerManagerService;
(( com.android.server.power.PowerManagerService ) v5 ).notifyWakeLockReleasedLocked ( v3 ); // invoke-virtual {v5, v3}, Lcom/android/server/power/PowerManagerService;->notifyWakeLockReleasedLocked(Lcom/android/server/power/PowerManagerService$WakeLock;)V
/* .line 774 */
} // :cond_3
v5 = this.mPowerManagerService;
(( com.android.server.power.PowerManagerService ) v5 ).notifyWakeLockAcquiredLocked ( v3 ); // invoke-virtual {v5, v3}, Lcom/android/server/power/PowerManagerService;->notifyWakeLockAcquiredLocked(Lcom/android/server/power/PowerManagerService$WakeLock;)V
/* .line 776 */
} // :goto_3
int v1 = 1; // const/4 v1, 0x1
/* .line 778 */
} // .end local v3 # "wakeLock":Lcom/android/server/power/PowerManagerService$WakeLock;
} // .end local v4 # "changed":Z
} // :cond_4
/* .line 779 */
} // :cond_5
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 780 */
v2 = this.mPowerManagerService;
(( com.android.server.power.PowerManagerService ) v2 ).setWakeLockDirtyLocked ( ); // invoke-virtual {v2}, Lcom/android/server/power/PowerManagerService;->setWakeLockDirtyLocked()V
/* .line 781 */
v2 = this.mPowerManagerService;
(( com.android.server.power.PowerManagerService ) v2 ).updatePowerStateLocked ( ); // invoke-virtual {v2}, Lcom/android/server/power/PowerManagerService;->updatePowerStateLocked()V
/* .line 783 */
} // .end local v1 # "ret":Z
} // :cond_6
/* monitor-exit v0 */
/* .line 784 */
return;
/* .line 783 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public Boolean shouldAlwaysWakeUpSecondaryDisplay ( ) {
/* .locals 1 */
/* .line 1669 */
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mAlwaysWakeUp:Z */
} // .end method
public Boolean shouldEnterHangupLocked ( ) {
/* .locals 1 */
/* .line 976 */
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mScreenProjectionEnabled:Z */
/* if-nez v0, :cond_1 */
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSynergyModeEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
protected Long shouldOverrideUserActivityTimeoutLocked ( Long p0, Long p1, Float p2 ) {
/* .locals 4 */
/* .param p1, "timeout" # J */
/* .param p3, "maximumScreenDimDurationConfig" # J */
/* .param p5, "maximumScreenDimRatioConfig" # F */
/* .line 983 */
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSynergyModeEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-wide v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mUserActivityTimeoutOverrideFromMirrorManager:J */
/* const-wide/16 v2, 0x0 */
/* cmp-long v2, v0, v2 */
/* if-ltz v2, :cond_0 */
/* .line 984 */
java.lang.Math .min ( p1,p2,v0,v1 );
/* move-result-wide p1 */
/* .line 986 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSituatedDimmingDueToAttention:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 987 */
/* long-to-float v0, p1 */
/* mul-float/2addr v0, p5 */
/* float-to-long v0, v0 */
java.lang.Math .min ( p3,p4,v0,v1 );
/* move-result-wide p1 */
/* .line 990 */
} // :cond_1
/* return-wide p1 */
} // .end method
public Boolean shouldRequestDisplayHangupLocked ( ) {
/* .locals 4 */
/* .line 932 */
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSynergyModeEnable:Z */
int v1 = 4; // const/4 v1, 0x4
int v2 = 1; // const/4 v2, 0x1
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPreWakefulness:I */
/* if-ne v3, v1, :cond_1 */
/* iget v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mWakefulness:I */
/* if-ne v3, v2, :cond_1 */
/* iget-boolean v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mIsKeyguardHasShownWhenExitHangup:Z */
/* if-nez v3, :cond_1 */
/* .line 936 */
v0 = com.android.server.wm.WindowManagerServiceStub .get ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 937 */
/* iput-boolean v2, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mIsKeyguardHasShownWhenExitHangup:Z */
/* .line 940 */
} // :cond_0
/* .line 942 */
} // :cond_1
/* iget-boolean v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mScreenProjectionEnabled:Z */
/* if-nez v3, :cond_2 */
if ( v0 != null) { // if-eqz v0, :cond_3
} // :cond_2
/* iget v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mWakefulness:I */
/* if-ne v0, v1, :cond_3 */
} // :cond_3
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
} // .end method
public Boolean shouldWakeUpWhenPluggedOrUnpluggedSkipByMiui ( ) {
/* .locals 1 */
/* .line 1896 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
protected void unregisterDeathCallbackLocked ( android.os.IBinder p0 ) {
/* .locals 2 */
/* .param p1, "token" # Landroid/os/IBinder; */
/* .line 1257 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 1258 */
v0 = this.mClientDeathCallbacks;
(( java.util.HashMap ) v0 ).remove ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/power/PowerManagerServiceImpl$ClientDeathCallback; */
/* .line 1259 */
/* .local v0, "deathCallback":Lcom/android/server/power/PowerManagerServiceImpl$ClientDeathCallback; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1260 */
int v1 = 0; // const/4 v1, 0x0
/* .line 1263 */
} // .end local v0 # "deathCallback":Lcom/android/server/power/PowerManagerServiceImpl$ClientDeathCallback;
} // :cond_0
return;
} // .end method
public void updateAllPartialWakeLockDisableState ( ) {
/* .locals 6 */
/* .line 729 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 730 */
int v1 = 0; // const/4 v1, 0x0
/* .line 731 */
/* .local v1, "changed":Z */
try { // :try_start_0
v2 = this.mWakeLocks;
(( java.util.ArrayList ) v2 ).iterator ( ); // invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_0
/* check-cast v3, Lcom/android/server/power/PowerManagerService$WakeLock; */
/* .line 732 */
/* .local v3, "wakeLock":Lcom/android/server/power/PowerManagerService$WakeLock; */
/* iget v4, v3, Lcom/android/server/power/PowerManagerService$WakeLock;->mFlags:I */
/* const v5, 0xffff */
/* and-int/2addr v4, v5 */
/* packed-switch v4, :pswitch_data_0 */
/* .line 734 */
/* :pswitch_0 */
v4 = (( com.android.server.power.PowerManagerServiceImpl ) p0 ).isWakelockDisabledByPolicy ( v3 ); // invoke-virtual {p0, v3}, Lcom/android/server/power/PowerManagerServiceImpl;->isWakelockDisabledByPolicy(Lcom/android/server/power/PowerManagerService$WakeLock;)Z
/* .line 735 */
/* .local v4, "disabled":Z */
v5 = /* invoke-direct {p0, v3, v4}, Lcom/android/server/power/PowerManagerServiceImpl;->setWakeLockDisabledStateLocked(Lcom/android/server/power/PowerManagerService$WakeLock;Z)Z */
/* or-int/2addr v1, v5 */
/* .line 736 */
/* nop */
/* .line 740 */
} // .end local v3 # "wakeLock":Lcom/android/server/power/PowerManagerService$WakeLock;
} // .end local v4 # "disabled":Z
} // :goto_1
/* .line 741 */
} // :cond_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 742 */
v2 = this.mPowerManagerService;
(( com.android.server.power.PowerManagerService ) v2 ).setWakeLockDirtyLocked ( ); // invoke-virtual {v2}, Lcom/android/server/power/PowerManagerService;->setWakeLockDirtyLocked()V
/* .line 743 */
v2 = this.mPowerManagerService;
(( com.android.server.power.PowerManagerService ) v2 ).updatePowerStateLocked ( ); // invoke-virtual {v2}, Lcom/android/server/power/PowerManagerService;->updatePowerStateLocked()V
/* .line 745 */
} // .end local v1 # "changed":Z
} // :cond_1
/* monitor-exit v0 */
/* .line 746 */
return;
/* .line 745 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public void updateBlockUntilScreenOnReady ( com.android.server.power.PowerGroup p0, android.hardware.display.DisplayManagerInternal$DisplayPowerRequest p1 ) {
/* .locals 2 */
/* .param p1, "powerGroup" # Lcom/android/server/power/PowerGroup; */
/* .param p2, "displayPowerRequest" # Landroid/hardware/display/DisplayManagerInternal$DisplayPowerRequest; */
/* .line 1920 */
/* nop */
/* .line 1921 */
v0 = (( com.android.server.power.PowerGroup ) p1 ).getGroupId ( ); // invoke-virtual {p1}, Lcom/android/server/power/PowerGroup;->getGroupId()I
/* if-nez v0, :cond_1 */
/* .line 1922 */
v0 = (( com.android.server.power.PowerGroup ) p1 ).isPoweringOnLocked ( ); // invoke-virtual {p1}, Lcom/android/server/power/PowerGroup;->isPoweringOnLocked()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget v0, p2, Landroid/hardware/display/DisplayManagerInternal$DisplayPowerRequest;->policy:I */
int v1 = 3; // const/4 v1, 0x3
/* if-ne v0, v1, :cond_1 */
/* sget-boolean v0, Lcom/android/server/power/PowerManagerServiceImpl;->SUPPORT_FOD:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = com.android.server.power.PowerManagerServiceImpl.SKIP_TRANSITION_WAKE_UP_DETAILS;
/* .line 1925 */
(( com.android.server.power.PowerGroup ) p1 ).getLastWakeReasonDetails ( ); // invoke-virtual {p1}, Lcom/android/server/power/PowerGroup;->getLastWakeReasonDetails()Ljava/lang/String;
v0 = /* .line 1924 */
/* if-nez v0, :cond_1 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
/* iput-boolean v0, p2, Landroid/hardware/display/DisplayManagerInternal$DisplayPowerRequest;->setBrightnessUntilScreenOnReady:Z */
/* .line 1926 */
return;
} // .end method
public void updateDimState ( Boolean p0, Long p1 ) {
/* .locals 2 */
/* .param p1, "dimEnable" # Z */
/* .param p2, "brightWaitTime" # J */
/* .line 1204 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mIsBrightWakeLock:Z */
/* .line 1205 */
/* iput-boolean p1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mDimEnable:Z */
/* .line 1206 */
/* iput-wide p2, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mBrightWaitTime:J */
/* .line 1207 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 1208 */
try { // :try_start_0
v1 = this.mPowerManagerService;
(( com.android.server.power.PowerManagerService ) v1 ).updatePowerStateLocked ( ); // invoke-virtual {v1}, Lcom/android/server/power/PowerManagerService;->updatePowerStateLocked()V
/* .line 1209 */
/* monitor-exit v0 */
/* .line 1210 */
return;
/* .line 1209 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void updateLowPowerModeIfNeeded ( android.hardware.display.DisplayManagerInternal$DisplayPowerRequest p0 ) {
/* .locals 1 */
/* .param p1, "displayPowerRequest" # Landroid/hardware/display/DisplayManagerInternal$DisplayPowerRequest; */
/* .line 1652 */
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mIsPowerSaveModeEnabled:Z */
/* if-nez v0, :cond_0 */
/* .line 1653 */
return;
/* .line 1655 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p1, Landroid/hardware/display/DisplayManagerInternal$DisplayPowerRequest;->lowPowerMode:Z */
/* .line 1656 */
/* iput v0, p1, Landroid/hardware/display/DisplayManagerInternal$DisplayPowerRequest;->screenLowPowerBrightnessFactor:F */
/* .line 1657 */
return;
} // .end method
protected Long updateNextDimTimeoutIfNeededLocked ( Long p0, Long p1 ) {
/* .locals 5 */
/* .param p1, "nextTimeout" # J */
/* .param p3, "lastUserActivityTime" # J */
/* .line 1114 */
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSynergyModeEnable:Z */
/* const-wide/16 v1, 0x0 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSituatedDimmingDueToSynergy:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-wide v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mUserActivityTimeoutOverrideFromMirrorManager:J */
/* cmp-long v0, v3, v1 */
/* if-ltz v0, :cond_0 */
/* iget-wide v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLastRequestDimmingTime:J */
/* cmp-long v0, v3, v1 */
/* if-lez v0, :cond_0 */
/* .line 1117 */
/* iget-wide p1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLastRequestDimmingTime:J */
/* .line 1118 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mDimEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mIsBrightWakeLock:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1119 */
/* iget-wide v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mBrightWaitTime:J */
/* add-long p1, p3, v0 */
/* .line 1120 */
} // :cond_1
/* iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSituatedDimmingDueToAttention:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* iget-wide v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLastRequestDimmingTime:J */
/* cmp-long v0, v3, v1 */
/* if-lez v0, :cond_2 */
/* .line 1121 */
java.lang.Math .min ( p1,p2,v3,v4 );
/* move-result-wide p1 */
/* .line 1123 */
} // :cond_2
} // :goto_0
/* return-wide p1 */
} // .end method
public void updatePowerGroupPolicy ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "groupId" # I */
/* .param p2, "policy" # I */
/* .line 1977 */
/* if-nez p1, :cond_1 */
/* .line 1978 */
int v0 = 2; // const/4 v0, 0x2
/* if-ne v0, p2, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 1979 */
/* .local v0, "isDimming":Z */
} // :goto_0
(( com.android.server.power.PowerManagerServiceImpl ) p0 ).setPickUpSensorEnable ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/power/PowerManagerServiceImpl;->setPickUpSensorEnable(Z)V
/* .line 1980 */
(( com.android.server.power.PowerManagerServiceImpl ) p0 ).setTouchInteractive ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/power/PowerManagerServiceImpl;->setTouchInteractive(Z)V
/* .line 1982 */
} // .end local v0 # "isDimming":Z
} // :cond_1
return;
} // .end method
protected void updateUserActivityLocked ( ) {
/* .locals 4 */
/* .line 1127 */
v0 = this.mPowerManager;
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
int v3 = 0; // const/4 v3, 0x0
(( android.os.PowerManager ) v0 ).userActivity ( v1, v2, v3, v3 ); // invoke-virtual {v0, v1, v2, v3, v3}, Landroid/os/PowerManager;->userActivity(JII)V
/* .line 1129 */
return;
} // .end method
public void updateWakefulnessLocked ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "wakefulness" # I */
/* .line 1050 */
/* iget v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mWakefulness:I */
/* iput v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPreWakefulness:I */
/* .line 1051 */
/* iput p1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mWakefulness:I */
/* .line 1052 */
v0 = this.mAttentionDetector;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1053 */
/* nop */
/* .line 1054 */
v1 = android.os.PowerManagerInternal .isInteractive ( p1 );
/* .line 1053 */
(( com.android.server.power.MiuiAttentionDetector ) v0 ).notifyInteractiveChange ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/power/MiuiAttentionDetector;->notifyInteractiveChange(Z)V
/* .line 1056 */
} // :cond_0
return;
} // .end method
