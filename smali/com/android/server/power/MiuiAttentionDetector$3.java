class com.android.server.power.MiuiAttentionDetector$3 implements android.content.ServiceConnection {
	 /* .source "MiuiAttentionDetector.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/power/MiuiAttentionDetector; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.power.MiuiAttentionDetector this$0; //synthetic
/* # direct methods */
public static void $r8$lambda$fqfwK1K1IlVKb8K7ccSUIkLUq-8 ( com.android.server.power.MiuiAttentionDetector$3 p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector$3;->lambda$onServiceConnected$0()V */
return;
} // .end method
public static void $r8$lambda$x6Ewf7hL_Bz2YQCjpr7u4TlUBQA ( com.android.server.power.MiuiAttentionDetector$3 p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/power/MiuiAttentionDetector$3;->lambda$cleanupService$1()V */
return;
} // .end method
 com.android.server.power.MiuiAttentionDetector$3 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/power/MiuiAttentionDetector; */
/* .line 250 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
private void lambda$cleanupService$1 ( ) { //synthethic
/* .locals 3 */
/* .line 279 */
v0 = this.this$0;
int v1 = 0; // const/4 v1, 0x0
com.android.server.power.MiuiAttentionDetector .-$$Nest$fputmAonService ( v0,v1 );
/* .line 280 */
v0 = this.this$0;
/* new-instance v1, Ljava/util/concurrent/CountDownLatch; */
int v2 = 1; // const/4 v2, 0x1
/* invoke-direct {v1, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V */
com.android.server.power.MiuiAttentionDetector .-$$Nest$fputmServiceBindingLatch ( v0,v1 );
/* .line 281 */
v0 = this.this$0;
int v1 = 0; // const/4 v1, 0x0
com.android.server.power.MiuiAttentionDetector .-$$Nest$fputmIsChecked ( v0,v1 );
/* .line 282 */
return;
} // .end method
private void lambda$onServiceConnected$0 ( ) { //synthethic
/* .locals 1 */
/* .line 257 */
v0 = this.this$0;
com.android.server.power.MiuiAttentionDetector .-$$Nest$mhandlePendingCallback ( v0 );
/* .line 258 */
return;
} // .end method
/* # virtual methods */
public void cleanupService ( ) {
/* .locals 2 */
/* .line 278 */
v0 = this.this$0;
com.android.server.power.MiuiAttentionDetector .-$$Nest$fgetmAttentionHandler ( v0 );
/* new-instance v1, Lcom/android/server/power/MiuiAttentionDetector$3$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0}, Lcom/android/server/power/MiuiAttentionDetector$3$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/power/MiuiAttentionDetector$3;)V */
(( com.android.server.power.MiuiAttentionDetector$AttentionHandler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;->post(Ljava/lang/Runnable;)Z
/* .line 283 */
return;
} // .end method
public void onBindingDied ( android.content.ComponentName p0 ) {
/* .locals 0 */
/* .param p1, "name" # Landroid/content/ComponentName; */
/* .line 269 */
(( com.android.server.power.MiuiAttentionDetector$3 ) p0 ).cleanupService ( ); // invoke-virtual {p0}, Lcom/android/server/power/MiuiAttentionDetector$3;->cleanupService()V
/* .line 270 */
return;
} // .end method
public void onNullBinding ( android.content.ComponentName p0 ) {
/* .locals 0 */
/* .param p1, "name" # Landroid/content/ComponentName; */
/* .line 274 */
(( com.android.server.power.MiuiAttentionDetector$3 ) p0 ).cleanupService ( ); // invoke-virtual {p0}, Lcom/android/server/power/MiuiAttentionDetector$3;->cleanupService()V
/* .line 275 */
return;
} // .end method
public void onServiceConnected ( android.content.ComponentName p0, android.os.IBinder p1 ) {
/* .locals 3 */
/* .param p1, "className" # Landroid/content/ComponentName; */
/* .param p2, "service" # Landroid/os/IBinder; */
/* .line 253 */
com.android.server.power.MiuiAttentionDetector .-$$Nest$sfgetTAG ( );
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Attention service connected success, service:"; // const-string v2, "Attention service connected success, service:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v1 );
/* .line 254 */
v0 = this.this$0;
android.os.Binder .allowBlocking ( p2 );
com.xiaomi.aon.IMiAON$Stub .asInterface ( v1 );
com.android.server.power.MiuiAttentionDetector .-$$Nest$fputmAonService ( v0,v1 );
/* .line 255 */
v0 = this.this$0;
com.android.server.power.MiuiAttentionDetector .-$$Nest$fgetmServiceBindingLatch ( v0 );
(( java.util.concurrent.CountDownLatch ) v0 ).countDown ( ); // invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V
/* .line 256 */
v0 = this.this$0;
com.android.server.power.MiuiAttentionDetector .-$$Nest$fgetmAttentionHandler ( v0 );
/* new-instance v1, Lcom/android/server/power/MiuiAttentionDetector$3$$ExternalSyntheticLambda1; */
/* invoke-direct {v1, p0}, Lcom/android/server/power/MiuiAttentionDetector$3$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/power/MiuiAttentionDetector$3;)V */
(( com.android.server.power.MiuiAttentionDetector$AttentionHandler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/power/MiuiAttentionDetector$AttentionHandler;->post(Ljava/lang/Runnable;)Z
/* .line 259 */
return;
} // .end method
public void onServiceDisconnected ( android.content.ComponentName p0 ) {
/* .locals 2 */
/* .param p1, "className" # Landroid/content/ComponentName; */
/* .line 263 */
com.android.server.power.MiuiAttentionDetector .-$$Nest$sfgetTAG ( );
final String v1 = "Attention service connected failure"; // const-string v1, "Attention service connected failure"
android.util.Slog .i ( v0,v1 );
/* .line 264 */
(( com.android.server.power.MiuiAttentionDetector$3 ) p0 ).cleanupService ( ); // invoke-virtual {p0}, Lcom/android/server/power/MiuiAttentionDetector$3;->cleanupService()V
/* .line 265 */
return;
} // .end method
