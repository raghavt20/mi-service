class com.android.server.power.MiuiAttentionDetector$AttentionHandler extends android.os.Handler {
	 /* .source "MiuiAttentionDetector.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/power/MiuiAttentionDetector; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "AttentionHandler" */
} // .end annotation
/* # static fields */
private static final Integer CHECK_ATTENTION_NO_USER_ACTIVITY;
private static final Integer CHECK_CONNECTION_EXPIRATION;
/* # instance fields */
final com.android.server.power.MiuiAttentionDetector this$0; //synthetic
/* # direct methods */
 com.android.server.power.MiuiAttentionDetector$AttentionHandler ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 527 */
this.this$0 = p1;
/* .line 528 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 529 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 5 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 533 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* packed-switch v0, :pswitch_data_0 */
/* .line 539 */
/* :pswitch_0 */
v0 = this.this$0;
v1 = com.android.server.power.MiuiAttentionDetector .-$$Nest$sfgetTYPE_CHECK_ATTENTION ( );
v2 = com.android.server.power.MiuiAttentionDetector .-$$Nest$sfgetFPS_CHECK_ATTENTION_SCREEN_OFF ( );
final String v3 = "no user activity"; // const-string v3, "no user activity"
/* const/16 v4, 0x7d0 */
(( com.android.server.power.MiuiAttentionDetector ) v0 ).checkAttention ( v1, v2, v3, v4 ); // invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/server/power/MiuiAttentionDetector;->checkAttention(IILjava/lang/String;I)V
/* .line 543 */
v0 = this.this$0;
int v1 = 1; // const/4 v1, 0x1
com.android.server.power.MiuiAttentionDetector .-$$Nest$msetLightSensorEnable ( v0,v1 );
/* .line 544 */
/* .line 536 */
/* :pswitch_1 */
v0 = this.this$0;
(( com.android.server.power.MiuiAttentionDetector ) v0 ).cancelAndUnbind ( ); // invoke-virtual {v0}, Lcom/android/server/power/MiuiAttentionDetector;->cancelAndUnbind()V
/* .line 537 */
/* nop */
/* .line 548 */
} // :goto_0
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
