public class com.android.server.power.ShutdownThreadImpl extends com.android.server.power.ShutdownThreadStub {
	 /* .source "ShutdownThreadImpl.java" */
	 /* # static fields */
	 private static final java.lang.String CUSTOMIZED_REGION;
	 private static final java.lang.String CUST_VAR;
	 private static final Boolean IS_CUSTOMIZATION;
	 private static final Boolean IS_CUSTOMIZATION_TEST;
	 private static final Boolean IS_CUSTOMIZED_REGION;
	 private static final java.lang.String OPCUST_ROOT_PATH;
	 private static final java.lang.String OPERATOR_ANIMATION_DISABLE_FLAG;
	 private static final java.lang.String OPERATOR_MUSIC_DISABLE_FLAG;
	 private static final java.lang.String OPERATOR_SHUTDOWN_ANIMATION_FILE;
	 private static final java.lang.String OPERATOR_SHUTDOWN_MUSIC_FILE;
	 private static final java.lang.String SHUTDOWN_ACTION_PROPERTY_MIUI;
	 private static final java.lang.String SHUTDOWN_ACTION_PROPERTY_MIUI_MUSIC;
	 private static final java.lang.String TAG;
	 private static Boolean sIsShutdownMusicPlaying;
	 /* # direct methods */
	 static void -$$Nest$sfputsIsShutdownMusicPlaying ( Boolean p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 com.android.server.power.ShutdownThreadImpl.sIsShutdownMusicPlaying = (p0!= 0);
		 return;
	 } // .end method
	 static com.android.server.power.ShutdownThreadImpl ( ) {
		 /* .locals 4 */
		 /* .line 40 */
		 /* sget-boolean v0, Lmiui/os/Build;->IS_CM_CUSTOMIZATION_TEST:Z */
		 int v1 = 0; // const/4 v1, 0x0
		 int v2 = 1; // const/4 v2, 0x1
		 /* if-nez v0, :cond_1 */
		 /* sget-boolean v0, Lmiui/os/Build;->IS_CU_CUSTOMIZATION_TEST:Z */
		 /* if-nez v0, :cond_1 */
		 /* sget-boolean v0, Lmiui/os/Build;->IS_CT_CUSTOMIZATION_TEST:Z */
		 if ( v0 != null) { // if-eqz v0, :cond_0
		 } // :cond_0
		 /* move v0, v1 */
	 } // :cond_1
} // :goto_0
/* move v0, v2 */
} // :goto_1
com.android.server.power.ShutdownThreadImpl.IS_CUSTOMIZATION_TEST = (v0!= 0);
/* .line 42 */
/* sget-boolean v0, Lmiui/os/Build;->IS_CM_CUSTOMIZATION:Z */
/* if-nez v0, :cond_2 */
/* sget-boolean v0, Lmiui/os/Build;->IS_CU_CUSTOMIZATION:Z */
/* if-nez v0, :cond_2 */
/* sget-boolean v0, Lmiui/os/Build;->IS_CT_CUSTOMIZATION:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
} // :cond_2
/* move v1, v2 */
} // :cond_3
com.android.server.power.ShutdownThreadImpl.IS_CUSTOMIZATION = (v1!= 0);
/* .line 45 */
final String v0 = "ro.miui.customized.region"; // const-string v0, "ro.miui.customized.region"
final String v1 = ""; // const-string v1, ""
android.os.SystemProperties .get ( v0,v1 );
/* .line 46 */
v1 = android.text.TextUtils .isEmpty ( v0 );
/* xor-int/2addr v1, v2 */
com.android.server.power.ShutdownThreadImpl.IS_CUSTOMIZED_REGION = (v1!= 0);
/* .line 130 */
if ( v1 != null) { // if-eqz v1, :cond_4
} // :cond_4
miui.os.Build .getCustVariant ( );
} // :goto_2
/* .line 131 */
/* sget-boolean v1, Lmiui/os/Build;->HAS_CUST_PARTITION:Z */
final String v2 = "/"; // const-string v2, "/"
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 132 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "/product/opcust/"; // const-string v3, "/product/opcust/"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 133 */
} // :cond_5
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "/data/miui/cust/"; // const-string v3, "/data/miui/cust/"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // :goto_3
/* .line 135 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v2, "theme/operator/boots/shutdownanimation.zip" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 136 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "theme/operator/boots/shutdownaudio.mp3" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
return;
} // .end method
public com.android.server.power.ShutdownThreadImpl ( ) {
/* .locals 0 */
/* .line 34 */
/* invoke-direct {p0}, Lcom/android/server/power/ShutdownThreadStub;-><init>()V */
return;
} // .end method
static Boolean checkAnimationFileExist ( ) {
/* .locals 2 */
/* .line 141 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/data/system/theme_magic/disable_operator_animation"; // const-string v1, "/data/system/theme_magic/disable_operator_animation"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
v0 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
/* if-nez v0, :cond_0 */
/* new-instance v0, Ljava/io/File; */
v1 = com.android.server.power.ShutdownThreadImpl.OPERATOR_SHUTDOWN_ANIMATION_FILE;
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 142 */
v0 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 141 */
} // :goto_0
} // .end method
static java.lang.String getShutdownMusicFilePath ( android.content.Context p0, Boolean p1 ) {
/* .locals 1 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "isReboot" # Z */
/* .line 153 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
private static java.lang.String getShutdownMusicFilePathInner ( android.content.Context p0, Boolean p1 ) {
/* .locals 3 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "isReboot" # Z */
/* .line 157 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/data/system/theme_magic/disable_operator_audio"; // const-string v1, "/data/system/theme_magic/disable_operator_audio"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
v0 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 158 */
/* .line 160 */
} // :cond_0
/* new-instance v0, Ljava/io/File; */
v2 = com.android.server.power.ShutdownThreadImpl.OPERATOR_SHUTDOWN_MUSIC_FILE;
/* invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
v0 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* move-object v1, v2 */
} // :cond_1
} // .end method
private static Boolean isSilentMode ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 224 */
final String v0 = "audio"; // const-string v0, "audio"
(( android.content.Context ) p0 ).getSystemService ( v0 ); // invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/media/AudioManager; */
/* .line 225 */
/* .local v0, "audio":Landroid/media/AudioManager; */
v1 = (( android.media.AudioManager ) v0 ).isSilentMode ( ); // invoke-virtual {v0}, Landroid/media/AudioManager;->isSilentMode()Z
} // .end method
static Boolean needVibrator ( ) {
/* .locals 1 */
/* .line 49 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
static void playShutdownMusic ( android.content.Context p0, Boolean p1 ) {
/* .locals 3 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "isReboot" # Z */
/* .line 167 */
/* const-string/jumbo v0, "sys.shutdown.miui" */
/* const-string/jumbo v1, "shutdown" */
android.os.SystemProperties .set ( v0,v1 );
/* .line 169 */
com.android.server.power.ShutdownThreadImpl .getShutdownMusicFilePathInner ( p0,p1 );
/* .line 170 */
/* .local v0, "shutdownMusicPath":Ljava/lang/String; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "shutdown music: " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " "; // const-string v2, " "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = com.android.server.power.ShutdownThreadImpl .isSilentMode ( p0 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "ShutdownThreadImpl"; // const-string v2, "ShutdownThreadImpl"
android.util.Log .d ( v2,v1 );
/* .line 171 */
v1 = com.android.server.power.ShutdownThreadImpl .isSilentMode ( p0 );
/* if-nez v1, :cond_0 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 173 */
/* const-string/jumbo v1, "sys.shutdown.miuimusic" */
/* const-string/jumbo v2, "shutdown_music" */
android.os.SystemProperties .set ( v1,v2 );
/* .line 176 */
} // :cond_0
return;
} // .end method
private static void playShutdownMusicImpl ( java.lang.String p0 ) {
/* .locals 7 */
/* .param p0, "shutdownMusicPath" # Ljava/lang/String; */
/* .line 180 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
/* .line 181 */
/* .local v0, "actionDoneSync":Ljava/lang/Object; */
int v1 = 1; // const/4 v1, 0x1
com.android.server.power.ShutdownThreadImpl.sIsShutdownMusicPlaying = (v1!= 0);
/* .line 184 */
try { // :try_start_0
/* new-instance v1, Landroid/media/MediaPlayer; */
/* invoke-direct {v1}, Landroid/media/MediaPlayer;-><init>()V */
/* .line 185 */
/* .local v1, "mediaPlayer":Landroid/media/MediaPlayer; */
(( android.media.MediaPlayer ) v1 ).reset ( ); // invoke-virtual {v1}, Landroid/media/MediaPlayer;->reset()V
/* .line 186 */
(( android.media.MediaPlayer ) v1 ).setDataSource ( p0 ); // invoke-virtual {v1, p0}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V
/* .line 187 */
(( android.media.MediaPlayer ) v1 ).prepare ( ); // invoke-virtual {v1}, Landroid/media/MediaPlayer;->prepare()V
/* .line 188 */
/* new-instance v2, Lcom/android/server/power/ShutdownThreadImpl$1; */
/* invoke-direct {v2, v0}, Lcom/android/server/power/ShutdownThreadImpl$1;-><init>(Ljava/lang/Object;)V */
/* .line 189 */
(( android.media.MediaPlayer ) v1 ).setOnCompletionListener ( v2 ); // invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V
/* .line 198 */
(( android.media.MediaPlayer ) v1 ).start ( ); // invoke-virtual {v1}, Landroid/media/MediaPlayer;->start()V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 202 */
} // .end local v1 # "mediaPlayer":Landroid/media/MediaPlayer;
/* .line 199 */
/* :catch_0 */
/* move-exception v1 */
/* .line 200 */
/* .local v1, "e":Ljava/lang/Exception; */
int v2 = 0; // const/4 v2, 0x0
com.android.server.power.ShutdownThreadImpl.sIsShutdownMusicPlaying = (v2!= 0);
/* .line 201 */
final String v2 = "ShutdownThreadImpl"; // const-string v2, "ShutdownThreadImpl"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "play shutdown music error:"; // const-string v4, "play shutdown music error:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v3 );
/* .line 204 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v1 */
/* const-wide/16 v3, 0x1388 */
/* add-long/2addr v1, v3 */
/* .line 205 */
/* .local v1, "endTimeForMusic":J */
/* monitor-enter v0 */
/* .line 206 */
} // :goto_1
try { // :try_start_1
/* sget-boolean v3, Lcom/android/server/power/ShutdownThreadImpl;->sIsShutdownMusicPlaying:Z */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 207 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v3 */
/* sub-long v3, v1, v3 */
/* .line 208 */
/* .local v3, "delay":J */
/* const-wide/16 v5, 0x0 */
/* cmp-long v5, v3, v5 */
/* if-gtz v5, :cond_0 */
/* .line 209 */
final String v5 = "ShutdownThreadImpl"; // const-string v5, "ShutdownThreadImpl"
final String v6 = "play shutdown music timeout"; // const-string v6, "play shutdown music timeout"
android.util.Log .d ( v5,v6 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 210 */
/* .line 213 */
} // :cond_0
try { // :try_start_2
(( java.lang.Object ) v0 ).wait ( v3, v4 ); // invoke-virtual {v0, v3, v4}, Ljava/lang/Object;->wait(J)V
/* :try_end_2 */
/* .catch Ljava/lang/InterruptedException; {:try_start_2 ..:try_end_2} :catch_1 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 215 */
/* .line 214 */
/* :catch_1 */
/* move-exception v5 */
/* .line 216 */
} // .end local v3 # "delay":J
} // :goto_2
/* .line 217 */
} // :cond_1
} // :goto_3
try { // :try_start_3
/* sget-boolean v3, Lcom/android/server/power/ShutdownThreadImpl;->sIsShutdownMusicPlaying:Z */
/* if-nez v3, :cond_2 */
/* .line 218 */
final String v3 = "ShutdownThreadImpl"; // const-string v3, "ShutdownThreadImpl"
final String v4 = "play shutdown music complete"; // const-string v4, "play shutdown music complete"
android.util.Log .d ( v3,v4 );
/* .line 220 */
} // :cond_2
/* monitor-exit v0 */
/* .line 221 */
return;
/* .line 220 */
/* :catchall_0 */
/* move-exception v3 */
/* monitor-exit v0 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* throw v3 */
} // .end method
static void recordShutdownTime ( android.content.Context p0, Boolean p1 ) {
/* .locals 4 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "reboot" # Z */
/* .line 229 */
/* new-instance v0, Lmiui/util/SystemAnalytics$Action; */
/* invoke-direct {v0}, Lmiui/util/SystemAnalytics$Action;-><init>()V */
/* .line 230 */
/* .local v0, "action":Lmiui/util/SystemAnalytics$Action; */
if ( p1 != null) { // if-eqz p1, :cond_0
final String v1 = "reboot"; // const-string v1, "reboot"
} // :cond_0
/* const-string/jumbo v1, "shutdown" */
} // :goto_0
final String v2 = "action"; // const-string v2, "action"
(( miui.util.SystemAnalytics$Action ) v0 ).addParam ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Lmiui/util/SystemAnalytics$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lmiui/util/SystemAnalytics$Action;
/* .line 231 */
/* const-string/jumbo v1, "time" */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v2 */
(( miui.util.SystemAnalytics$Action ) v0 ).addParam ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lmiui/util/SystemAnalytics$Action;->addParam(Ljava/lang/String;J)Lmiui/util/SystemAnalytics$Action;
/* .line 232 */
/* const-string/jumbo v1, "systemserver_bootshuttime" */
miui.util.SystemAnalytics .trackSystem ( p0,v1,v0 );
/* .line 233 */
return;
} // .end method
static void showShutdownAnimation ( android.content.Context p0, Boolean p1 ) {
/* .locals 0 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "isReboot" # Z */
/* .line 148 */
com.android.server.power.ShutdownThreadImpl .playShutdownMusic ( p0,p1 );
/* .line 149 */
return;
} // .end method
static void showShutdownDialog ( android.content.Context p0, Boolean p1 ) {
/* .locals 12 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "isReboot" # Z */
/* .line 85 */
/* new-instance v0, Landroid/app/Dialog; */
/* const v1, 0x103006d */
/* invoke-direct {v0, p0, v1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V */
/* .line 86 */
/* .local v0, "bootMsgDialog":Landroid/app/Dialog; */
(( android.app.Dialog ) v0 ).getContext ( ); // invoke-virtual {v0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;
android.view.LayoutInflater .from ( v1 );
/* const v2, 0x110c002f */
int v3 = 0; // const/4 v3, 0x0
(( android.view.LayoutInflater ) v1 ).inflate ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;
/* .line 87 */
/* .local v1, "view":Landroid/view/View; */
/* const/16 v2, 0x400 */
(( android.view.View ) v1 ).setSystemUiVisibility ( v2 ); // invoke-virtual {v1, v2}, Landroid/view/View;->setSystemUiVisibility(I)V
/* .line 88 */
(( android.app.Dialog ) v0 ).setContentView ( v1 ); // invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V
/* .line 89 */
int v2 = 0; // const/4 v2, 0x0
(( android.app.Dialog ) v0 ).setCancelable ( v2 ); // invoke-virtual {v0, v2}, Landroid/app/Dialog;->setCancelable(Z)V
/* .line 90 */
(( android.app.Dialog ) v0 ).getWindow ( ); // invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;
(( android.view.Window ) v3 ).getAttributes ( ); // invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;
/* .line 91 */
/* .local v3, "lp":Landroid/view/WindowManager$LayoutParams; */
int v4 = 1; // const/4 v4, 0x1
/* iput v4, v3, Landroid/view/WindowManager$LayoutParams;->screenOrientation:I */
/* .line 92 */
/* iput v4, v3, Landroid/view/WindowManager$LayoutParams;->layoutInDisplayCutoutMode:I */
/* .line 93 */
(( android.app.Dialog ) v0 ).getWindow ( ); // invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;
(( android.view.Window ) v5 ).setAttributes ( v3 ); // invoke-virtual {v5, v3}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V
/* .line 94 */
(( android.app.Dialog ) v0 ).getWindow ( ); // invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;
/* const/16 v6, 0x7e5 */
(( android.view.Window ) v5 ).setType ( v6 ); // invoke-virtual {v5, v6}, Landroid/view/Window;->setType(I)V
/* .line 95 */
(( android.app.Dialog ) v0 ).getWindow ( ); // invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;
/* const/high16 v6, 0x10000 */
(( android.view.Window ) v5 ).clearFlags ( v6 ); // invoke-virtual {v5, v6}, Landroid/view/Window;->clearFlags(I)V
/* .line 96 */
(( android.app.Dialog ) v0 ).show ( ); // invoke-virtual {v0}, Landroid/app/Dialog;->show()V
/* .line 98 */
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 99 */
/* const v4, 0x110a00c4 */
(( android.view.View ) v1 ).findViewById ( v4 ); // invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;
/* check-cast v4, Landroid/widget/ImageView; */
/* .line 100 */
/* .local v4, "shutdownImage":Landroid/widget/ImageView; */
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 101 */
(( android.widget.ImageView ) v4 ).setVisibility ( v2 ); // invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setVisibility(I)V
/* .line 102 */
(( android.widget.ImageView ) v4 ).getDrawable ( ); // invoke-virtual {v4}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;
/* check-cast v2, Landroid/graphics/drawable/AnimatedRotateDrawable; */
/* .line 103 */
/* .local v2, "animationDrawable":Landroid/graphics/drawable/AnimatedRotateDrawable; */
(( android.content.Context ) p0 ).getResources ( ); // invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v6, 0x110b003f */
v5 = (( android.content.res.Resources ) v5 ).getInteger ( v6 ); // invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I
(( android.graphics.drawable.AnimatedRotateDrawable ) v2 ).setFramesCount ( v5 ); // invoke-virtual {v2, v5}, Landroid/graphics/drawable/AnimatedRotateDrawable;->setFramesCount(I)V
/* .line 104 */
(( android.content.Context ) p0 ).getResources ( ); // invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v6, 0x110b0040 */
v5 = (( android.content.res.Resources ) v5 ).getInteger ( v6 ); // invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I
(( android.graphics.drawable.AnimatedRotateDrawable ) v2 ).setFramesDuration ( v5 ); // invoke-virtual {v2, v5}, Landroid/graphics/drawable/AnimatedRotateDrawable;->setFramesDuration(I)V
/* .line 105 */
(( android.graphics.drawable.AnimatedRotateDrawable ) v2 ).start ( ); // invoke-virtual {v2}, Landroid/graphics/drawable/AnimatedRotateDrawable;->start()V
/* .line 107 */
} // .end local v2 # "animationDrawable":Landroid/graphics/drawable/AnimatedRotateDrawable;
} // .end local v4 # "shutdownImage":Landroid/widget/ImageView;
} // :cond_0
/* .line 109 */
} // :cond_1
/* const-class v5, Lcom/android/server/lights/LightsManager; */
/* .line 110 */
com.android.server.LocalServices .getService ( v5 );
/* check-cast v5, Lcom/android/server/lights/LightsManager; */
/* .line 111 */
/* .local v5, "lightmanager":Lcom/android/server/lights/LightsManager; */
(( com.android.server.lights.LightsManager ) v5 ).getLight ( v2 ); // invoke-virtual {v5, v2}, Lcom/android/server/lights/LightsManager;->getLight(I)Lcom/android/server/lights/LogicalLight;
/* .line 112 */
/* .local v6, "light":Lcom/android/server/lights/LogicalLight; */
int v7 = 0; // const/4 v7, 0x0
/* .line 114 */
/* .local v7, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
try { // :try_start_0
final String v8 = "com.android.server.lights.MiuiLightsService$LightImpl"; // const-string v8, "com.android.server.lights.MiuiLightsService$LightImpl"
java.lang.Class .forName ( v8 );
/* move-object v7, v8 */
/* .line 115 */
/* const-string/jumbo v8, "setBrightness" */
int v9 = 2; // const/4 v9, 0x2
/* new-array v10, v9, [Ljava/lang/Class; */
v11 = java.lang.Integer.TYPE;
/* aput-object v11, v10, v2 */
v11 = java.lang.Boolean.TYPE;
/* aput-object v11, v10, v4 */
(( java.lang.Class ) v7 ).getMethod ( v8, v10 ); // invoke-virtual {v7, v8, v10}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 116 */
/* .local v8, "setBrightnessMethod":Ljava/lang/reflect/Method; */
/* new-array v9, v9, [Ljava/lang/Object; */
java.lang.Integer .valueOf ( v2 );
/* aput-object v10, v9, v2 */
java.lang.Boolean .valueOf ( v4 );
/* aput-object v2, v9, v4 */
(( java.lang.reflect.Method ) v8 ).invoke ( v6, v9 ); // invoke-virtual {v8, v6, v9}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/IllegalAccessException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/NoSuchMethodException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/ClassNotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 123 */
/* nop */
} // .end local v8 # "setBrightnessMethod":Ljava/lang/reflect/Method;
/* .line 118 */
/* :catch_0 */
/* move-exception v2 */
/* .line 122 */
/* .local v2, "e":Ljava/lang/ReflectiveOperationException; */
final String v4 = "ShutdownThreadImpl"; // const-string v4, "ShutdownThreadImpl"
final String v8 = "Failed to invoke MiuiLightsService setBrightness method"; // const-string v8, "Failed to invoke MiuiLightsService setBrightness method"
android.util.Log .e ( v4,v8,v2 );
/* .line 127 */
} // .end local v2 # "e":Ljava/lang/ReflectiveOperationException;
} // .end local v5 # "lightmanager":Lcom/android/server/lights/LightsManager;
} // .end local v6 # "light":Lcom/android/server/lights/LogicalLight;
} // .end local v7 # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
} // :goto_0
/* const-string/jumbo v2, "sys.in_shutdown_progress" */
final String v4 = "1"; // const-string v4, "1"
android.os.SystemProperties .set ( v2,v4 );
/* .line 128 */
return;
} // .end method
/* # virtual methods */
Boolean isCustomizedShutdownAnim ( ) {
/* .locals 4 */
/* .line 68 */
/* sget-boolean v0, Lcom/android/server/power/ShutdownThreadImpl;->IS_CUSTOMIZATION:Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* sget-boolean v0, Lcom/android/server/power/ShutdownThreadImpl;->IS_CUSTOMIZATION_TEST:Z */
/* if-nez v0, :cond_0 */
/* sget-boolean v0, Lcom/android/server/power/ShutdownThreadImpl;->IS_CUSTOMIZED_REGION:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
} // :cond_0
v0 = com.android.server.power.ShutdownThreadImpl .checkAnimationFileExist ( );
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 69 */
v0 = com.android.server.power.ShutdownThreadImpl.CUSTOMIZED_REGION;
final String v2 = "mx_at"; // const-string v2, "mx_at"
v2 = (( java.lang.String ) v2 ).equals ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
final String v2 = "persist.radio.op.name"; // const-string v2, "persist.radio.op.name"
final String v3 = "AT"; // const-string v3, "AT"
android.os.SystemProperties .get ( v2,v3 );
v2 = (( java.lang.String ) v3 ).equals ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_1 */
/* .line 70 */
/* .line 72 */
} // :cond_1
final String v2 = "gt_tg"; // const-string v2, "gt_tg"
v0 = (( java.lang.String ) v2 ).equals ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 73 */
final String v0 = "ro.miui.region"; // const-string v0, "ro.miui.region"
final String v2 = "GT"; // const-string v2, "GT"
android.os.SystemProperties .get ( v0,v2 );
/* .line 74 */
/* .local v0, "region":Ljava/lang/String; */
final String v2 = "BO"; // const-string v2, "BO"
v2 = (( java.lang.String ) v2 ).equals ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_2 */
final String v2 = "SV"; // const-string v2, "SV"
v2 = (( java.lang.String ) v2 ).equals ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_2 */
final String v2 = "HN"; // const-string v2, "HN"
v2 = (( java.lang.String ) v2 ).equals ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_2 */
/* .line 75 */
final String v2 = "NI"; // const-string v2, "NI"
v2 = (( java.lang.String ) v2 ).equals ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_2 */
/* .line 76 */
/* .line 79 */
} // .end local v0 # "region":Ljava/lang/String;
} // :cond_2
int v0 = 1; // const/4 v0, 0x1
/* .line 81 */
} // :cond_3
} // .end method
void showShutdownAnimOrDialog ( android.content.Context p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "isReboot" # Z */
/* .line 58 */
v0 = (( com.android.server.power.ShutdownThreadImpl ) p0 ).isCustomizedShutdownAnim ( ); // invoke-virtual {p0}, Lcom/android/server/power/ShutdownThreadImpl;->isCustomizedShutdownAnim()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 59 */
/* const-string/jumbo v0, "service.bootanim.exit" */
final String v1 = "0"; // const-string v1, "0"
android.os.SystemProperties .set ( v0,v1 );
/* .line 60 */
final String v0 = "ctl.start"; // const-string v0, "ctl.start"
final String v1 = "bootanim"; // const-string v1, "bootanim"
android.os.SystemProperties .set ( v0,v1 );
/* .line 61 */
com.android.server.power.ShutdownThreadImpl .showShutdownAnimation ( p1,p2 );
/* .line 63 */
} // :cond_0
com.android.server.power.ShutdownThreadImpl .showShutdownDialog ( p1,p2 );
/* .line 65 */
} // :goto_0
return;
} // .end method
void showShutdownAnimOrDialog ( android.content.Context p0, Boolean p1, android.app.ProgressDialog p2 ) {
/* .locals 0 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "isReboot" # Z */
/* .param p3, "pd" # Landroid/app/ProgressDialog; */
/* .line 54 */
(( com.android.server.power.ShutdownThreadImpl ) p0 ).showShutdownAnimOrDialog ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/android/server/power/ShutdownThreadImpl;->showShutdownAnimOrDialog(Landroid/content/Context;Z)V
/* .line 55 */
return;
} // .end method
