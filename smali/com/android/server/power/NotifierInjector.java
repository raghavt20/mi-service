public class com.android.server.power.NotifierInjector {
	 /* .source "NotifierInjector.java" */
	 /* # static fields */
	 private static final java.lang.String RESTORE_BRIGHTNESS_BROADCAST_ACTION;
	 private static final java.lang.String RESTORE_BRIGHTNESS_BROADCAST_PERMISSION;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private Boolean mAccelerometerRotationEnabled;
	 private Boolean mBeingHangUp;
	 private final android.content.Context mContext;
	 private Boolean mDisableRotationDueToHangUp;
	 private final android.os.Handler mHandler;
	 private final com.android.server.input.InputManagerInternal mInputManagerInternal;
	 private android.view.IWindowManager mWindowManagerService;
	 /* # direct methods */
	 static android.content.Context -$$Nest$fgetmContext ( com.android.server.power.NotifierInjector p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 p0 = this.mContext;
	 } // .end method
	 public com.android.server.power.NotifierInjector ( ) {
		 /* .locals 2 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .line 36 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 37 */
		 this.mContext = p1;
		 /* .line 38 */
		 /* new-instance v0, Landroid/os/Handler; */
		 android.os.Looper .getMainLooper ( );
		 /* invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
		 this.mHandler = v0;
		 /* .line 39 */
		 /* const-class v0, Lcom/android/server/input/InputManagerInternal; */
		 com.android.server.LocalServices .getService ( v0 );
		 /* check-cast v0, Lcom/android/server/input/InputManagerInternal; */
		 this.mInputManagerInternal = v0;
		 /* .line 40 */
		 /* nop */
		 /* .line 41 */
		 /* const-string/jumbo v0, "window" */
		 android.os.ServiceManager .getService ( v0 );
		 /* .line 40 */
		 android.view.IWindowManager$Stub .asInterface ( v0 );
		 this.mWindowManagerService = v0;
		 /* .line 43 */
		 (( com.android.server.power.NotifierInjector ) p0 ).updateAccelerometerRotationLocked ( ); // invoke-virtual {p0}, Lcom/android/server/power/NotifierInjector;->updateAccelerometerRotationLocked()V
		 /* .line 44 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 protected void dump ( java.io.PrintWriter p0 ) {
		 /* .locals 2 */
		 /* .param p1, "pw" # Ljava/io/PrintWriter; */
		 /* .line 115 */
		 /* new-instance v0, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v1 = "mBeingHangUp: "; // const-string v1, "mBeingHangUp: "
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget-boolean v1, p0, Lcom/android/server/power/NotifierInjector;->mBeingHangUp:Z */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 (( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
		 /* .line 116 */
		 /* new-instance v0, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v1 = "mDisableRotationDueToHangUp: "; // const-string v1, "mDisableRotationDueToHangUp: "
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget-boolean v1, p0, Lcom/android/server/power/NotifierInjector;->mDisableRotationDueToHangUp:Z */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 (( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
		 /* .line 117 */
		 return;
	 } // .end method
	 protected void onWakefulnessInHangUp ( Boolean p0, Integer p1 ) {
		 /* .locals 3 */
		 /* .param p1, "hangUp" # Z */
		 /* .param p2, "wakefulness" # I */
		 /* .line 77 */
		 /* iget-boolean v0, p0, Lcom/android/server/power/NotifierInjector;->mBeingHangUp:Z */
		 /* if-eq v0, p1, :cond_2 */
		 /* .line 78 */
		 /* iput-boolean p1, p0, Lcom/android/server/power/NotifierInjector;->mBeingHangUp:Z */
		 /* .line 79 */
		 /* new-instance v0, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v1 = "onWakefulnessInHangUp: "; // const-string v1, "onWakefulnessInHangUp: "
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* .line 80 */
		 /* iget-boolean v1, p0, Lcom/android/server/power/NotifierInjector;->mBeingHangUp:Z */
		 if ( v1 != null) { // if-eqz v1, :cond_0
			 final String v1 = "disable "; // const-string v1, "disable "
		 } // :cond_0
		 final String v1 = "enable "; // const-string v1, "enable "
	 } // :goto_0
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 final String v1 = "input event."; // const-string v1, "input event."
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 /* .line 79 */
	 final String v1 = "PowerManagerNotifierInjector"; // const-string v1, "PowerManagerNotifierInjector"
	 android.util.Slog .d ( v1,v0 );
	 /* .line 81 */
	 v0 = 	 android.os.PowerManagerInternal .isInteractive ( p2 );
	 /* .line 82 */
	 /* .local v0, "interactive":Z */
	 v1 = this.mInputManagerInternal;
	 /* iget-boolean v2, p0, Lcom/android/server/power/NotifierInjector;->mBeingHangUp:Z */
	 /* if-nez v2, :cond_1 */
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 int v2 = 1; // const/4 v2, 0x1
	 } // :cond_1
	 int v2 = 0; // const/4 v2, 0x0
} // :goto_1
(( com.android.server.input.InputManagerInternal ) v1 ).setInteractive ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/input/InputManagerInternal;->setInteractive(Z)V
/* .line 83 */
v1 = this.mHandler;
/* new-instance v2, Lcom/android/server/power/NotifierInjector$1; */
/* invoke-direct {v2, p0, p1}, Lcom/android/server/power/NotifierInjector$1;-><init>(Lcom/android/server/power/NotifierInjector;Z)V */
(( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 92 */
} // .end local v0 # "interactive":Z
} // :cond_2
return;
} // .end method
protected void sendBroadcastRestoreBrightness ( ) {
/* .locals 2 */
/* .line 103 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/power/NotifierInjector$2; */
/* invoke-direct {v1, p0}, Lcom/android/server/power/NotifierInjector$2;-><init>(Lcom/android/server/power/NotifierInjector;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 111 */
return;
} // .end method
protected void sendHangupBroadcast ( Boolean p0 ) {
/* .locals 5 */
/* .param p1, "hangup" # Z */
/* .line 95 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "miui.intent.action.HANG_UP_CHANGED"; // const-string v1, "miui.intent.action.HANG_UP_CHANGED"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 96 */
/* .local v0, "intent":Landroid/content/Intent; */
/* const/high16 v1, 0x50000000 */
(( android.content.Intent ) v0 ).addFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 97 */
final String v1 = "hang_up_enable"; // const-string v1, "hang_up_enable"
(( android.content.Intent ) v0 ).putExtra ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 98 */
v1 = this.mContext;
v2 = android.os.UserHandle.ALL;
final String v3 = "com.miui.permission.HANG_UP_CHANGED"; // const-string v3, "com.miui.permission.HANG_UP_CHANGED"
int v4 = 0; // const/4 v4, 0x0
(( android.content.Context ) v1 ).sendBroadcastAsUser ( v0, v2, v3, v4 ); // invoke-virtual {v1, v0, v2, v3, v4}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;Landroid/os/Bundle;)V
/* .line 100 */
return;
} // .end method
protected void updateAccelerometerRotationLocked ( ) {
/* .locals 4 */
/* .line 66 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v1 = -2; // const/4 v1, -0x2
final String v2 = "accelerometer_rotation"; // const-string v2, "accelerometer_rotation"
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$System .getIntForUser ( v0,v2,v3,v1 );
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* move v3, v1 */
} // :cond_0
/* iput-boolean v3, p0, Lcom/android/server/power/NotifierInjector;->mAccelerometerRotationEnabled:Z */
/* .line 69 */
return;
} // .end method
protected void updateRotationOffState ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "hangUp" # Z */
/* .line 49 */
final String v0 = "PowerManagerNotifierInjector"; // const-string v0, "PowerManagerNotifierInjector"
if ( p1 != null) { // if-eqz p1, :cond_0
try { // :try_start_0
/* iget-boolean v1, p0, Lcom/android/server/power/NotifierInjector;->mAccelerometerRotationEnabled:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 51 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/android/server/power/NotifierInjector;->mDisableRotationDueToHangUp:Z */
/* .line 52 */
v1 = this.mWindowManagerService;
int v2 = -1; // const/4 v2, -0x1
/* .line 53 */
/* const-string/jumbo v1, "updateRotationOffState: disable accelerometer rotation." */
android.util.Slog .d ( v0,v1 );
/* .line 60 */
/* :catch_0 */
/* move-exception v1 */
/* .line 54 */
} // :cond_0
/* if-nez p1, :cond_1 */
/* iget-boolean v1, p0, Lcom/android/server/power/NotifierInjector;->mDisableRotationDueToHangUp:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* iget-boolean v1, p0, Lcom/android/server/power/NotifierInjector;->mAccelerometerRotationEnabled:Z */
/* if-nez v1, :cond_1 */
/* .line 56 */
/* const-string/jumbo v1, "updateRotationOffState: enable accelerometer rotation." */
android.util.Slog .d ( v0,v1 );
/* .line 57 */
int v1 = 0; // const/4 v1, 0x0
/* iput-boolean v1, p0, Lcom/android/server/power/NotifierInjector;->mDisableRotationDueToHangUp:Z */
/* .line 58 */
v1 = this.mWindowManagerService;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 61 */
/* .local v1, "e":Landroid/os/RemoteException; */
} // :goto_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "updateRotationOffState: " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.os.RemoteException ) v1 ).toString ( ); // invoke-virtual {v1}, Landroid/os/RemoteException;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v0,v2 );
/* .line 62 */
} // .end local v1 # "e":Landroid/os/RemoteException;
} // :cond_1
} // :goto_1
/* nop */
/* .line 63 */
} // :goto_2
return;
} // .end method
