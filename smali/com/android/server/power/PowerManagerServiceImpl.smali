.class public Lcom/android/server/power/PowerManagerServiceImpl;
.super Lcom/android/server/power/PowerManagerServiceStub;
.source "PowerManagerServiceImpl.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.power.PowerManagerServiceStub$$"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/power/PowerManagerServiceImpl$PowerManagerStubHandler;,
        Lcom/android/server/power/PowerManagerServiceImpl$SettingsObserver;,
        Lcom/android/server/power/PowerManagerServiceImpl$ClientDeathCallback;
    }
.end annotation


# static fields
.field private static DEBUG:Z = false

.field private static final DEFAULT_SCREEN_OFF_TIMEOUT_SECONDARY_DISPLAY:I = 0x3a98

.field private static final DEFAULT_SUBSCREEN_SUPER_POWER_SAVE_MODE:I = 0x0

.field private static final DIRTY_SCREEN_BRIGHTNESS_BOOST:I = 0x800

.field private static final DIRTY_SETTINGS:I = 0x20

.field private static final FLAG_BOOST_BRIGHTNESS:I = 0x2

.field private static final FLAG_KEEP_SECONDARY_ALWAYS_ON:I = 0x1

.field private static final FLAG_SCREEN_PROJECTION:I = 0x0

.field private static final IS_FOLDABLE_DEVICE:Z

.field private static final KEY_DEVICE_LOCK:Ljava/lang/String; = "com.xiaomi.system.devicelock.locked"

.field private static final KEY_SECONDARY_SCREEN_ENABLE:Ljava/lang/String; = "subscreen_switch"

.field private static final MSG_UPDATE_FOREGROUND_APP:I = 0x1

.field private static final OPTIMIZE_WAKELOCK_ENABLED:Z

.field private static final PICK_UP_SENSOR_TYPE:I = 0x1fa265c

.field private static final POWER_SAVE_MODE_OPEN:Ljava/lang/String; = "POWER_SAVE_MODE_OPEN"

.field private static final REASON_CHANGE_SECONDARY_STATE_CAMERA_CALL:Ljava/lang/String; = "CAMERA_CALL"

.field private static final SCREEN_OFF_TIMEOUT_SECONDARY_DISPLAY:Ljava/lang/String; = "subscreen_display_time"

.field private static final SKIP_TRANSITION_WAKE_UP_DETAILS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final SUBSCREEN_SUPER_POWER_SAVE_MODE:Ljava/lang/String; = "subscreen_super_power_save_mode"

.field private static final SUB_DISPLAY_GROUP_ID:I = 0x1

.field private static final SUPPORT_FOD:Z

.field private static final TAG:Ljava/lang/String; = "PowerManagerServiceImpl"

.field private static final WAKE_LOCK_SCREEN_BRIGHT:I = 0x2

.field private static final WAKE_LOCK_SCREEN_DIM:I = 0x4

.field private static final WAKE_LOCK_STAY_AWAKE:I = 0x20

.field public static final WAKING_UP_DETAILS_BIOMETRIC:Ljava/lang/String; = "android.policy:BIOMETRIC"

.field public static final WAKING_UP_DETAILS_FINGERPRINT:Ljava/lang/String; = "android.policy:FINGERPRINT"

.field public static final WAKING_UP_DETAILS_GOTO_UNLOCK:Ljava/lang/String; = "com.android.systemui:GOTO_UNLOCK"

.field private static final mBrightnessDecreaseRatio:F


# instance fields
.field private isDeviceLock:Z

.field private mActivityTaskManager:Landroid/app/IActivityTaskManager;

.field private mAdaptiveSleepEnabled:Z

.field private mAlwaysWakeUp:Z

.field private mAonScreenOffEnabled:Z

.field private mAttentionDetector:Lcom/android/server/power/MiuiAttentionDetector;

.field private mBootCompleted:Z

.field private mBrightWaitTime:J

.field private mBrightnessBoostForSoftLightInProgress:Z

.field private mClientDeathCallbacks:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Landroid/os/IBinder;",
            "Lcom/android/server/power/PowerManagerServiceImpl$ClientDeathCallback;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mDimEnable:Z

.field private mDisplayStateChangeStateTime:J

.field private mDriveMode:I

.field private mFolded:Ljava/lang/Boolean;

.field private mForegroundAppPackageName:Ljava/lang/String;

.field private final mFreeformCallBack:Lmiui/app/IFreeformCallback;

.field private mGreezeManager:Lmiui/greeze/IGreezeManager;

.field private mHandler:Landroid/os/Handler;

.field private mHangUpEnabled:Z

.field private mIsBrightWakeLock:Z

.field private volatile mIsFreeFormOrSplitWindowMode:Z

.field private mIsKeyguardHasShownWhenExitHangup:Z

.field private mIsPowerSaveModeEnabled:Z

.field private mIsUserSetupComplete:Z

.field private mLastRequestDimmingTime:J

.field private mLastSecondaryDisplayGoToSleepReason:Ljava/lang/String;

.field private mLastSecondaryDisplayGoToSleepTime:J

.field private mLastSecondaryDisplayUserActivityTime:J

.field private mLastSecondaryDisplayWakeUpReason:Ljava/lang/String;

.field private mLastSecondaryDisplayWakeUpTime:J

.field private mLock:Ljava/lang/Object;

.field private mMiuiInputManagerInternal:Lcom/android/server/input/MiuiInputManagerInternal;

.field private mMiuiPowerStatisticTracker:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;

.field private mNoSupprotInnerScreenProximitySensor:Z

.field private mNotifierInjector:Lcom/android/server/power/NotifierInjector;

.field private mPendingForegroundAppPackageName:Ljava/lang/String;

.field private mPendingSetDirtyDueToRequestDimming:Z

.field private mPendingStopBrightnessBoost:Z

.field private mPickUpGestureWakeUpEnabled:Z

.field private mPickUpSensor:Landroid/hardware/Sensor;

.field private mPickUpSensorEnabled:Z

.field private final mPickUpSensorListener:Landroid/hardware/SensorEventListener;

.field private mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

.field private mPowerGroups:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/android/server/power/PowerGroup;",
            ">;"
        }
    .end annotation
.end field

.field private mPowerKeeperPolicy:Lcom/miui/whetstone/PowerKeeperPolicy;

.field private mPowerManager:Landroid/os/PowerManager;

.field private mPowerManagerService:Lcom/android/server/power/PowerManagerService;

.field private mPreWakefulness:I

.field private mResolver:Landroid/content/ContentResolver;

.field private mScreenProjectionEnabled:Z

.field private final mScreenWakeLockWhitelists:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSecondaryDisplayEnabled:Z

.field private mSecondaryDisplayScreenOffTimeout:J

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mSettingsObserver:Lcom/android/server/power/PowerManagerServiceImpl$SettingsObserver;

.field private mSituatedDimmingDueToAttention:Z

.field private mSituatedDimmingDueToSynergy:Z

.field private mSubScreenSuperPowerSaveMode:I

.field private mSupportAdaptiveSleep:Z

.field private mSynergyModeEnable:Z

.field private mSystemReady:Z

.field private final mTaskStackListener:Landroid/app/TaskStackListener;

.field private mTouchInteractiveUnavailable:Z

.field private final mUpdateForegroundAppRunnable:Ljava/lang/Runnable;

.field private mUserActivitySecondaryDisplaySummary:I

.field private mUserActivityTimeoutOverrideFromMirrorManager:J

.field private final mVisibleWindowUids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mWakeLocks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/android/server/power/PowerManagerService$WakeLock;",
            ">;"
        }
    .end annotation
.end field

.field private mWakefulness:I

.field private final mWindowListener:Lmiui/process/IForegroundWindowListener;


# direct methods
.method public static synthetic $r8$lambda$1rkCId4MrOI2k03MGNvReTtQiEM(Lcom/android/server/power/PowerManagerServiceImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updateForegroundApp()V

    return-void
.end method

.method public static synthetic $r8$lambda$pzfv8pcWLm9HcghS2Yx3ij0_avs(Lcom/android/server/power/PowerManagerServiceImpl;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/power/PowerManagerServiceImpl;->onFoldStateChanged(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/power/PowerManagerServiceImpl;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmLock(Lcom/android/server/power/PowerManagerServiceImpl;)Ljava/lang/Object;
    .locals 0

    iget-object p0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLock:Ljava/lang/Object;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmUpdateForegroundAppRunnable(Lcom/android/server/power/PowerManagerServiceImpl;)Ljava/lang/Runnable;
    .locals 0

    iget-object p0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mUpdateForegroundAppRunnable:Ljava/lang/Runnable;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mdoDieLocked(Lcom/android/server/power/PowerManagerServiceImpl;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/power/PowerManagerServiceImpl;->doDieLocked(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleSettingsChangedLocked(Lcom/android/server/power/PowerManagerServiceImpl;Landroid/net/Uri;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/power/PowerManagerServiceImpl;->handleSettingsChangedLocked(Landroid/net/Uri;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateForegroundAppPackageName(Lcom/android/server/power/PowerManagerServiceImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updateForegroundAppPackageName()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 4

    .line 89
    nop

    .line 90
    const-string v0, "ro.hardware.fp.fod"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/power/PowerManagerServiceImpl;->SUPPORT_FOD:Z

    .line 94
    const-string v0, "com.android.systemui:GOTO_UNLOCK"

    const-string v2, "android.policy:BIOMETRIC"

    const-string v3, "android.policy:FINGERPRINT"

    filled-new-array {v3, v0, v2}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/android/server/power/PowerManagerServiceImpl;->SKIP_TRANSITION_WAKE_UP_DETAILS:Ljava/util/List;

    .line 99
    sput-boolean v1, Lcom/android/server/power/PowerManagerServiceImpl;->DEBUG:Z

    .line 138
    nop

    .line 139
    const-string v0, "optimize_wakelock_enabled"

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/power/PowerManagerServiceImpl;->OPTIMIZE_WAKELOCK_ENABLED:Z

    .line 144
    nop

    .line 145
    const-string v0, "persist.sys.muiltdisplay_type"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    move v1, v2

    :cond_0
    sput-boolean v1, Lcom/android/server/power/PowerManagerServiceImpl;->IS_FOLDABLE_DEVICE:Z

    .line 197
    const-string v0, "brightness_decrease_ratio"

    const v1, 0x3f666666    # 0.9f

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getFloat(Ljava/lang/String;F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    sput v0, Lcom/android/server/power/PowerManagerServiceImpl;->mBrightnessDecreaseRatio:F

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 85
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceStub;-><init>()V

    .line 146
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mVisibleWindowUids:Ljava/util/List;

    .line 157
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mUserActivityTimeoutOverrideFromMirrorManager:J

    .line 165
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mIsKeyguardHasShownWhenExitHangup:Z

    .line 167
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mClientDeathCallbacks:Ljava/util/HashMap;

    .line 201
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mBrightWaitTime:J

    .line 205
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSubScreenSuperPowerSaveMode:I

    .line 231
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mScreenWakeLockWhitelists:Ljava/util/List;

    .line 382
    new-instance v0, Lcom/android/server/power/PowerManagerServiceImpl$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/android/server/power/PowerManagerServiceImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/power/PowerManagerServiceImpl;)V

    iput-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mUpdateForegroundAppRunnable:Ljava/lang/Runnable;

    .line 384
    new-instance v0, Lcom/android/server/power/PowerManagerServiceImpl$1;

    invoke-direct {v0, p0}, Lcom/android/server/power/PowerManagerServiceImpl$1;-><init>(Lcom/android/server/power/PowerManagerServiceImpl;)V

    iput-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mTaskStackListener:Landroid/app/TaskStackListener;

    .line 392
    new-instance v0, Lcom/android/server/power/PowerManagerServiceImpl$2;

    invoke-direct {v0, p0}, Lcom/android/server/power/PowerManagerServiceImpl$2;-><init>(Lcom/android/server/power/PowerManagerServiceImpl;)V

    iput-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mWindowListener:Lmiui/process/IForegroundWindowListener;

    .line 404
    new-instance v0, Lcom/android/server/power/PowerManagerServiceImpl$3;

    invoke-direct {v0, p0}, Lcom/android/server/power/PowerManagerServiceImpl$3;-><init>(Lcom/android/server/power/PowerManagerServiceImpl;)V

    iput-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mFreeformCallBack:Lmiui/app/IFreeformCallback;

    .line 1339
    new-instance v0, Lcom/android/server/power/PowerManagerServiceImpl$5;

    invoke-direct {v0, p0}, Lcom/android/server/power/PowerManagerServiceImpl$5;-><init>(Lcom/android/server/power/PowerManagerServiceImpl;)V

    iput-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPickUpSensorListener:Landroid/hardware/SensorEventListener;

    return-void
.end method

.method private boostScreenBrightness(Landroid/os/IBinder;Z)V
    .locals 4
    .param p1, "token"    # Landroid/os/IBinder;
    .param p2, "enabled"    # Z

    .line 1472
    if-eqz p1, :cond_0

    .line 1476
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 1477
    .local v0, "uid":I
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v1

    .line 1479
    .local v1, "ident":J
    :try_start_0
    invoke-direct {p0, p1, p2, v0}, Lcom/android/server/power/PowerManagerServiceImpl;->boostScreenBrightnessInternal(Landroid/os/IBinder;ZI)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1481
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 1482
    nop

    .line 1483
    return-void

    .line 1481
    :catchall_0
    move-exception v3

    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 1482
    throw v3

    .line 1473
    .end local v0    # "uid":I
    .end local v1    # "ident":J
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "token must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private boostScreenBrightnessInternal(Landroid/os/IBinder;ZI)V
    .locals 4
    .param p1, "token"    # Landroid/os/IBinder;
    .param p2, "enabled"    # Z
    .param p3, "uid"    # I

    .line 1315
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1316
    :try_start_0
    iget-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mBrightnessBoostForSoftLightInProgress:Z

    if-eq v1, p2, :cond_1

    .line 1317
    const/4 v1, 0x2

    invoke-direct {p0, p1, v1, p2}, Lcom/android/server/power/PowerManagerServiceImpl;->setDeathCallbackLocked(Landroid/os/IBinder;IZ)V

    .line 1318
    iput-boolean p2, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mBrightnessBoostForSoftLightInProgress:Z

    .line 1319
    if-eqz p2, :cond_0

    .line 1320
    const-string v1, "PowerManagerServiceImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Start boosting screen brightness: uid = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1321
    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPowerManager:Landroid/os/PowerManager;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/os/PowerManager;->boostScreenBrightness(J)V

    goto :goto_0

    .line 1323
    :cond_0
    invoke-direct {p0, p3}, Lcom/android/server/power/PowerManagerServiceImpl;->stopBoostingBrightnessLocked(I)V

    .line 1326
    :cond_1
    :goto_0
    monitor-exit v0

    .line 1327
    return-void

    .line 1326
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private clearWakeUpFlags(Landroid/os/IBinder;I)V
    .locals 3
    .param p1, "token"    # Landroid/os/IBinder;
    .param p2, "flag"    # I

    .line 1581
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 1583
    .local v0, "ident":J
    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/android/server/power/PowerManagerServiceImpl;->clearWakeUpFlagsInternal(Landroid/os/IBinder;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1585
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 1586
    nop

    .line 1587
    return-void

    .line 1585
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 1586
    throw v2
.end method

.method private clearWakeUpFlagsInternal(Landroid/os/IBinder;I)V
    .locals 9
    .param p1, "token"    # Landroid/os/IBinder;
    .param p2, "flag"    # I

    .line 1632
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1633
    :try_start_0
    sget-boolean v1, Lmiui/os/DeviceFeature;->IS_SUBSCREEN_DEVICE:Z

    if-eqz v1, :cond_0

    and-int/lit8 v1, p2, 0x1

    if-eqz v1, :cond_0

    .line 1634
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mAlwaysWakeUp:Z

    .line 1635
    invoke-virtual {p0, p1}, Lcom/android/server/power/PowerManagerServiceImpl;->unregisterDeathCallbackLocked(Landroid/os/IBinder;)V

    .line 1636
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 1637
    .local v4, "now":J
    iget-object v2, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPowerManagerService:Lcom/android/server/power/PowerManagerService;

    const/4 v3, 0x2

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x3e8

    invoke-virtual/range {v2 .. v8}, Lcom/android/server/power/PowerManagerService;->userActivitySecondaryDisplay(IJIII)V

    .line 1640
    .end local v4    # "now":J
    :cond_0
    monitor-exit v0

    .line 1641
    return-void

    .line 1640
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private doDieLocked()V
    .locals 0

    .line 1266
    invoke-virtual {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->clearRequestDimmingParamsLocked()V

    .line 1267
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->resetScreenProjectionSettingsLocked()V

    .line 1268
    return-void
.end method

.method private doDieLocked(I)V
    .locals 1
    .param p1, "flag"    # I

    .line 1271
    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mAlwaysWakeUp:Z

    if-eqz v0, :cond_0

    .line 1272
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mAlwaysWakeUp:Z

    .line 1273
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPowerManagerService:Lcom/android/server/power/PowerManagerService;

    invoke-virtual {v0}, Lcom/android/server/power/PowerManagerService;->updatePowerStateLocked()V

    goto :goto_0

    .line 1274
    :cond_0
    if-nez p1, :cond_1

    .line 1275
    invoke-virtual {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->clearRequestDimmingParamsLocked()V

    .line 1276
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->resetScreenProjectionSettingsLocked()V

    goto :goto_0

    .line 1277
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 1278
    const/16 v0, 0x3e8

    invoke-direct {p0, v0}, Lcom/android/server/power/PowerManagerServiceImpl;->stopBoostingBrightnessLocked(I)V

    .line 1280
    :cond_2
    :goto_0
    return-void
.end method

.method private getRealOwners(Lcom/android/server/power/PowerManagerService$WakeLock;)[I
    .locals 4
    .param p1, "wakeLock"    # Lcom/android/server/power/PowerManagerService$WakeLock;

    .line 617
    const/4 v0, 0x0

    new-array v1, v0, [I

    .line 618
    .local v1, "realOwners":[I
    iget-object v2, p1, Lcom/android/server/power/PowerManagerService$WakeLock;->mWorkSource:Landroid/os/WorkSource;

    if-nez v2, :cond_0

    .line 619
    const/4 v2, 0x1

    new-array v1, v2, [I

    .line 620
    iget v2, p1, Lcom/android/server/power/PowerManagerService$WakeLock;->mOwnerUid:I

    aput v2, v1, v0

    goto :goto_1

    .line 622
    :cond_0
    iget-object v0, p1, Lcom/android/server/power/PowerManagerService$WakeLock;->mWorkSource:Landroid/os/WorkSource;

    invoke-virtual {v0}, Landroid/os/WorkSource;->size()I

    move-result v0

    .line 623
    .local v0, "N":I
    new-array v1, v0, [I

    .line 624
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_1

    .line 625
    iget-object v3, p1, Lcom/android/server/power/PowerManagerService$WakeLock;->mWorkSource:Landroid/os/WorkSource;

    invoke-virtual {v3, v2}, Landroid/os/WorkSource;->get(I)I

    move-result v3

    aput v3, v1, v2

    .line 624
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 628
    .end local v0    # "N":I
    .end local v2    # "i":I
    :cond_1
    :goto_1
    return-object v1
.end method

.method private goToSleepSecondaryDisplay(JLjava/lang/String;)V
    .locals 10
    .param p1, "eventTime"    # J
    .param p3, "reason"    # Ljava/lang/String;

    .line 1540
    sget-boolean v0, Lmiui/os/DeviceFeature;->IS_SUBSCREEN_DEVICE:Z

    if-nez v0, :cond_0

    .line 1541
    return-void

    .line 1543
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    cmp-long v0, p1, v0

    if-gtz v0, :cond_4

    .line 1547
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 1548
    .local v0, "uid":I
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v7

    .line 1551
    .local v7, "ident":J
    :try_start_0
    iget-object v9, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLock:Ljava/lang/Object;

    monitor-enter v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1552
    :try_start_1
    iget-wide v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLastSecondaryDisplayWakeUpTime:J

    cmp-long v1, p1, v1

    if-ltz v1, :cond_3

    .line 1553
    invoke-virtual {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->getSecondaryDisplayWakefulnessLocked()I

    move-result v1

    invoke-static {v1}, Landroid/os/PowerManagerInternal;->isInteractive(I)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSystemReady:Z

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mBootCompleted:Z

    if-nez v1, :cond_1

    goto :goto_0

    .line 1559
    :cond_1
    invoke-direct {p0, p3}, Lcom/android/server/power/PowerManagerServiceImpl;->shouldGoToSleepWhileAlwaysOn(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1560
    const-string v1, "PowerManagerServiceImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Ignore "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to apply secondary display sleep while mAlwaysWakeUp"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1561
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1572
    invoke-static {v7, v8}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 1561
    return-void

    .line 1563
    :cond_2
    :try_start_2
    iput-wide p1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLastSecondaryDisplayGoToSleepTime:J

    .line 1564
    iput-object p3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLastSecondaryDisplayGoToSleepReason:Ljava/lang/String;

    .line 1565
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mAlwaysWakeUp:Z

    .line 1567
    const-string v1, "PowerManagerServiceImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Going to sleep secondary display from: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", reason: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1568
    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPowerManagerService:Lcom/android/server/power/PowerManagerService;

    iget-object v2, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPowerGroups:Landroid/util/SparseArray;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/power/PowerGroup;

    const/4 v5, 0x0

    move-wide v3, p1

    move v6, v0

    invoke-virtual/range {v1 .. v6}, Lcom/android/server/power/PowerManagerService;->goToSleepSecondaryDisplay(Lcom/android/server/power/PowerGroup;JII)V

    .line 1570
    monitor-exit v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1572
    invoke-static {v7, v8}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 1573
    nop

    .line 1574
    return-void

    .line 1556
    :cond_3
    :goto_0
    :try_start_3
    const-string v1, "PowerManagerServiceImpl"

    const-string v2, "goToSleepSecondaryDisplay: return"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1557
    monitor-exit v9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1572
    invoke-static {v7, v8}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 1557
    return-void

    .line 1570
    :catchall_0
    move-exception v1

    :try_start_4
    monitor-exit v9
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .end local v0    # "uid":I
    .end local v7    # "ident":J
    .end local p0    # "this":Lcom/android/server/power/PowerManagerServiceImpl;
    .end local p1    # "eventTime":J
    .end local p3    # "reason":Ljava/lang/String;
    :try_start_5
    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1572
    .restart local v0    # "uid":I
    .restart local v7    # "ident":J
    .restart local p0    # "this":Lcom/android/server/power/PowerManagerServiceImpl;
    .restart local p1    # "eventTime":J
    .restart local p3    # "reason":Ljava/lang/String;
    :catchall_1
    move-exception v1

    invoke-static {v7, v8}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 1573
    throw v1

    .line 1544
    .end local v0    # "uid":I
    .end local v7    # "ident":J
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "event time must not be in the future"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private handleSettingsChangedLocked(Landroid/net/Uri;)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .line 1152
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    goto/16 :goto_0

    :sswitch_0
    const-string/jumbo v1, "screen_project_in_screening"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto/16 :goto_1

    :sswitch_1
    const-string v1, "drive_mode_drive_mode"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    goto/16 :goto_1

    :sswitch_2
    const-string v1, "POWER_SAVE_MODE_OPEN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xc

    goto/16 :goto_1

    :sswitch_3
    const-string v1, "gaze_lock_screen_setting"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xb

    goto :goto_1

    :sswitch_4
    const-string v1, "accelerometer_rotation"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    goto :goto_1

    :sswitch_5
    const-string/jumbo v1, "subscreen_super_power_save_mode"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    goto :goto_1

    :sswitch_6
    const-string v1, "pick_up_gesture_wakeup_mode"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x9

    goto :goto_1

    :sswitch_7
    const-string/jumbo v1, "screen_project_hang_up_on"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_8
    const-string/jumbo v1, "user_setup_complete"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x7

    goto :goto_1

    :sswitch_9
    const-string v1, "adaptive_sleep"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xa

    goto :goto_1

    :sswitch_a
    const-string/jumbo v1, "synergy_mode"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    goto :goto_1

    :sswitch_b
    const-string v1, "com.xiaomi.system.devicelock.locked"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x5

    goto :goto_1

    :sswitch_c
    const-string/jumbo v1, "subscreen_display_time"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    goto :goto_1

    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_0

    goto :goto_2

    .line 1189
    :pswitch_0
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updatePowerSaveMode()V

    .line 1190
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPowerManagerService:Lcom/android/server/power/PowerManagerService;

    invoke-virtual {v0}, Lcom/android/server/power/PowerManagerService;->updatePowerStateLocked()V

    .line 1191
    goto :goto_2

    .line 1186
    :pswitch_1
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updateAonScreenOffConfig()V

    .line 1187
    goto :goto_2

    .line 1183
    :pswitch_2
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updateAdaptiveSleepLocked()V

    .line 1184
    goto :goto_2

    .line 1180
    :pswitch_3
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updatePickUpGestureWakeUpModeLocked()V

    .line 1181
    goto :goto_2

    .line 1177
    :pswitch_4
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updateSubScreenSuperPowerSaveMode()V

    .line 1178
    goto :goto_2

    .line 1174
    :pswitch_5
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updateUserSetupCompleteLocked()V

    .line 1175
    goto :goto_2

    .line 1171
    :pswitch_6
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updateSecondaryDisplayScreenOffTimeoutLocked()V

    .line 1172
    goto :goto_2

    .line 1168
    :pswitch_7
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updateDeviceLockState()V

    .line 1169
    goto :goto_2

    .line 1165
    :pswitch_8
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updateDriveModeLocked()V

    .line 1166
    goto :goto_2

    .line 1162
    :pswitch_9
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mNotifierInjector:Lcom/android/server/power/NotifierInjector;

    invoke-virtual {v0}, Lcom/android/server/power/NotifierInjector;->updateAccelerometerRotationLocked()V

    .line 1163
    goto :goto_2

    .line 1156
    :pswitch_a
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updateScreenProjectionLocked()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1157
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPowerManagerService:Lcom/android/server/power/PowerManagerService;

    invoke-virtual {v0}, Lcom/android/server/power/PowerManagerService;->updatePowerStateLocked()V

    .line 1158
    return-void

    .line 1195
    :cond_1
    :goto_2
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x6e271123 -> :sswitch_c
        -0x57f0fb82 -> :sswitch_b
        -0x55bbaa45 -> :sswitch_a
        -0x46cdc832 -> :sswitch_9
        -0x3a0f4851 -> :sswitch_8
        -0x35ba1871 -> :sswitch_7
        -0x350b0519 -> :sswitch_6
        -0x1a88b1ac -> :sswitch_5
        0x4d5796e -> :sswitch_4
        0x53b87d57 -> :sswitch_3
        0x56c145be -> :sswitch_2
        0x593d229f -> :sswitch_1
        0x790152b5 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_a
        :pswitch_a
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic lambda$setBootPhase$0()V
    .locals 1

    .line 926
    invoke-static {}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->getInstance()Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->bootCompleted()V

    return-void
.end method

.method private loadSettingsLocked()V
    .locals 0

    .line 325
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updateDriveModeLocked()V

    .line 326
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updateSecondaryDisplayScreenOffTimeoutLocked()V

    .line 327
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updateUserSetupCompleteLocked()V

    .line 328
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updatePickUpGestureWakeUpModeLocked()V

    .line 329
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updateAdaptiveSleepLocked()V

    .line 330
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updateAonScreenOffConfig()V

    .line 331
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updatePowerSaveMode()V

    .line 332
    return-void
.end method

.method private onFoldStateChanged(Z)V
    .locals 1
    .param p1, "folded"    # Z

    .line 498
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mFolded:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, p1, :cond_0

    .line 499
    return-void

    .line 501
    :cond_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mFolded:Ljava/lang/Boolean;

    .line 502
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updateProximityWakeLockDisabledState()V

    .line 503
    return-void
.end method

.method private printProximityWakeLockStatusChange(Lcom/android/server/power/PowerManagerService$WakeLock;Z)V
    .locals 3
    .param p1, "wakeLock"    # Lcom/android/server/power/PowerManagerService$WakeLock;
    .param p2, "disabled"    # Z

    .line 570
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 571
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "Proximity wakelock:["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 572
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 573
    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 574
    const-string v2, ", disabled: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 575
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 576
    const-string v2, ", reason: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 577
    if-eqz p2, :cond_2

    .line 578
    sget-boolean v1, Lcom/android/server/power/PowerManagerServiceImpl;->IS_FOLDABLE_DEVICE:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mFolded:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mNoSupprotInnerScreenProximitySensor:Z

    if-eqz v1, :cond_0

    .line 579
    const-string v1, "in unfold state"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 581
    :cond_0
    iget-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mIsFreeFormOrSplitWindowMode:Z

    if-eqz v1, :cond_1

    const-string v1, "in freeform or split window mode"

    goto :goto_0

    .line 582
    :cond_1
    const-string v1, "in background"

    .line 581
    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 585
    :cond_2
    sget-boolean v1, Lcom/android/server/power/PowerManagerServiceImpl;->IS_FOLDABLE_DEVICE:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mFolded:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_3

    iget-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mNoSupprotInnerScreenProximitySensor:Z

    if-nez v1, :cond_3

    .line 586
    const-string v1, "making a call from the internal screen"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 588
    :cond_3
    const-string v1, "in foreground"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 591
    :goto_1
    const-string v1, "PowerManagerServiceImpl"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 592
    return-void
.end method

.method private registerForegroundAppObserver()V
    .locals 2

    .line 371
    :try_start_0
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mActivityTaskManager:Landroid/app/IActivityTaskManager;

    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mTaskStackListener:Landroid/app/TaskStackListener;

    invoke-interface {v0, v1}, Landroid/app/IActivityTaskManager;->registerTaskStackListener(Landroid/app/ITaskStackListener;)V

    .line 372
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mFreeformCallBack:Lmiui/app/IFreeformCallback;

    invoke-static {v0}, Lmiui/app/MiuiFreeFormManager;->registerFreeformCallback(Lmiui/app/IFreeformCallback;)V

    .line 373
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mWindowListener:Lmiui/process/IForegroundWindowListener;

    invoke-static {v0}, Lmiui/process/ProcessManager;->registerForegroundWindowListener(Lmiui/process/IForegroundWindowListener;)V

    .line 376
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updateForegroundApp()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 379
    goto :goto_0

    .line 377
    :catch_0
    move-exception v0

    .line 380
    :goto_0
    return-void
.end method

.method private requestHangUpWhenScreenProject(Landroid/os/IBinder;Z)V
    .locals 3
    .param p1, "token"    # Landroid/os/IBinder;
    .param p2, "hangup"    # Z

    .line 1302
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 1304
    .local v0, "ident":J
    if-eqz p1, :cond_0

    .line 1305
    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/android/server/power/PowerManagerServiceImpl;->requestHangUpWhenScreenProjectInternal(Landroid/os/IBinder;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1308
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 1309
    throw v2

    .line 1308
    :cond_0
    :goto_0
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 1309
    nop

    .line 1310
    return-void
.end method

.method private requestHangUpWhenScreenProjectInternal(Landroid/os/IBinder;Z)V
    .locals 2
    .param p1, "token"    # Landroid/os/IBinder;
    .param p2, "hangup"    # Z

    .line 1223
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1224
    const/4 v1, 0x0

    :try_start_0
    invoke-direct {p0, p1, v1, p2}, Lcom/android/server/power/PowerManagerServiceImpl;->setDeathCallbackLocked(Landroid/os/IBinder;IZ)V

    .line 1225
    invoke-direct {p0, p2}, Lcom/android/server/power/PowerManagerServiceImpl;->setHangUpModeLocked(Z)V

    .line 1226
    monitor-exit v0

    .line 1227
    return-void

    .line 1226
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private resetScreenProjectionSettingsLocked()V
    .locals 3

    .line 886
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "screen_project_in_screening"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 888
    invoke-direct {p0, v2}, Lcom/android/server/power/PowerManagerServiceImpl;->setHangUpModeLocked(Z)V

    .line 889
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "synergy_mode"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 891
    return-void
.end method

.method private setDeathCallbackLocked(Landroid/os/IBinder;IZ)V
    .locals 2
    .param p1, "token"    # Landroid/os/IBinder;
    .param p2, "flag"    # I
    .param p3, "register"    # Z

    .line 1230
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1231
    if-eqz p3, :cond_0

    .line 1232
    :try_start_0
    invoke-virtual {p0, p1, p2}, Lcom/android/server/power/PowerManagerServiceImpl;->registerDeathCallbackLocked(Landroid/os/IBinder;I)V

    goto :goto_0

    .line 1234
    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/server/power/PowerManagerServiceImpl;->unregisterDeathCallbackLocked(Landroid/os/IBinder;)V

    .line 1236
    :goto_0
    monitor-exit v0

    .line 1237
    return-void

    .line 1236
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private setHangUpModeLocked(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .line 894
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 895
    nop

    .line 894
    const-string/jumbo v1, "screen_project_hang_up_on"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 896
    return-void
.end method

.method private setWakeLockDisabledStateLocked(Lcom/android/server/power/PowerManagerService$WakeLock;Z)Z
    .locals 2
    .param p1, "wakeLock"    # Lcom/android/server/power/PowerManagerService$WakeLock;
    .param p2, "disabled"    # Z

    .line 719
    const/4 v0, 0x0

    .line 720
    .local v0, "changed":Z
    iget-boolean v1, p1, Lcom/android/server/power/PowerManagerService$WakeLock;->mDisabled:Z

    if-eq v1, p2, :cond_0

    .line 721
    const/4 v0, 0x1

    .line 722
    iput-boolean p2, p1, Lcom/android/server/power/PowerManagerService$WakeLock;->mDisabled:Z

    .line 724
    :cond_0
    return v0
.end method

.method private shouldGoToSleepWhileAlwaysOn(Ljava/lang/String;)Z
    .locals 1
    .param p1, "reason"    # Ljava/lang/String;

    .line 1628
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mAlwaysWakeUp:Z

    if-eqz v0, :cond_0

    const-string v0, "CAMERA_CALL"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private stopBoostingBrightnessLocked(I)V
    .locals 2
    .param p1, "uid"    # I

    .line 1330
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "stop boosting screen brightness: uid = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PowerManagerServiceImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1331
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mBrightnessBoostForSoftLightInProgress:Z

    if-eqz v0, :cond_0

    .line 1332
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mBrightnessBoostForSoftLightInProgress:Z

    .line 1334
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPendingStopBrightnessBoost:Z

    .line 1335
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPowerManagerService:Lcom/android/server/power/PowerManagerService;

    invoke-virtual {v0}, Lcom/android/server/power/PowerManagerService;->updatePowerStateLocked()V

    .line 1336
    return-void
.end method

.method private systemReady()V
    .locals 6

    .line 271
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x1110013

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSupportAdaptiveSleep:Z

    .line 273
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x1103004f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 275
    .local v0, "whitelist":[Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mScreenWakeLockWhitelists:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 276
    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x1105006b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mNoSupprotInnerScreenProximitySensor:Z

    .line 279
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->registerForegroundAppObserver()V

    .line 280
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->resetScreenProjectionSettingsLocked()V

    .line 281
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->loadSettingsLocked()V

    .line 283
    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mResolver:Landroid/content/ContentResolver;

    const-string/jumbo v2, "screen_project_in_screening"

    invoke-static {v2}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSettingsObserver:Lcom/android/server/power/PowerManagerServiceImpl$SettingsObserver;

    const/4 v4, 0x0

    const/4 v5, -0x1

    invoke-virtual {v1, v2, v4, v3, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 286
    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mResolver:Landroid/content/ContentResolver;

    const-string/jumbo v2, "screen_project_hang_up_on"

    invoke-static {v2}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSettingsObserver:Lcom/android/server/power/PowerManagerServiceImpl$SettingsObserver;

    invoke-virtual {v1, v2, v4, v3, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 289
    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mResolver:Landroid/content/ContentResolver;

    const-string/jumbo v2, "synergy_mode"

    invoke-static {v2}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSettingsObserver:Lcom/android/server/power/PowerManagerServiceImpl$SettingsObserver;

    invoke-virtual {v1, v2, v4, v3, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 292
    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mResolver:Landroid/content/ContentResolver;

    const-string v2, "accelerometer_rotation"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSettingsObserver:Lcom/android/server/power/PowerManagerServiceImpl$SettingsObserver;

    invoke-virtual {v1, v2, v4, v3, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 295
    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mResolver:Landroid/content/ContentResolver;

    const-string v2, "com.xiaomi.system.devicelock.locked"

    invoke-static {v2}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSettingsObserver:Lcom/android/server/power/PowerManagerServiceImpl$SettingsObserver;

    invoke-virtual {v1, v2, v4, v3, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 298
    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mResolver:Landroid/content/ContentResolver;

    const-string/jumbo v2, "subscreen_display_time"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSettingsObserver:Lcom/android/server/power/PowerManagerServiceImpl$SettingsObserver;

    invoke-virtual {v1, v2, v4, v3, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 301
    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mResolver:Landroid/content/ContentResolver;

    const-string/jumbo v2, "subscreen_super_power_save_mode"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSettingsObserver:Lcom/android/server/power/PowerManagerServiceImpl$SettingsObserver;

    invoke-virtual {v1, v2, v4, v3, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 304
    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mResolver:Landroid/content/ContentResolver;

    const-string v2, "drive_mode_drive_mode"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSettingsObserver:Lcom/android/server/power/PowerManagerServiceImpl$SettingsObserver;

    invoke-virtual {v1, v2, v4, v3, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 307
    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mResolver:Landroid/content/ContentResolver;

    const-string v2, "pick_up_gesture_wakeup_mode"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSettingsObserver:Lcom/android/server/power/PowerManagerServiceImpl$SettingsObserver;

    invoke-virtual {v1, v2, v4, v3, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 310
    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mResolver:Landroid/content/ContentResolver;

    const-string v2, "adaptive_sleep"

    invoke-static {v2}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSettingsObserver:Lcom/android/server/power/PowerManagerServiceImpl$SettingsObserver;

    invoke-virtual {v1, v2, v4, v3, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 312
    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mResolver:Landroid/content/ContentResolver;

    const-string v2, "gaze_lock_screen_setting"

    invoke-static {v2}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSettingsObserver:Lcom/android/server/power/PowerManagerServiceImpl$SettingsObserver;

    invoke-virtual {v1, v2, v4, v3, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 314
    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mResolver:Landroid/content/ContentResolver;

    const-string v2, "POWER_SAVE_MODE_OPEN"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSettingsObserver:Lcom/android/server/power/PowerManagerServiceImpl$SettingsObserver;

    invoke-virtual {v1, v2, v4, v3, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 316
    sget-boolean v1, Lcom/android/server/power/PowerManagerServiceImpl;->IS_FOLDABLE_DEVICE:Z

    if-eqz v1, :cond_0

    .line 317
    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mContext:Landroid/content/Context;

    const-class v2, Landroid/hardware/devicestate/DeviceStateManager;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/devicestate/DeviceStateManager;

    .line 318
    .local v1, "deviceStateManager":Landroid/hardware/devicestate/DeviceStateManager;
    new-instance v2, Landroid/os/HandlerExecutor;

    iget-object v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mHandler:Landroid/os/Handler;

    invoke-direct {v2, v3}, Landroid/os/HandlerExecutor;-><init>(Landroid/os/Handler;)V

    new-instance v3, Landroid/hardware/devicestate/DeviceStateManager$FoldStateListener;

    iget-object v4, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mContext:Landroid/content/Context;

    new-instance v5, Lcom/android/server/power/PowerManagerServiceImpl$$ExternalSyntheticLambda1;

    invoke-direct {v5, p0}, Lcom/android/server/power/PowerManagerServiceImpl$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/power/PowerManagerServiceImpl;)V

    invoke-direct {v3, v4, v5}, Landroid/hardware/devicestate/DeviceStateManager$FoldStateListener;-><init>(Landroid/content/Context;Ljava/util/function/Consumer;)V

    invoke-virtual {v1, v2, v3}, Landroid/hardware/devicestate/DeviceStateManager;->registerCallback(Ljava/util/concurrent/Executor;Landroid/hardware/devicestate/DeviceStateManager$DeviceStateCallback;)V

    .line 321
    .end local v1    # "deviceStateManager":Landroid/hardware/devicestate/DeviceStateManager;
    :cond_0
    return-void
.end method

.method private updateAdaptiveSleepLocked()V
    .locals 4

    .line 345
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mResolver:Landroid/content/ContentResolver;

    const/4 v1, -0x2

    const-string v2, "adaptive_sleep"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    move v3, v1

    :cond_0
    iput-boolean v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mAdaptiveSleepEnabled:Z

    .line 347
    return-void
.end method

.method private updateAlwaysWakeUpIfNeededLocked(Landroid/os/IBinder;I)V
    .locals 1
    .param p1, "token"    # Landroid/os/IBinder;
    .param p2, "flag"    # I

    .line 1590
    if-eqz p1, :cond_0

    and-int/lit8 v0, p2, 0x1

    if-eqz v0, :cond_0

    .line 1591
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mAlwaysWakeUp:Z

    .line 1592
    invoke-direct {p0, p1, p2, v0}, Lcom/android/server/power/PowerManagerServiceImpl;->setDeathCallbackLocked(Landroid/os/IBinder;IZ)V

    .line 1594
    :cond_0
    return-void
.end method

.method private updateAonScreenOffConfig()V
    .locals 4

    .line 350
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, -0x2

    const-string v2, "gaze_lock_screen_setting"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v3, 0x1

    :cond_0
    iput-boolean v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mAonScreenOffEnabled:Z

    .line 352
    return-void
.end method

.method private updateDeviceLockState()V
    .locals 3

    .line 1219
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "com.xiaomi.system.devicelock.locked"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    :cond_0
    iput-boolean v2, p0, Lcom/android/server/power/PowerManagerServiceImpl;->isDeviceLock:Z

    .line 1220
    return-void
.end method

.method private updateDriveModeLocked()V
    .locals 4

    .line 335
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mResolver:Landroid/content/ContentResolver;

    const/4 v1, 0x0

    const/4 v2, -0x2

    const-string v3, "drive_mode_drive_mode"

    invoke-static {v0, v3, v1, v2}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    iput v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mDriveMode:I

    .line 337
    return-void
.end method

.method private updateForegroundApp()V
    .locals 4

    .line 422
    :try_start_0
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mActivityTaskManager:Landroid/app/IActivityTaskManager;

    invoke-interface {v0}, Landroid/app/IActivityTaskManager;->getFocusedRootTaskInfo()Landroid/app/ActivityTaskManager$RootTaskInfo;

    move-result-object v0

    .line 423
    .local v0, "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
    if-eqz v0, :cond_3

    iget-object v1, v0, Landroid/app/ActivityTaskManager$RootTaskInfo;->topActivity:Landroid/content/ComponentName;

    if-nez v1, :cond_0

    goto :goto_2

    .line 426
    :cond_0
    invoke-virtual {v0}, Landroid/app/ActivityTaskManager$RootTaskInfo;->getWindowingMode()I

    move-result v1

    const/4 v2, 0x5

    const/4 v3, 0x1

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mActivityTaskManager:Landroid/app/IActivityTaskManager;

    .line 427
    invoke-interface {v1}, Landroid/app/IActivityTaskManager;->isInSplitScreenWindowingMode()Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    move v1, v3

    :goto_1
    iput-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mIsFreeFormOrSplitWindowMode:Z

    .line 428
    iget-object v1, v0, Landroid/app/ActivityTaskManager$RootTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 430
    .local v1, "packageName":Ljava/lang/String;
    iput-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPendingForegroundAppPackageName:Ljava/lang/String;

    .line 431
    iget-object v2, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 432
    iget-object v2, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 435
    nop

    .end local v0    # "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
    .end local v1    # "packageName":Ljava/lang/String;
    goto :goto_3

    .line 424
    .restart local v0    # "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
    :cond_3
    :goto_2
    return-void

    .line 433
    .end local v0    # "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
    :catch_0
    move-exception v0

    .line 436
    :goto_3
    return-void
.end method

.method private updateForegroundAppPackageName()V
    .locals 2

    .line 439
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPendingForegroundAppPackageName:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mForegroundAppPackageName:Ljava/lang/String;

    .line 440
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPendingForegroundAppPackageName:Ljava/lang/String;

    .line 441
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updateProximityWakeLockDisabledState()V

    .line 443
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mMiuiPowerStatisticTracker:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;

    if-eqz v0, :cond_0

    .line 444
    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mForegroundAppPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->notifyForegroundAppChanged(Ljava/lang/String;)V

    .line 446
    :cond_0
    return-void
.end method

.method private updatePickUpGestureWakeUpModeLocked()V
    .locals 4

    .line 340
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mResolver:Landroid/content/ContentResolver;

    const/4 v1, -0x2

    const-string v2, "pick_up_gesture_wakeup_mode"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    move v3, v1

    :cond_0
    iput-boolean v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPickUpGestureWakeUpEnabled:Z

    .line 342
    return-void
.end method

.method private updatePowerSaveMode()V
    .locals 4

    .line 1645
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mResolver:Landroid/content/ContentResolver;

    const/4 v1, -0x2

    const-string v2, "POWER_SAVE_MODE_OPEN"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v3, 0x1

    :cond_0
    move v0, v3

    .line 1647
    .local v0, "isPowerSaveModeEnabled":Z
    iput-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mIsPowerSaveModeEnabled:Z

    .line 1648
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updatePowerSaveMode: mIsPowerSaveModeEnabled: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mIsPowerSaveModeEnabled:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "PowerManagerServiceImpl"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1649
    return-void
.end method

.method private updateProximityWakeLockDisabledState()V
    .locals 4

    .line 449
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 450
    :try_start_0
    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mWakeLocks:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/power/PowerManagerService$WakeLock;

    .line 451
    .local v2, "wakeLock":Lcom/android/server/power/PowerManagerService$WakeLock;
    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, Lcom/android/server/power/PowerManagerServiceImpl;->setProximityWakeLockDisabledStateLocked(Lcom/android/server/power/PowerManagerService$WakeLock;Z)V

    .line 452
    .end local v2    # "wakeLock":Lcom/android/server/power/PowerManagerService$WakeLock;
    goto :goto_0

    .line 453
    :cond_0
    monitor-exit v0

    .line 454
    return-void

    .line 453
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private updateScreenProjectionLocked()Z
    .locals 6

    .line 994
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mResolver:Landroid/content/ContentResolver;

    const-string/jumbo v1, "screen_project_in_screening"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v2

    .line 996
    .local v0, "screenProjectingEnabled":Z
    :goto_0
    iget-object v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mResolver:Landroid/content/ContentResolver;

    const-string/jumbo v4, "screen_project_hang_up_on"

    invoke-static {v3, v4, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-ne v3, v1, :cond_1

    move v3, v1

    goto :goto_1

    :cond_1
    move v3, v2

    .line 998
    .local v3, "hangUpEnabled":Z
    :goto_1
    iget-object v4, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mResolver:Landroid/content/ContentResolver;

    const-string/jumbo v5, "synergy_mode"

    invoke-static {v4, v5, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    if-ne v4, v1, :cond_2

    move v4, v1

    goto :goto_2

    :cond_2
    move v4, v2

    .line 1000
    .local v4, "synergyModeEnable":Z
    :goto_2
    iget-boolean v5, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mScreenProjectionEnabled:Z

    if-ne v0, v5, :cond_4

    iget-boolean v5, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mHangUpEnabled:Z

    if-ne v3, v5, :cond_4

    iget-boolean v5, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSynergyModeEnable:Z

    if-eq v4, v5, :cond_3

    goto :goto_3

    .line 1021
    :cond_3
    return v2

    .line 1003
    :cond_4
    :goto_3
    if-nez v3, :cond_5

    iget-boolean v5, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mHangUpEnabled:Z

    if-eqz v5, :cond_5

    .line 1004
    iput-boolean v2, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mHangUpEnabled:Z

    .line 1005
    return v1

    .line 1007
    :cond_5
    iput-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mScreenProjectionEnabled:Z

    .line 1008
    iput-boolean v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mHangUpEnabled:Z

    .line 1009
    iput-boolean v4, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSynergyModeEnable:Z

    .line 1010
    if-nez v0, :cond_6

    if-eqz v4, :cond_7

    :cond_6
    if-eqz v3, :cond_7

    .line 1011
    invoke-virtual {p0, v1}, Lcom/android/server/power/PowerManagerServiceImpl;->hangUpNoUpdateLocked(Z)Z

    goto :goto_4

    .line 1012
    :cond_7
    if-nez v0, :cond_8

    if-nez v4, :cond_8

    .line 1013
    invoke-virtual {p0, v2}, Lcom/android/server/power/PowerManagerServiceImpl;->hangUpNoUpdateLocked(Z)Z

    .line 1016
    :cond_8
    :goto_4
    iget-boolean v5, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mHangUpEnabled:Z

    if-eqz v5, :cond_9

    .line 1017
    invoke-direct {p0, v2}, Lcom/android/server/power/PowerManagerServiceImpl;->setHangUpModeLocked(Z)V

    .line 1019
    :cond_9
    return v1
.end method

.method private updateSecondaryDisplayScreenOffTimeoutLocked()V
    .locals 3

    .line 1612
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mResolver:Landroid/content/ContentResolver;

    const-string/jumbo v1, "subscreen_display_time"

    const/16 v2, 0x3a98

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSecondaryDisplayScreenOffTimeout:J

    .line 1615
    return-void
.end method

.method private updateSubScreenSuperPowerSaveMode()V
    .locals 4

    .line 1606
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mResolver:Landroid/content/ContentResolver;

    const/4 v1, 0x0

    const/4 v2, -0x2

    const-string/jumbo v3, "subscreen_super_power_save_mode"

    invoke-static {v0, v3, v1, v2}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    iput v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSubScreenSuperPowerSaveMode:I

    .line 1609
    return-void
.end method

.method private updateUserSetupCompleteLocked()V
    .locals 4

    .line 1618
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, -0x2

    const-string/jumbo v2, "user_setup_complete"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v3, 0x1

    :cond_0
    iput-boolean v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mIsUserSetupComplete:Z

    .line 1621
    return-void
.end method

.method private wakeUpSecondaryDisplay(JLjava/lang/String;)V
    .locals 6
    .param p1, "eventTime"    # J
    .param p3, "reason"    # Ljava/lang/String;

    .line 1491
    const/4 v1, 0x0

    const/4 v4, 0x0

    move-object v0, p0

    move-wide v2, p1

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/android/server/power/PowerManagerServiceImpl;->wakeUpSecondaryDisplay(Landroid/os/IBinder;JILjava/lang/String;)V

    .line 1492
    return-void
.end method

.method private wakeUpSecondaryDisplay(Landroid/os/IBinder;JILjava/lang/String;)V
    .locals 17
    .param p1, "token"    # Landroid/os/IBinder;
    .param p2, "eventTime"    # J
    .param p4, "flag"    # I
    .param p5, "reason"    # Ljava/lang/String;

    .line 1500
    move-object/from16 v1, p0

    move-wide/from16 v9, p2

    move-object/from16 v11, p5

    sget-boolean v0, Lmiui/os/DeviceFeature;->IS_SUBSCREEN_DEVICE:Z

    if-nez v0, :cond_0

    .line 1501
    return-void

    .line 1503
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    cmp-long v0, v9, v2

    if-gtz v0, :cond_5

    .line 1507
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v12

    .line 1508
    .local v12, "uid":I
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v13

    .line 1511
    .local v13, "ident":J
    :try_start_0
    iget-object v15, v1, Lcom/android/server/power/PowerManagerServiceImpl;->mLock:Ljava/lang/Object;

    monitor-enter v15
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1512
    :try_start_1
    iget-wide v2, v1, Lcom/android/server/power/PowerManagerServiceImpl;->mLastSecondaryDisplayWakeUpTime:J

    cmp-long v0, v9, v2

    if-ltz v0, :cond_4

    .line 1513
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/power/PowerManagerServiceImpl;->getSecondaryDisplayWakefulnessLocked()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    const-string v0, "CAMERA_CALL"

    .line 1516
    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    iget-boolean v0, v1, Lcom/android/server/power/PowerManagerServiceImpl;->mSystemReady:Z

    if-nez v0, :cond_3

    :cond_2
    goto :goto_0

    .line 1522
    :cond_3
    iput-wide v9, v1, Lcom/android/server/power/PowerManagerServiceImpl;->mLastSecondaryDisplayWakeUpTime:J

    iput-wide v9, v1, Lcom/android/server/power/PowerManagerServiceImpl;->mLastSecondaryDisplayUserActivityTime:J

    .line 1523
    iput-object v11, v1, Lcom/android/server/power/PowerManagerServiceImpl;->mLastSecondaryDisplayWakeUpReason:Ljava/lang/String;

    .line 1524
    move-object/from16 v8, p1

    move/from16 v7, p4

    invoke-direct {v1, v8, v7}, Lcom/android/server/power/PowerManagerServiceImpl;->updateAlwaysWakeUpIfNeededLocked(Landroid/os/IBinder;I)V

    .line 1526
    const-string v0, "PowerManagerServiceImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Waking up secondary display from: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", reason: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1527
    iget-object v0, v1, Lcom/android/server/power/PowerManagerServiceImpl;->mPowerManagerService:Lcom/android/server/power/PowerManagerService;

    iget-object v3, v1, Lcom/android/server/power/PowerManagerServiceImpl;->mPowerGroups:Landroid/util/SparseArray;

    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/android/server/power/PowerGroup;

    const/4 v6, 0x0

    const-string/jumbo v16, "sub-display"

    move-object v2, v0

    move-wide/from16 v4, p2

    move-object/from16 v7, v16

    move v8, v12

    invoke-virtual/range {v2 .. v8}, Lcom/android/server/power/PowerManagerService;->wakeUpSecondaryDisplay(Lcom/android/server/power/PowerGroup;JILjava/lang/String;I)V

    .line 1529
    monitor-exit v15
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1531
    invoke-static {v13, v14}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 1532
    nop

    .line 1533
    return-void

    .line 1518
    :cond_4
    :goto_0
    :try_start_2
    const-string v0, "PowerManagerServiceImpl"

    const-string/jumbo v2, "wakeUpSecondaryDisplay: return"

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1519
    monitor-exit v15
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1531
    invoke-static {v13, v14}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 1519
    return-void

    .line 1529
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v15
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .end local v12    # "uid":I
    .end local v13    # "ident":J
    .end local p0    # "this":Lcom/android/server/power/PowerManagerServiceImpl;
    .end local p1    # "token":Landroid/os/IBinder;
    .end local p2    # "eventTime":J
    .end local p4    # "flag":I
    .end local p5    # "reason":Ljava/lang/String;
    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1531
    .restart local v12    # "uid":I
    .restart local v13    # "ident":J
    .restart local p0    # "this":Lcom/android/server/power/PowerManagerServiceImpl;
    .restart local p1    # "token":Landroid/os/IBinder;
    .restart local p2    # "eventTime":J
    .restart local p4    # "flag":I
    .restart local p5    # "reason":Ljava/lang/String;
    :catchall_1
    move-exception v0

    invoke-static {v13, v14}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 1532
    throw v0

    .line 1504
    .end local v12    # "uid":I
    .end local v13    # "ident":J
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "event time must not be in the future"

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public addVisibleWindowUids(I)V
    .locals 2
    .param p1, "uid"    # I

    .line 507
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mVisibleWindowUids:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 508
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mVisibleWindowUids:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 510
    :cond_0
    return-void
.end method

.method protected adjustDirtyIfNeededLocked(I)I
    .locals 2
    .param p1, "mDirty"    # I

    .line 1136
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSynergyModeEnable:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSituatedDimmingDueToSynergy:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPendingSetDirtyDueToRequestDimming:Z

    if-eqz v0, :cond_0

    .line 1138
    or-int/lit8 p1, p1, 0x20

    .line 1139
    iput-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPendingSetDirtyDueToRequestDimming:Z

    .line 1141
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPendingStopBrightnessBoost:Z

    if-eqz v0, :cond_1

    .line 1142
    or-int/lit16 p1, p1, 0x800

    .line 1144
    :cond_1
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSituatedDimmingDueToAttention:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPendingSetDirtyDueToRequestDimming:Z

    if-eqz v0, :cond_2

    .line 1145
    or-int/lit8 p1, p1, 0x20

    .line 1146
    iput-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPendingSetDirtyDueToRequestDimming:Z

    .line 1148
    :cond_2
    return p1
.end method

.method public adjustScreenOffTimeoutIfNeededLocked(Lcom/android/server/power/PowerGroup;J)J
    .locals 2
    .param p1, "powerGroup"    # Lcom/android/server/power/PowerGroup;
    .param p2, "timeout"    # J

    .line 1661
    sget-boolean v0, Lmiui/os/DeviceFeature;->IS_SUBSCREEN_DEVICE:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/android/server/power/PowerGroup;->getGroupId()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1662
    iget-wide v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSecondaryDisplayScreenOffTimeout:J

    return-wide v0

    .line 1664
    :cond_0
    return-wide p2
.end method

.method protected adjustWakeLockDueToHangUpLocked(I)I
    .locals 1
    .param p1, "wakeLockSummary"    # I

    .line 1033
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mIsBrightWakeLock:Z

    .line 1034
    and-int/lit8 v0, p1, 0x26

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mScreenProjectionEnabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSynergyModeEnable:Z

    if-eqz v0, :cond_1

    .line 1037
    :cond_0
    and-int/lit8 p1, p1, -0x27

    goto :goto_0

    .line 1039
    :cond_1
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mDimEnable:Z

    if-eqz v0, :cond_2

    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_2

    .line 1041
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mIsBrightWakeLock:Z

    .line 1042
    and-int/lit8 p1, p1, -0x3

    .line 1043
    or-int/lit8 p1, p1, 0x4

    .line 1045
    :cond_2
    :goto_0
    return p1
.end method

.method public checkScreenWakeLockDisabledStateLocked()V
    .locals 6

    .line 521
    const/4 v0, 0x0

    .line 522
    .local v0, "changed":Z
    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mWakeLocks:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/power/PowerManagerService$WakeLock;

    .line 523
    .local v2, "wakeLock":Lcom/android/server/power/PowerManagerService$WakeLock;
    invoke-static {v2}, Lcom/android/server/power/PowerManagerService;->isScreenLock(Lcom/android/server/power/PowerManagerService$WakeLock;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget v3, v2, Lcom/android/server/power/PowerManagerService$WakeLock;->mOwnerUid:I

    .line 524
    invoke-static {v3}, Landroid/os/UserHandle;->isApp(I)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mScreenWakeLockWhitelists:Ljava/util/List;

    iget-object v4, v2, Lcom/android/server/power/PowerManagerService$WakeLock;->mPackageName:Ljava/lang/String;

    .line 525
    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 526
    iget-object v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mVisibleWindowUids:Ljava/util/List;

    iget v4, v2, Lcom/android/server/power/PowerManagerService$WakeLock;->mOwnerUid:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    .line 527
    .local v3, "state":Z
    iget-boolean v4, v2, Lcom/android/server/power/PowerManagerService$WakeLock;->mDisabled:Z

    if-ne v3, v4, :cond_2

    .line 528
    const/4 v0, 0x1

    .line 529
    xor-int/lit8 v4, v3, 0x1

    iput-boolean v4, v2, Lcom/android/server/power/PowerManagerService$WakeLock;->mDisabled:Z

    .line 530
    iget-boolean v4, v2, Lcom/android/server/power/PowerManagerService$WakeLock;->mDisabled:Z

    if-eqz v4, :cond_0

    .line 531
    iget-object v4, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPowerManagerService:Lcom/android/server/power/PowerManagerService;

    invoke-virtual {v4, v2}, Lcom/android/server/power/PowerManagerService;->notifyWakeLockReleasedLocked(Lcom/android/server/power/PowerManagerService$WakeLock;)V

    goto :goto_1

    .line 533
    :cond_0
    iget-object v4, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPowerManagerService:Lcom/android/server/power/PowerManagerService;

    invoke-virtual {v4, v2}, Lcom/android/server/power/PowerManagerService;->notifyWakeLockAcquiredLocked(Lcom/android/server/power/PowerManagerService$WakeLock;)V

    .line 535
    :goto_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "screen wakeLock:["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/android/server/power/PowerManagerService$WakeLock;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 536
    iget-boolean v5, v2, Lcom/android/server/power/PowerManagerService$WakeLock;->mDisabled:Z

    if-eqz v5, :cond_1

    const-string v5, "disabled"

    goto :goto_2

    :cond_1
    const-string v5, "enabled"

    :goto_2
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 535
    const-string v5, "PowerManagerServiceImpl"

    invoke-static {v5, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 539
    .end local v2    # "wakeLock":Lcom/android/server/power/PowerManagerService$WakeLock;
    .end local v3    # "state":Z
    :cond_2
    goto :goto_0

    .line 540
    :cond_3
    if-eqz v0, :cond_4

    .line 541
    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPowerManagerService:Lcom/android/server/power/PowerManagerService;

    invoke-virtual {v1}, Lcom/android/server/power/PowerManagerService;->setWakeLockDirtyLocked()V

    .line 543
    :cond_4
    return-void
.end method

.method public clearRequestDimmingParamsLocked()V
    .locals 4

    .line 1095
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSituatedDimmingDueToSynergy:Z

    const-wide/16 v1, -0x1

    const/4 v3, 0x0

    if-eqz v0, :cond_0

    .line 1096
    iput-boolean v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSituatedDimmingDueToSynergy:Z

    .line 1097
    iput-wide v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mUserActivityTimeoutOverrideFromMirrorManager:J

    .line 1098
    iput-wide v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLastRequestDimmingTime:J

    .line 1099
    iput-boolean v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPendingSetDirtyDueToRequestDimming:Z

    .line 1101
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSituatedDimmingDueToAttention:Z

    if-eqz v0, :cond_1

    .line 1102
    iput-boolean v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSituatedDimmingDueToAttention:Z

    .line 1103
    iput-wide v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLastRequestDimmingTime:J

    .line 1104
    iput-boolean v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPendingSetDirtyDueToRequestDimming:Z

    .line 1106
    :cond_1
    return-void
.end method

.method public clearVisibleWindowUids()V
    .locals 1

    .line 514
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mVisibleWindowUids:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 515
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mVisibleWindowUids:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 517
    :cond_0
    return-void
.end method

.method public dumpLocal(Ljava/io/PrintWriter;)V
    .locals 11
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 1930
    sget-boolean v0, Lcom/android/server/power/PowerDebugConfig;->DEBUG_PMS:Z

    sput-boolean v0, Lcom/android/server/power/PowerManagerServiceImpl;->DEBUG:Z

    .line 1931
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mScreenProjectionEnabled: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mScreenProjectionEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1932
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mHangUpEnabled: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mHangUpEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1933
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mSynergyModeEnable: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSynergyModeEnable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1934
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mIsKeyguardHasShownWhenExitHangup: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mIsKeyguardHasShownWhenExitHangup:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1935
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mSituatedDimmingDueToSynergy: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSituatedDimmingDueToSynergy:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1936
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mLastRequestDimmingTime: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLastRequestDimmingTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1937
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mPendingSetDirtyDueToRequestDimming: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPendingSetDirtyDueToRequestDimming:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1938
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mUserActivityTimeoutOverrideFromMirrorManager: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mUserActivityTimeoutOverrideFromMirrorManager:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1940
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mSituatedDimmingDueToAttention: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSituatedDimmingDueToAttention:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1941
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mIsPowerSaveModeEnabled: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mIsPowerSaveModeEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1942
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mTouchInteractiveUnavailable: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mTouchInteractiveUnavailable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1943
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mNotifierInjector:Lcom/android/server/power/NotifierInjector;

    invoke-virtual {v0, p1}, Lcom/android/server/power/NotifierInjector;->dump(Ljava/io/PrintWriter;)V

    .line 1945
    sget-boolean v0, Lmiui/os/DeviceFeature;->IS_SUBSCREEN_DEVICE:Z

    if-eqz v0, :cond_0

    .line 1946
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    .line 1947
    const-string v0, "Secondary display power state: "

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1948
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 1949
    .local v0, "now":J
    iget-wide v2, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLastSecondaryDisplayUserActivityTime:J

    sub-long v2, v0, v2

    .line 1950
    .local v2, "lastUserActivityDuration":J
    iget-wide v4, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLastSecondaryDisplayGoToSleepTime:J

    sub-long v4, v0, v4

    .line 1951
    .local v4, "lastGoToSleepDuration":J
    iget-wide v6, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLastSecondaryDisplayWakeUpTime:J

    sub-long v6, v0, v6

    .line 1953
    .local v6, "lastWakeUpDuration":J
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "    mLastSecondaryDisplayUserActivityTime: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 1954
    invoke-static {v2, v3}, Landroid/util/TimeUtils;->formatDuration(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " ago."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1953
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1955
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "    mLastSecondaryDisplayWakeUpTime: "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 1956
    invoke-static {v6, v7}, Landroid/util/TimeUtils;->formatDuration(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1955
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1957
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "    mLastSecondaryDisplayGoToSleepTime: "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 1958
    invoke-static {v4, v5}, Landroid/util/TimeUtils;->formatDuration(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1957
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1959
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "    mUserActivitySecondaryDisplaySummary: 0x"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mUserActivitySecondaryDisplaySummary:I

    .line 1960
    invoke-static {v9}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1959
    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1961
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "    mLastSecondaryDisplayWakeUpReason: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLastSecondaryDisplayWakeUpReason:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1962
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "    mLastSecondaryDisplayGoToSleepReason: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLastSecondaryDisplayGoToSleepReason:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1963
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "    mSecondaryDisplayScreenOffTimeout: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-wide v9, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSecondaryDisplayScreenOffTimeout:J

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " ms."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1964
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "    mAlwaysWakeUp: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-boolean v9, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mAlwaysWakeUp:Z

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1966
    .end local v0    # "now":J
    .end local v2    # "lastUserActivityDuration":J
    .end local v4    # "lastGoToSleepDuration":J
    .end local v6    # "lastWakeUpDuration":J
    :cond_0
    sget-boolean v0, Lcom/android/server/power/PowerManagerServiceImpl;->OPTIMIZE_WAKELOCK_ENABLED:Z

    if-eqz v0, :cond_1

    .line 1967
    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1968
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mIsFreeFormOrSplitWindowMode: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mIsFreeFormOrSplitWindowMode:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1970
    :cond_1
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mAttentionDetector:Lcom/android/server/power/MiuiAttentionDetector;

    if-eqz v0, :cond_2

    .line 1971
    invoke-virtual {v0, p1}, Lcom/android/server/power/MiuiAttentionDetector;->dump(Ljava/io/PrintWriter;)V

    .line 1973
    :cond_2
    return-void
.end method

.method public exitHangupWakefulnessLocked()Z
    .locals 2

    .line 963
    iget v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPreWakefulness:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mWakefulness:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method protected finishWakefulnessChangeIfNeededLocked()V
    .locals 3

    .line 968
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mScreenProjectionEnabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mHangUpEnabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSynergyModeEnable:Z

    if-eqz v0, :cond_1

    :cond_0
    iget v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mWakefulness:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    .line 970
    :cond_1
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mNotifierInjector:Lcom/android/server/power/NotifierInjector;

    const/4 v1, 0x0

    iget v2, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mWakefulness:I

    invoke-virtual {v0, v1, v2}, Lcom/android/server/power/NotifierInjector;->onWakefulnessInHangUp(ZI)V

    .line 972
    :cond_2
    return-void
.end method

.method public getDefaultDisplayLastUserActivity()J
    .locals 3

    .line 1754
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1755
    :try_start_0
    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPowerGroups:Landroid/util/SparseArray;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/power/PowerGroup;

    invoke-virtual {v1}, Lcom/android/server/power/PowerGroup;->getLastUserActivityTimeLocked()J

    move-result-wide v1

    monitor-exit v0

    return-wide v1

    .line 1756
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getDimDurationExtraTime(J)J
    .locals 4
    .param p1, "extraTimeMillis"    # J

    .line 1675
    iget v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mDriveMode:I

    const/4 v1, 0x1

    const-wide/16 v2, 0x0

    if-ne v0, v1, :cond_1

    cmp-long v0, p1, v2

    if-gtz v0, :cond_0

    goto :goto_0

    .line 1678
    :cond_0
    return-wide p1

    .line 1676
    :cond_1
    :goto_0
    return-wide v2
.end method

.method public getPartialWakeLockHoldByUid(I)I
    .locals 7
    .param p1, "uid"    # I

    .line 633
    const/4 v0, 0x0

    .line 634
    .local v0, "wakeLockNum":I
    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 635
    :try_start_0
    iget-object v2, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mWakeLocks:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/power/PowerManagerService$WakeLock;

    .line 636
    .local v3, "wakeLock":Lcom/android/server/power/PowerManagerService$WakeLock;
    iget-object v4, v3, Lcom/android/server/power/PowerManagerService$WakeLock;->mWorkSource:Landroid/os/WorkSource;

    .line 637
    .local v4, "ws":Landroid/os/WorkSource;
    if-nez v4, :cond_1

    iget v5, v3, Lcom/android/server/power/PowerManagerService$WakeLock;->mOwnerUid:I

    if-ne v5, p1, :cond_0

    :cond_1
    if-eqz v4, :cond_2

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/os/WorkSource;->get(I)I

    move-result v5

    if-eq v5, p1, :cond_2

    .line 638
    goto :goto_0

    .line 640
    :cond_2
    iget v5, v3, Lcom/android/server/power/PowerManagerService$WakeLock;->mFlags:I

    const v6, 0xffff

    and-int/2addr v5, v6

    .line 641
    .local v5, "wakeLockType":I
    const/4 v6, 0x1

    if-ne v5, v6, :cond_3

    .line 642
    add-int/lit8 v0, v0, 0x1

    .line 644
    .end local v3    # "wakeLock":Lcom/android/server/power/PowerManagerService$WakeLock;
    .end local v4    # "ws":Landroid/os/WorkSource;
    .end local v5    # "wakeLockType":I
    :cond_3
    goto :goto_0

    .line 645
    :cond_4
    monitor-exit v1

    .line 646
    return v0

    .line 645
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public getScreenWakeLockHoldByUid(I)I
    .locals 7
    .param p1, "uid"    # I

    .line 651
    const/4 v0, 0x0

    .line 652
    .local v0, "wakeLockNum":I
    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 653
    :try_start_0
    iget-object v2, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mWakeLocks:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/power/PowerManagerService$WakeLock;

    .line 654
    .local v3, "wakeLock":Lcom/android/server/power/PowerManagerService$WakeLock;
    iget-object v4, v3, Lcom/android/server/power/PowerManagerService$WakeLock;->mWorkSource:Landroid/os/WorkSource;

    .line 655
    .local v4, "ws":Landroid/os/WorkSource;
    if-nez v4, :cond_1

    iget v5, v3, Lcom/android/server/power/PowerManagerService$WakeLock;->mOwnerUid:I

    if-ne v5, p1, :cond_0

    :cond_1
    if-eqz v4, :cond_2

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/os/WorkSource;->get(I)I

    move-result v5

    if-eq v5, p1, :cond_2

    .line 656
    goto :goto_0

    .line 658
    :cond_2
    iget v5, v3, Lcom/android/server/power/PowerManagerService$WakeLock;->mFlags:I

    const v6, 0xffff

    and-int/2addr v5, v6

    .line 659
    .local v5, "wakeLockType":I
    sparse-switch v5, :sswitch_data_0

    goto :goto_1

    .line 663
    :sswitch_0
    add-int/lit8 v0, v0, 0x1

    .line 664
    nop

    .line 668
    .end local v3    # "wakeLock":Lcom/android/server/power/PowerManagerService$WakeLock;
    .end local v4    # "ws":Landroid/os/WorkSource;
    .end local v5    # "wakeLockType":I
    :goto_1
    goto :goto_0

    .line 669
    :cond_3
    monitor-exit v1

    .line 670
    return v0

    .line 669
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    nop

    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_0
        0xa -> :sswitch_0
        0x1a -> :sswitch_0
    .end sparse-switch
.end method

.method protected getSecondaryDisplayWakefulnessLocked()I
    .locals 2

    .line 1624
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPowerGroups:Landroid/util/SparseArray;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/power/PowerGroup;

    invoke-virtual {v0}, Lcom/android/server/power/PowerGroup;->getWakefulnessLocked()I

    move-result v0

    return v0
.end method

.method public hangUpNoUpdateLocked(Z)Z
    .locals 14
    .param p1, "hangUp"    # Z

    .line 900
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mBootCompleted:Z

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSystemReady:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x4

    if-eqz p1, :cond_0

    iget v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mWakefulness:I

    if-eq v1, v0, :cond_5

    :cond_0
    if-nez p1, :cond_1

    iget v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mWakefulness:I

    if-eq v1, v0, :cond_1

    goto :goto_2

    .line 905
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "hangUpNoUpdateLocked: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 906
    if-eqz p1, :cond_2

    const-string v2, "enter"

    goto :goto_0

    :cond_2
    const-string v2, "exit"

    :goto_0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " hang up mode..."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 905
    const-string v2, "PowerManagerServiceImpl"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 907
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    .line 908
    .local v1, "eventTime":J
    if-nez p1, :cond_3

    .line 909
    invoke-virtual {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updateUserActivityLocked()V

    .line 911
    :cond_3
    iget-object v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPowerManagerService:Lcom/android/server/power/PowerManagerService;

    const/4 v4, 0x0

    .line 913
    const/4 v13, 0x1

    if-eqz p1, :cond_4

    move v5, v0

    goto :goto_1

    :cond_4
    move v5, v13

    :goto_1
    const/16 v8, 0x3e8

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const-string v12, "hangup"

    .line 911
    move-wide v6, v1

    invoke-virtual/range {v3 .. v12}, Lcom/android/server/power/PowerManagerService;->setWakefulnessLocked(IIJIIILjava/lang/String;Ljava/lang/String;)V

    .line 916
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mNotifierInjector:Lcom/android/server/power/NotifierInjector;

    iget v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mWakefulness:I

    invoke-virtual {v0, p1, v3}, Lcom/android/server/power/NotifierInjector;->onWakefulnessInHangUp(ZI)V

    .line 917
    return v13

    .line 903
    .end local v1    # "eventTime":J
    :cond_5
    :goto_2
    const/4 v0, 0x0

    return v0
.end method

.method public init(Lcom/android/server/power/PowerManagerService;Ljava/util/ArrayList;Ljava/lang/Object;Landroid/content/Context;Landroid/os/Looper;Landroid/util/SparseArray;)V
    .locals 3
    .param p1, "powerManagerService"    # Lcom/android/server/power/PowerManagerService;
    .param p3, "lock"    # Ljava/lang/Object;
    .param p4, "context"    # Landroid/content/Context;
    .param p5, "looper"    # Landroid/os/Looper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/server/power/PowerManagerService;",
            "Ljava/util/ArrayList<",
            "Lcom/android/server/power/PowerManagerService$WakeLock;",
            ">;",
            "Ljava/lang/Object;",
            "Landroid/content/Context;",
            "Landroid/os/Looper;",
            "Landroid/util/SparseArray<",
            "Lcom/android/server/power/PowerGroup;",
            ">;)V"
        }
    .end annotation

    .line 241
    .local p2, "allWakeLocks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/power/PowerManagerService$WakeLock;>;"
    .local p6, "powerGroup":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/android/server/power/PowerGroup;>;"
    iput-object p4, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mContext:Landroid/content/Context;

    .line 242
    iput-object p1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPowerManagerService:Lcom/android/server/power/PowerManagerService;

    .line 243
    iput-object p2, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mWakeLocks:Ljava/util/ArrayList;

    .line 244
    iput-object p3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLock:Ljava/lang/Object;

    .line 246
    new-instance v0, Lcom/android/server/power/PowerManagerServiceImpl$PowerManagerStubHandler;

    invoke-direct {v0, p0, p5}, Lcom/android/server/power/PowerManagerServiceImpl$PowerManagerStubHandler;-><init>(Lcom/android/server/power/PowerManagerServiceImpl;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mHandler:Landroid/os/Handler;

    .line 247
    invoke-static {}, Lcom/miui/whetstone/PowerKeeperPolicy;->getInstance()Lcom/miui/whetstone/PowerKeeperPolicy;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPowerKeeperPolicy:Lcom/miui/whetstone/PowerKeeperPolicy;

    .line 248
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mResolver:Landroid/content/ContentResolver;

    .line 249
    new-instance v0, Lcom/android/server/power/PowerManagerServiceImpl$SettingsObserver;

    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/android/server/power/PowerManagerServiceImpl$SettingsObserver;-><init>(Lcom/android/server/power/PowerManagerServiceImpl;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSettingsObserver:Lcom/android/server/power/PowerManagerServiceImpl$SettingsObserver;

    .line 250
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSystemReady:Z

    .line 251
    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mContext:Landroid/content/Context;

    const-string v2, "power"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    iput-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPowerManager:Landroid/os/PowerManager;

    .line 252
    new-instance v1, Lcom/android/server/power/NotifierInjector;

    iget-object v2, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/android/server/power/NotifierInjector;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mNotifierInjector:Lcom/android/server/power/NotifierInjector;

    .line 253
    iput-object p6, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPowerGroups:Landroid/util/SparseArray;

    .line 254
    const-class v1, Lcom/android/server/policy/WindowManagerPolicy;

    invoke-static {v1}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/policy/WindowManagerPolicy;

    iput-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    .line 255
    const-class v1, Lcom/android/server/input/MiuiInputManagerInternal;

    invoke-static {v1}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/input/MiuiInputManagerInternal;

    iput-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mMiuiInputManagerInternal:Lcom/android/server/input/MiuiInputManagerInternal;

    .line 256
    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "sensor"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/SensorManager;

    iput-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSensorManager:Landroid/hardware/SensorManager;

    .line 257
    const v2, 0x1fa265c

    invoke-virtual {v1, v2, v0}, Landroid/hardware/SensorManager;->getDefaultSensor(IZ)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPickUpSensor:Landroid/hardware/Sensor;

    .line 258
    invoke-static {}, Landroid/app/ActivityTaskManager;->getService()Landroid/app/IActivityTaskManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mActivityTaskManager:Landroid/app/IActivityTaskManager;

    .line 259
    nop

    .line 260
    const-string v0, "greezer"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 259
    invoke-static {v0}, Lmiui/greeze/IGreezeManager$Stub;->asInterface(Landroid/os/IBinder;)Lmiui/greeze/IGreezeManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mGreezeManager:Lmiui/greeze/IGreezeManager;

    .line 261
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->systemReady()V

    .line 263
    invoke-static {}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->getInstance()Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mMiuiPowerStatisticTracker:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;

    .line 264
    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->init(Landroid/content/Context;)V

    .line 265
    sget-boolean v0, Lcom/android/server/power/MiuiAttentionDetector;->AON_SCREEN_OFF_SUPPORTED:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/android/server/power/MiuiAttentionDetector;->AON_SCREEN_ON_SUPPORTED:Z

    if-eqz v0, :cond_1

    .line 266
    :cond_0
    new-instance v0, Lcom/android/server/power/MiuiAttentionDetector;

    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPowerManager:Landroid/os/PowerManager;

    iget-object v2, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    invoke-direct {v0, p4, v1, p0, v2}, Lcom/android/server/power/MiuiAttentionDetector;-><init>(Landroid/content/Context;Landroid/os/PowerManager;Lcom/android/server/power/PowerManagerServiceImpl;Lcom/android/server/policy/WindowManagerPolicy;)V

    iput-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mAttentionDetector:Lcom/android/server/power/MiuiAttentionDetector;

    .line 268
    :cond_1
    return-void
.end method

.method public isAcquiredScreenBrightWakeLock(I)Z
    .locals 3
    .param p1, "groupId"    # I

    .line 1741
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1742
    :try_start_0
    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPowerGroups:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/power/PowerGroup;

    invoke-virtual {v1}, Lcom/android/server/power/PowerGroup;->getWakeLockSummaryLocked()I

    move-result v1

    .line 1743
    .local v1, "wakeLockSummary":I
    and-int/lit8 v2, v1, 0x6

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    move v1, v2

    .line 1745
    .local v1, "screenBrightWakeLock":Z
    monitor-exit v0

    .line 1746
    return v1

    .line 1745
    .end local v1    # "screenBrightWakeLock":Z
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public isAllowWakeUpList(I)Z
    .locals 4
    .param p1, "uid"    # I

    .line 675
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mGreezeManager:Lmiui/greeze/IGreezeManager;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 676
    return v1

    .line 680
    :cond_0
    :try_start_0
    invoke-interface {v0, p1}, Lmiui/greeze/IGreezeManager;->isAllowWakeUpList(I)Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 681
    :catch_0
    move-exception v0

    .line 682
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "PowerManagerServiceImpl"

    const-string v3, "isAllowWakeUpList error "

    invoke-static {v2, v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 684
    .end local v0    # "e":Ljava/lang/Exception;
    return v1
.end method

.method public isBrightnessBoostForSoftLightInProgress()Z
    .locals 1

    .line 1683
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mBrightnessBoostForSoftLightInProgress:Z

    return v0
.end method

.method public isInHangUpState()Z
    .locals 2

    .line 948
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mScreenProjectionEnabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSynergyModeEnable:Z

    if-eqz v0, :cond_1

    :cond_0
    iget v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mWakefulness:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isPendingStopBrightnessBoost()Z
    .locals 1

    .line 1688
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPendingStopBrightnessBoost:Z

    return v0
.end method

.method public isShutdownOrRebootPermitted(ZZLjava/lang/String;Z)Z
    .locals 2
    .param p1, "shutdown"    # Z
    .param p2, "confirm"    # Z
    .param p3, "reason"    # Ljava/lang/String;
    .param p4, "wait"    # Z

    .line 825
    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->isDeviceLock:Z

    if-eqz v0, :cond_1

    .line 826
    invoke-static {}, Lcom/android/server/UiThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    .line 827
    .local v0, "h":Landroid/os/Handler;
    if-eqz v0, :cond_0

    .line 828
    new-instance v1, Lcom/android/server/power/PowerManagerServiceImpl$4;

    invoke-direct {v1, p0}, Lcom/android/server/power/PowerManagerServiceImpl$4;-><init>(Lcom/android/server/power/PowerManagerServiceImpl;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 837
    :cond_0
    const/4 v1, 0x0

    return v1

    .line 839
    .end local v0    # "h":Landroid/os/Handler;
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public isSituatedDimmingDueToSynergy()Z
    .locals 1

    .line 954
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mScreenProjectionEnabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSynergyModeEnable:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSituatedDimmingDueToSynergy:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSubScreenSuperPowerSaveModeOpen()Z
    .locals 2

    .line 1597
    sget-boolean v0, Lmiui/os/DeviceFeature;->IS_SUBSCREEN_DEVICE:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSubScreenSuperPowerSaveMode:I

    if-eqz v0, :cond_0

    .line 1599
    const-string v0, "PowerManagerServiceImpl"

    const-string/jumbo v1, "wilderness subscreen_super_power_save_mode open"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1600
    const/4 v0, 0x1

    return v0

    .line 1602
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public isWakelockDisabledByPolicy(Lcom/android/server/power/PowerManagerService$WakeLock;)Z
    .locals 7
    .param p1, "wakeLock"    # Lcom/android/server/power/PowerManagerService$WakeLock;

    .line 695
    const/4 v0, 0x0

    .line 696
    .local v0, "disabled":Z
    invoke-direct {p0, p1}, Lcom/android/server/power/PowerManagerServiceImpl;->getRealOwners(Lcom/android/server/power/PowerManagerService$WakeLock;)[I

    move-result-object v1

    .line 697
    .local v1, "realOwners":[I
    iget-object v2, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPowerKeeperPolicy:Lcom/miui/whetstone/PowerKeeperPolicy;

    const/4 v3, 0x0

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mGreezeManager:Lmiui/greeze/IGreezeManager;

    if-nez v2, :cond_0

    goto :goto_3

    .line 701
    :cond_0
    :try_start_0
    array-length v2, v1

    :goto_0
    if-ge v3, v2, :cond_3

    aget v4, v1, v3

    .line 702
    .local v4, "realOwner":I
    iget-object v5, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPowerKeeperPolicy:Lcom/miui/whetstone/PowerKeeperPolicy;

    iget-object v6, p1, Lcom/android/server/power/PowerManagerService$WakeLock;->mTag:Ljava/lang/String;

    invoke-virtual {v5, v6, v4}, Lcom/miui/whetstone/PowerKeeperPolicy;->isWakelockDisabledByPolicy(Ljava/lang/String;I)Z

    move-result v5

    const/4 v6, 0x1

    if-eqz v5, :cond_1

    .line 703
    const/4 v0, 0x1

    .line 704
    const-string v2, "PowerKeeper"

    invoke-virtual {p0, p1, v6, v2}, Lcom/android/server/power/PowerManagerServiceImpl;->printPartialWakeLockDisabledIfNeeded(Lcom/android/server/power/PowerManagerService$WakeLock;ZLjava/lang/String;)V

    .line 705
    goto :goto_1

    .line 706
    :cond_1
    invoke-static {v4}, Landroid/os/UserHandle;->isApp(I)Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mGreezeManager:Lmiui/greeze/IGreezeManager;

    invoke-interface {v5, v4}, Lmiui/greeze/IGreezeManager;->isUidFrozen(I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 707
    const/4 v0, 0x1

    .line 708
    const-string v2, "UidFrozen"

    invoke-virtual {p0, p1, v6, v2}, Lcom/android/server/power/PowerManagerServiceImpl;->printPartialWakeLockDisabledIfNeeded(Lcom/android/server/power/PowerManagerService$WakeLock;ZLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 709
    goto :goto_1

    .line 701
    .end local v4    # "realOwner":I
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 714
    :cond_3
    :goto_1
    goto :goto_2

    .line 712
    :catch_0
    move-exception v2

    .line 713
    .local v2, "e":Ljava/lang/Exception;
    const-string v3, "PowerManagerServiceImpl"

    const-string v4, "isWakelockDisabledByPolicy error "

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 715
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_2
    return v0

    .line 698
    :cond_4
    :goto_3
    return v3
.end method

.method public lockNowIfNeededLocked(I)V
    .locals 2
    .param p1, "reason"    # I

    .line 1724
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSynergyModeEnable:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    iget v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mWakefulness:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 1727
    invoke-static {}, Lcom/android/server/wm/WindowManagerServiceStub;->get()Lcom/android/server/wm/WindowManagerServiceStub;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/server/wm/WindowManagerServiceStub;->isKeyGuardShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1728
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/android/server/policy/WindowManagerPolicy;->lockNow(Landroid/os/Bundle;)V

    .line 1729
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mIsKeyguardHasShownWhenExitHangup:Z

    .line 1731
    :cond_0
    return-void
.end method

.method public notifyAonScreenOnOffEvent(ZZJ)V
    .locals 5
    .param p1, "isScreenOn"    # Z
    .param p2, "hasFace"    # Z
    .param p3, "startTime"    # J

    .line 1846
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mMiuiPowerStatisticTracker:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;

    if-nez v0, :cond_0

    .line 1847
    return-void

    .line 1849
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 1850
    .local v0, "now":J
    sub-long v2, v0, p3

    .line 1851
    .local v2, "latencyMs":J
    iget-object v4, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mMiuiPowerStatisticTracker:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;

    invoke-virtual {v4, p1, p2, v2, v3}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->notifyAonScreenOnOffEvent(ZZJ)V

    .line 1852
    return-void
.end method

.method public notifyDimStateChanged(II)V
    .locals 4
    .param p1, "pendingRequestPolicy"    # I
    .param p2, "requestPolicy"    # I

    .line 1900
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mMiuiPowerStatisticTracker:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;

    if-nez v0, :cond_0

    .line 1901
    return-void

    .line 1904
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 1905
    .local v0, "now":J
    const/4 v2, 0x2

    if-eq v2, p2, :cond_1

    if-ne v2, p1, :cond_3

    .line 1907
    :cond_1
    if-ne v2, p2, :cond_2

    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    .line 1908
    .local v2, "isEnterDim":Z
    :goto_0
    iget-object v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mMiuiPowerStatisticTracker:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;

    invoke-virtual {v3, v0, v1, v2}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->notifyDimStateChanged(JZ)V

    .line 1910
    .end local v2    # "isEnterDim":Z
    :cond_3
    return-void
.end method

.method public notifyDisplayPortConnectStateChanged(JZLjava/lang/String;ILjava/lang/String;)V
    .locals 7
    .param p1, "physicalDisplayId"    # J
    .param p3, "isConnected"    # Z
    .param p4, "productName"    # Ljava/lang/String;
    .param p5, "frameRate"    # I
    .param p6, "resolution"    # Ljava/lang/String;

    .line 1858
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mMiuiPowerStatisticTracker:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;

    if-nez v0, :cond_0

    .line 1859
    return-void

    .line 1862
    :cond_0
    move-wide v1, p1

    move v3, p3

    move-object v4, p4

    move v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->notifyDisplayPortConnectStateChanged(JZLjava/lang/String;ILjava/lang/String;)V

    .line 1864
    return-void
.end method

.method public notifyDisplayStateChangeEndLocked(Z)V
    .locals 4
    .param p1, "isFirstDisplay"    # Z

    .line 1805
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mMiuiPowerStatisticTracker:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;

    if-eqz v0, :cond_1

    if-nez p1, :cond_0

    goto :goto_0

    .line 1809
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mDisplayStateChangeStateTime:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    .line 1810
    .local v0, "latencyMs":I
    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mMiuiPowerStatisticTracker:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;

    invoke-virtual {v1, v0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->notifyDisplayStateChangedLatencyLocked(I)V

    .line 1811
    return-void

    .line 1806
    .end local v0    # "latencyMs":I
    :cond_1
    :goto_0
    return-void
.end method

.method public notifyDisplayStateChangeStartLocked(Z)V
    .locals 2
    .param p1, "isFirstDisplay"    # Z

    .line 1797
    if-nez p1, :cond_0

    .line 1798
    return-void

    .line 1800
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mDisplayStateChangeStateTime:J

    .line 1801
    return-void
.end method

.method public notifyGestureEvent(Ljava/lang/String;ZI)V
    .locals 1
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "success"    # Z
    .param p3, "label"    # I

    .line 1835
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mMiuiPowerStatisticTracker:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;

    if-nez v0, :cond_0

    .line 1836
    return-void

    .line 1839
    :cond_0
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->notifyGestureEvent(Ljava/lang/String;ZI)V

    .line 1840
    return-void
.end method

.method public notifyScreenOnUnBlocker(IJI)V
    .locals 2
    .param p1, "displayId"    # I
    .param p2, "screenOnBlockStartRealTime"    # J
    .param p4, "delayMiles"    # I

    .line 1783
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mMiuiPowerStatisticTracker:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    goto :goto_0

    .line 1787
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sub-long/2addr v0, p2

    long-to-int v0, v0

    .line 1788
    .local v0, "delay":I
    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mMiuiPowerStatisticTracker:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;

    invoke-virtual {v1, v0, p4}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->notifyScreenOnUnBlocker(II)V

    .line 1789
    return-void

    .line 1784
    .end local v0    # "delay":I
    :cond_1
    :goto_0
    return-void
.end method

.method public notifyStayOnChanged(Z)V
    .locals 1
    .param p1, "stayOn"    # Z

    .line 1876
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mAttentionDetector:Lcom/android/server/power/MiuiAttentionDetector;

    if-eqz v0, :cond_0

    .line 1877
    invoke-virtual {v0, p1}, Lcom/android/server/power/MiuiAttentionDetector;->notifyStayOnChanged(Z)V

    .line 1879
    :cond_0
    return-void
.end method

.method public notifyTofPowerState(Z)V
    .locals 1
    .param p1, "wakeup"    # Z

    .line 1826
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mMiuiPowerStatisticTracker:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;

    if-nez v0, :cond_0

    .line 1827
    return-void

    .line 1830
    :cond_0
    invoke-virtual {v0, p1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->notifyTofPowerState(Z)V

    .line 1831
    return-void
.end method

.method public notifyUserActivityTimeChanged()V
    .locals 1

    .line 1869
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mAttentionDetector:Lcom/android/server/power/MiuiAttentionDetector;

    if-eqz v0, :cond_0

    .line 1870
    invoke-virtual {v0}, Lcom/android/server/power/MiuiAttentionDetector;->onUserActivityChanged()V

    .line 1872
    :cond_0
    return-void
.end method

.method public notifyUserAttentionChanged(JI)V
    .locals 4
    .param p1, "startTimeStamp"    # J
    .param p3, "result"    # I

    .line 1815
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mMiuiPowerStatisticTracker:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;

    if-nez v0, :cond_0

    .line 1816
    return-void

    .line 1819
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 1820
    .local v0, "now":J
    sub-long v2, v0, p1

    long-to-int v2, v2

    .line 1821
    .local v2, "latencyMs":I
    iget-object v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mMiuiPowerStatisticTracker:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;

    invoke-virtual {v3, v2, p3}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->notifyUserAttentionChanged(II)V

    .line 1822
    return-void
.end method

.method public notifyWakefulnessChangedLocked(IIJILjava/lang/String;)V
    .locals 1
    .param p1, "groupId"    # I
    .param p2, "wakefulness"    # I
    .param p3, "eventTime"    # J
    .param p5, "reason"    # I
    .param p6, "details"    # Ljava/lang/String;

    .line 1762
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mMiuiPowerStatisticTracker:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    goto :goto_0

    .line 1766
    :cond_0
    invoke-virtual {v0, p2, p3, p4, p5}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->notifyWakefulnessChangedLocked(IJI)V

    .line 1767
    invoke-static {}, Lcom/android/server/wm/WindowManagerServiceStub;->get()Lcom/android/server/wm/WindowManagerServiceStub;

    move-result-object v0

    invoke-interface {v0, p2, p6}, Lcom/android/server/wm/WindowManagerServiceStub;->trackScreenData(ILjava/lang/String;)V

    .line 1768
    return-void

    .line 1763
    :cond_1
    :goto_0
    return-void
.end method

.method public notifyWakefulnessCompletedLocked(I)V
    .locals 3
    .param p1, "groupId"    # I

    .line 1772
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mMiuiPowerStatisticTracker:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    goto :goto_0

    .line 1776
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 1777
    .local v0, "wakefulnessCompletedTime":J
    iget-object v2, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mMiuiPowerStatisticTracker:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;

    invoke-virtual {v2, v0, v1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->notifyWakefulnessCompletedLocked(J)V

    .line 1778
    return-void

    .line 1773
    .end local v0    # "wakefulnessCompletedTime":J
    :cond_1
    :goto_0
    return-void
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 7
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I

    .line 1410
    const/4 v0, 0x1

    const-string v1, "android.os.IPowerManager"

    packed-switch p1, :pswitch_data_0

    .line 1459
    const/4 v0, 0x0

    return v0

    .line 1412
    :pswitch_0
    :try_start_0
    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1413
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v1

    .line 1414
    .local v1, "duration":J
    invoke-virtual {p0, v1, v2}, Lcom/android/server/power/PowerManagerServiceImpl;->requestDimmingRightNow(J)V

    .line 1415
    return v0

    .line 1417
    .end local v1    # "duration":J
    :pswitch_1
    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1418
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    .line 1419
    .local v1, "token":Landroid/os/IBinder;
    invoke-virtual {p2}, Landroid/os/Parcel;->readBoolean()Z

    move-result v2

    .line 1420
    .local v2, "hangup":Z
    invoke-direct {p0, v1, v2}, Lcom/android/server/power/PowerManagerServiceImpl;->requestHangUpWhenScreenProject(Landroid/os/IBinder;Z)V

    .line 1421
    return v0

    .line 1423
    .end local v1    # "token":Landroid/os/IBinder;
    .end local v2    # "hangup":Z
    :pswitch_2
    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1424
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v1

    .line 1425
    .local v1, "wakeUpTime":J
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 1426
    .local v3, "wakeUpReason":Ljava/lang/String;
    invoke-direct {p0, v1, v2, v3}, Lcom/android/server/power/PowerManagerServiceImpl;->wakeUpSecondaryDisplay(JLjava/lang/String;)V

    .line 1427
    return v0

    .line 1429
    .end local v1    # "wakeUpTime":J
    .end local v3    # "wakeUpReason":Ljava/lang/String;
    :pswitch_3
    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1430
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v1

    .line 1431
    .local v1, "goToSleepTime":J
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 1432
    .local v3, "goToSleepReason":Ljava/lang/String;
    invoke-direct {p0, v1, v2, v3}, Lcom/android/server/power/PowerManagerServiceImpl;->goToSleepSecondaryDisplay(JLjava/lang/String;)V

    .line 1433
    return v0

    .line 1435
    .end local v1    # "goToSleepTime":J
    .end local v3    # "goToSleepReason":Ljava/lang/String;
    :pswitch_4
    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1436
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    .line 1437
    .local v2, "token1":Landroid/os/IBinder;
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v3

    .line 1438
    .local v3, "wakeUpTime1":J
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    .line 1439
    .local v5, "flag":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 1440
    .local v6, "wakeUpReason1":Ljava/lang/String;
    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/android/server/power/PowerManagerServiceImpl;->wakeUpSecondaryDisplay(Landroid/os/IBinder;JILjava/lang/String;)V

    .line 1441
    return v0

    .line 1443
    .end local v2    # "token1":Landroid/os/IBinder;
    .end local v3    # "wakeUpTime1":J
    .end local v5    # "flag":I
    .end local v6    # "wakeUpReason1":Ljava/lang/String;
    :pswitch_5
    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1444
    invoke-virtual {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->getSecondaryDisplayWakefulnessLocked()I

    move-result v1

    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1445
    return v0

    .line 1447
    :pswitch_6
    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1448
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    .line 1449
    .local v1, "token2":Landroid/os/IBinder;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 1450
    .local v2, "flag2":I
    invoke-direct {p0, v1, v2}, Lcom/android/server/power/PowerManagerServiceImpl;->clearWakeUpFlags(Landroid/os/IBinder;I)V

    .line 1451
    return v0

    .line 1453
    .end local v1    # "token2":Landroid/os/IBinder;
    .end local v2    # "flag2":I
    :pswitch_7
    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1454
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    .line 1455
    .local v1, "token3":Landroid/os/IBinder;
    invoke-virtual {p2}, Landroid/os/Parcel;->readBoolean()Z

    move-result v2

    .line 1456
    .local v2, "enabled":Z
    invoke-direct {p0, v1, v2}, Lcom/android/server/power/PowerManagerServiceImpl;->boostScreenBrightness(Landroid/os/IBinder;Z)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1457
    return v0

    .line 1461
    .end local v1    # "token3":Landroid/os/IBinder;
    .end local v2    # "enabled":Z
    :catch_0
    move-exception v0

    .line 1462
    .local v0, "e":Ljava/lang/RuntimeException;
    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0xfffff7
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onUserSwitching()V
    .locals 0

    .line 1199
    invoke-direct {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updateAonScreenOffConfig()V

    .line 1200
    return-void
.end method

.method public printPartialWakeLockDisabledIfNeeded(Lcom/android/server/power/PowerManagerService$WakeLock;ZLjava/lang/String;)V
    .locals 2
    .param p1, "wakeLock"    # Lcom/android/server/power/PowerManagerService$WakeLock;
    .param p2, "disabled"    # Z
    .param p3, "reason"    # Ljava/lang/String;

    .line 596
    iget-boolean v0, p1, Lcom/android/server/power/PowerManagerService$WakeLock;->mDisabled:Z

    if-eq v0, p2, :cond_0

    .line 597
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Partial wakeLock:["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "], disabled: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", procState: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/android/server/power/PowerManagerService$WakeLock;->mUidState:Lcom/android/server/power/PowerManagerService$UidState;

    iget v1, v1, Lcom/android/server/power/PowerManagerService$UidState;->mProcState:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", reason: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PowerManagerServiceImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 602
    :cond_0
    return-void
.end method

.method protected printWakeLockDetails(Lcom/android/server/power/PowerManagerService$WakeLock;Z)V
    .locals 3
    .param p1, "wakeLock"    # Lcom/android/server/power/PowerManagerService$WakeLock;
    .param p2, "isAcquired"    # Z

    .line 551
    iget v0, p1, Lcom/android/server/power/PowerManagerService$WakeLock;->mFlags:I

    const v1, 0xffff

    and-int/2addr v0, v1

    sparse-switch v0, :sswitch_data_0

    goto :goto_1

    .line 556
    :sswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 557
    .local v0, "sb":Ljava/lang/StringBuilder;
    if-eqz p2, :cond_0

    const-string v1, "Acquire wakelock: "

    goto :goto_0

    :cond_0
    const-string v1, "Release wakelock: "

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 558
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 559
    const-string v1, "PowerManagerServiceImpl"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 560
    nop

    .line 564
    .end local v0    # "sb":Ljava/lang/StringBuilder;
    :goto_1
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_0
        0xa -> :sswitch_0
        0x1a -> :sswitch_0
        0x20 -> :sswitch_0
    .end sparse-switch
.end method

.method public reassignRebootReason(II)Ljava/lang/String;
    .locals 8
    .param p1, "uid"    # I
    .param p2, "pid"    # I

    .line 1699
    const/4 v0, 0x0

    .line 1700
    .local v0, "reason":Ljava/lang/String;
    const-string v1, "Unexpected reboot or shutdown reason is null, the reason will be reassign by uid and processname"

    const-string v2, "PowerManagerServiceImpl"

    invoke-static {v2, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1701
    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mContext:Landroid/content/Context;

    const-string v3, "activity"

    invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    .line 1702
    .local v1, "am":Landroid/app/ActivityManager;
    const/4 v3, 0x0

    .line 1704
    .local v3, "processName":Ljava/lang/String;
    :try_start_0
    invoke-virtual {v1}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v4

    .line 1705
    .local v4, "runningProcesses":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 1706
    .local v6, "processInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    iget v7, v6, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-ne v7, p2, :cond_0

    .line 1707
    iget-object v2, v6, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1708
    .end local v3    # "processName":Ljava/lang/String;
    .local v2, "processName":Ljava/lang/String;
    move-object v3, v2

    goto :goto_1

    .line 1710
    .end local v2    # "processName":Ljava/lang/String;
    .end local v6    # "processInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    .restart local v3    # "processName":Ljava/lang/String;
    :cond_0
    goto :goto_0

    .line 1713
    .end local v4    # "runningProcesses":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    :cond_1
    :goto_1
    goto :goto_2

    .line 1711
    :catch_0
    move-exception v4

    .line 1712
    .local v4, "e":Ljava/lang/Exception;
    const-string v5, "Failed to get running app processes from ActivityManager"

    invoke-static {v2, v5, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1714
    .end local v4    # "e":Ljava/lang/Exception;
    :goto_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ","

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez v3, :cond_2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto :goto_3

    :cond_2
    move-object v4, v3

    :goto_3
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1715
    return-object v0
.end method

.method public recalculateGlobalWakefulnessSkipPowerGroup(Lcom/android/server/power/PowerGroup;)Z
    .locals 1
    .param p1, "powerGroup"    # Lcom/android/server/power/PowerGroup;

    .line 1887
    invoke-virtual {p1}, Lcom/android/server/power/PowerGroup;->getGroupId()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public recordShutDownTime()V
    .locals 11

    .line 847
    new-instance v0, Ljava/io/File;

    const-string v1, "/cache/recovery/last_utime"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 848
    .local v0, "last_utime":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    const-string v2, "PowerManagerServiceImpl"

    if-nez v1, :cond_0

    .line 849
    const-string v1, "last_utime doesn\'t exist"

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 850
    return-void

    .line 854
    :cond_0
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/FileReader;

    invoke-direct {v3, v0}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 855
    .local v1, "reader":Ljava/io/BufferedReader;
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    move-object v4, v3

    .local v4, "str":Ljava/lang/String;
    if-eqz v3, :cond_3

    .line 856
    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    .line 857
    .local v5, "usrConfirmTime":J
    const-wide/16 v7, 0x0

    cmp-long v3, v5, v7

    if-gtz v3, :cond_1

    .line 858
    const-string v3, "last_utime has invalid content"

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 859
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    .line 860
    return-void

    .line 867
    :cond_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    .line 869
    new-instance v3, Ljava/io/File;

    const-string v7, "/cache/recovery/last_shutdowntime"

    invoke-direct {v3, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 870
    .local v3, "last_ShutdownTime":Ljava/io/File;
    new-instance v7, Ljava/io/BufferedWriter;

    new-instance v8, Ljava/io/FileWriter;

    invoke-direct {v8, v3}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V

    invoke-direct {v7, v8}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    .line 871
    .local v7, "writer":Ljava/io/BufferedWriter;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    .line 872
    .local v8, "buf":Ljava/lang/String;
    invoke-virtual {v7, v8}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 873
    invoke-virtual {v7}, Ljava/io/BufferedWriter;->flush()V

    .line 874
    invoke-virtual {v7}, Ljava/io/BufferedWriter;->close()V

    .line 875
    const/4 v9, 0x1

    const/4 v10, 0x0

    invoke-virtual {v3, v9, v10}, Ljava/io/File;->setReadable(ZZ)Z

    move-result v9

    if-nez v9, :cond_2

    .line 876
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 877
    const-string/jumbo v9, "set last_shutdowntime readable failed"

    invoke-static {v2, v9}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 878
    return-void

    .line 882
    .end local v1    # "reader":Ljava/io/BufferedReader;
    .end local v3    # "last_ShutdownTime":Ljava/io/File;
    .end local v7    # "writer":Ljava/io/BufferedWriter;
    :cond_2
    goto :goto_0

    .line 863
    .end local v5    # "usrConfirmTime":J
    .end local v8    # "buf":Ljava/lang/String;
    .restart local v1    # "reader":Ljava/io/BufferedReader;
    :cond_3
    const-string v3, "last_utime is blank"

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 864
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 865
    return-void

    .line 880
    .end local v1    # "reader":Ljava/io/BufferedReader;
    .end local v4    # "str":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 881
    .local v1, "ex":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 883
    .end local v1    # "ex":Ljava/io/IOException;
    :goto_0
    return-void
.end method

.method protected registerDeathCallbackLocked(Landroid/os/IBinder;)V
    .locals 2
    .param p1, "token"    # Landroid/os/IBinder;

    .line 1248
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mClientDeathCallbacks:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1249
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Client token "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " has already registered."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PowerManagerServiceImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1250
    return-void

    .line 1252
    :cond_0
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mClientDeathCallbacks:Ljava/util/HashMap;

    new-instance v1, Lcom/android/server/power/PowerManagerServiceImpl$ClientDeathCallback;

    invoke-direct {v1, p0, p1}, Lcom/android/server/power/PowerManagerServiceImpl$ClientDeathCallback;-><init>(Lcom/android/server/power/PowerManagerServiceImpl;Landroid/os/IBinder;)V

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1253
    return-void
.end method

.method protected registerDeathCallbackLocked(Landroid/os/IBinder;I)V
    .locals 2
    .param p1, "token"    # Landroid/os/IBinder;
    .param p2, "flag"    # I

    .line 1240
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mClientDeathCallbacks:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1241
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Client token "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " has already registered."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PowerManagerServiceImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1242
    return-void

    .line 1244
    :cond_0
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mClientDeathCallbacks:Ljava/util/HashMap;

    new-instance v1, Lcom/android/server/power/PowerManagerServiceImpl$ClientDeathCallback;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/server/power/PowerManagerServiceImpl$ClientDeathCallback;-><init>(Lcom/android/server/power/PowerManagerServiceImpl;Landroid/os/IBinder;I)V

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1245
    return-void
.end method

.method protected requestDimmingRightNow(J)V
    .locals 4
    .param p1, "timeMillis"    # J

    .line 1288
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 1290
    .local v0, "ident":J
    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-lez v2, :cond_0

    .line 1291
    :try_start_0
    invoke-virtual {p0, p1, p2}, Lcom/android/server/power/PowerManagerServiceImpl;->requestDimmingRightNowInternal(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1294
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 1295
    throw v2

    .line 1294
    :cond_0
    :goto_0
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 1295
    nop

    .line 1296
    return-void
.end method

.method public requestDimmingRightNowDueToAttention()V
    .locals 6

    .line 1060
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1061
    :try_start_0
    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPowerGroups:Landroid/util/SparseArray;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/power/PowerGroup;

    .line 1062
    invoke-virtual {v1}, Lcom/android/server/power/PowerGroup;->getUserActivitySummaryLocked()I

    move-result v1

    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x1

    if-eqz v1, :cond_0

    move v2, v3

    :cond_0
    move v1, v2

    .line 1064
    .local v1, "isDimming":Z
    iget-boolean v2, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSituatedDimmingDueToAttention:Z

    if-nez v2, :cond_1

    if-nez v1, :cond_1

    .line 1066
    invoke-virtual {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updateUserActivityLocked()V

    .line 1068
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLastRequestDimmingTime:J

    .line 1069
    iput-boolean v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSituatedDimmingDueToAttention:Z

    .line 1070
    iput-boolean v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPendingSetDirtyDueToRequestDimming:Z

    .line 1071
    iget-object v2, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPowerManagerService:Lcom/android/server/power/PowerManagerService;

    invoke-virtual {v2}, Lcom/android/server/power/PowerManagerService;->updatePowerStateLocked()V

    .line 1073
    .end local v1    # "isDimming":Z
    :cond_1
    monitor-exit v0

    .line 1074
    return-void

    .line 1073
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method protected requestDimmingRightNowInternal(J)V
    .locals 4
    .param p1, "timeMillis"    # J

    .line 1076
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1077
    :try_start_0
    iget-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSituatedDimmingDueToSynergy:Z

    if-nez v1, :cond_1

    .line 1078
    sget-boolean v1, Lcom/android/server/power/PowerManagerServiceImpl;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 1079
    const-string v1, "PowerManagerServiceImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "requestDimmingRightNowInternal: timeout: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1082
    :cond_0
    invoke-virtual {p0}, Lcom/android/server/power/PowerManagerServiceImpl;->updateUserActivityLocked()V

    .line 1084
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLastRequestDimmingTime:J

    .line 1085
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSituatedDimmingDueToSynergy:Z

    .line 1086
    iput-boolean v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPendingSetDirtyDueToRequestDimming:Z

    .line 1087
    iput-wide p1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mUserActivityTimeoutOverrideFromMirrorManager:J

    .line 1088
    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPowerManagerService:Lcom/android/server/power/PowerManagerService;

    invoke-virtual {v1}, Lcom/android/server/power/PowerManagerService;->updatePowerStateLocked()V

    .line 1090
    :cond_1
    monitor-exit v0

    .line 1091
    return-void

    .line 1090
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public resetBoostBrightnessIfNeededLocked()V
    .locals 1

    .line 1692
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPendingStopBrightnessBoost:Z

    if-eqz v0, :cond_0

    .line 1693
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPendingStopBrightnessBoost:Z

    .line 1695
    :cond_0
    return-void
.end method

.method protected sendBroadcastRestoreBrightnessIfNeededLocked()V
    .locals 1

    .line 1213
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mDimEnable:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mIsBrightWakeLock:Z

    if-eqz v0, :cond_0

    .line 1214
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mNotifierInjector:Lcom/android/server/power/NotifierInjector;

    invoke-virtual {v0}, Lcom/android/server/power/NotifierInjector;->sendBroadcastRestoreBrightness()V

    .line 1216
    :cond_0
    return-void
.end method

.method public setBootPhase(I)V
    .locals 2
    .param p1, "phase"    # I

    .line 922
    const/16 v0, 0x3e8

    if-ne p1, v0, :cond_0

    .line 923
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mBootCompleted:Z

    .line 926
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/power/PowerManagerServiceImpl$$ExternalSyntheticLambda2;

    invoke-direct {v1}, Lcom/android/server/power/PowerManagerServiceImpl$$ExternalSyntheticLambda2;-><init>()V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 928
    :cond_0
    return-void
.end method

.method public setPickUpSensorEnable(Z)V
    .locals 4
    .param p1, "isDimming"    # Z

    .line 354
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSupportAdaptiveSleep:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/android/server/power/MiuiAttentionDetector;->AON_SCREEN_OFF_SUPPORTED:Z

    if-eqz v0, :cond_2

    :cond_0
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mAdaptiveSleepEnabled:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mAonScreenOffEnabled:Z

    if-eqz v0, :cond_2

    :cond_1
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPickUpGestureWakeUpEnabled:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    .line 357
    invoke-interface {v0}, Lcom/android/server/policy/WindowManagerPolicy;->isKeyguardShowingAndNotOccluded()Z

    move-result v0

    if-nez v0, :cond_2

    if-eqz p1, :cond_2

    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPickUpSensorEnabled:Z

    if-nez v0, :cond_2

    .line 360
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPickUpSensorEnabled:Z

    .line 361
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPickUpSensorListener:Landroid/hardware/SensorEventListener;

    iget-object v2, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPickUpSensor:Landroid/hardware/Sensor;

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    goto :goto_0

    .line 363
    :cond_2
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPickUpSensorEnabled:Z

    if-eqz v0, :cond_3

    if-nez p1, :cond_3

    .line 364
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPickUpSensorEnabled:Z

    .line 365
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPickUpSensorListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 367
    :cond_3
    :goto_0
    return-void
.end method

.method protected setProximityWakeLockDisabledStateLocked(Lcom/android/server/power/PowerManagerService$WakeLock;Z)V
    .locals 6
    .param p1, "wakeLock"    # Lcom/android/server/power/PowerManagerService$WakeLock;
    .param p2, "notAcquired"    # Z

    .line 464
    sget-boolean v0, Lcom/android/server/power/PowerManagerServiceImpl;->OPTIMIZE_WAKELOCK_ENABLED:Z

    if-eqz v0, :cond_9

    iget v0, p1, Lcom/android/server/power/PowerManagerService$WakeLock;->mOwnerUid:I

    invoke-static {v0}, Landroid/os/UserHandle;->isApp(I)Z

    move-result v0

    if-eqz v0, :cond_9

    iget v0, p1, Lcom/android/server/power/PowerManagerService$WakeLock;->mFlags:I

    const v1, 0xffff

    and-int/2addr v0, v1

    const/16 v1, 0x20

    if-ne v0, v1, :cond_9

    .line 467
    iget-object v0, p1, Lcom/android/server/power/PowerManagerService$WakeLock;->mPackageName:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mForegroundAppPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 468
    .local v0, "isInForeground":Z
    sget-boolean v1, Lcom/android/server/power/PowerManagerServiceImpl;->IS_FOLDABLE_DEVICE:Z

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mFolded:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    move v1, v3

    goto :goto_0

    :cond_0
    move v1, v2

    .line 469
    .local v1, "isUnfolded":Z
    :goto_0
    if-eqz v1, :cond_1

    iget-boolean v4, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mNoSupprotInnerScreenProximitySensor:Z

    if-eqz v4, :cond_1

    move v4, v3

    goto :goto_1

    :cond_1
    move v4, v2

    .line 470
    .local v4, "isDisabledProximitySensor":Z
    :goto_1
    if-eqz p2, :cond_6

    .line 471
    if-eqz v0, :cond_2

    iget-boolean v5, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mIsFreeFormOrSplitWindowMode:Z

    if-nez v5, :cond_2

    if-eqz v4, :cond_3

    :cond_2
    move v2, v3

    .line 473
    .local v2, "shouldDisabled":Z
    :cond_3
    iget-boolean v3, p1, Lcom/android/server/power/PowerManagerService$WakeLock;->mDisabled:Z

    if-eq v2, v3, :cond_5

    .line 474
    invoke-virtual {p1, v2}, Lcom/android/server/power/PowerManagerService$WakeLock;->setDisabled(Z)Z

    .line 475
    invoke-direct {p0, p1, v2}, Lcom/android/server/power/PowerManagerServiceImpl;->printProximityWakeLockStatusChange(Lcom/android/server/power/PowerManagerService$WakeLock;Z)V

    .line 477
    iget-boolean v3, p1, Lcom/android/server/power/PowerManagerService$WakeLock;->mDisabled:Z

    if-eqz v3, :cond_4

    .line 478
    iget-object v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPowerManagerService:Lcom/android/server/power/PowerManagerService;

    invoke-virtual {v3, p1}, Lcom/android/server/power/PowerManagerService;->notifyWakeLockReleasedLocked(Lcom/android/server/power/PowerManagerService$WakeLock;)V

    goto :goto_2

    .line 480
    :cond_4
    iget-object v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPowerManagerService:Lcom/android/server/power/PowerManagerService;

    invoke-virtual {v3, p1}, Lcom/android/server/power/PowerManagerService;->notifyWakeLockAcquiredLocked(Lcom/android/server/power/PowerManagerService$WakeLock;)V

    .line 482
    :goto_2
    iget-object v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPowerManagerService:Lcom/android/server/power/PowerManagerService;

    invoke-virtual {v3}, Lcom/android/server/power/PowerManagerService;->setWakeLockDirtyLocked()V

    .line 483
    iget-object v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPowerManagerService:Lcom/android/server/power/PowerManagerService;

    invoke-virtual {v3}, Lcom/android/server/power/PowerManagerService;->updatePowerStateLocked()V

    .line 485
    .end local v2    # "shouldDisabled":Z
    :cond_5
    goto :goto_3

    .line 487
    :cond_6
    iget-object v2, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    invoke-interface {v2}, Lcom/android/server/policy/WindowManagerPolicy;->isKeyguardOccluded()Z

    move-result v2

    if-nez v2, :cond_7

    if-eqz v0, :cond_8

    :cond_7
    iget-boolean v2, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mIsFreeFormOrSplitWindowMode:Z

    if-nez v2, :cond_8

    if-eqz v4, :cond_9

    .line 489
    :cond_8
    invoke-virtual {p1, v3}, Lcom/android/server/power/PowerManagerService$WakeLock;->setDisabled(Z)Z

    .line 491
    invoke-direct {p0, p1, v3}, Lcom/android/server/power/PowerManagerServiceImpl;->printProximityWakeLockStatusChange(Lcom/android/server/power/PowerManagerService$WakeLock;Z)V

    .line 495
    .end local v0    # "isInForeground":Z
    .end local v1    # "isUnfolded":Z
    .end local v4    # "isDisabledProximitySensor":Z
    :cond_9
    :goto_3
    return-void
.end method

.method public setTouchInteractive(Z)V
    .locals 2
    .param p1, "isDimming"    # Z

    .line 607
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mTouchInteractiveUnavailable:Z

    if-nez v0, :cond_0

    .line 608
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mTouchInteractiveUnavailable:Z

    .line 609
    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mMiuiInputManagerInternal:Lcom/android/server/input/MiuiInputManagerInternal;

    invoke-virtual {v1, v0}, Lcom/android/server/input/MiuiInputManagerInternal;->setDimState(Z)V

    goto :goto_0

    .line 610
    :cond_0
    if-nez p1, :cond_1

    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mTouchInteractiveUnavailable:Z

    if-eqz v0, :cond_1

    .line 611
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mTouchInteractiveUnavailable:Z

    .line 612
    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mMiuiInputManagerInternal:Lcom/android/server/input/MiuiInputManagerInternal;

    invoke-virtual {v1, v0}, Lcom/android/server/input/MiuiInputManagerInternal;->setDimState(Z)V

    .line 614
    :cond_1
    :goto_0
    return-void
.end method

.method public setUidPartialWakeLockDisabledState(ILjava/lang/String;Z)V
    .locals 9
    .param p1, "uid"    # I
    .param p2, "tag"    # Ljava/lang/String;
    .param p3, "disabled"    # Z

    .line 788
    if-nez p2, :cond_1

    invoke-static {p1}, Landroid/os/UserHandle;->isApp(I)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 789
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "can not disable all wakelock for uid "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 792
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 793
    :try_start_0
    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mWakeLocks:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/power/PowerManagerService$WakeLock;

    .line 794
    .local v2, "wakeLock":Lcom/android/server/power/PowerManagerService$WakeLock;
    const/4 v3, 0x0

    .line 795
    .local v3, "changed":Z
    iget v4, v2, Lcom/android/server/power/PowerManagerService$WakeLock;->mFlags:I

    const v5, 0xffff

    and-int/2addr v4, v5

    packed-switch v4, :pswitch_data_0

    goto :goto_4

    .line 797
    :pswitch_0
    invoke-direct {p0, v2}, Lcom/android/server/power/PowerManagerServiceImpl;->getRealOwners(Lcom/android/server/power/PowerManagerService$WakeLock;)[I

    move-result-object v4

    .line 798
    .local v4, "realOwners":[I
    array-length v5, v4

    const/4 v6, 0x0

    :goto_2
    if-ge v6, v5, :cond_4

    aget v7, v4, v6

    .line 799
    .local v7, "realOwner":I
    if-ne v7, p1, :cond_3

    if-eqz p2, :cond_2

    iget-object v8, v2, Lcom/android/server/power/PowerManagerService$WakeLock;->mTag:Ljava/lang/String;

    invoke-virtual {p2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 800
    :cond_2
    invoke-direct {p0, v2, p3}, Lcom/android/server/power/PowerManagerServiceImpl;->setWakeLockDisabledStateLocked(Lcom/android/server/power/PowerManagerService$WakeLock;Z)Z

    move-result v5

    move v3, v5

    .line 801
    goto :goto_3

    .line 798
    .end local v7    # "realOwner":I
    :cond_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 804
    :cond_4
    :goto_3
    nop

    .line 808
    .end local v4    # "realOwners":[I
    :goto_4
    if-eqz v3, :cond_6

    .line 809
    iget-boolean v4, v2, Lcom/android/server/power/PowerManagerService$WakeLock;->mDisabled:Z

    if-eqz v4, :cond_5

    .line 810
    iget-object v4, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPowerManagerService:Lcom/android/server/power/PowerManagerService;

    invoke-virtual {v4, v2}, Lcom/android/server/power/PowerManagerService;->notifyWakeLockReleasedLocked(Lcom/android/server/power/PowerManagerService$WakeLock;)V

    goto :goto_5

    .line 812
    :cond_5
    iget-object v4, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPowerManagerService:Lcom/android/server/power/PowerManagerService;

    invoke-virtual {v4, v2}, Lcom/android/server/power/PowerManagerService;->notifyWakeLockAcquiredLocked(Lcom/android/server/power/PowerManagerService$WakeLock;)V

    .line 814
    :goto_5
    const-string/jumbo v4, "unknown"

    invoke-virtual {p0, v2, p3, v4}, Lcom/android/server/power/PowerManagerServiceImpl;->printPartialWakeLockDisabledIfNeeded(Lcom/android/server/power/PowerManagerService$WakeLock;ZLjava/lang/String;)V

    .line 815
    iget-object v4, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPowerManagerService:Lcom/android/server/power/PowerManagerService;

    invoke-virtual {v4}, Lcom/android/server/power/PowerManagerService;->setWakeLockDirtyLocked()V

    .line 816
    iget-object v4, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPowerManagerService:Lcom/android/server/power/PowerManagerService;

    invoke-virtual {v4}, Lcom/android/server/power/PowerManagerService;->updatePowerStateLocked()V

    .line 818
    .end local v2    # "wakeLock":Lcom/android/server/power/PowerManagerService$WakeLock;
    .end local v3    # "changed":Z
    :cond_6
    goto :goto_1

    .line 819
    :cond_7
    monitor-exit v0

    .line 820
    return-void

    .line 819
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public setUidPartialWakeLockDisabledState(ILjava/lang/String;ZLjava/lang/String;)V
    .locals 10
    .param p1, "uid"    # I
    .param p2, "tag"    # Ljava/lang/String;
    .param p3, "disabled"    # Z
    .param p4, "reason"    # Ljava/lang/String;

    .line 750
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 751
    const/4 v1, 0x0

    .line 752
    .local v1, "ret":Z
    :try_start_0
    iget-object v2, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mWakeLocks:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/power/PowerManagerService$WakeLock;

    .line 753
    .local v3, "wakeLock":Lcom/android/server/power/PowerManagerService$WakeLock;
    const/4 v4, 0x0

    .line 754
    .local v4, "changed":Z
    iget v5, v3, Lcom/android/server/power/PowerManagerService$WakeLock;->mFlags:I

    const v6, 0xffff

    and-int/2addr v5, v6

    packed-switch v5, :pswitch_data_0

    goto :goto_2

    .line 756
    :pswitch_0
    invoke-direct {p0, v3}, Lcom/android/server/power/PowerManagerServiceImpl;->getRealOwners(Lcom/android/server/power/PowerManagerService$WakeLock;)[I

    move-result-object v5

    .line 757
    .local v5, "realOwners":[I
    array-length v6, v5

    const/4 v7, 0x0

    :goto_1
    if-ge v7, v6, :cond_2

    aget v8, v5, v7

    .line 758
    .local v8, "realOwner":I
    if-ne v8, p1, :cond_1

    if-eqz p2, :cond_0

    iget-object v9, v3, Lcom/android/server/power/PowerManagerService$WakeLock;->mTag:Ljava/lang/String;

    invoke-virtual {p2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 759
    :cond_0
    iget-boolean v9, v3, Lcom/android/server/power/PowerManagerService$WakeLock;->mDisabled:Z

    if-eq v9, p3, :cond_1

    .line 760
    invoke-virtual {p0, v3, p3, p4}, Lcom/android/server/power/PowerManagerServiceImpl;->printPartialWakeLockDisabledIfNeeded(Lcom/android/server/power/PowerManagerService$WakeLock;ZLjava/lang/String;)V

    .line 761
    iput-boolean p3, v3, Lcom/android/server/power/PowerManagerService$WakeLock;->mDisabled:Z

    .line 762
    const/4 v4, 0x1

    .line 757
    .end local v8    # "realOwner":I
    :cond_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 766
    :cond_2
    nop

    .line 770
    .end local v5    # "realOwners":[I
    :goto_2
    if-eqz v4, :cond_4

    .line 771
    iget-boolean v5, v3, Lcom/android/server/power/PowerManagerService$WakeLock;->mDisabled:Z

    if-eqz v5, :cond_3

    .line 772
    iget-object v5, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPowerManagerService:Lcom/android/server/power/PowerManagerService;

    invoke-virtual {v5, v3}, Lcom/android/server/power/PowerManagerService;->notifyWakeLockReleasedLocked(Lcom/android/server/power/PowerManagerService$WakeLock;)V

    goto :goto_3

    .line 774
    :cond_3
    iget-object v5, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPowerManagerService:Lcom/android/server/power/PowerManagerService;

    invoke-virtual {v5, v3}, Lcom/android/server/power/PowerManagerService;->notifyWakeLockAcquiredLocked(Lcom/android/server/power/PowerManagerService$WakeLock;)V

    .line 776
    :goto_3
    const/4 v1, 0x1

    .line 778
    .end local v3    # "wakeLock":Lcom/android/server/power/PowerManagerService$WakeLock;
    .end local v4    # "changed":Z
    :cond_4
    goto :goto_0

    .line 779
    :cond_5
    if-eqz v1, :cond_6

    .line 780
    iget-object v2, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPowerManagerService:Lcom/android/server/power/PowerManagerService;

    invoke-virtual {v2}, Lcom/android/server/power/PowerManagerService;->setWakeLockDirtyLocked()V

    .line 781
    iget-object v2, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPowerManagerService:Lcom/android/server/power/PowerManagerService;

    invoke-virtual {v2}, Lcom/android/server/power/PowerManagerService;->updatePowerStateLocked()V

    .line 783
    .end local v1    # "ret":Z
    :cond_6
    monitor-exit v0

    .line 784
    return-void

    .line 783
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public shouldAlwaysWakeUpSecondaryDisplay()Z
    .locals 1

    .line 1669
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mAlwaysWakeUp:Z

    return v0
.end method

.method public shouldEnterHangupLocked()Z
    .locals 1

    .line 976
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mScreenProjectionEnabled:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSynergyModeEnable:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method protected shouldOverrideUserActivityTimeoutLocked(JJF)J
    .locals 4
    .param p1, "timeout"    # J
    .param p3, "maximumScreenDimDurationConfig"    # J
    .param p5, "maximumScreenDimRatioConfig"    # F

    .line 983
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSynergyModeEnable:Z

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mUserActivityTimeoutOverrideFromMirrorManager:J

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_0

    .line 984
    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide p1

    .line 986
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSituatedDimmingDueToAttention:Z

    if-eqz v0, :cond_1

    .line 987
    long-to-float v0, p1

    mul-float/2addr v0, p5

    float-to-long v0, v0

    invoke-static {p3, p4, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide p1

    .line 990
    :cond_1
    return-wide p1
.end method

.method public shouldRequestDisplayHangupLocked()Z
    .locals 4

    .line 932
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSynergyModeEnable:Z

    const/4 v1, 0x4

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    iget v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPreWakefulness:I

    if-ne v3, v1, :cond_1

    iget v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mWakefulness:I

    if-ne v3, v2, :cond_1

    iget-boolean v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mIsKeyguardHasShownWhenExitHangup:Z

    if-nez v3, :cond_1

    .line 936
    invoke-static {}, Lcom/android/server/wm/WindowManagerServiceStub;->get()Lcom/android/server/wm/WindowManagerServiceStub;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/server/wm/WindowManagerServiceStub;->isKeyGuardShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 937
    iput-boolean v2, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mIsKeyguardHasShownWhenExitHangup:Z

    .line 940
    :cond_0
    return v2

    .line 942
    :cond_1
    iget-boolean v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mScreenProjectionEnabled:Z

    if-nez v3, :cond_2

    if-eqz v0, :cond_3

    :cond_2
    iget v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mWakefulness:I

    if-ne v0, v1, :cond_3

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    :goto_0
    return v2
.end method

.method public shouldWakeUpWhenPluggedOrUnpluggedSkipByMiui()Z
    .locals 1

    .line 1896
    const/4 v0, 0x1

    return v0
.end method

.method protected unregisterDeathCallbackLocked(Landroid/os/IBinder;)V
    .locals 2
    .param p1, "token"    # Landroid/os/IBinder;

    .line 1257
    if-eqz p1, :cond_0

    .line 1258
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mClientDeathCallbacks:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/power/PowerManagerServiceImpl$ClientDeathCallback;

    .line 1259
    .local v0, "deathCallback":Lcom/android/server/power/PowerManagerServiceImpl$ClientDeathCallback;
    if-eqz v0, :cond_0

    .line 1260
    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 1263
    .end local v0    # "deathCallback":Lcom/android/server/power/PowerManagerServiceImpl$ClientDeathCallback;
    :cond_0
    return-void
.end method

.method public updateAllPartialWakeLockDisableState()V
    .locals 6

    .line 729
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 730
    const/4 v1, 0x0

    .line 731
    .local v1, "changed":Z
    :try_start_0
    iget-object v2, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mWakeLocks:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/power/PowerManagerService$WakeLock;

    .line 732
    .local v3, "wakeLock":Lcom/android/server/power/PowerManagerService$WakeLock;
    iget v4, v3, Lcom/android/server/power/PowerManagerService$WakeLock;->mFlags:I

    const v5, 0xffff

    and-int/2addr v4, v5

    packed-switch v4, :pswitch_data_0

    goto :goto_1

    .line 734
    :pswitch_0
    invoke-virtual {p0, v3}, Lcom/android/server/power/PowerManagerServiceImpl;->isWakelockDisabledByPolicy(Lcom/android/server/power/PowerManagerService$WakeLock;)Z

    move-result v4

    .line 735
    .local v4, "disabled":Z
    invoke-direct {p0, v3, v4}, Lcom/android/server/power/PowerManagerServiceImpl;->setWakeLockDisabledStateLocked(Lcom/android/server/power/PowerManagerService$WakeLock;Z)Z

    move-result v5

    or-int/2addr v1, v5

    .line 736
    nop

    .line 740
    .end local v3    # "wakeLock":Lcom/android/server/power/PowerManagerService$WakeLock;
    .end local v4    # "disabled":Z
    :goto_1
    goto :goto_0

    .line 741
    :cond_0
    if-eqz v1, :cond_1

    .line 742
    iget-object v2, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPowerManagerService:Lcom/android/server/power/PowerManagerService;

    invoke-virtual {v2}, Lcom/android/server/power/PowerManagerService;->setWakeLockDirtyLocked()V

    .line 743
    iget-object v2, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPowerManagerService:Lcom/android/server/power/PowerManagerService;

    invoke-virtual {v2}, Lcom/android/server/power/PowerManagerService;->updatePowerStateLocked()V

    .line 745
    .end local v1    # "changed":Z
    :cond_1
    monitor-exit v0

    .line 746
    return-void

    .line 745
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public updateBlockUntilScreenOnReady(Lcom/android/server/power/PowerGroup;Landroid/hardware/display/DisplayManagerInternal$DisplayPowerRequest;)V
    .locals 2
    .param p1, "powerGroup"    # Lcom/android/server/power/PowerGroup;
    .param p2, "displayPowerRequest"    # Landroid/hardware/display/DisplayManagerInternal$DisplayPowerRequest;

    .line 1920
    nop

    .line 1921
    invoke-virtual {p1}, Lcom/android/server/power/PowerGroup;->getGroupId()I

    move-result v0

    if-nez v0, :cond_1

    .line 1922
    invoke-virtual {p1}, Lcom/android/server/power/PowerGroup;->isPoweringOnLocked()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p2, Landroid/hardware/display/DisplayManagerInternal$DisplayPowerRequest;->policy:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    sget-boolean v0, Lcom/android/server/power/PowerManagerServiceImpl;->SUPPORT_FOD:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/server/power/PowerManagerServiceImpl;->SKIP_TRANSITION_WAKE_UP_DETAILS:Ljava/util/List;

    .line 1925
    invoke-virtual {p1}, Lcom/android/server/power/PowerGroup;->getLastWakeReasonDetails()Ljava/lang/String;

    move-result-object v1

    .line 1924
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p2, Landroid/hardware/display/DisplayManagerInternal$DisplayPowerRequest;->setBrightnessUntilScreenOnReady:Z

    .line 1926
    return-void
.end method

.method public updateDimState(ZJ)V
    .locals 2
    .param p1, "dimEnable"    # Z
    .param p2, "brightWaitTime"    # J

    .line 1204
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mIsBrightWakeLock:Z

    .line 1205
    iput-boolean p1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mDimEnable:Z

    .line 1206
    iput-wide p2, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mBrightWaitTime:J

    .line 1207
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1208
    :try_start_0
    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPowerManagerService:Lcom/android/server/power/PowerManagerService;

    invoke-virtual {v1}, Lcom/android/server/power/PowerManagerService;->updatePowerStateLocked()V

    .line 1209
    monitor-exit v0

    .line 1210
    return-void

    .line 1209
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public updateLowPowerModeIfNeeded(Landroid/hardware/display/DisplayManagerInternal$DisplayPowerRequest;)V
    .locals 1
    .param p1, "displayPowerRequest"    # Landroid/hardware/display/DisplayManagerInternal$DisplayPowerRequest;

    .line 1652
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mIsPowerSaveModeEnabled:Z

    if-nez v0, :cond_0

    .line 1653
    return-void

    .line 1655
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p1, Landroid/hardware/display/DisplayManagerInternal$DisplayPowerRequest;->lowPowerMode:Z

    .line 1656
    sget v0, Lcom/android/server/power/PowerManagerServiceImpl;->mBrightnessDecreaseRatio:F

    iput v0, p1, Landroid/hardware/display/DisplayManagerInternal$DisplayPowerRequest;->screenLowPowerBrightnessFactor:F

    .line 1657
    return-void
.end method

.method protected updateNextDimTimeoutIfNeededLocked(JJ)J
    .locals 5
    .param p1, "nextTimeout"    # J
    .param p3, "lastUserActivityTime"    # J

    .line 1114
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSynergyModeEnable:Z

    const-wide/16 v1, 0x0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSituatedDimmingDueToSynergy:Z

    if-eqz v0, :cond_0

    iget-wide v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mUserActivityTimeoutOverrideFromMirrorManager:J

    cmp-long v0, v3, v1

    if-ltz v0, :cond_0

    iget-wide v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLastRequestDimmingTime:J

    cmp-long v0, v3, v1

    if-lez v0, :cond_0

    .line 1117
    iget-wide p1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLastRequestDimmingTime:J

    goto :goto_0

    .line 1118
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mDimEnable:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mIsBrightWakeLock:Z

    if-eqz v0, :cond_1

    .line 1119
    iget-wide v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mBrightWaitTime:J

    add-long p1, p3, v0

    goto :goto_0

    .line 1120
    :cond_1
    iget-boolean v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mSituatedDimmingDueToAttention:Z

    if-eqz v0, :cond_2

    iget-wide v3, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mLastRequestDimmingTime:J

    cmp-long v0, v3, v1

    if-lez v0, :cond_2

    .line 1121
    invoke-static {p1, p2, v3, v4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide p1

    .line 1123
    :cond_2
    :goto_0
    return-wide p1
.end method

.method public updatePowerGroupPolicy(II)V
    .locals 1
    .param p1, "groupId"    # I
    .param p2, "policy"    # I

    .line 1977
    if-nez p1, :cond_1

    .line 1978
    const/4 v0, 0x2

    if-ne v0, p2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 1979
    .local v0, "isDimming":Z
    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/server/power/PowerManagerServiceImpl;->setPickUpSensorEnable(Z)V

    .line 1980
    invoke-virtual {p0, v0}, Lcom/android/server/power/PowerManagerServiceImpl;->setTouchInteractive(Z)V

    .line 1982
    .end local v0    # "isDimming":Z
    :cond_1
    return-void
.end method

.method protected updateUserActivityLocked()V
    .locals 4

    .line 1127
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPowerManager:Landroid/os/PowerManager;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3, v3}, Landroid/os/PowerManager;->userActivity(JII)V

    .line 1129
    return-void
.end method

.method public updateWakefulnessLocked(I)V
    .locals 2
    .param p1, "wakefulness"    # I

    .line 1050
    iget v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mWakefulness:I

    iput v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mPreWakefulness:I

    .line 1051
    iput p1, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mWakefulness:I

    .line 1052
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl;->mAttentionDetector:Lcom/android/server/power/MiuiAttentionDetector;

    if-eqz v0, :cond_0

    .line 1053
    nop

    .line 1054
    invoke-static {p1}, Landroid/os/PowerManagerInternal;->isInteractive(I)Z

    move-result v1

    .line 1053
    invoke-virtual {v0, v1}, Lcom/android/server/power/MiuiAttentionDetector;->notifyInteractiveChange(Z)V

    .line 1056
    :cond_0
    return-void
.end method
