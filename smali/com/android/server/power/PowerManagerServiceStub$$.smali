.class public final Lcom/android/server/power/PowerManagerServiceStub$$;
.super Ljava/lang/Object;
.source "PowerManagerServiceStub$$.java"

# interfaces
.implements Lcom/miui/base/MiuiStubRegistry$ImplProviderManifest;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final collectImplProviders(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/miui/base/MiuiStubRegistry$ImplProvider<",
            "*>;>;)V"
        }
    .end annotation

    .line 20
    .local p1, "outProviders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/miui/base/MiuiStubRegistry$ImplProvider<*>;>;"
    new-instance v0, Lcom/android/server/power/PowerManagerServiceImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/power/PowerManagerServiceImpl$Provider;-><init>()V

    const-string v1, "com.android.server.power.PowerManagerServiceStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    new-instance v0, Lcom/android/server/power/ShutdownThreadImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/power/ShutdownThreadImpl$Provider;-><init>()V

    const-string v1, "com.android.server.power.ShutdownThreadStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    new-instance v0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.power.stats.BatteryStatsManagerStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    new-instance v0, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$Provider;-><init>()V

    const-string v1, "com.android.server.power.stats.ScreenPowerCalculatorStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    return-void
.end method
