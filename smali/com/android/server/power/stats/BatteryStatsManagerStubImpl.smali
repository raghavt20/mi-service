.class public Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;
.super Ljava/lang/Object;
.source "BatteryStatsManagerStubImpl.java"

# interfaces
.implements Lcom/android/server/power/stats/BatteryStatsManagerStub;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;
    }
.end annotation


# static fields
.field private static final BYTES_PER_GB:J = 0x40000000L

.field private static final BYTES_PER_KB:J = 0x400L

.field private static final BYTES_PER_MB:J = 0x100000L

.field private static final DEBUG:Z = false

.field private static final TAG:Ljava/lang/String; = "BatteryStatsManagerStub"


# instance fields
.field public mActiveCallback:Lcom/android/server/power/stats/BatteryStatsManagerStub$ActiveCallback;

.field private mCalculateAppMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentSoleUid:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mCalculateAppMap:Landroid/util/SparseArray;

    .line 31
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mCurrentSoleUid:I

    return-void
.end method

.method private getProcessForegroundTimeMs(ILandroid/os/BatteryStats;J)J
    .locals 11
    .param p1, "curUid"    # I
    .param p2, "batteryStats"    # Landroid/os/BatteryStats;
    .param p3, "realtimeUs"    # J

    .line 345
    invoke-virtual {p2}, Landroid/os/BatteryStats;->getUidStats()Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/BatteryStats$Uid;

    .line 346
    .local v0, "uid":Landroid/os/BatteryStats$Uid;
    if-nez v0, :cond_0

    const-wide/16 v1, 0x0

    return-wide v1

    .line 348
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1, p3, p4, v1}, Landroid/os/BatteryStats$Uid;->getProcessStateTime(IJI)J

    move-result-wide v2

    .line 350
    .local v2, "topStateDurationUs":J
    const-wide/16 v4, 0x0

    .line 351
    .local v4, "foregroundActivityDurationUs":J
    invoke-virtual {v0}, Landroid/os/BatteryStats$Uid;->getForegroundActivityTimer()Landroid/os/BatteryStats$Timer;

    move-result-object v6

    .line 352
    .local v6, "foregroundActivityTimer":Landroid/os/BatteryStats$Timer;
    if-eqz v6, :cond_1

    .line 353
    invoke-virtual {v6, p3, p4, v1}, Landroid/os/BatteryStats$Timer;->getTotalTimeLocked(JI)J

    move-result-wide v4

    .line 357
    :cond_1
    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v7

    .line 359
    .local v7, "totalForegroundDurationUs":J
    const-wide/16 v9, 0x3e8

    div-long v9, v7, v9

    return-wide v9
.end method

.method private readSoleCalculateApp(Landroid/os/Parcel;Lcom/android/server/power/stats/BatteryStatsImpl;)V
    .locals 8
    .param p1, "in"    # Landroid/os/Parcel;
    .param p2, "batteryStats"    # Lcom/android/server/power/stats/BatteryStatsImpl;

    .line 384
    iget-object v0, p0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mCalculateAppMap:Landroid/util/SparseArray;

    monitor-enter v0

    .line 385
    :try_start_0
    iget-object v1, p0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mCalculateAppMap:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->clear()V

    .line 386
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 387
    .local v1, "size":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_3

    .line 388
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 389
    .local v3, "uid":I
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 390
    .local v4, "pkgName":Ljava/lang/String;
    new-instance v5, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;

    iget-object v6, p2, Lcom/android/server/power/stats/BatteryStatsImpl;->mClock:Lcom/android/internal/os/Clock;

    iget-object v7, p2, Lcom/android/server/power/stats/BatteryStatsImpl;->mOnBatteryTimeBase:Lcom/android/server/power/stats/BatteryStatsImpl$TimeBase;

    invoke-direct {v5, v6, v7}, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;-><init>(Lcom/android/internal/os/Clock;Lcom/android/server/power/stats/BatteryStatsImpl$TimeBase;)V

    .line 391
    .local v5, "app":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;
    const/4 v6, 0x0

    .local v6, "j":I
    :goto_1
    const/16 v7, 0x64

    if-ge v6, v7, :cond_1

    .line 392
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_0

    .line 393
    iget-object v7, v5, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->screenBrightnessTimers:[Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;

    aget-object v7, v7, v6

    invoke-virtual {v7, p1}, Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;->readSummaryFromParcelLocked(Landroid/os/Parcel;)V

    .line 391
    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 397
    .end local v6    # "j":I
    :cond_1
    iput v3, v5, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->uid:I

    .line 398
    iput-object v4, v5, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->packageName:Ljava/lang/String;

    .line 399
    invoke-static {v3}, Landroid/os/UserHandle;->isApp(I)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 400
    iget-object v6, p0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mCalculateAppMap:Landroid/util/SparseArray;

    invoke-virtual {v6, v3, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 387
    .end local v3    # "uid":I
    .end local v4    # "pkgName":Ljava/lang/String;
    .end local v5    # "app":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 403
    .end local v1    # "size":I
    .end local v2    # "i":I
    :cond_3
    monitor-exit v0

    .line 404
    return-void

    .line 403
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private writeSoleCalculateApp(Landroid/os/Parcel;J)V
    .locals 5
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "elapsedRealtimeUs"    # J

    .line 407
    iget-object v0, p0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mCalculateAppMap:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    .line 408
    .local v0, "size":I
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 409
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_2

    .line 410
    iget-object v2, p0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mCalculateAppMap:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;

    .line 411
    .local v2, "app":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;
    if-eqz v2, :cond_1

    .line 412
    iget v3, v2, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->uid:I

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 413
    iget-object v3, v2, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->packageName:Ljava/lang/String;

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 414
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    const/16 v4, 0x64

    if-ge v3, v4, :cond_1

    .line 415
    iget-object v4, v2, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->screenBrightnessTimers:[Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;

    aget-object v4, v4, v3

    if-eqz v4, :cond_0

    .line 416
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 417
    iget-object v4, v2, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->screenBrightnessTimers:[Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;

    aget-object v4, v4, v3

    invoke-virtual {v4, p1, p2, p3}, Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;->writeSummaryFromParcelLocked(Landroid/os/Parcel;J)V

    goto :goto_2

    .line 419
    :cond_0
    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 414
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 409
    .end local v2    # "app":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;
    .end local v3    # "j":I
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 424
    .end local v1    # "i":I
    :cond_2
    return-void
.end method


# virtual methods
.method public getAppScreenInfo(Landroid/os/BatteryStats;J)[B
    .locals 16
    .param p1, "batteryStats"    # Landroid/os/BatteryStats;
    .param p2, "elapsedRealtimeMs"    # J

    .line 264
    move-object/from16 v1, p0

    const/4 v2, 0x0

    .line 265
    .local v2, "bytes":[B
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 267
    .local v3, "out":Landroid/os/Parcel;
    :try_start_0
    iget-object v0, v1, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mCalculateAppMap:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    const/4 v4, 0x0

    if-lez v0, :cond_3

    .line 268
    iget-object v0, v1, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mCalculateAppMap:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 271
    const/16 v0, 0x64

    new-array v5, v0, [J

    .line 272
    .local v5, "binTimes":[J
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    iget-object v7, v1, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mCalculateAppMap:Landroid/util/SparseArray;

    invoke-virtual {v7}, Landroid/util/SparseArray;->size()I

    move-result v7

    if-ge v6, v7, :cond_2

    .line 273
    iget-object v7, v1, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mCalculateAppMap:Landroid/util/SparseArray;

    invoke-virtual {v7, v6}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;

    .line 274
    .local v7, "app":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;
    if-eqz v7, :cond_1

    .line 275
    iget-object v8, v7, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->packageName:Ljava/lang/String;

    .line 276
    .local v8, "pkgName":Ljava/lang/String;
    iget v9, v7, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->uid:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-wide/16 v10, 0x3e8

    mul-long v12, p2, v10

    move-object/from16 v14, p1

    :try_start_1
    invoke-direct {v1, v9, v14, v12, v13}, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->getProcessForegroundTimeMs(ILandroid/os/BatteryStats;J)J

    move-result-wide v12

    .line 278
    .local v12, "fgTimeMs":J
    const/4 v9, 0x0

    .local v9, "bin":I
    :goto_1
    if-ge v9, v0, :cond_0

    .line 279
    iget-object v15, v7, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->screenBrightnessTimers:[Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;

    aget-object v15, v15, v9

    mul-long v0, p2, v10

    invoke-virtual {v15, v0, v1, v4}, Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;->getTotalTimeLocked(JI)J

    move-result-wide v0

    div-long/2addr v0, v10

    aput-wide v0, v5, v9

    .line 278
    add-int/lit8 v9, v9, 0x1

    const/16 v0, 0x64

    move-object/from16 v1, p0

    goto :goto_1

    .line 283
    .end local v9    # "bin":I
    :cond_0
    invoke-virtual {v3, v8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 284
    invoke-virtual {v3, v12, v13}, Landroid/os/Parcel;->writeLong(J)V

    .line 285
    invoke-virtual {v3, v5}, Landroid/os/Parcel;->writeLongArray([J)V

    goto :goto_2

    .line 274
    .end local v8    # "pkgName":Ljava/lang/String;
    .end local v12    # "fgTimeMs":J
    :cond_1
    move-object/from16 v14, p1

    .line 272
    .end local v7    # "app":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;
    :goto_2
    add-int/lit8 v6, v6, 0x1

    const/16 v0, 0x64

    move-object/from16 v1, p0

    goto :goto_0

    :cond_2
    move-object/from16 v14, p1

    .line 288
    .end local v5    # "binTimes":[J
    .end local v6    # "i":I
    goto :goto_3

    .line 289
    :cond_3
    move-object/from16 v14, p1

    invoke-virtual {v3, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 291
    :goto_3
    invoke-virtual {v3}, Landroid/os/Parcel;->marshall()[B

    move-result-object v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v2, v0

    goto :goto_5

    .line 292
    :catch_0
    move-exception v0

    goto :goto_4

    .line 295
    :catchall_0
    move-exception v0

    move-object/from16 v14, p1

    goto :goto_6

    .line 292
    :catch_1
    move-exception v0

    move-object/from16 v14, p1

    .line 293
    .local v0, "e":Ljava/lang/Exception;
    :goto_4
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 295
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_5
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 296
    nop

    .line 297
    return-object v2

    .line 295
    :catchall_1
    move-exception v0

    :goto_6
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 296
    throw v0
.end method

.method public getDefaultDataSlotId()I
    .locals 1

    .line 116
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/telephony/SubscriptionManager;->getDefaultDataSlotId()I

    move-result v0

    return v0
.end method

.method public getScreenCalculateMap()Landroid/util/SparseArray;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/SparseArray<",
            "Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;",
            ">;"
        }
    .end annotation

    .line 427
    iget-object v0, p0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mCalculateAppMap:Landroid/util/SparseArray;

    return-object v0
.end method

.method public getTargetScreenInfo(Ljava/lang/String;Landroid/os/BatteryStats;J)[B
    .locals 16
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "batteryStats"    # Landroid/os/BatteryStats;
    .param p3, "elapsedRealtimeMs"    # J

    .line 302
    move-object/from16 v1, p0

    const/4 v2, 0x0

    .line 303
    .local v2, "bytes":[B
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 305
    .local v3, "out":Landroid/os/Parcel;
    :try_start_0
    iget-object v0, v1, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mCalculateAppMap:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    const/4 v4, 0x0

    if-lez v0, :cond_5

    .line 306
    iget-object v0, v1, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mCalculateAppMap:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 308
    const/16 v0, 0x64

    new-array v5, v0, [J

    .line 309
    .local v5, "binTimes":[J
    const/4 v6, 0x0

    .line 310
    .local v6, "app":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;
    const/4 v7, 0x0

    .line 311
    .local v7, "isExist":Z
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    iget-object v9, v1, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mCalculateAppMap:Landroid/util/SparseArray;

    invoke-virtual {v9}, Landroid/util/SparseArray;->size()I

    move-result v9

    if-ge v8, v9, :cond_2

    .line 312
    iget-object v9, v1, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mCalculateAppMap:Landroid/util/SparseArray;

    invoke-virtual {v9, v8}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;

    move-object v6, v9

    .line 313
    if-eqz v6, :cond_0

    iget-object v9, v6, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->packageName:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-object/from16 v10, p1

    :try_start_1
    invoke-static {v9, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 314
    const/4 v7, 0x1

    .line 315
    goto :goto_1

    .line 313
    :cond_0
    move-object/from16 v10, p1

    .line 311
    :cond_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_2
    move-object/from16 v10, p1

    .line 318
    .end local v8    # "i":I
    :goto_1
    if-eqz v7, :cond_4

    .line 319
    const/4 v8, 0x1

    invoke-virtual {v3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 320
    iget v8, v6, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->uid:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-wide/16 v11, 0x3e8

    mul-long v13, p3, v11

    move-object/from16 v9, p2

    :try_start_2
    invoke-direct {v1, v8, v9, v13, v14}, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->getProcessForegroundTimeMs(ILandroid/os/BatteryStats;J)J

    move-result-wide v13

    .line 322
    .local v13, "fgTimeMs":J
    const/4 v8, 0x0

    .local v8, "bin":I
    :goto_2
    if-ge v8, v0, :cond_3

    .line 323
    iget-object v15, v6, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->screenBrightnessTimers:[Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;

    aget-object v15, v15, v8

    mul-long v0, p3, v11

    invoke-virtual {v15, v0, v1, v4}, Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;->getTotalTimeLocked(JI)J

    move-result-wide v0

    div-long/2addr v0, v11

    aput-wide v0, v5, v8

    .line 322
    add-int/lit8 v8, v8, 0x1

    const/16 v0, 0x64

    move-object/from16 v1, p0

    goto :goto_2

    .line 327
    .end local v8    # "bin":I
    :cond_3
    invoke-virtual {v3, v13, v14}, Landroid/os/Parcel;->writeLong(J)V

    .line 328
    invoke-virtual {v3, v5}, Landroid/os/Parcel;->writeLongArray([J)V

    goto :goto_3

    .line 339
    .end local v5    # "binTimes":[J
    .end local v6    # "app":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;
    .end local v7    # "isExist":Z
    .end local v13    # "fgTimeMs":J
    :catchall_0
    move-exception v0

    goto :goto_5

    .line 336
    :catch_0
    move-exception v0

    goto :goto_6

    .line 330
    .restart local v5    # "binTimes":[J
    .restart local v6    # "app":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;
    .restart local v7    # "isExist":Z
    :cond_4
    move-object/from16 v9, p2

    invoke-virtual {v3, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 332
    .end local v5    # "binTimes":[J
    .end local v6    # "app":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;
    .end local v7    # "isExist":Z
    :goto_3
    goto :goto_4

    .line 333
    :cond_5
    move-object/from16 v10, p1

    move-object/from16 v9, p2

    invoke-virtual {v3, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 335
    :goto_4
    invoke-virtual {v3}, Landroid/os/Parcel;->marshall()[B

    move-result-object v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-object v2, v0

    goto :goto_8

    .line 336
    :catch_1
    move-exception v0

    goto :goto_7

    .line 339
    :catchall_1
    move-exception v0

    move-object/from16 v10, p1

    :goto_5
    move-object/from16 v9, p2

    goto :goto_9

    .line 336
    :catch_2
    move-exception v0

    move-object/from16 v10, p1

    :goto_6
    move-object/from16 v9, p2

    .line 337
    .local v0, "e":Ljava/lang/Exception;
    :goto_7
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 339
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_8
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 340
    nop

    .line 341
    return-object v2

    .line 339
    :catchall_2
    move-exception v0

    :goto_9
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 340
    throw v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .line 128
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 142
    :pswitch_0
    iget v0, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {p0, v0}, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->noteStopGpsLocked(I)V

    .line 143
    goto :goto_0

    .line 139
    :pswitch_1
    iget v0, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {p0, v0}, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->noteStartGpsLocked(I)V

    .line 140
    goto :goto_0

    .line 136
    :pswitch_2
    invoke-virtual {p0}, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->noteResetAudioLocked()V

    .line 137
    goto :goto_0

    .line 133
    :pswitch_3
    iget v0, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {p0, v0}, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->noteAudioOffLocked(I)V

    .line 134
    goto :goto_0

    .line 130
    :pswitch_4
    iget v0, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {p0, v0}, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->noteAudioOnLocked(I)V

    .line 131
    nop

    .line 147
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public noteActivityPausedLocked(IJJ)V
    .locals 4
    .param p1, "uid"    # I
    .param p2, "elapsedRealtimeMs"    # J
    .param p4, "uptimeMs"    # J

    .line 240
    iget-object v0, p0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mCalculateAppMap:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->contains(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 241
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mCurrentSoleUid:I

    .line 242
    iget-object v1, p0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mCalculateAppMap:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;

    .line 243
    .local v1, "app":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;
    if-eqz v1, :cond_0

    iget v2, v1, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->oldBin:I

    if-le v2, v0, :cond_0

    .line 244
    iget-object v2, v1, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->screenBrightnessTimers:[Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;

    iget v3, v1, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->oldBin:I

    aget-object v2, v2, v3

    invoke-virtual {v2, p2, p3}, Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;->stopRunningLocked(J)V

    .line 245
    iput v0, v1, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->oldBin:I

    .line 248
    .end local v1    # "app":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;
    :cond_0
    return-void
.end method

.method public noteActivityResumedLocked(Landroid/os/BatteryStats;IJJLjava/lang/String;)V
    .locals 6
    .param p1, "batteryStats"    # Landroid/os/BatteryStats;
    .param p2, "uid"    # I
    .param p3, "elapsedRealtimeMs"    # J
    .param p5, "uptimeMs"    # J
    .param p7, "packageName"    # Ljava/lang/String;

    .line 206
    invoke-static {p2}, Landroid/os/UserHandle;->isApp(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 207
    return-void

    .line 209
    :cond_0
    iput p2, p0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mCurrentSoleUid:I

    .line 211
    move-object v0, p1

    check-cast v0, Lcom/android/server/power/stats/BatteryStatsImpl;

    .line 212
    .local v0, "impl":Lcom/android/server/power/stats/BatteryStatsImpl;
    iget-object v1, p0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mCalculateAppMap:Landroid/util/SparseArray;

    monitor-enter v1

    .line 213
    :try_start_0
    iget-object v2, p0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mCalculateAppMap:Landroid/util/SparseArray;

    invoke-virtual {v2, p2}, Landroid/util/SparseArray;->contains(I)Z

    move-result v2

    const/4 v3, -0x1

    if-nez v2, :cond_2

    .line 214
    new-instance v2, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;

    iget-object v4, v0, Lcom/android/server/power/stats/BatteryStatsImpl;->mClock:Lcom/android/internal/os/Clock;

    iget-object v5, v0, Lcom/android/server/power/stats/BatteryStatsImpl;->mOnBatteryTimeBase:Lcom/android/server/power/stats/BatteryStatsImpl$TimeBase;

    invoke-direct {v2, v4, v5}, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;-><init>(Lcom/android/internal/os/Clock;Lcom/android/server/power/stats/BatteryStatsImpl$TimeBase;)V

    .line 215
    .local v2, "app":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;
    iput p2, v2, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->uid:I

    .line 216
    iput-object p7, v2, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->packageName:Ljava/lang/String;

    .line 217
    iget v4, v0, Lcom/android/server/power/stats/BatteryStatsImpl;->mScreenBrightnessBin:I

    if-le v4, v3, :cond_1

    .line 218
    iget-object v3, v2, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->screenBrightnessTimers:[Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;

    iget v4, v0, Lcom/android/server/power/stats/BatteryStatsImpl;->mScreenBrightnessBin:I

    aget-object v3, v3, v4

    invoke-virtual {v3, p3, p4}, Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;->startRunningLocked(J)V

    .line 219
    iget v3, v0, Lcom/android/server/power/stats/BatteryStatsImpl;->mScreenBrightnessBin:I

    iput v3, v2, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->oldBin:I

    .line 221
    :cond_1
    iget-object v3, p0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mCalculateAppMap:Landroid/util/SparseArray;

    invoke-virtual {v3, p2, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    .line 223
    .end local v2    # "app":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;
    :cond_2
    iget-object v2, p0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mCalculateAppMap:Landroid/util/SparseArray;

    invoke-virtual {v2, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;

    .line 224
    .restart local v2    # "app":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;
    if-eqz v2, :cond_3

    iget v4, v0, Lcom/android/server/power/stats/BatteryStatsImpl;->mScreenBrightnessBin:I

    if-le v4, v3, :cond_3

    .line 225
    iget-object v3, v2, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->screenBrightnessTimers:[Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;

    iget v4, v0, Lcom/android/server/power/stats/BatteryStatsImpl;->mScreenBrightnessBin:I

    aget-object v3, v3, v4

    invoke-virtual {v3, p3, p4}, Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;->startRunningLocked(J)V

    .line 226
    iget v3, v0, Lcom/android/server/power/stats/BatteryStatsImpl;->mScreenBrightnessBin:I

    iput v3, v2, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->oldBin:I

    .line 229
    :cond_3
    :goto_0
    monitor-exit v1

    .line 236
    return-void

    .line 229
    .end local v2    # "app":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public noteAudioOffLocked(I)V
    .locals 1
    .param p1, "uid"    # I

    .line 41
    iget-object v0, p0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mActiveCallback:Lcom/android/server/power/stats/BatteryStatsManagerStub$ActiveCallback;

    if-eqz v0, :cond_0

    .line 42
    invoke-interface {v0, p1}, Lcom/android/server/power/stats/BatteryStatsManagerStub$ActiveCallback;->noteAudioOffLocked(I)V

    .line 43
    :cond_0
    return-void
.end method

.method public noteAudioOnLocked(I)V
    .locals 1
    .param p1, "uid"    # I

    .line 35
    iget-object v0, p0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mActiveCallback:Lcom/android/server/power/stats/BatteryStatsManagerStub$ActiveCallback;

    if-eqz v0, :cond_0

    .line 36
    invoke-interface {v0, p1}, Lcom/android/server/power/stats/BatteryStatsManagerStub$ActiveCallback;->noteAudioOnLocked(I)V

    .line 37
    :cond_0
    return-void
.end method

.method public noteMuteAudioInNeed(IIII)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "session"    # I
    .param p4, "status"    # I

    .line 378
    invoke-static {}, Lcom/miui/whetstone/server/WhetstoneActivityManagerService;->getSingletonService()Lcom/miui/whetstone/server/WhetstoneActivityManagerService;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 379
    invoke-static {}, Lcom/miui/whetstone/server/WhetstoneActivityManagerService;->getSingletonService()Lcom/miui/whetstone/server/WhetstoneActivityManagerService;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/whetstone/server/WhetstoneActivityManagerService;->notifyMuteAudioInNeed(IIII)V

    .line 381
    :cond_0
    return-void
.end method

.method public noteResetAudioLocked()V
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mActiveCallback:Lcom/android/server/power/stats/BatteryStatsManagerStub$ActiveCallback;

    if-eqz v0, :cond_0

    .line 48
    invoke-interface {v0}, Lcom/android/server/power/stats/BatteryStatsManagerStub$ActiveCallback;->noteResetAudioLocked()V

    .line 49
    :cond_0
    return-void
.end method

.method public noteScreenBrightnessLocked(Landroid/os/BatteryStats;IJJ)V
    .locals 3
    .param p1, "batteryStats"    # Landroid/os/BatteryStats;
    .param p2, "bin"    # I
    .param p3, "elapsedRealtimeMs"    # J
    .param p5, "uptimeMs"    # J

    .line 252
    iget v0, p0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mCurrentSoleUid:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 253
    iget-object v2, p0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mCalculateAppMap:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;

    .line 254
    .local v0, "app":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;
    if-eqz v0, :cond_0

    iget v2, v0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->oldBin:I

    if-le v2, v1, :cond_0

    .line 255
    iget-object v1, v0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->screenBrightnessTimers:[Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;

    iget v2, v0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->oldBin:I

    aget-object v1, v1, v2

    invoke-virtual {v1, p3, p4}, Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;->stopRunningLocked(J)V

    .line 256
    iget-object v1, v0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->screenBrightnessTimers:[Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;

    aget-object v1, v1, p2

    invoke-virtual {v1, p3, p4}, Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;->startRunningLocked(J)V

    .line 257
    iput p2, v0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->oldBin:I

    .line 260
    .end local v0    # "app":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;
    :cond_0
    return-void
.end method

.method public noteStartAudioInNeed(IIIII)V
    .locals 7
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "session"    # I
    .param p4, "port"    # I
    .param p5, "type"    # I

    .line 364
    invoke-static {}, Lcom/miui/whetstone/server/WhetstoneActivityManagerService;->getSingletonService()Lcom/miui/whetstone/server/WhetstoneActivityManagerService;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 365
    invoke-static {}, Lcom/miui/whetstone/server/WhetstoneActivityManagerService;->getSingletonService()Lcom/miui/whetstone/server/WhetstoneActivityManagerService;

    move-result-object v1

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-virtual/range {v1 .. v6}, Lcom/miui/whetstone/server/WhetstoneActivityManagerService;->notifyStartAudioInNeed(IIIII)V

    .line 367
    :cond_0
    return-void
.end method

.method public noteStartGpsLocked(I)V
    .locals 1
    .param p1, "uid"    # I

    .line 53
    iget-object v0, p0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mActiveCallback:Lcom/android/server/power/stats/BatteryStatsManagerStub$ActiveCallback;

    if-eqz v0, :cond_0

    .line 54
    invoke-interface {v0, p1}, Lcom/android/server/power/stats/BatteryStatsManagerStub$ActiveCallback;->noteStartGpsLocked(I)V

    .line 55
    :cond_0
    return-void
.end method

.method public noteStopAudioInNeed(IIIII)V
    .locals 7
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "session"    # I
    .param p4, "port"    # I
    .param p5, "type"    # I

    .line 371
    invoke-static {}, Lcom/miui/whetstone/server/WhetstoneActivityManagerService;->getSingletonService()Lcom/miui/whetstone/server/WhetstoneActivityManagerService;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 372
    invoke-static {}, Lcom/miui/whetstone/server/WhetstoneActivityManagerService;->getSingletonService()Lcom/miui/whetstone/server/WhetstoneActivityManagerService;

    move-result-object v1

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-virtual/range {v1 .. v6}, Lcom/miui/whetstone/server/WhetstoneActivityManagerService;->notifyStopAudioInNeed(IIIII)V

    .line 374
    :cond_0
    return-void
.end method

.method public noteStopGpsLocked(I)V
    .locals 1
    .param p1, "uid"    # I

    .line 59
    iget-object v0, p0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mActiveCallback:Lcom/android/server/power/stats/BatteryStatsManagerStub$ActiveCallback;

    if-eqz v0, :cond_0

    .line 60
    invoke-interface {v0, p1}, Lcom/android/server/power/stats/BatteryStatsManagerStub$ActiveCallback;->noteStopGpsLocked(I)V

    .line 61
    :cond_0
    return-void
.end method

.method public noteSyncStart(Ljava/lang/String;IZ)V
    .locals 7
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "uid"    # I
    .param p3, "start"    # Z

    .line 70
    const/16 v0, 0x2710

    if-lt p2, v0, :cond_2

    const/16 v0, 0x4e1f

    if-le p2, v0, :cond_0

    goto :goto_1

    .line 72
    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [I

    .line 73
    .local v0, "uids":[I
    const/4 v1, 0x0

    aput p2, v0, v1

    .line 74
    invoke-static {}, Lcom/miui/whetstone/PowerKeeperPolicy;->getInstance()Lcom/miui/whetstone/PowerKeeperPolicy;

    move-result-object v1

    const/4 v3, 0x4

    .line 75
    if-eqz p3, :cond_1

    const-string/jumbo v2, "syncstart"

    goto :goto_0

    :cond_1
    const-string/jumbo v2, "syncend"

    :goto_0
    move-object v4, v2

    .line 74
    move v2, p3

    move-object v5, p1

    move-object v6, v0

    invoke-virtual/range {v1 .. v6}, Lcom/miui/whetstone/PowerKeeperPolicy;->notifySyncEvent(IILjava/lang/String;Ljava/lang/String;[I)V

    .line 76
    return-void

    .line 71
    .end local v0    # "uids":[I
    :cond_2
    :goto_1
    return-void
.end method

.method public noteVideoFps(II)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "fps"    # I

    .line 453
    invoke-static {}, Lcom/miui/whetstone/server/WhetstoneActivityManagerService;->getSingletonService()Lcom/miui/whetstone/server/WhetstoneActivityManagerService;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 454
    invoke-static {}, Lcom/miui/whetstone/server/WhetstoneActivityManagerService;->getSingletonService()Lcom/miui/whetstone/server/WhetstoneActivityManagerService;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/miui/whetstone/server/WhetstoneActivityManagerService;->notifyVideoFps(II)V

    .line 456
    :cond_0
    return-void
.end method

.method public notifyDetailsWakeUp(Ljava/lang/String;ILjava/lang/String;)V
    .locals 3
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "uid"    # I
    .param p3, "name"    # Ljava/lang/String;

    .line 80
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 81
    .local v0, "b":Landroid/os/Bundle;
    const-string/jumbo v1, "uid"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 82
    const-string/jumbo v1, "type"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    const-string v1, "name"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    invoke-static {}, Lcom/miui/whetstone/PowerKeeperPolicy;->getInstance()Lcom/miui/whetstone/PowerKeeperPolicy;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2, v0}, Lcom/miui/whetstone/PowerKeeperPolicy;->notifyEvent(ILandroid/os/Bundle;)V

    .line 85
    return-void
.end method

.method public notifyEvent(ILandroid/os/Bundle;)V
    .locals 5
    .param p1, "resId"    # I
    .param p2, "bundle"    # Landroid/os/Bundle;

    .line 460
    if-nez p2, :cond_0

    .line 461
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    move-object p2, v0

    .line 463
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 464
    .local v0, "elapsedRealtimeMs":J
    const-string/jumbo v2, "startTime"

    invoke-virtual {p2, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 465
    invoke-static {}, Lcom/miui/whetstone/server/WhetstoneActivityManagerService;->getSingletonService()Lcom/miui/whetstone/server/WhetstoneActivityManagerService;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 466
    invoke-static {}, Lcom/miui/whetstone/server/WhetstoneActivityManagerService;->getSingletonService()Lcom/miui/whetstone/server/WhetstoneActivityManagerService;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Lcom/miui/whetstone/server/WhetstoneActivityManagerService;->notifyEvent(ILandroid/os/Bundle;)V

    .line 468
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "notifyEvent for resId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " used "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    sub-long/2addr v3, v0

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "BatteryStatsManagerStub"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 469
    return-void
.end method

.method public notifyWakeUp(Ljava/lang/String;)V
    .locals 3
    .param p1, "reason"    # Ljava/lang/String;

    .line 89
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 90
    .local v0, "b":Landroid/os/Bundle;
    const-string v1, "reason"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    invoke-static {}, Lcom/miui/whetstone/PowerKeeperPolicy;->getInstance()Lcom/miui/whetstone/PowerKeeperPolicy;

    move-result-object v1

    const/16 v2, 0xc

    invoke-virtual {v1, v2, v0}, Lcom/miui/whetstone/PowerKeeperPolicy;->notifyEvent(ILandroid/os/Bundle;)V

    .line 92
    return-void
.end method

.method public readFromParcelLocked(Landroid/os/Parcel;Landroid/os/BatteryStats;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;
    .param p2, "batteryStats"    # Landroid/os/BatteryStats;

    .line 174
    move-object v0, p2

    check-cast v0, Lcom/android/server/power/stats/BatteryStatsImpl;

    invoke-direct {p0, p1, v0}, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->readSoleCalculateApp(Landroid/os/Parcel;Lcom/android/server/power/stats/BatteryStatsImpl;)V

    .line 175
    return-void
.end method

.method public readSummaryFromParcel(Landroid/os/Parcel;Landroid/os/BatteryStats;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;
    .param p2, "batteryStats"    # Landroid/os/BatteryStats;

    .line 164
    move-object v0, p2

    check-cast v0, Lcom/android/server/power/stats/BatteryStatsImpl;

    invoke-direct {p0, p1, v0}, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->readSoleCalculateApp(Landroid/os/Parcel;Lcom/android/server/power/stats/BatteryStatsImpl;)V

    .line 165
    return-void
.end method

.method public reportActiveEvent(Landroid/os/Handler;II)V
    .locals 1
    .param p1, "handler"    # Landroid/os/Handler;
    .param p2, "what"    # I
    .param p3, "uid"    # I

    .line 121
    invoke-virtual {p1, p2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 122
    .local v0, "m":Landroid/os/Message;
    iput p3, v0, Landroid/os/Message;->arg1:I

    .line 123
    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 124
    return-void
.end method

.method public resetAllStatsLocked(JJ)V
    .locals 7
    .param p1, "uptimeMillis"    # J
    .param p3, "elapsedRealtimeMillis"    # J

    .line 184
    iget-object v0, p0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mCalculateAppMap:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 185
    :cond_0
    const-wide/16 v0, 0x3e8

    mul-long/2addr v0, p3

    .line 186
    .local v0, "elapsedRealtimeUs":J
    const/4 v2, 0x0

    .line 187
    .local v2, "currentApp":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v4, p0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mCalculateAppMap:Landroid/util/SparseArray;

    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v4

    if-ge v3, v4, :cond_2

    .line 188
    iget-object v4, p0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mCalculateAppMap:Landroid/util/SparseArray;

    invoke-virtual {v4, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;

    .line 189
    .local v4, "app":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;
    iget v5, v4, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->uid:I

    iget v6, p0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mCurrentSoleUid:I

    if-ne v5, v6, :cond_1

    .line 190
    move-object v2, v4

    .line 192
    :cond_1
    invoke-virtual {v4, v0, v1}, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->reset(J)V

    .line 187
    .end local v4    # "app":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 194
    .end local v3    # "i":I
    :cond_2
    iget-object v3, p0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mCalculateAppMap:Landroid/util/SparseArray;

    monitor-enter v3

    .line 195
    :try_start_0
    iget-object v4, p0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mCalculateAppMap:Landroid/util/SparseArray;

    invoke-virtual {v4}, Landroid/util/SparseArray;->clear()V

    .line 196
    iget v4, p0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mCurrentSoleUid:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_3

    if-eqz v2, :cond_3

    iget v4, v2, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->oldBin:I

    if-le v4, v5, :cond_3

    .line 197
    iget-object v4, p0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mCalculateAppMap:Landroid/util/SparseArray;

    iget v5, p0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mCurrentSoleUid:I

    invoke-virtual {v4, v5, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 198
    iget-object v4, v2, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->screenBrightnessTimers:[Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;

    iget v5, v2, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->oldBin:I

    aget-object v4, v4, v5

    invoke-virtual {v4, p3, p4}, Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;->startRunningLocked(J)V

    .line 200
    :cond_3
    monitor-exit v3

    .line 201
    return-void

    .line 200
    :catchall_0
    move-exception v4

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4
.end method

.method public setActiveCallback(Lcom/android/server/power/stats/BatteryStatsManagerStub$ActiveCallback;)V
    .locals 0
    .param p1, "callback"    # Lcom/android/server/power/stats/BatteryStatsManagerStub$ActiveCallback;

    .line 65
    iput-object p1, p0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mActiveCallback:Lcom/android/server/power/stats/BatteryStatsManagerStub$ActiveCallback;

    .line 66
    return-void
.end method

.method public writeSummaryToParcel(Landroid/os/Parcel;J)V
    .locals 0
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "elapsedRealtimeUs"    # J

    .line 169
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->writeSoleCalculateApp(Landroid/os/Parcel;J)V

    .line 170
    return-void
.end method

.method public writeToParcelLocked(Landroid/os/Parcel;J)V
    .locals 0
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "elapsedRealtimeUs"    # J

    .line 179
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->writeSoleCalculateApp(Landroid/os/Parcel;J)V

    .line 180
    return-void
.end method

.method public writeUidStatsToParcel(Landroid/os/BatteryStats;Landroid/os/Parcel;J)V
    .locals 4
    .param p1, "batteryStats"    # Landroid/os/BatteryStats;
    .param p2, "out"    # Landroid/os/Parcel;
    .param p3, "elapsedRealtimeUs"    # J

    .line 151
    invoke-virtual {p1}, Landroid/os/BatteryStats;->getUidStats()Landroid/util/SparseArray;

    move-result-object v0

    .line 152
    .local v0, "mUidStats":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/android/server/power/stats/BatteryStatsImpl$Uid;>;"
    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v1

    .line 153
    .local v1, "size":I
    invoke-virtual {p2, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 154
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 155
    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v3

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 156
    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/power/stats/BatteryStatsImpl$Uid;

    .line 158
    .local v3, "uid":Lcom/android/server/power/stats/BatteryStatsImpl$Uid;
    invoke-virtual {v3, p2, p3, p4}, Lcom/android/server/power/stats/BatteryStatsImpl$Uid;->writeToParcelLocked(Landroid/os/Parcel;J)V

    .line 154
    .end local v3    # "uid":Lcom/android/server/power/stats/BatteryStatsImpl$Uid;
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 160
    .end local v2    # "i":I
    :cond_0
    return-void
.end method
