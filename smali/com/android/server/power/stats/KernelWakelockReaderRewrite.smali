.class public Lcom/android/server/power/stats/KernelWakelockReaderRewrite;
.super Ljava/lang/Object;
.source "KernelWakelockReaderRewrite.java"


# static fields
.field private static final PROC_WAKELOCKS_FORMAT:[I

.field private static final TAG:Ljava/lang/String; = "KernelWakelockReader"

.field private static final WAKEUP_SOURCES_FORMAT:[I

.field private static sKernelWakelockUpdateVersion:I = 0x0

.field private static final sWakelockFile:Ljava/lang/String; = "/proc/wakelocks"

.field private static final sWakeupSourceFile:Ljava/lang/String; = "/d/wakeup_sources"


# instance fields
.field br_flag:Z

.field private final mProcWakelocksData:[J

.field private final mProcWakelocksName:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 18
    const/4 v0, 0x0

    sput v0, Lcom/android/server/power/stats/KernelWakelockReaderRewrite;->sKernelWakelockUpdateVersion:I

    .line 22
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/server/power/stats/KernelWakelockReaderRewrite;->PROC_WAKELOCKS_FORMAT:[I

    .line 32
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/server/power/stats/KernelWakelockReaderRewrite;->WAKEUP_SOURCES_FORMAT:[I

    return-void

    :array_0
    .array-data 4
        0x1409
        0x2009
        0x9
        0x9
        0x9
        0x2009
    .end array-data

    :array_1
    .array-data 4
        0x1009
        0x2109
        0x109
        0x109
        0x109
        0x109
        0x2109
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    iput-object v1, p0, Lcom/android/server/power/stats/KernelWakelockReaderRewrite;->mProcWakelocksName:[Ljava/lang/String;

    .line 45
    new-array v0, v0, [J

    iput-object v0, p0, Lcom/android/server/power/stats/KernelWakelockReaderRewrite;->mProcWakelocksData:[J

    .line 53
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/power/stats/KernelWakelockReaderRewrite;->br_flag:Z

    return-void
.end method

.method private readInfoCheck([BIIJ)I
    .locals 5
    .param p1, "buffer"    # [B
    .param p2, "len"    # I
    .param p3, "cnt"    # I
    .param p4, "startTime"    # J

    .line 56
    if-ltz p3, :cond_0

    array-length v0, p1

    sub-int/2addr v0, p2

    if-lez v0, :cond_1

    .line 57
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/power/stats/KernelWakelockReaderRewrite;->br_flag:Z

    .line 59
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, p4

    .line 60
    .local v0, "readTime":J
    const-wide/16 v2, 0x64

    cmp-long v2, v0, v2

    const-string v3, "KernelWakelockReader"

    if-lez v2, :cond_2

    .line 61
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Reading wakelock stats took "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "ms"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    :cond_2
    array-length v2, p1

    if-lt p2, v2, :cond_3

    .line 65
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Kernel wake locks exceeded buffer size "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    :cond_3
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, p2, :cond_5

    .line 68
    aget-byte v3, p1, v2

    if-nez v3, :cond_4

    .line 69
    move p2, v2

    .line 70
    goto :goto_1

    .line 67
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 73
    .end local v2    # "i":I
    :cond_5
    :goto_1
    return p2
.end method


# virtual methods
.method public parseProcWakelocks([BIZLcom/android/server/power/stats/KernelWakelockStats;)Lcom/android/server/power/stats/KernelWakelockStats;
    .locals 21
    .param p1, "wlBuffer"    # [B
    .param p2, "len"    # I
    .param p3, "wakeup_sources"    # Z
    .param p4, "staleStats"    # Lcom/android/server/power/stats/KernelWakelockStats;

    .line 147
    move-object/from16 v1, p0

    move-object/from16 v9, p1

    move/from16 v10, p2

    move-object/from16 v11, p4

    const/4 v0, 0x0

    move v12, v0

    .local v12, "i":I
    :goto_0
    const/16 v13, 0xa

    if-ge v12, v10, :cond_0

    aget-byte v0, v9, v12

    if-eq v0, v13, :cond_0

    aget-byte v0, v9, v12

    if-eqz v0, :cond_0

    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    .line 148
    :cond_0
    add-int/lit8 v0, v12, 0x1

    move v2, v0

    .local v2, "endIndex":I
    move v3, v0

    .line 150
    .local v3, "startIndex":I
    monitor-enter p0

    .line 151
    :try_start_0
    sget v0, Lcom/android/server/power/stats/KernelWakelockReaderRewrite;->sKernelWakelockUpdateVersion:I

    const/4 v14, 0x1

    add-int/2addr v0, v14

    sput v0, Lcom/android/server/power/stats/KernelWakelockReaderRewrite;->sKernelWakelockUpdateVersion:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_4

    move v15, v3

    .line 152
    .end local v3    # "startIndex":I
    .local v15, "startIndex":I
    :goto_1
    if-ge v2, v10, :cond_b

    .line 153
    move v0, v15

    move v8, v0

    .line 154
    .end local v2    # "endIndex":I
    .local v8, "endIndex":I
    :goto_2
    if-ge v8, v10, :cond_1

    :try_start_1
    aget-byte v0, v9, v8

    if-eq v0, v13, :cond_1

    aget-byte v0, v9, v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_1

    .line 155
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 222
    :catchall_0
    move-exception v0

    move v2, v8

    move v3, v15

    goto/16 :goto_a

    .line 158
    :cond_1
    add-int/lit8 v0, v10, -0x1

    if-le v8, v0, :cond_2

    .line 159
    move v2, v8

    goto/16 :goto_8

    .line 162
    :cond_2
    :try_start_2
    iget-object v0, v1, Lcom/android/server/power/stats/KernelWakelockReaderRewrite;->mProcWakelocksName:[Ljava/lang/String;

    move-object/from16 v16, v0

    .line 163
    .local v16, "nameStringArray":[Ljava/lang/String;
    iget-object v0, v1, Lcom/android/server/power/stats/KernelWakelockReaderRewrite;->mProcWakelocksData:[J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-object/from16 v17, v0

    .line 167
    .local v17, "wlData":[J
    move v0, v15

    .local v0, "j":I
    :goto_3
    if-ge v0, v8, :cond_4

    .line 168
    :try_start_3
    aget-byte v2, v9, v0

    and-int/lit16 v2, v2, 0x80

    if-eqz v2, :cond_3

    const/16 v2, 0x3f

    aput-byte v2, v9, v0

    .line 167
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 170
    .end local v0    # "j":I
    :cond_4
    nop

    .line 171
    if-eqz p3, :cond_5

    sget-object v0, Lcom/android/server/power/stats/KernelWakelockReaderRewrite;->WAKEUP_SOURCES_FORMAT:[I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-object v5, v0

    goto :goto_4

    .line 172
    :cond_5
    :try_start_4
    sget-object v0, Lcom/android/server/power/stats/KernelWakelockReaderRewrite;->PROC_WAKELOCKS_FORMAT:[I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move-object v5, v0

    :goto_4
    const/4 v0, 0x0

    .line 170
    move-object/from16 v2, p1

    move v3, v15

    move v4, v8

    move-object/from16 v6, v16

    move-object/from16 v7, v17

    move/from16 v18, v8

    .end local v8    # "endIndex":I
    .local v18, "endIndex":I
    move-object v8, v0

    :try_start_5
    invoke-static/range {v2 .. v8}, Landroid/os/Process;->parseProcLine([BII[I[Ljava/lang/String;[J[F)Z

    move-result v0

    move v2, v0

    .line 175
    .local v2, "parsed":Z
    const/4 v0, 0x0

    aget-object v0, v16, v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 176
    .local v3, "name":Ljava/lang/String;
    aget-wide v4, v17, v14

    long-to-int v4, v4

    .line 178
    .local v4, "count":I
    const-wide/16 v5, 0x3e8

    const/4 v0, 0x2

    if-eqz p3, :cond_6

    .line 180
    aget-wide v7, v17, v0

    mul-long/2addr v7, v5

    .local v7, "totalTime":J
    goto :goto_5

    .line 183
    .end local v7    # "totalTime":J
    :cond_6
    aget-wide v7, v17, v0

    const-wide/16 v19, 0x1f4

    add-long v7, v7, v19

    div-long/2addr v7, v5

    .line 186
    .restart local v7    # "totalTime":J
    :goto_5
    if-eqz v2, :cond_9

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_9

    .line 187
    invoke-virtual {v11, v3}, Lcom/android/server/power/stats/KernelWakelockStats;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 188
    new-instance v0, Lcom/android/server/power/stats/KernelWakelockStats$Entry;

    sget v5, Lcom/android/server/power/stats/KernelWakelockReaderRewrite;->sKernelWakelockUpdateVersion:I

    invoke-direct {v0, v4, v7, v8, v5}, Lcom/android/server/power/stats/KernelWakelockStats$Entry;-><init>(IJI)V

    invoke-virtual {v11, v3, v0}, Lcom/android/server/power/stats/KernelWakelockStats;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_7

    .line 191
    :cond_7
    invoke-virtual {v11, v3}, Lcom/android/server/power/stats/KernelWakelockStats;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/power/stats/KernelWakelockStats$Entry;

    .line 192
    .local v0, "kwlStats":Lcom/android/server/power/stats/KernelWakelockStats$Entry;
    iget v5, v0, Lcom/android/server/power/stats/KernelWakelockStats$Entry;->mVersion:I

    sget v6, Lcom/android/server/power/stats/KernelWakelockReaderRewrite;->sKernelWakelockUpdateVersion:I

    if-ne v5, v6, :cond_8

    .line 193
    iget v5, v0, Lcom/android/server/power/stats/KernelWakelockStats$Entry;->mCount:I

    add-int/2addr v5, v4

    iput v5, v0, Lcom/android/server/power/stats/KernelWakelockStats$Entry;->mCount:I

    .line 194
    iget-wide v5, v0, Lcom/android/server/power/stats/KernelWakelockStats$Entry;->mTotalTime:J

    add-long/2addr v5, v7

    iput-wide v5, v0, Lcom/android/server/power/stats/KernelWakelockStats$Entry;->mTotalTime:J

    goto :goto_6

    .line 196
    :cond_8
    iput v4, v0, Lcom/android/server/power/stats/KernelWakelockStats$Entry;->mCount:I

    .line 197
    iput-wide v7, v0, Lcom/android/server/power/stats/KernelWakelockStats$Entry;->mTotalTime:J

    .line 198
    sget v5, Lcom/android/server/power/stats/KernelWakelockReaderRewrite;->sKernelWakelockUpdateVersion:I

    iput v5, v0, Lcom/android/server/power/stats/KernelWakelockStats$Entry;->mVersion:I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 200
    .end local v0    # "kwlStats":Lcom/android/server/power/stats/KernelWakelockStats$Entry;
    :goto_6
    goto :goto_7

    .line 201
    :cond_9
    if-nez v2, :cond_a

    .line 203
    :try_start_6
    const-string v0, "KernelWakelockReader"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to parse proc line: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    new-instance v6, Ljava/lang/String;

    sub-int v13, v18, v15

    invoke-direct {v6, v9, v15, v13}, Ljava/lang/String;-><init>([BII)V

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Slog;->wtf(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 207
    goto :goto_7

    .line 205
    :catch_0
    move-exception v0

    .line 206
    .local v0, "e":Ljava/lang/Exception;
    :try_start_7
    const-string v5, "KernelWakelockReader"

    const-string v6, "Failed to parse proc line!"

    invoke-static {v5, v6}, Landroid/util/Slog;->wtf(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 209
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_a
    :goto_7
    add-int/lit8 v15, v18, 0x1

    .line 210
    .end local v2    # "parsed":Z
    .end local v16    # "nameStringArray":[Ljava/lang/String;
    .end local v17    # "wlData":[J
    move/from16 v2, v18

    const/16 v13, 0xa

    goto/16 :goto_1

    .line 222
    .end local v3    # "name":Ljava/lang/String;
    .end local v4    # "count":I
    .end local v7    # "totalTime":J
    :catchall_1
    move-exception v0

    move v3, v15

    move/from16 v2, v18

    goto :goto_a

    .end local v18    # "endIndex":I
    .restart local v8    # "endIndex":I
    :catchall_2
    move-exception v0

    move/from16 v18, v8

    move v3, v15

    move/from16 v2, v18

    .end local v8    # "endIndex":I
    .restart local v18    # "endIndex":I
    goto :goto_a

    .line 213
    .end local v18    # "endIndex":I
    .local v2, "endIndex":I
    :cond_b
    :goto_8
    :try_start_8
    invoke-virtual/range {p4 .. p4}, Lcom/android/server/power/stats/KernelWakelockStats;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 214
    .local v0, "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/power/stats/KernelWakelockStats$Entry;>;"
    :cond_c
    :goto_9
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_d

    .line 215
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/power/stats/KernelWakelockStats$Entry;

    iget v3, v3, Lcom/android/server/power/stats/KernelWakelockStats$Entry;->mVersion:I

    sget v4, Lcom/android/server/power/stats/KernelWakelockReaderRewrite;->sKernelWakelockUpdateVersion:I

    if-eq v3, v4, :cond_c

    .line 216
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_9

    .line 220
    :cond_d
    sget v3, Lcom/android/server/power/stats/KernelWakelockReaderRewrite;->sKernelWakelockUpdateVersion:I

    iput v3, v11, Lcom/android/server/power/stats/KernelWakelockStats;->kernelWakelockVersion:I

    .line 221
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    return-object v11

    .line 222
    .end local v0    # "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/power/stats/KernelWakelockStats$Entry;>;"
    :catchall_3
    move-exception v0

    move v3, v15

    goto :goto_a

    .end local v15    # "startIndex":I
    .local v3, "startIndex":I
    :catchall_4
    move-exception v0

    :goto_a
    :try_start_9
    monitor-exit p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    throw v0
.end method

.method public final readKernelWakelockStats(Lcom/android/server/power/stats/KernelWakelockStats;)Lcom/android/server/power/stats/KernelWakelockStats;
    .locals 17
    .param p1, "staleStats"    # Lcom/android/server/power/stats/KernelWakelockStats;

    .line 77
    move-object/from16 v7, p0

    move-object/from16 v8, p1

    const-string v9, "failed to read kernel wakelocks"

    const-string v10, "KernelWakelockReader"

    const v0, 0x8000

    new-array v11, v0, [B

    .line 80
    .local v11, "buffer":[B
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskReadsMask()I

    move-result v12

    .line 81
    .local v12, "oldMask":I
    const/4 v1, 0x0

    .line 83
    .local v1, "is":Ljava/io/FileInputStream;
    const/4 v13, 0x0

    :try_start_0
    new-instance v0, Ljava/io/FileInputStream;

    const-string v2, "/proc/wakelocks"

    invoke-direct {v0, v2}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    .end local v1    # "is":Ljava/io/FileInputStream;
    .local v0, "is":Ljava/io/FileInputStream;
    const/4 v1, 0x0

    .line 94
    .local v1, "wakeup_sources":Z
    move-object v14, v0

    move v15, v1

    goto :goto_0

    .line 85
    .end local v0    # "is":Ljava/io/FileInputStream;
    .local v1, "is":Ljava/io/FileInputStream;
    :catch_0
    move-exception v0

    move-object v2, v0

    .line 87
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    new-instance v0, Ljava/io/FileInputStream;

    const-string v3, "/d/wakeup_sources"

    invoke-direct {v0, v3}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5

    .line 88
    .end local v1    # "is":Ljava/io/FileInputStream;
    .restart local v0    # "is":Ljava/io/FileInputStream;
    const/4 v1, 0x1

    .line 93
    .local v1, "wakeup_sources":Z
    move-object v14, v0

    move v15, v1

    .line 97
    .end local v0    # "is":Ljava/io/FileInputStream;
    .end local v1    # "wakeup_sources":Z
    .end local v2    # "e":Ljava/lang/Exception;
    .local v14, "is":Ljava/io/FileInputStream;
    .local v15, "wakeup_sources":Z
    :goto_0
    const/4 v0, 0x0

    .line 98
    .local v0, "cnt":I
    const/4 v1, 0x0

    .line 99
    .local v1, "len":I
    :try_start_2
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v5

    .line 101
    .local v5, "startTime":J
    const/4 v2, 0x0

    iput-boolean v2, v7, Lcom/android/server/power/stats/KernelWakelockReaderRewrite;->br_flag:Z

    move v4, v1

    .line 102
    .end local v1    # "len":I
    .local v4, "len":I
    :goto_1
    array-length v1, v11

    sub-int/2addr v1, v4

    invoke-virtual {v14, v11, v4, v1}, Ljava/io/FileInputStream;->read([BII)I

    move-result v1

    move v0, v1

    if-lez v1, :cond_0

    .line 103
    add-int/2addr v4, v0

    .line 104
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "is.read return cnt = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v10, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 106
    :cond_0
    if-nez v4, :cond_1

    .line 107
    invoke-virtual {v14}, Ljava/io/FileInputStream;->close()V

    .line 108
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "is.read return cnt is zero ? "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v10, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    goto :goto_2

    .line 111
    :cond_1
    move-object/from16 v1, p0

    move-object v2, v11

    move v3, v4

    move/from16 v16, v4

    .end local v4    # "len":I
    .local v16, "len":I
    move v4, v0

    invoke-direct/range {v1 .. v6}, Lcom/android/server/power/stats/KernelWakelockReaderRewrite;->readInfoCheck([BIIJ)I

    move-result v1

    .line 112
    .end local v16    # "len":I
    .restart local v1    # "len":I
    invoke-virtual {v7, v11, v1, v15, v8}, Lcom/android/server/power/stats/KernelWakelockReaderRewrite;->parseProcWakelocks([BIZLcom/android/server/power/stats/KernelWakelockStats;)Lcom/android/server/power/stats/KernelWakelockStats;

    .line 113
    iget-boolean v2, v7, Lcom/android/server/power/stats/KernelWakelockReaderRewrite;->br_flag:Z

    if-eqz v2, :cond_2

    .line 114
    invoke-virtual {v14}, Ljava/io/FileInputStream;->close()V

    .line 115
    const-string v2, "read is at the end of file "

    invoke-static {v10, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 116
    nop

    .line 119
    .end local v0    # "cnt":I
    .end local v1    # "len":I
    .end local v5    # "startTime":J
    :goto_2
    nop

    .line 124
    invoke-static {v12}, Landroid/os/StrictMode;->setThreadPolicyMask(I)V

    .line 126
    :try_start_3
    invoke-virtual {v14}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 129
    goto :goto_3

    .line 127
    :catch_1
    move-exception v0

    move-object v1, v0

    move-object v0, v1

    .line 128
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v10, v9, v0}, Landroid/util/Slog;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 119
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_3
    return-object v8

    .line 118
    :cond_2
    goto :goto_0

    .line 124
    :catchall_0
    move-exception v0

    move-object v1, v0

    goto :goto_5

    .line 120
    :catch_2
    move-exception v0

    move-object v1, v0

    .line 121
    .local v1, "e":Ljava/io/IOException;
    :try_start_4
    invoke-static {v10, v9, v1}, Landroid/util/Slog;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 122
    nop

    .line 124
    invoke-static {v12}, Landroid/os/StrictMode;->setThreadPolicyMask(I)V

    .line 126
    :try_start_5
    invoke-virtual {v14}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    .line 129
    goto :goto_4

    .line 127
    :catch_3
    move-exception v0

    move-object v2, v0

    move-object v0, v2

    .line 128
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-static {v10, v9, v0}, Landroid/util/Slog;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 122
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_4
    return-object v13

    .line 124
    .end local v1    # "e":Ljava/io/IOException;
    :goto_5
    invoke-static {v12}, Landroid/os/StrictMode;->setThreadPolicyMask(I)V

    .line 126
    :try_start_6
    invoke-virtual {v14}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4

    .line 129
    goto :goto_6

    .line 127
    :catch_4
    move-exception v0

    move-object v2, v0

    move-object v0, v2

    .line 128
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-static {v10, v9, v0}, Landroid/util/Slog;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 130
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_6
    throw v1

    .line 89
    .end local v14    # "is":Ljava/io/FileInputStream;
    .end local v15    # "wakeup_sources":Z
    .local v1, "is":Ljava/io/FileInputStream;
    .restart local v2    # "e":Ljava/lang/Exception;
    :catch_5
    move-exception v0

    .line 90
    .local v0, "e2":Ljava/lang/Exception;
    const-string v3, "neither /proc/wakelocks nor /d/wakeup_sources exists"

    invoke-static {v10, v3}, Landroid/util/Slog;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    return-object v13
.end method
