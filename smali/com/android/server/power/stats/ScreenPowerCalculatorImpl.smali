.class public Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;
.super Lcom/android/server/power/stats/PowerCalculator;
.source "ScreenPowerCalculatorImpl.java"

# interfaces
.implements Lcom/android/server/power/stats/ScreenPowerCalculatorStub;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$PowerAndDuration;,
        Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$UidTimeStats;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field public static final MIN_ACTIVE_TIME_FOR_SMEARING:J = 0x927c0L

.field private static final POWER_DY_SCREEN_FULL:Ljava/lang/String; = "dy.screen.full"

.field private static final POWER_DY_SCREEN_HBM:Ljava/lang/String; = "dy.screen.hbm"

.field private static final POWER_DY_SCREEN_LOW:Ljava/lang/String; = "dy.screen.low"

.field private static final TAG:Ljava/lang/String; = "ScreenPowerCalculatorStub"


# instance fields
.field private mAppDurationMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$UidTimeStats;",
            ">;"
        }
    .end annotation
.end field

.field private mAppScreenFullPowerEstimators:[Lcom/android/server/power/stats/UsageBasedPowerEstimator;

.field private mAppScreenHBMPowerEstimators:[Lcom/android/server/power/stats/UsageBasedPowerEstimator;

.field private mAppScreenLowPowerEstimators:[Lcom/android/server/power/stats/UsageBasedPowerEstimator;

.field private mHasDyPowerController:Z

.field private mScreenFullPowerEstimators:[Lcom/android/server/power/stats/UsageBasedPowerEstimator;

.field private mScreenOnPowerEstimators:[Lcom/android/server/power/stats/UsageBasedPowerEstimator;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 57
    invoke-direct {p0}, Lcom/android/server/power/stats/PowerCalculator;-><init>()V

    .line 58
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->mAppDurationMap:Landroid/util/SparseArray;

    .line 59
    return-void
.end method

.method private calculateAppUsingMeasuredEnergy(Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$PowerAndDuration;Landroid/os/BatteryStats$Uid;J)V
    .locals 4
    .param p1, "appPowerAndDuration"    # Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$PowerAndDuration;
    .param p2, "u"    # Landroid/os/BatteryStats$Uid;
    .param p3, "rawRealtimeUs"    # J

    .line 191
    invoke-virtual {p0, p2, p3, p4}, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->getProcessForegroundTimeMs(Landroid/os/BatteryStats$Uid;J)J

    move-result-wide v0

    iput-wide v0, p1, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$PowerAndDuration;->durationMs:J

    .line 193
    invoke-virtual {p2}, Landroid/os/BatteryStats$Uid;->getScreenOnEnergyConsumptionUC()J

    move-result-wide v0

    .line 194
    .local v0, "chargeUC":J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 195
    const-string v2, "ScreenPowerCalculatorStub"

    const-string v3, "Screen energy not supported, so calculateApp shouldn\'t de called"

    invoke-static {v2, v3}, Landroid/util/Slog;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    const-wide/16 v2, 0x0

    iput-wide v2, p1, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$PowerAndDuration;->powerMah:D

    .line 197
    return-void

    .line 200
    :cond_0
    invoke-static {v0, v1}, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->uCtoMah(J)D

    move-result-wide v2

    iput-wide v2, p1, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$PowerAndDuration;->powerMah:D

    .line 201
    return-void
.end method

.method private calculateAppsBinPower(I[Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;J)D
    .locals 9
    .param p1, "display"    # I
    .param p2, "binTime"    # [Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;
    .param p3, "rawRealtimeUs"    # J

    .line 272
    const-wide/16 v0, 0x0

    .line 273
    .local v0, "power":D
    const/4 v2, 0x0

    .local v2, "bin":I
    :goto_0
    const/16 v3, 0x64

    if-ge v2, v3, :cond_1

    .line 274
    aget-object v3, p2, v2

    .line 275
    const/4 v4, 0x0

    invoke-virtual {v3, p3, p4, v4}, Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;->getTotalTimeLocked(JI)J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    div-long/2addr v3, v5

    .line 277
    .local v3, "brightnessTime":J
    const/16 v5, 0x32

    if-gt v2, v5, :cond_0

    .line 278
    iget-object v5, p0, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->mAppScreenFullPowerEstimators:[Lcom/android/server/power/stats/UsageBasedPowerEstimator;

    aget-object v5, v5, p1

    invoke-virtual {v5, v3, v4}, Lcom/android/server/power/stats/UsageBasedPowerEstimator;->calculatePower(J)D

    move-result-wide v5

    iget-object v7, p0, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->mAppScreenLowPowerEstimators:[Lcom/android/server/power/stats/UsageBasedPowerEstimator;

    aget-object v7, v7, p1

    .line 279
    invoke-virtual {v7, v3, v4}, Lcom/android/server/power/stats/UsageBasedPowerEstimator;->calculatePower(J)D

    move-result-wide v7

    sub-double/2addr v5, v7

    int-to-double v7, v2

    mul-double/2addr v5, v7

    const-wide/high16 v7, 0x4049000000000000L    # 50.0

    div-double/2addr v5, v7

    iget-object v7, p0, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->mAppScreenLowPowerEstimators:[Lcom/android/server/power/stats/UsageBasedPowerEstimator;

    aget-object v7, v7, p1

    .line 281
    invoke-virtual {v7, v3, v4}, Lcom/android/server/power/stats/UsageBasedPowerEstimator;->calculatePower(J)D

    move-result-wide v7

    add-double/2addr v5, v7

    .local v5, "binPowerMah":D
    goto :goto_1

    .line 283
    .end local v5    # "binPowerMah":D
    :cond_0
    iget-object v5, p0, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->mAppScreenHBMPowerEstimators:[Lcom/android/server/power/stats/UsageBasedPowerEstimator;

    aget-object v5, v5, p1

    invoke-virtual {v5, v3, v4}, Lcom/android/server/power/stats/UsageBasedPowerEstimator;->calculatePower(J)D

    move-result-wide v5

    iget-object v7, p0, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->mAppScreenFullPowerEstimators:[Lcom/android/server/power/stats/UsageBasedPowerEstimator;

    aget-object v7, v7, p1

    .line 284
    invoke-virtual {v7, v3, v4}, Lcom/android/server/power/stats/UsageBasedPowerEstimator;->calculatePower(J)D

    move-result-wide v7

    sub-double/2addr v5, v7

    add-int/lit8 v7, v2, -0x32

    int-to-double v7, v7

    mul-double/2addr v5, v7

    const-wide v7, 0x4048800000000000L    # 49.0

    div-double/2addr v5, v7

    iget-object v7, p0, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->mAppScreenFullPowerEstimators:[Lcom/android/server/power/stats/UsageBasedPowerEstimator;

    aget-object v7, v7, p1

    .line 286
    invoke-virtual {v7, v3, v4}, Lcom/android/server/power/stats/UsageBasedPowerEstimator;->calculatePower(J)D

    move-result-wide v7

    add-double/2addr v5, v7

    .line 292
    .restart local v5    # "binPowerMah":D
    :goto_1
    add-double/2addr v0, v5

    .line 273
    .end local v3    # "brightnessTime":J
    .end local v5    # "binPowerMah":D
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 294
    .end local v2    # "bin":I
    :cond_1
    return-wide v0
.end method

.method private calculateDuration(Landroid/os/BatteryStats;JI)J
    .locals 4
    .param p1, "batteryStats"    # Landroid/os/BatteryStats;
    .param p2, "rawRealtimeUs"    # J
    .param p4, "statsType"    # I

    .line 204
    invoke-virtual {p1, p2, p3, p4}, Landroid/os/BatteryStats;->getScreenOnTime(JI)J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    return-wide v0
.end method

.method private calculateOthersPowerFromBrightness(Landroid/os/BatteryStats;J)D
    .locals 21
    .param p1, "batteryStats"    # Landroid/os/BatteryStats;
    .param p2, "rawRealtimeUs"    # J

    .line 231
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-wide/from16 v2, p2

    iget-object v4, v0, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->mScreenOnPowerEstimators:[Lcom/android/server/power/stats/UsageBasedPowerEstimator;

    array-length v4, v4

    .line 232
    .local v4, "numDisplays":I
    const-wide/16 v5, 0x0

    .line 233
    .local v5, "power":D
    const/4 v7, 0x0

    .local v7, "display":I
    :goto_0
    if-ge v7, v4, :cond_1

    .line 234
    const/4 v8, 0x0

    invoke-virtual {v1, v2, v3, v8}, Landroid/os/BatteryStats;->getScreenOnTime(JI)J

    move-result-wide v9

    const-wide/16 v11, 0x3e8

    div-long/2addr v9, v11

    .line 235
    .local v9, "displayTime":J
    iget-object v13, v0, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->mScreenOnPowerEstimators:[Lcom/android/server/power/stats/UsageBasedPowerEstimator;

    aget-object v13, v13, v7

    invoke-virtual {v13, v9, v10}, Lcom/android/server/power/stats/UsageBasedPowerEstimator;->calculatePower(J)D

    move-result-wide v13

    add-double/2addr v5, v13

    .line 236
    invoke-virtual {v0, v2, v3}, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->getSoleCalculateAppsBinTime(J)[J

    move-result-object v13

    .line 237
    .local v13, "calculateAppTimeUs":[J
    const/4 v14, 0x0

    .local v14, "bin":I
    :goto_1
    const/16 v15, 0x64

    if-ge v14, v15, :cond_0

    .line 238
    invoke-virtual {v1, v14}, Landroid/os/BatteryStats;->getScreenBrightnessTimer(I)Landroid/os/BatteryStats$Timer;

    move-result-object v15

    invoke-virtual {v15, v2, v3, v8}, Landroid/os/BatteryStats$Timer;->getTotalTimeLocked(JI)J

    move-result-wide v15

    aget-wide v17, v13, v14

    sub-long v15, v15, v17

    move-wide/from16 v18, v9

    .end local v9    # "displayTime":J
    .local v18, "displayTime":J
    div-long v8, v15, v11

    .line 240
    .local v8, "brightnessTime":J
    iget-object v10, v0, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->mScreenFullPowerEstimators:[Lcom/android/server/power/stats/UsageBasedPowerEstimator;

    aget-object v10, v10, v7

    invoke-virtual {v10, v8, v9}, Lcom/android/server/power/stats/UsageBasedPowerEstimator;->calculatePower(J)D

    move-result-wide v15

    int-to-float v10, v14

    const/high16 v20, 0x3f000000    # 0.5f

    add-float v10, v10, v20

    float-to-double v11, v10

    mul-double/2addr v15, v11

    const-wide/high16 v10, 0x4059000000000000L    # 100.0

    div-double/2addr v15, v10

    .line 246
    .local v15, "binPowerMah":D
    add-double/2addr v5, v15

    .line 237
    .end local v8    # "brightnessTime":J
    .end local v15    # "binPowerMah":D
    add-int/lit8 v14, v14, 0x1

    move-wide/from16 v9, v18

    const/4 v8, 0x0

    const-wide/16 v11, 0x3e8

    goto :goto_1

    .end local v18    # "displayTime":J
    .restart local v9    # "displayTime":J
    :cond_0
    move-wide/from16 v18, v9

    .line 233
    .end local v9    # "displayTime":J
    .end local v13    # "calculateAppTimeUs":[J
    .end local v14    # "bin":I
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 252
    .end local v7    # "display":I
    :cond_1
    return-wide v5
.end method

.method private calculateSoleAppPowerFromBrightness(Landroid/os/BatteryStats;J)D
    .locals 7
    .param p1, "batteryStats"    # Landroid/os/BatteryStats;
    .param p2, "rawRealtimeUs"    # J

    .line 256
    iget-object v0, p0, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->mScreenOnPowerEstimators:[Lcom/android/server/power/stats/UsageBasedPowerEstimator;

    array-length v0, v0

    .line 257
    .local v0, "numDisplays":I
    const-wide/16 v1, 0x0

    .line 258
    .local v1, "power":D
    const/4 v3, 0x0

    .local v3, "display":I
    :goto_0
    if-ge v3, v0, :cond_3

    .line 259
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    iget-object v5, p0, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->mAppDurationMap:Landroid/util/SparseArray;

    invoke-virtual {v5}, Landroid/util/SparseArray;->size()I

    move-result v5

    if-ge v4, v5, :cond_2

    .line 260
    iget-object v5, p0, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->mAppDurationMap:Landroid/util/SparseArray;

    invoke-virtual {v5, v4}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$UidTimeStats;

    .line 261
    .local v5, "uidTimeStats":Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$UidTimeStats;
    if-eqz v5, :cond_1

    iget-object v6, v5, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$UidTimeStats;->screenBrightnessTimers:[Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;

    if-nez v6, :cond_0

    goto :goto_2

    .line 262
    :cond_0
    iget-object v6, v5, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$UidTimeStats;->screenBrightnessTimers:[Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;

    invoke-direct {p0, v3, v6, p2, p3}, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->calculateAppsBinPower(I[Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;J)D

    move-result-wide v1

    .line 259
    .end local v5    # "uidTimeStats":Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$UidTimeStats;
    :cond_1
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 258
    .end local v4    # "i":I
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 268
    .end local v3    # "display":I
    :cond_3
    return-wide v1
.end method

.method private calculateTotalDurationAndPower(Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$PowerAndDuration;ILandroid/os/BatteryStats;JIJ)V
    .locals 4
    .param p1, "totalPowerAndDuration"    # Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$PowerAndDuration;
    .param p2, "powerModel"    # I
    .param p3, "batteryStats"    # Landroid/os/BatteryStats;
    .param p4, "rawRealtimeUs"    # J
    .param p6, "statsType"    # I
    .param p7, "consumptionUC"    # J

    .line 170
    invoke-direct {p0, p3, p4, p5, p6}, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->calculateDuration(Landroid/os/BatteryStats;JI)J

    move-result-wide v0

    iput-wide v0, p1, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$PowerAndDuration;->durationMs:J

    .line 173
    packed-switch p2, :pswitch_data_0

    .line 179
    iget-boolean v0, p0, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->mHasDyPowerController:Z

    if-eqz v0, :cond_0

    .line 180
    invoke-direct {p0, p3, p4, p5}, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->calculateOthersPowerFromBrightness(Landroid/os/BatteryStats;J)D

    move-result-wide v0

    .line 181
    invoke-direct {p0, p3, p4, p5}, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->calculateSoleAppPowerFromBrightness(Landroid/os/BatteryStats;J)D

    move-result-wide v2

    add-double/2addr v0, v2

    iput-wide v0, p1, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$PowerAndDuration;->powerMah:D

    goto :goto_0

    .line 175
    :pswitch_0
    invoke-static {p7, p8}, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->uCtoMah(J)D

    move-result-wide v0

    iput-wide v0, p1, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$PowerAndDuration;->powerMah:D

    .line 176
    goto :goto_0

    .line 183
    :cond_0
    invoke-direct {p0, p3, p4, p5}, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->calculateTotalPowerFromBrightness(Landroid/os/BatteryStats;J)D

    move-result-wide v0

    iput-wide v0, p1, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$PowerAndDuration;->powerMah:D

    .line 187
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method private calculateTotalPowerFromBrightness(Landroid/os/BatteryStats;J)D
    .locals 17
    .param p1, "batteryStats"    # Landroid/os/BatteryStats;
    .param p2, "rawRealtimeUs"    # J

    .line 209
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-wide/from16 v2, p2

    iget-object v4, v0, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->mScreenOnPowerEstimators:[Lcom/android/server/power/stats/UsageBasedPowerEstimator;

    array-length v4, v4

    .line 210
    .local v4, "numDisplays":I
    const-wide/16 v5, 0x0

    .line 211
    .local v5, "power":D
    const/4 v7, 0x0

    .local v7, "display":I
    :goto_0
    if-ge v7, v4, :cond_1

    .line 212
    invoke-virtual {v1, v7, v2, v3}, Landroid/os/BatteryStats;->getDisplayScreenOnTime(IJ)J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    div-long/2addr v8, v10

    .line 214
    .local v8, "displayTime":J
    iget-object v12, v0, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->mScreenOnPowerEstimators:[Lcom/android/server/power/stats/UsageBasedPowerEstimator;

    aget-object v12, v12, v7

    invoke-virtual {v12, v8, v9}, Lcom/android/server/power/stats/UsageBasedPowerEstimator;->calculatePower(J)D

    move-result-wide v12

    add-double/2addr v5, v12

    .line 215
    const/4 v12, 0x0

    .local v12, "bin":I
    :goto_1
    const/16 v13, 0x64

    if-ge v12, v13, :cond_0

    .line 216
    invoke-virtual {v1, v7, v12, v2, v3}, Landroid/os/BatteryStats;->getDisplayScreenBrightnessTime(IIJ)J

    move-result-wide v13

    div-long/2addr v13, v10

    .line 218
    .local v13, "brightnessTime":J
    iget-object v15, v0, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->mScreenFullPowerEstimators:[Lcom/android/server/power/stats/UsageBasedPowerEstimator;

    aget-object v15, v15, v7

    invoke-virtual {v15, v13, v14}, Lcom/android/server/power/stats/UsageBasedPowerEstimator;->calculatePower(J)D

    move-result-wide v15

    int-to-float v10, v12

    const/high16 v11, 0x3f000000    # 0.5f

    add-float/2addr v10, v11

    float-to-double v10, v10

    mul-double/2addr v15, v10

    const-wide/high16 v10, 0x4059000000000000L    # 100.0

    div-double/2addr v15, v10

    .line 224
    .local v15, "binPowerMah":D
    add-double/2addr v5, v15

    .line 215
    .end local v13    # "brightnessTime":J
    .end local v15    # "binPowerMah":D
    add-int/lit8 v12, v12, 0x1

    const-wide/16 v10, 0x3e8

    goto :goto_1

    .line 211
    .end local v8    # "displayTime":J
    .end local v12    # "bin":I
    :cond_0
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 227
    .end local v7    # "display":I
    :cond_1
    return-wide v5
.end method

.method private smearScreenBatteryDrain(Landroid/util/SparseArray;Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$PowerAndDuration;J)V
    .locals 18
    .param p2, "totalPowerAndDuration"    # Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$PowerAndDuration;
    .param p3, "rawRealtimeUs"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray<",
            "Landroid/os/UidBatteryConsumer$Builder;",
            ">;",
            "Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$PowerAndDuration;",
            "J)V"
        }
    .end annotation

    .line 317
    .local p1, "uidBatteryConsumerBuilders":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/os/UidBatteryConsumer$Builder;>;"
    move-object/from16 v0, p1

    const-wide/16 v1, 0x0

    .line 318
    .local v1, "totalActivityTimeMs":J
    new-instance v3, Landroid/util/SparseLongArray;

    invoke-direct {v3}, Landroid/util/SparseLongArray;-><init>()V

    .line 319
    .local v3, "activityTimeArray":Landroid/util/SparseLongArray;
    invoke-virtual/range {p1 .. p1}, Landroid/util/SparseArray;->size()I

    move-result v4

    const/4 v5, 0x1

    sub-int/2addr v4, v5

    .local v4, "i":I
    :goto_0
    if-ltz v4, :cond_1

    .line 320
    invoke-virtual {v0, v4}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/os/UidBatteryConsumer$Builder;

    .line 321
    .local v6, "app":Landroid/os/UidBatteryConsumer$Builder;
    invoke-virtual {v6}, Landroid/os/UidBatteryConsumer$Builder;->getBatteryStatsUid()Landroid/os/BatteryStats$Uid;

    move-result-object v7

    .line 322
    .local v7, "uid":Landroid/os/BatteryStats$Uid;
    move-object/from16 v8, p0

    move-wide/from16 v9, p3

    invoke-virtual {v8, v7, v9, v10}, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->getProcessForegroundTimeMs(Landroid/os/BatteryStats$Uid;J)J

    move-result-wide v11

    .line 323
    .local v11, "timeMs":J
    invoke-virtual {v7}, Landroid/os/BatteryStats$Uid;->getUid()I

    move-result v13

    invoke-virtual {v3, v13, v11, v12}, Landroid/util/SparseLongArray;->put(IJ)V

    .line 324
    invoke-virtual {v6}, Landroid/os/UidBatteryConsumer$Builder;->isVirtualUid()Z

    move-result v13

    if-nez v13, :cond_0

    .line 325
    add-long/2addr v1, v11

    .line 319
    .end local v6    # "app":Landroid/os/UidBatteryConsumer$Builder;
    .end local v7    # "uid":Landroid/os/BatteryStats$Uid;
    .end local v11    # "timeMs":J
    :cond_0
    add-int/lit8 v4, v4, -0x1

    goto :goto_0

    :cond_1
    move-object/from16 v8, p0

    move-wide/from16 v9, p3

    .line 329
    .end local v4    # "i":I
    const-wide/32 v6, 0x927c0

    cmp-long v4, v1, v6

    if-ltz v4, :cond_3

    .line 330
    move-object/from16 v4, p2

    iget-wide v6, v4, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$PowerAndDuration;->powerMah:D

    .line 331
    .local v6, "totalScreenPowerMah":D
    invoke-virtual/range {p1 .. p1}, Landroid/util/SparseArray;->size()I

    move-result v11

    sub-int/2addr v11, v5

    .local v11, "i":I
    :goto_1
    if-ltz v11, :cond_2

    .line 332
    invoke-virtual {v0, v11}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/os/UidBatteryConsumer$Builder;

    .line 333
    .local v12, "app":Landroid/os/UidBatteryConsumer$Builder;
    invoke-virtual {v12}, Landroid/os/UidBatteryConsumer$Builder;->getUid()I

    move-result v13

    const-wide/16 v14, 0x0

    invoke-virtual {v3, v13, v14, v15}, Landroid/util/SparseLongArray;->get(IJ)J

    move-result-wide v13

    .line 334
    .local v13, "durationMs":J
    move-object v15, v3

    .end local v3    # "activityTimeArray":Landroid/util/SparseLongArray;
    .local v15, "activityTimeArray":Landroid/util/SparseLongArray;
    long-to-double v3, v13

    mul-double/2addr v3, v6

    move-wide/from16 v16, v6

    .end local v6    # "totalScreenPowerMah":D
    .local v16, "totalScreenPowerMah":D
    long-to-double v5, v1

    div-double/2addr v3, v5

    .line 335
    .local v3, "powerMah":D
    const/4 v5, 0x0

    invoke-virtual {v12, v5, v13, v14}, Landroid/os/UidBatteryConsumer$Builder;->setUsageDurationMillis(IJ)Landroid/os/BatteryConsumer$BaseBuilder;

    move-result-object v6

    check-cast v6, Landroid/os/UidBatteryConsumer$Builder;

    .line 336
    const/4 v7, 0x1

    invoke-virtual {v6, v5, v3, v4, v7}, Landroid/os/UidBatteryConsumer$Builder;->setConsumedPower(IDI)Landroid/os/BatteryConsumer$BaseBuilder;

    .line 331
    .end local v3    # "powerMah":D
    .end local v12    # "app":Landroid/os/UidBatteryConsumer$Builder;
    .end local v13    # "durationMs":J
    add-int/lit8 v11, v11, -0x1

    move-object/from16 v4, p2

    move v5, v7

    move-object v3, v15

    move-wide/from16 v6, v16

    goto :goto_1

    .end local v15    # "activityTimeArray":Landroid/util/SparseLongArray;
    .end local v16    # "totalScreenPowerMah":D
    .local v3, "activityTimeArray":Landroid/util/SparseLongArray;
    .restart local v6    # "totalScreenPowerMah":D
    :cond_2
    move-object v15, v3

    move-wide/from16 v16, v6

    .end local v3    # "activityTimeArray":Landroid/util/SparseLongArray;
    .end local v6    # "totalScreenPowerMah":D
    .restart local v15    # "activityTimeArray":Landroid/util/SparseLongArray;
    .restart local v16    # "totalScreenPowerMah":D
    goto :goto_2

    .line 329
    .end local v11    # "i":I
    .end local v15    # "activityTimeArray":Landroid/util/SparseLongArray;
    .end local v16    # "totalScreenPowerMah":D
    .restart local v3    # "activityTimeArray":Landroid/util/SparseLongArray;
    :cond_3
    move-object v15, v3

    .line 340
    .end local v3    # "activityTimeArray":Landroid/util/SparseLongArray;
    .restart local v15    # "activityTimeArray":Landroid/util/SparseLongArray;
    :goto_2
    return-void
.end method


# virtual methods
.method public calculate(Landroid/os/BatteryUsageStats$Builder;Landroid/os/BatteryStats;JJLandroid/os/BatteryUsageStatsQuery;)V
    .locals 18
    .param p1, "builder"    # Landroid/os/BatteryUsageStats$Builder;
    .param p2, "batteryStats"    # Landroid/os/BatteryStats;
    .param p3, "rawRealtimeUs"    # J
    .param p5, "rawUptimeUs"    # J
    .param p7, "query"    # Landroid/os/BatteryUsageStatsQuery;

    .line 94
    move-object/from16 v9, p0

    move-object/from16 v10, p1

    move-wide/from16 v11, p3

    new-instance v0, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$PowerAndDuration;

    const/4 v13, 0x0

    invoke-direct {v0, v13}, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$PowerAndDuration;-><init>(Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$PowerAndDuration-IA;)V

    move-object v14, v0

    .line 96
    .local v14, "totalPowerAndDuration":Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$PowerAndDuration;
    nop

    .line 97
    invoke-virtual/range {p1 .. p1}, Landroid/os/BatteryUsageStats$Builder;->getUidBatteryConsumerBuilders()Landroid/util/SparseArray;

    move-result-object v15

    .line 99
    .local v15, "uidBatteryConsumerBuilders":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/os/UidBatteryConsumer$Builder;>;"
    invoke-virtual {v15}, Landroid/util/SparseArray;->size()I

    move-result v0

    const/4 v7, 0x1

    sub-int/2addr v0, v7

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_1

    .line 100
    invoke-virtual {v15, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/UidBatteryConsumer$Builder;

    .line 101
    .local v1, "app":Landroid/os/UidBatteryConsumer$Builder;
    invoke-virtual {v1}, Landroid/os/UidBatteryConsumer$Builder;->getBatteryStatsUid()Landroid/os/BatteryStats$Uid;

    move-result-object v2

    .line 102
    .local v2, "uid":Landroid/os/BatteryStats$Uid;
    invoke-virtual {v9, v2, v11, v12}, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->getProcessForegroundTimeMs(Landroid/os/BatteryStats$Uid;J)J

    move-result-wide v3

    .line 103
    .local v3, "timeMs":J
    invoke-static {}, Lcom/android/server/power/stats/BatteryStatsManagerStub;->getInstance()Lcom/android/server/power/stats/BatteryStatsManagerStub;

    move-result-object v5

    check-cast v5, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;

    .line 104
    .local v5, "impl":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;
    if-eqz v5, :cond_0

    .line 105
    invoke-virtual {v5}, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->getScreenCalculateMap()Landroid/util/SparseArray;

    move-result-object v6

    .line 106
    .local v6, "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;>;"
    if-eqz v6, :cond_0

    invoke-virtual {v2}, Landroid/os/BatteryStats$Uid;->getUid()I

    move-result v8

    invoke-virtual {v6, v8}, Landroid/util/SparseArray;->contains(I)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 107
    new-instance v8, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$UidTimeStats;

    invoke-direct {v8, v13}, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$UidTimeStats;-><init>(Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$UidTimeStats-IA;)V

    .line 108
    .local v8, "uidTimeStats":Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$UidTimeStats;
    iput-wide v3, v8, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$UidTimeStats;->fgTimeMs:J

    .line 109
    invoke-virtual {v2}, Landroid/os/BatteryStats$Uid;->getUid()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;

    invoke-static {v7}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;

    iget-object v7, v7, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->screenBrightnessTimers:[Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;

    iput-object v7, v8, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$UidTimeStats;->screenBrightnessTimers:[Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;

    .line 110
    iget-object v7, v9, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->mAppDurationMap:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/os/BatteryStats$Uid;->getUid()I

    move-result v13

    invoke-virtual {v7, v13, v8}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 99
    .end local v1    # "app":Landroid/os/UidBatteryConsumer$Builder;
    .end local v2    # "uid":Landroid/os/BatteryStats$Uid;
    .end local v3    # "timeMs":J
    .end local v5    # "impl":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;
    .end local v6    # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;>;"
    .end local v8    # "uidTimeStats":Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$UidTimeStats;
    :cond_0
    add-int/lit8 v0, v0, -0x1

    const/4 v7, 0x1

    const/4 v13, 0x0

    goto :goto_0

    .line 115
    .end local v0    # "i":I
    :cond_1
    invoke-virtual/range {p2 .. p2}, Landroid/os/BatteryStats;->getScreenOnEnergyConsumptionUC()J

    move-result-wide v7

    .line 116
    .local v7, "consumptionUC":J
    move-object/from16 v13, p7

    invoke-static {v7, v8, v13}, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->getPowerModel(JLandroid/os/BatteryUsageStatsQuery;)I

    move-result v6

    .line 117
    .local v6, "powerModel":I
    const/16 v16, 0x0

    move-object/from16 v0, p0

    move-object v1, v14

    move v2, v6

    move-object/from16 v3, p2

    move-wide/from16 v4, p3

    move v13, v6

    .end local v6    # "powerModel":I
    .local v13, "powerModel":I
    move/from16 v6, v16

    move-wide/from16 v16, v7

    const/4 v10, 0x1

    .end local v7    # "consumptionUC":J
    .local v16, "consumptionUC":J
    invoke-direct/range {v0 .. v8}, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->calculateTotalDurationAndPower(Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$PowerAndDuration;ILandroid/os/BatteryStats;JIJ)V

    .line 120
    const-wide/16 v0, 0x0

    .line 121
    .local v0, "totalAppPower":D
    const-wide/16 v2, 0x0

    .line 126
    .local v2, "totalAppDuration":J
    const/4 v4, 0x0

    packed-switch v13, :pswitch_data_0

    .line 145
    move-wide/from16 v5, p3

    invoke-direct {v9, v15, v14, v5, v6}, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->smearScreenBatteryDrain(Landroid/util/SparseArray;Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$PowerAndDuration;J)V

    .line 147
    iget-wide v0, v14, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$PowerAndDuration;->powerMah:D

    .line 148
    iget-wide v2, v14, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$PowerAndDuration;->durationMs:J

    goto :goto_2

    .line 128
    :pswitch_0
    new-instance v5, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$PowerAndDuration;

    const/4 v6, 0x0

    invoke-direct {v5, v6}, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$PowerAndDuration;-><init>(Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$PowerAndDuration-IA;)V

    .line 129
    .local v5, "appPowerAndDuration":Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$PowerAndDuration;
    invoke-virtual {v15}, Landroid/util/SparseArray;->size()I

    move-result v6

    sub-int/2addr v6, v10

    .local v6, "i":I
    :goto_1
    if-ltz v6, :cond_3

    .line 130
    invoke-virtual {v15, v6}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/os/UidBatteryConsumer$Builder;

    .line 131
    .local v7, "app":Landroid/os/UidBatteryConsumer$Builder;
    invoke-virtual {v7}, Landroid/os/UidBatteryConsumer$Builder;->getBatteryStatsUid()Landroid/os/BatteryStats$Uid;

    move-result-object v8

    invoke-direct {v9, v5, v8, v11, v12}, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->calculateAppUsingMeasuredEnergy(Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$PowerAndDuration;Landroid/os/BatteryStats$Uid;J)V

    .line 133
    iget-wide v10, v5, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$PowerAndDuration;->durationMs:J

    invoke-virtual {v7, v4, v10, v11}, Landroid/os/UidBatteryConsumer$Builder;->setUsageDurationMillis(IJ)Landroid/os/BatteryConsumer$BaseBuilder;

    move-result-object v8

    check-cast v8, Landroid/os/UidBatteryConsumer$Builder;

    iget-wide v10, v5, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$PowerAndDuration;->powerMah:D

    .line 135
    invoke-virtual {v8, v4, v10, v11, v13}, Landroid/os/UidBatteryConsumer$Builder;->setConsumedPower(IDI)Landroid/os/BatteryConsumer$BaseBuilder;

    .line 137
    invoke-virtual {v7}, Landroid/os/UidBatteryConsumer$Builder;->isVirtualUid()Z

    move-result v8

    if-nez v8, :cond_2

    .line 138
    iget-wide v10, v5, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$PowerAndDuration;->powerMah:D

    add-double/2addr v0, v10

    .line 139
    iget-wide v10, v5, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$PowerAndDuration;->durationMs:J

    add-long/2addr v2, v10

    .line 129
    .end local v7    # "app":Landroid/os/UidBatteryConsumer$Builder;
    :cond_2
    add-int/lit8 v6, v6, -0x1

    move-wide/from16 v11, p3

    const/4 v10, 0x1

    goto :goto_1

    .line 142
    .end local v6    # "i":I
    :cond_3
    move-wide/from16 v5, p3

    .line 151
    .end local v5    # "appPowerAndDuration":Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$PowerAndDuration;
    :goto_2
    move-object/from16 v7, p1

    const/4 v8, 0x1

    invoke-virtual {v7, v4}, Landroid/os/BatteryUsageStats$Builder;->getAggregateBatteryConsumerBuilder(I)Landroid/os/AggregateBatteryConsumer$Builder;

    move-result-object v10

    iget-wide v11, v14, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$PowerAndDuration;->powerMah:D

    .line 154
    invoke-static {v11, v12, v0, v1}, Ljava/lang/Math;->max(DD)D

    move-result-wide v11

    .line 153
    invoke-virtual {v10, v4, v11, v12, v13}, Landroid/os/AggregateBatteryConsumer$Builder;->setConsumedPower(IDI)Landroid/os/BatteryConsumer$BaseBuilder;

    move-result-object v10

    check-cast v10, Landroid/os/AggregateBatteryConsumer$Builder;

    iget-wide v11, v14, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$PowerAndDuration;->durationMs:J

    .line 155
    invoke-virtual {v10, v4, v11, v12}, Landroid/os/AggregateBatteryConsumer$Builder;->setUsageDurationMillis(IJ)Landroid/os/BatteryConsumer$BaseBuilder;

    .line 158
    invoke-virtual {v7, v8}, Landroid/os/BatteryUsageStats$Builder;->getAggregateBatteryConsumerBuilder(I)Landroid/os/AggregateBatteryConsumer$Builder;

    move-result-object v8

    .line 160
    invoke-virtual {v8, v4, v0, v1, v13}, Landroid/os/AggregateBatteryConsumer$Builder;->setConsumedPower(IDI)Landroid/os/BatteryConsumer$BaseBuilder;

    move-result-object v8

    check-cast v8, Landroid/os/AggregateBatteryConsumer$Builder;

    .line 161
    invoke-virtual {v8, v4, v2, v3}, Landroid/os/AggregateBatteryConsumer$Builder;->setUsageDurationMillis(IJ)Landroid/os/BatteryConsumer$BaseBuilder;

    .line 162
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public getDyAppScreenPower(Landroid/os/BatteryStats;)[B
    .locals 22
    .param p1, "batteryStats"    # Landroid/os/BatteryStats;

    .line 375
    move-object/from16 v1, p0

    const/4 v2, 0x0

    .line 376
    .local v2, "bytes":[B
    invoke-static {}, Lcom/android/server/power/stats/BatteryStatsManagerStub;->getInstance()Lcom/android/server/power/stats/BatteryStatsManagerStub;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;

    .line 377
    .local v3, "impl":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;
    if-eqz v3, :cond_6

    .line 378
    const-wide/16 v4, 0x0

    .line 379
    .local v4, "powerMah":D
    const-wide/16 v6, 0x0

    .line 380
    .local v6, "fgTimeMs":J
    const/16 v0, 0x64

    new-array v8, v0, [J

    .line 381
    .local v8, "brightnessBinTimes":[J
    iget-object v9, v1, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->mScreenOnPowerEstimators:[Lcom/android/server/power/stats/UsageBasedPowerEstimator;

    array-length v9, v9

    .line 382
    .local v9, "numDisplays":I
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    mul-long/2addr v10, v12

    .line 383
    .local v10, "rawRealtimeUs":J
    invoke-virtual {v3}, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->getScreenCalculateMap()Landroid/util/SparseArray;

    move-result-object v12

    .line 384
    .local v12, "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;>;"
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_0
    invoke-virtual {v12}, Landroid/util/SparseArray;->size()I

    move-result v14

    if-ge v13, v14, :cond_5

    .line 385
    invoke-virtual {v12, v13}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v14

    .line 386
    .local v14, "uid":I
    invoke-virtual {v12, v14}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;

    .line 387
    .local v15, "uidStats":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;
    if-eqz v15, :cond_4

    iget-object v0, v15, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->screenBrightnessTimers:[Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;

    if-nez v0, :cond_0

    move-object/from16 v16, v2

    move-object/from16 v17, v3

    const/16 v0, 0x64

    goto :goto_3

    .line 388
    :cond_0
    iget-object v0, v15, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->packageName:Ljava/lang/String;

    move-object/from16 v16, v2

    .end local v2    # "bytes":[B
    .local v16, "bytes":[B
    const-string v2, "com.ss.android.ugc.aweme"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 389
    invoke-virtual/range {p1 .. p1}, Landroid/os/BatteryStats;->getUidStats()Landroid/util/SparseArray;

    move-result-object v0

    iget v2, v15, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->uid:I

    .line 390
    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/power/stats/BatteryStatsImpl$Uid;

    .line 391
    .local v0, "bsiUid":Lcom/android/server/power/stats/BatteryStatsImpl$Uid;
    move-object/from16 v17, v3

    .end local v3    # "impl":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;
    .local v17, "impl":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;
    invoke-virtual {v1, v0, v10, v11}, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->getProcessForegroundTimeMs(Landroid/os/BatteryStats$Uid;J)J

    move-result-wide v2

    .line 392
    .local v2, "timeMs":J
    move-wide v6, v2

    .line 393
    const/16 v18, 0x0

    move/from16 v21, v18

    move-object/from16 v18, v0

    move/from16 v0, v21

    .local v0, "display":I
    .local v18, "bsiUid":Lcom/android/server/power/stats/BatteryStatsImpl$Uid;
    :goto_1
    if-ge v0, v9, :cond_1

    .line 394
    move-wide/from16 v19, v6

    .end local v6    # "fgTimeMs":J
    .local v19, "fgTimeMs":J
    iget-object v6, v1, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->mScreenOnPowerEstimators:[Lcom/android/server/power/stats/UsageBasedPowerEstimator;

    aget-object v6, v6, v0

    invoke-virtual {v6, v2, v3}, Lcom/android/server/power/stats/UsageBasedPowerEstimator;->calculatePower(J)D

    move-result-wide v6

    add-double/2addr v4, v6

    .line 395
    iget-object v6, v15, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->screenBrightnessTimers:[Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;

    invoke-direct {v1, v0, v6, v10, v11}, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->calculateAppsBinPower(I[Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;J)D

    move-result-wide v6

    add-double/2addr v4, v6

    .line 393
    add-int/lit8 v0, v0, 0x1

    move-wide/from16 v6, v19

    goto :goto_1

    .end local v19    # "fgTimeMs":J
    .restart local v6    # "fgTimeMs":J
    :cond_1
    move-wide/from16 v19, v6

    .line 397
    .end local v0    # "display":I
    .end local v6    # "fgTimeMs":J
    .restart local v19    # "fgTimeMs":J
    const/4 v0, 0x0

    .local v0, "j":I
    :goto_2
    const/16 v6, 0x64

    if-ge v0, v6, :cond_2

    .line 398
    iget-object v7, v15, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->screenBrightnessTimers:[Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;

    aget-object v7, v7, v0

    .line 399
    const/4 v6, 0x0

    invoke-virtual {v7, v10, v11, v6}, Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;->getTotalTimeLocked(JI)J

    move-result-wide v6

    aput-wide v6, v8, v0

    .line 397
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 401
    .end local v0    # "j":I
    :cond_2
    move-wide/from16 v6, v19

    goto :goto_4

    .line 388
    .end local v2    # "timeMs":J
    .end local v17    # "impl":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;
    .end local v18    # "bsiUid":Lcom/android/server/power/stats/BatteryStatsImpl$Uid;
    .end local v19    # "fgTimeMs":J
    .restart local v3    # "impl":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;
    .restart local v6    # "fgTimeMs":J
    :cond_3
    move-object/from16 v17, v3

    const/16 v0, 0x64

    .end local v3    # "impl":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;
    .restart local v17    # "impl":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;
    goto :goto_3

    .line 387
    .end local v16    # "bytes":[B
    .end local v17    # "impl":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;
    .local v2, "bytes":[B
    .restart local v3    # "impl":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;
    :cond_4
    move-object/from16 v16, v2

    move-object/from16 v17, v3

    .line 384
    .end local v2    # "bytes":[B
    .end local v3    # "impl":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;
    .end local v14    # "uid":I
    .end local v15    # "uidStats":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;
    .restart local v16    # "bytes":[B
    .restart local v17    # "impl":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;
    :goto_3
    add-int/lit8 v13, v13, 0x1

    move-object/from16 v2, v16

    move-object/from16 v3, v17

    goto/16 :goto_0

    .end local v16    # "bytes":[B
    .end local v17    # "impl":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;
    .restart local v2    # "bytes":[B
    .restart local v3    # "impl":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;
    :cond_5
    move-object/from16 v16, v2

    move-object/from16 v17, v3

    .line 405
    .end local v2    # "bytes":[B
    .end local v3    # "impl":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;
    .end local v13    # "i":I
    .restart local v16    # "bytes":[B
    .restart local v17    # "impl":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;
    :goto_4
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 407
    .local v2, "out":Landroid/os/Parcel;
    :try_start_0
    invoke-virtual {v2, v4, v5}, Landroid/os/Parcel;->writeDouble(D)V

    .line 408
    invoke-virtual {v2, v6, v7}, Landroid/os/Parcel;->writeLong(J)V

    .line 409
    invoke-virtual {v2, v8}, Landroid/os/Parcel;->writeLongArray([J)V

    .line 410
    invoke-virtual {v2}, Landroid/os/Parcel;->marshall()[B

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 414
    .end local v16    # "bytes":[B
    .local v0, "bytes":[B
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 415
    move-object v2, v0

    goto :goto_7

    .line 414
    .end local v0    # "bytes":[B
    .restart local v16    # "bytes":[B
    :catchall_0
    move-exception v0

    goto :goto_5

    .line 411
    :catch_0
    move-exception v0

    .line 412
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 414
    .end local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 415
    goto :goto_6

    .line 414
    :goto_5
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 415
    throw v0

    .line 377
    .end local v4    # "powerMah":D
    .end local v6    # "fgTimeMs":J
    .end local v8    # "brightnessBinTimes":[J
    .end local v9    # "numDisplays":I
    .end local v10    # "rawRealtimeUs":J
    .end local v12    # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;>;"
    .end local v16    # "bytes":[B
    .end local v17    # "impl":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;
    .local v2, "bytes":[B
    .restart local v3    # "impl":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;
    :cond_6
    move-object/from16 v16, v2

    move-object/from16 v17, v3

    .line 418
    .end local v2    # "bytes":[B
    .end local v3    # "impl":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;
    .restart local v16    # "bytes":[B
    .restart local v17    # "impl":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;
    :goto_6
    move-object/from16 v2, v16

    .end local v16    # "bytes":[B
    .restart local v2    # "bytes":[B
    :goto_7
    return-object v2
.end method

.method public getForegroundActivityTotalTimeUs(Landroid/os/BatteryStats$Uid;J)J
    .locals 3
    .param p1, "uid"    # Landroid/os/BatteryStats$Uid;
    .param p2, "rawRealtimeUs"    # J

    .line 366
    invoke-virtual {p1}, Landroid/os/BatteryStats$Uid;->getForegroundActivityTimer()Landroid/os/BatteryStats$Timer;

    move-result-object v0

    .line 367
    .local v0, "timer":Landroid/os/BatteryStats$Timer;
    if-nez v0, :cond_0

    .line 368
    const-wide/16 v1, 0x0

    return-wide v1

    .line 370
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, p2, p3, v1}, Landroid/os/BatteryStats$Timer;->getTotalTimeLocked(JI)J

    move-result-wide v1

    return-wide v1
.end method

.method public getProcessForegroundTimeMs(Landroid/os/BatteryStats$Uid;J)J
    .locals 9
    .param p1, "uid"    # Landroid/os/BatteryStats$Uid;
    .param p2, "rawRealTimeUs"    # J

    .line 347
    const/4 v0, 0x0

    filled-new-array {v0}, [I

    move-result-object v1

    .line 349
    .local v1, "foregroundTypes":[I
    const-wide/16 v2, 0x0

    .line 350
    .local v2, "timeUs":J
    array-length v4, v1

    move v5, v0

    :goto_0
    if-ge v5, v4, :cond_0

    aget v6, v1, v5

    .line 351
    .local v6, "type":I
    invoke-virtual {p1, v6, p2, p3, v0}, Landroid/os/BatteryStats$Uid;->getProcessStateTime(IJI)J

    move-result-wide v7

    .line 353
    .local v7, "localTime":J
    add-long/2addr v2, v7

    .line 350
    .end local v6    # "type":I
    .end local v7    # "localTime":J
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 358
    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->getForegroundActivityTotalTimeUs(Landroid/os/BatteryStats$Uid;J)J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    return-wide v4
.end method

.method public getSoleCalculateAppsBinTime(J)[J
    .locals 9
    .param p1, "rawRealtimeUs"    # J

    .line 298
    const/16 v0, 0x64

    new-array v1, v0, [J

    .line 299
    .local v1, "totalBinTimes":[J
    const/4 v2, 0x0

    .local v2, "bin":I
    :goto_0
    if-ge v2, v0, :cond_2

    .line 300
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    iget-object v4, p0, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->mAppDurationMap:Landroid/util/SparseArray;

    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v4

    if-ge v3, v4, :cond_1

    .line 301
    iget-object v4, p0, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->mAppDurationMap:Landroid/util/SparseArray;

    invoke-virtual {v4, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$UidTimeStats;

    .line 302
    .local v4, "timeStats":Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$UidTimeStats;
    iget-object v5, v4, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$UidTimeStats;->screenBrightnessTimers:[Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;

    if-nez v5, :cond_0

    goto :goto_2

    .line 303
    :cond_0
    aget-wide v5, v1, v2

    iget-object v7, v4, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$UidTimeStats;->screenBrightnessTimers:[Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;

    aget-object v7, v7, v2

    const/4 v8, 0x0

    invoke-virtual {v7, p1, p2, v8}, Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;->getTotalTimeLocked(JI)J

    move-result-wide v7

    add-long/2addr v5, v7

    aput-wide v5, v1, v2

    .line 300
    .end local v4    # "timeStats":Lcom/android/server/power/stats/ScreenPowerCalculatorImpl$UidTimeStats;
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 299
    .end local v3    # "i":I
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 307
    .end local v2    # "bin":I
    :cond_2
    return-object v1
.end method

.method public init(Lcom/android/internal/os/PowerProfile;)V
    .locals 12
    .param p1, "powerProfile"    # Lcom/android/internal/os/PowerProfile;

    .line 63
    invoke-virtual {p1}, Lcom/android/internal/os/PowerProfile;->getNumDisplays()I

    move-result v0

    .line 64
    .local v0, "numDisplays":I
    new-array v1, v0, [Lcom/android/server/power/stats/UsageBasedPowerEstimator;

    iput-object v1, p0, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->mScreenOnPowerEstimators:[Lcom/android/server/power/stats/UsageBasedPowerEstimator;

    .line 65
    new-array v1, v0, [Lcom/android/server/power/stats/UsageBasedPowerEstimator;

    iput-object v1, p0, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->mScreenFullPowerEstimators:[Lcom/android/server/power/stats/UsageBasedPowerEstimator;

    .line 66
    new-array v1, v0, [Lcom/android/server/power/stats/UsageBasedPowerEstimator;

    iput-object v1, p0, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->mAppScreenLowPowerEstimators:[Lcom/android/server/power/stats/UsageBasedPowerEstimator;

    .line 67
    new-array v1, v0, [Lcom/android/server/power/stats/UsageBasedPowerEstimator;

    iput-object v1, p0, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->mAppScreenFullPowerEstimators:[Lcom/android/server/power/stats/UsageBasedPowerEstimator;

    .line 68
    new-array v1, v0, [Lcom/android/server/power/stats/UsageBasedPowerEstimator;

    iput-object v1, p0, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->mAppScreenHBMPowerEstimators:[Lcom/android/server/power/stats/UsageBasedPowerEstimator;

    .line 70
    const-string v1, "dy.screen.low"

    invoke-virtual {p1, v1}, Lcom/android/internal/os/PowerProfile;->getAveragePower(Ljava/lang/String;)D

    move-result-wide v1

    .line 71
    .local v1, "dyLowMa":D
    const-string v3, "dy.screen.full"

    invoke-virtual {p1, v3}, Lcom/android/internal/os/PowerProfile;->getAveragePower(Ljava/lang/String;)D

    move-result-wide v3

    .line 72
    .local v3, "dyFullMa":D
    const-string v5, "dy.screen.hbm"

    invoke-virtual {p1, v5}, Lcom/android/internal/os/PowerProfile;->getAveragePower(Ljava/lang/String;)D

    move-result-wide v5

    .line 73
    .local v5, "dyHbmMa":D
    const/4 v7, 0x0

    .local v7, "display":I
    :goto_0
    if-ge v7, v0, :cond_0

    .line 74
    iget-object v8, p0, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->mScreenOnPowerEstimators:[Lcom/android/server/power/stats/UsageBasedPowerEstimator;

    new-instance v9, Lcom/android/server/power/stats/UsageBasedPowerEstimator;

    .line 75
    const-string v10, "screen.on.display"

    invoke-virtual {p1, v10, v7}, Lcom/android/internal/os/PowerProfile;->getAveragePowerForOrdinal(Ljava/lang/String;I)D

    move-result-wide v10

    invoke-direct {v9, v10, v11}, Lcom/android/server/power/stats/UsageBasedPowerEstimator;-><init>(D)V

    aput-object v9, v8, v7

    .line 76
    iget-object v8, p0, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->mScreenFullPowerEstimators:[Lcom/android/server/power/stats/UsageBasedPowerEstimator;

    new-instance v9, Lcom/android/server/power/stats/UsageBasedPowerEstimator;

    .line 77
    const-string v10, "screen.full.display"

    invoke-virtual {p1, v10, v7}, Lcom/android/internal/os/PowerProfile;->getAveragePowerForOrdinal(Ljava/lang/String;I)D

    move-result-wide v10

    invoke-direct {v9, v10, v11}, Lcom/android/server/power/stats/UsageBasedPowerEstimator;-><init>(D)V

    aput-object v9, v8, v7

    .line 79
    iget-object v8, p0, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->mAppScreenLowPowerEstimators:[Lcom/android/server/power/stats/UsageBasedPowerEstimator;

    new-instance v9, Lcom/android/server/power/stats/UsageBasedPowerEstimator;

    invoke-direct {v9, v1, v2}, Lcom/android/server/power/stats/UsageBasedPowerEstimator;-><init>(D)V

    aput-object v9, v8, v7

    .line 80
    iget-object v8, p0, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->mAppScreenFullPowerEstimators:[Lcom/android/server/power/stats/UsageBasedPowerEstimator;

    new-instance v9, Lcom/android/server/power/stats/UsageBasedPowerEstimator;

    invoke-direct {v9, v3, v4}, Lcom/android/server/power/stats/UsageBasedPowerEstimator;-><init>(D)V

    aput-object v9, v8, v7

    .line 81
    iget-object v8, p0, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->mAppScreenHBMPowerEstimators:[Lcom/android/server/power/stats/UsageBasedPowerEstimator;

    new-instance v9, Lcom/android/server/power/stats/UsageBasedPowerEstimator;

    invoke-direct {v9, v5, v6}, Lcom/android/server/power/stats/UsageBasedPowerEstimator;-><init>(D)V

    aput-object v9, v8, v7

    .line 73
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 83
    .end local v7    # "display":I
    :cond_0
    const-wide/16 v7, 0x0

    cmpl-double v9, v3, v7

    if-eqz v9, :cond_1

    cmpl-double v7, v5, v7

    if-eqz v7, :cond_1

    const/4 v7, 0x1

    goto :goto_1

    :cond_1
    const/4 v7, 0x0

    :goto_1
    iput-boolean v7, p0, Lcom/android/server/power/stats/ScreenPowerCalculatorImpl;->mHasDyPowerController:Z

    .line 84
    return-void
.end method

.method public isPowerComponentSupported(I)Z
    .locals 1
    .param p1, "powerComponent"    # I

    .line 88
    if-nez p1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
