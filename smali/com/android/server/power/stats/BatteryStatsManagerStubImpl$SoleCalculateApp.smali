.class public Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;
.super Ljava/lang/Object;
.source "BatteryStatsManagerStubImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SoleCalculateApp"
.end annotation


# instance fields
.field public oldBin:I

.field public packageName:Ljava/lang/String;

.field public refreshRate:F

.field public screenBrightnessTimers:[Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;

.field public uid:I


# direct methods
.method public constructor <init>(Lcom/android/internal/os/Clock;Lcom/android/server/power/stats/BatteryStatsImpl$TimeBase;)V
    .locals 10
    .param p1, "clock"    # Lcom/android/internal/os/Clock;
    .param p2, "timeBase"    # Lcom/android/server/power/stats/BatteryStatsImpl$TimeBase;

    .line 438
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 434
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->oldBin:I

    .line 435
    const/16 v0, 0x64

    new-array v1, v0, [Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;

    iput-object v1, p0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->screenBrightnessTimers:[Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;

    .line 439
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 440
    iget-object v2, p0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->screenBrightnessTimers:[Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;

    new-instance v9, Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;

    const/4 v5, 0x0

    rsub-int v6, v1, -0x2710

    const/4 v7, 0x0

    move-object v3, v9

    move-object v4, p1

    move-object v8, p2

    invoke-direct/range {v3 .. v8}, Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;-><init>(Lcom/android/internal/os/Clock;Lcom/android/server/power/stats/BatteryStatsImpl$Uid;ILjava/util/ArrayList;Lcom/android/server/power/stats/BatteryStatsImpl$TimeBase;)V

    aput-object v9, v2, v1

    .line 439
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 443
    .end local v1    # "i":I
    :cond_0
    return-void
.end method


# virtual methods
.method public reset(J)V
    .locals 3
    .param p1, "elapsedRealtimeUs"    # J

    .line 446
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0x64

    if-ge v0, v1, :cond_0

    .line 447
    iget-object v1, p0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->screenBrightnessTimers:[Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;

    aget-object v1, v1, v0

    const/4 v2, 0x0

    invoke-virtual {v1, v2, p1, p2}, Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;->reset(ZJ)Z

    .line 446
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 449
    .end local v0    # "i":I
    :cond_0
    return-void
.end method
