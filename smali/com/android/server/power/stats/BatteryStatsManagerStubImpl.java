public class com.android.server.power.stats.BatteryStatsManagerStubImpl implements com.android.server.power.stats.BatteryStatsManagerStub {
	 /* .source "BatteryStatsManagerStubImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Long BYTES_PER_GB;
private static final Long BYTES_PER_KB;
private static final Long BYTES_PER_MB;
private static final Boolean DEBUG;
private static final java.lang.String TAG;
/* # instance fields */
public com.android.server.power.stats.BatteryStatsManagerStub$ActiveCallback mActiveCallback;
private android.util.SparseArray mCalculateAppMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Integer mCurrentSoleUid;
/* # direct methods */
public com.android.server.power.stats.BatteryStatsManagerStubImpl ( ) {
/* .locals 1 */
/* .line 22 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 30 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
this.mCalculateAppMap = v0;
/* .line 31 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mCurrentSoleUid:I */
return;
} // .end method
private Long getProcessForegroundTimeMs ( Integer p0, android.os.BatteryStats p1, Long p2 ) {
/* .locals 11 */
/* .param p1, "curUid" # I */
/* .param p2, "batteryStats" # Landroid/os/BatteryStats; */
/* .param p3, "realtimeUs" # J */
/* .line 345 */
(( android.os.BatteryStats ) p2 ).getUidStats ( ); // invoke-virtual {p2}, Landroid/os/BatteryStats;->getUidStats()Landroid/util/SparseArray;
(( android.util.SparseArray ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v0, Landroid/os/BatteryStats$Uid; */
/* .line 346 */
/* .local v0, "uid":Landroid/os/BatteryStats$Uid; */
/* if-nez v0, :cond_0 */
/* const-wide/16 v1, 0x0 */
/* return-wide v1 */
/* .line 348 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
(( android.os.BatteryStats$Uid ) v0 ).getProcessStateTime ( v1, p3, p4, v1 ); // invoke-virtual {v0, v1, p3, p4, v1}, Landroid/os/BatteryStats$Uid;->getProcessStateTime(IJI)J
/* move-result-wide v2 */
/* .line 350 */
/* .local v2, "topStateDurationUs":J */
/* const-wide/16 v4, 0x0 */
/* .line 351 */
/* .local v4, "foregroundActivityDurationUs":J */
(( android.os.BatteryStats$Uid ) v0 ).getForegroundActivityTimer ( ); // invoke-virtual {v0}, Landroid/os/BatteryStats$Uid;->getForegroundActivityTimer()Landroid/os/BatteryStats$Timer;
/* .line 352 */
/* .local v6, "foregroundActivityTimer":Landroid/os/BatteryStats$Timer; */
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 353 */
(( android.os.BatteryStats$Timer ) v6 ).getTotalTimeLocked ( p3, p4, v1 ); // invoke-virtual {v6, p3, p4, v1}, Landroid/os/BatteryStats$Timer;->getTotalTimeLocked(JI)J
/* move-result-wide v4 */
/* .line 357 */
} // :cond_1
java.lang.Math .min ( v2,v3,v4,v5 );
/* move-result-wide v7 */
/* .line 359 */
/* .local v7, "totalForegroundDurationUs":J */
/* const-wide/16 v9, 0x3e8 */
/* div-long v9, v7, v9 */
/* return-wide v9 */
} // .end method
private void readSoleCalculateApp ( android.os.Parcel p0, com.android.server.power.stats.BatteryStatsImpl p1 ) {
/* .locals 8 */
/* .param p1, "in" # Landroid/os/Parcel; */
/* .param p2, "batteryStats" # Lcom/android/server/power/stats/BatteryStatsImpl; */
/* .line 384 */
v0 = this.mCalculateAppMap;
/* monitor-enter v0 */
/* .line 385 */
try { // :try_start_0
v1 = this.mCalculateAppMap;
(( android.util.SparseArray ) v1 ).clear ( ); // invoke-virtual {v1}, Landroid/util/SparseArray;->clear()V
/* .line 386 */
v1 = (( android.os.Parcel ) p1 ).readInt ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I
/* .line 387 */
/* .local v1, "size":I */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
/* if-ge v2, v1, :cond_3 */
/* .line 388 */
v3 = (( android.os.Parcel ) p1 ).readInt ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I
/* .line 389 */
/* .local v3, "uid":I */
(( android.os.Parcel ) p1 ).readString ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;
/* .line 390 */
/* .local v4, "pkgName":Ljava/lang/String; */
/* new-instance v5, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp; */
v6 = this.mClock;
v7 = this.mOnBatteryTimeBase;
/* invoke-direct {v5, v6, v7}, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;-><init>(Lcom/android/internal/os/Clock;Lcom/android/server/power/stats/BatteryStatsImpl$TimeBase;)V */
/* .line 391 */
/* .local v5, "app":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp; */
int v6 = 0; // const/4 v6, 0x0
/* .local v6, "j":I */
} // :goto_1
/* const/16 v7, 0x64 */
/* if-ge v6, v7, :cond_1 */
/* .line 392 */
v7 = (( android.os.Parcel ) p1 ).readInt ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I
if ( v7 != null) { // if-eqz v7, :cond_0
/* .line 393 */
v7 = this.screenBrightnessTimers;
/* aget-object v7, v7, v6 */
(( com.android.server.power.stats.BatteryStatsImpl$StopwatchTimer ) v7 ).readSummaryFromParcelLocked ( p1 ); // invoke-virtual {v7, p1}, Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;->readSummaryFromParcelLocked(Landroid/os/Parcel;)V
/* .line 391 */
} // :cond_0
/* add-int/lit8 v6, v6, 0x1 */
/* .line 397 */
} // .end local v6 # "j":I
} // :cond_1
/* iput v3, v5, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->uid:I */
/* .line 398 */
this.packageName = v4;
/* .line 399 */
v6 = android.os.UserHandle .isApp ( v3 );
if ( v6 != null) { // if-eqz v6, :cond_2
/* .line 400 */
v6 = this.mCalculateAppMap;
(( android.util.SparseArray ) v6 ).put ( v3, v5 ); // invoke-virtual {v6, v3, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 387 */
} // .end local v3 # "uid":I
} // .end local v4 # "pkgName":Ljava/lang/String;
} // .end local v5 # "app":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;
} // :cond_2
/* add-int/lit8 v2, v2, 0x1 */
/* .line 403 */
} // .end local v1 # "size":I
} // .end local v2 # "i":I
} // :cond_3
/* monitor-exit v0 */
/* .line 404 */
return;
/* .line 403 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void writeSoleCalculateApp ( android.os.Parcel p0, Long p1 ) {
/* .locals 5 */
/* .param p1, "out" # Landroid/os/Parcel; */
/* .param p2, "elapsedRealtimeUs" # J */
/* .line 407 */
v0 = this.mCalculateAppMap;
v0 = (( android.util.SparseArray ) v0 ).size ( ); // invoke-virtual {v0}, Landroid/util/SparseArray;->size()I
/* .line 408 */
/* .local v0, "size":I */
(( android.os.Parcel ) p1 ).writeInt ( v0 ); // invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V
/* .line 409 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* if-ge v1, v0, :cond_2 */
/* .line 410 */
v2 = this.mCalculateAppMap;
(( android.util.SparseArray ) v2 ).valueAt ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v2, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp; */
/* .line 411 */
/* .local v2, "app":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp; */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 412 */
/* iget v3, v2, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->uid:I */
(( android.os.Parcel ) p1 ).writeInt ( v3 ); // invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V
/* .line 413 */
v3 = this.packageName;
(( android.os.Parcel ) p1 ).writeString ( v3 ); // invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 414 */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "j":I */
} // :goto_1
/* const/16 v4, 0x64 */
/* if-ge v3, v4, :cond_1 */
/* .line 415 */
v4 = this.screenBrightnessTimers;
/* aget-object v4, v4, v3 */
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 416 */
int v4 = 1; // const/4 v4, 0x1
(( android.os.Parcel ) p1 ).writeInt ( v4 ); // invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V
/* .line 417 */
v4 = this.screenBrightnessTimers;
/* aget-object v4, v4, v3 */
(( com.android.server.power.stats.BatteryStatsImpl$StopwatchTimer ) v4 ).writeSummaryFromParcelLocked ( p1, p2, p3 ); // invoke-virtual {v4, p1, p2, p3}, Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;->writeSummaryFromParcelLocked(Landroid/os/Parcel;J)V
/* .line 419 */
} // :cond_0
int v4 = 0; // const/4 v4, 0x0
(( android.os.Parcel ) p1 ).writeInt ( v4 ); // invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V
/* .line 414 */
} // :goto_2
/* add-int/lit8 v3, v3, 0x1 */
/* .line 409 */
} // .end local v2 # "app":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;
} // .end local v3 # "j":I
} // :cond_1
/* add-int/lit8 v1, v1, 0x1 */
/* .line 424 */
} // .end local v1 # "i":I
} // :cond_2
return;
} // .end method
/* # virtual methods */
public getAppScreenInfo ( android.os.BatteryStats p0, Long p1 ) {
/* .locals 16 */
/* .param p1, "batteryStats" # Landroid/os/BatteryStats; */
/* .param p2, "elapsedRealtimeMs" # J */
/* .line 264 */
/* move-object/from16 v1, p0 */
int v2 = 0; // const/4 v2, 0x0
/* .line 265 */
/* .local v2, "bytes":[B */
android.os.Parcel .obtain ( );
/* .line 267 */
/* .local v3, "out":Landroid/os/Parcel; */
try { // :try_start_0
v0 = this.mCalculateAppMap;
v0 = (( android.util.SparseArray ) v0 ).size ( ); // invoke-virtual {v0}, Landroid/util/SparseArray;->size()I
int v4 = 0; // const/4 v4, 0x0
/* if-lez v0, :cond_3 */
/* .line 268 */
v0 = this.mCalculateAppMap;
v0 = (( android.util.SparseArray ) v0 ).size ( ); // invoke-virtual {v0}, Landroid/util/SparseArray;->size()I
(( android.os.Parcel ) v3 ).writeInt ( v0 ); // invoke-virtual {v3, v0}, Landroid/os/Parcel;->writeInt(I)V
/* .line 271 */
/* const/16 v0, 0x64 */
/* new-array v5, v0, [J */
/* .line 272 */
/* .local v5, "binTimes":[J */
int v6 = 0; // const/4 v6, 0x0
/* .local v6, "i":I */
} // :goto_0
v7 = this.mCalculateAppMap;
v7 = (( android.util.SparseArray ) v7 ).size ( ); // invoke-virtual {v7}, Landroid/util/SparseArray;->size()I
/* if-ge v6, v7, :cond_2 */
/* .line 273 */
v7 = this.mCalculateAppMap;
(( android.util.SparseArray ) v7 ).valueAt ( v6 ); // invoke-virtual {v7, v6}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v7, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp; */
/* .line 274 */
/* .local v7, "app":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp; */
if ( v7 != null) { // if-eqz v7, :cond_1
/* .line 275 */
v8 = this.packageName;
/* .line 276 */
/* .local v8, "pkgName":Ljava/lang/String; */
/* iget v9, v7, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->uid:I */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* const-wide/16 v10, 0x3e8 */
/* mul-long v12, p2, v10 */
/* move-object/from16 v14, p1 */
try { // :try_start_1
/* invoke-direct {v1, v9, v14, v12, v13}, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->getProcessForegroundTimeMs(ILandroid/os/BatteryStats;J)J */
/* move-result-wide v12 */
/* .line 278 */
/* .local v12, "fgTimeMs":J */
int v9 = 0; // const/4 v9, 0x0
/* .local v9, "bin":I */
} // :goto_1
/* if-ge v9, v0, :cond_0 */
/* .line 279 */
v15 = this.screenBrightnessTimers;
/* aget-object v15, v15, v9 */
/* mul-long v0, p2, v10 */
(( com.android.server.power.stats.BatteryStatsImpl$StopwatchTimer ) v15 ).getTotalTimeLocked ( v0, v1, v4 ); // invoke-virtual {v15, v0, v1, v4}, Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;->getTotalTimeLocked(JI)J
/* move-result-wide v0 */
/* div-long/2addr v0, v10 */
/* aput-wide v0, v5, v9 */
/* .line 278 */
/* add-int/lit8 v9, v9, 0x1 */
/* const/16 v0, 0x64 */
/* move-object/from16 v1, p0 */
/* .line 283 */
} // .end local v9 # "bin":I
} // :cond_0
(( android.os.Parcel ) v3 ).writeString ( v8 ); // invoke-virtual {v3, v8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 284 */
(( android.os.Parcel ) v3 ).writeLong ( v12, v13 ); // invoke-virtual {v3, v12, v13}, Landroid/os/Parcel;->writeLong(J)V
/* .line 285 */
(( android.os.Parcel ) v3 ).writeLongArray ( v5 ); // invoke-virtual {v3, v5}, Landroid/os/Parcel;->writeLongArray([J)V
/* .line 274 */
} // .end local v8 # "pkgName":Ljava/lang/String;
} // .end local v12 # "fgTimeMs":J
} // :cond_1
/* move-object/from16 v14, p1 */
/* .line 272 */
} // .end local v7 # "app":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;
} // :goto_2
/* add-int/lit8 v6, v6, 0x1 */
/* const/16 v0, 0x64 */
/* move-object/from16 v1, p0 */
} // :cond_2
/* move-object/from16 v14, p1 */
/* .line 288 */
} // .end local v5 # "binTimes":[J
} // .end local v6 # "i":I
/* .line 289 */
} // :cond_3
/* move-object/from16 v14, p1 */
(( android.os.Parcel ) v3 ).writeInt ( v4 ); // invoke-virtual {v3, v4}, Landroid/os/Parcel;->writeInt(I)V
/* .line 291 */
} // :goto_3
(( android.os.Parcel ) v3 ).marshall ( ); // invoke-virtual {v3}, Landroid/os/Parcel;->marshall()[B
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* move-object v2, v0 */
/* .line 292 */
/* :catch_0 */
/* move-exception v0 */
/* .line 295 */
/* :catchall_0 */
/* move-exception v0 */
/* move-object/from16 v14, p1 */
/* .line 292 */
/* :catch_1 */
/* move-exception v0 */
/* move-object/from16 v14, p1 */
/* .line 293 */
/* .local v0, "e":Ljava/lang/Exception; */
} // :goto_4
try { // :try_start_2
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* .line 295 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_5
(( android.os.Parcel ) v3 ).recycle ( ); // invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V
/* .line 296 */
/* nop */
/* .line 297 */
/* .line 295 */
/* :catchall_1 */
/* move-exception v0 */
} // :goto_6
(( android.os.Parcel ) v3 ).recycle ( ); // invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V
/* .line 296 */
/* throw v0 */
} // .end method
public Integer getDefaultDataSlotId ( ) {
/* .locals 1 */
/* .line 116 */
miui.telephony.SubscriptionManager .getDefault ( );
v0 = (( miui.telephony.SubscriptionManager ) v0 ).getDefaultDataSlotId ( ); // invoke-virtual {v0}, Lmiui/telephony/SubscriptionManager;->getDefaultDataSlotId()I
} // .end method
public android.util.SparseArray getScreenCalculateMap ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Landroid/util/SparseArray<", */
/* "Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 427 */
v0 = this.mCalculateAppMap;
} // .end method
public getTargetScreenInfo ( java.lang.String p0, android.os.BatteryStats p1, Long p2 ) {
/* .locals 16 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .param p2, "batteryStats" # Landroid/os/BatteryStats; */
/* .param p3, "elapsedRealtimeMs" # J */
/* .line 302 */
/* move-object/from16 v1, p0 */
int v2 = 0; // const/4 v2, 0x0
/* .line 303 */
/* .local v2, "bytes":[B */
android.os.Parcel .obtain ( );
/* .line 305 */
/* .local v3, "out":Landroid/os/Parcel; */
try { // :try_start_0
v0 = this.mCalculateAppMap;
v0 = (( android.util.SparseArray ) v0 ).size ( ); // invoke-virtual {v0}, Landroid/util/SparseArray;->size()I
int v4 = 0; // const/4 v4, 0x0
/* if-lez v0, :cond_5 */
/* .line 306 */
v0 = this.mCalculateAppMap;
v0 = (( android.util.SparseArray ) v0 ).size ( ); // invoke-virtual {v0}, Landroid/util/SparseArray;->size()I
(( android.os.Parcel ) v3 ).writeInt ( v0 ); // invoke-virtual {v3, v0}, Landroid/os/Parcel;->writeInt(I)V
/* .line 308 */
/* const/16 v0, 0x64 */
/* new-array v5, v0, [J */
/* .line 309 */
/* .local v5, "binTimes":[J */
int v6 = 0; // const/4 v6, 0x0
/* .line 310 */
/* .local v6, "app":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp; */
int v7 = 0; // const/4 v7, 0x0
/* .line 311 */
/* .local v7, "isExist":Z */
int v8 = 0; // const/4 v8, 0x0
/* .local v8, "i":I */
} // :goto_0
v9 = this.mCalculateAppMap;
v9 = (( android.util.SparseArray ) v9 ).size ( ); // invoke-virtual {v9}, Landroid/util/SparseArray;->size()I
/* if-ge v8, v9, :cond_2 */
/* .line 312 */
v9 = this.mCalculateAppMap;
(( android.util.SparseArray ) v9 ).valueAt ( v8 ); // invoke-virtual {v9, v8}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v9, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp; */
/* move-object v6, v9 */
/* .line 313 */
if ( v6 != null) { // if-eqz v6, :cond_0
v9 = this.packageName;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_2 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* move-object/from16 v10, p1 */
try { // :try_start_1
v9 = android.text.TextUtils .equals ( v9,v10 );
if ( v9 != null) { // if-eqz v9, :cond_1
/* .line 314 */
int v7 = 1; // const/4 v7, 0x1
/* .line 315 */
/* .line 313 */
} // :cond_0
/* move-object/from16 v10, p1 */
/* .line 311 */
} // :cond_1
/* add-int/lit8 v8, v8, 0x1 */
} // :cond_2
/* move-object/from16 v10, p1 */
/* .line 318 */
} // .end local v8 # "i":I
} // :goto_1
if ( v7 != null) { // if-eqz v7, :cond_4
/* .line 319 */
int v8 = 1; // const/4 v8, 0x1
(( android.os.Parcel ) v3 ).writeInt ( v8 ); // invoke-virtual {v3, v8}, Landroid/os/Parcel;->writeInt(I)V
/* .line 320 */
/* iget v8, v6, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->uid:I */
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* const-wide/16 v11, 0x3e8 */
/* mul-long v13, p3, v11 */
/* move-object/from16 v9, p2 */
try { // :try_start_2
/* invoke-direct {v1, v8, v9, v13, v14}, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->getProcessForegroundTimeMs(ILandroid/os/BatteryStats;J)J */
/* move-result-wide v13 */
/* .line 322 */
/* .local v13, "fgTimeMs":J */
int v8 = 0; // const/4 v8, 0x0
/* .local v8, "bin":I */
} // :goto_2
/* if-ge v8, v0, :cond_3 */
/* .line 323 */
v15 = this.screenBrightnessTimers;
/* aget-object v15, v15, v8 */
/* mul-long v0, p3, v11 */
(( com.android.server.power.stats.BatteryStatsImpl$StopwatchTimer ) v15 ).getTotalTimeLocked ( v0, v1, v4 ); // invoke-virtual {v15, v0, v1, v4}, Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;->getTotalTimeLocked(JI)J
/* move-result-wide v0 */
/* div-long/2addr v0, v11 */
/* aput-wide v0, v5, v8 */
/* .line 322 */
/* add-int/lit8 v8, v8, 0x1 */
/* const/16 v0, 0x64 */
/* move-object/from16 v1, p0 */
/* .line 327 */
} // .end local v8 # "bin":I
} // :cond_3
(( android.os.Parcel ) v3 ).writeLong ( v13, v14 ); // invoke-virtual {v3, v13, v14}, Landroid/os/Parcel;->writeLong(J)V
/* .line 328 */
(( android.os.Parcel ) v3 ).writeLongArray ( v5 ); // invoke-virtual {v3, v5}, Landroid/os/Parcel;->writeLongArray([J)V
/* .line 339 */
} // .end local v5 # "binTimes":[J
} // .end local v6 # "app":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;
} // .end local v7 # "isExist":Z
} // .end local v13 # "fgTimeMs":J
/* :catchall_0 */
/* move-exception v0 */
/* .line 336 */
/* :catch_0 */
/* move-exception v0 */
/* .line 330 */
/* .restart local v5 # "binTimes":[J */
/* .restart local v6 # "app":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp; */
/* .restart local v7 # "isExist":Z */
} // :cond_4
/* move-object/from16 v9, p2 */
(( android.os.Parcel ) v3 ).writeInt ( v4 ); // invoke-virtual {v3, v4}, Landroid/os/Parcel;->writeInt(I)V
/* .line 332 */
} // .end local v5 # "binTimes":[J
} // .end local v6 # "app":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;
} // .end local v7 # "isExist":Z
} // :goto_3
/* .line 333 */
} // :cond_5
/* move-object/from16 v10, p1 */
/* move-object/from16 v9, p2 */
(( android.os.Parcel ) v3 ).writeInt ( v4 ); // invoke-virtual {v3, v4}, Landroid/os/Parcel;->writeInt(I)V
/* .line 335 */
} // :goto_4
(( android.os.Parcel ) v3 ).marshall ( ); // invoke-virtual {v3}, Landroid/os/Parcel;->marshall()[B
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_1 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_2 */
/* move-object v2, v0 */
/* .line 336 */
/* :catch_1 */
/* move-exception v0 */
/* .line 339 */
/* :catchall_1 */
/* move-exception v0 */
/* move-object/from16 v10, p1 */
} // :goto_5
/* move-object/from16 v9, p2 */
/* .line 336 */
/* :catch_2 */
/* move-exception v0 */
/* move-object/from16 v10, p1 */
} // :goto_6
/* move-object/from16 v9, p2 */
/* .line 337 */
/* .local v0, "e":Ljava/lang/Exception; */
} // :goto_7
try { // :try_start_3
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_2 */
/* .line 339 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_8
(( android.os.Parcel ) v3 ).recycle ( ); // invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V
/* .line 340 */
/* nop */
/* .line 341 */
/* .line 339 */
/* :catchall_2 */
/* move-exception v0 */
} // :goto_9
(( android.os.Parcel ) v3 ).recycle ( ); // invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V
/* .line 340 */
/* throw v0 */
} // .end method
public void handleMessage ( android.os.Message p0 ) {
/* .locals 1 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 128 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* packed-switch v0, :pswitch_data_0 */
/* .line 142 */
/* :pswitch_0 */
/* iget v0, p1, Landroid/os/Message;->arg1:I */
(( com.android.server.power.stats.BatteryStatsManagerStubImpl ) p0 ).noteStopGpsLocked ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->noteStopGpsLocked(I)V
/* .line 143 */
/* .line 139 */
/* :pswitch_1 */
/* iget v0, p1, Landroid/os/Message;->arg1:I */
(( com.android.server.power.stats.BatteryStatsManagerStubImpl ) p0 ).noteStartGpsLocked ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->noteStartGpsLocked(I)V
/* .line 140 */
/* .line 136 */
/* :pswitch_2 */
(( com.android.server.power.stats.BatteryStatsManagerStubImpl ) p0 ).noteResetAudioLocked ( ); // invoke-virtual {p0}, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->noteResetAudioLocked()V
/* .line 137 */
/* .line 133 */
/* :pswitch_3 */
/* iget v0, p1, Landroid/os/Message;->arg1:I */
(( com.android.server.power.stats.BatteryStatsManagerStubImpl ) p0 ).noteAudioOffLocked ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->noteAudioOffLocked(I)V
/* .line 134 */
/* .line 130 */
/* :pswitch_4 */
/* iget v0, p1, Landroid/os/Message;->arg1:I */
(( com.android.server.power.stats.BatteryStatsManagerStubImpl ) p0 ).noteAudioOnLocked ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->noteAudioOnLocked(I)V
/* .line 131 */
/* nop */
/* .line 147 */
} // :goto_0
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x3e8 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public void noteActivityPausedLocked ( Integer p0, Long p1, Long p2 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .param p2, "elapsedRealtimeMs" # J */
/* .param p4, "uptimeMs" # J */
/* .line 240 */
v0 = this.mCalculateAppMap;
v0 = (( android.util.SparseArray ) v0 ).contains ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/SparseArray;->contains(I)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 241 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mCurrentSoleUid:I */
/* .line 242 */
v1 = this.mCalculateAppMap;
(( android.util.SparseArray ) v1 ).get ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v1, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp; */
/* .line 243 */
/* .local v1, "app":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* iget v2, v1, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->oldBin:I */
/* if-le v2, v0, :cond_0 */
/* .line 244 */
v2 = this.screenBrightnessTimers;
/* iget v3, v1, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->oldBin:I */
/* aget-object v2, v2, v3 */
(( com.android.server.power.stats.BatteryStatsImpl$StopwatchTimer ) v2 ).stopRunningLocked ( p2, p3 ); // invoke-virtual {v2, p2, p3}, Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;->stopRunningLocked(J)V
/* .line 245 */
/* iput v0, v1, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->oldBin:I */
/* .line 248 */
} // .end local v1 # "app":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;
} // :cond_0
return;
} // .end method
public void noteActivityResumedLocked ( android.os.BatteryStats p0, Integer p1, Long p2, Long p3, java.lang.String p4 ) {
/* .locals 6 */
/* .param p1, "batteryStats" # Landroid/os/BatteryStats; */
/* .param p2, "uid" # I */
/* .param p3, "elapsedRealtimeMs" # J */
/* .param p5, "uptimeMs" # J */
/* .param p7, "packageName" # Ljava/lang/String; */
/* .line 206 */
v0 = android.os.UserHandle .isApp ( p2 );
/* if-nez v0, :cond_0 */
/* .line 207 */
return;
/* .line 209 */
} // :cond_0
/* iput p2, p0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mCurrentSoleUid:I */
/* .line 211 */
/* move-object v0, p1 */
/* check-cast v0, Lcom/android/server/power/stats/BatteryStatsImpl; */
/* .line 212 */
/* .local v0, "impl":Lcom/android/server/power/stats/BatteryStatsImpl; */
v1 = this.mCalculateAppMap;
/* monitor-enter v1 */
/* .line 213 */
try { // :try_start_0
v2 = this.mCalculateAppMap;
v2 = (( android.util.SparseArray ) v2 ).contains ( p2 ); // invoke-virtual {v2, p2}, Landroid/util/SparseArray;->contains(I)Z
int v3 = -1; // const/4 v3, -0x1
/* if-nez v2, :cond_2 */
/* .line 214 */
/* new-instance v2, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp; */
v4 = this.mClock;
v5 = this.mOnBatteryTimeBase;
/* invoke-direct {v2, v4, v5}, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;-><init>(Lcom/android/internal/os/Clock;Lcom/android/server/power/stats/BatteryStatsImpl$TimeBase;)V */
/* .line 215 */
/* .local v2, "app":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp; */
/* iput p2, v2, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->uid:I */
/* .line 216 */
this.packageName = p7;
/* .line 217 */
/* iget v4, v0, Lcom/android/server/power/stats/BatteryStatsImpl;->mScreenBrightnessBin:I */
/* if-le v4, v3, :cond_1 */
/* .line 218 */
v3 = this.screenBrightnessTimers;
/* iget v4, v0, Lcom/android/server/power/stats/BatteryStatsImpl;->mScreenBrightnessBin:I */
/* aget-object v3, v3, v4 */
(( com.android.server.power.stats.BatteryStatsImpl$StopwatchTimer ) v3 ).startRunningLocked ( p3, p4 ); // invoke-virtual {v3, p3, p4}, Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;->startRunningLocked(J)V
/* .line 219 */
/* iget v3, v0, Lcom/android/server/power/stats/BatteryStatsImpl;->mScreenBrightnessBin:I */
/* iput v3, v2, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->oldBin:I */
/* .line 221 */
} // :cond_1
v3 = this.mCalculateAppMap;
(( android.util.SparseArray ) v3 ).put ( p2, v2 ); // invoke-virtual {v3, p2, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 223 */
} // .end local v2 # "app":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;
} // :cond_2
v2 = this.mCalculateAppMap;
(( android.util.SparseArray ) v2 ).get ( p2 ); // invoke-virtual {v2, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v2, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp; */
/* .line 224 */
/* .restart local v2 # "app":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp; */
if ( v2 != null) { // if-eqz v2, :cond_3
/* iget v4, v0, Lcom/android/server/power/stats/BatteryStatsImpl;->mScreenBrightnessBin:I */
/* if-le v4, v3, :cond_3 */
/* .line 225 */
v3 = this.screenBrightnessTimers;
/* iget v4, v0, Lcom/android/server/power/stats/BatteryStatsImpl;->mScreenBrightnessBin:I */
/* aget-object v3, v3, v4 */
(( com.android.server.power.stats.BatteryStatsImpl$StopwatchTimer ) v3 ).startRunningLocked ( p3, p4 ); // invoke-virtual {v3, p3, p4}, Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;->startRunningLocked(J)V
/* .line 226 */
/* iget v3, v0, Lcom/android/server/power/stats/BatteryStatsImpl;->mScreenBrightnessBin:I */
/* iput v3, v2, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->oldBin:I */
/* .line 229 */
} // :cond_3
} // :goto_0
/* monitor-exit v1 */
/* .line 236 */
return;
/* .line 229 */
} // .end local v2 # "app":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
public void noteAudioOffLocked ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .line 41 */
v0 = this.mActiveCallback;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 42 */
/* .line 43 */
} // :cond_0
return;
} // .end method
public void noteAudioOnLocked ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .line 35 */
v0 = this.mActiveCallback;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 36 */
/* .line 37 */
} // :cond_0
return;
} // .end method
public void noteMuteAudioInNeed ( Integer p0, Integer p1, Integer p2, Integer p3 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "session" # I */
/* .param p4, "status" # I */
/* .line 378 */
com.miui.whetstone.server.WhetstoneActivityManagerService .getSingletonService ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 379 */
com.miui.whetstone.server.WhetstoneActivityManagerService .getSingletonService ( );
(( com.miui.whetstone.server.WhetstoneActivityManagerService ) v0 ).notifyMuteAudioInNeed ( p1, p2, p3, p4 ); // invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/whetstone/server/WhetstoneActivityManagerService;->notifyMuteAudioInNeed(IIII)V
/* .line 381 */
} // :cond_0
return;
} // .end method
public void noteResetAudioLocked ( ) {
/* .locals 1 */
/* .line 47 */
v0 = this.mActiveCallback;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 48 */
/* .line 49 */
} // :cond_0
return;
} // .end method
public void noteScreenBrightnessLocked ( android.os.BatteryStats p0, Integer p1, Long p2, Long p3 ) {
/* .locals 3 */
/* .param p1, "batteryStats" # Landroid/os/BatteryStats; */
/* .param p2, "bin" # I */
/* .param p3, "elapsedRealtimeMs" # J */
/* .param p5, "uptimeMs" # J */
/* .line 252 */
/* iget v0, p0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mCurrentSoleUid:I */
int v1 = -1; // const/4 v1, -0x1
/* if-eq v0, v1, :cond_0 */
/* .line 253 */
v2 = this.mCalculateAppMap;
(( android.util.SparseArray ) v2 ).get ( v0 ); // invoke-virtual {v2, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp; */
/* .line 254 */
/* .local v0, "app":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget v2, v0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->oldBin:I */
/* if-le v2, v1, :cond_0 */
/* .line 255 */
v1 = this.screenBrightnessTimers;
/* iget v2, v0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->oldBin:I */
/* aget-object v1, v1, v2 */
(( com.android.server.power.stats.BatteryStatsImpl$StopwatchTimer ) v1 ).stopRunningLocked ( p3, p4 ); // invoke-virtual {v1, p3, p4}, Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;->stopRunningLocked(J)V
/* .line 256 */
v1 = this.screenBrightnessTimers;
/* aget-object v1, v1, p2 */
(( com.android.server.power.stats.BatteryStatsImpl$StopwatchTimer ) v1 ).startRunningLocked ( p3, p4 ); // invoke-virtual {v1, p3, p4}, Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;->startRunningLocked(J)V
/* .line 257 */
/* iput p2, v0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->oldBin:I */
/* .line 260 */
} // .end local v0 # "app":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;
} // :cond_0
return;
} // .end method
public void noteStartAudioInNeed ( Integer p0, Integer p1, Integer p2, Integer p3, Integer p4 ) {
/* .locals 7 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "session" # I */
/* .param p4, "port" # I */
/* .param p5, "type" # I */
/* .line 364 */
com.miui.whetstone.server.WhetstoneActivityManagerService .getSingletonService ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 365 */
com.miui.whetstone.server.WhetstoneActivityManagerService .getSingletonService ( );
/* move v2, p1 */
/* move v3, p2 */
/* move v4, p3 */
/* move v5, p4 */
/* move v6, p5 */
/* invoke-virtual/range {v1 ..v6}, Lcom/miui/whetstone/server/WhetstoneActivityManagerService;->notifyStartAudioInNeed(IIIII)V */
/* .line 367 */
} // :cond_0
return;
} // .end method
public void noteStartGpsLocked ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .line 53 */
v0 = this.mActiveCallback;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 54 */
/* .line 55 */
} // :cond_0
return;
} // .end method
public void noteStopAudioInNeed ( Integer p0, Integer p1, Integer p2, Integer p3, Integer p4 ) {
/* .locals 7 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "session" # I */
/* .param p4, "port" # I */
/* .param p5, "type" # I */
/* .line 371 */
com.miui.whetstone.server.WhetstoneActivityManagerService .getSingletonService ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 372 */
com.miui.whetstone.server.WhetstoneActivityManagerService .getSingletonService ( );
/* move v2, p1 */
/* move v3, p2 */
/* move v4, p3 */
/* move v5, p4 */
/* move v6, p5 */
/* invoke-virtual/range {v1 ..v6}, Lcom/miui/whetstone/server/WhetstoneActivityManagerService;->notifyStopAudioInNeed(IIIII)V */
/* .line 374 */
} // :cond_0
return;
} // .end method
public void noteStopGpsLocked ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .line 59 */
v0 = this.mActiveCallback;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 60 */
/* .line 61 */
} // :cond_0
return;
} // .end method
public void noteSyncStart ( java.lang.String p0, Integer p1, Boolean p2 ) {
/* .locals 7 */
/* .param p1, "name" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .param p3, "start" # Z */
/* .line 70 */
/* const/16 v0, 0x2710 */
/* if-lt p2, v0, :cond_2 */
/* const/16 v0, 0x4e1f */
/* if-le p2, v0, :cond_0 */
/* .line 72 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* new-array v0, v0, [I */
/* .line 73 */
/* .local v0, "uids":[I */
int v1 = 0; // const/4 v1, 0x0
/* aput p2, v0, v1 */
/* .line 74 */
com.miui.whetstone.PowerKeeperPolicy .getInstance ( );
int v3 = 4; // const/4 v3, 0x4
/* .line 75 */
if ( p3 != null) { // if-eqz p3, :cond_1
/* const-string/jumbo v2, "syncstart" */
} // :cond_1
/* const-string/jumbo v2, "syncend" */
} // :goto_0
/* move-object v4, v2 */
/* .line 74 */
/* move v2, p3 */
/* move-object v5, p1 */
/* move-object v6, v0 */
/* invoke-virtual/range {v1 ..v6}, Lcom/miui/whetstone/PowerKeeperPolicy;->notifySyncEvent(IILjava/lang/String;Ljava/lang/String;[I)V */
/* .line 76 */
return;
/* .line 71 */
} // .end local v0 # "uids":[I
} // :cond_2
} // :goto_1
return;
} // .end method
public void noteVideoFps ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "fps" # I */
/* .line 453 */
com.miui.whetstone.server.WhetstoneActivityManagerService .getSingletonService ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 454 */
com.miui.whetstone.server.WhetstoneActivityManagerService .getSingletonService ( );
(( com.miui.whetstone.server.WhetstoneActivityManagerService ) v0 ).notifyVideoFps ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/whetstone/server/WhetstoneActivityManagerService;->notifyVideoFps(II)V
/* .line 456 */
} // :cond_0
return;
} // .end method
public void notifyDetailsWakeUp ( java.lang.String p0, Integer p1, java.lang.String p2 ) {
/* .locals 3 */
/* .param p1, "type" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .param p3, "name" # Ljava/lang/String; */
/* .line 80 */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 81 */
/* .local v0, "b":Landroid/os/Bundle; */
/* const-string/jumbo v1, "uid" */
(( android.os.Bundle ) v0 ).putInt ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 82 */
/* const-string/jumbo v1, "type" */
(( android.os.Bundle ) v0 ).putString ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 83 */
final String v1 = "name"; // const-string v1, "name"
(( android.os.Bundle ) v0 ).putString ( v1, p3 ); // invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 84 */
com.miui.whetstone.PowerKeeperPolicy .getInstance ( );
/* const/16 v2, 0x8 */
(( com.miui.whetstone.PowerKeeperPolicy ) v1 ).notifyEvent ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Lcom/miui/whetstone/PowerKeeperPolicy;->notifyEvent(ILandroid/os/Bundle;)V
/* .line 85 */
return;
} // .end method
public void notifyEvent ( Integer p0, android.os.Bundle p1 ) {
/* .locals 5 */
/* .param p1, "resId" # I */
/* .param p2, "bundle" # Landroid/os/Bundle; */
/* .line 460 */
/* if-nez p2, :cond_0 */
/* .line 461 */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* move-object p2, v0 */
/* .line 463 */
} // :cond_0
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v0 */
/* .line 464 */
/* .local v0, "elapsedRealtimeMs":J */
/* const-string/jumbo v2, "startTime" */
(( android.os.Bundle ) p2 ).putLong ( v2, v0, v1 ); // invoke-virtual {p2, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V
/* .line 465 */
com.miui.whetstone.server.WhetstoneActivityManagerService .getSingletonService ( );
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 466 */
com.miui.whetstone.server.WhetstoneActivityManagerService .getSingletonService ( );
(( com.miui.whetstone.server.WhetstoneActivityManagerService ) v2 ).notifyEvent ( p1, p2 ); // invoke-virtual {v2, p1, p2}, Lcom/miui/whetstone/server/WhetstoneActivityManagerService;->notifyEvent(ILandroid/os/Bundle;)V
/* .line 468 */
} // :cond_1
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "notifyEvent for resId="; // const-string v3, "notifyEvent for resId="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " used "; // const-string v3, " used "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v3 */
/* sub-long/2addr v3, v0 */
(( java.lang.StringBuilder ) v2 ).append ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v3 = "ms"; // const-string v3, "ms"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "BatteryStatsManagerStub"; // const-string v3, "BatteryStatsManagerStub"
android.util.Log .d ( v3,v2 );
/* .line 469 */
return;
} // .end method
public void notifyWakeUp ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "reason" # Ljava/lang/String; */
/* .line 89 */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 90 */
/* .local v0, "b":Landroid/os/Bundle; */
final String v1 = "reason"; // const-string v1, "reason"
(( android.os.Bundle ) v0 ).putString ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 91 */
com.miui.whetstone.PowerKeeperPolicy .getInstance ( );
/* const/16 v2, 0xc */
(( com.miui.whetstone.PowerKeeperPolicy ) v1 ).notifyEvent ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Lcom/miui/whetstone/PowerKeeperPolicy;->notifyEvent(ILandroid/os/Bundle;)V
/* .line 92 */
return;
} // .end method
public void readFromParcelLocked ( android.os.Parcel p0, android.os.BatteryStats p1 ) {
/* .locals 1 */
/* .param p1, "in" # Landroid/os/Parcel; */
/* .param p2, "batteryStats" # Landroid/os/BatteryStats; */
/* .line 174 */
/* move-object v0, p2 */
/* check-cast v0, Lcom/android/server/power/stats/BatteryStatsImpl; */
/* invoke-direct {p0, p1, v0}, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->readSoleCalculateApp(Landroid/os/Parcel;Lcom/android/server/power/stats/BatteryStatsImpl;)V */
/* .line 175 */
return;
} // .end method
public void readSummaryFromParcel ( android.os.Parcel p0, android.os.BatteryStats p1 ) {
/* .locals 1 */
/* .param p1, "in" # Landroid/os/Parcel; */
/* .param p2, "batteryStats" # Landroid/os/BatteryStats; */
/* .line 164 */
/* move-object v0, p2 */
/* check-cast v0, Lcom/android/server/power/stats/BatteryStatsImpl; */
/* invoke-direct {p0, p1, v0}, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->readSoleCalculateApp(Landroid/os/Parcel;Lcom/android/server/power/stats/BatteryStatsImpl;)V */
/* .line 165 */
return;
} // .end method
public void reportActiveEvent ( android.os.Handler p0, Integer p1, Integer p2 ) {
/* .locals 1 */
/* .param p1, "handler" # Landroid/os/Handler; */
/* .param p2, "what" # I */
/* .param p3, "uid" # I */
/* .line 121 */
(( android.os.Handler ) p1 ).obtainMessage ( p2 ); // invoke-virtual {p1, p2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 122 */
/* .local v0, "m":Landroid/os/Message; */
/* iput p3, v0, Landroid/os/Message;->arg1:I */
/* .line 123 */
(( android.os.Handler ) p1 ).sendMessage ( v0 ); // invoke-virtual {p1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 124 */
return;
} // .end method
public void resetAllStatsLocked ( Long p0, Long p1 ) {
/* .locals 7 */
/* .param p1, "uptimeMillis" # J */
/* .param p3, "elapsedRealtimeMillis" # J */
/* .line 184 */
v0 = this.mCalculateAppMap;
v0 = (( android.util.SparseArray ) v0 ).size ( ); // invoke-virtual {v0}, Landroid/util/SparseArray;->size()I
/* if-nez v0, :cond_0 */
return;
/* .line 185 */
} // :cond_0
/* const-wide/16 v0, 0x3e8 */
/* mul-long/2addr v0, p3 */
/* .line 186 */
/* .local v0, "elapsedRealtimeUs":J */
int v2 = 0; // const/4 v2, 0x0
/* .line 187 */
/* .local v2, "currentApp":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp; */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
v4 = this.mCalculateAppMap;
v4 = (( android.util.SparseArray ) v4 ).size ( ); // invoke-virtual {v4}, Landroid/util/SparseArray;->size()I
/* if-ge v3, v4, :cond_2 */
/* .line 188 */
v4 = this.mCalculateAppMap;
(( android.util.SparseArray ) v4 ).valueAt ( v3 ); // invoke-virtual {v4, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v4, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp; */
/* .line 189 */
/* .local v4, "app":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp; */
/* iget v5, v4, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->uid:I */
/* iget v6, p0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mCurrentSoleUid:I */
/* if-ne v5, v6, :cond_1 */
/* .line 190 */
/* move-object v2, v4 */
/* .line 192 */
} // :cond_1
(( com.android.server.power.stats.BatteryStatsManagerStubImpl$SoleCalculateApp ) v4 ).reset ( v0, v1 ); // invoke-virtual {v4, v0, v1}, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->reset(J)V
/* .line 187 */
} // .end local v4 # "app":Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;
/* add-int/lit8 v3, v3, 0x1 */
/* .line 194 */
} // .end local v3 # "i":I
} // :cond_2
v3 = this.mCalculateAppMap;
/* monitor-enter v3 */
/* .line 195 */
try { // :try_start_0
v4 = this.mCalculateAppMap;
(( android.util.SparseArray ) v4 ).clear ( ); // invoke-virtual {v4}, Landroid/util/SparseArray;->clear()V
/* .line 196 */
/* iget v4, p0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mCurrentSoleUid:I */
int v5 = -1; // const/4 v5, -0x1
/* if-eq v4, v5, :cond_3 */
if ( v2 != null) { // if-eqz v2, :cond_3
/* iget v4, v2, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->oldBin:I */
/* if-le v4, v5, :cond_3 */
/* .line 197 */
v4 = this.mCalculateAppMap;
/* iget v5, p0, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->mCurrentSoleUid:I */
(( android.util.SparseArray ) v4 ).put ( v5, v2 ); // invoke-virtual {v4, v5, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 198 */
v4 = this.screenBrightnessTimers;
/* iget v5, v2, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl$SoleCalculateApp;->oldBin:I */
/* aget-object v4, v4, v5 */
(( com.android.server.power.stats.BatteryStatsImpl$StopwatchTimer ) v4 ).startRunningLocked ( p3, p4 ); // invoke-virtual {v4, p3, p4}, Lcom/android/server/power/stats/BatteryStatsImpl$StopwatchTimer;->startRunningLocked(J)V
/* .line 200 */
} // :cond_3
/* monitor-exit v3 */
/* .line 201 */
return;
/* .line 200 */
/* :catchall_0 */
/* move-exception v4 */
/* monitor-exit v3 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v4 */
} // .end method
public void setActiveCallback ( com.android.server.power.stats.BatteryStatsManagerStub$ActiveCallback p0 ) {
/* .locals 0 */
/* .param p1, "callback" # Lcom/android/server/power/stats/BatteryStatsManagerStub$ActiveCallback; */
/* .line 65 */
this.mActiveCallback = p1;
/* .line 66 */
return;
} // .end method
public void writeSummaryToParcel ( android.os.Parcel p0, Long p1 ) {
/* .locals 0 */
/* .param p1, "out" # Landroid/os/Parcel; */
/* .param p2, "elapsedRealtimeUs" # J */
/* .line 169 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->writeSoleCalculateApp(Landroid/os/Parcel;J)V */
/* .line 170 */
return;
} // .end method
public void writeToParcelLocked ( android.os.Parcel p0, Long p1 ) {
/* .locals 0 */
/* .param p1, "out" # Landroid/os/Parcel; */
/* .param p2, "elapsedRealtimeUs" # J */
/* .line 179 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/power/stats/BatteryStatsManagerStubImpl;->writeSoleCalculateApp(Landroid/os/Parcel;J)V */
/* .line 180 */
return;
} // .end method
public void writeUidStatsToParcel ( android.os.BatteryStats p0, android.os.Parcel p1, Long p2 ) {
/* .locals 4 */
/* .param p1, "batteryStats" # Landroid/os/BatteryStats; */
/* .param p2, "out" # Landroid/os/Parcel; */
/* .param p3, "elapsedRealtimeUs" # J */
/* .line 151 */
(( android.os.BatteryStats ) p1 ).getUidStats ( ); // invoke-virtual {p1}, Landroid/os/BatteryStats;->getUidStats()Landroid/util/SparseArray;
/* .line 152 */
/* .local v0, "mUidStats":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/android/server/power/stats/BatteryStatsImpl$Uid;>;" */
v1 = (( android.util.SparseArray ) v0 ).size ( ); // invoke-virtual {v0}, Landroid/util/SparseArray;->size()I
/* .line 153 */
/* .local v1, "size":I */
(( android.os.Parcel ) p2 ).writeInt ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 154 */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
/* if-ge v2, v1, :cond_0 */
/* .line 155 */
v3 = (( android.util.SparseArray ) v0 ).keyAt ( v2 ); // invoke-virtual {v0, v2}, Landroid/util/SparseArray;->keyAt(I)I
(( android.os.Parcel ) p2 ).writeInt ( v3 ); // invoke-virtual {p2, v3}, Landroid/os/Parcel;->writeInt(I)V
/* .line 156 */
(( android.util.SparseArray ) v0 ).valueAt ( v2 ); // invoke-virtual {v0, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v3, Lcom/android/server/power/stats/BatteryStatsImpl$Uid; */
/* .line 158 */
/* .local v3, "uid":Lcom/android/server/power/stats/BatteryStatsImpl$Uid; */
(( com.android.server.power.stats.BatteryStatsImpl$Uid ) v3 ).writeToParcelLocked ( p2, p3, p4 ); // invoke-virtual {v3, p2, p3, p4}, Lcom/android/server/power/stats/BatteryStatsImpl$Uid;->writeToParcelLocked(Landroid/os/Parcel;J)V
/* .line 154 */
} // .end local v3 # "uid":Lcom/android/server/power/stats/BatteryStatsImpl$Uid;
/* add-int/lit8 v2, v2, 0x1 */
/* .line 160 */
} // .end local v2 # "i":I
} // :cond_0
return;
} // .end method
