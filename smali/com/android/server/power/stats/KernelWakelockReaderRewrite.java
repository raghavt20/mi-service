public class com.android.server.power.stats.KernelWakelockReaderRewrite {
	 /* .source "KernelWakelockReaderRewrite.java" */
	 /* # static fields */
	 private static final PROC_WAKELOCKS_FORMAT;
	 private static final java.lang.String TAG;
	 private static final WAKEUP_SOURCES_FORMAT;
	 private static Integer sKernelWakelockUpdateVersion;
	 private static final java.lang.String sWakelockFile;
	 private static final java.lang.String sWakeupSourceFile;
	 /* # instance fields */
	 Boolean br_flag;
	 private final mProcWakelocksData;
	 private final java.lang.String mProcWakelocksName;
	 /* # direct methods */
	 static com.android.server.power.stats.KernelWakelockReaderRewrite ( ) {
		 /* .locals 1 */
		 /* .line 18 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* .line 22 */
		 int v0 = 6; // const/4 v0, 0x6
		 /* new-array v0, v0, [I */
		 /* fill-array-data v0, :array_0 */
		 /* .line 32 */
		 int v0 = 7; // const/4 v0, 0x7
		 /* new-array v0, v0, [I */
		 /* fill-array-data v0, :array_1 */
		 return;
		 /* :array_0 */
		 /* .array-data 4 */
		 /* 0x1409 */
		 /* 0x2009 */
		 /* 0x9 */
		 /* 0x9 */
		 /* 0x9 */
		 /* 0x2009 */
	 } // .end array-data
	 /* :array_1 */
	 /* .array-data 4 */
	 /* 0x1009 */
	 /* 0x2109 */
	 /* 0x109 */
	 /* 0x109 */
	 /* 0x109 */
	 /* 0x109 */
	 /* 0x2109 */
} // .end array-data
} // .end method
public com.android.server.power.stats.KernelWakelockReaderRewrite ( ) {
/* .locals 2 */
/* .line 16 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 44 */
int v0 = 3; // const/4 v0, 0x3
/* new-array v1, v0, [Ljava/lang/String; */
this.mProcWakelocksName = v1;
/* .line 45 */
/* new-array v0, v0, [J */
this.mProcWakelocksData = v0;
/* .line 53 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/power/stats/KernelWakelockReaderRewrite;->br_flag:Z */
return;
} // .end method
private Integer readInfoCheck ( Object[] p0, Integer p1, Integer p2, Long p3 ) {
/* .locals 5 */
/* .param p1, "buffer" # [B */
/* .param p2, "len" # I */
/* .param p3, "cnt" # I */
/* .param p4, "startTime" # J */
/* .line 56 */
/* if-ltz p3, :cond_0 */
/* array-length v0, p1 */
/* sub-int/2addr v0, p2 */
/* if-lez v0, :cond_1 */
/* .line 57 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/power/stats/KernelWakelockReaderRewrite;->br_flag:Z */
/* .line 59 */
} // :cond_1
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* sub-long/2addr v0, p4 */
/* .line 60 */
/* .local v0, "readTime":J */
/* const-wide/16 v2, 0x64 */
/* cmp-long v2, v0, v2 */
final String v3 = "KernelWakelockReader"; // const-string v3, "KernelWakelockReader"
/* if-lez v2, :cond_2 */
/* .line 61 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Reading wakelock stats took "; // const-string v4, "Reading wakelock stats took "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0, v1 ); // invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v4 = "ms"; // const-string v4, "ms"
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v3,v2 );
/* .line 64 */
} // :cond_2
/* array-length v2, p1 */
/* if-lt p2, v2, :cond_3 */
/* .line 65 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Kernel wake locks exceeded buffer size "; // const-string v4, "Kernel wake locks exceeded buffer size "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " "; // const-string v4, " "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p3 ); // invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .v ( v3,v2 );
/* .line 67 */
} // :cond_3
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
/* if-ge v2, p2, :cond_5 */
/* .line 68 */
/* aget-byte v3, p1, v2 */
/* if-nez v3, :cond_4 */
/* .line 69 */
/* move p2, v2 */
/* .line 70 */
/* .line 67 */
} // :cond_4
/* add-int/lit8 v2, v2, 0x1 */
/* .line 73 */
} // .end local v2 # "i":I
} // :cond_5
} // :goto_1
} // .end method
/* # virtual methods */
public com.android.server.power.stats.KernelWakelockStats parseProcWakelocks ( Object[] p0, Integer p1, Boolean p2, com.android.server.power.stats.KernelWakelockStats p3 ) {
/* .locals 21 */
/* .param p1, "wlBuffer" # [B */
/* .param p2, "len" # I */
/* .param p3, "wakeup_sources" # Z */
/* .param p4, "staleStats" # Lcom/android/server/power/stats/KernelWakelockStats; */
/* .line 147 */
/* move-object/from16 v1, p0 */
/* move-object/from16 v9, p1 */
/* move/from16 v10, p2 */
/* move-object/from16 v11, p4 */
int v0 = 0; // const/4 v0, 0x0
/* move v12, v0 */
/* .local v12, "i":I */
} // :goto_0
/* const/16 v13, 0xa */
/* if-ge v12, v10, :cond_0 */
/* aget-byte v0, v9, v12 */
/* if-eq v0, v13, :cond_0 */
/* aget-byte v0, v9, v12 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* add-int/lit8 v12, v12, 0x1 */
/* .line 148 */
} // :cond_0
/* add-int/lit8 v0, v12, 0x1 */
/* move v2, v0 */
/* .local v2, "endIndex":I */
/* move v3, v0 */
/* .line 150 */
/* .local v3, "startIndex":I */
/* monitor-enter p0 */
/* .line 151 */
try { // :try_start_0
int v14 = 1; // const/4 v14, 0x1
/* add-int/2addr v0, v14 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_4 */
/* move v15, v3 */
/* .line 152 */
} // .end local v3 # "startIndex":I
/* .local v15, "startIndex":I */
} // :goto_1
/* if-ge v2, v10, :cond_b */
/* .line 153 */
/* move v0, v15 */
/* move v8, v0 */
/* .line 154 */
} // .end local v2 # "endIndex":I
/* .local v8, "endIndex":I */
} // :goto_2
/* if-ge v8, v10, :cond_1 */
try { // :try_start_1
/* aget-byte v0, v9, v8 */
/* if-eq v0, v13, :cond_1 */
/* aget-byte v0, v9, v8 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 155 */
/* add-int/lit8 v8, v8, 0x1 */
/* .line 222 */
/* :catchall_0 */
/* move-exception v0 */
/* move v2, v8 */
/* move v3, v15 */
/* goto/16 :goto_a */
/* .line 158 */
} // :cond_1
/* add-int/lit8 v0, v10, -0x1 */
/* if-le v8, v0, :cond_2 */
/* .line 159 */
/* move v2, v8 */
/* goto/16 :goto_8 */
/* .line 162 */
} // :cond_2
try { // :try_start_2
v0 = this.mProcWakelocksName;
/* move-object/from16 v16, v0 */
/* .line 163 */
/* .local v16, "nameStringArray":[Ljava/lang/String; */
v0 = this.mProcWakelocksData;
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_2 */
/* move-object/from16 v17, v0 */
/* .line 167 */
/* .local v17, "wlData":[J */
/* move v0, v15 */
/* .local v0, "j":I */
} // :goto_3
/* if-ge v0, v8, :cond_4 */
/* .line 168 */
try { // :try_start_3
/* aget-byte v2, v9, v0 */
/* and-int/lit16 v2, v2, 0x80 */
if ( v2 != null) { // if-eqz v2, :cond_3
/* const/16 v2, 0x3f */
/* aput-byte v2, v9, v0 */
/* .line 167 */
} // :cond_3
/* add-int/lit8 v0, v0, 0x1 */
/* .line 170 */
} // .end local v0 # "j":I
} // :cond_4
/* nop */
/* .line 171 */
if ( p3 != null) { // if-eqz p3, :cond_5
v0 = com.android.server.power.stats.KernelWakelockReaderRewrite.WAKEUP_SOURCES_FORMAT;
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* move-object v5, v0 */
/* .line 172 */
} // :cond_5
try { // :try_start_4
v0 = com.android.server.power.stats.KernelWakelockReaderRewrite.PROC_WAKELOCKS_FORMAT;
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_2 */
/* move-object v5, v0 */
} // :goto_4
int v0 = 0; // const/4 v0, 0x0
/* .line 170 */
/* move-object/from16 v2, p1 */
/* move v3, v15 */
/* move v4, v8 */
/* move-object/from16 v6, v16 */
/* move-object/from16 v7, v17 */
/* move/from16 v18, v8 */
} // .end local v8 # "endIndex":I
/* .local v18, "endIndex":I */
/* move-object v8, v0 */
try { // :try_start_5
v0 = /* invoke-static/range {v2 ..v8}, Landroid/os/Process;->parseProcLine([BII[I[Ljava/lang/String;[J[F)Z */
/* move v2, v0 */
/* .line 175 */
/* .local v2, "parsed":Z */
int v0 = 0; // const/4 v0, 0x0
/* aget-object v0, v16, v0 */
(( java.lang.String ) v0 ).trim ( ); // invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;
/* move-object v3, v0 */
/* .line 176 */
/* .local v3, "name":Ljava/lang/String; */
/* aget-wide v4, v17, v14 */
/* long-to-int v4, v4 */
/* .line 178 */
/* .local v4, "count":I */
/* const-wide/16 v5, 0x3e8 */
int v0 = 2; // const/4 v0, 0x2
if ( p3 != null) { // if-eqz p3, :cond_6
/* .line 180 */
/* aget-wide v7, v17, v0 */
/* mul-long/2addr v7, v5 */
/* .local v7, "totalTime":J */
/* .line 183 */
} // .end local v7 # "totalTime":J
} // :cond_6
/* aget-wide v7, v17, v0 */
/* const-wide/16 v19, 0x1f4 */
/* add-long v7, v7, v19 */
/* div-long/2addr v7, v5 */
/* .line 186 */
/* .restart local v7 # "totalTime":J */
} // :goto_5
if ( v2 != null) { // if-eqz v2, :cond_9
v0 = (( java.lang.String ) v3 ).length ( ); // invoke-virtual {v3}, Ljava/lang/String;->length()I
/* if-lez v0, :cond_9 */
/* .line 187 */
v0 = (( com.android.server.power.stats.KernelWakelockStats ) v11 ).containsKey ( v3 ); // invoke-virtual {v11, v3}, Lcom/android/server/power/stats/KernelWakelockStats;->containsKey(Ljava/lang/Object;)Z
/* if-nez v0, :cond_7 */
/* .line 188 */
/* new-instance v0, Lcom/android/server/power/stats/KernelWakelockStats$Entry; */
/* invoke-direct {v0, v4, v7, v8, v5}, Lcom/android/server/power/stats/KernelWakelockStats$Entry;-><init>(IJI)V */
(( com.android.server.power.stats.KernelWakelockStats ) v11 ).put ( v3, v0 ); // invoke-virtual {v11, v3, v0}, Lcom/android/server/power/stats/KernelWakelockStats;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 191 */
} // :cond_7
(( com.android.server.power.stats.KernelWakelockStats ) v11 ).get ( v3 ); // invoke-virtual {v11, v3}, Lcom/android/server/power/stats/KernelWakelockStats;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/power/stats/KernelWakelockStats$Entry; */
/* .line 192 */
/* .local v0, "kwlStats":Lcom/android/server/power/stats/KernelWakelockStats$Entry; */
/* iget v5, v0, Lcom/android/server/power/stats/KernelWakelockStats$Entry;->mVersion:I */
/* if-ne v5, v6, :cond_8 */
/* .line 193 */
/* iget v5, v0, Lcom/android/server/power/stats/KernelWakelockStats$Entry;->mCount:I */
/* add-int/2addr v5, v4 */
/* iput v5, v0, Lcom/android/server/power/stats/KernelWakelockStats$Entry;->mCount:I */
/* .line 194 */
/* iget-wide v5, v0, Lcom/android/server/power/stats/KernelWakelockStats$Entry;->mTotalTime:J */
/* add-long/2addr v5, v7 */
/* iput-wide v5, v0, Lcom/android/server/power/stats/KernelWakelockStats$Entry;->mTotalTime:J */
/* .line 196 */
} // :cond_8
/* iput v4, v0, Lcom/android/server/power/stats/KernelWakelockStats$Entry;->mCount:I */
/* .line 197 */
/* iput-wide v7, v0, Lcom/android/server/power/stats/KernelWakelockStats$Entry;->mTotalTime:J */
/* .line 198 */
/* iput v5, v0, Lcom/android/server/power/stats/KernelWakelockStats$Entry;->mVersion:I */
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_1 */
/* .line 200 */
} // .end local v0 # "kwlStats":Lcom/android/server/power/stats/KernelWakelockStats$Entry;
} // :goto_6
/* .line 201 */
} // :cond_9
/* if-nez v2, :cond_a */
/* .line 203 */
try { // :try_start_6
final String v0 = "KernelWakelockReader"; // const-string v0, "KernelWakelockReader"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Failed to parse proc line: "; // const-string v6, "Failed to parse proc line: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* new-instance v6, Ljava/lang/String; */
/* sub-int v13, v18, v15 */
/* invoke-direct {v6, v9, v15, v13}, Ljava/lang/String;-><init>([BII)V */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .wtf ( v0,v5 );
/* :try_end_6 */
/* .catch Ljava/lang/Exception; {:try_start_6 ..:try_end_6} :catch_0 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_1 */
/* .line 207 */
/* .line 205 */
/* :catch_0 */
/* move-exception v0 */
/* .line 206 */
/* .local v0, "e":Ljava/lang/Exception; */
try { // :try_start_7
final String v5 = "KernelWakelockReader"; // const-string v5, "KernelWakelockReader"
final String v6 = "Failed to parse proc line!"; // const-string v6, "Failed to parse proc line!"
android.util.Slog .wtf ( v5,v6 );
/* :try_end_7 */
/* .catchall {:try_start_7 ..:try_end_7} :catchall_1 */
/* .line 209 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_a
} // :goto_7
/* add-int/lit8 v15, v18, 0x1 */
/* .line 210 */
} // .end local v2 # "parsed":Z
} // .end local v16 # "nameStringArray":[Ljava/lang/String;
} // .end local v17 # "wlData":[J
/* move/from16 v2, v18 */
/* const/16 v13, 0xa */
/* goto/16 :goto_1 */
/* .line 222 */
} // .end local v3 # "name":Ljava/lang/String;
} // .end local v4 # "count":I
} // .end local v7 # "totalTime":J
/* :catchall_1 */
/* move-exception v0 */
/* move v3, v15 */
/* move/from16 v2, v18 */
} // .end local v18 # "endIndex":I
/* .restart local v8 # "endIndex":I */
/* :catchall_2 */
/* move-exception v0 */
/* move/from16 v18, v8 */
/* move v3, v15 */
/* move/from16 v2, v18 */
} // .end local v8 # "endIndex":I
/* .restart local v18 # "endIndex":I */
/* .line 213 */
} // .end local v18 # "endIndex":I
/* .local v2, "endIndex":I */
} // :cond_b
} // :goto_8
try { // :try_start_8
/* invoke-virtual/range {p4 ..p4}, Lcom/android/server/power/stats/KernelWakelockStats;->values()Ljava/util/Collection; */
/* .line 214 */
/* .local v0, "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/power/stats/KernelWakelockStats$Entry;>;" */
} // :cond_c
v3 = } // :goto_9
if ( v3 != null) { // if-eqz v3, :cond_d
/* .line 215 */
/* check-cast v3, Lcom/android/server/power/stats/KernelWakelockStats$Entry; */
/* iget v3, v3, Lcom/android/server/power/stats/KernelWakelockStats$Entry;->mVersion:I */
/* if-eq v3, v4, :cond_c */
/* .line 216 */
/* .line 220 */
} // :cond_d
/* iput v3, v11, Lcom/android/server/power/stats/KernelWakelockStats;->kernelWakelockVersion:I */
/* .line 221 */
/* monitor-exit p0 */
/* :try_end_8 */
/* .catchall {:try_start_8 ..:try_end_8} :catchall_3 */
/* .line 222 */
} // .end local v0 # "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/power/stats/KernelWakelockStats$Entry;>;"
/* :catchall_3 */
/* move-exception v0 */
/* move v3, v15 */
} // .end local v15 # "startIndex":I
/* .local v3, "startIndex":I */
/* :catchall_4 */
/* move-exception v0 */
} // :goto_a
try { // :try_start_9
/* monitor-exit p0 */
/* :try_end_9 */
/* .catchall {:try_start_9 ..:try_end_9} :catchall_4 */
/* throw v0 */
} // .end method
public final com.android.server.power.stats.KernelWakelockStats readKernelWakelockStats ( com.android.server.power.stats.KernelWakelockStats p0 ) {
/* .locals 17 */
/* .param p1, "staleStats" # Lcom/android/server/power/stats/KernelWakelockStats; */
/* .line 77 */
/* move-object/from16 v7, p0 */
/* move-object/from16 v8, p1 */
final String v9 = "failed to read kernel wakelocks"; // const-string v9, "failed to read kernel wakelocks"
final String v10 = "KernelWakelockReader"; // const-string v10, "KernelWakelockReader"
/* const v0, 0x8000 */
/* new-array v11, v0, [B */
/* .line 80 */
/* .local v11, "buffer":[B */
v12 = android.os.StrictMode .allowThreadDiskReadsMask ( );
/* .line 81 */
/* .local v12, "oldMask":I */
int v1 = 0; // const/4 v1, 0x0
/* .line 83 */
/* .local v1, "is":Ljava/io/FileInputStream; */
int v13 = 0; // const/4 v13, 0x0
try { // :try_start_0
/* new-instance v0, Ljava/io/FileInputStream; */
final String v2 = "/proc/wakelocks"; // const-string v2, "/proc/wakelocks"
/* invoke-direct {v0, v2}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 84 */
} // .end local v1 # "is":Ljava/io/FileInputStream;
/* .local v0, "is":Ljava/io/FileInputStream; */
int v1 = 0; // const/4 v1, 0x0
/* .line 94 */
/* .local v1, "wakeup_sources":Z */
/* move-object v14, v0 */
/* move v15, v1 */
/* .line 85 */
} // .end local v0 # "is":Ljava/io/FileInputStream;
/* .local v1, "is":Ljava/io/FileInputStream; */
/* :catch_0 */
/* move-exception v0 */
/* move-object v2, v0 */
/* .line 87 */
/* .local v2, "e":Ljava/lang/Exception; */
try { // :try_start_1
/* new-instance v0, Ljava/io/FileInputStream; */
final String v3 = "/d/wakeup_sources"; // const-string v3, "/d/wakeup_sources"
/* invoke-direct {v0, v3}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V */
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_5 */
/* .line 88 */
} // .end local v1 # "is":Ljava/io/FileInputStream;
/* .restart local v0 # "is":Ljava/io/FileInputStream; */
int v1 = 1; // const/4 v1, 0x1
/* .line 93 */
/* .local v1, "wakeup_sources":Z */
/* move-object v14, v0 */
/* move v15, v1 */
/* .line 97 */
} // .end local v0 # "is":Ljava/io/FileInputStream;
} // .end local v1 # "wakeup_sources":Z
} // .end local v2 # "e":Ljava/lang/Exception;
/* .local v14, "is":Ljava/io/FileInputStream; */
/* .local v15, "wakeup_sources":Z */
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
/* .line 98 */
/* .local v0, "cnt":I */
int v1 = 0; // const/4 v1, 0x0
/* .line 99 */
/* .local v1, "len":I */
try { // :try_start_2
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v5 */
/* .line 101 */
/* .local v5, "startTime":J */
int v2 = 0; // const/4 v2, 0x0
/* iput-boolean v2, v7, Lcom/android/server/power/stats/KernelWakelockReaderRewrite;->br_flag:Z */
/* move v4, v1 */
/* .line 102 */
} // .end local v1 # "len":I
/* .local v4, "len":I */
} // :goto_1
/* array-length v1, v11 */
/* sub-int/2addr v1, v4 */
v1 = (( java.io.FileInputStream ) v14 ).read ( v11, v4, v1 ); // invoke-virtual {v14, v11, v4, v1}, Ljava/io/FileInputStream;->read([BII)I
/* move v0, v1 */
/* if-lez v1, :cond_0 */
/* .line 103 */
/* add-int/2addr v4, v0 */
/* .line 104 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "is.read return cnt = "; // const-string v2, "is.read return cnt = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .v ( v10,v1 );
/* .line 106 */
} // :cond_0
/* if-nez v4, :cond_1 */
/* .line 107 */
(( java.io.FileInputStream ) v14 ).close ( ); // invoke-virtual {v14}, Ljava/io/FileInputStream;->close()V
/* .line 108 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "is.read return cnt is zero ? "; // const-string v2, "is.read return cnt is zero ? "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .v ( v10,v1 );
/* .line 109 */
/* .line 111 */
} // :cond_1
/* move-object/from16 v1, p0 */
/* move-object v2, v11 */
/* move v3, v4 */
/* move/from16 v16, v4 */
} // .end local v4 # "len":I
/* .local v16, "len":I */
/* move v4, v0 */
v1 = /* invoke-direct/range {v1 ..v6}, Lcom/android/server/power/stats/KernelWakelockReaderRewrite;->readInfoCheck([BIIJ)I */
/* .line 112 */
} // .end local v16 # "len":I
/* .restart local v1 # "len":I */
(( com.android.server.power.stats.KernelWakelockReaderRewrite ) v7 ).parseProcWakelocks ( v11, v1, v15, v8 ); // invoke-virtual {v7, v11, v1, v15, v8}, Lcom/android/server/power/stats/KernelWakelockReaderRewrite;->parseProcWakelocks([BIZLcom/android/server/power/stats/KernelWakelockStats;)Lcom/android/server/power/stats/KernelWakelockStats;
/* .line 113 */
/* iget-boolean v2, v7, Lcom/android/server/power/stats/KernelWakelockReaderRewrite;->br_flag:Z */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 114 */
(( java.io.FileInputStream ) v14 ).close ( ); // invoke-virtual {v14}, Ljava/io/FileInputStream;->close()V
/* .line 115 */
final String v2 = "read is at the end of file "; // const-string v2, "read is at the end of file "
android.util.Slog .w ( v10,v2 );
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 116 */
/* nop */
/* .line 119 */
} // .end local v0 # "cnt":I
} // .end local v1 # "len":I
} // .end local v5 # "startTime":J
} // :goto_2
/* nop */
/* .line 124 */
android.os.StrictMode .setThreadPolicyMask ( v12 );
/* .line 126 */
try { // :try_start_3
(( java.io.FileInputStream ) v14 ).close ( ); // invoke-virtual {v14}, Ljava/io/FileInputStream;->close()V
/* :try_end_3 */
/* .catch Ljava/lang/Exception; {:try_start_3 ..:try_end_3} :catch_1 */
/* .line 129 */
/* .line 127 */
/* :catch_1 */
/* move-exception v0 */
/* move-object v1, v0 */
/* move-object v0, v1 */
/* .line 128 */
/* .local v0, "e":Ljava/lang/Exception; */
android.util.Slog .wtf ( v10,v9,v0 );
/* .line 119 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_3
/* .line 118 */
} // :cond_2
/* .line 124 */
/* :catchall_0 */
/* move-exception v0 */
/* move-object v1, v0 */
/* .line 120 */
/* :catch_2 */
/* move-exception v0 */
/* move-object v1, v0 */
/* .line 121 */
/* .local v1, "e":Ljava/io/IOException; */
try { // :try_start_4
android.util.Slog .wtf ( v10,v9,v1 );
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* .line 122 */
/* nop */
/* .line 124 */
android.os.StrictMode .setThreadPolicyMask ( v12 );
/* .line 126 */
try { // :try_start_5
(( java.io.FileInputStream ) v14 ).close ( ); // invoke-virtual {v14}, Ljava/io/FileInputStream;->close()V
/* :try_end_5 */
/* .catch Ljava/lang/Exception; {:try_start_5 ..:try_end_5} :catch_3 */
/* .line 129 */
/* .line 127 */
/* :catch_3 */
/* move-exception v0 */
/* move-object v2, v0 */
/* move-object v0, v2 */
/* .line 128 */
/* .restart local v0 # "e":Ljava/lang/Exception; */
android.util.Slog .wtf ( v10,v9,v0 );
/* .line 122 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_4
/* .line 124 */
} // .end local v1 # "e":Ljava/io/IOException;
} // :goto_5
android.os.StrictMode .setThreadPolicyMask ( v12 );
/* .line 126 */
try { // :try_start_6
(( java.io.FileInputStream ) v14 ).close ( ); // invoke-virtual {v14}, Ljava/io/FileInputStream;->close()V
/* :try_end_6 */
/* .catch Ljava/lang/Exception; {:try_start_6 ..:try_end_6} :catch_4 */
/* .line 129 */
/* .line 127 */
/* :catch_4 */
/* move-exception v0 */
/* move-object v2, v0 */
/* move-object v0, v2 */
/* .line 128 */
/* .restart local v0 # "e":Ljava/lang/Exception; */
android.util.Slog .wtf ( v10,v9,v0 );
/* .line 130 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_6
/* throw v1 */
/* .line 89 */
} // .end local v14 # "is":Ljava/io/FileInputStream;
} // .end local v15 # "wakeup_sources":Z
/* .local v1, "is":Ljava/io/FileInputStream; */
/* .restart local v2 # "e":Ljava/lang/Exception; */
/* :catch_5 */
/* move-exception v0 */
/* .line 90 */
/* .local v0, "e2":Ljava/lang/Exception; */
final String v3 = "neither /proc/wakelocks nor /d/wakeup_sources exists"; // const-string v3, "neither /proc/wakelocks nor /d/wakeup_sources exists"
android.util.Slog .wtf ( v10,v3 );
/* .line 92 */
} // .end method
