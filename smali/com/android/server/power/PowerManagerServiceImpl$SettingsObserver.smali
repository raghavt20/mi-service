.class final Lcom/android/server/power/PowerManagerServiceImpl$SettingsObserver;
.super Landroid/database/ContentObserver;
.source "PowerManagerServiceImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/power/PowerManagerServiceImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SettingsObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/power/PowerManagerServiceImpl;


# direct methods
.method public constructor <init>(Lcom/android/server/power/PowerManagerServiceImpl;Landroid/os/Handler;)V
    .locals 0
    .param p2, "handler"    # Landroid/os/Handler;

    .line 1395
    iput-object p1, p0, Lcom/android/server/power/PowerManagerServiceImpl$SettingsObserver;->this$0:Lcom/android/server/power/PowerManagerServiceImpl;

    .line 1396
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 1397
    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 2
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 1401
    iget-object v0, p0, Lcom/android/server/power/PowerManagerServiceImpl$SettingsObserver;->this$0:Lcom/android/server/power/PowerManagerServiceImpl;

    invoke-static {v0}, Lcom/android/server/power/PowerManagerServiceImpl;->-$$Nest$fgetmLock(Lcom/android/server/power/PowerManagerServiceImpl;)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 1402
    :try_start_0
    iget-object v1, p0, Lcom/android/server/power/PowerManagerServiceImpl$SettingsObserver;->this$0:Lcom/android/server/power/PowerManagerServiceImpl;

    invoke-static {v1, p2}, Lcom/android/server/power/PowerManagerServiceImpl;->-$$Nest$mhandleSettingsChangedLocked(Lcom/android/server/power/PowerManagerServiceImpl;Landroid/net/Uri;)V

    .line 1403
    monitor-exit v0

    .line 1404
    return-void

    .line 1403
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
