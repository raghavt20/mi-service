.class final Lcom/android/server/power/MiuiAttentionDetector$SettingsObserver;
.super Landroid/database/ContentObserver;
.source "MiuiAttentionDetector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/power/MiuiAttentionDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SettingsObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/power/MiuiAttentionDetector;


# direct methods
.method public constructor <init>(Lcom/android/server/power/MiuiAttentionDetector;Landroid/os/Handler;)V
    .locals 0
    .param p2, "handler"    # Landroid/os/Handler;

    .line 552
    iput-object p1, p0, Lcom/android/server/power/MiuiAttentionDetector$SettingsObserver;->this$0:Lcom/android/server/power/MiuiAttentionDetector;

    .line 553
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 554
    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 2
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 558
    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    goto :goto_0

    :sswitch_0
    const-string/jumbo v1, "screen_project_in_screening"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    goto :goto_1

    :sswitch_1
    const-string v1, "gaze_lock_screen_setting"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_2
    const-string v1, "miui_people_near_screen_on"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_3
    const-string/jumbo v1, "synergy_mode"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    goto :goto_1

    :sswitch_4
    const-string v1, "external_display_connected"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    goto :goto_1

    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_0

    goto :goto_2

    .line 568
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector$SettingsObserver;->this$0:Lcom/android/server/power/MiuiAttentionDetector;

    invoke-static {v0}, Lcom/android/server/power/MiuiAttentionDetector;->-$$Nest$mupdateScreenProjectConfig(Lcom/android/server/power/MiuiAttentionDetector;)V

    .line 569
    iget-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector$SettingsObserver;->this$0:Lcom/android/server/power/MiuiAttentionDetector;

    invoke-static {v0}, Lcom/android/server/power/MiuiAttentionDetector;->-$$Nest$mupdateSynergyModeConfig(Lcom/android/server/power/MiuiAttentionDetector;)V

    .line 570
    iget-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector$SettingsObserver;->this$0:Lcom/android/server/power/MiuiAttentionDetector;

    invoke-static {v0}, Lcom/android/server/power/MiuiAttentionDetector;->-$$Nest$mupdateExternalDisplayConnectConfig(Lcom/android/server/power/MiuiAttentionDetector;)V

    .line 571
    goto :goto_2

    .line 563
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector$SettingsObserver;->this$0:Lcom/android/server/power/MiuiAttentionDetector;

    invoke-static {v0}, Lcom/android/server/power/MiuiAttentionDetector;->-$$Nest$mupdateAonScreenOffConfig(Lcom/android/server/power/MiuiAttentionDetector;)V

    .line 564
    goto :goto_2

    .line 560
    :pswitch_2
    iget-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector$SettingsObserver;->this$0:Lcom/android/server/power/MiuiAttentionDetector;

    invoke-static {v0}, Lcom/android/server/power/MiuiAttentionDetector;->-$$Nest$mupdateAonScreenOnConfig(Lcom/android/server/power/MiuiAttentionDetector;)V

    .line 561
    nop

    .line 575
    :goto_2
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x688973a8 -> :sswitch_4
        -0x55bbaa45 -> :sswitch_3
        0x6fc891c -> :sswitch_2
        0x53b87d57 -> :sswitch_1
        0x790152b5 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
