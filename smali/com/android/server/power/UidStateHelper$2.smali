.class Lcom/android/server/power/UidStateHelper$2;
.super Ljava/lang/Object;
.source "UidStateHelper.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/power/UidStateHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/power/UidStateHelper;


# direct methods
.method constructor <init>(Lcom/android/server/power/UidStateHelper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/power/UidStateHelper;

    .line 195
    iput-object p1, p0, Lcom/android/server/power/UidStateHelper$2;->this$0:Lcom/android/server/power/UidStateHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .line 198
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 205
    const/4 v0, 0x0

    return v0

    .line 200
    :pswitch_0
    iget v0, p1, Landroid/os/Message;->arg1:I

    .line 201
    .local v0, "uid":I
    iget v1, p1, Landroid/os/Message;->arg2:I

    .line 202
    .local v1, "state":I
    iget-object v2, p0, Lcom/android/server/power/UidStateHelper$2;->this$0:Lcom/android/server/power/UidStateHelper;

    invoke-static {v2, v0, v1}, Lcom/android/server/power/UidStateHelper;->-$$Nest$mdispatchUidStateChange(Lcom/android/server/power/UidStateHelper;II)V

    .line 203
    const/4 v2, 0x1

    return v2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
