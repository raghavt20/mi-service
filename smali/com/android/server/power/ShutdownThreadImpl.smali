.class public Lcom/android/server/power/ShutdownThreadImpl;
.super Lcom/android/server/power/ShutdownThreadStub;
.source "ShutdownThreadImpl.java"


# static fields
.field private static final CUSTOMIZED_REGION:Ljava/lang/String;

.field private static final CUST_VAR:Ljava/lang/String;

.field private static final IS_CUSTOMIZATION:Z

.field private static final IS_CUSTOMIZATION_TEST:Z

.field private static final IS_CUSTOMIZED_REGION:Z

.field private static final OPCUST_ROOT_PATH:Ljava/lang/String;

.field private static final OPERATOR_ANIMATION_DISABLE_FLAG:Ljava/lang/String; = "/data/system/theme_magic/disable_operator_animation"

.field private static final OPERATOR_MUSIC_DISABLE_FLAG:Ljava/lang/String; = "/data/system/theme_magic/disable_operator_audio"

.field private static final OPERATOR_SHUTDOWN_ANIMATION_FILE:Ljava/lang/String;

.field private static final OPERATOR_SHUTDOWN_MUSIC_FILE:Ljava/lang/String;

.field private static final SHUTDOWN_ACTION_PROPERTY_MIUI:Ljava/lang/String; = "sys.shutdown.miui"

.field private static final SHUTDOWN_ACTION_PROPERTY_MIUI_MUSIC:Ljava/lang/String; = "sys.shutdown.miuimusic"

.field private static final TAG:Ljava/lang/String; = "ShutdownThreadImpl"

.field private static sIsShutdownMusicPlaying:Z


# direct methods
.method static bridge synthetic -$$Nest$sfputsIsShutdownMusicPlaying(Z)V
    .locals 0

    sput-boolean p0, Lcom/android/server/power/ShutdownThreadImpl;->sIsShutdownMusicPlaying:Z

    return-void
.end method

.method static constructor <clinit>()V
    .locals 4

    .line 40
    sget-boolean v0, Lmiui/os/Build;->IS_CM_CUSTOMIZATION_TEST:Z

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_1

    sget-boolean v0, Lmiui/os/Build;->IS_CU_CUSTOMIZATION_TEST:Z

    if-nez v0, :cond_1

    sget-boolean v0, Lmiui/os/Build;->IS_CT_CUSTOMIZATION_TEST:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    move v0, v1

    goto :goto_1

    :cond_1
    :goto_0
    move v0, v2

    :goto_1
    sput-boolean v0, Lcom/android/server/power/ShutdownThreadImpl;->IS_CUSTOMIZATION_TEST:Z

    .line 42
    sget-boolean v0, Lmiui/os/Build;->IS_CM_CUSTOMIZATION:Z

    if-nez v0, :cond_2

    sget-boolean v0, Lmiui/os/Build;->IS_CU_CUSTOMIZATION:Z

    if-nez v0, :cond_2

    sget-boolean v0, Lmiui/os/Build;->IS_CT_CUSTOMIZATION:Z

    if-eqz v0, :cond_3

    :cond_2
    move v1, v2

    :cond_3
    sput-boolean v1, Lcom/android/server/power/ShutdownThreadImpl;->IS_CUSTOMIZATION:Z

    .line 45
    const-string v0, "ro.miui.customized.region"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/power/ShutdownThreadImpl;->CUSTOMIZED_REGION:Ljava/lang/String;

    .line 46
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    xor-int/2addr v1, v2

    sput-boolean v1, Lcom/android/server/power/ShutdownThreadImpl;->IS_CUSTOMIZED_REGION:Z

    .line 130
    if-eqz v1, :cond_4

    goto :goto_2

    :cond_4
    invoke-static {}, Lmiui/os/Build;->getCustVariant()Ljava/lang/String;

    move-result-object v0

    :goto_2
    sput-object v0, Lcom/android/server/power/ShutdownThreadImpl;->CUST_VAR:Ljava/lang/String;

    .line 131
    sget-boolean v1, Lmiui/os/Build;->HAS_CUST_PARTITION:Z

    const-string v2, "/"

    if-eqz v1, :cond_5

    .line 132
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/product/opcust/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 133
    :cond_5
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/data/miui/cust/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_3
    sput-object v0, Lcom/android/server/power/ShutdownThreadImpl;->OPCUST_ROOT_PATH:Ljava/lang/String;

    .line 135
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "theme/operator/boots/shutdownanimation.zip"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/android/server/power/ShutdownThreadImpl;->OPERATOR_SHUTDOWN_ANIMATION_FILE:Ljava/lang/String;

    .line 136
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "theme/operator/boots/shutdownaudio.mp3"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/power/ShutdownThreadImpl;->OPERATOR_SHUTDOWN_MUSIC_FILE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 34
    invoke-direct {p0}, Lcom/android/server/power/ShutdownThreadStub;-><init>()V

    return-void
.end method

.method static checkAnimationFileExist()Z
    .locals 2

    .line 141
    new-instance v0, Ljava/io/File;

    const-string v1, "/data/system/theme_magic/disable_operator_animation"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/android/server/power/ShutdownThreadImpl;->OPERATOR_SHUTDOWN_ANIMATION_FILE:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 142
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 141
    :goto_0
    return v0
.end method

.method static getShutdownMusicFilePath(Landroid/content/Context;Z)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "isReboot"    # Z

    .line 153
    const/4 v0, 0x0

    return-object v0
.end method

.method private static getShutdownMusicFilePathInner(Landroid/content/Context;Z)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "isReboot"    # Z

    .line 157
    new-instance v0, Ljava/io/File;

    const-string v1, "/data/system/theme_magic/disable_operator_audio"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 158
    return-object v1

    .line 160
    :cond_0
    new-instance v0, Ljava/io/File;

    sget-object v2, Lcom/android/server/power/ShutdownThreadImpl;->OPERATOR_SHUTDOWN_MUSIC_FILE:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v1, v2

    :cond_1
    return-object v1
.end method

.method private static isSilentMode(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .line 224
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 225
    .local v0, "audio":Landroid/media/AudioManager;
    invoke-virtual {v0}, Landroid/media/AudioManager;->isSilentMode()Z

    move-result v1

    return v1
.end method

.method static needVibrator()Z
    .locals 1

    .line 49
    const/4 v0, 0x0

    return v0
.end method

.method static playShutdownMusic(Landroid/content/Context;Z)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "isReboot"    # Z

    .line 167
    const-string/jumbo v0, "sys.shutdown.miui"

    const-string/jumbo v1, "shutdown"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    invoke-static {p0, p1}, Lcom/android/server/power/ShutdownThreadImpl;->getShutdownMusicFilePathInner(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v0

    .line 170
    .local v0, "shutdownMusicPath":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "shutdown music: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p0}, Lcom/android/server/power/ShutdownThreadImpl;->isSilentMode(Landroid/content/Context;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ShutdownThreadImpl"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    invoke-static {p0}, Lcom/android/server/power/ShutdownThreadImpl;->isSilentMode(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    .line 173
    const-string/jumbo v1, "sys.shutdown.miuimusic"

    const-string/jumbo v2, "shutdown_music"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    :cond_0
    return-void
.end method

.method private static playShutdownMusicImpl(Ljava/lang/String;)V
    .locals 7
    .param p0, "shutdownMusicPath"    # Ljava/lang/String;

    .line 180
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    .line 181
    .local v0, "actionDoneSync":Ljava/lang/Object;
    const/4 v1, 0x1

    sput-boolean v1, Lcom/android/server/power/ShutdownThreadImpl;->sIsShutdownMusicPlaying:Z

    .line 184
    :try_start_0
    new-instance v1, Landroid/media/MediaPlayer;

    invoke-direct {v1}, Landroid/media/MediaPlayer;-><init>()V

    .line 185
    .local v1, "mediaPlayer":Landroid/media/MediaPlayer;
    invoke-virtual {v1}, Landroid/media/MediaPlayer;->reset()V

    .line 186
    invoke-virtual {v1, p0}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    .line 187
    invoke-virtual {v1}, Landroid/media/MediaPlayer;->prepare()V

    .line 188
    new-instance v2, Lcom/android/server/power/ShutdownThreadImpl$1;

    invoke-direct {v2, v0}, Lcom/android/server/power/ShutdownThreadImpl$1;-><init>(Ljava/lang/Object;)V

    .line 189
    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 198
    invoke-virtual {v1}, Landroid/media/MediaPlayer;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 202
    .end local v1    # "mediaPlayer":Landroid/media/MediaPlayer;
    goto :goto_0

    .line 199
    :catch_0
    move-exception v1

    .line 200
    .local v1, "e":Ljava/lang/Exception;
    const/4 v2, 0x0

    sput-boolean v2, Lcom/android/server/power/ShutdownThreadImpl;->sIsShutdownMusicPlaying:Z

    .line 201
    const-string v2, "ShutdownThreadImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "play shutdown music error:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    const-wide/16 v3, 0x1388

    add-long/2addr v1, v3

    .line 205
    .local v1, "endTimeForMusic":J
    monitor-enter v0

    .line 206
    :goto_1
    :try_start_1
    sget-boolean v3, Lcom/android/server/power/ShutdownThreadImpl;->sIsShutdownMusicPlaying:Z

    if-eqz v3, :cond_1

    .line 207
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    sub-long v3, v1, v3

    .line 208
    .local v3, "delay":J
    const-wide/16 v5, 0x0

    cmp-long v5, v3, v5

    if-gtz v5, :cond_0

    .line 209
    const-string v5, "ShutdownThreadImpl"

    const-string v6, "play shutdown music timeout"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 210
    goto :goto_3

    .line 213
    :cond_0
    :try_start_2
    invoke-virtual {v0, v3, v4}, Ljava/lang/Object;->wait(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 215
    goto :goto_2

    .line 214
    :catch_1
    move-exception v5

    .line 216
    .end local v3    # "delay":J
    :goto_2
    goto :goto_1

    .line 217
    :cond_1
    :goto_3
    :try_start_3
    sget-boolean v3, Lcom/android/server/power/ShutdownThreadImpl;->sIsShutdownMusicPlaying:Z

    if-nez v3, :cond_2

    .line 218
    const-string v3, "ShutdownThreadImpl"

    const-string v4, "play shutdown music complete"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    :cond_2
    monitor-exit v0

    .line 221
    return-void

    .line 220
    :catchall_0
    move-exception v3

    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v3
.end method

.method static recordShutdownTime(Landroid/content/Context;Z)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "reboot"    # Z

    .line 229
    new-instance v0, Lmiui/util/SystemAnalytics$Action;

    invoke-direct {v0}, Lmiui/util/SystemAnalytics$Action;-><init>()V

    .line 230
    .local v0, "action":Lmiui/util/SystemAnalytics$Action;
    if-eqz p1, :cond_0

    const-string v1, "reboot"

    goto :goto_0

    :cond_0
    const-string/jumbo v1, "shutdown"

    :goto_0
    const-string v2, "action"

    invoke-virtual {v0, v2, v1}, Lmiui/util/SystemAnalytics$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lmiui/util/SystemAnalytics$Action;

    .line 231
    const-string/jumbo v1, "time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lmiui/util/SystemAnalytics$Action;->addParam(Ljava/lang/String;J)Lmiui/util/SystemAnalytics$Action;

    .line 232
    const-string/jumbo v1, "systemserver_bootshuttime"

    invoke-static {p0, v1, v0}, Lmiui/util/SystemAnalytics;->trackSystem(Landroid/content/Context;Ljava/lang/String;Lmiui/util/SystemAnalytics$Action;)V

    .line 233
    return-void
.end method

.method static showShutdownAnimation(Landroid/content/Context;Z)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "isReboot"    # Z

    .line 148
    invoke-static {p0, p1}, Lcom/android/server/power/ShutdownThreadImpl;->playShutdownMusic(Landroid/content/Context;Z)V

    .line 149
    return-void
.end method

.method static showShutdownDialog(Landroid/content/Context;Z)V
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "isReboot"    # Z

    .line 85
    new-instance v0, Landroid/app/Dialog;

    const v1, 0x103006d

    invoke-direct {v0, p0, v1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 86
    .local v0, "bootMsgDialog":Landroid/app/Dialog;
    invoke-virtual {v0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x110c002f

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 87
    .local v1, "view":Landroid/view/View;
    const/16 v2, 0x400

    invoke-virtual {v1, v2}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 88
    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 89
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 90
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    .line 91
    .local v3, "lp":Landroid/view/WindowManager$LayoutParams;
    const/4 v4, 0x1

    iput v4, v3, Landroid/view/WindowManager$LayoutParams;->screenOrientation:I

    .line 92
    iput v4, v3, Landroid/view/WindowManager$LayoutParams;->layoutInDisplayCutoutMode:I

    .line 93
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 94
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v5

    const/16 v6, 0x7e5

    invoke-virtual {v5, v6}, Landroid/view/Window;->setType(I)V

    .line 95
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v5

    const/high16 v6, 0x10000

    invoke-virtual {v5, v6}, Landroid/view/Window;->clearFlags(I)V

    .line 96
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 98
    if-eqz p1, :cond_1

    .line 99
    const v4, 0x110a00c4

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 100
    .local v4, "shutdownImage":Landroid/widget/ImageView;
    if-eqz v4, :cond_0

    .line 101
    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 102
    invoke-virtual {v4}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/AnimatedRotateDrawable;

    .line 103
    .local v2, "animationDrawable":Landroid/graphics/drawable/AnimatedRotateDrawable;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x110b003f

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    invoke-virtual {v2, v5}, Landroid/graphics/drawable/AnimatedRotateDrawable;->setFramesCount(I)V

    .line 104
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x110b0040

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    invoke-virtual {v2, v5}, Landroid/graphics/drawable/AnimatedRotateDrawable;->setFramesDuration(I)V

    .line 105
    invoke-virtual {v2}, Landroid/graphics/drawable/AnimatedRotateDrawable;->start()V

    .line 107
    .end local v2    # "animationDrawable":Landroid/graphics/drawable/AnimatedRotateDrawable;
    .end local v4    # "shutdownImage":Landroid/widget/ImageView;
    :cond_0
    goto :goto_0

    .line 109
    :cond_1
    const-class v5, Lcom/android/server/lights/LightsManager;

    .line 110
    invoke-static {v5}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/server/lights/LightsManager;

    .line 111
    .local v5, "lightmanager":Lcom/android/server/lights/LightsManager;
    invoke-virtual {v5, v2}, Lcom/android/server/lights/LightsManager;->getLight(I)Lcom/android/server/lights/LogicalLight;

    move-result-object v6

    .line 112
    .local v6, "light":Lcom/android/server/lights/LogicalLight;
    const/4 v7, 0x0

    .line 114
    .local v7, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :try_start_0
    const-string v8, "com.android.server.lights.MiuiLightsService$LightImpl"

    invoke-static {v8}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v8

    move-object v7, v8

    .line 115
    const-string/jumbo v8, "setBrightness"

    const/4 v9, 0x2

    new-array v10, v9, [Ljava/lang/Class;

    sget-object v11, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v11, v10, v2

    sget-object v11, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v11, v10, v4

    invoke-virtual {v7, v8, v10}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v8

    .line 116
    .local v8, "setBrightnessMethod":Ljava/lang/reflect/Method;
    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v2

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v9, v4

    invoke-virtual {v8, v6, v9}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 123
    nop

    .end local v8    # "setBrightnessMethod":Ljava/lang/reflect/Method;
    goto :goto_0

    .line 118
    :catch_0
    move-exception v2

    .line 122
    .local v2, "e":Ljava/lang/ReflectiveOperationException;
    const-string v4, "ShutdownThreadImpl"

    const-string v8, "Failed to invoke MiuiLightsService setBrightness method"

    invoke-static {v4, v8, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 127
    .end local v2    # "e":Ljava/lang/ReflectiveOperationException;
    .end local v5    # "lightmanager":Lcom/android/server/lights/LightsManager;
    .end local v6    # "light":Lcom/android/server/lights/LogicalLight;
    .end local v7    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :goto_0
    const-string/jumbo v2, "sys.in_shutdown_progress"

    const-string v4, "1"

    invoke-static {v2, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    return-void
.end method


# virtual methods
.method isCustomizedShutdownAnim()Z
    .locals 4

    .line 68
    sget-boolean v0, Lcom/android/server/power/ShutdownThreadImpl;->IS_CUSTOMIZATION:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/android/server/power/ShutdownThreadImpl;->IS_CUSTOMIZATION_TEST:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/android/server/power/ShutdownThreadImpl;->IS_CUSTOMIZED_REGION:Z

    if-eqz v0, :cond_3

    :cond_0
    invoke-static {}, Lcom/android/server/power/ShutdownThreadImpl;->checkAnimationFileExist()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 69
    sget-object v0, Lcom/android/server/power/ShutdownThreadImpl;->CUSTOMIZED_REGION:Ljava/lang/String;

    const-string v2, "mx_at"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "persist.radio.op.name"

    const-string v3, "AT"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 70
    return v1

    .line 72
    :cond_1
    const-string v2, "gt_tg"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 73
    const-string v0, "ro.miui.region"

    const-string v2, "GT"

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 74
    .local v0, "region":Ljava/lang/String;
    const-string v2, "BO"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "SV"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "HN"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 75
    const-string v2, "NI"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 76
    return v1

    .line 79
    .end local v0    # "region":Ljava/lang/String;
    :cond_2
    const/4 v0, 0x1

    return v0

    .line 81
    :cond_3
    return v1
.end method

.method showShutdownAnimOrDialog(Landroid/content/Context;Z)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "isReboot"    # Z

    .line 58
    invoke-virtual {p0}, Lcom/android/server/power/ShutdownThreadImpl;->isCustomizedShutdownAnim()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    const-string/jumbo v0, "service.bootanim.exit"

    const-string v1, "0"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    const-string v0, "ctl.start"

    const-string v1, "bootanim"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    invoke-static {p1, p2}, Lcom/android/server/power/ShutdownThreadImpl;->showShutdownAnimation(Landroid/content/Context;Z)V

    goto :goto_0

    .line 63
    :cond_0
    invoke-static {p1, p2}, Lcom/android/server/power/ShutdownThreadImpl;->showShutdownDialog(Landroid/content/Context;Z)V

    .line 65
    :goto_0
    return-void
.end method

.method showShutdownAnimOrDialog(Landroid/content/Context;ZLandroid/app/ProgressDialog;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "isReboot"    # Z
    .param p3, "pd"    # Landroid/app/ProgressDialog;

    .line 54
    invoke-virtual {p0, p1, p2}, Lcom/android/server/power/ShutdownThreadImpl;->showShutdownAnimOrDialog(Landroid/content/Context;Z)V

    .line 55
    return-void
.end method
