.class Lcom/android/server/power/MiuiAttentionDetector$2;
.super Lcom/xiaomi/aon/IMiAONListener$Stub;
.source "MiuiAttentionDetector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/power/MiuiAttentionDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/power/MiuiAttentionDetector;


# direct methods
.method constructor <init>(Lcom/android/server/power/MiuiAttentionDetector;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/power/MiuiAttentionDetector;

    .line 172
    iput-object p1, p0, Lcom/android/server/power/MiuiAttentionDetector$2;->this$0:Lcom/android/server/power/MiuiAttentionDetector;

    invoke-direct {p0}, Lcom/xiaomi/aon/IMiAONListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onCallbackListener(I[I)V
    .locals 4
    .param p1, "type"    # I
    .param p2, "data"    # [I

    .line 175
    if-eqz p2, :cond_6

    array-length v0, p2

    const/16 v1, 0xc

    if-ge v0, v1, :cond_0

    goto :goto_2

    .line 179
    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x5

    const/4 v2, 0x4

    const/4 v3, 0x1

    if-ne p1, v3, :cond_2

    .line 181
    aget v2, p2, v2

    if-nez v2, :cond_1

    aget v1, p2, v1

    if-nez v1, :cond_1

    .line 182
    iget-object v1, p0, Lcom/android/server/power/MiuiAttentionDetector$2;->this$0:Lcom/android/server/power/MiuiAttentionDetector;

    invoke-static {v1, v0}, Lcom/android/server/power/MiuiAttentionDetector;->-$$Nest$mhandleAttentionResult(Lcom/android/server/power/MiuiAttentionDetector;I)V

    goto :goto_1

    .line 184
    :cond_1
    iget-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector$2;->this$0:Lcom/android/server/power/MiuiAttentionDetector;

    invoke-static {v0, v3}, Lcom/android/server/power/MiuiAttentionDetector;->-$$Nest$mhandleAttentionResult(Lcom/android/server/power/MiuiAttentionDetector;I)V

    goto :goto_1

    .line 186
    :cond_2
    if-ne p1, v2, :cond_5

    .line 187
    aget v1, p2, v1

    if-eq v1, v3, :cond_4

    const/16 v1, 0xb

    aget v1, p2, v1

    if-ne v1, v3, :cond_3

    goto :goto_0

    .line 190
    :cond_3
    iget-object v1, p0, Lcom/android/server/power/MiuiAttentionDetector$2;->this$0:Lcom/android/server/power/MiuiAttentionDetector;

    invoke-static {v1, v0}, Lcom/android/server/power/MiuiAttentionDetector;->-$$Nest$mhandleAttentionResult(Lcom/android/server/power/MiuiAttentionDetector;I)V

    goto :goto_1

    .line 188
    :cond_4
    :goto_0
    iget-object v0, p0, Lcom/android/server/power/MiuiAttentionDetector$2;->this$0:Lcom/android/server/power/MiuiAttentionDetector;

    invoke-static {v0, v3}, Lcom/android/server/power/MiuiAttentionDetector;->-$$Nest$mhandleAttentionResult(Lcom/android/server/power/MiuiAttentionDetector;I)V

    .line 193
    :cond_5
    :goto_1
    return-void

    .line 176
    :cond_6
    :goto_2
    invoke-static {}, Lcom/android/server/power/MiuiAttentionDetector;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v0

    const-string v1, "The aon data is valid!"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    return-void
.end method

.method public onImageAvailiable(I)V
    .locals 0
    .param p1, "frame"    # I

    .line 197
    return-void
.end method
