.class public Lcom/android/server/power/statistic/DisplayPortEventStatistic;
.super Ljava/lang/Object;
.source "DisplayPortEventStatistic.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;
    }
.end annotation


# static fields
.field private static final CONNECTED_APP_DURATION:Ljava/lang/String; = "connected_app_duration"

.field private static final CONNECTED_DEVICE_INFO:Ljava/lang/String; = "connected_device_info"

.field private static final CONNECTED_DURATION:Ljava/lang/String; = "connected_duration"

.field private static final CONNECTED_TIMES:Ljava/lang/String; = "connected_times"

.field private static final DEBUG:Z

.field private static final DISPLAY_PORT_EVENT_NAME:Ljava/lang/String; = "display_port_statistic"

.field private static final TAG:Ljava/lang/String; = "DisplayPortEventStatist"

.field private static final UPDATE_REASON_DISPLAY_CONNECTED:I = 0x3

.field private static final UPDATE_REASON_FOREGROUND_APP:I = 0x2

.field private static final UPDATE_REASON_INTERACTIVE:I = 0x1

.field private static final UPDATE_REASON_RESET:I


# instance fields
.field private final mAppId:Ljava/lang/String;

.field private final mConnectedDeviceInfoList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;",
            ">;"
        }
    .end annotation
.end field

.field private mConnectedTimes:I

.field private final mContext:Landroid/content/Context;

.field private mForegroundAppName:Ljava/lang/String;

.field private final mForegroundAppUsageDurationMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mInteractive:Z

.field private mIsDisplayPortConnected:Z

.field private mLastConnectedTime:J

.field private final mLastDisplayPortEvent:Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;

.field private mLastEventTime:J

.field private mLastUpdateReason:I

.field private final mOneTrackerHelper:Lcom/android/server/power/statistic/OneTrackerHelper;

.field private mSumOfConnectedDuration:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 16
    nop

    .line 17
    const-string v0, "debug.miui.dp.statistic.dbg"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    sput-boolean v1, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->DEBUG:Z

    .line 16
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;Lcom/android/server/power/statistic/OneTrackerHelper;)V
    .locals 1
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "oneTrackerHelper"    # Lcom/android/server/power/statistic/OneTrackerHelper;

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mInteractive:Z

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mConnectedDeviceInfoList:Ljava/util/List;

    .line 55
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mForegroundAppUsageDurationMap:Ljava/util/Map;

    .line 56
    new-instance v0, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;

    invoke-direct {v0}, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;-><init>()V

    iput-object v0, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mLastDisplayPortEvent:Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;

    .line 60
    iput-object p1, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mAppId:Ljava/lang/String;

    .line 61
    iput-object p2, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mContext:Landroid/content/Context;

    .line 62
    iput-object p3, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mOneTrackerHelper:Lcom/android/server/power/statistic/OneTrackerHelper;

    .line 63
    return-void
.end method

.method private recordAppUsageTimeDuringConnected(J)V
    .locals 7
    .param p1, "now"    # J

    .line 242
    iget-object v0, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mForegroundAppUsageDurationMap:Ljava/util/Map;

    iget-object v1, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mForegroundAppName:Ljava/lang/String;

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 243
    .local v0, "duration":J
    iget-wide v2, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mLastEventTime:J

    sub-long v2, p1, v2

    add-long/2addr v2, v0

    .line 244
    .local v2, "newDuration":J
    sget-boolean v4, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->DEBUG:Z

    if-eqz v4, :cond_0

    .line 245
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "recordAppUsageTimeDuringConnected: mForegroundAppName: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mForegroundAppName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", newDuration: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "DisplayPortEventStatist"

    invoke-static {v5, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    :cond_0
    iget-object v4, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mForegroundAppUsageDurationMap:Ljava/util/Map;

    iget-object v5, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mForegroundAppName:Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 249
    return-void
.end method

.method private recordConnectedDuration(J)V
    .locals 8
    .param p1, "now"    # J

    .line 221
    iget-wide v0, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mLastConnectedTime:J

    sub-long v0, p1, v0

    .line 222
    .local v0, "duration":J
    sget-boolean v2, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->DEBUG:Z

    if-eqz v2, :cond_0

    .line 223
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "recordConnectedDuration: duration: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "DisplayPortEventStatist"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    :cond_0
    iget-wide v2, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mSumOfConnectedDuration:J

    add-long/2addr v2, v0

    iput-wide v2, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mSumOfConnectedDuration:J

    .line 227
    iget-object v2, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mConnectedDeviceInfoList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;

    .line 228
    .local v3, "displayPortEvent":Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;
    iget-object v4, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mLastDisplayPortEvent:Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;

    invoke-virtual {v4}, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->getPhysicalDisplayId()J

    move-result-wide v4

    .line 229
    invoke-virtual {v3}, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->getPhysicalDisplayId()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-nez v4, :cond_1

    .line 230
    return-void

    .line 232
    .end local v3    # "displayPortEvent":Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;
    :cond_1
    goto :goto_0

    .line 234
    :cond_2
    iget-object v2, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mConnectedDeviceInfoList:Ljava/util/List;

    iget-object v3, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mLastDisplayPortEvent:Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 235
    return-void
.end method

.method private resetDisplayPortEventInfo(J)V
    .locals 3
    .param p1, "now"    # J

    .line 175
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mConnectedTimes:I

    .line 176
    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mSumOfConnectedDuration:J

    .line 177
    invoke-direct {p0, p1, p2, v0}, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->updateLastAppUsageEventInfo(JI)V

    .line 178
    iget-object v0, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mLastDisplayPortEvent:Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;

    invoke-direct {p0, p1, p2, v0}, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->updateConnectedEventInfo(JLcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;)V

    .line 179
    iget-object v0, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mConnectedDeviceInfoList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 180
    iget-object v0, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mForegroundAppUsageDurationMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 181
    return-void
.end method

.method private updateConnectedEventInfo(JLcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;)V
    .locals 1
    .param p1, "eventTime"    # J
    .param p3, "displayPortEvent"    # Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;

    .line 209
    iget-boolean v0, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mIsDisplayPortConnected:Z

    if-eqz v0, :cond_0

    .line 210
    iput-wide p1, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mLastConnectedTime:J

    .line 211
    iget v0, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mConnectedTimes:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mConnectedTimes:I

    .line 212
    iget-object v0, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mLastDisplayPortEvent:Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;

    invoke-virtual {v0, p3}, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->copyFrom(Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;)V

    .line 214
    :cond_0
    return-void
.end method

.method private updateLastAppUsageEventInfo(JI)V
    .locals 3
    .param p1, "eventTime"    # J
    .param p3, "updateReason"    # I

    .line 193
    iget-boolean v0, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mIsDisplayPortConnected:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mInteractive:Z

    if-eqz v0, :cond_0

    .line 194
    iput-wide p1, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mLastEventTime:J

    .line 195
    iput p3, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mLastUpdateReason:I

    .line 196
    sget-boolean v0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 197
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "updateLastAppUsageEventInfo: mLastEventTime: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mLastEventTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mLastUpdateReason: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mLastUpdateReason:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "DisplayPortEventStatist"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    :cond_0
    return-void
.end method


# virtual methods
.method public handleConnectedStateChanged(Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;)V
    .locals 6
    .param p1, "displayPortEvent"    # Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;

    .line 256
    invoke-virtual {p1}, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->getIsConnected()Z

    move-result v0

    .line 257
    .local v0, "isConnected":Z
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    .line 258
    .local v1, "now":J
    iget-boolean v3, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mIsDisplayPortConnected:Z

    if-eq v3, v0, :cond_3

    .line 259
    iput-boolean v0, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mIsDisplayPortConnected:Z

    .line 260
    sget-boolean v3, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->DEBUG:Z

    if-eqz v3, :cond_0

    .line 261
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handleConnectedStateChanged: connected: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 262
    invoke-virtual {p1}, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->getIsConnected()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", physic display id: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 263
    invoke-virtual {p1}, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->getPhysicalDisplayId()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", product name: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 264
    invoke-virtual {p1}, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->getProductName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", resolution: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 265
    invoke-virtual {p1}, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->getResolution()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", frame rate: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 266
    invoke-virtual {p1}, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->getFrameRate()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 261
    const-string v4, "DisplayPortEventStatist"

    invoke-static {v4, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    :cond_0
    iget-boolean v3, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mIsDisplayPortConnected:Z

    if-eqz v3, :cond_1

    .line 270
    const/4 v3, 0x3

    invoke-direct {p0, v1, v2, v3}, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->updateLastAppUsageEventInfo(JI)V

    .line 271
    invoke-direct {p0, v1, v2, p1}, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->updateConnectedEventInfo(JLcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;)V

    goto :goto_0

    .line 274
    :cond_1
    iget-boolean v3, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mInteractive:Z

    if-eqz v3, :cond_2

    .line 275
    invoke-direct {p0, v1, v2}, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->recordAppUsageTimeDuringConnected(J)V

    .line 277
    :cond_2
    invoke-direct {p0, v1, v2}, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->recordConnectedDuration(J)V

    .line 280
    :cond_3
    :goto_0
    return-void
.end method

.method public handleForegroundAppChanged(Ljava/lang/String;)V
    .locals 4
    .param p1, "foregroundAppName"    # Ljava/lang/String;

    .line 283
    iget-object v0, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mForegroundAppName:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 284
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 285
    .local v0, "now":J
    iget-boolean v2, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mInteractive:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mIsDisplayPortConnected:Z

    if-eqz v2, :cond_0

    .line 286
    invoke-direct {p0, v0, v1}, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->recordAppUsageTimeDuringConnected(J)V

    .line 288
    :cond_0
    const/4 v2, 0x2

    invoke-direct {p0, v0, v1, v2}, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->updateLastAppUsageEventInfo(JI)V

    .line 290
    iput-object p1, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mForegroundAppName:Ljava/lang/String;

    .line 291
    sget-boolean v2, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->DEBUG:Z

    if-eqz v2, :cond_1

    .line 292
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleForegroundAppChanged: mForegroundAppName: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mForegroundAppName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "DisplayPortEventStatist"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    .end local v0    # "now":J
    :cond_1
    return-void
.end method

.method public handleInteractiveChanged(Z)V
    .locals 3
    .param p1, "interactive"    # Z

    .line 298
    iget-boolean v0, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mInteractive:Z

    if-eq v0, p1, :cond_1

    .line 300
    iput-boolean p1, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mInteractive:Z

    .line 301
    sget-boolean v0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 302
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "handleInteractiveChanged: mInteractive: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mInteractive:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "DisplayPortEventStatist"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 305
    .local v0, "now":J
    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->updateLastAppUsageEventInfo(JI)V

    .line 307
    iget-boolean v2, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mIsDisplayPortConnected:Z

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mInteractive:Z

    if-nez v2, :cond_1

    .line 308
    invoke-direct {p0, v0, v1}, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->recordAppUsageTimeDuringConnected(J)V

    .line 311
    .end local v0    # "now":J
    :cond_1
    return-void
.end method

.method public reportDisplayPortEventByIntent()V
    .locals 7

    .line 144
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 146
    .local v0, "now":J
    iget-boolean v2, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mIsDisplayPortConnected:Z

    if-eqz v2, :cond_0

    .line 147
    invoke-direct {p0, v0, v1}, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->recordConnectedDuration(J)V

    .line 148
    iget-boolean v2, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mInteractive:Z

    if-eqz v2, :cond_0

    .line 149
    invoke-direct {p0, v0, v1}, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->recordAppUsageTimeDuringConnected(J)V

    .line 152
    :cond_0
    iget-object v2, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mOneTrackerHelper:Lcom/android/server/power/statistic/OneTrackerHelper;

    iget-object v3, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mAppId:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mContext:Landroid/content/Context;

    .line 153
    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    .line 152
    const-string v5, "display_port_statistic"

    invoke-virtual {v2, v3, v5, v4}, Lcom/android/server/power/statistic/OneTrackerHelper;->getTrackEventIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 154
    .local v2, "intent":Landroid/content/Intent;
    iget v3, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mConnectedTimes:I

    if-eqz v3, :cond_1

    .line 155
    const-string v4, "connected_times"

    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v3

    iget-wide v4, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mSumOfConnectedDuration:J

    .line 156
    const-string v6, "connected_duration"

    invoke-virtual {v3, v6, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mConnectedDeviceInfoList:Ljava/util/List;

    .line 157
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "connected_device_info"

    invoke-virtual {v3, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mForegroundAppUsageDurationMap:Ljava/util/Map;

    .line 158
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "connected_app_duration"

    invoke-virtual {v3, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 160
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "reportDisplayPortEventByIntent: data: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "DisplayPortEventStatist"

    invoke-static {v4, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    :try_start_0
    iget-object v3, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mOneTrackerHelper:Lcom/android/server/power/statistic/OneTrackerHelper;

    invoke-virtual {v3, v2}, Lcom/android/server/power/statistic/OneTrackerHelper;->reportTrackEventByIntent(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 166
    goto :goto_0

    .line 164
    :catch_0
    move-exception v3

    .line 165
    .local v3, "e":Ljava/lang/Exception;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "reportDisplayPortEventByIntent fail! "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    .end local v3    # "e":Ljava/lang/Exception;
    :goto_0
    invoke-direct {p0, v0, v1}, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->resetDisplayPortEventInfo(J)V

    .line 168
    return-void
.end method
