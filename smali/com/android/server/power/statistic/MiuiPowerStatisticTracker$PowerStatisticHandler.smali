.class Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerStatisticHandler;
.super Landroid/os/Handler;
.source "MiuiPowerStatisticTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PowerStatisticHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;


# direct methods
.method public constructor <init>(Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 322
    iput-object p1, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerStatisticHandler;->this$0:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;

    .line 323
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 324
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .line 328
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_1

    .line 358
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerStatisticHandler;->this$0:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;

    invoke-static {v0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->-$$Nest$fgetmDimEventStatistic(Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;)Lcom/android/server/power/statistic/DimEventStatistic;

    move-result-object v0

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/android/server/power/statistic/DimEventStatistic$DimEvent;

    invoke-virtual {v0, v1}, Lcom/android/server/power/statistic/DimEventStatistic;->notifyDimStateChanged(Lcom/android/server/power/statistic/DimEventStatistic$DimEvent;)V

    .line 359
    goto/16 :goto_1

    .line 361
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerStatisticHandler;->this$0:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;

    invoke-virtual {v0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->dumpLocal()V

    .line 362
    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerStatisticHandler;->this$0:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;

    invoke-static {v0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->-$$Nest$fgetmTofEventStatistic(Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;)Lcom/android/server/power/statistic/TofEventStatistic;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/power/statistic/TofEventStatistic;->dumpLocal()V

    .line 363
    goto/16 :goto_1

    .line 355
    :pswitch_2
    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerStatisticHandler;->this$0:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;

    invoke-static {v0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->-$$Nest$fgetmDisplayPortEventStatistic(Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;)Lcom/android/server/power/statistic/DisplayPortEventStatistic;

    move-result-object v0

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->handleForegroundAppChanged(Ljava/lang/String;)V

    .line 356
    goto :goto_1

    .line 351
    :pswitch_3
    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerStatisticHandler;->this$0:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;

    invoke-static {v0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->-$$Nest$fgetmDisplayPortEventStatistic(Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;)Lcom/android/server/power/statistic/DisplayPortEventStatistic;

    move-result-object v0

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;

    invoke-virtual {v0, v1}, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->handleConnectedStateChanged(Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;)V

    .line 353
    goto :goto_1

    .line 348
    :pswitch_4
    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerStatisticHandler;->this$0:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;

    invoke-static {v0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->-$$Nest$fgetmTofEventStatistic(Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;)Lcom/android/server/power/statistic/TofEventStatistic;

    move-result-object v0

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/server/power/statistic/TofEventStatistic;->notifyTofPowerState(Z)V

    .line 349
    goto :goto_1

    .line 345
    :pswitch_5
    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerStatisticHandler;->this$0:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;

    invoke-static {v0}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->-$$Nest$fgetmTofEventStatistic(Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;)Lcom/android/server/power/statistic/TofEventStatistic;

    move-result-object v0

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget v2, p1, Landroid/os/Message;->arg1:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    iget v2, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {v0, v1, v3, v2}, Lcom/android/server/power/statistic/TofEventStatistic;->notifyGestureEvent(Ljava/lang/String;ZI)V

    .line 346
    goto :goto_1

    .line 342
    :pswitch_6
    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerStatisticHandler;->this$0:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {v0, v1, v2}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->handleUserAttentionChanged(II)V

    .line 343
    goto :goto_1

    .line 339
    :pswitch_7
    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerStatisticHandler;->this$0:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->handleDisplayStateChangedLatencyLocked(I)V

    .line 340
    goto :goto_1

    .line 336
    :pswitch_8
    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerStatisticHandler;->this$0:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    invoke-static {v0, v1, v2}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->-$$Nest$mhandleScreenOnUnBlocker(Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;II)V

    .line 337
    goto :goto_1

    .line 333
    :pswitch_9
    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerStatisticHandler;->this$0:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->handleWakefulnessCompleted(J)V

    .line 334
    goto :goto_1

    .line 330
    :pswitch_a
    iget-object v0, p0, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerStatisticHandler;->this$0:Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;

    invoke-virtual {v0, v1}, Lcom/android/server/power/statistic/MiuiPowerStatisticTracker;->handleWakefulnessChanged(Lcom/android/server/power/statistic/MiuiPowerStatisticTracker$PowerEvent;)V

    .line 331
    nop

    .line 367
    :goto_1
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
