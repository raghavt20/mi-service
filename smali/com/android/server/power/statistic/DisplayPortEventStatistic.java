public class com.android.server.power.statistic.DisplayPortEventStatistic {
	 /* .source "DisplayPortEventStatistic.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String CONNECTED_APP_DURATION;
private static final java.lang.String CONNECTED_DEVICE_INFO;
private static final java.lang.String CONNECTED_DURATION;
private static final java.lang.String CONNECTED_TIMES;
private static final Boolean DEBUG;
private static final java.lang.String DISPLAY_PORT_EVENT_NAME;
private static final java.lang.String TAG;
private static final Integer UPDATE_REASON_DISPLAY_CONNECTED;
private static final Integer UPDATE_REASON_FOREGROUND_APP;
private static final Integer UPDATE_REASON_INTERACTIVE;
private static final Integer UPDATE_REASON_RESET;
/* # instance fields */
private final java.lang.String mAppId;
private final java.util.List mConnectedDeviceInfoList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Integer mConnectedTimes;
private final android.content.Context mContext;
private java.lang.String mForegroundAppName;
private final java.util.Map mForegroundAppUsageDurationMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Long;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Boolean mInteractive;
private Boolean mIsDisplayPortConnected;
private Long mLastConnectedTime;
private final com.android.server.power.statistic.DisplayPortEventStatistic$DisplayPortEvent mLastDisplayPortEvent;
private Long mLastEventTime;
private Integer mLastUpdateReason;
private final com.android.server.power.statistic.OneTrackerHelper mOneTrackerHelper;
private Long mSumOfConnectedDuration;
/* # direct methods */
static com.android.server.power.statistic.DisplayPortEventStatistic ( ) {
/* .locals 2 */
/* .line 16 */
/* nop */
/* .line 17 */
final String v0 = "debug.miui.dp.statistic.dbg"; // const-string v0, "debug.miui.dp.statistic.dbg"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getInt ( v0,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
com.android.server.power.statistic.DisplayPortEventStatistic.DEBUG = (v1!= 0);
/* .line 16 */
return;
} // .end method
public com.android.server.power.statistic.DisplayPortEventStatistic ( ) {
/* .locals 1 */
/* .param p1, "appId" # Ljava/lang/String; */
/* .param p2, "context" # Landroid/content/Context; */
/* .param p3, "oneTrackerHelper" # Lcom/android/server/power/statistic/OneTrackerHelper; */
/* .line 59 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 41 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mInteractive:Z */
/* .line 53 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mConnectedDeviceInfoList = v0;
/* .line 55 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mForegroundAppUsageDurationMap = v0;
/* .line 56 */
/* new-instance v0, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent; */
/* invoke-direct {v0}, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;-><init>()V */
this.mLastDisplayPortEvent = v0;
/* .line 60 */
this.mAppId = p1;
/* .line 61 */
this.mContext = p2;
/* .line 62 */
this.mOneTrackerHelper = p3;
/* .line 63 */
return;
} // .end method
private void recordAppUsageTimeDuringConnected ( Long p0 ) {
/* .locals 7 */
/* .param p1, "now" # J */
/* .line 242 */
v0 = this.mForegroundAppUsageDurationMap;
v1 = this.mForegroundAppName;
/* const-wide/16 v2, 0x0 */
java.lang.Long .valueOf ( v2,v3 );
/* check-cast v0, Ljava/lang/Long; */
(( java.lang.Long ) v0 ).longValue ( ); // invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
/* move-result-wide v0 */
/* .line 243 */
/* .local v0, "duration":J */
/* iget-wide v2, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mLastEventTime:J */
/* sub-long v2, p1, v2 */
/* add-long/2addr v2, v0 */
/* .line 244 */
/* .local v2, "newDuration":J */
/* sget-boolean v4, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->DEBUG:Z */
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 245 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "recordAppUsageTimeDuringConnected: mForegroundAppName: "; // const-string v5, "recordAppUsageTimeDuringConnected: mForegroundAppName: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.mForegroundAppName;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = ", newDuration: "; // const-string v5, ", newDuration: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2, v3 ); // invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "DisplayPortEventStatist"; // const-string v5, "DisplayPortEventStatist"
android.util.Slog .d ( v5,v4 );
/* .line 248 */
} // :cond_0
v4 = this.mForegroundAppUsageDurationMap;
v5 = this.mForegroundAppName;
java.lang.Long .valueOf ( v2,v3 );
/* .line 249 */
return;
} // .end method
private void recordConnectedDuration ( Long p0 ) {
/* .locals 8 */
/* .param p1, "now" # J */
/* .line 221 */
/* iget-wide v0, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mLastConnectedTime:J */
/* sub-long v0, p1, v0 */
/* .line 222 */
/* .local v0, "duration":J */
/* sget-boolean v2, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 223 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "recordConnectedDuration: duration: "; // const-string v3, "recordConnectedDuration: duration: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0, v1 ); // invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "DisplayPortEventStatist"; // const-string v3, "DisplayPortEventStatist"
android.util.Slog .d ( v3,v2 );
/* .line 225 */
} // :cond_0
/* iget-wide v2, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mSumOfConnectedDuration:J */
/* add-long/2addr v2, v0 */
/* iput-wide v2, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mSumOfConnectedDuration:J */
/* .line 227 */
v2 = this.mConnectedDeviceInfoList;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_2
/* check-cast v3, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent; */
/* .line 228 */
/* .local v3, "displayPortEvent":Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent; */
v4 = this.mLastDisplayPortEvent;
(( com.android.server.power.statistic.DisplayPortEventStatistic$DisplayPortEvent ) v4 ).getPhysicalDisplayId ( ); // invoke-virtual {v4}, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->getPhysicalDisplayId()J
/* move-result-wide v4 */
/* .line 229 */
(( com.android.server.power.statistic.DisplayPortEventStatistic$DisplayPortEvent ) v3 ).getPhysicalDisplayId ( ); // invoke-virtual {v3}, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->getPhysicalDisplayId()J
/* move-result-wide v6 */
/* cmp-long v4, v4, v6 */
/* if-nez v4, :cond_1 */
/* .line 230 */
return;
/* .line 232 */
} // .end local v3 # "displayPortEvent":Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;
} // :cond_1
/* .line 234 */
} // :cond_2
v2 = this.mConnectedDeviceInfoList;
v3 = this.mLastDisplayPortEvent;
/* .line 235 */
return;
} // .end method
private void resetDisplayPortEventInfo ( Long p0 ) {
/* .locals 3 */
/* .param p1, "now" # J */
/* .line 175 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mConnectedTimes:I */
/* .line 176 */
/* const-wide/16 v1, 0x0 */
/* iput-wide v1, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mSumOfConnectedDuration:J */
/* .line 177 */
/* invoke-direct {p0, p1, p2, v0}, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->updateLastAppUsageEventInfo(JI)V */
/* .line 178 */
v0 = this.mLastDisplayPortEvent;
/* invoke-direct {p0, p1, p2, v0}, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->updateConnectedEventInfo(JLcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;)V */
/* .line 179 */
v0 = this.mConnectedDeviceInfoList;
/* .line 180 */
v0 = this.mForegroundAppUsageDurationMap;
/* .line 181 */
return;
} // .end method
private void updateConnectedEventInfo ( Long p0, com.android.server.power.statistic.DisplayPortEventStatistic$DisplayPortEvent p1 ) {
/* .locals 1 */
/* .param p1, "eventTime" # J */
/* .param p3, "displayPortEvent" # Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent; */
/* .line 209 */
/* iget-boolean v0, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mIsDisplayPortConnected:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 210 */
/* iput-wide p1, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mLastConnectedTime:J */
/* .line 211 */
/* iget v0, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mConnectedTimes:I */
/* add-int/lit8 v0, v0, 0x1 */
/* iput v0, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mConnectedTimes:I */
/* .line 212 */
v0 = this.mLastDisplayPortEvent;
(( com.android.server.power.statistic.DisplayPortEventStatistic$DisplayPortEvent ) v0 ).copyFrom ( p3 ); // invoke-virtual {v0, p3}, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->copyFrom(Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;)V
/* .line 214 */
} // :cond_0
return;
} // .end method
private void updateLastAppUsageEventInfo ( Long p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "eventTime" # J */
/* .param p3, "updateReason" # I */
/* .line 193 */
/* iget-boolean v0, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mIsDisplayPortConnected:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mInteractive:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 194 */
/* iput-wide p1, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mLastEventTime:J */
/* .line 195 */
/* iput p3, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mLastUpdateReason:I */
/* .line 196 */
/* sget-boolean v0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 197 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "updateLastAppUsageEventInfo: mLastEventTime: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mLastEventTime:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = ", mLastUpdateReason: "; // const-string v1, ", mLastUpdateReason: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mLastUpdateReason:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "DisplayPortEventStatist"; // const-string v1, "DisplayPortEventStatist"
android.util.Slog .d ( v1,v0 );
/* .line 201 */
} // :cond_0
return;
} // .end method
/* # virtual methods */
public void handleConnectedStateChanged ( com.android.server.power.statistic.DisplayPortEventStatistic$DisplayPortEvent p0 ) {
/* .locals 6 */
/* .param p1, "displayPortEvent" # Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent; */
/* .line 256 */
v0 = (( com.android.server.power.statistic.DisplayPortEventStatistic$DisplayPortEvent ) p1 ).getIsConnected ( ); // invoke-virtual {p1}, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->getIsConnected()Z
/* .line 257 */
/* .local v0, "isConnected":Z */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
/* .line 258 */
/* .local v1, "now":J */
/* iget-boolean v3, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mIsDisplayPortConnected:Z */
/* if-eq v3, v0, :cond_3 */
/* .line 259 */
/* iput-boolean v0, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mIsDisplayPortConnected:Z */
/* .line 260 */
/* sget-boolean v3, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->DEBUG:Z */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 261 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "handleConnectedStateChanged: connected: "; // const-string v4, "handleConnectedStateChanged: connected: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 262 */
v4 = (( com.android.server.power.statistic.DisplayPortEventStatistic$DisplayPortEvent ) p1 ).getIsConnected ( ); // invoke-virtual {p1}, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->getIsConnected()Z
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v4 = ", physic display id: "; // const-string v4, ", physic display id: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 263 */
(( com.android.server.power.statistic.DisplayPortEventStatistic$DisplayPortEvent ) p1 ).getPhysicalDisplayId ( ); // invoke-virtual {p1}, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->getPhysicalDisplayId()J
/* move-result-wide v4 */
(( java.lang.StringBuilder ) v3 ).append ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v4 = ", product name: "; // const-string v4, ", product name: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 264 */
(( com.android.server.power.statistic.DisplayPortEventStatistic$DisplayPortEvent ) p1 ).getProductName ( ); // invoke-virtual {p1}, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->getProductName()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ", resolution: "; // const-string v4, ", resolution: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 265 */
(( com.android.server.power.statistic.DisplayPortEventStatistic$DisplayPortEvent ) p1 ).getResolution ( ); // invoke-virtual {p1}, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->getResolution()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ", frame rate: "; // const-string v4, ", frame rate: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 266 */
v4 = (( com.android.server.power.statistic.DisplayPortEventStatistic$DisplayPortEvent ) p1 ).getFrameRate ( ); // invoke-virtual {p1}, Lcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;->getFrameRate()I
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 261 */
final String v4 = "DisplayPortEventStatist"; // const-string v4, "DisplayPortEventStatist"
android.util.Slog .d ( v4,v3 );
/* .line 268 */
} // :cond_0
/* iget-boolean v3, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mIsDisplayPortConnected:Z */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 270 */
int v3 = 3; // const/4 v3, 0x3
/* invoke-direct {p0, v1, v2, v3}, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->updateLastAppUsageEventInfo(JI)V */
/* .line 271 */
/* invoke-direct {p0, v1, v2, p1}, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->updateConnectedEventInfo(JLcom/android/server/power/statistic/DisplayPortEventStatistic$DisplayPortEvent;)V */
/* .line 274 */
} // :cond_1
/* iget-boolean v3, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mInteractive:Z */
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 275 */
/* invoke-direct {p0, v1, v2}, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->recordAppUsageTimeDuringConnected(J)V */
/* .line 277 */
} // :cond_2
/* invoke-direct {p0, v1, v2}, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->recordConnectedDuration(J)V */
/* .line 280 */
} // :cond_3
} // :goto_0
return;
} // .end method
public void handleForegroundAppChanged ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "foregroundAppName" # Ljava/lang/String; */
/* .line 283 */
v0 = this.mForegroundAppName;
v0 = android.text.TextUtils .equals ( v0,p1 );
/* if-nez v0, :cond_1 */
/* .line 284 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 285 */
/* .local v0, "now":J */
/* iget-boolean v2, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mInteractive:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* iget-boolean v2, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mIsDisplayPortConnected:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 286 */
/* invoke-direct {p0, v0, v1}, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->recordAppUsageTimeDuringConnected(J)V */
/* .line 288 */
} // :cond_0
int v2 = 2; // const/4 v2, 0x2
/* invoke-direct {p0, v0, v1, v2}, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->updateLastAppUsageEventInfo(JI)V */
/* .line 290 */
this.mForegroundAppName = p1;
/* .line 291 */
/* sget-boolean v2, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 292 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "handleForegroundAppChanged: mForegroundAppName: "; // const-string v3, "handleForegroundAppChanged: mForegroundAppName: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mForegroundAppName;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "DisplayPortEventStatist"; // const-string v3, "DisplayPortEventStatist"
android.util.Slog .d ( v3,v2 );
/* .line 295 */
} // .end local v0 # "now":J
} // :cond_1
return;
} // .end method
public void handleInteractiveChanged ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "interactive" # Z */
/* .line 298 */
/* iget-boolean v0, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mInteractive:Z */
/* if-eq v0, p1, :cond_1 */
/* .line 300 */
/* iput-boolean p1, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mInteractive:Z */
/* .line 301 */
/* sget-boolean v0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 302 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "handleInteractiveChanged: mInteractive: "; // const-string v1, "handleInteractiveChanged: mInteractive: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mInteractive:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "DisplayPortEventStatist"; // const-string v1, "DisplayPortEventStatist"
android.util.Slog .d ( v1,v0 );
/* .line 304 */
} // :cond_0
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 305 */
/* .local v0, "now":J */
int v2 = 1; // const/4 v2, 0x1
/* invoke-direct {p0, v0, v1, v2}, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->updateLastAppUsageEventInfo(JI)V */
/* .line 307 */
/* iget-boolean v2, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mIsDisplayPortConnected:Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* iget-boolean v2, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mInteractive:Z */
/* if-nez v2, :cond_1 */
/* .line 308 */
/* invoke-direct {p0, v0, v1}, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->recordAppUsageTimeDuringConnected(J)V */
/* .line 311 */
} // .end local v0 # "now":J
} // :cond_1
return;
} // .end method
public void reportDisplayPortEventByIntent ( ) {
/* .locals 7 */
/* .line 144 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 146 */
/* .local v0, "now":J */
/* iget-boolean v2, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mIsDisplayPortConnected:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 147 */
/* invoke-direct {p0, v0, v1}, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->recordConnectedDuration(J)V */
/* .line 148 */
/* iget-boolean v2, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mInteractive:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 149 */
/* invoke-direct {p0, v0, v1}, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->recordAppUsageTimeDuringConnected(J)V */
/* .line 152 */
} // :cond_0
v2 = this.mOneTrackerHelper;
v3 = this.mAppId;
v4 = this.mContext;
/* .line 153 */
(( android.content.Context ) v4 ).getPackageName ( ); // invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;
/* .line 152 */
final String v5 = "display_port_statistic"; // const-string v5, "display_port_statistic"
(( com.android.server.power.statistic.OneTrackerHelper ) v2 ).getTrackEventIntent ( v3, v5, v4 ); // invoke-virtual {v2, v3, v5, v4}, Lcom/android/server/power/statistic/OneTrackerHelper;->getTrackEventIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 154 */
/* .local v2, "intent":Landroid/content/Intent; */
/* iget v3, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mConnectedTimes:I */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 155 */
final String v4 = "connected_times"; // const-string v4, "connected_times"
(( android.content.Intent ) v2 ).putExtra ( v4, v3 ); // invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* iget-wide v4, p0, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->mSumOfConnectedDuration:J */
/* .line 156 */
final String v6 = "connected_duration"; // const-string v6, "connected_duration"
(( android.content.Intent ) v3 ).putExtra ( v6, v4, v5 ); // invoke-virtual {v3, v6, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;
v4 = this.mConnectedDeviceInfoList;
/* .line 157 */
(( java.lang.Object ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;
final String v5 = "connected_device_info"; // const-string v5, "connected_device_info"
(( android.content.Intent ) v3 ).putExtra ( v5, v4 ); // invoke-virtual {v3, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
v4 = this.mForegroundAppUsageDurationMap;
/* .line 158 */
(( java.lang.Object ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;
final String v5 = "connected_app_duration"; // const-string v5, "connected_app_duration"
(( android.content.Intent ) v3 ).putExtra ( v5, v4 ); // invoke-virtual {v3, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 160 */
} // :cond_1
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "reportDisplayPortEventByIntent: data: "; // const-string v4, "reportDisplayPortEventByIntent: data: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.content.Intent ) v2 ).getExtras ( ); // invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "DisplayPortEventStatist"; // const-string v4, "DisplayPortEventStatist"
android.util.Slog .d ( v4,v3 );
/* .line 163 */
try { // :try_start_0
v3 = this.mOneTrackerHelper;
(( com.android.server.power.statistic.OneTrackerHelper ) v3 ).reportTrackEventByIntent ( v2 ); // invoke-virtual {v3, v2}, Lcom/android/server/power/statistic/OneTrackerHelper;->reportTrackEventByIntent(Landroid/content/Intent;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 166 */
/* .line 164 */
/* :catch_0 */
/* move-exception v3 */
/* .line 165 */
/* .local v3, "e":Ljava/lang/Exception; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "reportDisplayPortEventByIntent fail! "; // const-string v6, "reportDisplayPortEventByIntent fail! "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v4,v5 );
/* .line 167 */
} // .end local v3 # "e":Ljava/lang/Exception;
} // :goto_0
/* invoke-direct {p0, v0, v1}, Lcom/android/server/power/statistic/DisplayPortEventStatistic;->resetDisplayPortEventInfo(J)V */
/* .line 168 */
return;
} // .end method
