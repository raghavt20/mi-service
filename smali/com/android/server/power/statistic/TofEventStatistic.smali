.class public Lcom/android/server/power/statistic/TofEventStatistic;
.super Ljava/lang/Object;
.source "TofEventStatistic.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;
    }
.end annotation


# static fields
.field private static final AON_GESTURE_CUE_TONE:Ljava/lang/String; = "miui_aon_gesture_cue_tone"

.field public static final FEATURE_AON_GESTURE_SUPPORT:Ljava/lang/String; = "config_aon_gesture_available"

.field private static final FEATURE_TOF_GESTURE_SUPPORT:Ljava/lang/String; = "config_tof_gesture_available"

.field private static final FEATURE_TOF_PROXIMITY_SUPPORT:Ljava/lang/String; = "config_tof_proximity_available"

.field private static final KEY_AON_APP_USAGE_FREQUENCY:Ljava/lang/String; = "aon_app_usage_frequency"

.field private static final KEY_AON_GESTURE_CUE_TONE_ENABLE:Ljava/lang/String; = "aon_gesture_cue_tone_enable"

.field private static final KEY_AON_GESTURE_ENABLE:Ljava/lang/String; = "aon_gesture_enable"

.field private static final KEY_AON_GESTURE_USAGE_COUNT:Ljava/lang/String; = "aon_gesture_count"

.field private static final KEY_TOF_APP_USAGE_FREQUENCY:Ljava/lang/String; = "tof_app_usage_frequency"

.field private static final KEY_TOF_GESTURE_CUE_TONE_ENABLE:Ljava/lang/String; = "tof_gesture_cue_tone_enable"

.field private static final KEY_TOF_GESTURE_ENABLE:Ljava/lang/String; = "tof_gesture_enable"

.field private static final KEY_TOF_GESTURE_USAGE_COUNT:Ljava/lang/String; = "tof_gesture_count"

.field private static final KEY_TOF_SCREEN_OFF_ENABLE:Ljava/lang/String; = "tof_screen_off_enable"

.field private static final KEY_TOF_SCREEN_OFF_USAGE_COUNT:Ljava/lang/String; = "tof_screen_off_count"

.field private static final KEY_TOF_SCREEN_ON_ENABLE:Ljava/lang/String; = "tof_screen_on_enable"

.field private static final KEY_TOF_SCREEN_ON_USAGE_COUNT:Ljava/lang/String; = "tof_screen_on_count"

.field private static final SUPPORT_AON_GESTURE:Z

.field private static final SUPPORT_TOF_GESTURE:Z

.field private static final SUPPORT_TOF_PROXIMITY:Z

.field public static final TAG:Ljava/lang/String; = "MiuiTofStatisticTracker"

.field public static final TOF_EVENT_NAME:Ljava/lang/String; = "tof_statistic"

.field private static final TOF_GESTURE_CUE_TONE:Ljava/lang/String; = "miui_tof_gesture_cue_tone"

.field public static final TOF_GESTURE_DOUBLE_PRESS:I = 0x7

.field public static final TOF_GESTURE_DOWN:I = 0x3

.field public static final TOF_GESTURE_DRAW_CIRCLE:I = 0x8

.field public static final TOF_GESTURE_LEFT:I = 0x1

.field public static final TOF_GESTURE_RIGHT:I = 0x2

.field public static final TOF_GESTURE_UP:I = 0x4


# instance fields
.field private mAonGestureCueToneEnable:Z

.field private mAonGestureEnable:Z

.field private mAppId:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mOneTrackerHelper:Lcom/android/server/power/statistic/OneTrackerHelper;

.field private mTofAppUsageEventsMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;",
            ">;"
        }
    .end annotation
.end field

.field private mTofGestureCueToneEnable:Z

.field private mTofGestureEnable:Z

.field private mTofGestureUsageCount:I

.field private mTofScreenOffEnable:Z

.field private mTofScreenOffUsageCount:I

.field private mTofScreenOnEnable:Z

.field private mTofScreenOnUsageCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 46
    const-string v0, "config_tof_proximity_available"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/power/statistic/TofEventStatistic;->SUPPORT_TOF_PROXIMITY:Z

    .line 47
    const-string v0, "config_tof_gesture_available"

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/power/statistic/TofEventStatistic;->SUPPORT_TOF_GESTURE:Z

    .line 48
    const-string v0, "config_aon_gesture_available"

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/power/statistic/TofEventStatistic;->SUPPORT_AON_GESTURE:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;Lcom/android/server/power/statistic/OneTrackerHelper;)V
    .locals 1
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "oneTrackerHelper"    # Lcom/android/server/power/statistic/OneTrackerHelper;

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofAppUsageEventsMap:Ljava/util/Map;

    .line 71
    iput-object p1, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mAppId:Ljava/lang/String;

    .line 72
    iput-object p2, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mContext:Landroid/content/Context;

    .line 73
    iput-object p3, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mOneTrackerHelper:Lcom/android/server/power/statistic/OneTrackerHelper;

    .line 74
    return-void
.end method

.method private getTofAppUsageEvents(Ljava/util/Map;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;",
            ">;)",
            "Ljava/util/List;"
        }
    .end annotation

    .line 146
    .local p1, "appUsageEvents":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 147
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;>;"
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 148
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;

    .line 149
    .local v3, "appUsage":Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 150
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;>;"
    .end local v3    # "appUsage":Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;
    goto :goto_0

    .line 151
    :cond_0
    return-object v0
.end method

.method private getTofAppUsageEventsString()Ljava/lang/String;
    .locals 3

    .line 137
    const-string v0, ""

    .line 138
    .local v0, "appUsage":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofAppUsageEventsMap:Ljava/util/Map;

    invoke-direct {p0, v1}, Lcom/android/server/power/statistic/TofEventStatistic;->getTofAppUsageEvents(Ljava/util/Map;)Ljava/util/List;

    move-result-object v1

    .line 139
    .local v1, "appUsageList":Ljava/util/List;
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 140
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 142
    :cond_0
    return-object v0
.end method

.method private resetTofEventData()V
    .locals 2

    .line 124
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofScreenOnEnable:Z

    .line 125
    iput-boolean v0, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofScreenOffEnable:Z

    .line 126
    iput-boolean v0, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofGestureEnable:Z

    .line 127
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofGestureCueToneEnable:Z

    .line 128
    iput-boolean v0, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mAonGestureEnable:Z

    .line 129
    iput-boolean v1, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mAonGestureCueToneEnable:Z

    .line 130
    iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofGestureUsageCount:I

    .line 131
    iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofScreenOnUsageCount:I

    .line 132
    iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofScreenOffUsageCount:I

    .line 133
    iget-object v0, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofAppUsageEventsMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 134
    return-void
.end method


# virtual methods
.method public dumpLocal()V
    .locals 3

    .line 179
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v1, "MiuiToFStaticTracker:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 180
    .local v0, "stringBuffer":Ljava/lang/StringBuffer;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\nScreenOnEnable:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofScreenOnEnable:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 181
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\nScreenOffEnable:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofScreenOffEnable:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 182
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\nGestureEnable:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofGestureEnable:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 183
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\nScreenOnCount:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofScreenOnUsageCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 184
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\nScreenOffCount:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofScreenOffUsageCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 185
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\nGestureEventCount:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofGestureUsageCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 186
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\nappUsage:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofAppUsageEventsMap:Ljava/util/Map;

    invoke-direct {p0, v2}, Lcom/android/server/power/statistic/TofEventStatistic;->getTofAppUsageEvents(Ljava/util/Map;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 188
    const-string v1, "MiuiTofStatisticTracker"

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    return-void
.end method

.method public notifyGestureEvent(Ljava/lang/String;ZI)V
    .locals 2
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "success"    # Z
    .param p3, "label"    # I

    .line 155
    iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofGestureUsageCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofGestureUsageCount:I

    .line 157
    if-nez p1, :cond_0

    .line 158
    return-void

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofAppUsageEventsMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 162
    new-instance v0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;

    invoke-direct {v0, p0}, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;-><init>(Lcom/android/server/power/statistic/TofEventStatistic;)V

    .line 163
    .local v0, "appUsage":Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;
    invoke-virtual {v0, p1}, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->setPackageName(Ljava/lang/String;)V

    .line 164
    iget-object v1, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofAppUsageEventsMap:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 166
    .end local v0    # "appUsage":Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;
    :cond_1
    iget-object v0, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofAppUsageEventsMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;

    .line 167
    .restart local v0    # "appUsage":Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;
    invoke-virtual {v0, p2, p3}, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->setGestureEvent(ZI)V

    .line 168
    return-void
.end method

.method public notifyTofPowerState(Z)V
    .locals 1
    .param p1, "wakeup"    # Z

    .line 171
    if-eqz p1, :cond_0

    .line 172
    iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofScreenOnUsageCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofScreenOnUsageCount:I

    goto :goto_0

    .line 174
    :cond_0
    iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofScreenOffUsageCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofScreenOffUsageCount:I

    .line 176
    :goto_0
    return-void
.end method

.method public reportTofEventByIntent()V
    .locals 8

    .line 77
    const-string v0, "MiuiTofStatisticTracker"

    sget-boolean v1, Lcom/android/server/power/statistic/TofEventStatistic;->SUPPORT_TOF_GESTURE:Z

    if-nez v1, :cond_0

    sget-boolean v2, Lcom/android/server/power/statistic/TofEventStatistic;->SUPPORT_TOF_PROXIMITY:Z

    if-nez v2, :cond_0

    sget-boolean v2, Lcom/android/server/power/statistic/TofEventStatistic;->SUPPORT_AON_GESTURE:Z

    if-nez v2, :cond_0

    .line 78
    return-void

    .line 80
    :cond_0
    iget-object v2, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "miui_tof_screen_on"

    const/4 v4, 0x0

    const/4 v5, -0x2

    invoke-static {v2, v3, v4, v5}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    move v2, v3

    goto :goto_0

    :cond_1
    move v2, v4

    :goto_0
    iput-boolean v2, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofScreenOnEnable:Z

    .line 82
    iget-object v2, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v6, "miui_tof_screen_off"

    invoke-static {v2, v6, v4, v5}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v2

    if-eqz v2, :cond_2

    move v2, v3

    goto :goto_1

    :cond_2
    move v2, v4

    :goto_1
    iput-boolean v2, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofScreenOffEnable:Z

    .line 84
    iget-object v2, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v6, "miui_tof_gesture"

    invoke-static {v2, v6, v4, v5}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v2

    if-eqz v2, :cond_3

    move v2, v3

    goto :goto_2

    :cond_3
    move v2, v4

    :goto_2
    iput-boolean v2, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofGestureEnable:Z

    .line 86
    iget-object v2, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v6, "miui_aon_gesture"

    invoke-static {v2, v6, v4, v5}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v2

    if-eqz v2, :cond_4

    move v2, v3

    goto :goto_3

    :cond_4
    move v2, v4

    :goto_3
    iput-boolean v2, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mAonGestureEnable:Z

    .line 88
    iget-object v2, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v6, "miui_tof_gesture_cue_tone"

    const/4 v7, -0x1

    invoke-static {v2, v6, v7, v5}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v2

    if-eqz v2, :cond_5

    move v2, v3

    goto :goto_4

    :cond_5
    move v2, v4

    :goto_4
    iput-boolean v2, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofGestureCueToneEnable:Z

    .line 90
    iget-object v2, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v6, "miui_aon_gesture_cue_tone"

    invoke-static {v2, v6, v7, v5}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v2

    if-eqz v2, :cond_6

    move v4, v3

    :cond_6
    iput-boolean v4, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mAonGestureCueToneEnable:Z

    .line 92
    iget-object v2, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mOneTrackerHelper:Lcom/android/server/power/statistic/OneTrackerHelper;

    iget-object v3, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mAppId:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mContext:Landroid/content/Context;

    .line 93
    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    .line 92
    const-string/jumbo v5, "tof_statistic"

    invoke-virtual {v2, v3, v5, v4}, Lcom/android/server/power/statistic/OneTrackerHelper;->getTrackEventIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 94
    .local v2, "intent":Landroid/content/Intent;
    sget-boolean v3, Lcom/android/server/power/statistic/TofEventStatistic;->SUPPORT_TOF_PROXIMITY:Z

    if-eqz v3, :cond_7

    .line 95
    const-string/jumbo v3, "tof_screen_on_enable"

    iget-boolean v4, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofScreenOnEnable:Z

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v3

    iget-boolean v4, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofScreenOffEnable:Z

    .line 96
    const-string/jumbo v5, "tof_screen_off_enable"

    invoke-virtual {v3, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v3

    iget v4, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofScreenOnUsageCount:I

    .line 97
    const-string/jumbo v5, "tof_screen_on_count"

    invoke-virtual {v3, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v3

    iget v4, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofScreenOffUsageCount:I

    .line 98
    const-string/jumbo v5, "tof_screen_off_count"

    invoke-virtual {v3, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 101
    :cond_7
    if-eqz v1, :cond_8

    .line 102
    const-string/jumbo v1, "tof_gesture_enable"

    iget-boolean v3, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofGestureEnable:Z

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    iget v3, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofGestureUsageCount:I

    .line 103
    const-string/jumbo v4, "tof_gesture_count"

    invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    iget-boolean v3, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofGestureCueToneEnable:Z

    .line 104
    const-string/jumbo v4, "tof_gesture_cue_tone_enable"

    invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    .line 105
    invoke-direct {p0}, Lcom/android/server/power/statistic/TofEventStatistic;->getTofAppUsageEventsString()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "tof_app_usage_frequency"

    invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 108
    :cond_8
    sget-boolean v1, Lcom/android/server/power/statistic/TofEventStatistic;->SUPPORT_AON_GESTURE:Z

    if-eqz v1, :cond_9

    .line 109
    const-string v1, "aon_gesture_enable"

    iget-boolean v3, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mAonGestureEnable:Z

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    iget v3, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mTofGestureUsageCount:I

    .line 110
    const-string v4, "aon_gesture_count"

    invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    iget-boolean v3, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mAonGestureCueToneEnable:Z

    .line 111
    const-string v4, "aon_gesture_cue_tone_enable"

    invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    .line 112
    invoke-direct {p0}, Lcom/android/server/power/statistic/TofEventStatistic;->getTofAppUsageEventsString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "aon_app_usage_frequency"

    invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 115
    :cond_9
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "reportGestureEventByIntent: intent:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    iget-object v1, p0, Lcom/android/server/power/statistic/TofEventStatistic;->mOneTrackerHelper:Lcom/android/server/power/statistic/OneTrackerHelper;

    invoke-virtual {v1, v2}, Lcom/android/server/power/statistic/OneTrackerHelper;->reportTrackEventByIntent(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 119
    goto :goto_5

    .line 117
    :catch_0
    move-exception v1

    .line 118
    .local v1, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "reportTofEvent fail! "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_5
    invoke-direct {p0}, Lcom/android/server/power/statistic/TofEventStatistic;->resetTofEventData()V

    .line 121
    return-void
.end method
