.class Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;
.super Ljava/lang/Object;
.source "TofEventStatistic.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/power/statistic/TofEventStatistic;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AppUsage"
.end annotation


# instance fields
.field private mFailCircleNum:I

.field private mFailDoubleTapNum:I

.field private mFailDownNum:I

.field private mFailLeftNum:I

.field private mFailRightNum:I

.field private mFailUpNum:I

.field private mPackageName:Ljava/lang/String;

.field private mSuccessCircleNum:I

.field private mSuccessDoubleTapNum:I

.field private mSuccessDownNum:I

.field private mSuccessLeftNum:I

.field private mSuccessRightNum:I

.field private mSuccessUpNum:I

.field private mUsageCount:Ljava/lang/String;

.field final synthetic this$0:Lcom/android/server/power/statistic/TofEventStatistic;


# direct methods
.method public constructor <init>(Lcom/android/server/power/statistic/TofEventStatistic;)V
    .locals 0

    .line 207
    iput-object p1, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->this$0:Lcom/android/server/power/statistic/TofEventStatistic;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 209
    return-void
.end method

.method public constructor <init>(Lcom/android/server/power/statistic/TofEventStatistic;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "mPackageName"    # Ljava/lang/String;
    .param p3, "mUsageCount"    # Ljava/lang/String;

    .line 211
    iput-object p1, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->this$0:Lcom/android/server/power/statistic/TofEventStatistic;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 212
    iput-object p2, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mPackageName:Ljava/lang/String;

    .line 213
    iput-object p3, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mUsageCount:Ljava/lang/String;

    .line 214
    return-void
.end method


# virtual methods
.method public getFailCircleNum()I
    .locals 1

    .line 297
    iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailCircleNum:I

    return v0
.end method

.method public getFailDoubleTapNum()I
    .locals 1

    .line 305
    iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailDoubleTapNum:I

    return v0
.end method

.method public getFailDownNum()I
    .locals 1

    .line 273
    iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailDownNum:I

    return v0
.end method

.method public getFailLeftNum()I
    .locals 1

    .line 281
    iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailLeftNum:I

    return v0
.end method

.method public getFailRightNum()I
    .locals 1

    .line 289
    iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailRightNum:I

    return v0
.end method

.method public getFailUpNum()I
    .locals 1

    .line 265
    iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailUpNum:I

    return v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .line 313
    iget-object v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public getSuccessCircleNum()I
    .locals 1

    .line 249
    iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessCircleNum:I

    return v0
.end method

.method public getSuccessDoubleTapNum()I
    .locals 1

    .line 257
    iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessDoubleTapNum:I

    return v0
.end method

.method public getSuccessDownNum()I
    .locals 1

    .line 225
    iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessDownNum:I

    return v0
.end method

.method public getSuccessLeftNum()I
    .locals 1

    .line 233
    iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessLeftNum:I

    return v0
.end method

.method public getSuccessRightNum()I
    .locals 1

    .line 241
    iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessRightNum:I

    return v0
.end method

.method public getSuccessUpNum()I
    .locals 1

    .line 217
    iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessUpNum:I

    return v0
.end method

.method public getUsageCount()Ljava/lang/String;
    .locals 1

    .line 321
    iget-object v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mUsageCount:Ljava/lang/String;

    return-object v0
.end method

.method public reset()V
    .locals 1

    .line 329
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessUpNum:I

    .line 330
    iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessDownNum:I

    .line 331
    iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessLeftNum:I

    .line 332
    iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessRightNum:I

    .line 333
    iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessCircleNum:I

    .line 334
    iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessDoubleTapNum:I

    .line 335
    iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailUpNum:I

    .line 336
    iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailDownNum:I

    .line 337
    iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailLeftNum:I

    .line 338
    iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailRightNum:I

    .line 339
    iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailCircleNum:I

    .line 340
    iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailDoubleTapNum:I

    .line 341
    return-void
.end method

.method public setFailCircleNum(I)V
    .locals 0
    .param p1, "mFailCircleNum"    # I

    .line 301
    iput p1, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailCircleNum:I

    .line 302
    return-void
.end method

.method public setFailDoubleTapNum(I)V
    .locals 0
    .param p1, "mFailDoubleTapNum"    # I

    .line 309
    iput p1, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailDoubleTapNum:I

    .line 310
    return-void
.end method

.method public setFailDownNum(I)V
    .locals 0
    .param p1, "mFailDownNum"    # I

    .line 277
    iput p1, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailDownNum:I

    .line 278
    return-void
.end method

.method public setFailLeftNum(I)V
    .locals 0
    .param p1, "mFailLeftNum"    # I

    .line 285
    iput p1, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailLeftNum:I

    .line 286
    return-void
.end method

.method public setFailRightNum(I)V
    .locals 0
    .param p1, "mFailRightNum"    # I

    .line 293
    iput p1, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailRightNum:I

    .line 294
    return-void
.end method

.method public setFailUpNum(I)V
    .locals 0
    .param p1, "mFailUpNum"    # I

    .line 269
    iput p1, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailUpNum:I

    .line 270
    return-void
.end method

.method public setGestureEvent(ZI)V
    .locals 1
    .param p1, "success"    # Z
    .param p2, "label"    # I

    .line 344
    packed-switch p2, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 381
    :pswitch_1
    if-eqz p1, :cond_0

    .line 382
    iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessCircleNum:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessCircleNum:I

    goto :goto_0

    .line 384
    :cond_0
    iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailCircleNum:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailCircleNum:I

    .line 386
    goto :goto_0

    .line 374
    :pswitch_2
    if-eqz p1, :cond_1

    .line 375
    iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessDoubleTapNum:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessDoubleTapNum:I

    goto :goto_0

    .line 377
    :cond_1
    iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailDoubleTapNum:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailDoubleTapNum:I

    .line 379
    goto :goto_0

    .line 360
    :pswitch_3
    if-eqz p1, :cond_2

    .line 361
    iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessUpNum:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessUpNum:I

    goto :goto_0

    .line 363
    :cond_2
    iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailUpNum:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailUpNum:I

    .line 365
    goto :goto_0

    .line 367
    :pswitch_4
    if-eqz p1, :cond_3

    .line 368
    iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessDownNum:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessDownNum:I

    goto :goto_0

    .line 370
    :cond_3
    iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailDownNum:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailDownNum:I

    .line 372
    goto :goto_0

    .line 353
    :pswitch_5
    if-eqz p1, :cond_4

    .line 354
    iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessRightNum:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessRightNum:I

    goto :goto_0

    .line 356
    :cond_4
    iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailRightNum:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailRightNum:I

    .line 358
    goto :goto_0

    .line 346
    :pswitch_6
    if-eqz p1, :cond_5

    .line 347
    iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessLeftNum:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessLeftNum:I

    goto :goto_0

    .line 349
    :cond_5
    iget v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailLeftNum:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailLeftNum:I

    .line 351
    nop

    .line 390
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public setPackageName(Ljava/lang/String;)V
    .locals 0
    .param p1, "mPackageName"    # Ljava/lang/String;

    .line 317
    iput-object p1, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mPackageName:Ljava/lang/String;

    .line 318
    return-void
.end method

.method public setSuccessCircleNum(I)V
    .locals 0
    .param p1, "mSuccessCircleNum"    # I

    .line 253
    iput p1, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessCircleNum:I

    .line 254
    return-void
.end method

.method public setSuccessDoubleTapNum(I)V
    .locals 0
    .param p1, "mSuccessDoubleTapNum"    # I

    .line 261
    iput p1, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessDoubleTapNum:I

    .line 262
    return-void
.end method

.method public setSuccessDownNum(I)V
    .locals 0
    .param p1, "mSuccessDownNum"    # I

    .line 229
    iput p1, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessDownNum:I

    .line 230
    return-void
.end method

.method public setSuccessLeftNum(I)V
    .locals 0
    .param p1, "mSuccessLeftNum"    # I

    .line 237
    iput p1, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessLeftNum:I

    .line 238
    return-void
.end method

.method public setSuccessRightNum(I)V
    .locals 0
    .param p1, "mSuccessRightNum"    # I

    .line 245
    iput p1, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessRightNum:I

    .line 246
    return-void
.end method

.method public setSuccessUpNum(I)V
    .locals 0
    .param p1, "mSuccessUpNum"    # I

    .line 221
    iput p1, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessUpNum:I

    .line 222
    return-void
.end method

.method public setUsageCount(Ljava/lang/String;)V
    .locals 0
    .param p1, "mUsageCount"    # Ljava/lang/String;

    .line 325
    iput-object p1, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mUsageCount:Ljava/lang/String;

    .line 326
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    .line 394
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "{app:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",success:{up:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessUpNum:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",down:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessDownNum:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ",left:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessLeftNum:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ",right:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v4, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessRightNum:I

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ",circle:"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v5, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessCircleNum:I

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ",doubletap:"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v6, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mSuccessDoubleTapNum:I

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v6, "},fail:{up:"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v6, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailUpNum:I

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailDownNum:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailLeftNum:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailRightNum:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailCircleNum:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/power/statistic/TofEventStatistic$AppUsage;->mFailDoubleTapNum:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
